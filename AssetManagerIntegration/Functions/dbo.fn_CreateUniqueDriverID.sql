SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CreateUniqueDriverID] (@sBranch varchar(20),@sRunNumber varchar(20),@sProntoID varchar(20))
returns varchar(10)
as
begin
	Declare @sOutput as char(10) ='' 
	Select @sOutput = Rtrim(Ltrim(CASE @sBranch WHEN 'Adelaide' THEN 'AD' WHEN 'Brisbane' THEN 'BN' WHEN 'Gold Coast' THEN 'GL' WHEN 'Goldcoast' THEN 'GL' WHEN 'Sydney' THEN 'SY' WHEN 'Melbourne' THEN 'ME' WHEN 'Perth' THEN 'PE' WHEN 'Nkope' THEN 'NK' ELSE 'XX' END 
	                  +  dbo.fn_prefillZeros(ISnull(@sRunNumber,'XXXX'),4)
					  +  ISnull(@sProntoID,'XXXX')))

	Return @sOutput
end

GO
