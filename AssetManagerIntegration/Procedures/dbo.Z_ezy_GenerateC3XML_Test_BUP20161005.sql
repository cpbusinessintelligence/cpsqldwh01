SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Z_ezy_GenerateC3XML_Test_BUP20161005]
AS
DECLARE @c3XML xml
BEGIN TRAN

Declare @Temp table (ItemNumber varchar(50))

--IF OBJECT_ID ('temp..#C3XMLLabels') IS NOT NULL
--	DROP TABLE #C3XMLLables
--	CREATE TABLE #C3XMLLables
--		(
--			[ItemNumber] VARCHAR(50)
--		)

--	INSERT INTO #C3XMLLables([ItemNumber]) 

	Insert into @Temp(ItemNumber)
	SELECT 
		[ItemNumber] 
	FROM 
		[dbo].[DeliveryItemLabel] (NOLOCK)
	WHERE 
		Isprocessed = 0 and istest=1

--Generate XML Starts
	SET @c3XML = (SELECT 
	(SELECT 
	'REQ' + CONVERT(VARCHAR,GETDATE(),112) +  CONVERT(VARCHAR,DATEPART(hour,GETDATE())) +  CONVERT(VARCHAR,DATEPART(minute,GETDATE())) +  CONVERT(VARCHAR,DATEPART(SECOND,GETDATE())) +  REPLACE(STR(CAST(ROW_NUMBER() OVER (ORDER BY [SNo]) as varchar) , 5), SPACE(1), '0')  AS RequestID,
	ltrim(rtrim(SourceSystem)) as SourceSystem,
	ltrim(rtrim(ProcessingCode)) as ProcessingCode,
	ltrim(rtrim(DI.ItemNumber)) as ItemNumber,
	ltrim(rtrim(CNACardNumber)) as CNACardNumber,
	ltrim(rtrim(SLCNACardNumber)) as SLCNACardNumber,
	ltrim(rtrim(RTCNACardNumber)) as RTCNACardNumber,
	ltrim(rtrim([Weight])) as [Weight],
	ltrim(rtrim(ServiceType)) as ServiceType,
	ltrim(rtrim(TrafficIndicator)) as TrafficIndicator,
	ltrim(rtrim(ActualItemNumber)) as ActualItemNumber,
	ltrim(rtrim(BillToCustomerID)) as BillToCustomerID,
	ltrim(rtrim(BillToCustomerAccountNumber)) as BillToCustomerAccountNumber,
	ltrim(rtrim(BillToName)) as BillToName,
	ltrim(rtrim(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(DeliveryToCompanyName,'&',''),'<',''),'>',''),'"',''),'''',''),'/',''),':',''),'!',''),'.',''),'_',''),char(7),''),char(22),'')))  as DeliveryToCompanyName,
	ltrim(rtrim(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(DeliveryToName,'&',''),'<',''),'>',''),'"',''),'''',''),'/',''),':',''),'!',''),'.',''),'_',''),char(7),''),char(22),''))) as   DeliveryToName,
	ltrim(rtrim(DeliveryToRecipientID)) as DeliveryToRecipientID,
	ltrim(rtrim(DeliveryToPhone)) as DeliveryToPhone,
	ltrim(rtrim(DeliveryToMail)) as DeliveryToMail,
	ltrim(rtrim(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(DeliveryToAddress1,'&',''),'<',''),'>',''),'"',''),'''',''),'/',''),':',''),'!',''),'.',''),'_',''),char(7),''),char(22),'') )) as DeliveryToAddress1,
	ltrim(rtrim(DeliveryToAddress2)) as DeliveryToAddress2,
	ltrim(rtrim(DeliveryToAddress3)) as DeliveryToAddress3,
	ltrim(rtrim(DeliveryToCity)) as DeliveryToCity,
	ltrim(rtrim(DeliveryToAddressZip)) as DeliveryToAddressZip,
	ltrim(rtrim(DeliveryToState)) as DeliveryToState,
	ltrim(rtrim(DeliveryToCountryCode)) as DeliveryToCountryCode,
	ltrim(rtrim(DeliveryToCountryName)) as DeliveryToCountryName,
	ltrim(rtrim(DeliveryToPreferredFromTime)) as DeliveryToPreferredFromTime,
	ltrim(rtrim(DeliveryToPreferredToTime)) as DeliveryToPreferredToTime,
	ltrim(rtrim(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(Instructions,'&',''),'<',''),'>',''),'"',''),'''',''),'/',''),':',''),'!',''),'.',''),'_',''),char(7),''),char(22),'')))  as Instructions,
	ltrim(rtrim(IsATL)) as IsATL,
	ltrim(rtrim(IsRedelivery)) as IsRedelivery,
	ltrim(rtrim(RedeliveryDate)) as RedeliveryDate,
	ltrim(rtrim(Courierid))  As AgentOrVendorId
	FROM
	[dbo].[DeliveryItemLabel] DI(NOLOCK)
			INNER JOIN @Temp t
	ON DI.[ItemNumber] = t.[ItemNumber]
	FOR XML PATH('Delivery'), TYPE)
	FOR XML PATH(''))
--Generate XML Ends

	UPDATE	DIL
	SET DIL.IsProcessed = 1,
	DIL.EditedBy = 'C3Service',
	DIL.EditedDatetime = getdate()
	FROM 
	[dbo].[DeliveryItemLabel] DIL(NOLOCK)
		INNER JOIN 
	@Temp t ON
		DIL.[ItemNumber] = t.[ItemNumber]

IF @@ERROR <> 0
	BEGIN
		ROLLBACK
		RETURN
	END
COMMIT

--IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Temp') DROP TABLE Temp;

SELECT @c3XML as c3


GO
