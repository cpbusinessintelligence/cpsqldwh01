SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Z_sp_LoadDeliveryItemLabelStatus_Test_BUP20161005](@FileName varchar(100)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadDeliveryItemLabelStatus
    --' ---------------------------
    --' Purpose: sp_LoadDeliveryItemLabelStatus-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 21 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

truncate table DeliveryItemLabelStatusStaging

--declare @FileName varchar(100)
--set @FileName='DeliveryStatus0810201593832.xml'

declare @FilePath varchar(200)
Set @FilePath='E:\ETL Processing\CPEzyTrakIntegraion_Test\Import DeliveryItemLabelStatus from EzyTrak\Input files\Process\' + @FileName




DECLARE @x XML, @sql NVARCHAR(MAX);

SELECT @sql = N'SELECT @X = CONVERT(xml,BulkColumn) 
 FROM OPENROWSET(BULK ''' + @FilePath + ''' ,SINGLE_BLOB) as p1';

EXEC sp_executesql @sql, N'@x XML OUTPUT', @x OUTPUT;

INSERT INTO DeliveryItemLabelStatusStaging(XMLData, LoadedDateTime)
select @x ,getdate()

--INSERT INTO PickupItemLabelStatusStaging(XMLData, LoadedDateTime)
--SELECT CONVERT(XML, BulkColumn) AS BulkColumn, GETDATE() 
--FROM OPENROWSET(BULK @FilePath, SINGLE_BLOB) AS x;


DECLARE @x1 xml
select @x1 = XMLData from DeliveryItemLabelStatusStaging

  
 Insert into  DeliveryItemLabelStatus([RequestID]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,[CPItemStatusCode]
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID])


  
  select

   T.c.value('RequestID[1]','VARCHAR(100)'),
   T.c.value('ItemNumber[1]','VARCHAR(100)'),
   T.c.value('CNANumber[1]','VARCHAR(100)'),
   T.c.value('SLCNACardNumber[1]','VARCHAR(100)'),
     T.c.value('RTCNACardNumber[1]','VARCHAR(100)'),
   T.c.value('ServiceType[1]','VARCHAR(100)'),
   T.c.value('TrafficIndicator[1]','VARCHAR(100)'),
   T.c.value('ItemWeight[1]','VARCHAR(100)'),
   T.c.value('StatusCode[1]','VARCHAR(100)'),
   T.c.value('StatusDescription[1]','VARCHAR(100)'),
   T.c.value('ReasonCode[1]','VARCHAR(100)'),
  T.c.value('ReasonDescription[1]','VARCHAR(100)'),
   T.c.value('CPItemStatusCode[1]','VARCHAR(100)'),
   T.c.value('CPItemReasonCode[1]','VARCHAR(100)'),
   T.c.value('StatusDate[1]','VARCHAR(100)'),
   T.c.value('CourierID[1]','VARCHAR(100)'),
   T.c.value('NumberOfAttempts[1]','VARCHAR(100)'),
   T.c.value('DeliveryToCompanyName[1]','VARCHAR(100)'),
   T.c.value('DeliveryToName[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress1[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress2[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress3[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress4[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressPOBox[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressCity[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressState[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressZip[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressCountryCode[1]','VARCHAR(100)'),
 --  T.c.value('DeliveryToAddressCountryName[1]','VARCHAR(100)'),
   T.c.value('DeliveryInstructions[1]','VARCHAR(100)'),
   T.c.value('DLB[1]','VARCHAR(100)'),
   T.c.value('PopStationID[1]','VARCHAR(100)')
from
    @x1.nodes('/ROOT/DeliveryStatus') T(c)

	Update [dbo].[DeliveryItemLabelStatusStaging] set isprocessed=1 

Update [dbo].[DeliveryItemLabelStatus] set popstationname=[DeliveryChoiceName],
                                           popstationdescription=[DeliveryChoiceDescription],
										   popstationlocation=[Address3],
										   popstationaddress=[Address1]+[Address2]+[Address3]
										   from [dbo].[DeliveryChoices] where [DeliveryChoiceID]=popstationid
										   and [isScannerGWDataProcessed]=0


 Insert into [dbo].[DeliveryItemLabelStatus] ([RequestID]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,[CPItemStatusCode]
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID]
	  ,[popstationname]
      ,[popstationdescription]
      ,[popstationlocation]
      ,[popstationaddress]
      ,[istest])

	  Select replace([RequestID],'REQ','RES')
      ,[SLCNACardNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,[CPItemStatusCode]
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID]
	  ,[popstationname]
      ,[popstationdescription]
      ,[popstationlocation]
      ,[popstationaddress]
      ,[istest]

from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] where 
[isScannerGWDataProcessed]=0 and [CPItemStatusCode] ='Drop off in POPStation'  



Update [dbo].[DeliveryItemLabelStatus] set Reasondescription= case when  reasoncode='DZ101' then '(Parcel Oversize)' when reasoncode='DZ102' then '(Locker Full)' else '' end
where [isScannerGWDataProcessed]=0 


end
GO
