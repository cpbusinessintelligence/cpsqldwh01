SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Z_sp_LoadDeliveryItemLabel_Test_BUP_JP_20170622] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadDeliveryItemLabel
    --' ---------------------------
    --' Purpose: sp_LoadDeliveryItemLabel-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 21 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Begin transaction cpweb

Insert into DeliveryItemLabel_Test(
SourceSystem,
ProcessingCode,
ItemNumber,
--SiblingItemNumber,
CNACardNumber,
SLCNACardNumber,
RTCNACardNumber,
Weight,
ServiceType,
--AmountToCollect,
TrafficIndicator,
ActualItemNumber,
BillToCustomerID,
BillToCustomerAccountNumber,
--BillToCustomerCostCentreCode,
BillToName,
DeliveryToCompanyName,
DeliveryToName,
DeliveryToRecipientID,
DeliveryToPhone,
DeliveryToMail,
DeliveryToAddress1,
DeliveryToAddress2,
DeliveryToAddress3,
DeliveryToCity,
DeliveryToAddressZip,
DeliveryToState,
DeliveryToCountryCode,
DeliveryToCountryName,
DeliveryToPreferredFromTime,
DeliveryToPreferredToTime,
Instructions,
IsATL,
IsRedelivery,
DLB,
IsProcessed)

Select 
       'CP_WEB',
       'N' as ProcessingCode,
	   ltrim(rtrim(Labelnumber)),
--	   '',
       '',
	   '',
	   '',
	   l.physicalweight,
	   c.ratecardid,
	   'LCL',
	   '',
	   '112995527',
	   '112995527',
	   'Retail' as CustomerName,
	  a1.firstname+''+a1.lastname,
       a1.firstname+''+a1.lastname,
	   '',
	    a1.phone, 
	    a1.email,
	   a1.address1,
	    a1.address2,
	   '',
	   a1.suburb,
	   a1.postcode,
	   isnull(s1.StateCode,isnull(a1.statename,'Unknown')),
       a1.countrycode,
	   a1.country,
	  convert(varchar(20),ConsignmentPreferPickupDate)+'T'+convert(varchar(12),cast(ConsignmentPreferPickupTime as Time)),
	  convert(varchar(20),dateadd("day",4,ConsignmentPreferPickupDate))+'T'+convert(varchar(12),cast(ConsignmentPreferPickupTime as Time)),
	   replace(isnull(specialinstruction,''),',',''),
	   IsATL,
	   0,
	   '',
	   0
from  [cpsqldwh01].[EzyFreight].[dbo].[tblConsignment] c left join [cpsqldwh01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [cpsqldwh01].[EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=c.consignmentid
                                            left join [cpsqldwh01].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [cpsqldwh01].[EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [cpsqldwh01].[EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											left Join [cpsqldwh01].[DWH].[dbo].[Postcodes] p on p.postcode=a.postcode and p.suburb=a.suburb
											
where  (c.userid=2368 or c.userid=1920  or c.userid= 2603 or c.userid=4273 or c.userid=4275 or c.userid=6177) and
--convert(date,c.createddatetime)= convert(date,getdate()-1)
c.createddatetime>= dateadd(minute,-600,getdate()) and c.isinternational=0
and not exists(select ItemNumber from DeliveryItemLabel_Test where Itemnumber=l.labelnumber)  and labelnumber not like '%CNA'


Commit transaction cpweb



Begin transaction cpedi

Insert into DeliveryItemLabel_Test(
SourceSystem,
ProcessingCode,
ItemNumber,
--SiblingItemNumber,
CNACardNumber,
SLCNACardNumber,
RTCNACardNumber,
Weight,
ServiceType,
--AmountToCollect,
TrafficIndicator,
ActualItemNumber,
BillToCustomerID,
BillToCustomerAccountNumber,
--BillToCustomerCostCentreCode,
BillToName,
DeliveryToCompanyName,
DeliveryToName,
DeliveryToRecipientID,
DeliveryToPhone,
DeliveryToMail,
DeliveryToAddress1,
DeliveryToAddress2,
DeliveryToAddress3,
DeliveryToCity,
DeliveryToAddressZip,
DeliveryToState,
DeliveryToCountryCode,
DeliveryToCountryName,
DeliveryToPreferredFromTime,
DeliveryToPreferredToTime,
Instructions,
IsATL,
IsRedelivery,
DLB,
IsProcessed)

Select 
       'CP_EDIGW',
       'N' as ProcessingCode,
	   ltrim(rtrim(cd.cc_coupon)),
--	   '',
       '',
	   '',
	   '',
	   cd_deadweight,
	   cd_pricecode,
	   'LCL',
	   '',
	   cd_account,
	   cd_account,
	   d.shortname as CustomerName,
	   convert(varchar(100),(case when  isnull(cd_delivery_contact,'') ='' then isnull(cd_delivery_addr0,'') else isnull(cd_delivery_contact,'') end))   as cd_delivery_con,
       convert(varchar(100),(case when  isnull(cd_delivery_contact,'') ='' then isnull(cd_delivery_addr0,'') else isnull(cd_delivery_contact,'') end)) as cd_delivery_contact,
	   '',
	   cd_delivery_contact_phone, 
	   cd_delivery_email,
	   cd_delivery_addr1,
	   cd_delivery_addr2,
	   cd_delivery_addr3,
	   cd_Delivery_suburb,
	   cd_delivery_postcode,
	   case when b.b_name ='Adelaide' then 'SA' 
	        when b.b_name ='Brisbane' then 'QLD'
			when b.b_name ='Gold Coast' then 'QLD'
			when b.b_name ='Melbourne' then 'VIC'
			when b.b_name ='Sydney' then 'NSW'
			when b.b_name ='Perth' then 'WA'
            when b.b_name ='Tasmania' then 'TAS'
			when b.b_name ='Northern Kope' then 'NT'
			when b.b_name ='Canberra' then 'ACT'
			else 'Unknown' end,
       'AU',
	   'Australia',
	  convert(varchar(20),cast(cd_date as date))+'T'+convert(varchar(12),cast('7:15 am' as Time)),
	  convert(varchar(20),cast(dateadd("day",4,cd_Date) as date))+'T'+convert(varchar(12),cast('7:15 am' as Time)),
	   replace(isnull(cd_special_instructions,''),',',''),
	   0,
	   0,
	   '',
	   0

from  [cpsqldwh01].[cpplEDI].[dbo].[Consignment] c (NOLOCK) left join [cpsqldwh01].[cpplEDI].[dbo].[cdcoupon] cd (NOLOCK) on cc_consignment=cd_id
                                                   left join  [cpsqldwh01].[Pronto].[dbo].[ProntoDebtor] d (NOLOCK) on d.accountcode=cd_account
												   left join  [cpsqldwh01].[cpplEDI].[dbo].[Branchs] b (NOLOCK) on b.b_id=c.cd_deliver_branch
												   where  cd_company_id in (5,56) and convert(date,cd_date)=convert(date,getdate())
												   --where c.cd_Date>= dateadd(hour,-1,getdate()) 
												   and not exists(select ItemNumber from DeliveryItemLabel_Test where Itemnumber=cd.cc_coupon)
												   and cd.cc_coupon not like '%CNA' and  c.cd_account not in ('112995519','112995527')
											        and cc_coupon not like 'CPW%'
Commit transaction cpedi


Begin transaction cpprepaid

Insert into DeliveryItemLabel_Test(
SourceSystem,
ProcessingCode,
ItemNumber,
--SiblingItemNumber,
CNACardNumber,
SLCNACardNumber,
RTCNACardNumber,
Weight,
ServiceType,
--AmountToCollect,
TrafficIndicator,
ActualItemNumber,
BillToCustomerID,
BillToCustomerAccountNumber,
--BillToCustomerCostCentreCode,
BillToName,
DeliveryToCompanyName,
DeliveryToName,
DeliveryToRecipientID,
DeliveryToPhone,
DeliveryToMail,
DeliveryToAddress1,
DeliveryToAddress2,
DeliveryToAddress3,
DeliveryToCity,
DeliveryToAddressZip,
DeliveryToState,
DeliveryToCountryCode,
DeliveryToCountryName,
DeliveryToPreferredFromTime,
DeliveryToPreferredToTime,
courierid,
Instructions,
IsATL,
IsRedelivery,
DLB,
IsProcessed)

Select  
       'CP_CPN',
       'N' as ProcessingCode,
	   ltrim(rtrim(l.labelnumber)),
--	   '',
       '',
	   '',
	   '',
	    0,
	   '',
	   'LCL',
	   '',
	   '112526900',
	   '112526900',
	   '',
	   '',
       '',
	   '',
	   '', 
	   '',
	   '',
	   '',
	   '',
	   'Sydney',
	   '2000',
	   'NSW',
       'AU',
	   'Australia',
	  convert(varchar(20),'2015-09-30')+'T'+convert(varchar(12),cast('0:00 am' as Time)),
	  convert(varchar(20),'2015-09-30')+'T'+convert(varchar(12),cast('0:00 am' as Time)),
	   '',
	   '',
	   0,
	   0,
	   '',
	   0
 from  [cpsqldwh01].[ScannerGateway].[dbo].[Label] l (NOLOCK)
where l.createddate>= dateadd(minute,-120,getdate()) 
and isnumeric(labelnumber)=1 and len(labelnumber)=11 
and not exists(select ItemNumber from DeliveryItemLabel_Test where Itemnumber=labelnumber) 
and labelnumber not like '%CNA' and labelnumber in ('63900107401',
'63900107402',
'70700063512',
'63900107404',
'63900107405',
'63900107406',
'63900107407',
'63900107408',
'63900107409',
'63900107410',
'63900107411',
'63900107412',
'63900107413',
'63900107414',
'63900107415',
'63900107416',
'63900107417',
'63900107418',
'63900107419',
'63900107420',
'63900107421',
'63900107422',
'63900107423',
'63900107424',
'63900107425',
'63900107426',
'63900107427',
'63900107428',
'63900107429',
'63900107430',
'63900107431',
'63900107432',
'63900107433',
'63900107434',
'63900107435',
'63900107436',
'63900107437',
'63900107438',
'63900107439',
'63900107440',
'63900107441',
'63900107442',
'63900107443',
'63900107444',
'63900107445',
'63900107446',
'63900107447',
'63900107448',
'63900107449',
'63900107450',
'62901360931',
'62901360930',
'070700063502',
'070700063503',
'70700063507',
'70700063509')
 --and  labelnumber like '605%'

 Commit transaction cpprepaid

  update DeliveryItemLabel_test set DeliveryToAddress1 =a.address1 ,DeliveryToAddress2 =isnull(a.address2,'') ,DeliveryToAddress3 ='',[DeliveryToCity]=a.suburb,[DeliveryToState]=isnull(s.statecode,a.statename) ,processingcode='U',isprocessed=0 ,ParcelType = '3'
  from DeliveryItemLabel_test join ezyfreight.dbo.tblredirecteditemlabel l on l.labelnumber=itemnumber join ezyfreight.dbo.tblredirectedconsignment c    on c.reconsignmentid=l.reconsignmentid
                                                 join ezyfreight.dbo.tbladdress a on a.addressid=newdeliveryaddressid
												  left join ezyfreight.dbo.tblstate s on s.stateid=a.stateid
where  isnull(Address1,'') like 'C/- %POPStation%' and labelnumber in (select itemnumber from DeliveryItemLabel_Test)
and isnull(DeliveryToAddress1,'') not like 'C/- %POPStation%' 






Update DeliveryItemLabel_Test set DeliveryToAddressZip=(Select ltrim(rtrim(replace(replace(Sortingcode,char(10),''),char(13),'')))  from [dbo].[DeliveryChoices] where substring(DeliveryToAddress1,5,3)=[DeliveryChoiceID] and [Category] like '%POPStation%')
where SourceSystem ='CP_WEB' and isnull(DeliveryToAddress1,'') like 'C/- %POPStation%' 
--and itemnumber='CPWPDC00000207101'
and isprocessed=0


end
GO
