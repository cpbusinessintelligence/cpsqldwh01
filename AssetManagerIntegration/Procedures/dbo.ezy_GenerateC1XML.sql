SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ezy_GenerateC1XML]
AS
DECLARE @c1XML VARCHAR(MAX)
Declare @Temp table(OrderNumber varchar(50))
BEGIN TRAN



Insert into @Temp(Ordernumber)
SELECT 
		Top 100 
		[BookingNumber] 
	FROM 
		[dbo].[Booking] 
	WHERE 
		Isprocessed = 0


--IF OBJECT_ID ('temp..#C1XMLLabels') IS NOT NULL
--	DROP TABLE #C1XMLLables
--	CREATE TABLE #C1XMLLables
--		(
--			OrderNumber VARCHAR(50)
--		)

	--INSERT INTO #C1XMLLables([OrderNumber]) 

	--SELECT 
	--	Top 100 
	--	[BookingNumber] 
	--FROM 
	--	[dbo].[Booking] 
	--WHERE 
	--	Isprocessed = 0

--Generate XML Starts
	SET @c1XML = (SELECT 
	(SELECT
	  'REQ' + CONVERT(VARCHAR,GETDATE(),112) +  CONVERT(VARCHAR,DATEPART(hour,GETDATE())) +  CONVERT(VARCHAR,DATEPART(minute,GETDATE())) +  CONVERT(VARCHAR,DATEPART(SECOND,GETDATE())) +  REPLACE(STR(CAST(ROW_NUMBER() OVER (ORDER BY [BookingID]) as varchar) , 5), SPACE(1), '0')  AS RequestID
	  ,[BookingNumber] As OrderNumber
	  ,'N' AS ProcessingCode
	  ,[BookingDate] As OrderDate
	  ,[CustomercontactName] As OrderBy
      ,'' As OrderPreferredVehicleType
      ,'CP_Cosmos' AS SourceSystem
	  ,'PEC' AS ServiceType
      ,[Customercode]
      ,[CustomercontactName] As CustomerName
      ,[PickupCustomer] As PickupName
	  ,PickupAddress0 AS PickupAddress1
	  ,PickupAddress1 As PickupAddress2
	  ,PickupAddress2 As PickupAddress3
	  ,Pickupsuburb As PickupAddressCity
	  ,Branch As PickupAddressState
	  ,PickupPostcode As PickupAddressZip
	  ,'CP' As PickupCountryCode
	  ,'Australia' AS PickupCountryName
	  ,PickupTime As PickupDateFrom
	  ,PickupTime As PickupDateTo
	  ,0 AS IsATL
	  ,0 AS Shipperflag
	--,Costom + ' ' + CostomId AS PickupNotification
	  ,'' AS PickupNotification
FROM
	[dbo].[Booking] Book INNER JOIN 
	@Temp t ON
	Book.[BookingNumber] = t.[OrderNumber]
	FOR XML PATH('PickupOrder'), TYPE)
	FOR XML PATH(''), ROOT('ROOT'))
--Generate XML Ends

	UPDATE	Book
	SET Book.Isprocessed = 1
	FROM 
	[dbo].[Booking] Book
		INNER JOIN 
	@Temp t ON
		Book.[BookingNumber] = t.[OrderNumber]

IF @@ERROR <> 0
	BEGIN
		ROLLBACK
		RETURN
	END
COMMIT
SELECT @c1XML as c1




GO
GRANT EXECUTE
	ON [dbo].[ezy_GenerateC1XML]
	TO [z_SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[ezy_GenerateC1XML]
	TO [SSISUser]
GO
