SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE ezy_GetNextBookingNumber
(
@BookingNumber VARCHAR(100) OUTPUT
)
AS
DECLARE @getLastNo INT
SELECT 
	TOP 1 @getLastNo = SUBSTRING(BookingNumber,LEN(BookingNumber)-3,4)
FROM [dbo].[Booking]
WHERE CAST(BookingDate AS DATE) = CAST(GETDATE() AS DATE)
ORDER BY BookingId Desc

IF(@getLastNo >= 1)
	BEGIN 
		IF(LEN(CAST(@getLastNo + 1 As Varchar(4))) = 1)
			SET @BookingNumber =  'CP' + REPLACE(CONVERT(VARCHAR(10),GETDATE(),103),'/','') + '000' + CAST(@getLastNo + 1 AS VARCHAR(4))
		IF(LEN(CAST(@getLastNo + 1 As Varchar(4))) = 2)
			SET @BookingNumber =  'CP' + REPLACE(CONVERT(VARCHAR(10),GETDATE(),103),'/','') + '00' + CAST(@getLastNo + 1 AS VARCHAR(4))
		IF(LEN(CAST(@getLastNo + 1 As Varchar(4))) = 3)
			SET @BookingNumber =  'CP' + REPLACE(CONVERT(VARCHAR(10),GETDATE(),103),'/','') + '0' + CAST(@getLastNo + 1 AS VARCHAR(4))
		IF(LEN(CAST(@getLastNo + 1 As Varchar(4))) = 4)
			SET @BookingNumber =  'CP' + REPLACE(CONVERT(VARCHAR(10),GETDATE(),103),'/','') + CAST(@getLastNo + 1 AS VARCHAR(4))
	END
ELSE
	BEGIN
		SET @BookingNumber = 'CP' + REPLACE(CONVERT(VARCHAR(10),GETDATE(),103),'/','') + '0001'
	END
GO
GRANT EXECUTE
	ON [dbo].[ezy_GetNextBookingNumber]
	TO [SSISUser]
GO
