SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_ApplicationLogs]
@AppName varchar(20),
@EmployeeID varchar(20),
@EmployeeName varchar(200),
@Device varchar(200),
@Severity varchar(50),
@Section varchar(100),
@Description nvarchar(max),
@Category varchar(20),
@Reference1 varchar(50),
@Reference2 varchar(50),
@Reference3 varchar(50),
@Request varchar(max)


--@err_messagevarchar(max) OUTPUT


AS
BEGIN try
DECLARE @err_message nvarchar(255)=''



	IF (@AppName = '' OR @AppName  IS NULL)
            BEGIN
			set @err_message += 'Please enter AppName. '
			
			  END


	IF(@Severity NOT IN ('Error','Information','Warning') OR @Severity IS NULL)
	
	BEGIN
	SET @err_message += 'Please enter Error, Information or Warning for Severity. '
	
	END

	IF (@Section='' OR @Section IS NULL)
	 BEGIN
            set @err_message += 'Please enter Section. '
                
			  END
	IF (@Description ='' OR @Description  IS NULL)
	 BEGIN
             set @err_message += 'Please enter Description. '
              
			  END

			  IF (@Category NOT IN ('Technical', 'User') OR @Category IS NULL)
	
	 BEGIN
             set @err_message += 'Please enter Technical or User for Category. '
              	  
			  END

	

	

	 ELSE IF(@err_message IS NULL OR @err_message ='')
	 --IF(@AppName IS NOT NULL AND @Severity IS NOT NULL AND @Section IS NOT NULL AND @Description IS NOT NULL AND @Category IS NOT NULL)
BEGIN
	insert into ApplicationLogs(AppName,Employee_ID,Employee_Name,AddDate,Device,Severity,Section,Description,Category,Reference1,Reference2,Reference3,Request) 
	values (@AppName,@EmployeeID,@EmployeeName,GETDATE(),@Device,@Severity,@Section,@Description,@Category,@Reference1,@Reference2,@Reference3,@Request)
	set @err_message +='Success'
	select @err_message
END
	
		IF(@err_message IS NOT NULL)
	BEGIN
	select @err_message
	--return @err_message
	END
	
 END try
 BEGIN CATCH
 
         SET @err_message ='Invalid'
		 
		 select @err_message
		 
    END CATCH
GO
