SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveDeliveryItemLabel]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(CreatedDatetime) from [dbo].[DeliveryItemLabel]
PRINT @MinDAte
PRINT DATEADD(day, 1,@MinDate)
Select  @MaxDate= DATEADD(day, 153,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[DeliveryItemLabel_Archive]
	(
	 [SourceSystem]
      ,[ProcessingCode]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[Weight]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ActualItemNumber]
      ,[BillToCustomerID]
      ,[BillToCustomerAccountNumber]
      ,[BillToName]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToRecipientID]
      ,[DeliveryToPhone]
      ,[DeliveryToMail]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToCity]
      ,[DeliveryToAddressZip]
      ,[DeliveryToState]
      ,[DeliveryToCountryCode]
      ,[DeliveryToCountryName]
      ,[DeliveryToPreferredFromTime]
      ,[DeliveryToPreferredToTime]
      ,[Instructions]
      ,[IsATL]
      ,[IsRedelivery]
      ,[RedeliveryDate]
      ,[DLB]
      ,[IsProcessed]
      ,[CreatedDatetime]
      ,[CreatedBy]
      ,[EditedDatetime]
      ,[EditedBy]
      ,[Courierid]
      ,[istest]
      ,[ParcelType]
	  )
	SELECT 
      [SourceSystem]
      ,[ProcessingCode]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[Weight]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ActualItemNumber]
      ,[BillToCustomerID]
      ,[BillToCustomerAccountNumber]
      ,[BillToName]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToRecipientID]
      ,[DeliveryToPhone]
      ,[DeliveryToMail]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToCity]
      ,[DeliveryToAddressZip]
      ,[DeliveryToState]
      ,[DeliveryToCountryCode]
      ,[DeliveryToCountryName]
      ,[DeliveryToPreferredFromTime]
      ,[DeliveryToPreferredToTime]
      ,[Instructions]
      ,[IsATL]
      ,[IsRedelivery]
      ,[RedeliveryDate]
      ,[DLB]
      ,[IsProcessed]
      ,[CreatedDatetime]
      ,[CreatedBy]
      ,[EditedDatetime]
      ,[EditedBy]
      ,[Courierid]
      ,[istest]
      ,[ParcelType]
  FROM [AssetManagerIntegration].[dbo].[DeliveryItemLabel] (nolock) where CreatedDatetime >= @MinDate and  CreatedDatetime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [AssetManagerIntegration].[dbo].[DeliveryItemLabel] where  CreatedDatetime >= @MinDate and  CreatedDatetime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
