SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_DeliveryItemLabelArchive_SS(@CreatedDatetime Datetime)
AS

Begin Transaction Delivery

Insert into [dbo].[DeliveryItemLabel_Archive]
Select 
[SourceSystem]
      ,[ProcessingCode]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[Weight]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ActualItemNumber]
      ,[BillToCustomerID]
      ,[BillToCustomerAccountNumber]
      ,[BillToName]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToRecipientID]
      ,[DeliveryToPhone]
      ,[DeliveryToMail]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToCity]
      ,[DeliveryToAddressZip]
      ,[DeliveryToState]
      ,[DeliveryToCountryCode]
      ,[DeliveryToCountryName]
      ,[DeliveryToPreferredFromTime]
      ,[DeliveryToPreferredToTime]
      ,[Instructions]
      ,[IsATL]
      ,[IsRedelivery]
      ,[RedeliveryDate]
      ,[DLB]
      ,[IsProcessed]
      ,[CreatedDatetime]
      ,[CreatedBy]
      ,[EditedDatetime]
      ,[EditedBy]
      ,[Courierid]
      ,[istest]
      ,[ParcelType]
 from [dbo].[DeliveryItemLabel]
where CreatedDatetime <= @CreatedDatetime

Delete from [dbo].[DeliveryItemLabel]
where CreatedDatetime <= @CreatedDatetime

Commit Transaction Delivery

GO
