SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_EnquireEzyreturnValidation]
    -- Add the parameters for the stored procedure here
    @ItemNo nvarchar(30) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
IF EXISTS (select ItemNumber from DeliveryItemLabel where (ItemNumber=@ItemNo and ItemNumber like 'RN%'))
BEGIN
IF EXISTS (select StatusCode from DeliveryItemLabelStatus where (ItemNumber=@ItemNo and (StatusCode = 'DL' or StatusCode = 'RP')))
BEGIN
--return Invalid.
select distinct 'InValid' as IsValid from DeliveryItemLabel
END
 Else
 BEGIN
 --return Valid
select distinct 'Valid' as IsValid from DeliveryItemLabel where (ItemNumber=@ItemNo and ItemNumber like 'RN%')
END
END
ELSE
BEGIN
select distinct 'NotFound' as IsValid from DeliveryItemLabel
END
END

--Exec [dbo].[sp_EnquireEzyreturnValidation] 'RN00436027610001'
GO
