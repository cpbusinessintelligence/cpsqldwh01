SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_EnquireItemDetail]
    -- Add the parameters for the stored procedure here
    @ItemNo nvarchar(30) = null
	AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
declare @var1_DeliveryChoiceID varchar(50);
declare @var2_DeliveryChoiceID varchar(50);
declare @handlingFee1 varchar(20);
declare @handlingFee2 varchar(20);
	IF EXISTS (select SLCNACardNumber from DeliveryItemLabel where (ItemNumber=@ItemNo and SLCNACardNumber!=''))
BEGIN

select 
    @var1_DeliveryChoiceID = DeliveryChoiceID
from
     VW_DeliveryChoicesBeat
where
 BeatID = (select distinct Courierid from DeliveryItemLabel where ItemNumber=@ItemNo)

select @handlingFee1 = SettingDetails from Settings where SettingName='HandlingFee' 

select '2' as [TYPE], SLCNACardNumber as [USER_ID], DeliveryToName as FIRST_NAME, '' as LAST_NAME, DeliveryToPhone as MOBILE, DeliveryToMail as EMAIL, 
@var1_DeliveryChoiceID as POPSTATION_ID, Courierid as COURIER_ID, '0.000' as COD,'0' as GST, @handlingFee1 as HANDLING_FEE, '' as ORDER_NUMBER, BillToCustomerID as MERCHANT_ID
from DeliveryItemLabel
where ItemNumber = @ItemNo
END
ELSE
BEGIN

select 
    @var2_DeliveryChoiceID = DeliveryChoiceID
from
    VW_DeliveryChoicesBeat
where
 BeatID = (select distinct Courierid from DeliveryItemLabel where ItemNumber=@ItemNo)

select @handlingFee2 = SettingDetails from Settings where SettingName='HandlingFee' 

select '3' as [TYPE], SLCNACardNumber as [USER_ID], DeliveryToName as FIRST_NAME, '' as LAST_NAME, DeliveryToPhone as MOBILE, DeliveryToMail as EMAIL, 
@var2_DeliveryChoiceID as POPSTATION_ID, Courierid as COURIER_ID, '0.000' as COD, '0' as GST, @handlingFee2 as HANDLING_FEE, '' as ORDER_NUMBER, BillToCustomerID as MERCHANT_ID
from DeliveryItemLabel 
where ItemNumber = @ItemNo
END
END

GO
