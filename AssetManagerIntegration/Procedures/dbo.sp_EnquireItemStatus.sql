SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_EnquireItemStatus]
    -- Add the parameters for the stored procedure here
    @ItemNo nvarchar(30) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    -- Select statements for procedure here
select StatusDate,StatusCode,StatusDescription,popstationlocation,ReasonCode,ReasonDescription,DeliveryToName,NumberOfAttempts
from DeliveryItemLabelStatus where ItemNumber = @ItemNo
END

--exec [dbo].[sp_EnquireItemStatus] 'RN00436027610001'
GO
