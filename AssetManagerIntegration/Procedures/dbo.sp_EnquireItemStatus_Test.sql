SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_EnquireItemStatus_Test]
    -- Add the parameters for the stored procedure here
    @ItemNo nvarchar(30) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
select StatusDate,StatusCode,StatusDescription,popstationlocation,ReasonCode,ReasonDescription,DeliveryToName,NumberOfAttempts
from DeliveryItemLabelStatus_Test where ItemNumber = @ItemNo
END

--exec [dbo].[sp_EnquireItemStatus_Test] 'RN00436027610001'
GO
