SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetDeliveryItemLabel]

AS

BEGIN
--select del.SNo,del.DeliveryToPhone,del.DeliveryToMail, del.ItemNumber,del.SLCNACardNumber,del.CourierId ,beat.DeliveryChoiceID as POPStationId , 
--Case when del.DeliveryToAddress1 LIKE '%POPStation%' then 'POPStation' else del.DeliveryToAddress1 end as DeliveryToAddress1 from DeliveryItemLabel del LEFT OUTER JOIN VW_DeliveryChoicesBeat beat ON del.Courierid=beat.BeatID where  del.isProcessed = 0


--select del.SNo,del.DeliveryToPhone,del.DeliveryToMail, del.ItemNumber, del.SLCNACardNumber,Case when del.DeliveryToAddress1 LIKE '%A01%' then (SELECT min(BeatID) FROM VW_DeliveryChoicesBeat where deliverychoiceid = 'A01') else  del.CourierId end as CourierId,beat.DeliveryChoiceID as POPStationId , 
--Case when del.DeliveryToAddress1 LIKE '%POPStation%' then 'POPStation' else del.DeliveryToAddress1 end as DeliveryToAddress1 from DeliveryItemLabel del LEFT OUTER JOIN VW_DeliveryChoicesBeat beat ON del.Courierid=beat.BeatID where  del.isProcessed = 0

--select
--del.SNo,del.DeliveryToPhone,del.DeliveryToMail, del.ItemNumber, del.SLCNACardNumber
--,Case when (del.DeliveryToAddress1 LIKE '%C/- %' and del.DeliveryToAddress1 like '%POPStation%' and del.DeliveryToAddress1 like '%:%' and isnull(Courierid,'')='')
--then (SELECT min(BeatID) FROM VW_DeliveryChoicesBeat where deliverychoiceid = replace(substring(REPLACE(DeliveryToAddress1,'C/- ',''),1,charindex(':',REPLACE(DeliveryToAddress1,'C/- ',''))),':','')) 
--else  del.CourierId 
--end as CourierId
--,beat.DeliveryChoiceID as POPStationId 
--, Case when del.DeliveryToAddress1 LIKE '%POPStation%' 
--then 'POPStation' 
--else del.DeliveryToAddress1 
--end as DeliveryToAddress1 
--from DeliveryItemLabel del LEFT OUTER JOIN VW_DeliveryChoicesBeat beat ON del.Courierid=beat.BeatID 
--where  del.isProcessed = 0

select   del.SNo,del.DeliveryToPhone,del.DeliveryToMail, del.ItemNumber, del.SLCNACardNumber,del.ParcelType
,Case when (del.DeliveryToAddress1 LIKE '%C/- %' and del.DeliveryToAddress1 LIKE '%POPStation%' and del.DeliveryToAddress1 like '%:%' and isnull(Courierid,'')='')  
then (SELECT min(BeatID) FROM VW_DeliveryChoicesBeat where deliverychoiceid = replace(substring(REPLACE(DeliveryToAddress1,'C/- ',''),1,charindex(':',REPLACE(DeliveryToAddress1,'C/- ',''))),':',''))
            else  del.CourierId
            end as CourierId
,beat.DeliveryChoiceID as POPStationId 
,Case when del.DeliveryToAddress1 LIKE '%POPStation%' 
then 'POPStation' 
else del.DeliveryToAddress1 
end as DeliveryToAddress1 
--into #Temp1
from DeliveryItemLabel del LEFT OUTER JOIN VW_DeliveryChoicesBeat beat ON del.Courierid=beat.BeatID 
where  del.isProcessed = 0 -- and beat.DeliveryChoiceID  is  null and ISnull(Courierid,'') <>''
order by del.SNo asc

--Update DeliveryItemLabel SET IsProcessed = 99 where ItemNumber in (Select ItemNumber from #Temp1)


END


GO
