SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_GetDeliveryItemLabelStatus_Test]

AS


BEGIN
if EXISTS (select * from DeliveryItemlabelStatusItemNumberStaging_Test)
Delete  from DeliveryItemlabelStatusItemNumberStaging_Test

ELSE

--select del.ItemNumber,'' as  ConsignmentNumber,del.StatusDate,del.CPItemStatusCode,CASE when CourierID is NULL THEN '9407' ELSE concat(substring(ltrim(rtrim(isnull(CourierID,''))),3,4) , '-', case left(ltrim(rtrim(isnull(CourierID,''))),2) WHEN 'BN'  THEN 'Brisbane'   WHEN  'GL'   THEN 'Gold Coast'  WHEN 'SY'  THEN 'Sydney'   WHEN  'ME'   THEN 'Melbourne'  WHEN  'PE' THEN 'Perth'  WHEN  'NK'  THEN'Nkope' end) end  as Contractor ,del.popstationname from DeliveryItemLabelStatus_Test del
-- where isScannerGWDataProcessed='False'


select del.ItemNumber,'' as  ConsignmentNumber,del.StatusDate, case when del.CPItemStatusCode = 'Transit to Post Office' THEN 'Transit to Depot' ELSE del.CPItemStatusCode end as CPItemStatusCode ,
'9407' as Contractor ,case when del.CPItemStatusCode='Transit to Post Office' then 'Failed Delivery at POPStation'+isnull(del.ReasonDescription,'') else del.popstationname end as popstationname  from DeliveryItemLabelStatus_Test del
 where isScannerGWDataProcessed='False'


 
 Union all
   select L.SLCNACardNumber as ItemNumber,
          '' as  ConsignmentNumber,del.StatusDate ,  
		  del.CPItemStatusCode   ,
'9407' as Contractor ,
--          CASE when del.CourierID is NULL THEN '9407' ELSE concat(substring(ltrim(rtrim(isnull(del.CourierID,''))),3,4) , '-', case left(ltrim(rtrim(isnull(del.CourierID,''))),2) WHEN 'BN'  THEN 'Brisbane'   WHEN  'GL'   THEN 'Gold Coast'  WHEN 'SY'  THEN 'Sydney'   WHEN  'ME'   THEN 'Melbourne'  WHEN  'PE' THEN 'Perth'  WHEN  'NK'  THEN'Nkope' end) end  as Contractor,
          case when del.CPItemStatusCode='Transit to Post Office' then 'Failed Delivery at POPStation'+isnull(del.ReasonDescription,'') else del.popstationname end as popstationname  
   from DeliveryItemLabelStatus_Test del Join DeliveryItemLabel_Test L on del.ItemNumber = L.ItemNumber
   where del.CPItemStatusCode  = 'Drop off in POPStation'  
	 and L.SLCNACardNumber <>''
	 and isScannerGWDataProcessed='False' 

END
GO
