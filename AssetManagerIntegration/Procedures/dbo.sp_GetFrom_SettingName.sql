SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetFrom_SettingName]
AS
BEGIn
select SettingDetails as EmailFrom from Settings where SettingName='From'
select SettingDetails as EmailTo from Settings where SettingName='To'
select SettingDetails as EmailCC  from Settings where SettingName='cc'
select SettingDetails as EmailBcc  from Settings where SettingName='bcc'
END
GO
