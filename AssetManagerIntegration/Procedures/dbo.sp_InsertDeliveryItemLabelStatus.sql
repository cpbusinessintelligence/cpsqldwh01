SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[sp_InsertDeliveryItemLabelStatus]
   @ITEM_NO varchar(100) null,
   @SYS_REF_NO varchar(100) null,
   @STATUS_CODE varchar(100) null,
   @STATUS_DATETIME datetime null,
   @LocationCode varchar(100) null,
   @CourierID varchar(100) null,
   --@DeliveryToAddressZip varchar(10) null,
   --@DeliveryToPhone varchar(50) null,
   --@DeliveryToName varchar(100) null,
   @ReasonCode varchar(15) null
   AS
BEGIN
    SET NOCOUNT ON;
	IF EXISTS (select StatusCodeCP from StatusCodeMapping where StatusCode=@STATUS_CODE and StatusCodeCP is not null)
BEGIN
if EXISTS(select ItemNumber from DeliveryItemLabel where ItemNumber=@ITEM_NO)
Begin
IF EXISTS (select DeliveryChoiceID as PopStationID from DeliveryChoices where DeliveryChoiceID=@LocationCode)
BEGIN
insert into DeliveryItemLabelStatus (ItemNumber,RequestID,StatusCode, StatusDate, CPItemStatusCode, isScannerGWDataProcessed, CreatedDatetime,CreatedBy,PopStationID,popstationname,popstationdescription,popstationaddress,CourierID,ReasonCode)
values(@ITEM_NO, @SYS_REF_NO, @STATUS_CODE, @STATUS_DATETIME,(select StatusCodeCP from StatusCodeMapping where StatusCode=@STATUS_CODE),0,GETDATE(),'Admin',(select DeliveryChoiceID as PopStationID from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select DeliveryChoiceName as PopStationName from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select DeliveryChoiceDescription as PopStationDescription from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select Address1 as PopStationAddress from DeliveryChoices where DeliveryChoiceID=@LocationCode),@CourierID,@ReasonCode)
select 1 as IsitemNumberExist from DeliveryItemLabel where (ItemNumber=@ITEM_NO)
END
ELSE
BEGIN
insert into DeliveryItemLabelStatus (ItemNumber,RequestID,StatusCode, StatusDate, CPItemStatusCode, isScannerGWDataProcessed, CreatedDatetime,CreatedBy,PopStationID,popstationname,popstationdescription,popstationaddress,CourierID,ReasonCode)
values(@ITEM_NO, @SYS_REF_NO, @STATUS_CODE, @STATUS_DATETIME,(select StatusCodeCP from StatusCodeMapping where StatusCode=@STATUS_CODE),0,GETDATE(),'Admin', @LocationCode,(select DeliveryChoiceName as PopStationName from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select DeliveryChoiceDescription as PopStationDescription from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select Address1 as PopStationAddress from DeliveryChoices where DeliveryChoiceID=@LocationCode),@CourierID,@ReasonCode)
select 1 as IsitemNumberExist from DeliveryItemLabel where (ItemNumber=@ITEM_NO)
END
END
ELSE
BEGIN
select distinct 0 as IsitemNumberExist from DeliveryItemLabel
END
END
ELSE
BEGIN
select distinct 0 as IsitemNumberExist from DeliveryItemLabel
END
END






GO
