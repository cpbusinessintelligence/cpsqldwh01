SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[sp_InsertDeliveryItemLabelStatus_Test]
   @ITEM_NO varchar(100) null,
   @SYS_REF_NO varchar(100) null,
   @STATUS_CODE varchar(100) null,
   @STATUS_DATETIME datetime null,
   @LocationCode varchar(100) null,
   @CourierID varchar(100) null,
   --@DeliveryToAddressZip varchar(10) null,
   --@DeliveryToPhone varchar(50) null,
   --@DeliveryToName varchar(100) null,
   @ReasonCode varchar(15) null
   AS
BEGIN
SET NOCOUNT ON;
	IF EXISTS (select StatusCodeCP from StatusCodeMapping where StatusCode=@STATUS_CODE and StatusCodeCP is not null)
BEGIN
if EXISTS(select ItemNumber from DeliveryItemLabel_Test where ItemNumber=@ITEM_NO)
Begin
IF EXISTS (select DeliveryChoiceID as PopStationID from DeliveryChoices where DeliveryChoiceID=@LocationCode)
BEGIN
--If record exist for this location code in deliverychoices then Insert record to DeliveryItemLabelStatus table.
		insert into DeliveryItemLabelStatus_Test (ItemNumber,RequestID,StatusCode, StatusDate, CPItemStatusCode, isScannerGWDataProcessed, CreatedDatetime,CreatedBy,PopStationID,popstationname,popstationdescription,popstationaddress,CourierID,ReasonCode)
		values(@ITEM_NO, @SYS_REF_NO, @STATUS_CODE, @STATUS_DATETIME,(select StatusCodeCP from StatusCodeMapping where StatusCode=@STATUS_CODE),0,GETDATE(),'Admin',(select DeliveryChoiceID as PopStationID from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select DeliveryChoiceName as PopStationName from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select DeliveryChoiceDescription as PopStationDescription from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select Address1 as PopStationAddress from DeliveryChoices where DeliveryChoiceID=@LocationCode),@CourierID,@ReasonCode)
		select 1 as IsitemNumberExist from DeliveryItemLabel_Test where (ItemNumber=@ITEM_NO)
		----
       Insert into [Redirection].[dbo].[SendMail]([ContextID]
			  ,[FromAddr]
			  ,[ToAddr]
			  ,[BCC]
			  ,[TransmitFlag]
			  ,[Parameter1]
			  ,[Parameter2]
			  ,[Parameter3]
			  ,[Parameter4]
			 ,[parameter5]
		   ,[parameter6])
	
	  Select  distinct 10,
				   'noreply@couriersplease.com.au',
				   Redirection.[dbo].[fn_CleanseEmail](Deliverytomail),
				   'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
				  -- 'kirsty.tuffley@couriersplease.com.au',
				   --Redirection.[dbo].[fn_CleanseEmail](Deliverytomail),
				   0,
				   ls.ItemNumber,
				   l.SLCNACardNumber,
				   ls.Popstationname,
				   isnull([Address3],'')+'<br/>'+isnull([Address2],'')+'<br/>'+isnull([Address1],'') ,
				   [Operation Hours],
				--   'kirsty.tuffley@couriersplease.com.au',
				 Redirection.[dbo].[fn_CleanseEmail](Deliverytomail)
	  

			from AssetManagerIntegration.[dbo].[DeliveryItemLabelStatus_Test] ls(NOLOCK) join Deliveryitemlabel_test l(NOLOCK) on l.itemnumber=ls.itemnumber 
																		  join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=l.itemnumber
																		  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
																		  join [AssetManagerIntegration].[dbo].deliverychoices(NOLOCK) on deliverychoiceid=popstationid
			where [CPItemStatusCode] ='Drop off in POPStation' and Redirection.dbo.[fn_ValidateEmail](Redirection.[dbo].[fn_CleanseEmail](Deliverytomail))=1
			 and l.itemnumber in (Select labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
			and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=10 and isactive=1)
			and ls.ItemNumber not in (Select parameter1 from Redirection.dbo.sendmail where contextid=10)
			and ls.ItemNumber = @ITEM_NO

		----
Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
     ,[parameter4]
	 ,[Parameter5])

Select  distinct  11,
       'noreply@couriersplease.com.au',

	  Redirection.dbo.[fn_CleansePhonenumber](l.Deliverytophone)+'@preview.pcsms.com.au',
	  'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',

	   0,
	   ls.ItemNumber,
       l.SLCNACardNumber,
	   ls.popstationid,
	   

	   Redirection.dbo.[fn_CleansePhonenumber](l.Deliverytophone),
	   deliverychoicedescription as POPStationName
from AssetManagerIntegration.[dbo].[DeliveryItemLabelStatus_test] ls(NOLOCK) join Deliveryitemlabel_test l(NOLOCK) on l.itemnumber=ls.itemnumber 
                                                              join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=l.itemnumber
															  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															  join AssetManagerIntegration.[dbo].deliverychoices(NOLOCK) on deliverychoiceid=popstationid
where [CPItemStatusCode] ='Drop off in POPStation'
 and Redirection.dbo.[fn_ValidateEmail](Redirection.[dbo].[fn_CleanseEmail](Deliverytomail))<>1 
 and Redirection.dbo.[fn_ValidatePhone](Redirection.dbo.[fn_CleansePhonenumber](l.Deliverytophone))=1

and   ls.itemnumber in (Select labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=11 and isactive=1)
and ls.ItemNumber not in (Select parameter1 from Redirection.dbo.sendmail where contextid=11)
and ls.ItemNumber = @ITEM_NO
------


END
ELSE
BEGIN
--If no record exist for this location code in deliverychoices then Insert only location code to the DeliveryItemLabelStatus table PopStationId field.
insert into DeliveryItemLabelStatus_Test (ItemNumber,RequestID,StatusCode, StatusDate, CPItemStatusCode, isScannerGWDataProcessed, CreatedDatetime,CreatedBy,PopStationID,popstationname,popstationdescription,popstationaddress,CourierID,ReasonCode)
values(@ITEM_NO, @SYS_REF_NO, @STATUS_CODE, @STATUS_DATETIME,(select StatusCodeCP from StatusCodeMapping where StatusCode=@STATUS_CODE),0,GETDATE(),'Admin', @LocationCode,(select DeliveryChoiceName as PopStationName from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select DeliveryChoiceDescription as PopStationDescription from DeliveryChoices where DeliveryChoiceID=@LocationCode),(select Address1 as PopStationAddress from DeliveryChoices where DeliveryChoiceID=@LocationCode),@CourierID,@ReasonCode)
select 1 as IsitemNumberExist from DeliveryItemLabel_Test where (ItemNumber=@ITEM_NO)
END
END
ELSE
BEGIN
select distinct 0 as IsitemNumberExist from DeliveryItemLabel_Test
END
END
ELSE
BEGIN
select distinct 0 as IsitemNumberExist from DeliveryItemLabel_Test
END
END





GO
