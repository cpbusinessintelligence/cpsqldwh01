SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadDeliveryItemLabelStatus](@FileName varchar(100)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadDeliveryItemLabelStatus
    --' ---------------------------
    --' Purpose: sp_LoadDeliveryItemLabelStatus-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 21 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

truncate table DeliveryItemLabelStatusStaging

--declare @FileName varchar(100)
--set @FileName='DeliveryStatus0810201593832.xml'

declare @FilePath varchar(200)

Set @FilePath='E:\ETL Processing\CPEzyTrakIntegraion\Import DeliveryItemLabelStatus from EzyTrak\Input files\Process\' + @FileName




DECLARE @x XML, @sql NVARCHAR(MAX);

SELECT @sql = N'SELECT @X = CONVERT(xml,BulkColumn) 
 FROM OPENROWSET(BULK ''' + @FilePath + ''' ,SINGLE_BLOB) as p1';

EXEC sp_executesql @sql, N'@x XML OUTPUT', @x OUTPUT;

INSERT INTO DeliveryItemLabelStatusStaging(XMLData, LoadedDateTime)
select @x ,getdate()

--INSERT INTO PickupItemLabelStatusStaging(XMLData, LoadedDateTime)
--SELECT CONVERT(XML, BulkColumn) AS BulkColumn, GETDATE() 
--FROM OPENROWSET(BULK @FilePath, SINGLE_BLOB) AS x;


DECLARE @x1 xml
select @x1 = XMLData from DeliveryItemLabelStatusStaging

  
 Insert into  DeliveryItemLabelStatus([RequestID]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,[CPItemStatusCode]
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID])


  
  select

   T.c.value('RequestID[1]','VARCHAR(100)'),
   T.c.value('ItemNumber[1]','VARCHAR(100)'),
   T.c.value('CNANumber[1]','VARCHAR(100)'),
   T.c.value('SLCNACardNumber[1]','VARCHAR(100)'),
     T.c.value('RTCNACardNumber[1]','VARCHAR(100)'),
   T.c.value('ServiceType[1]','VARCHAR(100)'),
   T.c.value('TrafficIndicator[1]','VARCHAR(100)'),
   T.c.value('ItemWeight[1]','VARCHAR(100)'),
   T.c.value('StatusCode[1]','VARCHAR(100)'),
   T.c.value('StatusDescription[1]','VARCHAR(100)'),
   T.c.value('ReasonCode[1]','VARCHAR(100)'),
  T.c.value('ReasonDescription[1]','VARCHAR(100)'),
   T.c.value('CPItemStatusCode[1]','VARCHAR(100)'),
   T.c.value('CPItemReasonCode[1]','VARCHAR(100)'),
   T.c.value('StatusDate[1]','VARCHAR(100)'),
   T.c.value('CourierID[1]','VARCHAR(100)'),
   T.c.value('NumberOfAttempts[1]','VARCHAR(100)'),
   T.c.value('DeliveryToCompanyName[1]','VARCHAR(100)'),
   T.c.value('DeliveryToName[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress1[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress2[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress3[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddress4[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressPOBox[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressCity[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressState[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressZip[1]','VARCHAR(100)'),
   T.c.value('DeliveryToAddressCountryCode[1]','VARCHAR(100)'),
 --  T.c.value('DeliveryToAddressCountryName[1]','VARCHAR(100)'),
   T.c.value('DeliveryInstructions[1]','VARCHAR(100)'),
   T.c.value('DLB[1]','VARCHAR(100)'),
   T.c.value('PopStationID[1]','VARCHAR(100)')
from
    @x1.nodes('/ROOT/DeliveryStatus') T(c)

	Update [dbo].[DeliveryItemLabelStatusStaging] set isprocessed=1 

Update [dbo].[DeliveryItemLabelStatus] set popstationname=[DeliveryChoiceName],
                                           popstationdescription=[DeliveryChoiceDescription],
										   popstationlocation=[Address3],
										   popstationaddress=isnull([Address1],'')+isnull([Address2],'')+isnull([Address3],'')
										   from [dbo].[DeliveryChoices] where [DeliveryChoiceID]=popstationid
										   and [isScannerGWDataProcessed]=0

								



Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
     ,[parameter5]
,[parameter6])



	  Select  distinct 10,
       'noreply@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleanseEmail](Deliverytomail),
	   'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	  -- 'kirsty.tuffley@couriersplease.com.au',
	   --Redirection.[dbo].[fn_CleanseEmail](Deliverytomail),
	   0,
	   ls.ItemNumber,
     ls.SLCNACardNumber,
	   ls.Popstationname,
	   isnull([Address3],'')+'<br/>'+isnull([Address2],'')+'<br/>'+isnull([Address1],'') ,
	   [Operation Hours],
	--   'kirsty.tuffley@couriersplease.com.au',
     Redirection.[dbo].[fn_CleanseEmail](Deliverytomail)
	  

from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] ls(NOLOCK) join Deliveryitemlabel l(NOLOCK) on l.itemnumber=ls.itemnumber 
                                                              join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=l.itemnumber
															  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															  join [EzyTrak Integration].[dbo].deliverychoices(NOLOCK) on deliverychoiceid=popstationid
where [CPItemStatusCode] ='Drop off in POPStation' and Redirection.dbo.[fn_ValidateEmail](Redirection.[dbo].[fn_CleanseEmail](Deliverytomail))=1
and  ls.ItemNumber not like '%SLCNA%' and ls.itemnumber in (Select labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and [isScannerGWDataProcessed]=0 
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=10 and isactive=1)
and ls.statusdate>='2016-08-12 06:00:00.000' and ls.ItemNumber not in (Select parameter1 from Redirection.dbo.sendmail where contextid=10)
and convert(Date,ls.statusdate)>=convert(date,dateadd(day,-1,getdate()))


Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
     ,[parameter4]
	 ,[Parameter5])

Select  distinct  11,
       'noreply@couriersplease.com.au',
	 --  '0466419484@preview.pcsms.com.au',
	  Redirection.dbo.[fn_CleansePhonenumber](l.Deliverytophone)+'@preview.pcsms.com.au',
	  'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   --Redirection.dbo.[fn_CleansePhonenumber](Deliverytophone)+'@preview.pcsms.com.au',
	   0,
	   ls.ItemNumber,
       ls.SLCNACardNumber,
	   ls.popstationid,
	   
	  -- '0466419484',
	   Redirection.dbo.[fn_CleansePhonenumber](l.Deliverytophone),
	   deliverychoicedescription as POPStationName
from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] ls(NOLOCK) join Deliveryitemlabel l(NOLOCK) on l.itemnumber=ls.itemnumber 
                                                              join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=l.itemnumber
															  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															  join [EzyTrak Integration].[dbo].deliverychoices(NOLOCK) on deliverychoiceid=popstationid
where [CPItemStatusCode] ='Drop off in POPStation' and Redirection.dbo.[fn_ValidateEmail](Redirection.[dbo].[fn_CleanseEmail](Deliverytomail))<>1 and Redirection.dbo.[fn_ValidatePhone](Redirection.dbo.[fn_CleansePhonenumber](l.Deliverytophone))=1
--and ls.ItemNumber='056446218640'
and  ls.ItemNumber not like '%SLCNA%' and ls.itemnumber in (Select labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=11 and isactive=1)
and [isScannerGWDataProcessed]=0 and ls.ItemNumber not in (Select parameter1 from Redirection.dbo.sendmail where contextid=11)
and convert(Date,ls.statusdate)>=convert(date,dateadd(day,-1,getdate()))

 Insert into [dbo].[DeliveryItemLabelStatus] ([RequestID]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,[CPItemStatusCode]
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID]
	  ,[popstationname]
      ,[popstationdescription]
      ,[popstationlocation]
      ,[popstationaddress]
      ,[istest])

	  Select replace([RequestID],'REQ','RES')
      ,[SLCNACardNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,[CPItemStatusCode]
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID]
	  ,[popstationname]
      ,[popstationdescription]
      ,[popstationlocation]
      ,[popstationaddress]
      ,[istest]

from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] where 
[isScannerGWDataProcessed]=0 and [CPItemStatusCode] ='Drop off in POPStation'  and [ItemNumber] not like '%SLCNA%'
and (SLCNACardNumber<>'' or CNACardNumber<>'' or RTCNACardNumber<>'')
and replace([RequestID],'REQ','RES') not in (select RequestID from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] )



 Insert into [dbo].[DeliveryItemLabelStatus] ([RequestID]
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,[CPItemStatusCode]
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID]
	  ,[popstationname]
      ,[popstationdescription]
      ,[popstationlocation]
      ,[popstationaddress]
      ,[istest])

	  Select replace([RequestID],'REQ','RES')
      ,[ItemNumber]
      ,[CNACardNumber]
      ,[SLCNACardNumber]
      ,[RTCNACardNumber]
      ,[ServiceType]
      ,[TrafficIndicator]
      ,[ItemWeight]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonCode]
      ,[ReasonDescription]
      ,'Delivered'
      ,[CPItemReasonCode]
      ,[StatusDate]
      ,[CourierID]
      ,[NumberOfAttempts]
      ,[DeliveryToCompanyName]
      ,[DeliveryToName]
      ,[DeliveryToAddress1]
      ,[DeliveryToAddress2]
      ,[DeliveryToAddress3]
      ,[DeliveryToAddress4]
      ,[DeliveryToAddressPOBox]
      ,[DeliveryToAddressCity]
      ,[DeliveryToAddressState]
      ,[DeliveryToAddressZip]
      ,[DeliveryToAddressCountryCode]
    --  ,[DeliveryToAddressCountryName]
      ,[DeliveryInstructions]
      ,[DLB]
	  ,[PopStationID]
	  ,''
      ,''
      ,[popstationlocation]
      ,[popstationaddress]
      ,[istest]

from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] where 
--itemnumber='CPA5SLT0022045'
 [isScannerGWDataProcessed]=0 
and [CPItemStatusCode] ='Delivered By POPStation'  and [ItemNumber] not like '%SLCNA%'
and replace([RequestID],'REQ','RES') not in (select RequestID from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] )


--itemnumber in ('CPAOXLZ1022670001',
--'CPALS5S0018918',
--'CPAR45T1016187') and [CPItemStatusCode] ='Drop off in POPStation'  

Update [dbo].[DeliveryItemLabelStatus] set Reasondescription= case when  reasoncode='DZ101' then '(Parcel Oversize)' when reasoncode='DZ102' then '(Locker Full)' else '' end
where [isScannerGWDataProcessed]=0 



end
GO
GRANT ALTER
	ON [dbo].[sp_LoadDeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT CONTROL
	ON [dbo].[sp_LoadDeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_LoadDeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[sp_LoadDeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_LoadDeliveryItemLabelStatus]
	TO [SSISUser]
GO
