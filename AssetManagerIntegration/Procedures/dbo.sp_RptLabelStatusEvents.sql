SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptLabelStatusEvents](@Labelnumber varchar(100)) as 
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_RptLabelStatusEvents
    --' ---------------------------
    --' Purpose: sp_RptLabelStatusEvents-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 30 Oct 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 30/10/2015    AK      1.00    Created the procedure                            

    --'=====================================================================


  Create table #temp (Label varchar(100),Status varchar(max),ScanDateandTime datetime)

  Insert into #temp 
  Select sourcereference,case when e.description='Link Scan' then Additionaltext1 else e.description end,t.eventdatetime
  from  [cpsqldwh01].[ScannerGateway].[dbo].[trackingevent] t (NOLOCK) join [cpsqldwh01].[ScannerGateway].[dbo].[eventtype] e (NOLOCK) on e.id=t.eventtypeid
  where t.sourcereference=@Labelnumber

  Insert into #temp
  Select ItemNumber,'New status sent to EzyTrak',[CreatedDatetime]
  from [EzyTrak Integration].[dbo].[DeliveryItemLabel] where [ItemNumber]=@Labelnumber

   Insert into #temp
  Select ItemNumber,'Update sent to EzyTrak with SLCNA '+ convert(varchar(12),[SLCNACardNumber]) ,[EditedDatetime]
  from [EzyTrak Integration].[dbo].[DeliveryItemLabel] where [ItemNumber]=@Labelnumber

  Insert into #temp
  Select Labelnumber,'Consignment Created for '+@Labelnumber,c.createddatetime
  from [cpsqldwh01].[EzyFreight].[dbo].tblitemlabel l join [cpsqldwh01].[EzyFreight].[dbo].tblconsignment c on c.consignmentid=l.consignmentid where @Labelnumber like 'CPW%' and  l.labelnumber=@Labelnumber

    Insert into #temp
  Select Labelnumber,'Label Created for '+@Labelnumber,l.createddate
  from [cpsqldwh01].[ScannerGateway].[dbo].[Label]  l  where l.labelnumber=@Labelnumber and  isnumeric(@Labelnumber)=1 and len(@Labelnumber)=11 

  
   Insert into #temp
   Select Itemnumber,[CPItemStatusCode]+case when [CPItemStatusCode] like '%POP%' then isnull(POPStationname,'') else '' end+'(Status Received from EzyTrak)',createddatetime
   from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] where [ItemNumber]=@Labelnumber


      Insert into #temp
   Select Itemnumber,[CPItemStatusCode]+'(Status Sent to HUB)',editeddatetime
   from [EzyTrak Integration].[dbo].[DeliveryItemLabelStatus] where [ItemNumber]=@Labelnumber and isScannerGWDataProcessed=1



   Select * from #temp order by ScanDateandTime

  end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptLabelStatusEvents]
	TO [SSISUser]
GO
