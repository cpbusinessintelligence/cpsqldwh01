SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

  CREATE PROCEDURE [dbo].[sp_UpdateDeliveryChoicesBeat]
    @BeatID varchar(50)
AS
BEGIN
    SET NOCOUNT ON;

    --update  [cpsqldev01].[SAMPLETESTDB].[dbo].DeliveryChoicesBeat
	update [cpsqlops01].[CouponCalculator].[dbo].[DeliveryChoicesBeat]
    SET IsProcess = 'True', UpdatedDateTime = GETDATE(),UpdatedBy='Admin'
	WHERE BeatID=@BeatID
END
GO
