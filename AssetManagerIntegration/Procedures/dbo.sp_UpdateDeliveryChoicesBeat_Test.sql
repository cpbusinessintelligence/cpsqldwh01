SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

  create PROCEDURE [dbo].[sp_UpdateDeliveryChoicesBeat_Test]
    @BeatID varchar(50)
AS
BEGIN
    SET NOCOUNT ON;

    update  [cpsqldev01].[SAMPLETESTDB].[dbo].DeliveryChoicesBeat
    SET IsProcess = 'True', UpdatedDateTime = GETDATE(),UpdatedBy='Admin'
	WHERE BeatID=@BeatID
END
GO
