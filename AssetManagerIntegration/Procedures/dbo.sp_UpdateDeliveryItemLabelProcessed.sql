SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateDeliveryItemLabelProcessed]
	--@ItemNumber varchar(100)
	@SNo int,
	@CourierId varchar(200)
AS
BEGIN
    SET NOCOUNT ON;

    update DeliveryItemLabel
    SET isProcessed = 'True' , Courierid=@CourierId, EditedDatetime=GETDATE() , EditedBy='Admin'
	WHERE SNo=@SNo
END
GO
