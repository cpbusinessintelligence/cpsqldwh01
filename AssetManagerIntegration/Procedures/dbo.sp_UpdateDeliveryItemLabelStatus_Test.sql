SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateDeliveryItemLabelStatus_Test]
   @ItemNumber varchar(100),
   @RequestID varchar(100),
   @CreatedDateTime datetime
AS
BEGIN
    SET NOCOUNT ON;

 update DeliveryItemLabelStatus_Test
   SET isScannerGWDataProcessed  = 'True', EditedDateTime  = GETDATE() , EditedBy ='Admin' where ItemNumber=@ItemNumber AND RequestID=@RequestID AND CreatedDatetime=@CreatedDateTime AND isScannerGWDataProcessed='False' 
	 --(select del.ItemNumber as TrackingNumber ,'Null' as  ConsignmentNumber,del.StatusDate as StatusDateTime ,SC.StatusCodeCP as ScanEvent ,'9407-Sydney' as Contractor ,del.popstationname as ExceptionReason from DeliveryItemLabelStatus del
--INNER JOIN  StatusCodeMapping SC ON del.StatusCode=SC.StatusCode where isScannerGWDataProcessed='False')

if exists(select * from DeliveryItemlabelStatusItemNumberStaging_Test)
delete from DeliveryItemlabelStatusItemNumberStaging_Test
END
GO
