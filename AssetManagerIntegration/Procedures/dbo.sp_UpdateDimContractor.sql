SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateDimContractor]
    @BeatID varchar(50)
AS
BEGIN
    SET NOCOUNT ON;

    update DimContractor
    SET IsBeatProcessed = 'True', BeatProcessedDateTime = GETDATE()
	WHERE BeatID=@BeatID
END
GO
