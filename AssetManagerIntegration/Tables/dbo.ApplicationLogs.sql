SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicationLogs] (
		[ID]                [int] IDENTITY(1, 1) NOT NULL,
		[AppName]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Employee_ID]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Employee_Name]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[AddDate]           [datetime] NOT NULL,
		[Device]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Severity]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Section]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Description]       [text] COLLATE Latin1_General_CI_AS NOT NULL,
		[Category]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Reference1]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Reference2]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Reference3]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Request]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ApplicationLogs]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ApplicationLogs] SET (LOCK_ESCALATION = TABLE)
GO
