SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryItemLabel] (
		[SNo]                             [int] IDENTITY(1, 1) NOT NULL,
		[SourceSystem]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ProcessingCode]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ItemNumber]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CNACardNumber]                   [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[SLCNACardNumber]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[RTCNACardNumber]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Weight]                          [decimal](18, 5) NULL,
		[ServiceType]                     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[TrafficIndicator]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ActualItemNumber]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[BillToCustomerID]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillToCustomerAccountNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillToName]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToCompanyName]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToName]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToRecipientID]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToPhone]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToMail]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress1]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress2]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress3]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToCity]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressZip]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToState]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToCountryCode]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToCountryName]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToPreferredFromTime]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToPreferredToTime]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Instructions]                    [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsATL]                           [bit] NULL,
		[IsRedelivery]                    [bit] NULL,
		[RedeliveryDate]                  [datetime] NULL,
		[DLB]                             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]                     [bit] NULL,
		[CreatedDatetime]                 [datetime] NULL,
		[CreatedBy]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]                  [datetime] NULL,
		[EditedBy]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Courierid]                       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[istest]                          [bit] NULL,
		[ParcelType]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DeliveryItemLabel]
	ADD
	CONSTRAINT [DF__DeliveryI__Creat__114A936A]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[DeliveryItemLabel]
	ADD
	CONSTRAINT [DF__DeliveryI__Creat__123EB7A3]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DeliveryItemLabel]
	ADD
	CONSTRAINT [DF__DeliveryI__Edite__1332DBDC]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[DeliveryItemLabel]
	ADD
	CONSTRAINT [DF__DeliveryI__Edite__14270015]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[DeliveryItemLabel]
	ADD
	CONSTRAINT [DF__DeliveryI__istes__2A164134]
	DEFAULT ((0)) FOR [istest]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20151013-093632]
	ON [dbo].[DeliveryItemLabel] ([ItemNumber])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeliveryItemLabel_ItemNumber]
	ON [dbo].[DeliveryItemLabel] ([ItemNumber])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeliveryItemLabel_SLCNACardNumber]
	ON [dbo].[DeliveryItemLabel] ([SLCNACardNumber])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeliveryItemLabel_ItemNumber_SLCNACardNumber]
	ON [dbo].[DeliveryItemLabel] ([ItemNumber], [SLCNACardNumber])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeliveryItemLabel_SNo]
	ON [dbo].[DeliveryItemLabel] ([SNo])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeliveryItemLabel_CreatedDatetime]
	ON [dbo].[DeliveryItemLabel] ([CreatedDatetime])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[DeliveryItemLabel] SET (LOCK_ESCALATION = TABLE)
GO
