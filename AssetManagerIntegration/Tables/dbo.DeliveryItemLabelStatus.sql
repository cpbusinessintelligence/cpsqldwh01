SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryItemLabelStatus] (
		[RequestID]                        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ItemNumber]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CNACardNumber]                    [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[SLCNACardNumber]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[RTCNACardNumber]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[ServiceType]                      [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[TrafficIndicator]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ItemWeight]                       [decimal](18, 5) NULL,
		[StatusCode]                       [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[StatusDescription]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReasonCode]                       [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[ReasonDescription]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CPItemStatusCode]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CPItemReasonCode]                 [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[StatusDate]                       [datetime] NULL,
		[CourierID]                        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NumberOfAttempts]                 [int] NULL,
		[DeliveryToCompanyName]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToName]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToPhone]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress1]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress2]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress3]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress4]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressPOBox]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressCity]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressState]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressZip]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressCountryCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressCountryName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryInstructions]             [nvarchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[DLB]                              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[isScannerGWDataProcessed]         [bit] NOT NULL,
		[CreatedDatetime]                  [datetime] NULL,
		[CreatedBy]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]                   [datetime] NULL,
		[EditedBy]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PopStationID]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[popstationname]                   [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[popstationdescription]            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[popstationlocation]               [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[popstationaddress]                [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[istest]                           [bit] NULL
)
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatus]
	ADD
	CONSTRAINT [DF__DeliveryI__istes__2B0A656D]
	DEFAULT ((0)) FOR [istest]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatus]
	ADD
	CONSTRAINT [DF__DeliveryI__isSca__73BA3083]
	DEFAULT ((0)) FOR [isScannerGWDataProcessed]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatus]
	ADD
	CONSTRAINT [DF__DeliveryI__Creat__74AE54BC]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatus]
	ADD
	CONSTRAINT [DF__DeliveryI__Creat__75A278F5]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatus]
	ADD
	CONSTRAINT [DF__DeliveryI__Edite__76969D2E]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatus]
	ADD
	CONSTRAINT [DF__DeliveryI__Edite__778AC167]
	DEFAULT ('Admin') FOR [EditedBy]
GO
CREATE NONCLUSTERED INDEX [IX_DeliveryItemLabelStatus_ScannerGWDataProcessed]
	ON [dbo].[DeliveryItemLabelStatus] ([isScannerGWDataProcessed], [istest])
	INCLUDE ([ItemNumber])
	ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[DeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT CONTROL
	ON [dbo].[DeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT DELETE
	ON [dbo].[DeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT INSERT
	ON [dbo].[DeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT SELECT
	ON [dbo].[DeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[DeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT UPDATE
	ON [dbo].[DeliveryItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT ALTER
	ON [dbo].[DeliveryItemLabelStatus]
	TO [SSISUser]
GO
GRANT CONTROL
	ON [dbo].[DeliveryItemLabelStatus]
	TO [SSISUser]
GO
GRANT DELETE
	ON [dbo].[DeliveryItemLabelStatus]
	TO [SSISUser]
GO
GRANT SELECT
	ON [dbo].[DeliveryItemLabelStatus]
	TO [SSISUser]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatus] SET (LOCK_ESCALATION = TABLE)
GO
