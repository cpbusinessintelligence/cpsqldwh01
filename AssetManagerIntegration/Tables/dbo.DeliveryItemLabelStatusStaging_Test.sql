SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[DeliveryItemLabelStatusStaging_Test] (
		[Id]                 [int] IDENTITY(1, 1) NOT NULL,
		[XMLData]            [xml] NULL,
		[LoadedDateTime]     [datetime] NULL,
		[isprocessed]        [bit] NULL,
		CONSTRAINT [PK__Delivery__3214EC072B95E3E3]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatusStaging_Test]
	ADD
	CONSTRAINT [DF__DeliveryI__ispro__50FB042B]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatusStaging_Test] SET (LOCK_ESCALATION = TABLE)
GO
