SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryItemlabelStatusItemNumberStaging_Test] (
		[RequestID]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ItemNumber]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[DeliveryItemlabelStatusItemNumberStaging_Test] SET (LOCK_ESCALATION = TABLE)
GO
