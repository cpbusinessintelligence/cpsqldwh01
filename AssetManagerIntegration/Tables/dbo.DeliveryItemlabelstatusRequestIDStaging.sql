SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryItemlabelstatusRequestIDStaging] (
		[Requestid]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Createddatetime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[DeliveryItemlabelstatusRequestIDStaging]
	ADD
	CONSTRAINT [DF__DeliveryI__Creat__756D6ECB]
	DEFAULT (getdate()) FOR [Createddatetime]
GO
ALTER TABLE [dbo].[DeliveryItemlabelstatusRequestIDStaging] SET (LOCK_ESCALATION = TABLE)
GO
