SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryItemlabelstatusRequestIDStaging_Test] (
		[Requestid]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Createddatetime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[DeliveryItemlabelstatusRequestIDStaging_Test]
	ADD
	CONSTRAINT [DF__DeliveryI__Creat__54CB950F]
	DEFAULT (getdate()) FOR [Createddatetime]
GO
ALTER TABLE [dbo].[DeliveryItemlabelstatusRequestIDStaging_Test] SET (LOCK_ESCALATION = TABLE)
GO
