SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ExceptionErrorLog] (
		[Sno]                 [bigint] IDENTITY(1, 1) NOT NULL,
		[type]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[status]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[errormsg]            [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Createdby]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[ExceptionErrorLog]
	ADD
	CONSTRAINT [DF__Exception__Creat__656C112C]
	DEFAULT ('Admin') FOR [Createdby]
GO
ALTER TABLE [dbo].[ExceptionErrorLog]
	ADD
	CONSTRAINT [DF__Exception__Creat__66603565]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[ExceptionErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
