SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parameters] (
		[ParametersID]     [int] IDENTITY(1, 1) NOT NULL,
		[Code]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]      [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
		[Value]            [varchar](800) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]      [datetime] NOT NULL,
		[EditWho]          [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[EditDateTime]     [datetime] NOT NULL,
		CONSTRAINT [PK_Parameters]
		PRIMARY KEY
		CLUSTERED
		([ParametersID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Parameters]
	ADD
	CONSTRAINT [DF__Parameter__AddWh__15502E78]
	DEFAULT ('Admin') FOR [AddWho]
GO
ALTER TABLE [dbo].[Parameters]
	ADD
	CONSTRAINT [DF__Parameter__AddDa__164452B1]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[Parameters]
	ADD
	CONSTRAINT [DF__Parameter__EditW__173876EA]
	DEFAULT ('Admin') FOR [EditWho]
GO
ALTER TABLE [dbo].[Parameters]
	ADD
	CONSTRAINT [DF__Parameter__EditD__182C9B23]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
ALTER TABLE [dbo].[Parameters] SET (LOCK_ESCALATION = TABLE)
GO
