SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickupItemLabelStatus] (
		[RequestID]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OrderNumber]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusCode]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDescription]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReasonDescription]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusBy]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDate]                   [datetime] NULL,
		[TrackingNumber]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Length]                       [decimal](12, 2) NULL,
		[Width]                        [decimal](12, 2) NULL,
		[Height]                       [decimal](12, 2) NULL,
		[ItemType]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ItemStatus]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TotalWeight]                  [decimal](18, 2) NULL,
		[TotalVolWeight]               [decimal](18, 2) NULL,
		[TotalQty]                     [int] NULL,
		[CourierID]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Remarks]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SplInstructions]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isScannerGWDataProcessed]     [bit] NOT NULL,
		[CreatedDatetime]              [datetime] NULL,
		[CreatedBy]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]               [datetime] NULL,
		[EditedBy]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PickupItemLabelStatus]
	ADD
	CONSTRAINT [DF__PickupIte__isSca__693CA210]
	DEFAULT ((0)) FOR [isScannerGWDataProcessed]
GO
ALTER TABLE [dbo].[PickupItemLabelStatus]
	ADD
	CONSTRAINT [DF__PickupIte__Creat__6A30C649]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[PickupItemLabelStatus]
	ADD
	CONSTRAINT [DF__PickupIte__Creat__6B24EA82]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[PickupItemLabelStatus]
	ADD
	CONSTRAINT [DF__PickupIte__Edite__6C190EBB]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[PickupItemLabelStatus]
	ADD
	CONSTRAINT [DF__PickupIte__Edite__6D0D32F4]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[PickupItemLabelStatus] SET (LOCK_ESCALATION = TABLE)
GO
