SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PickupItemLabelStatusStaging] (
		[Id]                 [int] IDENTITY(1, 1) NOT NULL,
		[XMLData]            [xml] NULL,
		[LoadedDateTime]     [datetime] NULL,
		[isprocessed]        [bit] NULL,
		CONSTRAINT [PK__PickupIt__3214EC075E53BFEB]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[PickupItemLabelStatusStaging]
	ADD
	CONSTRAINT [DF__PickupIte__ispro__0C85DE4D]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[PickupItemLabelStatusStaging] SET (LOCK_ESCALATION = TABLE)
GO
