SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Settings] (
		[SettingID]           [int] IDENTITY(1, 1) NOT NULL,
		[SettingName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SettingDetails]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		CONSTRAINT [unique_constraint_SettingName]
		UNIQUE
		NONCLUSTERED
		([SettingName])
		ON [PRIMARY],
		CONSTRAINT [PK_Settings]
		PRIMARY KEY
		CLUSTERED
		([SettingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Settings] SET (LOCK_ESCALATION = TABLE)
GO
