SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatusCodeMapping] (
		[StatusCodeID]     [int] IDENTITY(1, 1) NOT NULL,
		[StatusCode]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Description]      [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[StatusCodeCP]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_StatusCodeMapping]
		PRIMARY KEY
		CLUSTERED
		([StatusCodeID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[StatusCodeMapping] SET (LOCK_ESCALATION = TABLE)
GO
