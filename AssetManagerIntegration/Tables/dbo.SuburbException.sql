SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuburbException] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[Postcode]            [int] NULL,
		[Suburb]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Zone]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Createdby]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		[Editedby]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDateTime]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[SuburbException]
	ADD
	CONSTRAINT [DF__SuburbExc__Creat__0F975522]
	DEFAULT ('Admin') FOR [Createdby]
GO
ALTER TABLE [dbo].[SuburbException]
	ADD
	CONSTRAINT [DF__SuburbExc__Creat__108B795B]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[SuburbException]
	ADD
	CONSTRAINT [DF__SuburbExc__Edite__117F9D94]
	DEFAULT ('Admin') FOR [Editedby]
GO
ALTER TABLE [dbo].[SuburbException]
	ADD
	CONSTRAINT [DF__SuburbExc__Edite__1273C1CD]
	DEFAULT (getdate()) FOR [EditedDateTime]
GO
ALTER TABLE [dbo].[SuburbException] SET (LOCK_ESCALATION = TABLE)
GO
