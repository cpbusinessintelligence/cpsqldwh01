SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[VW_DeliveryChoicesBeat_Test] as 
select [ID]
      ,[DeliveryChoiceID]
      ,[BeatID]
      ,[IsProcess]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
  FROM cpsqldev01.[SAMPLETESTDB].[dbo].[DeliveryChoicesBeat]
 -- FROM cpsqlops01.[CouponCalculator].[dbo].[DeliveryChoicesBeat]


GO
