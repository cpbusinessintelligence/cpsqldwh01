SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[Generate_Revenue_Recovery_Report] 
AS
BEGIN

declare @ProntoBillingDate date,@maxreportbillingdate date

set @ProntoBillingDate = (select max(ProntoBillingDate) from SortationRevenueRecoveryReport)
set @maxreportbillingdate = dateadd(d,90,@ProntoBillingDate)

--select @ProntoBillingDate,@maxreportbillingdate

--select billingdate,count(*) from Temp_prontobilling group by billingdate

TRUNCATE TABLE Temp_prontobilling
INSERT INTO Temp_prontobilling
SELECT 
      [ConsignmentReference]
	  ,[ConsignmentDate]
      ,[AccountCode]
      ,[AccountName]
	  ,AccountingDate
	  ,AccountingPeriod
	  ,AccountingWeek
	  ,BillingDate
      ,[AccountBillToCode]
      ,[AccountBillToName]
      ,[ServiceCode]
   
      ,[OriginLocality]
      ,[OriginPostcode]
   
      ,[DestinationLocality]
      ,[DestinationPostcode]
      ,RevenueBusinessUnit
	  ,RevenueOriginZone
      ,RevenueDestinationZone
      ,[ItemQuantity]
      ,[DeclaredWeight]
      ,[DeadWeight]
      ,[DeclaredVolume]
      ,[Volume]
      ,[ChargeableWeight]
   
      ,[BilledFreightCharge]
      ,[BilledFuelSurcharge]
      ,[BilledTransportCharge]
      ,[BilledInsurance]
      ,[BilledOtherCharge]
      ,[BilledTotal]

--INTO Temp_prontobilling
   
FROM [Pronto].[dbo].[ProntoBilling] WITH(NOLOCK)
WHERE BillingDate > @ProntoBillingDate and BillingDate <= @maxreportbillingdate
AND [ConsignmentReference] NOT IN (SELECT [ConsignmentReference] FROM SortationRevenueRecoveryReport WHERE IsSent = 1 )

IF  EXISTS (SELECT * FROM sys.indexes WHERE NAME = N'idx_Temp_prontobilling_ConsignmentReference') 
DROP INDEX [idx_Temp_prontobilling_ConsignmentReference] ON Temp_prontobilling
--GO
--DROP INDEX IF EXISTS [idx_Temp_prontobilling_ConsignmentReference] ON Temp_prontobilling

CREATE NONCLUSTERED INDEX idx_Temp_prontobilling_ConsignmentReference ON Temp_prontobilling(ConsignmentReference)

-- Generate PriceEngineData only for records in Temp_prontobilling

EXEC [dbo].[Generate_PriceEngine_Data]

SELECT * INTO #temp_edidata FROM tblGETPricingDataForCWC WHERE ConsignmentNumber in (SELECT ConsignmentReference FROM Temp_prontobilling)

CREATE NONCLUSTERED INDEX idx_temp_edidata_ConsignmentNumber ON #temp_edidata(ConsignmentNumber)

--DROP TABLE #temp
SELECT 
	p.ConsignmentReference
	,p.consignmentdate
	,p.ServiceCode																		AS ProntoServiceCode
	,p.AccountCode																		AS ProntoAccountCode
	,p.AccountBillToCode																AS ProntoBillToCode
	,p.AccountBillToName																AS ProntoCompanyName
	,p.OriginLocality																	AS PrnotoOriginaLocality
	,p.AccountingPeriod																	AS ProntoAccountingPeriod
	,p.AccountingWeek																	AS ProntoAccountingWeek
	,p.BillingDate																		AS ProntoBillingDate
	,p.AccountingDate																	AS ProntoAccountingDate
	,p.OriginPostcode																	AS ProntoOriginPostcode
	,p.DestinationLocality																AS ProntoDestinationLocality
	,p.DestinationPostcode																AS ProntoDestinationPostcode
	,p.RevenueBusinessUnit
	,p.RevenueOriginZone
	,p.RevenueDestinationZone
	,cast(p.ItemQuantity as INT)														AS ProntoChargebleItems

	,cast(p.volume as decimal(6,3))														AS ProntoVolume
	,cast(p.BilledFreightCharge as decimal(10,2))										AS ProntoBilledFreightCharge	
	,cast(p.BilledFuelSurcharge as decimal(10,2))										AS ProntoBilledFuelSurcharge
	,cast(p.BilledTransportCharge as decimal(10,2))										AS ProntoBilledTransportCharge
	,cast(p.BilledInsurance as decimal(10,2))											AS ProntoBilledInsurance
	,cast(p.BilledOtherCharge as decimal(10,2))											AS ProntoBilledOtherCharge


	,edi.servicecode																	AS EDIServiceCode
	,edi.DeclaredWeight																	AS EDIDeclaredWeight
	,edi.ChargebleItems																	AS EDIChargebleItems
	,edi.TotalNetCharge																	AS EDITotalNetCharge
	,edi.FuelSurcharge																	AS EDIFuelSurcharge
	,edi.GST																			AS EDIGST
	,edi.FinalCharge																	AS EDIFinalCharge
	,CASE WHEN ltrim(rtrim(p.ServiceCode)) <> ltrim(rtrim(edi.servicecode)) 
		  THEN 'Service Code Changed From '+edi.servicecode + ' To '+p.ServiceCode 
		  ELSE '' END																	AS MenifestReference
	,edi.DeclaredVolume
	,edi.DelcaredCubicWeight
	,edi.Insurance
	,edi.ChargeMethod
	,edi.ChargePerKilo
	,edi.BasicCharge
	,cast(p.BilledTotal as decimal(10,2))												AS ProntoCharge
	,cast(edi.TotalChargeExGST as decimal(10,2))										AS EDITotalChargeExGST
	,cast(p.BilledTotal-edi.TotalChargeExGST as decimal(10,2))							AS RevenueRecovered
	,case when p.BilledTotal-edi.TotalChargeExGST > 0 THEN 'Y' ELSE '' END				AS RevenueProtected
	,CAST(p.ChargeableWeight as decimal(10,2))											AS ProntoChargebleweight
	,CAST(edi.OriginalBilledWeight	as decimal(10,2))									AS EDIChargebleWeight
	,CAST(p.ChargeableWeight - edi.OriginalBilledWeight	as decimal(10,2))				AS WeightVariation
	,Case when i.SortationCount > 0 then 'SYDSORT' else '' end							AS SYDSortation

INTO #temp

FROM Temp_prontobilling p

JOIN #temp_edidata edi ON p.ConsignmentReference = edi.ConsignmentNumber
OUTER APPLY (SELECT count(*) as SortationCount FROM  [dbo].[Incoming_CWCDetails] WHERE cd_connote = p.ConsignmentReference) i

WHERE edi.finalcharge > 0 

--Added By PV ON 2020-09-15: To identify old sortation--
Select Distinct cd_connote, Case de.Code When 'HB' Then 'SYDCWC' Else 'BNECWC' End [Sortation]
Into #tmpOldSortationInsert
From #temp tmp
	Join CpplEDI..consignment (nolock) c on c.cd_connote = tmp.ConsignmentReference
	join CpplEDI..cdcoupon (nolock) d on d.cc_consignment = c.cd_id
	join ScannerGateway..TrackingEvent (nolock) t on t.SourceReference = d.cc_coupon
	join ScannerGateway..Driver (nolock) dr on dr.Id = t.DriverId and dr.Code = 9994 and dr.DepotId is not null
	join ScannerGateway..Depot (nolock) de on de.Id = dr.DepotId

Update t Set SYDSortation = [Sortation]
From #temp t
Join #tmpOldSortationInsert s On t.ConsignmentReference = s.cd_connote
--Added By PV ON 2020-09-15: To identify old sortation - Ends here---

--Added By PV ON 2020-11-24: To identify New Syd sortation, for which connote is null in [Incoming_CWCDetails] table--
Select Distinct ConsignmentReference, 'SYDSORT' [Sortation]
Into #tmpNewSydSortationInsert
From #temp tmp
	Join CpplEDI..consignment (nolock) c on c.cd_connote = tmp.ConsignmentReference
	join CpplEDI..cdcoupon (nolock) d on d.cc_consignment = c.cd_id
	join ScannerGateway..TrackingEvent (nolock) t on t.SourceReference = d.cc_coupon
	join ScannerGateway..Driver (nolock) dr on dr.Id = t.DriverId	
Where dr.Code = 9416

Update t Set SYDSortation = s.[Sortation]
From #temp t
Join #tmpNewSydSortationInsert s On t.ConsignmentReference = s.ConsignmentReference
Where IsNull(t.SYDSortation,'') = ''
--Added By PV ON 2020-11-24: To identify New Syd sortation, for which connote is null in [Incoming_CWCDetails] table - Ends here---

DELETE FROM SortationRevenueRecoveryReport WHERE ConsignmentReference IN (select ConsignmentReference FROM #temp)

--truncate table SortationRevenueRecoveryReport
--drop table SortationRevenueRecoveryReport

INSERT INTO SortationRevenueRecoveryReport
(
		ConsignmentReference
		,consignmentdate
		,ProntoServiceCode
		,ProntoAccountCode
		,ProntoBillToCode
		,ProntoCompanyName
		,PrnotoOriginaLocality
		,ProntoAccountingPeriod
		,ProntoAccountingWeek
		,ProntoBillingDate
		,ProntoAccountingDate
		,ProntoOriginPostcode
		,ProntoDestinationLocality
		,ProntoDestinationPostcode
		,ProntoChargebleItems
		,ProntoVolume
		,ProntoBilledFreightCharge
		,ProntoBilledFuelSurcharge
		,ProntoBilledTransportCharge
		,ProntoBilledInsurance
		,ProntoBilledOtherCharge
		,EDIServiceCode
		,EDIDeclaredWeight
		,EDIChargebleItems
		,EDITotalNetCharge
		,EDIFuelSurcharge
		,EDIGST
		,EDIFinalCharge
		,MenifestReference
		,DeclaredVolume
		,DelcaredCubicWeight
		,Insurance
		,ChargeMethod
		,ChargePerKilo
		,BasicCharge
		,ProntoCharge
		,EDITotalChargeExGST
		,RevenueRecovered
		,RevenueProtected
		,ProntoChargebleweight
		,EDIChargebleWeight
		,WeightVariation
		,SYDSortation
		,RevenueBusinessUnit
		,RevenueOriginZone
		,RevenueDestinationZone
		,TypeOfBreach
		,ChargeOfBreach
		,SourceOfBreach
)

SELECT 

		ConsignmentReference
		,consignmentdate
		,ProntoServiceCode
		,ProntoAccountCode
		,ProntoBillToCode
		,ProntoCompanyName
		,PrnotoOriginaLocality
		,ProntoAccountingPeriod
		,ProntoAccountingWeek
		,ProntoBillingDate
		,ProntoAccountingDate
		,ProntoOriginPostcode
		,ProntoDestinationLocality
		,ProntoDestinationPostcode
		,ProntoChargebleItems
		,ProntoVolume
		,ProntoBilledFreightCharge
		,ProntoBilledFuelSurcharge
		,ProntoBilledTransportCharge
		,ProntoBilledInsurance
		,ProntoBilledOtherCharge
		,EDIServiceCode
		,EDIDeclaredWeight
		,EDIChargebleItems
		,EDITotalNetCharge
		,EDIFuelSurcharge
		,EDIGST
		,EDIFinalCharge
		,MenifestReference
		,DeclaredVolume
		,DelcaredCubicWeight
		,Insurance
		,ChargeMethod
		,ChargePerKilo
		,BasicCharge
		,ProntoCharge
		,EDITotalChargeExGST
		,RevenueRecovered
		,RevenueProtected
		,ProntoChargebleweight
		,EDIChargebleWeight
		,WeightVariation
		,SYDSortation
		,RevenueBusinessUnit
		,RevenueOriginZone
		,RevenueDestinationZone
		,''
		,''
		,''
FROM #temp

UNION
SELECT 
		PB.[Consignment reference]
		,consignmentdate
		,ProntoServiceCode
		,ProntoAccountCode
		,ProntoBillToCode
		,ProntoCompanyName
		,PrnotoOriginaLocality
		,ProntoAccountingPeriod
		,ProntoAccountingWeek
		,ProntoBillingDate
		,ProntoAccountingDate
		,ProntoOriginPostcode
		,ProntoDestinationLocality
		,ProntoDestinationPostcode
		,ProntoChargebleItems
		,ProntoVolume
		,ProntoBilledFreightCharge
		,ProntoBilledFuelSurcharge
		,ProntoBilledTransportCharge
		,ProntoBilledInsurance
		,ProntoBilledOtherCharge
		,EDIServiceCode
		,EDIDeclaredWeight
		,EDIChargebleItems
		,EDITotalNetCharge
		,EDIFuelSurcharge
		,EDIGST
		,EDIFinalCharge
		,MenifestReference
		,DeclaredVolume
		,DelcaredCubicWeight
		,Insurance
		,ChargeMethod
		,ChargePerKilo
		,BasicCharge
		,0 As ProntoCharge --PV: Updated to 0 based Kyeelee's email and ticket#23211 
		,0 As EDITotalChargeExGST  --PV: Updated to 0 based Kyeelee's email and ticket#23211
		,0 As RevenueRecovered  --PV: Updated to 0 based Kyeelee's email and ticket#23211
		,RevenueProtected
		,ProntoChargebleweight
		,EDIChargebleWeight
		,WeightVariation
		,SYDSortation
		,RevenueBusinessUnit
		,RevenueOriginZone
		,RevenueDestinationZone
		,''
		,'25'
		,PB.[Service]
FROM #temp tmp
Join CPSQLOPS01.[ProntoBilling].[dbo].[ProntoBilling_Breaches] PB (NOLOCK) ON tmp.ConsignmentReference = Left(PB.[Consignment reference],Len(PB.[Consignment reference])-3)

/*



select ProntoBillingDate,sum(revenueRecovery) from SortationRevenueRecoveryReport where sydsortation = 'Y' and revenuerecovered = 'Y'
group by ProntoBillingDate
order by 1

select ProntoBillingDate,count(*) from SortationRevenueRecoveryReport group by ProntoBillingDate order by 1

delete from SortationRevenueRecoveryReport where ProntoBillingDate = '2019-11-30'

select BillingDate,count(*)  
FROM [Pronto].[dbo].[ProntoBilling] WITH(NOLOCK)
WHERE BillingDate >='2019-11-18' 
GROUP BY BillingDate

2019-11-18	4180
2019-11-20	58
2019-11-23	461674
2019-11-30	6


update SortationRevenueRecoveryReport set issent = 1 , sentdatetime = getdate() -3

*/

END


GO
