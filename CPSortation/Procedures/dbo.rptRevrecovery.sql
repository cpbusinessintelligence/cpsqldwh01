SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[rptRevrecovery] 
@fromdate date,@todate date,@accountcode varchar(10)
	-- Add the parameters for the stored procedure here

AS
BEGIN

	SELECT  (CASE WHEN left(ConsignmentReference,3) = 'CPW' THEN 'CP Website' ELSE 'EDI' END)	AS DataSource 
	,'No'																				AS ChangeBilled
      ,[ConsignmentReference]
     
      ,[ProntoCompanyName]
	  ,[ProntoAccountCode]
	  ,TypeOfBreach		AS TypeOfBreach
	  ,[EDITotalNetCharge] [OriginalFreigtCharge]
	  ,[ProntoBilledTransportCharge] [BilledFreightCharge]
	    ,[RevenueRecovered]
		 ,[RevenueProtected]
		 ,ChargeOfBreach	AS ChargeOfBreach
	   ,SourceOfBreach	AS SourceOfBreach
		  ,[EDIServiceCode] [OriginalServiceCode]
		  ,ProntoServiceCode
		   ,[MenifestReference]
		     ,[EDIChargebleWeight] [DeclaredTotalConsignmentWeight]
			 ,[ProntoChargebleweight] [DeclaredDeadWeight]
			 
      ,[WeightVariation]
	  ,[EDIDeclaredWeight] [BilledConsignmentWeight]
	    ,[DelcaredCubicWeight] 
		,[PrnotoOriginaLocality] [PickupSuburb]
		,ProntoOriginPostcode [PickupPostcode]
		
		  ,[ProntoDestinationLocality] [DestincationSuburt]
      ,[ProntoDestinationPostcode] [DestinationPostcode]
	   ,[RevenueBusinessUnit]
      ,[RevenueOriginZone]
      ,[RevenueDestinationZone]
	   ,[EDIChargebleItems]
	    ,[ChargeMethod]
		 ,[ChargePerKilo]
      ,[BasicCharge]
    
   --   ,[ProntoAccountingPeriod]
      ,[ProntoAccountingWeek]
      ,[ProntoBillingDate]
	   ,[SYDSortation]
   	
		
     
  FROM [CPSortation].[dbo].[SortationRevenueRecoveryReport] where (ProntoBillingDate>=@fromdate
  and prontobillingdate <=@todate) 
  and   (Prontoaccountcode = @AccountCode or @AccountCode is null)
  and (IsSent=1) AND left(ConsignmentReference,3) <> 'CPW' 
END
GO
GRANT EXECUTE
	ON [dbo].[rptRevrecovery]
	TO [ReportUser]
GO
