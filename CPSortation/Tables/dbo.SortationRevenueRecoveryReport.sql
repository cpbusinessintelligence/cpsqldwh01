SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SortationRevenueRecoveryReport] (
		[RawID]                           [bigint] IDENTITY(1, 1) NOT NULL,
		[ConsignmentReference]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[consignmentdate]                 [date] NULL,
		[ProntoServiceCode]               [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntoAccountCode]               [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntoBillToCode]                [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntoCompanyName]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrnotoOriginaLocality]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProntoAccountingPeriod]          [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[ProntoAccountingWeek]            [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[ProntoBillingDate]               [date] NOT NULL,
		[ProntoAccountingDate]            [date] NULL,
		[ProntoOriginPostcode]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ProntoDestinationLocality]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProntoDestinationPostcode]       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ProntoChargebleItems]            [int] NULL,
		[ProntoVolume]                    [decimal](6, 3) NULL,
		[ProntoBilledFreightCharge]       [decimal](38, 6) NULL,
		[ProntoBilledFuelSurcharge]       [decimal](38, 6) NULL,
		[ProntoBilledTransportCharge]     [decimal](38, 6) NULL,
		[ProntoBilledInsurance]           [decimal](38, 6) NULL,
		[ProntoBilledOtherCharge]         [decimal](38, 6) NULL,
		[EDIServiceCode]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EDIDeclaredWeight]               [decimal](38, 6) NULL,
		[EDIChargebleItems]               [int] NOT NULL,
		[EDITotalNetCharge]               [decimal](38, 6) NULL,
		[EDIFuelSurcharge]                [decimal](38, 6) NULL,
		[EDIGST]                          [decimal](38, 6) NULL,
		[EDIFinalCharge]                  [decimal](38, 6) NULL,
		[MenifestReference]               [varchar](60) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredVolume]                  [decimal](38, 6) NULL,
		[DelcaredCubicWeight]             [decimal](38, 6) NULL,
		[Insurance]                       [int] NOT NULL,
		[ChargeMethod]                    [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[ChargePerKilo]                   [decimal](20, 5) NULL,
		[BasicCharge]                     [decimal](20, 5) NULL,
		[ProntoCharge]                    [decimal](10, 2) NULL,
		[EDITotalChargeExGST]             [decimal](38, 6) NULL,
		[RevenueRecovered]                [decimal](10, 2) NULL,
		[RevenueProtected]                [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntoChargebleweight]           [decimal](38, 6) NULL,
		[EDIChargebleWeight]              [decimal](38, 6) NULL,
		[WeightVariation]                 [decimal](38, 6) NULL,
		[SYDSortation]                    [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsSent]                          [bit] NULL,
		[SentDateTime]                    [datetime] NULL,
		[RevenueBusinessUnit]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueOriginZone]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDestinationZone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TypeOfBreach]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ChargeOfBreach]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SourceOfBreach]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_SortationRevenueRecoveryReport1]
		PRIMARY KEY
		CLUSTERED
		([RawID])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_SortationRevenueRecoveryReport_11]
	ON [dbo].[SortationRevenueRecoveryReport] ([consignmentdate])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SortationRevenueRecoveryReport_21]
	ON [dbo].[SortationRevenueRecoveryReport] ([ProntoBillingDate])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SortationRevenueRecoveryReport_31]
	ON [dbo].[SortationRevenueRecoveryReport] ([IsSent])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SortationRevenueRecoveryReport_41]
	ON [dbo].[SortationRevenueRecoveryReport] ([SentDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SortationRevenueRecoveryReport1]
	ON [dbo].[SortationRevenueRecoveryReport] ([ConsignmentReference])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[SortationRevenueRecoveryReport] SET (LOCK_ESCALATION = TABLE)
GO
