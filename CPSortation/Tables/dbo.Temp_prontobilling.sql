SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_prontobilling] (
		[ConsignmentReference]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentDate]            [date] NULL,
		[AccountCode]                [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingDate]             [date] NULL,
		[AccountingPeriod]           [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[AccountingWeek]             [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[BillingDate]                [date] NOT NULL,
		[AccountBillToCode]          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountBillToName]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[OriginLocality]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginPostcode]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DestinationLocality]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPostcode]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[RevenueBusinessUnit]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueOriginZone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDestinationZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ItemQuantity]               [float] NULL,
		[DeclaredWeight]             [float] NULL,
		[DeadWeight]                 [float] NULL,
		[DeclaredVolume]             [float] NULL,
		[Volume]                     [float] NULL,
		[ChargeableWeight]           [float] NULL,
		[BilledFreightCharge]        [money] NULL,
		[BilledFuelSurcharge]        [money] NULL,
		[BilledTransportCharge]      [money] NULL,
		[BilledInsurance]            [money] NULL,
		[BilledOtherCharge]          [money] NULL,
		[BilledTotal]                [money] NULL
)
GO
CREATE NONCLUSTERED INDEX [idx_Temp_prontobilling_ConsignmentReference]
	ON [dbo].[Temp_prontobilling] ([ConsignmentReference])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Temp_prontobilling] SET (LOCK_ESCALATION = TABLE)
GO
