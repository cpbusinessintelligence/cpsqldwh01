SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sysssislog] (
		[id]              [int] IDENTITY(1, 1) NOT NULL,
		[event]           [sysname] COLLATE Latin1_General_CI_AS NOT NULL,
		[computer]        [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[operator]        [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[source]          [nvarchar](1024) COLLATE Latin1_General_CI_AS NOT NULL,
		[sourceid]        [uniqueidentifier] NOT NULL,
		[executionid]     [uniqueidentifier] NOT NULL,
		[starttime]       [datetime] NOT NULL,
		[endtime]         [datetime] NOT NULL,
		[datacode]        [int] NOT NULL,
		[databytes]       [image] NULL,
		[message]         [nvarchar](2048) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__sysssisl__3213E83F9AB4F48E]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[sysssislog] SET (LOCK_ESCALATION = TABLE)
GO
