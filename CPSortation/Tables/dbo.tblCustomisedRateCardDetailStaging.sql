SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomisedRateCardDetailStaging] (
		[id]                  [int] IDENTITY(1, 1) NOT NULL,
		[Account]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UniqueKey]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Service]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EffectiveDate]       [date] NULL,
		[OriginZone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DestinationZone]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[MinimumCharge]       [float] NULL,
		[BasicCharge]         [float] NULL,
		[FuelOverride]        [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[FuelPercentage]      [float] NULL,
		[Rounding]            [float] NULL,
		[ChargePerKilo]       [float] NULL,
		[CreatedDateTime]     [datetime] NULL,
		CONSTRAINT [PK_tblCustomisedRateCardDetailStaging]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCustomisedRateCardDetailStaging]
	ADD
	CONSTRAINT [DF_tblCustomisedRateCardDetailStaging_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_tblCustomisedRateCardDetailStaging]
	ON [dbo].[tblCustomisedRateCardDetailStaging] ([Account], [Service], [OriginZone], [DestinationZone], [EffectiveDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCustomisedRateCardDetailStaging] SET (LOCK_ESCALATION = TABLE)
GO
