SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblGETPricingDataForCWC] (
		[ConsignmentID]                [int] NULL,
		[CustomerAccountCode]          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[CompanyId]                    [int] NULL,
		[ConsignmentNumber]            [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentDate]              [date] NULL,
		[TarrifAccountCode]            [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[OriginPriceZoneCode]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPriceZoneCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ShippingDate]                 [nvarchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredVolume]               [decimal](20, 10) NULL,
		[DelcaredCubicWeight]          [decimal](20, 8) NULL,
		[DeclaredWeight]               [decimal](20, 5) NULL,
		[OriginalBilledWeight]         [decimal](20, 8) NULL,
		[MeasuredVolume]               [decimal](20, 5) NULL,
		[MeasuredWeight]               [decimal](20, 5) NULL,
		[ChargebleItems]               [int] NOT NULL,
		[Insurance]                    [int] NOT NULL,
		[ChargeMethod]                 [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[ChargePerKilo]                [decimal](20, 5) NULL,
		[BasicCharge]                  [decimal](20, 5) NULL,
		[TotalNetCharge]               [decimal](38, 6) NULL,
		[FuelSurcharge]                [decimal](38, 6) NULL,
		[TotalChargeExGST]             [decimal](38, 6) NULL,
		[GST]                          [decimal](38, 6) NULL,
		[FinalCharge]                  [decimal](38, 6) NULL
)
GO
ALTER TABLE [dbo].[tblGETPricingDataForCWC] SET (LOCK_ESCALATION = TABLE)
GO
