SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblNonCustomisedRateCardDetailStaging] (
		[id]                  [int] IDENTITY(1, 1) NOT NULL,
		[UniqueKey]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Account]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Service]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EffectiveDate]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[OriginZone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DestinationZone]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[MinimumCharge]       [decimal](10, 4) NULL,
		[BasicCharge]         [decimal](10, 4) NULL,
		[FuelOverride]        [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[FuelPercentage]      [decimal](10, 4) NULL,
		[Rounding]            [decimal](10, 4) NULL,
		[ChargePerKilo]       [decimal](10, 4) NULL,
		[CreatedDateTime]     [datetime] NULL,
		CONSTRAINT [PK_tblNonCustomisedRateCardDetailStaging]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblNonCustomisedRateCardDetailStaging]
	ADD
	CONSTRAINT [DF_tblNonCustomisedRateCardDetailStaging_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_tblNonCustomisedRateCardDetailStaging]
	ON [dbo].[tblNonCustomisedRateCardDetailStaging] ([Service], [OriginZone], [DestinationZone], [EffectiveDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblNonCustomisedRateCardDetailStaging] SET (LOCK_ESCALATION = TABLE)
GO
