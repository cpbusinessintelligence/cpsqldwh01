SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPricingZoneLookUp] (
		[ID]                     [bigint] NOT NULL,
		[PostCodeID]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                 [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[State]                  [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[PrizeZone]              [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[PrizeZoneName]          [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[ETAZone]                [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[ETAZoneName]            [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RedemptionZone]         [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RedemptionZoneName]     [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[DepotCode]              [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[SortCode]               [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[IsPickup]               [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[IsDelivery]             [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[PostCodeSuburb]         [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]               [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[PostCodeOld]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]        [datetime] NULL,
		[CreatedBy]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblPricingZoneLookUp]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_tblPricingZoneLookUp]
	ON [dbo].[tblPricingZoneLookUp] ([PostCode], [Suburb])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPricingZoneLookUp] SET (LOCK_ESCALATION = TABLE)
GO
