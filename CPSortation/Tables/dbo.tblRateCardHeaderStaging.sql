SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCardHeaderStaging] (
		[id]                    [int] IDENTITY(1, 1) NOT NULL,
		[Service]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ValidFrom]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FuelOverride]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel]                  [decimal](10, 4) NULL,
		[ChargeMethod]          [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[VolumetricDivisor]     [int] NULL,
		[TariffId]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Logwho]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Logdate]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		CONSTRAINT [PK_tblRateCardHeaderStaging]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRateCardHeaderStaging]
	ADD
	CONSTRAINT [DF_tblRateCardHeaderStaging_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_tblRateCardHeaderStaging]
	ON [dbo].[tblRateCardHeaderStaging] ([ValidFrom], [Service])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRateCardHeaderStaging] SET (LOCK_ESCALATION = TABLE)
GO
