SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCustomisedHeaderStaging] (
		[id]                    [int] IDENTITY(1, 1) NOT NULL,
		[Accountcode]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UniqueKey]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Name]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ValidFrom]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FuelOverride]          [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[Fuel]                  [numeric](10, 4) NULL,
		[ChargeMethod]          [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[VolumetricDivisor]     [numeric](10, 4) NULL,
		[TariffId]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Logwho]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Logdate]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		CONSTRAINT [PK_tblRateCustomisedHeaderStaging]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRateCustomisedHeaderStaging]
	ADD
	CONSTRAINT [DF_tblRateCustomisedHeaderStaging_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_tblRateCustomisedHeaderStaging]
	ON [dbo].[tblRateCustomisedHeaderStaging] ([ValidFrom], [Service], [Accountcode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRateCustomisedHeaderStaging] SET (LOCK_ESCALATION = TABLE)
GO
