SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[website_consignment] (
		[ConsignmentID]                   [int] NULL,
		[ConsignmentCode]                 [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentPreferPickupDate]     [date] NULL,
		[AccountNumber]                   [nvarchar](40) COLLATE Latin1_General_CI_AS NULL,
		[RateCardID]                      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TotalWeight]                     [numeric](10, 4) NULL,
		[TotalVolume]                     [numeric](10, 4) NULL,
		[TotalMeasureWeight]              [numeric](10, 4) NULL,
		[TotalMeasureVolume]              [numeric](10, 4) NULL,
		[NoOfItems]                       [int] NULL,
		[PickupSuburb]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostCode]                  [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DestinationSuburb]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPostCode]             [nvarchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[website_consignment] SET (LOCK_ESCALATION = TABLE)
GO
