SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




















--select * from  [dbo].[GetEnrichedDataForCWCPricing]



CREATE VIEW [dbo].[GetEnrichedDataForCWCPricing]
AS

WITH maxCDID as
(
SELECT
	cd_id
	,ROW_NUMBER() OVER(PARTITION BY cd_connote order by cd_id desc) as rownum 
from [CpplEDI].dbo.[consignment] -- where cd_connote in (select ConsignmentReference from [dbo].[Temp_ProntoBilling_0411_1111]) 
)
SELECT distinct
	edi.cd_id
	,cd_company_id
	,cd_account															AS acct
	,cast(cd_connote as varchar(40))									AS cd_connote
	,cast(cd_consignment_date as date)									AS cd_consignment_date
	,cast(FORMAT(cd_consignment_date,'yyyy-MM-dd') as nvarchar(10))		AS Shipdate
	,cd_items															AS itemqty
	,cd_delivery_suburb													AS destsuburb
	,cd_delivery_postcode												AS destpostcode
	,cd_pickup_suburb													AS originsuburb
	,cd_pickup_postcode													AS originpostcode
	,cd_pricecode														AS [service]
	,cd_insurance														AS Insurance
	,cast(cd_volume as decimal(20,5))									AS volume
	,cast(cd_deadweight as decimal(20,5))								AS [weight]
	,cast(cd_measured_deadweight as decimal(20,5))						AS MeasuredWeight		
	,cast(cd_measured_volume as decimal(20,5))							AS MeasuredVolume

FROM [CpplEDI].dbo.[consignment] edi WITH (NOLOCK) JOIN maxCDID on edi.cd_id = maxCDID.cd_id and maxCDID.rownum = 1
/*

-- website consignment data

UNION

SELECT 
	ConsignmentID															AS cd_id
--	,null																	AS Company_Id
	,AccountNumber															AS acct
	,ConsignmentCode														AS cd_connote
	,ConsignmentPreferPickupDate											AS cd_consignment_date
	,cast(FORMAT(ConsignmentPreferPickupDate,'yyyy-MM-dd') as nvarchar(10))	AS Shipdate
	,NoOfItems																AS itemqty
	,DestinationSuburb														AS destsuburb
	,try_cast(DestinationPostCode as int)									AS destpostcode
	,PickupSuburb															AS originsuburb
	,try_cast(PickupPostCode as int)										AS originpostcode
	,RateCardID																AS [service]
	,0																		AS Insurance
	,TotalVolume															AS volume
	,TotalWeight															AS [weight]
	,TotalMeasureWeight														AS MeasuredWeight
	,TotalMeasureVolume														AS MeasuredVolume
FROM [dbo].[website_consignment]
*/

/*
SELECT 0 as cd_id
      ,0 as cd_company_id
     ,[Account code]													AS acct
	, [Consignment reference] as cd_connote
	,[Consignment date] as cd_consignment_date
	,Getdate() as cd_eta_date

	,cast(FORMAT(getdate(),'yyyy-MM-dd') as nvarchar(10))		AS Shipdate
	,convert(integer,[Item quantity]) 														AS itemqty
	,[Receiver locality]												AS destsuburb
	,[Receiver postcode]											AS destpostcode
	,[Sender locality]												AS originsuburb
	,[Sender postcode]												AS originpostcode
	,edi.cd_pricecode												AS [service]
	,0.00													AS Insurance
	,COnvert(decimal(20,5),[Declared volume]) 	AS volume							
	,Convert(decimal(20,5),[Declared weight]) AS [weight]
						
	,0.00					AS MeasuredWeight		
	,0.00						AS MeasuredVolume

       
 
  FROM [dbo].[Temp_JP_ProntoBillingRecon]
   outer apply (select cd_pricecode from edi_consignment where cd_connote = [Consignment reference] ) edi
*/
/*
select 

	ConsignmentReference												AS cd_connote
	,accountcode														as acct
	,accountbilltocode													as acctbillto
	,consignmentdate													AS Shipdate
	,[ItemQuantity]														as itemqty
	,[DestinationLocality]												AS destsuburb
	,[DestinationPostcode]												AS destpostcode
	,[OriginLocality]													AS originsuburb
	,[OriginPostcode]													AS originpostcode
	,[ServiceCode]														AS [service]
	,0																	AS Insurance
	,cast([DeclaredVolume] as decimal(20,5))							AS volume
	--,cast([DeclaredWeight] as decimal(20,5))							AS [weight]
	,cast(ChargeableWeight as decimal(20,5))							AS [weight]
	,0.0																AS MeasuredWeight		
	,0.0																AS MeasuredVolume
FROM [dbo].[temp_ProntoBilling_04112019] 

--inner join [dbo].[edi_cdcoupon] c with(nolock)  on c.cc_consignment = edi.cd_id
--WHERE cd_connote in (SELECT ConsignmentId FROM [dbo].[Incoming_CWCDetails])
--where cd_id =72441091
*/
--select recon.Service,edi.cd_pricecode  from [Temp_JP_ProntoBillingRecon] recon outer apply (select cd_pricecode from edi_consignment where cd_connote = [Consignment reference] ) edi
--select recon.Service,edi.cd_pricecode,recon.[Declared weight],edi.cd_deadweight,recon.[Declared volume],edi.cd_volume  from [Temp_JP_ProntoBillingRecon] recon outer apply (select cd_pricecode,cd_volume,cd_deadweight from edi_consignment where cd_connote = [Consignment reference] ) edi
GO
