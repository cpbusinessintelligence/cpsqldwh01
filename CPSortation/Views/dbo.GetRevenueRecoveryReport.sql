SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



--select * from [dbo].[GetRevenueRecoveryReport]

CREATE VIEW [dbo].[GetRevenueRecoveryReport]
AS

select 

	(CASE WHEN left(ConsignmentReference,3) = 'CPW' THEN 'CP Website' ELSE 'EDI' END)	AS DataSource 
	,'No'																				AS ChangeBilled
	,ConsignmentReference
	,ProntoCompanyName
	,ProntoAccountCode
	,TypeOfBreach																		AS TypeOfBreach
	,cast(EDITotalChargeExGST AS VARCHAR(20))											AS OriginalFrieghtCharge
	,cast(ProntoCharge AS VARCHAR(20))													AS BilledFrieghtCharge
	,cast(RevenueRecovered AS VARCHAR(20))												AS RevenueRecovered
	,RevenueProtected
	,ChargeOfBreach																		AS ChargeOfBreach
	,SourceOfBreach																		AS SourceOfBreach
	,EDIServiceCode																		AS OriginalServiceCode
	,ProntoServiceCode																	AS BilledServiceCode
	,MenifestReference	
	,cast(EDIChargebleWeight AS VARCHAR(20))											AS DeclaredTotalConsignmentWeight
	,cast(ProntoChargebleweight	 AS VARCHAR(20))										AS BilledConsignmentWeight
	,cast(WeightVariation AS VARCHAR(20))												AS WeightVariation
	,cast(cast(EDIDeclaredWeight AS DECIMAL(16,2)) AS VARCHAR(20))						AS DeclaredDeadWeight
	,cast(cast(DelcaredCubicWeight AS DECIMAL(16,2)) AS VARCHAR(20))					AS DelcaredCubicWeight

	,PrnotoOriginaLocality																AS PickupSuburb
	,ProntoOriginPostcode																AS PickupPostcode
	,ProntoDestinationLocality															AS DestinationSuburb
	,ProntoDestinationPostcode															AS DestinationPostcode
	,RevenueBusinessUnit
	,RevenueOriginZone
	,RevenueDestinationZone
	,cast(ProntoChargebleItems AS VARCHAR(20))											AS TotalQty
	,ChargeMethod
	,cast(cast(ChargePerKilo AS DECIMAL(16,2)) AS VARCHAR(20))							AS ChargePerKilo
	,cast(cast(BasicCharge AS DECIMAL(16,2)) AS VARCHAR(20))							AS BasicCharge
	--,max(ProntoAccountingWeek) over()													AS BillingWeek
	,ProntoAccountingWeek 													AS BillingWeek
	,cast(ProntoBillingDate	AS VARCHAR(15))												AS BillingDate
	,SYDSortation																		AS IsSortation

FROM SortationRevenueRecoveryReport
WHERE (IsSent is null or isSent = 0) 
	--AND left(ConsignmentReference,3) <> 'CPW' -- Commented by PV on 2021-03-17: To include Transdirect Data
	AND ( RevenueRecovered <> 0 OR ChargeOfBreach > 0)
GO
