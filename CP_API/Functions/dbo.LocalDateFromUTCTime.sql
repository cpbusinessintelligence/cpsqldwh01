SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================


create FUNCTION [dbo].LocalDateFromUTCTime
(
    @UTC datetime

)
RETURNS datetime
AS
BEGIN

    
	DECLARE @utcdate DATETIME = @UTC
	DECLARE @AEST datetime
	set @AEST = Dateadd(hh, Datediff(hh,@utcdate, Getdate()), @utcdate)
	
   -- return converted datetime
    return @AEST

END
GO
