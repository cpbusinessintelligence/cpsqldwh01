SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPAPI_CPAPI_Maintenance]
@Date datetime  --'2017-10-10'
As 
begin
Begin Try
BEGIN TRAN 

		PRINT '-------- START DISABLING TABLE CONSTRAINT --------';

		ALTER TABLE [cp_api].[dbo].[Event]  NOCHECK CONSTRAINT ALL

		PRINT '-------- END DISABLING TABLE CONSTRAINT --------';

		PRINT '-------- START COPYING DATA--------';

		PRINT '------------Add Table [Event] Record In Archive-----------------------';

		INSERT INTO [cp_api].[dbo].[Event_archive_1]
		SELECT [Id],[Token],[Status],[Message],[Request],[RequestUrl],[Response],[InternalLogs],[API],[CreatedDateTime],[Sandbox] 
		FROM [cp_api].[dbo].[Event]  
		where Convert(date, CONVERT(datetime2, [CreatedDateTime], 1), 103) <= @Date


		PRINT '-------- END COPYING DATA --------';


		PRINT '-------- DELETE DATA START --------';

		Delete FROM [cp_api].[dbo].[Event] where Convert(date, CONVERT(datetime2, [CreatedDateTime], 1), 103) <= @Date

		PRINT '-------- DELETE DATA END --------';

		PRINT '-------- START ENABLING TABLE CONSTRAINT --------';

		ALTER TABLE [cp_api].[dbo].[Event]  CHECK CONSTRAINT ALL

		PRINT '-------- END ENABLING TABLE CONSTRAINT --------';
		select 'OK' as Result

	COMMIT TRAN 
		END TRY
				BEGIN CATCH
				begin
					rollback tran
					--INSERT INTO EzyFreight.[dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
					-- VALUES
					--	   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
					--	   ,'SPCPAPI_CPAPI_DatabaseMaintenance', 2)

					   select cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as Result
				end
				END CATCH
End
GO
