SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPAPI_GetEventCount]
@Date datetime  --'2017-10-10'
As begin

 SELECT Count(*) as 'Event Count'
 FROM [cp_api].[dbo].[Event] 
 where Convert(date, CONVERT(datetime2, [CreatedDateTime], 1), 103) <= @Date

 SELECT Count(*) as 'Event Archive Count'
 FROM [cp_api].[dbo].[Event_archive_1] 
 

End
GO
GRANT EXECUTE
	ON [dbo].[SPCPAPI_GetEventCount]
	TO [ReportUser_1]
GO
