SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure SPCPPL_GetCP_APIEvents
@jobNumber varchar(50) = null

As 
Begin
		select top 100 percent ID, Token, [Status], [Message], Request, RequestUrl, Response, InternalLogs, API
		from cp_api..[Event] with(Nolock)
		where RequestURL in ('/v1/domestic/bookPickup','/v2/domestic/bookPickup') and [Status] = 'SUCCESS' And
		(
		case when isnull(@jobNumber,'') = '' then '' else Response end  ='{"responseCode":"SUCCESS","msg":"","data":{"jobNumber":"'+ @jobNumber +'"}}' or
		case when isnull(@jobNumber,'') = '' then '' else Response end  ='<?xml version="1.0" encoding="utf-16"?>
<ResponseTemplateOfBookPickupResponse xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <responseCode>SUCCESS</responseCode>
  <msg />
  <data>
    <jobNumber>'+@jobNumber+'</jobNumber>
  </data>
</ResponseTemplateOfBookPickupResponse>'
		)
		order by 1 desc
				
End
GO
GRANT EXECUTE
	ON [dbo].[SPCPPL_GetCP_APIEvents]
	TO [ReportUser_1]
GO
