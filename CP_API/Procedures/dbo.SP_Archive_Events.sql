SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jeff Embro
-- Create date: 26/05/2016
-- Description:	Archives Event Data
-- =============================================
CREATE PROCEDURE [dbo].[SP_Archive_Events]
	 @daysOld int = 190
AS
BEGIN
	
	--print 'days old: ' + Cast(@daysOld as varchar);

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @event_id int;
	DECLARE @token int;
	DECLARE @status nvarchar(300);
	DECLARE @message nvarchar(300);
	DECLARE @request nvarchar(max);
	DECLARE @request_url nvarchar(max);
	DECLARE @response nvarchar(max);
	DECLARE @internal_logs nvarchar(max);
	DECLARE @api int;
	DECLARE @created_date_time datetimeoffset(7);
	DECLARE @sandbox bit;
	DECLARE @recs_deleted int;

	SET @recs_deleted = 0;

    -- Insert statements for procedure here
	DECLARE archive_events_cursor CURSOR FOR 
		SELECT [Id],[Token],[Status],[Message],[Request],[RequestUrl],[Response],[InternalLogs],[API],[CreatedDateTime],[Sandbox] 
		  FROM [cp_api].[dbo].[Event]	
		 where DATEDIFF(DD,CreatedDateTime,GETDATE()) > @daysOld 
		 order by Id asc;

	OPEN archive_events_cursor
	FETCH NEXT FROM archive_events_cursor 
	INTO @event_id, @token, @status, @message, @request, @request_url,@response, @internal_logs, @api, @created_date_time, @sandbox
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--print 'processing id:' + Cast(@event_id as varchar)
		BEGIN TRY
			begin transaction;
			INSERT INTO [dbo].[EventArchive]
			   ([Token],[Status],[Message],[Request],[RequestUrl],[Response],[InternalLogs],[API],[CreatedDateTime],[Sandbox])
			VALUES
			   (@token, @status, @message, @request, @request_url,@response, @internal_logs, @api, @created_date_time, @sandbox);
			--print 'inserted into archive table id:' + Cast(@event_id as varchar);

			delete from [dbo].[Event] where Id = @event_id;
		--	print 'deleted id:' + Cast(@event_id as varchar);
			SET @recs_deleted = @recs_deleted + 1;
			commit;
		END TRY
		BEGIN CATCH
		--	print 'rolling back id:' + Cast(@event_id as varchar);
			rollback;
		END CATCH
		FETCH NEXT FROM archive_events_cursor
		INTO @event_id, @token, @status, @message, @request, @request_url,@response, @internal_logs, @api, @created_date_time, @sandbox
	END 
	CLOSE archive_events_cursor;
	DEALLOCATE archive_events_cursor;
	return @recs_deleted;
END
GO
