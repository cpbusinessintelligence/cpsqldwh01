SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tejes Singam
-- Create date: 26/03/2018
-- Description:	Creates aggregate data for historical API requests
-- =============================================
CREATE PROCEDURE [dbo].[SP_Request_APIHistoricalReport]	 (@Startdate datetime,@EndDate datetime)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

--DECLARE @Month int
--DECLARE @Year int

--SET @Month=3
--SET @year=2018


--SET @StartDate='2018-02-01'
--SET @EndDate='2018-02-28'


select acc.CpAccount as AccountCode
      ,acc.Name as AccountName
	  ,count(e.[Id]) as RequestCount,
sum(case when RequestUrl='/v1/domestic/deliveryOptions' then 1 else 0 end) as DeliveryOptionsCount,
sum(case when RequestUrl='/v1/international/countries' then 1 else 0 end) as CountriesCount,
sum(case when RequestUrl='/v1/domestic/couponcalculator' then 1 else 0 end) as CouponCalculatorCount,
sum(case when RequestUrl='/v1/domestic/bookPickup' then 1 else 0 end) as DomesticPickupCount,
sum(case when RequestUrl='/v2/domestic/bookPickup' then 1 else 0 end) as DomesticPickupV2,
sum(case when RequestUrl='/v1/domestic/quote' then 1 else 0 end) as DomesticQuotingv1Count,
sum(case when RequestUrl='/v2/domestic/quote' then 1 else 0 end) as DomesticQuotingv2Count,
sum(case when RequestUrl='/v1/domestic/shipment/bulkcreate' then 1 else 0 end) as DomesticShipmentBulkCreateCount,
sum(case when RequestUrl='/v1/domestic/shipment/cancel' then 1 else 0 end) as DomesticShipmentCancellationCount,
sum(case when RequestUrl='/v1/domestic/shipment/create' then 1 else 0 end) as DomesticShipmentCreationV1Count,
sum(case when RequestUrl='/v2/domestic/shipment/create' then 1 else 0 end) as DomesticShipmentCreationV2Count,
sum(case when RequestUrl='/v1/domestic/shipment/label' then 1 else 0 end) as DomesticShipmentLabelCount,
sum(case when RequestUrl='/v1/domestic/shipment/validate' then 1 else 0 end) as Domesticvalidationv1Count,
sum(case when RequestUrl='/v2/domestic/shipment/validate' then 1 else 0 end) as Domesticvalidationv2Count,
sum(case when RequestUrl='/v1/international/quote' then 1 else 0 end) as InternationalQuoting,
sum(case when RequestUrl='/v1/international/shipment/create' then 1 else 0 end) as InternationalShipmentCreationCount,
sum(case when RequestUrl='/v1/international/shipment/label' then 1 else 0 end) as InternationalShipmentLabelsCount,
sum(case when RequestUrl='/v1/international/shipment/validate' then 1 else 0 end) as InternationalValidationCount,
sum(case when RequestUrl='/v1/domestic/locateParcel' then 1 else 0 end) as LocateParcelCount,
sum(case when RequestUrl='/v1/locations' then 1 else 0 end) as LocationsCount,
sum(case when RequestUrl='/v1/domestic/redirect' then 1 else 0 end) as RedirectionOptionsCount,
convert(decimal(15,2),0.00) as TotalRevenue
into #Temp1
from [cp_api].[dbo].[Event] e (NOLOCK)
join cp_api.dbo.Token as t (NOLOCK) on e.Token = t.Id
join cp_api.dbo.Account as acc (NOLOCK) on t.Account = acc.Id
where convert(date,e.[CreatedDateTime]) between DATEADD(MONTH,-5,@StartDate) and @EndDate
GROUP BY acc.CpAccount ,acc.Name
ORDER  BY count(e.[Id]) DESC


Select T.AccountCode
      ,T.AccountName
	  ,MAX(RequestCount) as RequestCount
	  ,SUM(P.[BilledTotal]) as TotalRevenue
	  ,MAX(LocateParcelCount) as LocateParcelCount
	  ,MAX(DeliveryOptionsCount) as DeliveryOptionsCount
	  ,MAX(DomesticPickupCount) as DomesticPickupCount
	  ,MAX(DomesticPickupV2) as DomesticPickupV2
	  ,MAX(DomesticQuotingv1Count) as DomesticQuotingv1Count
	  ,MAX(DomesticQuotingv2Count) as DomesticQuotingv2Count
	  ,MAX(DomesticShipmentCreationV1Count) as DomesticShipmentCreationV1Count
	  ,MAX(DomesticShipmentCreationV2Count) as DomesticShipmentCreationV2Count
	  ,MAX(DomesticShipmentLabelCount) as DomesticShipmentLabelCount
	  ,MAX(Domesticvalidationv1Count) as Domesticvalidationv1Count
	  ,MAX(Domesticvalidationv2Count) as Domesticvalidationv2Count
	  ,MAX(DomesticShipmentBulkCreateCount) as DomesticShipmentBulkCreateCount
	  ,MAX(DomesticShipmentCancellationCount) as DomesticShipmentCancellationCount
	  ,MAX(InternationalQuoting) as InternationalQuoting
	  ,MAX(InternationalShipmentCreationCount) as InternationalShipmentCreationCount
	  ,MAX(InternationalShipmentLabelsCount) as InternationalShipmentLabelsCount
	  ,MAX(InternationalValidationCount) as InternationalValidationCount
	  ,MAX(LocationsCount) as LocationsCount
	  ,MAX(CountriesCount) as CountriesCount
	  ,MAX(CouponCalculatorCount) as CouponCalculatorCount
	  ,MAX(RedirectionOptionsCount) as RedirectionOptionsCount
	  from #Temp1 T JOIN [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
		ON T.AccountCode=P.[AccountCode]
		where [BillingDate] >= DATEADD(MONTH,-5,@StartDate) and [BillingDate] <= @EndDate
		GROUP BY  T.AccountCode
      ,T.AccountName
	  ORDER BY MAX(RequestCount) DESC,SUM(P.[BilledTotal]) DESC


END

GO
GRANT EXECUTE
	ON [dbo].[SP_Request_APIHistoricalReport]
	TO [ReportUser_1]
GO
GRANT EXECUTE
	ON [dbo].[SP_Request_APIHistoricalReport]
	TO [ReportUser]
GO
