SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jeff Embro
-- Create date: 08/08/2016
-- Description:	Creates aggregate data for requests
-- =============================================
CREATE PROCEDURE [dbo].[SP_Request_Aggregates]	 
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	truncate table dw_request_counts;

	insert into dw_request_counts
	SELECT e.[Token] as TokenId
		  ,e.[API] as ApiId
		  ,e.[Sandbox]     
		  ,acc.Id as AccountId
		  ,acc.Name as AccountName
		  ,acc.CpAccount
		  ,min(e.CreatedDateTime) as FirstHit
		  ,max(e.CreatedDateTime) as LastHit
		  ,count(e.[Id]) as RequestCount	
	  FROM (select [Id],[Token],[API],[Sandbox],[CreatedDateTime] from [cp_api].[dbo].[Event]) as e
	  --(UNION select [Id],[Token],[API],[Sandbox],[CreatedDateTime] from [cp_api].[dbo].[EventArchive]) as e
		join cp_api.dbo.Token as t on e.Token = t.Id
		join cp_api.dbo.Account as acc on t.Account = acc.Id
	  where isnull(e.token,0)<> 0
	--  and acc.Id not in (1,25)
	  group by e.[Token]
			,e.[API]
			,e.[Sandbox]
			,acc.Id
			,acc.Name
			,acc.CpAccount;

	truncate table dw_request_count_totals;

	insert into dw_request_count_totals
	select tmp2.AccountId,tmp2.AccountName,tmp2.CpAccount
		, min(tmp2.FirstHitTest) as FirstHitTest 
		, min(tmp2.FirstHitLive) as FirstHitLive
		, max(tmp2.LastHitTest) as LastHitTest 
		, max(tmp2.LastHitLive) as LastHitLive
		, sum(tmp2.RequestCountTest) as RequestCountTest
		, sum(tmp2.RequestCountLive) as RequestCountLive		
		from 
		(
			select tmp.AccountId,tmp.AccountName,tmp.CpAccount, tmp.Sandbox
			, case when tmp.Sandbox = 1 then min(tmp.FirstHit) else NULL END as FirstHitTest
			, case when tmp.Sandbox = 0 then min(tmp.FirstHit) else NULL END as FirstHitLive
			, case when tmp.Sandbox = 1 then max(tmp.LastHit) else NULL END as LastHitTest
			, case when tmp.Sandbox = 0 then max(tmp.LastHit) else NULL END as LastHitLive
			, case when tmp.Sandbox = 1 then sum(tmp.RequestCount) else 0 END as RequestCountTest
			, case when tmp.Sandbox = 0 then sum(tmp.RequestCount) else 0 END as RequestCountLive
			from dw_request_counts as tmp (NOLOCK)
			group by tmp.AccountId,tmp.AccountName,tmp.CpAccount, tmp.Sandbox
		) as tmp2 	
		group by tmp2.AccountId,tmp2.AccountName,tmp2.CpAccount
	 ;

	return 1;
END
GO
