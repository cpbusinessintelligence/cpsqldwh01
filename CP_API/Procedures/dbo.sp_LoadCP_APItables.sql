SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_LoadCP_APItables] as
begin


truncate table  [dbo].[Account]
Insert into [Account]
SELECT * FROM [CPSQLWEB03].[cp_api].[dbo].[Account] with (NOLOCK)



Truncate table [dbo].[API]
set IDENTITY_INSERT [dbo].[API] on
Insert into [API] ([Id]
      ,[Tag]
      ,[Title]
      ,[UrlPath])
SELECT [Id]
      ,[Tag]
      ,[Title]
      ,[UrlPath] FROM [CPSQLWEB03].[cp_api].[dbo].[API] with (NOLOCK)
set IDENTITY_INSERT [dbo].[API] off


Truncate table [dbo].[ELMAH_Error]
set IDENTITY_INSERT [dbo].[ELMAH_Error] on
Insert into [ELMAH_Error] ([ErrorId]
      ,[Application]
      ,[Host]
      ,[Type]
      ,[Source]
      ,[Message]
      ,[User]
      ,[StatusCode]
      ,[TimeUtc]
      ,[Sequence]
      ,[AllXml])
SELECT [ErrorId]
      ,[Application]
      ,[Host]
      ,[Type]
      ,[Source]
      ,[Message]
      ,[User]
      ,[StatusCode]
      ,[TimeUtc]
      ,[Sequence]
      ,[AllXml] FROM [CPSQLWEB03].[cp_api].[dbo].[ELMAH_Error] with (NOLOCK)
set IDENTITY_INSERT [dbo].[ELMAH_Error] off


Truncate table [dbo].[ErrorAttribute]
set IDENTITY_INSERT [dbo].[ErrorAttribute] on
Insert into [ErrorAttribute] ([Id]
      ,[MappingID]
      ,[MatchOn]
      ,[Replacement])
SELECT [Id]
      ,[MappingID]
      ,[MatchOn]
      ,[Replacement] FROM [CPSQLWEB03].[cp_api].[dbo].[ErrorAttribute] with (NOLOCK)
set IDENTITY_INSERT [dbo].[ErrorAttribute] off


Truncate table [dbo].[ErrorMap]
set IDENTITY_INSERT [dbo].[ErrorMap] on
Insert into [ErrorMap] ( [Id]
      ,[ExternalSystem]
      ,[ExternalResponseCode]
      ,[ExternalMessage]
      ,[InternalResponseCode]
      ,[InternalMessage])
SELECT  [Id]
      ,[ExternalSystem]
      ,[ExternalResponseCode]
      ,[ExternalMessage]
      ,[InternalResponseCode]
      ,[InternalMessage] FROM [CPSQLWEB03].[cp_api].[dbo].[ErrorMap] with (NOLOCK)
set IDENTITY_INSERT [dbo].[ErrorMap]  off

--Truncate table [dbo].[Event]
--Insert into [Event]
--SELECT * FROM [CPSQLWEB03].[cp_api].[dbo].[Event] with (NOLOCK)
set IDENTITY_INSERT [Event] on
Insert into [Event] ([Id]
      ,[Token]
      ,[Status]
      ,[Message]
      ,[Request]
      ,[RequestUrl]
      ,[Response]
      ,[InternalLogs]
      ,[API]
      ,[CreatedDateTime]
      ,[Sandbox])
SELECT [Id]
      ,[Token]
      ,[Status]
      ,[Message]
      ,[Request]
      ,[RequestUrl]
      ,[Response]
      ,[InternalLogs]
      ,[API]
      ,[CreatedDateTime]
      ,[Sandbox] FROM [CPSQLWEB03].[cp_api].[dbo].[Event] with (NOLOCK) where convert(date,CreatedDateTime) = convert(date,getdate()-1)
set IDENTITY_INSERT [Event] off



--Truncate table [dbo].[EventArchive]
--Insert into [EventArchive]
--SELECT * FROM [CPSQLWEB03].[cp_api].[dbo].[EventArchive] with (NOLOCK)




Truncate table [dbo].[ExternalSystem]
set IDENTITY_INSERT [ExternalSystem] on
Insert into [ExternalSystem] ([Id]
      ,[Tag])
SELECT [Id]
      ,[Tag] FROM [CPSQLWEB03].[cp_api].[dbo].[ExternalSystem] with (NOLOCK)
set IDENTITY_INSERT [ExternalSystem] off


Truncate table [dbo].[Permission]
set IDENTITY_INSERT [Permission] on
Insert into [Permission] ([Id]
      ,[Account]
      ,[API]
      ,[Active])
SELECT [Id]
      ,[Account]
      ,[API]
      ,[Active] FROM [CPSQLWEB03].[cp_api].[dbo].[Permission] with (NOLOCK)
set IDENTITY_INSERT [Permission] off

Truncate table [dbo].[ResetRequest]
set IDENTITY_INSERT [dbo].[ResetRequest] on
Insert into [ResetRequest] ([Id]
      ,[User]
      ,[Code]
      ,[CreatedDate]
      ,[Active])
SELECT [Id]
      ,[User]
      ,[Code]
      ,[CreatedDate]
      ,[Active] FROM [CPSQLWEB03].[cp_api].[dbo].[ResetRequest] with (NOLOCK)
set IDENTITY_INSERT [dbo].[ResetRequest] off


Truncate table [dbo].[Setting]
set IDENTITY_INSERT [dbo].[Setting] on
Insert into [Setting] ([Id]
      ,[Setting]
      ,[Value])
SELECT [Id]
      ,[Setting]
      ,[Value] FROM [CPSQLWEB03].[cp_api].[dbo].[Setting] with (NOLOCK)
set IDENTITY_INSERT [dbo].[Setting] off



Truncate table [dbo].[Token]
set IDENTITY_INSERT   [dbo].[Token] on
Insert into [Token] ( [Id]
      ,[Account]
      ,[Description]
      ,[Token]
      ,[Sandbox]
      ,[Active]
      ,[CreatedDateTime])
SELECT  [Id]
      ,[Account]
      ,[Description]
      ,[Token]
      ,[Sandbox]
      ,[Active]
      ,[CreatedDateTime] FROM [CPSQLWEB03].[cp_api].[dbo].[Token] with (NOLOCK)
set IDENTITY_INSERT   [dbo].[Token] off


--Truncate table [dbo].[User]
set IDENTITY_INSERT  [dbo].[User] on
Insert into [User] ( [Id]
      ,[Name]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[Email]
      ,[Password]
      ,[Admin]
      ,[AccountId])
SELECT  [Id]
      ,[Name]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[Email]
      ,[Password]
      ,[Admin]
      ,[AccountId] FROM [CPSQLWEB03].[cp_api].[dbo].[User] u with (NOLOCK) where u.[id] not in 
	  (select [ID] from [User])
set IDENTITY_INSERT  [dbo].[User] off

end


GO
