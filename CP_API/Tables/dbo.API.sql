SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[API] (
		[Id]          [int] IDENTITY(1, 1) NOT NULL,
		[Tag]         [nvarchar](300) COLLATE Latin1_General_CI_AS NOT NULL,
		[Title]       [nvarchar](300) COLLATE Latin1_General_CI_AS NULL,
		[UrlPath]     [nvarchar](300) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_API]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[API] SET (LOCK_ESCALATION = TABLE)
GO
