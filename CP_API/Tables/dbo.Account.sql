SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account] (
		[Id]                   [int] NOT NULL,
		[Name]                 [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[CreatedDate]          [datetimeoffset](7) NOT NULL,
		[HourlyLimit]          [int] NULL,
		[DailyLimit]           [int] NULL,
		[ActiveTest]           [bit] NOT NULL,
		[ActiveProduction]     [bit] NOT NULL,
		[CpAccount]            [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_Account]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Account]
	WITH CHECK
	ADD CONSTRAINT [FK_Account_User]
	FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id])
ALTER TABLE [dbo].[Account]
	CHECK CONSTRAINT [FK_Account_User]

GO
ALTER TABLE [dbo].[Account] SET (LOCK_ESCALATION = TABLE)
GO
