SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ErrorAttribute] (
		[Id]              [int] IDENTITY(1, 1) NOT NULL,
		[MappingID]       [int] NOT NULL,
		[MatchOn]         [nvarchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Replacement]     [nvarchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_ErrorAttribute]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ErrorAttribute] SET (LOCK_ESCALATION = TABLE)
GO
