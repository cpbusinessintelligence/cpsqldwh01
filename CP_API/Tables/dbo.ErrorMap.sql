SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ErrorMap] (
		[Id]                       [int] IDENTITY(1, 1) NOT NULL,
		[ExternalSystem]           [int] NOT NULL,
		[ExternalResponseCode]     [nvarchar](300) COLLATE Latin1_General_CI_AS NOT NULL,
		[ExternalMessage]          [nvarchar](300) COLLATE Latin1_General_CI_AS NOT NULL,
		[InternalResponseCode]     [nvarchar](300) COLLATE Latin1_General_CI_AS NULL,
		[InternalMessage]          [nvarchar](300) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ErrorMap]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ErrorMap] SET (LOCK_ESCALATION = TABLE)
GO
