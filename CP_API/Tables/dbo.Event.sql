SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Event] (
		[Id]                  [int] IDENTITY(1, 1) NOT NULL,
		[Token]               [int] NULL,
		[Status]              [nvarchar](300) COLLATE Latin1_General_CI_AS NOT NULL,
		[Message]             [nvarchar](300) COLLATE Latin1_General_CI_AS NULL,
		[Request]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RequestUrl]          [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Response]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[InternalLogs]        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[API]                 [int] NULL,
		[CreatedDateTime]     [datetimeoffset](7) NOT NULL,
		[Sandbox]             [bit] NOT NULL,
		CONSTRAINT [PK_Event]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_Event_Sandbox_Status_CreatedDateTime]
	ON [dbo].[Event] ([Sandbox], [Status], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_Token_CreatedDateTime]
	ON [dbo].[Event] ([Token], [CreatedDateTime])
	INCLUDE ([Status], [Sandbox])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_Sandbox_Status_Token_CreatedDateTime]
	ON [dbo].[Event] ([API], [Sandbox], [Status], [Token], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_Sandbox_Status_Token_CreatedDateTime]
	ON [dbo].[Event] ([Sandbox], [Status], [Token], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_Status_Token_CreatedDateTime]
	ON [dbo].[Event] ([API], [Status], [Token], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_Status_Token_CreatedDateTime]
	ON [dbo].[Event] ([Status], [Token], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_Sandbox_Token_CreatedDateTime]
	ON [dbo].[Event] ([API], [Sandbox], [Token], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_Sandbox_Token_CreatedDateTime]
	ON [dbo].[Event] ([Sandbox], [Token], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_Token_CreatedDateTime]
	ON [dbo].[Event] ([API], [Token], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_Sandbox_Status_CreatedDateTime]
	ON [dbo].[Event] ([API], [Sandbox], [Status], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_Status_CreatedDateTime]
	ON [dbo].[Event] ([API], [Status], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_Status_CreatedDateTime]
	ON [dbo].[Event] ([Status], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_Sandbox_CreatedDateTime]
	ON [dbo].[Event] ([API], [Sandbox], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_Sandbox_CreatedDateTime]
	ON [dbo].[Event] ([Sandbox], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_API_CreatedDateTime]
	ON [dbo].[Event] ([API], [CreatedDateTime])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Event_CreatedDateTime]
	ON [dbo].[Event] ([CreatedDateTime])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Event] SET (LOCK_ESCALATION = TABLE)
GO
