SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Event_archive_1] (
		[Id]                  [int] NULL,
		[Token]               [int] NULL,
		[Status]              [nvarchar](300) COLLATE Latin1_General_CI_AS NULL,
		[Message]             [nvarchar](300) COLLATE Latin1_General_CI_AS NULL,
		[Request]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RequestUrl]          [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Response]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[InternalLogs]        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[API]                 [int] NULL,
		[CreatedDateTime]     [datetimeoffset](7) NULL,
		[Sandbox]             [bit] NULL
)
GO
ALTER TABLE [dbo].[Event_archive_1] SET (LOCK_ESCALATION = TABLE)
GO
