SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Permission] (
		[Id]          [int] IDENTITY(1, 1) NOT NULL,
		[Account]     [int] NOT NULL,
		[API]         [int] NOT NULL,
		[Active]      [bit] NOT NULL,
		CONSTRAINT [PK_Permission]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Permission] SET (LOCK_ESCALATION = TABLE)
GO
