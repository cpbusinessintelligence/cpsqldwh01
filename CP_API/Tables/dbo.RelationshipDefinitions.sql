SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RelationshipDefinitions] (
		[Id]                   [int] IDENTITY(1, 1) NOT NULL,
		[PK_Table]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FK_Table]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PK_Id]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FK_Id]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RelationshipName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RelationshipDefinitions] SET (LOCK_ESCALATION = TABLE)
GO
