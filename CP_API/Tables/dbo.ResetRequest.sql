SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ResetRequest] (
		[Id]              [int] IDENTITY(1, 1) NOT NULL,
		[User]            [int] NOT NULL,
		[Code]            [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDate]     [datetimeoffset](7) NOT NULL,
		[Active]          [bit] NOT NULL,
		CONSTRAINT [PK_ResetRequest]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ResetRequest] SET (LOCK_ESCALATION = TABLE)
GO
