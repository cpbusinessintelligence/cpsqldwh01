SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Setting] (
		[Id]          [int] IDENTITY(1, 1) NOT NULL,
		[Setting]     [nvarchar](300) COLLATE Latin1_General_CI_AS NOT NULL,
		[Value]       [nvarchar](300) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_Setting]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Setting]
	ON [dbo].[Setting] ([Setting])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Setting] SET (LOCK_ESCALATION = TABLE)
GO
