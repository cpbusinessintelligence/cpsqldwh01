SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Token] (
		[Id]                  [int] IDENTITY(1, 1) NOT NULL,
		[Account]             [int] NOT NULL,
		[Description]         [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[Token]               [nvarchar](300) COLLATE Latin1_General_CI_AS NOT NULL,
		[Sandbox]             [bit] NOT NULL,
		[Active]              [bit] NOT NULL,
		[CreatedDateTime]     [datetimeoffset](7) NOT NULL,
		CONSTRAINT [PK_Token]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Token]
	ON [dbo].[Token] ([Token])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Token_Account_Token]
	ON [dbo].[Token] ([Account], [Token])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Token] SET (LOCK_ESCALATION = TABLE)
GO
