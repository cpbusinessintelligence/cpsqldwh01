SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User] (
		[Id]              [int] IDENTITY(1, 1) NOT NULL,
		[Name]            [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[Active]          [bit] NOT NULL,
		[CreatedBy]       [int] NOT NULL,
		[CreatedDate]     [datetimeoffset](7) NOT NULL,
		[Email]           [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[Password]        [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[Admin]           [bit] NOT NULL,
		[AccountId]       [int] NULL,
		CONSTRAINT [UQ__User__A9D105345AA7F073]
		UNIQUE
		NONCLUSTERED
		([Email])
		ON [PRIMARY],
		CONSTRAINT [PK_User]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[User]
	WITH CHECK
	ADD CONSTRAINT [FK_User_User]
	FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id])
ALTER TABLE [dbo].[User]
	CHECK CONSTRAINT [FK_User_User]

GO
ALTER TABLE [dbo].[User] SET (LOCK_ESCALATION = TABLE)
GO
