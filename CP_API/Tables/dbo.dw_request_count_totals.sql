SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dw_request_count_totals] (
		[AccountId]            [int] NOT NULL,
		[AccountName]          [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[CpAccount]            [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[FirstHitTest]         [datetimeoffset](7) NULL,
		[FirstHitLive]         [datetimeoffset](7) NULL,
		[LastHitTest]          [datetimeoffset](7) NULL,
		[LastHitLive]          [datetimeoffset](7) NULL,
		[RequestCountTest]     [int] NULL,
		[RequestCountLive]     [int] NULL
)
GO
ALTER TABLE [dbo].[dw_request_count_totals] SET (LOCK_ESCALATION = TABLE)
GO
