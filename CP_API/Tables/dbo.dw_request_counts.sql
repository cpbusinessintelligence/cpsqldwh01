SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dw_request_counts] (
		[TokenId]          [int] NULL,
		[ApiId]            [int] NULL,
		[Sandbox]          [bit] NOT NULL,
		[AccountId]        [int] NOT NULL,
		[AccountName]      [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[CpAccount]        [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[FirstHit]         [datetimeoffset](7) NULL,
		[LastHit]          [datetimeoffset](7) NULL,
		[RequestCount]     [int] NULL
)
GO
ALTER TABLE [dbo].[dw_request_counts] SET (LOCK_ESCALATION = TABLE)
GO
