SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jobin philip
-- Create date: 26 March 2019
-- Description:	This function will look for Public holidays nationally, state wise and Zone wise
-- =============================================
CREATE FUNCTION [dbo].[JP_GetNextAvailableCycle] 
(	
	-- Add the parameters for the function here
	@Date Date,  -- This will be ReadyAtTime if provided by customer . Otherwise leave blank
	@PickupPostcode Varchar(20),
	@PickupSuburb Varchar(20)
)
RETURNS  @OutputTable table (NextCycleStart DateTime , NextCycleFinish DateTime, Comment Varchar(50))
AS
 
	BEGIN
	   
	 Declare @State as Varchar(20)
	 Declare @Zone as Varchar(20)
	 Declare @Profile as Varchar(20)
	 Declare @CutOff1 as time(7)
	 Declare @CutOff2 as time(7)
	 Declare @CutOff3 as time(7)
	 Declare @CutOff4 as time(7)
	 Declare @CutOff5 as time(7)

	 Select @State ='', 
	        @Zone = '' ,
			@Profile = 'dd',
	        @CutOff1 = CutOff1, 
			@CutOff2 = CutOff2,
			@CutOff3 = CutOff3, 
			@CutOff4 = CutOff4, 
			@CutOff5 = CutOff5 
		from JP_SUburbs 
		Where Postcode = @PickupPostcode and Suburb = @PickupSuburb

    if isnull(@Profile,'') = '' 
	    Begin
	    Insert into @OutputTable Select Null,Null,@PickupSuburb
		END
	Else
	  Insert into @OutputTable Select Null,Null,'No profile found'

	 
	Return 
	--SELECT Datename(weekday,@Date) as td , @Date as date

END
GO
