SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_GetParameter] (@sCode varchar(100))
returns varchar(800)
as
begin
	Declare @sParmValue varchar(800)

	Select 
		@sParmValue = [Value] 
	From 
		Parameters
	Where 
		[Code]=@sCode

	If @sParmValue Is Null
		Select @sParmValue=''

	Return @sParmValue
end
GO
