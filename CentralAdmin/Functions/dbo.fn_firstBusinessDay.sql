SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_firstBusinessDay](@date date , @State varchar(20) ,@Zone Varchar(20))
RETURNS int
AS

BEGIN;
    -- Pass teh state and Zone to find out zone wise public holidays
    --- The iteration/output variable:
    DECLARE @businessDay As date = @date;
	   DECLARE @Count As int = 0;


	Declare @TempHolidays table ( TempDate Date, [HolidayType] Varchar(20), [State/Zone Name] Varchar(20) )
    INSERT INTO @TempHolidays SELECT top 10 [HolidayType] ,[State/Zone Name],[Date]    
							  FROM [CentralAdmin].[dbo].[JP_PublicHolidays]
							  Where Date > = @Date and Date <= Getdate() +20  order by Date asc 

    --- Increment the value by one day if this
   


    --WHILE (EXISTS (SELECT [date] FROM [dbo].[JP_PublicHolidays] WHERE [date]=@businessDay and [HolidayType] = 'National'))
    --    SET @businessDay=DATEADD(dd, 1, @businessDay);
    --- Done:
    RETURN (Select count(*) From @TempHolidays);
END;


GO
