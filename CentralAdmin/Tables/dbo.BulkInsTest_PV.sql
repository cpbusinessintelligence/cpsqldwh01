SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BulkInsTest_PV] (
		[VarcharS]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IntS]            [int] NULL,
		[BitS]            [bit] NULL,
		[VarcharSMax]     [varchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[BulkInsTest_PV] SET (LOCK_ESCALATION = TABLE)
GO
