SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouriersPlease_Inventory_Bulk_Update] (
		[Service No ]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tier 1]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description   User]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Financial Code]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Reporting Budget]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Last Bill Date]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Name]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CouriersPlease_Inventory_Bulk_Update] SET (LOCK_ESCALATION = TABLE)
GO
