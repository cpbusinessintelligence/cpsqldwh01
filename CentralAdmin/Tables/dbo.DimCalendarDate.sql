SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimCalendarDate] (
		[Id]                      [int] IDENTITY(1, 1) NOT NULL,
		[Date]                    [date] NOT NULL,
		[Day]                     [int] NULL,
		[WeekNumber]              [int] NULL,
		[WeekDay]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[WeekEndingdate]          [date] NULL,
		[CalendarMonthNumber]     [int] NULL,
		[CalendarMonthName]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CalendarQuarter]         [int] NULL,
		[CalendarYear]            [int] NULL,
		[FiscalMonthNumber]       [int] NULL,
		[FiscalQuarter]           [int] NULL,
		[FiscalYear]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SortOrder]               [int] NULL,
		CONSTRAINT [UQ__DimCalen__77387D0712EBE630]
		UNIQUE
		NONCLUSTERED
		([Date])
		ON [PRIMARY],
		CONSTRAINT [PK__DimCalen__3214EC070DF03496]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimCalendarDate] SET (LOCK_ESCALATION = TABLE)
GO
