SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_ProfileDefinition] (
		[JP_ProfileDefinitionID]     [int] IDENTITY(1, 1) NOT NULL,
		[ProfileType]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProfileCode]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProfileDescription]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]                   [bit] NOT NULL,
		[AddWho]                     [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]                [datetime] NOT NULL,
		[EditWho]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]               [datetime] NULL,
		CONSTRAINT [PK_JP_ProfileDefinitionID]
		PRIMARY KEY
		CLUSTERED
		([JP_ProfileDefinitionID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[JP_ProfileDefinition] SET (LOCK_ESCALATION = TABLE)
GO
