SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_PublicHolidays] (
		[JP_PublicHolidaysID]     [int] IDENTITY(1, 1) NOT NULL,
		[HolidayType]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[State/Zone Name]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date]                    [date] NOT NULL,
		[Description]             [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]             [datetime] NOT NULL,
		[EditWho]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]            [datetime] NULL,
		CONSTRAINT [PK_JP_PublicHolidaysID]
		PRIMARY KEY
		CLUSTERED
		([JP_PublicHolidaysID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[JP_PublicHolidays] SET (LOCK_ESCALATION = TABLE)
GO
