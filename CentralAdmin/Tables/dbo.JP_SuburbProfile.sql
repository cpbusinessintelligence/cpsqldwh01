SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_SuburbProfile] (
		[JP_SuburbProfileID]     [int] IDENTITY(1, 1) NOT NULL,
		[ProfileCode]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProfileDescription]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DayOfWeek]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsWorking]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[NumberCycles]           [int] NOT NULL,
		[AddWho]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]            [datetime] NOT NULL,
		[EditWho]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]           [datetime] NULL,
		CONSTRAINT [PK_JP_SuburbProfileID]
		PRIMARY KEY
		CLUSTERED
		([JP_SuburbProfileID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[JP_SuburbProfile] SET (LOCK_ESCALATION = TABLE)
GO
