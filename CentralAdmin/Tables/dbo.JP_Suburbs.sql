SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_Suburbs] (
		[JP_SuburbsID]      [int] IDENTITY(1, 1) NOT NULL,
		[Postcode]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Suburb]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceCycle]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CutOff1]           [time](7) NULL,
		[CutOff2]           [time](7) NULL,
		[CutOff3]           [time](7) NULL,
		[CutOff4]           [time](7) NULL,
		[CutOff5]           [time](7) NULL,
		[AddWho]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]       [datetime] NOT NULL,
		[EditWho]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]      [datetime] NULL,
		[State]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Zone]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[WeekDayNumber]     [int] NULL,
		CONSTRAINT [PK_JP_SuburbsID]
		PRIMARY KEY
		CLUSTERED
		([JP_SuburbsID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[JP_Suburbs] SET (LOCK_ESCALATION = TABLE)
GO
