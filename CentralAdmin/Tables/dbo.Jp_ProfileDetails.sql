SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Jp_ProfileDetails] (
		[ProfileDetailID]     [int] IDENTITY(1, 1) NOT NULL,
		[ProfileID]           [int] NOT NULL,
		[StartTime]           [time](7) NOT NULL,
		[EndTime]             [time](7) NOT NULL,
		[IsWorking]           [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]         [datetime] NOT NULL,
		[EditWho]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]        [datetime] NULL,
		[Order]               [int] NULL,
		CONSTRAINT [PK_Jp_ProfileDetails]
		PRIMARY KEY
		CLUSTERED
		([ProfileDetailID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Jp_ProfileDetails] SET (LOCK_ESCALATION = TABLE)
GO
