SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mobile phones] (
		[ Mobile Phones]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Chumma]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[No]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NOPe]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Nopeee]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Name]               [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Mobile phones] SET (LOCK_ESCALATION = TABLE)
GO
