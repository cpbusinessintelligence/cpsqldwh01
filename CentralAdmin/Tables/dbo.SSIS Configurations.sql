SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SSIS Configurations] (
		[ConfigurationFilter]     [nvarchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConfiguredValue]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[PackagePath]             [nvarchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConfiguredValueType]     [nvarchar](20) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SSIS Configurations] SET (LOCK_ESCALATION = TABLE)
GO
