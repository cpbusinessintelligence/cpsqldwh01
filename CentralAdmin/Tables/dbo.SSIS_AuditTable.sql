SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SSIS_AuditTable] (
		[Id]            [uniqueidentifier] NOT NULL,
		[FileName]      [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Message]       [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[Action]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Process]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TimeStamp]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[SSIS_AuditTable]
	ADD
	CONSTRAINT [DF_SSIS_AuditTable_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SSIS_AuditTable]
	ADD
	CONSTRAINT [DF_SSIS_AuditTable_TimeStamp]
	DEFAULT (getdate()) FOR [TimeStamp]
GO
ALTER TABLE [dbo].[SSIS_AuditTable] SET (LOCK_ESCALATION = TABLE)
GO
