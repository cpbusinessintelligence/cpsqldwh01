SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SSIS_ETLConfigurations] (
		[ID]                               [int] NOT NULL,
		[Category]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FTPDownloadGroup]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FTPType]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FTPPort]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FTPServer]                        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FTPLogin]                         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FTPPassword]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FTPFolder]                        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FTPExtractCompletionFilename]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[FTPFileName]                      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[BypassCopyingfromFTPServer]       [bit] NOT NULL,
		[BypassUnzippingFiles]             [bit] NOT NULL,
		[ProcessingDirectory]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ArchiveDirectory]                 [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ErrorDirectory]                   [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[GZZipFileName]                    [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[TarZipFileName]                   [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FileName]                         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FileExtension]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RowDelimiter]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ColumnDelimiter]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ByPassLoadTable]                  [bit] NOT NULL,
		[LoadTableName]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TableName]                        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LoadStoreProceedure]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]                         [bit] NOT NULL,
		[IsDeleted]                        [bit] NOT NULL,
		[EmailNotifyCompletion]            [bit] NOT NULL,
		[ErrorNotifyEmailAddress]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NotifyCompleteEmailAddressTo]     [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NotifyCOmpleteEmailAddressCc]     [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[AddedBy]                          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AddedOn]                          [datetime] NOT NULL,
		CONSTRAINT [PK__SSIS_ETL__3214EC27AA7D703E]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations]
	ADD
	CONSTRAINT [DF_SSIS_ETLConfigurations_AddedOn]
	DEFAULT (getdate()) FOR [AddedOn]
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations]
	ADD
	CONSTRAINT [DF_SSIS_ETLConfigurations_BypassCopyingfromFTPServer]
	DEFAULT ((0)) FOR [BypassCopyingfromFTPServer]
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations]
	ADD
	CONSTRAINT [DF_SSIS_ETLConfigurations_ByPassLoadTable]
	DEFAULT ((0)) FOR [ByPassLoadTable]
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations]
	ADD
	CONSTRAINT [DF_SSIS_ETLConfigurations_BypassUnzippingFiles]
	DEFAULT ((0)) FOR [BypassUnzippingFiles]
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations]
	ADD
	CONSTRAINT [DF_SSIS_ETLConfigurations_EmailNotifyCompletion]
	DEFAULT ((0)) FOR [EmailNotifyCompletion]
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations]
	ADD
	CONSTRAINT [DF_SSIS_ETLConfigurations_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations]
	ADD
	CONSTRAINT [DF_SSIS_ETLConfigurations_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SSIS_ETLConfigurations] SET (LOCK_ESCALATION = TABLE)
GO
