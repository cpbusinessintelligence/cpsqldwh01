SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SSIS_FileToTableMap_bkup_AH] (
		[Id]                          [uniqueidentifier] NOT NULL,
		[FileName]                    [varchar](254) COLLATE Latin1_General_CI_AS NULL,
		[FtpFileName]                 [varchar](254) COLLATE Latin1_General_CI_AS NULL,
		[LoadTableName]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TableName]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LoadStoredProcedure]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LegacyDbStoredProcedure]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RowDelimiter]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ColumnDelimiter]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FtpDownloadGroup]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsFlag]                      [bit] NULL,
		[IsActive]                    [bit] NULL,
		[EmailNotifyCompletion]       [bit] NULL,
		[NotifyEmailAddressTo]        [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[NotifyEmailAddressCc]        [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__SSIS_Fil__3214EC07B7E8FD3C]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SSIS_FileToTableMap_bkup_AH]
	ADD
	CONSTRAINT [DF_SSIS_FileToTableMap_Id_bkup_AH]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SSIS_FileToTableMap_bkup_AH]
	ADD
	CONSTRAINT [DF_SSIS_FileToTableMap_IsFlag_bkup_AH]
	DEFAULT ((0)) FOR [IsFlag]
GO
ALTER TABLE [dbo].[SSIS_FileToTableMap_bkup_AH]
	ADD
	CONSTRAINT [DF_SSIS_FileToTableMap_DeleteData_bkup_AH]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SSIS_FileToTableMap_bkup_AH] SET (LOCK_ESCALATION = TABLE)
GO
