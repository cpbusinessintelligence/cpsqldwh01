SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMail] (
		[SendMailID]           [int] NOT NULL,
		[FromAddr]             [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToAddr]               [varchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Subject]              [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[MessageLine1]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine2]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine3]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine4]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine5]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine6]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine7]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine8]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine9]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[MessageLine10]        [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[TransmitFlag]         [int] NOT NULL,
		[AddWho]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]          [datetime] NOT NULL,
		[ModifiedDateTime]     [datetime] NULL,
		CONSTRAINT [PK__SendMail__D9D8B1DBCC46C4A0]
		PRIMARY KEY
		CLUSTERED
		([SendMailID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SendMail] SET (LOCK_ESCALATION = TABLE)
GO
