SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscriptions] (
		[Category]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Reportname]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[To]                   [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[CC]                   [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[BCC]                  [varchar](800) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IncludeReport]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IncludeLink]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[RenderFormat]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[shouldsendreport]     [int] NULL,
		[isactive]             [int] NULL,
		[sendorder]            [int] NULL,
		[serialnumber]         [int] IDENTITY(1, 1) NOT NULL,
		[Subject]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[isconsolidate]        [bit] NULL
)
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscription__CC__5AD01DD6]
	DEFAULT ('') FOR [CC]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscriptio__BCC__5BC4420F]
	DEFAULT ('') FOR [BCC]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Param__5CB86648]
	DEFAULT ('') FOR [Parameter1]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Param__5DAC8A81]
	DEFAULT ('') FOR [Parameter2]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Param__5EA0AEBA]
	DEFAULT ('') FOR [Parameter3]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Param__5F94D2F3]
	DEFAULT ('') FOR [Parameter4]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Param__6088F72C]
	DEFAULT ('') FOR [Parameter5]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Inclu__617D1B65]
	DEFAULT ('True') FOR [IncludeReport]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Inclu__62713F9E]
	DEFAULT ('False') FOR [IncludeLink]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Rende__636563D7]
	DEFAULT ('Excel') FOR [RenderFormat]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__shoul__64598810]
	DEFAULT ((1)) FOR [shouldsendreport]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__isact__654DAC49]
	DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__sendo__6641D082]
	DEFAULT ((1)) FOR [sendorder]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__Subje__01E9EAF7]
	DEFAULT ('') FOR [Subject]
GO
ALTER TABLE [dbo].[Subscriptions]
	ADD
	CONSTRAINT [DF__Subscript__iscon__3B226853]
	DEFAULT ((0)) FOR [isconsolidate]
GO
GRANT ALTER
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT CONTROL
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT DELETE
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT INSERT
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT REFERENCES
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT SELECT
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT UPDATE
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
GRANT VIEW DEFINITION
	ON [dbo].[Subscriptions]
	TO [ReportUser]
GO
ALTER TABLE [dbo].[Subscriptions] SET (LOCK_ESCALATION = TABLE)
GO
