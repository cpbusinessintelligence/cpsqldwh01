SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Subscriptions_Development] (
		[Category]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Reportname]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[To]                   [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[CC]                   [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[BCC]                  [varchar](800) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IncludeReport]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IncludeLink]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[RenderFormat]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[shouldsendreport]     [int] NULL,
		[isactive]             [int] NULL,
		[sendorder]            [int] NULL,
		[serialnumber]         [int] IDENTITY(1, 1) NOT NULL,
		[Subject]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[isconsolidate]        [bit] NULL
)
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscription__CC__5AD01DD61]
	DEFAULT ('') FOR [CC]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscriptio__BCC__5BC4420F1]
	DEFAULT ('') FOR [BCC]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Param__5CB866481]
	DEFAULT ('') FOR [Parameter1]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Param__5DAC8A811]
	DEFAULT ('') FOR [Parameter2]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Param__5EA0AEBA1]
	DEFAULT ('') FOR [Parameter3]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Param__5F94D2F31]
	DEFAULT ('') FOR [Parameter4]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Param__6088F72C1]
	DEFAULT ('') FOR [Parameter5]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Inclu__617D1B651]
	DEFAULT ('True') FOR [IncludeReport]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Inclu__62713F9E1]
	DEFAULT ('False') FOR [IncludeLink]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Rende__636563D71]
	DEFAULT ('Excel') FOR [RenderFormat]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__shoul__645988101]
	DEFAULT ((1)) FOR [shouldsendreport]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__isact__654DAC491]
	DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__sendo__6641D0821]
	DEFAULT ((1)) FOR [sendorder]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__Subje__01E9EAF71]
	DEFAULT ('') FOR [Subject]
GO
ALTER TABLE [dbo].[Subscriptions_Development]
	ADD
	CONSTRAINT [DF__Subscript__iscon__3B2268531]
	DEFAULT ((0)) FOR [isconsolidate]
GO
ALTER TABLE [dbo].[Subscriptions_Development] SET (LOCK_ESCALATION = TABLE)
GO
