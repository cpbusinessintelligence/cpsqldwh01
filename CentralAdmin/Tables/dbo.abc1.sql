SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[abc1] (
		[Record type]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Consignment reference]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Consignment date]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Manifest reference]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Manifest date]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Account code]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender name]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender address 1]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender address 2]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender locality]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender State]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender postcode]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Receiver name]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address 1]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address 2]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Receiver locality]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Receiver state]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Receiver postcode]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer reference]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Release ASN]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Return Authorisation Number]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 3]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 4]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Special instructions]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Item quantity]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Declared weight]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Measured weight]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Declared volume]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Measured volume]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Price override]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Insurance category]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Declared value]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Insurance Price Override]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Test Flag]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Dangerous goods flag]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Release Not Before]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Release Not After]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Logistics Units]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[abc1] SET (LOCK_ESCALATION = TABLE)
GO
