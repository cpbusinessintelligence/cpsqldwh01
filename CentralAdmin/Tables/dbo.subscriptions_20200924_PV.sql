SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriptions_20200924_PV] (
		[Category]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Reportname]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[To]                   [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[CC]                   [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[BCC]                  [varchar](800) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IncludeReport]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IncludeLink]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[RenderFormat]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[shouldsendreport]     [int] NULL,
		[isactive]             [int] NULL,
		[sendorder]            [int] NULL,
		[serialnumber]         [int] IDENTITY(1, 1) NOT NULL,
		[Subject]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[isconsolidate]        [bit] NULL
)
GO
ALTER TABLE [dbo].[subscriptions_20200924_PV] SET (LOCK_ESCALATION = TABLE)
GO
