SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[test] (
		[Name]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[City]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Zip]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[test] SET (LOCK_ESCALATION = TABLE)
GO
