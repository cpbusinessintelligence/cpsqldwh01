SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[test2] (
		[DELTA]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_bookno]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_date]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_type]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_book_time]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_custid]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_pcustid]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_dcustid]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_fromsub]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_tosub]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_from 1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_from 2]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_from 3]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_from 4]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_from 5]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_to 1]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_to 2]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_to 3]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_to 4]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_to 5]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_instr 1]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_instr 2]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_instr 3]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_instr 4]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 1]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 2]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 3]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 4]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 5]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 6]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 7]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 8]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 9]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 10]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 11]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 12]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 13]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 14]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 15]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_comments 16]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_caller]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_who]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_where]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_created_by]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_allocated_by]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_driver]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_cdriver]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_ddriver]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_pdriver]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_status]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_booked]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_alloc]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_pickup]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_complete]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_pod]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_pod_name]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_queued]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_sent]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_received]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_time_accepted]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_rp]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_flags]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_eta_from]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_eta_to]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_qkey]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_rebook]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_qoperid]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_coperid]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_query_time]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_emm_id]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_rebook_bookno]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[b_rebook_date]       [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[test2] SET (LOCK_ESCALATION = TABLE)
GO
