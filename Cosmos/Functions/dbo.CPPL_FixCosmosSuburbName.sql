SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.CPPL_FixCosmosSuburbName
(
	@SuburbName	varchar(100)
)
RETURNS varchar(100)
AS
BEGIN

	SELECT @SuburbName = LTRIM(RTRIM(ISNULL(@SuburbName, '')));
	
	IF CHARINDEX('<', @SuburbName) > 1
	BEGIN
		SELECT @SuburbName = LEFT(@SuburbName, (CHARINDEX('<', @SuburbName) - 1));
	END
	
	IF CHARINDEX('{', @SuburbName) > 1
	BEGIN
		SELECT @SuburbName = LEFT(@SuburbName, (CHARINDEX('{', @SuburbName) - 1));
	END

	IF CHARINDEX('(', @SuburbName) > 1
	BEGIN
		SELECT @SuburbName = LEFT(@SuburbName, (CHARINDEX('(', @SuburbName) - 1));
	END

	IF CHARINDEX('/', @SuburbName) > 1
	BEGIN
		SELECT @SuburbName = LEFT(@SuburbName, (CHARINDEX('/', @SuburbName) - 1));
	END

	IF CHARINDEX(',', @SuburbName) > 1
	BEGIN
		SELECT @SuburbName = LEFT(@SuburbName, (CHARINDEX(',', @SuburbName) - 1));
	END

	IF CHARINDEX('-', @SuburbName) > 1
	BEGIN
		SELECT @SuburbName = LEFT(@SuburbName, (CHARINDEX('-', @SuburbName) - 1));
	END

	IF CHARINDEX('$', @SuburbName) > 1
	BEGIN
		SELECT @SuburbName = LEFT(@SuburbName, (CHARINDEX('$', @SuburbName) - 1));
	END

	SELECT @SuburbName = REPLACE(
							REPLACE(
								REPLACE(
									REPLACE(
										REPLACE(
											REPLACE(
												REPLACE(
													REPLACE(
														REPLACE(
															REPLACE(
																@SuburbName
															, '9', '')
														, '8', '')
													, '7', '')
												, '6', '')
											, '5', '')
										, '4', '')
									, '3', '')
								, '2', '')
							, '1', '')
						 , '0', '')

	RETURN LTRIM(RTRIM(@SuburbName));

END;
GO
