SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_FormatPhoneNumbers]
(
	@Input	varchar(50)
)
RETURNS varchar(50)
AS
BEGIN
    Declare @Output  as Varchar(50) = ''
    Declare @Process as Varchar(50)
    Select @Process = Ltrim(Rtrim(REPLACE(@Input,' ','')))
    if LEN(@Process) = 9 
       Select  @Process = '0'+ @Process
    
    if LEN(@Process) = 10   -- Full Phone Number
       BEGIN
         if SUBSTRING(@process,2,1) = '4'  -- Mobile
           Select @Output = SUBSTRING(@process,1,4)+' '+  SUBSTRING(@process,5,3) + ' ' +  SUBSTRING(@process,8,3)       
         Else  -- Land Line
           Select @Output = SUBSTRING(@process,1,2)+' '+  SUBSTRING(@process,3,4) + ' ' +  SUBSTRING(@process,7,4)
       END
    if LEN(@Process) = 8   -- Land Line  
        Select @Output =   SUBSTRING(@process,1,4) + ' ' +  SUBSTRING(@process,5,4)
        
    if @Output = ''
       Select @Output = @Input 
	RETURN @Output;

END;

GO
GRANT EXECUTE
	ON [dbo].[CPPL_FormatPhoneNumbers]
	TO [ReportUser]
GO
