SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_DriverDetailsChangedinCosmos]

AS
BEGIN

	SET NOCOUNT ON;
       SELECT [Branch]
      ,[DriverNumber]
      --,[ProntoId] 
      --,[ContractorType]
      --,[DriverName]
      --,[CompanyName]
      --,[StartDate]
      --,[EndDate]
      --,[OuterDriver]
      --,[LastModifiedDate]
      --,[CreatedDate]
      --,[EffectiveDate]
      --,[IsActive]
      --,[ArchiveDate]
      --,[ArchiveReason1]
        INTO #Temp1
        FROM [Cosmos].[dbo].[Driver] Where LastModifiedDate > GETDATE() -2
        
     SELECT D.[Branch]
      ,D.[DriverNumber]
      ,[ProntoId] 
      ,IsNull([ContractorType],'') as [ContractorType]
      ,[DriverName]
      ,[CompanyName]
      ,Convert(Date,[StartDate]) as [StartDate]
      ,[EndDate]
      ,[OuterDriver]
      ,[LastModifiedDate]
      ,[CreatedDate]
      ,[EffectiveDate]
      ,Case [IsActive] When 0 THEN 'N' ELSe 'Y' END as IsActive
      ,[ArchiveDate]
      ,[ArchiveReason1]
     FROM [Cosmos].[dbo].[Driver] D Join #Temp1 T On D.Branch = T.Branch and D.DriverNumber = T.DriverNumber
     Order by  D.[Branch] asc,D.Drivernumber asc,D.LastModifiedDate Desc

	SET NOCOUNT OFF;
	
END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_DriverDetailsChangedinCosmos]
	TO [ReportUser]
GO
