SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_FranchiseContactDetails]
 @State		varchar(20)
AS
BEGIN
 /* This store proc is used for Reports*/

   SET NOCOUNT ON;
   Select CONVERT(vARCHAR(20),case branch WHEN 'Sydney' THEN 'NSW' WHEN 'Brisbane' THEN 'QLD' WHEN 'Goldcoast' THEN 'QLD' WHEN 'Gold Coast' THEN 'QLD' WHEN 'Adelaide' THEN 'SA' WHEN 'Melbourne' THEN 'VIC' ELSE Branch END ) As State,
          Case when isnull(branch, '') = 'Goldcoast'	then 'Gold Coast'	else isnull(branch, '')end as [Branch],
		  drivernumber as [Run Number],
		  isnull(companyname, drivername) as [Business Name],
		  --ISNULL(DriverName, '') as [Driver Name],
		  isnull(phonenumber, '') as [Phone],
		  isnull(mobilenumber, '') as [Mobile],
		  startdate as [Start Date],
		  ISNULL(ABN, '') as [ABN],
		  ISNULL(ProntoId, 'UNKNOWN') AS [FranchiseeCode]
   Into #Temp1
   from Cosmos.dbo.driver (nolock)
   where isactive = 1 
         and IsDeleted = 0
         and isnull(contractortype, '') = 'C'
		 and DriverNumber < 8000
		 and Branch NOT IN ('nkope','perth')
		 and ISNULL(ProntoId, '') NOT LIKE '[ABCGMSO]99'
		 AND ISNULL(DriverName, '') NOT LIKE '%MANAGEMENT%'
		 AND ISNULL(DriverName, '') NOT LIKE '%mgmt%'
		 AND ISNULL(DriverName, '') NOT LIKE '%mgt%'
		 AND ISNULL(DriverName, '') NOT LIKE '%managme%'
		 AND ISNULL(DriverName, '') NOT LIKE '%second scanner%'
		 AND ISNULL(DriverName, '') NOT LIKE '%vacant run%'
		 AND ISNULL(DriverName, '') NOT LIKE '%test unit%'
   order by branch asc, drivernumber asc

   Select State
         ,Branch
         ,[Run Number]
         ,[Business Name] 
         ,dbo.CPPL_FormatPhoneNumbers(Phone) as Phone
         ,dbo.CPPL_FormatPhoneNumbers(Mobile) as Mobile
         ,[Start Date]
         ,ABN
         ,FranchiseeCode  
   from #Temp1 
   WHERE State = @State 
         ANd [FranchiseeCode] <>'UNKNO'
	SET NOCOUNT OFF;
	
END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_FranchiseContactDetails]
	TO [ReportUser]
GO
