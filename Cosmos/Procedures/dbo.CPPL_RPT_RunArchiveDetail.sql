SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_RunArchiveDetail]
(
	@Branch		varchar(20),
	@StartDate	date,
	@EndDate	date
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT @Branch = LTRIM(RTRIM(ISNULL(@Branch, '')));
	SELECT @EndDate = ISNULL(@EndDate, GETDATE());
	SELECT @StartDate = ISNULL(@StartDate, DATEADD(year, -3, @EndDate));
	
	select 
	distinct 
	UPPER(CASE ISNULL(Branch, '')
		WHEN 'goldcoast' THEN 'Gold Coast'
		ELSE ISNULL(Branch, '')
	END) AS Branch, 
	CONVERT(smallint, CASE ISNUMERIC(MobileId)
		WHEN 1 THEN CONVERT(smallint, MobileId)
		ELSE 0
	END) AS [MobileId],
	DriverNumber AS [ActualRunId],
	ISNULL(DriverName, '') AS [DriverName], ISNULL(CompanyName, '') AS [CompanyName],
	ISNULL(PhoneNumber, '') AS [PhoneNumber], 
	ISNULL(MobileNumber, '') AS [MobileNumber],
	StartDate, EndDate,
	ArchiveDate, 
	CASE LTRIM(RTRIM(ISNULL(ArchiveReason1, '')))
		WHEN '' THEN 'UNKNOWN'
		ELSE LTRIM(RTRIM(ISNULL(ArchiveReason1, '')))
	END AS [ArchiveReason],
	ISNULL(ProntoId, 'UNKNOWN') AS [FranchiseeCode]
	 from Driver 
	where ArchiveDate is not null
	AND ArchiveDate BETWEEN @StartDate AND @EndDate 
	AND
	(
		(LTRIM(RTRIM(ISNULL(ArchiveReason2, ''))) = ''
		 AND
		 LTRIM(RTRIM(ISNULL(ArchiveReason3, ''))) = ''
		 AND
		 LTRIM(RTRIM(ISNULL(ArchiveReason4, ''))) = '')
		OR
		(LTRIM(RTRIM(ISNULL(ArchiveReason1, ''))) != '')
	)
	AND IsActive = 1
	AND ((LTRIM(RTRIM(ISNULL(Branch, ''))) = @Branch) OR (@Branch = ''))
	UNION ALL
	select 
	distinct 
	UPPER(CASE ISNULL(Branch, '')
		WHEN 'goldcoast' THEN 'Gold Coast'
		ELSE ISNULL(Branch, '')
	END) AS Branch, 
	CONVERT(smallint, CASE ISNUMERIC(MobileId)
		WHEN 1 THEN CONVERT(smallint, MobileId)
		ELSE 0
	END) AS [MobileId],
	DriverNumber AS [ActualRunId],
	DriverName, CompanyName,
	ISNULL(PhoneNumber, '') AS [PhoneNumber], 
	ISNULL(MobileNumber, '') AS [MobileNumber],
	StartDate, EndDate,
	ArchiveDate, 
	LTRIM(RTRIM(ISNULL(ArchiveReason2, ''))) AS [ArchiveReason],
	ISNULL(ProntoId, 'UNKNOWN') AS [FranchiseeCode]
	 from Driver 
	where ArchiveDate is not null
	AND ArchiveDate BETWEEN @StartDate AND @EndDate 
	AND LTRIM(RTRIM(ISNULL(ArchiveReason2, ''))) != ''
	AND IsActive = 1
	AND ((LTRIM(RTRIM(ISNULL(Branch, ''))) = @Branch) OR (@Branch = ''))
	UNION ALL
	select 
	distinct 
	UPPER(CASE ISNULL(Branch, '')
		WHEN 'goldcoast' THEN 'Gold Coast'
		ELSE ISNULL(Branch, '')
	END) AS Branch, 
	CONVERT(smallint, CASE ISNUMERIC(MobileId)
		WHEN 1 THEN CONVERT(smallint, MobileId)
		ELSE 0
	END) AS [MobileId],
	DriverNumber AS [ActualRunId],
	DriverName, CompanyName,
	ISNULL(PhoneNumber, '') AS [PhoneNumber], 
	ISNULL(MobileNumber, '') AS [MobileNumber],
	StartDate, EndDate,
	ArchiveDate, 
	LTRIM(RTRIM(ISNULL(ArchiveReason3, ''))) AS [ArchiveReason],
	ISNULL(ProntoId, 'UNKNOWN') AS [FranchiseeCode]
	 from Driver 
	where ArchiveDate is not null
	AND ArchiveDate BETWEEN @StartDate AND @EndDate 
	AND LTRIM(RTRIM(ISNULL(ArchiveReason3, ''))) != ''
	AND IsActive = 1
	AND ((LTRIM(RTRIM(ISNULL(Branch, ''))) = @Branch) OR (@Branch = ''))
	UNION ALL
	select 
	distinct 
	UPPER(CASE ISNULL(Branch, '')
		WHEN 'goldcoast' THEN 'Gold Coast'
		ELSE ISNULL(Branch, '')
	END) AS Branch, 
	CONVERT(smallint, CASE ISNUMERIC(MobileId)
		WHEN 1 THEN CONVERT(smallint, MobileId)
		ELSE 0
	END) AS [MobileId],
	DriverNumber AS [ActualRunId],
	DriverName, CompanyName,
	ISNULL(PhoneNumber, '') AS [PhoneNumber], 
	ISNULL(MobileNumber, '') AS [MobileNumber],
	StartDate, EndDate,
	ArchiveDate, 
	LTRIM(RTRIM(ISNULL(ArchiveReason4, ''))) AS [ArchiveReason],
	ISNULL(ProntoId, 'UNKNOWN') AS [FranchiseeCode]
	 from Driver 
	where ArchiveDate is not null
	AND ArchiveDate BETWEEN @StartDate AND @EndDate 
	AND LTRIM(RTRIM(ISNULL(ArchiveReason4, ''))) != ''
	AND IsActive = 1
	AND ((LTRIM(RTRIM(ISNULL(Branch, ''))) = @Branch) OR (@Branch = ''))

	--ORDER BY Branch, MobileId 

	SET NOCOUNT OFF;
	
END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_RunArchiveDetail]
	TO [ReportUser]
GO
