SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC dbo.CPPL_RPT_RunArchiveSummary
(
	@Branch		varchar(20),
	@StartDate	date,
	@EndDate	date
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT @Branch = LTRIM(RTRIM(ISNULL(@Branch, '')));
	SELECT @EndDate = ISNULL(@EndDate, GETDATE());
	SELECT @StartDate = ISNULL(@StartDate, DATEADD(year, -3, @EndDate));

	SELECT DISTINCT
	UPPER(CASE ISNULL(d.Branch, '')
			WHEN 'goldcoast' THEN 'Gold Coast'
			ELSE ISNULL(d.Branch, '')
	END) AS [Branch],
	CASE LTRIM(RTRIM(ISNULL(ArchiveReason1, '')))
		WHEN '' THEN 'UNKNOWN'
		ELSE UPPER(LTRIM(RTRIM(ISNULL(ArchiveReason1, ''))))
	END AS [Reason],
	COUNT(*) AS [EventCount],
	CASE WHEN MONTH(d.ArchiveDate) < 7
		THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
		ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
	END AS [Year]
	from Driver d (NOLOCK)
	where d.ArchiveDate Is Not Null
	AND d.ArchiveDate BETWEEN @StartDate AND @EndDate
	AND d.IsActive = 1
	AND (
			(LTRIM(RTRIM(ISNULL(d.Branch, ''))) = @Branch)
			 OR
			(@Branch = '')
	)
	GROUP BY LTRIM(RTRIM(ISNULL(ArchiveReason1, ''))),
		d.Branch,
		CASE WHEN MONTH(d.ArchiveDate) < 7
			THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
			ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
		END
	UNION ALL
	SELECT DISTINCT
	UPPER(CASE ISNULL(d.Branch, '')
			WHEN 'goldcoast' THEN 'Gold Coast'
			ELSE ISNULL(d.Branch, '')
	END) AS [Branch],
	LTRIM(RTRIM(ISNULL(ArchiveReason2, ''))) AS [Reason],
	COUNT(*) AS [EventCount],
	CASE WHEN MONTH(d.ArchiveDate) < 7
		THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
		ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
	END AS [Year]
	from Driver d (NOLOCK)
	where d.ArchiveDate Is Not Null
	AND d.ArchiveDate BETWEEN @StartDate AND @EndDate
	AND d.IsActive = 1
	AND (
			(LTRIM(RTRIM(ISNULL(d.Branch, ''))) = @Branch)
			 OR
			(@Branch = '')
	)
	AND LTRIM(RTRIM(ISNULL(ArchiveReason2, ''))) != ''
	GROUP BY LTRIM(RTRIM(ISNULL(ArchiveReason2, ''))),
		d.Branch,
		CASE WHEN MONTH(d.ArchiveDate) < 7
			THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
			ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
		END
	UNION ALL
	SELECT DISTINCT
	UPPER(CASE ISNULL(d.Branch, '')
			WHEN 'goldcoast' THEN 'Gold Coast'
			ELSE ISNULL(d.Branch, '')
	END) AS [Branch],
	LTRIM(RTRIM(ISNULL(ArchiveReason3, ''))) AS [Reason],
	COUNT(*) AS [EventCount],
	CASE WHEN MONTH(d.ArchiveDate) < 7
		THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
		ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
	END AS [Year]
	from Driver d (NOLOCK)
	where d.ArchiveDate Is Not Null
	AND d.ArchiveDate BETWEEN @StartDate AND @EndDate
	AND d.IsActive = 1
	AND (
			(LTRIM(RTRIM(ISNULL(d.Branch, ''))) = @Branch)
			 OR
			(@Branch = '')
	)
	AND LTRIM(RTRIM(ISNULL(ArchiveReason3, ''))) != ''
	GROUP BY LTRIM(RTRIM(ISNULL(ArchiveReason3, ''))),
		d.Branch,
		CASE WHEN MONTH(d.ArchiveDate) < 7
			THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
			ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
		END
	UNION ALL
	SELECT DISTINCT
	UPPER(CASE ISNULL(d.Branch, '')
			WHEN 'goldcoast' THEN 'Gold Coast'
			ELSE ISNULL(d.Branch, '')
	END) AS [Branch],
	LTRIM(RTRIM(ISNULL(ArchiveReason4, ''))) AS [Reason],
	COUNT(*) AS [EventCount],
	CASE WHEN MONTH(d.ArchiveDate) < 7
		THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
		ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
	END AS [Year]
	from Driver d (NOLOCK)
	where d.ArchiveDate Is Not Null
	AND d.ArchiveDate BETWEEN @StartDate AND @EndDate
	AND d.IsActive = 1
	AND (
			(LTRIM(RTRIM(ISNULL(d.Branch, ''))) = @Branch)
			 OR
			(@Branch = '')
	)
	AND LTRIM(RTRIM(ISNULL(ArchiveReason4, ''))) != ''
	GROUP BY LTRIM(RTRIM(ISNULL(ArchiveReason4, ''))),
		d.Branch,
		CASE WHEN MONTH(d.ArchiveDate) < 7
			THEN CONVERT(char(4), (YEAR(d.ArchiveDate) - 1)) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate)))
			ELSE CONVERT(char(4), (YEAR(d.ArchiveDate))) + '/' + CONVERT(char(4), (YEAR(d.ArchiveDate) + 1))
		END;
	--ORDER BY Reason ASC, [Year] ASC

	SET NOCOUNT OFF;

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_RunArchiveSummary]
	TO [ReportUser]
GO
