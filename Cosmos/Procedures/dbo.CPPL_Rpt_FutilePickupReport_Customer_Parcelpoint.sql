SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[CPPL_Rpt_FutilePickupReport_Customer_Parcelpoint] 
(
	@StartDate	date,
	@EndDate	date
	--@Account varchar(500)
)
AS
BEGIN

      --'=====================================================================
    --' CP -Stored Procedure -CPPL_Rpt_FutilePickupReport
    --' ---------------------------
    --' Purpose: Displays all futile Pickups-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 04 Feb 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 04/02/2015    AB      1.00    Created the procedure                             --AB20150204
	--' 17/02/2015    AB      1.00    Modified the procedure to inclue consignment number --AB20150204

    --'=====================================================================

select cb_consignment,cb_cosmos_date,cb_cosmos_job as BookingId into #temp from cpplEDI.dbo.cdcosmosbooking where cb_cosmos_date between @StartDate and @EndDate



select cd_connote as Connote,
       convert(date,cb_cosmos_date)  as Bookingdate,
       BookingId,
       cd_account as CustomerAccount,
       d.shortname as CustomerName,
       convert(varchar(100),'') as  PickupDriver,
       cd_pickup_addr0 as PickupCustomer,
       b.b_name as PickupBranch
into #temp1 from #temp t left join cpplEDI.dbo.consignment c on cb_consignment=cd_id
left join cpplEDI.dbo.Branchs b on b.b_id=cd_pickup_branch
left join cpplEDI.dbo.Branchs b1 on b1.b_id=cd_deliver_branch
left join Pronto.dbo.Prontodebtor d on d.accountcode=c.cd_account


  select   Bookingnumber,
           b.Branch,
		   case when b.branch='Adelaide' then 'SA'
		   when b.branch='Brisbane' or b.branch='GoldCoast' then 'QLD'
		   when  b.branch='Melbourne' then 'VIC'
		   when b.branch='Sydney' then 'NSW'
		   when b.branch='Perth' then 'WA'
		   else 'Unknown' end as State,
           b.BookingId,
		 isnull(BookingDate,'') as BookingDate,
         isnull(BookingDateTime,'') as BookingDateTime,
		 BookingType,
		 bookingcustomerid,
		 convert(varchar(100),'') as Customercode,
		 convert(varchar(500),'') as CustomerName,
		 convert(varchar(500),'') as PickupCustomer,
		 convert(varchar(100),'') as Connote,
		-- convert(varchar(100),'') as DeliveryAddress1,
		-- convert(varchar(100),'') as DeliveryAddress2,
		-- convert(varchar(100),'') as DeliveryAddress3,
		 b_futile_stamp as FutileStamp,
		 convert(int,b_futile_driver) as FutileDriver,
		 b_futile_reason as FutileReason
		 into #temp2
    from cosmos.dbo.booking b 
		 where bookingtype='J' and (b_futile_stamp is not null and b_futile_stamp<>'') 
		 and convert(date,bookingdate) between @StartDate and @EndDate and isactive=1
		 order by b.Branch,b.BookingId,convert(date,Bookingdate) 

		 update #temp2 set Customercode=c.ProntoAccountCode,CustomerName=c.Name
--,DeliveryAddress1=	isnull(c.DeliveryAddress1,''),DeliveryAddress2=isnull(c.DeliveryAddress2,''),DeliveryAddress3=	isnull(c.DeliveryAddress3,'') 
from #temp2 t left join customer c on c.CustomerId=t.BookingCustomerId where c.isactive=1

--update #temp set Customercode=c1.ca_account,CustomerName=c.c_Name
----,DeliveryAddress1=	isnull(c.DeliveryAddress1,''),DeliveryAddress2=isnull(c.DeliveryAddress2,''),DeliveryAddress3=	isnull(c.DeliveryAddress3,'') 
--from #temp t left join [cpplEDI].[dbo].[companies] c on c.c_id=t.BookingCustomerId left join [cpplEDI].[dbo].companyaccount c1 on c1.ca_company_id=c.c_id 

update #temp2 set Customercode=c.ProntoAccountCode,CustomerName=c.Name
--,DeliveryAddress1=	isnull(c.DeliveryAddress1,''),DeliveryAddress2=isnull(c.DeliveryAddress2,''),DeliveryAddress3=	isnull(c.DeliveryAddress3,'') 
from #temp2 t left join customer c on c.CustomerId=t.BookingCustomerId where Customercode is null or customercode=''

update #temp2 set Customercode=Customeraccount,CustomerName=t1.CustomerName,Connote=t1.connote,PickupCustomer=t1.PickupCustomer
from #temp2 t join #temp1 t1 on t.bookingid=t1.BookingId and t.bookingdate=t1.bookingdate and t.branch=replace(t1.PickupBranch,'Gold Coast','GoldCoast')



select * from #temp2 where Customercode in ('112871512','112955711','112994363','112994371','112994389','112994397','113031181','113062244','WI12871512') order by Branch,Futiledriver

	SET NOCOUNT OFF;

END


GO
GRANT EXECUTE
	ON [dbo].[CPPL_Rpt_FutilePickupReport_Customer_Parcelpoint]
	TO [ReportUser]
GO
