SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LoadActivityImport]
AS

      --'=====================================================================
    --' CP -Stored Procedure -[LoadActivityImport]
    --' ---------------------------
    --' Purpose: Loads Activity Import Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

BEGIN

 Declare @Date as Date 
 Select @Date = CONVERT(Date,GETDATE())

 
 UPDATE dbo.Load_ActivityImport SET SerialNumber = Rtrim(Ltrim(REPLACE(SerialNumber,'"','')))
 UPDATE dbo.Load_ActivityImport SET ActivityFlag = Rtrim(Ltrim(REPLACE(ActivityFlag,'"','')))
 
 INSERT INTO [Cosmos].[dbo].[ActivityImport]
           ([ActivityDateTime]
           ,[SerialNumber]
           ,[ActivityFlag]
           ,[ActivityFileOn])
           
        SELECT  Convert(DateTime,[ActivityDateTime])
      ,[SerialNumber]
      ,[ActivityFlag]
      ,@Date
  FROM [Cosmos].[dbo].[Load_ActivityImport]
  
  Truncate table [Cosmos].[dbo].[Load_ActivityImport]
    Select 1
END


GO
