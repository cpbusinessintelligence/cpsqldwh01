SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SP_CosmosMaintenance]

@StartDate datetime = null --'2015-05-28'

As 
Begin
Begin Try
BEGIN TRAN 

--EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

		PRINT '-------- START DISABLING TABLE CONSTRAINT --------';

		ALTER TABLE ActivityImport NOCHECK CONSTRAINT ALL
		ALTER TABLE Booking NOCHECK CONSTRAINT ALL
		ALTER TABLE Customer NOCHECK CONSTRAINT ALL


		
		PRINT '-------- END DISABLING TABLE CONSTRAINT --------';

				PRINT '-------- START COPYING DATA--------';
				------------Add Table ActivityImport Record In Archive-----------------------

				PRINT '-------- COPY ADDRESS --------';
				INSERT INTO ActivityImport_Archive2017 
				select * from ActivityImport with(NoLock) where convert(Date, [ActivityFileOn], 103) <= @StartDate 

				------------Add Table Booking Record In Archive-----------------------

				PRINT '-------- COPY TCONSIGNMENT STAGING TABLE--------';
				INSERT INTO Booking_Archive2017 
				select * from Booking with(NoLock) where convert(Date, CreatedDate, 103) <= @StartDate 


				------------Add Table Customer Record In Archive-----------------------

				PRINT '-------- COPY CONSIGNMENT TABLE --------';
				INSERT INTO Customer_Archive2017 
				select * from Customer with(NoLock) where convert(Date, CreatedDate, 103) <= '2016-01-01' and IsActive = 0 



				

				----------------------------------------------------------------------------------------------------------------------------
				PRINT '-------- END COPYING DATA --------';

		----Delete Record from table---------------------

				PRINT '-------- DELETE DATA START --------';

				Delete from ActivityImport where convert(Date, [ActivityFileOn], 103) <= @StartDate 
		
				Delete from Booking where convert(Date, CreatedDate, 103) <= @StartDate
		
				Delete from Customer where convert(Date, CreatedDate, 103) <= '2016-01-01' and IsActive = 0 
		

				PRINT '-------- DELETE DATA END --------';

		--EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
		PRINT '-------- START ENABLING TABLE CONSTRAINT --------';

		ALTER TABLE ActivityImport CHECK CONSTRAINT ALL
		ALTER TABLE Booking CHECK CONSTRAINT ALL
		ALTER TABLE Customer CHECK CONSTRAINT ALL

		

		PRINT '-------- END ENABLING TABLE CONSTRAINT --------';
		select 'OK' as Result

		COMMIT TRAN 
		--rollback tran
		END TRY
				BEGIN CATCH
				begin
					rollback tran
					INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
					 VALUES
						   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
						   ,'SP_CosmosMaintenance', 2)

						   select cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as Result
				end
				END CATCH

END 

GO
