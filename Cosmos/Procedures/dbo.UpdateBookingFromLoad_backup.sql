SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[UpdateBookingFromLoad_backup]
AS

SET DATEFORMAT ymd;
SET NOCOUNT ON;

/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_Booking]) > 0 
BEGIN
	BEGIN TRAN


	INSERT INTO [Cosmos].[dbo].[Booking]
			   ([BookingId]
			   ,[Branch]
			   ,[BookingDate]
			   ,[BookingType]
			   ,[BookingDateTime]
			   ,[BookingCustomerId]
			   ,[PickupCustomerId]
			   ,[DeliverCustomerId]
			   ,[FromSuburbId]
			   ,[ToSuburbId]
			   ,[FromAddress1]
			   ,[FromAddress2]
			   ,[FromAddress3]
			   ,[FromAddress4]
			   ,[FromAddress5]
			   ,[ToAddress1]
			   ,[ToAddress2]
			   ,[ToAddress3]
			   ,[ToAddress4]
			   ,[ToAddress5]
			   ,[InstructionsLine1]
			   ,[InstructionsLine2]
			   ,[InstructionsLine3]
			   ,[InstructionsLine4]
			   ,[CommentsLine1]
			   ,[CommentsLine2]
			   ,[CommentsLine3]
			   ,[CommentsLine4]
			   ,[CommentsLine5]
			   ,[CommentsLine6]
			   ,[CommentsLine7]
			   ,[CommentsLine8]
			   ,[CommentsLine9]
			   ,[CommentsLine10]
			   ,[CommentsLine11]
			   ,[CommentsLine12]
			   ,[CommentsLine13]
			   ,[CommentsLine14]
			   ,[CommentsLine15]
			   ,[CommentsLine16]
			   ,[Caller]
			   ,[Who]
			   ,[Where]
			   ,[CreatedById]
			   ,[AllocatedBy]
			   ,[DriverId]
			   ,[CDriverId]
			   ,[DeliverDriverId]
			   ,[PickupDriverId]
			   ,[Status]
			   ,[TimeBooked]
			   ,[TimeAllocated]
			   ,[TimePickup]
			   ,[TimeComplete]
			   ,[TimePOD]
			   ,[PODName]
			   ,[TimeQueued]
			   ,[TimeSent]
			   ,[TimeReceived]
			   ,[TimeAccepted]
			   ,[ReceiverPays]
			   ,[Flags]
			   ,[EtaFrom]
			   ,[EtaTo]
			   ,[QueryKey]
			   ,[Rebook]
			   ,[QueryOperatorId]
			   ,[COperatorId]
			   ,[QueryTime]
			   ,[EmmId]
			   ,[RebookBookingId]
			   ,[RebookDate]
			   ,b_futile_stamp
               ,b_futile_driver
               ,b_futile_reason
			   ,[EffectiveDate]
			   ,[IsDeleted])
	SELECT DISTINCT
		  [b_bookno]
		  ,UPPER(LEFT([Load_Booking].[Branch], 1)) + (RIGHT(LOWER([Load_Booking].[Branch]), LEN([Load_Booking].[Branch]) -1)) AS Branch
		  ,CAST([b_date] AS DATE)
		  ,[b_type]
		  ,CAST([b_book_time] AS DATETIME)
		  ,[b_custid]
		  ,[b_pcustid]
		  ,[b_dcustid]
		  ,[b_fromsub]
		  ,[b_tosub]
		  ,[b_from 1]
		  ,[b_from 2]
		  ,[b_from 3]
		  ,[b_from 4]
		  ,[b_from 5]
		  ,[b_to 1]
		  ,[b_to 2]
		  ,[b_to 3]
		  ,[b_to 4]
		  ,[b_to 5]
		  ,[b_instr 1]
		  ,[b_instr 2]
		  ,[b_instr 3]
		  ,[b_instr 4]
		  ,[b_comments 1]
		  ,[b_comments 2]
		  ,[b_comments 3]
		  ,[b_comments 4]
		  ,[b_comments 5]
		  ,[b_comments 6]
		  ,[b_comments 7]
		  ,[b_comments 8]
		  ,[b_comments 9]
		  ,[b_comments 10]
		  ,[b_comments 11]
		  ,[b_comments 12]
		  ,[b_comments 13]
		  ,[b_comments 14]
		  ,[b_comments 15]
		  ,[b_comments 16]
		  ,[b_caller]
		  ,[b_who]
		  ,[b_where]
		  ,[b_created_by]
		  ,[b_allocated_by]
		  ,[b_driver]
		  ,[b_cdriver]
		  ,[b_ddriver]
		  ,[b_pdriver]
		  ,[b_status]
		  ,CAST([b_time_booked] AS DATETIME)
		  ,CAST([b_time_alloc] AS DATETIME)
		  ,CAST([b_time_pickup] AS DATETIME)
		  ,CAST([b_time_complete] AS DATETIME)
		  ,[b_time_pod]
		  ,[b_pod_name]
		  ,[b_time_queued]
		  ,[b_time_sent]
		  ,[b_time_received]
		  ,[b_time_accepted]
		  ,[b_rp]
		  ,[b_flags]
		  ,CAST([b_eta_from] AS TIME)
		  ,CAST([b_eta_to] AS TIME)
		  ,[b_qkey]
		  ,[b_rebook]
		  ,[b_qoperid]
		  ,[b_coperid]
		  ,CAST([b_query_time] AS DATETIME)
		  ,[b_emm_id]
		  ,[b_rebook_bookno]
		  ,CAST([b_rebook_date] AS DATETIME)
		  ,b_futile_stamp
          ,b_futile_driver
          ,b_futile_reason
		  ,[TIMESTAMP]
		  ,0
	  FROM [Load_Booking]
	  WHERE (DELTA = 'ADD' OR DELTA = 'MOD') AND   (ISDATE(b_date) = 1 AND ISDATE([b_book_time]) = 1) AND 
		NOT EXISTS (SELECT * FROM [Booking]
		WHERE Booking.BookingId = Load_Booking.b_bookno
		AND Booking.Branch = Load_Booking.BRANCH
		AND Booking.EffectiveDate = Load_Booking.[TIMESTAMP]
		AND Booking.BookingDate = Load_Booking.b_date
		AND IsActive = 1)


		UPDATE [Booking] 
		SET [IsDeleted] = 1, [LastModifiedDate] = GETDATE()
		FROM [Booking]
		JOIN [Load_Booking] ON Booking.BookingId = Load_Booking.b_bookno
		AND Booking.Branch = Load_Booking.BRANCH
		AND Booking.EffectiveDate = Load_Booking.[TIMESTAMP]
		 WHERE Delta = 'DEL';
		 
	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			TRUNCATE TABLE [Load_Booking];
		END;    

		
END;



GO
