SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[UpdateBookingFromLoad_backup20150216]
AS

      --'=====================================================================
    --' CP -Stored Procedure -[UpdateBookingFromLoad]
    --' ---------------------------
    --' Purpose: Loads Booking Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


SET DATEFORMAT ymd;
SET NOCOUNT ON;

/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_Booking]) > 0 
BEGIN
	BEGIN TRAN;


		/**** Select bookingid with changed and new data ****/
		SELECT Booking.Bookingnumber, l.*
		INTO #Staging	
		FROM [Load_Booking] l
		LEFT JOIN [Booking] ON Bookingnumber=(case when l.Branch='Sydney' then 'SYD' when l.Branch='Melbourne' then 'MEL' when l.Branch='Brisbane' then 'BNE' when l.Branch='Adelaide' then 'ADL' when l.Branch='Goldcoast' then 'GLC' else 'XXX' end)+isnull(convert(varchar(20),year(CAST([b_date] AS DATE))),'0000')+isnull(right('00'+convert(varchar(20),month(CAST([b_date] AS DATE))),2),'00')+isnull(right('00'+convert(varchar(20),day(CAST([b_date] AS DATE))),2),'00')+convert(varchar(20),[dbo].[fn_PrefillZeros] ([b_bookno],6))
       --AND CONVERT(DATETIME, [TIMESTAMP], 120) = CONVERT(DATETIME, EffectiveDate, 120)
		AND Booking.IsActive = 1
		WHERE (Delta = 'ADD' OR Delta = 'MOD') AND 
			(([Booking].EffectiveDate != l.[TIMESTAMP])
				OR
			([Booking].[Bookingnumber] Is Null))

	/**** Mark changed records as inactive ****/
		UPDATE Booking
		SET IsActive = 0 
		FROM #Staging s
		JOIN Booking ON (case when s.Branch='Sydney' then 'SYD' when s.Branch='Melbourne' then 'MEL' when s.Branch='Brisbane' then 'BNE' when s.Branch='Adelaide' then 'ADL' when s.Branch='Goldcoast' then 'GLC' else 'XXX' end)+isnull(convert(varchar(20),year(CAST([b_date] AS DATE))),'0000')+isnull(right('00'+convert(varchar(20),month(CAST([b_date] AS DATE))),2),'00')+isnull(right('00'+convert(varchar(20),day(CAST([b_date] AS DATE))),2),'00')+convert(varchar(20),[dbo].[fn_PrefillZeros] ([b_bookno],6)) = Booking.Bookingnumber

	--WITH dups AS
	--	(
	--		SELECT DISTINCT 
	--		Branch, BookingId,bookingdate, COUNT(*) AS [BookingCount]
	--		FROM Booking 
	--		GROUP BY Branch, BookingId,bookingdate
	--		HAVING COUNT(*) > 1
	--	)
 --      delete Booking
	--	FROM Booking 
	--	JOIN dups 
	--	ON Booking.BookingId = dups.BookingId 
	--	and booking.BookingDate=dups.bookingdate
	--	AND Booking.Branch = dups.Branch 
	--	--AND Booking.EffectiveDate = dups.EffectiveDate 
	--	AND Booking.IsActive = 0;

	


	INSERT INTO [Cosmos].[dbo].[Booking]
			   ([BookingNumber]
			   ,[BookingId]
			   ,[Branch]
			   ,[BookingDate]
			   ,[BookingType]
			   ,[BookingDateTime]
			   ,[BookingCustomerId]
			   ,[PickupCustomerId]
			   ,[DeliverCustomerId]
			   ,[FromSuburbId]
			   ,[ToSuburbId]
			   ,[FromAddress1]
			   ,[FromAddress2]
			   ,[FromAddress3]
			   ,[FromAddress4]
			   ,[FromAddress5]
			   ,[ToAddress1]
			   ,[ToAddress2]
			   ,[ToAddress3]
			   ,[ToAddress4]
			   ,[ToAddress5]
			   ,[InstructionsLine1]
			   ,[InstructionsLine2]
			   ,[InstructionsLine3]
			   ,[InstructionsLine4]
			   ,[CommentsLine1]
			   ,[CommentsLine2]
			   ,[CommentsLine3]
			   ,[CommentsLine4]
			   ,[CommentsLine5]
			   ,[CommentsLine6]
			   ,[CommentsLine7]
			   ,[CommentsLine8]
			   ,[CommentsLine9]
			   ,[CommentsLine10]
			   ,[CommentsLine11]
			   ,[CommentsLine12]
			   ,[CommentsLine13]
			   ,[CommentsLine14]
			   ,[CommentsLine15]
			   ,[CommentsLine16]
			   ,[Caller]
			   ,[Who]
			   ,[Where]
			   ,[CreatedById]
			   ,[AllocatedBy]
			   ,[DriverId]
			   ,[CDriverId]
			   ,[DeliverDriverId]
			   ,[PickupDriverId]
			   ,[Status]
			   ,[TimeBooked]
			   ,[TimeAllocated]
			   ,[TimePickup]
			   ,[TimeComplete]
			   ,[TimePOD]
			   ,[PODName]
			   ,[TimeQueued]
			   ,[TimeSent]
			   ,[TimeReceived]
			   ,[TimeAccepted]
			   ,[ReceiverPays]
			   ,[Flags]
			   ,[EtaFrom]
			   ,[EtaTo]
			   ,[QueryKey]
			   ,[Rebook]
			   ,[QueryOperatorId]
			   ,[COperatorId]
			   ,[QueryTime]
			   ,[EmmId]
			   ,[RebookBookingId]
			   ,[RebookDate]
			   ,b_futile_stamp
               ,b_futile_driver
               ,b_futile_reason
			   ,[EffectiveDate]
			   ,[IsDeleted])
	SELECT DISTINCT
	      (case when Branch='Sydney' then 'SYD' when Branch='Melbourne' then 'MEL' when Branch='Brisbane' then 'BNE' when Branch='Adelaide' then 'ADL' when Branch='Goldcoast' then 'GLC' else 'XXX' end)+isnull(convert(varchar(20),year(CAST([b_date] AS DATE))),'0000')+isnull(right('00'+convert(varchar(20),month(CAST([b_date] AS DATE))),2),'00')+isnull(right('00'+convert(varchar(20),day(CAST([b_date] AS DATE))),2),'00')+convert(varchar(20),[dbo].[fn_PrefillZeros] ([b_bookno],6))
		  ,[b_bookno]
		  ,UPPER(LEFT(s.[Branch], 1)) + (RIGHT(LOWER(s.[Branch]), LEN(s.[Branch]) -1)) AS Branch
		  ,CAST([b_date] AS DATE)
		  ,[b_type]
		  ,[b_book_time]
		  ,[b_custid]
		  ,[b_pcustid]
		  ,[b_dcustid]
		  ,[b_fromsub]
		  ,[b_tosub]
		  ,[b_from 1]
		  ,[b_from 2]
		  ,[b_from 3]
		  ,[b_from 4]
		  ,[b_from 5]
		  ,[b_to 1]
		  ,[b_to 2]
		  ,[b_to 3]
		  ,[b_to 4]
		  ,[b_to 5]
		  ,[b_instr 1]
		  ,[b_instr 2]
		  ,[b_instr 3]
		  ,[b_instr 4]
		  ,[b_comments 1]
		  ,[b_comments 2]
		  ,[b_comments 3]
		  ,[b_comments 4]
		  ,[b_comments 5]
		  ,[b_comments 6]
		  ,[b_comments 7]
		  ,[b_comments 8]
		  ,[b_comments 9]
		  ,[b_comments 10]
		  ,[b_comments 11]
		  ,[b_comments 12]
		  ,[b_comments 13]
		  ,[b_comments 14]
		  ,[b_comments 15]
		  ,[b_comments 16]
		  ,[b_caller]
		  ,[b_who]
		  ,[b_where]
		  ,[b_created_by]
		  ,[b_allocated_by]
		  ,[b_driver]
		  ,[b_cdriver]
		  ,[b_ddriver]
		  ,[b_pdriver]
		  ,[b_status]
		  ,[b_time_booked]
		  ,[b_time_alloc] 
		  ,[b_time_pickup] 
		  ,[b_time_complete] 
		  ,[b_time_pod]
		  ,[b_pod_name]
		  ,[b_time_queued]
		  ,[b_time_sent]
		  ,[b_time_received]
		  ,[b_time_accepted]
		  ,[b_rp]
		  ,[b_flags]
		  ,[b_eta_from]
		  ,[b_eta_to]
		  ,[b_qkey]
		  ,[b_rebook]
		  ,[b_qoperid]
		  ,[b_coperid]
		  ,[b_query_time]
		  ,[b_emm_id]
		  ,[b_rebook_bookno]
		  ,[b_rebook_date]
		  ,b_futile_stamp
          ,b_futile_driver
          ,b_futile_reason
		  ,[TIMESTAMP]
		  ,0
	  FROM #staging s
	  
	  
	  
	 -- [Load_Booking]
	 -- WHERE (DELTA = 'ADD' OR DELTA = 'MOD') AND   (ISDATE(b_date) = 1) and
	  
	 -- -- AND ISDATE([b_book_time]) = 1) AND 
		--NOT EXISTS (SELECT * FROM [Booking]
		--WHERE Booking.BookingId = Load_Booking.b_bookno
		--AND Booking.Branch = Load_Booking.BRANCH
		--AND Booking.EffectiveDate = Load_Booking.[TIMESTAMP]
		--AND Booking.BookingDate = Load_Booking.b_date
		--AND IsActive = 1)


		UPDATE [Booking] 
		SET [IsDeleted] = 1, [LastModifiedDate] = GETDATE()
		FROM [Booking]
		JOIN [Load_Booking] ON Booking.BookingId = Load_Booking.b_bookno
		AND Booking.Branch = Load_Booking.BRANCH
		AND Booking.EffectiveDate = Load_Booking.[TIMESTAMP]
		 WHERE Delta = 'DEL';

		 update [Booking] set isactive=0 where [IsDeleted] = 1 and isactive=1;


		 
	--WITH dups AS
	--	(
	--		SELECT DISTINCT 
	--		Branch, BookingId,bookingdate, COUNT(*) AS [BookingCount]
	--		FROM Booking 
	 --        WHERE [IsActive] = 1
	--		GROUP BY Branch, BookingId,bookingdate
	--		HAVING COUNT(*) > 1
	--	)
 --      delete Booking
	--	FROM Booking 
	--	JOIN dups 
	--	ON Booking.BookingId = dups.BookingId 
	--	and booking.BookingDate=dups.bookingdate
	--	AND Booking.Branch = dups.Branch 
	--	--AND Booking.EffectiveDate = dups.EffectiveDate 
	--	AND Booking.IsActive = 0;
	
		WITH x AS
		(
		select
		BookingId,Branch,bookingdate, COUNT(*) AS [RowCount], MAX(EffectiveDate) AS [MaxEffective]
		from Booking
		where IsActive = 1
		group by BookingId,Branch,bookingdate
		having COUNT(*) > 1
		)
		UPDATE Booking 
		SET IsActive = 0
	--	SELECT *
		FROM Booking JOIN x
		ON x.BookingId = Booking.BookingId and x.Branch=Booking.branch
		AND Booking.IsActive = 1
		AND Booking.EffectiveDate != x.MaxEffective;
		
	


			
		 
	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
		----TRUNCATE TABLE [Load_Booking];
		END;    

		
END;
GO
