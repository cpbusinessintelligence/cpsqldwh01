SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateContractorTypeFromLoad]
AS

      --'=====================================================================
    --' CP -Stored Procedure -[UpdateContractorTypeFromLoad]
    --' ---------------------------
    --' Purpose: Loads Contractor Type Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_ContractorType]) > 0 
BEGIN
	BEGIN TRAN

	/**** Load changed records into staging table ****/
	SELECT ContractorType.Id
		,[DELTA]
		,[Load_ContractorType].[TIMESTAMP]
		,UPPER(LEFT(Load_ContractorType.Branch, 1)) + (RIGHT(LOWER(Load_ContractorType.[Branch]), LEN(Load_ContractorType.[Branch]) -1)) AS Branch
		,[c_conttype]
		,c_conttdescr
		INTO #Staging	
	FROM [Load_ContractorType]
	LEFT JOIN [ContractorType] ON ContractorType.Code = c_conttype
	AND [Load_ContractorType].Branch = [ContractorType].Branch 
	AND CONVERT(DATETIME, [Load_ContractorType].[TIMESTAMP], 120) = CONVERT(DATETIME, EffectiveDate, 120)
	AND ContractorType.IsActive = 1
	WHERE (Delta = 'ADD' OR Delta = 'MOD' ) 


	/**** Update IsActive flag on changed records ****/
	UPDATE ContractorType
	SET IsActive = 1
	SELECT * FROM ContractorType
	JOIN #Staging ON #Staging.Id = ContractorType.Id
	WHERE #Staging.[TIMESTAMP] = EffectiveDate



	/**** InserT new and changed records ****/
	INSERT INTO [Cosmos].[dbo].[ContractorType]
			   ([Branch]
			  ,[Code]
			  ,[Description] 
			  ,[IsActive]
			  ,[EffectiveDate]
			  ,[IsDeleted]
			  ,[CreatedDate]
			  ,[LastModifiedDate]
	)
	SELECT [BRANCH]
		  ,c_conttype
		  ,c_conttdescr
		  ,1 AS [IsActive]
		  ,[TIMESTAMP] AS [EffectiveDate]
		  ,0 AS [IsDeleted]
		  ,GETDATE() AS [CreatedDate]
		  ,GETDATE() AS [LastModifiedDate]
	  FROM #Staging;

	/**** Drop staging table, no longer needed ****/
	DROP TABLE #Staging;


	/**** Mark deleted records accordingly ****/
	UPDATE ContractorType
	SET IsDeleted = 1
	FROM ContractorType 
	JOIN Load_ContractorType ON ContractorType.Code = c_conttype 
							AND ContractorType.Branch = Load_ContractorType.BRANCH 
							AND ContractorType.IsActive = 1
							AND ContractorType.IsDeleted = 0
	WHERE DELTA = 'DEL';


	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			/**** Empty load table ****/
			TRUNCATE TABLE [Load_ContractorType];
		END;    
END;
GO
