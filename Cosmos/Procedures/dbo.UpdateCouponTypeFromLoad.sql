SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC  [dbo].[UpdateCouponTypeFromLoad]
AS

      --'=====================================================================
    --' CP -Stored Procedure -[UpdateCouponTypeFromLoad]
    --' ---------------------------
    --' Purpose: Loads Coupon Type Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM Load_CouponType) > 0 
	BEGIN
	BEGIN TRAN

	/**** Load changed records into staging table ****/
	SELECT CouponType.Id, Load_CouponType.*
	INTO #Staging
	FROM Load_CouponType
	LEFT JOIN CouponType ON CouponType.TypeId = c_typeid 
							AND CouponType.Branch = Load_CouponType.BRANCH 
							AND CouponType.IsActive = 1
	WHERE (DELTA = 'ADD' OR (DELTA = 'MOD' AND CouponType.EffectiveDate != [TIMESTAMP]))

	/**** Update IsActive flag on changed records ****/
	UPDATE CouponType
	SET IsActive = 0
	FROM CouponType
	JOIN #Staging ON #Staging.Id = CouponType.Id



	/**** Insert new and changed records ****/
	INSERT INTO CouponType
			   ([Branch]
			   ,[TypeId]
			   ,[DisplayOrder]
			   ,[IsPrimary]
			   ,[Abbreviation]
			   ,[Name]
			   ,[Description]
			   ,[CreatedDate]
			   ,[EffectiveDate]
			   ,[IsActive])
	SELECT [BRANCH]
		  ,[c_typeid]
		  ,[c_disporder]
		  ,CASE [c_primary] WHEN 'Y' THEN 1 ELSE 0 END AS [c_primary]
		  ,[c_abbr]
		  ,[c_type]
		  ,[c_descr]
		  ,GETDATE() AS CreatedDate
		  ,[TIMESTAMP] AS EffectiveDate
		  ,1 AS IsActive
	  FROM #Staging;

	/**** Drop staging table, no longer needed ****/
	DROP TABLE #Staging;

	/**** Mark deleted records accordingly ****/
	UPDATE CouponType
	SET IsDeleted = 1
	FROM CouponType
	JOIN Load_CouponType ON CouponType.TypeId = c_typeid 
							AND CouponType.Branch = Load_CouponType.BRANCH 
							AND CouponType.IsActive = 1
							AND CouponType.IsDeleted = 0
	WHERE DELTA = 'DEL';

	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			/**** Empty load table ****/
			TRUNCATE TABLE Load_CouponType;
		END;    
END;
GO
