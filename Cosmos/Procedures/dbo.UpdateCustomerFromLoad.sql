SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[UpdateCustomerFromLoad]
AS


      --'=====================================================================
    --' CP -Stored Procedure -[UpdateCustomerFromLoad]
    --' ---------------------------
    --' Purpose: Loads Customer Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_Customer]) > 0 
BEGIN

	BEGIN TRY
	
		BEGIN TRAN;

		-- first we delete any inactives with duplicate Effective Dates
		-- (shouldn't ordinarily happen, but may if something goes wrong)
		--
		WITH dups AS
		(
			SELECT DISTINCT 
			Branch, CustomerId, EffectiveDate, COUNT(*) AS [CustCount]
			FROM Customer 
			GROUP BY Branch, CustomerId, EffectiveDate 
			HAVING COUNT(*) > 1
		)
		DELETE Customer
		FROM Customer 
		JOIN dups 
		ON Customer.CustomerId = dups.CustomerId 
		AND Customer.Branch = dups.Branch 
		AND Customer.EffectiveDate = dups.EffectiveDate 
		AND Customer.IsActive = 0;


		-- elimate duplicate "active" customer IDs
		WITH x AS
		(
		select
		CustomerId, COUNT(*) AS [RowCount], MAX(EffectiveDate) AS [MaxEffective]
		from Customer 
		where IsActive = 1
		group by CustomerId 
		having COUNT(*) > 1
		)
		UPDATE Customer 
		SET IsActive = 0
		--SELECT *
		FROM Customer JOIN x
		ON x.CustomerId = Customer.CustomerId 
		AND Customer.IsActive = 1
		AND Customer.EffectiveDate != x.MaxEffective;


		/**** Load changed records into staging table ****/
		SELECT Customer.Id
			,[DELTA]
			  ,[TIMESTAMP]
			  ,UPPER(LEFT(Load_Customer.Branch, 1)) + (RIGHT(LOWER(Load_Customer.[Branch]), LEN(Load_Customer.[Branch]) -1)) AS Branch
			  ,CASE WHEN ISNUMERIC([c_custid]) = 1
				THEN [c_custid]
				ELSE 0
			  END AS [c_custid]
			  ,[c_custcode]
			  ,[c_custname]
			,NULLIF([c_daddr 1], '')[c_daddr 1]
			,NULLIF([c_daddr 2], '')[c_daddr 2]
			,NULLIF([c_daddr 3], '')[c_daddr 3]
			,NULLIF([c_dinstr 1], '')[c_dinstr 1]
			,NULLIF([c_dinstr 2], '')[c_dinstr 2]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_dsub]) = 1
					THEN [c_dsub]
					ELSE 0
				END, '') AS [c_dsub]
			,NULLIF([c_iaddr 1], '')[c_iaddr 1]
			,NULLIF([c_iaddr 2], '')[c_iaddr 2]
			,NULLIF([c_iaddr 3], '')[c_iaddr 3]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_isub]) = 1
					THEN [c_isub]
					ELSE 0
				END, '') AS [c_isub]
			,NULLIF([c_contact 1], '')[c_contact 1]
			,NULLIF([c_contact 2], '')[c_contact 2]
			,NULLIF([c_contact 3], '')[c_contact 3]
			,NULLIF([c_emailaddr 1], '')[c_emailaddr 1]
			,NULLIF([c_emailaddr 2], '')[c_emailaddr 2]
			,NULLIF([c_emailaddr 3], '')[c_emailaddr 3]
			,NULLIF([c_phone 1], '')[c_phone 1]
			,NULLIF([c_phone 2], '')[c_phone 2]
			,NULLIF([c_phone 3], '')[c_phone 3]
			,NULLIF([c_fax], '')[c_fax]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_opentime]) = 1
					THEN [c_opentime]
					ELSE Null
				END, '') AS TIME) AS [c_opentime]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_closetime]) = 1
					THEN [c_closetime]
					ELSE Null
				END, '') AS TIME) AS [c_closetime]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_drivno]) = 1
					THEN [c_drivno]
					ELSE 0
				END, '')[c_drivno]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_depotno]) = 1
					THEN [c_depotno]
					ELSE 0
				END, '')[c_depotno]
			,NULLIF([c_bookinstr 1], '')[c_bookinstr 1]
			,NULLIF([c_bookinstr 2], '')[c_bookinstr 2]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_startdate]) = 1
					THEN [c_startdate]
					ELSE Null
				END, '') AS DATE) AS [c_startdate]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_lastused 1]) = 1
					THEN [c_lastused 1]
					ELSE Null
				END, '') AS DATE) AS [c_lastused 1]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_lastused 2]) = 1
					THEN [c_lastused 2]
					ELSE Null
				END, '') AS DATE) AS [c_lastused 2]
			,NULLIF([c_acctype], '')[c_acctype]
			,NULLIF([c_indcode], '')[c_indcode]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 1]) = 1
					THEN [c_coupons 1]
					ELSE 0
				END, '')[c_coupons 1]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 2]) = 1
					THEN [c_coupons 2]
					ELSE 0
				END, '')[c_coupons 2]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 3]) = 1
					THEN [c_coupons 3]
					ELSE 0
				END, '')[c_coupons 3]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 4]) = 1
					THEN [c_coupons 4]
					ELSE 0
				END, '')[c_coupons 4]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 5]) = 1
					THEN [c_coupons 5]
					ELSE 0
				END, '')[c_coupons 5]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 6]) = 1
					THEN [c_coupons 6]
					ELSE 0
				END, '')[c_coupons 6]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 7]) = 1
					THEN [c_coupons 7]
					ELSE 0
				END, '')[c_coupons 7]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_coupons 8]) = 1
					THEN [c_coupons 8]
					ELSE 0
				END, '')[c_coupons 8]
			,CASE ISNULL([c_regrun], '') WHEN 'Y' THEN 1 ELSE 0 END AS [c_regrun]
			,NULLIF([c_pickupdays], '')[c_pickupdays]
			,NULLIF([c_remarks], '')[c_remarks]
			,NULLIF([c_salesrep 1], '')[c_salesrep 1]
			,NULLIF([c_salesrep 2], '')[c_salesrep 2]
			,NULLIF([c_rating], '')[c_rating]
			,CASE ISNULL([c_status], '') WHEN 'Y' THEN 1 ELSE 0 END AS [c_status]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_modified]) = 1
					THEN [c_modified]
					ELSE Null
				END, '') AS DATE) AS [c_modified]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_addedby]) = 1
					THEN [c_addedby]
					ELSE 0
				END, '')[c_addedby]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_addedwhen]) = 1
					THEN [c_addedwhen]
					ELSE Null
				END, '') AS DATE) AS [c_addedwhen]
			,CASE ISNULL([c_eft], '') WHEN 'Y' THEN 1 ELSE 0 END AS [c_eft]
			,NULLIF(
				CASE WHEN ISNUMERIC([c_salesrepcode]) = 1
					THEN [c_salesrepcode]
					ELSE 0
				END, '')[c_salesrepcode]
			,CAST(NULLIF(
				CASE WHEN ISDATE([c_salesrepstart]) = 1
					THEN [c_salesrepstart]
					ELSE Null
				END, '') AS DATE) AS [c_salesrepstart]
			,NULLIF([c_accode], '')[c_accode]
		INTO #Staging	
		FROM [Load_Customer]
		LEFT JOIN [Customer] ON [CustomerId] = c_custid
		AND [Load_Customer].Branch = [Customer].Branch 
		--AND CONVERT(DATETIME, [TIMESTAMP], 120) = CONVERT(DATETIME, EffectiveDate, 120)
		AND Customer.IsActive = 1
		WHERE (Delta = 'ADD' OR (Delta = 'MOD')); --AND Customer.EffectiveDate != [TIMESTAMP]))


		/**** Update IsActive flag on changed records ****/
		UPDATE Customer
		SET IsActive = 0
		FROM Customer
		JOIN #Staging ON #Staging.Id = Customer.Id
		WHERE #Staging.Id Is Not Null;


		/**** Insert new and changed records ****/
		INSERT INTO [Cosmos].[dbo].[Customer]
				   ([Branch]
				   ,[CustomerId]
				   ,[Code]
				   ,[Name]
				   ,[DeliveryAddress1]
				   ,[DeliveryAddress2]
				   ,[DeliveryAddress3]
				   ,[DeliveryInstrustions1]
				   ,[DeliveryInstrustions2]
				   ,[DeliverySuburb]
				   ,[InvoiceAddress1]
				   ,[InvoiceAddress2]
				   ,[InvoiceAddress3]
				   ,[InvoiceSuburb]
				   ,[Contact1]
				   ,[Contact2]
				   ,[Contact3]
				   ,[EmailAddress1]
				   ,[EmailAddress2]
				   ,[EmailAddress3]
				   ,[Phone1]
				   ,[Phone2]
				   ,[Phone3]
				   ,[Fax]
				   ,[OpenTime]
				   ,[CloseTime]
				   ,[DriverNumber]
				   ,[DepotNumber]
				   ,[BookInstrustions1]
				   ,[BookInstrustions2]
				   ,[Startdate]
				   ,[Lastused1]
				   ,[Lastused2]
				   ,[AccType]
				   ,[IndCode]
				   ,[Coupons1]
				   ,[Coupons2]
				   ,[Coupons3]
				   ,[Coupons4]
				   ,[Coupons5]
				   ,[Coupons6]
				   ,[Coupons7]
				   ,[Coupons8]
				   ,[RegularRun]
				   ,[PickupDays]
				   ,[Remarks]
				   ,[SalesRep1]
				   ,[SalesRep2]
				   ,[Rating]
				   ,[Status]
				   ,[Modified]
				   ,[AddedBy]
				   ,[AddedWhen]
				   ,[EFT]
				   ,[SalesRepCode]
				   ,[SalesRepStart]
				   ,[ProntoAccountCode]
				   ,[IsActive]
				   ,[EffectiveDate]
				   ,[IsDeleted]
				   ,[CreatedDate]
				   ,[LastModifiedDate])
		SELECT [BRANCH]
			  ,[c_custid]
			  ,[c_custcode]
			  ,[c_custname]
			  ,[c_daddr 1]
			  ,[c_daddr 2]
			  ,[c_daddr 3]
			  ,[c_dinstr 1]
			  ,[c_dinstr 2]
			  ,[c_dsub]
			  ,[c_iaddr 1]
			  ,[c_iaddr 2]
			  ,[c_iaddr 3]
			  ,[c_isub]
			  ,[c_contact 1]
			  ,[c_contact 2]
			  ,[c_contact 3]
			  ,[c_emailaddr 1]
			  ,[c_emailaddr 2]
			  ,[c_emailaddr 3]
			  ,[c_phone 1]
			  ,[c_phone 2]
			  ,[c_phone 3]
			  ,[c_fax]
			  ,[c_opentime]
			  ,[c_closetime]
			  ,[c_drivno]
			  ,[c_depotno]
			  ,[c_bookinstr 1]
			  ,[c_bookinstr 2]
			  ,[c_startdate]
			  ,[c_lastused 1]
			  ,[c_lastused 2]
			  ,[c_acctype]
			  ,[c_indcode]
			  ,[c_coupons 1]
			  ,[c_coupons 2]
			  ,[c_coupons 3]
			  ,[c_coupons 4]
			  ,[c_coupons 5]
			  ,[c_coupons 6]
			  ,[c_coupons 7]
			  ,[c_coupons 8]
			  ,[c_regrun]
			  ,[c_pickupdays]
			  ,[c_remarks]
			  ,[c_salesrep 1]
			  ,[c_salesrep 2]
			  ,[c_rating]
			  ,[c_status]
			  ,[c_modified]
			  ,[c_addedby]
			  ,[c_addedwhen]
			  ,[c_eft]
			  ,[c_salesrepcode]
			  ,[c_salesrepstart]
			  --SS --removed CR
			
			  , replace(replace([c_accode],char(10),''),char(13),'')
			  ,1 AS [IsActive]
			  ,[TIMESTAMP] AS [EffectiveDate]
			  ,0 AS [IsDeleted]
			  ,GETDATE() AS [CreatedDate]
			  ,GETDATE() AS [LastModifiedDate]
		  FROM #Staging;

		/**** Drop staging table, no longer needed ****/
		DROP TABLE #Staging;

		/**** Mark deleted records accordingly ****/
		UPDATE Customer
		SET IsDeleted = 1
		FROM Customer 
		JOIN Load_Customer ON Customer.CustomerId = c_custid 
								AND Customer.Branch = Load_Customer.BRANCH 
								AND Customer.IsActive = 1
								AND Customer.IsDeleted = 0
		WHERE DELTA = 'DEL';
		
	END TRY
	BEGIN CATCH

			ROLLBACK WORK;

			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return error
			-- information about the original error that caused
			-- execution to jump to the CATCH block.
			RAISERROR (
						@ErrorMessage, -- Message text.
						@ErrorSeverity, -- Severity.
						@ErrorState -- State.
					   );
			
			RETURN 1;
	
	END CATCH

	/**** Empty load table ****/
	TRUNCATE TABLE Load_Customer;
	
	COMMIT WORK;

END;

GO
