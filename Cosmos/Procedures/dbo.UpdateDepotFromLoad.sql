SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateDepotFromLoad]
AS


      --'=====================================================================
    --' CP -Stored Procedure -[UpdateDepotFromLoad]
    --' ---------------------------
    --' Purpose: Loads Depot Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================
SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_Depot]) > 0 
BEGIN
	BEGIN TRAN
	INSERT INTO [Cosmos].[dbo].[Depot]
				([Branch]
			   ,[DepotNumber]
			   ,[DepotCode]
			   ,[DepotName]
			   ,[DepotAddress1]
			   ,[DepotAddress2]
			   ,[DepotSubcode]
			   ,[DepotTimes1]
			   ,[DepotTimes2]
			   ,[DepotTimes3]
			   ,[DepotTimes4]
			   ,[DepotTimes5]
			   ,[DepotTimes6]
			   ,[DepotTimes7]
			   ,[DepotTimes8]
			   ,[DepotContact]
			   ,[IsDeleted]
			   ,[LastModifiedDate]
			   ,[CreatedDate]
			   ,[EffectiveDate],[NetworkCategory])
	SELECT 
			UPPER(LEFT(Branch, 1)) + (RIGHT(LOWER([Branch]), LEN(Branch) -1)) AS Branch
			,[d_depotno]
			,[d_depotcode]
			,[d_depotname]
			,[d_depotaddr 1]
			,[d_depotaddr 2]
			,[d_depotsubcode]
			,CAST([d_depottimes 1] AS TIME)[d_depottimes 1]
			,CAST([d_depottimes 2] AS TIME)[d_depottimes 2]
			,CAST([d_depottimes 3] AS TIME)[d_depottimes 3]
			,CAST([d_depottimes 4] AS TIME)[d_depottimes 4]
			,CAST([d_depottimes 5] AS TIME)[d_depottimes 5]
			,CAST([d_depottimes 6] AS TIME)[d_depottimes 6]
			,CAST([d_depottimes 7] AS TIME)[d_depottimes 7]
			,CAST([d_depottimes 8] AS TIME)[d_depottimes 8]
			,[d_depotcontact]
			,0
			,GETDATE()
			,GETDATE()
			,[TIMESTAMP],''
	FROM [Cosmos].[dbo].[Load_Depot]
	  WHERE (DELTA = 'ADD' OR DELTA = 'MOD') AND 
		NOT EXISTS (SELECT * FROM [Depot]
		WHERE Depot.DepotNumber = [Load_Depot].d_depotno
		AND Depot.Branch = [Load_Depot].BRANCH
		AND Depot.EffectiveDate = [Load_Depot].[TIMESTAMP])

		UPDATE [Depot] 
		SET [IsDeleted] = 1, [LastModifiedDate] = GETDATE()
		FROM [Depot] 
		JOIN [Load_Depot] ON Depot.DepotNumber = [Load_Depot].d_depotno
		AND Depot.Branch = [Load_Depot].BRANCH
		AND Depot.EffectiveDate = [Load_Depot].[TIMESTAMP]
		WHERE DELTA = 'DEL';

	-- catch any stray active duplicates
	WITH A ([Branch],DepotNumber, DepotCount, MaxEffectiveDate)
	AS (
	SELECT [Branch]
		  ,DepotNumber
		  ,COUNT(*) AS DepotCount
		  ,MAX(EffectiveDate) AS MaxEffectiveDate
	  FROM [Cosmos].[dbo].[Depot]
	  WHERE [IsActive] = 1
	  AND ISNULL(DepotNumber, '') != ''
	  GROUP BY [Branch],DepotNumber,[IsActive]
	  HAVING COUNT(*) > 1
	)
	UPDATE Cosmos.dbo.Depot SET IsActive = 0
	--select *
	FROM Cosmos.dbo.Depot d, A 
	WHERE d.Branch = A.Branch 
	AND d.DepotNumber = A.DepotNumber
	AND EffectiveDate != MaxEffectiveDate
	AND d.IsActive = 1
	AND ISNULL(d.DepotNumber, '') != '';

	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			TRUNCATE TABLE Load_Depot;
			EXEC dbo.UpdateScannerGWDepot;
		END;    
END;



GO
