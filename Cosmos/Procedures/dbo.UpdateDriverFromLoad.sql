SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateDriverFromLoad]
AS


      --'=====================================================================
    --' CP -Stored Procedure -[UpdateDriverFromLoad]
    --' ---------------------------
    --' Purpose: Loads Driver Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM Load_Driver) > 0 
BEGIN
	BEGIN TRAN

	/**** Select driver.id with changed and new data ****/
		SELECT Driver.Id, Load_Driver.*
		INTO #Staging	
		FROM [Load_Driver]
		LEFT JOIN [Driver] ON [DriverNumber] = [d_drivno] 
		AND [Load_Driver].Branch = [Driver].Branch 
		--AND CONVERT(DATETIME, [TIMESTAMP], 120) = CONVERT(DATETIME, EffectiveDate, 120)
		AND Driver.IsActive = 1
		WHERE (Delta = 'ADD' OR Delta = 'MOD') AND 
			(([Driver].EffectiveDate != [Load_Driver].[TIMESTAMP])
				OR
			([Driver].[Id] Is Null))

	/**** Mark changed records as inactive ****/
		UPDATE Driver 
		SET IsActive = 0 
		FROM #Staging
		JOIN Driver ON #Staging.Id = Driver.Id

	/**** Insert new records for new and changed data ****/
		INSERT INTO [Cosmos].[dbo].[Driver] 
				([Branch]
				,[DriverNumber]
				,[ContractorType]
				,[DriverName]
				,[Address1]
				,[Address2]
				,[Address3]
				,[PostCode]
				,[MobileId]
				,[NextOfKinName1]
				,[NextOfKinName2]
				,[NextOfKin1HomePhone]
				,[NextOfKin2HomePhone]
				,[NextOfKin1WorkPhone]
				,[NextOfKin2WorkPhone]
				,[NextOfKin1MobilePhone]
				,[NextOfKin2MobilePhone]
				,[NextOfKin1Relationship]
				,[NextOfKin2Relationship]
				,[Comments1]
				,[Comments2]
				,[NrmaMembershipNumber]
				,[CompanyName]
				,[CompanyAddress1]
				,[CompanyAddress2]
				,[CompanyAddress3]
				,[Radio]
				,[VehicleMake]
				,[VehicleModel]
				,[VehicleYear]
				,[VehicleRegistration]
				,[VehicleRegistrationExpriry]
				,[PhoneNumber]
				,[MobileNumber]
				,[FaxNumber]
				,[PagerNumber]
				,[MobileDeviceType]
				,[StartDate]
				,[EndDate]
				,[DepotNo]
				,[Statzone]
				,[CreditLimit]
				,[ABN]
				,[BankName]
				,[BankBSB]
				,[BankAccountNumber]
				,[BankAccountName]
				,[TaxFileNumber]
				,[CMExclude]
				,[AgentName]
				,[CombineWith]
				,[RateTable]
				,[OuterDriver]
				,[ManifestDriver]
				,[ProntoId]
				,[IsDeleted]
				,[EffectiveDate]
				,[CreatedDate]
				,[IsActive]
				,[ArchiveDate]
				,[ArchiveReason1]
				,[ArchiveReason2] 
				,[ArchiveReason3] 
				,[ArchiveReason4]
				,etazone
				,pricingzone)
		SELECT DISTINCT
			UPPER(LEFT(Branch, 1)) + (RIGHT(LOWER([Branch]), LEN([Branch]) -1)) AS Branch
			,[d_drivno]
			,[d_contractor_type]
			,[d_drivname]
			,[d_drivaddr1]
			,[d_drivaddr2]
			,[d_drivaddr3]
			,CASE ISNUMERIC(d_postcode) WHEN 1 THEN CAST(REPLACE([d_postcode],'.','') AS INT) END AS d_postcode
			,[d_mobid]
			,[d_nkin1]
			,[d_nkin2]
			,[d_nkinphh1]
			,[d_nkinphh2]
			,[d_nkinphw1]
			,[d_nkinphw2]
			,[d_nkinphm1]
			,[d_nkinphm2]
			,[d_nkinrel1]
			,[d_nkinrel2]
			,[d_comments1]
			,[d_comments2]
			,[d_nrma]
			,[d_compname]
			,[d_compaddr1]
			,[d_compaddr2]
			,[d_compaddr3]
			,[d_radio]
			,[d_make]
			,[d_model]
			,[d_year]
			,[d_rego]
			,[d_regoexp]
			,[d_phone]
			,[d_mobile]
			,[d_fax]
			,[d_pager]
			,[d_mobtype]
			,[d_startdate]
			,[d_enddate]
			,[d_depotno]
			,[d_statzone]
			,[d_credit_limit]
			,[d_abn]
			,[d_bank]
			,[d_bsb]
			,[d_account]
			,[d_account_name]
			,[d_taxfileno]
			,[d_cmexclude]
			,[d_agentname]
			,[d_combine_with]
			,[d_ratetable]
			,[d_outerdriver]
			,[d_manifestdriver]
			,[d_pronto_id]
			,0 AS IsDeleted
			,CONVERT(DATETIME, [TIMESTAMP], 120) AS EffectiveDate
			,GETDATE() AS CreatedDate
			,1 AS IsActive
			--,CONVERT(DATETIME, [d_archive_date], 104) AS ArchiveDate
			,CASE ISDATE([d_archive_date])
				WHEN 1 THEN CONVERT(DATETIME, [d_archive_date])
				ELSE Null
			END AS ArchiveDate
			,ISNULL(d_archive_reason1, '')
			,ISNULL(d_archive_reason2, '')
			,ISNULL(d_archive_reason3, '')
			,REPLACE(REPLACE(ISNULL(d_archive_reason4, ''), CHAR(10), ''), CHAR(13), '')
			,etazone
			,pricingzone
		FROM #Staging

		/**** Update deleted records ****/
		UPDATE [Cosmos].[dbo].[Driver]
		SET [IsDeleted] = 1, LastModifiedDate = GETDATE()
		FROM [Driver] 
		JOIN [Load_Driver] ON [DriverNumber] = [d_drivno] 
		AND [Load_Driver].Branch = [Driver].Branch 
		AND CONVERT(DATETIME, [TIMESTAMP], 120) = CONVERT(DATETIME, EffectiveDate, 120)
		WHERE Delta = 'DEL' AND IsActive = 1;
		
		/**** Catch any duplicates and mark the old records as inactive ****/
		WITH A ([Branch],[DriverNumber], DriverCount, MaxEffectiveDate)
		AS (
		SELECT [Branch]
			  ,[DriverNumber]
			  ,COUNT(*) AS DriverCount
			  ,MAX(EffectiveDate) AS MaxEffectiveDate
		  FROM [Cosmos].[dbo].[Driver]
		  WHERE [IsActive] = 1
		  GROUP BY [Branch],[DriverNumber],[IsActive]
		  HAVING COUNT(*) > 1
		)
		UPDATE Driver SET IsActive = 0
		FROM Driver, A 
		WHERE Driver.Branch = A.Branch 
		AND Driver.DriverNumber = A.DriverNumber
		AND EffectiveDate != MaxEffectiveDate
		AND Driver.IsActive = 1;
		
		
	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			/**** Empty load table ****/
		    --TRUNCATE TABLE [Load_Driver];
			
			-- update Franchisee details based on any new information
			EXEC dbo.UpdateFranchiseeDetailsFromCosmosDrivers;
			
			-- update driver details in ScannerGateway
			EXEC UpdateScannerGWDriver;
		END;    

END;
GO
