SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UpdateFranchiseeDetailsFromCosmosDrivers]
AS
BEGIN

      --'=====================================================================
    --' CP -Stored Procedure -[UpdateFranchiseeDetailsFromCosmosDrivers]
    --' ---------------------------
    --' Purpose: Updates Franchisee Details From Cosmos Drivers-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

	MERGE Franchisees AS Target
	USING
	(
		SELECT 
		DISTINCT(LTRIM(RTRIM(ISNULL(cd.ProntoId, '')))) AS [ProntoId]
		, (
			SELECT TOP 1 ISNULL(CASE 
									WHEN LTRIM(RTRIM(ISNULL(cd2.CompanyName, ''))) = ''
									THEN cd2.DriverName
									ELSE cd2.CompanyName
								END, 'UNKNOWN')
			FROM Driver cd2
			WHERE cd2.ProntoId = cd.ProntoId
			AND
				ISNULL(cd2.DriverName, '') NOT LIKE '%MANAGEMENT%run%'
				AND ISNULL(cd2.DriverName, '') NOT LIKE '%mgmt%run%'
				AND ISNULL(cd2.DriverName, '') NOT LIKE '%mgt%run%'
				AND ISNULL(cd2.DriverName, '') NOT LIKE '%managme%run%'
			    --AND ISNULL(cd2.DriverName, '') NOT LIKE '%second scanner%'
			    --AND ISNULL(cd2.DriverName, '') NOT LIKE '%vacant run%'
			    --AND ISNULL(cd2.DriverName, '') NOT LIKE '%test unit%'
			AND cd2.IsDeleted = 0
			AND cd2.IsActive = 1
			ORDER BY cd2.EffectiveDate DESC
		) AS [FranchiseeName]
		FROM
		Driver cd
		WHERE LTRIM(RTRIM(ISNULL(cd.ContractorType, ''))) = 'C'
		AND LTRIM(RTRIM(ISNULL(cd.ProntoId, ''))) LIKE '[ABCGMSPO][0123456789][0123456789]%'
		AND IsActive = 1
		AND IsDeleted = 0
		AND (
			(cd.Branch = 'brisbane' AND cd.ProntoId LIKE 'B%')
			OR
			(cd.Branch = 'goldcoast' AND cd.ProntoId LIKE 'G%')
			OR
			(cd.Branch NOT IN ('brisbane', 'goldcoast'))
		)
		AND Branch NOT IN ('nkope', 'perth')
		AND ProntoId NOT LIKE '[ABCGMOSP]099'
	)
	AS Source
	ON LTRIM(RTRIM(ISNULL(Target.ProntoId, ''))) = Source.ProntoId
	WHEN
		NOT MATCHED BY TARGET
			AND LTRIM(RTRIM(ISNULL(Source.FranchiseeName, ''))) != '' THEN
			INSERT
			(
				FranchiseeId,
				ProntoId,
				FranchiseeName,
				DateCreated,
				DateUpdated
			)
			VALUES
			(
				NEWID(),
				Source.ProntoId,
				Source.FranchiseeName,
				GETDATE(),
				GETDATE()
			)
	WHEN
		MATCHED AND
			LTRIM(RTRIM(ISNULL(Target.FranchiseeName, ''))) != LTRIM(RTRIM(ISNULL(Source.FranchiseeName, '')))
			AND LTRIM(RTRIM(ISNULL(Source.FranchiseeName, ''))) != ''
		THEN
			UPDATE SET
				Target.FranchiseeName = Source.FranchiseeName,
				Target.DateUpdated = GETDATE();

END
GO
