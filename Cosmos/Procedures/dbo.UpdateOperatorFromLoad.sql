SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateOperatorFromLoad]
AS


      --'=====================================================================
    --' CP -Stored Procedure -[UpdateOperatorFromLoad]
    --' ---------------------------
    --' Purpose: Loads Operator Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_Operator]) > 0 
BEGIN

	BEGIN TRAN


		INSERT INTO [Cosmos].[dbo].[Operator]
			(
			[Branch]
			,[Id]
			,[Name]
			,[IsDeleted]
			,[EffectiveDate]
			,[CreatedDate]
			)
		SELECT 
			UPPER(LEFT([Load_Operator].[Branch], 1)) + (RIGHT(LOWER([Load_Operator].[Branch]), LEN([Load_Operator].[Branch]) -1)) AS Branch
			,[o_id]
			,[o_name]
			,0
			,[TIMESTAMP]
			,GETDATE()
		FROM [Load_Operator] 
	  WHERE (DELTA = 'ADD' OR DELTA = 'MOD') AND 
		NOT EXISTS (SELECT * FROM [Operator]
		WHERE [Operator].Branch = [Load_Operator].BRANCH
			AND [Operator].Id = [Load_Operator].o_id
		AND [Operator].EffectiveDate = [Load_Operator].[TIMESTAMP])


		UPDATE [Operator] 
		SET [IsDeleted] = 1, [LastModifiedDate] = GETDATE()
		FROM [Operator]
		JOIN [Load_Operator] ON [Operator].Branch = [Load_Operator].BRANCH
			AND [Operator].Id = [Load_Operator].o_id
		WHERE Delta = 'DEL';
		
		TRUNCATE TABLE Load_Operator;
	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			TRUNCATE TABLE Load_Operator;
		END;    
END;



GO
