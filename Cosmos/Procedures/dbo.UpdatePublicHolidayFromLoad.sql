SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdatePublicHolidayFromLoad]
AS


      --'=====================================================================
    --' CP -Stored Procedure -[UpdatePublicHolidayFromLoad]
    --' ---------------------------
    --' Purpose: Loads Public Holiday Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM Load_PublicHoliday) > 0 
BEGIN

	BEGIN TRAN

		MERGE PublicHoliday AS Target
		USING
		(
			SELECT
			  DISTINCT
				BRANCH AS [Branch],
				h_date AS [HolidayDate],
				MAX(ISNULL(h_descr, 'UNKNOWN')) AS [HolidayDesc]
			FROM Load_PublicHoliday
			WHERE DELTA IN ('ADD','MOD')
			AND BRANCH Is Not Null
			AND h_date Is Not Null
			AND DELTA Is Not Null
			GROUP BY BRANCH, h_date
		) AS Source
		ON Target.BRANCH = Source.Branch
		AND Target.HolidayDate = CONVERT(date, CONVERT(datetime, Source.HolidayDate, 120))
		WHEN MATCHED THEN
			UPDATE
				SET
					Target.HolidayDesc = Source.HolidayDesc,
					Target.IsActive = 1,
					Target.IsDeleted = 0,
					Target.LastModifiedDate = GETDATE()
		WHEN NOT MATCHED THEN
			INSERT
			(
				Branch,
				HolidayDate,
				HolidayDesc,
				IsActive,
				IsDeleted,
				LastModifiedDate,
				CreatedDate
			)
			VALUES
			(
				Source.Branch,
				Source.HolidayDate,
				Source.HolidayDesc,
				1,
				0,
				GETDATE(),
				GETDATE()
			);

		/**** Update deleted records ****/
		UPDATE [Cosmos].[dbo].PublicHoliday 
		SET [IsDeleted] = 1, LastModifiedDate = GETDATE(), IsActive = 0
		FROM PublicHoliday ph
		JOIN Load_PublicHoliday lph
		ON ph.Branch = ISNULL(lph.BRANCH, '')
		AND ph.HolidayDate = CONVERT(date, CONVERT(datetime, ISNULL(lph.h_date, '1jan1900'), 120))
		WHERE lph.DELTA = 'DEL';

	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			/**** Empty load table ****/
			TRUNCATE TABLE Load_PublicHoliday;
		END;    

END;
GO
