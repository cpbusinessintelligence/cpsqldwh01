SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateSalesFromLoad]
AS


      --'=====================================================================
    --' CP -Stored Procedure -[UpdateSalesFromLoad]
    --' ---------------------------
    --' Purpose: Loads Sales Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_Sales]) > 0 
BEGIN
	BEGIN TRAN

	/**** Load changed records into staging table ****/
	SELECT DISTINCT Sales.Id
		,[DELTA]
		,[Load_Sales].[TIMESTAMP]
		,UPPER(LEFT(Load_Sales.Branch, 1)) + (RIGHT(LOWER(Load_Sales.[Branch]), LEN(Load_Sales.[Branch]) -1)) AS Branch
		,CAST([s_salesid] AS INT) AS [s_salesid]
		,CAST([s_operator] AS INT) AS [s_operator]
		,CAST([s_date] AS DATE) AS [s_date]
		,[s_telnr]
		,[s_fax]
		,[s_contact]
		,[s_company]
		,[s_addr 1]
		,[s_addr 2]
		,[s_heard]
		,[s_freight]
		,[s_when]
		,[s_use]
		,[s_weekly]
		,[s_comments 1]
		,[s_comments 2]
		,CAST([s_drivno] AS INT) AS [s_drivno]
		,CAST([s_salesrepid] AS INT) AS [s_salesrepid]
		,CASE [s_result] WHEN 'Y' THEN 1 ELSE 0 END AS [s_result]
		,[s_reason]
		,CAST([s_coupons 1] AS INT) AS [s_coupons 1]
		,CAST([s_coupons 2] AS INT) AS [s_coupons 2]
		,CAST([s_coupons 3] AS INT) AS [s_coupons 3]
		,CAST([s_coupons 4] AS INT) AS [s_coupons 4]
		,CAST([s_salesval] AS MONEY) AS [s_salesval]
		,[s_scomment 1]
		,[s_scomment 2]
		,CASE [s_completed] WHEN 'Y' THEN 1 ELSE 0 END AS [s_completed]
		,[s_timestamp]
	INTO #Staging	
	FROM [Load_Sales]
	LEFT JOIN [Sales] ON [SalesId] = s_salesid
	AND [Load_Sales].Branch = [Sales].Branch 
	--AND CONVERT(DATETIME, [Load_Sales].[TIMESTAMP], 120) = CONVERT(DATETIME, EffectiveDate, 120)
	AND Sales.IsActive = 1
	WHERE (Delta = 'ADD' OR Delta = 'MOD' )  AND [Sales].EffectiveDate != [Load_Sales].[TIMESTAMP]


	/**** Update IsActive flag on changed records ****/
	UPDATE Sales
	SET IsActive = 0
	SELECT * FROM Sales
	JOIN #Staging ON #Staging.Id = Sales.Id
	WHERE #Staging.[TIMESTAMP] = EffectiveDate



	/**** InserT new and changed records ****/
	INSERT INTO [Cosmos].[dbo].[Sales]
			   ([Branch]
			  ,[SalesId]
			  ,[Operator]
			  ,[Date]
			  ,[TelephoneNumber]
			  ,[Fax]
			  ,[Contact]
			  ,[Company]
			  ,[Address1]
			  ,[Address2]
			  ,[Heard]
			  ,[Freight]
			  ,[When]
			  ,[Use]
			  ,[Weekly]
			  ,[Comments1]
			  ,[Comments2]
			  ,[DriverNumber]
			  ,[SalesRepId]
			  ,[Result]
			  ,[Reason]
			  ,[Coupons1]
			  ,[Coupons2]
			  ,[Coupons3]
			  ,[Coupons4]
			  ,[SalesValue]
			  ,[SComment1]
			  ,[SComment2]
			  ,[Completed]
			  ,[TimeStamp]
			  ,[IsActive]
			  ,[EffectiveDate]
			  ,[IsDeleted]
			  ,[CreatedDate]
			  ,[LastModifiedDate]
	)
	SELECT [BRANCH]
		  ,[s_salesid]
		  ,[s_operator]
		  ,[s_date]
		  ,[s_telnr]
		  ,[s_fax]
		  ,[s_contact]
		  ,[s_company]
		  ,[s_addr 1]
		  ,[s_addr 2]
		  ,[s_heard]
		  ,[s_freight]
		  ,[s_when]
		  ,[s_use]
		  ,[s_weekly]
		  ,[s_comments 1]
		  ,[s_comments 2]
		  ,[s_drivno]
		  ,[s_salesrepid]
		  ,[s_result]
		  ,[s_reason]
		  ,[s_coupons 1]
		  ,[s_coupons 2]
		  ,[s_coupons 3]
		  ,[s_coupons 4]
		  ,[s_salesval]
		  ,[s_scomment 1]
		  ,[s_scomment 2]
		  ,[s_completed]
		  ,[s_timestamp]
		  ,1 AS [IsActive]
		  ,[TIMESTAMP] AS [EffectiveDate]
		  ,0 AS [IsDeleted]
		  ,GETDATE() AS [CreatedDate]
		  ,GETDATE() AS [LastModifiedDate]
	  FROM #Staging;

	/**** Drop staging table, no longer needed ****/
	DROP TABLE #Staging;


	/**** Mark deleted records accordingly ****/
	UPDATE Sales
	SET IsDeleted = 1
	FROM Sales 
	JOIN Load_Sales ON Sales.SalesId = s_salesid 
							AND Sales.Branch = Load_Sales.BRANCH 
							AND Sales.IsActive = 1
							AND Sales.IsDeleted = 0
	WHERE DELTA = 'DEL';


	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			/**** Empty load table ****/
			TRUNCATE TABLE [Load_Sales];
		END;    
END;
GO
