SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UpdateScannerGWDepot]
AS
BEGIN


      --'=====================================================================
    --' CP -Stored Procedure -[UpdateScannerGWDepot]
    --' ---------------------------
    --' Purpose: Updates Scanner Gateway Depot table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


	INSERT INTO ScannerGateway.dbo.Depot
	(
		Code,
		[Name],
		BranchId,
		StateId,
		CosmosDepotCode,
		IsProntoExport,
		ProntoExportStartDate,
		IsActive,
		IsDeleted,
		CreatedDate,
		LastModifiedDate
	)
	SELECT 
		DISTINCT
		d.DepotCode,
		d.DepotName,
		b2.Id,
		s2.Id,
		d.DepotNumber,
		0,
		Null,
		d.IsActive,
		d.IsDeleted,
		GETDATE(),
		GETDATE()
	FROM (Depot d
	LEFT OUTER JOIN 
	  (ScannerGateway.dbo.depot ScannerGatewayD
	   INNER JOIN
		(ScannerGateway.dbo.Branch b
		 INNER JOIN ScannerGateway.dbo.[State] s
		 ON b.StateId = s.Id)
	ON ScannerGatewayD.BranchId = b.Id)
	ON d.DepotNumber = ScannerGatewayD.CosmosDepotCode
	AND d.Branch = b.CosmosBranch)
	INNER JOIN ScannerGateway.dbo.Branch b2
	ON d.Branch = b2.CosmosBranch
	INNER JOIN ScannerGateway.dbo.[State] s2
	ON b2.StateId = s2.Id
	WHERE d.IsActive = 1
	AND d.IsDeleted = 0
	AND ScannerGatewayD.Id is null
	--order by d.Branch, d.DepotNumber 

END
GO
