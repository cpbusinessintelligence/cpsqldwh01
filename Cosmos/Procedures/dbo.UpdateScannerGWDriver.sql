SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[UpdateScannerGWDriver]
AS
BEGIN


      --'=====================================================================
    --' CP -Stored Procedure -[UpdateScannerGWDriver]
    --' ---------------------------
    --' Purpose: Updates Scanner Gateway Driver Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


/**** Update System Codes Table with New Contractor Types ****/
CREATE TABLE #types (Code varchar(10), Descr varchar(100));

WITH types AS 
(
	SELECT DISTINCT ContractorType.Code, ISNULL([Description], 'Other') AS [Description]
	FROM ContractorType
	WHERE Code Is Not Null
	UNION
	select distinct(drv.ContractorType), ISNULL(ct.Description, 'Other') AS [Description]
	from Driver drv
	left outer join ContractorType ct
	ON drv.ContractorType = ct.Code 
	where drv.ContractorType is not null
)
INSERT INTO #types (Code, Descr)
SELECT 
Code, MIN([Description])
FROM types 
GROUP BY Code;


DECLARE @Code VARCHAR(2), @Desc VARCHAR(100);
DECLARE cont_cursor CURSOR FOR 
SELECT Code, Descr FROM #types;
--SELECT DISTINCT ContractorType.Code, ISNULL([Description], 'Other')
--FROM ContractorType
--WHERE Code Is Not Null;
--AND [Description] Is Null;

OPEN cont_cursor;

FETCH NEXT FROM cont_cursor 
INTO @Code, @Desc;

WHILE @@FETCH_STATUS = 0
BEGIN
    EXEC ScannerGateway.dbo.cppl_InsertUpdateSystemCode @Discriminator = 'DriverTypeCode', @FriendlyCode = @Code, @Description = @Desc
    FETCH NEXT FROM cont_cursor 
    INTO @Code, @Desc;
END
CLOSE cont_cursor;
DEALLOCATE cont_cursor;

/**** Capture data of records that have changed ****/
SELECT 
  ScannerGatewayDriver.Id AS DriverId
, Driver.Branch 
, Driver.DriverNumber
, ISNULL(Driver.DriverName, '') AS [DriverName]
, Driver.ProntoId
, Driver.IsActive
--, ScannerGatewayDriver.IsContractor
-- 16/12/2010 - Craig Parris
-- set the IsContractor flag based on the type from Cosmos
, CASE ISNULL(Driver.ContractorType, 'O') WHEN 'C' THEN 1 ELSE 0 END AS [IsContractor]
, Driver.StartDate
, Driver.EndDate
, ScannerGatewayBranch.Id AS BranchId
, ScannerGatewayBranch_Cosmos.Id AS CosmosBranchId
, Driver.EffectiveDate AS EffectiveDate
-- 16/12/2010 - Craig Parris
-- default contractor type to 'Other' if it's null
, ISNULL(Driver.ContractorType, 'O') AS [ContractorType]
, SystemCodes.Id AS DriverTypeId
-- 16/12/2010 - Craig Parris
-- need to include Depot in the update from Cosmos now
, Driver.DepotNo 
, ScannerGatewayDepot.Id AS [DepotId]
INTO #T
FROM [Cosmos].dbo.Driver
LEFT JOIN ScannerGateway.dbo.Driver AS ScannerGatewayDriver ON Driver.DriverNumber = ScannerGatewayDriver.Code 
	AND (SELECT TOP 1 B.Id FROM ScannerGateway.dbo.Branch B
			WHERE B.CosmosBranch = Driver.Branch
				AND B.IsDeleted = 0) = ScannerGatewayDriver.BranchId
	-- 16/12/2010 - Craig Parris
	-- we also need to include driver records in the updates if the Depot or Name has changed
	-- (this is actually done in the WHERE criteria)
/*	AND (SELECT TOP 1 D.Id FROM ScannerGateway.ScannerGateway.dbo.Depot D WHERE D.CosmosDepotCode = Driver.DepotNo
			AND D.BranchId = (SELECT TOP 1 B4.Id FROM ScannerGateway.ScannerGateway.dbo.Branch B4
								WHERE ISNULL(B4.CosmosBranch, '') = Driver.Branch)) = ScannerGatewayDriver.DepotId
	AND ISNULL(Driver.DriverName, '') = ScannerGatewayDriver.Name*/
	AND (ScannerGatewayDriver.IsActive = 1) AND (ScannerGatewayDriver.IsDeleted = 0)
LEFT JOIN ScannerGateway.dbo.Branch AS ScannerGatewayBranch ON BranchId = ScannerGatewayBranch.Id
	AND ScannerGatewayBranch.IsDeleted = 0
	AND ScannerGatewayBranch.IsActive = 1
LEFT JOIN ScannerGateway.dbo.Branch AS ScannerGatewayBranch_Cosmos
				ON ScannerGatewayBranch_Cosmos.CosmosBranch = Driver.Branch
					AND ScannerGatewayBranch_Cosmos.IsDeleted = 0
					AND ScannerGatewayBranch_Cosmos.IsActive = 1
LEFT JOIN ScannerGateway.dbo.SystemCodes AS SystemCodes
				ON SystemCodes.Discriminator = 'DriverTypeCode'
					AND SystemCodes.FriendlyCode = Driver.ContractorType
						AND SystemCodes.IsDeleted = 0
-- 16/12/2010 - Craig Parris
-- join to the depot table to get the Depot Id
-- (sometimes drivers in the Adelaide branch are linked to a depot that's in the Nkope branch)
--
-- 12/04/2011 - Craig Parris
--
-- this shouldn't happen anymore
--
LEFT JOIN ScannerGateway.dbo.Depot AS [ScannerGatewayDepot] ON ISNULL(ScannerGatewayDepot.CosmosDepotCode, -99) = ISNULL(Driver.DepotNo, 0)
	AND ((ScannerGatewayDepot.BranchId = (SELECT TOP 1 B2.Id FROM ScannerGateway.dbo.Branch B2 WHERE 
								(ISNULL(B2.CosmosBranch, '') = Driver.Branch)
									AND B2.IsDeleted = 0
									AND B2.IsActive = 1))
		--OR
		--	(Driver.Branch = 'adelaide'
		--	AND
		--	ScannerGatewayDepot.BranchId = (SELECT B5.Id FROM ScannerGateway.ScannerGateway.dbo.Branch B5 WHERE
		--							ISNULL(B5.CosmosBranch, '') = 'nkope'
		--								AND B5.IsDeleted = 0
		--								AND B5.IsActive = 1))
		)
	AND ScannerGatewayDepot.IsActive = 1
	AND ScannerGatewayDepot.IsDeleted = 0
WHERE Driver.IsActive = 1 AND Driver.IsDeleted = 0
AND (
		(
			Driver.ProntoId != ScannerGatewayDriver.ProntoDriverCode 
		)
	OR
		(
			Driver.ProntoId Is Null AND ScannerGatewayDriver.ProntoDriverCode Is Not Null
		)
	OR
		(
			Driver.ProntoId Is Not Null AND ScannerGatewayDriver.ProntoDriverCode Is Null
		)
	OR  (
			((CASE ISNULL(Driver.ContractorType, 'O') WHEN 'C' THEN 1 ELSE 0 END != ScannerGatewayDriver.IsContractor)
			 OR ScannerGatewayDriver.IsContractor IS NULL)
		)
	OR  (
			ISNULL(Driver.DepotNo, 0) > 0 AND ScannerGatewayDriver.DepotId Is Null
				AND ScannerGatewayDepot.Id Is Not Null
		)
	OR
		(
			ScannerGatewayDepot.Id Is Not Null
			AND
			ScannerGatewayDriver.DepotId Is Not Null
			AND
			ScannerGatewayDriver.DepotId != ScannerGatewayDepot.Id
		)
	OR
		(
			ScannerGatewayDepot.Id Is Not Null AND ScannerGatewayDriver.DepotId Is Null
		)
	OR
		(
			ScannerGatewayDepot.Id Is Null AND ScannerGatewayDriver.DepotId Is Not Null
		)
	OR
		(
			ISNULL(Driver.DriverName, '') != ISNULL(ScannerGatewayDriver.Name, '')
		)
	OR
		(
			Driver.ContractorType Is Not Null AND SystemCodes.FriendlyCode Is Null
		)
	OR
		(
			ISNULL(Driver.ContractorType, 'O') != ISNULL(SystemCodes.FriendlyCode, '')
			AND Driver.ContractorType Is Not Null
			AND SystemCodes.FriendlyCode Is Not Null
		)
	OR  (
		ScannerGatewayDriver.Id IS NULL
		)
	OR
		(
			SystemCodes.Id Is Not Null
			AND ScannerGatewayDriver.DriverTypeId Is Null
		)
	OR
		(
			SystemCodes.Id Is Null
			AND ScannerGatewayDriver.DriverTypeId Is Not Null
		)
	OR
		(
			SystemCodes.Id Is Not Null
			AND ScannerGatewayDriver.DriverTypeId Is Not Null
			AND SystemCodes.Id != ScannerGatewayDriver.DriverTypeId
		)
	)



/**** Update IsActive flag on changed records ****/
UPDATE ScannerGateway.dbo.Driver
SET IsActive = 0, LastModifiedDate = GETDATE() --, EffectiveDate = GETDATE()
FROM ScannerGateway.dbo.Driver AS ScannerGatewayDriver 
JOIN #T ON #T.DriverId = ScannerGatewayDriver.Id
WHERE NOT(ScannerGatewayDriver.Code = #T.DriverNumber AND ScannerGatewayDriver.BranchId = #T.BranchId AND ScannerGatewayDriver.EffectiveDate = CAST(GETDATE()AS DATE))


/**** Insert new records for records that changed ****/
INSERT INTO ScannerGateway.dbo.Driver
(Code, Name, ProntoDriverCode, IsActive, IsContractor, DriverStartDate, DriverEndDate, BranchId, EffectiveDate, DriverTypeId, DepotId)
--SELECT DISTINCT DriverNumber, DriverName, ProntoId, #T.IsActive, ISNULL(#T.IsContractor, 0) AS IsContractor, StartDate, EndDate, ISNULL(#T.BranchId, CosmosBranchId) AS BranchId, CAST(#T.EffectiveDate AS DATE) AS EffectiveDate, #T.DriverTypeId, #T.DepotId
--01/07/2011 - Craig Parris
--not sure why effective date was being cast as DATE only
SELECT DISTINCT DriverNumber, DriverName, ProntoId, #T.IsActive, ISNULL(#T.IsContractor, 0) AS IsContractor, StartDate, EndDate, ISNULL(#T.BranchId, CosmosBranchId) AS BranchId, CAST(#T.EffectiveDate AS DATETIME) AS EffectiveDate, #T.DriverTypeId, #T.DepotId
FROM #T
WHERE NOT EXISTS (SELECT ScannerGatewayDriver.Id FROM ScannerGateway.dbo.Driver AS ScannerGatewayDriver
	WHERE ScannerGatewayDriver.Code = #T.DriverNumber
	AND ScannerGatewayDriver.BranchId = #T.BranchId
	AND ScannerGatewayDriver.EffectiveDate = #T.EffectiveDate 
	AND ScannerGatewayDriver.IsActive = 1
	AND ScannerGatewayDriver.Id = #t.DriverId)


/**** INSERT NEW RECORDS INTO ScannerGateway TABLE ****/
INSERT INTO ScannerGateway.dbo.Driver
(Code, Name, ProntoDriverCode, IsActive, IsContractor, DriverStartDate, DriverEndDate, BranchId, EffectiveDate, DepotId)
SELECT DISTINCT DriverNumber, DriverName, ProntoId, 1, 0, StartDate, EndDate, ScannerGatewayBranch.Id, 
--CAST(EffectiveDate AS DATE), 
EffectiveDate,
ScannerGatewayDepot.Id
FROM [Cosmos].dbo.Driver
LEFT JOIN ScannerGateway.dbo.Branch AS ScannerGatewayBranch ON Driver.Branch = ScannerGatewayBranch.CosmosBranch 
LEFT JOIN ScannerGateway.dbo.Depot AS ScannerGatewayDepot ON ISNULL(Driver.DepotNo, 0) = ISNULL(ScannerGatewayDepot.CosmosDepotCode, 0)
	AND ScannerGatewayDepot.BranchId = (SELECT B3.Id FROM ScannerGateway.dbo.Branch B3 WHERE ISNULL(B3.CosmosBranch, '') = Driver.Branch)
WHERE NOT EXISTS (	SELECT * 
					FROM ScannerGateway.dbo.Driver AS ScannerGatewayDriver WHERE Driver.DriverNumber = ScannerGatewayDriver.Code 
					AND Driver.Branch = ScannerGatewayBranch.CosmosBranch 
					AND ScannerGatewayDriver.IsActive = 1 
					AND ScannerGatewayDriver.IsDeleted = 0);


DROP TABLE #T;

END
GO
