SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateStatZoneFromLoad]
AS

      --'=====================================================================
    --' CP -Stored Procedure -[UpdateStatZoneFromLoad]
    --' ---------------------------
    --' Purpose: Loads StatZone Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM Load_StatZone) > 0 
BEGIN

	BEGIN TRAN

	/**** Load changed records into staging table ****
	SELECT StatZone.Id
		,[DELTA]
		,[Load_StatZone].[TIMESTAMP]
		,UPPER(LEFT(Load_StatZone.Branch, 1)) + (RIGHT(LOWER(Load_StatZone.[Branch]), LEN(Load_StatZone.[Branch]) -1)) AS Branch
		,CAST([sz_id] AS INT) AS [sz_id]
		,sz_name
		--INTO #Staging	
	FROM [Load_StatZone]
	LEFT JOIN [StatZone] ON StatZone.Id = sz_id
	AND [Load_StatZone].Branch = [StatZone].Branch 
	AND CONVERT(DATETIME, [Load_StatZone].[TIMESTAMP], 120) = CONVERT(DATETIME, EffectiveDate, 120)
	AND StatZone.IsActive = 1
	WHERE (Delta = 'ADD' OR Delta = 'MOD' ) 


	/**** Update IsActive flag on changed records ****/
	UPDATE StatZone
	SET IsActive = 1
	SELECT * FROM StatZone
	JOIN #Staging ON #Staging.Id = StatZone.Id
	WHERE #Staging.[TIMESTAMP] = EffectiveDate



	/**** InserT new and changed records ****/
	INSERT INTO [Cosmos].[dbo].[StatZone]
			   ([Branch]
			  ,[StatZoneId]
			  ,[Operator]
			  ,[Date]
			  ,[TelephoneNumber]
			  ,[Fax]
			  ,[Contact]
			  ,[Company]
			  ,[Address1]
			  ,[Address2]
			  ,[Heard]
			  ,[Freight]
			  ,[When]
			  ,[Use]
			  ,[Weekly]
			  ,[Comments1]
			  ,[Comments2]
			  ,[DriverNumber]
			  ,[StatZoneRepId]
			  ,[Result]
			  ,[Reason]
			  ,[Coupons1]
			  ,[Coupons2]
			  ,[Coupons3]
			  ,[Coupons4]
			  ,[StatZoneValue]
			  ,[SComment1]
			  ,[SComment2]
			  ,[Completed]
			  ,[TimeStamp]
			  ,[IsActive]
			  ,[EffectiveDate]
			  ,[IsDeleted]
			  ,[CreatedDate]
			  ,[LastModifiedDate]
	)
	SELECT [BRANCH]
		  ,[s_StatZoneid]
		  ,[s_operator]
		  ,[s_date]
		  ,[s_telnr]
		  ,[s_fax]
		  ,[s_contact]
		  ,[s_company]
		  ,[s_addr 1]
		  ,[s_addr 2]
		  ,[s_heard]
		  ,[s_freight]
		  ,[s_when]
		  ,[s_use]
		  ,[s_weekly]
		  ,[s_comments 1]
		  ,[s_comments 2]
		  ,[s_drivno]
		  ,[s_StatZonerepid]
		  ,[s_result]
		  ,[s_reason]
		  ,[s_coupons 1]
		  ,[s_coupons 2]
		  ,[s_coupons 3]
		  ,[s_coupons 4]
		  ,[s_StatZoneval]
		  ,[s_scomment 1]
		  ,[s_scomment 2]
		  ,[s_completed]
		  ,[s_timestamp]
		  ,1 AS [IsActive]
		  ,[TIMESTAMP] AS [EffectiveDate]
		  ,0 AS [IsDeleted]
		  ,GETDATE() AS [CreatedDate]
		  ,GETDATE() AS [LastModifiedDate]
	  FROM #Staging;

	/**** Drop staging table, no longer needed ****/
	DROP TABLE #Staging;


	/**** Mark deleted records accordingly ****/
	UPDATE StatZone
	SET IsDeleted = 1
	FROM StatZone 
	JOIN Load_StatZone ON StatZone.StatZoneId = s_StatZoneid 
							AND StatZone.Branch = Load_StatZone.BRANCH 
							AND StatZone.IsActive = 1
							AND StatZone.IsDeleted = 0
	WHERE DELTA = 'DEL';

	/**** Empty load table ****/
	TRUNCATE TABLE Load_StatZone;


	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			--TRUNCATE TABLE Load_StatZone;
			COMMIT TRAN;
		END;    

*/

END;
GO
