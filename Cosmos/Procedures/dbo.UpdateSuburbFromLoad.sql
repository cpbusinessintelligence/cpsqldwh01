SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateSuburbFromLoad]
AS

      --'=====================================================================
    --' CP -Stored Procedure -[UpdateSuburbFromLoad]
    --' ---------------------------
    --' Purpose: Loads Suburb Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================

SET DATEFORMAT ymd;
SET NOCOUNT ON;
/**** Only do work if records exist in load table ****/
IF (SELECT COUNT(*) FROM [Load_Suburb]) > 0 
BEGIN
	BEGIN TRAN

	INSERT INTO [Cosmos].[dbo].[Suburb]
	(
	[Branch]
		  ,[Code]
		  ,[Abbr]
		  ,[Zone]
		  ,[Name]
		  ,[DepotNumber]
		  ,[PostCode]
		  ,[Uses]
		  ,[Driver]
		  ,[Cut]
		  ,[EtaRangeFrom0]
		  ,[EtaRangeTo0]
		  ,[EtaRangeFrom1]
		  ,[EtaRangeTo1]
		  ,[EtaRangeFrom2]
		  ,[EtaRangeTo2]
		  ,[EtaRangeFrom3]
		  ,[EtaRangeTo3]
		  ,[MapX]
		  ,[MapY]
		  ,[IsDeleted]
		  ,[LastModifiedDate]
		  ,[CreatedDate]
		  ,[EffectiveDate]
		  )
	SELECT 
			UPPER(LEFT(Branch, 1)) + (RIGHT(LOWER([Branch]), LEN(Branch) -1)) AS Branch
			,[s_subcode]
			,[s_subabbr]
			,[s_subzone]
			,[s_subname]
			,[s_depotno]
			,[s_postcode]
			,[s_uses]
			,[s_driver]
			,[s_cut]
			,CAST([s_eta_range_from_0] AS TIME) AS [s_eta_range_from_0]
			,CAST([s_eta_range_to_0] AS TIME) AS [s_eta_range_to_0]
			,CAST([s_eta_range_from_1] AS TIME) AS [s_eta_range_from_1]
			,CAST([s_eta_range_to_1] AS TIME) AS [s_eta_range_to_1]
			,CAST([s_eta_range_from_2] AS TIME) AS [s_eta_range_from_2]
			,CAST([s_eta_range_to_2] AS TIME) AS [s_eta_range_to_2]
			,CAST([s_eta_range_from_3] AS TIME) AS [s_eta_range_from_3]
			,CAST([s_eta_range_to_3] AS TIME) AS [s_eta_range_to_3]
			,[s_map_x]
			,[s_map_y]
			,0
			,GETDATE()
			,GETDATE()
			,[TIMESTAMP]
	FROM [Cosmos].[dbo].[Load_Suburb]
	  WHERE (DELTA = 'ADD' OR DELTA = 'MOD') AND 
		NOT EXISTS (SELECT * FROM [Suburb]
		WHERE Suburb.Code = Load_Suburb.s_subcode
		AND Suburb.Branch = Load_Suburb.BRANCH
		AND Suburb.EffectiveDate = Load_Suburb.[TIMESTAMP]
		AND Suburb.PostCode = Load_Suburb.s_postcode)


		UPDATE [Suburb] 
		SET [IsDeleted] = 1, [LastModifiedDate] = GETDATE()
		FROM [Suburb] 
		JOIN [Load_Suburb] ON Suburb.Code = Load_Suburb.s_subcode
		AND Suburb.Branch = Load_Suburb.BRANCH
		AND Suburb.EffectiveDate = Load_Suburb.[TIMESTAMP]
		AND Suburb.PostCode = Load_Suburb.s_postcode
		WHERE Delta = 'DEL';
		
	IF @@ERROR <> 0
		BEGIN 
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			COMMIT TRAN;
			TRUNCATE TABLE Load_Suburb;
		END;    
END;



GO
