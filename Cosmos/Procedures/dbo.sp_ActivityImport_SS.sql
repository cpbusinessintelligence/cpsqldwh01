SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_ActivityImport_SS (@ActivityDateTime DateTime)
as

Begin transaction PerfRep9

Insert into [dbo].[ActivityImport_Archive2018]
select * from [dbo].[ActivityImport]
where [ActivityDateTime] < = @ActivityDateTime

Delete from [dbo].[ActivityImport]
where [ActivityDateTime] < = @ActivityDateTime


Commit transaction PerfRep9

GO
