SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveCosmos_Booking]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(CreatedDate) from [Booking]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO Booking_Archive2018
	SELECT [BookingId]
      ,[Branch]
      ,[BookingDate]
      ,[BookingType]
      ,[BookingDateTime]
      ,[BookingCustomerId]
      ,[PickupCustomerId]
      ,[DeliverCustomerId]
      ,[FromSuburbId]
      ,[ToSuburbId]
      ,[FromAddress1]
      ,[FromAddress2]
      ,[FromAddress3]
      ,[FromAddress4]
      ,[FromAddress5]
      ,[ToAddress1]
      ,[ToAddress2]
      ,[ToAddress3]
      ,[ToAddress4]
      ,[ToAddress5]
      ,[InstructionsLine1]
      ,[InstructionsLine2]
      ,[InstructionsLine3]
      ,[InstructionsLine4]
      ,[CommentsLine1]
      ,[CommentsLine2]
      ,[CommentsLine3]
      ,[CommentsLine4]
      ,[CommentsLine5]
      ,[CommentsLine6]
      ,[CommentsLine7]
      ,[CommentsLine8]
      ,[CommentsLine9]
      ,[CommentsLine10]
      ,[CommentsLine11]
      ,[CommentsLine12]
      ,[CommentsLine13]
      ,[CommentsLine14]
      ,[CommentsLine15]
      ,[CommentsLine16]
      ,[Caller]
      ,[Who]
      ,[Where]
      ,[CreatedById]
      ,[AllocatedBy]
      ,[DriverId]
      ,[CDriverId]
      ,[DeliverDriverId]
      ,[PickupDriverId]
      ,[Status]
      ,[TimeBooked]
      ,[TimeAllocated]
      ,[TimePickup]
      ,[TimeComplete]
      ,[TimePOD]
      ,[PODName]
      ,[TimeQueued]
      ,[TimeSent]
      ,[TimeReceived]
      ,[TimeAccepted]
      ,[ReceiverPays]
      ,[Flags]
      ,[EtaFrom]
      ,[EtaTo]
      ,[QueryKey]
      ,[Rebook]
      ,[QueryOperatorId]
      ,[COperatorId]
      ,[QueryTime]
      ,[EmmId]
      ,[RebookBookingId]
      ,[RebookDate]
      ,[IsDeleted]
      ,[LastModifiedDate]
      ,[CreatedDate]
      ,[EffectiveDate]
      ,[IsActive]
      ,[b_futile_stamp]
      ,[b_futile_driver]
      ,[b_futile_reason]
      ,[BookingNumber]
  FROM [Cosmos].[dbo].[Booking] (nolock) where CreatedDate >= @MinDate and  CreatedDate <= DATEADD(day, 1,@MinDate)


	DELETE FROM [Cosmos].[dbo].[Booking] where  CreatedDate >= @MinDate and  CreatedDate <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
