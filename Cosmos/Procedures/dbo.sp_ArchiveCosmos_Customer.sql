SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveCosmos_Customer]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min([CreatedDate]) from [Customer] where isactive = 0
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [Customer_Archive2018]
	SELECT [Id]
      ,[Branch]
      ,[CustomerId]
      ,[Code]
      ,[Name]
      ,[DeliveryAddress1]
      ,[DeliveryAddress2]
      ,[DeliveryAddress3]
      ,[DeliveryInstrustions1]
      ,[DeliveryInstrustions2]
      ,[DeliverySuburb]
      ,[InvoiceAddress1]
      ,[InvoiceAddress2]
      ,[InvoiceAddress3]
      ,[InvoiceSuburb]
      ,[Contact1]
      ,[Contact2]
      ,[Contact3]
      ,[EmailAddress1]
      ,[EmailAddress2]
      ,[EmailAddress3]
      ,[Phone1]
      ,[Phone2]
      ,[Phone3]
      ,[Fax]
      ,[OpenTime]
      ,[CloseTime]
      ,[DriverNumber]
      ,[DepotNumber]
      ,[BookInstrustions1]
      ,[BookInstrustions2]
      ,[Startdate]
      ,[Lastused1]
      ,[Lastused2]
      ,[AccType]
      ,[IndCode]
      ,[Coupons1]
      ,[Coupons2]
      ,[Coupons3]
      ,[Coupons4]
      ,[Coupons5]
      ,[Coupons6]
      ,[Coupons7]
      ,[Coupons8]
      ,[RegularRun]
      ,[PickupDays]
      ,[Remarks]
      ,[SalesRep1]
      ,[SalesRep2]
      ,[Rating]
      ,[Status]
      ,[Modified]
      ,[AddedBy]
      ,[AddedWhen]
      ,[EFT]
      ,[SalesRepCode]
      ,[SalesRepStart]
      ,[ProntoAccountCode]
      ,[IsActive]
      ,[EffectiveDate]
      ,[IsDeleted]
      ,[CreatedDate]
      ,[LastModifiedDate]
  FROM [Cosmos].[dbo].[Customer] (nolock) where isactive = 0 and [CreatedDate] >= @MinDate and  [CreatedDate] <= DATEADD(day, 1,@MinDate)


	DELETE FROM [Cosmos].[dbo].[Customer] where isactive = 0 and  [CreatedDate] >= @MinDate and [CreatedDate] <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
