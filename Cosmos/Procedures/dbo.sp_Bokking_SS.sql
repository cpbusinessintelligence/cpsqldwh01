SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc sp_Bokking_SS (@CreatedDate DateTime)
as

Begin transaction PerfRep10

Insert into [dbo].[Booking_Archive2018]
select * from [dbo].Booking
where [CreatedDate] < = @CreatedDate

Delete from [dbo].Booking
where [CreatedDate] < = @CreatedDate


Commit transaction PerfRep10

GO
