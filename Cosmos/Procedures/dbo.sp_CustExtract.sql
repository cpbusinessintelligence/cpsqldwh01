SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_CustExtract
as
begin
SELECT 
      [Branch]
      ,[CustomerId]
      ,[Code]
      ,[Name]
      ,[DeliveryAddress1]
      ,[DeliveryAddress2]
      ,[DeliveryAddress3]
         ,InvoiceAddress1
         ,InvoiceAddress2
         ,InvoiceAddress3
      ,ProntoAccountCode
     
  FROM [Cosmos].[dbo].[Customer]
  where [IsActive] =1
  end
GO
GRANT EXECUTE
	ON [dbo].[sp_CustExtract]
	TO [ReportUser]
GO
