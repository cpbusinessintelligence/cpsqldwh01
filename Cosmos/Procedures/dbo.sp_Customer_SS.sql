SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc sp_Customer_SS (@CreatedDate DateTime)
as

Begin transaction PerfRep8

Insert into [dbo].[Customer_Archive2018]
select * from [dbo].[Customer]
where isactive =0
and [CreatedDate] < = @CreatedDate

Delete from [dbo].[Customer]
where  isactive =0
and [CreatedDate] < = @CreatedDate


Commit transaction PerfRep8

GO
