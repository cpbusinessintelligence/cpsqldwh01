SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_DriverExtract
as
begin

select Branch,DriverNumber,prontoid,Drivername,Address1,Address2,Address3,PostCode  from [dbo].[Driver]
where ContractorType = 'c' and IsActive = 1
end
GO
GRANT EXECUTE
	ON [dbo].[sp_DriverExtract]
	TO [ReportUser]
GO
