SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Praveen Valappil
-- Create date: 07/07/2020
-- Description:	Created on behalf of Jobin, to send weekly report to John
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUpdatedActivityImportData_Weekly]
AS
BEGIN
	SELECT [SerialNumber], Left([SerialNumber],3) as Prefix , Min([ActivityDateTime]) as date 
	INTO #Tmp1
	FROM [Cosmos].[dbo].[ActivityImport]
	WHERE [ActivityDateTime] >= '2020-05-01 00:00:07.000'
	GROUP BY [SerialNumber]

	--Drop table #Tmp1
	SELECT CONVERT(DATE,[date]) as [Date] , COUNT(*) as PrepaidCount 
	FROM #Tmp1
	WHERE Revenue.[dbo].[CPPL_fn_GetCouponTypeFromPrefix](Prefix) NOT IN ('COD','REDELIVERY CPN','LINK - REGIONAL','PROMO','LINK','ATL','IRP TRK','RETURN TRK','REDELIVERY CARD','IRP')
	GROUP BY CONVERT(DATE,[date])
	ORDER BY 1 
End
GO
GRANT EXECUTE
	ON [dbo].[sp_GetUpdatedActivityImportData_Weekly]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_GetUpdatedActivityImportData_Weekly]
	TO [SSISUser]
GO
