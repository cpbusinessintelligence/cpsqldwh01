SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptDriverActivitybyCouponPrefix](@StartDate Date,@EndDate Date,@ProntoDriverCode varchar(100)) as
begin
  
  --'=====================================================================
    --' CP -Stored Procedure -sp_DriverActivitybyCouponPrefix
    --' ---------------------------
    --' Purpose: sp_DriverActivitybyCouponPrefix-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Dec 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/12/2015    AB      1.00    Created the procedure                             --AB20151209

    --'=====================================================================




select distinct serialnumber
into #temp
from cosmos.dbo.activityimport where convert(date,activitydatetime) between @StartDate and @EndDate
and len(serialnumber)=11 and isnumeric(serialnumber)=1

--drop table #temp2

Select serialnumber,convert(datetime,'') as PUDatetime,convert(varchar(100),'') as PUBranch,convert(varchar(100),'') as PUDrivernumber,convert(varchar(100),'') as PUProntoDriverCode,
convert(datetime,'') as DelDatetime,convert(varchar(100),'') as DelBranch,convert(varchar(100),'') as DelDrivernumber,convert(varchar(100),'') as DelProntoDriverCode
into #temp1
from #temp


Select serialnumber,min(eventdatetime) as minded
into #temp2
from #temp1 join scannergateway.dbo.trackingevent e (NOLOCK) on e.sourcereference=serialnumber
where eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2') and convert(date,eventdatetime) between @StartDate and @EndDate
group by serialnumber

Update #temp1 set DelDatetime=minded from #temp2 t1 where #temp1.serialnumber=t1.serialnumber


Update #temp1 set PUDatetime=eventdatetime,PUBranch=b.name,PUDrivernumber=d.code,PUProntoDriverCode=d.ProntoDriverCode
from #temp1 join scannergateway.dbo.trackingevent t (NOLoCK) on t.sourcereference=serialnumber join scannergateway.dbo.driver d on d.id=t.driverid join
scannergateway.dbo.branch b on b.id=d.branchid
where eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C' and d.ProntoDriverCode=@ProntoDriverCode and convert(date,eventdatetime) between @StartDate and @EndDate


Update #temp1 set DelBranch=b.name,DelDrivernumber=d.code,DelProntoDriverCode=d.ProntoDriverCode
from #temp1 join scannergateway.dbo.trackingevent t (NOLoCK) on t.sourcereference=serialnumber and  DelDatetime=t.eventdatetime join scannergateway.dbo.driver d on d.id=t.driverid join
scannergateway.dbo.branch b on b.id=d.branchid
where eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')  and d.ProntoDriverCode=@ProntoDriverCode and convert(date,eventdatetime) between @StartDate and @EndDate

Select *,left(ltrim(rtrim(serialnumber)),3) as Prefix,convert(varchar(100),'') as Coupontype into #temp3 from #temp1

Update #temp3 set Coupontype=P.Category
from #temp3 join dwh.dbo.dimproduct p on p.code=prefix

select convert(date,PUDatetime) as PUDate,PUDriverNumber,PUBranch,PUProntoDriverCode,Prefix,CouponType,count(PUDrivernumber)  as Pickupcount
into #temp4 from #temp3
where PUProntoDrivercode=@ProntoDriverCode
group by convert(date,PUDatetime),PUDriverNumber,PUBranch,PUProntoDriverCode,Prefix,CouponType

select convert(date,DelDatetime) as DelDate,DelDriverNumber,DelBranch,DelProntoDriverCode,Prefix,CouponType,count(DelDrivernumber)  as DeliveryCount
into #temp5 from #temp3
where DelProntoDrivercode=@ProntoDriverCode
group by convert(date,DelDatetime),DelDriverNumber,DelBranch,DelProntoDriverCode,Prefix,CouponType


Create table #tempFinal(Date date,[Action] varchar(100),DriverNumber int,Branch varchar(100),ProntoDriverCode varchar(100),Prefix varchar(100),CouponType varchar(100),Count int)

Insert into #tempFinal
Select PUDate,'Pickup',PUDriverNumber,PUBranch,PUProntoDriverCode,Prefix,CouponType,Pickupcount from #temp4


Insert into #tempFinal
Select DelDate,'Delivered',DelDriverNumber,DelBranch,DelProntoDriverCode,Prefix,CouponType,Deliverycount from #temp5


select * from #tempFinal where ProntoDriverCode=@ProntoDriverCode
order by Date,Action desc,Prefix

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriverActivitybyCouponPrefix]
	TO [ReportUser]
GO
