SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_RptDriverActivitybyEDIServicecode(@StartDate Date,@EndDate Date,@ProntoDriverCode varchar(100)) as
begin
  
  --'=====================================================================
    --' CP -Stored Procedure -sp_DriverActivitybyCouponPrefix
    --' ---------------------------
    --' Purpose: sp_DriverActivitybyCouponPrefix-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Dec 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/12/2015    AB      1.00    Created the procedure                             --AB20151209

    --'=====================================================================

	
  Select Sourcereference,eventdatetime as Eventdatetime,d.code as Run,d.ProntoDriverCode as Contractor,case when e.Description='Attempted delivery' then 'Delivered' else  e.Description end as Event ,convert(varchar(100),'') as Connote,convert(varchar(100),'') as Servicecode,convert(varchar(200),'') as  Pricecodedescription,convert(int,0) as [Count]
  into #temp
  from scannergateway.dbo.trackingevent(NOLOCK) t join scannergateway.dbo.driver d on d.id=t.driverid join scannergateway.dbo.branch b on b.id=d.branchid
           join scannergateway.dbo.eventtype e on t.eventtypeid=e.id
		    where convert(date,eventdatetime) 
  between @StartDate and @EndDate and not (len(sourcereference)=11 and isnumeric(sourcereference)=1)
  and eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2','98EBB899-A15E-4826-8D05-516E744C466C')
  and ProntoDriverCode=@ProntoDriverCode

 Update #temp set Connote=cd_connote,Servicecode=cd_pricecode
from #temp join cpplEDI.dbo.cdcoupon cc on cc.cc_coupon=SourceReference join cpplEDI.dbo.consignment c on c.cd_id=cc_consignment

Update #temp set Pricecodedescription=pc_name
from cppledi.dbo.pricecodes p where pc_code=servicecode


Select distinct convert(date,eventdatetime) as Date,connote,event,servicecode,Pricecodedescription,Run,Contractor
into #temp1 from #temp 

select Date,Event,Servicecode,PriceCodeDescription,Run,Contractor,count(*) as Count
from #temp1 
group by Date,event,servicecode,Pricecodedescription,Run,Contractor
order by Date,Event desc


end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriverActivitybyEDIServicecode]
	TO [ReportUser]
GO
