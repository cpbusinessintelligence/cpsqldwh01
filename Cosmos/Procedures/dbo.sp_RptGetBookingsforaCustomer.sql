SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptGetBookingsforaCustomer] (@Account Varchar(20)) as
begin


      --'=====================================================================
    --' CP -Stored Procedure -sp_RptGetMissedBookings
    --' ---------------------------
    --' Purpose: sp_RptGetMissedBookings-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Aug 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/08/2016    AB      1.00    Created the procedure                             --AB20160805

    --'=====================================================================


Select Top 1  cd_Connote as Connote,cd_date as ConsignmentDate,cd_account as Account,c_name as CompanyName,cd_pickup_addr0 as PickupAddr1,cd_pickup_addr1 as PickupAddr2,cd_pickup_addr2 as PickupAddr3,cd_pickup_addr3 as PickupAddr4,cd_pickup_suburb as PickupSuburb,cd_pickup_postcode as PickupPostcode
into #temp
from cpplEDI.dbo.consignment join  cpplEDI.dbo.companyaccount on ca_account=cd_account
                             join  cpplEDI.dbo.companies c on c_id=ca_company_id

where cd_account = @Account
and convert(date,cd_date)>=convert(date,dateadd(day,-14,getdate()))

Select t.*,isnull(Branch,'') as Branch,isnull(PickupDriverId,'') as PickupDriverId,case when isnull(Status,'')='F' then 'Finished' else isnull(Status,'') end as Status,isnull(b_futile_stamp,'')  as FutileStamp,isnull(b_futile_driver,'') as FutileDriver,isnull(b_futile_reason,'') as FutileReason
from #temp t left join cosmos.dbo.booking b on isnull(caller,'')=Connote
--where b.bookingid is null

--Select * from cosmos.dbo.booking

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptGetBookingsforaCustomer]
	TO [ReportUser]
GO
