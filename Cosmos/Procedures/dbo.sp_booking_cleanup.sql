SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_booking_cleanup
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	


select distinct b.name,b.Code,b.PostCode,b.Branch into #tmpsuburb from Suburb b where b.Code not in (15,49,623,1720,1056) 

--select * from #tmpsuburb --where name like '%,%'

--Removing $ characater from suburb
--Select *, case CHARINDEX('$', name) When 0 then  name Else  LEFT(name, CHARINDEX('$', name)- 1) end as Sub   from #tmpsuburb where name like '%$%'


Update #tmpsuburb Set name = case CHARINDEX('$', name) When 0 then  name 
Else  LEFT(name, CHARINDEX('$', name)- 1) end 

--Removing < character from suburb
--Select *, case CHARINDEX('<', name) When 0 then  name Else  LEFT(name, CHARINDEX('<', name)- 1) end as Sub   from #tmpsuburb where name like '%<%'


Update #tmpsuburb Set name = case CHARINDEX('<', name) When 0 then  name 
Else  LEFT(name, CHARINDEX('<', name)- 1) end 

--Removing ( from suburb
--Select *, case CHARINDEX('(', name) When 0 then  name Else  LEFT(name, CHARINDEX('(', name)- 1) end as Sub   from #tmpsuburb where name like '%(%'

Update #tmpsuburb Set name = case CHARINDEX('(', name) When 0 then  name 
Else  LEFT(name, CHARINDEX('(', name)- 1) end


--Removing { from suburb
--Select *, case CHARINDEX('{', name) When 0 then  name Else  LEFT(name, CHARINDEX('{', name)- 1) end as Sub   from #tmpsuburb where name like '%{%'

Update #tmpsuburb Set name = case CHARINDEX('{', name) When 0 then  name 
Else  LEFT(name, CHARINDEX('{', name)- 1) end


--Removing , from suburb
--Select *, case CHARINDEX(',', name) When 0 then  name Else  LEFT(name, CHARINDEX(',', name)- 2) end as Sub   from #tmpsuburb where name like '%,%'

Update #tmpsuburb Set name = case CHARINDEX(',', name) When 0 then  name 
Else  LEFT(name, CHARINDEX(',', name)- 2) end


select distinct
	CreatedDate
      ,[BookingType]
       
      ,[FromAddress1]
      ,[FromAddress2]
      ,[FromAddress3]
      ,[FromAddress4]
	  ,b.FromSuburbId
	  ,s1.Name [FromSuburb]
	  ,s1.PostCode [FromPostcode]
	      
      
     into #tmpbookingFromaddress
  
 from Booking b
inner join #tmpsuburb s1  on b.FromSuburbId=s1.Code and s1.Branch=b.Branch
--left join #tmpsuburb s2 on b.ToSuburbId=s2.Code
where 
BookingType='J'
and month(b.CreatedDate)=8 



--Set Fromaddress4 null when fromaddress1 is null
update #tmpbookingFromaddress set  fromaddress4 =null where FromAddress1 is null

--Set fromaddress4 is null when fromaddress contains coupons

update #tmpbookingFromaddress set  fromaddress4 =null where FromAddress4 like '%COUPONS%'

--Set FromAddress4 null where FromAddress contains digit

update #tmpbookingFromaddress set  fromaddress4 =null where isnumeric(FromAddress4)=1

--Set Fromaddress null where address4 contains PPW- not useful data

update #tmpbookingFromaddress set  fromaddress4 =null where FromAddress4 like 'PPw%'

--Set FromAddress1 and FromAddress2 is null where fromaddress contains consignment
update #tmpbookingFromaddress set  FromAddress1 =null,FromAddress2=null where FromAddress1 like '%CN:CP%'

--Set FromAddress1 null and FromAddress2 null where FromAddress1 has reminder or redelivery info

update #tmpbookingFromaddress set  FromAddress1 =null,FromAddress2=null where FromAddress1 like 'Reminder%' or FromAddress1 like 'Redelivery%'

--Delete the records where fromaddress1 and fromaddress2 is null

Delete from  #tmpbookingFromaddress where FromAddress1 is null and FromAddress2 is null

----#tmpbookingToaddress To addressinfor

 select distinct
	CreatedDate
      ,[BookingType]
         
      ,[ToAddress1]
      ,[ToAddress2]
      ,[ToAddress3]
     ,s2.Name [ToSuburb]
	 ,s2.PostCode [ToPostcode]
        
     into #tmpbookingToaddress
     
 from Booking b
--left join #tmpsuburb s1  on b.FromSuburbId=s1.Code
inner join #tmpsuburb s2 on b.ToSuburbId=s2.Code and b.branch=s2.branch
where 
BookingType='J' 
and month(b.CreatedDate)=8

--Set TOAddress1,Toaddress2 and Toaddress3 null where Toaddress1 contains consignments

update #tmpbookingToaddress set ToAddress1 = null,ToAddress2=null,ToAddress3=null where ToAddress1 like '%Consignments%'

--Set Toaddress1 is null where Toaddress1 contains consignments starts with A-CP
 update #tmpbookingToaddress set ToAddress1=null where  ToAddress1 like '%A-CP%'

 --Set Toaddress3 is null where Toaddress3 contains ITEM info

 update #tmpbookingToaddress  set ToAddress3 = null where toaddress3 like '%ITEM%'

--Delete records where Toaddress1 and Toaddress2 is null
Delete from  #tmpbookingToaddress where ToAddress1 is null and ToAddress2 is null 

--Delete records where Toaddress1 contains Authority to Leave 

Delete from  #tmpbookingToaddress  where  ToAddress1 = 'Authority To Leave'

Select * from #tmpbookingFromaddress

select * from #tmpbookingToaddress


END
GO
