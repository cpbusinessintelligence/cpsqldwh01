SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityImport] (
		[ActivityDateTime]     [datetime] NOT NULL,
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ActivityFlag]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFileOn]       [date] NOT NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_ActivityImport]
	ON [dbo].[ActivityImport] ([ActivityDateTime])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActivityImport] SET (LOCK_ESCALATION = TABLE)
GO
