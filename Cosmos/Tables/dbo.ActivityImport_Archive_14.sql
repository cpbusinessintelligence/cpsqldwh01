SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityImport_Archive_14] (
		[ActivityDateTime]     [datetime] NULL,
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFlag]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFileOn]       [date] NULL
)
GO
ALTER TABLE [dbo].[ActivityImport_Archive_14] SET (LOCK_ESCALATION = TABLE)
GO
