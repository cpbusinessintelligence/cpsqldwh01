SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractorType] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Code]                 [varchar](2) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[IsActive]             [bit] NOT NULL,
		CONSTRAINT [PK_ContractorType]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ContractorType]
	ADD
	CONSTRAINT [DF_ContractorType_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[ContractorType]
	ADD
	CONSTRAINT [DF_ContractorType_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ContractorType]
	ADD
	CONSTRAINT [DF_ContractorType_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[ContractorType]
	ADD
	CONSTRAINT [DF_ContractorType_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ContractorType]
	ADD
	CONSTRAINT [DF_ContractorType_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[ContractorType]
	ADD
	CONSTRAINT [DF_ContractorType_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ContractorType] SET (LOCK_ESCALATION = TABLE)
GO
