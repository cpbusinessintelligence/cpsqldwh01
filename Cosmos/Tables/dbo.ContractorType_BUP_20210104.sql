SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractorType_BUP_20210104] (
		[Id]                   [uniqueidentifier] NOT NULL,
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Code]                 [varchar](2) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[IsActive]             [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ContractorType_BUP_20210104] SET (LOCK_ESCALATION = TABLE)
GO
