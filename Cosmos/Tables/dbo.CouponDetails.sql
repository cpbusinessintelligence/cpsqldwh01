SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponDetails] (
		[CouponNumber]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Prefix]                    [int] NULL,
		[CouponPrice]               [money] NULL,
		[BookStartCouponNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookQty]                   [int] NULL,
		[BookSaleBranch]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookDateSold]              [date] NULL,
		[BookSaleAmount]            [money] NULL,
		[BookInsuranceAmount]       [money] NULL,
		[BookSoldTo]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                [date] NULL,
		[PickupDriver]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]              [date] NULL,
		[DeliveryDriver]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScan]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CouponStatus]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CouponRedeemed]            [bit] NULL,
		[AccountingPeriod]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[FinancialYear]             [varchar](10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CouponDetails] SET (LOCK_ESCALATION = TABLE)
GO
