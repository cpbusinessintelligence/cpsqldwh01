SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponType] (
		[Id]                [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[TypeId]            [int] NOT NULL,
		[DisplayOrder]      [int] NULL,
		[IsPrimary]         [bit] NULL,
		[Abbreviation]      [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Name]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]         [bit] NOT NULL,
		[CreatedDate]       [datetime] NOT NULL,
		[EffectiveDate]     [datetime] NOT NULL,
		[IsActive]          [bit] NOT NULL,
		CONSTRAINT [PK_CouponType]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[CouponType]
	ADD
	CONSTRAINT [DF_CouponType_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[CouponType]
	ADD
	CONSTRAINT [DF_CouponType_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CouponType]
	ADD
	CONSTRAINT [DF_CouponType_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[CouponType]
	ADD
	CONSTRAINT [DF_CouponType_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[CouponType]
	ADD
	CONSTRAINT [DF_CouponType_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
CREATE NONCLUSTERED INDEX [IX_CouponType]
	ON [dbo].[CouponType] ([TypeId], [Branch], [Name])
	ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CouponType_1]
	ON [dbo].[CouponType] ([Branch], [TypeId], [IsActive], [EffectiveDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[CouponType] SET (LOCK_ESCALATION = TABLE)
GO
