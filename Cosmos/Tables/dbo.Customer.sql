SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer] (
		[Id]                        [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustomerId]                [int] NULL,
		[Code]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Name]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress1]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress2]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress3]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryInstrustions1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryInstrustions2]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]            [int] NULL,
		[InvoiceAddress1]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceAddress2]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceAddress3]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceSuburb]             [int] NULL,
		[Contact1]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contact2]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contact3]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EmailAddress1]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EmailAddress2]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EmailAddress3]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Phone1]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Phone2]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Phone3]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fax]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OpenTime]                  [time](7) NULL,
		[CloseTime]                 [time](7) NULL,
		[DriverNumber]              [int] NULL,
		[DepotNumber]               [int] NULL,
		[BookInstrustions1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookInstrustions2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Startdate]                 [date] NULL,
		[Lastused1]                 [date] NULL,
		[Lastused2]                 [date] NULL,
		[AccType]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IndCode]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons1]                  [int] NULL,
		[Coupons2]                  [int] NULL,
		[Coupons3]                  [int] NULL,
		[Coupons4]                  [int] NULL,
		[Coupons5]                  [int] NULL,
		[Coupons6]                  [int] NULL,
		[Coupons7]                  [int] NULL,
		[Coupons8]                  [int] NULL,
		[RegularRun]                [bit] NULL,
		[PickupDays]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Remarks]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SalesRep1]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SalesRep2]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rating]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]                    [bit] NULL,
		[Modified]                  [date] NULL,
		[AddedBy]                   [int] NULL,
		[AddedWhen]                 [date] NULL,
		[EFT]                       [bit] NULL,
		[SalesRepCode]              [int] NULL,
		[SalesRepStart]             [date] NULL,
		[ProntoAccountCode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]                  [bit] NOT NULL,
		[EffectiveDate]             [datetime] NOT NULL,
		[IsDeleted]                 [bit] NOT NULL,
		[CreatedDate]               [datetime] NOT NULL,
		[LastModifiedDate]          [datetime] NOT NULL,
		CONSTRAINT [PK_Customer]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Customer]
	ADD
	CONSTRAINT [DF_Customer_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Customer]
	ADD
	CONSTRAINT [DF_Customer_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Customer]
	ADD
	CONSTRAINT [DF_Customer_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[Customer]
	ADD
	CONSTRAINT [DF_Customer_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Customer]
	ADD
	CONSTRAINT [DF_Customer_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Customer]
	ADD
	CONSTRAINT [DF_Customer_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Customer]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Customer_Customer]
	FOREIGN KEY ([Id]) REFERENCES [dbo].[Customer] ([Id])
ALTER TABLE [dbo].[Customer]
	CHECK CONSTRAINT [FK_Customer_Customer]

GO
CREATE UNIQUE CLUSTERED INDEX [IX_Customer]
	ON [dbo].[Customer] ([Branch], [CustomerId], [IsActive], [EffectiveDate])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Customer_ActiveDeletedBranchCodePronto]
	ON [dbo].[Customer] ([IsActive], [IsDeleted])
	INCLUDE ([Branch], [Code], [ProntoAccountCode])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Customer_ProntoIsDeletedIsActive]
	ON [dbo].[Customer] ([ProntoAccountCode], [IsDeleted], [IsActive])
	INCLUDE ([Id], [Code])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Customer] SET (LOCK_ESCALATION = TABLE)
GO
