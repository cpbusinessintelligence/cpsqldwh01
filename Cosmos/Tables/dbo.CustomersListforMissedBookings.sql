SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomersListforMissedBookings] (
		[ID]                [int] IDENTITY(1, 1) NOT NULL,
		[AccountNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CustomersListforMissedBookings] SET (LOCK_ESCALATION = TABLE)
GO
