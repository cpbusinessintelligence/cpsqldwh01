SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Depot] (
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DepotNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DepotCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DepotName]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DepotAddress1]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DepotAddress2]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DepotSubcode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DepotTimes1]          [time](7) NULL,
		[DepotTimes2]          [time](7) NULL,
		[DepotTimes3]          [time](7) NULL,
		[DepotTimes4]          [time](7) NULL,
		[DepotTimes5]          [time](7) NULL,
		[DepotTimes6]          [time](7) NULL,
		[DepotTimes7]          [time](7) NULL,
		[DepotTimes8]          [time](7) NULL,
		[DepotContact]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[NetworkCategory]      [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_Depot]
		PRIMARY KEY
		CLUSTERED
		([Branch], [DepotNumber], [EffectiveDate])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Depot]
	ADD
	CONSTRAINT [DF_Depot_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Depot] SET (LOCK_ESCALATION = TABLE)
GO
