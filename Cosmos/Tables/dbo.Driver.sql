SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Driver] (
		[Id]                             [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]                         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DriverNumber]                   [int] NOT NULL,
		[ContractorType]                 [varchar](2) COLLATE Latin1_General_CI_AS NULL,
		[DriverName]                     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Address1]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address2]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address3]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]                       [int] NULL,
		[MobileId]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKinName1]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKinName2]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin1HomePhone]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin2HomePhone]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin1WorkPhone]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin2WorkPhone]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin1MobilePhone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin2MobilePhone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin1Relationship]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NextOfKin2Relationship]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Comments1]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Comments2]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NrmaMembershipNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompanyName]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CompanyAddress1]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CompanyAddress2]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CompanyAddress3]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Radio]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[VehicleMake]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[VehicleModel]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[VehicleYear]                    [int] NULL,
		[VehicleRegistration]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[VehicleRegistrationExpriry]     [datetime] NULL,
		[PhoneNumber]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MobileNumber]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FaxNumber]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PagerNumber]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MobileDeviceType]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StartDate]                      [datetime] NULL,
		[EndDate]                        [datetime] NULL,
		[DepotNo]                        [int] NULL,
		[Statzone]                       [int] NULL,
		[CreditLimit]                    [money] NULL,
		[ABN]                            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BankName]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankBSB]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankAccountNumber]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankAccountName]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TaxFileNumber]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CMExclude]                      [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[AgentName]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CombineWith]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateTable]                      [smallint] NULL,
		[OuterDriver]                    [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[ManifestDriver]                 [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[ProntoId]                       [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]                      [bit] NULL,
		[LastModifiedDate]               [datetime] NULL,
		[CreatedDate]                    [datetime] NULL,
		[EffectiveDate]                  [datetime] NOT NULL,
		[IsActive]                       [bit] NULL,
		[ArchiveDate]                    [date] NULL,
		[ArchiveReason1]                 [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[ArchiveReason2]                 [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[ArchiveReason3]                 [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[ArchiveReason4]                 [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[etazone]                        [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[pricingzone]                    [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_Driver]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_LastModified]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_Driver]
	ON [dbo].[Driver] ([Branch], [DriverNumber], [IsActive], [EffectiveDate])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Driver_ProntoIdContractorTypeIsActive]
	ON [dbo].[Driver] ([ContractorType], [ProntoId], [IsActive])
	INCLUDE ([DriverName], [EffectiveDate])
	ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
GRANT DELETE
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[Driver] SET (LOCK_ESCALATION = TABLE)
GO
