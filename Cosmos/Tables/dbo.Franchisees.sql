SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Franchisees] (
		[FranchiseeId]       [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[ProntoId]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[FranchiseeName]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[DateCreated]        [datetime] NOT NULL,
		[DateUpdated]        [datetime] NOT NULL,
		[EmailAddress]       [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_Franchisees]
		PRIMARY KEY
		CLUSTERED
		([FranchiseeId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Franchisees]
	ADD
	CONSTRAINT [DF_Franchisees_FranchiseeID]
	DEFAULT (newid()) FOR [FranchiseeId]
GO
ALTER TABLE [dbo].[Franchisees]
	ADD
	CONSTRAINT [DF_Franchisees_DateCreated]
	DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Franchisees]
	ADD
	CONSTRAINT [DF_Franchisees_DateUpdated]
	DEFAULT (getdate()) FOR [DateUpdated]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_Franchisees_ProntoID]
	ON [dbo].[Franchisees] ([ProntoId])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Franchisees] SET (LOCK_ESCALATION = TABLE)
GO
