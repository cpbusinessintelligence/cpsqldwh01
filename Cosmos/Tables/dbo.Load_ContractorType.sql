SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ContractorType] (
		[DELTA]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_conttype]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_conttdescr]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ContractorType] SET (LOCK_ESCALATION = TABLE)
GO
