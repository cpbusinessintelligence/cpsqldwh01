SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_CosmosCouponData] (
		[CouponNumber]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[BookStartCouponNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookQty]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookSaleBranch]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookDateSold]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookSaleAmount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookInsuranceAmount]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookSoldTo]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDriver]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriver]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScan]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_CosmosCouponData] SET (LOCK_ESCALATION = TABLE)
GO
