SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_CouponType] (
		[DELTA]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_typeid]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_disporder]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_primary]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_abbr]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_type]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_descr]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_CouponType] SET (LOCK_ESCALATION = TABLE)
GO
