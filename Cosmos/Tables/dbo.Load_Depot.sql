SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_Depot] (
		[DELTA]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depotno]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depotcode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depotname]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depotaddr 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depotaddr 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depotsubcode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 2]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 3]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 4]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 5]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 6]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 7]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depottimes 8]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[d_depotcontact]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_Depot] SET (LOCK_ESCALATION = TABLE)
GO
