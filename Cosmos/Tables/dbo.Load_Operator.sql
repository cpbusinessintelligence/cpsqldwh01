SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_Operator] (
		[DELTA]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[o_id]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[o_name]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_Operator] SET (LOCK_ESCALATION = TABLE)
GO
