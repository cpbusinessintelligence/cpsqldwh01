SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_PublicHoliday] (
		[DELTA]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILETYPE]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[h_date]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[h_descr]       [varchar](200) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_PublicHoliday] SET (LOCK_ESCALATION = TABLE)
GO
