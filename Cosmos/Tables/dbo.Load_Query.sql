SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_Query] (
		[DELTA]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[q_qkey]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[q_msg]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_Query] SET (LOCK_ESCALATION = TABLE)
GO
