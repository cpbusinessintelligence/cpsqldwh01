SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_Sales] (
		[DELTA]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_salesid]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_operator]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_date]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_telnr]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_fax]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_contact]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_company]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_addr 1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_addr 2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_heard]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_freight]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_when]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_use]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_weekly]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_comments 1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_comments 2]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_drivno]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_salesrepid]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_result]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_reason]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_coupons 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_coupons 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_coupons 3]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_coupons 4]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_salesval]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_scomment 1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_scomment 2]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_completed]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_timestamp]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_Sales] SET (LOCK_ESCALATION = TABLE)
GO
