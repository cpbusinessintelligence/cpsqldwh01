SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_Salesrep] (
		[DELTA]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_salesrepid]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_name]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_login]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_address 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_address 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_address 3]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_phone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_mobile]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_Salesrep] SET (LOCK_ESCALATION = TABLE)
GO
