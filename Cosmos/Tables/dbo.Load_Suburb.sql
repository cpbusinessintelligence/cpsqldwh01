SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_Suburb] (
		[DELTA]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TIMESTAMP]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FILE TYPE]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_subcode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_subabbr]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_subzone]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_subname]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_depotno]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_postcode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_uses]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_driver]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_cut]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_from_0]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_to_0]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_from_1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_to_1]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_from_2]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_to_2]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_from_3]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_eta_range_to_3]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_map_x]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[s_map_y]                [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_Suburb] SET (LOCK_ESCALATION = TABLE)
GO
