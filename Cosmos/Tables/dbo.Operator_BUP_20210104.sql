SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Operator_BUP_20210104] (
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Id]                   [int] NOT NULL,
		[Name]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Operator_BUP_20210104] SET (LOCK_ESCALATION = TABLE)
GO
