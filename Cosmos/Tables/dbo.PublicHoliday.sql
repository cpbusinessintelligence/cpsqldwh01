SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PublicHoliday] (
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[HolidayDate]          [date] NOT NULL,
		[HolidayDesc]          [varchar](200) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]             [bit] NOT NULL,
		[IsDeleted]            [bit] NOT NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		CONSTRAINT [PK_PublicHoliday]
		PRIMARY KEY
		CLUSTERED
		([Branch], [HolidayDate])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[PublicHoliday]
	ADD
	CONSTRAINT [DF_PublicHoliday_IsActive]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PublicHoliday]
	ADD
	CONSTRAINT [DF_PublicHoliday_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[PublicHoliday]
	ADD
	CONSTRAINT [DF_PublicHoliday_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[PublicHoliday]
	ADD
	CONSTRAINT [DF_PublicHoliday_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PublicHoliday] SET (LOCK_ESCALATION = TABLE)
GO
