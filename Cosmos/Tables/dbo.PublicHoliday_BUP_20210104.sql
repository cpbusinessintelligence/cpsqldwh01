SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PublicHoliday_BUP_20210104] (
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[HolidayDate]          [date] NOT NULL,
		[HolidayDesc]          [varchar](200) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]             [bit] NOT NULL,
		[IsDeleted]            [bit] NOT NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL
)
GO
ALTER TABLE [dbo].[PublicHoliday_BUP_20210104] SET (LOCK_ESCALATION = TABLE)
GO
