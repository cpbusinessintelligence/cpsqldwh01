SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Query] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Key]                  [int] NOT NULL,
		[Message]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[IsActive]             [bit] NOT NULL,
		CONSTRAINT [PK_Query]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Query]
	ADD
	CONSTRAINT [DF_Query_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Query]
	ADD
	CONSTRAINT [DF_Query_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Query]
	ADD
	CONSTRAINT [DF_Query_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Query]
	ADD
	CONSTRAINT [DF_Query_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Query]
	ADD
	CONSTRAINT [DF_Query_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[Query]
	ADD
	CONSTRAINT [DF_Query_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Query] SET (LOCK_ESCALATION = TABLE)
GO
