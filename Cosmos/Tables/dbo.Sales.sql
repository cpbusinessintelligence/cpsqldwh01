SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SalesId]              [int] NOT NULL,
		[Operator]             [int] NULL,
		[Date]                 [date] NULL,
		[TelephoneNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fax]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contact]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Company]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address1]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address2]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Heard]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Freight]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[When]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Use]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Weekly]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Comments1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Comments2]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]         [int] NULL,
		[SalesRepId]           [int] NULL,
		[Result]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Reason]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons1]             [int] NULL,
		[Coupons2]             [int] NULL,
		[Coupons3]             [int] NULL,
		[Coupons4]             [int] NULL,
		[SalesValue]           [money] NULL,
		[SComment1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SComment2]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Completed]            [bit] NULL,
		[TimeStamp]            [datetime] NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[IsActive]             [bit] NOT NULL,
		CONSTRAINT [PK_Sales]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Sales]
	ADD
	CONSTRAINT [DF_Sales_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Sales]
	ADD
	CONSTRAINT [DF_Sales_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Sales]
	ADD
	CONSTRAINT [DF_Sales_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Sales]
	ADD
	CONSTRAINT [DF_Sales_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Sales]
	ADD
	CONSTRAINT [DF_Sales_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[Sales]
	ADD
	CONSTRAINT [DF_Sales_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_Sales]
	ON [dbo].[Sales] ([Branch], [SalesId], [Date], [IsActive], [EffectiveDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sales] SET (LOCK_ESCALATION = TABLE)
GO
