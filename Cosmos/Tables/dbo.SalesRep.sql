SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesRep] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SalesRepId]           [int] NOT NULL,
		[Name]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Login]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address1]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address2]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address3]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Phone]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Mobile]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[IsActive]             [bit] NOT NULL,
		CONSTRAINT [PK_SalesRep]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SalesRep]
	ADD
	CONSTRAINT [DF_SalesRep_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SalesRep]
	ADD
	CONSTRAINT [DF_SalesRep_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SalesRep]
	ADD
	CONSTRAINT [DF_SalesRep_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[SalesRep]
	ADD
	CONSTRAINT [DF_SalesRep_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SalesRep]
	ADD
	CONSTRAINT [DF_SalesRep_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[SalesRep]
	ADD
	CONSTRAINT [DF_SalesRep_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SalesRep] SET (LOCK_ESCALATION = TABLE)
GO
