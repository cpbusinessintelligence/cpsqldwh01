SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales_BUP_20210104] (
		[Id]                   [uniqueidentifier] NOT NULL,
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SalesId]              [int] NOT NULL,
		[Operator]             [int] NULL,
		[Date]                 [date] NULL,
		[TelephoneNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fax]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contact]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Company]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address1]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address2]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Heard]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Freight]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[When]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Use]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Weekly]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Comments1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Comments2]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]         [int] NULL,
		[SalesRepId]           [int] NULL,
		[Result]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Reason]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons1]             [int] NULL,
		[Coupons2]             [int] NULL,
		[Coupons3]             [int] NULL,
		[Coupons4]             [int] NULL,
		[SalesValue]           [money] NULL,
		[SComment1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SComment2]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Completed]            [bit] NULL,
		[TimeStamp]            [datetime] NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[IsActive]             [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[Sales_BUP_20210104] SET (LOCK_ESCALATION = TABLE)
GO
