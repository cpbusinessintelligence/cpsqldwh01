SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatZone] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[StatZoneId]           [int] NOT NULL,
		[Name]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		[IsActive]             [bit] NOT NULL,
		CONSTRAINT [PK_StatZone]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[StatZone]
	ADD
	CONSTRAINT [DF_StatZone_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[StatZone]
	ADD
	CONSTRAINT [DF_StatZone_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[StatZone]
	ADD
	CONSTRAINT [DF_StatZone_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[StatZone]
	ADD
	CONSTRAINT [DF_StatZone_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[StatZone]
	ADD
	CONSTRAINT [DF_StatZone_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[StatZone]
	ADD
	CONSTRAINT [DF_StatZone_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[StatZone] SET (LOCK_ESCALATION = TABLE)
GO
