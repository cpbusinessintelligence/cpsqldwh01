SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Suburb] (
		[Branch]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Code]                 [int] NOT NULL,
		[Abbr]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Zone]                 [int] NULL,
		[Name]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DepotNumber]          [int] NULL,
		[PostCode]             [int] NOT NULL,
		[Uses]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Driver]               [int] NULL,
		[Cut]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EtaRangeFrom0]        [time](7) NULL,
		[EtaRangeTo0]          [time](7) NULL,
		[EtaRangeFrom1]        [time](7) NULL,
		[EtaRangeTo1]          [time](7) NULL,
		[EtaRangeFrom2]        [time](7) NULL,
		[EtaRangeTo2]          [time](7) NULL,
		[EtaRangeFrom3]        [time](7) NULL,
		[EtaRangeTo3]          [time](7) NULL,
		[MapX]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MapY]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]            [bit] NULL,
		[LastModifiedDate]     [datetime] NULL,
		[CreatedDate]          [datetime] NULL,
		[EffectiveDate]        [datetime] NOT NULL,
		CONSTRAINT [PK_Suburb]
		PRIMARY KEY
		CLUSTERED
		([Branch], [Code], [PostCode], [EffectiveDate])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Suburb] SET (LOCK_ESCALATION = TABLE)
GO
