SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_Customer] (
		[Branch]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustomerId]                [int] NULL,
		[Code]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Name]                      [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryAddress1]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryAddress2]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryAddress3]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryInstrustions1]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryInstrustions2]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliverySuburb]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostcode]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburbid]          [int] NOT NULL,
		[InvoiceAddress1]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[InvoiceAddress2]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[InvoiceAddress3]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[InvoiceSuburb]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[InvoicePostcode]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceSuburbid]           [int] NOT NULL,
		[Contact1]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Contact2]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Contact3]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[EmailAddress1]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[EmailAddress2]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[EmailAddress3]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[Phone1]                    [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Phone2]                    [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Phone3]                    [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fax]                       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Lastused1]                 [date] NOT NULL
)
GO
ALTER TABLE [dbo].[Temp_Customer] SET (LOCK_ESCALATION = TABLE)
GO
