SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[debext_Archive2017] (
		[ProntoCode]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Column 5]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Column 6]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Column 7]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Column 8]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[PhoneNumber]     [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FAx]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 11]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 12]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 13]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 14]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 15]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 16]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 17]       [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[debext_Archive2017] SET (LOCK_ESCALATION = TABLE)
GO
