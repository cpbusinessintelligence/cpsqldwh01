SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddress] (
		[AddressID]            [int] IDENTITY(1, 1) NOT NULL,
		[AddressTypeID]        [int] NOT NULL,
		[PrepaidAccountID]     [int] NULL,
		[UserID]               [int] NULL,
		[FirstName]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LastName]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CompanyName]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Address1]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[Address2]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address3]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                [int] NOT NULL,
		[Postcode]             [varchar](4) COLLATE Latin1_General_CI_AS NOT NULL,
		[Phone]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Mobile]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[InActiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetimeoffset](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblAddress]
		PRIMARY KEY
		CLUSTERED
		([AddressID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblAddress]
	WITH CHECK
	ADD CONSTRAINT [tblAddressType_tblAddress]
	FOREIGN KEY ([AddressTypeID]) REFERENCES [dbo].[tblAddressType] ([AddressTypeID])
ALTER TABLE [dbo].[tblAddress]
	CHECK CONSTRAINT [tblAddressType_tblAddress]

GO
ALTER TABLE [dbo].[tblAddress] SET (LOCK_ESCALATION = TABLE)
GO
