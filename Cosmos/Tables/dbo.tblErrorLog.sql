SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrorLog] (
		[Id]               [int] IDENTITY(1, 1) NOT NULL,
		[Error]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[FunctionInfo]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ClientId]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreateDate]       [datetime] NULL,
		[Request]          [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[URL]              [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblErrorLog]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblErrorLog]
	ADD
	CONSTRAINT [DF_tblErrorLog_CreateDate]
	DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tblErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
