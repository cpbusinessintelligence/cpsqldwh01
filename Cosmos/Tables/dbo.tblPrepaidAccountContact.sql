SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPrepaidAccountContact] (
		[ContactID]            [int] IDENTITY(1, 1) NOT NULL,
		[PrepaidAccountID]     [int] NOT NULL,
		[ContactType]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FirstName]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LastName]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Mobile]               [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Phone]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Email]                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Comment]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[InActiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetimeoffset](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblPrepaidAccountContact]
		PRIMARY KEY
		CLUSTERED
		([ContactID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblPrepaidAccountContact]
	WITH NOCHECK
	ADD CONSTRAINT [tblPrepaidAccount_tblPrepaidAccountContact]
	FOREIGN KEY ([PrepaidAccountID]) REFERENCES [dbo].[tblPrepaidAccount] ([PrepaidAccountID])
ALTER TABLE [dbo].[tblPrepaidAccountContact]
	CHECK CONSTRAINT [tblPrepaidAccount_tblPrepaidAccountContact]

GO
ALTER TABLE [dbo].[tblPrepaidAccountContact] SET (LOCK_ESCALATION = TABLE)
GO
