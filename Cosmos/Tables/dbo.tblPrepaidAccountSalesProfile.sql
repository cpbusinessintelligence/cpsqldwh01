SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblPrepaidAccountSalesProfile] (
		[tblPrepaidAccountSalesProfileID]     [int] IDENTITY(1, 1) NOT NULL,
		[PrepaidAccountID]                    [int] NOT NULL,
		[InventoryID]                         [int] NOT NULL,
		[IsDiscountApplicable]                [bit] NULL,
		[DiscountPercentage]                  [decimal](4, 2) NULL,
		[DiscountStartDate]                   [datetime] NULL,
		[DiscountEndDate]                     [datetime] NULL,
		[SalesPrice]                          [decimal](18, 4) NOT NULL,
		[IsActive]                            [bit] NULL,
		[InActiveDateTime]                    [datetime] NULL,
		[UTCDateTime]                         [datetimeoffset](7) NOT NULL,
		[LocalDateTime]                       [datetime] NOT NULL,
		[CreatedDateTime]                     [datetime] NOT NULL,
		[CreatedBy]                           [int] NOT NULL,
		[UpdatedDateTime]                     [datetime] NULL,
		[UpdatedBy]                           [int] NULL,
		CONSTRAINT [PK_tblPrepaidAccountSalesProfile]
		PRIMARY KEY
		CLUSTERED
		([tblPrepaidAccountSalesProfileID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblPrepaidAccountSalesProfile]
	WITH NOCHECK
	ADD CONSTRAINT [tblPrepaidAccount_tblPrepaidAccountSalesProfile]
	FOREIGN KEY ([PrepaidAccountID]) REFERENCES [dbo].[tblPrepaidAccount] ([PrepaidAccountID])
ALTER TABLE [dbo].[tblPrepaidAccountSalesProfile]
	CHECK CONSTRAINT [tblPrepaidAccount_tblPrepaidAccountSalesProfile]

GO
ALTER TABLE [dbo].[tblPrepaidAccountSalesProfile] SET (LOCK_ESCALATION = TABLE)
GO
