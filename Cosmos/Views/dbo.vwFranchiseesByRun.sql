SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwFranchiseesByRun]
AS
SELECT     f.FranchiseeId,d.ArchiveDate, 
           f.ProntoId AS FranchiseeCode, 
           f.FranchiseeName, 
           ISNULL(d.CompanyName, '') AS CompanyName, 
           d.Branch, 
           CASE ISNULL(d .Branch, '') WHEN 'goldcoast' THEN 'Gold Coast' ELSE ISNULL(d .Branch, '') END AS BranchName,
           CASE WHEN ((d .ArchiveDate IS NOT NULL) OR (d .DriverNumber BETWEEN 8000 AND 8999)) THEN ISNULL(d .MobileId, CONVERT(varchar(30), d .DriverNumber)) ELSE d .DriverNumber END AS RunNumber,
           D.Drivername, 
           CASE ISNULL(d.ContractorType,'') WHEN 'C' Then 'CONTRACTOR' WHEN 'M' THEN 'MANAGEMENT' WHEN 'V' THEN 'VACANT' WHEN 'S' THEN 'SECOND SCANNER' WHEN 'O' THEN 'OTHER' WHEN 'B' THEN 'BARCODE' WHEN 'D' THEN 'DEPOT SCANNER' ELSE '' END as ContractorType,
           CAST(d.StartDate AS DATE) AS StartDate,
           CAST(d.EndDate AS DATE) AS EndDate,
           CASE WHEN ((d .ArchiveDate IS NOT NULL) OR (d .DriverNumber BETWEEN 8000 AND 8999)) THEN CONVERT(varchar(30), d .DriverNumber) ELSE '' END AS ArchivedRunNumber, 
           CONVERT(bit, CASE WHEN ((d .ArchiveDate IS NOT NULL) OR(d .DriverNumber BETWEEN 8000 AND 8999)) THEN 1 ELSE 0 END) AS IsArchived
FROM         dbo.Franchisees AS f WITH (NOLOCK) LEFT OUTER JOIN
                      dbo.Driver AS d WITH (NOLOCK) ON f.ProntoId = d.ProntoId AND d.IsActive = 1 AND (LTRIM(RTRIM(ISNULL(d.Branch, ''))) = 'brisbane' AND 
                      LTRIM(RTRIM(ISNULL(d.ProntoId, ''))) LIKE 'B[0123456789]%' OR
                      LTRIM(RTRIM(ISNULL(d.Branch, ''))) = 'goldcoast' AND LTRIM(RTRIM(ISNULL(d.ProntoId, ''))) LIKE 'G[0123456789]%' OR
                      LTRIM(RTRIM(ISNULL(d.Branch, ''))) NOT IN ('brisbane', 'goldcoast'))
                      AND ISNULL(d.IsDeleted, 0) = 0
                     -- And d.ArchiveDate is null
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[24] 4[32] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "f"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 197
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 235
               Bottom = 114
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 3420
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 6660
         Alias = 2565
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwFranchiseesByRun', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'vwFranchiseesByRun', NULL, NULL
GO
