SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vwFranchiseesByRun_BUP_JP_20130618]
AS
SELECT     f.FranchiseeId,d.ArchiveDate, 
           f.ProntoId AS FranchiseeCode, 
           f.FranchiseeName, 
           ISNULL(d.CompanyName, '') AS CompanyName, 
           d.Branch, 
           CASE ISNULL(d .Branch, '') WHEN 'goldcoast' THEN 'Gold Coast' ELSE ISNULL(d .Branch, '') END AS BranchName,
           CASE WHEN ((d .ArchiveDate IS NOT NULL) OR (d .DriverNumber BETWEEN 8000 AND 8999)) THEN ISNULL(d .MobileId, CONVERT(varchar(30), d .DriverNumber)) ELSE d .DriverNumber END AS RunNumber, 
           CASE ISNULL(d.ContractorType,'') WHEN 'C' Then 'CONTRACTOR' WHEN 'M' THEN 'MANAGEMENT' WHEN 'V' THEN 'VACANT' WHEN 'S' THEN 'SECOND SCANNER' ELSE '' END as ContractorType,
           CAST(d.StartDate AS DATE) AS StartDate,
           CAST(d.EndDate AS DATE) AS EndDate,
           CASE WHEN ((d .ArchiveDate IS NOT NULL) OR (d .DriverNumber BETWEEN 8000 AND 8999)) THEN CONVERT(varchar(30), d .DriverNumber) ELSE '' END AS ArchivedRunNumber, 
           CONVERT(bit, CASE WHEN ((d .ArchiveDate IS NOT NULL) OR(d .DriverNumber BETWEEN 8000 AND 8999)) THEN 1 ELSE 0 END) AS IsArchived
FROM         dbo.Franchisees AS f WITH (NOLOCK) LEFT OUTER JOIN
                      dbo.Driver AS d WITH (NOLOCK) ON f.ProntoId = d.ProntoId AND d.IsActive = 1 AND (LTRIM(RTRIM(ISNULL(d.Branch, ''))) = 'brisbane' AND 
                      LTRIM(RTRIM(ISNULL(d.ProntoId, ''))) LIKE 'B[0123456789]%' OR
                      LTRIM(RTRIM(ISNULL(d.Branch, ''))) = 'goldcoast' AND LTRIM(RTRIM(ISNULL(d.ProntoId, ''))) LIKE 'G[0123456789]%' OR
                      LTRIM(RTRIM(ISNULL(d.Branch, ''))) NOT IN ('brisbane', 'goldcoast'))
                      AND ISNULL(d.IsDeleted, 0) = 0




GO
