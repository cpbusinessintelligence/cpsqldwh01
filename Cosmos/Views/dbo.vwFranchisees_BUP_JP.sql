SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[vwFranchisees_BUP_JP]
AS

with fr as
(
	select 
	f.FranchiseeId,
	f.ProntoId as [FranchiseeCode],
	f.FranchiseeName,
	f.EmailAddress,
	(
		select 
		top 1
		Id
		FROM Driver d1 (NOLOCK)
		WHERE d1.ProntoId = f.ProntoId
		and IsActive = 1
		AND
		(
			(LTRIM(RTRIM(ISNULL(d1.Branch, ''))) = 'brisbane' 
				AND LTRIM(RTRIM(ISNULL(d1.ProntoId, ''))) LIKE 'B[0123456789]%')
			OR
			(LTRIM(RTRIM(ISNULL(d1.Branch, ''))) = 'goldcoast' 
				AND LTRIM(RTRIM(ISNULL(d1.ProntoId, ''))) LIKE 'G[0123456789]%')
			OR
			(LTRIM(RTRIM(ISNULL(d1.Branch, ''))) NOT IN ('brisbane', 'goldcoast'))
		)
		AND ISNULL(IsDeleted, 0) = 0
		order by d1.EffectiveDate DESC
	) AS [DriverId]
	from Franchisees f (NOLOCK)
	WHERE f.ProntoId NOT IN
	(
		'A030','B004','M039','M029','M282','M001','S030','B099','G099'
	)
)
select
fr.FranchiseeId,
fr.FranchiseeCode,
fr.FranchiseeName,
ISNULL(fr.EmailAddress, '') AS [EmailAddress],
ISNULL(d.CompanyName, '') AS [CompanyName],
ISNULL(d.Address1, '') AS [Address1],
ISNULL(d.Address2, '') AS [Address2],
ISNULL(d.Address3, '') AS [Address3],
ISNULL(d.CompanyAddress1, '') AS [CompanyAddress1],
ISNULL(d.CompanyAddress2, '') AS [CompanyAddress2],
ISNULL(d.CompanyAddress3, '') AS [CompanyAddress3],
ISNULL(d.ABN, '') AS [ABN],
ISNULL(d.Branch, 'UNKNOWN') AS [FranchiseeBranch],
CASE WHEN ((d.ArchiveDate Is Not Null) OR (d.DriverNumber BETWEEN 8000 AND 8999))
	THEN 
		CASE WHEN ISNUMERIC(d.MobileId) = 1
			THEN CONVERT(int, d.MobileId)
			ELSE ISNULL(d.DriverNumber, 0)
		END
	ELSE 
		ISNULL(d.DriverNumber, 0)
END AS [RunNumber],
CASE WHEN ((d.ArchiveDate Is Not Null) OR (d.DriverNumber BETWEEN 8000 AND 8999))
	THEN 
		('ARCHIVED - Archived Run: ' + CONVERT(varchar(20), ISNULL(d.DriverNumber, 0)))
	ELSE 
		ISNULL(d.Comments1, '')
END AS [Comments],
CASE WHEN ((d.ArchiveDate Is Not Null) OR (d.DriverNumber BETWEEN 8000 AND 8999))
	THEN 1
	ELSE 0
END AS [IsArchived],
ISNULL(d.PhoneNumber, '') AS [PhoneNumber],
ISNULL(d.MobileNumber, '') AS [MobileNumber],
ISNULL(d.FaxNumber, '') AS [FaxNumber]
from fr 
left outer join Driver d (NOLOCK)
ON fr.DriverId = d.Id 





GO
