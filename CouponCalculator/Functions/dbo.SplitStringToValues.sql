SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 CREATE FUNCTION [dbo].[SplitStringToValues]   (@valuein nvarchar(max), @delimiter char(1)= '_')
RETURNS @dataout TABLE 
(
    data nvarchar(100)
)
AS
BEGIN
	DECLARE @chrind INT
	DECLARE @Piece nvarchar(100)
	
	SELECT @chrind = 1 
	WHILE @chrind > 0
	BEGIN
		SELECT @chrind = CHARINDEX(@delimiter,@valuein)
		
		IF @chrind  > 0
			SELECT @Piece = LEFT(@valuein,@chrind - 1)
		ELSE
			SELECT @Piece = @valuein
			
		INSERT  @dataout VALUES(CAST(@Piece AS VARCHAR))
		
		SELECT @valuein = RIGHT(@valuein,LEN(@valuein) - @chrind)
		
		IF LEN(@valuein) = 0 
			BREAK
	END
RETURN
END
GO
