SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

     --'=====================================================================
    --' CP -Stored Procedure -[fnCalculateCouponsfromPostCode]
    --' ---------------------------
    --' Purpose: To Calculate Coupons From Postcodes]-----
    --' Developer: JP (Couriers Please Pty Ltd)
    --' Date: 04/09/2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 04/09/2014    JP     1.00                                                      --JP20140904

    --'=====================================================================

CREATE function [dbo].[fnCalculateCouponsfromPostCode] (@FromPostCode varchar(20),
                                           @FromSuburb varchar(50),
										   @ToPostCode varchar(20),
										   @ToSuburb varchar(20),
										   @DeclaredWeight varchar(20),
										   @VolumeWeight varchar(20),
										   @SatchelCount Int)
       returns varchar(50)
as
begin
    Declare @WeightForCalculation as decimal(12,2)  =Convert(decimal(12,2),CASE WHEN CONVERT(Decimal(12,2),@DeclaredWeight) > CONVERT(Decimal(12,2),@VolumeWeight) THEN @DeclaredWeight ELSE @VolumeWeight END )
	Declare @WeightBreak as Varchar(20) = CASE WHEN @SatchelCount = 1 THEN 'SAT' ELSE (CASE WHEN @WeightForCalculation between 0 and 15 THEN 'WB1'
											   WHEN @WeightForCalculation between 16 and 25 THEN 'WB2'
											   WHEN @WeightForCalculation between 26 and 40 THEN 'WB3'
											   ELSE 'XXX'
											   END) END

	Declare @sParmValue as varchar(20)
	SElect @sParmValue =  Isnull(CASE @WeightBreak WHEN 'WB1' THEN (Select [WB1Count] from CouponCalculator.[dbo].[CouponCalculator] Where FromZOne =  CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@FromPostCode,@FromSuburb) and Tozone = CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@ToPostCode,@ToSuburb))
	                                        WHEN 'WB2' THEN (Select [WB2Count] from CouponCalculator.[dbo].[CouponCalculator] Where FromZOne =  CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@FromPostCode,@FromSuburb) and Tozone = CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@ToPostCode,@ToSuburb))
											WHEN 'WB3' THEN (Select [WB3Count] from CouponCalculator.[dbo].[CouponCalculator] Where FromZOne =  CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@FromPostCode,@FromSuburb) and Tozone = CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@ToPostCode,@ToSuburb))
											WHEN 'SAT' THEN (Select SatchelCount from CouponCalculator.[dbo].[CouponCalculator] Where FromZOne =  CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@FromPostCode,@FromSuburb) and Tozone = CouponCalculator.[dbo].[fnGetPrepaidCouponZone](@ToPostCode,@ToSuburb))
											ELSE '' END,'')

		
	Return @sParmValue
end



GO
