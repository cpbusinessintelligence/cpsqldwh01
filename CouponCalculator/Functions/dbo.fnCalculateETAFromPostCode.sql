SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

     --'=====================================================================
    --' CP -Stored Procedure -[fnCalculateETAFromPostCode]
    --' ---------------------------
    --' Purpose: To Calculate ETA From Postcodes]-----
    --' Developer: JP (Couriers Please Pty Ltd)
    --' Date: 04/09/2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 04/09/2014    JP     1.00                                                      --JP20140904

    --'=====================================================================

CREATE function [dbo].[fnCalculateETAFromPostCode] (@FromPostCode varchar(20),
                                       @FromSuburb varchar(50),
									   @ToPostCode varchar(20),
									   @ToSuburb varchar(20))
       returns varchar(500)
as
begin
	Declare @sParmValue varchar(800)

	Select @sParmValue = ISNULL((Select ETA from CouponCalculator.dbo.ETACalculator  Where FromZOne = CouponCalculator.[dbo].[fnGetETAZone](@FromPostCode,@FromSuburb) and [ToZone] = CouponCalculator.[dbo].[fnGetETAZone](@ToPostCode,@ToSuburb)),'')
	
	
	Return @sParmValue
end


GO
