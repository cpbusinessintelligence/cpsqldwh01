SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

     --'=====================================================================
    --' CP -Stored Procedure -[fnCalculateETAFromZone]
    --' ---------------------------
    --' Purpose: To Calculate ETA from Zone]-----
    --' Developer: JP (Couriers Please Pty Ltd)
    --' Date: 04/09/2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 04/09/2014    JP     1.00                                                      --JP20140904

    --'=====================================================================

CREATE function [dbo].[fnCalculateETAFromZone] (@FromZone varchar(20),                                    
											    @ToZone varchar(20))
       returns varchar(50)
as
begin
	Declare @sParmValue varchar(80)

	Select @sParmValue = ISNULL((Select ETA from CouponCalculator.dbo.ETACalculator  Where FromZOne = Rtrim(Ltrim(@FromZone)) and [ToZone] = Rtrim(Ltrim(@ToZone))),'')
	
	
	Return @sParmValue
end


GO
