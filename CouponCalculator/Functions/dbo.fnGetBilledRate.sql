SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[fnGetBilledRate] (@InputWeight float,@WeightBracket1 float,@WeightBracket2 float,@BaseRate float,@ChargePerWb1 float,@ChargePerwb2 float,@MinimumRate float) returns float  as
begin


	declare @ct int =1
	declare @Ouputcharge float

	If @WeightBracket2=0
	begin
	  If @InputWeight<=@WeightBracket1 
          set @Ouputcharge= case when (@BaseRate+@WeightBracket1*@ChargePerWb1)>@MinimumRate then (@BaseRate+@WeightBracket1*@ChargePerWb1) else @MinimumRate end 
	else
	begin
	  while @WeightBracket1*@ct<(@InputWeight-@WeightBracket1)
	  begin
	    set @ct=@ct+1;
      end
	  set @ct=@ct+1
	  set @Ouputcharge= case when (@BaseRate+(@WeightBracket1*@ct)*@ChargePerWb1)>@MinimumRate then   (@BaseRate+(@WeightBracket1*@ct)*@ChargePerWb1)    else @MinimumRate end
	end
	end
  Else If @WeightBracket2<>0
	begin
	If @InputWeight<=@WeightBracket1
	   set @Ouputcharge= case when (@BaseRate+@WeightBracket1*@ChargePerWb1) > @MinimumRate then  (@BaseRate+@WeightBracket1*@ChargePerWb1) else @MinimumRate end
	else
    begin
	   while @WeightBracket2*@ct<(@InputWeight-@WeightBracket1)
	   begin
          set @ct=@ct+1;
	   end
    set @Ouputcharge= case when (@BaseRate+((@WeightBracket1*@ChargePerWb1)+((@WeightBracket2*@ct)*@ChargePerWb2)))> @MinimumRate then (@BaseRate+((@WeightBracket1*@ChargePerWb1)+((@WeightBracket2*@ct)*@ChargePerWb2))) else @MinimumRate end
	end

  end
  return @Ouputcharge
end
GO
