SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
     --'=====================================================================
    --' CP -Stored Procedure -[fnGetRedemptionZone] 
    --' ---------------------------
    --' Purpose: To Calculate RedemptionZone]-----
    --' Developer: JP (Couriers Please Pty Ltd)
    --' Date: 04/09/2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 04/09/2014    JP     1.00                                                      --JP20140904

    --'=====================================================================


CREATE FUNCTION [dbo].[fnGetRedemptionZone](@PostCode varchar(20) ,@Suburb varchar(20))
returns varchar(800)
as
begin
Return ISNULL((Select [RedemptionZone]  FROM CouponCalculator.dbo.Postcodes 
                            Where Postcode =  CASE LEn(Rtrim(Ltrim(@PostCode))) WHEN 3 THEN '0'+Rtrim(Ltrim(@PostCode)) ELSE  Rtrim(Ltrim(@PostCode)) END 
								  and Suburb = Rtrim(Ltrim(@Suburb))),'')
end


GO
