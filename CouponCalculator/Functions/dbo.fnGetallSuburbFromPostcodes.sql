SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

     --'=====================================================================
    --' CP -Stored Procedure -[fnGetallSuburbFromPostcodes] 
    --' ---------------------------
    --' Purpose: To Calculate Suburb From Postcodes]-----
    --' Developer: JP (Couriers Please Pty Ltd)
    --' Date: 04/09/2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 04/09/2014    JP     1.00                                                      --JP20140904

    --'=====================================================================

CREATE FUNCTION [dbo].[fnGetallSuburbFromPostcodes](@PostCode varchar(20) )
RETURNS TABLE
 AS
 RETURN

 --(Select 'XXXXX' as Suburb)

(Select TOP 100 Suburb  FROM CouponCalculator.dbo.Postcodes 
Where Postcode =  CASE LEn(Rtrim(Ltrim(@PostCode))) WHEN 3 THEN '0'+Rtrim(Ltrim(@PostCode)) ELSE  Rtrim(Ltrim(@PostCode)) END 
order by suburb  )

GO
