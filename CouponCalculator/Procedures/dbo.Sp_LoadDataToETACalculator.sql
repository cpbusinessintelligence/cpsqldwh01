SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Sp_LoadDataToETACalculator 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
truncate table dbo.[ETACalculator]

INSERT INTO [dbo].[ETACalculator]
           ([FromZone]
           ,[ToZone]
           ,[PrimaryNetworkCategory]
           ,[SecondaryNetworkCategory]

           ,[ETA]
           ,[FromETA]
           ,[ToETA]
           ,[1stPickupCutOffTime]
           ,[1stDeliveryCutOffTime]
           ,[1stAllowedDays]
           ,[2ndPickupCutOffTime]
           ,[2ndDeliveryCutOffTime]
           ,[2ndAllowedDays]

           ,[AIR_ETA]
           ,[AIR_FromETA]
           ,[AIR_ToETA]
           ,[AIR_1stDeliveryCutOffTime]
           ,[AIR_1stAllowedDays]
           ,[AIR_2ndDeliveryCutOffTime]
           ,[AIR_2ndAllowedDays]

           ,[SameDay_ETA]
           ,[SameDay_FromETA]
           ,[SameDay_ToETA]
           ,[SameDay_1stDeliveryCutOffTime]
           ,[SameDay_1stAllowedDays]
           ,[SameDay_2ndDeliveryCutOffTime]
           ,[SameDay_2ndAllowedDays]

           ,[AddWho]
           ,[AddDateTime]
           ,[EditWho]
           ,[EditDateTime]

           ,[Domestic_OffPeak_ETA]
           ,[Domestic_OffPeak_FromETA]
           ,[Domestic_OffPeak_ToETA]
           ,[Domestic_OffPeak_1stDeliveryCutOff]
           ,[Domestic_OffPeak_1stAllowedDays]
           ,[Domestic_OffPeak_2ndDeliveryCutOff]
           ,[Domestic_OffPeak_2ndAllowedDays])
    /****** Script for SelectTopNRows command from SSMS  ******/
SELECT [FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]

      ,[ETA]
      ,[FromETA]
      ,[ToETA]
      ,[1stPickupCutOffTime]
      ,[1stDeliveryCutOffTime]
      ,[1stAllowedDays]
      ,[2ndPickupCutOffTime]
      ,[2ndDeliveryCutOffTime]
      ,[2ndAllowedDays]

      ,[Domestic_Priority_ETA]
      ,[Domestic_Priority_FromETA]
      ,[Domestic_Priority_ToETA]
      ,[Domestic_Priority_1stDeliveryCutOff]
      ,[Domestic_Priority_1stAllowedDays]
      ,[Domestic_Priority_2ndDeliveryCutOff]
      ,[Domestic_Priority_2ndAllowedDays]

      ,[SameDay_ETA]
      ,[SameDay_FromETA]
      ,[SameDay_ToETA]
      ,[SameDay_1stDeliveryCutOffTime]
      ,[SameDay_1stAllowedDays]
      ,[SameDay_2ndDeliveryCutOffTime]
      ,[SameDay_2ndAllowedDays]

         ,'SG',Getdate(), 'SG',Getdate()


      ,[Domestic_OffPeak_ETA]
      ,[Domestic_OffPeak_FromETA]
      ,[Domestic_OffPeak_ToETA]
      ,[Domestic_OffPeak_1stDeliveryCutOff]
      ,[Domestic_OffPeak_1stAllowedDays]
      ,[Domestic_OffPeak_2ndDeliveryCutOff]
      ,[Domestic_OffPeak_2ndAllowedDays]

  FROM [CouponCalculator].[dbo].[Network_ETACalculator]

END
GO
