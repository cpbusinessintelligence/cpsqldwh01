SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Z_sp_RptRateCardDetailsByZone](@AccountNumber varchar(50)) as 

begin


  --  Select case when isnull(Account,'')='' then 'Card' else Account end as Account,convert(varchar(100),'') as Accountname,Service,p1.pc_name as PriceCodeName,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%'  then 'Parcel/Satchel' else 'Blocks' end as Category, convert(date,max(convert(date,[Effective Date]))) as [Effective Date] 
  --into #temp 
  --from [CouponCalculator].[dbo].finalratesv1 
  --                                          left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=service
  --where case when isnull(Account,'')='' then 'Card' else Account end=case when @AccountNumber='' then 'CARD' else @AccountNumber end 
  --group by case when isnull(Account,'')='' then 'Card' else Account end,Service,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end

  Select case when isnull(Account,'')='' 
       then 'Card' else Account 
	   end as Account,
	   convert(varchar(100),'') as Accountname,
	   CP.cp_pricecode as RateCard,
	   p1.pc_name as PriceCodeName,
	   case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' 
	   then 'Parcel/Satchel'  else 'Blocks' end as Category,
	   convert(date,max(convert(date,[Effective Date]))) as [Effective Date] 
  into #temp 
  from [CouponCalculator].[dbo].finalratesv1 
  left join [cpsqldwh01].cpplEDI.dbo.companyaccount AS CA 
  ON Account=CA.ca_account
  LEFT OUTER JOIN
            [cpsqldwh01].cpplEDI.dbo.companies AS C ON CA.ca_company_id = C.c_id INNER JOIN
            [cpsqldwh01].cpplEDI.dbo.companypricecode AS CP ON C.c_id = CP.cp_company_id
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=CP.cp_pricecode
   where case when isnull(Account,'')='' then 'Card' else Account end=case when @AccountNumber='' then 'CARD' else @AccountNumber end 
		and CP.cp_pricecode not like 'EXP%' and CP.cp_pricecode not like 'SAV%'and CP.cp_pricecode not like 'IMP%'
  group by case when isnull(Account,'')='' then 'Card' else Account end,CP.cp_pricecode,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end

  update #temp set Accountname=p.shortname from Pronto.dbo.ProntoDebtor p where account=billto

  Select case when isnull(r.Account,'')='' then 'Card' else r.Account end as Account
      ,t.RateCard
	  ,t.Category
      ,convert(date,r.[Effective Date]) as [Effective Date]
	  ,[Origin Zone]
      --,Z.[ZoneDescription] as [Origin Zone]
      ,Z.[ZoneDescription] as [DestinationZoneDesc]
	  ,[Destination Zone]
      ,[Minimum Charge]
      ,[Basic Charge]
      ,[Fuel Override]
      ,[Fuel Percentage]
     -- ,[Rounding 1] as WeightBreak
	  ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.0000 end as ChargePerKilo
	  ,case when convert(float,[Rounding 1])=25.00 then 25.0000*convert(float,[Charge 1]) else 0.0000 end as ChargePer25Kilo
	  ,convert(int,100) as Showall
	  ,convert(int,0) as SortOrder
	  ,convert(int,0) as cnt
into #temp1	  
from [CouponCalculator].[dbo].finalratesv1 r 
join #temp t on t.Account=r.Account and case when t.Account='CARD' then '' else t.Account end=r.Account and convert(date,t.[Effective Date])=convert(date,r.[Effective Date])
join [dbo].[ZoneDetails] Z on [Destination Zone]= [ZoneCode]
where   isnull(r.Account,'') =@AccountNumber and t.RateCard NOT LIKE 'P%'
--and r.Service=@Service and r.[Origin Zone]=@OriginZone


 update #temp1 set Showall =case when ordering=0 then 1 else 0 end,SortOrder=Ordering from cpsqlrct01.OperationalReporting.[dbo].[Zones_Pronto_Mapping] where [Destination Zone]=zone
 update #temp1 set cnt =(select count(*) from #temp1)
  select
            ROW_NUMBER() over (order by t.cnt desc, t.Account, t.RateCard) as RowId, t.*
        from (select * from #Temp1) as t
		where showall=0 order by RowId
 --select * from #temp1 where showall=0 order by SortOrder

END




GO
GRANT EXECUTE
	ON [dbo].[Z_sp_RptRateCardDetailsByZone]
	TO [ReportUser]
GO
