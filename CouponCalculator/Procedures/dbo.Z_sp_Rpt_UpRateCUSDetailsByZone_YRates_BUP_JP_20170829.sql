SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[Z_sp_Rpt_UpRateCUSDetailsByZone_YRates_BUP_JP_20170829](@AccountNumber varchar(50)) as 

BEGIN



SELECT  distinct U.[Accountcode] as Account
      ,U.[BillTo]
      ,Isnull(p.shortname,'') as Accountname
      ,Case When U.[OldServiceCode]=U.[ServiceCode]
	        THEN U.[OldServiceCode]
			ELSE U.[ServiceCode] 
			END as ServiceCode
	  ,p1.pc_name as PriceCodeName
	  ,U.[RateCardDescription] as RateCardDescription
	  ,case when p1.pc_name like '%Parcel%' 
	          or p1.pc_name like '%Satchel%' 
			  or p1.pc_name like '%item%' 
	          then 'Parcel/Satchel'  
	   else 'Blocks' end as Category
	  ,[RateCardCategory]
	  ,'11 September, 2017' as [Effective Date]
  Into #Temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.ServiceCode
  left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.BillTo
  where U.AccountCode =@AccountNumber


  
  select * into #Temp1 from (Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	     ,convert(float,[Minimum Charge]) as [Minimum Charge]
         ,convert(float,[Basic Charge]) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	    ,convert(float,[Charge 1]) as ChargePerKilo
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where LTRIM(RTRIM(t.[RateCardDescription])) like 'Standard%' and r.ServiceCode like 'Y%'
UNION
  Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	     ,convert(float,[Minimum Charge]) as [Minimum Charge]
         ,convert(float,[Basic Charge]) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     , convert(float,[Charge 1]) as ChargePerKilo 
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on t.Account=r.AccountCode and t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where r.AccountCode=@AccountNumber and LTRIM(RTRIM(t.[RateCardDescription])) like 'Customised%' and r.ServiceCode like 'Y%') T


SELECT
    t.*,
    Rowindex = ROW_NUMBER() OVER(PARTITION BY [Origin Zone] ORDER BY [Origin Zone]),
    RowNumber = DENSE_RANK() OVER (ORDER BY [Origin Zone])
FROM (select * from #Temp1) as t Order by t.ServiceCode asc


END
GO
