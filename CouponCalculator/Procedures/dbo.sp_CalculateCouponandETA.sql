SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


     --'=====================================================================
    --' CP -Stored Procedure -[sp_CalculateCouponandETA] 
    --' ---------------------------
    --' Purpose: To Calculate COupon and ETA Data-----
    --' Developer: JP (Couriers Please Pty Ltd)
    --' Date: 31 Mar 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 31/03/2014    JP     1.00                                                      --JP20140331

    --'=====================================================================


CREATE PROCEDURE [dbo].[sp_CalculateCouponandETA] 
                                          (@FromPostCode varchar(20),
                                           @FromSuburb varchar(50),
										   @ToPostCode varchar(20),
										   @ToSuburb varchar(20),
										   @DeclaredWeight varchar(20),
										   @VolumeWeight varchar(20),
										   @SatchelCount Int)
	AS
BEGIN

  Declare @WeightForCalculation as decimal(12,2)  =Convert(decimal(12,2),CASE WHEN CONVERT(Decimal(12,2),@DeclaredWeight) > CONVERT(Decimal(12,2),@VolumeWeight) THEN @DeclaredWeight ELSE @VolumeWeight END )
   Declare @WeightBreak as Varchar(20) = CASE WHEN @SatchelCount = 1 THEN 'SAT' ELSE (CASE WHEN @WeightForCalculation >= 0 and @WeightForCalculation <=15.99 THEN 'WB1'
											   WHEN @WeightForCalculation >= 16.00 and @WeightForCalculation <= 25.99 THEN 'WB2'
											   WHEN @WeightForCalculation >= 26.00 and  @WeightForCalculation <= 40.00 THEN 'WB3'
											   ELSE 'XXX'
											   END) END


Select Top 1 P.ETAZone as FromETAZone  ,P.PrizeZone as FromPrizeZone  into #TempFrom from [dbo].[PostCodes] P Join dbo.ETACalculator E On P.ETAZone = E.FromZone			                              
																       where P.Postcode = Rtrim(Ltrim(@FromPostCode)) and P.Suburb = Rtrim(Ltrim(@FromSuburb))


Select Top 1 P.ETAZone as ToETAZone  ,P.PrizeZone as ToPrizeZone  into #TempTo from [dbo].[PostCodes] P Join dbo.ETACalculator E On P.ETAZone = E.FromZone			                              
																       where P.Postcode = Rtrim(Ltrim(@ToPostCode)) and P.Suburb = Rtrim(Ltrim(@ToSuburb))

Select @FromPostCode as FromPostCode ,
       @FromSuburb as FromSuburb ,
	   (Select FromETAZone From #TempFrom) as FFromETAZone,
	   (Select FromPrizeZone From #TempFrom) as FromPrizeZone,
	   @ToPostCode as ToPostCode ,
	   @ToSuburb as ToSuburb ,
	   (Select ToETAZone From #TempTo) as ToETAZone,
	   (Select ToPrizeZone From #TempTo) as ToPrizeZone,
	   [dbo].[fnCalculateETAFromZone]((Select FromETAZone From #TempFrom),(Select ToETAZone From #TempTo)) + ' Days' As ETA,
	   1 as MetroCoupons,
	   [dbo].[fnCalculateCouponsFromZone]((Select FromPrizeZone From #TempFrom),(Select ToPrizeZone From #TempTo),@DeclaredWeight,@VolumeWeight,@SatchelCount) As Links,
	   @WeightForCalculation as Weight,
	   @WeightBreak as WeightBracket

END


GO
