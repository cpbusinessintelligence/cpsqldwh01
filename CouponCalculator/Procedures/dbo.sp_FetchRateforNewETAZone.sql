SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_FetchRateforNewETAZone](@NewZone varchar(30),@Zonetocopyfrom varchar(30)) as
begin


Select Account,Service,[origin zone],[destination zone],Max(convert(datetime,[Effective Date]) ) as effectivedate
--,count(*) as ct
into #temp
from [dbo].[Rates mar] 
where [destination zone]=@Zonetocopyfrom
group by Account,Service,[origin zone],[destination zone]

--Select * from #temp where account='111222402'

-- Select * from rates where service='CE3' and Account='' and [destination zone]='DRW'

 SELECT r. [Account]
      ,r.[Service]
      ,convert(datetime,t.EffectiveDate) as [Effective Date]
      ,[Tariff ID]
	  , r. [Origin Zone]
	--,[Destination Zone]
      , @NewZone as [Destination Zone]
      ,[Minimum Charge]
      ,[Basic Charge]
      ,[Fuel Override]
      ,[Fuel Percentage]
      ,[Rounding 1]
      ,[Rounding 2]
      ,[Rounding 3]
      ,[Rounding 4]
      ,[Rounding 5]
      ,[Rounding 6]
      ,[Rounding 7]
      ,[Rounding 8]
      ,[Rounding 9]
      ,[Rounding 10]
      ,[Charge 1]
      ,[Charge 2]
      ,[Charge 3]
      ,[Charge 4]
      ,[Charge 5]
      ,[Charge 6]
      ,[Charge 7]
      ,[Charge 8]
      ,[Charge 9]
      ,[Charge 10]
      ,[Break 1]
      ,[Break 2]
      ,[Break 3]
      ,[Break 4]
      ,[Break 5]
      ,[Break 6]
      ,[Break 7]
      ,[Break 8]
      ,[Break 9]
      ,[Break 10]
 FROM #temp t join [dbo].[Rates mar] r on r.account=t.account and r.service=t.service and r.[destination zone]=t.[destination zone] and r.[origin zone]=t.[origin zone] and  r.[effective date]=t.effectivedate
 order by Account,Service,[Origin Zone]
  --where [destination zone]='ADL' and service='X35'
  --where r.[destination zone]=@Zonetocopyfrom 


  end

GO
