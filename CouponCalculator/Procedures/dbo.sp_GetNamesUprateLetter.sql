SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc sp_GetNamesUprateLetter
(
@CustomerName Varchar(500),
@Designation Varchar(500),
@ManagerName Varchar(500),
@ManagerEmailorMobile Varchar(500),
@State Varchar(50)
)

as
begin

Insert into dbo.NamesUprateLetter(CustomerName,AccountManager,ManagerName,EmailorMobile,State)
Values(@CustomerName,@Designation,@ManagerName,@ManagerEmailorMobile,@State)
end
GO
