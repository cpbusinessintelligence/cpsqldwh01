SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc dbo.sp_NowGoReconcil_AH (@StartDate Varchar(20),@EndDate Varchar(20), @DriverID Varchar(250), @Showall Varchar (200)
)
As
Begin

Declare @stmt nVarchar(max)
--Declare @StartDate Varchar(20),@EndDate Varchar(20), @DriverID Varchar(250), @Showall Varchar (200)
--set @StartDate = '2019-05-27'
--set @EndDate ='2019-06-02'
--set @DriverID = 'ABC'
--set @Showall = 'Missed'
--Declare @stmt nVarchar(max)
if @Showall = 'Missed'
SELECT distinct N.[LabelNumber] as LabelNumber
      ,[DriverExtRef] As NowGoDriverID
      ,[DriverRunNumber] AS NowGoCosmosDriver
      ,[Branch] as NowGoCosmosBranch
       ,CASE [IsProcessed] when 1 THEN 'Yes' else 'No'  end as [IsProcessed]  
      ,Convert(Date,EventDateTime) as NowGoEvent
         ,Convert(Date,L.CreatedDate) AS CosmosCreated

  FROM [ScannerGateway].[dbo].[tbl_TrackingEventNowGo] N left Join ScannerGateway..Label L on N.LabelNumber= L.LabelNumber
  WHere Convert(Date,EventDateTime) between @StartDate and @EndDate and ([DriverExtRef] = @DriverID  or @DriverID is null) and
	L.LabelNumber is NULL 
  else
  SELECT distinct N.[LabelNumber] as LabelNumber
      ,[DriverExtRef] As NowGoDriverID
      ,[DriverRunNumber] AS NowGoCosmosDriver
      ,[Branch] as NowGoCosmosBranch
       ,CASE [IsProcessed] when 1 THEN 'Yes' else 'No'  end as [IsProcessed]  
      ,Convert(Date,EventDateTime) as NowGoEvent
         ,Convert(Date,L.CreatedDate) AS CosmosCreated

  FROM [ScannerGateway].[dbo].[tbl_TrackingEventNowGo] N left Join ScannerGateway..Label L on N.LabelNumber= L.LabelNumber
  WHere Convert(Date,EventDateTime) between @StartDate and @EndDate and ([DriverExtRef] = @DriverID  or @DriverID is null)


--set @stmt =
--'SELECT distinct N.[LabelNumber] as LabelNumber
--      ,[DriverExtRef] As NowGoDriverID
--      ,[DriverRunNumber] AS NowGoCosmosDriver
--      ,[Branch] as NowGoCosmosBranch
--       ,CASE [IsProcessed] when 1 THEN ''Yes'' else ''No''  end as [IsProcessed]  
--      ,Convert(Date,EventDateTime) as NowGoEvent
--         ,Convert(Date,L.CreatedDate) AS CosmosCreated
--  FROM [ScannerGateway].[dbo].[tbl_TrackingEventNowGo] N left Join ScannerGateway..Label L on N.LabelNumber= L.LabelNumber
--  WHere Convert(Date,EventDateTime) between convert(date, '''+@StartDate + ''')'
--  set @stmt = @stmt +' and  convert(date,''' +@EndDate+ ''') '
--set @stmt = @stmt +' and ([DriverExtRef] = ISNULL(' + @DriverID + ''')' 
-- --set @stmt = @stmt + ''' or [DriverExtRef] is null)'
----set @stmt = @stmt + @DriverID+''' is null)'
--   if  @Showall = 'Missed' set @stmt = @stmt+ ' and L.LabelNumber is NULL' 

--  Print(@stmt)
----Exec sp_executesql @stmt
  End
GO
