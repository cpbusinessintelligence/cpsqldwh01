SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RPTPrepaidCouponCalculatorByWeightBracket] 
    --' ---------------------------
    --' Purpose: RPT on Prepaid COupon Calculator-----
    --' Developer: JP (Couriers Please Pty Ltd)
    --' Date: 31 Mar 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 31/03/2014    JP     1.00                                                      --JP20140331

    --'=====================================================================


CREATE PROCEDURE [dbo].[sp_RPTPrepaidCouponCalculatorByWeightBracket] 
                                          (@WeightBracket varchar(20))
	AS
BEGIN
 IF @WeightBracket = 'WB1'
       SELECT[FromZone] ,[ToZone] ,[WB1Count] as Link FROM [CouponCalculator].[dbo].[CouponCalculator]-- Where [WB1Count] <> ''
 ELSE IF @WeightBracket = 'WB2'
       SELECT[FromZone] ,[ToZone] ,[WB2Count] as Link FROM [CouponCalculator].[dbo].[CouponCalculator]-- Where [WB2Count] <> ''
 ELSE IF @WeightBracket = 'WB3'
       SELECT[FromZone] ,[ToZone] ,[WB3Count] as Link FROM [CouponCalculator].[dbo].[CouponCalculator] --Where [WB3Count] <> ''
 ELSE IF @WeightBracket = 'SAT'
       SELECT[FromZone] ,[ToZone] ,SatchelCount as Link FROM [CouponCalculator].[dbo].[CouponCalculator] Where SatchelCount <> ''
 ELSE 
       SELECT ''  as [FromZone] ,'' as [ToZone] , ''  as Link
END


GO
