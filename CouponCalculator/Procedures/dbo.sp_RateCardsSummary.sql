SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tejes Singam
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RateCardsSummary](@AccountNumber varchar(50))
AS
BEGIN
Select case when isnull(Account,'')='' 
       then 'Card' else Account 
	   end as Account,
	   convert(varchar(100),'') as Accountname,
	   Service,
	   p1.pc_name as PriceCodeName,
	   case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' 
	   then 'Parcel/Satchel'  else 'Blocks' end as Category,
	   convert(date,max(convert(date,[Effective Date]))) as [Effective Date] 
  into #temp 
  from [CouponCalculator].[dbo].finalratesv1 
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=service
  --left join [cpsqldwh01].cpplEDI.dbo.companyaccount AS CA 
  --ON Account=CA.ca_account
  --LEFT OUTER JOIN
  --          [cpsqldwh01].cpplEDI.dbo.companies AS C ON CA.ca_company_id = C.c_id INNER JOIN
  --          [cpsqldwh01].cpplEDI.dbo.companypricecode AS CP ON C.c_id = CP.cp_company_id
    where case when isnull(Account,'')='' then 'Card' else Account end=case when @AccountNumber='' then 'CARD' else @AccountNumber end  and Service not like 'EXP%' and Service not like 'SAV%'and Service not like 'IMP%'
  group by case when isnull(Account,'')='' then 'Card' else Account end,Service,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end
  --from [CouponCalculator].[dbo].finalratesv1 
  --left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=service
  --where case when isnull(Account,'')='' then 'Card' 
  --      else Account end=case 
		--when @AccountNumber='' then 'CARD' else @AccountNumber end and Service not like 'EXP%' and Service not like 'SAV%'and Service not like 'IMP%'
  --group by case when isnull(Account,'')='' then 'Card' else Account end,Service,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end

  update #temp set Accountname=p.shortname from Pronto.dbo.ProntoDebtor p where account=billto

    Select * from  #temp



END

GO
GRANT EXECUTE
	ON [dbo].[sp_RateCardsSummary]
	TO [ReportUser]
GO
