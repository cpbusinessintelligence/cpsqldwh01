SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptRateCardDetailsByZone_P](@AccountNumber varchar(50)) as 

--,@Service varchar(50),@OriginZone varchar(50)
begin

  Select case when isnull(Account,'')='' 
       then 'Card' else Account 
	   end as Account,
	   convert(varchar(100),'') as Accountname,
	   Service,
	   p1.pc_name as PriceCodeName,
	   case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' 
	   then 'Parcel/Satchel'  else 'Blocks' end as Category,
	   convert(date,max(convert(date,[Effective Date]))) as [Effective Date] 
  into #temp 
  from [CouponCalculator].[dbo].finalratesv1 (NOLOCK)
  --  left join [cpsqldwh01].cpplEDI.dbo.companyaccount CA (NOLOCK)
  --ON Account=CA.ca_account
  --LEFT OUTER JOIN
  --          [cpsqldwh01].cpplEDI.dbo.companies C (NOLOCK) ON CA.ca_company_id = C.c_id INNER JOIN
  --          [cpsqldwh01].cpplEDI.dbo.companypricecode CP (NOLOCK) ON C.c_id = CP.cp_company_id
  left join cpplEDI.dbo.pricecodes p1 (NOLOCK) on p1.pc_code=Service
   where case when isnull(Account,'')='' then 'Card' else Account end=case when @AccountNumber='' then 'CARD' else @AccountNumber end 
		and Service not like 'EXP%' and Service not like 'SAV%'and Service not like 'IMP%'
  group by case when isnull(Account,'')='' then 'Card' else Account end,Service,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end

  update #temp set Accountname=p.shortname from Pronto.dbo.ProntoDebtor p where account=billto

   
  Select case when isnull(r.Account,'')='' then 'Card' else r.Account end as Account
      ,r.Service
	  ,t.Category
      ,convert(date,r.[Effective Date]) as [Effective Date]
      ,[Origin Zone]
	  ,D.[ZoneDescription] as [PickupZoneDesc]
      ,[Destination Zone]
	  ,Z.[Description] as [DestinationZoneDesc]
      ,[Minimum Charge]
      ,[Basic Charge]
      ,[Fuel Override]
      ,[Fuel Percentage]
     -- ,[Rounding 1] as WeightBreak
	  ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.0000 end as ChargePerKilo
	  ,case when convert(float,[Rounding 1])=25.00 then 25.0000*convert(float,[Charge 1]) else 0.0000 end as ChargePer25Kilo
	  ,convert(int,100) as Showall
	  ,convert(int,0) as SortOrder
	  --,convert(int,0) as cnt
into #temp1	  
from [CouponCalculator].[dbo].finalratesv1 r 
join #temp t on t.Service=r.Service and case when t.Account='CARD' then '' else t.Account end=r.Account and convert(date,t.[Effective Date])=convert(date,r.[Effective Date])
join [dbo].[ZoneDescriptions] Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].[ZoneDescriptions] D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where   isnull(r.Account,'') =@AccountNumber and r.Service LIKE 'P%'


 update #temp1 set Showall =case when ordering=0 then 1 else 0 end,SortOrder=Ordering from cpsqlrct01.OperationalReporting.[dbo].[Zones_Pronto_Mapping] where [Destination Zone]=zone
 --update #temp1 set cnt =(select count(*) from #temp1)
 -- select
 --           ROW_NUMBER() over (order by t.cnt desc, t.Account, t.Service) as RowId, t.*
 --       from (select * from #Temp1) as t 
	--	where showall=0 order by RowId

SELECT
    t.*,
    Rowindex = ROW_NUMBER() OVER(PARTITION BY [Origin Zone] ORDER BY [Origin Zone]),
    RowNumber = DENSE_RANK() OVER (ORDER BY [Origin Zone])
FROM (select * from #Temp1) as t 
where showall=0 

END

GO
