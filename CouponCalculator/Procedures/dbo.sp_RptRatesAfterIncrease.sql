SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptRatesAfterIncrease](@AccountNumber varchar(50),@Service varchar(50),@Effectivedate date,@Showall bit) as 
begin


    Select case when isnull(Account,'')='' then 'Card' else Account end as Account,convert(varchar(100),'') as Accountname,Service,p1.pc_name as PriceCodeName,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%'  then 'Parcel/Satchel' else 'Blocks' end as Category, convert(date,max(convert(date,[Effective Date]))) as [Effective Date] 
  into #temp 
  from [CouponCalculator].[dbo].finalratesv1 
                                            left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=service
  where case when isnull(Account,'')='' then 'Card' else Account end=case when @AccountNumber='' then 'CARD' else @AccountNumber end and Service not like 'EXP%' and Service not like 'SAV%'and Service not like 'IMP%'
  group by case when isnull(Account,'')='' then 'Card' else Account end,Service,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end

  update #temp set Accountname=p.shortname from Pronto.dbo.ProntoDebtor p where account=billto


 
 
  Select case when isnull(r.Account,'')='' then 'Card' else r.Account end as Account
      ,r.Service
      ,convert(date,r.[Effective Date]) as [Effective Date]
      ,[Origin Zone]
      ,[Destination Zone]
      ,[Minimum Charge]
      ,[Basic Charge]
      ,[Fuel Override]
      ,[Fuel Percentage]
     -- ,[Rounding 1] as WeightBreak
	  ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1])  else 0.0000 end as ChargePerKilo
	  ,case when convert(float,[Rounding 1])=25.00 then 25.0000*convert(float,[Charge 1]) else 0.0000 end as ChargePer25Kilo
	  ,convert(int,100) as Showall
	  ,convert(int,0) as SortOrder
into #temp1	  
from [CouponCalculator].[dbo].finalratesv1 r join #temp t on t.Service=r.Service and case when t.Account='CARD' then '' else t.Account end=r.Account and convert(date,t.[Effective Date])=convert(date,r.[Effective Date])
where  isnull(r.Account,'') =@AccountNumber and r.Service=@Service and convert(date,r.[Effective Date])=convert(date,@Effectivedate)

-- 02/08/2018 : Kalpesh -- Remove cpsqlrct01 link from the query As cpsqlrct01 is shutdown --
 --update #temp1 set Showall =case when ordering=0 then 1 else 0 end,SortOrder=Ordering from cpsqlrct01.OperationalReporting.[dbo].[Zones_Pronto_Mapping] where [Destination Zone]=zone

 update #temp1 set Showall =case when ordering=0 then 1 else 0 end,SortOrder=Ordering from [dbo].[Zones_Pronto_Mapping] where [Destination Zone]=zone

 If(@Showall=0)

 select * from #temp1 where showall=@Showall order by [Origin Zone],SortOrder

 else

 select * from #temp1  order by [Origin Zone],SortOrder

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptRatesAfterIncrease]
	TO [ReportUser]
GO
