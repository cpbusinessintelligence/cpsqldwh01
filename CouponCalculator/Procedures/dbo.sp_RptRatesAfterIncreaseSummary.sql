SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptRatesAfterIncreaseSummary](@AccountNumber varchar(50)) as 
begin


    Select case when isnull(Account,'')='' then 'Card' else Account end as Account,convert(varchar(100),'') as Accountname,Service,p1.pc_name as PriceCodeName,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel'  else 'Blocks' end as Category, convert(date,max(convert(date,[Effective Date]))) as [Effective Date] 
  into #temp 
  from [CouponCalculator].[dbo].finalratesv1 
                                            left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=service
  where case when isnull(Account,'')='' then 'Card' else Account end=case when @AccountNumber='' then 'CARD' else @AccountNumber end and Service not like 'EXP%' and Service not like 'SAV%'and Service not like 'IMP%'
  group by case when isnull(Account,'')='' then 'Card' else Account end,Service,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end

  update #temp set Accountname=p.shortname from Pronto.dbo.ProntoDebtor p where account=billto
  --  Select case when Account='' then 'Card' else Account end as Account,P.shortname as Accountname,Service,max([Effective Date]) as [Effective Date] 
  --into #temp 
  --from [CouponCalculator].[dbo].[Rates MAR] left join Pronto.dbo.Prontodebtor p on account=billto
  --where case when Account='' then 'Card' else Account end='112726070'
  --group by case when Account='' then 'Card' else Account end,Service,P.shortname


  Select * from  #temp

  --select * from [CouponCalculator].[dbo].[Rates MAR] where account='112726070'  cpplEDI.dbo.pricecodes

  --Select * from [CouponCalculator].[dbo].[Rates MAR] where account='111222402' Pronto.dbo.Prontodebtor [CouponCalculator].[dbo].[Rates MAR]  where service='A55' and account='112675558'

  
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptRatesAfterIncreaseSummary]
	TO [ReportUser]
GO
