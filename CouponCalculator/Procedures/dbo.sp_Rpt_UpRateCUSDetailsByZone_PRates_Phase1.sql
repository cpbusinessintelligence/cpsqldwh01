SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_Rpt_UpRateCUSDetailsByZone_PRates_Phase1](@AccountNumber varchar(50)) as 

BEGIN


Declare @TarriffCode as VArchar(20) =  (Select Tariff from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber )
Declare @FromZone as VArchar(20) =  (Select Case Territory WHEN 'CAD' THEN 'ADL' WHEN 'CBN' THEN 'BNE' WHEN 'CCB' THEN 'CBR' WHEN 'CCC' THEN 'NTL' WHEN 'CCF' THEN 'CFS' WHEN 'CME' THEN 'MEL' WHEN 'COO' THEN 'OOL' WHEN 'CPE' THEN 'PER' WHEN 'CSY' THEN 'SYD' ELSE '' END 
    from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber )

Select RevenueOriginZone into #TempZones from [CouponCalculator].[dbo].[UP2019_CustomerZones] Where AccountCode  = @AccountNumber
insert into #TempZOnes Select @FromZone




SELECT  distinct U.[Accountcode] as Account
      ,U.[BillTo]
	  ,RateCardDescription
      ,ISnull(p.shortname,'') as Accountname
      ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN U.[OldServiceCode]
                     ELSE U.[ServiceCode] 
                     END as ServiceCode
         ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN p1.pc_name
                     ELSE p2.pc_name
                     END as PriceCodeName
          ,case when p1.pc_name like '%Parcel%' 
                 or p1.pc_name like '%Satchel%' 
                       or p1.pc_name like '%item%'
                       or p2.pc_name like '%Parcel%' 
                       or p2.pc_name like '%item%' 
                       or p2.pc_name like '%Satchel%'  
                 then 'Parcel/Satchel'  
          else 'Blocks' end as Category
         ,[RateCardCategory]
         ,[Effective  Date] as [Effective Date]
		 	  	  , @TarriffCode as tariff
		 into #temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.[OldServiceCode]
  left join cpplEDI.dbo.pricecodes p2 on p2.pc_code=U.[ServiceCode]
  left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.BillTo
  where U.AccountCode =@AccountNumber
------------------------P rates

 CREATE TABLE #temp_tariff(
	[Account] [varchar](50) NULL,
	[ServiceCode] [nvarchar](50) NULL,
	[category]  [nvarchar](50) NULL,
	[Effective Date] [nvarchar](50)  NULL,
	
	[Origin Zone] [varchar](50) NULL,
	[PickupZoneDesc] [varchar](50) NULL,
	[Destination Zone] [varchar](50) NULL,
	[DestinationZoneDesc] [varchar](50) NULL,
	[Minimum Charge] [float] NULL,
	[Basic Charge] [float] NULL,
	[Fuel Override] [varchar](50) NULL,
	[Fuel Percentage] [varchar](50) NULL,
	
	[ChargeperKilo] [float] NULL)


  If @TarriffCode=0 or @TarriffCode=''
 Begin
 Insert into #temp_tariff( Account
         ,ServiceCode
	     ,Category
         ,[Effective Date]
         ,[Origin Zone]
	     ,[PickupZoneDesc]
         ,[Destination Zone]
	     ,[DestinationZoneDesc]
	    ,[Minimum Charge]
         , [Basic Charge]
         ,[Fuel Override]
        , [Fuel Percentage]
	     , ChargePerKilo)
		   Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	  ,Round(convert(float,[Minimum Charge]),2) as [Minimum Charge]
         ,Round(convert(float,[Basic Charge]),2) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     , Round(convert(float,[Charge 1]),2) as ChargePerKilo 
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on CASE WHEN isnull(t.tariff,0) = 0 THEN @AccountNumber ELSE @TarriffCode end=r.AccountCode and t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where r.AccountCode=CASE WHEN isnull(t.tariff,0) = 0 THEN @AccountNumber ELSE @TarriffCode end  and LTRIM(RTRIM(t.RateCardCategory)) = 'Customised' and r.ServiceCode like 'P%'
end
else
Begin
Insert into #temp_tariff( Account
         ,ServiceCode
	     ,Category
         ,[Effective Date]
         ,[Origin Zone]
	     ,[PickupZoneDesc]
         ,[Destination Zone]
	     ,[DestinationZoneDesc]
	    ,[Minimum Charge]
         , [Basic Charge]
         ,[Fuel Override]
        , [Fuel Percentage]
	     , ChargePerKilo)
		   Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	  ,Round(convert(float,[Minimum Charge]),2) as [Minimum Charge]
         ,Round(convert(float,[Basic Charge]),2) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     , Round(convert(float,[Charge 1]),2) as ChargePerKilo 
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on CASE WHEN isnull(t.tariff,0) = 0 THEN @AccountNumber ELSE @TarriffCode end=r.AccountCode and t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where r.AccountCode=CASE WHEN isnull(t.tariff,0) = 0 THEN @AccountNumber ELSE @TarriffCode end  and LTRIM(RTRIM(t.RateCardCategory)) = 'Customised' and r.ServiceCode like 'P%'
and r.[Origin Zone] in (Select RevenueOriginZone from #TempZones) 
end
  
  select * into #Temp1 from (Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	  ,Round(convert(float,[Minimum Charge]),2) as [Minimum Charge]
         ,Round(convert(float,[Basic Charge]),2) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	    ,Round(convert(float,[Charge 1]),2) as ChargePerKilo
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where LTRIM(RTRIM(t.RateCardCategory)) = 'Standard' and r.ServiceCode like 'P%' and r.[Origin Zone] in (Select RevenueOriginZone from #TempZones) 
UNION
select * from #temp_tariff

) T

SELECT
    t.*,
    Rowindex = ROW_NUMBER() OVER(PARTITION BY [Origin Zone] ORDER BY [Origin Zone]),
    RowNumber = DENSE_RANK() OVER (ORDER BY [Origin Zone])
FROM (select * from #Temp1) as t Order by t.ServiceCode asc


END



--exec sp_Rpt_UpRateCUSDetailsByZone_PRates_Phase1 '113089940'
GO
