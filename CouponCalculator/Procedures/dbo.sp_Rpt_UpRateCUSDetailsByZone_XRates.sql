SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


--exec [dbo].[sp_Rpt_UpRateCUSDetailsByZone_XRates] '113007900'


CREATE procedure [dbo].[sp_Rpt_UpRateCUSDetailsByZone_XRates](@AccountNumber varchar(50)) as 

BEGIN

--declare @AccountNumber varchar(50)

--set @AccountNumber='112903885'

--Declare @TarriffCode as VArchar(20) =  (Select  Tariff from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber )
Declare @FromZone as VArchar(20) =  (Select Case Territory WHEN 'CAD' THEN 'ADL' WHEN 'CBN' THEN 'BNE' WHEN 'CCB' THEN 'CBR' WHEN 'CCC' THEN 'NTL' WHEN 'CCF' THEN 'CFS' WHEN 'CME' THEN 'MEL' WHEN 'COO' THEN 'OOL' WHEN 'CPE' THEN 'PER' WHEN 'CSY' THEN 'SYD' ELSE '' END 
    from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber )

Select RevenueOriginZone into #TempZones from [CouponCalculator].[dbo].[UP2020_CustomerZones] Where AccountCode  = @AccountNumber
insert into #TempZOnes Select @FromZone
--drop table #temp1
SELECT  distinct U.[Accountcode] as Account
      ,RateCardCategory as [RateCardDescription]
      ,U.[BillTo]
      ,ISnull(U.shortname,'') as Accountname
      ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN U.[OldServiceCode]
                     ELSE U.[ServiceCode] 
                     END as Service
         ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN p1.pc_name
                     ELSE p2.pc_name
                     END as PriceCodeName
          ,case when p1.pc_name like '%Parcel%' 
                 or p1.pc_name like '%Satchel%' 
                       or p1.pc_name like '%item%'
                       or p2.pc_name like '%Parcel%' 
                       or p2.pc_name like '%item%' 
                       or p2.pc_name like '%Satchel%'  
                 then 'Parcel/Satchel'  
          else 'Blocks' end as Category
         ,[RateCardCategory]
         ,U.[Effective Date] 
		 ,Tariff_Account_Code as [Tariff_Account_Code]
		 into #temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.[OldServiceCode]
  left join cpplEDI.dbo.pricecodes p2 on p2.pc_code=U.[ServiceCode]
 -- left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.Accountcode
  where U.AccountCode  = @AccountNumber

   select * into #Temp1 from (Select  t.Account as Account
         ,t.Service
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	     ,convert(float,[Minimum Charge]) as [Minimum Charge]
         ,convert(float,[Basic Charge]) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.00 end as ChargePerKilo
	     ,case when convert(float,[Rounding 1])=25.00 then 25.00*convert(float,[Charge 1]) else 0.00 end as ChargePer25Kilo
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on t.Service=r.Service
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where r.Account='' and LTRIM(RTRIM(t.[RateCardDescription])) like 'Standard%' and r.Service  like 'X%' and r.[Origin Zone] =@FromZOne
UNION
  Select  t.Account as Account
         ,t.Service
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	     ,convert(float,[Minimum Charge]) as [Minimum Charge]
         ,convert(float,[Basic Charge]) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.00 end as ChargePerKilo
	     ,case when convert(float,[Rounding 1])=25.00 then 25.00*convert(float,[Charge 1]) else 0.00 end as ChargePer25Kilo  
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on isnull(t.Tariff_Account_Code,t.Account) =r.Account and t.Service=r.Service
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where --r.AccountCode=CASE WHEN t.tariff = 0 THEN @AccountNumber ELSE @TarriffCode end and
 LTRIM(RTRIM(t.[RateCardDescription])) like 'Customised%' and r.Service  like 'X%')T

 SELECT t.*,
       --,RowNumber = DENSE_RANK() OVER (ORDER BY [ServiceCode],[Origin Zone])
	   DENSE_RANK() OVER (PARTITION BY [Service] ORDER BY [Service] ASC,[Origin Zone] ASC)  as [RowNumber] 
	   ,Rowindex = DENSE_RANK() OVER (ORDER BY [Service] ASC)
  FROM (select * from #Temp1) as t order by t.[Service] asc

END
GO
