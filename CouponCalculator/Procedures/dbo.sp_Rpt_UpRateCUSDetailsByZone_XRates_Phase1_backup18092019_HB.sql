SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_Rpt_UpRateCUSDetailsByZone_XRates_Phase1_backup18092019_HB](@AccountNumber varchar(50)) as 

BEGIN
--select * from #temp1

--declare @AccountNumber varchar(50)

--set @AccountNumber='112903885'

Declare @TarriffCode as VArchar(20) =  (Select Tariff from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber and phase='Phase2')
/*******************CREATE RATE CARD SUMMARY***********************************/
Declare @FromZone as VArchar(20) =  (Select Case Territory WHEN 'CAD' THEN 'ADL' WHEN 'CBN' THEN 'BNE' WHEN 'CCB' THEN 'CBR' WHEN 'CCC' THEN 'NTL' WHEN 'CCF' THEN 'CFS' WHEN 'CME' THEN 'MEL' WHEN 'COO' THEN 'OOL' WHEN 'CPE' THEN 'PER' WHEN 'CSY' THEN 'SYD' ELSE '' END 
    from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber and phase='Phase2')


	Select RevenueOriginZone into #TempZones from [CouponCalculator].[dbo].[UP2019_CustomerZones] Where AccountCode  = @AccountNumber
insert into #TempZOnes Select @FromZone



SELECT  distinct U.[Accountcode] as Account
      ,U.[BillTo]
      ,Isnull(p.shortname,'') as Accountname
      ,Case When U.[OldServiceCode]=U.[ServiceCode]
	        THEN U.[OldServiceCode]
			ELSE U.[ServiceCode] 
			END as ServiceCode
	  ,p1.pc_name as PriceCodeName
	  ,U.[RateCardDescription] as RateCardDescription
	  ,case when p1.pc_name like '%Parcel%' 
	          or p1.pc_name like '%Satchel%' 
			  or p1.pc_name like '%item%' 
	          then 'Parcel/Satchel'  
	   else 'Blocks' end as Category
	  ,[RateCardCategory]
	  ,'09 September, 2019' as [Effective Date]
	  , @TarriffCode as tariff
  Into #Temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.ServiceCode
  left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.BillTo
  where U.AccountCode =@AccountNumber and [Effective  Date]='October 21, 2019' --only for phase2

  /*******************GET ALL THE X Rates Summary***********************************/
  
  select * into #Temp1 from (Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	    ,Round(convert(float,[Minimum Charge]),2) as [Minimum Charge]
         ,Round(convert(float,[Basic Charge]),2) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	    ,Round(convert(float,[Charge 1]),2) as ChargePerKilo
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where LTRIM(RTRIM(t.RateCardCategory)) = 'Standard' and r.ServiceCode like 'X%' and r.[Origin Zone] in (Select RevenueOriginZone from #TempZones) 
UNION
  Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	    ,Round(convert(float,[Minimum Charge]),2) as [Minimum Charge]
         ,Round(convert(float,[Basic Charge]),2) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     , Round(convert(float,[Charge 1]),2) as ChargePerKilo 
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on CASE WHEN t.tariff = 0 THEN @AccountNumber ELSE @TarriffCode end=r.AccountCode and t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where r.AccountCode=CASE WHEN t.tariff = 0 THEN @AccountNumber ELSE @TarriffCode end and LTRIM(RTRIM(t.RateCardCategory)) = 'Customised' and r.ServiceCode like 'X%' and r.ServiceCode <> 'X53') T


SELECT
    t.*,
    Rowindex = ROW_NUMBER() OVER(PARTITION BY [Origin Zone] ORDER BY [Origin Zone]),
    RowNumber = DENSE_RANK() OVER (ORDER BY [Origin Zone])
FROM (select * from #Temp1) as t Order by t.ServiceCode asc


END










GO
