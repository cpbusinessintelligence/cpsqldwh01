SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[sp_Rpt_UpRateCUSDetailsByZone_phase1_BUP_Jp_20190909](@AccountNumber varchar(50)) as 


BEGIN
--select * from #temp


--declare @AccountNumber varchar(50)

--set @AccountNumber='112903885'

Declare @TarriffCode as VArchar(20) =  (Select Tariff from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber)
Declare @FromZone as VArchar(20) =  (Select Case Territory WHEN 'CAD' THEN 'ADL' WHEN 'CBN' THEN 'BNE' WHEN 'CCB' THEN 'CBR' WHEN 'CCC' THEN 'NTL' WHEN 'CCF' THEN 'CFS' WHEN 'CME' THEN 'MEL' WHEN 'COO' THEN 'OOL' WHEN 'CPE' THEN 'PER' WHEN 'CSY' THEN 'SYD' ELSE '' END 
    from [dbo].[Uprate_CUSAccountSummary] Where Accountcode =@AccountNumber)

Select RevenueOriginZone into #TempZones from [CouponCalculator].[dbo].[UP2018_CustomerZones] Where AccountCode  = @AccountNumber
insert into #TempZOnes Select @FromZone

SELECT  distinct U.[Accountcode] as Account
      ,RateCardDescription
      ,U.[BillTo]
      ,ISnull(p.shortname,'') as Accountname
      ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN U.[OldServiceCode]
                     ELSE U.[ServiceCode] 
                     END as ServiceCode
         ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN p1.pc_name
                     ELSE p2.pc_name
                     END as PriceCodeName
          ,case when p1.pc_name like '%Parcel%' 
                 or p1.pc_name like '%Satchel%' 
                       or p1.pc_name like '%item%'
                       or p2.pc_name like '%Parcel%' 
                       or p2.pc_name like '%item%' 
                       or p2.pc_name like '%Satchel%'  
                 then 'Parcel/Satchel'  
          else 'Blocks' end as Category
         ,[RateCardCategory]
         ,'11 September, 2017' as [Effective Date]
		 ,@TarriffCode as tariff
		 into #temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.[OldServiceCode]
  left join cpplEDI.dbo.pricecodes p2 on p2.pc_code=U.[ServiceCode]
  left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.BillTo
  where U.AccountCode  = @AccountNumber

  

  select * into #Temp1 from (Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	     ,convert(float,[Minimum Charge]) as [Minimum Charge]
         ,convert(float,[Basic Charge]) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.00 end as ChargePerKilo
	     ,case when convert(float,[Rounding 1])=25.00 then 25.00*convert(float,[Charge 1]) else 0.00 end as ChargePer25Kilo
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where LTRIM(RTRIM(t.[RateCardCategory])) like 'Standard%' and r.ServiceCode not like 'P%'and r.ServiceCode not like 'X%'
and r.ServiceCode not like 'Y%' and r.[Origin Zone] in (Select RevenueOriginZone from #TempZones)

UNION
  Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	     ,convert(float,[Minimum Charge]) as [Minimum Charge]
         ,convert(float,[Basic Charge]) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.00 end as ChargePerKilo
	     ,case when convert(float,[Rounding 1])=25.00 then 25.00*convert(float,[Charge 1]) else 0.00 end as ChargePer25Kilo  
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on CASE WHEN Tariff = 0 THEN  t.Account ELSE t.tariff end =r.AccountCode and t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where r.AccountCode=CASE WHEN isnull(t.tariff,0) = 0 THEN @AccountNumber ELSE @TarriffCode end and LTRIM(RTRIM(t.[RateCardCategory])) like 'Customised%' and r.ServiceCode not like 'P%' and r.ServiceCode not like 'X%'
and r.ServiceCode not like 'Y%'
UNION
  Select  t.Account as Account
         ,t.ServiceCode
	     ,t.Category
         ,t.[Effective Date]
         ,[Origin Zone]
	     ,D.[ZoneDescription] as [PickupZoneDesc]
         ,[Destination Zone]
	     ,Z.[Description] as [DestinationZoneDesc]
	     ,convert(float,[Minimum Charge]) as [Minimum Charge]
         ,convert(float,[Basic Charge]) as [Basic Charge]
         ,[Fuel Override]
        ,[Fuel Percentage]
	     ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.00 end as ChargePerKilo
	     ,case when convert(float,[Rounding 1])=25.00 then 25.00*convert(float,[Charge 1]) else 0.00 end as ChargePer25Kilo  
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on CASE WHEN isnull(t.tariff,0) = 0 THEN  t.Account ELSE t.tariff end =r.AccountCode and t.ServiceCode=r.ServiceCode
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where r.AccountCode=CASE WHEN isnull(t.tariff,0) = 0 THEN @AccountNumber ELSE @TarriffCode end and LTRIM(RTRIM(t.[RateCardCategory])) like 'Customised%' and r.ServiceCode ='X53'
and @AccountNumber in ('112637020',
'111224408',
'112650916',
'112650940')) T



--select * from #temp1

  --  SELECT 
  --     --,RowNumber = DENSE_RANK() OVER (ORDER BY [ServiceCode],[Origin Zone])
	 --  DENSE_RANK() OVER (PARTITION BY [Origin Zone] ORDER BY [ServiceCode],[Origin Zone] ASC)  as [Rowindex] 
	 --  ,RowNumber = DENSE_RANK() OVER (ORDER BY [ServiceCode]),t.* 
  --FROM (select * from #Temp1) as t

  --    SELECT 
  --     --,RowNumber = DENSE_RANK() OVER (ORDER BY [ServiceCode],[Origin Zone])
	 --  DENSE_RANK() OVER (PARTITION BY [ServiceCode] ORDER BY [ServiceCode],[Origin Zone] ASC)  as [RowNumber] 
	 --  ,Rowindex = DENSE_RANK() OVER (ORDER BY [ServiceCode]),t.* 
  --FROM (select * from #Temp1) as t

       SELECT 
       --,RowNumber = DENSE_RANK() OVER (ORDER BY [ServiceCode],[Origin Zone])
	   DENSE_RANK() OVER (PARTITION BY [ServiceCode] ORDER BY [ServiceCode] ASC,[Origin Zone] ASC)  as [RowNumber] 
	   ,Rowindex = DENSE_RANK() OVER (ORDER BY [ServiceCode] ASC),t.* 
  FROM (select * from #Temp1) as t order by [ServiceCode] asc

END

--select * from  [CouponCalculator].[dbo].[Uprate_CustomerRatecards] where Accountcode='112903885'
GO
