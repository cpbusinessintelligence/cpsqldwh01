SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_Rpt_UpRate_StandardCardRate](@AccountNumber varchar(50)) as 

BEGIN

Select case when isnull(F.[AccountCode],'')='' 
       then 'Card' 
	   end as Account,
	   convert(varchar(100),'') as Accountname,
	   F.ServiceCode,
	   p1.pc_name as PriceCodeName,
	   case when p1.pc_name like '%Parcel%' 
	          or p1.pc_name like '%Satchel%' 
			  or p1.pc_name like '%item%' 
	          then 'Parcel/Satchel'  
	   else 'Blocks' end as Category,
	   convert(date,max(convert(date,[Effective Date]))) as [Effective Date] 
  into #temp 
  from [CouponCalculator].[dbo].[Uprate_FinalRateCards] F
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=F.ServiceCode
  where F.[AccountCode]= COALESCE(NULLIF(@AccountNumber, ''),F.[AccountCode]) 
  --      and F.ServiceCode not like 'EXP%' 
		--and F.ServiceCode not like 'SAV%'
		--and F.ServiceCode not like 'IMP%'
  group by (case when isnull(F.[AccountCode],'')='' then 'Card' end),F.ServiceCode,p1.pc_name
  ,   case when p1.pc_name like '%Parcel%' 
	          or p1.pc_name like '%Satchel%' 
			  or p1.pc_name like '%item%' 
	          then 'Parcel/Satchel'  
	   else 'Blocks' end
  update #temp set Accountname=p.shortname from Pronto.dbo.ProntoDebtor p where account=billto

 Select 'Card' as Account
      ,r.ServiceCode
	  ,t.Category
      ,convert(date,r.[Effective Date]) as [Effective Date]
      ,[Origin Zone]
	  ,D.[ZoneDescription] as [PickupZoneDesc]
      ,[Destination Zone]
	  ,Z.[Description] as [DestinationZoneDesc]
	  ,convert(float,[Minimum Charge]) as [Minimum Charge]
      ,convert(float,[Basic Charge]) as [Basic Charge]
      ,[Fuel Override]
      ,[Fuel Percentage]
	  ,case when convert(float,[Rounding 1])<>25.00 then convert(float,[Charge 1]) else 0.00 end as ChargePerKilo
	  ,case when convert(float,[Rounding 1])=25.00 then 25.00*convert(float,[Charge 1]) else 0.00 end as ChargePer25Kilo
	  ,convert(int,100) as Showall
	  ,convert(int,0) as SortOrder
into #temp1	  
from [CouponCalculator].[dbo].[Uprate_FinalRateCards]  r 
join #temp t on t.ServiceCode=r.ServiceCode and case when t.Account='CARD' then '' end=r.AccountCode and convert(date,t.[Effective Date])=convert(date,r.[Effective Date])
join [dbo].ZoneDescriptions Z (NOLOCK) on [Destination Zone]= [ZoneCode]
join [dbo].ZoneDescriptions D (NOLOCK) on [Origin Zone]= D.[ZoneCode]
where   r.[AccountCode]= COALESCE(NULLIF(@AccountNumber, ''),r.[AccountCode]) 


 update #temp1 set Showall =case when ordering=0 then 1 else 0 end,SortOrder=Ordering from cpsqlrct01.OperationalReporting.[dbo].[Zones_Pronto_Mapping] where [Destination Zone]=zone

  SELECT
    t.*,
    Rowindex = ROW_NUMBER() OVER(PARTITION BY [ServiceCode] ORDER BY [ServiceCode],[Origin Zone]),
    RowNumber = DENSE_RANK() OVER (ORDER BY [ServiceCode],[Origin Zone])
FROM (select * from #Temp1) as t
where showall=0 

END
GO
