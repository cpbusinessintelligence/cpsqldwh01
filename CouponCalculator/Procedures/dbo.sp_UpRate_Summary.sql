SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Heena Bajaj
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_UpRate_Summary](@AccountNumber varchar(50))
AS
BEGIN


SELECT  distinct U.[Accountcode] as Account
      ,U.[BillTo]
      ,ISnull(U.shortname,'') as Accountname
      ,UPPER(U.[ServiceCode])
               as ServiceCode
         , p1.pc_name  as PriceCodeName
          ,case when (p1.pc_name like '%Parcel%' 
               --  or p1.pc_name like '%Satchel%' 
                       or p1.pc_name like '%item%')
                     
                     --  or p2.pc_name like '%Satchel%'  
					and  p1.pc_name not like '%Pallets%'
					and  p1.pc_name not like '%AGG%' 
                 then 'Parcel' 
				 
		 when  --p1.pc_name like '%Parcel%' 
                  p1.pc_name like '%Satchel%' 
                    
                 then 'Satchel' 
				 when   p1.pc_name like '%Pallets%'  then
				'Pallets'
				when p1.pc_code in ('AEP10','AEP25','AGEP0','AGEP1','AGEP3','AGEP5','AGHP0','AGHP1','AGHP3','AGHP5','AGP10','AGSP0','AGSP1','AGSP3',
				'AGSP5','AGVP0','AGVP1','AGVP3','AGVP5','AHP10','AHP25','ASP25','AVP10','AVP15','AVP20','AVP25') then 'Parcel'
			when p1.pc_code in ('AGEBK','AGHBK','AGSBK','AGVBK') then 'Blocks'
          else 'Blocks' end as Category
         ,[RateCardCategory]
         , [Effective Date] as [Effective Date]
		 into #Temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
 -- left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.[OldServiceCode]
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.[ServiceCode]
  --left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.BillTo
  where U.AccountCode  =  @AccountNumber




    Select * from  #Temp order by ServiceCode Asc

	END
GO
