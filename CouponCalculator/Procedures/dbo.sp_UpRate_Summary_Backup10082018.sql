SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tejes Singam
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpRate_Summary_Backup10082018](@AccountNumber varchar(50))
AS
BEGIN

SELECT  distinct U.[Accountcode] as Account
      ,U.[BillTo]
      ,ISnull(U.shortname,'') as Accountname
      ,UPPER(Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN U.[OldServiceCode]
                     ELSE U.[ServiceCode] 
                     END) as ServiceCode
         ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN p1.pc_name
                     ELSE p2.pc_name
                     END as PriceCodeName
          ,case when p1.pc_name like '%Parcel%' 
                 or p1.pc_name like '%Satchel%' 
                       or p1.pc_name like '%item%'
                       or p2.pc_name like '%Parcel%' 
                       or p2.pc_name like '%item%' 
                       or p2.pc_name like '%Satchel%'  
                 then 'Parcel/Satchel'  
          else 'Blocks' end as Category
         ,[RateCardCategory]
         ,'11 September, 2017' as [Effective Date]
		 into #Temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.[OldServiceCode]
  left join cpplEDI.dbo.pricecodes p2 on p2.pc_code=U.[ServiceCode]
  --left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.BillTo
  where U.AccountCode  = @AccountNumber


--Select [Accountcode] as Account,
--	   convert(varchar(100),'') as Accountname,
--	   F.ServiceCode,
--	   p1.pc_name as PriceCodeName,
--	   case when p1.pc_name like '%Parcel%' 
--	          or p1.pc_name like '%Satchel%' 
--			  or p1.pc_name like '%item%' 
--	          then 'Parcel/Satchel'  
--	   else 'Blocks' end as Category,
--	   convert(date,max(convert(date,'2017-09-11'))) as [Effective Date] 
--  into #temp 
--  from #Temp1 F
--  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=F.ServiceCode
--  where case when isnull(F.AccountCode,'')='' then 'Card' else F.AccountCode end=case when @AccountNumber='' then 'CARD' else @AccountNumber end  and F.ServiceCode not like 'EXP%' and F.ServiceCode not like 'SAV%'and F.ServiceCode not like 'IMP%'
  
  
  
  
--  group by [Accountcode],F.ServiceCode,p1.pc_name,case when p1.pc_name like '%Parcel%' or p1.pc_name like '%Satchel%' or p1.pc_name like '%item%' then 'Parcel/Satchel' else 'Blocks' end


--update #Temp1 set Accountname=p.shortname from Pronto.dbo.ProntoDebtor p where Accountcode=p.BillTo

    Select * from  #Temp order by ServiceCode Asc

	END

GO
