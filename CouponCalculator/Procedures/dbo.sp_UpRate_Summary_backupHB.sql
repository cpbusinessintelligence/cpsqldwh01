SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Heena Bajaj
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create  PROCEDURE [dbo].[sp_UpRate_Summary_backupHB](@AccountNumber varchar(50))
AS
BEGIN


SELECT  distinct U.[Accountcode] as Account
      ,U.[BillTo]
      ,ISnull(U.shortname,'') as Accountname
      ,UPPER(Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN U.[OldServiceCode]
                     ELSE U.[ServiceCode] 
                     END) as ServiceCode
         ,Case When U.[OldServiceCode]=U.[ServiceCode]
               THEN p1.pc_name
                     ELSE p2.pc_name
                     END as PriceCodeName
          ,case when p1.pc_name like '%Parcel%' 
               --  or p1.pc_name like '%Satchel%' 
                       or p1.pc_name like '%item%'
                       or p2.pc_name like '%Parcel%' 
                       or p2.pc_name like '%item%' 
                     --  or p2.pc_name like '%Satchel%'  
					and ( p1.pc_name not like '%Pallets%' or p2.pc_name like '%Pallets%')
                 then 'Parcel' 
				 
		 when  --p1.pc_name like '%Parcel%' 
                  p1.pc_name like '%Satchel%' 
                      -- or p1.pc_name like '%item%'
                      -- or p2.pc_name like '%Parcel%' 
                      -- or p2.pc_name like '%item%' 
                       or p2.pc_name like '%Satchel%'  
                 then 'Satchel' 
				 when   p1.pc_name like '%Pallets%' or p2.pc_name like '%Pallets%' then
				'Pallets'
          else 'Blocks' end as Category
         ,[RateCardCategory]
         ,'21 September, 2020' as [Effective Date]
		 into #Temp
  FROM [CouponCalculator].[dbo].[Uprate_CustomerRatecards] U
  left join cpplEDI.dbo.pricecodes p1 on p1.pc_code=U.[OldServiceCode]
  left join cpplEDI.dbo.pricecodes p2 on p2.pc_code=U.[ServiceCode]
  --left Join Pronto.dbo.ProntoDebtor p On U.Accountcode=p.BillTo
  where U.AccountCode  = @AccountNumber




    Select * from  #Temp order by ServiceCode Asc

	END

	
--select * from cpplEDI.dbo.pricecodes where pc_name like '%Pallets%' or pc_name like '%item%'
GO
