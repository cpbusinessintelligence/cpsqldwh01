SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].['Temp_Zones'] (
		[Zone]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Description]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Priority]        [float] NULL,
		[Warehouse]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[State]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NewZone]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NewZoneName]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].['Temp_Zones'] SET (LOCK_ESCALATION = TABLE)
GO
