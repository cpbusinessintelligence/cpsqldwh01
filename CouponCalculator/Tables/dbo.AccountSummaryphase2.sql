SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountSummaryphase2] (
		[State]               [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]         [int] NULL,
		[BillTo]              [int] NULL,
		[Shortname]           [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Address1]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CustomerEmail]       [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[TARIFF]              [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RepEMail]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ToEmail]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CCEMail]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[BccEMail]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AccountSummaryphase2] SET (LOCK_ESCALATION = TABLE)
GO
