SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountSummaryphase2_PartA] (
		[State]               [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RepCode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[OriginalRepName]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Territory]           [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]         [int] NOT NULL,
		[BillTo]              [int] NOT NULL,
		[Shortname]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Address1]            [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerEmail]       [nvarchar](150) COLLATE Latin1_General_CI_AS NOT NULL,
		[TARIFF]              [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RepEMail]            [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToEmail]             [nvarchar](150) COLLATE Latin1_General_CI_AS NOT NULL,
		[CCEMail]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[BccEMail]            [nvarchar](100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AccountSummaryphase2_PartA] SET (LOCK_ESCALATION = TABLE)
GO
