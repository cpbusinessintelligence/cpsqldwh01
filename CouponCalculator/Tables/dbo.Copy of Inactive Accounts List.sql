SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Copy of Inactive Accounts List] (
		[Child_Account_Code]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[asdf]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Ex_Zone]                [nvarchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[column_4]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Copy of Inactive Accounts List] SET (LOCK_ESCALATION = TABLE)
GO
