SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponCalculator] (
		[CouponCalculatoID]     [int] NOT NULL,
		[FromZone]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[WB1Count]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[WB2Count]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[WB3Count]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[SatchelCount]          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]           [datetime] NULL,
		[EditWho]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]          [datetime] NULL,
		[RedirectionFee]        [decimal](12, 2) NULL,
		CONSTRAINT [PK__CouponCa__BD79D27303BDFDFC]
		PRIMARY KEY
		CLUSTERED
		([CouponCalculatoID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[CouponCalculator]
	ADD
	CONSTRAINT [DF_CouponCalculator_AddDateTime]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[CouponCalculator]
	ADD
	CONSTRAINT [DF_CouponCalculator_EditDateTime]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
ALTER TABLE [dbo].[CouponCalculator]
	ADD
	CONSTRAINT [DF_CouponCalculator_SatchelCount]
	DEFAULT ((0)) FOR [SatchelCount]
GO
ALTER TABLE [dbo].[CouponCalculator]
	ADD
	CONSTRAINT [DF_CouponCalculator_WB1Count]
	DEFAULT ((0)) FOR [WB1Count]
GO
ALTER TABLE [dbo].[CouponCalculator]
	ADD
	CONSTRAINT [DF_CouponCalculator_WB2Count]
	DEFAULT ((0)) FOR [WB2Count]
GO
ALTER TABLE [dbo].[CouponCalculator]
	ADD
	CONSTRAINT [DF_CouponCalculator_WB3Count]
	DEFAULT ((0)) FOR [WB3Count]
GO
CREATE NONCLUSTERED INDEX [IX_CouponCalculator]
	ON [dbo].[CouponCalculator] ([FromZone], [ToZone])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[CouponCalculator] SET (LOCK_ESCALATION = TABLE)
GO
