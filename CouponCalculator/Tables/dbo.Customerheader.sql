SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customerheader] (
		[Accountcode]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Name]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Valid From]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi Override]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge Method]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volumetric Divisor]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff Id]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Log who]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Log date]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Customerheader] SET (LOCK_ESCALATION = TABLE)
GO
