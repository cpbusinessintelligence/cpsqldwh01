SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customised_RS6.5_15] (
		[Account]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Service]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Effective_Date]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Tariff_ID]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Origin_Zone]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Destination_Zone]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Minimum_Charge]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Basic_Charge]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel_Override]        [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel_Percentage]      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_1]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_2]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_3]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_4]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_5]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_6]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_7]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_8]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_9]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_10]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_1]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_2]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_3]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_4]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_5]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_6]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_7]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_8]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_9]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_10]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_1]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_2]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_3]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_4]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_5]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_6]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_7]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_8]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_9]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Break_10]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Customised_RS6.5_15] SET (LOCK_ESCALATION = TABLE)
GO
