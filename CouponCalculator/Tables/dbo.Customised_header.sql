SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customised_header] (
		[Accountcode]            [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Name]                   [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Service]                [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Valid_From]             [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Fuel_Override]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Fuel]                   [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Multi_Override]         [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Multi]                  [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Charge_Method]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Volumetric_Divisor]     [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Tariff_Id]              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Log_who]                [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Log_date]               [varchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Customised_header] SET (LOCK_ESCALATION = TABLE)
GO
