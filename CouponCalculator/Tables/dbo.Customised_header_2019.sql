SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customised_header_2019] (
		[Accountcode]            [int] NULL,
		[Name]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Valid_From]             [datetime2](7) NULL,
		[Fuel_Override]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi_Override]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi]                  [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Charge_Method]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volumetric_Divisor]     [int] NULL,
		[Tariff_Id]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Log_who]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Log_date]               [datetime2](7) NULL
)
GO
ALTER TABLE [dbo].[Customised_header_2019] SET (LOCK_ESCALATION = TABLE)
GO
