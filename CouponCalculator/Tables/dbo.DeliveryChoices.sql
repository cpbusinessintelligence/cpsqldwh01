SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryChoices] (
		[ID]                            [int] IDENTITY(1, 1) NOT NULL,
		[Category]                      [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryChoiceID]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sortingcode]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryChoiceName]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryChoiceDescription]     [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Operation Hours]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address1]                      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address2]                      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address3]                      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                         [varchar](55) COLLATE Latin1_General_CI_AS NULL,
		[Country]                       [varchar](55) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]               [datetime] NULL,
		[CreatedBy]                     [varchar](55) COLLATE Latin1_General_CI_AS NULL,
		[ModifiedDateTime]              [datetime] NULL,
		[ModifiedBy]                    [varchar](55) COLLATE Latin1_General_CI_AS NULL,
		[Latitude]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Longtitude]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[IsRedeliveryAvailable]         [bit] NULL
)
GO
ALTER TABLE [dbo].[DeliveryChoices] SET (LOCK_ESCALATION = TABLE)
GO
