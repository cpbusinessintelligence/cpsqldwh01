SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ETACalculator_OldSchema] (
		[ETAID]            [int] NOT NULL,
		[FromZone]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ETA]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]      [datetime] NOT NULL,
		[EditWho]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]     [datetime] NULL,
		CONSTRAINT [PK__ETACalcu__054F348658CA263C]
		PRIMARY KEY
		CLUSTERED
		([ETAID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ETACalculator_OldSchema]
	ADD
	CONSTRAINT [DF_ETACalculator_AddDateTime]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[ETACalculator_OldSchema]
	ADD
	CONSTRAINT [DF_ETACalculator_EditDateTime]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_ETACalculator]
	ON [dbo].[ETACalculator_OldSchema] ([FromZone], [ToZone])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ETACalculator_OldSchema] SET (LOCK_ESCALATION = TABLE)
GO
