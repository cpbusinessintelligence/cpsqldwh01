SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailBodyTemp] (
		[EmailBody]     [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[sno]           [int] NULL
)
GO
ALTER TABLE [dbo].[EmailBodyTemp] SET (LOCK_ESCALATION = TABLE)
GO
