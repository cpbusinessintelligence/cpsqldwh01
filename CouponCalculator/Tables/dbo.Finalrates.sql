SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Finalrates] (
		[Account]              [float] NULL,
		[Service]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Effective Date]       [datetime] NULL,
		[Tariff ID]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Origin Zone]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Destination Zone]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Minimum Charge]       [float] NULL,
		[Basic Charge]         [float] NULL,
		[Fuel Override]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]      [float] NULL,
		[Rounding 1]           [float] NULL,
		[Rounding 2]           [numeric](3, 2) NOT NULL,
		[Rounding 3]           [numeric](3, 2) NOT NULL,
		[Rounding 4]           [numeric](3, 2) NOT NULL,
		[Rounding 5]           [numeric](3, 2) NOT NULL,
		[Rounding 6]           [numeric](3, 2) NOT NULL,
		[Rounding 7]           [numeric](3, 2) NOT NULL,
		[Rounding 8]           [numeric](3, 2) NOT NULL,
		[Rounding 9]           [numeric](3, 2) NOT NULL,
		[Rounding 10]          [numeric](3, 2) NOT NULL,
		[Charge 1]             [decimal](12, 4) NULL,
		[Charge 2]             [int] NOT NULL,
		[Charge 3]             [int] NOT NULL,
		[Charge 4]             [int] NOT NULL,
		[Charge 5]             [int] NOT NULL,
		[Charge 6]             [int] NOT NULL,
		[Charge 7]             [int] NOT NULL,
		[Charge 8]             [int] NOT NULL,
		[Charge 9]             [int] NOT NULL,
		[Charge 10]            [int] NOT NULL,
		[Break 1]              [numeric](10, 2) NOT NULL,
		[Break 2]              [numeric](10, 2) NOT NULL,
		[Break 3]              [numeric](10, 2) NOT NULL,
		[Break 4]              [numeric](10, 2) NOT NULL,
		[Break 5]              [numeric](10, 2) NOT NULL,
		[Break 6]              [numeric](10, 2) NOT NULL,
		[Break 7]              [numeric](10, 2) NOT NULL,
		[Break 8]              [numeric](10, 2) NOT NULL,
		[Break 9]              [numeric](10, 2) NOT NULL,
		[Break 10]             [numeric](10, 2) NOT NULL
)
GO
ALTER TABLE [dbo].[Finalrates] SET (LOCK_ESCALATION = TABLE)
GO
