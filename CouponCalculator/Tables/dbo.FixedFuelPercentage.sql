SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FixedFuelPercentage] (
		[Account]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FuelPercentage]     [decimal](12, 2) NULL
)
GO
ALTER TABLE [dbo].[FixedFuelPercentage] SET (LOCK_ESCALATION = TABLE)
GO
