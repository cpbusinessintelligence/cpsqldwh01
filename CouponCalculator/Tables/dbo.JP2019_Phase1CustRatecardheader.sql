SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP2019_Phase1CustRatecardheader] (
		[Account]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[From date]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi Override Charge]     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi %]                   [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Qty or Weight]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Volumetric Divisor]        [int] NOT NULL,
		[ID]                        [varchar](110) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[JP2019_Phase1CustRatecardheader] SET (LOCK_ESCALATION = TABLE)
GO
