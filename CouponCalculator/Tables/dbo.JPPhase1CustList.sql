SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JPPhase1CustList] (
		[ChildAccount]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New Service Code]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[JPPhase1CustList] SET (LOCK_ESCALATION = TABLE)
GO
