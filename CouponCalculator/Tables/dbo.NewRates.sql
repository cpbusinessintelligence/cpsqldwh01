SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewRates] (
		[Account]              [float] NULL,
		[Service]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Effective Date]       [datetime] NULL,
		[Tariff ID]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Origin Zone]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Destination Zone]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Minimum Charge]       [decimal](14, 4) NULL,
		[Basic Charge]         [decimal](14, 4) NULL,
		[Fuel Override]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]      [float] NULL,
		[Rounding 1]           [float] NULL,
		[Rounding 2]           [float] NULL,
		[Rounding 3]           [float] NULL,
		[Rounding 4]           [float] NULL,
		[Rounding 5]           [float] NULL,
		[Rounding 6]           [float] NULL,
		[Rounding 7]           [float] NULL,
		[Rounding 8]           [float] NULL,
		[Rounding 9]           [float] NULL,
		[Rounding 10]          [float] NULL,
		[Charge 1]             [decimal](12, 4) NULL,
		[Charge 2]             [float] NULL,
		[Charge 3]             [float] NULL,
		[Charge 4]             [float] NULL,
		[Charge 5]             [float] NULL,
		[Charge 6]             [float] NULL,
		[Charge 7]             [float] NULL,
		[Charge 8]             [float] NULL,
		[Charge 9]             [float] NULL,
		[Charge 10]            [float] NULL,
		[Break 1]              [float] NULL,
		[Break 2]              [float] NULL,
		[Break 3]              [float] NULL,
		[Break 4]              [float] NULL,
		[Break 5]              [float] NULL,
		[Break 6]              [float] NULL,
		[Break 7]              [float] NULL,
		[Break 8]              [float] NULL,
		[Break 9]              [float] NULL,
		[Break 10]             [float] NULL
)
GO
ALTER TABLE [dbo].[NewRates] SET (LOCK_ESCALATION = TABLE)
GO
