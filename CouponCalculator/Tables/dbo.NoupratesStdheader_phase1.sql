SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoupratesStdheader_phase1] (
		[Account]                   [int] NULL,
		[New_Service_Code]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[From date]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi Override Charge]     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi %]                   [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Qty or Weight]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Volumetric Divisor]        [int] NULL,
		[Tariff ID]                 [nvarchar](110) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[NoupratesStdheader_phase1] SET (LOCK_ESCALATION = TABLE)
GO
