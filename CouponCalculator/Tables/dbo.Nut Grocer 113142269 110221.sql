SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nut Grocer 113142269 110221] (
		[Account]              [int] NOT NULL,
		[Service]              [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Effective_Date]       [datetime2](7) NOT NULL,
		[Tariff_ID]            [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Origin_Zone]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Destination_Zone]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Minimum_Charge]       [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Basic_Charge]         [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel_Override]        [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel_Percentage]      [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_1]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_2]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_3]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_4]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_5]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_6]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_7]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_8]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_9]           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Rounding_10]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Charge_1]             [float] NOT NULL,
		[Charge_2]             [int] NOT NULL,
		[Charge_3]             [int] NOT NULL,
		[Charge_4]             [int] NOT NULL,
		[Charge_5]             [int] NOT NULL,
		[Charge_6]             [int] NOT NULL,
		[Charge_7]             [int] NOT NULL,
		[Charge_8]             [int] NOT NULL,
		[Charge_9]             [int] NOT NULL,
		[Charge_10]            [int] NOT NULL,
		[Break_1]              [float] NOT NULL,
		[Break_2]              [float] NOT NULL,
		[Break_3]              [float] NOT NULL,
		[Break_4]              [float] NOT NULL,
		[Break_5]              [float] NOT NULL,
		[Break_6]              [float] NOT NULL,
		[Break_7]              [float] NOT NULL,
		[Break_8]              [float] NOT NULL,
		[Break_9]              [float] NOT NULL,
		[Break_10]             [float] NOT NULL
)
GO
ALTER TABLE [dbo].[Nut Grocer 113142269 110221] SET (LOCK_ESCALATION = TABLE)
GO
