SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Phase2PartACustRatecardheader_HB22102019] (
		[Account]                   [int] NOT NULL,
		[Service]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[From date]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel_Override]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi Override Charge]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi %]                   [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Qty or Weight]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volumetric Divisor]        [int] NULL,
		[Tariff ID]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Phase2PartACustRatecardheader_HB22102019] SET (LOCK_ESCALATION = TABLE)
GO
