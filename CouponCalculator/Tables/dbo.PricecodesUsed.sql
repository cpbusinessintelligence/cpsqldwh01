SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PricecodesUsed] (
		[Code]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Name]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Category]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PricecodesUsed] SET (LOCK_ESCALATION = TABLE)
GO
