SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prontoloadratesv1] (
		[Id]                          [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[zone-from]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[zone-to]                     [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[Minimum]                     [varchar](7) COLLATE Latin1_General_CI_AS NULL,
		[Basic]                       [varchar](7) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Surcharge]              [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Surcharge Override]     [varchar](9) COLLATE Latin1_General_CI_AS NULL,
		[rounding 1]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 2]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 3]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 4]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 5]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 6]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 7]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 8]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 9]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[rounding 10]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[charges 1]                   [varchar](9) COLLATE Latin1_General_CI_AS NULL,
		[charges 2]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 3]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 4]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 5]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 6]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 7]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 8]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 9]                   [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[charges 10]                  [varchar](52) COLLATE Latin1_General_CI_AS NULL,
		[breaks 1]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 2]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 3]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 4]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 5]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 6]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 7]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 8]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 9]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[breaks 10]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Prontoloadratesv1] SET (LOCK_ESCALATION = TABLE)
GO
