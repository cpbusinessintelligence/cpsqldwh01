SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RateCardDetail_Customized] (
		[RateCardID]                 [int] NOT NULL,
		[RateCardCode]               [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[NewRateCardCode]            [varchar](101) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                   [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]                     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[FreightMinimumCharge]       [decimal](15, 4) NOT NULL,
		[FreightBaseCharge]          [decimal](15, 4) NOT NULL,
		[IsItemRate]                 [bit] NOT NULL,
		[ItemRate]                   [decimal](15, 4) NOT NULL,
		[NumberWtBreaks]             [int] NOT NULL,
		[FirstWeightBreak]           [decimal](15, 4) NOT NULL,
		[SubsequentWeightBreak]      [decimal](15, 4) NOT NULL,
		[ChargePerKiloWtBreak1]      [decimal](15, 4) NOT NULL,
		[ChargePerKiloWtBrerak2]     [decimal](15, 4) NOT NULL,
		[FuelOverRide]               [bit] NOT NULL,
		[FuelPercentage]             [decimal](15, 4) NOT NULL,
		[CreatedDateTime]            [datetime] NOT NULL,
		[CreatedBy]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[UpdatedDateTime]            [datetime] NULL,
		[UpdatedBy]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RateCardDetail_Customized]
	ADD
	CONSTRAINT [DF__RateCardD__Creat__405A880E]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[RateCardDetail_Customized] SET (LOCK_ESCALATION = TABLE)
GO
