SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RateCardEquivalent] (
		[ratecard]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceEq]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RateCardEquivalent] SET (LOCK_ESCALATION = TABLE)
GO
