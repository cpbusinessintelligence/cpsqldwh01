SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STD header 2020] (
		[Service]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Valid_From]             [datetime2](7) NOT NULL,
		[Fuel_Override]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi]                  [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Charge_Method]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Volumetric_Divisor]     [int] NULL,
		[Tariff_Id]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Log_who]                [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Log_date]               [datetime2](7) NOT NULL
)
GO
ALTER TABLE [dbo].[STD header 2020] SET (LOCK_ESCALATION = TABLE)
GO
