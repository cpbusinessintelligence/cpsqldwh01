SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Service] (
		[ServiceCode]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[From_Zone]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[To_Zone]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Service] SET (LOCK_ESCALATION = TABLE)
GO
