SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SilBag] (
		[Account]                     [float] NULL,
		[OldCode]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NewCode]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date]                        [datetime] NULL,
		[Id]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[zone-from]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[zone-to]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Minimum]                     [float] NULL,
		[Basic]                       [float] NULL,
		[Fuel Surcharge]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Surcharge Override]     [float] NULL,
		[rounding 1]                  [float] NULL,
		[rounding 2]                  [float] NULL,
		[rounding 3]                  [float] NULL,
		[rounding 4]                  [float] NULL,
		[rounding 5]                  [float] NULL,
		[rounding 6]                  [float] NULL,
		[rounding 7]                  [float] NULL,
		[rounding 8]                  [float] NULL,
		[rounding 9]                  [float] NULL,
		[rounding 10]                 [float] NULL,
		[charges 1]                   [float] NULL,
		[charges 2]                   [float] NULL,
		[charges 3]                   [float] NULL,
		[charges 4]                   [float] NULL,
		[charges 5]                   [float] NULL,
		[charges 6]                   [float] NULL,
		[charges 7]                   [float] NULL,
		[charges 8]                   [float] NULL,
		[charges 9]                   [float] NULL,
		[charges 10]                  [float] NULL,
		[breaks 1]                    [float] NULL,
		[breaks 2]                    [float] NULL,
		[breaks 3]                    [float] NULL,
		[breaks 4]                    [float] NULL,
		[breaks 5]                    [float] NULL,
		[breaks 6]                    [float] NULL,
		[breaks 7]                    [float] NULL,
		[breaks 8]                    [float] NULL,
		[breaks 9]                    [float] NULL,
		[breaks 10]                   [float] NULL
)
GO
ALTER TABLE [dbo].[SilBag] SET (LOCK_ESCALATION = TABLE)
GO
