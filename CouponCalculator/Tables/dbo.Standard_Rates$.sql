SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Standard_Rates$] (
		[Service Code]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Lookup]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Minimum]                  [float] NULL,
		[Base ]                    [float] NULL,
		[Per Kilo / Per Item]      [float] NULL,
		[Flat rate per 25kg]       [float] NULL,
		[Pronto_Minimum]           [float] NULL,
		[Pronto_Basic]             [float] NULL,
		[Pronto_FuelSurcharge]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_rounding1]         [float] NULL,
		[Pronto_charges1]          [float] NULL
)
GO
ALTER TABLE [dbo].[Standard_Rates$] SET (LOCK_ESCALATION = TABLE)
GO
