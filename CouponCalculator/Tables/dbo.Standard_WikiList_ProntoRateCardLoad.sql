SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Standard_WikiList_ProntoRateCardLoad] (
		[Service]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Valid From]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge Method]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volumetric Divisor]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff Id]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Log who]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Log date]               [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Standard_WikiList_ProntoRateCardLoad] SET (LOCK_ESCALATION = TABLE)
GO
