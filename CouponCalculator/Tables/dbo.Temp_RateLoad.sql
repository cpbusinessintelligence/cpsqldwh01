SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_RateLoad] (
		[Id]                          [nvarchar](257) COLLATE Latin1_General_CI_AS NULL,
		[zone-from]                   [nvarchar](257) COLLATE Latin1_General_CI_AS NULL,
		[zone-to]                     [nvarchar](257) COLLATE Latin1_General_CI_AS NULL,
		[Minimum]                     [varchar](7) COLLATE Latin1_General_CI_AS NULL,
		[Basic]                       [varchar](7) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Surcharge]              [nvarchar](257) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Surcharge Override]     [varchar](9) COLLATE Latin1_General_CI_AS NULL,
		[rounding 1]                  [decimal](12, 2) NULL,
		[rounding 2]                  [decimal](12, 2) NULL,
		[rounding 3]                  [decimal](12, 2) NULL,
		[rounding 4]                  [decimal](12, 2) NULL,
		[rounding 5]                  [decimal](12, 2) NULL,
		[rounding 6]                  [decimal](12, 2) NULL,
		[rounding 7]                  [decimal](12, 2) NULL,
		[rounding 8]                  [decimal](12, 2) NULL,
		[rounding 9]                  [decimal](12, 2) NULL,
		[rounding 10]                 [decimal](12, 2) NULL,
		[charges 1]                   [varchar](9) COLLATE Latin1_General_CI_AS NULL,
		[charges 2]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 3]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 4]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 5]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 6]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 7]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 8]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 9]                   [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[charges 10]                  [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[breaks 1]                    [numeric](10, 2) NOT NULL,
		[breaks 2]                    [numeric](10, 2) NOT NULL,
		[breaks 3]                    [numeric](10, 2) NOT NULL,
		[breaks 4]                    [numeric](10, 2) NOT NULL,
		[breaks 5]                    [numeric](10, 2) NOT NULL,
		[breaks 6]                    [numeric](10, 2) NOT NULL,
		[breaks 7]                    [numeric](10, 2) NOT NULL,
		[breaks 8]                    [numeric](10, 2) NOT NULL,
		[breaks 9]                    [numeric](10, 2) NOT NULL,
		[breaks 10]                   [numeric](10, 2) NOT NULL
)
GO
ALTER TABLE [dbo].[Temp_RateLoad] SET (LOCK_ESCALATION = TABLE)
GO
