SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_Zonesv1] (
		[ZoneID]        [float] NULL,
		[Category]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ZoneCode]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ZoneName]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Priority]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[F7]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Temp_Zonesv1] SET (LOCK_ESCALATION = TABLE)
GO
