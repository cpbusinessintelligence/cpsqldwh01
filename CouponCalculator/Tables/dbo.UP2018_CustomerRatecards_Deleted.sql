SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UP2018_CustomerRatecards_Deleted] (
		[Accountcode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UP2018_CustomerRatecards_Deleted] SET (LOCK_ESCALATION = TABLE)
GO
