SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UP2018_CustomerZones] (
		[AccountCode]           [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[RevenueOriginZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UP2018_CustomerZones] SET (LOCK_ESCALATION = TABLE)
GO
