SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UP2019_ActiveZones] (
		[PrizeZone]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Category]      [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UP2019_ActiveZones] SET (LOCK_ESCALATION = TABLE)
GO
