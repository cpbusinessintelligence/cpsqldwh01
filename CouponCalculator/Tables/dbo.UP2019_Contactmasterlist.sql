SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UP2019_Contactmasterlist] (
		[State]               [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]             [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]     [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]         [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]              [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Prontoname]          [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Address1]            [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[CustomerEmail]       [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[TARIFF]              [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RepEMail]            [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ToEmail]             [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[CCEMail]             [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[BccEMail]            [nvarchar](262) COLLATE Latin1_General_CI_AS NULL,
		[EmailBody]           [varchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UP2019_Contactmasterlist] SET (LOCK_ESCALATION = TABLE)
GO
