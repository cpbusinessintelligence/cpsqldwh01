SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UP2019_Gatewaymasterlist] (
		[State]                    [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]                  [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]          [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Billing_Code]             [int] NULL,
		[Child_Account_Code]       [int] NULL,
		[Pronto_Name]              [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_Name]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_Code_Created]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_ID]               [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Tariff_Account_Code]      [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Software]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Mapping]                  [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Address1]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Address2]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Address3]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Email]                    [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Sales_Rep_Email_Add]      [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Controlled_Logistics]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Current_Service_Code]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[New_Service_Code]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[increase_2018_2019]       [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Effective__Date]          [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Approved_by]              [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Date_of_Approval]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Stand_Cust]               [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Stand_Cust_type]          [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Batch1_17_18]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UP2019_Gatewaymasterlist] SET (LOCK_ESCALATION = TABLE)
GO
