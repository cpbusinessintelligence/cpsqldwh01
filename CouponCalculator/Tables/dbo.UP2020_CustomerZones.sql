SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UP2020_CustomerZones] (
		[accountcode]           [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[RevenueOriginZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UP2020_CustomerZones] SET (LOCK_ESCALATION = TABLE)
GO
