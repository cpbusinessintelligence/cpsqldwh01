SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Up2019_MissingStdRates_bup15082019] (
		[ServiceCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[TOZone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Up2019_MissingStdRates_bup15082019] SET (LOCK_ESCALATION = TABLE)
GO
