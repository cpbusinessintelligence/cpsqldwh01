SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UpdateLetterGeneral] (
		[CustomerName]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[EmailorMobile]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[State]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UpdateLetterGeneral] SET (LOCK_ESCALATION = TABLE)
GO
