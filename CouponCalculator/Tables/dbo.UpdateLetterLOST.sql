SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UpdateLetterLOST] (
		[CustomerName]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[EmailorMobile]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[ManagerName]       [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[State]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[UpdateLetterLOST] SET (LOCK_ESCALATION = TABLE)
GO
