SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_CUSAccountSummary2020] (
		[state]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Repcode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[territory]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]              [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[address1]            [int] NULL,
		[CustomerEmail]       [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[Tariff]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepEMail]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToEmail]             [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[CCEMail]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BccEMail]            [varchar](28) COLLATE Latin1_General_CI_AS NOT NULL,
		[EmailBody]           [varchar](7381) COLLATE Latin1_General_CI_AS NOT NULL,
		[ContactName]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Designation]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Repname]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Phase]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_CUSAccountSummary2020] SET (LOCK_ESCALATION = TABLE)
GO
