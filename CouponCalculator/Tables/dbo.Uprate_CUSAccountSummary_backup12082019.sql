SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_CUSAccountSummary_backup12082019] (
		[State]               [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]             [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]     [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Territory]           [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]         [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]              [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]           [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Address1]            [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[CustomerEmail]       [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[TARIFF]              [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RepEMail]            [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[ToEmail]             [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[CCEMail]             [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[BccEMail]            [nvarchar](262) COLLATE Latin1_General_CI_AS NULL,
		[EmailBody]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ContactName]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Designation]         [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[RepName]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_CUSAccountSummary_backup12082019] SET (LOCK_ESCALATION = TABLE)
GO
