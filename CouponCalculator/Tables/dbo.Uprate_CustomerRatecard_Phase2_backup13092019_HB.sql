SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_CustomerRatecard_Phase2_backup13092019_HB] (
		[asdf]                    [float] NULL,
		[Accountcode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto Shortname]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Gateway Shortname]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NewServiceCode]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Effective  Date]         [datetime] NULL,
		[OldServiceCode]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_CustomerRatecard_Phase2_backup13092019_HB] SET (LOCK_ESCALATION = TABLE)
GO
