SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_CustomerRatecards2020] (
		[Accountcode]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                  [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Tariff_Account_Code]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective Date]          [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[OldServiceCode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]             [int] NULL,
		[Date of Approval]        [int] NULL,
		[Percentage]              [int] NULL
)
GO
ALTER TABLE [dbo].[Uprate_CustomerRatecards2020] SET (LOCK_ESCALATION = TABLE)
GO
