SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_CustomerRatecards_backup17092019_HB] (
		[Accountcode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]               [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Effective  Date]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OldServiceCode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_CustomerRatecards_backup17092019_HB] SET (LOCK_ESCALATION = TABLE)
GO
