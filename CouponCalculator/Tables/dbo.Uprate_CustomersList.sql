SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_CustomersList] (
		[Accountcode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[% increase 2017-2018]     [varchar](150) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_CustomersList] SET (LOCK_ESCALATION = TABLE)
GO
