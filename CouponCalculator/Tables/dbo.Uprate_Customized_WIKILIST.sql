SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_Customized_WIKILIST] (
		[Accountcode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]                [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[% increase 2017-2018]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Decision]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Comments]                 [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_Customized_WIKILIST] SET (LOCK_ESCALATION = TABLE)
GO
