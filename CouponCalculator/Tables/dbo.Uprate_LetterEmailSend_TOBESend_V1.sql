SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_LetterEmailSend_TOBESend_V1] (
		[State]                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]              [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]      [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Territory]            [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]          [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]               [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]            [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Address1]             [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Email]                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[TARIFF]               [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[SalesPersonEmail]     [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BccEmail]             [varchar](262) COLLATE Latin1_General_CI_AS NULL,
		[Contact Name]         [varchar](62) COLLATE Latin1_General_CI_AS NULL,
		[Remarks]              [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[EMail_Gemma]          [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[EMail_Template]       [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Categrory]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_LetterEmailSend_TOBESend_V1] SET (LOCK_ESCALATION = TABLE)
GO
