SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_MasterListv2] (
		[Count]                                 [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[State]                                 [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]                               [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]                       [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Territory]                             [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]                           [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]                             [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Address1]                              [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[GatewayName]                           [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[GatewayCode]                           [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Email]                                 [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[FirstBillingDate]                      [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[LastBillingDate]                       [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[GW - Jobins List]                      [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Software]                              [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Rate Card Description]                 [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Margin]                                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Service Code (Combined GW&Pronto)]     [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[New Service Code]                      [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[% increase 2017-2018]                  [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Effective  Date]                       [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]                           [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]                      [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Tariff]                                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[TRUE FALSE]                            [varchar](150) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_MasterListv2] SET (LOCK_ESCALATION = TABLE)
GO
