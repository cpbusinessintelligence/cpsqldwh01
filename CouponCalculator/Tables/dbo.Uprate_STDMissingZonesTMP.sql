SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_STDMissingZonesTMP] (
		[AccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_STDMissingZonesTMP] SET (LOCK_ESCALATION = TABLE)
GO
