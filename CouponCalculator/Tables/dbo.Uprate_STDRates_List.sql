SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_STDRates_List] (
		[Count]                                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]                               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]                             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]                             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address1]                              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GatewayName]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GatewayCode]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Email]                                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FirstBillingDate]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastBillingDate]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GW - Jobins List]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Software]                              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rate Card Description]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Margin]                                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service Code (Combined GW&Pronto)]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New Service Code]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[% increase 2017-2018]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective  Date]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff]                                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TRUE FALSE]                            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_STDRates_List] SET (LOCK_ESCALATION = TABLE)
GO
