SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_SalesRepDetails] (
		[OriginalRepName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Emaill]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]           [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_SalesRepDetails] SET (LOCK_ESCALATION = TABLE)
GO
