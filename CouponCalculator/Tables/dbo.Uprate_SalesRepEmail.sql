SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_SalesRepEmail] (
		[Rep Name]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EmailID]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_SalesRepEmail] SET (LOCK_ESCALATION = TABLE)
GO
