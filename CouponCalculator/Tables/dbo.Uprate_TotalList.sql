SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_TotalList] (
		[State]                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]              [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]      [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Territory]            [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]          [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]               [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]            [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Address1]             [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Email]                [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[TARIFF]               [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[SalesPersonEmail]     [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BccEmail]             [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Contact Name]         [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Remarks]              [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[EMail_Gemma]          [varchar](150) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_TotalList] SET (LOCK_ESCALATION = TABLE)
GO
