SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_Wikilist3] (
		[Accountcode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                           [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]                        [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[% increase 2017-2018]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]                         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Comment]                          [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Action before emailing rates]     [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_Wikilist3] SET (LOCK_ESCALATION = TABLE)
GO
