SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uprate_ZoneCombinations] (
		[FromZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]       [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Uprate_ZoneCombinations] SET (LOCK_ESCALATION = TABLE)
GO
