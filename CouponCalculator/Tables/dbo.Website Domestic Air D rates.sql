SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Website Domestic Air D rates] (
		[Account]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective_Date]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff_ID]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Origin_Zone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Destination_Zone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Minimum_Charge]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Basic_Charge]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel_Override]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel_Percentage]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_1]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_2]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_3]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_4]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_5]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_6]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_7]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_8]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_9]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding_10]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_1]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_2]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_3]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_4]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_5]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_6]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_7]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_8]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_9]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_10]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_1]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_2]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_3]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_4]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_5]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_6]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_7]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_8]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_9]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break_10]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Website Domestic Air D rates] SET (LOCK_ESCALATION = TABLE)
GO
