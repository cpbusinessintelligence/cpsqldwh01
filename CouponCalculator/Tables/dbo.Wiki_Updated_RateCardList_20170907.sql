SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Wiki_Updated_RateCardList_20170907] (
		[Accountcode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective  Date]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OldServiceCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Action]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Wiki_Updated_RateCardList_20170907] SET (LOCK_ESCALATION = TABLE)
GO
