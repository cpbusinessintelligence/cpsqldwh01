SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_CouponCalculator_SS03042019] (
		[CouponCalculatoID]     [int] NOT NULL,
		[FromZone]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[WB1Count]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[WB2Count]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[WB3Count]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[SatchelCount]          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]           [datetime] NULL,
		[EditWho]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]          [datetime] NULL,
		[RedirectionFee]        [decimal](12, 2) NULL
)
GO
ALTER TABLE [dbo].[Z_CouponCalculator_SS03042019] SET (LOCK_ESCALATION = TABLE)
GO
