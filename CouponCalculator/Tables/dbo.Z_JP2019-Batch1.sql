SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_JP2019-Batch1] (
		[Billing Code]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Child Account Code]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff Account Code]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Current Service Code]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New Service Code]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[% increase 2018-2019]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Batch 1 or Batch 2]       [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_JP2019-Batch1] SET (LOCK_ESCALATION = TABLE)
GO
