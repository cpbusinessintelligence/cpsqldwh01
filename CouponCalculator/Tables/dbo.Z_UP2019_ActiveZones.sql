SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_UP2019_ActiveZones] (
		[PrizefromZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrizeToZone]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Category]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_UP2019_ActiveZones] SET (LOCK_ESCALATION = TABLE)
GO
