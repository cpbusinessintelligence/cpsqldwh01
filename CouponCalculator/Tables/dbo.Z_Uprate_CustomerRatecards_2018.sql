SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_Uprate_CustomerRatecards_2018] (
		[AccountCode]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Effective date]          [varchar](11) COLLATE Latin1_General_CI_AS NOT NULL,
		[OldServiceCode]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_Uprate_CustomerRatecards_2018] SET (LOCK_ESCALATION = TABLE)
GO
