SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_Uprate_CustomerRatecards_backup13082018] (
		[Accountcode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Effective  Date]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OldServiceCode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_Uprate_CustomerRatecards_backup13082018] SET (LOCK_ESCALATION = TABLE)
GO
