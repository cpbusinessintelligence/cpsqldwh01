SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_Zone to Zone Coupons Calculator - 2015-07-23] (
		[Lookup]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WB1]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WB2]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WB3]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Satchel]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_Zone to Zone Coupons Calculator - 2015-07-23] SET (LOCK_ESCALATION = TABLE)
GO
