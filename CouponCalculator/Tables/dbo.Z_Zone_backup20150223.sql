SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_Zone_backup20150223] (
		[ZoneID]           [int] NOT NULL,
		[Category]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ZoneCode]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ZoneName]         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Priority]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddWho]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]      [datetime] NOT NULL,
		[EditWho]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[Z_Zone_backup20150223] SET (LOCK_ESCALATION = TABLE)
GO
