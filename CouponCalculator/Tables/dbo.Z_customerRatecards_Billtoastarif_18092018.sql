SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_customerRatecards_Billtoastarif_18092018] (
		[Accountcode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[BillTo]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntShortname]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Shortname]               [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RateCardCategory]        [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceCode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RateCardDescription]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Effective__Date]         [datetime2](7) NOT NULL,
		[OldServiceCode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Approved_by]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Date_of_Approval]        [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Percentage]              [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Z_customerRatecards_Billtoastarif_18092018] SET (LOCK_ESCALATION = TABLE)
GO
