SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tempcustomisedrate_wikki_13092018] (
		[Account]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Effective Date]       [datetime] NULL,
		[Tariff ID]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Origin Zone]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Destination Zone]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Minimum Charge]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Basic Charge]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 1]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 2]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 3]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 4]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 5]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 6]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 7]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 8]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 9]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 10]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 1]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 2]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 3]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 4]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 5]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 6]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 7]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 8]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 9]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 10]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 1]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 2]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 3]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 4]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 5]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 6]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 7]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 8]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 9]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 10]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_tempcustomisedrate_wikki_13092018] SET (LOCK_ESCALATION = TABLE)
GO
