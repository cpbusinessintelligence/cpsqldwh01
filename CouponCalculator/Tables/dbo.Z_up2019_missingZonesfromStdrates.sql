SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_up2019_missingZonesfromStdrates] (
		[Allzone_service]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Allzone_fromzone]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Allzone_tozone]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_service]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_originZone]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_destinationzone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_up2019_missingZonesfromStdrates] SET (LOCK_ESCALATION = TABLE)
GO
