SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zone] (
		[ZoneID]           [int] NOT NULL,
		[Category]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ZoneCode]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ZoneName]         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Priority]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddWho]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]      [datetime] NOT NULL,
		[EditWho]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]     [datetime] NULL,
		CONSTRAINT [PK__Zone__601667952491A4AE]
		PRIMARY KEY
		CLUSTERED
		([ZoneID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Zone]
	ADD
	CONSTRAINT [DF_Zone_AddDateTime]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[Zone]
	ADD
	CONSTRAINT [DF_Zone_EditDateTime]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_Zone]
	ON [dbo].[Zone] ([ZoneCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Zone] SET (LOCK_ESCALATION = TABLE)
GO
