SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZoneDescriptions] (
		[ZoneCode]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Description]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ZoneDescription]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ZoneDescriptions] SET (LOCK_ESCALATION = TABLE)
GO
