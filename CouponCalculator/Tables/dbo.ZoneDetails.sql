SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZoneDetails] (
		[ZoneCode]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ZoneDescription]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ZoneDetails] SET (LOCK_ESCALATION = TABLE)
GO
