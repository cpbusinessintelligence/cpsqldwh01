SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zones_Pronto_Mapping] (
		[Zone]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Description]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Priority]           [float] NULL,
		[Warehouse]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[State]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ZoneEquavilant]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Area1]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Area2]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP Zone ]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[WorksheetZone]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Ordering]           [float] NULL
)
GO
ALTER TABLE [dbo].[Zones_Pronto_Mapping] SET (LOCK_ESCALATION = TABLE)
GO
