SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[changes] (
		[Account]              [int] NULL,
		[Service]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective_Date]       [datetime2](7) NULL,
		[Tariff_ID]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Origin_Zone]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Destination_Zone]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Minimum_Charge]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Basic_Charge]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel_Override]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel_Percentage]      [int] NULL,
		[Rounding_1]           [int] NULL,
		[Rounding_2]           [int] NULL,
		[Rounding_3]           [int] NULL,
		[Rounding_4]           [int] NULL,
		[Rounding_5]           [int] NULL,
		[Rounding_6]           [int] NULL,
		[Rounding_7]           [int] NULL,
		[Rounding_8]           [int] NULL,
		[Rounding_9]           [int] NULL,
		[Rounding_10]          [int] NULL,
		[Charge_1]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge_2]             [int] NULL,
		[Charge_3]             [int] NULL,
		[Charge_4]             [int] NULL,
		[Charge_5]             [int] NULL,
		[Charge_6]             [int] NULL,
		[Charge_7]             [int] NULL,
		[Charge_8]             [int] NULL,
		[Charge_9]             [int] NULL,
		[Charge_10]            [int] NULL,
		[Break_1]              [float] NULL,
		[Break_2]              [float] NULL,
		[Break_3]              [float] NULL,
		[Break_4]              [float] NULL,
		[Break_5]              [float] NULL,
		[Break_6]              [float] NULL,
		[Break_7]              [float] NULL,
		[Break_8]              [float] NULL,
		[Break_9]              [float] NULL,
		[Break_10]             [float] NULL
)
GO
ALTER TABLE [dbo].[changes] SET (LOCK_ESCALATION = TABLE)
GO
