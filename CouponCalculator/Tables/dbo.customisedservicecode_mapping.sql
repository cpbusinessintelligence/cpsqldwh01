SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customisedservicecode_mapping] (
		[Service]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Q_W]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[330_unique_codes]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[customisedservicecode_mapping] SET (LOCK_ESCALATION = TABLE)
GO
