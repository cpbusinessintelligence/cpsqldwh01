SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[effectivedate_05102020_change] (
		[Child_Account_Code]      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Tariff_Account_Code]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[effectivedate_05102020_change] SET (LOCK_ESCALATION = TABLE)
GO
