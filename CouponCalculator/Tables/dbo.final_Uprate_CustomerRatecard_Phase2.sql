SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[final_Uprate_CustomerRatecard_Phase2] (
		[Account]                 [int] NULL,
		[Service]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ratecarddescription]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective__Date]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff ID]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Origin Zone]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Destination Zone]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Minimum Charge]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Basic Charge]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 1]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 2]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 3]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 4]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 5]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 6]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 7]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 8]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 9]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rounding 10]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 1]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 2]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 3]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 4]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 5]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 6]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 7]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 8]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 9]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge 10]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 1]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 2]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 3]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 4]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 5]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 6]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 7]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 8]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 9]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Break 10]                [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[final_Uprate_CustomerRatecard_Phase2] SET (LOCK_ESCALATION = TABLE)
GO
