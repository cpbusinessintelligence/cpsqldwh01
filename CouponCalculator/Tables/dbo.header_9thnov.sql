SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[header_9thnov] (
		[Account]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[From date]                 [datetime] NULL,
		[Fuel Override]             [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel Percentage]           [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi Override Charge]     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi %]                   [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Qty or Weight]             [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[Volumetric Divisor]        [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[ID]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[header_9thnov] SET (LOCK_ESCALATION = TABLE)
GO
