SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[header_Phase2PartA] (
		[AccountCode]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[From date]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi Override Charge]     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi %]                   [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Qty or Weight]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Volumetric Divisor]        [int] NULL,
		[ID]                        [nvarchar](122) COLLATE Latin1_General_CI_AS NULL,
		[Tariff]                    [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[header_Phase2PartA] SET (LOCK_ESCALATION = TABLE)
GO
