SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[missingrevenuezone] (
		[Child_Account_Code]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Ex_Zone]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[missingrevenuezone] SET (LOCK_ESCALATION = TABLE)
GO
