SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[phase2PartAstdheader_22102019HB] (
		[Account]                   [int] NOT NULL,
		[Service]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[From date]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Fuel_Override]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi Override Charge]     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi %]                   [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Qty or Weight]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volumetric Divisor]        [int] NULL,
		[Tariff ID]                 [nvarchar](76) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[phase2PartAstdheader_22102019HB] SET (LOCK_ESCALATION = TABLE)
GO
