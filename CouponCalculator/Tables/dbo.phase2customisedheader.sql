SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[phase2customisedheader] (
		[Account]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New_Service_Code]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[From date]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Override]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fuel Percentage]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Multi Override Charge]     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Multi %]                   [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Qty or Weight]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Volumetric Divisor]        [int] NULL,
		[ID]                        [nvarchar](122) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[phase2customisedheader] SET (LOCK_ESCALATION = TABLE)
GO
