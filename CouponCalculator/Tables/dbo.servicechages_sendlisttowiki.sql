SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[servicechages_sendlisttowiki] (
		[Billing_Code]                           [int] NOT NULL,
		[Child_Account_Code]                     [int] NOT NULL,
		[Pronto_Name]                            [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Gateway_Name]                           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Tariff_Account_Code]                    [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[a]                                      [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Current_Service_Code__From_Batch1_]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[New_S_Code_Batch_2]                     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[__increase_2018_2019]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Cust_type]                              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[servicechages_sendlisttowiki] SET (LOCK_ESCALATION = TABLE)
GO
