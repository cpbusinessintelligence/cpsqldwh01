SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[std_customersemail] (
		[column1]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[column2]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[std_customersemail] SET (LOCK_ESCALATION = TABLE)
GO
