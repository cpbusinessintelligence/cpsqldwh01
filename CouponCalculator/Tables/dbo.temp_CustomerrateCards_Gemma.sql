SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_CustomerrateCards_Gemma] (
		[Sno]                     [float] NULL,
		[Accountcode]             [float] NULL,
		[BillTo]                  [float] NULL,
		[Shortname]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Shortname1]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Effective  Date]         [datetime] NULL,
		[OldServiceCode]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Approved by]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date of Approval]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[temp_CustomerrateCards_Gemma] SET (LOCK_ESCALATION = TABLE)
GO
