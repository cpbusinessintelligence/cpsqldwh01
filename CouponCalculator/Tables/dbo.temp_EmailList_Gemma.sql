SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_EmailList_Gemma] (
		[Accountcode]         [float] NULL,
		[BillTo]              [float] NULL,
		[Shortname]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[State]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Territory]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address1]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CustomerEmail]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[TARIFF]              [float] NULL,
		[RepEMail]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ToEmail]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CCEMail]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Emailbody]           [varchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[temp_EmailList_Gemma] SET (LOCK_ESCALATION = TABLE)
GO
