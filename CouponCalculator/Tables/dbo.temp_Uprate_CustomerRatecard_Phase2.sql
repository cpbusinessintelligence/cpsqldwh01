SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_Uprate_CustomerRatecard_Phase2] (
		[Accountcode]             [int] NULL,
		[BillTo]                  [int] NULL,
		[Shortname]               [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RateCardCategory]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RateCardDescription]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective__Date]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OldServiceCode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Approved_by]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date_of_Approval]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Percentage]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TariffAccount]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[temp_Uprate_CustomerRatecard_Phase2] SET (LOCK_ESCALATION = TABLE)
GO
