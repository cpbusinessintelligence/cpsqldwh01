SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_customeremails] (
		[Accountcode]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Contact Email]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[temp_customeremails] SET (LOCK_ESCALATION = TABLE)
GO
