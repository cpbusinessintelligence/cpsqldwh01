SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[up2018_contactmasterlist] (
		[State]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Code]                             [float] NULL,
		[Pronto Name]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Gateway Name]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Gateway Code Created]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Tariff Account Code]              [float] NULL,
		[Address1 (some are T/A Name)]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address2]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address3]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Email]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[up2018_contactmasterlist] SET (LOCK_ESCALATION = TABLE)
GO
