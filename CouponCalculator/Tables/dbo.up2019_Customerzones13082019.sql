SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[up2019_Customerzones13082019] (
		[AccountCOde]           [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[RevenueOriginZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[up2019_Customerzones13082019] SET (LOCK_ESCALATION = TABLE)
GO
