SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[updated_masterlist] (
		[State]                    [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Billing_Code]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Child_Account_Code]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_Name]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_Name]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_Code_Created]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_ID]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff_Account_Code]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Email]                    [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sales_Rep_Email_Add]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Desc]                     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Current_Service_Code]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New_Service_Code]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Heena_s___Increase]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[updated_masterlist] SET (LOCK_ESCALATION = TABLE)
GO
