SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[uprate2019_phase2] (
		[State]                            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]                          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Billing_Code]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Child_Account_Code]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_Name]                      [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_Name]                     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_Code_Created]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Gateway_ID]                       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tariff_Account_Code]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Software]                         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Mapping]                          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address1__some_are_T_A_Name_]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Address2]                         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Address3]                         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Email]                            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Sales_Rep_Email_Add]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Controlled_Logistics]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Current_Service_Code]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New_Service_Code]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[__increase_2018_2019]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Effective__Date]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Approved_by]                      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date_of_Approval]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Stand_Cust_type]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Stand_Cust]                       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[__inc_List_of_SC_in_GW]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[uprate2019_phase2] SET (LOCK_ESCALATION = TABLE)
GO
