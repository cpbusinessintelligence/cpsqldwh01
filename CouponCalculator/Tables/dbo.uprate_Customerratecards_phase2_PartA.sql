SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[uprate_Customerratecards_phase2_PartA] (
		[Accountcode]             [int] NOT NULL,
		[BillTo]                  [int] NOT NULL,
		[Shortname]               [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RateCardCategory]        [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceCode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RateCardDescription]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Effective__Date]         [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[OldServiceCode]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Approved_by]             [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Date_of_Approval]        [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Percentage]              [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Tariff]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[uprate_Customerratecards_phase2_PartA] SET (LOCK_ESCALATION = TABLE)
GO
