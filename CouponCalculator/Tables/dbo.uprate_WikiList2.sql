SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[uprate_WikiList2] (
		[Accountcode]                      [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]                           [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]                        [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[% increase 2017-2018]             [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]                         [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Comment]                          [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Action before emailing rates]     [varchar](150) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[uprate_WikiList2] SET (LOCK_ESCALATION = TABLE)
GO
