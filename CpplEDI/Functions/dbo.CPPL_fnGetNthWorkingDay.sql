SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_fnGetNthWorkingDay](@StartDate DATETIME, @DaysToAdd INT, @CountToday INT)
RETURNS DATE
     AS
  BEGIN --------------------------------------------------------------------------------------
-- RETURN (
--         SELECT d.EndDate 
--              + CASE WHEN DATENAME(dw,d.EndDate) = 'Saturday' THEN 2
--                     WHEN DATENAME(dw,d.EndDate) = 'Sunday'   THEN 1  
--                     ELSE 0
--                END AS EndDate 
--           FROM (SELECT DATEADD(wk,@DaysToAdd/5, @StartDate
--                                               + CASE WHEN DATENAME(dw,@StartDate) = 'Saturday' THEN 2 
--                                                      WHEN DATENAME(dw,@StartDate) = 'Sunday'   THEN 1
--                                                      ELSE 0 
--                                                 END
--                                               + @DaysToAdd%5-@CountToday
--) AS EndDate
--)d
--) --End of Return


DECLARE @nDate datetime, @addsub int
  SET @ndate = @StartDate
  IF @DaysToAdd > 0
    SET @addsub = 1
  ELSE
    SET @addsub = -1
   WHILE @DaysToAdd <> 0 -- Keep adding/subtracting a day until @bdays becomes 0
    BEGIN
       SELECT @ndate = dateadd(day,1*@addsub,@ndate)
      SELECT @DaysToAdd =
      CASE datepart(weekday,@ndate)-- ignore if it is Sat or Sunday        WHEN 1 THEN @bdays
        WHEN 7 THEN @DaysToAdd
        WHEN 1 THEN @DaysToAdd
        ELSE @DaysToAdd - 1*@addsub
      END
     END
 RETURN @nDate


    END --End of Function


GO
