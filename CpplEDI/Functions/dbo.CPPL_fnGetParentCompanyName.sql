SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[CPPL_fnGetParentCompanyName] 
(
	-- Add the parameters for the function here
	@CompanyAccountID as Varchar(20)
)
RETURNS varchar(100)
AS
BEGIN
	-- Declare the return variable here
	
	-- Return the result of the function
	RETURN Isnull((SELECT Top 1 c_name
  FROM [cpplEDI].[dbo].[companies] CMP join [cpplEDI].[dbo].companyaccount CA  ON CMP.c_id = CA.ca_company_id  where  CA.ca_account =@CompanyAccountID),'')

END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_fnGetParentCompanyName]
	TO [ReportUser]
GO
