SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[CPPL_fn_CalculateBusinessDays]
(
	@StartDate				date,
	@EndDate				date,
	@OriginBranchId			uniqueidentifier,
	@DestinationBranchId	uniqueidentifier,
	@CheckPublicHols		bit
)
RETURNS smallint
AS
BEGIN

	DECLARE @BusinessDays smallint;
	SELECT @BusinessDays = -1;
	
	SELECT @CheckPublicHols = ISNULL(@CheckPublicHols, 0);
	
	DECLARE @OriginBranch varchar(20), @DestinationBranch varchar(20);
	SELECT @OriginBranch = '';
	SELECT @DestinationBranch = '';
	
	IF (((@StartDate Is Null) OR (@EndDate Is Null))
			OR
				(@StartDate Is Not Null AND @EndDate Is Not Null
					AND @EndDate < @StartDate))
	BEGIN
		RETURN @BusinessDays;
	END
	ELSE
	BEGIN
	
		IF (@OriginBranchId Is Not Null)
		BEGIN
			SELECT @OriginBranch = ISNULL(CosmosBranch, '')
			FROM ScannerGateway.dbo.Branch
			WHERE Id = @OriginBranchId;
		END
		
		IF (@DestinationBranchId Is Not Null)
		BEGIN
			SELECT @DestinationBranch = ISNULL(CosmosBranch, '')
			FROM ScannerGateway.dbo.Branch
			WHERE Id = @DestinationBranchId;
		END
		
		DECLARE @nextDate date;
		SELECT @nextDate = @StartDate;
		DECLARE @days smallint;
		
		SELECT @days = 0;
		
		WHILE @nextDate < @EndDate
		BEGIN
		
			SELECT @nextDate = DATEADD(day, 1, @nextDate);
			
			IF @CheckPublicHols = 1
			BEGIN
				/*
				-- old public holiday calculation
				--
				IF ((DATEPART(weekday, @nextDate) NOT IN (1,7))
						OR EXISTS
						(
							SELECT 1 FROM RDS.Cosmos.dbo.PublicHoliday
							WHERE HolidayDate = @nextDate
							AND Branch IN (ISNULL(@DestinationBranch, ''))
							AND IsActive = 1
						)
						OR EXISTS
						(
							-- assume holidays in all branches are national holidays
							SELECT 
							DISTINCT(HolidayDate)
							FROM RDS.Cosmos.dbo.PublicHoliday
							WHERE HolidayDate = @nextDate
							AND IsActive = 1
							GROUP BY HolidayDate
							HAVING COUNT(*) >= 5
						)
					)
				BEGIN
					SELECT @days = @days + 1;
				END*/
				
				IF (DATEPART(weekday, @nextDate) NOT IN (1,7))
				BEGIN
					IF @nextDate = @EndDate
					BEGIN
						--
						-- we assume that the date delivery has occurred is not a public holiday
						--
						SELECT @days = @days + 1;
					END
					ELSE
					BEGIN
						IF @nextDate = DATEADD(day, -1, @EndDate)
						BEGIN
							--
							-- check if there's a public holiday specific to the destination, as well as national
							--
							IF (NOT EXISTS (SELECT 1 FROM Cosmos.dbo.PublicHoliday
											WHERE HolidayDate = @nextDate
												AND Branch IN (ISNULL(@DestinationBranch, ''))
													AND IsActive = 1)
									AND NOT EXISTS (SELECT DISTINCT(HolidayDate) FROM Cosmos.dbo.PublicHoliday
													WHERE HolidayDate = @nextDate
														AND IsActive = 1
															GROUP BY HolidayDate
																HAVING COUNT(*) >= 4)) -- should be 5, for all branches
							BEGIN
								-- exclude obvious dates that will always be national holidays
								IF @nextDate NOT IN 
									('1jan2013','25dec2012','26dec2012','26jan2013','28jan2013',
										'25dec2013','26dec2013','1jan2014','26jan2014','27jan2014',
										'25dec2014','26dec2014')
								BEGIN
									SELECT @days = @days + 1;
								END
							END
						END
						ELSE
						BEGIN
							--
							-- check if there's a national holiday (signified by all branches having it configured)
							--
							IF NOT EXISTS (SELECT DISTINCT(HolidayDate) FROM Cosmos.dbo.PublicHoliday
											WHERE HolidayDate = @nextDate
											AND IsActive = 1
											GROUP BY HolidayDate
											HAVING COUNT(*) >= 4) -- should be 5, for all branches
							BEGIN
								SELECT @days = @days + 1;
							END
						END
					END
				END
			END
			ELSE
			BEGIN
				IF (DATEPART(weekday, @nextDate) NOT IN (1,7))
				BEGIN
					SELECT @days = @days + 1;
				END
			END
		
		END
		
		SELECT @BusinessDays = @days;
		RETURN @BusinessDays;
	
	END
	
	RETURN @BusinessDays;

END

GO
