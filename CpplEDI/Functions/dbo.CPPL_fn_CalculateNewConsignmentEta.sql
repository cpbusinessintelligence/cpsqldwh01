SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[CPPL_fn_CalculateNewConsignmentEta]
(
	@StartDate				date,
	@BusinessDays			smallint,
	@DestinationBranchId	uniqueidentifier
)
RETURNS date
AS
BEGIN

	DECLARE @returnDate date;
	DECLARE @counter smallint;
	DECLARE @tmpDate date;
	
	SELECT @returnDate = Null;
	SELECT @counter = 0;
	SELECT @tmpDate = Null;
	
	SELECT @BusinessDays = ISNULL(@BusinessDays, 0);
	
	DECLARE @DestinationBranch varchar(20);
	SELECT @DestinationBranch = '';
	
	IF ((@StartDate Is Null) OR (@BusinessDays < 0))
	BEGIN
		RETURN Null;
	END
	
	IF (@BusinessDays = 0)
	BEGIN
		RETURN @StartDate;
	END
	
	IF (@DestinationBranchId Is Not Null)
	BEGIN
		SELECT @DestinationBranch = ISNULL(CosmosBranch, '')
		FROM ScannerGateway.dbo.Branch
		WHERE Id = @DestinationBranchId;
	END
	
	SELECT @tmpDate = @StartDate;
	
	WHILE (@counter < @BusinessDays)
	BEGIN

		SELECT @tmpDate = DATEADD(day, 1, @tmpDate);
	
		IF (DATEPART(weekday, @tmpDate) NOT IN (1,7))
		BEGIN
		
			-- not a weekend day, check national public holidays
			IF NOT EXISTS (SELECT DISTINCT(HolidayDate) FROM Cosmos.dbo.PublicHoliday
							WHERE HolidayDate = @tmpDate
							AND IsActive = 1
							GROUP BY HolidayDate
							HAVING COUNT(*) >= 4) -- should be 5, for all branches
			BEGIN
				-- exclude obvious dates that will always be national holidays
				IF @tmpDate NOT IN 
					('1jan2013','25dec2012','26dec2012','26jan2013','28jan2013',
						'25dec2013','26dec2013','1jan2014','26jan2014','27jan2014',
						'25dec2014','26dec2014')
				BEGIN
					-- this day is OK to count as a business day
					SELECT @counter = @counter + 1;
				END
			END
		
		END
		
		WHILE (@counter = @BusinessDays)
		BEGIN
		
			-- the last day gets checked for a public holiday at the destination branch
			IF ((EXISTS (SELECT 1 FROM Cosmos.dbo.PublicHoliday
								WHERE HolidayDate = @tmpDate
									AND Branch IN (ISNULL(@DestinationBranch, ''))
										AND IsActive = 1))
			OR
			(
				-- shouldn't be a weekend either
				(DATEPART(weekday, @tmpDate) IN (1,7))
			))
			BEGIN
				-- try the next day
				SELECT @tmpDate = DATEADD(day, 1, @tmpDate);
			END
			ELSE
			BEGIN
				-- current day is OK, so increment the counter to exit the loop
				SELECT @counter = @counter + 1;
			END
		
		END		-- WHILE (@counter = @BusinessDays)
	
	END		-- (@counter < @BusinessDays)

	SELECT @returnDate = @tmpDate;
	RETURN @returnDate;

END

GO
