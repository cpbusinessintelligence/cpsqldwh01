SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_GetPickupStateOrDeliveryState_ByPostcode]
(
	@Postcode Int
)
RETURNS varchar(10)
AS
BEGIN

	DECLARE @ret varchar(15);
	SELECT @ret = Null;
	
	SELECT @ret = CASE 	 When ((@Postcode Between 2000 and 2599)  OR 
							   (@Postcode Between 2619 and 2899)  OR
							   (@Postcode Between 2921 and 2999)
							  ) Then 'NSW'
	                    When  ((@Postcode Between 2600 and 2618)  OR 
							   (@Postcode Between 2900 and 2920)  
							  ) Then 'ACT'
  				        When ((@Postcode Between 3000 and 3999)  
							  ) Then 'VIC'
                        When  ((@Postcode Between 4000 and 4999)  
							   ) Then 'QLD'

						When ((@Postcode Between 5000 and 5799)  
							   ) Then 'SA'
                        When ((@Postcode Between 6000 and 6797)  
							   ) Then 'WA'  

                        When ((@Postcode Between 7000 and 7799)  
							  ) Then 'TAS'  

						When ((@Postcode like '8%') or (@Postcode like '08%'))Then 'NT'
						ELSE ''
				END
	RETURN @ret;
	
END
GO
