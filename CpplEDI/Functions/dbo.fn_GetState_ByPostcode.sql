SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_GetState_ByPostcode]
(
	@Postcode varchar(15)
)
RETURNS varchar(10)
AS
BEGIN

	DECLARE @ret varchar(15);
	SELECT @ret = Null;
	
	SELECT @ret = CASE 
					WHEN @postcode like '2%' THEN 'NSW'
					WHEN @postcode like '3%' THEN 'VIC'
					WHEN @postcode like '7%' THEN 'TAS'
					WHEN @postcode like '4%' THEN 'QLD' 
					WHEN @postcode like '5%' THEN 'SA'
					WHEN (@postcode like '8%' OR  @postcode like '08%') THEN 'NT'
					WHEN @postcode like '6%' THEN 'WA'
				ELSE ''  END
	RETURN @ret;
	
END
GO
