SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_AgentDeliveryVsPOD]-- 3
  (@WorkingDay Integer)
AS
BEGIN

SELECT  [cd_id]
      ,1 as ConsCount
      ,case  WHEN [cd_activity_stamp] is null Then 0 Else 1 end as RecdCount
      ,case WHEN cd_deliver_stamp is null THEN 0 Else 1 end as DelvdCount
      ,CASE WHEN [cd_agent_pod_stamp] is null THEN 0 ELSE 1 END as PODCount
      ,[cd_account]
      ,[cd_agent_id]
      ,[cd_connote]
      ,[cd_date]
     -- , cpplEDI.[dbo].[CPPL_fnGetWeekEndingDate](CONVERT(Date,[cd_date])) as WeekendingDate
    --  ,CONVERT(Varchar(50),'') as Classification
      ,Convert(Date,[cd_customer_eta]) as [cd_customer_eta]
   --   ,CASE WHEN cd_deliver_stamp is not null  THEN DATEDIFF(day,[cd_customer_eta],cd_deliver_stamp) ELSE -9999 END as OrigETATime
      ,[cd_items]
      ,[cd_activity_stamp]
      ,[cd_deliver_stamp]
      ,[cd_deliver_driver]
      ,[cd_deliver_count]
      ,[cd_deliver_pod]
      ,[cd_pricecode]
      ,[cd_delivery_agent]
      ,[cd_agent_pod_desired]
      ,[cd_agent_pod_name]
      ,[cd_agent_pod_stamp]
      ,[cd_last_status]
 Into #Temp1
  FROM [cpplEDI].[dbo].[consignment] (NOLOCK)
  where   Convert(Date,[cd_customer_eta]) = [dbo].[CPPL_fnGetNthWorkingDay](Getdate(),@WorkingDay,0)
     and cd_test = 'N' and cd_release = 'Y'
  
  

 
 Select T.[cd_customer_eta] as CustomerETADate,
        @WorkingDay as [WorkingDayDiff],
		A.a_state as AgentState,
        ----[dbo].[CPPL_fnCalcDelivAgentState](A.a_name) as AgentState,
        Isnull([dbo].[CPPL_fnCalcDelivAgentName] (A.a_name),'Unknown') as AgentName,
      --  T.Classification,
        SUM(ConsCount) as ConsCount ,
        SUM(RecdCount) as RecdCount,
        SUM(DelvdCount) as DelvdCount,
        SUM(PODCount) as PODCount,
        CONVERT(decimal(12,4),0) as PercenttoData,
        CONVERT(decimal(12,4),0) as PercenttoRecd,
        CONVERT(decimal(12,4),0) as PercenttoPOD
   Into #TempFinal 
   from #Temp1 T left join cpplEDi.dbo.agents A (NOLOCK) on T.[cd_agent_id]= A.a_id
    group by [cd_customer_eta], A.a_name,A.a_state--Classification
    

 Update #TempFinal SET PercenttoData = (CONVERT(decimal(12,4),RecdCount)/CONVERT(decimal(12,4),ConsCount))  Where ConsCount>0
 Update #TempFinal SET PercenttoRecd = (CONVERT(decimal(12,4),DelvdCount)/CONVERT(decimal(12,4),ConsCount))  Where ConsCount>0
 Update #TempFinal SET PercenttoPOD = (CONVERT(decimal(12,4),PODCount)/CONVERT(decimal(12,4),DelvdCount))  Where DelvdCount>0
 
 
 INSERT INTO [cpplEDI].[dbo].[DeliveryVsPODSummary]
           ([ReportDate]
           ,[CustomerETADate]
           ,[WorkingDayDiff]
           ,[AgentState]
           ,[AgentName]
           ,[ConsignmentCount]
           ,[RecievedCount]
           ,[DeliveredCount]
           ,[PODCount]
           ,[PercenttoData]
           ,[PercenttoRecd]
           ,[PercenttoPOD])   
 Select CONVERT(Date,Getdate()),[CustomerETADate],[WorkingDayDiff],[AgentState],[AgentName],ConsCount,RecdCount,DelvdCount,PODCount,PercenttoData,PercenttoRecd,[PercenttoPOD] from #TempFinal

END

GO
