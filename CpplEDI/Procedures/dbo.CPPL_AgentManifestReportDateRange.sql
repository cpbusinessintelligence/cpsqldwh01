SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_AgentManifestReportDateRange]
( @StartDate  Date,@EndDate  Date)
AS
BEGIN
  Select [ac_connote] as AgentConNote
      ,Convert(Date,[ac_date]) as AgentCnteDate
      ,[ac_consignment] as ConsignmentID
      ,[ac_receiver_postcode] as ReceiverPostCode
      ,[ac_receiver_location] as ReceiverLocation
  INto #Temp1
  FROM [cpplEDI].[dbo].[apconnote] where CONVERT(Date,ac_date) >= @StartDate and  CONVERT(Date,ac_date) <= @EndDate 

Select  AgentConNote
        ,AgentCnteDate
		,ReceiverPostCode
        ,ReceiverLocation
        ,cd_connote as ConsignmentNumber
        ,cd_account as CustAccount
        ,UPPER([cd_pickup_addr0]) as PickUpAddr1
        ,UPPER([cd_pickup_addr1]) as PickUpAddr2
        ,UPPER([cd_pickup_addr2]) as PickUpAddr3
        ,UPPER([cd_pickup_suburb]) As PickupSuburb
        ,[cd_pickup_postcode]As PickupPostCode
        
from #Temp1 T Join [cpplEDI].[dbo].[consignment] C On  T.ConsignmentID = C.Cd_id Order by 1 asc

END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_AgentManifestReportDateRange]
	TO [ReportUser]	
WITH GRANT OPTION
GO
