SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

 CREATE proc [dbo].[CPPL_ItemsVsRevenue] as
  begin
  select cd_id
  ,cd_connote
  ,cd_date
  ,cd_account
  ,cd_items
  ,cd_coupons
  ,count(cd.cc_id) as manualcouponcount
-- ,count(a_cppl) as agentcount
  ,pb.billedtotal
  ,pb.RevenueOriginZone
  ,pb.RevenueDestinationZone
  ,pb.billingdate as ProntoBillingDate
  ,pb.deliveryzone as networkcategory
  ,og.state as ogstate
  ,dg.state as dgstate
   into #temp 
  from [cpplEDI].[dbo].[consignment]  
 -- inner join agents a on a.a_id=cd_agent_id
  inner join cdcoupon cd on cd_id=cd.cc_consignment
  

  left outer join [Pronto].[dbo].[prontobilling] pb 
  on pb.consignmentreference=cd_connote 

  LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim og 
				ON pb.OriginLocality = og.Locality
				AND pb.OriginPostcode = og.Postcode 
				
				LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim dg 
				ON pb.DestinationLocality = dg.Locality
				AND pb.DestinationPostcode = dg.Postcode 
				--AND dg.CompanyCode = 'CL1'
  where  pb.billingdate >='2014-06-01' and pb.billingdate <='2014-06-02' and cd_test<>'T'

  group by  cd_id
  ,cd_connote
  ,cd_date
  ,cd_account
  ,cd_items
  ,cd_agent_id
  ,cd_coupons
  ,pb.billedtotal
  ,pb.deliveryzone
  ,pb.RevenueOriginZone
  ,pb.RevenueDestinationZone
  ,pb.billingdate 
  ,og.state 
  ,dg.state 

select cd_id as consid,cd_agent_id,count(a_cppl) as internalagentcount into #temp1  from consignment  inner join agents a  on a.a_id=cd_agent_id and a_cppl='Y' left outer join [Pronto].[dbo].[prontobilling] pb 
  on pb.consignmentreference=cd_connote  where  pb.billingdate >='2014-06-01' and pb.billingdate <='2014-06-02' and cd_test<>'T' group by cd_id,cd_agent_id 

  select cd_id as consid1,cd_agent_id as agentid1,count(a_cppl) as externalagentcount into #temp2 from consignment  inner join agents a  on a.a_id=cd_agent_id and a_cppl='N' left outer join [Pronto].[dbo].[prontobilling] pb 
  on pb.consignmentreference=cd_connote  where  pb.billingdate >='2014-06-01' and pb.billingdate <='2014-06-02' and cd_test<>'T' group by cd_id,cd_agent_id 

select * into #temp3 from #temp left outer join  #temp1 on #temp.cd_id=#temp1.consid 

select * into #tempfinal from #temp3 left outer join #temp2 on #temp3.cd_id=#temp2.consid1

--inner join #temp2 on #temp2.consid1=#temp.cd_id


update #tempfinal set networkcategory=( [Pronto].[dbo].[CPPL_Fn_GetnetworkCategoryforEDI](#tempfinal.RevenueOriginZone,#tempfinal.RevenueDestinationZone,#tempfinal.ogstate,#tempfinal.dgstate))
--update #temp set manualcouponcount =(select count(cd.cc_id) from #temp inner join cdcoupon cd on #temp.cd_id=cd.cc_consignment )
select ProntoBillingDate,networkcategory,ogstate as RevenueOriginstate,sum(internalagentcount) as internalagentcount,sum(externalagentcount) as externalagentcount,sum(cd_items) as useritemcount,sum(cd_coupons) as usercouponcount,sum(manualcouponcount) as manualcouponcount,sum(billedtotal) as revenue from #tempfinal
group by ProntoBillingDate,networkcategory,ogstate
end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_ItemsVsRevenue]
	TO [ReportUser]
GO
