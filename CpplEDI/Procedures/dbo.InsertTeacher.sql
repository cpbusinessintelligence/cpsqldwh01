SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertTeacher](@id int,@fn varchar(100),@ln varchar(100)) 
as
Begin
Insert into dbo.Teacher(id,fn,ln)
select @id,@fn,@ln
end
GO
