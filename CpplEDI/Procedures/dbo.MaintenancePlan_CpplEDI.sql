SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[MaintenancePlan_CpplEDI]
AS 
BEGIN
	--Reorganize
	ALTER INDEX [PK__addresse__566AFA9AECCB7ECE] ON [dbo].[addresses] REORGANIZE  



	ALTER INDEX [PK__agedbala__560806CCC4135B3C] ON [dbo].[agedbalances] REORGANIZE  



	ALTER INDEX [idx_agentinfo_etazone] ON [dbo].[agentinfo] REORGANIZE  



	ALTER INDEX [PK__agentinf__0372DAEE78F94523] ON [dbo].[agentinfo] REORGANIZE  



	ALTER INDEX [PK__agentinf__0372DAEE2DF1CFAF] ON [dbo].[agentinfo2] REORGANIZE  



	ALTER INDEX [PK__agents__566AFA9A29221E27] ON [dbo].[agents] REORGANIZE  



	ALTER INDEX [PK__apconnot__A36ABBF4EE7275F3] ON [dbo].[apconnote] REORGANIZE  



	ALTER INDEX [PK__apinfo__F67A77ABA640B1B7] ON [dbo].[apinfo] REORGANIZE  



	ALTER INDEX [PK__aplink__84248F83D55E77C5] ON [dbo].[aplink] REORGANIZE  



	ALTER INDEX [PK__apmanife__B95A8ED071751EC9] ON [dbo].[apmanifest] REORGANIZE  



	ALTER INDEX [PK__authinfo__0372DAEEE683BD2F] ON [dbo].[authinfo] REORGANIZE  



	ALTER INDEX [PK__billing__4E29C30D1890A5C9] ON [dbo].[billing] REORGANIZE  



	ALTER INDEX [PK__bookins__5F352EC7500679BE] ON [dbo].[bookins] REORGANIZE  



	ALTER INDEX [PK__bookintr__73478FBEB4919FDC] ON [dbo].[bookintrack] REORGANIZE  



	ALTER INDEX [idx_Branchs_EmmcodeId] ON [dbo].[branchs] REORGANIZE  



	ALTER INDEX [PK__branchs__4E29C30D529E2231] ON [dbo].[branchs] REORGANIZE  



	ALTER INDEX [PK__ccreweig__00108C257C358E97] ON [dbo].[ccreweigh] REORGANIZE  



	ALTER INDEX [PK__ccreweig__E5E69EB8743BD1CC] ON [dbo].[ccreweighhold] REORGANIZE  



	ALTER INDEX [PK__cdadditi__64D017294269E002] ON [dbo].[cdadditional] REORGANIZE  



	ALTER INDEX [idx_cdaudit_consignmentstamp] ON [dbo].[cdaudit] REORGANIZE  



	ALTER INDEX [PK__cdaudit__0875B1F89E504993] ON [dbo].[cdaudit] REORGANIZE  



	ALTER INDEX [IX_cdcoupon_consignment] ON [dbo].[cdcoupon] REORGANIZE  



	ALTER INDEX [IX_cdcoupon_coupon] ON [dbo].[cdcoupon] REORGANIZE  



	ALTER INDEX [PK__cdcoupon__9F1E187B699DCD7D] ON [dbo].[cdcoupon] REORGANIZE  



	ALTER INDEX [PK__cdintern__4FE5E8DC70993512] ON [dbo].[cdinternal] REORGANIZE  



	ALTER INDEX [idx_cdref_consignment] ON [dbo].[cdref] REORGANIZE  



	ALTER INDEX [PK__cdref__AB69D8CF52040E59] ON [dbo].[cdref] REORGANIZE  



	ALTER INDEX [PK__cdreweig__C64341AD80A46928] ON [dbo].[cdreweigh] REORGANIZE  



	ALTER INDEX [PK__cdsundry__53E00ADAA33A4616] ON [dbo].[cdsundry] REORGANIZE  



	ALTER INDEX [PK__checkwor__0372DAEE12DE6027] ON [dbo].[checkwork] REORGANIZE  



	ALTER INDEX [PK__chrisco__5130BD17E6D1ABAF] ON [dbo].[chrisco] REORGANIZE  



	ALTER INDEX [idx_companies_cclass] ON [dbo].[companies] REORGANIZE  



	ALTER INDEX [PK__companie__213EE7741B2D57B6] ON [dbo].[companies] REORGANIZE  



	ALTER INDEX [idx_companyclass_shortname] ON [dbo].[companyclass] REORGANIZE  



	ALTER INDEX [PK__companyc__9F1E187BA2F21916] ON [dbo].[companyclass] REORGANIZE  



	ALTER INDEX [idx_consignment_connote] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [idx_consignment_idcd_account] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [idx_consignment_idmanifestimportdate] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [IX_Consignment_cd_date] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [IX_consignment_customer_eta] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [IX_consignment_manifest_id] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [NonClusteredIndex-20170914-185417] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [PK__consignm__D551B536CC82C0AC] ON [dbo].[consignment] REORGANIZE  



	ALTER INDEX [idx_consignment_Archive_19-20_connote] ON [dbo].[consignment_Archive_19-20] REORGANIZE  



	ALTER INDEX [idx_consignment_Archive_19-20_idcd_account] ON [dbo].[consignment_Archive_19-20] REORGANIZE  



	ALTER INDEX [IX_Consignment_Archive_19-20_cd_date] ON [dbo].[consignment_Archive_19-20] REORGANIZE  



	ALTER INDEX [NonClusteredIndex-20210127_consignment_Archive_19-20] ON [dbo].[consignment_Archive_19-20] REORGANIZE  



	ALTER INDEX [PK__consignm__D551B5367370EC77] ON [dbo].[consignment_Archive_19-20] REORGANIZE  



	ALTER INDEX [idx_consignment12MonthsData_connote] ON [dbo].[consignment12MonthsData] REORGANIZE  



	ALTER INDEX [idx_consignment12MonthsData_idcd_account] ON [dbo].[consignment12MonthsData] REORGANIZE  



	ALTER INDEX [idx_consignment12MonthsData_idmanifestimportdate] ON [dbo].[consignment12MonthsData] REORGANIZE  



	ALTER INDEX [IX_consignment12MonthsData_cd_date] ON [dbo].[consignment12MonthsData] REORGANIZE  



	ALTER INDEX [IX_consignment12MonthsData_customer_eta] ON [dbo].[consignment12MonthsData] REORGANIZE  



	ALTER INDEX [IX_consignment12MonthsData_manifest_id] ON [dbo].[consignment12MonthsData] REORGANIZE  



	ALTER INDEX [NonClusteredIndex-consignment12MonthsData-20170914-185417] ON [dbo].[consignment12MonthsData] REORGANIZE  



	ALTER INDEX [PK__consignm__457BF54F82ED588A] ON [dbo].[consignmentstatus] REORGANIZE  



	ALTER INDEX [PK__Customer__453846D1C6A471CF] ON [dbo].[CustomerCodeSBMapping] REORGANIZE  



	ALTER INDEX [idx_customereta_FromTo] ON [dbo].[customereta] REORGANIZE  



	ALTER INDEX [PK__Customer__DE71736143FC9E03] ON [dbo].[CustomerUniqueCodes] REORGANIZE  



	ALTER INDEX [PK__depot__D95F582B7BBD5EF0] ON [dbo].[depot] REORGANIZE  



	ALTER INDEX [PK__dhlinfo__0C76E3CB30AF0AC5] ON [dbo].[dhlinfo] REORGANIZE  



	ALTER INDEX [PK__dhlroute__737CA9F7C9A9E09F] ON [dbo].[dhlroute] REORGANIZE  



	ALTER INDEX [PK__dirty_co__33FDC975B5211940] ON [dbo].[dirty_consignment] REORGANIZE  



	ALTER INDEX [PK__dirty_co__33FDC9756380F7BE] ON [dbo].[dirty_coupon] REORGANIZE  



	ALTER INDEX [idx_Driver_NumberBranch] ON [dbo].[driver] REORGANIZE  



	ALTER INDEX [PK__driver__01AE93E2B78B7CDB] ON [dbo].[driver] REORGANIZE  



	ALTER INDEX [PK__DWhTrace__AAAC09D8F0BA1651] ON [dbo].[DWhTrace] REORGANIZE  



	ALTER INDEX [PK__enquiry__36B150C6E762A7BF] ON [dbo].[enquiry] REORGANIZE  



	ALTER INDEX [PK__enquiry___36B150C6D26673CB] ON [dbo].[enquiry_temp] REORGANIZE  



	ALTER INDEX [PK__enquiryd__36B150C641737C42] ON [dbo].[enquiryd] REORGANIZE  



	ALTER INDEX [PK__exceptio__ABD24D7F332CB1AD] ON [dbo].[exceptionmap] REORGANIZE  



	ALTER INDEX [PK__exports__3E2ED64AF31E5D64] ON [dbo].[exports] REORGANIZE  



	ALTER INDEX [PK__ftpsetup__656F05D19FB5DDC1] ON [dbo].[ftpsetup] REORGANIZE  



	ALTER INDEX [PK__ods__49FB61C4ACF2BF1C] ON [dbo].[ods] REORGANIZE  



	ALTER INDEX [PK__groupnam__ED1647CD2A1F6E75] ON [dbo].[groupname] REORGANIZE  



	ALTER INDEX [PK__groups__5FC00A1BD3ADA9AE] ON [dbo].[groups] REORGANIZE  



	ALTER INDEX [PK__holidays__430F8EDBE17D3D77] ON [dbo].[holidays] REORGANIZE  



	ALTER INDEX [PK__importco__28FBF46CE9A0F226] ON [dbo].[importconfig] REORGANIZE  



	ALTER INDEX [PK__imports__98F919BA6D3A590F] ON [dbo].[imports] REORGANIZE  



	ALTER INDEX [PK__invoices__ADF81AD336191E32] ON [dbo].[invoicestore] REORGANIZE  



	ALTER INDEX [PK__irp__249BBCD20C1CB260] ON [dbo].[irp] REORGANIZE  



	ALTER INDEX [idx_Load_DailyCWCData_PackNSend_ConsignmentNumber] ON [dbo].[Load_DailyCWCData_PackNSend] REORGANIZE  



	ALTER INDEX [idx_Load_DailyCWCData_Sendle_ConsignmentNumber] ON [dbo].[Load_DailyCWCData_Sendle] REORGANIZE  



	ALTER INDEX [PK__location__03F2B4754FA07AD4] ON [dbo].[location] REORGANIZE  



	ALTER INDEX [PK__login__F3DBC57395D45369] ON [dbo].[login] REORGANIZE  



	ALTER INDEX [idx_manifest_idreleasedstampdate] ON [dbo].[manifest] REORGANIZE  



	ALTER INDEX [PK__manifest__7C8D7D2914B80182] ON [dbo].[manifest] REORGANIZE  



	ALTER INDEX [PK__noncosmo__3957D839217EDA06] ON [dbo].[noncosmosdriver] REORGANIZE  



	ALTER INDEX [PK__ogmanife__47AC10B2414F0767] ON [dbo].[ogmanifest] REORGANIZE  



	ALTER INDEX [PK__ogmexcep__07D4819EC61D5663] ON [dbo].[ogmexceptions] REORGANIZE  



	ALTER INDEX [idx_pAccountCode] ON [dbo].[OpsAccountsforReporting] REORGANIZE  



	ALTER INDEX [PK__pcodebra__87E16D73282BFBAD] ON [dbo].[pcodebranch] REORGANIZE  



	ALTER INDEX [PK__PDG_Rela__3214EC075F85B894] ON [dbo].[PDG_RelationshipDefinitions] REORGANIZE  



	ALTER INDEX [PK__pickupco__1D3A69C0A0EA8635] ON [dbo].[pickupconsignment] REORGANIZE  



	ALTER INDEX [PK__pickups__82E06B91D51CC15D] ON [dbo].[pickups] REORGANIZE  



	ALTER INDEX [PK__price__82E06B910966905C] ON [dbo].[price] REORGANIZE  



	ALTER INDEX [PK__pricecod__20A61766D85EDA72] ON [dbo].[pricecodes] REORGANIZE  



	ALTER INDEX [PK__pricegro__B056F8C31DD4449B] ON [dbo].[pricegroup] REORGANIZE  



	ALTER INDEX [PK__pwreset__47B09F8E2AC91034] ON [dbo].[pwreset] REORGANIZE  



	ALTER INDEX [PK__rate_dat__D3D01ED2E7E052B6] ON [dbo].[rate_data] REORGANIZE  



	ALTER INDEX [PK__rating__C4762327C91A8BBD] ON [dbo].[rating] REORGANIZE  



	ALTER INDEX [PK__receipts__C4762327D08D8F0D] ON [dbo].[receipts] REORGANIZE  



	ALTER INDEX [PK__redelive__A107D412156C161C] ON [dbo].[redeliverylog] REORGANIZE  



	ALTER INDEX [PK__return_i__C476232798AE2EE6] ON [dbo].[return_info] REORGANIZE  



	ALTER INDEX [PK__salesrep__5C9E98B9F70C7B21] ON [dbo].[salesrep] REORGANIZE  



	ALTER INDEX [PK__sessions__3213E83FA098BAC9] ON [dbo].[sessions] REORGANIZE  



	ALTER INDEX [PK__sortatio__2C77E71198D5C333] ON [dbo].[sortationdriver] REORGANIZE  



	ALTER INDEX [PK__suburb__2F3684F402720096] ON [dbo].[suburb] REORGANIZE  



	ALTER INDEX [PK__survey20__13B4F3FA8E1E04DB] ON [dbo].[survey2015] REORGANIZE  



	ALTER INDEX [PK_tblCouponSalesTracking] ON [dbo].[tbl_CouponSales] REORGANIZE  



	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REORGANIZE  



	ALTER INDEX [PK__Teacher__3213E83F6CD38C21] ON [dbo].[Teacher] REORGANIZE  



	ALTER INDEX [TempScannersidx] ON [dbo].[TempScanners] REORGANIZE  



	ALTER INDEX [PK__trackkey__E579775FFE187DB7] ON [dbo].[trackkey] REORGANIZE  



	ALTER INDEX [PK__transact__E579775F26033596] ON [dbo].[transactions] REORGANIZE  



	ALTER INDEX [PK__type__0E8FC1F1386196A1] ON [dbo].[type] REORGANIZE  



	ALTER INDEX [PK__users__3213E83F3C2A8FF3] ON [dbo].[users] REORGANIZE  



	ALTER INDEX [PK__validati__AD3D84415C93BB05] ON [dbo].[validation] REORGANIZE  



	ALTER INDEX [PK__warehous__1198F2A3A4F058DB] ON [dbo].[warehouse] REORGANIZE  



	ALTER INDEX [PK__zonegrid__DD40DED7E11A701E] ON [dbo].[zonegrid] REORGANIZE  



	ALTER INDEX [PK__zonemaps__FBEBD92034B4B2E6] ON [dbo].[zonemaps] REORGANIZE  



	ALTER INDEX [PK__zonepric__BFD796BA5783AFC5] ON [dbo].[zoneprice] REORGANIZE  



	ALTER INDEX [PK__zones__977743E643E6DABD] ON [dbo].[zones] REORGANIZE  



	ALTER INDEX [PK__zones1__977743E695BD8079] ON [dbo].[zones1] REORGANIZE  



	ALTER INDEX [PK__zoning__977743E69B35424C] ON [dbo].[zoning] REORGANIZE  



	ALTER INDEX [PK__zoning1__977743E64339667E] ON [dbo].[zoning1] REORGANIZE  


	--Rebuild


	ALTER INDEX [PK__addresse__566AFA9AECCB7ECE] ON [dbo].[addresses] REBUILD  



	ALTER INDEX [PK__agedbala__560806CCC4135B3C] ON [dbo].[agedbalances] REBUILD  



	ALTER INDEX [idx_agentinfo_etazone] ON [dbo].[agentinfo] REBUILD  



	ALTER INDEX [PK__agentinf__0372DAEE78F94523] ON [dbo].[agentinfo] REBUILD  



	ALTER INDEX [PK__agentinf__0372DAEE2DF1CFAF] ON [dbo].[agentinfo2] REBUILD  



	ALTER INDEX [PK__agents__566AFA9A29221E27] ON [dbo].[agents] REBUILD  



	ALTER INDEX [PK__apconnot__A36ABBF4EE7275F3] ON [dbo].[apconnote] REBUILD  



	ALTER INDEX [PK__apinfo__F67A77ABA640B1B7] ON [dbo].[apinfo] REBUILD  



	ALTER INDEX [PK__aplink__84248F83D55E77C5] ON [dbo].[aplink] REBUILD  



	ALTER INDEX [PK__apmanife__B95A8ED071751EC9] ON [dbo].[apmanifest] REBUILD  



	ALTER INDEX [PK__authinfo__0372DAEEE683BD2F] ON [dbo].[authinfo] REBUILD  



	ALTER INDEX [PK__billing__4E29C30D1890A5C9] ON [dbo].[billing] REBUILD  



	ALTER INDEX [PK__bookins__5F352EC7500679BE] ON [dbo].[bookins] REBUILD  



	ALTER INDEX [PK__bookintr__73478FBEB4919FDC] ON [dbo].[bookintrack] REBUILD  



	ALTER INDEX [idx_Branchs_EmmcodeId] ON [dbo].[branchs] REBUILD  



	ALTER INDEX [PK__branchs__4E29C30D529E2231] ON [dbo].[branchs] REBUILD  



	ALTER INDEX [PK__ccreweig__00108C257C358E97] ON [dbo].[ccreweigh] REBUILD  



	ALTER INDEX [PK__ccreweig__E5E69EB8743BD1CC] ON [dbo].[ccreweighhold] REBUILD  



	ALTER INDEX [PK__cdadditi__64D017294269E002] ON [dbo].[cdadditional] REBUILD  



	ALTER INDEX [idx_cdaudit_consignmentstamp] ON [dbo].[cdaudit] REBUILD  



	ALTER INDEX [PK__cdaudit__0875B1F89E504993] ON [dbo].[cdaudit] REBUILD  



	ALTER INDEX [IX_cdcoupon_consignment] ON [dbo].[cdcoupon] REBUILD  



	ALTER INDEX [IX_cdcoupon_coupon] ON [dbo].[cdcoupon] REBUILD  



	ALTER INDEX [PK__cdcoupon__9F1E187B699DCD7D] ON [dbo].[cdcoupon] REBUILD  



	ALTER INDEX [PK__cdintern__4FE5E8DC70993512] ON [dbo].[cdinternal] REBUILD  



	ALTER INDEX [idx_cdref_consignment] ON [dbo].[cdref] REBUILD  



	ALTER INDEX [PK__cdref__AB69D8CF52040E59] ON [dbo].[cdref] REBUILD  



	ALTER INDEX [PK__cdreweig__C64341AD80A46928] ON [dbo].[cdreweigh] REBUILD  



	ALTER INDEX [PK__cdsundry__53E00ADAA33A4616] ON [dbo].[cdsundry] REBUILD  



	ALTER INDEX [PK__checkwor__0372DAEE12DE6027] ON [dbo].[checkwork] REBUILD  



	ALTER INDEX [PK__chrisco__5130BD17E6D1ABAF] ON [dbo].[chrisco] REBUILD  



	ALTER INDEX [idx_companies_cclass] ON [dbo].[companies] REBUILD  



	ALTER INDEX [PK__companie__213EE7741B2D57B6] ON [dbo].[companies] REBUILD  



	ALTER INDEX [idx_companyclass_shortname] ON [dbo].[companyclass] REBUILD  



	ALTER INDEX [PK__companyc__9F1E187BA2F21916] ON [dbo].[companyclass] REBUILD  



	ALTER INDEX [idx_consignment_connote] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [idx_consignment_idcd_account] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [idx_consignment_idmanifestimportdate] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [IX_Consignment_cd_date] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [IX_consignment_customer_eta] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [IX_consignment_manifest_id] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [NonClusteredIndex-20170914-185417] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [PK__consignm__D551B536CC82C0AC] ON [dbo].[consignment] REBUILD  



	ALTER INDEX [idx_consignment_Archive_19-20_connote] ON [dbo].[consignment_Archive_19-20] REBUILD  



	ALTER INDEX [idx_consignment_Archive_19-20_idcd_account] ON [dbo].[consignment_Archive_19-20] REBUILD  



	ALTER INDEX [IX_Consignment_Archive_19-20_cd_date] ON [dbo].[consignment_Archive_19-20] REBUILD  



	ALTER INDEX [NonClusteredIndex-20210127_consignment_Archive_19-20] ON [dbo].[consignment_Archive_19-20] REBUILD  



	ALTER INDEX [PK__consignm__D551B5367370EC77] ON [dbo].[consignment_Archive_19-20] REBUILD  



	ALTER INDEX [idx_consignment12MonthsData_connote] ON [dbo].[consignment12MonthsData] REBUILD  



	ALTER INDEX [idx_consignment12MonthsData_idcd_account] ON [dbo].[consignment12MonthsData] REBUILD  



	ALTER INDEX [idx_consignment12MonthsData_idmanifestimportdate] ON [dbo].[consignment12MonthsData] REBUILD  



	ALTER INDEX [IX_consignment12MonthsData_cd_date] ON [dbo].[consignment12MonthsData] REBUILD  



	ALTER INDEX [IX_consignment12MonthsData_customer_eta] ON [dbo].[consignment12MonthsData] REBUILD  



	ALTER INDEX [IX_consignment12MonthsData_manifest_id] ON [dbo].[consignment12MonthsData] REBUILD  



	ALTER INDEX [NonClusteredIndex-consignment12MonthsData-20170914-185417] ON [dbo].[consignment12MonthsData] REBUILD  



	ALTER INDEX [PK__consignm__457BF54F82ED588A] ON [dbo].[consignmentstatus] REBUILD  



	ALTER INDEX [PK__Customer__453846D1C6A471CF] ON [dbo].[CustomerCodeSBMapping] REBUILD  



	ALTER INDEX [idx_customereta_FromTo] ON [dbo].[customereta] REBUILD  



	ALTER INDEX [PK__Customer__DE71736143FC9E03] ON [dbo].[CustomerUniqueCodes] REBUILD  



	ALTER INDEX [PK__depot__D95F582B7BBD5EF0] ON [dbo].[depot] REBUILD  



	ALTER INDEX [PK__dhlinfo__0C76E3CB30AF0AC5] ON [dbo].[dhlinfo] REBUILD  



	ALTER INDEX [PK__dhlroute__737CA9F7C9A9E09F] ON [dbo].[dhlroute] REBUILD  



	ALTER INDEX [PK__dirty_co__33FDC975B5211940] ON [dbo].[dirty_consignment] REBUILD  



	ALTER INDEX [PK__dirty_co__33FDC9756380F7BE] ON [dbo].[dirty_coupon] REBUILD  



	ALTER INDEX [idx_Driver_NumberBranch] ON [dbo].[driver] REBUILD  



	ALTER INDEX [PK__driver__01AE93E2B78B7CDB] ON [dbo].[driver] REBUILD  



	ALTER INDEX [PK__DWhTrace__AAAC09D8F0BA1651] ON [dbo].[DWhTrace] REBUILD  



	ALTER INDEX [PK__enquiry__36B150C6E762A7BF] ON [dbo].[enquiry] REBUILD  



	ALTER INDEX [PK__enquiry___36B150C6D26673CB] ON [dbo].[enquiry_temp] REBUILD  



	ALTER INDEX [PK__enquiryd__36B150C641737C42] ON [dbo].[enquiryd] REBUILD  



	ALTER INDEX [PK__exceptio__ABD24D7F332CB1AD] ON [dbo].[exceptionmap] REBUILD  



	ALTER INDEX [PK__exports__3E2ED64AF31E5D64] ON [dbo].[exports] REBUILD  



	ALTER INDEX [PK__ftpsetup__656F05D19FB5DDC1] ON [dbo].[ftpsetup] REBUILD  



	ALTER INDEX [PK__ods__49FB61C4ACF2BF1C] ON [dbo].[ods] REBUILD  



	ALTER INDEX [PK__groupnam__ED1647CD2A1F6E75] ON [dbo].[groupname] REBUILD  



	ALTER INDEX [PK__groups__5FC00A1BD3ADA9AE] ON [dbo].[groups] REBUILD  



	ALTER INDEX [PK__holidays__430F8EDBE17D3D77] ON [dbo].[holidays] REBUILD  



	ALTER INDEX [PK__importco__28FBF46CE9A0F226] ON [dbo].[importconfig] REBUILD  



	ALTER INDEX [PK__imports__98F919BA6D3A590F] ON [dbo].[imports] REBUILD  



	ALTER INDEX [PK__invoices__ADF81AD336191E32] ON [dbo].[invoicestore] REBUILD  



	ALTER INDEX [PK__irp__249BBCD20C1CB260] ON [dbo].[irp] REBUILD  



	ALTER INDEX [idx_Load_DailyCWCData_PackNSend_ConsignmentNumber] ON [dbo].[Load_DailyCWCData_PackNSend] REBUILD  



	ALTER INDEX [idx_Load_DailyCWCData_Sendle_ConsignmentNumber] ON [dbo].[Load_DailyCWCData_Sendle] REBUILD  



	ALTER INDEX [PK__location__03F2B4754FA07AD4] ON [dbo].[location] REBUILD  



	ALTER INDEX [PK__login__F3DBC57395D45369] ON [dbo].[login] REBUILD  



	ALTER INDEX [idx_manifest_idreleasedstampdate] ON [dbo].[manifest] REBUILD  



	ALTER INDEX [PK__manifest__7C8D7D2914B80182] ON [dbo].[manifest] REBUILD  



	ALTER INDEX [PK__noncosmo__3957D839217EDA06] ON [dbo].[noncosmosdriver] REBUILD  



	ALTER INDEX [PK__ogmanife__47AC10B2414F0767] ON [dbo].[ogmanifest] REBUILD  



	ALTER INDEX [PK__ogmexcep__07D4819EC61D5663] ON [dbo].[ogmexceptions] REBUILD  



	ALTER INDEX [idx_pAccountCode] ON [dbo].[OpsAccountsforReporting] REBUILD  



	ALTER INDEX [PK__pcodebra__87E16D73282BFBAD] ON [dbo].[pcodebranch] REBUILD  



	ALTER INDEX [PK__PDG_Rela__3214EC075F85B894] ON [dbo].[PDG_RelationshipDefinitions] REBUILD  



	ALTER INDEX [PK__pickupco__1D3A69C0A0EA8635] ON [dbo].[pickupconsignment] REBUILD  



	ALTER INDEX [PK__pickups__82E06B91D51CC15D] ON [dbo].[pickups] REBUILD  



	ALTER INDEX [PK__price__82E06B910966905C] ON [dbo].[price] REBUILD  



	ALTER INDEX [PK__pricecod__20A61766D85EDA72] ON [dbo].[pricecodes] REBUILD  



	ALTER INDEX [PK__pricegro__B056F8C31DD4449B] ON [dbo].[pricegroup] REBUILD  



	ALTER INDEX [PK__pwreset__47B09F8E2AC91034] ON [dbo].[pwreset] REBUILD  



	ALTER INDEX [PK__rate_dat__D3D01ED2E7E052B6] ON [dbo].[rate_data] REBUILD  



	ALTER INDEX [PK__rating__C4762327C91A8BBD] ON [dbo].[rating] REBUILD  



	ALTER INDEX [PK__receipts__C4762327D08D8F0D] ON [dbo].[receipts] REBUILD  



	ALTER INDEX [PK__redelive__A107D412156C161C] ON [dbo].[redeliverylog] REBUILD  



	ALTER INDEX [PK__return_i__C476232798AE2EE6] ON [dbo].[return_info] REBUILD  



	ALTER INDEX [PK__salesrep__5C9E98B9F70C7B21] ON [dbo].[salesrep] REBUILD  



	ALTER INDEX [PK__sessions__3213E83FA098BAC9] ON [dbo].[sessions] REBUILD  



	ALTER INDEX [PK__sortatio__2C77E71198D5C333] ON [dbo].[sortationdriver] REBUILD  



	ALTER INDEX [PK__suburb__2F3684F402720096] ON [dbo].[suburb] REBUILD  



	ALTER INDEX [PK__survey20__13B4F3FA8E1E04DB] ON [dbo].[survey2015] REBUILD  



	ALTER INDEX [PK_tblCouponSalesTracking] ON [dbo].[tbl_CouponSales] REBUILD  



	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REBUILD  



	ALTER INDEX [PK__Teacher__3213E83F6CD38C21] ON [dbo].[Teacher] REBUILD  



	ALTER INDEX [TempScannersidx] ON [dbo].[TempScanners] REBUILD  



	ALTER INDEX [PK__trackkey__E579775FFE187DB7] ON [dbo].[trackkey] REBUILD  



	ALTER INDEX [PK__transact__E579775F26033596] ON [dbo].[transactions] REBUILD  



	ALTER INDEX [PK__type__0E8FC1F1386196A1] ON [dbo].[type] REBUILD  



	ALTER INDEX [PK__users__3213E83F3C2A8FF3] ON [dbo].[users] REBUILD  



	ALTER INDEX [PK__validati__AD3D84415C93BB05] ON [dbo].[validation] REBUILD  



	ALTER INDEX [PK__warehous__1198F2A3A4F058DB] ON [dbo].[warehouse] REBUILD  



	ALTER INDEX [PK__zonegrid__DD40DED7E11A701E] ON [dbo].[zonegrid] REBUILD  



	ALTER INDEX [PK__zonemaps__FBEBD92034B4B2E6] ON [dbo].[zonemaps] REBUILD  



	ALTER INDEX [PK__zonepric__BFD796BA5783AFC5] ON [dbo].[zoneprice] REBUILD  



	ALTER INDEX [PK__zones__977743E643E6DABD] ON [dbo].[zones] REBUILD  



	ALTER INDEX [PK__zones1__977743E695BD8079] ON [dbo].[zones1] REBUILD  



	ALTER INDEX [PK__zoning__977743E69B35424C] ON [dbo].[zoning] REBUILD  



	ALTER INDEX [PK__zoning1__977743E64339667E] ON [dbo].[zoning1] REBUILD  

	--Update Statistics



	UPDATE STATISTICS [dbo].[AccountToConsignmentRange] 
	



	UPDATE STATISTICS [dbo].[addresses] 
	



	UPDATE STATISTICS [dbo].[addresses_Archive2017] 
	



	UPDATE STATISTICS [dbo].[addresses1] 
	



	UPDATE STATISTICS [dbo].[agedbalances] 
	



	UPDATE STATISTICS [dbo].[agentemail] 
	



	UPDATE STATISTICS [dbo].[agentinfo] 
	



	UPDATE STATISTICS [dbo].[agentinfo2] 
	



	UPDATE STATISTICS [dbo].[agentinfo3] 
	



	UPDATE STATISTICS [dbo].[agents] 
	



	UPDATE STATISTICS [dbo].[Agents List for Exile Soft Integration] 
	



	UPDATE STATISTICS [dbo].[Amendments] 
	



	UPDATE STATISTICS [dbo].[AmwayTransitTime] 
	



	UPDATE STATISTICS [dbo].[apconnote] 
	



	UPDATE STATISTICS [dbo].[apinfo] 
	



	UPDATE STATISTICS [dbo].[aplink] 
	



	UPDATE STATISTICS [dbo].[apmanifest] 
	



	UPDATE STATISTICS [dbo].[authinfo] 
	



	UPDATE STATISTICS [dbo].[billing] 
	



	UPDATE STATISTICS [dbo].[bookins] 
	



	UPDATE STATISTICS [dbo].[bookintrack] 
	



	UPDATE STATISTICS [dbo].[branchs] 
	



	UPDATE STATISTICS [dbo].[bulkreturn] 
	



	UPDATE STATISTICS [dbo].[ccreweigh] 
	



	UPDATE STATISTICS [dbo].[ccreweigh_load] 
	



	UPDATE STATISTICS [dbo].[ccreweighhold] 
	



	UPDATE STATISTICS [dbo].[cdadditional] 
	



	UPDATE STATISTICS [dbo].[cdadditional_load] 
	



	UPDATE STATISTICS [dbo].[cdaudit] 
	



	UPDATE STATISTICS [dbo].[cdcosmosbooking] 
	



	UPDATE STATISTICS [dbo].[cdcoupon] 
	



	UPDATE STATISTICS [dbo].[cdcoupon_Archive_17-18] 
	



	UPDATE STATISTICS [dbo].[cdcoupon_Archive_18-19] 
	



	UPDATE STATISTICS [dbo].[cdcoupon_Archive_19-20] 
	



	UPDATE STATISTICS [dbo].[cdcoupon_Incrementalload] 
	



	UPDATE STATISTICS [dbo].[cdcoupon_load] 
	



	UPDATE STATISTICS [dbo].[CdCoupon_PBI] 
	



	UPDATE STATISTICS [dbo].[cdextra] 
	



	UPDATE STATISTICS [dbo].[cdinternal] 
	



	UPDATE STATISTICS [dbo].[cditem] 
	



	UPDATE STATISTICS [dbo].[cdref] 
	



	UPDATE STATISTICS [dbo].[cdref_load] 
	



	UPDATE STATISTICS [dbo].[cdreweigh] 
	



	UPDATE STATISTICS [dbo].[cdreweigh_load] 
	



	UPDATE STATISTICS [dbo].[cdsundry] 
	



	UPDATE STATISTICS [dbo].[CellnetStatus] 
	



	UPDATE STATISTICS [dbo].[checkwork] 
	



	UPDATE STATISTICS [dbo].[chrisco] 
	



	UPDATE STATISTICS [dbo].[companies] 
	



	UPDATE STATISTICS [dbo].[companyaccount] 
	



	UPDATE STATISTICS [dbo].[companyagentoverride] 
	



	UPDATE STATISTICS [dbo].[companyclass] 
	



	UPDATE STATISTICS [dbo].[companyclpricecode] 
	



	UPDATE STATISTICS [dbo].[companycontact] 
	



	UPDATE STATISTICS [dbo].[companyefpricecode] 
	



	UPDATE STATISTICS [dbo].[companypricecode] 
	



	UPDATE STATISTICS [dbo].[companyremapagent] 
	



	UPDATE STATISTICS [dbo].[companysscc] 
	



	UPDATE STATISTICS [dbo].[ConsDel] 
	



	UPDATE STATISTICS [dbo].[consignment] 
	



	UPDATE STATISTICS [dbo].[consignment_Archive_17-18] 
	



	UPDATE STATISTICS [dbo].[consignment_Archive_18-19] 
	



	UPDATE STATISTICS [dbo].[consignment_Archive_19-20] 
	



	UPDATE STATISTICS [dbo].[consignment_Incrementalload] 
	


	UPDATE STATISTICS [dbo].[Consignment_PBI] 
	



	UPDATE STATISTICS [dbo].[consignment12MonthsData] 
	



	UPDATE STATISTICS [dbo].[consignmentstatus] 
	


	UPDATE STATISTICS [dbo].[cpplcpngrid] 
	



	UPDATE STATISTICS [dbo].[cpplgrid] 
	



	UPDATE STATISTICS [dbo].[Customer_GatewayuniqID] 
	



	UPDATE STATISTICS [dbo].[CustomerCodeSBMapping] 
	



	UPDATE STATISTICS [dbo].[customereta] 
	



	UPDATE STATISTICS [dbo].[CustomerStatusUpdateStaging] 
	



	UPDATE STATISTICS [dbo].[CustomerStatusUpdateStaging_Bulk] 
	



	UPDATE STATISTICS [dbo].[CustomerStatusUpdateStaging_Seko] 
	



	UPDATE STATISTICS [dbo].[CustomerStatusUpdateStaging_Sendle] 
	



	UPDATE STATISTICS [dbo].[CustomerStatusUpdateStaging_Transdirect] 
	



	UPDATE STATISTICS [dbo].[CustomerStatusUpdateStaging_Transdirect_Secondone] 
	



	UPDATE STATISTICS [dbo].[CustomerUniqueCodes] 
	



	UPDATE STATISTICS [dbo].[CWC_AgentOverRide] 
	



	UPDATE STATISTICS [dbo].[CWCDataForPricing] 
	



	UPDATE STATISTICS [dbo].[DeliveryVsPODSummary] 
	



	UPDATE STATISTICS [dbo].[depot] 
	



	UPDATE STATISTICS [dbo].[dhlinfo] 
	



	UPDATE STATISTICS [dbo].[dhlroute] 
	



	UPDATE STATISTICS [dbo].[DIFOT_InternalStaffDetails] 
	



	UPDATE STATISTICS [dbo].[DIFOT_RSComp] 
	



	UPDATE STATISTICS [dbo].[DIFOTArchiveData] 
	



	UPDATE STATISTICS [dbo].[DIFOTSample] 
	



	UPDATE STATISTICS [dbo].[dirty_consignment] 
	



	UPDATE STATISTICS [dbo].[dirty_coupon] 
	



	UPDATE STATISTICS [dbo].[driver] 
	



	UPDATE STATISTICS [dbo].[DWhTrace] 
	



	UPDATE STATISTICS [dbo].[ediconfig] 
	



	UPDATE STATISTICS [dbo].[enquiry] 
	



	UPDATE STATISTICS [dbo].[enquiry_temp] 
	



	UPDATE STATISTICS [dbo].[enquiryd] 
	



	UPDATE STATISTICS [dbo].[enquiryd_Archive2017] 
	



	UPDATE STATISTICS [dbo].[etagrid] 
	



	UPDATE STATISTICS [dbo].[etastate] 
	



	UPDATE STATISTICS [dbo].[exceptionmap] 
	



	UPDATE STATISTICS [dbo].[exports] 
	



	UPDATE STATISTICS [dbo].[ExtendedDIFOT_RSCo] 
	



	UPDATE STATISTICS [dbo].[ExtendedDIFOT_RSComponents] 
	


	UPDATE STATISTICS [dbo].[ExtendedDIFOTRSComponents] 
	



	UPDATE STATISTICS [dbo].[ftpsetup] 
	



	UPDATE STATISTICS [dbo].[GatewayImportFlag] 
	



	UPDATE STATISTICS [dbo].[Gatewayuniquecode] 
	



	UPDATE STATISTICS [dbo].[Gatewayuniquecode_updated] 
	



	UPDATE STATISTICS [dbo].[ods] 
	



	UPDATE STATISTICS [dbo].[groupname] 
	



	UPDATE STATISTICS [dbo].[groups] 
	



	UPDATE STATISTICS [dbo].[guessSSCC] 
	



	UPDATE STATISTICS [dbo].[holidays] 
	



	UPDATE STATISTICS [dbo].[Hub] 
	



	UPDATE STATISTICS [dbo].[Hubb] 
	


	UPDATE STATISTICS [dbo].[iccompany] 
	



	UPDATE STATISTICS [dbo].[icfile] 
	



	UPDATE STATISTICS [dbo].[importconfig] 
	



	UPDATE STATISTICS [dbo].[imports] 
	



	UPDATE STATISTICS [dbo].[InternalStaff] 
	



	UPDATE STATISTICS [dbo].[invoicestore] 
	



	UPDATE STATISTICS [dbo].[irp] 
	



	UPDATE STATISTICS [dbo].[irpcoupon] 
	



	UPDATE STATISTICS [dbo].[Load_DailyCWCData_PackNSend] 
	



	UPDATE STATISTICS [dbo].[Load_DailyCWCData_Sendle] 
	



	UPDATE STATISTICS [dbo].[Load_DailyCWCData_TransDirect] 
	



	UPDATE STATISTICS [dbo].[location] 
	



	UPDATE STATISTICS [dbo].[login] 
	



	UPDATE STATISTICS [dbo].[manifest] 
	



	UPDATE STATISTICS [dbo].[manifest_Archive2017] 
	



	UPDATE STATISTICS [dbo].[ManifestDataTable_T] 
	



	UPDATE STATISTICS [dbo].[ManualInvoice] 
	



	UPDATE STATISTICS [dbo].[NewToAdd] 
	



	UPDATE STATISTICS [dbo].[noncosmosdriver] 
	



	UPDATE STATISTICS [dbo].[NowConsignemnt] 
	



	UPDATE STATISTICS [dbo].[ogmanifest] 
	



	UPDATE STATISTICS [dbo].[ogmexceptions] 
	



	UPDATE STATISTICS [dbo].[OpsAccountsforReporting] 
	



	UPDATE STATISTICS [dbo].[OPSTemp] 
	



	UPDATE STATISTICS [dbo].[ParameterMissedpickup] 
	



	UPDATE STATISTICS [dbo].[pcodebranch] 
	



	UPDATE STATISTICS [dbo].[PDG_RelationshipDefinitions] 
	



	UPDATE STATISTICS [dbo].[pickupconsignment] 
	



	UPDATE STATISTICS [dbo].[PickupLabels_1300] 
	



	UPDATE STATISTICS [dbo].[PickupLabelsLastOneYear2] 
	



	UPDATE STATISTICS [dbo].[PickupLabelsLastOneYear3] 
	



	UPDATE STATISTICS [dbo].[pickups] 
	



	UPDATE STATISTICS [dbo].[ppp] 
	



	UPDATE STATISTICS [dbo].[PrepaidRevenueProtection] 
	



	UPDATE STATISTICS [dbo].[PrepaidRevenueProtection_Archive2017] 
	




	UPDATE STATISTICS [dbo].[PrepaidRevenueProtectionDeleted] 
	



	UPDATE STATISTICS [dbo].[PrepaidRevenueProtectionDeleted_Archive2017] 
	



	UPDATE STATISTICS [dbo].[PrepaidRevenueProtectionExceptions] 
	



	UPDATE STATISTICS [dbo].[price] 
	



	UPDATE STATISTICS [dbo].[pricecodes] 
	



	UPDATE STATISTICS [dbo].[pricegroup] 
	



	UPDATE STATISTICS [dbo].[pricegroup2] 
	



	UPDATE STATISTICS [dbo].[pricezonemap] 
	


	UPDATE STATISTICS [dbo].[ProntoDriverExclusionList] 
	



	UPDATE STATISTICS [dbo].[pwreset] 
	



	UPDATE STATISTICS [dbo].['Query Results$'] 
	



	UPDATE STATISTICS [dbo].[R1] 
	



	UPDATE STATISTICS [dbo].[rate_data] 
	



	UPDATE STATISTICS [dbo].[rating] 
	



	UPDATE STATISTICS [dbo].[receipts] 
	



	UPDATE STATISTICS [dbo].[redeliverylog] 
	



	UPDATE STATISTICS [dbo].[Redemp] 
	



	UPDATE STATISTICS [dbo].[Redemp_FinalList] 
	



	UPDATE STATISTICS [dbo].[Redemp_Master] 
	



	UPDATE STATISTICS [dbo].[Redemp_ZoneMapping] 
	



	UPDATE STATISTICS [dbo].[Redemption] 
	



	UPDATE STATISTICS [dbo].[RedemptionReport] 
	



	UPDATE STATISTICS [dbo].[RedemptionZones] 
	



	UPDATE STATISTICS [dbo].[return_info] 
	



	UPDATE STATISTICS [dbo].[RSTMP] 
	


	UPDATE STATISTICS [dbo].[salesrep] 
	



	UPDATE STATISTICS [dbo].[sessions] 
	



	UPDATE STATISTICS [dbo].[sortationdriver] 
	



	UPDATE STATISTICS [dbo].[Student] 
	



	UPDATE STATISTICS [dbo].[suburb] 
	



	UPDATE STATISTICS [dbo].[survey2015] 
	



	UPDATE STATISTICS [dbo].[sysdiagrams] 
	



	UPDATE STATISTICS [dbo].[T1] 
	



	UPDATE STATISTICS [dbo].[T2] 
	



	UPDATE STATISTICS [dbo].[tbl_CouponSales] 
	



	UPDATE STATISTICS [dbo].[tbl_DIFOTAccountNumbers_ToExcelLoad] 
	



	UPDATE STATISTICS [dbo].[tblEmail_ConsignmentStatus] 
	



	UPDATE STATISTICS [dbo].[tblEmailNotification] 
	



	UPDATE STATISTICS [dbo].[tblErrorLog] 
	



	UPDATE STATISTICS [dbo].[tblWineDeliveryCustomerStatusDeliverAPIResponse] 
	



	UPDATE STATISTICS [dbo].[Teacher] 
	



	UPDATE STATISTICS [dbo].[Teacher_Reloaded] 
	



	UPDATE STATISTICS [dbo].[TMPBC_Scans] 
	



	UPDATE STATISTICS [dbo].[trackcompany] 
	



	UPDATE STATISTICS [dbo].[trackkey] 
	



	UPDATE STATISTICS [dbo].[transactions] 
	


	UPDATE STATISTICS [dbo].[TST] 
	



	UPDATE STATISTICS [dbo].[type] 
	



	UPDATE STATISTICS [dbo].[uctmp] 
	



	UPDATE STATISTICS [dbo].[unknowncoupons] 
	



	UPDATE STATISTICS [dbo].[unknownwork] 
	



	UPDATE STATISTICS [dbo].[useragents] 
	



	UPDATE STATISTICS [dbo].[userclass] 
	



	UPDATE STATISTICS [dbo].[usercompany] 
	



	UPDATE STATISTICS [dbo].[users] 
	



	UPDATE STATISTICS [dbo].[userwarehouse] 
	



	UPDATE STATISTICS [dbo].[utest] 
	



	UPDATE STATISTICS [dbo].[validation] 
	



	UPDATE STATISTICS [dbo].[warehouse] 
	



	UPDATE STATISTICS [dbo].[Wendi1] 
	



	UPDATE STATISTICS [dbo].[Wendi2] 
	



	UPDATE STATISTICS [dbo].[x1] 
	


	UPDATE STATISTICS [dbo].[zonegrid] 
	



	UPDATE STATISTICS [dbo].[zonemaps] 
	



	UPDATE STATISTICS [dbo].[zoneprice] 
	



	UPDATE STATISTICS [dbo].[zones] 
	



	UPDATE STATISTICS [dbo].[zones1] 
	



	UPDATE STATISTICS [dbo].[zoning] 
	



	UPDATE STATISTICS [dbo].[zoning1] 
	

--Shrink DB

	DBCC SHRINKDATABASE(N'CpplEDI')


END
GO
