SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


Create Procedure [dbo].[SP_CPPLEDIMaintenance]

@StartDate datetime = null --'2015-05-28'

As 
Begin
Begin Try
BEGIN TRAN 

--EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

		PRINT '-------- START DISABLING TABLE CONSTRAINT --------';

		ALTER TABLE enquiryd NOCHECK CONSTRAINT ALL
		ALTER TABLE manifest NOCHECK CONSTRAINT ALL
		ALTER TABLE PrepaidRevenueProtection NOCHECK CONSTRAINT ALL
		ALTER TABLE PrepaidRevenueProtectionDeleted NOCHECK CONSTRAINT ALL


		
		PRINT '-------- END DISABLING TABLE CONSTRAINT --------';

				PRINT '-------- START COPYING DATA--------';
				------------Add Table ActivityImport Record In Archive-----------------------

				------------Add Table Booking Record In Archive-----------------------

				PRINT '-------- COPY TCONSIGNMENT STAGING TABLE--------';
				INSERT INTO enquiryd_Archive2017 
				select * from enquiryd with(NoLock) where convert(Date, "Date", 103) <= @StartDate 


				INSERT INTO manifest_Archive2017 
				select * from manifest with(NoLock) where convert(Date, m_Date, 103) <= @StartDate 

				INSERT INTO PrepaidRevenueProtection_Archive2017 
				select * from PrepaidRevenueProtection with(NoLock) where convert(Date, Createddatetime, 103) <= @StartDate 
				
								INSERT INTO PrepaidRevenueProtectionDeleted_Archive2017 
				select * from PrepaidRevenueProtectionDeleted with(NoLock) where convert(Date, "date", 103) <= @StartDate 

				----------------------------------------------------------------------------------------------------------------------------
				PRINT '-------- END COPYING DATA --------';

		----Delete Record from table---------------------

				PRINT '-------- DELETE DATA START --------';

				Delete from enquiryd where convert(Date, "Date", 103) <= @StartDate 
		
				Delete from manifest where convert(Date, m_Date, 103) <= @StartDate

				Delete from PrepaidRevenueProtection where convert(Date, Createddatetime, 103) <= @StartDate

					Delete from PrepaidRevenueProtectionDeleted where convert(Date,  "date", 103) <= @StartDate
		

				PRINT '-------- DELETE DATA END --------';

		--EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
		PRINT '-------- START ENABLING TABLE CONSTRAINT --------';

	ALTER TABLE enquiryd CHECK CONSTRAINT ALL
		ALTER TABLE manifest CHECK CONSTRAINT ALL
		ALTER TABLE PrepaidRevenueProtection CHECK CONSTRAINT ALL
		ALTER TABLE PrepaidRevenueProtectionDeleted CHECK CONSTRAINT ALL

		

		PRINT '-------- END ENABLING TABLE CONSTRAINT --------';
		select 'OK' as Result

		COMMIT TRAN 
		--rollback tran
		END TRY
				BEGIN CATCH
				begin
					rollback tran
					INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
					 VALUES
						   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
						   ,'SP_CPPLEDIMaintenance', 2)

						   select cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as Result
				end
				END CATCH

END 

GO
