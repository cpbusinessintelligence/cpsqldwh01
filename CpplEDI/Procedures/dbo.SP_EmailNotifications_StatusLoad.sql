SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--EXEC [dbo].[Sp_RptDIFOT_InternalStaff_AllAccounts_Updated] 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_EmailNotifications_StatusLoad] 

WITH RECOMPILE
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;


   
Select [ConsignmentNumber]
      ,[Name]
      ,[Email]
      ,convert(datetime,'') as [EventDateTime]
      ,convert(varchar(100),'') as [EventType]
      ,[IsActive]
      ,convert(bit,0) as [EmailNotificationFlag]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
into #Temp1
from CpplEDI.[dbo].[tblEmailNotification] E (Nolock) 
 

Update T1 Set T1.EventType = 'Pickup'
       ,T1.[EventDateTime] = T.[EventDateTime] from [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join #Temp1 T1 (NOLOCK)
ON T.[SourceReference]=T1.[ConsignmentNumber] where T.[EventTypeId]='98EBB899-A15E-4826-8D05-516E744C466C'

Update T1 Set T1.EventType = 'Consolidate'
       ,T1.[EventDateTime] = T.[EventDateTime] from [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join #Temp1 T1 (NOLOCK) 
ON T.[SourceReference]=T1.[ConsignmentNumber] where T.[EventTypeId]='F47CABB2-55AA-4F19-B5EE-C2754268D1AF'

Update T1 Set T1.EventType = 'Deconsolidate'
       ,T1.[EventDateTime] = T.[EventDateTime] from [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join #Temp1 T1 (NOLOCK)
ON T.[SourceReference]=T1.[ConsignmentNumber] where T.[EventTypeId]='CE36ABD3-F2B5-425C-9220-EBCC6047BE8D'

Update T1 Set T1.EventType = 'Out For Delivery'
       ,T1.[EventDateTime] = T.[EventDateTime] from [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join #Temp1 T1 (NOLOCK)
ON T.[SourceReference]=T1.[ConsignmentNumber] where T.[EventTypeId]='93B2E381-6A89-4F2E-9131-2DC2FB300941'

Update T1 Set T1.EventType = 'Delivered'
       ,T1.[EventDateTime] = T.[EventDateTime] from [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join #Temp1 T1 (NOLOCK)
ON T.[SourceReference]=T1.[ConsignmentNumber] where T.[EventTypeId]='47CFA05F-3897-4F1F-BDF4-00C6A69152E3'



INSERT INTO [dbo].[tblEmail_ConsignmentStatus]
           ([ConsignmentNumber]
           ,[Name]
           ,[Email]
           ,[EventDateTime]
           ,[EventType]
           ,[IsActive]
           ,[EmailNotificationFlag]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[UpdatedDateTime]
           ,[UpdatedBy])

Select [ConsignmentNumber]
           ,[Name]
           ,[Email]
           ,[EventDateTime]
           ,[EventType]
           ,[IsActive]
           ,[EmailNotificationFlag]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[UpdatedDateTime]
           ,[UpdatedBy] from #Temp1 (NOLOCK)



END












GO
