SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tejes>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_RptItemsLeftInBay]  @Branch varchar(20),@Date date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT L.[LabelNumber] as LabelNumber
      ,MAX([EventDateTime]) as LastScanDateTime
	  ,datediff(Day,MAX(T.[EventDateTime]),T.[CreatedDate]) as DaysFromFirstScan
	  ,'' as Zone
	  ,De.[Name] as [Depot]
      ,D.Code as DriverCode
      ,[ProntoDriverCode]
  FROM [ScannerGateway].[dbo].[TrackingEvent] T (nolock)
  LEFT JOIN [dbo].[Label] L
   ON T.LabelId=L.Id
   LEFT JOIN [ScannerGateway].[dbo].[Branch] B (nolock)
    ON T.[CosmosBranchId]=B.[CosmosBranchId]
	LEFT JOIN [ScannerGateway].[dbo].[Driver] D (nolock)
	ON T.DriverId=D.[Id]
	LEFT JOIN [ScannerGateway].[dbo].[Depot] De (nolock)
	ON D.[DepotId]=De.Id
  where EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
  and EventDateTime <= @Date 
  and EventDatetime > = @Date-1 
  and D.[IsActive] = 1
  --and T.CosmosBranchId = 5 
  and B.[Name] =@Branch
  GROUP BY L.[LabelNumber],De.[Name],D.Code,[ProntoDriverCode],T.[CreatedDate]

END
GO
