SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Prakash Gupta>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Rpt_ConsignmentLabelTrack] 
	-- Add the parameters for the stored procedure here
	@Inputparam NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE  @whereclause NVARCHAR(20)
IF EXISTS (select * from [CpplEDI].[dbo].[consignment](Nolock) A where A.cd_connote =  @Inputparam)
BEGIN
set @whereclause = 'A.cd_connote'
Print 'Record found in [CpplEDI].[dbo].[consignment] '
END
ELSE
BEGIN
IF EXISTS(select * from [EzyFreight].[dbo].[tblRedelivery] (Nolock) B where B.LableNumber  = @Inputparam)
BEGIN
set @whereclause = 'B.LableNumber'
Print 'Record found in [EzyFreight].[dbo].[tblRedelivery] '
END
ELSE
BEGIN
IF EXISTS(select * from [EzyFreight].[dbo].[tblRedirectedConsignment] (Nolock) C where c.ConsignmentCode= @Inputparam)  
BEGIN
set @whereclause = 'c.ConsignmentCode'
Print 'Record found in [EzyFreight].[dbo].[tblRedirectedConsignment] '
END
ELSE
BEGIN
set @whereclause = 'A.cd_connote'
Print 'No Record Found'
END
END
END

--select
--A.cd_account,
--A.cd_id,
--A.cd_connote,
--A.cd_date,
--A.cd_consignment_date,
--A.cd_pickup_addr0 as PickupAddress0,
--A.cd_pickup_addr1 as PickupAddress1,
--A.cd_pickup_suburb  PickupSuberb,
--A.cd_pickup_postcode as PickupPostcode,
--A.cd_pickup_contact as PickupContact,
--A.cd_pickup_contact_phone as PickupPhone,
--A.cd_delivery_addr0 as DeliveryAddress0,
--A.cd_delivery_addr1 as DeliveryAddress1,
--A.cd_delivery_email as DeliveryEmail,
--A.cd_delivery_suburb as DeliverySuberb,
--A.cd_delivery_postcode as DeliveryPostcode, 
--A.cd_delivery_contact_phone as DeliveryPhone,
--A.cd_special_instructions as SpecialInstruction,
--A.cd_items as Items,
--A.cd_deadweight as Deadweight,
--A.cd_volume as Volunme,
--A.cd_measured_deadweight as Measured_Deadweight,
--A.cd_measured_volume as Measured_Volume,


--B.RedeliveryType,
--B.CardReferenceNumber,
--B.LableNumber,
--B.ConsignmentCode,
--B.Branch,
--B.State,
--B.AttemptedRedeliveryTime,
--B.SenderName,
--B.NumberOfItem,
--B.DestinationName,
--B.DestinationAddress,
--B.DestinationSuburb,
--B.DestinationPostCode,
--B.SelectedETADate,
--B.CreatedDateTime,
--B.UpdatedDateTime,
--B.IsProcessed,
--B.Firstname,
--B.Lastname,
--B.Email,
--B.PhoneNumber,
--B.SPInstruction,

--C.ConsignmentCode,
--C.SelectedDeliveryOption,
--C.CurrentETA,
--C.NewETA,
--C.NoOfItems,
--C.ATL,
--C.IsProcessed,
--C.CreatedDateTime,
--C.UpdatedDateTTime,
--C.isscannerGWdataprocessed,

--SM.ContextID,
--SM.FromAddr,
--SM.ToAddr,
--SM.Bcc,
--SM.Parameter1,
--SM.Parameter3,
--SM.TransmitFlag,
--SM.AddDateTime,
--SM.ModifiedDateTime,
--SM.iserror,
--SM.ReturnAPICode,

--T.EventTypeId , 
--T.EventDateTime , 
--T.DriverId, 
--T.AdditionalText1 , 
--T.SourceReference, 

--L.LabelNumber,
--L.CreatedDate



--	from [CpplEDI].[dbo].[consignment](Nolock) A left join [EzyFreight].[dbo].[tblRedelivery] (Nolock) B on A.cd_connote = B.ConsignmentCode 
--                                                 left join [EzyFreight].[dbo].[tblRedirectedConsignment] (Nolock) C on A.cd_connote = C.ConsignmentCode
--													left  join ScannerGateway..label (Nolock) L on L.LabelNumber= B.LableNumber
--													left  join ScannerGateway.dbo.TrackingEvent (NOLOCK) T on L.ID = T.LabelId 
--													left join Redirection..sendmail (Nolock) SM on SM.parameter1= A.cd_connote
--													where A.cd_connote = 'CPAR21E0028701'
											


Declare @STRSQL NVARCHAR(MAX)
SET @STRSQL= ' Select 
A.cd_account,
A.cd_id,
A.cd_connote,
A.cd_date,
A.cd_consignment_date,
A.cd_pickup_addr0 as PickupAddress0,
A.cd_pickup_addr1 as PickupAddress1,
A.cd_pickup_suburb  PickupSuberb,
A.cd_pickup_postcode as PickupPostcode,
A.cd_pickup_contact as PickupContact,
A.cd_pickup_contact_phone as PickupPhone,
A.cd_delivery_addr0 as DeliveryAddress0,
A.cd_delivery_addr1 as DeliveryAddress1,
A.cd_delivery_email as DeliveryEmail,
A.cd_delivery_suburb as DeliverySuberb,
A.cd_delivery_postcode as DeliveryPostcode, 
A.cd_delivery_contact_phone as DeliveryPhone,
A.cd_special_instructions as SpecialInstruction,
A.cd_items as Items,
A.cd_deadweight as Deadweight,
A.cd_volume as Volunme,
A.cd_measured_deadweight as Measured_Deadweight,
A.cd_measured_volume as Measured_Volume,


B.RedeliveryType,
B.CardReferenceNumber,
B.LableNumber,
B.ConsignmentCode,
B.Branch,
B.State,
B.AttemptedRedeliveryTime,
B.SenderName,
B.NumberOfItem,
B.DestinationName,
B.DestinationAddress,
B.DestinationSuburb,
B.DestinationPostCode,
B.SelectedETADate,
B.CreatedDateTime,
B.UpdatedDateTime,
B.IsProcessed,
B.Firstname,
B.Lastname,
B.Email,
B.PhoneNumber,
B.SPInstruction,

C.ConsignmentCode,
C.SelectedDeliveryOption,
C.CurrentETA,
C.NewETA,
C.NoOfItems,
C.ATL,
C.IsProcessed,
C.CreatedDateTime,
C.UpdatedDateTTime,
C.isscannerGWdataprocessed,

SM.ContextID,
SM.FromAddr,
SM.ToAddr,
SM.Bcc,
SM.Parameter1,
SM.Parameter3,
SM.TransmitFlag,
SM.AddDateTime,
SM.ModifiedDateTime,
SM.iserror,
SM.ReturnAPICode,

T.EventTypeId , 
T.EventDateTime , 
T.DriverId, 
T.AdditionalText1 , 
T.SourceReference, 

L.LabelNumber,
L.CreatedDate



	from [CpplEDI].[dbo].[consignment](Nolock) A left join [EzyFreight].[dbo].[tblRedelivery] (Nolock) B on A.cd_connote = B.ConsignmentCode 
                                                 left join [EzyFreight].[dbo].[tblRedirectedConsignment] (Nolock) C on A.cd_connote = C.ConsignmentCode
													left  join ScannerGateway..label (Nolock) L on L.LabelNumber= B.LableNumber
													left  join ScannerGateway.dbo.TrackingEvent (NOLOCK) T on L.ID = T.LabelId 
													left join Redirection..sendmail (Nolock) SM on SM.parameter1= A.cd_connote
													 where ' + @whereclause + ' = ' + '''' + @Inputparam + ''''


exec sp_executesql @STRSQL
END
GO
