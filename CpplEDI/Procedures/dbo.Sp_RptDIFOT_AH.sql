SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDIFOT_AH] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

 
  CREATE TABLE TempScanners(
	        [ScannerNumber] [varchar](20) NULL,
	        [Name] [varchar](50) NULL)
	        
	 -- INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6500','QUERY FREIGHT')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6501','SHORT/SPLIT CONSIGNMENT')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6502','INCORRECT/INSUFFICIENT ADDRESS')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6503','DAMAGED')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6504','NO FREIGHT')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6505','CLOSED')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6506','CONNOTE REQUIRED')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6507','CARD LEFT')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6508','DG PAPERWORK REQUIRED')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6509','REFUSED')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6510','NO ACCESS TO LEAVE CARD')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6511','UNSAFE TO LEAVE')
	  INSERT INTO  TempScanners ([ScannerNumber],[Name]) VALUES ('6513', 'Return to Sender')
      CREATE clustered index TempScannersidx ON TempScanners([ScannerNumber])

	  CREATE TABLE TempDIFOT (
	  Consignment  varchar(30) NULL,
         AccountNumber varchar(30) NULL,
		 Items varchar(30) NULL, 
		 cd_date datetime , 
		 cd_pickup_suburb VarcHAR(50) null,
		 cd_pickup_postcode VARCHAR (20) NULL, 
		   Category varchar(20), 
		  SubCategory varchar(20), 
		   Exceptions varchar(120),
        PickupZone varchar(20), 
		 cd_delivery_addr0 varchar(50),
		 cd_delivery_addr1  varchar(50),
		 cd_delivery_addr2 varchar(50),
		 cd_delivery_suburb varchar(50),
		 cd_delivery_postcode  varchar(50), 
		  DeliveryState varchar(20),
          DeliveryZone varchar(20),
		  NetworkCategoryID varchar(20),
		  NetworkCategory varchar(20),
		  FromETA varchar(20),
		  ToETA varchar(20),
		   ETA varchar(20),
   	      ETADate DATETIME,
		  StatusID  varchar(20),
		  StatusDescription  varchar(50),
		cc_coupon  VARCHAR(30),
		PickupDate DATETIME,
		  OutForDeliveryDate DATETIME,
		 AttemptedDeliveryDate DATETIME,
		  AttemptedDeliveryCard varchar(20),
		 DeliveryDate DATETIME,
		 Total INT,
		  [PickupScannedBy] varchar(20),
		  [AttemptedDeliveryScannedBy] varchar(20),
		 [DeliveryScannedBy] varchar(20))

  insert INTO TempDIFOT
Select  cd_Connote as Consignment ,
         cd_Account as AccountNumber,
		 cd_items as Items, 
		 cd_date , 
		 cd_pickup_suburb ,
		 cd_pickup_postcode, 
		  convert(varchar(20),'') as Category, 
		  convert(varchar(20),'') as SubCategory, 
		  convert(varchar(120),'') as Exceptions,
         convert(varchar(20),'') as PickupZone, 
		 cd_delivery_addr0,
		 cd_delivery_addr1,
		 cd_delivery_addr2,
		 cd_delivery_suburb,
		 cd_delivery_postcode , 
		 convert(varchar(20),'') as DeliveryState,
         convert(varchar(20),'') as DeliveryZone,
		 convert(varchar(20),'') as NetworkCategoryID,
		 convert(varchar(20),'') as NetworkCategory,
		 convert(varchar(20),'') as FromETA,
		 convert(varchar(20),'') as ToETA,
		  convert(varchar(20),'') as ETA,
   	     convert(Datetime,Null) as ETADate,
		 convert(varchar(20),'') as StatusID,
		 convert(varchar(50),'') as StatusDescription,
		 Rtrim(Ltrim(Replace(cc_coupon,' ',''))) as cc_coupon,
		 convert(Datetime,Null) as PickupDate,
		 convert(Datetime,Null) as OutForDeliveryDate,
		 convert(Datetime,Null) as AttemptedDeliveryDate,
		 convert(varchar(20),'') as AttemptedDeliveryCard,
		 convert(Datetime,Null) as DeliveryDate,
		 convert(int,Null) as Total,
		 convert(varchar(20),'') as [PickupScannedBy],
		 convert(varchar(20),'') as [AttemptedDeliveryScannedBy],
		convert(varchar(20),'') as [DeliveryScannedBy]


from CpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
Where  --cd_account = @AccountNumber
--and 
cd_date >= '2020-01-01' and cd_date <= '2020-04-01'   
and cd_delivery_addr0 <> 'Iconic C/O Seko' and cd_release = 'Y'

--@StartDate@EndDate
Update TempDIFOT SET PickupZone= P.ETAZone From TempDIFOT T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_pickup_postcode = P.PostCode and T.cd_pickup_suburb = P.Suburb
Update TempDIFOT SET DeliveryZOne= P.ETAZone From TempDIFOT T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode and T.cd_delivery_suburb = P.Suburb
Update TempDIFOT SET DeliveryState= P.State From TempDIFOT T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode 
Update TempDIFOT SET PickupZone= P.ETAZone From TempDIFOT T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_pickup_postcode = P.PostCode  where PickupZone =''
Update TempDIFOT SET DeliveryZOne= P.ETAZone From TempDIFOT T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode  where DeliveryZone = ''




Update TempDIFOT SET FromETA  = E.FromETA ,
                  ToETA = E.ToETA,
				  ETA = E.ETA,
				  NetworkCategory = E.PrimaryNetworkCategory
   From TempDIFOT T join  PerformanceReporting.[dbo].ETACalculator E on T.PickupZone  = E.FromZone and T.DeliveryZone = E.ToZone WHere E.Category = 'Standard'

  Delete   from TempDIFOT where ETA ='XXX'

 Update TempDIFOT SET PickupDate =  E.EventDateTime  From TempDIFOT T join ScannerGateway.[dbo].[Label] L On  T.cc_coupon = L.LabelNumber
												 join ScannerGateway.dbo.TrackingEvent E on L.ID = 	E.LabelId
									Where E.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' 
									             

 Update TempDIFOT SET OutForDeliveryDate =  E.EventDateTime  From TempDIFOT T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
												 join ScannerGateway.dbo.TrackingEvent E on L.ID = 	E.LabelId
									Where  E.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941'


 Update TempDIFOT SET AttemptedDeliveryDate =  E.EventDateTime  
                          From TempDIFOT T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
												 join ScannerGateway.dbo.TrackingEvent E on L.ID = 	E.LabelId
									Where E.EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' 

Update TempDIFOT SET AttemptedDeliveryCard = replace(additionaltext1,'Link Coupon ',''), AttemptedDeliveryDate = E.EventDateTime  
From TempDIFOT T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference
 Where eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA' or additionaltext1 like 'Link Coupon 191%') 


 Update TempDIFOT SET DeliveryDate =  E.EventDateTime  
                          From TempDIFOT T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
												 join ScannerGateway.dbo.TrackingEvent E on L.ID = 	E.LabelId
									Where E.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' 


Update TempDIFOT SEt Exceptions = Left(S.Name,20) from TempDIFOT T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
												 join ScannerGateway.dbo.TrackingEvent E on L.ID = 	E.LabelId
												 Join TempScanners S on E.AdditionalText1 = S.ScannerNumber
												 Where E.EventTYpeID = 'E293FFDE-76E3-4E69-BCEB-473F91B4350C' and AdditionalText1 like '65%'

Update TempDIFOT SET Category = 'Redirection' ,SubCategory = R.[SelectedDeliveryOption] From TempDIFOT T join EzYFreight.dbo.tblRedirectedConsignment R on T.Consignment= R.ConsignmentCode Where IsProcessed =1


Update TempDIFOT SET ETADate  = PerformanceReporting.[dbo].[fn_CalculateDomesticWeekendsandPublicHolidays] (PickupDate,PickupZone,DeliveryZone,Case WHEN Category = 'Redirection' and subcategory in ('POPPoint','Alternate Address') THEN ToETA+1 ELSE ToETA END) where PickupDate is not null 


Update TempDIFOT SET StatusDescription =  'No Activity' WHere  PickupDate is null and OutForDeliveryDate is null and AttemptedDeliveryDate is null and DeliveryDate is null and StatusDescription = ''
Update TempDIFOT Set StatusDescription =  'Cant Calculate' where ETADate is null and StatusDescription = ''
Update TempDIFOT SET StatusDescription =  'Exceptions' Where  Exceptions <>'' and StatusDescription = ''
Update TempDIFOT SET StatusDescription =  'On Time' Where Datediff(day,ETADate,Isnull(AttemptedDeliveryDate,DeliveryDate))<=0 and ETADate is not null and StatusDescription = ''
Update TempDIFOT SET StatusDescription =  'Not On Time' Where Datediff(day,ETADate,Isnull(AttemptedDeliveryDate,DeliveryDate))>0 and ETADate is not null and StatusDescription = ''

Update TempDIFOT SET StatusDescription =  CASE WHEN Datediff(day,ETADate,GETDATE()) <=0 THEN 'On TIme' ELSE 'Not On Time' END  Where 	StatusDescription = ''



Select * from TempDIFOT 
END

GO
