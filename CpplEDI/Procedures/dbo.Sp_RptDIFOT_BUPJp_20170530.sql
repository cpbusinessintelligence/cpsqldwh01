SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDIFOT_BUPJp_20170530] @AccountNumber varchar(30), @StartDate Date, @EndDate Date,@BusinessDays int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

   
Select   cd_Connote ,
         cd_Account,
              cd_items, 
               cd_date , 
               cd_pickup_suburb ,
              cd_pickup_postcode, 
         convert(varchar(20),'') as PickupZone, 
               cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
               convert(varchar(20),'') as DeliveryState,
         convert(varchar(20),'') as DeliveryZone,
              convert(varchar(20),'') as NetworkCategoryID,
              convert(varchar(20),'') as NetworkCategory,
              convert(varchar(20),'') as FromETA,
              convert(varchar(20),'') as ToETA,
                convert(varchar(20),'') as ETA,
            convert(Datetime,Null) as ETADate,
              convert(varchar(20),'') as StatusID,
              convert(varchar(50),'') as StatusDescription,
                cc_coupon,
              convert(Datetime,Null) as FirstActivityDatetime,
              convert(varchar(50),'') as FirstScanType,
              convert(Datetime,Null) as PickupDate,
              convert(Datetime,Null) as OutForDeliveryDate,
              convert(Datetime,Null) as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard,
              convert(Datetime,Null) as DeliveryDate,
			  convert(int,Null) as Total,
			  convert(varchar(20),'') as [PickupScannedBy],
			  convert(varchar(20),'') as [AttemptedDeliveryScannedBy],
			  convert(varchar(20),'') as [DeliveryScannedBy]

into #Temp1
from CpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
Where  cd_account = @AccountNumber
and cd_date >= @StartDate and cd_date <= @EndDate  
--@StartDate@EndDate
Update #Temp1 SET PickupZone= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_pickup_postcode = P.PostCode and T.cd_pickup_suburb = P.Suburb
Update #Temp1 SET DeliveryZOne= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode and T.cd_delivery_suburb = P.Suburb
Update #Temp1 SET DeliveryState= P.State From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode 

Update #Temp1 SET FromETA  = E.FromETA ,
                  ToETA = E.ToETA,
                             ETA = E.ETA,
                             NetworkCategory = E.PrimaryNetworkCategory

   From #Temp1 T join  PerformanceReporting.[dbo].ETACalculator E on T.PickupZone  = E.FromZone and T.DeliveryZone = E.ToZone 


Update #Temp1 SET    FirstActivityDatetime = P. FirstDateTime,
                                    FirstScanType =  P.FirstScanType,
                                    PickupDate = P.PickupDateTime,
                                    OutForDeliveryDate = P.OutForDeliverDateTime,
                                    AttemptedDeliveryDate = P.AttemptedDeliveryDateTime,
                                    AttemptedDeliveryCard=P.AttemptedDeliveryCardNumber,
                                    DeliveryDate=P.DeliveryDateTime,
									ETADate=P.ETADate,
                                    NetworkCategoryId = P.[NetworkCategoryID],
                                    StatusID = P.[OnTimeStatusId],
									[PickupScannedBy]=isnull(p.[PickupScannedBy],'Unknown'),
									[AttemptedDeliveryScannedBy]=isnull(p.[AttemptedDeliveryScannedBy],'Unknown'),
									[DeliveryScannedBy]=isnull(p.[DeliveryScannedBy],'Unknown')

  From #Temp1 T join PerformanceReporting.[dbo].[PrimaryLabels] P on T.cc_coupon =P.LabelNumber

 
 Update #Temp1 Set StatusDescription = Case WHEN T.StatusID ='1' THen 'On Time'
                                                                           WHEN T.StatusID ='2' THen ''       
                                                                           WHEN T.StatusID ='4' THen 'On Time'  
                                                                           WHEN T.StatusID ='5' THen ''  
                                                                           WHEN T.StatusID ='6' THen 'On Time' 
                                                                           WHEN T.StatusID ='7' THen '' 
                                                                           WHEN T.StatusID ='8' THen '' 
                                                                           WHEN T.StatusID ='9' THen ''
                                                                           WHEN T.StatusID = '12' Then ''
                                                                           ELSE ''
                                                                           END
  From #Temp1 t Join PerformanceReporting.[dbo].[DimStatus] D on T.StatusID = D.ID





Update #Temp1 SET                 FirstActivityDatetime = Null,
                                                FirstScanType='',
                                                PickupDate= Null,
                                                OutForDeliveryDate =  Null,
                                                AttemptedDeliveryDate =  Null,
                                                DeliveryDate = Null
Where StatusDescription = ''

Update #Temp1 SET PickupDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.PickupDate is null and E.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' and StatusDescription = ''

Update #Temp1 SET OutForDeliveryDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.OutForDeliveryDate is null and E.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' and StatusDescription = ''
           
 Update #Temp1 SET AttemptedDeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.AttemptedDeliveryDate is null and E.EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' and StatusDescription = ''

Update #Temp1 SET DeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.DeliveryDate is null and E.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and StatusDescription = ''

															                                                                                       
Update #Temp1 SET ETADate  = PerformanceReporting.[dbo].[fn_CalculateDomesticWeekendsandPublicHolidays] (PickupDate,PickupZone,DeliveryZone,@BusinessDays ) where PickupDate is not null and ETADate is null
Update #Temp1 SET StatusDescription =  'No Activity' WHere  PickupDate is null and OutForDeliveryDate is null and AttemptedDeliveryDate is null and DeliveryDate is null

Update #Temp1 Set StatusDescription = 'Cant Calculate' where PickupDate is null and StatusDescription = ''
Update #Temp1 SET StatusDescription =  CASE  WHEN (AttemptedDeliveryDate is null and DeliveryDate is null and  DateDiff(day, ETADate,Getdate()) >=0) THEN 'In Progress' ELSE 'Not On Time' ENd WHere  StatusDescription = '' and AttemptedDeliveryDate is null and DeliveryDate is null
Update #Temp1 Set StatusDescription = CASE when DateDiff(day, Isnull(AttemptedDeliveryDate,DeliveryDate),ETADate)>=0 Then 'On Time' ELse 'Not On Time' END where  StatusDescription = ''

update #Temp1 Set Total = (select count(*) from #Temp1 where StatusDescription='Not On Time') where #temp1.StatusDescription='Not On Time'

Select * from #Temp1 
END
GO
