SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_RptDIFOT_IntStaff_LinehaulCalculations] @WeekEndingDate Date

WITH RECOMPILE
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;


--SELECT COALESCE(NULLIF(PickupState,''), 'N/A') as PickupState
--      ,sum(case when [Category] LIKE '%NoPickUpScan%' then 1 ELSE 0 end) as PickupCount
--	  ,sum(case when Category LIKE '%MISSED%' then 1 ELSE 0 end) as MissedCount
--	  ,sum(case when [Category] LIKE '%NoDeliveryScan%' then 1 ELSE 0 end) as NoDeliveryScanCount
--	  ,sum(case when Category LIKE '%Connected%' then 1 ELSE 0 end) as ConnectedCount
--	  ,convert(decimal(13,2),sum(case when [Category] LIKE '%NoPickUpScan%' then 1 ELSE 0 end)*100/Count(*)) as NoPickUpPercentage
--	  ,convert(decimal(13,2),sum(case when Category LIKE '%MISSED%' then 1 ELSE 0 end)*100/count(*)) as MissedPercentage
--	  ,convert(decimal(13,2),sum(case when [Category] LIKE '%NoDeliveryScan%' then 1 ELSE 0 end)*100/Count(*)) as NoDeliveryPercentage
--	  ,convert(decimal(13,2),sum(case when Category LIKE '%Connected%' then 1 ELSE 0 end)*100/count(*)) as ConnectedPercentage
--	  ,count(*) as Total
--FROM [CpplEDI].[dbo].[DIFOT_InternalStaffDetails]
--where convert(date,WeekEndingDate)= @WeekEndingDate
--group by PickupState

SELECT COALESCE(NULLIF(PickupState,''), 'N/A') as PickupState
      ,COALESCE(NULLIF(DeliveryState,''), 'N/A') as DeliveryState
      ,sum(case when [Category] LIKE '%NoPickUpScan%' then 1 ELSE 0 end) as PickupCount
	  ,sum(case when Category LIKE '%MISSED%' then 1 ELSE 0 end) as MissedCount
	  ,sum(case when [Category] LIKE '%NoDeliveryScan%' then 1 ELSE 0 end) as NoDeliveryScanCount
	  ,sum(case when Category LIKE '%Connected%' then 1 ELSE 0 end) as ConnectedCount
	  ,convert(decimal(13,2),sum(case when [Category] LIKE '%NoPickUpScan%' then 1 ELSE 0 end)*100/Count(*)) as NoPickUpPercentage
	  ,convert(decimal(13,2),sum(case when Category LIKE '%MISSED%' then 1 ELSE 0 end)*100/count(*)) as MissedPercentage
	  ,convert(decimal(13,2),sum(case when [Category] LIKE '%NoDeliveryScan%' then 1 ELSE 0 end)*100/Count(*)) as NoDeliveryPercentage
	  ,convert(decimal(13,2),sum(case when Category LIKE '%Connected%' then 1 ELSE 0 end)*100/count(*)) as ConnectedPercentage
	  ,count(*) as Total
FROM [CpplEDI].[dbo].[DIFOT_InternalStaffDetails]
where [NetworkCategory]='INTERSTATE' and convert(date,[WeekEndingDate])= @WeekEndingDate
group by PickupState,DeliveryState
order by PickupState asc,DeliveryState ASC

END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptDIFOT_IntStaff_LinehaulCalculations]
	TO [ReportUser]
GO
