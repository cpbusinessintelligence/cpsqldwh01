SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--EXEC [dbo].[Sp_RptDIFOT_InternalStaff_AllAccounts_Updated] 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDIFOT_InternalStaff_AllAccounts_Updated] (@WeekEndingDate Date)

WITH RECOMPILE
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

--DECLARE @WeekEndingDate Date

--SET @WeekEndingDate=getdate()

DECLARE @StartDate Date
DECLARE @EndDate Date
DECLARE @BusinessDays int

--SET @WeekEndingDate='2017-06-24'

SET @StartDate = DATEADD(dd, -(DATEPART(dw, @WeekEndingDate)-1), @WeekEndingDate)
SET @EndDate =DATEADD(dd, 7-(DATEPART(dw, @WeekEndingDate)), @WeekEndingDate) 
SET @BusinessDays = 1


   
Select   cd_Connote as ConsignmentNumber,
         cd_Account as AccountCode,
		 R.AccountName as AccountName,
              cd_items, 
               cd_date , 
               cd_pickup_suburb ,
              cd_pickup_postcode, 
         convert(varchar(20),'') as PickupZone,
		 convert(varchar(20),'') as PickupState,
               cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
               convert(varchar(20),'') as DeliveryState,
         convert(varchar(20),'') as DeliveryZone,
              convert(varchar(20),'') as NetworkCategoryID,
              convert(varchar(20),'') as NetworkCategory,
              convert(varchar(20),'') as FromETA,
              convert(varchar(20),'') as ToETA,
                convert(varchar(20),'') as ETA,
            convert(Datetime,Null) as ETADate,
              convert(varchar(20),'') as StatusID,
              convert(varchar(50),'') as StatusDescription,
			  convert(varchar(50),'') as Category,
                cc_coupon,
              convert(Datetime,Null) as FirstActivityDatetime,
              convert(varchar(50),'') as FirstScanType,
              convert(Datetime,Null) as PickupDate,
              convert(Datetime,Null) as OutForDeliveryDate,
              convert(Datetime,Null) as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard,
              convert(Datetime,Null) as DeliveryDate,
			  convert(varchar(20),'') as [PickupScannedBy],
			  convert(varchar(20),'') as [AttemptedDeliveryScannedBy],
			  convert(varchar(20),'') as [DeliveryScannedBy],
			  convert(Datetime,Null) as ConsolidateScannedAt,
			  convert(varchar(20),'') as [InterstateConnectivity]
into #Temp1
from CpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
join [CpplEDI].[dbo].[OpsAccountsforReporting] R (Nolock) on cd_account =R.AccountCode
Where  cd_account = R.AccountCode
and convert(date,cd_date) >= @StartDate and convert(date,cd_date) <= @EndDate  
--@StartDate@EndDate
Update #Temp1 SET PickupZone= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P (Nolock) on T.cd_pickup_postcode = P.PostCode and T.cd_pickup_suburb = P.Suburb
Update #Temp1 SET PickupState= P.State From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P (Nolock) on T.cd_pickup_postcode = P.PostCode
Update #Temp1 SET DeliveryZOne= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P (Nolock) on T.cd_delivery_postcode = P.PostCode and T.cd_delivery_suburb = P.Suburb
Update #Temp1 SET DeliveryState= P.State From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P (Nolock) on T.cd_delivery_postcode = P.PostCode 

Update #Temp1 SET FromETA  = E.FromETA ,
                  ToETA = E.ToETA,
                             ETA = E.ETA,
                             NetworkCategory = E.PrimaryNetworkCategory

   From #Temp1 T join  PerformanceReporting.[dbo].ETACalculator E (Nolock) on T.PickupZone  = E.FromZone and T.DeliveryZone = E.ToZone 

Update #Temp1 SET    FirstActivityDatetime = P. FirstDateTime,
                                    FirstScanType =  P.FirstScanType,
                                    PickupDate = P.PickupDateTime,
                                    OutForDeliveryDate = P.OutForDeliverDateTime,
                                    AttemptedDeliveryDate = P.AttemptedDeliveryDateTime,
                                    AttemptedDeliveryCard=P.AttemptedDeliveryCardNumber,
                                    DeliveryDate=P.DeliveryDateTime,
                                    ETADate = P.ETADate,
                                    NetworkCategoryId = P.[NetworkCategoryID],
                                    StatusID = P.[OnTimeStatusId],
									[PickupScannedBy]=isnull(p.[PickupScannedBy],'Unknown'),
									[AttemptedDeliveryScannedBy]=isnull(p.[AttemptedDeliveryScannedBy],'Unknown'),
									[DeliveryScannedBy]=isnull(p.[DeliveryScannedBy],'Unknown')
  From #Temp1 T join PerformanceReporting.[dbo].[PrimaryLabels] P (Nolock) on T.cc_coupon =P.LabelNumber

 
 Update #Temp1 Set StatusDescription = Case WHEN T.StatusID ='1' THen 'On Time'
                                                                           WHEN T.StatusID ='2' THen ''       
                                                                           WHEN T.StatusID ='4' THen 'On Time'  
                                                                           WHEN T.StatusID ='5' THen ''  
                                                                           WHEN T.StatusID ='6' THen 'On Time' 
                                                                           WHEN T.StatusID ='7' THen '' 
                                                                           WHEN T.StatusID ='8' THen '' 
                                                                           WHEN T.StatusID ='9' THen ''
                                                                           WHEN T.StatusID = '12' Then ''
                                                                           ELSE ''
                                                                           END
  From #Temp1 t Join PerformanceReporting.[dbo].[DimStatus] D (Nolock) on T.StatusID = D.ID

Update #Temp1 SET                 FirstActivityDatetime = Null,
                                                FirstScanType='',
                                                PickupDate= Null,
                                                OutForDeliveryDate =  Null,
                                                AttemptedDeliveryDate =  Null,
                                                DeliveryDate = Null
Where StatusDescription = ''

Update #Temp1 SET PickupDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.PickupDate is null and E.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' 
															  --and StatusDescription = ''

Update #Temp1 SET OutForDeliveryDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.OutForDeliveryDate is null and E.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' 
															  --and StatusDescription = ''
           
 Update #Temp1 SET AttemptedDeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.AttemptedDeliveryDate is null and E.EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' 
															  --and StatusDescription = ''

Update #Temp1 SET DeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.DeliveryDate is null and E.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' 
															  --and StatusDescription = ''

Update #Temp1 SET ConsolidateScannedAt =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.ConsolidateScannedAt is null and E.EventTypeId = 'F47CABB2-55AA-4F19-B5EE-C2754268D1AF' 
															  --and StatusDescription = ''

Update #Temp1 SET [InterstateConnectivity]	= Case  WHEN ConsolidateScannedAt is null Then 'No Scan' WHen convert(date,PickupDate)= convert(date,ConsolidateScannedAt) THEN ' Connected' Else 'Missed' END WHere NetworkCategory = 'Interstate'														                                                                                       
Update #Temp1 SET ETADate  = PerformanceReporting.[dbo].[fn_CalculateDomesticWeekendsandPublicHolidays] (PickupDate,PickupZone,DeliveryZone,4) where PickupDate is not null and ETADate is null
Update #Temp1 SET StatusDescription =  'No Activity' WHere  PickupDate is null and OutForDeliveryDate is null and AttemptedDeliveryDate is null and DeliveryDate is null

Update #Temp1 Set StatusDescription = 'Cant Calculate' where PickupDate is null and StatusDescription = ''
Update #Temp1 SET StatusDescription =  CASE  WHEN (AttemptedDeliveryDate is null and DeliveryDate is null and  DateDiff(day, ETADate,Getdate()) >=0) THEN 'In Progress' ELSE 'Not On Time' ENd WHere  StatusDescription = '' and AttemptedDeliveryDate is null and DeliveryDate is null
Update #Temp1 Set StatusDescription = CASE when DateDiff(day, Isnull(AttemptedDeliveryDate,DeliveryDate),ETADate)>=0 Then 'On Time' ELse 'Not On Time' END where  StatusDescription = ''


--Update #Temp1 Set Category = 'NoPickupScan' where PickupDate is null 
--Update #Temp1 Set Category = 'NoDeliveryScan' where DeliveryDate is null
--Update #Temp1 SET Category =  'Missed' Where DateDiff(day, PickupDate,DeliveryDate) > 0 
--Update #Temp1 SET Category =  'Connected' Where DateDiff(day, PickupDate,DeliveryDate) = 0 

Update #Temp1 Set Category = 'NoPickupScan' where convert(date,PickupDate) is null 
Update #Temp1 Set Category = 'NoDeliveryScan' where convert(date,DeliveryDate) is null
Update #Temp1 Set Category = 'NoConsolidatedScan' where convert(date,ConsolidateScannedAt) is null
Update #Temp1 SET Category =  'Missed' Where DateDiff(day, convert(date,PickupDate),convert(date,ConsolidateScannedAt)) > 0 
Update #Temp1 SET Category =  'Connected' Where DateDiff(day,convert(date,PickupDate),convert(date,ConsolidateScannedAt)) = 0



IF EXISTS (SELECT * FROM [CpplEDI].[dbo].[DIFOT_InternalStaffDetails] WHERE convert(date,[WeekEndingDate])=convert(date, @WeekEndingDate))

 DELETE FROM [CpplEDI].[dbo].[DIFOT_InternalStaffDetails] WHERE datepart(MM,[WeekEndingDate]) < datepart(MM,getdate())-3



INSERT INTO [CpplEDI].[dbo].[DIFOT_InternalStaffDetails]
([ConsignmentNumber]
      ,[AccountCode]
      ,[AccountName]
      ,[NoofItems]
      ,[ConsignmentDate]
      ,[PickupSuburb]
      ,[PickupPostcode]
      ,[PickupZone]
      ,[PickupState]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[DeliverySuburb]
      ,[DeliveryPostCode]
      ,[DeliveryState]
      ,[DeliveryZone]
      ,[NetworkCategory]
      ,[NetworkID]
      ,[FromETA]
      ,[ToETA]
      ,[ETA]
      ,[ETADate]
      ,[StatusID]
      ,[Status]
	  ,[Category]
      ,[LabelNumber]
      ,[FirstActivityDatetime]
      ,[FirstScanType]
      ,[PickupDate]
      ,[OutForDeliveryDate]
      ,[AttemptedDeliveryDate]
      ,[AttemptedDeliveryCard]
      ,[DeliveryDate]
      ,[PickupScannedBy]
      ,[AttemptedDeliveryScannedBy]
      ,[DeliveryScannedBy]
      ,[ConsolidateScannedAt]
      ,[WeekEndingDate]
      ,[InterstateConnectivity]
      ,[CreatedDate])
Select   [ConsignmentNumber],
         MAX(AccountCode) as AccountCode,
              MAX(UPPER(AccountName)) as AccountName,
         MAX(cd_items) as NoofItems, 
         Min(Convert(Date,cd_date)) as ConsignmentDate ,  
         MAX(UPPER(cd_pickup_suburb)) as PickupSuburb,
         MAX(UPPER(cd_pickup_postcode)) as PickupPostcode,
         MAX(UPPER(PickupZone)) as PickupZone,
		 MAX(UPPER(PickupState)) as PickupState,
              MAX(UPPER(cd_delivery_addr0)) as Address1,
         MAX(UPPER(cd_delivery_addr1)) as Address2,
              MAX(UPPER(cd_delivery_addr2)) as Address3,
         MAX(UPPER(cd_delivery_suburb)) as DeliverySuburb,
         MAX(UPPER(cd_delivery_postcode)) as DeliveryPostCode, 
         MAX(UPPER(DeliveryState)) as DeliveryState,
         MAX(UPPER(DeliveryZone)) as DeliveryZone,
              MAX(UPPER(NetworkCategory)) as NetworkCategory,
         Max(NetworkCategoryID) as NetworkID,
         Max(FromETA) as FromETA,
         Max(ToETA) as ToETA,
        MAX(UPPER(ETA)) as ETA,
        MAX(Convert(Date,ETADate)) as ETADate,
        MAX(StatusID) as StatusID,
        MAX(UPPER(StatusDescription)) as Status,
		MAX(UPPER(Category)) as Category,
        MAX(cc_coupon) as LabelNumber,
        Max(FirstActivityDatetime) as FirstActivityDatetime ,
        MAX(FirstScanType) as FirstScanType,
        Max(PickupDate) as PickupDate,
        Max(OutForDeliveryDate) as OutForDeliveryDate,
        Max(AttemptedDeliveryDate) as AttemptedDeliveryDate,
        Max(AttemptedDeliveryCard) as AttemptedDeliveryCard,
        Max(DeliveryDate) as DeliveryDate,
        MAX([PickupScannedBy]) as PickupScannedBy,
        MAX([AttemptedDeliveryScannedBy]) as AttemptedDeliveryScannedBy,
        MAX([DeliveryScannedBy]) as DeliveryScannedBy,
        MAX([ConsolidateScannedAt]) as ConsolidateScannedAt,
		MAX(@WeekEndingDate) as WeekEndingDate,
        MAX([InterstateConnectivity]) as InterstateConnectivity,
		MAX(getdate()) as CreatedDate
        From #Temp1 
Group By  ConsignmentNumber

END












GO
GRANT EXECUTE
	ON [dbo].[Sp_RptDIFOT_InternalStaff_AllAccounts_Updated]
	TO [ReportUser]
GO
