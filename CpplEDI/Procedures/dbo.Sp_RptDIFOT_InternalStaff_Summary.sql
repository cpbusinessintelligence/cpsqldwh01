SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDIFOT_InternalStaff_Summary] @WeekEndingDate Date

WITH RECOMPILE
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

SELECT [ConsignmentNumber]
      ,[AccountCode]
      ,[AccountName]
      ,[NoofItems]
      ,[ConsignmentDate]
      ,[PickupSuburb]
      ,[PickupPostcode]
      ,[PickupZone]
      ,[PickupState]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[DeliverySuburb]
      ,[DeliveryPostCode]
      ,[DeliveryState]
      ,[DeliveryZone]
      ,[NetworkCategory]
      ,[NetworkID]
      ,[FromETA]
      ,[ToETA]
      ,[ETA]
      ,[ETADate]
      ,[StatusID]
      ,[Status]
      ,[Category]
      ,[LabelNumber]
      ,[FirstActivityDatetime]
      ,[FirstScanType]
      ,[PickupDate]
      ,[OutForDeliveryDate]
      ,[AttemptedDeliveryDate]
      ,[AttemptedDeliveryCard]
      ,[DeliveryDate]
      ,[PickupScannedBy]
      ,[AttemptedDeliveryScannedBy]
      ,[DeliveryScannedBy]
      ,[ConsolidateScannedAt]
      ,[WeekEndingDate]
      ,[InterstateConnectivity]
      ,[CreatedDate]
  FROM CpplEDI.[dbo].[DIFOT_InternalStaffDetails]
  where convert(date,WeekEndingDate)=@WeekEndingDate

END


GO
GRANT EXECUTE
	ON [dbo].[Sp_RptDIFOT_InternalStaff_Summary]
	TO [ReportUser]
GO
