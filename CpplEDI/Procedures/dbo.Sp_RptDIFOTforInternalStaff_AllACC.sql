SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDIFOTforInternalStaff_AllACC] @StartDate Date, @EndDate Date,@BusinessDays int

WITH RECOMPILE
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

   
Select   cd_Connote as ConsignmentNumber,
         cd_Account as AccountCode,
		 R.AccountName as AccountName,
              cd_items, 
               cd_date , 
               cd_pickup_suburb ,
              cd_pickup_postcode, 
         convert(varchar(20),'') as PickupZone, 
               cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
               convert(varchar(20),'') as DeliveryState,
         convert(varchar(20),'') as DeliveryZone,
              convert(varchar(20),'') as NetworkCategoryID,
              convert(varchar(20),'') as NetworkCategory,
              convert(varchar(20),'') as FromETA,
              convert(varchar(20),'') as ToETA,
                convert(varchar(20),'') as ETA,
            convert(Datetime,Null) as ETADate,
              convert(varchar(20),'') as StatusID,
              convert(varchar(50),'') as StatusDescription,
                cc_coupon,
              convert(Datetime,Null) as FirstActivityDatetime,
              convert(varchar(50),'') as FirstScanType,
              convert(Datetime,Null) as PickupDate,
              convert(Datetime,Null) as OutForDeliveryDate,
              convert(Datetime,Null) as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard,
              convert(Datetime,Null) as DeliveryDate,
			  convert(int,Null) as TotalByCustomer,
			  convert(int,Null) as TotalOnTimeByCustomer,
			  convert(decimal(12,2),0) as PercentageByCustomer,
			  convert(bigint,Null) as NoScanTotal,
			  --convert(decimal(12,2),0) as NoScanPercentage,
			    convert(bigint,Null) as ConnectedTotal,
			  --convert(decimal(12,2),0) as ConnectedPercentage,
			    convert(bigint,Null) as MissedCountTotal,
			  --convert(decimal(12,2),0) as MissedCountPercentage,
			  --convert(int,Null) as Total,
			  convert(varchar(20),'') as [PickupScannedBy],
			  convert(varchar(20),'') as [AttemptedDeliveryScannedBy],
			  convert(varchar(20),'') as [DeliveryScannedBy],
			  convert(Datetime,Null) as ConsolidateScannedAt,
			  convert(varchar(20),'') as [InterstateConnectivity]
into #Temp1
from CpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
join [dbo].[OpsAccountsforReporting] R (Nolock) on cd_account =R.AccountCode
Where  cd_account = R.AccountCode
and convert(date,cd_date) >= @StartDate and convert(date,cd_date) <= @EndDate  
--@StartDate@EndDate
Update #Temp1 SET PickupZone= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P (Nolock) on T.cd_pickup_postcode = P.PostCode and T.cd_pickup_suburb = P.Suburb
Update #Temp1 SET DeliveryZOne= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P (Nolock) on T.cd_delivery_postcode = P.PostCode and T.cd_delivery_suburb = P.Suburb
Update #Temp1 SET DeliveryState= P.State From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P (Nolock) on T.cd_delivery_postcode = P.PostCode 

Update #Temp1 SET FromETA  = E.FromETA ,
                  ToETA = E.ToETA,
                             ETA = E.ETA,
                             NetworkCategory = E.PrimaryNetworkCategory

   From #Temp1 T join  PerformanceReporting.[dbo].ETACalculator E (Nolock) on T.PickupZone  = E.FromZone and T.DeliveryZone = E.ToZone 


Update #Temp1 SET    FirstActivityDatetime = P. FirstDateTime,
                                    FirstScanType =  P.FirstScanType,
                                    PickupDate = P.PickupDateTime,
                                    OutForDeliveryDate = P.OutForDeliverDateTime,
                                    AttemptedDeliveryDate = P.AttemptedDeliveryDateTime,
                                    AttemptedDeliveryCard=P.AttemptedDeliveryCardNumber,
                                    DeliveryDate=P.DeliveryDateTime,
                                    ETADate = P.ETADate,
                                    NetworkCategoryId = P.[NetworkCategoryID],
                                    StatusID = P.[OnTimeStatusId],
									[PickupScannedBy]=isnull(p.[PickupScannedBy],'Unknown'),
									[AttemptedDeliveryScannedBy]=isnull(p.[AttemptedDeliveryScannedBy],'Unknown'),
									[DeliveryScannedBy]=isnull(p.[DeliveryScannedBy],'Unknown')

  From #Temp1 T join PerformanceReporting.[dbo].[PrimaryLabels] P (Nolock) on T.cc_coupon =P.LabelNumber

 
 Update #Temp1 Set StatusDescription = Case WHEN T.StatusID ='1' THen 'On Time'
                                                                           WHEN T.StatusID ='2' THen ''       
                                                                           WHEN T.StatusID ='4' THen 'On Time'  
                                                                           WHEN T.StatusID ='5' THen ''  
                                                                           WHEN T.StatusID ='6' THen 'On Time' 
                                                                           WHEN T.StatusID ='7' THen '' 
                                                                           WHEN T.StatusID ='8' THen '' 
                                                                           WHEN T.StatusID ='9' THen ''
                                                                           WHEN T.StatusID = '12' Then ''
                                                                           ELSE ''
                                                                           END
  From #Temp1 t Join PerformanceReporting.[dbo].[DimStatus] D (Nolock) on T.StatusID = D.ID





Update #Temp1 SET                 FirstActivityDatetime = Null,
                                                FirstScanType='',
                                                PickupDate= Null,
                                                OutForDeliveryDate =  Null,
                                                AttemptedDeliveryDate =  Null,
                                                DeliveryDate = Null
Where StatusDescription = ''

Update #Temp1 SET PickupDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.PickupDate is null and E.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' and StatusDescription = ''

Update #Temp1 SET OutForDeliveryDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.OutForDeliveryDate is null and E.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' and StatusDescription = ''
           
 Update #Temp1 SET AttemptedDeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.AttemptedDeliveryDate is null and E.EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' and StatusDescription = ''

Update #Temp1 SET DeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.DeliveryDate is null and E.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and StatusDescription = ''

Update #Temp1 SET ConsolidateScannedAt =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E (Nolock) on L.ID =    E.LabelId
                                                              Where T.DeliveryDate is null and E.EventTypeId = 'F47CABB2-55AA-4F19-B5EE-C2754268D1AF' and StatusDescription = ''

Update #Temp1 SET [InterstateConnectivity]	= Case  WHEN ConsolidateScannedAt is null Then 'No Scan' WHen convert(date,PickupDate)= convert(date,ConsolidateScannedAt) THEN ' Connected' Else 'Missed' END WHere NetworkCategory = 'Interstate'														                                                                                       
Update #Temp1 SET ETADate  = PerformanceReporting.[dbo].[fn_CalculateDomesticWeekendsandPublicHolidays] (PickupDate,PickupZone,DeliveryZone,4) where PickupDate is not null and ETADate is null
Update #Temp1 SET StatusDescription =  'No Activity' WHere  PickupDate is null and OutForDeliveryDate is null and AttemptedDeliveryDate is null and DeliveryDate is null

Update #Temp1 Set StatusDescription = 'Cant Calculate' where PickupDate is null and StatusDescription = ''
Update #Temp1 SET StatusDescription =  CASE  WHEN (AttemptedDeliveryDate is null and DeliveryDate is null and  DateDiff(day, ETADate,Getdate()) >=0) THEN 'In Progress' ELSE 'Not On Time' ENd WHere  StatusDescription = '' and AttemptedDeliveryDate is null and DeliveryDate is null
Update #Temp1 Set StatusDescription = CASE when DateDiff(day, Isnull(AttemptedDeliveryDate,DeliveryDate),ETADate)>=0 Then 'On Time' ELse 'Not On Time' END where  StatusDescription = ''

Select * into #temp2 From #Temp1
Update #Temp1 SET TotalByCustomer =  (Select count(*) from #Temp2 Where #Temp2.AccountCode = #Temp1.AccountCode)
Update #Temp1 SET TotalOnTimeByCustomer =  (Select count(*) from #Temp2 Where #Temp2.AccountCode = #Temp1.AccountCode and #Temp2.StatusDescription= 'On Time')
Update #Temp1 SET PercentageByCustomer= convert(decimal(13,2),TotalOnTimeByCustomer)*100.0/convert(decimal(13,2),TotalByCustomer) Where TotalByCustomer >0
--update #Temp1 
--set DeliveryState = (CASE 
--                      WHEN DeliveryState in (2751,3086,3086)
--                        THEN 'NA'
--                    END);

update #Temp1 
set NoScanTotal = (CASE 
                      WHEN InterstateConnectivity = 'No Scan'
                        THEN '1'
						WHEN InterstateConnectivity = ' '
                        THEN '0'
						WHEN InterstateConnectivity = 'Connected'
                        THEN '0'
						 WHEN InterstateConnectivity = 'MISSED'
                        THEN '0'
                    END);

update #Temp1 
set ConnectedTotal = (CASE 
                      WHEN InterstateConnectivity = 'Connected'
                        THEN '1'
						WHEN InterstateConnectivity = ' '
                        THEN '0'
						 WHEN InterstateConnectivity = 'MISSED'
                        THEN '0'
						WHEN InterstateConnectivity = 'No Scan'
                        THEN '0'
                    END);

update #Temp1 
set MissedCountTotal = (CASE 
                      WHEN InterstateConnectivity = ' '
                        THEN '1'
                      WHEN InterstateConnectivity = 'MISSED'
                        THEN '1'
						WHEN InterstateConnectivity = 'Connected'
                        THEN '0'
						WHEN InterstateConnectivity = 'No Scan'
                        THEN '0'
                    END);
--Update #Temp1 SET NoScanTotal =  (Select count(*) from #Temp2 Where #Temp2.AccountCode = #Temp1.AccountCode and InterstateConnectivity = 'No Scan')
--Update #Temp1 SET TotalOnTimeByCustomer =  (Select count(*) from #Temp2 Where #Temp2.AccountCode = #Temp1.AccountCode and #Temp2.StatusDescription= 'On Time')

--Update #Temp1 SET NoScanPercentage= (Select count(*) from #Temp1 where NoScanTotal='1')*100/TotalByCustomer Where TotalByCustomer >0
--Update #Temp1 SET ConnectedPercentage= (Select count(*) from #Temp1 where ConnectedTotal='1')*100/TotalByCustomer Where TotalByCustomer >0
--Update #Temp1 SET MissedCountPercentage= (Select count(*) from #Temp1 where MissedCountTotal='1')*100/TotalByCustomer Where TotalByCustomer >0

--Update #Temp1 SET NoScanTotal =  (Select count(*) from #Temp2 Where #Temp2.AccountCode = #Temp1.AccountCode and #Temp2.InterstateConnectivity = 'No Scan')
--Update #Temp1 SET NoScanPercentage= NoScanTotal*100.0/TotalByCustomer Where TotalByCustomer >0

--Update #Temp1 SET ConnectedTotal =  (Select count(*) from #Temp2 Where #Temp2.AccountCode = #Temp1.AccountCode and #Temp2.InterstateConnectivity = 'Connected')
--Update #Temp1 SET ConnectedPercentage= ConnectedTotal*100.0/TotalByCustomer Where TotalByCustomer >0

--Update #Temp1 SET MissedCountTotal =  (Select count(*) from #Temp2 Where #Temp2.AccountCode = #Temp1.AccountCode and #Temp2.InterstateConnectivity = ' ')
--Update #Temp1 SET MissedCountPercentage= MissedCountTotal*100.0/TotalByCustomer Where TotalByCustomer >0


select * from #Temp1 (Nolock) order by StatusDescription Asc
END










GO
GRANT EXECUTE
	ON [dbo].[Sp_RptDIFOTforInternalStaff_AllACC]
	TO [ReportUser]
GO
