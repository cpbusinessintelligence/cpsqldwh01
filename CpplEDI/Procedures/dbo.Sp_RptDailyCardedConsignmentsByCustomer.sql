SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyCardedConsignmentsByCustomer] @AccountCode varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select   cd_Connote ,
         cc_coupon,
         cd_Account,
              cd_items, 
               cd_date ,
              cd_pickup_suburb ,
              cd_pickup_postcode, 
 
               cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 

              convert(Datetime,Null)  as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard

into #Temp1
from cpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
Where  cd_account = @AccountCode
and cd_date >= Getdate()-30 

Update #Temp1 SET AttemptedDeliveryCard = replace(additionaltext1,'Link Coupon ',''), AttemptedDeliveryDate = E.EventDateTime  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference Where
eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA' or additionaltext1 like 'Link Coupon 191%') 
 
Select * from #Temp1 where AttemptedDeliveryDate between dateadd(day,datediff(day,1,GETDATE()),0) and dateadd(day,datediff(day,0,GETDATE()),0)
END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptDailyCardedConsignmentsByCustomer]
	TO [ReportUser]
GO
