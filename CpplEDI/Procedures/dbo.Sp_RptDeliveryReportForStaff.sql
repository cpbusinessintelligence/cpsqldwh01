SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--[Sp_RptDeliveryReportForStaff] '113058192','2018-02-01','2018-02-06'
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDeliveryReportForStaff] @AccountCode varchar(20),@StartDate Date,@EndDate Date 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

				select c.cd_id,c.cd_connote,
		c.cd_date,
		'' as Style,
		c.cd_delivery_addr0,
		c.cd_delivery_addr1,
		c.cd_delivery_addr2,
		c.cd_delivery_addr3,
		c.cd_delivery_suburb as Suburb, 
		c.cd_delivery_postcode as Postcode, 
		c.cd_pickup_suburb as picup_suburb,
		c.cd_pickup_postcode as pickup_postcode,
		convert(varchar(20),'') as State,
		c.cd_items, 
		convert(varchar(5000),'') as Coupons, 
		convert(varchar(500),'') as [References],
		c.cd_deadweight, 
		c.cd_volume,
		convert(varchar(50),'') as Status , 
		convert(varchar(100),'') as [POD Name],
		convert(varchar(20),'') as [POD Date],
		convert(varchar(20),'') as [POD Time],
		c.cd_delivery_agent as [Delivery Agent],
		convert(varchar(50),'') as [Delivery Agent Name],
		convert(varchar(100),'') as DriverId,
		convert(varchar(100),'') as Driver,
		cd_pickup_addr0 as Supplier,
		c.cd_pickup_contact as [Sender Name],
		convert(varchar(10),'') as [From Zone],
		convert(varchar(10),'') as [To Zone],
		c.cd_items as [No of Items],
		convert(varchar(200),'') as [POD URL],
		c.cd_pricecode as [Rate Card],
		convert(varchar(10),'') as [Route],
		convert(varchar(5),'') as StatusCode,
		c.cd_pickup_suburb,
		c.cd_eta_date
		into #temp
		 from consignment c 
		where cd_date between @StartDate and @EndDate
		and 	cd_account = @AccountCode
		--and cd_connote='S50527700'

		SELECT T1.cd_id,T1.cd_connote,   
				replace(STUFF(  
				(  
				SELECT ',' + T2.cc_coupon  
				FROM cdcoupon T2  
				WHERE T1.cd_id = T2.cc_consignment  
				FOR XML PATH ('')  
				),1,1,''),' ','')  as coupons
		into #temp1
		FROM #temp T1  
		--where cd_connote='CPWI55000022972'
		GROUP BY T1.cd_id,T1.cd_connote

		SELECT T1.cd_id,T1.cd_connote,   
			   replace( STUFF(  
				(  
				SELECT ',' + T2.cr_reference  
				FROM [CpplEDI].[dbo].[cdref] T2  
				WHERE T1.cd_id = T2.cr_consignment  
				FOR XML PATH ('')  
				),1,1,''),' ','')  as Reference
		into #temp2
		FROM #temp T1  
		GROUP BY T1.cd_id,T1.cd_connote

		update #temp set Coupons= cd.coupons from #temp1 cd where #temp.cd_connote=cd.cd_connote
		update #temp set  [References] =isnull(t2.Reference,'')  from #temp2 t2 where #temp.cd_connote= t2.cd_connote
		update #temp set  [Delivery Agent Name]=a.a_name from CpplEDI.dbo.agents a where #temp.[Delivery Agent]= a.a_id
		update #temp set [From zone]=performancereporting.[dbo].[fn_GetETAZone](Rtrim(Ltrim(#temp.pickup_postcode)),Rtrim(Ltrim(#temp.picup_suburb)))
		update #temp set [To zone]=performancereporting.[dbo].[fn_GetETAZone](Rtrim(Ltrim(#temp.postcode)),Rtrim(Ltrim(#temp.suburb)))	
		update #temp Set State= p.State from performancereporting.dbo.postcodes p where  p.PostCode= #temp.Postcode

		--update #temp set 
		--[POD Name]= t.AdditionalText1 , 
		--[POD Date]=convert(date,eventDateTime,120),
		--[POD Time]=convert(varchar(10),eventDateTime,108),
		--DriverID=t.DriverId,
		--[Delivery Agent]=isnull(convert(varchar(20),AgentId),''),
		--[Route]= isnull(convert(varchar(20),RunId),'')
		--from scannergateway.dbo.trackingevent t 
		--where  t.sourcereference=#temp.Coupons and t.eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
		
update #temp
Set State= p.State from performancereporting.dbo.postcodes p where  p.PostCode= #temp.Postcode
		update #temp set 
		[POD Name]= t.AdditionalText1 , 
		[POD Date]=convert(date,eventDateTime,120),
		[POD Time]=convert(varchar(10),eventDateTime,108),
		DriverID=t.DriverId,
		[Delivery Agent]=isnull(convert(varchar(20),AgentId),''),
		[Route]= isnull(convert(varchar(20),RunId),''),
		StatusCode=(case when t.eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' then 'D' 
		     when t.eventtypeid='FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' then 'A'  end )
from scannergateway.dbo.trackingevent t 
where t.sourcereference in (substring(coupons,0,15),substring(coupons,0,CHARINDEX(',', coupons, 0)))
and t.EventDateTime=(select max(t2.EventDateTime) from scannergateway.dbo.trackingevent as t2 where t.sourcereference = t2.sourcereference)

--update #temp 
--set Driver=name
--from scannergateway.dbo.driver d 
--where #temp.DriverId = d.id

--update #temp set [POD URL]='http://edi.couriersplease.com.au/index.php?fn=signature&id='+convert(varchar(50),CosmosSignatureId)+'&coupon='+convert(varchar(20),t.coupons)+'&branch='+convert(varchar(10),CosmosBranchId)
--from #temp t join [ScannerGateway].dbo.Label l on l.LabelNumber in (substring(coupons,0,15),substring(coupons,0,CHARINDEX(',', coupons, 0)))
--	left join  [ScannerGateway].dbo.TrackingEvent te on te.LabelId= l.Id
--	where CosmosSignatureId<>0

select * from #temp 


END

GO
GRANT EXECUTE
	ON [dbo].[Sp_RptDeliveryReportForStaff]
	TO [ReportUser]
GO
