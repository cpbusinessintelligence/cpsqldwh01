SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptExtendedDIFOTforRSComponents] @Year int, @Month int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @StartDate DateTime;
	Declare @EndDate DateTIme;

-- First date of month
SET @StartDate = DATEADD(MONTH, @Month-1, DATEADD(YEAR, @Year-1900, 0));
-- Last date of month
SET @EndDate = DATEADD(SS, -1, DATEADD(MONTH, @Month, DATEADD(YEAR, @Year-1900, 0)))

	--Set @StartDate=DATEADD(month,@Month-1,DATEADD(year,@Year-1900,0))
	--set @EndDate = DATEADD(day,-1,DATEADD(month,@Month,DATEADD(year,@Year-1900,0))) + '23:59:59:000'  ;
		
    CREATE TABLE #TempScanners(
               [ScannerNumber] [varchar](20) NULL,
               [Name] [varchar](50) NULL)
               
        -- INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6500','QUERY FREIGHT')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6501','SHORT/SPLIT CONSIGNMENT')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6502','INCORRECT/INSUFFICIENT ADDRESS')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6503','DAMAGED')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6504','NO FREIGHT')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6505','CLOSED')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6506','CONNOTE REQUIRED')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6507','CARD LEFT')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6508','DG PAPERWORK REQUIRED')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6509','REFUSED')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6510','NO ACCESS TO LEAVE CARD')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6511','UNSAFE TO LEAVE')
         INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6513', 'RETURN TO SENDER')
      CREATE clustered index #TempScannersidx ON #TempScanners([ScannerNumber])
	  
--SELECT  [Accountcode]
--      ,[BillTo]
--      ,[Shortname]
--  INTO #TempAccounts
--  FROM [Pronto].[dbo].[ProntoDebtor]
--    Where Accountcode in ('112814702', '112987045' ,'112833348','112984265' , '113014054' ,'112987540' , '112987532')


Select   cd_Connote ,
         cd_Account,
              cd_items, 
               cd_date , 
               cd_pickup_suburb ,
              cd_pickup_postcode, 
                convert(varchar(20),'') as Category, 
                convert(varchar(20),'') as SubCategory, 
                convert(varchar(120),'') as Exceptions,
         convert(varchar(20),'') as PickupZone, 
               cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
               convert(varchar(20),'') as DeliveryState,
         convert(varchar(20),'') as DeliveryZone,
              convert(varchar(20),'') as NetworkCategoryID,
              convert(varchar(20),'') as NetworkCategory,
              convert(varchar(20),'') as FromETA,
              convert(varchar(20),'') as ToETA,
                convert(varchar(20),'') as ETA,
            convert(Datetime,Null) as ETADate,
              convert(varchar(20),'') as StatusID,
              convert(varchar(50),'') as StatusDescription,
              Rtrim(Ltrim(Replace(cc_coupon,' ',''))) as cc_coupon,
              convert(Datetime,Null) as PickupDate,
              convert(Datetime,Null) as OutForDeliveryDate,
              convert(Datetime,Null) as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard,
              convert(Datetime,Null) as DeliveryDate,
			  convert(int,null) as Total

into #Temp1 
from cpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
                                       --Join #TempAccounts A on C.cd_account = A.AccountCode
Where  cd_date >= @StartDate and  cd_date <= @EndDate  and cd_delivery_addr0 <> 'Iconic C/O Seko' and 
cd_account in ('112814702', '112987045' ,'112833348','112984265' , '113014054' ,'112987540' , '112987532')

--Finding Zones

Update #Temp1 SET DeliveryState= CASE cd_Account When '112814702' THEN 'NSW_SmithField'
                                                  When '112987045' THEN 'QLD'
												  WHEN '112833348' THEN 'NSW_Geodis'
												  WHEN '112984265' THEN 'VIC'
												  WHEN '113014054' THEN 'WA'
												  WHEN '112987540' THEN 'N/A'
												  WHEN '112987532' THEN 'N/A2'
												  ELSE 'UNKNOWN'
												  END
												  
 From #Temp1 

Update #Temp1 SET PickupZone= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_pickup_postcode = P.PostCode and T.cd_pickup_suburb = P.Suburb
Update #Temp1 SET DeliveryZOne= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode and T.cd_delivery_suburb = P.Suburb
--Update #Temp1 SET DeliveryState= CASE cd_Account When '112814702' THEN 'NSW_SmithField'
--                                                  When '112987045' THEN 'QLD'
--												  WHEN '112833348' THEN 'NSW_Geodis'
--												  WHEN '112984265' THEN 'VIC'
--												  WHEN '113014054' THEN 'WA'
--												  WHEN '112987540' THEN 'N/A'
--												  WHEN '112987532' THEN 'N/A2'
--												  ELSE 'UNKNOWN'
--												  END 


 --From #Temp1 
--Update #Temp1 SET PickupZone= MAX(P.ETAZone) From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_pickup_postcode = P.PostCode  where PickupZone =''
--Update #Temp1 SET DeliveryZOne= MAX(P.ETAZone) From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode  where DeliveryZone = ''

Update #Temp1 SET FromETA  = E.FromETA ,
                  ToETA = E.ToETA,
                             ETA = E.ETA,
                             NetworkCategory = E.PrimaryNetworkCategory
   From #Temp1 T join  PerformanceReporting.[dbo].ETACalculator E on T.PickupZone  = E.FromZone and T.DeliveryZone = E.ToZone WHere E.Category = 'Standard'

  Delete   from #temp1 where ETA ='XXX'

  --select distinct deliverystate,cd_account from #Temp1

  Update #Temp1 SET FromETA = 1 where FromEta=0
     
 Update #Temp1 SET PickupDate =  E.EventDateTime From #Temp1 T join ScannerGateway.[dbo].[Label] L On  T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEventTMP E on L.ID =    E.LabelId
                                                              Where E.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' 
                                                                           

Update #Temp1 SET OutForDeliveryDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEventTMP E on L.ID =    E.LabelId
                                                              Where  E.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941'


Update #Temp1 SET AttemptedDeliveryDate =  E.EventDateTime
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEventTMP E on L.ID =    E.LabelId
                                                              Where E.EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' 

Update #Temp1 SET AttemptedDeliveryCard = replace(additionaltext1,'Link Coupon ',''), AttemptedDeliveryDate = E.EventDateTime
From #Temp1 T Join ScannerGateway.dbo.TrackingEventTMP E on T.cc_coupon = E.SourceReference
Where eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA' or additionaltext1 like 'Link Coupon 191%') 


Update #Temp1 SET DeliveryDate =  E.EventDateTime
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEventTMP E on L.ID =    E.LabelId
                                                              Where E.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' 


Update #Temp1 SEt Exceptions = Left(S.Name,20) from #Temp1 T join ScannerGateway.[dbo].[Label] L On  Rtrim(Ltrim(T.cc_coupon)) = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEventTMP E on L.ID =    E.LabelId
                                                                                  Join #TempScanners S on E.AdditionalText1 = S.ScannerNumber
                                                                                  Where E.EventTYpeID = 'E293FFDE-76E3-4E69-BCEB-473F91B4350C' and AdditionalText1 like '65%'

Update #Temp1 SET SubCategory = R.[SelectedDeliveryOption] From #Temp1 T join EzYFreight.dbo.tblRedirectedConsignment R on T.cd_connote= R.ConsignmentCode Where IsProcessed =1
Update #Temp1 SET StatusDescription= 'Redelivery', Category = 'Redelivery',  SubCategory = CASE WHEN AttemptedDeliveryCard LIke '%SLCNA' THEN    'POPStation' WHEN  AttemptedDeliveryCard LIke '%RTCNA' THEN  'POPShop' WHEN  AttemptedDeliveryCard LIke '%DPCNA' THEN  'Depot' WHEN  AttemptedDeliveryCard LIke '191%' THEN  'Depot' ELSE '' END WHere AttemptedDeliveryCard<>'' and Category =''
--Update #Temp1 SET Category = 'Normal Redelivery' Where  Category =''

Update #Temp1 SET ETADate  = PerformanceReporting.[dbo].[fn_CalculateDomesticWeekendsandPublicHolidays] (PickupDate,PickupZone,DeliveryZone,Case WHEN Category = 'Redirection' and subcategory in ('POPPoint','Alternate Address') THEN ToETA+1 ELSE ToETA END) where PickupDate is not null and ETA <>''
Update #Temp1 SET StatusDescription =  'No Activity' WHere  PickupDate is null and OutForDeliveryDate is null and AttemptedDeliveryDate is null and DeliveryDate is null and StatusDescription = ''
Update #Temp1 Set StatusDescription =  'Cant Calculate' where ETADate is null and StatusDescription = ''
Update #temp1 SET StatusDescription =  'Exceptions' Where  Exceptions <>'' and StatusDescription = ''
Update #temp1 SET StatusDescription =  'On Time' Where Datediff(day,ETADate,Isnull(AttemptedDeliveryDate,DeliveryDate))<=0 and ETADate is not null and StatusDescription = ''
Update #temp1 SET StatusDescription =  'Not On Time' Where Datediff(day,ETADate,Isnull(AttemptedDeliveryDate,DeliveryDate))>0 and ETADate is not null and StatusDescription = ''
Update #temp1 SET StatusDescription =  CASE WHEN Datediff(day,ETADate,GETDATE()) <=0 THEN 'On Time' ELSE 'Not On Time' END  Where     StatusDescription = ''
update #temp1 SET Total= (select count(*) from #temp1)

UPDATE #Temp1 SET Category = 'NOT ON TIME' Where  StatusDescription ='Not On Time'
UPDATE #Temp1 SET Category = 'ON TIME' Where  StatusDescription ='On Time'
UPDATE #Temp1 SET Category = 'ON TIME' Where  StatusDescription ='No Activity'
UPDATE #Temp1 SET Category = 'ON TIME' Where  StatusDescription ='Cant Calculate'
UPDATE #Temp1 SET Category = 'ON TIME' Where  StatusDescription ='Redelivery'
UPDATE #Temp1 SET Category = 'ON TIME' Where  StatusDescription ='Exceptions'

--select distinct cd_account,deliverystate from #Temp1

--select convert(varchar(4),cd_date,120)+RIGHT('0' + RTRIM(MONTH(cd_date)), 2) as MonthKey,
--LEFT(DATENAME(MONTH,cd_date),3)+'-'+convert(varchar(4),cd_date,120) as [month],
--cd_Account as Account,
----DeliveryState,
--convert(varchar(20),NULL) as DeliveryState,
--98.5 as KPI,
--sum(case [Category] when 'On Time' then 1 else 0 end) as performanceCount,
--count(cd_connote) as Total,
--convert(decimal(10,2), 0) as performance 
--into #Temp5
--from #Temp1
--group by convert(varchar(4),cd_date,120)+RIGHT('0' + RTRIM(MONTH(cd_date)), 2) ,
--LEFT(DATENAME(MONTH,cd_date),3)+'-'+convert(varchar(4),cd_date,120),
--cd_Account,DeliveryState

--update #Temp5 SET performance = performanceCount*100.0/Total Where Total >0

--Update t1 set t1.DeliveryState =t.DeliveryState from #Temp1 t join  #Temp5 t1 on  t.cd_Account=t1.Account
--select * from #Temp5

----DROP table #Temp5

--select * from #Temp1
----------------------------------------------------------------

select cd_Connote as Consignment ,
           (select MAX(cd_Account) from #Temp1  where tt.cd_connote=cd_connote group by cd_connote) as Account,
              cd_items as NoofItems, 
               Convert(Date,cd_date) as ConsignmentDate , 
               cd_pickup_suburb as PickupSuburb,
             cd_pickup_postcode as PickupPostcode, 
               Category as Category, 
              SubCategory as SubCategory, 
              Exceptions as Exceptions,
         PickupZone as PickupZone, 
              cd_delivery_addr0 as Address1,
              cd_delivery_addr1 as Address2,
      cd_delivery_suburb as DeliverySuburb,
             cd_delivery_postcode as DeliveryPostCode, 
              DeliveryState as DeliveryState,
         DeliveryZone as DeliveryZone,
         NetworkCategory as NetworkCategory,
        ETA,
            ETADate,
              StatusDescription as Status,
                PickupDate as PickupDate,
                OutForDeliveryDate as OutForDeliveryDate,
                AttemptedDeliveryDate as AttemptedDeliveryDate,
                AttemptedDeliveryCard,
                DeliveryDate
				,(select max(Total) from #Temp1  where tt.cd_connote=cd_connote group by cd_connote) as Total
				into #temp2
 from #Temp1 tt;

INSERT INTO [dbo].[ExtendedDIFOT_RSComponents]
([monthkey],
[MonthName],
[account],
[DeliveryState],
[KPI],
[performancecount],
[total],
[performance])
select convert(varchar(4),[ConsignmentDate],120)+RIGHT('0' + RTRIM(MONTH([ConsignmentDate])), 2) as MonthKey,
LEFT(DATENAME(MONTH,[ConsignmentDate]),3)+'-'+convert(varchar(4),[ConsignmentDate],120) as [month],
Account,
--DeliveryState,
convert(varchar(20),NULL) as DeliveryState,
98.5 as KPI,
sum(case [Status] when 'On Time' then 1 else 0 end) as performanceCount,
count(consignment) as Total,
convert(decimal(10,2), 0) as performance 
from #Temp2
group by convert(varchar(4),ConsignmentDate,120)+RIGHT('0' + RTRIM(MONTH(ConsignmentDate)), 2) ,
LEFT(DATENAME(MONTH,ConsignmentDate),3)+'-'+convert(varchar(4),ConsignmentDate,120),
Account,DeliveryState

Update t1 set t1.DeliveryState =t.DeliveryState from #Temp2 t join [dbo].[ExtendedDIFOT_RSComponents] t1 on  t.Account=t1.Account

update [dbo].[ExtendedDIFOT_RSComponents] SET performance = performanceCount*100.0/Total Where Total >0

----DROP table #Temp3

--select * from #Temp3

select cd_Connote as Consignment ,
          --MAX(cd_Account) as Account,
		  (select MAX(cd_Account) from #Temp1  where tt.cd_connote=cd_connote group by cd_connote) as Account,
              cd_items as NoofItems, 
               Convert(Date,cd_date) as ConsignmentDate , 
               cd_pickup_suburb as PickupSuburb,
             cd_pickup_postcode as PickupPostcode, 
               Category as Category, 
              SubCategory as SubCategory, 
              Exceptions as Exceptions,
         PickupZone as PickupZone, 
              cd_delivery_addr0 as Address1,
              cd_delivery_addr1 as Address2,
      cd_delivery_suburb as DeliverySuburb,
             cd_delivery_postcode as DeliveryPostCode, 
              DeliveryState as DeliveryState,
         DeliveryZone as DeliveryZone,
         NetworkCategory as NetworkCategory,
        ETA,
            ETADate,
              StatusDescription as Status,
                PickupDate as PickupDate,
                OutForDeliveryDate as OutForDeliveryDate,
                AttemptedDeliveryDate as AttemptedDeliveryDate,
                AttemptedDeliveryCard,
                DeliveryDate
				,(select max(Total) from #Temp1  where tt.cd_connote=cd_connote group by cd_connote) as Total
 from #Temp1 tt;

 END





-- SELECT
--    cd_connote, COUNT(*)
--FROM
--    #Temp1
--GROUP BY
--    cd_connote
--HAVING 
--    COUNT(*) > 1

GO
GRANT EXECUTE
	ON [dbo].[Sp_RptExtendedDIFOTforRSComponents]
	TO [ReportUser]
GO
