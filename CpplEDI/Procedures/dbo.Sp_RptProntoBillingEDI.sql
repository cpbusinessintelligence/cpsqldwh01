SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO









-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Pronto Billing,,>

--113035356 WI13035356



--EXEC [Sp_RptProntoBilling_SS_03072017] '113035356|113003651|||','2017-01-01','2017-07-07'

-- =============================================

CREATE PROCEDURE [dbo].[Sp_RptProntoBillingEDI] (@AccountCode varchar(2000), @StartDate Date, @EndDate Date) AS 

BEGIN

	

	SET NOCOUNT ON;

--SET @AccountCode = '112877139|113075030|113102321|112726070|112951223|113102511|112904388|113029631|112655618|113102339'

SELECT Item into #Temp
FROM CpplEdi..[Split](@AccountCode)


SELECT d.Accountcode into #temp3 FROM #temp t jOIN Pronto..ProntoDebtor d ON t.Item= d.Accountcode



SELECT 

       DATEPART (MM,a.AccountingDate) AS BillingMonth

       ,DATEPART (YY,a.AccountingDate) AS BillingYear

       ,a.BillingDate

       ,a.AccountingDate

       ,a.AccountCode

       ,a.AccountName

       ,a.AccountBillToCode

       ,a.AccountBillToName

       ,a.ConsignmentReference

       ,a.ConsignmentDate

       ,a.ServiceCode

       ,a.ItemQuantity

       ,a.DeclaredWeight

       ,a.DeclaredVolume

       ,a.ChargeableWeight

       ,a.TariffId

       ,a.OriginLocality

       ,a.OriginPostcode

       ,a.RevenueOriginZone

       ,a.DestinationLocality

       ,a.DestinationPostcode

       ,a.RevenueDestinationZone

       ,a.BilledFreightCharge

       ,a.BilledFuelSurcharge

       ,a.BilledInsurance

       ,a.BilledTotal

  

INTO #Temp1

 

FROM CPsqLDWH01.pRONTO.[dbo].[ProntoBilling] AS A

  INNER JOIN #temp3 as B

  ON(A.Accountcode = B.Accountcode)

where 

--and AccountingDate > DATEADD (MM, -6 ,GETDATE())

 BillingDate BETWEEN @StartDate AND @EndDate

 --and a.AccountBillToCode in (SELECT item FROM #temp)

--order by AccountingDate desc





SELECT

       a.BillingMonth

       ,a.BillingYear

       ,a.BillingDate

       ,a.AccountingDate

       ,a.AccountCode

       ,a.AccountName

       ,a.AccountBillToCode

       ,a.AccountBillToName

       ,a.ConsignmentReference

       ,a.ConsignmentDate

       ,a.ServiceCode

       ,a.TariffId

       ,a.OriginPostcode

       ,a.OriginLocality

       ,a.RevenueOriginZone

       ,a.DestinationPostcode

       ,a.DestinationLocality

       ,a.RevenueDestinationZone

		,a.DeclaredVolume

		,a.DeclaredWeight

		,a.ItemQuantity

	   ,Convert(Float,CASE 

         WHEN b.[cd_deadweight] > (b.[cd_volume] * 250 ) THEN  b.[cd_deadweight]

         ELSE b.[cd_volume] * 250

         END) AS ChargeableWeight
      -- ,a.ChargeableWeight

       ,a.BilledFreightCharge

       ,a.BilledFuelSurcharge

       ,a.BilledInsurance

       ,a.BilledTotal

       ,b.cd_pickup_addr0 as Sender

       

FROM #Temp1 AS a

 

LEFT JOIN  cppledi.[dbo].[Consignment] AS b

       ON a.ConsignmentReference = b.[cd_connote]

order by AccountingDate desc


 

   

END






GO
GRANT EXECUTE
	ON [dbo].[Sp_RptProntoBillingEDI]
	TO [ReportUser]
GO
