SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO













-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Pronto Billing,,>

--113035356 WI13035356



--EXEC [Sp_RptProntoBilling_SS_03072017] '113035356|113003651|||','2017-01-01','2017-07-07'

-- =============================================

CREATE PROCEDURE [dbo].[Sp_RptProntoBillingEDI_SSIS_Manual] 
--(@AccountCode varchar(2000), @StartDate Date, @EndDate Date) AS 
as
BEGIN

	

	SET NOCOUNT ON;

--SET @AccountCode = '112877139|113075030|113102321|112726070|112951223|113102511|112904388|113029631|112655618|113102339'

--drop table #Temp3

Declare @StartDate Datetime
  Declare @Enddate Datetime
  --set @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0)
  set @StartDate ='2019-03-01'
  set  @Enddate ='2019-05-02'
  --select @StartDate
--set @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0)
--    set @Enddate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),6)
  Declare @AccountCode Varchar(500)
  --Set @AccountCode = '112877139|113075030|113102321|113102339|112726070|112951223|113102511|112904388|113029631|113101596|113101604'

--  SELECT Item into #Temp
--FROM CpplEdi..[Split](@AccountCode)

SELECT Accountcode into #temp3 FROM  Pronto..ProntoDebtor where Accountcode in ('112877139','113075030','113102321','113102339','112726070','112951223','113102511','112904388','113029631','113101596','113101604','112655618')


SELECT 

       DATEPART (MM,a.AccountingDate) AS BillingMonth

       ,DATEPART (YY,a.AccountingDate) AS BillingYear

       ,a.BillingDate

       ,a.AccountingDate

       ,a.AccountCode

       ,a.AccountName

       ,a.AccountBillToCode

       ,a.AccountBillToName

       ,a.ConsignmentReference

       ,a.ConsignmentDate

       ,a.ServiceCode

       ,a.ItemQuantity

       ,a.DeclaredWeight

       ,a.DeclaredVolume

       ,a.ChargeableWeight

       ,a.TariffId

       ,a.OriginLocality

       ,a.OriginPostcode

       ,a.RevenueOriginZone

       ,a.DestinationLocality

       ,a.DestinationPostcode

       ,a.RevenueDestinationZone

       ,a.BilledFreightCharge

       ,a.BilledFuelSurcharge

       ,a.BilledInsurance

       ,a.BilledTotal

  

INTO #Temp1

 

FROM CPsqLDWH01.pRONTO.[dbo].[ProntoBilling] AS A

  INNER JOIN #temp3 as B

  ON(A.Accountcode = B.Accountcode)

where 

--and AccountingDate > DATEADD (MM, -6 ,GETDATE())

accountingdate BETWEEN convert(date,@StartDate) AND convert(date,@EndDate)

 --and a.AccountBillToCode in (SELECT item FROM #temp)

--order by AccountingDate desc





SELECT

       a.BillingMonth

       ,a.BillingYear

       ,a.BillingDate

       ,a.AccountingDate

       ,a.AccountCode

       ,a.AccountName

       ,a.AccountBillToCode

       ,a.AccountBillToName

       ,a.ConsignmentReference

       ,a.ConsignmentDate

       ,a.ServiceCode

       ,a.TariffId

       ,a.OriginPostcode

       ,a.OriginLocality

       ,a.RevenueOriginZone

       ,a.DestinationPostcode

       ,a.DestinationLocality

       ,a.RevenueDestinationZone

		,a.DeclaredVolume

		,a.DeclaredWeight

		,a.ItemQuantity

	   ,Convert(Float,CASE 

         WHEN b.[cd_deadweight] > (b.[cd_volume] * 250 ) THEN  b.[cd_deadweight]

         ELSE b.[cd_volume] * 250

         END) AS ChargeableWeight
      -- ,a.ChargeableWeight

       ,a.BilledFreightCharge

       ,a.BilledFuelSurcharge

       ,a.BilledInsurance

       ,a.BilledTotal

       ,b.cd_pickup_addr0 as Sender

       

FROM #Temp1 AS a

 

LEFT JOIN  cppledi.[dbo].[Consignment] AS b

       ON a.ConsignmentReference = b.[cd_connote]

order by AccountingDate desc


 

   

END










GO
