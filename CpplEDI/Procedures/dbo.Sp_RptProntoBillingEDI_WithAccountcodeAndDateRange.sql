SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		Praveen Valappil

-- Create date: 2020-03-30

-- Description:	Generating pronto billing records for an account code for the given date range

-- =============================================
-- Sp_RptProntoBillingEDI_WithAccountcodeAndDateRange
CREATE PROCEDURE [dbo].[Sp_RptProntoBillingEDI_WithAccountcodeAndDateRange] 
AS 
BEGIN	

	SET NOCOUNT ON;

	Declare @AccountCode varchar(2000) = '112949003'
	, @StartDate Date ='2020-04-13'
	, @EndDate Date ='2020-05-22'
	
	SELECT
       DATEPART (MM,a.AccountingDate) AS BillingMonth
       ,DATEPART (YY,a.AccountingDate) AS BillingYear
       ,a.BillingDate
       ,a.AccountingDate
       ,a.AccountCode
       ,a.AccountName
       ,a.AccountBillToCode
       ,a.AccountBillToName
       ,a.ConsignmentReference
       ,a.ConsignmentDate
       ,a.ServiceCode
       ,a.ItemQuantity
       ,a.DeclaredWeight
       ,a.DeclaredVolume
       ,a.ChargeableWeight
       ,a.TariffId
       ,a.OriginLocality
       ,a.OriginPostcode
       ,a.RevenueOriginZone
       ,a.DestinationLocality
       ,a.DestinationPostcode
       ,a.RevenueDestinationZone
       ,a.BilledFreightCharge
       ,a.BilledFuelSurcharge
       ,a.BilledInsurance
       ,a.BilledTotal
	INTO #Temp1
	FROM CPsqLDWH01.pRONTO.[dbo].[ProntoBilling] AS A
	Where AccountingDate Between convert(date,@StartDate) And convert(date,@EndDate)
		And a.AccountBillToCode = @AccountCode

	SELECT
      	a.BillingMonth
      	,a.BillingYear
		,a.BillingDate
      	,a.AccountingDate
      	,a.AccountCode
      	,a.AccountName
      	,a.AccountBillToCode
      	,a.AccountBillToName
      	,a.ConsignmentReference
		,a.ConsignmentDate
      	,a.ServiceCode
      	,a.TariffId
		,a.OriginPostcode
      	,a.OriginLocality
      	,a.RevenueOriginZone
		,a.DestinationPostcode
      	,a.DestinationLocality
      	,a.RevenueDestinationZone
		,a.DeclaredVolume
		,a.DeclaredWeight
		,a.ItemQuantity
		,Convert(Float,
			CASE 
         		WHEN b.[cd_deadweight] > (b.[cd_volume] * 250 ) THEN  b.[cd_deadweight]
        		ELSE b.[cd_volume] * 250
        	END) AS ChargeableWeight
       ,a.BilledFreightCharge
       ,a.BilledFuelSurcharge
       ,a.BilledInsurance
       ,a.BilledTotal
       ,b.cd_pickup_addr0 as Sender
	FROM #Temp1 AS a
	LEFT JOIN  cppledi.[dbo].[Consignment] AS b
       ON a.ConsignmentReference = b.[cd_connote]
	ORDER BY AccountingDate desc
END
GO
