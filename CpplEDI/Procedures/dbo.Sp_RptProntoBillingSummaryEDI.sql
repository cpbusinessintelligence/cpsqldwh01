SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO










-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Pronto Billing,,>

--113035356 WI13035356



--EXEC [Sp_RptProntoBilling_SS_03072017] '113035356|113003651|||','2017-01-01','2017-07-07'

-- =============================================

CREATE PROCEDURE [dbo].[Sp_RptProntoBillingSummaryEDI] (@AccountCode varchar(2000), @StartDate Date, @EndDate Date,@ServiceCode Varchar(max)) AS 

BEGIN

	

	SET NOCOUNT ON;


SELECT * INTO #temp 

FROM[dbo].[Split](@AccountCode)



SELECT d.Accountcode into #temp3 FROM #temp t jOIN pronto.dbo.ProntoDebtor d ON t.Item= d.Accountcode



SELECT 

       DATEPART (MM,a.AccountingDate) AS BillingMonth

       ,DATEPART (YY,a.AccountingDate) AS BillingYear

       ,a.BillingDate

       ,a.AccountingDate
        ,a.consignmentreference
       ,a.AccountCode

       ,a.AccountName

       ,a.AccountBillToCode

       ,a.AccountBillToName

       ,a.ServiceCode

       ,a.ItemQuantity

       ,a.DeclaredWeight

       ,a.DeclaredVolume

       ,a.ChargeableWeight

       ,a.TariffId

       ,a.RevenueOriginZone

       ,a.BilledFreightCharge

       ,a.BilledFuelSurcharge

       ,a.BilledInsurance

       ,a.BilledTotal

  

INTO #Temp1

 

FROM Pronto.[dbo].[ProntoBilling] AS A

  INNER JOIN #temp3 as B

  ON(A.Accountcode = B.Accountcode)

where 

--and AccountingDate > DATEADD (MM, -6 ,GETDATE())

 BillingDate >= @StartDate AND BillingDate <=@EndDate
 and  a.ServiceCode IN (select * from dbo.fn_Split(@ServiceCode,null))  

 --and a.AccountBillToCode in (SELECT item FROM #temp)

--order by AccountingDate desc


--select * from #Temp1 where  AccountCode = '111222428'


Select
DATEPART (MM,a.AccountingDate) AS BillingMonth,
DATEPART (YY,a.AccountingDate) AS BillingYear,
a.BillingDate,
a.AccountingDate ,
a.AccountCode,
a.AccountName,
a.AccountBillToCode,
 a.AccountBillToName,
a.ServiceCode,
a.TariffId,
a.RevenueOriginZone,
 SUM(a.DeclaredVolume) DeclaredVolume,
  SUM(a.DeclaredWeight) DeclaredWeight
  , SUM(a.ItemQuantity) ItemQuantity
, SUM(Convert(Float,CASE 

         WHEN b.[cd_deadweight] > (b.[cd_volume] * 250 ) THEN  b.[cd_deadweight]

         ELSE b.[cd_volume] * 250

         END) ) as ChargeableWeight
, SUM(a.BilledFreightCharge) BilledFreightCharge
       , SUM(a.BilledFuelSurcharge) BilledFuelSurcharge
       , SUM(a.BilledInsurance) BilledInsurance
       , SUM(a.BilledTotal) BilledTotal
from #Temp1 a
LEFT JOIN  cppledi.[dbo].[Consignment] AS b

       ON a.ConsignmentReference = b.[cd_connote]
Group by
DATEPART (MM,a.AccountingDate),
DATEPART (YY,a.AccountingDate),
a.BillingDate,
a.AccountingDate,
a.AccountCode,
a.AccountName,
a.AccountBillToCode,
a.ServiceCode,
a.RevenueOriginZone,
a.TariffId,
 a.AccountBillToName


 End




GO
GRANT EXECUTE
	ON [dbo].[Sp_RptProntoBillingSummaryEDI]
	TO [ReportUser]
GO
