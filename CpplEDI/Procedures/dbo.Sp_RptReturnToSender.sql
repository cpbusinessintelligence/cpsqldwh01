SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptReturnToSender]  @AccountCode varchar(20), @StartDate date,@EndDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Select   cd_Connote ,
         cd_id,
         cc_coupon,
         cd_Account,
              cd_items, 
               cd_date ,             
              cd_pickup_suburb ,
              cd_pickup_postcode,          
               cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
              convert(varchar(20),'')  as HasRTS,
              convert(Datetime,Null)  as RTSCageScannedTime

into #Temp1
from cpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
Where  cd_account = @AccountCode --'112808712' 
and cd_date >= @StartDate and cd_date <= @EndDate 


Select cd_connote,cc_coupon,EventDateTime into #temp2 from #Temp1 T join ScannerGateway.dbo.label L on T.cc_coupon= L.Labelnumber
                       join ScannerGateway.dbo.TrackingEvent E on L.ID = E.LabelId
                                     where E.Additionaltext1='6513'

Update #Temp1 SET HasRTs= 'Yes' ,RTSCageScannedTime = EventDateTime   From #Temp1 T Join #Temp2 T2 on T.cc_coupon =T2.cc_coupon

Select * from #temp1

END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptReturnToSender]
	TO [ReportUser]
GO
