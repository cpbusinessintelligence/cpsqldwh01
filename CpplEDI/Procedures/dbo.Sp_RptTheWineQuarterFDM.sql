SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <30th Jan,2017>
-- Description:	<The Wine Quarter Manifeast File>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptTheWineQuarterFDM] (@Account varchar(100))
--(@From Date, @To Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    
   SELECT distinct
       cd_connote as [Connote No]
	  ,isnull(l.sourcereference,'') LabelNumber
	,c.cd_account
      ,cast(cd_date as Date) as Date	
	  ,'DEL' as Type
	  ,cd_delivery_addr0 as [Receiver Address 0]
	  ,cd_delivery_addr1 as [Receiver Address 1]
	  ,cd_delivery_addr2 as [Receiver Address 2]	
	  , cd_delivery_suburb as [Receiver Suburb]
	  , cd_delivery_postcode as [Receiver Postcode]
	  , cd_pickup_count as [No of items]
	  ,cd.cc_coupon as [Item Number]
	  ,r.cr_reference as [Reference]
	  ,cd_import_deadweight as Weight
	  , cd_volume as Cube
	  , cd_pickup_contact as [Sender Name]
	  , cd_pickup_addr0 as [Sender Address 0]	
	  , cd_pickup_addr1 as [Sender Address 1]	 
	  , cd_pickup_addr2 as [Sender Address 2]	
	    , cd_pickup_suburb as [Sender Suburb]
	  , cd_pickup_postcode as [Sender Postcode]
	  ,'' as [Sender State]
	  ,'' as Service
	  ,cd_company_id as ID
	  , com.c_code as [Sender Code]
	  ,cd_special_instructions as [Special Instructions]
	  ,'' as [Contact Name]
	  ,cd_delivery_contact_phone as [Contact Phone]
	  ,cd_delivery_email as [Contact email]
	  ,'' [ATL Field]
	  ,cc_coupon coupon
	   FROM [CpplEDI].[dbo].[consignment] c left join  [CpplEDI].dbo.cdcoupon cd on c.cd_id= cd.cc_consignment
	    join [ScannerGateway].dbo.Trackingevent l on l.sourcereference = cd.cc_coupon	
	   left join [CpplEDI].[dbo].[cdref] r on r.cr_consignment=cd_id 
	   left join [CpplEDI].dbo.companies com on com.c_id= cd_company_id
	   --join ScannerGateway.dbo.trackingevent 
	   where c.cd_date between dateadd(day,-30,getdate()) and dateadd(day,0,getdate())
	   and c.cd_account = @Account and convert(date,eventdatetime)=convert(date,getdate())
	   and EventTypeId='1A35AB45-B82A-492A-A1E8-6415BF846C75'
	   --'112988894'
	   --and l.sourcereference is not null
	  
END

GO
GRANT EXECUTE
	ON [dbo].[Sp_RptTheWineQuarterFDM]
	TO [ReportUser]
GO
