SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author: Tejes S

-- Create date: 2018-03-12

-- Description:	As per the Request ID :6808 (SEKO - returns cage scan report)

-- =============================================



CREATE PROCEDURE [dbo].[Z_sp_RptReturnsCageScanReport_Seko] @Date Date

AS

BEGIN

	SET FMTONLY OFF 
	SET NOCOUNT ON;

	--set @AccountCode='112976535'
--Declare @Date DateTime
--set @Date= '2018-03-09'


DECLARE @StartDate DateTime
DECLARE @EndDate DateTime

SET @StartDate=dateadd(dd, datediff(dd,0,@Date),0)- 1
SEt @EndDate=(dateadd(dd, datediff(dd,0,@Date),0)- 1)+ '23:59:59' 


Select C.cd_account,C.cd_connote , P.cc_coupon,c.cd_id,c.cd_date,[cd_pickup_addr0]
      ,[cd_pickup_addr1]
      ,[cd_pickup_suburb]
      ,[cd_pickup_postcode]
into #Temp1
from cpplEDI.dbo.consignment C Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
where cd_account in ('113058192','113066385',
'112952007',
'112951975',
'112951991',
'112951983',
'112887229',
'113015440',
'112923644',
'112935846',
'112935853',
'112935861',
'112935887',
'113015424',
'112906136'
)and cd_date < = getdate()  and cd_date > = Dateadd(day, -30, getdate())



Select T.cd_account as AccountNumber
     ,T.cd_connote as ConsignmentNumber
	 ,t.cc_coupon as LabelNumber
	 ,t.cd_id as consignmnentId
	 ,L.Id as LabelId
	 ,T.cd_date as ConsignmentDate
	 ,[cd_pickup_addr0] as PickupAddress
     ,[cd_pickup_suburb] as PickupSuburb
     ,[cd_pickup_postcode] as PickupPostCode
into #Temp2  
from #Temp1 T left Join ScannerGateway.dbo.Label L on T.cc_coupon = L.LabelNumber


Select AccountNumber
     ,ConsignmentNumber
	 ,LabelNumber
	 ,consignmnentId
	 ,T.LabelId
	 ,ConsignmentDate
	 ,PickupAddress
     ,PickupSuburb
     ,PickupPostCode into #Temp3 from #Temp2 T Join   ScannerGateway.[dbo].[TrackingEvent] P on T.LabelId = P.LabelID
where P.EventTypeId='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and P.[AdditionalText2] like 'DLB 20325%' 


--Select AccountNumber
--      ,ConsignmentNumber 
--	  ,T.cd_id
--	  ,LabelNumber 
--	  ,P.EventDateTime as PickupDateTime
--	  ,ConsignmentDate
--	  ,PickupAddress
--      ,PickupSuburb
--      ,PickupPostCode
--into #temp3
--from #Temp2 T Join ScannerGateway.[dbo].[TrackingEvent] P on T.ID = P.LabelID


--select  T.*,P.Description as Status into #Temp4 from #temp3 T join  ScannerGateway.[dbo].[EventType] P on T.EventTypeId = P.ID


--select AccountNumber
--      ,Consignment
--	  ,LabelNumber
--	  ,ConsignmentDate
--	  --,[Status]
--	  --,StatusDateTime 
--from #Temp4 t 
--where  ConsignmentDate >= @StartDate and ConsignmentDate<=@EndDate    
--ORDER BY Consignment,LabelNumber, ConsignmentDate,[Status]


Select distinct AccountNumber
     ,ConsignmentNumber
	 ,LabelNumber
	 ,convert(date,ConsignmentDate) as [ConsignmentDate]
	 ,PickupAddress
     ,PickupSuburb
     ,PickupPostCode
from  #Temp3
where  ConsignmentDate >= @StartDate and ConsignmentDate<=@EndDate    
ORDER BY ConsignmentNumber,LabelNumber, ConsignmentDate

END

GO
