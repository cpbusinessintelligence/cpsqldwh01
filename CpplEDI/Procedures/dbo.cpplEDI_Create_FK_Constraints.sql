SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cpplEDI_Create_FK_Constraints] AS
BEGIN


      --'=====================================================================
    --' CP -Stored Procedure -[cpplEDI_Create_FK_Constraints]
    --' ---------------------------
    --' Purpose: Creates FK Constraints-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


	DECLARE @PK_Table varchar(100)
			,@FK_Table varchar(100)
			,@PK_Id varchar(100)
			,@FK_Id varchar(100)
			,@RelationshipName varchar(100)
			,@SqlStatement varchar(1000)

	DECLARE Relationships_Cursor CURSOR FOR
	SELECT [PK_Table]
		  ,[FK_Table]
		  ,[PK_Id]
		  ,[FK_Id]
		  ,[RelationshipName]
	  FROM [cpplEDI].[dbo].[PDG_RelationshipDefinitions] where 
	   [PK_Table] IS NOT NULL
		  AND [FK_Table] IS NOT NULL
		  AND [PK_Id] IS NOT NULL
		  AND [FK_Id] IS NOT NULL
		  AND [RelationshipName] IS NOT NULL;
	OPEN Relationships_Cursor;

	FETCH NEXT FROM Relationships_Cursor
	INTO @PK_Table, @FK_Table, @PK_Id, @FK_Id, @RelationshipName;

	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			SET @SqlStatement = '
			IF OBJECT_ID(''' + @RelationshipName + ''', ''F'')IS NULL
			ALTER TABLE ' + @FK_Table + ' WITH NOCHECK ADD CONSTRAINT 
			' + @RelationshipName + ' FOREIGN KEY (' + @FK_Id + ') REFERENCES ' + @PK_Table + '
			(' + @PK_Id + ') ON UPDATE NO ACTION ON DELETE NO ACTION NOT FOR REPLICATION;

			ALTER TABLE ' + @FK_Table + ' NOCHECK CONSTRAINT ' + @RelationshipName + ';
			'
			BEGIN TRANSACTION;
				BEGIN TRY
					EXEC(@SqlStatement);
					COMMIT TRANSACTION;
					PRINT 'Constraint ' + @RelationshipName + ' created.';
				END TRY
				BEGIN CATCH
					SELECT ERROR_MESSAGE() AS ErrorMessage, @SqlStatement AS SqlStatement;
					PRINT '** FAILD to create Constraint ' + @RelationshipName;
					IF @@TRANCOUNT > 0
						ROLLBACK TRANSACTION;
				END CATCH
			FETCH NEXT FROM Relationships_Cursor
				INTO @PK_Table, @FK_Table, @PK_Id, @FK_Id, @RelationshipName;
		END;
	CLOSE Relationships_Cursor;
	DEALLOCATE Relationships_Cursor;

	-- 31/12/2009 - Craig Parris
	--
	-- added this index drop/creation proc so that it's included in the nightly database refresh
	EXEC dbo.cpplEDI_Create_cpplEdi_Indexes;

	-- reset the flag to allow the ODS import to run now
	IF NOT EXISTS (SELECT 1 FROM GatewayImportFlag)
	BEGIN
		INSERT INTO GatewayImportFlag 
		(GatewayImportCanRun)
		VALUES
		(1);
	END
	ELSE
	BEGIN
		UPDATE GatewayImportFlag 
		SET GatewayImportCanRun = 1;
	END

END

GO
