SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[cpplEDI_Create_cpplEdi_Indexes]
AS
BEGIN

      --'=====================================================================
    --' CP -Stored Procedure -[cpplEDI_Create_cpplEdi_Indexes]
    --' ---------------------------
    --' Purpose: Creates cpplEDI Indexes-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


	BEGIN TRAN;

	/****** Object:  Index [IX_cdcoupon_coupon]    Script Date: 03/19/2010 15:52:42 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cdcoupon]') AND name = N'IX_cdcoupon_coupon')
	DROP INDEX [IX_cdcoupon_coupon] ON [dbo].[cdcoupon] WITH ( ONLINE = OFF );

	
	/****** Object:  Index [IX_cdcoupon_consignment]    Script Date: 03/19/2010 15:52:42 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cdcoupon]') AND name = N'IX_cdcoupon_consignment')
	DROP INDEX [IX_cdcoupon_consignment] ON [dbo].[cdcoupon] WITH ( ONLINE = OFF );

	/****** Object:  Index [idx_consignment_connote]    Script Date: 08/22/2012 08:11:15 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[consignment]') AND name = N'idx_consignment_connote')
	DROP INDEX [idx_consignment_connote] ON [dbo].[consignment] WITH ( ONLINE = OFF )

	/****** Object:  Index [idx_Branchs_EmmcodeId]    Script Date: 03/19/2010 15:54:48 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[branchs]') AND name = N'idx_Branchs_EmmcodeId')
	DROP INDEX [idx_Branchs_EmmcodeId] ON [dbo].[branchs] WITH ( ONLINE = OFF );

	/****** Object:  Index [IX_consignment_manifest_id]    Script Date: 03/19/2010 15:55:48 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[consignment]') AND name = N'IX_consignment_manifest_id')
	DROP INDEX [IX_consignment_manifest_id] ON [dbo].[consignment] WITH ( ONLINE = OFF );

	/****** Object:  Index [idx_Driver_NumberBranch]    Script Date: 03/19/2010 15:56:42 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[driver]') AND name = N'idx_Driver_NumberBranch')
	DROP INDEX [idx_Driver_NumberBranch] ON [dbo].[driver] WITH ( ONLINE = OFF );

	/****** Object:  Index [idx_cdaudit_consignmentstamp]    Script Date: 07/23/2010 16:25:33 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cdaudit]') AND name = N'idx_cdaudit_consignmentstamp')
	DROP INDEX [idx_cdaudit_consignmentstamp] ON [dbo].[cdaudit] WITH ( ONLINE = OFF );

	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cdref]') AND name = N'idx_cdref_consignment')
	DROP INDEX [idx_cdref_consignment] ON [dbo].[cdref] WITH ( ONLINE = OFF );

	/****** Object:  Index [idx_consignment_idmanifestimportdate]    Script Date: 07/23/2010 20:01:00 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[consignment]') AND name = N'idx_consignment_idmanifestimportdate')
	DROP INDEX [idx_consignment_idmanifestimportdate] ON [dbo].[consignment] WITH ( ONLINE = OFF );

	/****** Object:  Index [IX_cdcoupon_consignment]    Script Date: 07/23/2010 20:05:02 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cdcoupon]') AND name = N'IX_cdcoupon_consignment')
	DROP INDEX [IX_cdcoupon_consignment] ON [dbo].[cdcoupon] WITH ( ONLINE = OFF );

	/****** Object:  Index [idx_manifest_idreleasedstampdate]    Script Date: 07/23/2010 20:07:37 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[manifest]') AND name = N'idx_manifest_idreleasedstampdate')
	DROP INDEX [idx_manifest_idreleasedstampdate] ON [dbo].[manifest] WITH ( ONLINE = OFF );

	/****** Object:  Index [idx_customereta_FromTo]    Script Date: 03/03/2011 09:33:54 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customereta]') AND name = N'idx_customereta_FromTo')
	DROP INDEX [idx_customereta_FromTo] ON [dbo].[customereta] WITH ( ONLINE = OFF )

	/****** Object:  Index [idx_agentinfo_etazone]    Script Date: 03/03/2011 10:06:02 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agentinfo]') AND name = N'idx_agentinfo_etazone')
	DROP INDEX [idx_agentinfo_etazone] ON [dbo].[agentinfo] WITH ( ONLINE = OFF )

	/****** Object:  Index [idx_consignment_idmanifestimportdate]    Script Date: 07/23/2010 20:01:00 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[consignment]') AND name = N'IX_consignment_customer_eta')
	DROP INDEX [IX_consignment_customer_eta] ON [dbo].[consignment] WITH ( ONLINE = OFF );

	/****** Object:  Index [idx_companyclass_shortname]    Script Date: 03/28/2011 17:22:09 ******/
	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[companyclass]') AND name = N'idx_companyclass_shortname')
	DROP INDEX [idx_companyclass_shortname] ON [dbo].[companyclass] WITH ( ONLINE = OFF )

	IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[companies]') AND name = N'idx_companies_cclass')
	DROP INDEX [idx_companies_cclass] ON [dbo].[companies] WITH ( ONLINE = OFF )

	COMMIT TRAN;


	/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
	BEGIN TRANSACTION
	SET QUOTED_IDENTIFIER ON
	SET ARITHABORT ON
	SET NUMERIC_ROUNDABORT OFF
	SET CONCAT_NULL_YIELDS_NULL ON
	SET ANSI_NULLS ON
	SET ANSI_PADDING ON
	SET ANSI_WARNINGS ON
	COMMIT
	/* BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX IX_cdcoupon_consignment ON dbo.cdcoupon
		(
		cc_consignment
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	ALTER TABLE dbo.cdcoupon SET (LOCK_ESCALATION = TABLE)
	COMMIT */

	BEGIN TRANSACTION
/*	CREATE NONCLUSTERED INDEX IX_consignment_manifest_id ON dbo.consignment
		(
		cd_manifest_id
		) 
		INCLUDE (cd_id, cd_connote)
		WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
*/
	CREATE NONCLUSTERED INDEX [IX_consignment_manifest_id] ON [dbo].[consignment] 
	(
		[cd_manifest_id] ASC,
		[cd_company_id] ASC
	)
	INCLUDE ( [cd_id],
	[cd_connote]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	ALTER TABLE dbo.consignment SET (LOCK_ESCALATION = TABLE)
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [idx_Branchs_EmmcodeId] ON [dbo].[branchs] 
	(
		[b_id] ASC,
		[b_emmcode] ASC
	)
	INCLUDE ( [b_name],
	[b_shortname]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [idx_Driver_NumberBranch] ON [dbo].[driver] 
	(
		[dr_branch] ASC,
		[dr_number] ASC
	)
	INCLUDE ( [dr_id],
	[dr_name]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [idx_cdaudit_consignmentstamp] ON [dbo].[cdaudit] 
	(
		[ca_consignment] ASC,
		[ca_stamp] ASC
	)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [idx_cdref_consignment] ON [dbo].[cdref] 
	(
		[cr_consignment] ASC
	)
	INCLUDE ( [cr_reference]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT
	
	
	
	BEGIN TRANSACTION
	
	CREATE NONCLUSTERED INDEX IX_consignment_customer_eta ON dbo.consignment
	(
	cd_customer_eta DESC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	COMMIT

	BEGIN TRANSACTION
	
/*	CREATE NONCLUSTERED INDEX [idx_consignment_idmanifestimportdate] ON [dbo].[consignment] 
	(
		[cd_id] ASC,
		[cd_import_id] ASC,
		[cd_manifest_id] ASC,
		[cd_date] ASC,
		[cd_consignment_date] ASC
	)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
*/
/****** Object:  Index [idx_consignment_idmanifestimportdate]    Script Date: 03/28/2011 17:20:27 ******/
	CREATE NONCLUSTERED INDEX [idx_consignment_idmanifestimportdate] ON [dbo].[consignment] 
	(
		[cd_id] ASC,
		[cd_import_id] ASC,
		[cd_manifest_id] ASC,
		[cd_date] ASC,
		[cd_consignment_date] ASC,
		[cd_company_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

	COMMIT 

	BEGIN TRANSACTION
	/****** Object:  Index [IX_cdcoupon_consignment]    Script Date: 07/23/2010 20:05:03 ******/
	CREATE NONCLUSTERED INDEX [IX_cdcoupon_consignment] ON [dbo].[cdcoupon] 
	(
		[cc_consignment] ASC
	)
	INCLUDE ( [cc_id],
	[cc_company_id],
	[cc_coupon])
	WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [idx_manifest_idreleasedstampdate] ON [dbo].[manifest] 
	(
		[m_id] ASC,
		[m_date] ASC,
		[m_stamp] ASC,
		[m_released] ASC
	)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [IX_cdcoupon_coupon] ON [dbo].[cdcoupon] 
	(
		[cc_coupon] ASC
	)
	INCLUDE ( [cc_id],
	[cc_company_id],
	[cc_consignment])
	WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [idx_customereta_FromTo] ON [dbo].[customereta] 
	(
		[ce_from] ASC,
		[ce_to] ASC
	)
	INCLUDE ( [ce_days])
	WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	CREATE NONCLUSTERED INDEX [idx_agentinfo_etazone] ON [dbo].[agentinfo] 
	(
		[postcode] ASC,
		[suburb] ASC,
		[ai_eta_zone] ASC
	)
	WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	/****** Object:  Index [idx_companyclass_shortname]    Script Date: 03/28/2011 17:22:10 ******/
	CREATE NONCLUSTERED INDEX [idx_companyclass_shortname] ON [dbo].[companyclass] 
	(
		[cc_id] ASC,
		[cc_shortname] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT

	BEGIN TRANSACTION
	/****** Object:  Index [idx_companies_cclass]    Script Date: 03/28/2011 17:23:07 ******/
	CREATE NONCLUSTERED INDEX [idx_companies_cclass] ON [dbo].[companies] 
	(
		[c_id] ASC,
		[c_class] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT
	
	BEGIN TRANSACTION
	/****** Object:  Index [idx_consignment_connote]    Script Date: 08/22/2012 08:11:15 ******/
	CREATE NONCLUSTERED INDEX [idx_consignment_connote] ON [dbo].[consignment] 
	(
		[cd_connote] ASC
	)
	INCLUDE ( [cd_deliver_stamp],
	[cd_delivery_agent],
	[cd_agent_pod],
	[cd_agent_pod_stamp]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	COMMIT
	
END

GO
