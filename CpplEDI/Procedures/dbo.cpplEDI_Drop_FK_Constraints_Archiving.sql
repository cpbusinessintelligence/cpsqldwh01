SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[cpplEDI_Drop_FK_Constraints_Archiving] AS

-- as this proc runs before the tables are reloaded, we'll set the flag
-- so that the import into ODS cannot run until the table loads are finished

--IF NOT EXISTS (SELECT 1 FROM GatewayImportFlag)
--BEGIN
--	INSERT INTO GatewayImportFlag 
--	(GatewayImportCanRun)
--	VALUES
--	(0);
--END
--ELSE
--BEGIN
--	UPDATE GatewayImportFlag 
--	SET GatewayImportCanRun = 0;
--END

DECLARE @PK_Table varchar(100)
            ,@FK_Table varchar(100)
            ,@PK_Id varchar(100)
            ,@FK_Id varchar(100)
            ,@RelationshipName varchar(100)
            ,@SqlStatement varchar(1000)

DECLARE Relationships_Cursor CURSOR FOR
SELECT [PK_Table]
      ,[FK_Table]
      ,[PK_Id]
      ,[FK_Id]
      ,[RelationshipName]
  FROM [cpplEDI].[dbo].[PDG_RelationshipDefinitions]
  WHERE [PK_Table] IS NOT NULL
      AND [FK_Table] IS NOT NULL
      AND [PK_Id] IS NOT NULL
      AND [FK_Id] IS NOT NULL
      AND [RelationshipName] IS NOT NULL
	   and PK_Table in ('dbo.consignment','dbo.cdcoupon')

OPEN Relationships_Cursor;

FETCH NEXT FROM Relationships_Cursor
INTO @PK_Table, @FK_Table, @PK_Id, @FK_Id, @RelationshipName;

WHILE @@FETCH_STATUS = 0
      BEGIN
            SET @SqlStatement = '
            IF OBJECT_ID(''' + @RelationshipName + ''', ''F'')IS NOT NULL
            ALTER TABLE ' + @FK_Table + ' DROP CONSTRAINT ' + @RelationshipName + ';'
            BEGIN TRANSACTION;
                  BEGIN TRY
                        EXEC(@SqlStatement);
                        COMMIT TRANSACTION;
                        PRINT 'Constraint ' + @RelationshipName + ' dropped.';
                  END TRY
                  BEGIN CATCH
                        SELECT ERROR_MESSAGE() AS ErrorMessage, @SqlStatement AS SqlStatement;
                        PRINT '** FAILD to drop Constraint ' + @RelationshipName;
                        PRINT @SqlStatement;
                        IF @@TRANCOUNT > 0
                              ROLLBACK TRANSACTION;
                  END CATCH
            FETCH NEXT FROM Relationships_Cursor
                  INTO @PK_Table, @FK_Table, @PK_Id, @FK_Id, @RelationshipName;
      END;
CLOSE Relationships_Cursor;
DEALLOCATE Relationships_Cursor;

GO
