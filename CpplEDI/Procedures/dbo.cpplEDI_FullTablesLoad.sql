SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE proc [dbo].[cpplEDI_FullTablesLoad] as

begin



     --'=====================================================================

    --' CP -Stored Procedure -[cpplEDI_FullTablesLoad]

    --' ---------------------------

    --' Purpose: Full load of small tables-----

    --' Developer: Abhigna (Couriers Please Pty Ltd)

    --' Date: 03 Sep 2014

    --' Copyright: 2014 Couriers Please Pty Ltd

    --' Change Log: 

    --' Date          Who     Ver     Reason                                            Bookmark

    --' ----          ---     ---     -----                                             -------

    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

	--  05/11/2014    AB      1.00    Altered the procedure  to accommodate create and insert data into tables in the  same stored procedure                          --AB20141105
	--  07/07/2017    SS              Altered the procedure to fix errors



    --'=====================================================================



-----------Drop and recreate the tables--------------------

--exec [dbo].[cpplEDI_Tables_DropandCreation]

--Tables to be manually loaded-Book1,Book3,CustomercodSBMapping,CustomerUniqueCodes,CWC_AgentOverride,[DeliveryVsPODSummary],GatewayImportFlag,[PDG_RelationshipDefinitions],sysdiagrams----

--If(select count(*) from temp_tablesload)=109

--delete from temp_tablesload where SQLCount is null

----------Load the data into tables created--------------



IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='addresses') DROP TABLE [addresses];

CREATE TABLE [addresses] (

  [a_id] int NOT NULL,

  [a_company_id] int DEFAULT '0',

  [a_user] varchar(32) DEFAULT '',

  [a_pickup] char(1) DEFAULT 'N',

  [a_delivery] char(1) DEFAULT 'N',

  [a_code] varchar(40) DEFAULT NULL,

  [a_addr0] varchar(40) DEFAULT '',

  [a_addr1] varchar(40) DEFAULT '',

  [a_addr2] varchar(40) DEFAULT '',

  [a_addr3] varchar(40) DEFAULT '',

  [a_suburb] varchar(30) DEFAULT '',

  [a_postcode] int DEFAULT '0',

  [a_contact] varchar(40) DEFAULT '',

  [a_phone] varchar(20) DEFAULT '',

  [a_email] varchar(255) DEFAULT '',

  PRIMARY KEY ([a_id]),

) ;

INSERT INTO addresses select * from openquery(MySQLMain,'select * from cpplEDI.addresses');INSERT INTO Temp_tablesload values('addresses',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.addresses')),(select count(*) from addresses
));

Print '2'

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agedbalances') DROP TABLE [agedbalances];

CREATE TABLE [agedbalances] (

  [a_company_id] int NOT NULL DEFAULT '0',

  [a_current] FLOAT(25) DEFAULT '0',

  [a_7days] FLOAT(25) DEFAULT '0',

  [a_14days] FLOAT(25) DEFAULT '0',

  [a_21days] FLOAT(25) DEFAULT '0',

  [a_28days] FLOAT(25) DEFAULT '0',

  [a_60days] FLOAT(25) DEFAULT '0',

  [a_90days] FLOAT(25) DEFAULT '0',

  [a_over] FLOAT(25) DEFAULT '0',

  [a_total] FLOAT(25) DEFAULT '0',

  [a_asat] SMALLDATETIME default NULL,

  PRIMARY KEY ([a_company_id])

) ;

INSERT INTO agedbalances select * from openquery(MySQLMain,'select * from cpplEDI.agedbalances');INSERT INTO Temp_tablesload values('agedbalances',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.agedbalances')),(select count(*) from agedbalances));

Print '3'

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentemail') DROP TABLE [agentemail];

CREATE TABLE [agentemail] (

    [ae_agent] int NOT NULL DEFAULT '0',

  [ae_description] varchar(64) NOT NULL DEFAULT '',

  [ae_email] varchar(128) NOT NULL DEFAULT '',

  [ae_class] int NOT NULL DEFAULT '0',

  [ae_style] char(1) DEFAULT 'N',

) ;

INSERT INTO agentemail select * from openquery(MySQLMain,'select * from cpplEDI.agentemail');INSERT INTO Temp_tablesload values('agentemail',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.agentemail')),(select count(*) from agentemail));


Print '4'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentinfo') DROP TABLE [agentinfo];

CREATE TABLE [agentinfo] (

  [zoning_id] int NOT NULL DEFAULT '0',

  [postcode] int DEFAULT NULL,

  [suburb] varchar(64) DEFAULT NULL,

  [state] varchar(10) DEFAULT NULL,

  [agent_id] int NOT NULL DEFAULT '0',

  [alt_agent_id] int NOT NULL DEFAULT '0',

  [zoneid] int DEFAULT '0',

  [subzoneid] int DEFAULT '0',

  [branch] int DEFAULT NULL,

  [depot] int DEFAULT NULL,

  [cppldriver] int DEFAULT '0',

  [ai_id] int NOT NULL,

  [ai_eta_zone] varchar(12) DEFAULT '',

  [ai_sort_code] varchar(12) DEFAULT NULL,

  [a_imported] char(2) DEFAULT 'N',

  [ai_sort_nsw] char(2) DEFAULT '',

  [ai_sort_act] char(2) DEFAULT '',

  [ai_sort_vic] char(2) DEFAULT '',

  [ai_sort_tas] char(2) DEFAULT '',

  [ai_sort_qld] char(2) DEFAULT '',

  [ai_sort_sa] char(2) DEFAULT '',

  [ai_sort_nt] char(2) DEFAULT '',

  [ai_sort_wa] char(2) DEFAULT '',

  [ai_sort_suffix] char(8) DEFAULT '',

  [ai_eta_range_code] char(12) DEFAULT '',

  PRIMARY KEY ([ai_id]),

) ;

INSERT INTO agentinfo select * from openquery(MySQLMain,'select * from cpplEDI.agentinfo');INSERT INTO Temp_tablesload values('agentinfo',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.agentinfo')),(select count(*) from agentinfo
));


Print '5'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentinfo2') DROP TABLE [agentinfo2];

CREATE TABLE [agentinfo2] (

   [ai_id] int NOT NULL,

  [agent_id] int NOT NULL DEFAULT '0',

  [pricing_id] char(4) NOT NULL DEFAULT '',

  [postcode] int DEFAULT NULL,

  [suburb] char(32) DEFAULT NULL,

  [state] char(10) DEFAULT NULL,

  [metro] int DEFAULT NULL,

  [links] int DEFAULT NULL,

  [zone] char(10) DEFAULT NULL,

  [zoneid] int DEFAULT '0',

  [depot] int DEFAULT NULL,

  [branch] int DEFAULT NULL,

  PRIMARY KEY ([ai_id]),

) ;

INSERT INTO agentinfo2 select * from openquery(MySQLMain,'select * from cpplEDI.agentinfo2');INSERT INTO Temp_tablesload values('agentinfo2',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.agentinfo2')),(select count(*) from agentinfo2));


Print '6'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentinfo3') DROP TABLE [agentinfo3];

CREATE TABLE [agentinfo3] (

 [zoning_id] int NOT NULL DEFAULT '0',

  [postcode] int DEFAULT NULL,

  [suburb] varchar(64) DEFAULT NULL,

  [state] varchar(10) DEFAULT NULL,

  [agent_id] int NOT NULL DEFAULT '0',

  [zoneid] int DEFAULT '0',

  [subzoneid] int DEFAULT '0',

  [branch] int DEFAULT NULL,

  [depot] int DEFAULT NULL,

  [cppldriver] int DEFAULT '0',

  [ai_id] int NOT NULL DEFAULT '0',

  [ai_eta_zone] varchar(12) DEFAULT '',

  [ai_sort_code] varchar(12) DEFAULT NULL,

  [a_imported] char(1) DEFAULT 'N',

  [ai_sort_nsw] char(1) DEFAULT '',

  [ai_sort_act] char(1) DEFAULT '',

  [ai_sort_vic] char(1) DEFAULT '',

  [ai_sort_tas] char(1) DEFAULT '',

  [ai_sort_qld] char(1) DEFAULT '',

  [ai_sort_sa] char(1) DEFAULT '',

  [ai_sort_nt] char(1) DEFAULT '',

  [ai_sort_wa] char(1) DEFAULT '',

  [ai_sort_suffix] char(8) DEFAULT ''

) ;

INSERT INTO agentinfo3 select * from openquery(MySQLMain,'select * from cpplEDI.agentinfo3');INSERT INTO Temp_tablesload values('agentinfo3',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.agentinfo3')),(select count(*) from agentinfo3));






Print '7'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='apconnote') DROP TABLE [apconnote];

CREATE TABLE [apconnote] (

   [ac_id] int NOT NULL,

  [ac_connote] char(16) NOT NULL,

  [ac_date] SMALLDATETIME default NULL,

  [ac_consignment] int NOT NULL,

  [ac_sender_name] char(40) DEFAULT '',

  [ac_sender_addr1] char(40) DEFAULT '',

  [ac_sender_addr2] char(40) DEFAULT '',

  [ac_sender_suburb] char(40) DEFAULT '',

  [ac_sender_postcode] char(10) DEFAULT '',

  [ac_receiver_name] char(40) DEFAULT '',

  [ac_receiver_addr1] char(40) DEFAULT '',

  [ac_receiver_addr2] char(40) DEFAULT '',

  [ac_receiver_suburb] char(40) DEFAULT '',

  [ac_receiver_postcode] char(10) DEFAULT '',

  [ac_atl] char(1) DEFAULT 'N',

  [ac_special_instructions] char(120) DEFAULT '',

  [ac_manifest_stamp] datetime default NULL,

  [ac_manifest_id] int DEFAULT '0',

  [ac_sender_location] char(16) DEFAULT '',

  [ac_receiver_location] char(16) DEFAULT '',

  [ac_service] char(16) DEFAULT '',

  [ac_ap_shipment] varchar(255) DEFAULT ''

  PRIMARY KEY ([ac_id]),

) ;

INSERT INTO apconnote select * from openquery(MySQLMain,'select * from cpplEDI.apconnote');INSERT INTO Temp_tablesload values('apconnote',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.apconnote')),(select count(*) from apconnote
));


Print '8'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='apinfo') DROP TABLE [apinfo];

CREATE TABLE [apinfo] (

   [ai_sender_id] char(16) NOT NULL,

  [ai_sender_name] char(40) DEFAULT '',

  [ai_sender_addr1] char(40) DEFAULT '',

  [ai_sender_addr2] char(40) DEFAULT '',

  [ai_sender_suburb] char(40) DEFAULT '',

  [ai_sender_postcode] int DEFAULT '0',

  [ai_sender_record_no] int DEFAULT '0',

  [ai_connote_prefix] char(8) DEFAULT '',

  [ai_connote_id] int DEFAULT '1000000',

  [ai_connote_start] int DEFAULT '1000000',

  [ai_connote_finish] int DEFAULT '9999999',

  [ai_merchant_location_id] char(16) DEFAULT '',

  [ai_manifest_filename_prefix] char(16) DEFAULT '',

  [ai_manifest_id] int DEFAULT '0',

  [ai_merchant_id] char(16) DEFAULT '',

  [ai_charge_code] char(16) DEFAULT '',

  [ai_charge_description] char(40) DEFAULT '',

  [ai_transaction_id] int DEFAULT '0',

  [ai_post_account] char(20) DEFAULT '',

  [ai_lodgement_facility] char(80) DEFAULT '',

  [ai_manifest_username] char(32) DEFAULT '',

  [ai_transfer_host] varchar(256) DEFAULT NULL,

  [ai_transfer_username] varchar(256) DEFAULT NULL,

  [ai_transfer_password] varchar(256) DEFAULT NULL,

  [ai_transfer_path] varchar(500) NULL,

  [ai_special_agent_id] varchar(256) DEFAULT NULL,
  [ai_sender_location] char(1)


  PRIMARY KEY ([ai_sender_id])

) ;



INSERT INTO apinfo select * from openquery(MySQLMain,'select * from cpplEDI.apinfo');INSERT INTO Temp_tablesload values('apinfo',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.apinfo')),(select count(*) from apinfo));








Print '9'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='aplink') DROP TABLE [aplink];

CREATE TABLE [aplink] (

   [al_id] int NOT NULL,

  [al_cpl_connote] char(32) NOT NULL,

  [al_cpl_label] char(32) NOT NULL,

  [al_cpl_id] int DEFAULT '0',

  [al_ap_connote] char(16) NOT NULL,

  [al_ap_label] char(32) NOT NULL,

  [al_ap_no] int NOT NULL,

  [al_ap_id] int DEFAULT '0',

  [al_status] char(1) DEFAULT 'N',

  [al_weight] FLOAT(25) DEFAULT '0',

  [al_printed] char(1) DEFAULT 'N',
  
  [al_ap_full] varchar(255) DEFAULT '',

  [al_ap_item_id] varchar(255) DEFAULT ''

  PRIMARY KEY ([al_id]),

) ;

INSERT INTO aplink select * from openquery(MySQLMain,'select * from cpplEDI.aplink');INSERT INTO Temp_tablesload values('aplink',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.aplink')),(select count(*) from aplink));



IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='apmanifest') DROP TABLE [apmanifest];

CREATE TABLE [apmanifest] (

  [am_id] int NOT NULL,

  [am_sending_id] char(16) DEFAULT '',

  [am_receiving_id] char(16) DEFAULT '',

  [am_submitted] datetime default NULL,

  [am_lodged] datetime default NULL,

  [am_created] datetime default NULL,

  [am_connotes] int DEFAULT '0',

  [am_labels] int DEFAULT '0',

  [am_transmitted] datetime default NULL,

  [am_local_id] int DEFAULT '0',

  [am_received] datetime default NULL,



  [am_approved] datetime default NULL,



  [am_cancelled] datetime default NULL,

  [am_order_id] varchar(255) default ''

  PRIMARY KEY ([am_id])

) ;



INSERT INTO apmanifest select * from openquery(MySQLMain,'select * from cpplEDI.apmanifest');INSERT INTO Temp_tablesload values('apmanifest',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.apmanifest')),(select count(*) from apmanifest));


Print '10'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='authinfo') DROP TABLE [authinfo];

CREATE TABLE [authinfo] (

  [ai_id] int NOT NULL,

  [ai_type] char(1) NOT NULL DEFAULT '',

  [ai_name] varchar(32) NOT NULL DEFAULT '',

  PRIMARY KEY ([ai_id])

) ;



INSERT INTO authinfo select * from openquery(MySQLMain,'select * from cpplEDI.authinfo');INSERT INTO Temp_tablesload values('authinfo',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.authinfo')),(select count(*) from authinfo));


Print '11'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='billing') DROP TABLE [billing];



CREATE TABLE [billing] (

  [b_id] int NOT NULL,

  [b_company] int NOT NULL DEFAULT '0',

  [b_date] SMALLDATETIME default NULL,

  [b_stamp] datetime default NULL,

  [b_consignments] int DEFAULT '0',

  [b_bookins] int DEFAULT '0',

  [b_total] FLOAT(25) DEFAULT '0',

  PRIMARY KEY ([b_id])

) ;



INSERT INTO billing select * from openquery(MySQLMain,'select * from cpplEDI.billing');
INSERT INTO Temp_tablesload values('billing',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.billing')),(select count(*) from billing));


Print '12'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='bookins') DROP TABLE [bookins];

CREATE TABLE [bookins] (

  [bi_id] int NOT NULL,

  [bi_company_id] int NOT NULL DEFAULT '0',

  [bi_import_id] int DEFAULT NULL,

  [bi_warehouse_id] int NOT NULL DEFAULT '0',

  [bi_date] SMALLDATETIME default NULL,

  [bi_connote] varchar(32) NOT NULL DEFAULT '',

  [bi_charge] varchar(32) NOT NULL DEFAULT '',

  [bi_units] int DEFAULT '1',

  [bi_units_entries] int DEFAULT '0',

  [bi_deadweight] FLOAT(25) DEFAULT NULL,

  [bi_volume] FLOAT(25) DEFAULT NULL,

  [bi_reference] varchar(64) DEFAULT '',

  [bi_sender_addr0] varchar(40) DEFAULT '',

  [bi_sender_addr1] varchar(40) DEFAULT '',

  [bi_sender_addr2] varchar(40) DEFAULT '',

  [bi_sender_addr3] varchar(40) DEFAULT '',

  [bi_sender_suburb] varchar(30) DEFAULT '',

  [bi_sender_postcode] int DEFAULT '0',

  [bi_receiver_addr0] varchar(40) DEFAULT '',

  [bi_receiver_addr1] varchar(40) DEFAULT '',

  [bi_receiver_addr2] varchar(40) DEFAULT '',

  [bi_receiver_addr3] varchar(40) DEFAULT '',

  [bi_receiver_suburb] varchar(30) DEFAULT '',

  [bi_receiver_postcode] int DEFAULT '0',

  [bi_pricecode] varchar(16) DEFAULT 'BOOKIN',

  [bi_special] varchar(80) DEFAULT '',

  [bi_not_before] SMALLDATETIME default NULL,

  [bi_not_after] SMALLDATETIME default NULL,

  [bi_revised_date] SMALLDATETIME default NULL,

  [bi_revised_time] time DEFAULT '00:00:00',

  [bi_received] SMALLDATETIME default NULL,

  [bi_released] SMALLDATETIME default NULL,

  [bi_comments] varchar(80) DEFAULT NULL,

  [bi_label] varchar(64) DEFAULT NULL,

  [bi_scan_received] datetime default NULL,

  [bi_scan_released] datetime default NULL,

  [bi_billing_id] int DEFAULT '0',

  [bi_billing_date] SMALLDATETIME default NULL,

  [bi_export_id] int DEFAULT '0',

  [bi_export2_id] int DEFAULT '0',

  [bi_agent_pod_name] varchar(64) DEFAULT NULL,

  [bi_agent_pod_stamp] datetime default NULL,

  [bi_agent_pod_entry] datetime default NULL,

  PRIMARY KEY ([bi_id]),

) ;

INSERT INTO bookins select * from openquery(MySQLMain,'select * from cpplEDI.bookins');INSERT INTO Temp_tablesload values('bookins',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.bookins')),(select count(*) from bookins));


Print '13'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='bookintrack') DROP TABLE [bookintrack];

CREATE TABLE [bookintrack] (

  [bt_id] int NOT NULL,

  [bt_bookin] int NOT NULL DEFAULT '0',

  PRIMARY KEY ([bt_id]),

) ;



INSERT INTO bookintrack select * from openquery(MySQLMain,'select * from cpplEDI.bookintrack');INSERT INTO Temp_tablesload values('bookintrack',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.bookintrack')),(select count(*) from bookintrack));

Print '14'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='branchs') DROP TABLE [branchs];

CREATE TABLE [branchs] (

  [b_id] int NOT NULL,

  [b_name] varchar(32) NOT NULL DEFAULT '',

  [b_trackhost] varchar(128) NOT NULL DEFAULT '',

  [b_trackport] varchar(32) NOT NULL DEFAULT '',

  [b_multitrack] char(1) DEFAULT 'N',

  [b_depot] int DEFAULT NULL,

  [b_shortname] varchar(8) DEFAULT '',

  [b_internal_name] varchar(128) DEFAULT NULL,

  [b_emmcode] varchar(64) DEFAULT NULL,

  [b_in_difot] char(1) DEFAULT 'N',

  [b_description_metro] char(128) DEFAULT '',

  [b_description_country] char(128) DEFAULT '',

  PRIMARY KEY ([b_id])

) ;



INSERT INTO branchs select * from openquery(MySQLMain,'select * from cpplEDI.branchs');INSERT INTO Temp_tablesload values('branchs',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.branchs')),(select count(*) from branchs));


Print '15'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ccreweighhold') DROP TABLE [ccreweighhold];

CREATE TABLE [ccreweighhold] (

 [crh_coupon] char(32) NOT NULL DEFAULT '',

  [crh_coupon_id] int NOT NULL,

  [crh_stamp] datetime default NULL,

  [crh_checked] char(1) DEFAULT 'N',

  [crh_location] char(16) DEFAULT '',

  [crh_deadweight] FLOAT(25) DEFAULT '0',

  [crh_dimension0] FLOAT(25) DEFAULT '0',

  [crh_dimension1] FLOAT(25) DEFAULT '0',

  [crh_dimension2] FLOAT(25) DEFAULT '0',

  [crh_volume] FLOAT(25) DEFAULT '0',

  PRIMARY KEY ([crh_coupon]),

) ;



INSERT INTO ccreweighhold select * from openquery(MySQLMain,'select * from cpplEDI.ccreweighhold');INSERT INTO Temp_tablesload values('ccreweighhold',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ccreweighhold')),(select count(*
) from ccreweighhold));


Print '16'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdaudit') DROP TABLE [cdaudit];

CREATE TABLE [cdaudit] (

  [ca_id] int NOT NULL,

  [ca_consignment] int NOT NULL DEFAULT '0',

  [ca_stamp] datetime default NULL,

  [ca_account] varchar(32) DEFAULT '',

  [ca_info] varchar(255) DEFAULT '',

  PRIMARY KEY ([ca_id]),

) ;



INSERT INTO cdaudit select * from openquery(MySQLMain,'select * from cpplEDI.cdaudit');INSERT INTO Temp_tablesload values('cdaudit',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdaudit')),(select count(*) from cdaudit));


Print '17'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdinternal') DROP TABLE [cdinternal];

CREATE TABLE [cdinternal] (

  [ci_id] int NOT NULL,

  [ci_company_id] int NOT NULL DEFAULT '0',

  [ci_consignment] int NOT NULL DEFAULT '0',

  [ci_parent] int NOT NULL DEFAULT '0',

  [ci_coupon] char(32) DEFAULT '',

  PRIMARY KEY ([ci_id]),

) ;



INSERT INTO cdinternal select * from openquery(MySQLMain,'select * from cpplEDI.cdinternal');INSERT INTO Temp_tablesload values('cdinternal',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdinternal')),(select count(*) from cdinternal));



--IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdsundry') DROP TABLE [cdsundry];

--CREATE TABLE [cdsundry] (

--  [cn_consignment] int NOT NULL DEFAULT '0',

--  [cn_status] varchar(8) DEFAULT '',

--  [cn_cost] FLOAT(25) DEFAULT NULL,

--  [cn_note] text,

--  [cn_stamp] datetime default NULL,

--  [cn_notified] char(1) DEFAULT 'N',

--  [cn_delcared_value] FLOAT(25) DEFAULT NULL,

--  [cn_insurance] varchar(8) DEFAULT '',

--  [cn_dangerous_goods] char(1) DEFAULT 'N',

--  [cn_email] varchar(255) DEFAULT '',

--  [cn_validation_info] varchar(255) DEFAULT '',

--  [cn_releaseasn] char(25) DEFAULT '',

--  [cn_notbefore] SMALLDATETIME default NULL,

--  [cn_notafter] SMALLDATETIME default NULL,

--  [cn_ezyfreight_price] FLOAT(25) DEFAULT '0',

--  PRIMARY KEY ([cn_consignment])

--) ;





--INSERT INTO cdsundry select * from openquery(MySQLMain,'select * from cpplEDI.cdsundry');INSERT INTO Temp_tablesload values('cdsundry',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdsundry')),(select count(*) from cdsundry));



Print '18'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='checkwork') DROP TABLE [checkwork];

CREATE TABLE [checkwork] (

  [zoning_id] int NOT NULL DEFAULT '0',

  [postcode] int DEFAULT NULL,

  [suburb] varchar(64) DEFAULT NULL,

  [state] varchar(10) DEFAULT NULL,

  [agent_id] int NOT NULL DEFAULT '0',

  [zoneid] int DEFAULT '0',

  [subzoneid] int DEFAULT '0',

  [branch] int DEFAULT NULL,

  [depot] int DEFAULT NULL,

  [cppldriver] int DEFAULT '0',

  [ai_id] int NOT NULL,

  [ai_eta_zone] varchar(12) DEFAULT '',

  [ai_sort_code] varchar(12) DEFAULT NULL,

  [a_imported] char(1) DEFAULT 'N',

  [ai_sort_nsw] char(1) DEFAULT '',

  [ai_sort_act] char(1) DEFAULT '',

  [ai_sort_vic] char(1) DEFAULT '',

  [ai_sort_tas] char(1) DEFAULT '',

  [ai_sort_qld] char(1) DEFAULT '',

  [ai_sort_sa] char(1) DEFAULT '',

  [ai_sort_nt] char(1) DEFAULT '',

  [ai_sort_wa] char(1) DEFAULT '',

  [ai_sort_suffix] char(8) DEFAULT '',

  PRIMARY KEY ([ai_id]),

) ;



INSERT INTO checkwork select * from openquery(MySQLMain,'select * from cpplEDI.checkwork');INSERT INTO Temp_tablesload values('checkwork',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.checkwork')),(select count(*) from checkwork
));


Print '19'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='chrisco') DROP TABLE [chrisco];

CREATE TABLE [chrisco] (

  [ch_id] int NOT NULL,

  [ch_stamp] datetime default NULL,

  [ch_barcode] varchar(64) NOT NULL DEFAULT '',

  [ch_code] int DEFAULT '1',

  [ch_podstamp] datetime default NULL,

  [ch_podname] varchar(255) DEFAULT '',

  [ch_location] varchar(8) DEFAULT 'SYD',

  [ch_processstamp] datetime default NULL,

  [ch_success] char(1) DEFAULT 'N',

  PRIMARY KEY ([ch_id])

) ;



INSERT INTO chrisco select * from openquery(MySQLMain,'select * from cpplEDI.chrisco');INSERT INTO Temp_tablesload values('chrisco',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.chrisco')),(select count(*) from chrisco));


Print '20'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyclass') DROP TABLE [companyclass];

CREATE TABLE [companyclass] (

  [cc_id] int NOT NULL,

  [cc_shortname] varchar(32) NOT NULL DEFAULT '',

  [cc_longname] varchar(64) NOT NULL DEFAULT '',

  [cc_pickup_portion] varchar(64) NOT NULL DEFAULT '',

  [cc_delivery_portion] varchar(64) NOT NULL DEFAULT '',

  [cc_export_prefix] varchar(64) DEFAULT '',

  [cc_export_date] SMALLDATETIME default NULL,

  [cc_export_id] int DEFAULT '1',

  [cc_export_required] char(1) DEFAULT 'N',

  [cc_export_ftp_host] varchar(128) DEFAULT NULL,

  [cc_export_ftp_user] varchar(128) DEFAULT NULL,

  [cc_export_ftp_password] varchar(128) DEFAULT NULL,

  [cc_export_ftp_directory] varchar(128) DEFAULT NULL,

  [cc_ogm] char(1) DEFAULT 'N',

  [cc_direct_book_ezyfreight] char(1) DEFAULT 'N',

  [cc_export2_ftp_host] varchar(128) DEFAULT NULL,

  [cc_export2_ftp_user] varchar(128) DEFAULT NULL,

  [cc_export2_ftp_password] varchar(128) DEFAULT NULL,

  [cc_export2_ftp_directory] varchar(128) DEFAULT NULL,

  [cc_export2_required] char(1) DEFAULT 'N',

  [cc_export2_date] SMALLDATETIME default NULL,

  [cc_export2_id] int DEFAULT '1',

  [cc_export2_prefix] varchar(64) DEFAULT '',

  [cc_export2_daily] char(1) DEFAULT 'N',

  [cc_pay_prefix] varchar(16) DEFAULT '',

  [cc_base_prefix] varchar(8) DEFAULT '',

  [cc_label_name] varchar(255) DEFAULT '',

  [cc_image_file] varchar(255) DEFAULT 'image-POD.ps',

  [cc_invoice_reprint] char(1) DEFAULT 'N',

  [cc_book_pickups] char(1) DEFAULT 'N',

  PRIMARY KEY ([cc_id])

) ;



INSERT INTO companyclass select * from openquery(MySQLMain,'select * from cpplEDI.companyclass');INSERT INTO Temp_tablesload values('companyclass',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.companyclass')),(select count(*) from companyclass));


Print '21'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyclpricecode') DROP TABLE [companyclpricecode];

CREATE TABLE [companyclpricecode] (

  [clp_company_id] int NOT NULL DEFAULT '0',

  [clp_order] int NOT NULL DEFAULT '0',

  [clp_pricecode] varchar(32) NOT NULL DEFAULT '',

) ;



INSERT INTO companyclpricecode select * from openquery(MySQLMain,'select * from cpplEDI.companyclpricecode');INSERT INTO Temp_tablesload values('companyclpricecode',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.companyclpricecode')),(select count(*) from companyclpricecode));


Print '22'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companycontact') DROP TABLE [companycontact];

CREATE TABLE [companycontact] (

  [cc_company_id] int NOT NULL DEFAULT '0',

  [cc_order] int NOT NULL DEFAULT '0',

  [cc_style] varchar(20) DEFAULT NULL,

  [cc_name] varchar(40) DEFAULT NULL,

  [cc_phone] varchar(16) DEFAULT NULL,

  [cc_email] varchar(255) DEFAULT NULL,

) ;



INSERT INTO companycontact select * from openquery(MySQLMain,'select * from cpplEDI.companycontact');INSERT INTO Temp_tablesload values('companycontact',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.companycontact')),(select count(*) from companycontact));


Print '23'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyefpricecode') DROP TABLE [companyefpricecode];

CREATE TABLE [companyefpricecode] (

  [cep_company_id] int NOT NULL DEFAULT '0',

  [cep_order] int NOT NULL DEFAULT '0',

  [cep_pricecode] varchar(32) NOT NULL DEFAULT '',

) ;



INSERT INTO companyefpricecode select * from openquery(MySQLMain,'select * from cpplEDI.companyefpricecode');INSERT INTO Temp_tablesload values('companyefpricecode',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.companyefpricecode')),(select count(*) from companyefpricecode));


Print '24'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companypricecode') DROP TABLE [companypricecode];

CREATE TABLE [companypricecode] (

  [cp_company_id] int NOT NULL DEFAULT '0',

  [cp_order] int NOT NULL DEFAULT '0',

  [cp_match] varchar(32) NOT NULL DEFAULT '',

  [cp_pricecode] varchar(32) NOT NULL DEFAULT '',

  [cp_cubic_factor] char(1) DEFAULT 'N',

  [cp_style] char(1) DEFAULT 'I',

) ;



INSERT INTO companypricecode select * from openquery(MySQLMain,'select * from cpplEDI.companypricecode');INSERT INTO Temp_tablesload values('companypricecode',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.companypricecode')),(select count(*) from companypricecode));


Print '25'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyremapagent') DROP TABLE [companyremapagent];

CREATE TABLE [companyremapagent] (

  [ra_company_id] int NOT NULL DEFAULT '0',

  [ra_order] int NOT NULL DEFAULT '0',

  [ra_from_agent_id] int NOT NULL DEFAULT '0',

  [ra_to_agent_id] int NOT NULL DEFAULT '0',

  [ra_pickup] char(1) NOT NULL DEFAULT 'N',

  [ra_delivery] char(1) NOT NULL DEFAULT 'N',

  [ra_import_only] char(1) DEFAULT 'N',

) ;



INSERT INTO companyremapagent select * from openquery(MySQLMain,'select * from cpplEDI.companyremapagent');INSERT INTO Temp_tablesload values('companyremapagent',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.companyremapagent'))
,(select count(*) from companyremapagent));


Print '26'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companysscc') DROP TABLE [companysscc];

CREATE TABLE [companysscc] (

  [cs_company_id] int NOT NULL DEFAULT '0',

  [cs_order] int NOT NULL DEFAULT '0',

  [cs_match] varchar(32) NOT NULL DEFAULT '',

) ;



INSERT INTO companysscc select * from openquery(MySQLMain,'select * from cpplEDI.companysscc');INSERT INTO Temp_tablesload values('companysscc',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.companysscc')),(select count(*) from companysscc));


Print '27'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='consignmentstatus') DROP TABLE [consignmentstatus];

CREATE TABLE [consignmentstatus] (

  [cs_code] varchar(8) NOT NULL DEFAULT '',

  [cs_description] varchar(64) NOT NULL DEFAULT '',

  [cs_dhl_code] varchar(8) DEFAULT NULL,

  [cs_failure] char(1) DEFAULT 'N',

  PRIMARY KEY ([cs_code])

) ;



INSERT INTO consignmentstatus select * from openquery(MySQLMain,'select * from cpplEDI.consignmentstatus');INSERT INTO Temp_tablesload values('consignmentstatus',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.consignmentstatus'))
,(select count(*) from consignmentstatus));


Print '28'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cpplcpngrid') DROP TABLE [cpplcpngrid];

CREATE TABLE [cpplcpngrid] (

  [ccg_pricegroup] int NOT NULL DEFAULT '0',

  [ccg_pricecode] varchar(16) NOT NULL DEFAULT '',

  [ccg_fromzone] int NOT NULL DEFAULT '0',

  [ccg_tozone] int NOT NULL DEFAULT '0',

  [ccg_present] char(1) DEFAULT 'N',

  [ccg_baseprice] FLOAT(25) DEFAULT '0',

  [ccg_15kg] FLOAT(25) DEFAULT '0',

  [ccg_25kg] FLOAT(25) DEFAULT '0',

  [ccg_pickup_payamount] FLOAT(25) DEFAULT '0',

  [ccg_pickup_15kg] FLOAT(25) DEFAULT '0',

  [ccg_pickup_25kg] FLOAT(25) DEFAULT '0',

  [ccg_deliver_payamount] FLOAT(25) DEFAULT '0',

  [ccg_deliver_15kg] FLOAT(25) DEFAULT '0',

  [ccg_deliver_25kg] FLOAT(25) DEFAULT '0',

) ;





INSERT INTO cpplcpngrid select * from openquery(MySQLMain,'select * from cpplEDI.cpplcpngrid');INSERT INTO Temp_tablesload values('cpplcpngrid',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cpplcpngrid')),(select count(*) from cpplcpngrid));


Print '29'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cpplgrid') DROP TABLE [cpplgrid];

CREATE TABLE [cpplgrid] (

  [cg_pricegroup] int NOT NULL DEFAULT '0',

  [cg_pricecode] varchar(16) NOT NULL DEFAULT '',

  [cg_fromzone] int NOT NULL DEFAULT '0',

  [cg_tozone] int NOT NULL DEFAULT '0',

  [cg_present] char(1) DEFAULT 'N',

  [cg_baseprice] FLOAT(25) DEFAULT '0',

  [cg_kgincluded] FLOAT(25) DEFAULT '0',

  [cg_kgprice] FLOAT(25) DEFAULT '0',

  [cg_pickup_payamount] FLOAT(25) DEFAULT '0',

  [cg_pickup_paybreak] FLOAT(25) DEFAULT '25',

  [cg_pickup_kgpre] FLOAT(25) DEFAULT '0',

  [cg_pickup_kgpost] FLOAT(25) DEFAULT '0',

  [cg_deliver_payamount] FLOAT(25) DEFAULT '0',

  [cg_deliver_paybreak] FLOAT(25) DEFAULT '25',

  [cg_deliver_kgpre] FLOAT(25) DEFAULT '0',

  [cg_deliver_kgpost] FLOAT(25) DEFAULT '0',

) ;



INSERT INTO cpplgrid select * from openquery(MySQLMain,'select * from cpplEDI.cpplgrid');INSERT INTO Temp_tablesload values('cpplgrid',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cpplgrid')),(select count(*) from cpplgrid));


Print '30'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='customereta') DROP TABLE [customereta];

CREATE TABLE [customereta] (

  [ce_from] char(12) DEFAULT '',

  [ce_to] char(12) DEFAULT '',

  [ce_days] FLOAT(25) DEFAULT '1',

) ;



INSERT INTO customereta select * from openquery(MySQLMain,'select * from cpplEDI.customereta');INSERT INTO Temp_tablesload values('customereta',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.customereta')),(select count(*) from customereta));



IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='depot') DROP TABLE [depot];

CREATE TABLE [depot] (

  [d_id] int NOT NULL,

  [d_code] varchar(16) DEFAULT NULL,

  [d_branch] int DEFAULT NULL,

  [d_name] varchar(64) DEFAULT NULL,

  [d_suburb] int DEFAULT NULL,

  [d_address0] varchar(32) DEFAULT NULL,

  [d_address1] varchar(32) DEFAULT NULL,

  [d_contact] varchar(32) DEFAULT NULL,

  [d_style] tinyint DEFAULT '3',

  PRIMARY KEY ([d_id]),

) ;



INSERT INTO depot select * from openquery(MySQLMain,'select * from cpplEDI.depot');INSERT INTO Temp_tablesload values('depot',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.depot')),(select count(*) from depot));


Print '31'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dhlinfo') DROP TABLE [dhlinfo];

CREATE TABLE [dhlinfo] (

  [dhl_awb] varchar(16) NOT NULL DEFAULT '',

  [dhl_origin] varchar(4) DEFAULT NULL,

  [dhl_route] varchar(6) DEFAULT NULL,

  [dhl_service_area] varchar(4) DEFAULT NULL,

  [dhl_location] varchar(4) DEFAULT NULL,

  [dhl_userid] varchar(6) DEFAULT NULL,

  [dhl_completed] char(1) DEFAULT 'N',

  [dhl_consignment] int DEFAULT NULL,
  
  dhl_cycle Varchar(500)

  PRIMARY KEY ([dhl_awb]),

) ;



INSERT INTO dhlinfo select * from openquery(MySQLMain,'select * from cpplEDI.dhlinfo');
INSERT INTO Temp_tablesload values('dhlinfo',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.dhlinfo')),(select count(*) from dhlinfo));


Print '32'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dhlroute') DROP TABLE [dhlroute];

CREATE TABLE [dhlroute] (

  [dr_route] char(8) NOT NULL,

  [dr_agent] int DEFAULT '0',

  [dr_pickup_suburb] char(64) DEFAULT NULL,

  [dr_pickup_postcode] int DEFAULT NULL,

  [dr_branch] int DEFAULT NULL,

  [dr_company_id] int DEFAULT '21',

  PRIMARY KEY ([dr_route])

) ;



INSERT INTO dhlroute select * from openquery(MySQLMain,'select * from cpplEDI.dhlroute');
INSERT INTO Temp_tablesload values('dhlroute',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.dhlroute')),(select count(*) from dhlroute));




Print '33'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dirty_consignment') DROP TABLE [dirty_consignment];

CREATE TABLE [dirty_consignment] (

  [dc_id] int NOT NULL DEFAULT '0',

  [dc_company_id] int NOT NULL DEFAULT '0',

  PRIMARY KEY ([dc_id])

) ;



INSERT INTO dirty_consignment select * from openquery(MySQLMain,'select * from cpplEDI.dirty_consignment');INSERT INTO Temp_tablesload values('dirty_consignment',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.dirty_consignment'))
,(select count(*) from dirty_consignment));


Print '34'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dirty_coupon') DROP TABLE [dirty_coupon];

CREATE TABLE [dirty_coupon] (

  [dc_id] int NOT NULL DEFAULT '0',

  [dc_company_id] int NOT NULL DEFAULT '0',

  PRIMARY KEY ([dc_id])

) ;



INSERT INTO dirty_coupon select * from openquery(MySQLMain,'select * from cpplEDI.dirty_coupon');INSERT INTO Temp_tablesload values('dirty_coupon',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.dirty_coupon')),(select count(*) from dirty_coupon));


Print '35'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='driver') DROP TABLE [driver];

CREATE TABLE [driver] (

  [dr_id] int NOT NULL,

  [dr_branch] int DEFAULT NULL,

  [dr_number] int DEFAULT NULL,

  [dr_name] char(64) DEFAULT NULL,

  [dr_depot] int DEFAULT NULL,

  [dr_warehouse] char(16) DEFAULT NULL,

  [dr_agentname] char(16) DEFAULT '',

  [dr_typecode] char(2) DEFAULT NULL,

  [dr_type] char(32) DEFAULT NULL,

  PRIMARY KEY ([dr_id]),

) ;



INSERT INTO driver select * from openquery(MySQLMain,'select * from cpplEDI.driver');INSERT INTO Temp_tablesload values('driver',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.driver')),(select count(*) from driver));


Print '36'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ediconfig') DROP TABLE [ediconfig];

CREATE TABLE [ediconfig] (

  [ec_parameter] varchar(256) NOT NULL,

  [ec_style] char(16) DEFAULT 'string',

  [ec_section] varchar(256) DEFAULT '',

  [ec_description] varchar(256) DEFAULT '',

  [ec_value] varchar(256) DEFAULT '',

) ;





INSERT INTO ediconfig select * from openquery(MySQLMain,'select * from cpplEDI.ediconfig');INSERT INTO Temp_tablesload values('ediconfig',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ediconfig')),(select count(*) from ediconfig
));



--IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='enquiry') DROP TABLE [enquiry];

--CREATE TABLE [enquiry] (

--  [srno] int NOT NULL,

--  [pname] varchar(100) NOT NULL DEFAULT '',

--  [pcompanyno] varchar(100) NOT NULL DEFAULT '',

--  [couponno] varchar(100) NOT NULL DEFAULT '',

--  [type] varchar(100) NOT NULL DEFAULT '',

--  [status] varchar(100) NOT NULL DEFAULT '',

--  [sdatetime] datetime default NULL,

--  [edatetime] datetime default NULL,

--  [dhours] int NOT NULL DEFAULT '0',

--  [dmin] int NOT NULL DEFAULT '0',

--  [pdriver] int NOT NULL DEFAULT '0',

--  [pdriverbranch] int DEFAULT '0',

--  [contact] varchar(100) NOT NULL DEFAULT '',

--  [padd1] varchar(100) NOT NULL DEFAULT '',

--  [padd2] varchar(100) DEFAULT '',

--  [padd3] varchar(100) DEFAULT '',

--  [psub] varchar(100) NOT NULL DEFAULT '',

--  [ppcode] varchar(10) DEFAULT '',

--  [dadd1] varchar(100) NOT NULL DEFAULT '',

--  [dadd2] varchar(100) DEFAULT '',

--  [dadd3] varchar(100) DEFAULT '',

--  [dsub] varchar(100) NOT NULL DEFAULT '',

--  [dpcode] varchar(10) DEFAULT '',

--  [dname] varchar(100) NOT NULL DEFAULT '',

--  [dcompanyno] varchar(100) NOT NULL DEFAULT '',

--  [ddriver] int NOT NULL DEFAULT '0',

--  [ddriverbranch] int DEFAULT '0',

--  [name] varchar(100) NOT NULL DEFAULT '',

--  [branch] varchar(100) NOT NULL DEFAULT '',

--  [callyn] varchar(100) NOT NULL DEFAULT '0',

--  [lastcall] datetime null default NULL,

--  [goodd] varchar(100) NOT NULL DEFAULT '',

--  [csr] varchar(100) NOT NULL DEFAULT '',

--  PRIMARY KEY ([srno])

--) ;



--INSERT INTO enquiry select * from openquery(MySQLMain,'select * from cpplEDI.enquiry');INSERT INTO Temp_tablesload values('enquiry',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.enquiry')),(select count(*) from enquiry));


Print '37'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='enquiryd') DROP TABLE [enquiryd];

CREATE TABLE [enquiryd] (

  [srno] int NOT NULL,

  [date] SMALLDATETIME default NULL,

  [time] time NOT NULL DEFAULT '00:00:00',

  [csr] varchar(100) NOT NULL DEFAULT '',

  [details] text NOT NULL,

  [action] text NOT NULL,

  [ensrno] int NOT NULL DEFAULT '0',

  PRIMARY KEY ([srno])

) ;





INSERT INTO enquiryd select * from openquery(MySQLMain,'select * from cpplEDI.enquiryd');INSERT INTO Temp_tablesload values('enquiryd',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.enquiryd')),(select count(*) from enquiryd));


Print '38'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='etagrid') DROP TABLE [etagrid];

CREATE TABLE [etagrid] (

  [eg_from] varchar(10) NOT NULL DEFAULT '',

  [eg_to] varchar(10) NOT NULL DEFAULT '',

  [eg_postcode] int DEFAULT '0',

  [eg_suburb] varchar(40) DEFAULT '',

  [eg_send] int NOT NULL DEFAULT '0',

  [eg_leg1] varchar(10) DEFAULT '',

  [eg_leg1_days] int DEFAULT '0',

  [eg_leg2] varchar(10) DEFAULT '',

  [eg_leg2_days] int DEFAULT '0',

  [eg_leg3] varchar(10) DEFAULT '',

  [eg_leg3_days] int DEFAULT '0',

  [eg_to_days] int DEFAULT '0',

) ;



INSERT INTO etagrid select * from openquery(MySQLMain,'select * from cpplEDI.etagrid');INSERT INTO Temp_tablesload values('etagrid',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.etagrid')),(select count(*) from etagrid));


Print '39'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='etastate') DROP TABLE [etastate];

CREATE TABLE [etastate] (

  [es_zone] char(8) DEFAULT NULL,

  [es_state] char(8) DEFAULT NULL,

) ;



INSERT INTO etastate select * from openquery(MySQLMain,'select * from cpplEDI.etastate');INSERT INTO Temp_tablesload values('etastate',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.etastate')),(select count(*) from etastate));


Print '40'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pwreset') DROP TABLE [pwreset];

CREATE TABLE [pwreset] (

  [pr_id] int  NOT NULL,

  [pr_userid] int  NOT NULL,

  [pr_username] varchar(32) NOT NULL,

  [pr_email] varchar(256) NOT NULL,

  [pr_create_stamp] datetime default NULL,

  [pr_unique_id] varchar(32) NOT NULL,

  [pr_state] int DEFAULT '0',

  [pr_reset_stamp] datetime default NULL,

  PRIMARY KEY ([pr_id]),

) ;



INSERT INTO pwreset select * from openquery(MySQLMain,'select * from cpplEDI.pwreset');INSERT INTO Temp_tablesload values('pwreset',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.pwreset')),(select count(*) from pwreset));


Print '41'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='exceptionmap') DROP TABLE [exceptionmap];

CREATE TABLE [exceptionmap] (

  [em_id] int NOT NULL,

  [em_style] char(1) DEFAULT 'D',

  [em_string] varchar(64) NOT NULL DEFAULT '',

  [em_status] varchar(8) NOT NULL DEFAULT '',

  [em_fullmatch] char(1) DEFAULT 'N',

  PRIMARY KEY ([em_id]),

) ;





INSERT INTO exceptionmap select * from openquery(MySQLMain,'select * from cpplEDI.exceptionmap');INSERT INTO Temp_tablesload values('exceptionmap',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.exceptionmap')),(select count(*) from exceptionmap));


Print '42'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='exports') DROP TABLE [exports];

CREATE TABLE [exports] (

  [e_id] int NOT NULL,

  [e_stamp] datetime default NULL,

  [e_filename] varchar(64) NOT NULL DEFAULT '',

  [e_consignments] int DEFAULT '0',

  [e_bookins] int DEFAULT '0',

  [e_class] int DEFAULT '0',

  [e_2nd] char(1) DEFAULT 'N',

  PRIMARY KEY ([e_id])

) ;



INSERT INTO exports select * from openquery(MySQLMain,'select * from cpplEDI.exports');INSERT INTO Temp_tablesload values('exports',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.exports')),(select count(*) from exports));


Print '43'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ftpsetup') DROP TABLE [ftpsetup];

CREATE TABLE [ftpsetup] (

  [fs_user] varchar(64) NOT NULL DEFAULT '',

  [fs_password] varchar(64) NOT NULL DEFAULT '',

  [fs_description] varchar(64) DEFAULT NULL,

  [fs_uid] int DEFAULT '32767',

  [fs_gid] int DEFAULT '32767',

  [fs_dir] varchar(255) DEFAULT '/tmp',

  PRIMARY KEY ([fs_user])

) ;



INSERT INTO ftpsetup select * from openquery(MySQLMain,'select * from cpplEDI.ftpsetup');INSERT INTO Temp_tablesload values('ftpsetup',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ftpsetup')),(select count(*) from ftpsetup));


Print '44'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='goods') DROP TABLE [goods];

CREATE TABLE [goods] (

  [g_id] int NOT NULL,

  [g_company_id] int DEFAULT '0',

  [g_user] varchar(32) DEFAULT '',

  [g_description] varchar(32) DEFAULT '',

  [g_deadweight] FLOAT(25) DEFAULT NULL,

  [g_dimension0] FLOAT(25) DEFAULT NULL,

  [g_dimension1] FLOAT(25) DEFAULT NULL,

  [g_dimension2] FLOAT(25) DEFAULT NULL,

  [g_volume] FLOAT(25) DEFAULT NULL,

  PRIMARY KEY ([g_id])

) ;



INSERT INTO goods select * from openquery(MySQLMain,'select * from cpplEDI.goods');INSERT INTO Temp_tablesload values('goods',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.goods')),(select count(*) from goods));


Print '45'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='groupname') DROP TABLE [groupname];

CREATE TABLE [groupname] (

  [groupname] varchar(32) NOT NULL DEFAULT '',

  [description] varchar(64) DEFAULT NULL,

  PRIMARY KEY ([groupname])

) ;



INSERT INTO groupname select * from openquery(MySQLMain,'select * from cpplEDI.groupname');INSERT INTO Temp_tablesload values('groupname',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.groupname')),(select count(*) from groupname
));


Print '46'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='groups') DROP TABLE [groups];

CREATE TABLE [groups] (

  [account] varchar(32) NOT NULL DEFAULT '',

  [groups] varchar(32) NOT NULL DEFAULT '',

  PRIMARY KEY ([account],[groups])

) ;





INSERT INTO groups select * from openquery(MySQLMain,'select * from cpplEDI.groups');INSERT INTO Temp_tablesload values('groups',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.groups')),(select count(*) from groups));


Print '47'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='guessSSCC') DROP TABLE [guessSSCC];

CREATE TABLE [guessSSCC] (

  [gs_portion] char(7) DEFAULT NULL,

  [gs_name] char(32) DEFAULT NULL,

  [gs_company_id] int DEFAULT NULL,

) ;



INSERT INTO guessSSCC select * from openquery(MySQLMain,'select * from cpplEDI.guessSSCC');INSERT INTO Temp_tablesload values('guessSSCC',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.guessSSCC')),(select count(*) from guessSSCC
));


Print '48'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='holidays') DROP TABLE [holidays];

CREATE TABLE [holidays] (

  [h_id] int NOT NULL,

  [h_state] varchar(10) NOT NULL DEFAULT 'ALL',

  [h_style] char(1) DEFAULT 'S',

  [h_holiday] varchar(20) NOT NULL DEFAULT '',

  [h_name] varchar(64) DEFAULT '',

  PRIMARY KEY ([h_id])

) ;



INSERT INTO holidays select * from openquery(MySQLMain,'select * from cpplEDI.holidays');INSERT INTO Temp_tablesload values('holidays',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.holidays')),(select count(*) from holidays));


Print '49'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='iccompany') DROP TABLE [iccompany];

CREATE TABLE [iccompany] (

  [ic_importconfig] int NOT NULL DEFAULT '0',

  [ic_order] int NOT NULL DEFAULT '0',

  [ic_match] varchar(32) NOT NULL DEFAULT '',

  [ic_company_id] varchar(32) NOT NULL DEFAULT '',

) ;



INSERT INTO iccompany select * from openquery(MySQLMain,'select * from cpplEDI.iccompany');INSERT INTO Temp_tablesload values('iccompany',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.iccompany')),(select count(*) from iccompany
));


Print '50'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='icfile') DROP TABLE [icfile];

CREATE TABLE [icfile] (

  [if_importconfig] int NOT NULL DEFAULT '0',

  [if_order] int NOT NULL DEFAULT '0',

  [if_match] varchar(32) NOT NULL DEFAULT '',

) ;



INSERT INTO icfile select * from openquery(MySQLMain,'select * from cpplEDI.icfile');INSERT INTO Temp_tablesload values('icfile',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.icfile')),(select count(*) from icfile));



--IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='imports') DROP TABLE [imports];

--CREATE TABLE [imports] (

--  [i_id] int NOT NULL,

--  [i_style] char(1) DEFAULT 'C',

--  [i_company] int DEFAULT NULL,

--  [i_stamp] datetime default NULL,

--  [i_filename] varchar(64) DEFAULT NULL,

--  [i_consignments] int DEFAULT NULL,

--  [i_references] int DEFAULT NULL,

--  [i_coupons] int DEFAULT NULL,

--  [i_bookins] int DEFAULT NULL,

--  [i_date] SMALLDATETIME default NULL,

--  [i_manifest_id] int DEFAULT '0',

--  PRIMARY KEY ([i_id]),

--) ;



--INSERT INTO imports select * from openquery(MySQLMain,'select * from cpplEDI.imports');INSERT INTO Temp_tablesload values('imports',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.imports')),(select count(*) from imports));




Print '51'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='invoicestore') DROP TABLE [invoicestore];

CREATE TABLE [invoicestore] (

  [is_id] int NOT NULL,

  [is_account] char(32) NOT NULL DEFAULT '',

  [is_invoiceno] char(32) NOT NULL DEFAULT '',

  [is_date] SMALLDATETIME default NULL,

  [is_imported] datetime default NULL,

  PRIMARY KEY ([is_id]),

) ;





INSERT INTO invoicestore select * from openquery(MySQLMain,'select * from cpplEDI.invoicestore');INSERT INTO Temp_tablesload values('invoicestore',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.invoicestore')),(select count(*) from invoicestore));


Print '52'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='irp') DROP TABLE [irp];

CREATE TABLE [irp] (

  [irp_id] int NOT NULL,

  [irp_consignment] int NOT NULL DEFAULT '0',

  [irp_delivery_branch] int NOT NULL DEFAULT '0',

  [irp_delivery_phone] char(32) DEFAULT '',

  [irp_delivery_details] char(255) DEFAULT '',

  [irp_delivery_suburb_string] char(40) DEFAULT NULL,

  [irp_delivery_suburb] int DEFAULT '0',

  [irp_pickup_branch] int NOT NULL DEFAULT '0',

  [irp_pickup_phone] char(32) DEFAULT '',

  [irp_pickup_details] char(255) DEFAULT '',

  [irp_pickup_suburb_string] char(40) DEFAULT NULL,

  [irp_pickup_suburb] int DEFAULT '0',

  [irp_pickup_contact] char(32) DEFAULT '',

  [irp_pickup_location] char(32) DEFAULT '',

  [irp_pickup_date] SMALLDATETIME default NULL,

  [irp_pickup_time] time DEFAULT '00:00:00',

  [irp_job_no] int DEFAULT '0',

  [irp_job_date] SMALLDATETIME default NULL,

  [irp_coupon] char(32) DEFAULT '',

  [irp_coupon_id] int DEFAULT '0',

  [irp_tracker] char(32) DEFAULT '',

  [irp_tracker_id] int DEFAULT '0',

  [irp_dimension0] int DEFAULT '0',

  [irp_dimension1] int DEFAULT '0',

  [irp_dimension2] int DEFAULT '0',

  [irp_cube] int DEFAULT '0',

  [irp_deadweight] int DEFAULT '0',

  [irp_deleted] char(1) DEFAULT 'N',

  PRIMARY KEY ([irp_id]),

) ;



INSERT INTO irp select * from openquery(MySQLMain,'select * from cpplEDI.irp');INSERT INTO Temp_tablesload values('irp',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.irp')),(select count(*) from irp));


Print '53'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='irpcoupon') DROP TABLE [irpcoupon];

CREATE TABLE [irpcoupon] (

  [ic_irp_id] int NOT NULL DEFAULT '0',

  [ic_coupon] char(32) DEFAULT '',

  [ic_coupon_id] int DEFAULT '0',

  [ic_tracker] char(1) DEFAULT 'Y'

) ;



INSERT INTO irpcoupon select * from openquery(MySQLMain,'select * from cpplEDI.irpcoupon');INSERT INTO Temp_tablesload values('irpcoupon',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.irpcoupon')),(select count(*) from irpcoupon
));


Print '54'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='importconfig') DROP TABLE [importconfig];

CREATE TABLE [importconfig] (

  [ic_id] int NOT NULL,

  [ic_description] varchar(32) NOT NULL DEFAULT '',

  [ic_company_id] int NOT NULL DEFAULT '0',

  [ic_mechanism] char(1) DEFAULT 'F',

  [ic_style] char(1) DEFAULT 'C',

  [ic_format] varchar(32) NOT NULL DEFAULT '',

  [ic_default_insurance] char(1) DEFAULT '0',

  [ic_force_default_insurance] char(1) DEFAULT '0',

  [ic_unique_connote] char(1) DEFAULT 'N',

  [ic_unique_timeout] int DEFAULT '366',

  [ic_update_connote] char(1) DEFAULT 'N',

  [ic_update_timeout] int DEFAULT '14',

  [ic_import_to_ezyfreight] char(1) DEFAULT 'N',

  [ic_validate_import] char(1) DEFAULT 'N',

  [ic_ftp_host] varchar(64) DEFAULT NULL,

  [ic_ftp_user] varchar(64) DEFAULT NULL,

  [ic_ftp_password] varchar(64) DEFAULT NULL,

  [ic_ftp_directory] varchar(64) DEFAULT NULL,

  [ic_ftp_archive] varchar(64) DEFAULT NULL,

  [ic_frequency] char(1) DEFAULT 'A',

  [ic_default_addr0] varchar(40) DEFAULT '',

  [ic_default_addr1] varchar(40) DEFAULT '',

  [ic_default_addr2] varchar(40) DEFAULT '',

  [ic_default_addr3] varchar(40) DEFAULT '',

  [ic_default_suburb] varchar(30) DEFAULT '',

  [ic_default_postcode] int DEFAULT '0',

  [ic_default_always] char(1) DEFAULT 'N',

  [ic_default_atl] char(1) DEFAULT 'N',

  [ic_lastlog] varchar(128) DEFAULT NULL,

  [ic_laststamp] datetime default NULL,

  [ic_lastok] char(1) DEFAULT 'Y',

  [ic_lastfile] varchar(128) DEFAULT NULL,

  [ic_lastfilestamp] datetime default NULL,

  [ic_default_weight] FLOAT(25) DEFAULT '0',

  [ic_default_volume] FLOAT(25) DEFAULT '0',

  [ic_default_per_item] char(1) DEFAULT 'N',

  [ic_book_pickups] char(1) DEFAULT 'N',

  [ic_email_recipient] char(1) DEFAULT 'N',

  [ic_missing_alert] char(1) DEFAULT 'N',

  [ic_missing_time] int DEFAULT '0',

  [ic_missing_email] varchar(255) DEFAULT '',

  [ic_missing_name] varchar(255) DEFAULT '',

  [ic_missing_checked] datetime default NULL,

  PRIMARY KEY ([ic_id]),

) ;



INSERT INTO importconfig 
(ic_id	,
ic_description	,
ic_company_id	,
ic_mechanism	,
ic_style	,
ic_format	,
ic_default_insurance	,
ic_force_default_insurance	,
ic_unique_connote	,
ic_unique_timeout	,
ic_update_connote	,
ic_update_timeout	,
ic_import_to_ezyfreight	,
ic_validate_import	,
ic_ftp_host	,
ic_ftp_user	,
ic_ftp_password	,
ic_ftp_directory	,
ic_ftp_archive	,
ic_frequency	,
ic_default_addr0	,
ic_default_addr1	,
ic_default_addr2	,
ic_default_addr3	,
ic_default_suburb	,
ic_default_postcode	,
ic_default_always	,
ic_default_atl	,
ic_lastlog	,
ic_laststamp	,
ic_lastok	,
ic_lastfile	,
ic_lastfilestamp	,
ic_default_weight	,
ic_default_volume	,
ic_default_per_item	,
ic_book_pickups	,
ic_email_recipient	,
ic_missing_alert	,
ic_missing_time	,
ic_missing_email	,
ic_missing_name	,
ic_missing_checked	
)

select 
ic_id	,
ic_description	,
ic_company_id	,
ic_mechanism	,
ic_style	,
ic_format	,
ic_default_insurance	,
ic_force_default_insurance	,
ic_unique_connote	,
ic_unique_timeout	,
ic_update_connote	,
ic_update_timeout	,
ic_import_to_ezyfreight	,
ic_validate_import	,
ic_ftp_host	,
ic_ftp_user	,
ic_ftp_password	,
ic_ftp_directory	,
ic_ftp_archive	,
ic_frequency	,
ic_default_addr0	,
ic_default_addr1	,
ic_default_addr2	,
ic_default_addr3	,
ic_default_suburb	,
ic_default_postcode	,
ic_default_always	,
ic_default_atl	,
ic_lastlog	,
ic_laststamp	,
ic_lastok	,
ic_lastfile	,
ic_lastfilestamp	,
ic_default_weight	,
ic_default_volume	,
ic_default_per_item	,
ic_book_pickups	,
ic_email_recipient	,
ic_missing_alert	,
ic_missing_time	,
ic_missing_email	,
ic_missing_name	,
ic_missing_checked	

from openquery(MySQLMain,'select * from cpplEDI.importconfig');INSERT INTO Temp_tablesload values('importconfig',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.importconfig')),(select count(*) from importconfig));


Print '54'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='login') DROP TABLE [login];

CREATE TABLE [login] (

  [username] varchar(100) NOT NULL DEFAULT '',

  [password] varchar(100) NOT NULL DEFAULT '',

  [branch] varchar(100) NOT NULL DEFAULT '',

  [level] int NOT NULL DEFAULT '0',

  PRIMARY KEY ([username])

) ;



INSERT INTO [cpplEDI].[dbo].[login] select * from openquery(MySQLMain,'select * from cpplEDI.login');INSERT INTO Temp_tablesload values('login',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.login')),(select count(*) from [login]
));






Print '55'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='location') DROP TABLE location;

CREATE TABLE location (
 l_number int NOT NULL DEFAULT '0',
  l_branch char(16) NOT NULL DEFAULT '',
  l_name char(128) DEFAULT '',
  l_custid int DEFAULT '0',
  l_custname char(128) DEFAULT '',
  l_update datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY
 KEY (l_branch,l_number)

  );



INSERT INTO [cpplEDI].[dbo].location select * from openquery(MySQLMain,'select * from cpplEDI.location');INSERT INTO Temp_tablesload values('location',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.location')),(select count(*) from location));










Print '56'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='manifest') DROP TABLE [manifest];

CREATE TABLE [manifest] (

  [m_id] int NOT NULL,

  [m_date] SMALLDATETIME default NULL,

  [m_company_id] int NOT NULL DEFAULT '0',

  [m_description] varchar(32) DEFAULT '',

  [m_comments] varchar(255) DEFAULT '',

  [m_consignments] int DEFAULT '0',

  [m_stamp] datetime default NULL,

  [m_class] int NOT NULL DEFAULT '0',

  [m_import_id] int DEFAULT NULL,

  [m_released] char(1) DEFAULT 'N',

  [m_printed] char(1) DEFAULT 'N',

  PRIMARY KEY ([m_id])

) ;



INSERT INTO manifest select * from openquery(MySQLMain,'select * from cpplEDI.manifest');INSERT INTO Temp_tablesload values('manifest',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.manifest')),(select count(*) from manifest));


Print '57'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='noncosmosdriver') DROP TABLE [noncosmosdriver];

CREATE TABLE [noncosmosdriver] (

  [nc_branch] int NOT NULL DEFAULT '0',

  [nc_contractor] int NOT NULL DEFAULT '0',

  [nc_description] varchar(64) DEFAULT NULL,

  PRIMARY KEY ([nc_branch],[nc_contractor])

) ;



INSERT INTO noncosmosdriver select * from openquery(MySQLMain,'select * from cpplEDI.noncosmosdriver');INSERT INTO Temp_tablesload values('noncosmosdriver',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.noncosmosdriver')),(select
 count(*) from noncosmosdriver));


 Print '58'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ogmanifest') DROP TABLE [ogmanifest];

CREATE TABLE [ogmanifest] (

  [ogm_id] int NOT NULL,

  [ogm_date] SMALLDATETIME default NULL,

  [ogm_stamp] datetime default NULL,

  [ogm_class] int NOT NULL DEFAULT '0',

  [ogm_agent] int NOT NULL DEFAULT '0',

  [ogm_count] int DEFAULT '0',

  [ogm_returns] char(1) DEFAULT 'N',

  PRIMARY KEY ([ogm_id]),

) ;



INSERT INTO ogmanifest select * from openquery(MySQLMain,'select * from cpplEDI.ogmanifest');INSERT INTO Temp_tablesload values('ogmanifest',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ogmanifest')),
(select count(*) from ogmanifest));


Print '59'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ogmexceptions') DROP TABLE [ogmexceptions];

CREATE TABLE [ogmexceptions] (

  [oge_id] int NOT NULL,

  [oge_ogm_id] int NOT NULL DEFAULT '0',

  [oge_consignment_id] int NOT NULL DEFAULT '0',

  [oge_description] varchar(255) DEFAULT NULL,

  PRIMARY KEY ([oge_id])

) ;



INSERT INTO ogmexceptions select * from openquery(MySQLMain,'select * from cpplEDI.ogmexceptions');INSERT INTO Temp_tablesload values('ogmexceptions',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ogmexceptions')),(select count(*
) from ogmexceptions));


Print '60'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='rating') DROP TABLE [rating];

CREATE TABLE [rating] (

  [r_id] int NOT NULL,

  [r_company_id] int NOT NULL DEFAULT '0',

  [r_name] varchar(32) DEFAULT NULL,

  [r_description] varchar(32) DEFAULT NULL,

  PRIMARY KEY ([r_id])

) ;



INSERT INTO rating select * from openquery(MySQLMain,'select * from cpplEDI.rating');INSERT INTO Temp_tablesload values('rating',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.rating')),(select count(*) from rating));


Print '61'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='receipts') DROP TABLE [receipts];

CREATE TABLE [receipts] (

  [r_id] int NOT NULL,

  [r_company_id] int NOT NULL DEFAULT '0',

  [r_user_id] int NOT NULL DEFAULT '0',

  [r_date] SMALLDATETIME default NULL,

  [r_creation_stamp] datetime default NULL,

  [r_exclusive] FLOAT(25) DEFAULT '0',

  [r_gst] FLOAT(25) DEFAULT '0',

  [r_total] FLOAT(25) DEFAULT '0',

  [r_unallocated] FLOAT(25) DEFAULT '0',

  [r_type] char(1) DEFAULT '',

  [r_style] char(1) DEFAULT '',

  [r_closed] char(1) DEFAULT 'N',

  [r_reversed] char(1) DEFAULT 'N',

  [r_bank] varchar(32) DEFAULT NULL,

  [r_branch] varchar(32) DEFAULT NULL,

  [r_drawer] varchar(50) DEFAULT NULL,

  [r_no] varchar(32) DEFAULT NULL,

  [r_reference] varchar(32) DEFAULT NULL,

  [r_comment] varchar(64) DEFAULT NULL,

  PRIMARY KEY ([r_id])

) ;



INSERT INTO receipts select * from openquery(MySQLMain,'select * from cpplEDI.receipts');INSERT INTO Temp_tablesload values('receipts',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.receipts')),(select count(*) from receipts));


Print '2'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='return_info') DROP TABLE [return_info];

CREATE TABLE [return_info] (

  [r_id] int NOT NULL,

  [r_company_id] int NOT NULL DEFAULT '0',

  [r_consignment] int NOT NULL DEFAULT '0',

  PRIMARY KEY ([r_id])

) ;



INSERT INTO return_info select * from openquery(MySQLMain,'select * from cpplEDI.return_info');INSERT INTO Temp_tablesload values('return_info',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.return_info')),
(select count(*) from return_info));


Print '3'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='salesrep') DROP TABLE [salesrep];

CREATE TABLE [salesrep] (

  [sr_id] int NOT NULL,

  [sr_code] varchar(8) NOT NULL DEFAULT '',

  [sr_name] varchar(40) NOT NULL DEFAULT '',

  [sr_addr0] varchar(40) DEFAULT NULL,

  [sr_addr1] varchar(40) DEFAULT NULL,

  [sr_addr2] varchar(40) DEFAULT NULL,

  [sr_addr3] varchar(40) DEFAULT NULL,

  [sr_suburb] varchar(30) DEFAULT NULL,

  [sr_postcode] int DEFAULT '0',

  [sr_phone] varchar(16) DEFAULT NULL,

  [sr_started] SMALLDATETIME default NULL,

  [sr_terminated] SMALLDATETIME default NULL,

  PRIMARY KEY ([sr_id]),

) ;



INSERT INTO salesrep select * from openquery(MySQLMain,'select * from cpplEDI.salesrep');INSERT INTO Temp_tablesload values('salesrep',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.salesrep')),(select count(*) from salesrep));


Print '4'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='sessions') DROP TABLE [sessions];

CREATE TABLE [sessions] (

  [id] varchar(32) NOT NULL DEFAULT '',

  [access] int DEFAULT NULL,

  [data] text,

  PRIMARY KEY ([id])

) ;



INSERT INTO [cpplEDI].[dbo].[sessions] select * from openquery(MySQLMain,'select * from cpplEDI.sessions');INSERT INTO Temp_tablesload values('sessions',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.sessions')),(select count(*) 
from [sessions]));


Print '5'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='sortationdriver') DROP TABLE [sortationdriver];

CREATE TABLE [sortationdriver] (

  [s_branch] int NOT NULL DEFAULT '0',

  [s_contractor] int NOT NULL DEFAULT '0',

  [s_description] varchar(64) DEFAULT NULL,

  PRIMARY KEY ([s_branch],[s_contractor])

) ;



INSERT INTO sortationdriver select * from openquery(MySQLMain,'select * from cpplEDI.sortationdriver');INSERT INTO Temp_tablesload values('sortationdriver',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.sortationdriver')),(select
 count(*) from sortationdriver));


 Print '6'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='suburb') DROP TABLE [suburb];

CREATE TABLE [suburb] (

  [s_id] int NOT NULL,

  [s_cosmosid] int DEFAULT NULL,

  [s_branch] int DEFAULT NULL,

  [s_depot] int DEFAULT NULL,

  [s_driver] int DEFAULT NULL,

  [s_postcode] int DEFAULT NULL,

  [s_name] varchar(32) DEFAULT NULL,

  [s_state] varchar(10) DEFAULT NULL,

  [s_zone] varchar(10) DEFAULT NULL,

  PRIMARY KEY ([s_id]),

) ;



INSERT INTO suburb select * from openquery(MySQLMain,'select * from cpplEDI.suburb');INSERT INTO Temp_tablesload values('suburb',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.suburb')),(select count(*) from suburb));






Print '7'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='survey2015') DROP TABLE [survey2015];

CREATE TABLE survey2015 (
  s_user char(32) NOT NULL,
  s_answered date ,
  s_skipped varchar(1) DEFAULT 'N',
  s_q1 varchar(16) DEFAULT '',
  s_q2 varchar(16) DEFAULT '',
  s_q3 varchar(16) DEFAULT '',
  s_q4 varchar(16) DEFAULT '',
  s_q5 varchar(max),

  PRIMARY KEY (s_user)
) ;

INSERT INTO survey2015 select * from openquery(MySQLMain,'select * from cpplEDI.survey2015');INSERT INTO Temp_tablesload values('survey2015',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.survey2015'))
,(select count(*) from survey2015));






Print '8'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='trackcompany') DROP TABLE [trackcompany];

CREATE TABLE [trackcompany] (

  [tc_track] int NOT NULL DEFAULT '0',

  [tc_company_id] int NOT NULL DEFAULT '0',

) ;



INSERT INTO trackcompany select * from openquery(MySQLMain,'select * from cpplEDI.trackcompany');INSERT INTO Temp_tablesload values('trackcompany',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.trackcompany')),(select count(*) from trackcompany));


Print '9'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='trackkey') DROP TABLE [trackkey];

CREATE TABLE [trackkey] (

  [t_id] int  NOT NULL,

  [t_key] varchar(32) NOT NULL DEFAULT '',

  [t_description] varchar(64) NOT NULL DEFAULT '',

  [t_includeeta] char(1) DEFAULT 'N',

  [t_limited] char(1) DEFAULT 'N',

  [t_api] char(1) DEFAULT 'N',

  [t_message] varchar(500) NOT NULL DEFAULT '',
  
  [t_last_used] datetime default NULL,

  [t_use_count] int default '0',

  PRIMARY KEY ([t_id]),

) ;



INSERT INTO trackkey select * from openquery(MySQLMain,'select * from cpplEDI.trackkey');INSERT INTO Temp_tablesload values('trackkey',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.trackkey')),(select count(*) from trackkey));

--select * from openquery(MySQLMain,'select* from cpplEDI.trackkey')
Print '10'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='transactions') DROP TABLE [transactions];

CREATE TABLE [transactions] (

  [t_id] int NOT NULL,

  [t_company_id] int NOT NULL DEFAULT '0',

  [t_date] SMALLDATETIME default NULL,

  [t_creation_stamp] datetime default NULL,

  [t_exclusive] FLOAT(25) DEFAULT '0',

  [t_gst] FLOAT(25) DEFAULT '0',

  [t_total] FLOAT(25) DEFAULT '0',

  [t_unallocated] FLOAT(25) DEFAULT '0',

  [t_type] char(1) DEFAULT '',

  [t_billing_id] int DEFAULT NULL,

  [t_parent_id] int DEFAULT NULL,

  [t_receipt_id] int DEFAULT NULL,

  [t_open] char(1) DEFAULT 'N',

  PRIMARY KEY ([t_id])

) ;



INSERT INTO transactions select * from openquery(MySQLMain,'select * from cpplEDI.transactions');INSERT INTO Temp_tablesload values('transactions',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.transactions')),(select count(*) from transactions));


Print '1'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='type') DROP TABLE [type];

CREATE TABLE [type] (

  [ename] varchar(100) NOT NULL DEFAULT '',

  [ediscription] varchar(100) NOT NULL DEFAULT '',

  PRIMARY KEY ([ename])

) ;



INSERT INTO [cpplEDI].[dbo].[type] select * from openquery(MySQLMain,'select * from cpplEDI.type');INSERT INTO Temp_tablesload values('type',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.type')),(select count(*) from [type]));


Print '2'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='uctmp') DROP TABLE [uctmp];

CREATE TABLE [uctmp] (

  [uc_coupon] char(40) NOT NULL DEFAULT '',

  [uc_action] char(20) NOT NULL DEFAULT '',

  [uc_stamp] datetime default NULL,

  [uc_company_id] int NOT NULL DEFAULT '0',

  [uc_contractor] int DEFAULT '0',

  [uc_branch] char(20) DEFAULT ''

) ;



INSERT INTO uctmp select * from openquery(MySQLMain,'select * from cpplEDI.uctmp');INSERT INTO Temp_tablesload values('uctmp',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.uctmp')),(select count(*) from uctmp));


Print '3'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='unknowncoupons') DROP TABLE [unknowncoupons];

CREATE TABLE [unknowncoupons] (

  [uc_coupon] char(40) NOT NULL DEFAULT '',

  [uc_action] char(20) NOT NULL DEFAULT '',

  [uc_stamp] datetime default NULL,

  [uc_company_id] int NOT NULL DEFAULT '0',

  [uc_contractor] int DEFAULT '0',

  [uc_branch] char(20) DEFAULT '',

) ;



INSERT INTO unknowncoupons select * from openquery(MySQLMain,'select * from cpplEDI.unknowncoupons');INSERT INTO Temp_tablesload values('unknowncoupons',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.unknowncoupons')),(select count(*) from unknowncoupons));


Print '4'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='unknownwork') DROP TABLE [unknownwork];

CREATE TABLE [unknownwork] (

  [uc_coupon] char(40) NOT NULL DEFAULT '',

  [uc_action] char(20) NOT NULL DEFAULT '',

  [uc_stamp] datetime default NULL,

  [uc_company_id] int NOT NULL DEFAULT '0',

  [uc_contractor] int DEFAULT '0',

  [uc_branch] char(20) DEFAULT '',

) ;



INSERT INTO unknownwork select * from openquery(MySQLMain,'select * from cpplEDI.unknownwork');INSERT INTO Temp_tablesload values('unknownwork',@@Error,(select * from openquery(MySQLMain,
'select count(*) from cpplEDI.unknownwork')),(select count(*) from unknownwork));


Print '5'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='useragents') DROP TABLE [useragents];

CREATE TABLE [useragents] (

  [user_id] int NOT NULL DEFAULT '0',

  [agent_id] int NOT NULL DEFAULT '0',

) ;



INSERT INTO useragents select * from openquery(MySQLMain,'select * from cpplEDI.useragents');INSERT INTO Temp_tablesload values('useragents',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.useragents')),
(select count(*) from useragents));


Print '6'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='userclass') DROP TABLE [userclass];

CREATE TABLE [userclass] (

  [uc_user] int NOT NULL DEFAULT '0',

  [uc_class] int NOT NULL DEFAULT '0',

  [uc_email_manifest] char(1) DEFAULT 'N',

  [uc_email] varchar(255) DEFAULT NULL,

) ;



INSERT INTO userclass select * from openquery(MySQLMain,'select * from cpplEDI.userclass');INSERT INTO Temp_tablesload values('userclass',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.userclass')),(select count(*) from userclass
));


Print '7'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='usercompany') DROP TABLE [usercompany];

CREATE TABLE [usercompany] (

  [user_id] int NOT NULL DEFAULT '0',

  [company_id] int NOT NULL DEFAULT '0',

  [email_manifest] char(1) DEFAULT 'N',

  [email] varchar(255) DEFAULT NULL,

) ;



INSERT INTO usercompany select * from openquery(MySQLMain,'select * from cpplEDI.usercompany');INSERT INTO Temp_tablesload values('usercompany',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.usercompany')),
(select count(*) from usercompany));


Print '8'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='userwarehouse') DROP TABLE [userwarehouse];

CREATE TABLE [userwarehouse] (

  [uw_user_id] int NOT NULL DEFAULT '0',

  [uw_warehouse_id] int NOT NULL DEFAULT '0',

  [uw_email_manifest] char(1) DEFAULT 'N',

  [uw_email] varchar(255) DEFAULT NULL,

) ;



INSERT INTO userwarehouse select * from openquery(MySQLMain,'select * from cpplEDI.userwarehouse');INSERT INTO Temp_tablesload values('userwarehouse',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.userwarehouse')),(select count(*
) from userwarehouse));


Print '9'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='utest') DROP TABLE [utest];

CREATE TABLE [utest] (

  [id] int  NOT NULL DEFAULT '0',

  [account] varchar(32) NOT NULL DEFAULT '',

  [password] varchar(32) NOT NULL DEFAULT '',

  [encrypted] varchar(32) NOT NULL DEFAULT '',

  [md5] char(64) DEFAULT NULL,

  [fullname] varchar(64) DEFAULT NULL,

  [email] varchar(256) DEFAULT NULL,

  [lastlogin] datetime default NULL,

  [userlevel] int DEFAULT '0',

  [flags] int DEFAULT '0',

  [pReturns] char(1) DEFAULT 'N',

  [pAgents] char(1) DEFAULT 'N',

  [pAdmin] char(1) DEFAULT 'N',

  [pPricing] char(1) DEFAULT 'N',

  [pMaintenance] char(1) DEFAULT 'N',

  [pBilling] char(1) DEFAULT 'N',

  [pTracking] char(1) DEFAULT 'N',

  [pBookin] char(1) DEFAULT 'N',

  [pFinancial] char(1) DEFAULT 'N',

  [pSupervisor] char(1) DEFAULT 'N',

  [pPODEntry] char(1) DEFAULT 'N',

  [pConnote] char(1) DEFAULT 'N',

  [pUndelivered] char(1) DEFAULT 'N',

  [pInvoiceReprint] char(1) DEFAULT 'N',

  [pSplitDeliveries] char(1) DEFAULT 'N',

  [pDaily] char(1) DEFAULT 'N',

  [pTRAP] char(1) DEFAULT 'N',

  [pMonitor] char(1) DEFAULT 'N',

  [pNoStats] char(1) DEFAULT 'N',

  [pEzyFreightConsignment] char(1) DEFAULT 'N',

  [pEzyFreightLabels] char(1) DEFAULT 'N',

  [pEzyFreightPickup] char(1) DEFAULT 'N',

  [pStaff] char(1) DEFAULT 'N',

  [pControlledReport] char(1) DEFAULT 'N',

  [fCSV] char(1) DEFAULT 'N',

  [contractor_number] int DEFAULT '0',

  [contractor_branch] int DEFAULT '0',

  [fContractor] char(1) DEFAULT 'N',

  [pEMM] char(1) DEFAULT 'N',

  [emm_branch] int DEFAULT '0',

  [pwchanged] SMALLDATETIME default NULL,

  [pwexempt] char(1) DEFAULT 'N',

  [disabled] char(1) DEFAULT 'N',

  [pAPConnote] char(1) DEFAULT 'N'

) ;



INSERT INTO utest select * from openquery(MySQLMain,'select * from cpplEDI.utest');INSERT INTO Temp_tablesload values('utest',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.utest')),(select count(*) from utest));


Print '1'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='validation') DROP TABLE [validation];

CREATE TABLE [validation] (

  [v_id] int NOT NULL,

  [v_minimum_deadweight_required] char(1) DEFAULT 'N',

  [v_minimum_deadweight_value] FLOAT(25) DEFAULT NULL,

  [v_minimum_volume_required] char(1) DEFAULT 'N',

  [v_minimum_volume_value] FLOAT(25) DEFAULT '0',

  [v_minimum_ratio_required] char(1) DEFAULT 'N',

  [v_minimum_ratio_value] FLOAT(25) DEFAULT '0',

  [v_minimum_items_required] char(1) DEFAULT 'N',

  [v_minimum_items_value] int DEFAULT '0',

  [v_maximum_deadweight_required] char(1) DEFAULT 'N',

  [v_maximum_deadweight_value] FLOAT(25) DEFAULT NULL,

  [v_maximum_volume_required] char(1) DEFAULT 'N',

  [v_maximum_volume_value] FLOAT(25) DEFAULT '0',

  [v_maximum_ratio_required] char(1) DEFAULT 'N',

  [v_maximum_ratio_value] FLOAT(25) DEFAULT '0',

  [v_maximum_items_required] char(1) DEFAULT 'N',

  [v_maximum_items_value] int DEFAULT '0',

  [v_no_suburb_validation] char(1) DEFAULT 'N',

  v_quote_check char(1) DEFAULT 'N'

  PRIMARY KEY ([v_id])

) ;





INSERT INTO [cpplEDI].[dbo].[validation] select * from openquery(MySQLMain,'select * from cpplEDI.validation');INSERT INTO Temp_tablesload values('validation',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.validation')),(select count(*) from [validation]));


Print '2'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='warehouse') DROP TABLE [warehouse];

CREATE TABLE [warehouse] (

  [w_id] int NOT NULL,

  [w_name] varchar(32) NOT NULL DEFAULT '',

  [w_description] varchar(64) DEFAULT NULL,

  PRIMARY KEY ([w_id]),

) ;



INSERT INTO warehouse select * from openquery(MySQLMain,'select * from cpplEDI.warehouse');INSERT INTO Temp_tablesload values('warehouse',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.warehouse')),(select count(*) from warehouse
));


Print '3'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='x1') DROP TABLE [x1];

CREATE TABLE [x1] (

  [ccg_pricegroup] int NOT NULL DEFAULT '0',

  [ccg_pricecode] varchar(16) NOT NULL DEFAULT '',

  [ccg_fromzone] int NOT NULL DEFAULT '0',

  [ccg_tozone] int NOT NULL DEFAULT '0',

  [ccg_present] char(1) DEFAULT 'N',

  [ccg_baseprice] FLOAT(25) DEFAULT '0',

  [ccg_15kg] FLOAT(25) DEFAULT '0',

  [ccg_25kg] FLOAT(25) DEFAULT '0',

  [ccg_pickup_payamount] FLOAT(25) DEFAULT '0',

  [ccg_pickup_15kg] FLOAT(25) DEFAULT '0',

  [ccg_pickup_25kg] FLOAT(25) DEFAULT '0',

  [ccg_deliver_payamount] FLOAT(25) DEFAULT '0',

  [ccg_deliver_15kg] FLOAT(25) DEFAULT '0',

  [ccg_deliver_25kg] FLOAT(25) DEFAULT '0'

) ;



INSERT INTO x1 select * from openquery(MySQLMain,'select * from cpplEDI.x1');INSERT INTO Temp_tablesload values('x1',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.x1')),(select count(*) from x1));


Print '4'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zonegrid') DROP TABLE [zonegrid];

CREATE TABLE [zonegrid] (

  [zp_fromzone] int NOT NULL DEFAULT '0',

  [zp_tozone] int NOT NULL DEFAULT '0',

  [zp_price] int NOT NULL DEFAULT '0',

  [zp_pricegroup_id] int NOT NULL DEFAULT '0',

  [zp_multiplier] int DEFAULT '1',

  [zp_pricecode] varchar(16) NOT NULL DEFAULT 'STD',

  PRIMARY KEY ([zp_fromzone],[zp_tozone],[zp_price],[zp_pricegroup_id],[zp_pricecode])

) ;



INSERT INTO zonegrid select * from openquery(MySQLMain,'select * from cpplEDI.zonegrid');INSERT INTO Temp_tablesload values('zonegrid',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.zonegrid')),(select count(*) from zonegrid));


Print '5'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zonemaps') DROP TABLE [zonemaps];

CREATE TABLE [zonemaps] (

  [zm_id] int NOT NULL,

  [zm_name] varchar(32) NOT NULL DEFAULT '',

  PRIMARY KEY ([zm_id])

) ;



INSERT INTO zonemaps select * from openquery(MySQLMain,'select * from cpplEDI.zonemaps');INSERT INTO Temp_tablesload values('zonemaps',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.zonemaps')),(select count(*) from zonemaps));


Print '6'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zoneprice') DROP TABLE [zoneprice];

CREATE TABLE [zoneprice] (

  [zp_zone] int NOT NULL DEFAULT '0',

  [zp_price] int NOT NULL DEFAULT '0',

  [zp_company] int NOT NULL DEFAULT '0',

  [zp_multiplier] int DEFAULT '1',

  [zp_pricecode] varchar(16) DEFAULT 'STD',

  PRIMARY KEY ([zp_zone],[zp_price],[zp_company])

) ;



INSERT INTO zoneprice select * from openquery(MySQLMain,'select * from cpplEDI.zoneprice');INSERT INTO Temp_tablesload values('zoneprice',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.zoneprice')),(select count(*) from zoneprice
));


Print '7'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zones') DROP TABLE [zones];

CREATE TABLE [zones] (

  [z_id] int NOT NULL,

  [z_code] varchar(16) DEFAULT NULL,

  [z_name] varchar(32) DEFAULT NULL,

  [z_warehouse_id] int NOT NULL DEFAULT '0',

  [z_level] char(1) DEFAULT '3',

  [z_order] int DEFAULT '1000',

  [z_color] varchar(64) DEFAULT NULL,

  PRIMARY KEY ([z_id])

) ;



INSERT INTO zones select * from openquery(MySQLMain,'select * from cpplEDI.zones');INSERT INTO Temp_tablesload values('zones',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.zones')),(select count(*) from zones));


Print '8'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zoning') DROP TABLE [zoning];

CREATE TABLE [zoning] (

  [z_id] int NOT NULL,

  [z_name] varchar(32) NOT NULL DEFAULT '',

  PRIMARY KEY ([z_id])

) ;



INSERT INTO zoning select * from openquery(MySQLMain,'select * from cpplEDI.zoning');INSERT INTO Temp_tablesload values('zoning',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.zoning')),(select count(*) from zoning));




Print '9'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='users') DROP TABLE [users];

CREATE TABLE [users] (

  [id] int  NOT NULL,

  [account] varchar(32) NOT NULL DEFAULT '',

  [salt] char(64) DEFAULT '',

  [md5] char(64) DEFAULT NULL,

  [fullname] varchar(64) DEFAULT NULL,

  [email] varchar(256) DEFAULT NULL,

  [lastlogin] datetime default NULL,

  [userlevel] int DEFAULT '0',

  [flags] int DEFAULT '0',

  [pReturns] char(1) DEFAULT 'N',

  [pAgents] char(1) DEFAULT 'N',

  [pAdmin] char(1) DEFAULT 'N',

  [pPricing] char(1) DEFAULT 'N',

  [pMaintenance] char(1) DEFAULT 'N',

  [pBilling] char(1) DEFAULT 'N',

  [pTracking] char(1) DEFAULT 'N',

  [pBookin] char(1) DEFAULT 'N',

  [pFinancial] char(1) DEFAULT 'N',

  [pSupervisor] char(1) DEFAULT 'N',

  [pPODEntry] char(1) DEFAULT 'N',

  [pConnote] char(1) DEFAULT 'N',

  [pUndelivered] char(1) DEFAULT 'N',

  [pInvoiceReprint] char(1) DEFAULT 'N',

  [pSplitDeliveries] char(1) DEFAULT 'N',

  [pDaily] char(1) DEFAULT 'N',

  [pTRAP] char(1) DEFAULT 'N',

  [pMonitor] char(1) DEFAULT 'N',

  [pNoStats] char(1) DEFAULT 'N',

  [pEzyFreightConsignment] char(1) DEFAULT 'N',

  [pEzyFreightLabels] char(1) DEFAULT 'N',

  [pEzyFreightPickup] char(1) DEFAULT 'N',

  [pEzyFreightImport] char(1) DEFAULT 'N',

  [pEzyFreightImportAddress] char(1) DEFAULT 'N',

  [pEzyFreightPricing] char(1) DEFAULT 'N',

  [pStaff] char(1) DEFAULT 'N',

  [pControlledReport] char(1) DEFAULT 'N',

  [fCSV] char(1) DEFAULT 'N',

  [contractor_number] int DEFAULT '0',

  [contractor_branch] int DEFAULT '0',

  [fContractor] char(1) DEFAULT 'N',

  [pEMM] char(1) DEFAULT 'N',

  [emm_branch] int DEFAULT '0',

  [pwchanged] SMALLDATETIME default NULL,

  [pwexempt] char(1) DEFAULT 'N',

  [disabled] char(1) DEFAULT 'N',

  [pAPConnote] char(1) DEFAULT 'N',

  [pEbay] char(1) DEFAULT 'N',

  pEditConsignments char(1) DEFAULT 'N',

  pEzyFreightItems char(1) DEFAULT 'N',

  pITAdmin char(1) DEFAULT 'N',

  pRedirect char(1) DEFAULT 'N',

  u_additional_permissions varchar(264),
 
	fCSVStyle char(1),
	loginattempts int,
	attemptstamp datetime

  PRIMARY KEY ([id]),

) ;

INSERT INTO users select  * from openquery(MySQLMain,'select * from cpplEDI.users');INSERT INTO Temp_tablesload values('users',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.users')),(select count(*) from users));


-- Added by Sinshith
Print '9'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pickups') DROP TABLE pickups;
CREATE TABLE dbo.pickups (
  p_id  int  NOT NULL, 
  p_pending char DEFAULT 'Y',
  p_style char DEFAULT 'N',
  p_cosmos_branch int DEFAULT NULL,
  p_agent int DEFAULT NULL,
  p_cosmos_code char(32) DEFAULT NULL,
  p_company int DEFAULT NULL,
  p_company_name varchar(40) DEFAULT NULL,
  p_pickup_addr0 varchar(40) DEFAULT NULL,
  p_pickup_addr1 varchar(40) DEFAULT NULL,
  p_pickup_addr2 varchar(40) DEFAULT NULL,
  p_pickup_addr3 varchar(40) DEFAULT NULL,
  p_pickup_suburb varchar(40) DEFAULT NULL,
  p_pickup_postcode int DEFAULT NULL,
  p_pickup_contact varchar(40) DEFAULT NULL,
  p_pickup_phone varchar(40) DEFAULT NULL,
  p_created_stamp datetime DEFAULT '0000-00-00 00:00:00',
  p_updated_stamp datetime DEFAULT '0000-00-00 00:00:00',
  p_release_stamp datetime DEFAULT '0000-00-00 00:00:00',
  p_sent_stamp datetime DEFAULT '0000-00-00 00:00:00',
  p_confirmed_stamp datetime DEFAULT '0000-00-00 00:00:00',
  p_consignments int DEFAULT NULL,
  p_items int DEFAULT NULL,
  p_weight int DEFAULT NULL,
  p_volume [float] DEFAULT NULL,
  p_cosmos_date date DEFAULT '0000-00-00',
  p_cosmos_job int DEFAULT '0',
  p_accept_stamp datetime DEFAULT '0000-00-00 00:00:00',
 p_pickup_stamp datetime DEFAULT '0000-00-00 00:00:00',
  p_futile_stamp datetime DEFAULT '0000-00-00 00:00:00'
 PRIMARY KEY CLUSTERED 
(
	p_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO pickups select * from openquery(MySQLMain,'select * from cpplEDI.pickups');INSERT INTO Temp_tablesload values('pickups',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.pickups')),(select count(*) from pickups));

Print '1'
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pickupconsignment') DROP TABLE pickupconsignment;

CREATE TABLE dbo.pickupconsignment (
  pc_id int  NOT NULL, 
  pc_pickup int DEFAULT NULL,
  pc_consignment int DEFAULT NULL,
  pc_connote varchar(32) DEFAULT NULL,
  pc_reference varchar(32) DEFAULT NULL,
  pc_delivery_addr0 varchar(40) DEFAULT NULL,
  pc_delivery_addr1 varchar(40) DEFAULT NULL,
  pc_delivery_addr2 varchar(40) DEFAULT NULL,
  pc_delivery_addr3 varchar(40) DEFAULT NULL,
  pc_delivery_suburb varchar(40) DEFAULT NULL,
  pc_delivery_postcode int DEFAULT NULL,
  pc_delivery_contact varchar(40) DEFAULT NULL,
  pc_delivery_phone varchar(40) DEFAULT NULL,
  pc_items int DEFAULT NULL,
  pc_weight int DEFAULT NULL,
  pc_volume float DEFAULT NULL
  PRIMARY KEY CLUSTERED 
(
	pc_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO pickupconsignment select * from openquery(MySQLMain,'select * from cpplEDI.pickupconsignment');INSERT INTO Temp_tablesload values('pickupconsignment',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.pickupconsignment')),(select count(*) from pickupconsignment));





end




GO
