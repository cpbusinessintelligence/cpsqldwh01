SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO















CREATE proc [dbo].[cpplEDI_IncrementalTablesLoad_Consignment_cdCoupon_bkup_AH_20210314] as
 begin

DECLARE @RowsDeleted INTEGER
SET @RowsDeleted = 1

-------------Loading consignment table---------------------

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='consignment_Incrementalload') DROP TABLE consignment_Incrementalload;
SELECT * INTO CpplEDI.dbo.consignment_Incrementalload
FROM openquery(MySQLMain, 'SELECT * FROM cpplEDI.consignment where cd_date>=NOW() - INTERVAL 2 MONTH;')


WHILE (@RowsDeleted > 0)
    BEGIN
        -- delete 10,000 rows a time
        DELETE TOP (10000) FROM consignment where cd_date >=dateadd(mm,-2,getdate())
        SET @RowsDeleted = @@ROWCOUNT
    END

MERGE Consignment AS cd
USING (select * from consignment_Incrementalload where [cd_consignment_date] <> '7068-11-25' 
--and [cd_consignment_date] <> '7665-07-06' and [cd_consignment_date] <> '7665-07-07' --added by PV on 2020-07-07
and Year(cd_consignment_date) > 1950 and Year(cd_consignment_date) < 2050  --added by PV on 2020-08-09
and [cd_connote] not in ('CPBXDZC1007304',
'CPBXDZC1007303','ï»¿CPBXM4ZW00072139')) AS cs ON cs.cd_id=cd.cd_id
when matched then update set 
       [cd_id] = cs.cd_id
      ,[cd_company_id] =cs.cd_company_id
      ,[cd_account] =cs.cd_account
      ,[cd_agent_id] =cs.cd_agent_id
      ,[cd_import_id] =cs.cd_import_id
      ,[cd_ogm_id] =cs.cd_ogm_id
      ,[cd_manifest_id] =cs.cd_manifest_id
      ,[cd_connote] =cs.cd_connote      
	  ,[cd_date] = convert(smalldatetime,IsNull( cs.cd_date,'1900-01-01'))
      ,[cd_consignment_date] = Convert(smalldatetime,replace( cs.cd_consignment_date,'1753-01-01','1900-01-01'))
      ,[cd_eta_date] = convert(smalldatetime,IsNull( cs.cd_eta_date,'1900-01-01'))
      ,[cd_eta_earliest] = convert(smalldatetime,IsNull( cs.cd_eta_earliest,'1900-01-01'))
      ,[cd_customer_eta] =  convert(smalldatetime,IsNull( cs.cd_customer_eta,'1900-01-01'))     
      ,[cd_pickup_addr0] =cs.cd_pickup_addr0
      ,[cd_pickup_addr1] =cs.cd_pickup_addr1
      ,[cd_pickup_addr2] =cs.cd_pickup_addr2
      ,[cd_pickup_addr3] =cs.cd_pickup_addr3
      ,[cd_pickup_suburb] =cs.cd_pickup_suburb
      ,[cd_pickup_postcode] =cs.cd_pickup_postcode
      ,[cd_pickup_record_no] =cs.cd_pickup_record_no
      ,[cd_pickup_confidence] =cs.cd_pickup_confidence
      ,[cd_pickup_contact] =cs.cd_pickup_contact
      ,[cd_pickup_contact_phone] =cs.cd_pickup_contact_phone
      ,[cd_delivery_addr0] =cs.cd_delivery_addr0
      ,[cd_delivery_addr1] =cs.cd_delivery_addr1
      ,[cd_delivery_addr2] =cs.cd_delivery_addr2
      ,[cd_delivery_addr3] =cs.cd_delivery_addr3
      ,[cd_delivery_email] =cs.cd_delivery_email
      ,[cd_delivery_suburb] =cs.cd_delivery_suburb
      ,[cd_delivery_postcode] =cs.cd_delivery_postcode
      ,[cd_delivery_record_no] =cs.cd_delivery_record_no
      ,[cd_delivery_confidence] =cs.cd_delivery_confidence
      ,[cd_delivery_contact] =cs.cd_delivery_contact
      ,[cd_delivery_contact_phone] =cs.cd_delivery_contact_phone
      ,[cd_special_instructions] =cs.cd_special_instructions
      ,[cd_stats_branch] =cs.cd_stats_branch
      ,[cd_stats_depot] =cs.cd_stats_depot
      ,[cd_pickup_branch] =cs.cd_pickup_branch
      ,[cd_pickup_pay_branch] =cs.cd_pickup_pay_branch
      ,[cd_deliver_branch] =cs.cd_deliver_branch
      ,[cd_deliver_pay_branch] =cs.cd_deliver_pay_branch
      ,[cd_special_driver] =cs.cd_special_driver
      ,[cd_pickup_revenue] =cs.cd_pickup_revenue
      ,[cd_deliver_revenue] =cs.cd_deliver_revenue
      ,[cd_pickup_billing] =cs.cd_pickup_billing
      ,[cd_deliver_billing] =cs.cd_deliver_billing
      ,[cd_pickup_charge] =cs.cd_pickup_charge
      ,[cd_pickup_charge_actual] =cs.cd_pickup_charge_actual
      ,[cd_deliver_charge] =cs.cd_deliver_charge
      ,[cd_deliver_payment_actual] =cs.cd_deliver_payment_actual
      ,[cd_pickup_payment] =cs.cd_pickup_payment
      ,[cd_pickup_payment_actual] =cs.cd_pickup_payment_actual
      ,[cd_deliver_payment] =cs.cd_deliver_payment
      ,[cd_deliver_charge_actual] =cs.cd_deliver_charge_actual
      ,[cd_special_payment] =cs.cd_special_payment
      ,[cd_insurance_billing] =cs.cd_insurance_billing
      ,[cd_items] =cs.cd_items
      ,[cd_coupons] =cs.cd_coupons
      ,[cd_references] =cs.cd_references
      ,[cd_rating_id] =cs.cd_rating_id
      ,[cd_chargeunits] =cs.cd_chargeunits
      ,[cd_deadweight] =cs.cd_deadweight
      ,[cd_dimension0] =cs.cd_dimension0
      ,[cd_dimension1] =cs.cd_dimension1
      ,[cd_dimension2] =cs.cd_dimension2
      ,[cd_volume] =cs.cd_volume
      ,[cd_volume_automatic] =cs.cd_volume_automatic
      ,[cd_import_deadweight] =cs.cd_import_deadweight
      ,[cd_import_volume] =cs.cd_import_volume
      ,[cd_measured_deadweight] =cs.cd_measured_deadweight
      ,[cd_measured_volume] =cs.cd_measured_volume
      ,[cd_billing_id] =cs.cd_billing_id     
	  ,[cd_billing_date] =convert(smalldatetime,IsNull(  cs.cd_billing_date,'1900-01-01'))
      ,[cd_export_id] =cs.cd_export_id
      ,[cd_export2_id] =cs.cd_export2_id
	  ,[cd_pickup_pay_date] =convert(smalldatetime,IsNull( cs.cd_pickup_pay_date,'1900-01-01') )
      ,[cd_delivery_pay_date] =convert(smalldatetime,IsNull( cs.cd_delivery_pay_date,'1900-01-01') ) 
      ,[cd_transfer_pay_date] =convert(smalldatetime,IsNull(  cs.cd_transfer_pay_date,'1900-01-01'))     
      ,[cd_activity_stamp] =cs.cd_activity_stamp
      ,[cd_activity_driver] =cs.cd_activity_driver
      ,[cd_pickup_stamp] =cs.cd_pickup_stamp
      ,[cd_pickup_driver] =cs.cd_pickup_driver
      ,[cd_pickup_pay_driver] =cs.cd_pickup_pay_driver
      ,[cd_pickup_count] =cs.cd_pickup_count
      ,[cd_pickup_notified] =cs.cd_pickup_notified
      ,[cd_accept_stamp] =cs.cd_accept_stamp
      ,[cd_accept_driver] =cs.cd_accept_driver
      ,[cd_accept_count] =cs.cd_accept_count
      ,[cd_accept_notified] =cs.cd_accept_notified
      ,[cd_indepot_notified] =cs.cd_indepot_notified
      ,[cd_indepot_stamp] =cs.cd_indepot_stamp
      ,[cd_indepot_driver] =cs.cd_indepot_driver
      ,[cd_indepot_count] =cs.cd_indepot_count
      ,[cd_transfer_notified] =cs.cd_transfer_notified
      ,[cd_failed_stamp] =cs.cd_failed_stamp
      ,[cd_failed_driver] =cs.cd_failed_driver
      ,[cd_deliver_stamp] =cs.cd_deliver_stamp
      ,[cd_deliver_driver] =cs.cd_deliver_driver
      ,[cd_deliver_pay_driver] =cs.cd_deliver_pay_driver
      ,[cd_deliver_count] =cs.cd_deliver_count
      ,[cd_deliver_pod] =cs.cd_deliver_pod
      ,[cd_deliver_notified] =cs.cd_deliver_notified
      ,[cd_pickup_pay_notified] =cs.cd_pickup_pay_notified
      ,[cd_deliver_pay_notified] =cs.cd_deliver_pay_notified
      ,[cd_printed] =cs.cd_printed
      ,[cd_returns] =cs.cd_returns
      ,[cd_release] =cs.cd_release
      ,[cd_release_stamp] =cs.cd_release_stamp
      ,[cd_pricecode] =cs.cd_pricecode
      ,[cd_insurance] =cs.cd_insurance
      ,[cd_pickup_agent] =cs.cd_pickup_agent
      ,[cd_delivery_agent] =cs.cd_delivery_agent
      ,[cd_agent_pod] =cs.cd_agent_pod
      ,[cd_agent_pod_desired] =cs.cd_agent_pod_desired
      ,[cd_agent_pod_name] =cs.cd_agent_pod_name
      ,[cd_agent_pod_stamp] =cs.cd_agent_pod_stamp
      ,[cd_agent_pod_entry] =cs.cd_agent_pod_entry
      ,[cd_completed] =cs.cd_completed
      ,[cd_cancelled] =cs.cd_cancelled
      ,[cd_cancelled_stamp] =cs.cd_cancelled_stamp
      ,[cd_cancelled_by] =cs.cd_cancelled_by
      ,[cd_test] =cs.cd_test
      ,[cd_dirty] =cs.cd_dirty
      ,[cd_transfer_stamp] =cs.cd_transfer_stamp
      ,[cd_transfer_driver] =cs.cd_transfer_driver
      ,[cd_transfer_count] =cs.cd_transfer_count
      ,[cd_transfer_to] =cs.cd_transfer_to
      ,[cd_toagent_notified] =cs.cd_toagent_notified
      ,[cd_toagent_stamp] =cs.cd_toagent_stamp
      ,[cd_toagent_driver] =cs.cd_toagent_driver
      ,[cd_toagent_count] =cs.cd_toagent_count
      ,[cd_toagent_name] =cs.cd_toagent_name
      ,[cd_last_status] =cs.cd_last_status
      ,[cd_last_notified] =cs.cd_last_notified
      ,[cd_last_info] =cs.cd_last_info
      ,[cd_last_driver] =cs.cd_last_driver
      ,[cd_last_stamp] =cs.cd_last_stamp
      ,[cd_last_count] =cs.cd_last_count
      ,[cd_accept_driver_branch] =cs.cd_accept_driver_branch
      ,[cd_activity_driver_branch] =cs.cd_activity_driver_branch
      ,[cd_deliver_driver_branch] =cs.cd_deliver_driver_branch
      ,[cd_deliver_pay_driver_branch] =cs.cd_deliver_pay_driver_branch
      ,[cd_failed_driver_branch] =cs.cd_failed_driver_branch
      ,[cd_indepot_driver_branch] =cs.cd_indepot_driver_branch
      ,[cd_last_driver_branch] =cs.cd_last_driver_branch
      ,[cd_pickup_driver_branch] =cs.cd_pickup_driver_branch
      ,[cd_pickup_pay_driver_branch] =cs.cd_pickup_pay_driver_branch
      ,[cd_special_driver_branch] =cs.cd_special_driver_branch
      ,[cd_toagent_driver_branch] =cs.cd_toagent_driver_branch
      ,[cd_transfer_driver_branch] =cs.cd_transfer_driver_branch
														
when not matched then INSERT values(cs.cd_id
           ,cs.cd_company_id
           ,cs.[cd_account]
           ,cs.[cd_agent_id]
           ,cs.[cd_import_id]
           ,cs.[cd_ogm_id]
           ,cs.[cd_manifest_id]
           ,cs.[cd_connote]
		   ,convert(smalldatetime,IsNull( cs.[cd_date],'1900-01-01'))
           ,Convert(smalldatetime,replace( cs.cd_consignment_date,'1753-01-01','1900-01-01'))
           ,convert(smalldatetime,IsNull( cs.[cd_eta_date],'1900-01-01'))
           ,convert(smalldatetime,IsNull( cs.[cd_eta_earliest],'1900-01-01'))
           ,convert(smalldatetime,IsNull( cs.[cd_customer_eta],'1900-01-01'))           
           ,cs.[cd_pickup_addr0]
           ,cs.[cd_pickup_addr1]
           ,cs.[cd_pickup_addr2]
           ,cs.[cd_pickup_addr3]
           ,cs.[cd_pickup_suburb]
           ,cs.[cd_pickup_postcode]
           ,cs.[cd_pickup_record_no]
           ,cs.[cd_pickup_confidence]
           ,cs.[cd_pickup_contact]
           ,cs.[cd_pickup_contact_phone]
           ,cs.[cd_delivery_addr0]
           ,cs.[cd_delivery_addr1]
           ,cs.[cd_delivery_addr2]
           ,cs.[cd_delivery_addr3]
           ,cs.[cd_delivery_email]
           ,cs.[cd_delivery_suburb]
           ,cs.[cd_delivery_postcode]
           ,cs.[cd_delivery_record_no]
           ,cs.[cd_delivery_confidence]
           ,cs.[cd_delivery_contact]
           ,cs.[cd_delivery_contact_phone]
           ,cs.[cd_special_instructions]
           ,cs.[cd_stats_branch]
           ,cs.[cd_stats_depot]
           ,cs.[cd_pickup_branch]
           ,cs.[cd_pickup_pay_branch]
           ,cs.[cd_deliver_branch]
           ,cs.[cd_deliver_pay_branch]
           ,cs.[cd_special_driver]
           ,cs.[cd_pickup_revenue]
           ,cs.[cd_deliver_revenue]
           ,cs.[cd_pickup_billing]
           ,cs.[cd_deliver_billing]
           ,cs.[cd_pickup_charge]
           ,cs.[cd_pickup_charge_actual]
           ,cs.[cd_deliver_charge]
           ,cs.[cd_deliver_payment_actual]
           ,cs.[cd_pickup_payment]
           ,cs.[cd_pickup_payment_actual]
           ,cs.[cd_deliver_payment]
           ,cs.[cd_deliver_charge_actual]
           ,cs.[cd_special_payment]
           ,cs.[cd_insurance_billing]
           ,cs.[cd_items]
           ,cs.[cd_coupons]
           ,cs.[cd_references]
           ,cs.[cd_rating_id]
           ,cs.[cd_chargeunits]
           ,cs.[cd_deadweight]
           ,cs.[cd_dimension0]
           ,cs.[cd_dimension1]
           ,cs.[cd_dimension2]
           ,cs.[cd_volume]
           ,cs.[cd_volume_automatic]
           ,cs.[cd_import_deadweight]
           ,cs.[cd_import_volume]
           ,cs.[cd_measured_deadweight]
           ,cs.[cd_measured_volume]
           ,cs.[cd_billing_id]
		   ,convert(smalldatetime,IsNull( cs.[cd_billing_date],'1900-01-01'))
           ,cs.[cd_export_id]
           ,cs.[cd_export2_id] 
           ,convert(smalldatetime,IsNull( cs.cd_pickup_pay_date,'1900-01-01'))
           ,convert(smalldatetime,IsNull( cs.[cd_delivery_pay_date],'1900-01-01'))
           ,convert(smalldatetime,IsNull( cs.[cd_transfer_pay_date],'1900-01-01'))           
           ,cs.[cd_activity_stamp]
           ,cs.[cd_activity_driver]
           ,cs.[cd_pickup_stamp]
           ,cs.[cd_pickup_driver]
           ,cs.[cd_pickup_pay_driver]
           ,cs.[cd_pickup_count]
           ,cs.[cd_pickup_notified]
           ,cs.[cd_accept_stamp]
           ,cs.[cd_accept_driver]
           ,cs.[cd_accept_count]
           ,cs.[cd_accept_notified]
           ,cs.[cd_indepot_notified]
           ,cs.[cd_indepot_stamp]
           ,cs.[cd_indepot_driver]
           ,cs.[cd_indepot_count]
           ,cs.[cd_transfer_notified]
           ,cs.[cd_failed_stamp]
           ,cs.[cd_failed_driver]
           ,cs.[cd_deliver_stamp]
           ,cs.[cd_deliver_driver]
           ,cs.[cd_deliver_pay_driver]
           ,cs.[cd_deliver_count]
           ,cs.[cd_deliver_pod]
           ,cs.[cd_deliver_notified]
           ,cs.[cd_pickup_pay_notified]
           ,cs.[cd_deliver_pay_notified]
           ,cs.[cd_printed]
           ,cs.[cd_returns]
           ,cs.[cd_release]
           ,cs.[cd_release_stamp]
           ,cs.[cd_pricecode]
           ,cs.[cd_insurance]
           ,cs.[cd_pickup_agent]
           ,cs.[cd_delivery_agent]
           ,cs.[cd_agent_pod]
           ,cs.[cd_agent_pod_desired]
           ,cs.[cd_agent_pod_name]
           ,cs.[cd_agent_pod_stamp]
           ,cs.[cd_agent_pod_entry]
           ,cs.[cd_completed]
           ,cs.[cd_cancelled]
           ,cs.[cd_cancelled_stamp]
           ,cs.[cd_cancelled_by]
           ,cs.[cd_test]
           ,cs.[cd_dirty]
           ,cs.[cd_transfer_stamp]
           ,cs.[cd_transfer_driver]
           ,cs.[cd_transfer_count]
           ,cs.[cd_transfer_to]
           ,cs.[cd_toagent_notified]
           ,cs.[cd_toagent_stamp]
           ,cs.[cd_toagent_driver]
           ,cs.[cd_toagent_count]
           ,cs.[cd_toagent_name]
           ,cs.[cd_last_status]
           ,cs.[cd_last_notified]
           ,cs.[cd_last_info]
           ,cs.[cd_last_driver]
           ,cs.[cd_last_stamp]
           ,cs.[cd_last_count]
           ,cs.[cd_accept_driver_branch]
           ,cs.[cd_activity_driver_branch]
           ,cs.[cd_deliver_driver_branch]
           ,cs.[cd_deliver_pay_driver_branch]
           ,cs.[cd_failed_driver_branch]
           ,cs.[cd_indepot_driver_branch]
           ,cs.[cd_last_driver_branch]
           ,cs.[cd_pickup_driver_branch]
           ,cs.[cd_pickup_pay_driver_branch]
           ,cs.[cd_special_driver_branch]
           ,cs.[cd_toagent_driver_branch]
           ,cs.[cd_transfer_driver_branch]);

INSERT INTO Temp_tablesload values('consignment',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.consignment')),(select count(*) from consignment));
------------------Loading cdCoupon Table----------

			
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdcoupon_Incrementalload') DROP TABLE cdcoupon_Incrementalload;
SELECT * INTO CpplEDI.dbo.cdcoupon_Incrementalload
FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.cdcoupon cd  inner join (cpplEDI.consignment cs) on ( cd.cc_consignment=cs.cd_id ) where cd_date>=NOW() - INTERVAL 1 MONTH ;')

--DECLARE @RowsDeleted INTEGER
set @RowsDeleted=1
WHILE (@RowsDeleted > 0)
    BEGIN
        -- delete 10,000 rows a time
        DELETE TOP (10000) cd  FROM cdcoupon cd  inner join consignment cs on  cd.cc_consignment=cs.cd_id where cd_date >=dateadd(mm,-1,getdate())
        SET @RowsDeleted = @@ROWCOUNT
    END


MERGE cdcoupon AS cd
USING (select * from cdcoupon_Incrementalload) AS cs ON cs.cc_id=cd.cc_id
when matched then UPDATE
   SET [cc_id] =cs.cc_id
      ,[cc_company_id] =cs.cc_company_id
      ,[cc_consignment] =cs.cc_consignment
      ,[cc_coupon] =cs.cc_coupon
      ,[cc_activity_stamp] =cs.cc_activity_stamp
      ,[cc_pickup_stamp] =cs.cc_pickup_stamp
      ,[cc_accept_stamp] =cs.cc_accept_stamp
      ,[cc_indepot_stamp] =cs.cc_indepot_stamp
      ,[cc_transfer_stamp] =cs.cc_transfer_stamp
      ,[cc_deliver_stamp] =cs.cc_deliver_stamp
      ,[cc_failed_stamp] =cs.cc_failed_stamp
      ,[cc_activity_driver] =cs.cc_activity_driver
      ,[cc_pickup_driver] =cs.cc_pickup_driver
      ,[cc_accept_driver] =cs.cc_accept_driver
      ,[cc_indepot_driver] =cs.cc_indepot_driver
      ,[cc_transfer_driver] =cs.cc_transfer_driver
      ,[cc_transfer_to] =cs.cc_transfer_to
      ,[cc_toagent_driver] =cs.cc_toagent_driver
      ,[cc_toagent_stamp] =cs.cc_toagent_stamp
      ,[cc_toagent_name] =cs.cc_toagent_name
      ,[cc_deliver_driver] =cs.cc_deliver_driver
      ,[cc_failed_driver] =cs.cc_failed_driver
      ,[cc_deliver_pod] =cs.cc_deliver_pod
      ,[cc_failed] =cs.cc_failed
      ,[cc_exception_stamp] =cs.cc_exception_stamp
      ,[cc_exception_code] =cs.cc_exception_code
      ,[cc_unit_type] =cs.cc_unit_type
      ,[cc_internal] =cs.cc_internal
      ,[cc_link_coupon] =cs.cc_link_coupon
      ,[cc_dirty] =cs.cc_dirty
      ,[cc_last_status] =cs.cc_last_status
      ,[cc_last_driver] =cs.cc_last_driver
      ,[cc_last_stamp] =cs.cc_last_stamp
      ,[cc_last_info] =cs.cc_last_info
      ,[cc_accept_driver_branch] =cs.cc_accept_driver_branch
      ,[cc_activity_driver_branch] =cs.cc_activity_driver_branch
      ,[cc_deliver_driver_branch] =cs.cc_deliver_driver_branch
      ,[cc_failed_driver_branch] =cs.cc_failed_driver_branch
      ,[cc_indepot_driver_branch] =cs.cc_indepot_driver_branch
      ,[cc_last_driver_branch] =cs.cc_last_driver_branch
      ,[cc_pickup_driver_branch] =cs.cc_pickup_driver_branch
      ,[cc_toagent_driver_branch] =cs.cc_toagent_driver_branch
      ,[cc_tranfer_driver_branch] =cs.cc_tranfer_driver_branch

when not matched then INSERT values(cs.[cc_id]
           ,cs.[cc_company_id]
           ,cs.[cc_consignment]
           ,cs.[cc_coupon]
           ,cs.[cc_activity_stamp]
           ,cs.[cc_pickup_stamp]
           ,cs.[cc_accept_stamp]
           ,cs.[cc_indepot_stamp]
           ,cs.[cc_transfer_stamp]
           ,cs.[cc_deliver_stamp]
           ,cs.[cc_failed_stamp]
           ,cs.[cc_activity_driver]
           ,cs.[cc_pickup_driver]
           ,cs.[cc_accept_driver]
           ,cs.[cc_indepot_driver]
           ,cs.[cc_transfer_driver]
           ,cs.[cc_transfer_to]
           ,cs.[cc_toagent_driver]
           ,cs.[cc_toagent_stamp]
           ,cs.[cc_toagent_name]
           ,cs.[cc_deliver_driver]
           ,cs.[cc_failed_driver]
           ,cs.[cc_deliver_pod]
           ,cs.[cc_failed]
           ,cs.[cc_exception_stamp]
           ,cs.[cc_exception_code]
           ,cs.[cc_unit_type]
           ,cs.[cc_internal]
           ,cs.[cc_link_coupon]
           ,cs.[cc_dirty]
           ,cs.[cc_last_status]
           ,cs.[cc_last_driver]
           ,cs.[cc_last_stamp]
           ,cs.[cc_last_info]
           ,cs.[cc_accept_driver_branch]
           ,cs.[cc_activity_driver_branch]
           ,cs.[cc_deliver_driver_branch]
           ,cs.[cc_failed_driver_branch]
           ,cs.[cc_indepot_driver_branch]
           ,cs.[cc_last_driver_branch]
           ,cs.[cc_pickup_driver_branch]
           ,cs.[cc_toagent_driver_branch]
           ,cs.[cc_tranfer_driver_branch]);

INSERT INTO Temp_tablesload values('cdcoupon',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdcoupon cd  inner join (cpplEDI.consignment cs) on ( cd.cc_consignment=cs.cd_id )')),(select count(*) from cdcoupon inner join consignment  on cc_consignment=cd_id ));


--Below set of code is moved to a separate SP hence commented - by PV on 2020-08-25
/*
-----Loading cdref table-----------------
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdref_load') DROP TABLE [cdref_load];
SELECT * INTO CpplEDI.dbo.cdref_load
FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.cdref cd  inner join (cpplEDI.consignment cs) on ( cd.cr_consignment=cs.cd_id ) where cd_date>=NOW() - INTERVAL 2 MONTH;')

--DECLARE @RowsDeleted INTEGER
set @RowsDeleted=1
WHILE (@RowsDeleted > 0)
    BEGIN
        -- delete 10,000 rows a time
        DELETE TOP (10000) cd  FROM cdref cd  inner join 
		
		consignment cs on  cd.cr_consignment=cs.cd_id where cd_date >=dateadd(mm,-2,getdate())
        SET @RowsDeleted = @@ROWCOUNT
    END



MERGE cdref AS cd
USING (select * from cdref_load) AS cs ON cs.cr_id=cd.cr_id
when matched then UPDATE 
SET [cr_id] =cs.cr_id
    ,[cr_company_id] =cs.cr_company_id
    ,[cr_consignment] =cs.cr_consignment
    ,[cr_reference] =cs.cr_reference
when not matched then INSERT values([cr_id]
                                   ,[cr_company_id]
                                   ,[cr_consignment]
                                   ,[cr_reference]);


INSERT INTO Temp_tablesload values('cdref',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdref cd  inner join (cpplEDI.consignment cs) on ( cd.cr_consignment=cs.cd_id ) ')),(select count(*) from cdref inner join consignment  on cr_consignment=cd_id ));
*/
--Code commented by PV ends here - on 2020-08-25


--Below set of code is moved to a separate SP hence commented - by PV on 2020-08-11
/*
-------------Loading cdreweigh table------------------
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdreweigh_load') DROP TABLE [cdreweigh_load];
SELECT * INTO CpplEDI.dbo.cdreweigh_load
FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.cdreweigh cd  where cr_stamp>=NOW() - INTERVAL 2 MONTH ;')

MERGE cdreweigh AS cd
USING (select * from cdreweigh_load) AS cs ON cs.[cr_consignment]=cd.[cr_consignment]
when matched then UPDATE 
SET [cr_consignment] =cs.cr_consignment
    ,[cr_stamp] =cs.cr_stamp
    ,[cr_deadweight] =cs.cr_deadweight
    ,[cr_volume] =cs.cr_volume
    ,[cr_chargeweight] =cs.cr_chargeweight
when not matched then INSERT values([cr_consignment] 
    ,[cr_stamp]
    ,[cr_deadweight]
    ,[cr_volume]
    ,[cr_chargeweight]);


INSERT INTO Temp_tablesload values('cdreweigh',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdreweigh')),(select count(*) from cdreweigh));

-------------Loading ccreweigh table------------------
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ccreweigh_load') DROP TABLE [ccreweigh_load];
SELECT * INTO CpplEDI.dbo.ccreweigh_load
FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.ccreweigh cd  where ccr_stamp>=NOW() - INTERVAL 2 MONTH ;')

MERGE ccreweigh AS cd
USING (select * from ccreweigh_load) AS cs ON cs.[ccr_coupon_id]=cd.[ccr_coupon_id]
when matched then UPDATE 
SET [ccr_coupon] = cs.ccr_coupon
,[ccr_coupon_id] = cs.ccr_coupon_id
,[ccr_stamp] = cs.ccr_stamp
,[ccr_location] = cs.ccr_location
,[ccr_deadweight] = cs.ccr_deadweight
,[ccr_dimension0] = cs.ccr_dimension0
,[ccr_dimension1] = cs.ccr_dimension1
,[ccr_dimension2] = cs.ccr_dimension2
,[ccr_volume] = cs.ccr_volume
when not matched then INSERT values([ccr_coupon]
           ,[ccr_coupon_id]
           ,[ccr_stamp]
           ,[ccr_location]
           ,[ccr_deadweight]
           ,[ccr_dimension0]
           ,[ccr_dimension1]
           ,[ccr_dimension2]
           ,[ccr_volume]);


INSERT INTO Temp_tablesload values('ccreweigh',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ccreweigh')),(select count(*) from ccreweigh));
*/
--Code commented by PV ends here - on 2020-08-11


------------------Loading all other tables-----------
------exec [dbo].[cpplEDI_FullTablesLoad]

-----Create Indexes and Foreign Key Constraints------
------exec [dbo].[cpplEDI_Create_FK_Constraints]

end

--exec [dbo].[cpplEDI_Drop_FK_Constraints]

--exec [dbo].[cpplEDI_IncrementalTablesLoad]








GO
