SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[cpplEDI_IncrementalTablesLoad_cdRef] as
BEGIN

	-----Loading cdref table-----------------
	DECLARE @RowsDeleted INTEGER
	SET @RowsDeleted = 1

	IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdref_load') DROP TABLE [cdref_load];
	SELECT * INTO CpplEDI.dbo.cdref_load
	FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.cdref cd  inner join (cpplEDI.consignment cs) on ( cd.cr_consignment=cs.cd_id ) where cd_date>=NOW() - INTERVAL 1 MONTH;')

	--DECLARE @RowsDeleted INTEGER
	SET @RowsDeleted=1
	WHILE (@RowsDeleted > 0)
    BEGIN
        -- delete 10,000 rows a time
        DELETE TOP (10000) cd  
		FROM cdref cd  
			INNER JOIN consignment cs on  cd.cr_consignment=cs.cd_id where cd_date >=dateadd(mm,-1,getdate())
        
		SET @RowsDeleted = @@ROWCOUNT
    END

	MERGE cdref AS cd
	USING (select * from cdref_load) AS cs ON cs.cr_id=cd.cr_id
	WHEN MATCHED THEN 
		UPDATE 
			SET [cr_id] =cs.cr_id
				,[cr_company_id] =cs.cr_company_id
				,[cr_consignment] =cs.cr_consignment
				,[cr_reference] =cs.cr_reference
	WHEN NOT MATCHED THEN 
		INSERT values([cr_id]
                    ,[cr_company_id]
                    ,[cr_consignment]
                    ,[cr_reference]);

	INSERT INTO Temp_tablesload values('cdref',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdref cd  inner join (cpplEDI.consignment cs) on ( cd.cr_consignment=cs.cd_id ) ')),(select count(*) from cdref inner join consignment  on cr_consignment=cd_id ));

END

GO
