SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-08-11
-- Description:	Moved this set of code from cpplEDI_IncrementalTablesLoad, to load reweigh data even if Conginment table load fails. 
-- =============================================
CREATE PROCEDURE [dbo].[cpplEDI_ReWeigh_IncrementalTablesLoad]
AS
BEGIN
	
	SET NOCOUNT ON;

    IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdreweigh_load') DROP TABLE [cdreweigh_load];
	SELECT * INTO CpplEDI.dbo.cdreweigh_load
	FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.cdreweigh cd  where cr_stamp>=NOW() - INTERVAL 2 MONTH ;')

	MERGE cdreweigh AS cd
	USING (SELECT * FROM cdreweigh_load) AS cs ON cs.[cr_consignment]=cd.[cr_consignment]
	WHEN MATCHED THEN UPDATE 
	SET [cr_consignment] =cs.cr_consignment
		,[cr_stamp] =cs.cr_stamp
		,[cr_deadweight] =cs.cr_deadweight
		,[cr_volume] =cs.cr_volume
		,[cr_chargeweight] =cs.cr_chargeweight
	WHEN NOT MATCHED THEN INSERT VALUES([cr_consignment] 
		,[cr_stamp]
		,[cr_deadweight]
		,[cr_volume]
		,[cr_chargeweight]);

	INSERT INTO Temp_tablesload values('cdreweigh',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdreweigh')),(select count(*) from cdreweigh));
	
	-------------Loading ccreweigh table------------------
	IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ccreweigh_load') DROP TABLE [ccreweigh_load];
	SELECT * INTO CpplEDI.dbo.ccreweigh_load
	FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.ccreweigh cd  where ccr_stamp>=NOW() - INTERVAL 2 MONTH ;')

	MERGE ccreweigh AS cd
	USING (SELECT * FROM ccreweigh_load) AS cs ON cs.[ccr_coupon_id]=cd.[ccr_coupon_id]
	WHEN MATCHED THEN UPDATE 
	SET [ccr_coupon] = cs.ccr_coupon
	,[ccr_coupon_id] = cs.ccr_coupon_id
	,[ccr_stamp] = cs.ccr_stamp
	,[ccr_location] = cs.ccr_location
	,[ccr_deadweight] = cs.ccr_deadweight
	,[ccr_dimension0] = cs.ccr_dimension0
	,[ccr_dimension1] = cs.ccr_dimension1
	,[ccr_dimension2] = cs.ccr_dimension2
	,[ccr_volume] = cs.ccr_volume
	WHEN NOT MATCHED THEN INSERT VALUES([ccr_coupon]
			   ,[ccr_coupon_id]
			   ,[ccr_stamp]
			   ,[ccr_location]
			   ,[ccr_deadweight]
			   ,[ccr_dimension0]
			   ,[ccr_dimension1]
			   ,[ccr_dimension2]
			   ,[ccr_volume]);

	INSERT INTO Temp_tablesload values('ccreweigh',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ccreweigh')),(select count(*) from ccreweigh));
------------------

END
GO
