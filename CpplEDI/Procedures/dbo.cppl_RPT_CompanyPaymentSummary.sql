SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[cppl_RPT_CompanyPaymentSummary]-- 3

AS
BEGIN
Declare @CurrentMonth As Integer = (Datepart(year,Getdate()) *100)+ Datepart(Month,Getdate())
Declare @Month_1 As Integer =  (Datepart(year,Dateadd(Month,-1,Getdate())) *100)+ Datepart(Month,Dateadd(Month,-1,Getdate()))
Declare @Month_2 As Integer =  (Datepart(year,Dateadd(Month,-2,Getdate())) *100)+ Datepart(Month,Dateadd(Month,-2,Getdate()))
Declare @Month_3 As Integer =  (Datepart(year,Dateadd(Month,-3,Getdate())) *100)+ Datepart(Month,Dateadd(Month,-3,Getdate()))
Declare @Month_4 As Integer =  (Datepart(year,Dateadd(Month,-4,Getdate())) *100)+ Datepart(Month,Dateadd(Month,-4,Getdate()))
Declare @Month_5 As Integer =  (Datepart(year,Dateadd(Month,-5,Getdate())) *100)+ Datepart(Month,Dateadd(Month,-5,Getdate()))
Declare @Month_6 As Integer =  (Datepart(year,Dateadd(Month,-6,Getdate())) *100)+ Datepart(Month,Dateadd(Month,-6,Getdate()))

Select C.C_code as GatewayCode,
       C.c_name as CompanyName ,
       ISnull(A.ca_account,'') as CompanyAccount 
       ,Convert(Decimal(12,2),0) as CurrentMonth
       ,Convert(Decimal(12,2),0) as [Month-1]
       ,Convert(Decimal(12,2),0) as [Month-2]
       ,Convert(Decimal(12,2),0) as [Month-3]
       ,Convert(Decimal(12,2),0) as [Month-4]
       ,Convert(Decimal(12,2),0) as [Month-5]
       ,Convert(Decimal(12,2),0) as [Month-6]
  into #TempFinal    
  from cpplEDI.dbo.companies C LEft join cppledi.dbo.companyaccount A ON C.c_id = A.ca_company_id WHere C.c_class =2
  --Select * from  #TempFinal    order by GatewayCode asc
  
  Select (Datepart(year,BillingDate) *100)+ Datepart(Month,BillingDate) as YearMonth,
		  AccountCode,
		  SUM(BilledTotal) as BilledTotal 
  Into #Temp1
  from Pronto.dbo.ProntoBilling WHere BillingDate >= DATEADD(MONTH,-6,Convert(Date,GEtdate()))
  Group by (Datepart(year,BillingDate) *100)+ Datepart(Month,BillingDate),AccountCode
  
  Update #TempFinal SET  CurrentMonth = #Temp1.BilledTotal  From #TempFinal Join #Temp1 on #TempFinal.CompanyAccount = #Temp1.AccountCode and #Temp1.YearMonth = @CurrentMonth
  Update #TempFinal SET  [Month-1] = #Temp1.BilledTotal  From #TempFinal Join #Temp1 on #TempFinal.CompanyAccount = #Temp1.AccountCode and #Temp1.YearMonth = @Month_1
  Update #TempFinal SET  [Month-2] = #Temp1.BilledTotal  From #TempFinal Join #Temp1 on #TempFinal.CompanyAccount = #Temp1.AccountCode and #Temp1.YearMonth = @Month_2
  Update #TempFinal SET  [Month-3] = #Temp1.BilledTotal  From #TempFinal Join #Temp1 on #TempFinal.CompanyAccount = #Temp1.AccountCode and #Temp1.YearMonth = @Month_3
  Update #TempFinal SET  [Month-4] = #Temp1.BilledTotal  From #TempFinal Join #Temp1 on #TempFinal.CompanyAccount = #Temp1.AccountCode and #Temp1.YearMonth = @Month_4
  Update #TempFinal SET  [Month-5] = #Temp1.BilledTotal  From #TempFinal Join #Temp1 on #TempFinal.CompanyAccount = #Temp1.AccountCode and #Temp1.YearMonth = @Month_5
  Update #TempFinal SET  [Month-6] = #Temp1.BilledTotal  From #TempFinal Join #Temp1 on #TempFinal.CompanyAccount = #Temp1.AccountCode and #Temp1.YearMonth = @Month_6
  
  Select * from #TempFinal order by CompanyName asc
  
END



GO
GRANT EXECUTE
	ON [dbo].[cppl_RPT_CompanyPaymentSummary]
	TO [ReportUser]
GO
