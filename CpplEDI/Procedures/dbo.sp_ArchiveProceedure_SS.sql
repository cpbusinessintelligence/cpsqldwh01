SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[sp_ArchiveProceedure_SS] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveEventDate = @ArchiveDate
	

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION trncpplEDI
SELECT [cd_id] into #Temp1 	  FROM [dbo].[consignment] with (nolock) WHERE cd_date<=@ArchiveEventDate

INSERT INTO [dbo].[consignment_Archive_18-19]
            
     			SELECT [cd_id]
      ,[cd_company_id]
      ,[cd_account]
      ,[cd_agent_id]
      ,[cd_import_id]
      ,[cd_ogm_id]
      ,[cd_manifest_id]
      ,[cd_connote]
      ,[cd_date]
      ,[cd_consignment_date]
      ,[cd_eta_date]
      ,[cd_eta_earliest]
      ,[cd_customer_eta]
      ,[cd_pickup_addr0]
      ,[cd_pickup_addr1]
      ,[cd_pickup_addr2]
      ,[cd_pickup_addr3]
      ,[cd_pickup_suburb]
      ,[cd_pickup_postcode]
      ,[cd_pickup_record_no]
      ,[cd_pickup_confidence]
      ,[cd_pickup_contact]
      ,[cd_pickup_contact_phone]
      ,[cd_delivery_addr0]
      ,[cd_delivery_addr1]
      ,[cd_delivery_addr2]
      ,[cd_delivery_addr3]
      ,[cd_delivery_email]
      ,[cd_delivery_suburb]
      ,[cd_delivery_postcode]
      ,[cd_delivery_record_no]
      ,[cd_delivery_confidence]
      ,[cd_delivery_contact]
      ,[cd_delivery_contact_phone]
      ,[cd_special_instructions]
      ,[cd_stats_branch]
      ,[cd_stats_depot]
      ,[cd_pickup_branch]
      ,[cd_pickup_pay_branch]
      ,[cd_deliver_branch]
      ,[cd_deliver_pay_branch]
      ,[cd_special_driver]
      ,[cd_pickup_revenue]
      ,[cd_deliver_revenue]
      ,[cd_pickup_billing]
      ,[cd_deliver_billing]
      ,[cd_pickup_charge]
      ,[cd_pickup_charge_actual]
      ,[cd_deliver_charge]
      ,[cd_deliver_payment_actual]
      ,[cd_pickup_payment]
      ,[cd_pickup_payment_actual]
      ,[cd_deliver_payment]
      ,[cd_deliver_charge_actual]
      ,[cd_special_payment]
      ,[cd_insurance_billing]
      ,[cd_items]
      ,[cd_coupons]
      ,[cd_references]
      ,[cd_rating_id]
      ,[cd_chargeunits]
      ,[cd_deadweight]
      ,[cd_dimension0]
      ,[cd_dimension1]
      ,[cd_dimension2]
      ,[cd_volume]
      ,[cd_volume_automatic]
      ,[cd_import_deadweight]
      ,[cd_import_volume]
      ,[cd_measured_deadweight]
      ,[cd_measured_volume]
      ,[cd_billing_id]
      ,[cd_billing_date]
      ,[cd_export_id]
      ,[cd_export2_id]
      ,[cd_pickup_pay_date]
      ,[cd_delivery_pay_date]
      ,[cd_transfer_pay_date]
      ,[cd_activity_stamp]
      ,[cd_activity_driver]
      ,[cd_pickup_stamp]
      ,[cd_pickup_driver]
      ,[cd_pickup_pay_driver]
      ,[cd_pickup_count]
      ,[cd_pickup_notified]
      ,[cd_accept_stamp]
      ,[cd_accept_driver]
      ,[cd_accept_count]
      ,[cd_accept_notified]
      ,[cd_indepot_notified]
      ,[cd_indepot_stamp]
      ,[cd_indepot_driver]
      ,[cd_indepot_count]
      ,[cd_transfer_notified]
      ,[cd_failed_stamp]
      ,[cd_failed_driver]
      ,[cd_deliver_stamp]
      ,[cd_deliver_driver]
      ,[cd_deliver_pay_driver]
      ,[cd_deliver_count]
      ,[cd_deliver_pod]
      ,[cd_deliver_notified]
      ,[cd_pickup_pay_notified]
      ,[cd_deliver_pay_notified]
      ,[cd_printed]
      ,[cd_returns]
      ,[cd_release]
      ,[cd_release_stamp]
      ,[cd_pricecode]
      ,[cd_insurance]
      ,[cd_pickup_agent]
      ,[cd_delivery_agent]
      ,[cd_agent_pod]
      ,[cd_agent_pod_desired]
      ,[cd_agent_pod_name]
      ,[cd_agent_pod_stamp]
      ,[cd_agent_pod_entry]
      ,[cd_completed]
      ,[cd_cancelled]
      ,[cd_cancelled_stamp]
      ,[cd_cancelled_by]
      ,[cd_test]
      ,[cd_dirty]
      ,[cd_transfer_stamp]
      ,[cd_transfer_driver]
      ,[cd_transfer_count]
      ,[cd_transfer_to]
      ,[cd_toagent_notified]
      ,[cd_toagent_stamp]
      ,[cd_toagent_driver]
      ,[cd_toagent_count]
      ,[cd_toagent_name]
      ,[cd_last_status]
      ,[cd_last_notified]
      ,[cd_last_info]
      ,[cd_last_driver]
      ,[cd_last_stamp]
      ,[cd_last_count]
      ,[cd_accept_driver_branch]
      ,[cd_activity_driver_branch]
      ,[cd_deliver_driver_branch]
      ,[cd_deliver_pay_driver_branch]
      ,[cd_failed_driver_branch]
      ,[cd_indepot_driver_branch]
      ,[cd_last_driver_branch]
      ,[cd_pickup_driver_branch]
      ,[cd_pickup_pay_driver_branch]
      ,[cd_special_driver_branch]
      ,[cd_toagent_driver_branch]
      ,[cd_transfer_driver_branch]
  FROM [dbo].[consignment] with (nolock) WHERE cd_date<=@ArchiveEventDate


INSERT INTO [dbo].[cdcoupon_Archive_18-19]
            
     			SELECT [cc_id]
      ,[cc_company_id]
      ,[cc_consignment]
      ,[cc_coupon]
      ,[cc_activity_stamp]
      ,[cc_pickup_stamp]
      ,[cc_accept_stamp]
      ,[cc_indepot_stamp]
      ,[cc_transfer_stamp]
      ,[cc_deliver_stamp]
      ,[cc_failed_stamp]
      ,[cc_activity_driver]
      ,[cc_pickup_driver]
      ,[cc_accept_driver]
      ,[cc_indepot_driver]
      ,[cc_transfer_driver]
      ,[cc_transfer_to]
      ,[cc_toagent_driver]
      ,[cc_toagent_stamp]
      ,[cc_toagent_name]
      ,[cc_deliver_driver]
      ,[cc_failed_driver]
      ,[cc_deliver_pod]
      ,[cc_failed]
      ,[cc_exception_stamp]
      ,[cc_exception_code]
      ,[cc_unit_type]
      ,[cc_internal]
      ,[cc_link_coupon]
      ,[cc_dirty]
      ,[cc_last_status]
      ,[cc_last_driver]
      ,[cc_last_stamp]
      ,[cc_last_info]
      ,[cc_accept_driver_branch]
      ,[cc_activity_driver_branch]
      ,[cc_deliver_driver_branch]
      ,[cc_failed_driver_branch]
      ,[cc_indepot_driver_branch]
      ,[cc_last_driver_branch]
      ,[cc_pickup_driver_branch]
      ,[cc_toagent_driver_branch]
      ,[cc_tranfer_driver_branch] FROM  cdcoupon cd with (nolock) inner join #temp1 cs with (nolock) on  cs.cd_id =cd.cc_consignment




	DELETE FROM [consignment] WHERE cd_date<=@ArchiveEventDate

	DELETE cd FROM cdcoupon cd with (nolock) inner join #temp1 cs with (nolock) on  cs.cd_id =cd.cc_consignment



	COMMIT TRANSACTION trncpplEDI

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------





GO
