SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Archive_CdCoupon]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min([cd_date]) from [dbo].[consignment]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL DROP TABLE #Temp1

SELECT [cd_id] into #Temp1 	  FROM [dbo].[consignment] with (nolock) WHERE cd_date=@MinDate
--SET @InsertQuery = '
	INSERT INTO [dbo].[cdcoupon_Archive_19-20]
	SELECT [cc_id]
      ,[cc_company_id]
      ,[cc_consignment]
      ,[cc_coupon]
      ,[cc_activity_stamp]
      ,[cc_pickup_stamp]
      ,[cc_accept_stamp]
      ,[cc_indepot_stamp]
      ,[cc_transfer_stamp]
      ,[cc_deliver_stamp]
      ,[cc_failed_stamp]
      ,[cc_activity_driver]
      ,[cc_pickup_driver]
      ,[cc_accept_driver]
      ,[cc_indepot_driver]
      ,[cc_transfer_driver]
      ,[cc_transfer_to]
      ,[cc_toagent_driver]
      ,[cc_toagent_stamp]
      ,[cc_toagent_name]
      ,[cc_deliver_driver]
      ,[cc_failed_driver]
      ,[cc_deliver_pod]
      ,[cc_failed]
      ,[cc_exception_stamp]
      ,[cc_exception_code]
      ,[cc_unit_type]
      ,[cc_internal]
      ,[cc_link_coupon]
      ,[cc_dirty]
      ,[cc_last_status]
      ,[cc_last_driver]
      ,[cc_last_stamp]
      ,[cc_last_info]
      ,[cc_accept_driver_branch]
      ,[cc_activity_driver_branch]
      ,[cc_deliver_driver_branch]
      ,[cc_failed_driver_branch]
      ,[cc_indepot_driver_branch]
      ,[cc_last_driver_branch]
      ,[cc_pickup_driver_branch]
      ,[cc_toagent_driver_branch]
      ,[cc_tranfer_driver_branch] FROM  cdcoupon cd with (nolock) inner join #temp1 cs with (nolock) on  cs.cd_id =cd.cc_consignment


	DELETE d FROM [CpplEDI].[dbo].cdcoupon d
	join [CpplEDI].[dbo].consignment c on d.cc_consignment = c.cd_id
	 where  cd_date = @MinDate 
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
