SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Boomerang_dailyconsolidateddelivery] --(@Accountname varchar(100))

AS

begin

select

convert(date,c.cd_date) [Master Consignment date],
convert(date,c1.cd_date),
c.cd_delivery_addr0 [Master Consignment Account Name],
 br_barcode [Child Consignment],
 c.cd_connote [Master Consignment],

c1.cd_date [Child Consignment Created date],
e.CustomerRefNo [CustomerReference],
a.Email [Sender Email],
a.Phone [Sender Phone],

a.Address1 [PickupAddress1],
a.Address2 [PickupAddress2],
a.Suburb [suburb],
a.PostCode [Postcode],
c1.cd_delivery_contact +'/'+c1.cd_delivery_contact_phone [ChildConsignment.Account/CompanyContact Email]

,com.c_name [supplier]

  from [dbo].[consignment] (nolock) c join cdcoupon cc on c.cd_id=cc.cc_consignment
join bulkreturn br on br_bulkbarcode=cc_coupon
join cdcoupon cc1 on cc1.cc_coupon=br_barcode
join [dbo].[consignment] (nolock) c1 on c1.cd_id=cc1.cc_consignment
join [EzyFreight].[dbo].[tblConsignment] e (nolock)  on c1.cd_connote=e.ConsignmentCode
join companies com on com.c_id=c.cd_company_id
 left join [EzyFreight].[dbo].tblAddress a on e.pickupid=a.addressid
  left join [EzyFreight].[dbo].tbladdress ab on e.DestinationID=ab.AddressID
  left join [EzyFreight].[dbo].tblCompanyUsers u on u.userid=e.userid
  left join [EzyFreight].[dbo].tblCompany com1 on com1.companyid=u.companyid
--where convert(date,c1.cd_date)=convert(date,getdate()-1) and c.cd_delivery_addr0=@AccountName
END

GO
GRANT EXECUTE
	ON [dbo].[sp_Boomerang_dailyconsolidateddelivery]
	TO [ReportUser]
GO
