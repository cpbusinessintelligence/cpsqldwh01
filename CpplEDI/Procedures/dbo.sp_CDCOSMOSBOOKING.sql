SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_CDCOSMOSBOOKING
aS
bEGIN

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdcosmosbooking') DROP TABLE [cdcosmosbooking];
CREATE TABLE [cdcosmosbooking] (
  [cb_consignment] int NOT NULL DEFAULT '0',
  [cb_send_stamp] datetime default NULL,
  [cb_reply_stamp] datetime default NULL,
  [cb_branch] int DEFAULT '0',
  [cb_cosmos_date] SMALLDATETIME default NULL,
  [cb_cosmos_job] int DEFAULT '0',
  [cb_style] char(1) DEFAULT 'C',
  [cb_card_notified] char(1) DEFAULT 'N',
  [cb_sms_stamp] datetime default NULL,
  [cb_sms_number] char(16) DEFAULT '',
  [cb_reschedule_stamp] datetime default NULL,
  [cb_reschedule_date] SMALLDATETIME default NULL,
  [cb_reschedule_am_pm] char(1) DEFAULT '',
  [cb_reschedule_style] char(1) DEFAULT '',
  [cb_reschedule_instructions] char(80) DEFAULT '',
  [cb_redelivery_stamp] datetime default NULL,
  [cb_redelivery_date] SMALLDATETIME default NULL,
  [cb_redelivery_am_pm] char(1) DEFAULT '',
  [cb_redelivery_style] char(1) DEFAULT '',
  [cb_redelivery_instructions] char(80) DEFAULT ''
 -- PRIMARY KEY ([cb_consignment])
) ;

--INSERT INTO cdcosmosbooking select * from openquery(MySQLMain,'select * from cpplEDI.cdcosmosbooking where cb_consignment <> ''44169684''');INSERT INTO Temp_tablesload values('cdcosmosbooking',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.cdcosmosbooking')),(select count(*) from cdcosmosbooking));

INSERT INTO cdcosmosbooking([cb_consignment],[cb_send_stamp],[cb_reply_stamp],[cb_branch],[cb_cosmos_date],[cb_cosmos_job],[cb_style]) select * from openquery(MySQLMain,'select pc_consignment,p_sent_stamp,p_confirmed_stamp,p_cosmos_branch,p_cosmos_date,p_cosmos_job,p_style from cpplEDI.pickups LEFT JOIN  cpplEDI.pickupconsignment ON p_id = pc_pickup where  pc_consignment <> ''44169684''');
INSERT INTO Temp_tablesload values('cdcosmosbooking',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.pickups LEFT JOIN  cpplEDI.pickupconsignment ON p_id = pc_pickup')),(select count(*) from cdcosmosbooking));

END
GO
