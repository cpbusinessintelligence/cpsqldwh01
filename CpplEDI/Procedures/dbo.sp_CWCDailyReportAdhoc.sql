SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE Proc [dbo].[sp_CWCDailyReportAdhoc]
(
@AccountCode Varchar(200),
@StartDate Date,
@EndDate Date
)
as
Begin

--Declare @Date Datetime
--Set @Date = Convert(date,Getdate()-1)


Select 
convert(date,cd_date) as ConsignmentDate,
co.cd_connote as ConsignmentNumber,
cc_coupon Label,
cd_delivery_addr0 as CustomerName,
cd_account AccountCode,
cd_deadweight as DeclaredWeight,
[ccr_deadweight] as MeasuredWeightLabel,
cd_pricecode as DeclaredServiceCode,
cd_volume*250 as DeclaredCubicWeight,
[ccr_volume]*250 as MeasuredCubicWeightLabel,
cd_items as ItemQuantity,
[ccr_dimension0] as Length,
[ccr_dimension1] as Width,
[ccr_dimension2] as Height into #Temp1
From Consignment co
inner join cdCoupon cdc
on(co.cd_id = cdc.cc_consignment)
left join [CpplEDI].[dbo].[ccreweigh] cdr
On(cdc.cc_coupon = cdr.ccr_coupon)
left join pronto.[dbo].[Incoming_CWCDetails] cwc
On(cwc.Barcodes= cdc.cc_coupon and 
IsSent = 1 )
where --cc_coupon in (select Barcodes from pronto.[dbo].[Incoming_CWCDetails] where 
convert(date,dwstimestamp) between @StartDate and @EndDate 
and   (cd_account =@Accountcode

 or @AccountCode is null)
--in ('112951223','113102511')
order by 2

--select * into #Temp2 from #Temp1 where MeasuredWeightLabel is null

--select * into #Temp3 from #Temp1 where MeasuredWeightLabel is not null

--select * from  #Temp2


update #Temp1
set Length = crh_dimension0,Width = crh_dimension1,Height = crh_dimension2,MeasuredWeightLabel = crh_deadweight,MeasuredCubicWeightLabel= crh_volume*250
from #Temp1 a
left join [dbo].[ccreweighhold] b
on(a.Label = b.crh_coupon)
where (MeasuredCubicWeightLabel is null or length is null or Height is null or Width is null or MeasuredWeightLabel is null)


--update #Temp3
--set Length = crh_dimension0,Width = crh_dimension1,Height = crh_dimension2,MeasuredWeightLabel = crh_deadweight,MeasuredCubicWeightLabel= crh_volume*250
--from #Temp3 a
--left join [dbo].[ccreweighhold] b
--on(a.Label = b.crh_coupon)

----where crh_coupon = 'CPAFXLT7511952'
--select * into #Temp4 from  #Temp2
--union all
--select * from  #Temp3

select * from #Temp1 
End


GO
GRANT EXECUTE
	ON [dbo].[sp_CWCDailyReportAdhoc]
	TO [ReportUser]
GO
