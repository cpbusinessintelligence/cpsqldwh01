SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[sp_CWCDailyReportTransDirect]
as
Begin

    Declare @Date Date = Getdate()-1
	--Declare @Date Date = '2020-05-24'
	Declare @StartDate datetime;
	Declare @EndDate datetime;

	Set @StartDate= Convert(Datetime,Convert(Varchar(20),@Date) + ' 00:00:00:000')
	Set @EndDate = Convert(Datetime,Convert(Varchar(20),@Date) + ' 23:59:59:000')
	print @StartDate
	print @EndDate

	DECLARE @Temp3 TABLE
	(
			Barcodes varchar(50) ,   weight varchar(50),   Length varchar(50),  Height varchar(50), Width varchar(50)
			, Volume varchar(50)
			,  DwsTimestamp datetime
	)
	Insert Into @Temp3 
	Select Barcodes,weight,Length,Height,Width,Volume,DwsTimestamp 
	From
	(
		Select 
			ccr_coupon as Barcodes, ccr_deadweight as weight, [ccr_dimension0]as Length,
			[ccr_dimension2] as Width,
			[ccr_dimension1]  as Height,
			[ccr_volume]*250 as volume, 
			ccr_stamp as DwsTimestamp 
		From [CpplEDI].[dbo].[ccreweigh] 
		Where ccr_stamp between @StartDate and  @EndDate
		UNION ALL 
		Select 
			crh_coupon as Barcodes, crh_deadweight as weight, crh_dimension0 as Length,
			crh_dimension1 as Width,
			crh_dimension2 as Height, 
			crh_volume *250 as volume, 
			crh_stamp as DwsTimestamp 
		From [CpplEDI].[dbo].[ccreweighhold]
		Where crh_coupon not in (select ccr_coupon from [CpplEDI].[dbo].[ccreweigh] where ccr_stamp between @StartDate and  @EndDate  ) 
		and crh_stamp between @StartDate and  @EndDate
	) A


	Select 
		convert(date,cd_date) as ConsignmentDate
		,c.cd_connote as ConsignmentNumber
		,ltrim(rtrim(cc_coupon)) Label
		,cd_delivery_addr0 as CustomerName
		,cd_account AccountCode
		,cd_pricecode as DeclaredServiceCode
		,cd_deadweight as DeclaredWeight
		,convert(decimal(7,2),T.weight) as MeasuredWeightLabel
		,convert(decimal(7,2),cd_volume*250) as DeclaredCubicWeight
		,convert(decimal(7,2),T.Volume) as MeasuredCubicWeightLabel
		,cd_items as ItemQuantity
		,Convert(Varchar(30),T.Dwstimestamp,121) as MeasuredTime
		,T.Length as Length
		,T.Width as Width
		,T.Height as Height 
	From @temp3 T 
		Join  cdCoupon Cc on T.Barcodes = Cc.cc_coupon
		Join consignment c on c.cd_id = Cc.cc_consignment 
	Where cd_account in ('113102339','113102321','113075030','112877139')  
	Order by cd_connote

End
GO
GRANT EXECUTE
	ON [dbo].[sp_CWCDailyReportTransDirect]
	TO [ReportUser]
GO
