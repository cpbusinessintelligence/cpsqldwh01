SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[sp_CWCDailyReport_BUP_JP_20200522]
as
Begin

    Declare @Date Date = Getdate()-1
	--Declare @Date Date = '2020-05-22'
	Declare @StartDate datetime;
	Declare @EndDate datetime;

	Set @StartDate= Convert(Datetime,Convert(Varchar(20),@Date) + ' 00:00:00:000')
	set @EndDate = Convert(Datetime,Convert(Varchar(20),@Date) + ' 23:59:59:000')
	--Drop table #temp3
	--Drop table #temp1

select Barcodes ,   weight,   Length,  Height, Width 
, Volume
,  DwsTimestamp 
into #temp3
from(
--Select  Barcodes ,  convert(decimal(7,2),isnull(weight,0))/1000 as weight
--,  CubeLength as Length, CubeHeight as Height, CubeWidth as Width 
--,NULL as Volume
--, DwsTimestamp as DwsTimestamp into #temp1  from pronto.[dbo].[Incoming_CWCDetails] 
--where DwsTimestamp between @StartDate and  @EndDate  
--and issent =1
--UNION ALL
select ccr_coupon as Barcodes, ccr_deadweight as weight, [ccr_dimension0]as Length,
[ccr_dimension2] as Width,
[ccr_dimension1]  as Height,
[ccr_volume] as volume, 
ccr_stamp as DwsTimestamp 
from [CpplEDI].[dbo].[ccreweigh] where 
ccr_stamp between @StartDate and  @EndDate
UNION ALL 
select crh_coupon as Barcodes, crh_deadweight as weight, crh_dimension0 as Length,
crh_dimension1 as Width,
crh_dimension2 as Height, 
crh_volume as volume, 
crh_stamp as DwsTimestamp 
from [CpplEDI].[dbo].[ccreweighhold]   where 
crh_coupon not in (select ccr_coupon from [CpplEDI].[dbo].[ccreweigh] where ccr_stamp between @StartDate and  @EndDate  ) 
and crh_stamp between @StartDate and  @EndDate
) A

--select * into #temp4 from  #temp1 where barcodes not in (select barcodes from #temp3)



--select * from #temp4 where barcodes in (select ccr_coupon from CpplEDI..ccreweigh)
--select * from #emp4 where barcodes in  (select crh_coupon from CpplEDI..ccreweighhold)

Select Cc.cc_coupon as Label, 
 c.cd_connote as ConsignmentNumber ,
 convert(date,cd_date)  as ConsignmentDate,
 cd_delivery_addr0 as CustomerName,
cd_account as AccountCode,
T.Weight as MeasuredWeightLabel,
T.Volume as MeasuredCubicWeightLabel,
T.DwsTimestamp as MeasuredTime,
T.Height,
T.Width,
T.Length,
cd_pricecode as DeclaredServiceCode,
cd_deadweight as DeclaredWeight,
 cd_volume*250 as DeclaredCubicWeight,
 cd_items as ItemQuantity into #temp2
 from #Temp3 T JOIn  cdCoupon Cc on T.Barcodes = Cc.cc_coupon
join consignment c on c.cd_id = Cc.cc_consignment 
where cd_account in ('112951223','113102511') order by cd_connote




select * from #temp2 

/*

Select 
convert(date,cd_date) as ConsignmentDate,
co.cd_connote as ConsignmentNumber,
ltrim(rtrim(cc_coupon)) Label,
cd_delivery_addr0 as CustomerName,
cd_account AccountCode,
cd_pricecode as DeclaredServiceCode,
cd_deadweight as DeclaredWeight,
ISNULL(convert(decimal(7,2),weight)/1000,[ccr_deadweight]) as MeasuredWeightLabel,
cd_volume*250 as DeclaredCubicWeight,
[ccr_volume]*250 as MeasuredCubicWeightLabel,
cd_items as ItemQuantity,
ISNULL(Dwstimestamp,ccr_stamp) as MeasuredTime,
Isnull([ccr_dimension0],CubeLength) as Length,
Isnull([ccr_dimension1],CubeWidth) as Width,
Isnull([ccr_dimension2],CubeHeight) as Height
into #Temp1
From Consignment (nolock )co inner join cdCoupon (nolock)cdc  on(co.cd_id = cdc.cc_consignment)
                              left join [CpplEDI].[dbo].[ccreweigh] cdr On(cdc.cc_coupon = cdr.ccr_coupon)
							  left join pronto.[dbo].[Incoming_CWCDetails] cwc  On(cwc.Barcodes= cdc.cc_coupon and IsSent = 1 )
where dwstimestamp >= getdate()-1
and cd_account in ('112951223','113102511') --,'113102339','113102321','113075030','112877139') 
order by 2




update #Temp1
set Length = crh_dimension0,Width = crh_dimension1,Height = crh_dimension2,MeasuredWeightLabel = crh_deadweight,MeasuredCubicWeightLabel= crh_volume*250
from #Temp1 a
left join [dbo].[ccreweighhold] b
on(a.Label = b.crh_coupon)
where (MeasuredCubicWeightLabel is null or length is null or Height is null or Width is null  or MeasuredWeightLabel is null)






 select * from #Temp1 
 	*/
End
GO
