SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






--exec [dbo].[sp_CWCDailyReport]

CREATE Proc [dbo].[sp_CWCDailyReport_Bkup_AH_220502020]
--@AccountNumber nvarchar(max)
as
Begin

--drop table #temp1

Declare @Date Datetime
Set @Date = Convert(date,Getdate()-1)


Select 
convert(date,cd_date) as ConsignmentDate,
co.cd_connote as ConsignmentNumber,
ltrim(rtrim(cc_coupon)) Label,
cd_delivery_addr0 as CustomerName,
cd_account AccountCode,
cd_pricecode as DeclaredServiceCode,
cd_deadweight as DeclaredWeight,
ISNULL(convert(decimal(7,2),weight)/1000,[ccr_deadweight]) as MeasuredWeightLabel,
cd_volume*250 as DeclaredCubicWeight,
[ccr_volume]*250 as MeasuredCubicWeightLabel,
cd_items as ItemQuantity,
ISNULL(Dwstimestamp,ccr_stamp) as MeasuredTime,
Isnull([ccr_dimension0],CubeLength) as Length,
Isnull([ccr_dimension1],CubeWidth) as Width,
Isnull([ccr_dimension2],CubeHeight) as Height into #Temp1
From Consignment (nolock )co
inner join cdCoupon (nolock)cdc
on(co.cd_id = cdc.cc_consignment)
left join [CpplEDI].[dbo].[ccreweigh] cdr
On(cdc.cc_coupon = cdr.ccr_coupon)
left join pronto.[dbo].[Incoming_CWCDetails] cwc
On(cwc.Barcodes= cdc.cc_coupon and 
IsSent = 1 )
where ---cc_coupon in (select Barcodes from pronto.[dbo].[Incoming_CWCDetails] where 
--convert(date,dwstimestamp) = '2020-02-27'
convert(date,dwstimestamp) = convert(Date,@Date)
and cd_account in ('112951223','113102511') --,'113102339','113102321','113075030','112877139') 
order by 2


--Insert into #Temp1
--Select distinct
----convert(date,dwstimestamp),
--convert(date,co.CreatedDateTime) as ConsignmentDate,
--co.ConsignmentCode as ConsignmentNumber,
--LabelNumber Label,
--companyname as CustomerName,
--AccountNumber AccountCode,
--RateCardID as DeclaredServiceCode,
--MeasureWeight as DeclaredWeight,
--[ccr_deadweight] as MeasuredWeightLabel,
--DeclareVolume*250 as DeclaredCubicWeight,
--[ccr_volume]*250 as MeasuredCubicWeightLabel,
--Quantity as ItemQuantity,
--ISNULL(Dwstimestamp,ccr_stamp),
--[ccr_dimension0] as Length,
--[ccr_dimension1] as Width,
--[ccr_dimension2] as Height-- into #Temp1
--From EzyFreight.dbo.tblConsignment (nolock )co

--inner join EzyFreight.dbo.tblitemLabel (nolock)cdc
--on(co.consignmentid = cdc.consignmentid)
--inner join EzyFreight.dbo.tblcompanyusers (nolock) u on u.userid =co.userid
--inner join EzyFreight.dbo.tblcompany (nolock) c on c.companyid=u.companyid

--left join [CpplEDI].[dbo].[ccreweigh] cdr
--On(cdc.LabelNumber = cdr.ccr_coupon)
--left join pronto.[dbo].[Incoming_CWCDetails] cwc
--On(cwc.Barcodes= cdc.LabelNumber and 
--IsSent = 1 ) 
--where convert(date,dwstimestamp) = convert(Date,@Date)  and
-- AccountNumber in  ('112951223','113102511') 


--select * into #Temp2 from #Temp1 where MeasuredWeightLabel is null

--select * into #Temp3 from #Temp1 where MeasuredWeightLabel is not null

--select * from  #Temp2



update #Temp1
set Length = crh_dimension0,Width = crh_dimension1,Height = crh_dimension2,MeasuredWeightLabel = crh_deadweight,MeasuredCubicWeightLabel= crh_volume*250
from #Temp1 a
left join [dbo].[ccreweighhold] b
on(a.Label = b.crh_coupon)
where (MeasuredCubicWeightLabel is null or length is null or Height is null or Width is null  or MeasuredWeightLabel is null)

--update #Temp3
--set Length = crh_dimension0,Width = crh_dimension1,Height = crh_dimension2,MeasuredWeightLabel = crh_deadweight,MeasuredCubicWeightLabel= crh_volume*250
--from #Temp3 a
--left join [dbo].[ccreweighhold] b
--on(a.Label = b.crh_coupon)

--where crh_coupon = 'CPAFXLT7511952'
--select * into #Temp4 from  #Temp2
--union all
--select * from  #Temp3

 select * from #Temp1 --t where t.AccountCode in (SELECT * FROM dbo.fn_Split(@AccountNumber,','))


/*select t.*,d.Shortname from #Temp4 t left join pronto.dbo.prontodebtor d on left(@AccountNumber,9)=d.Accountcode

where t.AccountCode in (SELECT * FROM dbo.fn_Split(@AccountNumber,','))*/

End


--exec  [dbo].[sp_CWCDailyReport]
GO
GRANT EXECUTE
	ON [dbo].[sp_CWCDailyReport_Bkup_AH_220502020]
	TO [ReportUser]
GO
