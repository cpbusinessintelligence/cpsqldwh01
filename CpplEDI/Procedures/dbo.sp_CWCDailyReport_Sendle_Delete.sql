SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_CWCDailyReport_Sendle_Delete]
AS
BEGIN

	Delete From [Load_DailyCWCData_Sendle]
	Where 
		ProcessedDate <= GETDATE()-45
		And IsSent =1
END
GO
