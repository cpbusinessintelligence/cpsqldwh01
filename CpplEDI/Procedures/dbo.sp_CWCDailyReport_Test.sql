SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc [dbo].[sp_CWCDailyReport_Test]
@AccountNumber nvarchar(max)
as
Begin

Declare @Date Datetime
Set @Date = Convert(date,Getdate()-1)


Select 
convert(date,cd_date) as ConsignmentDate,
co.cd_connote as ConsignmentNumber,
cc_coupon Label,
cd_delivery_addr0 as CustomerName,
cd_account AccountCode,
cd_pricecode as DeclaredServiceCode,
cd_deadweight as DeclaredWeight,
[ccr_deadweight] as MeasuredWeightLabel,
cd_volume*250 as DeclaredCubicWeight,
[ccr_volume]*250 as MeasuredCubicWeightLabel,
cd_items as ItemQuantity,
[ccr_dimension0] as Length,
[ccr_dimension1] as Width,
[ccr_dimension2] as Height into #Temp1
From Consignment co
inner join cdCoupon cdc
on(co.cd_id = cdc.cc_consignment)
left join [CpplEDI].[dbo].[ccreweigh] cdr
On(cdc.cc_coupon = cdr.ccr_coupon)
left join pronto.[dbo].[Incoming_CWCDetails] cwc
On(cwc.Barcodes= cdc.cc_coupon and 
IsSent = 1 )
where ---cc_coupon in (select Barcodes from pronto.[dbo].[Incoming_CWCDetails] where 
convert(date,dwstimestamp) = convert(Date,@Date) 
and cd_account in ('112951223','113102511','113102339','113102321','113075030','112877139')
order by 2

select * into #Temp2 from #Temp1 where MeasuredWeightLabel is null

select * into #Temp3 from #Temp1 where MeasuredWeightLabel is not null

--select * from  #Temp2


update #Temp2
set Length = crh_dimension0,Width = crh_dimension1,Height = crh_dimension2,MeasuredWeightLabel = crh_deadweight,MeasuredCubicWeightLabel= crh_volume*250
from #Temp2 a
left join [dbo].[ccreweighhold] b
on(a.Label = b.crh_coupon)

--where crh_coupon = 'CPAFXLT7511952'
select * into #Temp4 from  #Temp2
union all
select * from  #Temp3

select t.*,d.Shortname from #Temp4 t left join pronto.dbo.prontodebtor d on t.accountcode=d.Accountcode

where t.AccountCode in (SELECT * FROM dbo.fn_Split(@AccountNumber,','))

End
--(SELECT Number FROM dbo.fn_SplitInt(@sParameterString,','))
--exec [dbo].[sp_CWCDailyReport_Test] 112951223

GO
