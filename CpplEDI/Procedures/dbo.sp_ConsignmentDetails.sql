SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_ConsignmentDetails] as
begin
  --'=====================================================================
    --' CP -Stored Procedure -sp_ConsignmentDetails
    --' ---------------------------
    --' Purpose: To get all consignment details for yesterday's consignments for Exile soft Integration-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 02 Apr 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 02/04/2015    AB      1.00    Created the procedure                             --AB20150402

    --'=====================================================================



Declare  @temp Table   ( cd_id int,
                         Consignment varchar(100),
                         LabelNumber varchar(100),
	                     AgentID int,
	                     AgentName varchar(200),
	                     PickupDate DateTime,
	                     PickupAddr0 varchar(500),
	                     PickupAddr1 varchar(500),
	                     PickupAddr2 varchar(500),
	                     PickupAddr3 varchar(500),
	                     DeliveryAddr0 varchar(500),
	                     DeliveryAddr1 varchar(500),
	                     DeliveryAddr2 varchar(500),
	                     DeliveryAddr3 varchar(500),
	                     Pieces int,
	                     DeadWeight decimal(20,2),
	                     SpecialInstructions varchar(500),
	                     FromETADate DateTime,
						 ToETADate DateTime)

Insert into @Temp
Select  cd_id,
        cd_connote as Consignment,
        ltrim(rtrim(cc.cc_coupon)),
	   cd_agent_id as AgentID,
	   a.a_name as AgentName,
	   isnull(cd_pickup_stamp,'') as PickupDate,
	   isnull(cd_pickup_addr0,'') as PickupAddr0,
	   isnull(cd_pickup_addr1 ,'') as PickupAddr1,
	   isnull(cd_pickup_addr2,'') as PickupAddr2,
	   isnull(cd_pickup_addr3,'') as PickupAddr3,
	   isnull(cd_delivery_addr0,'') as DeliveryAddr0,
	   isnull(cd_delivery_addr1,'') as DeliveryAddr1,
	   isnull(cd_delivery_addr2,'') as DeliveryAddr2,
	   isnull(cd_delivery_addr3,'') as DeliveryAddr3,
	   convert(int,0) as Pieces,
	   isnull(cd_deadweight,'') as DeadWeight,
	   REPLACE(REPLACE(REPLACE(REPLACE(isnull(cd_special_instructions,''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' ') as SpecialInstructions,
	   isnull(cd_eta_earliest,'') as FromETADate,
	   isnull(cd_customer_ETA,'') as ToETADate
 --into #temp
from cpplEDI.dbo.consignment c 
left join cpplEDI.dbo.cdcoupon cc on c.cd_id=cc.cc_consignment
 join cpplEDI.dbo.agents a on a.a_id=cd_agent_id
where cd_date=
--='2016-03-18'
convert(date,dateadd("dd",-1,getdate())) 
 and a_name not like '%Couriers Please%' and a_name not like '%CPPL%' 
--and a.a_name in (select AgentName from [dbo].[Agents List for Exile Soft Integration])


update @Temp set Pieces=(select count(*) from cpplEDI.dbo.cdcoupon cc where cd_id=cc.cc_consignment)

select Consignment ,
       LabelNumber,
       AgentID ,
	   AgentName ,
	   PickupDate ,
	   PickupAddr0 ,
	   PickupAddr1 ,
	   PickupAddr2 ,
	   PickupAddr3 ,
	   DeliveryAddr0 ,
	   DeliveryAddr1 ,
	   DeliveryAddr2 ,
	   DeliveryAddr3 ,
	   Pieces ,
	   DeadWeight ,
	   SpecialInstructions ,
	   FromETADate,
	   ToETADate from @Temp
	   order by Consignment

end
GO
GRANT EXECUTE
	ON [dbo].[sp_ConsignmentDetails]
	TO [SSISUser]
GO
