SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetConignmentsforHubbed] as
begin

 --'=====================================================================
    --' CP -Stored Procedure - [sp_GetConignmentsforHubbed]
    --' ---------------------------
    --' Purpose: [sp_GetConignmentsforHubbed]-----
    --' Developer: Sinshith (Couriers Please Pty Ltd)
    --' Date: 28 03 2018
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                                                
	--  28 03 2018    SS
    --'=====================================================================
			  
			  
			  Delete from CpplEDI.dbo.cdextra where ce_consignment in (Select ce_consignment  From openquery(MySQLMain,'select * from cpplEDI.cdextra where ce_key = ''DROPOFFPOPID'''))
			  Insert into CpplEDI.dbo.cdextra(ce_consignment,ce_key,ce_value)
			  Select ce_consignment,ce_key,ce_value from openquery(MySQLMain,'select * from cpplEDI.cdextra where ce_key = ''DROPOFFPOPID''')

End

GO
