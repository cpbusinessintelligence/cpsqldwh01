SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure sp_GettheLatestStatusUpdateforConsignment as
begin
select cd_connote,cd_date,cc_coupon,convert(datetime,'') as Maxeventdatetime,convert(varchar(100),'') as LastStatus
into #temp
from consignment join cdcoupon on cc_consignment=cd_id where cd_date>='2015-05-29'

select * from #temp where cc_coupon='CPABPUZ0352517001'

select * from scannergateway.dbo.trackingevent where sourcereference='CPABPUZ0352517001'

update #temp set Maxeventdatetime=(select max(eventdatetime) from scannergateway.dbo.trackingevent t(NOLOCK) where t.sourcereference=#temp.cc_coupon)
from scannergateway.dbo.trackingevent t (NOLOCK)  where t.sourcereference=#temp.cc_coupon

update #temp set LastStatus=et.description from scannergateway.dbo.trackingevent t(NOLOCK) join scannergateway.[dbo].[EventType] et on et.id=t.eventtypeid
where eventdatetime=Maxeventdatetime and cc_coupon=sourcereference and eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')

update #temp set LastStatus= et.description from scannergateway.dbo.trackingevent t(NOLOCK) join scannergateway.[dbo].[EventType] et on et.id=t.eventtypeid
where eventdatetime=Maxeventdatetime and cc_coupon=sourcereference and laststatus=''

select distinct cd_connote,laststatus,count(*) from #temp
group by cd_connote,laststatus
having count(*)>1

end
GO
