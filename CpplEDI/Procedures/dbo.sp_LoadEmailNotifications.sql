SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_LoadEmailNotifications](@date date) as

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

Select   cd_Connote as ConsignmnetNumber,
         cd_Account as AccountNumber,
		 convert(varchar(200),'') as Email,
		 Convert(Varchar(200),'') as Name,
		 Convert(datetime,'') as EventDateTime,
		 Convert(varchar(100),'') as EventType,
		 Convert(bit,0) as StatusFlag
into #Temp1 
from cpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
                                       

Update #Temp1 SET EventType =  'No Activity' Where  PickupDate is null and OutForDeliveryDate is null and AttemptedDeliveryDate is null and DeliveryDate is null and StatusDescription = ''
Update #Temp1 Set EventType =  'Cant Calculate' where ETADate is null and StatusDescription = ''
Update #temp1 SET EventType =  'Exceptions' Where  Exceptions <>'' and StatusDescription = ''
Update #temp1 SET EventType =  'On Time' Where Datediff(day,ETADate,Isnull(AttemptedDeliveryDate,DeliveryDate))<=0 and ETADate is not null and StatusDescription = ''
Update #temp1 SET EventType =  'Not On Time' Where Datediff(day,ETADate,Isnull(AttemptedDeliveryDate,DeliveryDate))>0 and ETADate is not null and StatusDescription = ''
Update #temp1 SET EventType =  CASE WHEN Datediff(day,ETADate,GETDATE()) <=0 THEN 'On Time' ELSE 'Not On Time' END  Where     StatusDescription = ''

--INSERT INTO 
END


GO
