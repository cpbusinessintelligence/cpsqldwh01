SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadcpplEDITablesforSingleSignOn]
as
begin


     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadcpplEDITablesforSingleSignOn]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 07 Dec 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 07/12/2015    AB      1.00    Created the procedure                             --AB20151207

    --'=====================================================================


  MERGE companies AS cd
USING (select * from openquery(MySQLMain,'select * from cpplEDI.companies')) AS cs ON cs.c_id=cd.c_id
when matched then UPDATE 
SET [c_id]=cs.c_id
      ,[c_code]=cs.c_code
      ,[c_name]=cs.c_name
      ,[c_zoning_id]=cs.c_zoning_id
      ,[c_pricegroup_id]=cs.c_pricegroup_id
      ,[c_class]=cs.c_class
      ,[c_returns]=cs.c_returns
      ,[c_return_connote_format]=cs.c_return_connote_format
      ,[c_return_tracking_format]=cs.c_return_tracking_format
      ,[c_return_manifest_format]=cs.c_return_manifest_format
      ,[c_return_connote_id]=cs.c_return_connote_id
      ,[c_return_tracking_id]=cs.c_return_tracking_id
      ,[c_return_manifest_id]=cs.c_return_manifest_id
      ,[c_default_addr0]=cs.c_default_addr0
      ,[c_default_addr1]=cs.c_default_addr1
      ,[c_default_addr2]=cs.c_default_addr2
      ,[c_default_addr3]=cs.c_default_addr3
      ,[c_default_suburb]=cs.c_default_suburb
      ,[c_default_postcode]=cs.c_default_postcode
      ,[c_podrequired]=cs.c_podrequired
      ,[c_active]=cs.c_active
      ,[c_update_style]=cs.c_update_style
      ,[c_autoquery]=cs.c_autoquery
      ,[c_billing_cycle]=cs.c_billing_cycle
      ,[c_billing_trigger]=cs.c_billing_trigger
      ,[c_billing_group]=cs.c_billing_group
      ,[c_abn]=cs.c_abn
      ,[c_gstreg]=cs.c_gstreg
      ,[c_asic]=cs.c_asic
      ,[c_salesrep_id]=cs.c_salesrep_id
      ,[c_billing_name]=cs.c_billing_name
      ,[c_billing_addr0]=cs.c_billing_addr0
      ,[c_billing_addr1]=cs.c_billing_addr1
      ,[c_billing_addr2]=cs.c_billing_addr2
      ,[c_billing_addr3]=cs.c_billing_addr3
      ,[c_billing_suburb]=cs.c_billing_suburb
      ,[c_billing_postcode]=cs.c_billing_postcode
      ,[c_billing_email]=cs.c_billing_email
      ,[c_default_paystyle]=cs.c_default_paystyle
      ,[c_default_bank]=cs.c_default_bank
      ,[c_default_branch]=cs.c_default_branch
      ,[c_default_drawer]=cs.c_default_drawer
      ,[c_cosmos_transfer]=cs.c_cosmos_transfer
      ,[c_auth_owner]=cs.c_auth_owner
      ,[c_auth_group]=cs.c_auth_group
      ,[c_validation_id]=cs.c_validation_id
      ,[c_ezyfreight_pricecode]=cs.c_ezyfreight_pricecode
      ,[c_ezyfreight_volume]=cs.c_ezyfreight_volume
      ,[c_ezyfreight_weight]=cs.c_ezyfreight_weight
      ,[c_ezyfreight_pieces]=cs.c_ezyfreight_pieces
      ,[c_ezyfreight_zonemap]=cs.c_ezyfreight_zonemap
      ,[c_ezyfreight_connote_format]=cs.c_ezyfreight_connote_format
      ,[c_ezyfreight_connote_id]=cs.c_ezyfreight_connote_id
      ,[c_ezyfreight_consolidate]=cs.c_ezyfreight_consolidate
      ,[c_ezyfreight_insurance]=cs.c_ezyfreight_insurance
      ,[c_ezyfreight_insurance_choices]=cs.c_ezyfreight_insurance_choices
      ,[c_ezyfreight_header]=cs.c_ezyfreight_header
      ,[c_ezyfreight_no_email]=cs.c_ezyfreight_no_email
      ,[c_ezyfreight_atl]=cs.c_ezyfreight_atl
      ,[c_returns_pricecode]=cs.c_returns_pricecode
      ,[c_last_check]=cs.c_last_check
      ,[c_check_frequency]=cs.c_check_frequency
      ,[c_cosmos_code]=cs.c_cosmos_code
      ,[c_update_format]=cs.c_update_format
      ,[c_update_frequency]=cs.c_update_frequency
      ,[c_update_ftp_host]=cs.c_update_ftp_host
      ,[c_update_ftp_port]=cs.c_update_ftp_port
      ,[c_update_ftp_user]=cs.c_update_ftp_user
      ,[c_update_ftp_password]=cs.c_update_ftp_password
      ,[c_update_ftp_directory]=cs.c_update_ftp_directory
      ,[c_update_email]=cs.c_update_email
      ,[c_update_filename_format]=cs.c_update_filename_format
      ,[c_update_id]=cs.c_update_id
      ,[c_update_passive]=cs.c_update_passive
      ,[c_export_special]=cs.c_export_special
      ,[c_cubic_factor]=cs.c_cubic_factor
      ,[c_difot_failedok]=cs.c_difot_failedok
      ,[c_application_details]=cs.c_application_details
      ,[c_notes]=cs.c_notes
      ,[c_ezyfreight_default_instructions]=cs.c_ezyfreight_default_instructions
      ,[c_ezyfreight_no_auto_release]=cs.c_ezyfreight_no_auto_release
      ,[c_ezyfreight_cube_exempt]=cs.c_ezyfreight_cube_exempt
      ,[c_daily_deliveries_email]=cs.c_daily_deliveries_email
      ,[c_sms_on_pickup]=cs.c_sms_on_pickup
      ,[c_alternate_agents]=cs.c_alternate_agents
      ,[c_items_and_coupons]=cs.c_items_and_coupons
      ,[c_no_oversize_warning]=cs.c_no_oversize_warning
      ,[c_obd_notify_mail]=cs.c_obd_notify_mail
	  ,[c_returns_available]=cs.c_returns_available 
	  ,[c_booking_fixed_time]=cs.c_booking_fixed_time
	  ,[c_booking_from_first]=cs.c_booking_from_first
	  ,[c_booking_from_last]=cs.c_booking_from_last
	  ,[c_booking_auto_rebook]=cs.c_booking_auto_rebook
	  ,[c_booking_consolidated]=cs.c_booking_consolidated
	  ,[c_labelless]=cs.c_labelless

when not matched then INSERT values([c_id]
      ,[c_code]
      ,[c_name]
      ,[c_zoning_id]
      ,[c_pricegroup_id]
      ,[c_class]
      ,[c_returns]
      ,[c_return_connote_format]
      ,[c_return_tracking_format]
      ,[c_return_manifest_format]
      ,[c_return_connote_id]
      ,[c_return_tracking_id]
      ,[c_return_manifest_id]
      ,[c_default_addr0]
      ,[c_default_addr1]
      ,[c_default_addr2]
      ,[c_default_addr3]
      ,[c_default_suburb]
      ,[c_default_postcode]
      ,[c_podrequired]
      ,[c_active]
      ,[c_update_style]
      ,[c_autoquery]
      ,[c_billing_cycle]
      ,[c_billing_trigger]
      ,[c_billing_group]
      ,[c_abn]
      ,[c_gstreg]
      ,[c_asic]
      ,[c_salesrep_id]
      ,[c_billing_name]
      ,[c_billing_addr0]
      ,[c_billing_addr1]
      ,[c_billing_addr2]
      ,[c_billing_addr3]
      ,[c_billing_suburb]
      ,[c_billing_postcode]
      ,[c_billing_email]
      ,[c_default_paystyle]
      ,[c_default_bank]
      ,[c_default_branch]
      ,[c_default_drawer]
      ,[c_cosmos_transfer]
      ,[c_auth_owner]
      ,[c_auth_group]
      ,[c_validation_id]
      ,[c_ezyfreight_pricecode]
      ,[c_ezyfreight_volume]
      ,[c_ezyfreight_weight]
      ,[c_ezyfreight_pieces]
      ,[c_ezyfreight_zonemap]
      ,[c_ezyfreight_connote_format]
      ,[c_ezyfreight_connote_id]
      ,[c_ezyfreight_consolidate]
      ,[c_ezyfreight_insurance]
      ,[c_ezyfreight_insurance_choices]
      ,[c_ezyfreight_header]
      ,[c_ezyfreight_no_email]
      ,[c_ezyfreight_atl]
      ,[c_returns_pricecode]
      ,[c_last_check]
      ,[c_check_frequency]
      ,[c_cosmos_code]
      ,[c_update_format]
      ,[c_update_frequency]
      ,[c_update_ftp_host]
      ,[c_update_ftp_port]
      ,[c_update_ftp_user]
      ,[c_update_ftp_password]
      ,[c_update_ftp_directory]
      ,[c_update_email]
      ,[c_update_filename_format]
      ,[c_update_id]
      ,[c_update_passive]
      ,[c_export_special]
      ,[c_cubic_factor]
      ,[c_difot_failedok]
      ,[c_application_details]
      ,[c_notes]
      ,[c_ezyfreight_default_instructions]
      ,[c_ezyfreight_no_auto_release]
      ,[c_ezyfreight_cube_exempt]
      ,[c_daily_deliveries_email]
      ,[c_sms_on_pickup]
      ,[c_alternate_agents]
      ,[c_items_and_coupons]
      ,[c_no_oversize_warning]
      ,[c_obd_notify_mail]
	  ,[c_returns_available]
	   ,[c_booking_fixed_time]
	  ,[c_booking_from_first]
	  ,[c_booking_from_last]
	  ,[c_booking_auto_rebook]
	  ,[c_booking_consolidated]
	  ,c_labelless

	  );



truncate table companyaccount
INSERT INTO companyaccount select * from openquery(MySQLMain,'select * from cpplEDI.companyaccount');

truncate table users
INSERT INTO users select * from openquery(MySQLMain,'select * from cpplEDI.users');

truncate table usercompany
INSERT INTO usercompany select * from openquery(MySQLMain,'select * from cpplEDI.usercompany')


end
GO
