SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 22/05/2020
-- Description:	To get not billed consignments for the given date range
-- =============================================
-- Exec [sp_RPTConsignmentVsProntoBilling_Reconciliation] '2020-04-01','2020-06-30'
CREATE PROCEDURE [dbo].[sp_RPTConsignmentVsProntoBilling_Reconciliation]
	@FromDate Date = NULL
	,@ToDate Date = NULL
AS
BEGIN
	SET NOCOUNT ON;

	Select 
		Distinct 
		cd_connote [Consignment]
		, cd_date [CD_Date]
		, cd_account [Accountcode]
		, cd_pickup_addr0 [Sender]
		, cd_delivery_addr0 [Deliver To]
		, cd_manifest_id [Manifest]
		, CASE WHEN ISNULL(cd_last_status,'') ='' THEN 'No Activity' WHEN cd_last_status ='futile' THEN 'Futile' ELSE 'Has Activity' END [Activity]
		, CASE WHEN cd_test = 'Y' Then 'Yes' ELSE 'No' END [cd_test]
		, [cd_pricecode] ServiceCode
		, cd_company_id
	Into #tmpEDI
	From CpplEDI.dbo.consignment CG (nolock)
		Left Join Pronto.dbo.ProntoBilling PB (nolock) On CG.cd_connote = PB.ConsignmentReference
	Where CONVERT(date,cd_date) >= @FromDate and CONVERT(date,cd_date) <= @ToDate
		And PB.ConsignmentReference is null 
		
	
	Select T1. *,c_code [companycode], Pg.pg_name [pricegroup]
	From #tmpEDI T1
		Left Join companies C on T1.cd_company_id = C.c_id
		Left Join pricegroup2 pg on c.c_pricegroup_id = pg.pg_id	
	Where 
		cd_company_id NOT IN (2200,209,516,2393,2125,210,96)
		--AND ISNUMERIC([Consignment]) = 0
		--AND (
		--	ISNULL(ServiceCode,'') NOT IN ('15','55','BU0','BU1','BU10','BU15','BU2','BU20','BU25','BU3','BU5','BUK','BUKM','CTY','DS0','DS1','DS10','DS15','DS2','DS20','DS25','DS3','DS5','NPU','X53','CTYR','NDL','RTDP','RTHP','RTRN','UPS')
		--	AND ISNULL(ServiceCode,'') not like 'EXP%'
		--	AND ISNULL(ServiceCode,'') not like 'SAV%'
		--	AND ISNULL(ServiceCode,'') not like 'RDR%'
		--	)
		AND ISNULL(ServiceCode,'') NOT IN (Select Code From Pronto.dbo.tbl_ProntoEDIExclusionSubCodes)
		AND ISNULL(ServiceCode,'') not like 'EXP%'
		AND ISNULL(ServiceCode,'') not like 'SAV%'
		AND ISNULL(ServiceCode,'') not like 'RDR%'
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RPTConsignmentVsProntoBilling_Reconciliation]
	TO [ReportUser]
GO
