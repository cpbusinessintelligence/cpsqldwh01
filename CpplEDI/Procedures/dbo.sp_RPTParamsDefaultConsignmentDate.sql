SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 03/06/2020
-- Description:	To get default report params fromdate n todate
-- =============================================
CREATE PROCEDURE [sp_RPTParamsDefaultConsignmentDate] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Date Date;
	DECLARE @dow int;
	DECLARE @mod int;
	DECLARE @cd_date_Last Date;
	DECLARE @cd_date_start Date ;

	SET @Date = GETDATE();
	SET @dow = datepart(dw, @date);
	SEt @mod = CASE WHEN @dow IN (7, 1) THEN (@dow % 7) + 12 ELSE (@dow % 7) + 5 END;
	SET @cd_date_Last = cast(dateadd(d, - @mod, @date) AS date) ;
	SET @cd_date_start = cast(dateadd(d, - 14, @cd_date_Last) AS date);
	
	SELECT @cd_date_start [cd_date_start], @cd_date_Last [cd_date_last];
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RPTParamsDefaultConsignmentDate]
	TO [ReportUser]
GO
