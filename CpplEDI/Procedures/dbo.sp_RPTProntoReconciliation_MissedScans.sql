SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 28/05/2020
-- Description:	To get missed Pickup/Delivery scans for billed connotes
-- =============================================
-- Exec [sp_RPTProntoReconciliation_MissedScans] '2020-04-01','2020-06-30'
CREATE PROCEDURE [dbo].[sp_RPTProntoReconciliation_MissedScans]
	@FromDate Date = NULL
	,@ToDate Date = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
----Load EDI------------------------------------------------------------
	Select Suburb, Postcode,Right(MIN(Destination_ID),4) [Driver]
	Into #tmpCruchDriver
	From ScannerGateway..tbl_CrunchTable
	Group By Suburb, Postcode

	Select S.[Name] [Suburb],S.PostCode, MIN(S.Driver) [Driver]
	Into #tmpCosmosDriver
	From Cosmos..Suburb S
	Where EtaRangeFrom0 IS NOT NULL
	Group By S.[Name],S.PostCode

	Select 
		Case When T1.suburb IS NULL THEN T2.Suburb Else T1.suburb End [Suburb]
		,Case When T1.postcode IS NULL THEN T2.PostCode Else T1.postcode End [Postcode]
		,Case When T1.Driver IS NULL THEN T2.Driver Else T1.Driver End [Driver]
	Into #tmpDriver
	From #tmpCruchDriver T1
	FULL Join #tmpCosmosDriver T2 On T1.suburb = T2.Suburb and T1.postcode = T2.PostCode		
	---
	
	Select cd_date,cd_connote,cd_last_status,cd_test,cd_account,cd_pickup_suburb,cd_pickup_postcode,cd_delivery_suburb,cd_delivery_postcode,cd_pricecode,cd_company_id,cd_pickup_pay_driver,cd_deliver_driver
	Into #tmpCG 
	From CpplEDI.dbo.consignment (nolock)
	Where cd_date >= @FromDate and cd_date <= @ToDate
		And cd_test = 'N'

	Select 
		cd_date [Date]
		,cd_connote [SerialNumber]
		,CASE WHEN ISNULL(cd_last_status,'') ='' THEN 'No Activity' WHEN cd_last_status ='futile' THEN 'Futile' ELSE 'Has Activity' END [Activity_status]
		,cd_account
		,cd_pickup_suburb
		,cd_pickup_postcode
		,cd_pickup_pay_driver
		,PickupContractor
		,PickupDate
		,PickupRctiDate
		,PickupRctiAmount
		,cd_delivery_suburb
		,cd_delivery_postcode
		,cd_deliver_driver
		,DeliveryContractor
		,DeliveryDate
		,DeliveryRctiDate
		,DeliveryRctiAmount
		,RevenueAmount
		,'EDI' [Type]
		, cd_pricecode [ServiceCode]
		, cd_company_id		
	Into #tmpEDI
	From #tmpCG tCG 
		Join Pronto.dbo.ProntoCouponDetails PCD (nolock) On tCG.cd_connote = PCD.SerialNumber
	Where (DeliveryDate IS NULL OR PickupDate IS NULL)
	-----
	Select 
		[Date]
		,[SerialNumber]
		,[Activity_status]
		,cd_account
		,cd_pickup_suburb
		,cd_pickup_postcode
		,Case When cd_pickup_pay_driver = 0 Then tPD.Driver Else cd_pickup_pay_driver End cd_pickup_pay_driver
		,PickupContractor
		,PickupDate
		,PickupRctiDate
		,PickupRctiAmount
		,cd_delivery_suburb
		,cd_delivery_postcode
		,Case When cd_deliver_driver = 0 Then tDD.Driver Else cd_deliver_driver End cd_deliver_driver
		,DeliveryContractor
		,DeliveryDate
		,DeliveryRctiDate
		,DeliveryRctiAmount
		,RevenueAmount
		,[Type]
		, [ServiceCode]
	Into #tmpEDIFinal
	From #tmpEDI T1
		Left Join companies C on T1.cd_company_id = C.c_id
		Left Join pricegroup2 pg on c.c_pricegroup_id = pg.pg_id	
		Left Join #tmpDriver tPD On tPD.Suburb = T1.cd_pickup_suburb and tPD.postcode = T1.cd_pickup_postcode 
		Left Join #tmpDriver tDD On tDD.Suburb = T1.cd_delivery_suburb and tDD.postcode = T1.cd_delivery_postcode	
	Where 
		cd_company_id NOT IN (2200,209,516,2393,2125,210,96)
		AND ISNULL([ServiceCode],'') NOT IN (Select Code From Pronto.dbo.tbl_ProntoEDIExclusionSubCodes)
		AND ISNULL([ServiceCode],'') NOT LIKE 'EXP%'
		AND ISNULL([ServiceCode],'') NOT LIKE 'SAV%'
		AND ISNULL([ServiceCode],'') NOT LIKE 'RDR%'

----Load Prepaid--------------------------------------------------------
	--Select barcode,Max(SaleDateTime) SaleDateTime, Min(driverWareHouseNumber) driverWareHouseNumber 
	--Into #tmpCouponSales
	--From CpplEDI..tbl_couponsales
	--Group By barcode

	Select SerialNumber, Max(ActivityDateTime) As [ActivityDateTime] 
	Into #tmpAI
	From Cosmos.dbo.ActivityImport (nolock)
	Where ActivityDateTime >= @FromDate And ActivityDateTime <= @ToDate
	Group By SerialNumber
	
	Select 
		tAI.ActivityDateTime [Date]
		,tAI.SerialNumber [SerialNumber]
		,'Has Activity' As [Activity_status]
		,PSSN.Accountcode As cd_account
		,PD.Locality As cd_pickup_suburb
		,PD.Postcode As cd_pickup_postcode
		--,Convert(Varchar(4),RIGHT(driverWareHouseNumber,4)) As cd_pickup_pay_driver
		, '' As cd_pickup_pay_driver
		,PickupContractor
		,PickupDate
		,PickupRctiDate
		,PickupRctiAmount
		,'' As cd_delivery_suburb
		,'' As cd_delivery_postcode
		,'' As cd_deliver_driver
		,DeliveryContractor
		,DeliveryDate
		,DeliveryRctiDate
		,DeliveryRctiAmount
		,RevenueAmount
		,'Prepaid' [Type]
		,Substring(tAI.SerialNumber,1,3) [ServiceCode]	
	Into #tmpPrepaid
	From #tmpAI tAI
		Join Pronto.dbo.ProntoCouponDetails PCD (nolock) On tAI.SerialNumber = PCD.SerialNumber
		Left Join Pronto.dbo.ProntoStockSerialNumber PSSN (nolock) On PCD.StartSerialNumber = PSSN.SerialNumber
		Left Join Pronto.dbo.ProntoDebtor PD (nolock) On PSSN.Accountcode = PD.Accountcode
		--Left Join #tmpCouponSales T2 On PCD.StartSerialNumber = T2.barcode
	Where (PCD.DeliveryDate IS NULL OR PCD.PickupDate IS NULL)

	-------------------------------------
	SELECT
		CONVERT(DATE,[Date]) [Date]
		,[SerialNumber]
		,[Activity_status]
		,cd_account
		,cd_pickup_suburb
		,cd_pickup_postcode
		,CONVERT(VARCHAR(4),cd_pickup_pay_driver) AS cd_pickup_driver
		,PickupContractor
		,PickupDate
		--,PickupRctiDate
		--,PickupRctiAmount
		,cd_delivery_suburb
		,cd_delivery_postcode
		,CONVERT(VARCHAR(4),cd_deliver_driver) AS cd_deliver_driver
		,DeliveryContractor
		,DeliveryDate
		--,DeliveryRctiDate
		--,DeliveryRctiAmount
		,RevenueAmount
		,[Type]
		,[ServiceCode]
	FROM #tmpEDIFinal
	UNION
	SELECT
		CONVERT(DATE,[Date]) [Date]
		,[SerialNumber]
		,[Activity_status]
		,cd_account
		,cd_pickup_suburb
		,cd_pickup_postcode
		,CONVERT(VARCHAR(4),cd_pickup_pay_driver) AS cd_pickup_driver
		,PickupContractor
		,PickupDate
		--,PickupRctiDate
		--,PickupRctiAmount
		,cd_delivery_suburb
		,cd_delivery_postcode
		,CONVERT(VARCHAR(4),cd_deliver_driver) AS cd_deliver_driver
		,DeliveryContractor
		,DeliveryDate
		--,DeliveryRctiDate
		--,DeliveryRctiAmount
		,RevenueAmount
		,[Type]
		,[ServiceCode]
	FROM #tmpPrepaid
	WHERE Revenue.[dbo].[CPPL_fn_GetCouponTypeFromPrefix](LEFT(SerialNumber,3)) 
		NOT IN ('COD','REDELIVERY CPN','LINK - REGIONAL','PROMO','LINK','ATL','IRP TRK','RETURN TRK','REDELIVERY CARD','IRP')
			
END

GO
GRANT EXECUTE
	ON [dbo].[sp_RPTProntoReconciliation_MissedScans]
	TO [ReportUser]
GO
