SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptAmwayConsignmentsDueandNotDeliveredDetail] as
begin
    --'=====================================================================
    --' CP -Stored Procedure -[sp_RptAmwayConsignmentsDueandNotDeliveredDetail]
    --' ---------------------------
    --' Purpose: sp_RptAmwayConsignmentsDueandNotDeliveredDetail-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 24 Feb 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 24/02/2016    AB      1.00    Created the procedure                             --AB20160224

    --'=====================================================================

select cd_id,
       cd_connote,
	   convert(date,'') as NewETA,
	   convert(int,1000) as BusinessDays,
	   cd_eta_earliest as MinETA,
	   cd_date,
	  ---- cc_coupon as Label,
	  ---- cd_eta_earliest as MinETA,
	  ---- cd_customer_eta as MaxETA,
	  ---- cd_pickup_addr0 as PickupAddress0,
	  ---- cd_pickup_addr1 as  PickupAddress1,
	  ---- cd_pickup_addr2 as PickupAddress2,
	  ---- cd_pickup_addr3 as PickupAddress3,
	   cd_pickup_postcode as PickupPostcode,
	   cd_pickup_suburb as PickupSuburb,
	  ---- b.b_name as PickupBranch,
	  ---- case when b.b_name ='Adelaide' then 'SA'
   ----         when b.b_name='Brisbane' then 'QLD'
			----when b.b_name='Gold Coast' then 'QLD'
			----when b.b_name ='Melbourne' then 'VIC'
			----when b.b_name='Perth' then 'WA'
			----when b.b_name='Sydney' then 'NSW' else b.b_name end as PickupState,
	  ---- cd_pickup_contact as PickupContact,
	  ---- cd_pickup_contact_phone as PickupContactPhone,
   ----    cd_Delivery_addr0 as DeliveryAddress0,
	  ---- cd_Delivery_addr1 as  DeliveryAddress1,
	  ---- cd_Delivery_addr2 as DeliveryAddress2,
	  ---- cd_Delivery_addr3 as DeliveryAddress3,
	   cd_Delivery_postcode as DeliveryPostcode,
	   cd_delivery_suburb as DeliverySuburb,
	  ---- b1.b_name as DeliveryBranch, 
	  ---- case when b1.b_name ='Adelaide' then 'SA'
   ----         when b1.b_name='Brisbane' then 'QLD'
			----when b1.b_name='Gold Coast' then 'QLD'
			----when b1.b_name ='Melbourne' then 'VIC'
			----when b1.b_name='Perth' then 'WA'
			----when b1.b_name='Sydney' then 'NSW' else b1.b_name end as DeliveryState,                                                                                                                                                                                                                                                                                                                                               
   ----    cd_Delivery_contact as DeliveryContact,
	  ---- cd_Delivery_contact_phone as DeliveryContactPhone,
	   convert(datetime,'') as PickupDatetime,
	   convert(datetime,'') as InDepotDatetime,
	   convert(datetime,'') as OutForDeliveryDatetime,
	   convert(datetime,'') as AttemptedDeliveryDatetime,
	   convert(datetime,'') as DeliveryDatetime,
	    convert(varchar(100),'') as Event,
	   convert(varchar(100),'') as RedeliveryCard,
	   isnull(cd_last_status,'') as LastStatus
into #temp0
from cpplEDI.dbo.consignment c (NOLOCK) 
                               left join cpplEDI.dbo.Agents a (NOLOCK) on a.a_id=cd_agent_id 
                               left join cpplEDI.dbo.branchs b(NOLOCK)  on b.b_id=cd_pickup_branch
							   left join cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=cd_deliver_branch
where cd_account='112962311'  and cd_date>=dateadd(day,-30,getdate())  and a_cppl='Y' and a_shortname not in ('CPLINTERNATIONAL','CTI LOGISTICS') 


Update #temp0 set BusinessDays=[Transit Time]
from [dbo].[AmwayTransitTime] where [Postcode]=DeliveryPostcode and [Suburb]=DeliverySuburb

Update #temp0 set NewETA=MinETA where BusinessDays=1000

Update #temp0 set NewETA=DWH.dbo.[fn_CalculateWeekendsandPublicHolidays](convert(date,cd_date),(Select etazone from dwh.dbo.Postcodes where postcode=PickupPostcode and suburb=PickupSuburb),BusinessDays)
where NewETA='' or NewETA is null or NewETA='1900-01-01'


Select c.*,cc_coupon as Label
into #temp
from #temp0 c left join cpplEDI.dbo.cdcoupon cc (NOLOCK) on cc_consignment=cd_id
where NewETA=convert(date,getdate())
--convert(date,getdate())



Update #temp set PickupDatetime=eventdatetime from  #temp join scannergateway.dbo.trackingevent te(NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(Label)) where eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C' 
                                                                                                                                                       --'98EBB899-A15E-4826-8D05-516E744C466C'
Update #temp set InDepotDatetime=eventdatetime from  #temp join scannergateway.dbo.trackingevent te(NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(Label)) where eventtypeid='B8D04A85-A65B-41EA-9056-A950BE2CB509' 
  

Update #temp set OutForDeliveryDatetime=eventdatetime from  #temp join scannergateway.dbo.trackingevent te(NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(Label)) where eventtypeid='93B2E381-6A89-4F2E-9131-2DC2FB300941'


Update #temp set DeliveryDatetime=eventdatetime,Event='Delivered' from  #temp join scannergateway.dbo.trackingevent te(NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(Label)) where eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and isnull(additionaltext1,'') not like 'NH%'


Update #temp set AttemptedDeliveryDatetime=eventdatetime,Event='Attempted Delivery' from  #temp join scannergateway.dbo.trackingevent te(NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(Label)) where eventtypeid='FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'
and Event=''

Update #temp set RedeliveryCard=replace(AdditionalText1,'Link Coupon ','') from  #temp join scannergateway.dbo.trackingevent te(NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(Label)) where eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D'
and Event='Attempted Delivery' and (AdditionalText1 like 'Link Coupon 191%' or AdditionalText1 like 'Link Coupon %CNA')

Update #temp set RedeliveryCard=AdditionalText1 from  #temp join scannergateway.dbo.trackingevent te(NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(Label)) where eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
and RedeliveryCard='' and Event='Attempted Delivery' and isnull(AdditionalText1,'') like 'NHC%'

Update #temp set LastStatus = 'Pickup' where PickupDatetime is not null and PickupDatetime <>'1900-01-01 00:00:00.000'
Update #temp set LastStatus = 'In Depot' where InDepotDatetime is not null and InDepotDatetime <>'1900-01-01 00:00:00.000'
Update #temp set LastStatus = 'Out For Delivery' where OutForDeliveryDatetime is not null and OutForDeliveryDatetime <>'1900-01-01 00:00:00.000'
Update #temp set LastStatus = 'Attempted Delivery' where AttemptedDeliveryDatetime is not null and AttemptedDeliveryDatetime <>'1900-01-01 00:00:00.000'

Update #temp set LastStatus = 'No Activity' where LastStatus=''



Select *,case when RedeliveryCard like '191%' then 'Couriers Please'
              when RedeliveryCard like 'NHC%' then 'NewsAgent'
			  when RedeliveryCard like '%CNA%' then 'PopStation'
			  else '' end as TypeofRedelivery
			  
from #temp where  [Event]<>'Delivered' 

--Select 0 as cd_id,'ABC' as cd_connote,getdate() as PickupDatetime,getdate() as OutForDeliveryDatetime,getdate() as AttemptedDeliveryDatetime,'2016-02-24 12:00:00.000' as DeliveryDatetime,'Del' as Event,'XYZ' as RedeliveryCard,'WEC' as Label



end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptAmwayConsignmentsDueandNotDeliveredDetail]
	TO [ReportUser]
GO
