SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptAmwayConsignmentsDueandNotDeliveredSummary] as
begin
    --'=====================================================================
    --' CP -Stored Procedure -[sp_RptAmwayConsignmentsDueandNotDeliveredSummary]
    --' ---------------------------
    --' Purpose: sp_RptAmwayConsignmentsDueandNotDeliveredDetail-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 24 Feb 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 24/02/2016    AB      1.00    Created the procedure                             --AB20160224

    --'=====================================================================

select cd_id,
       cd_connote,
	   cd_date,
	  -- cc_coupon as Label,
	   cd_eta_earliest as MinETA,
	   cd_customer_eta as MaxETA,
	   convert(date,'') as NewETA,
	   convert(int,1000) as BusinessDays,
	   cd_pickup_addr0 as PickupAddress0,
	   cd_pickup_addr1 as  PickupAddress1,
	   cd_pickup_addr2 as PickupAddress2,
	   cd_pickup_addr3 as PickupAddress3,
       cd_pickup_postcode as PickupPostcode,
	   cd_pickup_suburb as PickupSuburb,
	   b.b_name as PickupBranch,
	   case when b.b_name ='Adelaide' then 'SA'
            when b.b_name='Brisbane' then 'QLD'
			when b.b_name='Gold Coast' then 'QLD'
			when b.b_name ='Melbourne' then 'VIC'
			when b.b_name='Perth' then 'WA'
			when b.b_name='Sydney' then 'NSW' else b.b_name end as PickupState,
	   cd_pickup_contact as PickupContact,
	   cd_pickup_contact_phone as PickupContactPhone,
       cd_Delivery_addr0 as DeliveryAddress0,
	   cd_Delivery_addr1 as  DeliveryAddress1,
	   cd_Delivery_addr2 as DeliveryAddress2,
	   cd_Delivery_addr3 as DeliveryAddress3,
	   cd_Delivery_postcode as DeliveryPostcode,
	   cd_delivery_suburb as DeliverySuburb,
	   b1.b_name as DeliveryBranch, 
	   case when b1.b_name ='Adelaide' then 'SA'
            when b1.b_name='Brisbane' then 'QLD'
			when b1.b_name='Gold Coast' then 'QLD'
			when b1.b_name ='Melbourne' then 'VIC'
			when b1.b_name='Perth' then 'WA'
			when b1.b_name='Sydney' then 'NSW' else b1.b_name end as DeliveryState,                                                                                                                                                                                                                                                                                                                                                
       cd_Delivery_contact as DeliveryContact,
	   cd_Delivery_contact_phone as DeliveryContactPhone,
	   convert(datetime,'') as DeliveryDatetime,
	   convert(varchar(100),'') as [Event],
	   convert(varchar(100),'') as RedeliveryCard
into #temp0
from cpplEDI.dbo.consignment c(NOLOCK) 
                               left join cpplEDI.dbo.Agents a(NOLOCK)  on a.a_id=cd_agent_id 
                               left join cpplEDI.dbo.branchs b(NOLOCK)  on b.b_id=cd_pickup_branch
							   left join cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=cd_deliver_branch
where cd_account='112962311' and cd_date>=dateadd(day,-30,getdate())  and a_cppl='Y' and a_shortname not in ('CPLINTERNATIONAL','CTI LOGISTICS') 


Select c.*,cc_coupon as Label
into #temp
from #temp0 c left join cpplEDI.dbo.cdcoupon cc(NOLOCK)  on cc_consignment=cd_id



Update #temp set DeliveryDatetime=eventdatetime,Event='Delivered' from #temp join scannergateway.dbo.trackingevent te(NOLOCK) on sourcereference=Label where eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and isnull(additionaltext1 ,'') not like 'NH%'

Update #temp set DeliveryDatetime=eventdatetime,Event='Attempted Delivery',RedeliveryCard=replace(AdditionalText1,'Link Coupon ','') from #temp join scannergateway.dbo.trackingevent te(NOLOCK) on sourcereference=Label where eventtypeid='FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'
and Event=''

Update #temp set BusinessDays=[Transit Time]
from [dbo].[AmwayTransitTime] where [Postcode]=DeliveryPostcode and [Suburb]=DeliverySuburb

Update #temp set NewETA=MinETA where BusinessDays=1000

Update #temp set NewETA=DWH.dbo.[fn_CalculateWeekendsandPublicHolidays](convert(date,cd_date),(Select etazone from dwh.dbo.Postcodes where postcode=PickupPostcode and suburb=PickupSuburb),BusinessDays)
where NewETA='' or NewETA is null or NewETA='1900-01-01'


--from [dbo].[AmwayTransitTime] where [Postcode]=DeliveryPostcode and [Suburb]=DeliverySuburb

--Select * from #temp

Select cd_connote as Connote,
       cd_date,
       MinETA,
	   MaxETA,
	   NewETA,
	   BusinessDays,
	   PickupAddress0,
	   PickupAddress1,
	   PickupAddress2,
	   PickupAddress3,
	   PickupPostcode,
	   PickupSuburb,
	   Pickupbranch,
	   PickupState,
	   PickupContact,
	   PickupContactPhone,
       DeliveryAddress0,
	   DeliveryAddress1,
	   DeliveryAddress2,
	   DeliveryAddress3,
	   DeliveryPostcode,
	   DeliverySuburb,
	   DeliveryBranch,
	   DeliveryState,
	   DeliveryContact,
	   DeliveryContactPhone,
	   count(*) as TotalItems,
	   sum(case when [Event]='Delivered' then 1 else 0 end ) as ItemsDelivered,
	   sum(case when [Event]='Attempted Delivery' then 1 else 0 end ) as FailedDeliveryItems
into #temp1
from #temp
group by cd_connote, cd_date,
       MinETA,
	   MaxETA,
	   NewETA,
	   BusinessDays,PickupAddress0,PickupAddress1,PickupAddress2,PickupAddress3,PickupPostcode,PickupSuburb,Pickupbranch,PickupState,PickupContact,PickupContactPhone,
DeliveryAddress0,DeliveryAddress1,DeliveryAddress2,DeliveryAddress3,DeliveryPostcode,DeliverySuburb,DeliveryBranch,DeliveryState,DeliveryContact,DeliveryContactPhone

Select * from #temp1 where NewETA =convert(date,getdate())
and TotalItems<>ItemsDelivered 

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptAmwayConsignmentsDueandNotDeliveredSummary]
	TO [ReportUser]
GO
