SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_RptAttemptedorCardLeftStatus_BUP20161102](@Account varchar(100)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_RptAttemptedorCardLeftStatus
    --' ---------------------------
    --' Purpose: sp_RptAttemptedorCardLeftStatus-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 22 Jul 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 22/07/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

select distinct cd_connote as Connote, cd_pickup_addr0 as PickupBusinessName,cd_pickup_contact as PickupContact,Sourcereference as [Tracking No] ,et.Description,Eventdatetime,isnull(Additionaltext1,'') as Additionaltext1,isnull(ExceptionReason,'') as ExceptionReason

from cpplEDI.dbo.consignment (NOLOCK) join cpplEDI.dbo.cdcoupon (NOLOCK) on cc_consignment=cd_id  join ScannerGateway.dbo.trackingevent te(NOLOCK) on te.SourceReference=cc_coupon join ScannerGateway.dbo.eventtype et(NOLOCK) on et.id=eventtypeid
where cd_account=@Account and cd_date=convert(date,dateadd(day,-1,getdate())) and et.Description in ('Attempted Delivery' ) 
--and convert(date,te.eventdatetime)=convert(date,getdate())
order by cd_connote, Sourcereference

end
GO
