SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE Proc [dbo].[sp_RptCancelledConnotesReport_SS06112017]
(
	@StartDate Datetime,
	@EndDate Datetime
)
AS

BEGIN

     --'=====================================================================
    --' CP -Stored Procedure sp_RptCancelledConnotesReport
    --' ---------------------------
    --' Purpose: 
    --' Developer: Sinshith
    --' Date: 20- Oct - 2017
    --' Copyright: 
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                    Bookmark
    --' ----          ---     ---     -----                                                                      -------
    --'=====================================================================


select 
cd_connote,
cd_date,
cc_coupon,
cd_id,
c_name
into #Test 
from cpplEDI.dbo.consignment C
Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
inner join [companyaccount] Acc
On(C.cd_company_id = Acc.ca_company_id)
inner join [companies] Com
On(Acc.ca_company_id = Com.c_id)
where cd_test = 'Y' 
and  cd_date between @StartDate and @EndDate
order by cd_date desc


select c_name as CustomerName,
cd_connote as ConsignmentNumber,
cd_date as ConsignmentDate,
Min(Te.EventDatetime) PickupDate,
Et.Description as Status
,Concat(Te.ExceptionReason,AdditionalText1,AdditionalText2) as ExceptionReason,
Dr.Code PickupDriverNum,
Dr.ProntoDriverCode as PickupProntoDrivercode,
Aud.ca_stamp as CancelledTime,
ca_account As CancelledPerson 
into #Pickup
from #Test Tes
join (select * from  [ScannerGateway].[dbo].[TrackingEvent_Archive] 
union all select * from  ScannerGateway..TrackingEvent) Te
on Tes.cc_coupon = Te.SourceReference
join ScannerGateway..Eventtype Et
On(Te.EventTypeId = Et.Id)
join ScannerGateway..[Driver] Dr
On(Te.DriverId = Dr.Id)
join [CpplEDI].[dbo].[cdaudit] Aud
On(Tes.cd_id = Aud.[ca_consignment])
where EventTypeId in ( '98EBB899-A15E-4826-8D05-516E744C466C')
 --and  cd_connote = 'CPAFXLC2319929'
 Group by
 c_name,
 cd_connote,
 cd_date,
 Et.Description,
 Concat(Te.ExceptionReason,AdditionalText1,AdditionalText2),
 Dr.Code,
 Dr.ProntoDriverCode,
 Aud.ca_stamp,
 ca_account


 select 
 c_name as CustomerName,
 cd_connote as ConsignmentNumber,
 cd_date as ConsignmentDate,
 Min(Te.EventDatetime) DeliveredDate ,
 Et.Description as Status,
 Concat(Te.ExceptionReason,AdditionalText1,AdditionalText2) as ExceptionReason,
 Dr.Code DeliveredDriverNum,
 Dr.ProntoDriverCode as DelProntoDrivercode,
 Aud.ca_stamp as CancelledTime,
 ca_account As CancelledPerson 
 into #Delivery
 from #Test Tes
join (select * from  [ScannerGateway].[dbo].[TrackingEvent_Archive] 
union all select * from  ScannerGateway..TrackingEvent) Te
on Tes.cc_coupon = Te.SourceReference
join ScannerGateway..Eventtype Et
On(Te.EventTypeId = Et.Id)
join ScannerGateway..[Driver] Dr
On(Te.DriverId = Dr.Id)
join [CpplEDI].[dbo].[cdaudit] Aud
On(Tes.cd_id = Aud.[ca_consignment])

where EventTypeId in ( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3')
 --and  cd_connote = 'CPAFXLC2319929'
 Group by
 c_name,
 cd_connote,
 cd_date,
 Et.Description,
 Concat(Te.ExceptionReason,AdditionalText1,AdditionalText2),
 Dr.Code,
 Dr.ProntoDriverCode,
 Aud.ca_stamp,
 ca_account


select distinct 
Pick.CustomerName,
Pick.ConsignmentNumber,
Pick.ConsignmentDate,
PickupDate,
Pick.ExceptionReason as PickupExceptionReason,
PickupDriverNum,
PickupProntoDrivercode,
DeliveredDate,
Del.ExceptionReason as DeliveryExceptionReason,
DeliveredDriverNum,
DelProntoDrivercode,
Pick.cancelledTime,
Pick.CancelledPerson into #Final
from 
#Pickup as Pick
Left join #Delivery as Del
on(Pick.ConsignmentNumber = Del.ConsignmentNumber)



update #Final
set CustomerName = CompanyName from 
#Final F
inner join
ezyfreight..tblconsignment Con with (Nolock)
on(F.ConsignmentNumber = Con.ConsignmentCode)
inner join ezyfreight..tblcompanyusers cu
on(Con.UserID = cu.UserID)
inner join ezyfreight..tblcompany com
on(cu.companyID = com.companyid)
where ConsignmentNumber like 'cpw%'

Select * from #Final


End



GO
