SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure sp_RptCellnetBilledConsignments (@StartDate date,@EndDate date) as
begin

Select Con.cd_connote
      ,cd_date
	  ,cd_items
	  ,[AccountCode]
      ,[AccountName]
      ,[AccountBillToCode]
      ,[AccountBillToName]
      ,[ServiceCode]
	  ,[BillingDate]
      ,[AccountingDate]
      ,[InvoiceNumber]
	  ,[DeclaredWeight]
      ,[DeadWeight]
      ,[DeclaredVolume]
      ,[Volume]
      ,[ChargeableWeight]
	  ,[BilledFreightCharge]
      ,[BilledFuelSurcharge]
      ,[BilledTransportCharge]
      ,[BilledInsurance]
      ,[BilledOtherCharge]
      ,[BilledTotal]
from   cpplEDI.dbo.consignment CON  left join Pronto.dbo.ProntoBilling p on replace(p.consignmentreference,'_R','')=con.cd_connote
	                                
	 Where CON.Cd_account in ('111224572','111222428','112821418','112821426')
	  and  Convert(date,CON.cd_date) >= @StartDate and   Convert(date,CON.cd_date) <= @EndDate

end
GO
