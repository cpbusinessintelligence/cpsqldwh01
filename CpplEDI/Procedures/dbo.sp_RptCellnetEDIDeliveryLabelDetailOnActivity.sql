SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptCellnetEDIDeliveryLabelDetailOnActivity](@StartDate date,@EndDate date) as
begin

	SET NOCOUNT ON;
	with X as
	(
	Select con.cd_connote ,
	        con.cd_company_id ,
			c.c_name as companyname,
	           CC.cc_coupon ,
			   a_name as AgentName,
			   a_cppl as CouriersPleaseAgent,
			   con.cd_customer_eta as ETADate,
				   Convert(Date,CON.cd_date) as ConDate ,
	               CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
									 and te.EventTypeId  in ('98EBB899-A15E-4826-8D05-516E744C466C','B8D04A85-A65B-41EA-9056-A950BE2CB509')
				                    ORDER BY te.EventDateTime ASC)) as FirstScanCouponID,
				  --CONVERT(Varchar(50),(select top 1 Id
				  --                  FROM TrackingEvent te (nolock)
				  --                  WHERE te.LabelId = l.Id  and l.LabelNumber = CON.cd_connote
				  --                  ORDER BY te.EventDateTime ASC)) as FirstScanConnoteID,                 
				   CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                    and te.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'
				                    ORDER BY te.EventDateTime ASC)) as PickUpScanCouponID,
				  --CONVERT(Varchar(max),(select top 1 exceptionreason
				  --                  FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				  --                  WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				  --                  and te.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'
				  --                  ORDER BY te.EventDateTime ASC)) as PickUpException,
					CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('B8D04A85-A65B-41EA-9056-A950BE2CB509')
				                    ORDER BY te.EventDateTime ASC)) as IndepotScanCouponID,
                   	CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('E293FFDE-76E3-4E69-BCEB-473F91B4350C','1A35AB45-B82A-492A-A1E8-6415BF846C75')
				                    ORDER BY te.EventDateTime ASC)) as TransferScanCouponID,	
					CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('F47CABB2-55AA-4F19-B5EE-C2754268D1AF')
				                    ORDER BY te.EventDateTime ASC)) as InTransitScanCouponID,	
					CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('93B2E381-6A89-4F2E-9131-2DC2FB300941')
				                    ORDER BY te.EventDateTime ASC)) as OutForDeliveryScanCouponID,
									
									
				 CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				                    ORDER BY te.EventDateTime ASC)) as AttemptedDeliveryScanCouponID,
									  
					--CONVERT(Varchar(max),(select top 1 exceptionreason
				 --                   FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				 --                   WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				 --                   and te.EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
				 --                   ORDER BY te.EventDateTime ASC)) as IndepotException,

				   CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
											'41A8F8F9-D57E-40F0-9D9D-97767AC3069E')
				                    ORDER BY te.EventDateTime ASC)) as DeliveryScanCouponID  ,

				CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				            ORDER BY te.EventDateTime desc)) as LastScanCouponID,

					--CONVERT(Varchar(max),(select top 1 exceptionreason
				 --                   FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				 --                   WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				 --                    AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
					--						'41A8F8F9-D57E-40F0-9D9D-97767AC3069E','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				 --                   ORDER BY te.EventDateTime ASC)) as DeliveryScanException,

			                   
                CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                    AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
											'41A8F8F9-D57E-40F0-9D9D-97767AC3069E',
											'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				            ORDER BY te.EventDateTime ASC)) as MeasureScanCouponID
                --   CONVERT(Varchar(50),(select top 1 Id
				            --        FROM ScannerGateway.dbo.TrackingEvent te (nolock)
				            --        WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				            --        AND   te.EventTypeId ='E293FFDE-76E3-4E69-BCEB-473F91B4350C'
				            --ORDER BY te.EventDateTime ASC)) as TransferScanCouponID				            
           --        CONVERT(Varchar(50),(select top 1 Id
				       --             FROM TrackingEvent te (nolock)
				       --             WHERE te.LabelId = l.Id  and l.LabelNumber = CON.cd_connote
				       --             AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
											--'41A8F8F9-D57E-40F0-9D9D-97767AC3069E',
											--'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				       --             ORDER BY te.EventDateTime ASC)) as MeasureScanConsignmentID	

	 from cpplEDI.dbo.consignment CON  left join cpplEDI.dbo.Agents on a_id=cd_agent_id 
	                                   left join cpplEDI.dbo.cdcoupon CC on CON.cd_id = CC.cc_consignment 
	                                   left join ScannerGateway.dbo.label l on  cc.cc_coupon = L.LabelNumber 
									   left join cpplEDI.dbo.companies c on c.c_id=cd_company_id
									   --left join cpplEDI.dbo.companyaccount on ca_account=
	 Where CON.Cd_account in ('111224572','111222428','112821418','112821426')
	  and  Convert(date,CON.cd_date) >= @StartDate and   Convert(date,CON.cd_date) <= @EndDate
	)  Select  
	
	
	       T.cd_Connote as Connote ,
	       T.ConDate,
	       T.cc_coupon  as LabelNumber,
		   AgentName,
		   CouriersPleaseAgent as [CouriersPleaseAgent?],
	       T.cd_company_id  as CompanyId,
		   companyname,
		 --  T.ETADate,
	       TE1.EventDateTime as FirtScanDateTime,
	       TE1.EventTypeId as FirstScanType,
		  -- dbo.CPPL_fn_CalculateBusinessDays(CONVERT(date, T.ConDate),CONVERT(date, TE1.EventDateTime), pb.id,pb.id, 1) as [CreationToFirstScanDays],
	       CONVERT(Varchar(50),'') as FirstScanDescription,
	       TE2.EventDateTime as PickupScanDateTime,
		   isnull(TE2.ExceptionReason ,'') as PickUpException,
		   TEI.EventDateTime as IndepotScanDateTime,
		   isnull(TEI.ExceptionReason,'') as IndepotException,
		   TR.EventDateTime as TransferScanDateTime,
		   TI.EventDateTime as InTransitScanDateTime,
		   --case when TR.EventDateTime is not null and CouriersPleaseAgent='Y' then 
		   TEA.EventDateTime as OutForDeliveryDateTime,
		   isnull(TEA.ExceptionReason,'') as OutForDeliveryException,
		   TEAt.EventDateTime as AttemptedDeliveryDateTime,
		   isnull(TEAt.ExceptionReason,'Other') as AttemptedDeliveryException,
	       TE3.EventDateTime as DeliveryScanDateTime,
		   isnull(TE3.ExceptionReason,'') as DeliveryException,
	      -- TE4.EventDateTime as MeasureScanDateTime,
		  TL.EventDateTime as LastScanDateTime,
		  Tle.Description as LastScanDescription,
		  isnull(TL.ExceptionReason,'') as LastScanException,
	       replace(TE4.ExceptionReason,'','') as ExceptionReason,
		   convert(varchar(50),'') as Status,
		    convert(varchar(max),'') as ExceptionInformation,
			 convert(varchar(50),'') as Code,
			 convert(varchar(max),'') as Description
           INTO #TEMp1
	         from X T left join ScannerGateway.dbo.Trackingevent TE1 on T.FirstScanCouponID = TE1.ID
	                      left  Join ScannerGateway.dbo.Trackingevent TE2 on T.PickUpScanCouponID  = TE2.ID
						  left  Join ScannerGateway.dbo.Trackingevent TEI on T.IndepotScanCouponID  = TEI.ID
						  left  Join ScannerGateway.dbo.Trackingevent TR on T.TransferScanCouponID  = TR.ID
						  left  Join ScannerGateway.dbo.Trackingevent TI on InTransitScanCouponID=TI.ID	
						   left  Join ScannerGateway.dbo.Trackingevent TL on T.LastScanCouponID=TL.ID
						   left  Join ScannerGateway.dbo.Eventtype TLe on Tle.id=TL.eventtypeid
						  Left Join ScannerGateway.dbo.Trackingevent TEA on T.OutForDeliveryScanCouponID = TEA.ID
						  Left Join ScannerGateway.dbo.Trackingevent TEAt on T.AttemptedDeliveryScanCouponID = TEAt.ID
						   Left Join ScannerGateway.dbo.Trackingevent TE3 on T.DeliveryScanCouponID = TE3.ID
						   LEft Join ScannerGateway.dbo.Trackingevent TE4 on T.MeasureScanCouponID = TE4.ID
						  -- LEft Join ScannerGateway.dbo.TrackingEvent TE5 on T.TransferScanCouponID  = TE5.ID
						   LEFT OUTER JOIN ScannerGateway.dbo.Driver pd (NOLOCK) ON ISNULL(TE1.driverid,TE2.driverid) = pd.Id 
	                       LEFT OUTER JOIN ScannerGateway.dbo.Branch pb (NOLOCK) ON pd.BranchId = pb.Id 

Update #TEMp1 SET FirstScanDescription =  E.Description  From #TEMp1 Join ScannerGateway.dbo.EventType E On #TEMp1.FirstScanType =E.Id 

Update #TEMp1 SET AttemptedDeliveryException='' where AttemptedDeliveryDateTime is null


Update #TEMp1 SET Status='Pickup',exceptioninformation=case when LastScanException ='Futile Pickup No Goods To Go' then 'Futile Pickup No Goods To Go'
                                       when LastScanException ='Futile Pickup Not Home Closed' then 'Futile Pickup No One Home'
									   when LastScanException ='' then 'Successful Pickup' else '' end 
							     ,code=case when LastScanException ='Futile Pickup No Goods To Go' then '102'
                                       when LastScanException ='Futile Pickup Not Home Closed' then '102'
									   when LastScanException ='' then '101' else '' end 
								  ,Description=case when LastScanException ='Futile Pickup No Goods To Go' then 'Failed Pickup (Futile)'
                                       when LastScanException ='Futile Pickup Not Home Closed' then 'Failed Pickup (Futile)'
									   when LastScanException ='' then 'Picked Up by Carrier' else '' end 
where LastScanDescription='pickup' 


Update #TEMp1 SET Status='In Depot',exceptioninformation=''
                                   ,code='301'
								  ,Description='Scanned in Depot' 
where LastScanDescription='In Depot' 





Update #TEMp1 SET Status='Transit',exceptioninformation='Consolidate for Linehaul'
                                   ,code='306'
								  ,Description='Scanned to Cage or CRYPAL'
where LastScanDescription='Consolidate' 

Update #TEMp1 SET Status='Agent',exceptioninformation='Transfer to Agent'
                                   ,code='302'
								  ,Description='In Transit - Onforward Via '+AgentName
where LastScanDescription in ('Transfer','HandOver') and [CouriersPleaseAgent?]='N'

Update #TEMp1 SET Status='transfer',exceptioninformation='Hand off to Othe Contractor'
                                   ,code='305'
								  ,Description='In Transit to Destination'
where LastScanDescription in ('Transfer','HandOver') and [CouriersPleaseAgent?]='Y'




Update #TEMp1 SET Status='Accept',exceptioninformation='Out for Delivery (OBD)'
                                   ,code='304'
								  ,Description='In Transit to Destination'
where LastScanDescription='Out For Delivery' 


Update #TEMp1 SET Status='Failed',exceptioninformation=isnull(c.exceptioninformation,'other')
                                   ,code=isnull(c.code,'261')
								  ,Description=isnull(c.description,'Attempted Delivery - Unable to Deliver - Other Reasons')
from #TEMp1 left join CellnetStatus c on  ltrim(rtrim(LastScanException))=ltrim(rtrim(c.exceptioninformation)) and c.status='failed'
where LastScanDescription='Attempted Delivery' 


Update #TEMp1 SET Status='Delivery',exceptioninformation=case when LastScanException='Delivered Damaged' then 'Delivered Damaged' else 'Delivered' end
                                   ,code='201'
								  ,Description=case when LastScanException='Delivered Damaged' then 'Successful delivery -  Delivered damaged' else 'Successful Delivery' end
where LastScanDescription='Delivered' 



Update #TEMp1 SET Status='Other',exceptioninformation='Other'
                                   ,code=''
								  ,Description=''
from #TEMp1 
where isnull(LastScanDescription,'') not in ('pickup' ,'In Depot','Consolidate' ,'Transfer','HandOver','Out For Delivery' ,'Attempted Delivery' ,'Delivered' )


Update #TEMp1 SET Status='No activity',exceptioninformation=''
                                   ,code=''
								  ,Description=''
from #TEMp1 
where LastScanDateTime is null



	Select * from #Temp1  




	SET NOCOUNT OFF;
END

GO
GRANT EXECUTE
	ON [dbo].[sp_RptCellnetEDIDeliveryLabelDetailOnActivity]
	TO [ReportUser]
GO
