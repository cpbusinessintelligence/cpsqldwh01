SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_RptCellnetEDIDeliveryLabelDetailOnActivity_BUP20160419](@StartDate date,@EndDate date) as
begin

	SET NOCOUNT ON;
	with X as
	(
	Select con.cd_connote ,
	        con.cd_company_id ,
			c.c_name as companyname,
	           CC.cc_coupon ,
			   con.cd_customer_eta as ETADate,
				   Convert(Date,CON.cd_date) as ConDate ,
	               CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
									 and te.EventTypeId  in ('98EBB899-A15E-4826-8D05-516E744C466C','B8D04A85-A65B-41EA-9056-A950BE2CB509')
				                    ORDER BY te.EventDateTime ASC)) as FirstScanCouponID,
				  --CONVERT(Varchar(50),(select top 1 Id
				  --                  FROM TrackingEvent te (nolock)
				  --                  WHERE te.LabelId = l.Id  and l.LabelNumber = CON.cd_connote
				  --                  ORDER BY te.EventDateTime ASC)) as FirstScanConnoteID,                 
				   CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                    and te.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'
				                    ORDER BY te.EventDateTime ASC)) as PickUpScanCouponID,
					CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('B8D04A85-A65B-41EA-9056-A950BE2CB509')
				                    ORDER BY te.EventDateTime ASC)) as IndepotScanCouponID,
				   CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
											'41A8F8F9-D57E-40F0-9D9D-97767AC3069E','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				                    ORDER BY te.EventDateTime ASC)) as DeliveryScanCouponID  ,
				                                   
                   CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.vw_Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                    AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
											'41A8F8F9-D57E-40F0-9D9D-97767AC3069E',
											'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				            ORDER BY te.EventDateTime ASC)) as MeasureScanCouponID
                --   CONVERT(Varchar(50),(select top 1 Id
				            --        FROM ScannerGateway.dbo.TrackingEvent te (nolock)
				            --        WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				            --        AND   te.EventTypeId ='E293FFDE-76E3-4E69-BCEB-473F91B4350C'
				            --ORDER BY te.EventDateTime ASC)) as TransferScanCouponID				            
           --        CONVERT(Varchar(50),(select top 1 Id
				       --             FROM TrackingEvent te (nolock)
				       --             WHERE te.LabelId = l.Id  and l.LabelNumber = CON.cd_connote
				       --             AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
											--'41A8F8F9-D57E-40F0-9D9D-97767AC3069E',
											--'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				       --             ORDER BY te.EventDateTime ASC)) as MeasureScanConsignmentID	

	 from cpplEDI.dbo.consignment CON  left join cpplEDI.dbo.cdcoupon CC on CON.cd_id = CC.cc_consignment 
	                                   left join ScannerGateway.dbo.label l on  cc.cc_coupon = L.LabelNumber 
									   left join cpplEDI.dbo.companies c on c.c_id=cd_company_id
									   --left join cpplEDI.dbo.companyaccount on ca_account=
	 Where CON.Cd_account in ('111224572','111222428','112821418','112821426')
	  and  Convert(date,CON.cd_date) >= @StartDate and   Convert(date,CON.cd_date) <= @EndDate
	)  Select  
	
	
	       T.cd_Connote as Connote ,
	       T.ConDate,
	       T.cc_coupon  as LabelNumber,
	       T.cd_company_id  as CompanyId,
		   companyname,
		 --  T.ETADate,
	       TE1.EventDateTime as FirtScanDateTime,
	       TE1.EventTypeId as FirstScanType,
		  -- dbo.CPPL_fn_CalculateBusinessDays(CONVERT(date, T.ConDate),CONVERT(date, TE1.EventDateTime), pb.id,pb.id, 1) as [CreationToFirstScanDays],
	       CONVERT(Varchar(50),'') as FirstScanDescription,
	       TE2.EventDateTime as PickupScanDateTime,
		   TEI.EventDateTime as IndepotScanDateTime,
	       TE3.EventDateTime as DeliveryScanDateTime,
	      -- TE4.EventDateTime as MeasureScanDateTime,
	       replace(TE4.ExceptionReason,'','') as ExceptionReason
	      -- TE4.EventTypeId as MEasureScanType,
	     -- CONVERT(Varchar(50),'') as MeasureScanDescription,
	      -- TE5.EventDateTime as TransferScanDateTime

	       

           INTO #TEMp1
	         from X T left join ScannerGateway.dbo.vw_Trackingevent TE1 on T.FirstScanCouponID = TE1.ID
	                      left  Join ScannerGateway.dbo.vw_Trackingevent TE2 on T.PickUpScanCouponID  = TE2.ID
						  left  Join ScannerGateway.dbo.vw_Trackingevent TEI on T.IndepotScanCouponID  = TEI.ID
						   Left Join ScannerGateway.dbo.vw_Trackingevent TE3 on T.DeliveryScanCouponID = TE3.ID
						   LEft Join ScannerGateway.dbo.vw_Trackingevent TE4 on T.MeasureScanCouponID = TE4.ID
						  -- LEft Join ScannerGateway.dbo.TrackingEvent TE5 on T.TransferScanCouponID  = TE5.ID
						   LEFT OUTER JOIN ScannerGateway.dbo.Driver pd (NOLOCK) ON ISNULL(TE1.driverid,TE2.driverid) = pd.Id 
	                       LEFT OUTER JOIN ScannerGateway.dbo.Branch pb (NOLOCK) ON pd.BranchId = pb.Id 

Update #TEMp1 SET FirstScanDescription =  E.Description From #TEMp1 Join ScannerGateway.dbo.EventType E On #TEMp1.FirstScanType =E.Id 
--Update #TEMp1 SET MeasureScanDescription =  E.Description From #TEMp1 Join ScannerGateway.dbo.EventType E On #TEMp1.MEasureScanType  =E.Id 


--Select Connote, 
      
--	   companyname,
--       CONVERT(Varchar(100),'') as Sender,
--       MAX(Condate) as ConsignmentDate,
--       MIN(LabelNumber) AS LabelNumber,
--       COUNT(*) as ItemCount,
--       MIN (cd_company_id) as cd_company_id,
--       MIN(FirtScanDateTime) as FirtScanDateTime,
--       MAX(FirstScanDescription) as FirstScanDescription,
--       MIN(PickupScanDateTime) as PickupScanDateTime,
--	   MIN(DeliveryScanDateTime) as DeliveryScanDateTime,
--	   MIN(MeasureScanDateTime) as MeasureScanDateTime,
--	   MAX(ExceptionReason) as ExceptionReason ,
--	   MAX(MeasureScanDescription) as MeasureScanDescription,
--	   MAX(TransferScanDateTime) as TransferScanDateTime,
--	   CONVERT(varchar(50),'') As PerformanceReason ,
--	   [CreationToFirstScanDays],
--	   ETADate as OriginalETA,
--	   CONVERT(Datetime,Null) as NewETA,
--	   CONVERT(Varchar(50),'') as FromSuburb,
--	   CONVERT(Varchar(50),'') as FromPostCode,
--	   CONVERT(Varchar(50),'') as FromZone,
--	   CONVERT(Varchar(50),'') as ToSuburb,
--	   CONVERT(Varchar(50),'') as ToPostCode,
--	   CONVERT(Varchar(50),'') as ToZone
-- INTO #Temp2
-- from #Temp1
-- Group by Connote,[CreationToFirstScanDays],ETADate  --Where PerformanceReason in('', '','')
 	
-- Update #Temp2 SET Sender = C.cd_pickup_addr0           ,FromSuburb  =UPPER(C.cd_pickup_suburb),FromPostCode =C.cd_pickup_postcode,ToPostCode =C.cd_delivery_postcode,ToSuburb = C.cd_delivery_suburb       From  #Temp2 T Join  cpplEDI.dbo.Consignment C On T.Connote = C.cd_connote
 
-- Update #Temp2 SET FromZOne  = etazone From #Temp2 T Join CouponCalculator.dbo.Postcodes D on  T.FromPostCode = D.Postcode and T.FromSuburb = D.Suburb
-- Update #Temp2 SET ToZOne  = etazone From #Temp2 T  Join CouponCalculator.dbo.Postcodes D on  T.ToPostCode = D.Postcode and T.ToSuburb = D.Suburb
-- --Update #Temp2 SET ToZOne  = ISNULL(D.PrimaryBillZone, ISNULL(D.SecondaryBillZone, '')) From #Temp2 T  Join CouponCalculator.dbo.Postcodes D on  T.ToPostCode = D.Postcode  and D.CompanyCode = 'CL1' and T.ToZone = ''
 
--Update #Temp2 SET PerformanceReason = 'No Activity' where FirtScanDateTime is null and MeasureScanDateTime is null and TransferScanDateTime is null

--Update #temp2 set NewETA=dbo.CPPL_fn_CalculateNewConsignmentEta(
--						CONVERT(date, FirtScanDateTime),
--						(dbo.CPPL_fn_CalculateBusinessDays(ConsignmentDate, OriginalETA, Null, Null, 0)),
--						Null)
--	WHERE FirtScanDateTime  Is Not Null;

--Update #temp2 set NewETA=OriginalETA where NewETA is null


----Update #Temp2 SET ETADelivery = dbo.[CPPL_fn_SKynetCalculateDPerformanceETA_backup] (FromZone,ToZone, cd_company_id , FirtScanDateTime) WHere FirtScanDateTime is not null
--Update #Temp2 Set  MeasureScanDateTime = TransferScanDateTime , MeasureScanDescription = 'Transfer to Agent',PerformanceReason = 'Agent Transfer' Where MeasureScanDateTime is null and TransferScanDateTime is not null
--Update #Temp2 Set  PerformanceReason =CASE  WHEN DATEDIFF(MI,NewETA,MeasureScanDateTime) >=0 THEN 'Not On Time' ELSE 'On Time' END  Where PerformanceReason ='' and MeasureScanDateTime is not null

--Update #Temp2 Set  PerformanceReason = 'In Progress' Where PerformanceReason = ''    and NewETA  > Getdate()
--Update #Temp2 Set  PerformanceReason = 'Not on Time' Where PerformanceReason = ''    and NewETA  < Getdate()


--If @Fromzone='ALL'
	Select * from #Temp1  
	--order by PerformanceReason desc
	--order by PerformanceReason desc 
	--else
	--Select * from #Temp2 where FromZone=@FromZone order by PerformanceReason desc 



	SET NOCOUNT OFF;
END

GO
