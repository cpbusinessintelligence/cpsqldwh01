SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_RptConsCountAgentwise
(
@StartDate date,
@EndDate date
)
as
Begin

 --'=====================================================================
    --' CP -Stored Procedure [sp_RptConsCountAgemtwise] '',''
    --' ---------------------------
    --' Purpose: Get the list of count of Consignments Agentwise
    --' Developer: Sinshith
    --' Date: 28 09 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------

    --'=====================================================================
SELECT cd_id,
	   a_name,
	   concat(DATEPART(year,cd_date) ,0,  DATEPART(MONTH,cd_date)) as [Month],
     -- [cd_date],
	   [cd_connote]
	   into #Temp1
  FROM [CpplEDI].[dbo].[consignment] Cons with (Nolock)
  INNER JOIN 
       [CpplEDI].[dbo].[Agents] Ag with (Nolock)
	   ON (Cons.cd_agent_id =Ag.a_id) 
WHERE convert(date,cd_date) between @StartDate and @EndDate
AND a_CPPL = 'N'

Select 
      [Month],
      a_name,
      count([cd_connote]) as [No of Consignments],
      count(cc_coupon) as [No of Labels]    
 FROM #Temp1 a
 INNER JOIN CpplEDI.dbo.cdcoupon Lab
 ON (a.cd_id = Lab.cc_consignment)
 GROUP BY [Month],a_name
 ORDER BY [Month] 

 End
GO
GRANT CONTROL
	ON [dbo].[sp_RptConsCountAgentwise]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptConsCountAgentwise]
	TO [ReportUser]
GO
