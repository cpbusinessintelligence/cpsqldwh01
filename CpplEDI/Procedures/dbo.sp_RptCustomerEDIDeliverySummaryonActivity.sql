SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptCustomerEDIDeliverySummaryonActivity](@Account varchar(50),@StartDate date,@EndDate date) as
begin

    --'=====================================================================
    --' CP -Stored Procedure -[sp_RptCustomerEDIDeliverySummaryonActivity]
    --' ---------------------------
    --' Purpose:Weekly Ontime Performance Report on Redeliveries-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 25 MAy 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 25/05/2016    AB      1.00    Created the procedure                             --AB20160525

    --'=====================================================================

with X as
	(
	Select con.cd_connote ,
	        con.cd_company_id ,
			c.c_name as CompanyName,
	           CC.cc_coupon ,
			   Convert(Date,CON.cd_date) as ConDate ,
			   con.cd_customer_eta as ETADate,
			   (Select top 1  cr_reference 
			       from cdref where cr_consignment=con.cd_id order by cr_reference)as Reference,
                case when  cd_deliver_driver is not null and cd_deliver_driver<>0 then 'Delivered'
				     when (cd_accept_driver is not null and cd_accept_driver<>0)or (cd_toagent_driver is not null and cd_toagent_driver<>0) then 'Out for Del'
				     when cd_indepot_driver is not null and cd_indepot_driver<>0 then 'In Depot'
					 when cd_pickup_driver is not null and cd_pickup_driver<>0 then  'Picked Up'
                     else 'No Activity' end as Status,
			   cd_Delivery_addr0 as DeliveryAddress0,
	   cd_Delivery_addr1 as  DeliveryAddress1,
	   cd_Delivery_addr2 as DeliveryAddress2,
	   cd_Delivery_addr3 as DeliveryAddress3,
	   cd_Delivery_postcode as DeliveryPostcode,
	   cd_delivery_suburb as DeliverySuburb,
	   b1.b_name as DeliveryBranch, 
	   case when b1.b_name ='Adelaide' then 'SA'
            when b1.b_name='Brisbane' then 'QLD'
			when b1.b_name='Gold Coast' then 'QLD'
			when b1.b_name ='Melbourne' then 'VIC'
			when b1.b_name='Perth' then 'WA'
			when b1.b_name='Sydney' then 'NSW' else b1.b_name end as DeliveryState,
			  cd_pickup_addr0 as PickupAddress0,
	   cd_pickup_addr1 as  PickupAddress1,
	   cd_pickup_addr2 as PickupAddress2,
	   cd_pickup_addr3 as PickupAddress3,
       cd_pickup_postcode as PickupPostcode,
	   cd_pickup_suburb as PickupSuburb,
	   b.b_name as PickupBranch,
	   [cd_deliver_stamp] as PODDate,
	   cd_deliver_pod as PODName,
	   case when b.b_name ='Adelaide' then 'SA'
            when b.b_name='Brisbane' then 'QLD'
			when b.b_name='Gold Coast' then 'QLD'
			when b.b_name ='Melbourne' then 'VIC'
			when b.b_name='Perth' then 'WA'
			when b.b_name='Sydney' then 'NSW' else b.b_name end as PickupState, 
			 cd_pricecode as ServiceCode,
			 con.cd_deliver_stamp as DeliverydatefromOriginalConsignment,
	                CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
									 and te.EventTypeId  in ('98EBB899-A15E-4826-8D05-516E744C466C','B8D04A85-A65B-41EA-9056-A950BE2CB509')
				                    ORDER BY te.EventDateTime ASC)) as FirstScanCouponID,                
				   CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                    and te.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'
				                    ORDER BY te.EventDateTime ASC)) as PickUpScanCouponID,
				   CONVERT(Varchar(50),(select top 1 Id
				                    FROM ScannerGateway.dbo.Trackingevent te (nolock)
				                    WHERE te.LabelId = l.Id  and l.LabelNumber = CC.cc_coupon
				                     AND   te.EventTypeId IN ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
											'41A8F8F9-D57E-40F0-9D9D-97767AC3069E',
											'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
				                    ORDER BY te.EventDateTime ASC)) as DeliveryScanCouponID
	 from cpplEDI.dbo.consignment CON (NOLOCK) left join cpplEDI.dbo.cdcoupon CC (NOLOCK) on CON.cd_id = CC.cc_consignment 
	                                   left join ScannerGateway.dbo.label l (NOLOCK) on  cc.cc_coupon = L.LabelNumber 
									   left join cpplEDI.dbo.Agents a(NOLOCK)  on a.a_id=cd_agent_id 
                               left join cpplEDI.dbo.branchs b(NOLOCK)  on b.b_id=cd_pickup_branch
							   left join cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=cd_deliver_branch
							   left join cpplEDI.dbo.companies c on c.c_id=cd_company_id
							 --  left join cpplEDI.dbo.cdref cr on cr.cr_consignment=cd_id
	 Where CON.Cd_account in  (select * from  [DWH].[dbo].[fn_GetMultipleValues](@Account)) 
	  --and cd_connote='S50384815'
	  and Convert(date,CON.cd_date) >= @StartDate and   Convert(date,CON.cd_date) <= @EndDate
	)  Select  
	
	
	       T.cd_Connote as Connote ,
	       T.ConDate,
	       T.cc_coupon  as LabelNumber,
		   T.Reference,
	       T.cd_company_id ,
		   T.CompanyName,
		   T.ETADate,
		   T.Status,
		    T.DeliveryAddress0,
	   T.DeliveryAddress1,
	   T.DeliveryAddress2,
	   T.DeliveryAddress3,
	   T.DeliveryPostcode,
	   T.DeliverySuburb,
	  T.DeliveryBranch, 
	  T.DeliveryState,
	  T.PickupAddress0,
	  T.PickupAddress1,
	  T.PickupAddress2,
	  T.PickupAddress3,
       T.PickupPostcode,
	   T.PickupSuburb,
	  T.PickupBranch,
	  T.PODDate,
	  T.PODName,
	   T.PickupState, 
		T.ServiceCode,
	       TE1.EventDateTime as FirtScanDateTime,
	       TE1.EventTypeId as FirstScanType,
		   dbo.CPPL_fn_CalculateBusinessDays(CONVERT(date, T.ConDate),CONVERT(date, TE1.EventDateTime), pb.id,pb.id, 1) as [CreationToFirstScanDays],
	       CONVERT(Varchar(50),'') as FirstScanDescription,
	       TE2.EventDateTime as PickupScanDateTime,
	       TE3.EventDateTime as DeliveryScanDateTime,
		 --  dbo.CPPL_fn_CalculateBusinessDays(te1.EventDateTime, ISNULL(Isnull(te3.eventdatetime, T.DeliverydatefromOriginalConsignment), T.PODDate), pb.Id, ISNULL(db.Id, pb.Id), 1) AS [First Scan To Delivery Days],
		   convert(datetime,null) as NewETA
	     --  TE4.EventDateTime as MeasureScanDateTime,
	     --  replace(TE4.ExceptionReason,'','') as ExceptionReason,
	     --  TE4.EventTypeId as MEasureScanType,
	      -- CONVERT(Varchar(50),'') as MeasureScanDescription,
	      -- TE5.EventDateTime as TransferScanDateTime

	       

           INTO #TEMp1
	         from X T left join ScannerGateway.dbo.Trackingevent TE1 (NOLOCK) on T.FirstScanCouponID = TE1.ID
	                      left  Join ScannerGateway.dbo.Trackingevent TE2  (NOLOCK) on T.PickUpScanCouponID  = TE2.ID
						   Left Join ScannerGateway.dbo.Trackingevent TE3(NOLOCK)  on T.DeliveryScanCouponID = TE3.ID
						 --  LEft Join ScannerGateway.dbo.Trackingevent TE4 on T.MeasureScanCouponID = TE4.ID
						 --  LEft Join ScannerGateway.dbo.Trackingevent TE5 on T.TransferScanCouponID  = TE5.ID
						   LEFT OUTER JOIN ScannerGateway.dbo.Driver pd (NOLOCK) ON ISNULL(TE1.driverid,TE2.driverid) = pd.Id 
	                       LEFT OUTER JOIN ScannerGateway.dbo.Branch pb (NOLOCK) ON pd.BranchId = pb.Id 
						   LEFT OUTER JOIN ScannerGateway.dbo.Driver dd (NOLOCK) ON Isnull( te3.DriverId,'') = dd.Id 
						   LEFT OUTER JOIN ScannerGateway.dbo.Branch db (NOLOCK) ON dd.BranchId = db.Id 


						   
Update #TEMp1 SET FirstScanDescription =  E.Description From #TEMp1 Join ScannerGateway.dbo.EventType E On #TEMp1.FirstScanType =E.Id 

Update #temp1 set NewETA=dbo.CPPL_fn_CalculateNewConsignmentEta(
						CONVERT(date, FirtScanDateTime),
						(dbo.CPPL_fn_CalculateBusinessDays(ConDate, ETADate, Null, Null, 0)),
						Null)
	WHERE FirtScanDateTime  Is Not Null;

Update #temp1 set NewETA=ETADate where NewETA is null or convert(date,NewETA)< convert(date,ETADate)



Select     Connote ,
	       ConDate,
	        --LabelNumber,
		   min(Reference) as Reference,
	       cd_company_id ,
		   CompanyName,
		   ETADate as GatewayETA,
		   NewETA,
		   
		   DeliveryAddress0,
	   DeliveryAddress1,
	   DeliveryAddress2,
	   DeliveryAddress3,
	   DeliveryPostcode,
	   DeliverySuburb,
	  DeliveryBranch, 
	  DeliveryState,
	  PickupAddress0,
	  PickupAddress1,
	  PickupAddress2,
	  PickupAddress3,
      PickupPostcode,
	  PickupSuburb,
	  PickupBranch,
	  min(isnull(PODDate,DeliveryScanDateTime)) as PODDate,
	  min(PODName) as PODName,
	   PickupState, 
		ServiceCode,
     	MIN(CASE WHEN 
	  CONVERT(date, (CASE WHEN ISNULL(DeliveryScanDateTime, '1jan1900') <> '1jan1900'
		                  THEN convert(date,DeliveryScanDateTime)
		                  ELSE  '1jan2099' end)) > NewETA 
		THEN 'N'
		ELSE 'Y'
	END) AS [DeliveredOnTime?],
	MAX(CASE WHEN  FirtScanDateTime is null and DeliveryScanDateTime is null 
		THEN 'N'
		ELSE 'Y'
	END) AS [HasActivity?],
	CASE WHEN (SUM(ISNULL(CASE WHEN (ISNULL(DeliveryScanDateTime, '1jan1900') = '1jan1900' )
				THEN 0.000
				ELSE 1.000
			END, 0)) / COUNT(*)) < 1.000
		THEN 'N'
		ELSE 'Y'
	END AS [DeliveredInFull?],
	SUM(ISNULL(CASE WHEN (ISNULL(DeliveryScanDateTime, '1jan1900') = '1jan1900' )
				THEN 0
				ELSE 1
	END, 0)) AS [ItemsDelivered],
	COUNT(*) AS [ItemsTotal],
	 --  FirtScanDateTime,
	 --  FirstScanType,
	 --  [First Scan To Delivery Days],
	 --  FirstScanDescription,
	 --  PickupScanDateTime,
	  min( DeliveryScanDateTime) as DeliveryScanDateTime,
	   convert(varchar(50),'') as FromZone,
	   convert(varchar(50),'') as ToZone,
	   convert(varchar(60),'') as ETA,
	   Status
	  -- convert(varchar(100),'') as PerformanceReason
 INTO #Temp2
 from #Temp1
 Group by Connote ,
	       ConDate,
	       --LabelNumber,
	       cd_company_id ,
		   CompanyName,
		   ETADate,
		   NewETA,
		   DeliveryAddress0,
	   DeliveryAddress1,
	   DeliveryAddress2,
	   DeliveryAddress3,
	   DeliveryPostcode,
	   DeliverySuburb,
	  DeliveryBranch, 
	  DeliveryState,
	  PickupAddress0,
	  PickupAddress1,
	  PickupAddress2,
	  PickupAddress3,
      PickupPostcode,
	  PickupSuburb,
	  PickupBranch,
	 -- isnull(PODDate,DeliveryScanDateTime),
	   PickupState, 
		ServiceCode,
		Status
 	
 
 Update #Temp2 SET FromZOne  = etazone From #Temp2 T Join CouponCalculator.dbo.Postcodes D on  T.PickupPostcode = D.Postcode and T.PickupSuburb = D.Suburb
 Update #Temp2 SET ToZOne  = etazone From #Temp2 T  Join CouponCalculator.dbo.Postcodes D on  T.DeliveryPostCode = D.Postcode and T.DeliverySuburb = D.Suburb
 Update #Temp2 SET ETA=e.ETA  from CouponCalculator.dbo.etacalculator e where #Temp2.fromzone=e.fromzone and #Temp2.tozone=e.tozone
 Update #Temp2 SET [DeliveredOnTime?]='' where  DeliveryscanDatetime is null     and convert(date,NewETA)  > convert(date,Getdate())

 
-- --Update #Temp2 SET ToZOne  = ISNULL(D.PrimaryBillZone, ISNULL(D.SecondaryBillZone, '')) From #Temp2 T  Join CouponCalculator.dbo.Postcodes D on  T.ToPostCode = D.Postcode  and D.CompanyCode = 'CL1' and T.ToZone = ''
 
----Update #Temp2 SET PerformanceReason = 'No Activity' where FirtScanDateTime is null and MeasureScanDateTime is null and TransferScanDateTime is null

----Update #Temp2 Set  PerformanceReason =CASE  WHEN DATEDIFF(MI,NewETA,MeasureScanDateTime) >=0 THEN 'Not On Time' ELSE 'On Time' END  Where PerformanceReason ='' and MeasureScanDateTime is not null


--Update #Temp2 Set  PerformanceReason = 'In Progress' Where DeliveryscanDatetime is null     and convert(date,NewETA)  > convert(date,Getdate())
--Update #Temp2 Set  PerformanceReason = 'Not on Time' Where PerformanceReason = ''    and convert(date,NewETA)  < convert(date,Getdate())


Select Con.cd_connote
	  ,[AccountCode] as ChildAccountCode
      ,[AccountName] as ChildAccountName
      ,[AccountBillToCode] as ParentAccountCode
      ,[AccountBillToName] as ParentAccountName
	  ,[BillingDate]
      ,[AccountingDate]
      ,[InvoiceNumber]
	  ,[BilledFreightCharge]
      ,[BilledFuelSurcharge]
      ,[BilledTransportCharge]
      ,[BilledInsurance]
      ,[BilledOtherCharge]
      ,[BilledTotal]
	  into #tempBill
from   cpplEDI.dbo.consignment CON (NOLOCK) left join Pronto.dbo.ProntoBilling p  on replace(p.consignmentreference,'_R','')=con.cd_connote
Where CON.Cd_account in (select * from  [DWH].[dbo].[fn_GetMultipleValues](@Account)) 
	  and  Convert(date,CON.cd_date) >= @StartDate and   Convert(date,CON.cd_date) <= @EndDate






----If @Fromzone='ALL'
	Select t.*,datepart(year,ConDate) as CalendarYear,datepart(Month,ConDate) as CalendarMonth,datepart(week,ConDate)  as CalendarWeek 
	       ,ChildAccountCode
           ,ChildAccountName
           ,ParentAccountCode
           ,ParentAccountName
           --,[ServiceCode]
	      ,[BillingDate]
           ,[AccountingDate]
           ,[InvoiceNumber]
	       ,[BilledFreightCharge]
           ,[BilledFuelSurcharge]
           ,[BilledTransportCharge]
           ,[BilledInsurance]
           ,[BilledOtherCharge]
           ,[BilledTotal]
	from #Temp2 t left join #tempBill t1 on cd_connote=connote
	--order by PerformanceReason desc
	--order by PerformanceReason desc 
	--else
	--Select * from #Temp2 where FromZone=@FromZone order by PerformanceReason desc 

 --select * from #tempbill
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptCustomerEDIDeliverySummaryonActivity]
	TO [ReportUser]
GO
