SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--[sp_RptCustomerStatusUpdates_AdstoneHourly_PV]
-- =============================================
CREATE  PROCEDURE [dbo].[sp_RptCustomerStatusUpdates_AdstoneHourly_PV_20200908]  
AS
BEGIN

	SET FMTONLY OFF 
	SET NOCOUNT ON;

	--set @AccountCode='112976535'
	Declare @Date datetime
	Set @Date = GETDATE()

	Select 
		C.cd_connote
		,P.cc_coupon
		,c.cd_id
	Into #Temp1
	From cpplEDI.dbo.consignment C Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
	Where cd_account in  
	(
	'112999917',
	'113088934',
	'113088959',
	'113088967',
	'113088975',
	'113046502'
	)
	and cd_date > = Dateadd(day, -60, @Date) and cd_date < = @Date

	Select
		T.cd_connote
		,T.cc_coupon
		,T.cd_id
		,L.Id 
	Into #Temp2  
	From #Temp1 T left Join ScannerGateway.dbo.Label L on T.cc_coupon = L.LabelNumber

	--Added by PV on 2020-08-24 for Ticket#20121
	Select distinct b_id, Replace(b_name,'Gold Coast','Goldcoast') b_name 
	Into #tmpBranch
	From branchs
	--Added by PV Ends here -on 2020-08-24 for Ticket#20121

	Select 
		T.cd_connote as Consignemnt
		,T.cd_id
		,T.cc_coupon as LabelNumber
		,P.EventTypeId
		,P.EventDateTime,Convert(Varchar(2500),P.ExceptionReason) as ExceptionReason
		,P.AdditionalText1
		,P.AdditionalText2 as PODName
		,TB.b_name [DepotName]
	Into #temp3
	From #Temp2 T 
		Join ScannerGateway.[dbo].[TrackingEvent] P on T.ID = P.LabelID		
		Left Join #tmpBranch TB on P.CosmosBranchId = TB.b_id --Added by PV on 2020-08-24 for Ticket#20121
	Where EventDateTime between   
	--dateadd(day,-31,@Date) and @Date 
	dateadd(hour,-2000,@Date) and @Date

	Select * into #temp5
	From #temp3 Te
	Inner Join (select distinct DriverName,driverNumber from Cosmos.dbo.Driver) Dr On (Te.AdditionalText1 =  convert(Varchar(500),driverNumber) ) 

	Update #temp3
	Set ExceptionReason = Convert(Varchar(500),DriverNumber) + ' '+(convert(Varchar(500),DriverName))  
	From #temp5 Te
		Inner Join #temp3 On (Te.AdditionalText1 = #temp3.AdditionalText1  )

	Update #temp3
	Set ExceptionReason =  Te.AdditionalText1
	From #temp3 Te
		Inner Join #temp3 On (Te.AdditionalText1 = #temp3.AdditionalText1  )
	Where isnumeric(Te.AdditionalText1)<>1 

	Select 
		Consignemnt
		,LabelNumber
		,cd_id
		,P.Description as ActualStatus
		,CONVERT(VARCHAR(20),
			CASE P.Description 
				WHEN 'Transfer' THEN (CASE WHEN ExceptionReason like '%CAGE%' THEN '702' WHEN ExceptionReason like 'CAGE%' THEN '702'  ELSE '701' END)																									
				WHEN 'Pickup' THEN (CASE WHEN ExceptionReason like 'Futile%' THEN '102' ELSE '101' END)
				WHEN 'consolidate' THEN '402'
				WHEN 'Redirected' THEN '403'
				WHEN 'Failed delivery at popstation' THEN '706'
				WHEN 'Deconsolidate' THEN '212'
				WHEN 'Out For Delivery' THEN (CASE   WHEN ExceptionReason like '%In Depot%' THEN '201'  WHEN ExceptionReason like '%Damaged%' THEN '412' WHEN ExceptionReason like 'Damaged%' THEN '412' ELSE '411' END)
				WHEN 'Delivered' THEN (CASE   WHEN PODName like '%NEWSAGENT%' THEN '310'  WHEN PODName like '%DLB%' THEN '602' ELSE '601' END)
				WHEN 'Link Scan' THEN '515'
				WHEN 'Attempted Delivery' THEN 
				(
				CASE ExceptionReason 
					WHEN 'Card Left - Closed redeliver next cycle' THEN '413'
					WHEN 'Closed redeliver next cycle' THEN '413'
					WHEN 'Card Left - In Vehicle' THEN '512'
					WHEN 'Card Left - Return to Depot' THEN '512'
					WHEN 'Return to Sender - Card Left no Response' THEN '501'
					WHEN 'Return to Sender - Wrong Address' THEN '502'
					WHEN 'Return to Sender - Insufficient Address' THEN '503'
					WHEN 'Return to Sender - Refused delivery' THEN '504'
					WHEN 'Return to Sender - Moved' THEN '505'
					WHEN 'No payment for COD' THEN '518'
					WHEN 'Other' THEN '506'
					WHEN 'Incomplete consignment' THEN '507'
					WHEN 'Card Left - Unsafe to leave' THEN '516' 
					ELSE '517' 
				END 
				)
				WHEN 'In Depot' THEN 
				(
				CASE WHEN ExceptionReason like '%Manifest%' THEN '301'  
					 WHEN ExceptionReason like '%futile%' THEN '102'
					 ELSE '201' 
				END
				)
				WHEN 'Accepted by NewsAgent' THEN '311'
				WHEN 'Drop off in POPStation' THEN '312'
				WHEN 'redelivery' THEN '401'
				WHEN 'reweigh' THEN '210'
				WHEN 'tranship' THEN '211'
				WHEN 'Recovered From POPStation' THEN '705'
				WHEN 'In Transit' THEN '400'
				WHEN 'Handover' THEN 
				(
				CASE WHEN ExceptionReason like 'Chargeable%' THEN '210' 
					WHEN ExceptionReason like '%Chargeable%' THEN '210' 
					WHEN ExceptionReason like 'Reweigh%' THEN '210'  
					WHEN ExceptionReason like '%Reweigh%' THEN '210' ELSE '301' 
					END
				)
				WHEN 'Expired From POPStation' THEN '704'
				WHEN 'Delivered By POPStation' THEN '611'
				WHEN 'Delay of Delivery' THEN '703'
		   ELSE '' 
		END															   											   											   	   
	    ) as MappedStatusCode
		,EventDateTime as StatusDateTime
		,Case when ExceptionReason  like '%pop%' Then replace(ExceptionReason,',',';') else isnull(ExceptionReason,'') end as ExceptionReason
		,Case P.Description When 'Delivered' Then PODName WHEN 'Link Scan' THEN  Replace(PODName,'Link Coupon ','') Else '' END as PODName	
		,DepotName  
	Into #temp4
	From #temp3 T 
		Join ScannerGateway.[dbo].[EventType] P on T.EventTypeId = P.ID

	Update #temp4
	Set PODName = CASE WHEN PODName IS NULL THEN ExceptionReason ELSE PODName END
	Where ActualStatus = 'Delivered'

	Update #temp4
	Set ExceptionReason = NULL 
	Where ActualStatus = 'Delivered' and PODName is not null

	Update #temp4
	Set  ActualStatus = 'Transfer',ExceptionReason = PODName,PODNAME = NULL
	Where MappedStatusCode = 310
	
	Select Consignemnt,LabelNumber,ActualStatus,StatusDateTime,MappedStatusCode,PODName,max(ExceptionReason) as ExceptionReason,max(r.cr_reference) as Reference--,Max(DepotName)DepotName
	From #temp4 t 
		Left Join cppledi.dbo.cdref r on t.cd_id=r.cr_consignment           
	Group By Consignemnt,MappedStatusCode,LabelNumber,StatusDateTime,ActualStatus,PODName

END

GO
