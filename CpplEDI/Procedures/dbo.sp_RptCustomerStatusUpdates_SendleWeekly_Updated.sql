SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[sp_RptCustomerStatusUpdates_SendleWeekly_Updated]  

AS

BEGIN
Declare @Date DATEtime
	set @Date= getdate()

Select Sourcereference as LabelNumber,
P.Description as ActualStatus, 
convert(Varchar(20),CASE  P.Description WHEN 'Transfer' THEN (CASE   WHEN ExceptionReason like '%CAGE%' THEN '702' WHEN ExceptionReason like 'CAGE%' THEN '702'  ELSE '701' END)                                                                                                                                                                        
WHEN 'Pickup' THEN (CASE   WHEN ExceptionReason like 'Futile%' THEN '102' ELSE '101' END)
WHEN 'consolidate' THEN '402'

WHEN 'Redirected' THEN '403'
WHEN 'Failed delivery at popstation' THEN '706'

                                                                              WHEN 'Deconsolidate' THEN '212'



                                                                              WHEN 'Out For Delivery' THEN (CASE   WHEN ExceptionReason like '%In Depot%' THEN '201'  WHEN ExceptionReason like '%Damaged%' THEN '412' WHEN ExceptionReason like 'Damaged%' THEN '412' ELSE '411' END)



                                                                              WHEN 'Delivered' THEN (CASE   WHEN AdditionalText2 like '%NEWSAGENT%' THEN '310'  WHEN ExceptionReason like 'to%' THEN '602' ELSE '601' END)



                                                                              WHEN 'Link Scan' THEN '515'



                                                                              WHEN 'Attempted Delivery' THEN (CASE  ExceptionReason WHEN 'Card Left - Closed redeliver next cycle' THEN '413'



                                                                                                                                                                           WHEN 'Closed redeliver next cycle' THEN '413'



                                                                                                                                                                           WHEN 'Card Left - In Vehicle' THEN '512'



                                                                                                                                                                           WHEN 'Card Left - Return to Depot' THEN '512'



                                                                                                                                                                           WHEN 'Return to Sender - Card Left no Response' THEN '501'



                                                                                                                                                                           WHEN 'Return to Sender - Wrong Address' THEN '502'



                                                                                                                                                                           WHEN 'Return to Sender - Insufficient Address' THEN '503'



                                                                                                                                                                           WHEN 'Return to Sender - Refused delivery' THEN '504'



                                                                                                                                                                           WHEN 'Return to Sender - Moved' THEN '505'



                                                                                                                                                                           WHEN 'No payment for COD' THEN '518'



                                                                                                                                                                           WHEN 'Other' THEN '506'



                                                                                                                                                                           WHEN 'Incomplete consignment' THEN '507'



                                                                                                                                                                           WHEN 'Card Left - Unsafe to leave' THEN '516' 



                                                                                                                                                                           ELSE '517' END )



                                                                              WHEN 'In Depot' THEN (CASE   WHEN ExceptionReason like '%Manifest%' THEN '301' ELSE '201' END)



                                                                              WHEN 'Accepted by NewsAgent' THEN '311'



                                                                              WHEN 'Drop off in POPStation' THEN '312'


                                                                              WHEN 'Dropped off at POPShop' THEN '100'



                                                                              WHEN 'redelivery' THEN '401'



                                                                              WHEN 'reweigh' THEN '210'



                                                                              WHEN 'tranship' THEN '211'



                                                                              WHEN 'Recovered From POPStation' THEN '705'



                                                                              WHEN 'In Transit' THEN '400'



                                                                              WHEN 'Handover' THEN (CASE   WHEN ExceptionReason like 'Chargeable%' THEN '210' WHEN ExceptionReason like '%Chargeable%' THEN '210' WHEN ExceptionReason like 'Reweigh%' THEN '210'  WHEN ExceptionReason like '%Reweigh%' THEN '210' ELSE '301' 


END)



                                                                              WHEN 'Expired From POPStation' THEN '704'



                                                                              WHEN 'Delivered By POPStation' THEN '611'



                                                                              WHEN 'Delay of Delivery' THEN '703'



                                                                              ELSE '' END                                                                                                                                                                                                                                                                           



           ) as MappedStatusCode , 



          EventDateTime as StatusDateTime,  



          Case when ExceptionReason  like '%pop%' Then replace(ExceptionReason,',',';') else isnull(ExceptionReason,'') end as ExceptionReason,
Case P.Description When 'Delivered' Then AdditionalText2 WHEN 'Link Scan' THEN  Replace(AdditionalText2,'Link Coupon ','') Else '' END as PODName      
--into #temp4
from ScannerGateway.[dbo].[TrackingEvent] T (nolock) 
join  ScannerGateway.[dbo].[EventType] P (nolock) on T.EventTypeId = P.ID
where (SourceReference like '%AFXL%' or SourceReference like '%BZL5%')
 and EventDateTime between
 dateadd(DAY,-9,@Date) and @Date

  End

GO
