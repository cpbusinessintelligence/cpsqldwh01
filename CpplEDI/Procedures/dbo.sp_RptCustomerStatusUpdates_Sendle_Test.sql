SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================



-- Author:		Sinshith Sachidanandan



-- Create date: <Create Date,,>



-- Description:	<Description,,>

--[sp_RptCustomerStatusUpdates_SS31072017] '112976535','2017-07-07'

-- =============================================



Create PROCEDURE [dbo].[sp_RptCustomerStatusUpdates_Sendle_Test] 


	--@AccountCode VARCHAR (50),
	--@Date DATEtime

AS

BEGIN

	SET FMTONLY OFF 
	SET NOCOUNT ON;

	--set @AccountCode='112976535'
	Declare @Date DATEtime
SET @Date= dateadd(day,-1,getdate());

--Get all Label details from EDI
SELECT C.cd_connote , 
       P.cc_coupon,
	   c.cd_id
INTO #Temp1
	FROM cpplEDI.dbo.consignment C JOIN cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
	WHERE cd_account in ('113102511','112951223')
    AND cd_date > = Dateadd(day, -30, @Date) and cd_date < = @Date

SELECT T.cd_connote as Consignemnt,
      t.cc_coupon as LabelNumber, 
	  L.Id 
    INTO #Temp2  
	FROM #Temp1 T LEFT JOIN ScannerGateway.dbo.Label L ON T.cc_coupon = L.LabelNumber

--Get all Label details from Website
SELECT  ConsignmentCode  as Consignemnt,
        LabelNumber INTO #TempWS1
	    FROM [EzyFreight].[dbo].tblConsignment a with (NOLOCK) 
INNER JOIN [EzyFreight].[dbo].[tblItemLabel] b
      ON (a.ConsignmentID = b.ConsignmentID)
INNER JOIN [EzyFreight].[dbo].[tblCompanyUsers] c
      ON(a.UserID = c.UserID)
INNER JOIN [EzyFreight].[dbo].[tblCompany] d
  ON(c.CompanyID = d.CompanyID)
  WHERE d.AccountNumber in ('113102511','112951223')
  AND a.CreatedDateTime > = Dateadd(day, -30, @Date) and a.CreatedDateTime  < = @Date

SELECT Consignemnt,
       LabelNumber INTO #TempWSCombined
FROM #TempWS1

  UNION ALL

SELECT 
      Consignemnt,
	  LabelNumber
FROM #Temp2


--Get Tracking data
SELECT Consignemnt ,  
       LabelNumber ,
	   P.EventTypeId , 
	   P.EventDateTime,
	   P.ExceptionReason ,
	   P.AdditionalText1 as PODName

INTO #temp3 

FROM #TempWSCombined A 
JOIN ScannerGateway.[dbo].[TrackingEvent] P 
ON A.LabelNumber = P.SourceReference
WHERE EventDateTime >= @Date

SELECT DISTINCT Consignemnt,
       LabelNumber,
       P.Description as ActualStatus, 
	   convert(Varchar(20),CASE  P.Description WHEN 'Transfer' THEN (CASE   WHEN ExceptionReason like '%CAGE%' THEN '702' WHEN ExceptionReason like 'CAGE%' THEN '702'  ELSE '701' END)																									

											   WHEN 'Pickup' THEN (CASE   WHEN ExceptionReason like 'Futile%' THEN '102' ELSE '101' END)

											   WHEN 'consolidate' THEN '402'

											   WHEN 'Deconsolidate' THEN '212'

											   WHEN 'Out For Delivery' THEN (CASE   WHEN ExceptionReason like '%In Depot%' THEN '201'  WHEN ExceptionReason like '%Damaged%' THEN '412' WHEN ExceptionReason like 'Damaged%' THEN '412' ELSE '411' END)

											   WHEN 'Delivered' THEN (CASE   WHEN ExceptionReason like '%NEWSAGENT%' THEN '311'  WHEN ExceptionReason like 'to%' THEN '602' ELSE '601' END)

											   WHEN 'Link Scan' THEN '515'

											   WHEN 'Attempted Delivery' THEN (CASE  ExceptionReason WHEN 'Card Left - Closed redeliver next cycle' THEN '413'

																									WHEN 'Closed redeliver next cycle' THEN '413'

																									WHEN 'Card Left - In Vehicle' THEN '512'

																									WHEN 'Card Left - Return to Depot' THEN '512'

																									WHEN 'Return to Sender - Card Left no Response' THEN '501'

																									WHEN 'Return to Sender - Wrong Address' THEN '502'

																									WHEN 'Return to Sender - Insufficient Address' THEN '503'

																									WHEN 'Return to Sender - Refused delivery' THEN '504'

																									WHEN 'Return to Sender - Moved' THEN '505'

																									WHEN 'No payment for COD' THEN '518'

																									WHEN 'Other' THEN '506'
																									WHEN 'Incomplete consignment' THEN '507'
																									WHEN 'Card Left - Unsafe to leave' THEN '516' 
																									ELSE '517' END )
											   WHEN 'In Depot' THEN (CASE   WHEN ExceptionReason like '%Manifest%' THEN '301' ELSE '201' END)
											   WHEN 'Accepted by NewsAgent' THEN '311'
											   WHEN 'Drop off in POPStation' THEN '312'
											   --SS
											   WHEN 'Dropped off at POPShop' THEN '103'
											   WHEN 'Futile Pickup' THEN '102'
											   WHEN 'redelivery' THEN '401'
											   WHEN 'reweigh' THEN '210'
											   WHEN 'tranship' THEN '211'
											   WHEN 'Recovered FROM POPStation' THEN '705'
											   WHEN 'In Transit' THEN '400'
											   WHEN 'Handover' THEN (CASE   WHEN ExceptionReason like 'Chargeable%' THEN '210' WHEN ExceptionReason like '%Chargeable%' THEN '210' WHEN ExceptionReason like 'Reweigh%' THEN '210'  WHEN ExceptionReason like '%Reweigh%' THEN '210' ELSE '301' 

END)

											   WHEN 'Expired FROM POPStation' THEN '704'
											   WHEN 'Delivered By POPStation' THEN '611'
											   WHEN 'Delay of Delivery' THEN '703'

											   ELSE '' END															   											   											   	   

	    ) as MappedStatusCode , 
	   EventDateTime as StatusDateTime,  
	   Case when ExceptionReason  like '%pop%' Then replace(ExceptionReason,',',';') else isnull(ExceptionReason,'') end as ExceptionReason,
	   Case P.Description When 'Delivered' Then PODName WHEN 'Link Scan' THEN  Replace(PODName,'Link Coupon ','') Else '' END as PODName	  
	   FROM #temp3 T join  ScannerGateway.[dbo].[EventType] P on T.EventTypeId = P.ID

END




GO
