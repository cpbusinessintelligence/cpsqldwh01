SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================



-- Author:		<Author,,Name>



-- Create date: <Create Date,,>



-- Description:	<Description,,>

--[sp_RptCustomerStatusUpdates] '112976535','2017-07-07'

-- =============================================



CREATE PROCEDURE [dbo].[sp_RptCustomerStatusUpdates_ShippitDailyBulk_Amway]  

AS

BEGIN

	SET FMTONLY OFF 
	SET NOCOUNT ON;

	--set @AccountCode='112976535'
	Declare @Date DATEtime
	set @Date= getdate()

Select C.cd_connote , P.cc_coupon,c.cd_id
	into #Temp1
	from cpplEDI.dbo.consignment C Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
where cd_account in (
'112962311'

)

 

	and cd_date > = Dateadd(day, -30, @Date) and cd_date < = @Date
Select T.cd_connote,t.cc_coupon,t.cd_id, L.Id 
into #Temp2  
from #Temp1 T left Join ScannerGateway.dbo.Label L on T.cc_coupon = L.LabelNumber



Select T.cd_connote as Consignemnt ,T.cd_id, T.cc_coupon as LabelNumber ,P.EventTypeId , P.EventDateTime,P.ExceptionReason ,P.AdditionalText1,P.AdditionalText2 as  PODName
into #temp3
 from        #Temp2 T Join   ScannerGateway.[dbo].[TrackingEvent] P on T.ID = P.LabelID
  Where EventDateTime between   dateadd(hour,-30,@Date) and @Date
 --Where EventDateTime between   dateadd(day,-50,@Date) and @Date
  select * into #temp5
 from #temp3 Te
inner join (select distinct DriverName,driverNumber from Cosmos.dbo.Driver) Dr On (Te.AdditionalText1 =  convert(Varchar(Max),driverNumber) ) 

Alter table #temp3
alter column ExceptionReason Varchar(2000)

update #temp3
Set ExceptionReason = Convert(Varchar(Max),DriverNumber) + ' '+(convert(Varchar(Max),DriverName))  
from #temp5 Te
inner join #temp3 
on (Te.AdditionalText1 = #temp3.AdditionalText1  )

update #temp3
Set ExceptionReason =  ltrim(rtrim(Te.AdditionalText1))
from #temp3 Te
inner join #temp3 
on (Te.AdditionalText1 =  ltrim(rtrim(#temp3.AdditionalText1))  )
where isnumeric(Te.AdditionalText1)<>1 


Select Consignemnt,LabelNumber,cd_id,
P.Description as ActualStatus, 
convert(Varchar(1000),CASE  P.Description WHEN 'Transfer' THEN (CASE   WHEN ExceptionReason like '%CAGE%' THEN '702' WHEN ExceptionReason like 'CAGE%' THEN '702'  ELSE '701' END)																									
 WHEN 'Pickup' THEN (CASE   WHEN ExceptionReason like 'Futile%' THEN '102' ELSE '101' END)
WHEN 'consolidate' THEN '402'

WHEN 'Redirected' THEN '403'
WHEN 'Failed delivery at popstation' THEN '706'

											   WHEN 'Deconsolidate' THEN '212'



											   WHEN 'Out For Delivery' THEN (CASE   WHEN ExceptionReason like '%In Depot%' THEN '201'  WHEN ExceptionReason like '%Damaged%' THEN '412' WHEN ExceptionReason like 'Damaged%' THEN '412' ELSE '411' END)



											   WHEN 'Delivered' THEN (CASE   WHEN (PODName like '%NEWSAGENT%' OR PODName like '%NEWSAGENCY%') THEN '310'  WHEN PODName like '%DLB%' THEN '602' ELSE '601' END)



											   WHEN 'Link Scan' THEN '515'



											   WHEN 'Attempted Delivery' THEN (CASE  ExceptionReason WHEN 'Card Left - Closed redeliver next cycle' THEN '413'



																									WHEN 'Closed redeliver next cycle' THEN '413'



																									WHEN 'Card Left - In Vehicle' THEN '512'



																									WHEN 'Card Left - Return to Depot' THEN '512'



																									WHEN 'Return to Sender - Card Left no Response' THEN '501'



																									WHEN 'Return to Sender - Wrong Address' THEN '502'



																									WHEN 'Return to Sender - Insufficient Address' THEN '503'



																									WHEN 'Return to Sender - Refused delivery' THEN '504'



																									WHEN 'Return to Sender - Moved' THEN '505'



																									WHEN 'No payment for COD' THEN '518'



																									WHEN 'Other' THEN '506'



																									WHEN 'Incomplete consignment' THEN '507'



																									WHEN 'Card Left - Unsafe to leave' THEN '516' 



																									ELSE '517' END )



											   	   WHEN 'In Depot' THEN (CASE   WHEN ExceptionReason like '%Manifest%' THEN '301'  
											                             When ExceptionReason like  '%futile%' then '102'
																		 ELSE '201' END)



											   WHEN 'Accepted by NewsAgent' THEN '311'



											   WHEN 'Drop off in POPStation' THEN '312'



											   WHEN 'redelivery' THEN '401'



											   WHEN 'reweigh' THEN '210'



											   WHEN 'tranship' THEN '211'



											   WHEN 'Recovered From POPStation' THEN '705'



											   WHEN 'In Transit' THEN '400'



											   WHEN 'Handover' THEN (CASE   WHEN ExceptionReason like 'Chargeable%' THEN '210' WHEN ExceptionReason like '%Chargeable%' THEN '210' WHEN ExceptionReason like 'Reweigh%' THEN '210'  WHEN ExceptionReason like '%Reweigh%' THEN '210' ELSE '301' 


END)



											   WHEN 'Expired From POPStation' THEN '704'



											   WHEN 'Delivered By POPStation' THEN '611'



											   WHEN 'Delay of Delivery' THEN '703'



											   ELSE '' END															   											   											   	   



	    ) as MappedStatusCode , 



	   EventDateTime as StatusDateTime,  



	   Case when ExceptionReason  like '%pop%' Then replace(ExceptionReason,',',';') else isnull(ExceptionReason,'') end as ExceptionReason,
Case P.Description When 'Delivered' Then PODName WHEN 'Link Scan' THEN  Replace(PODName,'Link Coupon ','') Else '' END as PODName	  
into #temp4
from #temp3 T join  ScannerGateway.[dbo].[EventType] P on T.EventTypeId = P.ID


 update #temp4
  set PODName = case when PODName is null then ExceptionReason else PODName  end
  where ActualStatus = 'Delivered'

      update #temp4
  set ExceptionReason = NULL 
  where  ActualStatus = 'Delivered'
  and PODName is not null

  update #temp4
  set  ActualStatus = 'Transfer',ExceptionReason = PODName,PODNAME = NULL
  where MappedStatusCode = 310


	select Consignemnt,LabelNumber,ActualStatus,StatusDateTime,MappedStatusCode,PODName,max(ExceptionReason) as ExceptionReason,max(r.cr_reference) as Reference from #temp4 t left join cppledi.dbo.cdref r on t.cd_id=r.cr_consignment           
	group by Consignemnt,MappedStatusCode,LabelNumber,StatusDateTime,ActualStatus,PODName

END





































GO
