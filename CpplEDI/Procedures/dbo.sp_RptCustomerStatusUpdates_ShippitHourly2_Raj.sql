SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-09-23
-- Description:	Splitted sp_RptCustomerStatusUpdates_ShippitHourly because of increase in volume of data, so Pet circle account code moved to this SP
-- [sp_RptCustomerStatusUpdates] '112976535','2017-07-07'
-- =============================================

CREATE PROCEDURE [dbo].[sp_RptCustomerStatusUpdates_ShippitHourly2_Raj]  
AS
BEGIN

	SET FMTONLY OFF 
	SET NOCOUNT ON;

	--set @AccountCode='112976535'
	Declare @Date datetime
	Set @Date= getdate()

	Select C.cd_connote , P.cc_coupon,c.cd_id
	Into #Temp1
	From cpplEDI.dbo.consignment C Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
	Where cd_account In 
                  ('112808712',
                  '113138986',
                  '113137343',
                  '113137350',
                  '113137368',
                  '113137368',
                  '113137384',
                  '113138994',
                  '113139307',
                  '113139901',
				  '113138218',
				  '113117485',
				  '113140495'
	
	              )
	And cd_date > = Dateadd(day, -60, @Date) and cd_date < = @Date

	Select T.cd_connote,t.cc_coupon,t.cd_id, L.Id 
	Into #Temp2  
	From #Temp1 T 
	Left Join ScannerGateway.dbo.Label L on T.cc_coupon = L.LabelNumber

	Select T.cd_connote as Consignemnt ,T.cd_id, T.cc_coupon as LabelNumber ,P.EventTypeId , P.EventDateTime,Convert(Varchar(2500),P.ExceptionReason) as ExceptionReason ,P.AdditionalText1,P.AdditionalText2 as  PODName
	Into #temp3
	From #Temp2 T 
	Join ScannerGateway.[dbo].[TrackingEvent] P on T.ID = P.LabelID
	--Where EventDateTime between   
	----dateadd(day,-31,@Date) and @Date
	--dateadd(hour,-2,@Date) and @Date : Commented by PV to include missing scans because of delayed insert into TE table
	Where CreatedDate between   dateadd(hour,-2,@Date) and @Date --PV: Chaged to createddate to avoid missing scans in hourly report.

	Select * Into #temp5
	From #temp3 Te
	Inner Join (Select Distinct DriverName,driverNumber From Cosmos.dbo.Driver) Dr On (Te.AdditionalText1 =  convert(Varchar(500),driverNumber)) 

	Update #temp3
		Set ExceptionReason = Convert(Varchar(500),DriverNumber) + ' '+(convert(Varchar(500),DriverName))  
	From #temp5 Te
	Inner Join #temp3 On (Te.AdditionalText1 = #temp3.AdditionalText1)

	Update #temp3
		Set ExceptionReason =  Te.AdditionalText1
	From #temp3 Te
	Inner Join #temp3 On (Te.AdditionalText1 = #temp3.AdditionalText1  )
	Where isnumeric(Te.AdditionalText1)<>1 

	Select 
		Consignemnt
		,LabelNumber
		,cd_id
		,P.Description as ActualStatus
		,convert(Varchar(20),CASE P.Description 
			WHEN 'Transfer' THEN (CASE   WHEN ExceptionReason like '%CAGE%' THEN '702' WHEN ExceptionReason like 'CAGE%' THEN '702'  ELSE '701' END)																									
			WHEN 'Pickup' THEN (CASE   WHEN ExceptionReason like 'Futile%' THEN '102' ELSE '101' END)
			WHEN 'consolidate' THEN '402'
			WHEN 'Redirected' THEN '403'
			WHEN 'Failed delivery at popstation' THEN '706'
			WHEN 'Deconsolidate' THEN '212'
			WHEN 'Out For Delivery' THEN (CASE   WHEN ExceptionReason like '%In Depot%' THEN '201'  WHEN ExceptionReason like '%Damaged%' THEN '412' WHEN ExceptionReason like 'Damaged%' THEN '412' ELSE '411' END)
			WHEN 'Delivered' THEN (CASE   WHEN (PODName like '%NEWSAGENT%' OR PODName like '%NEWSAGENCY%') THEN '310'  WHEN PODName like '%DLB%' THEN '602' ELSE '601' END)
			WHEN 'Link Scan' THEN '515'
			WHEN 'Attempted Delivery' THEN 
				(CASE  ExceptionReason 
					WHEN 'Card Left - Closed redeliver next cycle' THEN '413'
					WHEN 'Closed redeliver next cycle' THEN '413'
					WHEN 'Card Left - In Vehicle' THEN '512'
					WHEN 'Card Left - Return to Depot' THEN '512'
					WHEN 'Return to Sender - Card Left no Response' THEN '501'
					WHEN 'Return to Sender - Wrong Address' THEN '502'
					WHEN 'Return to Sender - Insufficient Address' THEN '503'
					WHEN 'Return to Sender - Refused delivery' THEN '504'
					WHEN 'Return to Sender - Moved' THEN '505'
					WHEN 'No payment for COD' THEN '518'
					WHEN 'Other' THEN '506'
					WHEN 'Incomplete consignment' THEN '507'
					WHEN 'Card Left - Unsafe to leave' THEN '516' 
					ELSE '517' END 
				)
				WHEN 'In Depot' THEN (CASE WHEN ExceptionReason like '%Manifest%' THEN '301'  
											When ExceptionReason like  '%futile%' then '102'
											ELSE '201' END)
				WHEN 'Accepted by NewsAgent' THEN '311'
				WHEN 'Drop off in POPStation' THEN '312'
				WHEN 'redelivery' THEN '401'
				WHEN 'reweigh' THEN '210'
				WHEN 'tranship' THEN '211'
				WHEN 'Recovered From POPStation' THEN '705'
				WHEN 'In Transit' THEN '400'
				WHEN 'Handover' THEN (CASE   WHEN ExceptionReason like 'Chargeable%' THEN '210' WHEN ExceptionReason like '%Chargeable%' THEN '210' WHEN ExceptionReason like 'Reweigh%' THEN '210'  WHEN ExceptionReason like '%Reweigh%' THEN '210' ELSE '301' END)
				WHEN 'Expired From POPStation' THEN '704'
				WHEN 'Delivered By POPStation' THEN '611'
				WHEN 'Delay of Delivery' THEN '703'
			ELSE '' END															   											   											   	   
	    ) as MappedStatusCode 
		,EventDateTime as StatusDateTime
		,Case when ExceptionReason  like '%pop%' Then replace(ExceptionReason,',',';') else isnull(ExceptionReason,'') end as ExceptionReason
		,Case P.Description When 'Delivered' Then PODName WHEN 'Link Scan' THEN  Replace(PODName,'Link Coupon ','') Else '' END as PODName	  
		
	Into #temp4
	From #temp3 T join  ScannerGateway.[dbo].[EventType] P on T.EventTypeId = P.ID


	update #temp4
	set PODName = case when PODName is null then ExceptionReason else PODName  end
	where ActualStatus = 'Delivered'

	update #temp4
	set ExceptionReason = NULL 
	where  ActualStatus = 'Delivered'
	and PODName is not null

	update #temp4
	set  ActualStatus = 'Transfer',ExceptionReason = PODName,PODNAME = NULL
	where MappedStatusCode = 310

	-- Added by PV on 2020-10-16: Set 601 for status 'Delivery'
	Update #temp4
	Set MappedStatusCode = '601'
	Where ActualStatus = 'delivery' and MappedStatusCode = '' 

	select Consignemnt,LabelNumber,ActualStatus,StatusDateTime,MappedStatusCode,PODName,max(ExceptionReason) as ExceptionReason,max(r.cr_reference) as Reference from #temp4 t left join cppledi.dbo.cdref r on t.cd_id=r.cr_consignment   and ActualStatus <>'delivery'        
	group by Consignemnt,MappedStatusCode,LabelNumber,StatusDateTime,ActualStatus,PODName

END
GO
