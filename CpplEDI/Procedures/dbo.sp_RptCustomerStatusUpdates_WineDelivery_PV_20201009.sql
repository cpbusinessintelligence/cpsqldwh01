SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-08-10
-- Description:	Status report for Wine Delivery(Its copy of Shippit hourly report, only cd_account = '113035430' is updated. No other changes)
--[sp_RptCustomerStatusUpdates_WineDelivery] 'Daily'

-- =============================================
CREATE PROCEDURE [dbo].[sp_RptCustomerStatusUpdates_WineDelivery_PV_20201009]
@StatusType Varchar(10) = 'Daily'
AS
BEGIN

	SET FMTONLY OFF 
	SET NOCOUNT ON;

	--set @AccountCode='112976535'
	DECLARE @Date datetime
	DECLARE @Interval Int = -30

	IF @StatusType = 'Hourly' 
		Set @Interval = -2

	Set @Date= getdate()

	Select C.cd_connote , P.cc_coupon,c.cd_id
	Into #Temp1
	From cpplEDI.dbo.consignment C 
		JOIN cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
	Where cd_account IN ('113035430') 
		AND cd_date > = DATEADD(DAY, -60, @Date) and cd_date < = @Date
	
	Select T.cd_connote,t.cc_coupon,t.cd_id, L.Id 
	Into #Temp2  
	From #Temp1 T 
		LEFT JOIN ScannerGateway.dbo.Label L on T.cc_coupon = L.LabelNumber

	Select T.cd_connote as Consignment,T.cd_id,T.cc_coupon as LabelNumber,P.EventTypeId, P.EventDateTime,CONVERT(VARCHAR(2500),P.ExceptionReason) as ExceptionReason,P.AdditionalText1,P.AdditionalText2 as  PODName
	Into #temp3
	From #Temp2 T 
		JOIN ScannerGateway.[dbo].[TrackingEvent] P on T.ID = P.LabelID
	Where CreatedDate BETWEEN DATEADD(HOUR,@Interval,@Date) AND @Date --PV: Chaged to createddate to avoid missing scans in hourly report.
	
	Select * Into #temp5
	From #temp3 Te
	INNER JOIN (Select distinct DriverName,driverNumber From Cosmos.dbo.Driver) Dr ON (Te.AdditionalText1 =  CONVERT(VARCHAR(500),driverNumber) ) 

	UPDATE #temp3
	Set ExceptionReason = CONVERT(VARCHAR(500),DriverNumber) + ' '+(CONVERT(VARCHAR(500),DriverName))  
	From #temp5 Te
		INNER JOIN #temp3 ON (Te.AdditionalText1 = #temp3.AdditionalText1)

	UPDATE #temp3
	Set ExceptionReason =  Te.AdditionalText1
	From #temp3 Te
		INNER JOIN #temp3 ON (Te.AdditionalText1 = #temp3.AdditionalText1)
	Where ISNUMERIC(Te.AdditionalText1)<>1 

	Select 
		Consignment
		, LabelNumber
		, cd_id
		, P.Description as ActualStatus
		, CONVERT(VARCHAR(20),
			CASE P.Description 
				WHEN 'Transfer' THEN (CASE WHEN ExceptionReason like '%CAGE%' THEN '702' WHEN ExceptionReason like 'CAGE%' THEN '702'  ELSE '701' END)																									
				WHEN 'Pickup' THEN (CASE   WHEN ExceptionReason like 'Futile%' THEN '102' ELSE '101' END)
				WHEN 'consolidate' THEN '402'
				WHEN 'Redirected' THEN '403'
				WHEN 'Failed delivery at popstation' THEN '706'
				WHEN 'Deconsolidate' THEN '212'
				WHEN 'Out For Delivery' THEN (CASE   WHEN ExceptionReason like '%In Depot%' THEN '201'  WHEN ExceptionReason like '%Damaged%' THEN '412' WHEN ExceptionReason like 'Damaged%' THEN '412' ELSE '411' END)
				WHEN 'Delivered' THEN (CASE   WHEN (PODName like '%NEWSAGENT%' OR PODName like '%NEWSAGENCY%') THEN '310'  WHEN PODName like '%DLB%' THEN '602' ELSE '601' END)
				WHEN 'Link Scan' THEN '515'
				WHEN 'Attempted Delivery' THEN (
					CASE  ExceptionReason 
						WHEN 'Card Left - Closed redeliver next cycle' THEN '413'
						WHEN 'Closed redeliver next cycle' THEN '413'
						WHEN 'Card Left - In Vehicle' THEN '512'
						WHEN 'Card Left - Return to Depot' THEN '512'
						WHEN 'Return to Sender - Card Left no Response' THEN '501'
						WHEN 'Return to Sender - Wrong Address' THEN '502'
						WHEN 'Return to Sender - Insufficient Address' THEN '503'
						WHEN 'Return to Sender - Refused delivery' THEN '504'
						WHEN 'Return to Sender - Moved' THEN '505'
						WHEN 'No payment for COD' THEN '518'
						WHEN 'Other' THEN '506'
						WHEN 'Incomplete consignment' THEN '507'
						WHEN 'Card Left - Unsafe to leave' THEN '516' 
						ELSE '517' 
					END)
				WHEN 'In Depot' THEN (
					CASE WHEN ExceptionReason like '%Manifest%' THEN '301'  
						WHEN ExceptionReason like  '%futile%' then '102'
						ELSE '201' 
					END)
				WHEN 'Accepted by NewsAgent' THEN '311'
				WHEN 'Drop off in POPStation' THEN '312'
				WHEN 'redelivery' THEN '401'
				WHEN 'reweigh' THEN '210'
				WHEN 'tranship' THEN '211'
				WHEN 'Recovered From POPStation' THEN '705'
				WHEN 'In Transit' THEN '400'
				WHEN 'Handover' THEN (
					CASE WHEN ExceptionReason like 'Chargeable%' THEN '210' 
						WHEN ExceptionReason like '%Chargeable%' THEN '210' 
						WHEN ExceptionReason like 'Reweigh%' THEN '210'
						WHEN ExceptionReason like '%Reweigh%' THEN '210' 
						ELSE '301' 
					END)
				WHEN 'Expired From POPStation' THEN '704'
				WHEN 'Delivered By POPStation' THEN '611'
				WHEN 'Delay of Delivery' THEN '703'
			ELSE '' END
		) AS MappedStatusCode
		, EventDateTime as StatusDateTime
		, CASE WHEN ExceptionReason LIKE '%pop%' THEN REPLACE(ExceptionReason,',',';') ELSE ISNULL(ExceptionReason,'') END AS ExceptionReason
		, CASE P.DESCRIPTION WHEN 'Delivered' THEN PODName WHEN 'Link Scan' THEN  Replace(PODName,'Link Coupon ','') Else '' END as PODName	  
	Into #temp4
	From #temp3 T 
		JOIN ScannerGateway.[dbo].[EventType] P on T.EventTypeId = P.ID

	UPDATE #temp4
	SET PODName = CASE WHEN PODName IS NULL THEN ExceptionReason ELSE PODName END
	WHERE ActualStatus = 'Delivered'

	UPDATE #temp4
	SET ExceptionReason = NULL 
	WHERE  ActualStatus = 'Delivered' AND PODName IS NOT NULL

	UPDATE #temp4
	SET  ActualStatus = 'Transfer',ExceptionReason = PODName, PODNAME = NULL
	WHERE MappedStatusCode = 310

	SELECT Consignment,LabelNumber,ActualStatus,StatusDateTime,MappedStatusCode,PODName,MAX(ExceptionReason) AS ExceptionReason,MAX(r.cr_reference) AS Reference 
	FROM #temp4 t 
		LEFT JOIN cppledi.dbo.cdref r on t.cd_id=r.cr_consignment           
	GROUP BY Consignment,MappedStatusCode,LabelNumber,StatusDateTime,ActualStatus,PODName

END

GO
