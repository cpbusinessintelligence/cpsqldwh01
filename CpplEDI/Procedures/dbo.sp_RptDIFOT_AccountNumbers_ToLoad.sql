SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-04-21
-- Description:	Load difot report for the list of account numbers given
-- =============================================
-- exec [sp_RptDIFOT_AccountNumbers_ToLoad] 'Export DIFOT to Excel FirstAcivity'
CREATE PROCEDURE [dbo].[sp_RptDIFOT_AccountNumbers_ToLoad]
@PackageName Varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select AccountNumber,FilePath 
	From tbl_DIFOTAccountNumbers_ToExcelLoad 
	Where PackageName = @PackageName And IsActive = 1 And IsFinished = 0
	Order by FilePath

END
GO
