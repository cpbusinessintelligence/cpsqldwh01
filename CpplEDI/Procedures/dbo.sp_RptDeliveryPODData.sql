SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptDeliveryPODData](@StartDate date,@EndDate date) as begin


     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptDeliveryPODData] 
	    --' ---------------------------
    --' Purpose: sp_RptDeliveryPODData-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 31 Oct 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 31/10/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

select con.cd_connote as ConnoteNumber,
       cd_id as Consignmentid,
       cd_pricecode as Productcode,
	   pc_name as ProductName, 
	   max(convert(Date,( case when e.description='Pickup' then te.eventdatetime else '' end))) as Collectiondate,
	   con.cd_date as EnteredDate,
	   con.[cd_deadweight],
	   cd_account as Accountnumber,
	   'RS Components'  as AccountName,
	    max([cr_reference]) as CustomerReference,
	   cd_items as NoofParcels,
	   cd_deadweight as DeclaredWeight,
	   cd_volume as DeclaredVolume,
	   [cd_pickup_addr0] as SenderName,
       [cd_pickup_suburb] as  SenderSuburb ,
	   [cd_pickup_postcode] as SenderPostcode,
	   case when b.b_name='Adelaide' then 'SA'
	        when b.b_name='Melbourne' then 'VIC'
			when b.b_name='Sydney' then 'NSW'
			when b.b_name='Brisbane' then 'QLD'
			when b.b_name='GoldCoast' then 'QLD'
			when b.b_name='Gold Coast' then 'QLD'
			when b.b_name='Perth' then 'WA' else b.b_name end  as SenderState,
	   [cd_delivery_addr0] as DeliveryName,
       [cd_delivery_suburb] as  DeliverySuburb ,
	   [cd_delivery_postcode] as DeliveryPostcode,
	    case when b1.b_name='Adelaide' then 'SA'
	        when b1.b_name='Melbourne' then 'VIC'
			when b1.b_name='Sydney' then 'NSW'
			when b1.b_name='Brisbane' then 'QLD'
			when b1.b_name='GoldCoast' then 'QLD'
			when b1.b_name='Gold Coast' then 'QLD'
			when b1.b_name='Perth' then 'WA' else b1.b_name end   as DeliveryState,
	   max(case when e.description='Pickup' then te.eventdatetime else '' end) as PickupDatetime,
	    max(case when e.description='Delivered' then te.eventdatetime else '' end) as DeliveryDatetime,
	   convert(varchar(500),'') as LastScanRecorded,
	   max(convert(Date,( case when e.description='Pickup' then te.eventdatetime else '' end))) as PickupDate,
	   max(convert(time,( case when e.description='Pickup' then te.eventdatetime else '' end))) as PickupTime,
	   max(convert(Date,( case when e.description='Delivered' then te.eventdatetime else '' end ))) as DeliveryDate,
	   max(convert(time,( case when e.description='Delivered' then te.eventdatetime else '' end))) as DeliveryTime,
	    max(case when e.description='Delivered' then 'Successful Delivery' else '' end) DeliveryStatus,
	   cd_deliver_pod as Receivedby
	  -- te.sourcereference,eventdatetime,e.description as Scantype,cc_consignment
into #temp1
from scannergateway.dbo.trackingevent te join scannergateway.dbo.eventtype  e on te.eventtypeid=e.id
                                         --join scannergateway.dbo.eventtype  e1 on te.eventtypeid=e1.id
										 join cpplEDI.dbo.cdcoupon on cc_coupon=sourcereference
										 join cpplEDI.dbo.consignment con on cd_id=cc_consignment
										 join cpplEDI.dbo.pricecodes p on pc_code=[cd_pricecode]
                                          join cpplEDI.dbo.cdref on cr_consignment=cd_id
								          join cpplEDI.dbo.branchs b on b.b_id=cd_pickup_branch
								          join cpplEDI.dbo.branchs b1 on b1.b_id=cd_deliver_branch
where e.description in ('Pickup','Delivered') and convert(Date,eventdatetime) between @StartDate and @EndDate
--=convert(Date,getdate())
and con.cd_account IN ('112833348','112984265','112987045','112987532','112987540','112814702','113014054')
group by  con.cd_connote ,
       cd_id ,
       cd_pricecode ,
	   pc_name , 
	   con.cd_date ,
	   con.cd_items,
	   con.[cd_deadweight],
	   cd_account ,
	   cd_items ,
	   cd_deadweight ,
	   cd_volume ,
	   [cd_pickup_addr0] ,
       [cd_pickup_suburb]  ,
	   [cd_pickup_postcode] ,
	   case when b.b_name='Adelaide' then 'SA'
	        when b.b_name='Melbourne' then 'VIC'
			when b.b_name='Sydney' then 'NSW'
			when b.b_name='Brisbane' then 'QLD'
			when b.b_name='GoldCoast' then 'QLD'
			when b.b_name='Gold Coast' then 'QLD'
			when b.b_name='Perth' then 'WA' else b.b_name end,
	   [cd_delivery_addr0] ,
       [cd_delivery_suburb]  ,
	   [cd_delivery_postcode] ,
	   case when b1.b_name='Adelaide' then 'SA'
	        when b1.b_name='Melbourne' then 'VIC'
			when b1.b_name='Sydney' then 'NSW'
			when b1.b_name='Brisbane' then 'QLD'
			when b1.b_name='GoldCoast' then 'QLD'
			when b1.b_name='Gold Coast' then 'QLD'
			when b1.b_name='Perth' then 'WA' else b1.b_name end ,
	   cd_deliver_pod
order by cd_connote

update #temp1 set PickupDatetime=te.eventdatetime,PickupDate=convert(date,te.eventdatetime),PickupTime=convert(time,te.eventdatetime),Collectiondate=convert(date,te.eventdatetime)
from #temp1 join cpplEDI.dbo.cdcoupon on cc_consignment=Consignmentid
            join scannergateway.dbo.trackingevent te on cc_coupon=sourcereference
			join scannergateway.dbo.eventtype  e on te.eventtypeid=e.id 
where e.description  in ('Pickup') and (PickupDatetime is null or convert(date,PickupDatetime)='1900-01-01')


update #temp1 set DeliveryDatetime=te.eventdatetime,DeliveryDate=convert(date,te.eventdatetime),DeliveryTime=convert(time,te.eventdatetime)
from #temp1 join cpplEDI.dbo.cdcoupon on cc_consignment=Consignmentid
            join scannergateway.dbo.trackingevent te on cc_coupon=sourcereference
			join scannergateway.dbo.eventtype  e on te.eventtypeid=e.id 
where e.description  in ('Delivered') and (DeliveryDatetime is null or convert(date,DeliveryDatetime)='1900-01-01')




Select Consignmentid,ConnoteNumber,max(eventdatetime) as LastScandatetime,convert(varchar(100),'') as Event
into #temp2
from #temp1 join cpplEDI.dbo.cdcoupon on cc_consignment=Consignmentid
            join scannergateway.dbo.trackingevent te on cc_coupon=sourcereference
			join scannergateway.dbo.eventtype  e on te.eventtypeid=e.id 
where e.description not in ('Pickup','Delivered')
group by Consignmentid,ConnoteNumber


update #temp2 set Event=e.Description
from #temp2 join cpplEDI.dbo.cdcoupon on cc_consignment=Consignmentid
            join scannergateway.dbo.trackingevent te on cc_coupon=sourcereference and eventdatetime=LastScandatetime
			join scannergateway.dbo.eventtype  e on te.eventtypeid=e.id  

update #temp1 set LastScanRecorded=convert(varchar(100),LastScandatetime)+'  '+Event
from #temp2 where #temp2.Consignmentid=#temp1.Consignmentid

Select * from #temp1


end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDeliveryPODData]
	TO [ReportUser]
GO
