SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Heena Bajaj>
-- Create date: <21/08/2019>
-- Description:	<Description,,>
-- =============================================
--exec [dbo].[sp_RptFutile_MissedPickups] 'Missed Pickup','ALL','2020-06-05','2020-06-06','NSW'

CREATE PROCEDURE [dbo].[sp_RptFutile_MissedPickups] (@consignmentstatus Varchar(50),@Customeraccount varchar(50),@StartDate Date,@EndDate Date,@state varchar(50)) 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select	Distinct		 c.cd_account [Customer Account Number],
	c.cd_connote [Consignment Number],
	c.cd_pricecode [Service code],
	d.Shortname [Customer Name],
	cd.cb_cosmos_job [Booking Number],
	cd.cb_cosmos_date [Booking Date],
	cd_pickup_addr0 as "Pickup Customer Name",
	cd_pickup_addr1 as "Pickup Address1",
	cd_pickup_addr2 as "Pickup Address2",
	cd_pickup_addr3 as "Pickup Address3",
	cd_pickup_suburb as "Pickup Suburb",
	cd_pickup_postcode as "Pickup Postcode",

	bo.timesent [Job sent time]
	,bo.timeaccepted [Job accepted time],
  case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
			case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
			'Customer PU' as [Customer PU/Hubbed PU],
			'' as [Hubbed location],
				case cd_last_status when '' then 'Missed Pickup' else cd_last_status end as [ConsignmentStatus],
			bo.b_futile_stamp [Futile Time Stamp],
			REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
			bo.DriverId [Driver Number],
				DC.pricingzone as 'ByZone',
				b.b_name [Branch],
						case  when b.b_name='Adelaide' then 'SA'
                 when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
                 when  b.b_name='Melbourne' then 'VIC'
                 when b.b_name='Sydney' then 'NSW'
                 when b.b_name='Perth' then 'WA'
                 else 'Unknown' end as [State],
				 (CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
			cd_date [Consignment Date]
		
		
into #tempreport
from

cpplEDI.dbo.consignment(nolock) c 
inner join CpplEDI.dbo.cdcosmosbooking  cd on cd.cb_consignment=c.cd_id
inner join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
inner join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch

--left join pronto..prontodebtor PD
--On(c.cd_account = PD.Accountcode)

left join [cosmos].[dbo].[Driver]  DC--PerformanceReporting..DimContractor DC
On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)

left join [cosmos].[dbo].Operator o on o.Id=bo.allocatedby and o.branch=bo.branch

left join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
where cd_last_status in ('Futile','')  and bo.IsActive = 1  
--and
-- c.cd_account=@Customeraccount 
 and c.cd_date >=@StartDate and c.cd_date <= @EndDate --and c.cd_account in ('112951223','113102511')
 --and DC.DepotName<>''
 union all

 select	Distinct		 c.cd_account [Customer Account Number],
	c.cd_connote [Consignment Number],
	c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number],
				convert (date,scanTime) [Booking Date],
		
				cd_pickup_addr0 as "Pickup Customer Name",
					cd_pickup_addr1 as "Pickup Address1",
					cd_pickup_addr2 as "Pickup Address2",
					cd_pickup_addr3 as "Pickup Address3",
					cd_pickup_suburb as "Pickup Suburb",
					cd_pickup_postcode as "Pickup Postcode",

				bo.timesent [Job sent time]
			,bo.timeaccepted [Job accepted time],
			case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
			case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
			'Hubbed PU' as [Customer PU/Hubbed PU],
			hb.agentname [Hubbed location],
				case cd_last_status when '' then 'Missed Pickup' else cd_last_status end as [ConsignmentStatus],
			bo.b_futile_stamp [Futile Time Stamp],
			REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
			bo.DriverId [Driver Number],
				DC.pricingzone as 'ByZone',
				b.b_name [Branch],
						case  when b.b_name='Adelaide' then 'SA'
                 when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
                 when  b.b_name='Melbourne' then 'VIC'
                 when b.b_name='Sydney' then 'NSW'
                 when b.b_name='Perth' then 'WA'
                 else 'Unknown' end as [State],
				 bo.bookingnumber  as [Pickup booking],
			--	 (CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
			cd_date [Consignment Date]
		
		
--into #tempreport
from

cpplEDI.dbo.consignment(nolock) c 
--inner join CpplEDI.dbo.cdcosmosbooking  cd on cd.cb_consignment=c.cd_id
 inner join CpplEDI.dbo.cdcoupon cc on c.cd_id= cc.cc_consignment
  inner join [ScannerGateway].[dbo].[HubbedStaging] hb on  hb.TrackingNumber=cc.cc_coupon
inner join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
inner join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
left join [cosmos].[dbo].[Driver]  DC--PerformanceReporting..DimContractor DC
On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)

left join [cosmos].[dbo].Operator o on o.[Id]=bo.allocatedby

left join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode

where cd_last_status in ('Futile','')  and bo.IsActive = 1  
and c.cd_date >=@StartDate and c.cd_date <= @EndDate

select * into #tempreport1 from #tempreport where consignmentstatus in (Select item FROm dbo.fn_Split(@consignmentstatus, ','))  and
State in (Select item FROm dbo.fn_Split(@state, ','))
--and  [Customer Account Number] in (Select [Customer Account Number] FROm dbo.fn_Split(@Customeraccount, ','))


If @Customeraccount = 'All'
select * from #tempreport where [Consignment Date] >=@StartDate and [Consignment Date] <= @EndDate
and consignmentstatus in (Select item FROm dbo.fn_Split(@consignmentstatus, ','))  and
State in (Select item FROm dbo.fn_Split(@state, ','))

Else

select * from #tempreport where [Consignment Date] >=@StartDate and [Consignment Date] <= @EndDate
and consignmentstatus in (Select item FROm dbo.fn_Split(@consignmentstatus, ','))  and
State in (Select item FROm dbo.fn_Split(@state, ',')) and [Customer Account Number] = @Customeraccount


END

GO
GRANT EXECUTE
	ON [dbo].[sp_RptFutile_MissedPickups]
	TO [ReportUser]
GO
