SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Heena Bajaj>
-- Create date: <21/08/2019>
-- Description:	<Description,,>
-- =============================================
--exec [dbo].[sp_RptFutile_MissedPickups] '','112951223','2019-08-15','2019-08-15','NSW'

CREATE PROCEDURE [dbo].[sp_RptFutile_MissedPickups_SS26022020] (@consignmentstatus Varchar(50),@Customeraccount varchar(50),@StartDate Date,@EndDate Date,@state varchar(50)) 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select			 c.cd_account [Customer Account Number],
					c.cd_last_status [Consignment_Status],
					b.b_name [Branch],
					case  when b.b_name='Adelaide' then 'SA'
                 when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
                 when  b.b_name='Melbourne' then 'VIC'
                 when b.b_name='Sydney' then 'NSW'
                 when b.b_name='Perth' then 'WA'
                 else 'Unknown' end as [State],
				c.cd_connote [Consignment Number],
				d.Shortname [Customer Name],
				(CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
				cd.cb_cosmos_job [Booking Number],
				cd.cb_cosmos_date [Booking Date],
				case cd_last_status when '' then 'Missed Pickup' else cd_last_status end as [ConsignmentStatus],
			bo.b_futile_stamp [Futile Time Stamp],
			bo.b_futile_reason [Futile reason],
			bo.DriverId [Driver Number]
			,cd_date [Consignment Date]
			,bo.timesent [Job sent time]
			,bo.timeaccepted [Job accepted time]
into #tempreport
from

cpplEDI.dbo.consignment(nolock) c 
inner join CpplEDI.dbo.cdcosmosbooking  cd on cd.cb_consignment=c.cd_id
inner join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
inner join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch

left join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
where cd_last_status in ('Futile','')  and bo.IsActive = 1  and
 c.cd_account=@Customeraccount and c.cd_date >=@StartDate and c.cd_date <= @EndDate --and c.cd_account in ('112951223','113102511')



select * from #tempreport where consignmentstatus in (Select item FROm dbo.fn_Split(@consignmentstatus, ','))  and
State in (Select item FROm dbo.fn_Split(@state, ',')) 




END


--select * from pronto.dbo.prontodebtor where shortname like '%sendle%'

--select * from #tempreport where [Customer Account Number]='112951223' and [Consignment Date] ='2019-08-15' and consignmentstatus in ('missed pickup')

--exec sp_RptFutile_MissedPickups 'Missed Pickup','113102511','2019-11-18','2019-11-18','WA'
GO
