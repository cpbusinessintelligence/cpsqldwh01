SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-09-08
-- Description:	
-- =============================================
--exec [dbo].[sp_RptFutile_MissedPickups_Summary] 'Futile,Missed Pickup,Picked up','ALL','2020-09-04','2020-09-04','NSW,SA,QLD,WA,VIC'

CREATE PROCEDURE [dbo].[sp_RptFutile_MissedPickups_Summary] (@consignmentstatus Varchar(50),@Customeraccount varchar(50),@StartDate Date,@EndDate Date,@state varchar(50)) 	
AS
BEGIN
	SET NOCOUNT ON;

	--Declare @consignmentstatus Varchar(50) = 'Futile,Missed Pickup,Picked up'
	--		,@Customeraccount varchar(50) = 'ALL'
	--		,@StartDate Date = '2020-09-04'
	--		,@EndDate Date = '2020-09-04'
	--		,@state varchar(50)	= 'NSW,SA,QLD,WA,VIC'

	Select
		Distinct 
			c.cd_account [Customer Account Number]
			, c.cd_connote [Consignment Number]
			, b.b_name [Branch]
			, Case cd_last_status When '' Then 'Missed Pickup' When 'Futile' Then 'Futile' Else 'Picked Up' End As [ConsignmentStatus]
			, Case  When b.b_name='Adelaide' Then 'SA'
					When b.b_name='Brisbane' or b.b_name='Gold Coast' Then 'QLD'
					When  b.b_name='Melbourne' Then 'VIC'
					When b.b_name='Sydney' Then 'NSW'
					When b.b_name='Perth' Then 'WA'
				Else 'Unknown' End As [State]
			, cd_date [Consignment Date]
			, cd.cb_cosmos_job [Booking Number]
	Into #tempreport
	From cpplEDI.dbo.consignment(nolock) c 
		Inner Join CpplEDI.dbo.cdcosmosbooking  cd on cd.cb_consignment=c.cd_id
		Inner Join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
		Inner Join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		Left Join [cosmos].[dbo].[Driver]  DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		Left Join [cosmos].[dbo].Operator o on o.Id=bo.allocatedby and o.branch=bo.branch
		Left Join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
	Where --cd_last_status In ('Futile','')  And 
		bo.IsActive = 1  
		And c.cd_date >=@StartDate 
		And c.cd_date <= @EndDate	
	
	Union All
 
	Select	
		Distinct 
			c.cd_account [Customer Account Number]
			, c.cd_connote [Consignment Number]
			, b.b_name [Branch]
			, Case cd_last_status When '' Then 'Missed Pickup' When 'Futile' Then 'Futile' Else 'Picked Up' End As [ConsignmentStatus]
			, Case  When b.b_name='Adelaide' Then 'SA'
					When b.b_name='Brisbane' or b.b_name='Gold Coast' Then 'QLD'
					When  b.b_name='Melbourne' Then 'VIC'
					When b.b_name='Sydney' Then 'NSW'
					When b.b_name='Perth' Then 'WA'
				Else 'Unknown' End As [State]
			, cd_date [Consignment Date]
			, rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number]
	From cpplEDI.dbo.consignment(nolock) c 
		Inner Join CpplEDI.dbo.cdcoupon cc on c.cd_id= cc.cc_consignment
		Inner Join [ScannerGateway].[dbo].[HubbedStaging] hb on  hb.TrackingNumber=cc.cc_coupon
		Inner Join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
		Inner Join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		Left Join [cosmos].[dbo].[Driver]  DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		Left Join [cosmos].[dbo].Operator o on o.[Id]=bo.allocatedby
		Left Join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
	Where --cd_last_status In ('Futile','')  And 
		bo.IsActive = 1  
		And c.cd_date >=@StartDate 
		And c.cd_date <= @EndDate
---------------	
	If @Customeraccount = 'All'
	Begin
		Select * Into #tempAllCust From #tempreport 
		Where [Consignment Date] >=@StartDate 
			and [Consignment Date] <= @EndDate
			and [State] in (Select item FROm dbo.fn_Split(@state, ','))

		Select Branch, Convert(int, 0) [Futile], Convert(int, 0) [Missed] , Convert(int, 0) [Pickup] , Convert(Int, 0 ) [Total]
		Into #tempAllCust2
		From #tempAllCust
		Group by Branch

		Update T1 Set Futile = T2.Total
		From #tempAllCust2 T1
		Join (
			Select Branch, ConsignmentStatus, Count(*) [Total]
			From #tempAllCust
			Group by Branch, ConsignmentStatus
			) T2 ON T1.Branch = T2.Branch
		Where T2.ConsignmentStatus = 'Futile'

		Update T1 Set Missed = T2.Total
		From #tempAllCust2 T1
		Join (
			Select Branch, ConsignmentStatus, count(*) [Total]
			From #tempAllCust
			Group by Branch, ConsignmentStatus
			) T2 ON T1.Branch = T2.Branch 
		Where T2.ConsignmentStatus = 'Missed Pickup'

		Update T1 Set Pickup = T2.Total
		From #tempAllCust2 T1
		Join (
			Select Branch, ConsignmentStatus, count(*) [Total]
			From #tempAllCust
			Group by Branch, ConsignmentStatus
			) T2 ON T1.Branch = T2.Branch
		Where T2.ConsignmentStatus = 'Picked Up'

		Update T1 Set T1.Total = T2.Total
		From #tempAllCust2 T1
		Join (
			Select Branch, count(*) [Total]
			From #tempAllCust
			Group by Branch
			) T2 ON T1.Branch = T2.Branch
--------------------------------------------
		Select * from #tempAllCust2
	End
	Else
	Begin
		Select * Into #tempCustAcNum From #tempreport 
		Where [Consignment Date] >=@StartDate and [Consignment Date] <= @EndDate
			and [State] in (Select item FROm dbo.fn_Split(@state, ',')) 
			and [Customer Account Number] = @Customeraccount

		Select Branch, Convert(int, 0) [Futile], Convert(int, 0) [Missed] , Convert(int, 0) [Pickup] , Convert(Int, 0 ) [Total]
		Into #tempCustAcNum2
		From #tempCustAcNum
		Group by Branch

		Update T1 Set Futile = T2.Total
		From #tempCustAcNum2 T1
		Join (
			Select Branch, ConsignmentStatus, Count(*) [Total]
			From #tempCustAcNum
			Group by Branch, ConsignmentStatus
			) T2 ON T1.Branch = T2.Branch
		Where T2.ConsignmentStatus = 'Futile'

		Update T1 Set Missed = T2.Total
		From #tempCustAcNum2 T1
		Join (
			Select Branch, ConsignmentStatus, count(*) [Total]
			From #tempCustAcNum
			Group by Branch, ConsignmentStatus
			) T2 ON T1.Branch = T2.Branch 
		Where T2.ConsignmentStatus = 'Missed Pickup'

		Update T1 Set Pickup = T2.Total
		From #tempCustAcNum2 T1
		Join (
			Select Branch, ConsignmentStatus, count(*) [Total]
			From #tempCustAcNum
			Group by Branch, ConsignmentStatus
			) T2 ON T1.Branch = T2.Branch
		Where T2.ConsignmentStatus = 'Picked Up'

		Update T1 Set T1.Total = T2.Total
		From #tempCustAcNum2 T1
		Join (
			Select Branch, count(*) [Total]
			From #tempCustAcNum
			Group by Branch
			) T2 ON T1.Branch = T2.Branch
--------------------------------------------	
		Select * from #tempCustAcNum2
	End
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptFutile_MissedPickups_Summary]
	TO [ReportUser]
GO
