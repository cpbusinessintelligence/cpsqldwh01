SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Heena Bajaj>
-- Create date: <21/08/2019>
-- Description:	<Description,,>
-- Modified by: Praveen Valappil
-- =============================================
--exec [dbo].[sp_RptFutile_MissedPickups_Updated_PV] 'Futile,Missed Pickup,Partial Pickup','ALL','2020-09-04','2020-09-04','NSW,SA,QLD,WA,VIC'
CREATE PROCEDURE [dbo].[sp_RptFutile_MissedPickups_Updated] (@consignmentstatus Varchar(50),@Customeraccount varchar(50),@StartDate Date,@EndDate Date,@state varchar(50)) 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Drop table #tempreport
	--Drop table #tempPartialCG
	--Declare @consignmentstatus Varchar(50) = 'Futile,Missed Pickup,Picked Up'
	--	,@Customeraccount varchar(50) = 'ALL'
	--	,@StartDate Date = '2020-09-04'
	--	,@EndDate Date = '2020-09-04'
	--	,@state varchar(50)	= 'NSW,SA,QLD,WA,VIC'


	/************************/
	Select --cd_pickup_stamp,cd_connote,cp.cc_coupon, TE.EventTypeId
		Distinct
		c.cd_id,
		c.cd_account,
		c.cd_connote,
		c.cd_pricecode,
		cd_pickup_addr0,
		cd_pickup_addr1,
		cd_pickup_addr2,
		cd_pickup_addr3,
		cd_pickup_suburb,
		cd_pickup_postcode,
		cd_last_status,
		cd_date,
		cd_pickup_branch
	Into #tempPartialCG
	From CpplEDI..consignment c (nolock) 
	Join cppledi..cdcoupon cp (nolock) On c.cd_id = cp.cc_consignment
	Left Join ScannerGateway..TrackingEvent TE (nolock) On cp.cc_coupon =  TE.SourceReference --and TE.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'
	where 
		c.cd_date >=@StartDate and c.cd_date <= @EndDate
		and cd_pickup_stamp is not null 
		and TE.EventTypeId Is Null
	/***********************/

    -- Insert statements for procedure here
	Select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		cd.cb_cosmos_job [Booking Number],
		cd.cb_cosmos_date [Booking Date],
		cd_pickup_addr0 as "Pickup Customer Name",
		cd_pickup_addr1 as "Pickup Address1",
		cd_pickup_addr2 as "Pickup Address2",
		cd_pickup_addr3 as "Pickup Address3",
		cd_pickup_suburb as "Pickup Suburb",
		cd_pickup_postcode as "Pickup Postcode",
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Customer PU' as [Customer PU/Hubbed PU],
		'' as [Hubbed location],
		case cd_last_status when '' then 'Missed Pickup' else 'Futile' end as [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
			when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
			when  b.b_name='Melbourne' then 'VIC'
			when b.b_name='Sydney' then 'NSW'
			when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		(CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
		cd_date [Consignment Date]		
	Into #tempreport
	From cpplEDI.dbo.consignment(nolock) c 
		inner join CpplEDI.dbo.cdcosmosbooking  cd on cd.cb_consignment=c.cd_id
		inner join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		--left join pronto..prontodebtor PD On(c.cd_account = PD.Accountcode)
		left join [cosmos].[dbo].[Driver]  DC--PerformanceReporting..DimContractor DC
			On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator o on o.Id=bo.allocatedby and o.branch=bo.branch
		left join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
	where cd_last_status in ('Futile','')  
		and bo.IsActive = 1  
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate
	
	Union All
 
	select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number],
		convert (date,scanTime) [Booking Date],		
		cd_pickup_addr0 as [Pickup Customer Name],
		cd_pickup_addr1 as [Pickup Address1],
		cd_pickup_addr2 as [Pickup Address2],
		cd_pickup_addr3 as [Pickup Address3],
		cd_pickup_suburb as [Pickup Suburb],
		cd_pickup_postcode as [Pickup Postcode],
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Hubbed PU' as [Customer PU/Hubbed PU],
		hb.agentname [Hubbed location],
		case cd_last_status when '' then 'Missed Pickup' else 'Futile' end as [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
		when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
		when  b.b_name='Melbourne' then 'VIC'
		when b.b_name='Sydney' then 'NSW'
		when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		bo.bookingnumber  as [Pickup booking],
		--(CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
		cd_date [Consignment Date]		
		--into #tempreport
	From
		cpplEDI.dbo.consignment(nolock) c 
		--inner join CpplEDI.dbo.cdcosmosbooking  cd on cd.cb_consignment=c.cd_id
		inner join CpplEDI.dbo.cdcoupon cc on c.cd_id= cc.cc_consignment
		inner join [ScannerGateway].[dbo].[HubbedStaging] hb on  hb.TrackingNumber=cc.cc_coupon
		inner join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		left join [cosmos].[dbo].[Driver]  DC--PerformanceReporting..DimContractor DC
			On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator o on o.[Id]=bo.allocatedby and o.Branch = bo.Branch
		left join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
	Where cd_last_status in ('Futile','')  
		and bo.IsActive = 1  
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate

	Union All

	Select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		cd.cb_cosmos_job [Booking Number],
		cd.cb_cosmos_date [Booking Date],
		cd_pickup_addr0 as "Pickup Customer Name",
		cd_pickup_addr1 as "Pickup Address1",
		cd_pickup_addr2 as "Pickup Address2",
		cd_pickup_addr3 as "Pickup Address3",
		cd_pickup_suburb as "Pickup Suburb",
		cd_pickup_postcode as "Pickup Postcode",
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Customer PU' as [Customer PU/Hubbed PU],
		'' as [Hubbed location],
		'Partial Pickup' as [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
			when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
			when  b.b_name='Melbourne' then 'VIC'
			when b.b_name='Sydney' then 'NSW'
			when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		(CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
		cd_date [Consignment Date]
	From #tempPartialCG c 
		inner join CpplEDI.dbo.cdcosmosbooking  cd on cd.cb_consignment=c.cd_id		
		inner join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		left join [cosmos].[dbo].[Driver]  DC--PerformanceReporting..DimContractor DC
			On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator o on o.Id=bo.allocatedby and o.branch=bo.branch
		left join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
	Where bo.IsActive = 1 
		and cd_last_status not in ('Futile','')
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate
	
	Union ALL
	Select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number],
		convert (date,scanTime) [Booking Date],		
		cd_pickup_addr0 as [Pickup Customer Name],
		cd_pickup_addr1 as [Pickup Address1],
		cd_pickup_addr2 as [Pickup Address2],
		cd_pickup_addr3 as [Pickup Address3],
		cd_pickup_suburb as [Pickup Suburb],
		cd_pickup_postcode as [Pickup Postcode],
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Hubbed PU' as [Customer PU/Hubbed PU],
		hb.agentname [Hubbed location],
		'Partial Pickup' as [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
		when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
		when  b.b_name='Melbourne' then 'VIC'
		when b.b_name='Sydney' then 'NSW'
		when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		bo.bookingnumber  as [Pickup booking],
		cd_date [Consignment Date]		
	From
		#tempPartialCG c
		inner join CpplEDI.dbo.cdcoupon cc on c.cd_id= cc.cc_consignment
		inner join [ScannerGateway].[dbo].[HubbedStaging] hb on  hb.TrackingNumber=cc.cc_coupon
		inner join [CpplEDI].dbo.[branchs] b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		left join [cosmos].[dbo].[Driver]  DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator o on o.[Id]=bo.allocatedby and o.Branch = bo.Branch
		left join pronto.dbo.ProntoDebtor d on c.cd_account=d.Accountcode
	Where bo.IsActive = 1  
		and cd_last_status not in ('Futile','')
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate
		--CPAFXLC10508423

--select * from #tempreport where [Consignment Number] in (
--Select [Consignment Number] from #tempreport
--Group by [Consignment Number]
--Having count(*) > 1)


	--Select * Into #tempreport1 From #tempreport 
	--Where	consignmentstatus In (Select item From dbo.fn_Split(@consignmentstatus, ','))  
	--		and [State] in (Select item From dbo.fn_Split(@state, ','))
		--and [Customer Account Number] in (Select [Customer Account Number] FROm dbo.fn_Split(@Customeraccount, ','))

----	
	If @Customeraccount = 'All'
		Select * From #tempreport 
		Where [Consignment Date] >=@StartDate and [Consignment Date] <= @EndDate
			and consignmentstatus in (Select item FROm dbo.fn_Split(@consignmentstatus, ','))  
			and [State] in (Select item FROm dbo.fn_Split(@state, ','))
	Else
		Select * From #tempreport 
		Where [Consignment Date] >=@StartDate and [Consignment Date] <= @EndDate
			and consignmentstatus in (Select item FROm dbo.fn_Split(@consignmentstatus, ','))  
			and [State] in (Select item FROm dbo.fn_Split(@state, ',')) and [Customer Account Number] = @Customeraccount

END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptFutile_MissedPickups_Updated]
	TO [ReportUser]
GO
