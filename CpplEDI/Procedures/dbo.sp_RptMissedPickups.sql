SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- Drop procedure [sp_RptMissedPickups_SS26032018]


CREATE Procedure [dbo].[sp_RptMissedPickups](@StartDate Date,@EndDate Date) as
begin


     --'=====================================================================
    --' CP -Stored Procedure -sp_RptMissedPickups
	-- [sp_RptMissedPickups_SS26032018] '2018-03-22','2018-03-26','113075030'
    --' ---------------------------
    --' Purpose: Displays all Missed Pickups-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Mar 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/03/2015    AB      1.00    Created the procedure                             --AB20150309

    --'=====================================================================

select cb_consignment,
       cb_cosmos_date,
	   cb_cosmos_job as BookingId 
into #temp 
from cdcosmosbooking where cb_cosmos_date>=@StartDate and cb_cosmos_date<=@EndDate



select cd_connote as Connote, 
       cd_id,
	   convert(varchar(100),'') as Reference,
       convert(date,cb_cosmos_date)  as Bookingdate,
	   BookingId,convert(varchar(40),'') as CosmosBookingid,
	   cd_account as CustomerAccount,
       d.shortname as CustomerName,
       convert(varchar(100),'') as  PickupDriver,
	   cd_pickup_addr0 as PickupCustomer,
	   cd_pickup_addr1 as PickupAdd1,
	   cd_pickup_addr2 as PickupAdd2,
	   cd_pickup_addr3 as PickupAdd3,
	   cd_pickup_postcode as PickupPostcode,
	   cd_pickup_suburb as Pickupsuburb,
	   b.b_name as PickupBranch,
       convert(datetime,'') as TimeSent,
	   convert(datetime,'') as TimeBooked,
	   convert(datetime,'') as PickupETA,
	   convert(varchar(100),'') as Status,
	   convert(varchar(100),'') as RedeliveryFlag,
	   cd_pickup_stamp as ActualPickupDateTime,
       replace(cd_deliver_driver,0,'') as DeliverDriver,
	   b1.b_name as DeliveryBranch,
	   cd_last_status,
	   convert(varchar(10),'') as PickupETAZone,
	   convert(varchar(50),'') as RebookBookingId,
	   convert(date,'') as RebookedDate,
	   convert(datetime,'') as FutileStamp,
	   convert(varchar(10),'') as FutileDriver,
	   convert(varchar(500),'') as FutileReason
into #temp1 
from #temp t left join consignment c on cb_consignment=cd_id
             left join Branchs b on b.b_id=cd_pickup_branch
             left join Branchs b1 on b1.b_id=cd_deliver_branch
             left join Pronto.dbo.Prontodebtor d on d.accountcode= c.cd_account

			 union all

	   select 
	   cd_connote as Connote, 
       cd_id,
	   convert(varchar(100),'') as Reference,
       convert(date,BookingDate)  as Bookingdate,
	   BookingId,convert(varchar(40),'') as CosmosBookingid,
	   cd_account as CustomerAccount,
       d.shortname as CustomerName,
       convert(varchar(100),'') as  PickupDriver,
	   cd_pickup_addr0 as PickupCustomer,
	   cd_pickup_addr1 as PickupAdd1,
	   cd_pickup_addr2 as PickupAdd2,
	   cd_pickup_addr3 as PickupAdd3,
	   cd_pickup_postcode as PickupPostcode,
	   cd_pickup_suburb as Pickupsuburb,
	   b.b_name as PickupBranch,
       convert(datetime,'') as TimeSent,
	   convert(datetime,'') as TimeBooked,
	   convert(datetime,'') as PickupETA,
	   convert(varchar(100),'') as Status,
	   convert(varchar(100),'') as RedeliveryFlag,
	   cd_pickup_stamp as ActualPickupDateTime,
       replace(cd_deliver_driver,0,'') as DeliverDriver,
	   b1.b_name as DeliveryBranch,
	   cd_last_status,
	   convert(varchar(10),'') as PickupETAZone,
	   convert(varchar(50),'') as RebookBookingId,
	   convert(date,'') as RebookedDate,
	   convert(datetime,'') as FutileStamp,
	   convert(varchar(10),'') as FutileDriver,
	   convert(varchar(500),'') as FutileReason
				  from cosmos.dbo.booking Bo
				  inner join cppledi..consignment CONSIGNMENT with(nolock)
				  On(Bo.ToAddress5 = CONSIGNMENT.cd_connote)
				   left join cppledi..Branchs b on b.b_id=cd_pickup_branch
                   left join cppledi..Branchs b1 on b1.b_id=cd_deliver_branch
                   left join Pronto.dbo.Prontodebtor d on d.accountcode= CONSIGNMENT.cd_account 
				   left join cppledi.dbo.ParameterMissedpickup Par on d.Accountcode =Par.AccountNumber
				   where IsAPI = 1
				   and BookingDate >=@StartDate and BookingDate<=@EndDate 

update #temp1 set Timesent=b.Timesent,
               --   RebookBookingId=b.RebookBookingId,
				--  RebookedDate=b.RebookDate,
                  TimeBooked=b.timebooked,
                  PickupDriver=DriverId,
				--  PickupBranch=Branch ,
				  FutileStamp= b_futile_stamp ,
		          FutileDriver=convert(int,b_futile_driver),
		          FutileReason=b_futile_reason 
				  from  #temp1 t join cosmos.dbo.booking b on t.BookingId=b.bookingid and b.bookingdate=t.Bookingdate and replace(PickupBranch,'Gold Coast','GoldCoast')=Branch


update #temp1 set RebookBookingId=b.BookingId,
				 RebookedDate=b.BookingDate
from  cosmos.dbo.booking b where b.rebookbookingid=#temp1.bookingid  and #temp1.PickupBranch=b.Branch and b.rebookdate=#temp1.bookingdate

--join cosmos.dbo.booking b on t.BookingId=b.bookingid and b.bookingdate=t.Bookingdate and replace(PickupBranch,'Gold Coast','GoldCoast')=Branch

 update #temp1 set RedeliveryFlag=p.RedeliveryFlag from [DWH].[dbo].[Postcodes] p where p.PostCode=PickupPostcode and p.suburb=Pickupsuburb

  update #temp1 set RedeliveryFlag='CALL' where (RedeliveryFlag is null or RedeliveryFlag='')

  
  update #temp1 set Reference=cr_reference from cdref where cd_id=cr_consignment

--/*Temp Fix*/				  
--update #temp1 set Timesent=b.Timesent,
--                  BookingDate=b.bookingdate,
--                  TimeBooked=b.timebooked,
--                  PickupDriver=DriverId,
--				--  PickupBranch=Branch ,
--				  FutileStamp= b_futile_stamp ,
--		          FutileDriver=convert(int,b_futile_driver),
--		          FutileReason=b_futile_reason 
--				  from  #temp1 t join cosmos.dbo.booking b on t.BookingId=b.bookingid and b.bookingdate=dateadd("DAY",-1,t.Bookingdate) and replace(PickupBranch,'Gold Coast','GoldCoast')=Branch
--where replace(PickupBranch,'Gold Coast','GoldCoast') in ('GoldCoast','Brisbane','Adelaide') and (t.TimeSent is null or t.TimeSent ='1900-01-01 00:00:00.000') and (t.TimeBooked is null or t.TimeBooked ='1900-01-01 00:00:00.000')
--				  and (PickupETA is null or PickupETA ='1900-01-01 00:00:00.000') and t.FutileDriver='' and t.FutileReason=''

			

update #temp1 set PickupETAZone=d.ETAZone
                  from #temp1 join [DWH].[dbo].[DimContractor] d on d.drivernumber=PickupDriver and d.branch=PickupBranch



update #temp1 set PickupETA=Convert(datetime, Convert(varchar(10),[DWH].[dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups] (convert(date,(case when convert(time,isnull(TimeSent,TimeBooked))<='15:00:00.000' then Convert(datetime, Convert(varchar(10), isnull(TimeSent,TimeBooked), 103) + ' 23:59:00', 103) else Convert(datetime, Convert(varchar(10), dateadd(DAY,1,isnull(TimeSent,TimeBooked)), 103) + ' 23:59:00', 103) end )),PickupETAZone,0), 103) + ' 23:59:00', 103)
where PickupBranch ='Melbourne' and RedeliveryFlag  in ('AMPM','PM')

update #temp1 set PickupETA=Convert(datetime, Convert(varchar(10),[DWH].[dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups] (convert(date, Convert(datetime, Convert(varchar(10), dateadd(DAY,1,isnull(TimeSent,TimeBooked)), 103) + ' 23:59:00', 103) ),PickupETAZone,0), 103) + ' 23:59:00', 103)
where PickupBranch ='Melbourne' and RedeliveryFlag in ('AM','CALL')


update #temp1 set PickupETA=Convert(datetime, Convert(varchar(10),[DWH].[dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups] (convert(date,(case when convert(time,isnull(TimeSent,TimeBooked))<='16:00:00.000' then Convert(datetime, Convert(varchar(10), isnull(TimeSent,TimeBooked), 103) + ' 23:59:00', 103) else Convert(datetime, Convert(varchar(10), dateadd(DAY,1,isnull(TimeSent,TimeBooked)), 103) + ' 23:59:00', 103) end )),PickupETAZone,0), 103) + ' 23:59:00', 103)
where PickupBranch <>'Melbourne' and RedeliveryFlag  in ('AMPM','PM')

update #temp1 set PickupETA=Convert(datetime, Convert(varchar(10),[DWH].[dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups] (convert(date, Convert(datetime, Convert(varchar(10), dateadd(DAY,1,isnull(TimeSent,TimeBooked)), 103) + ' 23:59:00', 103) ),PickupETAZone,0), 103) + ' 23:59:00', 103)
where PickupBranch <>'Melbourne' and RedeliveryFlag in ('AM','CALL')

update #temp1 set status=case when isnull(ActualPickupDateTime,'')<=isnull(pickupETA,'') then 'Picked up' else 'Missed Pickup' end  where ActualPickupDateTime is not null

update #temp1 set status='Missed Pickup' where  (ActualPickupDateTime is null or ActualPickupDateTime ='1900-01-01 00:00:00.000')

--update #temp1 set status='In Progress' where PickupETA>getdate() and (ActualPickupDateTime is null or ActualPickupDateTime ='1900-01-01 00:00:00.000')



select BookingDate,
       CustomerName,
	   PickupCustomer,
	   PickupAdd1,
	   PickupAdd2,
	   PickupAdd3,
	  case when  min(convert(int,len(status))) =9 then 'Picked up' else 'Missed Pickup' end  as Statusupdate
into #temp2
from #temp1
where PickupCustomer <>'Private Residence'
group by BookingDate,
         CustomerName,
		 PickupCustomer,
		 PickupAdd1,
		 PickupAdd2,
		 PickupAdd3
order by BookingDate,CustomerName,PickupCustomer,PickupAdd1,PickupAdd2,PickupAdd3



update #temp1 set status ='Goods Not Ready'
              from #temp1 t  join #temp2 t1 on t.bookingdate=t1.bookingdate and t.CustomerName=t1.CustomerName and t.PickupCustomer=t1.PickupCustomer and t.PickupAdd1=t1.PickupAdd1 and t.PickupAdd2=t1.PickupAdd2 and t.PickupAdd3=t1.PickupAdd3
              where t.status<>t1.Statusupdate



update #temp1 set status='In Progress' where PickupETA>getdate() and (ActualPickupDateTime is null or ActualPickupDateTime ='1900-01-01 00:00:00.000')



update #temp1 set PickupBranch='Unknown' where PickupBranch is NULL

update #temp1 set status= 'Futile Pickup' where (futilestamp is not null and futilereason<>'')

update #temp1 set status='Not a CP Pickup' where RedeliveryFlag='NA' and status='Missed Pickup'
                              
update #temp1 set status='Missed and Not yet Picked up' where status='Missed Pickup' and (ActualPickupDateTime is null or ActualPickupDateTime ='1900-01-01 00:00:00.000')


select Connote,
       Reference,
       Bookingdate,
	   BookingId,
	   CustomerAccount,
	   CustomerName,
	   RedeliveryFlag,
	   PickupCustomer,
	   PickupAdd1,
	   PickupAdd2,
	   PickupAdd3,
	   PickupPostcode,
	   Pickupsuburb,
	   PickupDriver,
	   PickupBranch,
	   TimeSent,
	   TimeBooked,	
	   PickupETA,
	   ActualPickupDateTime,
	   DeliverDriver,
	   DeliveryBranch,
	   RebookBookingId,
	   isnull(RebookedDate,'') as RebookedDate,
	   cd_last_status as LastStatus,
	   Status,FutileStamp,
	   FutileDriver,
	   FutileReason
from #temp1 
where RedeliveryFlag<>''
order by bookingdate,customeraccount,pickupcustomer,pickupadd1,pickupadd2,pickupadd3 asc

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptMissedPickups]
	TO [ReportUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[sp_RptMissedPickups]
	TO [ReportUser]
GO
