SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-12-11
-- Description:	
-- =============================================
-- [sp_RptOutstandingStatusReport] '113135263','2020-12-21','2021-01-07'
CREATE PROCEDURE [dbo].[sp_RptOutstandingStatusReport_20210120]
	@AccountCode varchar(20),
	@FromDate date,
	@toDate date
AS
BEGIN
	SELECT 
		cd_id
		,[cd_account]
		,[cd_connote]
		,[cd_date]
		,[cd_customer_eta]
		,[cd_pickup_addr0]
		,[cd_pickup_suburb]
		,[cd_pickup_postcode]
		,Convert(Varchar(5),'') As [cd_pickup_state]  
		,[cd_delivery_suburb]
		,[cd_delivery_postcode]
		,Convert(Varchar(5),'') As [cd_deliver_State]  
		,[cd_items]
	INTO #Temp1
	FROM [CpplEDI]..[consignment] (NOLOCK)
	WHERE cd_account = @AccountCode and cd_date>= @FromDate and cd_date<= @toDate
	--WHERE cd_account = '113116651' and cd_date>= '2020-12-10' and cd_date<= '2020-12-11'

	SELECT T.*,C.cc_coupon 
	INTO #Temp2 
	FROM #Temp1 T 
	JOIN [cdcoupon] C ON T.cd_id = C.cc_consignment

	SELECT T2.*, L.[Id] , convert(Varchar(100),'') as EventType,convert(Datetime,'') as EventDatetime,convert(Varchar(100),'') as Driver,convert(Varchar(100),'') as ProntoID,convert(Varchar(100),'') as Barcode,convert(Varchar(100),'') as Branch,convert(Varchar(250),'') as ExceptionReason
	INTO #TempFinal
	FROM #Temp2 T2 
	Left join [ScannerGateway]..label L on T2.cc_coupon = L.LabelNumber

	SELECT T.*,ET.[Description] as Eventtype, D.code as CosmosId , D.ProntoDriverCode,
		Case Left(D.ProntoDriverCode,1) 
			When 'A' Then 'Adelaide'
			When 'S' Then 'Sydney'
			When 'W' Then 'Perth' 
			When 'G' Then 'Goldcoast'
			When 'M' Then 'Melbourne'
			When 'B' Then 'Brisbane'
			When 'C' Then 'Sydney'	
		End CosmosBranch  
	Into #TempLookup 
	From #TempFInal TF 
	Left Join [ScannerGateway]..[TrackingEvent] T on TF.Id = T.Labelid
	Left Join [ScannerGateway]..[EventType] ET on T.EventTypeId= ET.ID
	Left Join [ScannerGateway]..[Driver] D on T.DriverId = D.Id

	Select SourceReference, Max(EventDateTime) EventDateTime
	Into #TempDeleteDeliveredItems
	From #TempLookup 
	Where Eventtype in ('delivery','Delivered')
	Group By SourceReference

	Delete TD From #TempDeleteDeliveredItems TD
	Join #TempLookup TL On TL.SourceReference = TD.SourceReference
	Where TL.EventDateTime >= TD.EventDateTime
	And TL.Eventtype = 'Returned to Courier'

	Delete From #TempFInal Where cc_coupon In (Select SourceReference From #TempDeleteDeliveredItems)
	Delete From #TempLookup Where SourceReference In (Select SourceReference From #TempDeleteDeliveredItems)
		
	Delete From #TempLookup Where ExceptionReason like 'FF:Manifest%'	

	Update #TempFinal SET EventType = (Select top 1 EventType From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc)
	Update #TempFinal SET EventDatetime = (Select top 1 EventDatetime From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc)
	Update #TempFinal SET Driver = (Select top 1 CosmosId From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc)
	Update #TempFinal SET ProntoID = (Select top 1 ProntoDriverCode From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc)
	--Update #TempFinal SET EventType = Case EventType When 'Handover' Then 'CWC Scan' When 'Delivery' Then 'Delivered' Else EventType End  
	Update #TempFinal SET EventType = Case EventType When 'Delivery' Then 'Delivered' Else EventType End  
	Update #TempFinal SET EventType = 'No Activity' Where EventType Is Null and EventDatetime is null
	Update #TempFinal SET cd_pickup_state = dbo.fn_GetState_ByPostcode (cd_pickup_postcode), cd_deliver_State = dbo.fn_GetState_ByPostcode (cd_delivery_postcode) 
	Update #TempFinal SET Barcode = (Select top 1 AdditionalText1 From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc)
	Update #TempFinal SET Branch = (Select top 1 CosmosBranch From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc)
	Update #TempFinal SET ExceptionReason = (Select top 1 ExceptionReason From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc)
	
	Select Distinct DriverNumber, DriverName,ProntoId,Branch, Case When cast(RIGHT(Isnull(ProntoId,0),3) as varchar(3))  < '100' Then DriverName Else 'Run' End Description 
	Into #tmpCosmosDriver
	From Cosmos..driver where Isactive=1
		
	Select TF.*
	, Case When EventType = 'Transfer' Then 'Transferred to '+ Isnull(cd.description,'') +' '+ RIGHT('000' + Barcode, 4)  + '' Else '' End As [Description]  		
	, PC1.ETAZone [OriginZone]
	, PC2.ETAZone [DestinationZone]
	From #TempFInal TF
	Left Join #tmpCosmosDriver cd On convert(varchar(250),cd.DriverNumber) = convert(varchar(250),TF.Barcode) and cd.branch = TF.Branch
	Left Join CouponCalculator..PostCodes PC1 On PC1.PostCode = TF.[cd_pickup_postcode] and PC1.Suburb = TF.[cd_pickup_suburb]
	Left Join CouponCalculator..PostCodes PC2 On PC2.PostCode = TF.[cd_delivery_postcode] and PC2.Suburb = TF.[cd_delivery_suburb]
	--Select * from #TempFInal

End 


GO
