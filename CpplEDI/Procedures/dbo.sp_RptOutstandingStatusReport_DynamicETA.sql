SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-12-11
-- Description:	
-- =============================================
-- [sp_RptOutstandingStatusReport_DynamicETA] '112951223','2021-07-19','2021-07-20'
-- [sp_RptOutstandingStatusReport_DynamicETA_Plus1Day] '112951223','2021-07-19','2021-07-20'
CREATE PROCEDURE [dbo].[sp_RptOutstandingStatusReport_DynamicETA_Plus1Day]
	@AccountCode varchar(20),
	@FromDate date,
	@toDate date
AS
BEGIN

	Declare @AdditionalETADayFlag varchar(2) = 'Y'
	Declare @NoAdditionalDay int = 1
----------------------------------------------	
	SELECT 
		cd_id
		,[cd_account]
		,[cd_connote]
		,[cd_date]
		,[cd_customer_eta]
		,[cd_pickup_addr0]
		,[cd_pickup_suburb]
		,[cd_pickup_postcode]
		,Convert(Varchar(5),'') As [cd_pickup_state]  
		,[cd_delivery_suburb]
		,[cd_delivery_postcode]
		,Convert(Varchar(5),'') As [cd_deliver_State]  
		,[cd_items]
		, cd_pricecode
		, cd_pickup_stamp
		, cd_delivery_addr1
	INTO #Temp1
	FROM [CpplEDI]..[consignment] (NOLOCK)
	WHERE cd_account = @AccountCode and cd_date>= @FromDate and cd_date<= @toDate
	--WHERE cd_account = '112951223' and cd_date>= '2021-02-09' and cd_date<= '2021-03-09'
-- 12 Secs
-------------------
	SELECT T.*,C.cc_coupon 
	INTO #Temp2 
	FROM #Temp1 T 
	JOIN [cdcoupon] C (NOLOCK) ON T.cd_id = C.cc_consignment
-- 9 sec
------------------

	SELECT T2.*, L.[Id] 
		, Convert(Varchar(100),'') as EventType
		, Convert(Datetime,'') as EventDatetime
		, Convert(Varchar(100),'') as Driver
		, Convert(Varchar(100),'') as ProntoID
		, Convert(Varchar(100),'') as Barcode
		, Convert(Varchar(100),'') as Branch
		, Convert(Varchar(250),'') as ExceptionReason		
		, Convert(Varchar(250),'') as [AdditionalText1]		
		, Convert(datetime,NULL) PickupDateTime
		, Convert(varchar(25),NULL) PickupScannedBy
		, Convert(datetime,'') AttemptedDeliveryDateTime
		, Convert(varchar(25),'') AttemptedDeliveryScannedBy
		, Convert(date,'') DelayofDeliveryDate
		, Convert(datetime,'') FirstDateTime
		, Convert(varchar(50),'') FirstScanType				
		, Convert(varchar(250),NULL) [AttemptedDeliveryCardNumber]
		, Convert(varchar(250),NULL) [CardCategory]
		, Convert(int,NULL)  [DeliveryOptionId]
		, Convert(varchar(50),NULL) [LatestEventScanBy]
	INTO #TempFinal
	FROM #Temp2 T2 
	Left join [ScannerGateway]..label (NOLOCK) L on T2.cc_coupon = L.LabelNumber
--12 secs
-----------------

	SELECT T.*,ET.[Description] as Eventtype, D.code as CosmosId , D.ProntoDriverCode,
		Case Left(D.ProntoDriverCode,1) 
			When 'A' Then 'Adelaide'
			When 'S' Then 'Sydney'
			When 'W' Then 'Perth' 
			When 'G' Then 'Goldcoast'
			When 'M' Then 'Melbourne'
			When 'B' Then 'Brisbane'
			When 'C' Then 'Sydney'	
		End CosmosBranch		
		,Convert(datetime,NULL) PickupDateTime
		,Convert(varchar(25),NULL) PickupScannedBy
		,Convert(datetime,NULL) AttemptedDeliveryDateTime
		,Convert(varchar(25),NULL) AttemptedDeliveryScannedBy
		,Convert(date,NULL) DelayofDeliveryDate		
		,Convert(varchar(250),NULL) [AttemptedDeliveryCardNumber]
		,Convert(varchar(250),NULL) [CardCategory]
		,Convert(int,NULL)  [DeliveryOptionId]
		, TF.[cd_connote] [Consignmentcode]
		, Convert(varchar(50),NULL) [LatestEventScanBy]
	Into #TempLookup 
	From #TempFInal TF 
	Left Join [ScannerGateway]..[TrackingEvent] T (NOLOCK) on TF.Id = T.Labelid
	Left Join [ScannerGateway]..[EventType] ET (NOLOCK) on T.EventTypeId= ET.ID
	Left Join [ScannerGateway]..[Driver] D (NOLOCK) on T.DriverId = D.Id
--4.34 Min
--------------

	Select SourceReference, Max(EventDateTime) EventDateTime
	Into #TempDeleteDeliveredItems
	From #TempLookup 
	Where Eventtype in ('delivery','Delivered')
	Group By SourceReference
---21 sec 
--------------
	Delete TD From #TempDeleteDeliveredItems TD
	Join #TempLookup TL On TL.SourceReference = TD.SourceReference
	Where TL.EventDateTime >= TD.EventDateTime
	And TL.Eventtype = 'Returned to Courier'
--7 sec
--------------
	Delete From #TempFInal Where cc_coupon In (Select SourceReference From #TempDeleteDeliveredItems)
	Delete From #TempLookup Where SourceReference In (Select SourceReference From #TempDeleteDeliveredItems)		
	Delete From #TempLookup Where ExceptionReason like 'FF:Manifest%'	
--25 secs
----------------------------------------------
	CREATE TABLE #DeliveryOptions 
	( 
		DeliveryOptionId int IDENTITY
		,Category varchar(250) NULL
		,Labelnumber varchar(30)
		,ConsignmentCode varchar(30)
		,FailedDeliveryCardNumber varchar(250) NULL
		,Deliverymethod varchar(250) NULL		
		,ScanDatetime datetime NULL
		,[PickupETAZone] varchar(250) NULL
		,[NewDeliveryETAZone] varchar(250) NULL
		,ETADate date NULL
	)	
	Insert Into #DeliveryOptions ([Category],[Labelnumber],[ConsignmentCode],[FailedDeliveryCardNumber],[Deliverymethod],[ScanDatetime], PickupETAZone,NewDeliveryETAZone,[ETADate])	
	Select	
			Convert(varchar(250),'Redirection')  [Category]
			,l.labelnumber [Labelnumber]
			,c.consignmentcode consignmentcode
			,Convert(varchar(250), '') [FailedDeliveryCardNumber]
			,[SelectedDeliveryOption] [Deliverymethod]
			,c.[CreatedDateTime] [ScanDatetime] 
			,p.etazone [PickupETAZone]
			,p1.etazone [NewDeliveryETAZone]
			, NULL ETADate	
	From #TempLookup TD
		Join EzyFreight.dbo.tblredirecteditemlabel l (nolock) on l.LabelNumber = td.SourceReference
		Join EzyFreight.dbo.tblredirectedconsignment c (nolock) on l.reconsignmentid=c.reconsignmentid
		Join EzyFreight.dbo.tbladdress a (nolock) on a.addressid=[PickupAddressID]
		Join EzyFreight.dbo.tbladdress a1 (nolock) on a1.addressid=[NewDeliveryAddressID]
		Join PerformanceReporting.dbo.Postcodes p (nolock) on p.postcode=a.postcode and p.suburb=a.suburb
		Join PerformanceReporting.dbo.Postcodes p1 (nolock) on p1.postcode=a1.postcode and p1.suburb=a1.suburb	
	Where SourceReference is not null
--10 Secs	
	Insert Into #DeliveryOptions ([Category],[Labelnumber],[ConsignmentCode],[FailedDeliveryCardNumber],[ScanDatetime],[ETADate])		
	Select	Convert(varchar(250),'Redelivery on a Date') [Category]
			,l.lablenumber [Labelnumber]
			, l.ConsignmentCode consignmentcode
			,l.[CardReferenceNumber] [FailedDeliveryCardNumber]
			,l.[CreatedDateTime] [ScanDatetime]
		   ,CAST(l.[SelectedETADate] AS DATETIME) +  CAST(CAST('23:59:59' AS TIME) AS DATETIME) ETADate	
	From #TempLookup TD
		Join [EzyFreight].[dbo].[tblRedelivery] l (nolock) on l.lablenumber = td.SourceReference 
	Where l.IsProcessed=1 and SourceReference is not null
-- 2 secs

	Update #TempLookup SET [AttemptedDeliveryCardNumber] = AdditionalText1
						,[CardCategory] = Case	When (ISNULL(AdditionalText1,'') like 'NHCLC%' OR isnull(AdditionalText1,'') like '%RTCNA' OR isnull(AdditionalText1,'') like 'NHCLC%') Then 'Redelivery to Hubbed'
									When ISNULL(AdditionalText1,'') like '%SLCNA' Then 'Redelivery to POPStation'
									When (ISNULL(AdditionalText1,'') like '191%' OR ISNULL(AdditionalText1,'') like '%DPCNA') Then 'Standard Redelivery' Else '' End
						,[DeliveryOptionId]=d.[DeliveryOptionid]
    From #TempLookup T 
		Left Join #DeliveryOptions d On d.[FailedDeliveryCardNumber] = AdditionalText1
	Where SourceReference is not null 
		and T.[EventType] = 'Link Scan' And (ISNULL(AdditionalText1,'') like 'NHCLC%' OR ISNULL(AdditionalText1,'') like '191%' OR ISNULL(AdditionalText1,'') like '%CNA' OR ISNULL(AdditionalText1,'') like 'NHCLC%') AND ISNULL([CardCategory],'')=''
-- 3 secs

 	Insert Into #DeliveryOptions ([Category],[Labelnumber],[ConsignmentCode],[FailedDeliveryCardNumber],[ScanDatetime])
	Select Case	When [CardCategory]='Redelivery to HUBBED' Then 'Redelivery to HUBBED' 
				When [CardCategory]='Redelivery to POPStation' Then 'Redelivery to POPStation'
			Else '' End [Category]
		,l.SourceReference [Labelnumber]
		,Consignmentcode [consignmentcode]
		,l.[AttemptedDeliveryCardNumber] [FailedDeliveryCardNumber]
		,l.AttemptedDeliveryDateTime [ScanDatetime]	
	From #TempLookup l 
	Where ISNULL(CardCategory,'') IN ('Redelivery to Hubbed','Redelivery to POPStation') 		
		And Not Exists (Select 1 From #DeliveryOptions d Where l.SourceReference=d.labelnumber And l.CardCategory=d.category)
-- 2 secs
----------------------------------------------

	Update #TempLookup SET	PickupDateTime = T.EventDateTime
							, PickupScannedBy = PerformanceReporting.dbo.fn_CreateUniqueDriverID(CosmosBranch,CosmosId,ProntoDriverCode)
	From  #TempLookup T Where T.[Eventtype] = 'Pickup' And PickupDateTime IS NULL
-- < 1 sec
	Update #TempLookup SET	AttemptedDeliveryDateTime = T.EventDateTime
							, AttemptedDeliveryScannedBy = [PerformanceReporting].dbo.fn_CreateUniqueDriverID(CosmosBranch,CosmosId,ProntoDriverCode)
	From #TempLookup T Where T.[Eventtype] = 'Transfer' And AttemptedDeliveryDateTime IS NULL And t.AdditionalText1='6500'
-- < 1 sec
	Update #TempLookup SET	AttemptedDeliveryDateTime = T.EventDateTime
							, AttemptedDeliveryScannedBy = [PerformanceReporting].dbo.fn_CreateUniqueDriverID(CosmosBranch,CosmosId,ProntoDriverCode)							
    From #TempLookup T 	Where T.[Eventtype] = 'Attempted Delivery' And AttemptedDeliveryDateTime IS NULL
--< 1 sec
	------------------
	Update #TempLookup SET	LatestEventScanBy = [PerformanceReporting].dbo.fn_CreateUniqueDriverID(CosmosBranch,CosmosId,ProntoDriverCode)							
    From #TempLookup T 	Where EventDateTime IS NOT NULL
-- 5 secs
	----------------
	update #TempLookup set ExceptionReason=REPLACE(ExceptionReason,'31 Jun','1 Jul')
	Update #TempLookup SET	DelayofDeliveryDate = CAST(SUBSTRING(REPLACE(T.[ExceptionReason],'Delay: ',''),1,(charindex(':',replace(T.[ExceptionReason],'Delay: ',''),1)-1)) as date)
    From  #TempLookup T	Where T.[Eventtype] = 'Delay of Delivery'  And DelayofDeliveryDate IS Null
-- 25 Secs
----------------------------------------------
	Update #TempFinal SET EventType = (Select top 1 EventType From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) -- 2
	Update #TempFinal SET EventDatetime = (Select top 1 EventDatetime From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) -- < 1
	Update #TempFinal SET Driver = (Select top 1 CosmosId From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) -- < 1
	Update #TempFinal SET ProntoID = (Select top 1 ProntoDriverCode From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) -- < 1
	Update #TempFinal SET EventType = Case EventType 
											When 'Handover' Then 
												Case 
													When Driver = '9416' Then 'CWC Scan' 
													When Driver = '9994' Then 'CWC Scan' 
												Else 'Transfer' End 
											When 'Delivery' Then 'Delivered' Else EventType End   -- <1
	Update #TempFinal SET EventType = 'No Activity' Where EventType Is Null and EventDatetime is null -- <1
	Update #TempFinal SET cd_pickup_state = dbo.fn_GetState_ByPostcode (cd_pickup_postcode), cd_deliver_State = dbo.fn_GetState_ByPostcode (cd_delivery_postcode) -- < 1
	Update #TempFinal SET Barcode = (Select top 1 AdditionalText1 From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) -- <1
	Update #TempFinal SET Branch = (Select top 1 CosmosBranch From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) -- < 1
	Update #TempFinal SET ExceptionReason = (Select top 1 ExceptionReason From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) -- 10 
	----------------------------------------------
	Update #TempFinal SET AdditionalText1 = (Select top 1 Isnull(AdditionalText1,'') From #TempLookup where LabelId = #TempFinal.ID and AdditionalText1 is not null ORDER BY EventDatetime desc)	--<1
	Update #TempFinal SET FirstDateTime = (Select Min(EventDatetime) From #TempLookup Where LabelId = #TempFinal.ID) --<1
	Update #TempFinal SET FirstScanType = (Select top 1 Case EventType 
															When 'Handover' Then 
																Case When Driver = '9416' Then 'CWC Scan' When Driver = '9994' Then 'CWC Scan' 	Else 'Transfer' End 
															When 'Delivery' Then 'Delivered' Else EventType End  
											From #TempLookup Where LabelId = #TempFinal.ID and EventDatetime = #TempFinal.FirstDateTime ) --<1
	Update #TempFinal SET PickupDateTime = (Select Max(PickupDateTime) From #TempLookup where LabelId = #TempFinal.ID) --<1
	Update #TempFinal SET PickupScannedBy = (Select Max(PickupScannedBy) From #TempLookup where LabelId = #TempFinal.ID and EventDatetime = #TempFinal.PickupDateTime) --<1
	Update #TempFinal SET AttemptedDeliveryDateTime = (Select Max(AttemptedDeliveryDateTime) From #TempLookup where LabelId = #TempFinal.ID) --<1
	Update #TempFinal SET AttemptedDeliveryScannedBy = (Select Max(AttemptedDeliveryScannedBy) From #TempLookup where LabelId = #TempFinal.ID and EventDatetime = #TempFinal.AttemptedDeliveryDateTime)	 --<1
	Update #TempFinal SET DelayofDeliveryDate = (Select Max(DelayofDeliveryDate) From #TempLookup where LabelId = #TempFinal.ID) --<1
	Update #TempFinal SET AttemptedDeliveryCardNumber = (Select Top 1 AttemptedDeliveryCardNumber From #TempLookup Where LabelId = #TempFinal.ID and AttemptedDeliveryCardNumber Is not null ORDER BY EventDatetime desc) --<1
	Update #TempFinal SET CardCategory = (Select Top 1 CardCategory From #TempLookup where LabelId = #TempFinal.ID and CardCategory is not null ORDER BY EventDatetime desc) -- 36 sec
	Update #TempFinal SET DeliveryOptionId = (Select Top 1 DeliveryOptionId From #TempLookup where LabelId = #TempFinal.ID and DeliveryOptionId is not null ORDER BY EventDatetime desc) --<1
----------------------------------------------
	Update #TempFinal SET LatestEventScanBy = (Select top 1 LatestEventScanBy From #TempLookup where LabelId = #TempFinal.ID ORDER BY EventDatetime desc) --2.34 min
--------------------------------------------------
	Select Distinct DriverNumber, DriverName,ProntoId,Branch, Case When cast(RIGHT(Isnull(ProntoId,0),3) as varchar(3))  < '100' Then DriverName Else 'Run' End Description 
	Into #tmpCosmosDriver
	From Cosmos..driver (NOLOCK) 
	Where Isactive=1
-- < 1
---------------------------------------------		
	
	Select 
		cd_account 
		, cd_connote
		, cd_date
		, cd_customer_eta
		, cd_pickup_addr0
		, cd_pickup_suburb
		, cd_pickup_postcode
		, cd_pickup_state
		, cd_delivery_suburb
		, cd_delivery_postcode
		, cd_deliver_State
		, cd_items
		, rtrim(cc_coupon) cc_coupon
		, EventType
		, EventDatetime
		, Driver
		, Case When EventType = 'CwC Scan' Then 
			Case 
				When Driver = '9416' Then 'Sydney' 
				When Driver = '9994' Then 'Brisbane' 
			Else TF.Branch End 
			Else TF.Branch End [Branch]
		, TF.ProntoID
		, Case When EventType = 'Transfer' Then 'Transferred to '+ Isnull(cd.description,'') +' '+ RIGHT('000' + Barcode, 4)  + '' Else '' End As [Description]  				
		, TF.ExceptionReason
		, PC1.ETAZone [OriginZone]
		, PC2.ETAZone [DestinationZone]
		, cd_pricecode
		, Convert (date, NULL) [PickupETA]
		, Convert (date, NULL) [LatestEventETA]
		, Convert (Varchar(20), NULL) [RevenueType]	
		, Convert (Varchar(20), NULL) [ProductType]
		, [CardCategory]
		, [DeliveryOptionId]
		, [AttemptedDeliveryCardNumber]
		, PickupDateTime
		, PickupScannedBy
		, AttemptedDeliveryDateTime
		, DelayofDeliveryDate
		, AdditionalText1		
		, cd_delivery_addr1
		, [cd_date] [LabelCreatedDateTime]
		, CASE WHEN FirstScanType IN ('Pickup','Transfer','In Depot','Consolidate','Deconsolidate','Handover','In Transit','Left in Depot','Link Scan','Misdirected','Out For Delivery','Tranship','accepted in Depot')	THEN FirstDateTime ELSE NULL END [FirstDateTime]
		, Convert (Varchar(20), NULL) [LatestEventScanZone] 
		, LatestEventScanBy
	Into #TempLoadData
	From #TempFInal TF
	Left Join #tmpCosmosDriver cd On convert(varchar(250),cd.DriverNumber) = convert(varchar(250),TF.Barcode) and cd.branch = TF.Branch
	Left Join CouponCalculator..PostCodes (NOLOCK) PC1 On PC1.PostCode = TF.[cd_pickup_postcode] and PC1.Suburb = TF.[cd_pickup_suburb]
	Left Join CouponCalculator..PostCodes (NOLOCK) PC2 On PC2.PostCode = TF.[cd_delivery_postcode] and PC2.Suburb = TF.[cd_delivery_suburb]	
-- 30 secs
-----------------------------------------------------------
	Update #TempLoadData Set [CardCategory] =  Case When cd_delivery_addr1 like 'C/- %POPStation%' then '1st Time Delivery to POPStation' When cd_delivery_addr1 like '%NewsAgency%' Then '1st Time Delivery to HUBBED' Else '' End
	Update #TempLoadData Set [RevenueType] = Case When (ISnumeric([cc_coupon])=1 and len([cc_coupon]) =11) Then 'Prepaid' Else 'EDI' End
	update #TempLoadData set ProductType='Standard'
	Update #TempLoadData Set ProductType = p.category  
	From #TempLoadData l 
		Join [PerformanceReporting].[dbo].[DimProduct] p on l.cd_pricecode=p.Code
	Where p.Category in ('ACE','Domestic Priority','Domestic Off Peak')
-- 4 secs	

	Update #TempLoadData SET [LatestEventScanZone] = CASE C.ETAZone 
															WHEN 'MEL0M' THEN 'MELOM' 
															WHEN 'mel' THEN 'MELM' 
															WHEN 'mel1' THEN 'MELM'
															WHEN 'melm1' THEN 'MELM'
															WHEN 'BNME' THEN 'BNEM'
															WHEN 'BREM2' THEN 'BNEM2'
															WHEN 'WOLF' THEN 'WOLR'
															WHEN 'NCLR' THEN 'NTLR'
													 ELSE C.ETAZone END  
    From #TempLoadData L 
		Join PerformanceReporting.[dbo].[DimContractor]  C on L.LatestEventScanBy = C.DriverID 	
--<1	
	Update #TempLoadData SET [LatestEventScanZone] = Case L.Branch 
															When 'Adelaide' Then 'ADLM'
															When 'Brisbane' Then 'BNEM' 
															When 'Goldcoast' Then 'OOLM' 
															When 'Melbourne' Then 'MELM' 
															When 'Perth' Then 'PERM' 
															When 'Sydney' Then 'SYDM' 														
														Else ''
													End
    From #TempLoadData L Where (Isnull(LatestEventScanZone,'') = '' Or LatestEventScanZone ='0' )

	Update #TempLoadData SET [LatestEventScanZone] = Case When L.Driver = '9416' Then 'SYDM' Else '' End
	From #TempLoadData L Where  (Isnull(LatestEventScanZone,'') = '' or LatestEventScanZone ='0')

-- <1
-- Total 5.15 Mins
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--Updating ETAs
	Update #TempLoadData 
	SET [PickupETA] = PerformanceReporting.[dbo].[fn_CheckforaPublicHolidayinaZone]([DelayofDeliveryDate],DestinationZone) 
	Where DelayofDeliveryDate is not null and [PickupETA] is null
-- <1	
	--Update it based on where ETAdate is null and Redelivery
	Update #TempLoadData 
	SET [PickupETA] =	PerformanceReporting.[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,cd_account,LEft(t.cc_coupon,3),OriginZone,DestinationZone,cd_pricecode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
	From #TempLoadData t 
		Join #DeliveryOptions d on d.labelnumber = t.cc_coupon
	Where  t.[PickupETA] is null and d.Category in ('Redelivery to HUBBED','Redelivery to POPStation')
-- 8 secs		
	Update #DeliveryOptions 
	SET ETADate = PerformanceReporting.[dbo].[fn_CheckforaPublicHolidayinaZone](CAST(CAST(dateadd(day,1,AttemptedDeliveryDateTime) AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME),t.DestinationZone)
	From  #DeliveryOptions d  
		Join #TempLoadData t on d.labelnumber = t.cc_coupon
	Where d.ETADate is null and d.Category in ('Redelivery to HUBBED','Redelivery to POPStation')
-- <1
	--Update it based on where Pickupdate is not null and ETAdate is null and Redirection
    Update #TempLoadData 
	SET [PickupETA] = PerformanceReporting.[dbo].[fn_CheckforaPublicHolidayinaZone](dateadd(day,1,PerformanceReporting.[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,cd_account,LEft(t.cc_coupon,3),d.PickUpetaZone,d.NewDeliveryETAZone,cd_pricecode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)),d.NewDeliveryETAZone)
    From #TempLoadData t 
		Join #DeliveryOptions d on d.DeliveryOptionId=t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.PickupETA is null and d.[Category]='Redirection' and d.DeliveryMethod<>'Authority To Leave'
-- <1
    --Update it based on where Pickupdate is not null and ETAdate is null and Redirection and 'Authority to leave'	
	Update #TempLoadData 
	SET [PickupETA] = PerformanceReporting.[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,cd_account,LEft(t.cc_coupon,3),d.PickUpetaZone,d.NewDeliveryETAZone,cd_pricecode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)
	From #TempLoadData t 
		Join #DeliveryOptions d on d.DeliveryOptionId = t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.[PickupETA] is null and d.[Category]='Redirection' and d.DeliveryMethod='Authority To Leave'
-- <1	
	--Update it when pickupdate is not null and eta is null
	Update #TempLoadData 
	SET [PickupETA] = PerformanceReporting.[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,cd_account,LEft(cc_coupon,3),OriginZone,DestinationZone,cd_pricecode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
    --Where [PickupDateTime] is not null and [PickupETA] is null
	Where ISNULL([PickupDateTime],[FirstDateTime]) is not null and [PickupETA] is null and Isnull(ExceptionReason,'') not like '%Futile%'
--9.25 Min	
	--Update it when pickupdate and eta is null and Revenutype is EDI
	--Update #TempLoadData 
	--SET [PickupETA] = PerformanceReporting.[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,cd_account,LEft(cc_coupon,3),OriginZone,DestinationZone,cd_pricecode,Convert(datetime,[LabelCreatedDateTime]),'') 
	--Where [PickupDateTime] is null and RevenueType = 'EDI' and [PickupETA] is null
---3.35 Min
	Update #TempLoadData 
	SET LatestEventETA = PerformanceReporting.[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,cd_account,LEft(cc_coupon,3),LatestEventScanZone,DestinationZone,cd_pricecode,Convert(datetime,[EventDatetime]),LatestEventScanBy)     
	Where [EventDatetime] is not null and [LatestEventETA] is null and Isnull(ExceptionReason,'') not like '%Futile%'	
--7.58 Min	
--Total 16.11
-------------------------------------------------------------------------------------------------------------------------------

	Select
		cd_account
		, cd_connote
		, cc_coupon
		, Convert(date,cd_date) cd_date
		, OriginZone
		, DestinationZone
		, convert(date,cd_customer_eta) cd_customer_eta
		--, PickupETA
		, Case 
			When PickupETA Is Null Then
				Case When EventType In ('Pickup','Transfer','In Depot','Consolidate','Deconsolidate','Handover','CWC Scan','In Transit','Left in Depot','Link Scan','Misdirected','Out For Delivery','Tranship','accepted in Depot') Then
					LatestEventETA	
				Else 
					PickupETA
				End
			Else
				PickupETA
		  End As PickupETA		
		--, LatestEventETA
		, Case When EventType ='pickup' Then 
				Case When LatestEventETA <= PickupETA Then 
						LatestEventETA
					Else PickupETA 
				End
			Else LatestEventETA 
		 End As LatestEventETA		
		, cd_pickup_addr0
		, cd_pickup_suburb
		, cd_pickup_postcode
		, cd_pickup_state
		, cd_delivery_suburb
		, cd_delivery_postcode
		, cd_deliver_State
		, cd_items
		, Case	When ExceptionReason Like '%Futile%' Then 
					'Futile -' + EventType
				Else
					EventType
		  End as EventType
		, EventDatetime
		, Driver
		, Branch
		, ProntoID
		, [Description]
		, ExceptionReason	
		--, Convert (date, NULL) [ActualETA]
	Into #TempFinalLoad
	From #TempLoadData



	IF @AdditionalETADayFlag = 'Y' 
	BEGIN
		
		--Update #TempFinalLoad Set [ActualETA] = LatestEventETA
		
		Update #TempFinalLoad Set LatestEventETA = DATEADD(day,@NoAdditionalDay,LatestEventETA)
		
		Update #TempFinalLoad Set LatestEventETA = PerformanceReporting.[dbo].[fn_CheckforaPublicHolidayinaZone](LatestEventETA,DestinationZone) 
		Where LatestEventETA is not null
	
	END

	Select * From #TempFinalLoad

End 


GO
GRANT EXECUTE
	ON [dbo].[sp_RptOutstandingStatusReport_DynamicETA]
	TO [ReportUser]
GO
