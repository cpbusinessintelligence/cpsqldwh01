SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptQuantiumJBHIFIConsignmentstatus] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_RptQuantiumJBHIFIConsignmentstatus
    --' ---------------------------
    --' Purpose: sp_RptQuantiumJBHIFIConsignmentstatus-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 24 Mar 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 24/03/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

	Select distinct Sourcereference as [Tracking No] ,
			convert(varchar(10),case when et.Description='Delivered' then '20' 
				when et.Description='Attempted Delivery' then '60'
				when et.Description='Out For Delivery' then '40'
				when et.Description='Pickup' then '5'  else '' end) as [Status Code],
				convert(varchar(16),right('00'+convert(varchar(10),datepart(day,EventDatetime)),2)+'/'+right('00'+convert(varchar(2),datepart(month,EventDatetime)),2)+'/'+convert(varchar(10),year(EventDatetime))) as [Date],
				right('00'+convert(varchar(10),datepart(Hour,EventDatetime)),2)+':'+right('00'+convert(varchar(10),datepart(Minute,EventDatetime)),2) as [Time] ,
				'' as MAWB,
				'AU' as Destination
	from  ScannerGateway.dbo.trackingevent te(NOLOCK)  join ScannerGateway.dbo.eventtype et(NOLOCK) on et.id=eventtypeid
	where --( te.SourceReference like 'CPAI3GZ%' or te.SourceReference like 'CPA0YJT%' or te.SourceReference like 'CPBCQJZ%')  
	( te.SourceReference like 'CPAI3GZ%' or te.SourceReference like 'CPA0YJT%' or te.SourceReference like 'CPBCQJZ%' or te.SourceReference like 'CPW49L%' or te.SourceReference like 'CPAI3GE' or te.SourceReference like 'CPBCQJE' or te.SourceReference like 'CPB7UI%'
	)  
	and et.Description in ('Delivered' ,'Attempted Delivery' ,'Out For Delivery' ,'Pickup')
	and convert(date,eventdatetime)=convert(date,getdate())

	union

	Select 'END',
			'' as StatusCode,
			'',
			'',
			'',
			''
----Added by PV - To include website consignments: START-- 	
--	UNION
--	Select Distinct 
--		SourceReference as [Tracking No] 
--		, CONVERT(VARCHAR(10),
--			CASE 
--				WHEN ET.[Description] = 'Delivered' THEN '20' 
--				WHEN ET.[Description]='Attempted Delivery' then '60'
--				WHEN ET.[Description]='Out For Delivery' then '40'
--				WHEN ET.[Description]='Pickup' then '5'  else '' end
--			) as [Status Code]
--		, CONVERT(VARCHAR(16),RIGHT('00'+CONVERT(VARCHAR(10),DATEPART(DAY,EventDateTime)),2)+'/'+RIGHT('00'+CONVERT(VARCHAR(2),DATEPART(MONTH,EventDateTime)),2)+'/'+CONVERT(VARCHAR(10),YEAR(EventDatetime))) AS [Date]
--		, RIGHT('00'+CONVERT(VARCHAR(10),DATEPART(HOUR,EventDatetime)),2)+':'+RIGHT('00'+CONVERT(VARCHAR(10),DATEPART(MINUTE,EventDateTime)),2) AS [Time]
--		, '' as MAWB
--		, 'AU' as Destination
--	From [ScannerGateway].[dbo].[TrackingEvent] (nolock) TE
--		Join [ScannerGateway].[dbo].[EventType] ET On TE.EventTypeId = ET.Id
--		Join [EzyFreight].[dbo].[tblItemLabel] (nolock) IL On TE.SourceReference = IL.LabelNumber
--		Join [EzyFreight].[dbo].[tblConsignment] (nolock) CG On IL.ConsignmentID = CG.ConsignmentID
--		Join [EzyFreight].[dbo].[tblCompanyUsers] TCU (nolock) On CG.UserID = TCU.UserID 
--		Join [EzyFreight].[dbo].[tblCompany] TC (nolock) On TCU.CompanyID = TC.CompanyID
--	Where 
--		--(TE.SourceReference LIKE 'CPWRTHP%' OR TE.SourceReference LIKE 'CPWRTDP%')  
--		(te.SourceReference LIKE 'CPWRTRN%') 
--		And ET.[Description] in ('Delivered' ,'Attempted Delivery' ,'Out For Delivery' ,'Pickup')
--		And CONVERT(DATE,EventDateTime) = CONVERT(DATE,GETDATE())
--		And TC.AccountNumber = '113033518'
--	--Added by PV - To include website consignments: END --

End
GO
GRANT EXECUTE
	ON [dbo].[sp_RptQuantiumJBHIFIConsignmentstatus]
	TO [SSISUser]
GO
