SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_RptQuantiumJBHIFIConsignmentstatus_BUP20161018] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_RptQuantiumJBHIFIConsignmentstatus
    --' ---------------------------
    --' Purpose: sp_RptQuantiumJBHIFIConsignmentstatus-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 24 Mar 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 24/03/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

Select distinct Sourcereference as [Tracking No] ,
       convert(varchar(10),case when et.Description='Delivered' then '20' 
            when et.Description='Attempted Delivery' then '60'
			when et.Description='Out For Delivery' then '40'
			when et.Description='Pickup' then '5'  else '' end) as [Status Code],
			convert(varchar(16),right('00'+convert(varchar(2),datepart(month,EventDatetime)),2)+'/'+right('00'+convert(varchar(10),datepart(day,EventDatetime)),2)+'/'+convert(varchar(10),year(EventDatetime))+' '+right('00'+convert(varchar(10),datepart(Hour,EventDatetime)),2)+':'+right('00'+convert(varchar(10),datepart(Minute,EventDatetime)),2)) as [Date Time],
			'' as MAWB,
			'AU' as Destination
from cpplEDI.dbo.consignment (NOLOCK) join cpplEDI.dbo.cdcoupon (NOLOCK) on cc_consignment=cd_id  join ScannerGateway.dbo.trackingevent te(NOLOCK) on te.SourceReference=cc_coupon join ScannerGateway.dbo.eventtype et(NOLOCK) on et.id=eventtypeid
where cd_account in ('112970702','113033518') and cd_date=convert(date,dateadd(day,0,getdate())) and et.Description in ('Delivered' ,'Attempted Delivery' ,'Out For Delivery' ,'Pickup')

union

Select 'END',
        '' as StatusCode,
		'',
		'',
		''


end
GO
