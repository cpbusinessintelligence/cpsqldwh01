SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- EXEC [sp_RptQuantiumJBHIFIConsignmentstatus_SFTP] 'MN'
CREATE PROCEDURE [dbo].[sp_RptQuantiumJBHIFIConsignmentstatus_SFTP] 
@Interval Varchar(10) = 'MN'
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure - sp_RptQuantiumJBHIFIConsignmentstatus
    --' ---------------------------
    --' Purpose: Created repilica of existing report(sp_RptQuantiumJBHIFIConsignmentstatus). Update only date filter 
    --' Developer: Praveen Valappil
    --' Date: 2020-08-13
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 
    --'=====================================================================
	--Declare @Interval Varchar(10) = 'MN'
	Declare @ReportStartDate Datetime 
	Declare @ReportEndDate Datetime = GETDATE()

	If @Interval = 'AN'
		Select @ReportStartDate = DATEADD(HOUR,-9,@ReportEndDate)
	Else
		Select @ReportStartDate = DATEADD(HOUR,-17,@ReportEndDate)
	
	SELECT DISTINCT 
		Sourcereference as [Tracking No]
		,CONVERT(VARCHAR(10),
			CASE WHEN et.Description='Delivered' then '20' 
				WHEN et.Description='Attempted Delivery' then '60'
				WHEN et.Description='Out For Delivery' then '40'
				WHEN et.Description='Pickup' then '5'  else '' 
			END) AS [Status Code]
		,CONVERT(VARCHAR(16),RIGHT('00'+CONVERT(VARCHAR(10),DATEPART(DAY,EventDatetime)),2)+'/'+RIGHT('00'+CONVERT(VARCHAR(2),DATEPART(MONTH,EventDatetime)),2)+'/'+CONVERT(VARCHAR(10),YEAR(EventDatetime))) AS [Date]
		,RIGHT('00'+CONVERT(VARCHAR(10),DATEPART(HOUR,EventDatetime)),2)+':'+RIGHT('00'+CONVERT(VARCHAR(10),DATEPART(MINUTE,EventDatetime)),2) AS [Time]
		,'' AS MAWB
		,'AU' AS Destination
	FROM ScannerGateway.dbo.trackingevent te(NOLOCK)  
		JOIN ScannerGateway.dbo.eventtype et(NOLOCK) on et.id=eventtypeid
	where 
		--( te.SourceReference like 'CPAI3GZ%' or te.SourceReference like 'CPA0YJT%' or te.SourceReference like 'CPBCQJZ%')  
		( te.SourceReference like 'CPAI3GZ%' 
			or te.SourceReference like 'CPA0YJT%' 
			or te.SourceReference like 'CPBCQJZ%' 
			or te.SourceReference like 'CPW49L%' 
			or te.SourceReference like 'CPAI3GE' 
			or te.SourceReference like 'CPBCQJE'
			or te.SourceReference like 'CPB7UI%'
		)
		AND et.Description in ('Delivered' ,'Attempted Delivery' ,'Out For Delivery' ,'Pickup')
		--AND convert(date,eventdatetime)=convert(date,getdate()) -- commented by PV on 2020-08-13
		And te.EventDateTime >= @ReportStartDate and te.EventDateTime <= @ReportEndDate

	UNION
	Select 'END',
			'' as StatusCode,
			'',
			'',
			'',
			''
End
GO
