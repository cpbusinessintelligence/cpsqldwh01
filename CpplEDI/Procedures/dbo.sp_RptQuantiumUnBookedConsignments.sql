SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptQuantiumUnBookedConsignments] as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptQuantiumInvalidationConsignments]
    --' ---------------------------
    --' Purpose: Get the list of Quantium consignments in Validation-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 19 Jan 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 19/01/2016    AB      1.00                                                     --AB20160119

    --'=====================================================================


SELECT distinct cd_connote as Connote
                ,cd_date as Consignmentdate
	--  ,cc_coupon
	 -- ,Branch
	  ,c.cd_pickup_addr0 as PickupContact
	  ,c.cd_pickup_addr1 as PickupAdd1
	  ,c.cd_pickup_addr2 as PickupAdd2
	  ,c.cd_pickup_addr3 as PickupAdd3
	  ,c.cd_pickup_suburb as PickupSuburb
	  ,c.cd_pickup_postcode as PickupPostcodea
	  ,c.cd_delivery_contact as RecipientName
	  ,c.cd_delivery_contact_phone as RecipientPhone
	  ,c.cd_delivery_email as RecipientEmail
	  ,c.cd_special_instructions as specialInstructions
	  ,cr.cr_reference as Reference
	 -- ,isnull(BookingDatetime,bookingdate) as BookingDate
FROM [CpplEDI].[dbo].[consignment] c left join [CpplEDI].[dbo].[cdref] cr on cr.cr_consignment=cd_id left join cosmos.dbo.booking on caller=cd_connote  where cd_connote like 'CPA0YJ%' 
and bookingdate is null and cd_release<>'V' and cd_date>=dateadd(month,-1,convert(date,getdate()))
order by cd_date

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptQuantiumUnBookedConsignments]
	TO [ReportUser]
GO
