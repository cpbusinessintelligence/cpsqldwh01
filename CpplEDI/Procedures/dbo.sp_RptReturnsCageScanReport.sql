SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author: Tejes S

-- Create date: 2018-03-13

-- Description:	As per the Request ID :6808 (Generic returns cage scan report)

-- =============================================



CREATE  PROCEDURE [dbo].[sp_RptReturnsCageScanReport] @AccountCode varchar(30),@Date Date

AS

BEGIN

	SET FMTONLY OFF 
	SET NOCOUNT ON;

	--set @AccountCode='112976535'
--Declare @Date DateTime
--set @Date= '2018-03-09'


DECLARE @StartDate DateTime
DECLARE @EndDate DateTime

SET @StartDate=dateadd(dd, datediff(dd,0,@Date),0)- 1
SEt @EndDate=(dateadd(dd, datediff(dd,0,@Date),0)- 1)+ '23:59:59' 


Select C.cd_account,C.cd_connote , P.cc_coupon,c.cd_id,c.cd_date
into #Temp1
from cpplEDI.dbo.consignment C Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
where cd_account =@AccountCode and cd_date < = getdate()  and cd_date > = Dateadd(day, -30, getdate())



Select T.cd_account,T.cd_connote,t.cc_coupon,t.cd_id, L.Id,T.cd_date
into #Temp2  
from #Temp1 T left Join ScannerGateway.dbo.Label L on T.cc_coupon = L.LabelNumber


Select T.cd_account as AccountNumber,T.cd_connote as Consignment ,T.cd_id, T.cc_coupon as LabelNumber ,P.EventTypeId , P.EventDateTime as StatusDateTime,cd_date as ConsignmentDate
into #temp3
from #Temp2 T Join   ScannerGateway.[dbo].[TrackingEvent] P on T.ID = P.LabelID


select  T.*,P.Description as Status into #Temp4 from #temp3 T join  ScannerGateway.[dbo].[EventType] P on T.EventTypeId = P.ID


select AccountNumber,Consignment,LabelNumber, ConsignmentDate,[Status],StatusDateTime from #Temp4 t 
where  ConsignmentDate >= @StartDate and ConsignmentDate<=@EndDate    
ORDER BY Consignment,LabelNumber, ConsignmentDate,[Status]

END


GO
GRANT EXECUTE
	ON [dbo].[sp_RptReturnsCageScanReport]
	TO [ReportUser]
GO
