SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[sp_RptReturnsCageScanReport_Seko_Mel] @Date Date

AS

BEGIN

       SET FMTONLY OFF 
       SET NOCOUNT ON;

       --set @AccountCode='112976535'
--Declare @Date DateTime
--set @Date= '2018-03-09'


DECLARE @StartDate DateTime
DECLARE @EndDate DateTime

SET @StartDate=dateadd(dd, datediff(dd,0,@Date),0)- 1 + '12:30:00' 
SEt @EndDate=(dateadd(dd, datediff(dd,0,@Date),0)) + '12:29:59' 

SELECT  convert(Varchar(50),[SourceReference]) as LabelNumber
	  , convert(Varchar(50),'') as ConsignmentNumber
	  , convert(Varchar(50),'') as AccountCode
	  , convert(Varchar(50),'') as PickUpAddress1
	  , convert(Varchar(50),'') as PickupAddress2
	  , convert(Varchar(50),'') as PickupSuburb
	  , convert(Varchar(50),'') as PickuPPostcode
	  , convert(Varchar(50),'') as ConsignmentDate
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]
      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
 Into #Temp1
  FROM [ScannerGateway].[dbo].[TrackingEvent] where AdditionalText2 like '%DLB 45569%' 
  and [EventDateTime] between @StartDate and @Enddate

  Update #Temp1 SET ConsignmentNumber = CON.cd_connote,
                   PickUpAddress1 = CON.cd_pickup_addr0,
				   PickUpAddress2 = CON.cd_pickup_addr1,
				   PickuPPostcode = CON.cd_pickup_postcode,
				   PickupSuburb= CON.cd_pickup_suburb,
                    AccountCode = CON.cd_account,
					ConsignmentDate = CON.cd_date
  
                   From #Temp1 T left join CpplEDI.dbo.cdcoupon C on T.LabelNumber= c.cc_coupon
                                                         left join CpplEDI.dbo.consignment CON on c.cc_consignment = CON.cd_id


Select  distinct AccountCode as AccountNumber
       ,ConsignmentNumber
       ,LabelNumber
       ,convert(date,ConsignmentDate) as [ConsignmentDate]
	   , [EventDateTime] as ScannedOn
       ,PickupAddress1
	   ,PickupAddress2
       ,PickupSuburb
       ,PickupPostCode
from  #Temp1 where [EventDateTime] between @StartDate and @Enddate 
--and AccountCode IN ('112955711','113058192','113066385','113057327','113059794','113059802','113087811') 
ORDER BY [EventDateTime] ASC

END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptReturnsCageScanReport_Seko_Mel]
	TO [ReportUser]
GO
