SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE  PROCEDURE [dbo].[sp_RptReturnsRevenueReport] @StartDate Date, @EndDate Date
AS

BEGIN

       --SET FMTONLY OFF 
       SET NOCOUNT ON;


--DECLARE @StartDate DateTime
--DECLARE @EndDate DateTime

--set @StartDate='01-07-2020'
--Print @StartDate
--SET @StartDate=dateadd(dd, datediff(dd,0,@Date),0)- 1 + '15:00:00' 
--SEt @EndDate=(dateadd(dd, datediff(dd,0,@Date),0)) + '14:59:59' 

select 

TC.ConsignmentCode,
CN.cd_special_instructions [Additional Instrunctions],
PB.AccountCode [Account Number],
TC.CreatedDateTime,
TC.RateCardID,
TC.CustomerRefNo,
TC.ConsignmentPreferPickupDate [Booking ready date],
--TC.ConsignmentPreferPickupTime [Booking ready time],
Case when ConsignmentPreferPickupTime Like '00:00:00.0000000' then '12:00 AM' else ConsignmentPreferPickupTime end [Booking ready time],
PB.billingdate,
PB.BilledFreightCharge,
CN.cd_last_status,
CASE WHEN PB.ServiceCode in ('RTHP','RTDP') THEN  'Direct pickup'   WHEN PB.ServiceCode in ('RTRN') THEN 'Drop off' ELSE '' END AS Return_method
	

/*from pronto.[dbo].[ProntoBilling]   (nolock) PB   Join CPPLEDI.dbo.Consignment (nolock) CN on  CN.cd_connote = PB.ConsignmentReference 
inner join [EzyFreight].[dbo].[tblConsignment] (nolock) TC on PB.ConsignmentReference = TC.ConsignmentCode 
										   join [EzyFreight].[dbo].tblStatus TS on TC.ConsignmentStatus = TS.StatusID*/

from [EzyFreight].[dbo].[tblConsignment] (nolock) TC join pronto.[dbo].[ProntoBilling]   (nolock) PB  on  PB.ConsignmentReference = TC.ConsignmentCode 
 Join CPPLEDI.dbo.Consignment (nolock) CN on  CN.cd_connote = PB.ConsignmentReference 
  join [EzyFreight].[dbo].tblStatus TS on TC.ConsignmentStatus = TS.StatusID

where convert(date,TC.CreatedDateTime)  between @StartDate and @EndDate  and PB.ServiceCode in ('RTDP', 'RTHP', 'RTRN') 
order by TC.CreatedDateTime ASC


END

--exec [dbo].[sp_RptReturnsRevenueReport] '07-05-2020','07-07-2020'

select top 100 * from CPPLEDI.dbo.Consignment (nolock)
GO
GRANT EXECUTE
	ON [dbo].[sp_RptReturnsRevenueReport]
	TO [SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptReturnsRevenueReport]
	TO [ReportUser]
GO
