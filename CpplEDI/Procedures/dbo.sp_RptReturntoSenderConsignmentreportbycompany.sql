SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptReturntoSenderConsignmentreportbycompany](@StartDate date,@EndDate date, @Account varchar(100)) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Declare @AccountName varchar(100)

select ca_account , c_name
into #temp1
from [CpplEDI].[dbo].[companyaccount] ca  inner join [CpplEDI].[dbo].[companies]  c
on ca.ca_company_id = c.c_id
where  ca_company_id in (select c_id from [CpplEDI].[dbo].[companies] where c_name like CONCAT('%',@Account,'%'))

Set @AccountName = (select top 1 c_name from #temp1)

select con.cd_company_id, convert(varchar(100),@AccountName) as [Account Name], con.cd_connote, cd_account,con.cd_pickup_addr0,con.cd_pickup_addr1,
con.cd_pickup_addr2,con.cd_pickup_addr3,con.[cd_pickup_suburb], con.[cd_pickup_branch],
con.[cd_pickup_postcode], con.cd_date,  max(EventDateTime) as EventDateTime
into #temp2
from [dbo].[consignment] con inner join [dbo].[cdcoupon] cd on con.cd_id=cd.cc_consignment
inner join scannergateway.dbo.trackingevent te on te.SourceReference= cd.cc_coupon
where con.cd_date >= @StartDate
and con.cd_date <= @EndDate
and con.cd_account in (select ca_account from #temp1)
and ltrim(rtrim(additionaltext1))='6513'
group by con.cd_company_id,con.cd_connote, cd_account,  con.cd_date , con.cd_pickup_addr0,con.cd_pickup_addr1,
con.cd_pickup_addr2,con.cd_pickup_addr3,con.[cd_pickup_suburb], con.[cd_pickup_branch],
con.[cd_pickup_postcode]

select * from #temp2

END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptReturntoSenderConsignmentreportbycompany]
	TO [ReportUser]
GO
