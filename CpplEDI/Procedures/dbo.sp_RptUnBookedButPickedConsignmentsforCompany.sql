SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptUnBookedButPickedConsignmentsforCompany](@Account varchar(100), @StartDate date, @EndDate date) as
begin

    --' ---------------------------
    --' Purpose: Get the list of Unbooked but picked consignments in Validation-----
    --' Developer: Satya.Gandu
    --' Date: 05 Dec 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --'=====================================================================

	SELECT distinct cd_id,cd_account,cd_connote as Connote
      ,cd_date as Consignmentdate	
	  ,c.cd_pickup_addr0 as PickupContact
	  ,c.cd_pickup_addr1 as PickupAdd1
	  ,c.cd_pickup_addr2 as PickupAdd2	 
	  ,c.cd_pickup_suburb as PickupSuburb
	  ,c.cd_pickup_postcode as PickupPostcodea
	  ,c.cd_delivery_contact as RecipientName
	  ,c.cd_delivery_contact_phone as RecipientPhone
	  ,c.cd_delivery_email as RecipientEmail
	  ,c.cd_special_instructions as specialInstructions
	  ,cr.cr_reference as Reference	  
	  ,BookingId
	  ,BookingDateTime
	  ,Status as Booking_Status
	  ,b_futile_stamp
	  ,b_futile_reason
	  into #temp1
	
FROM [CpplEDI].[dbo].[consignment] c 
left join [CpplEDI].[dbo].[cdref] cr on cr.cr_consignment=cd_id 
left join cosmos.dbo.booking on caller=cd_connote  

where   c.cd_account = @Account
and bookingdate is null and cd_release<>'V' 
and cd_date between @StartDate and @EndDate
order by cd_date


select 
	cd_id,cd_account, 
	Connote
      ,Consignmentdate	
	  , PickupContact
	  , PickupAdd1
	  , PickupAdd2	 
	  , PickupSuburb
	  , PickupPostcodea
	  , RecipientName
	  , RecipientPhone
	  , RecipientEmail
	  ,specialInstructions
	  ,Reference	
	  ,BookingId
	  ,BookingDateTime
	  ,Booking_Status
	  ,b_futile_stamp
	  ,b_futile_reason
	 ,min(EventDateTime) as EventDate

	 into #temp2

 from #temp1 t left join dbo.cdcoupon cd on t.cd_id= cd.cc_consignment
left join ScannerGateway.dbo.TrackingEvent te on cd.cc_coupon = te.SourceReference
where EventTypeId='98EBB899-A15E-4826-8D05-516E744C466C'
group by 
	cd_id,cd_account, 
	Connote
      ,Consignmentdate	
	  , PickupContact
	  , PickupAdd1
	  , PickupAdd2	 
	  , PickupSuburb
	  , PickupPostcodea
	  , RecipientName
	  , RecipientPhone
	  , RecipientEmail
	  ,specialInstructions
	  ,Reference
	  ,BookingId
	  ,BookingDateTime
	  ,Booking_Status
	  ,b_futile_stamp
	  ,b_futile_reason
	  order by Connote

	  select *, 
	  isnull(EventDate,'') PickupDateTime,
	  case when eventdate=null
	  then ''
	  else 'pickedup' 
	  end as Status
	  into #temp3
	  from #temp2

	  select * from #temp3


end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptUnBookedButPickedConsignmentsforCompany]
	TO [ReportUser]
GO
