SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_Rpt_PrepaidRevenueProtectionDetail_V1](@StartDate datetime,@EndDate datetime,@BusinessUnit varchar(15))
as 
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_PrepaidRevenueProtectionDetail_V1]
    --' ---------------------------
    --' Purpose: Get the difference between Interstate Links required and Actual links and thus implement Revenue Protection-----
    --' Developer: zTejes (Couriers Please Pty Ltd)
    --' Date: 08 May 2018
    --' Copyright: 2018 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/05/2018    TS      1.00    Created the procedure                             --TS20180508

    --'=====================================================================


SELECT [RevenueRecognisedDate]
      ,[Labelnumber]
      ,[CouponType]
      ,[BU]
      ,[BUState]
      ,[Accountcode]
      ,[Accountname]
      ,[PickupDriverNumber]
      ,[PickupDriverBranch]
      ,[PickupReportingDriver]
      ,[PickupReportingDriverBranch]
      ,[DeliveryDriverNumber]
      ,[DeliveryDriverBranch]
      ,[cd_dead_weight]
      ,[FromZone]
      ,[ToZone]
      ,[LinksCount]
      ,[LinksRequired]
      ,[Difference]
      ,[RevenueDifference]
      ,[Createdby]
      ,[Createddatetime]
      ,[Editedby]
      ,[Editeddatetime]
         ,CONVERT (INT,'') AS IncludeFlag

INTO #Temp1

  FROM [dbo].[PrepaidRevenueProtection]

  WHERE [RevenueRecognisedDate] >= @StartDate AND [RevenueRecognisedDate] <= @EndDate

   select distinct sourcereference,substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1))))  as Linkcoupontemp,'' as Linkcnt into  #125 from #Temp1 a
 inner join scannergateway..trackingevent te
 On(a.labelnumber = te.sourcereference)
 where additionaltext1 like '%125%' and additionaltext1 like '%link%'
 and len(substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1)))) ) = 11

 
delete from #Temp1 where Labelnumber in (
 Select SourceReference from #125)


  DELETE FROM #Temp1 WHERE ToZone = ''
  DELETE FROM #Temp1 WHERE ToZone = '.'
  DELETE FROM #Temp1 WHERE FromZone = ''
  DELETE FROM #Temp1 WHERE FromZone = '.'

SELECT [RevenueRecognisedDate]
         ,[Accountcode]
      ,[Accountname]
      --,[PickupReportingDriver]
      --,[PickupReportingDriverBranch]
      ,[FromZone]
      ,[ToZone]
      ,SUM ([LinksCount]) AS LinksCollected
      ,SUM ([LinksRequired]) AS LinksReq
         ,SUM ([Difference]) AS LinksDifference
         ,CONVERT (INT,0) AS IncludeFlag
      
INTO #Temp2

FROM  #Temp1

GROUP BY [RevenueRecognisedDate]
         ,[Accountcode]
      ,[Accountname]
      --,[PickupReportingDriver]
      --,[PickupReportingDriverBranch]
      ,[FromZone]
      ,[ToZone]

UPDATE #Temp2 SET IncludeFlag = 1 WHERE LinksDifference > 0

UPDATE #Temp1 SET IncludeFlag = b.IncludeFlag FROM #Temp1 AS a LEFT JOIN #Temp2 AS b ON a.Accountcode = b.Accountcode AND a.RevenueRecognisedDate = b.RevenueRecognisedDate AND a.FromZone = b.FromZone AND a.ToZone = b.ToZone

--UPDATE #Temp1 SET IncludeFlag = b.IncludeFlag FROM #Temp1 AS a LEFT JOIN #Temp2 AS b ON a.Accountcode = b.Accountcode AND a.RevenueRecognisedDate = b.RevenueRecognisedDate 


If @BusinessUnit='ALL'
select * from #Temp1 WHERE IncludeFlag = 1 AND PickupDriverNumber <> '' and RevenueRecogniseddate between @StartDate and @Enddate 
ORDER BY RevenueRecogniseddate

else

select * from #Temp1 WHERE IncludeFlag = 1 AND PickupDriverNumber <> '' and RevenueRecogniseddate between @StartDate and @Enddate and BUState=@BusinessUnit
ORDER BY RevenueRecogniseddate

--SELECT * FROM #Temp1 WHERE IncludeFlag = 1 AND PickupDriverNumber <> ''

END 
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_PrepaidRevenueProtectionDetail_V1]
	TO [ReportUser]
GO
