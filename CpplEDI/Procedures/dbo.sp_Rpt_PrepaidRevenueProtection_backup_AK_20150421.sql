SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[sp_Rpt_PrepaidRevenueProtection_backup_AK_20150421](@StartDate date,@Enddate date,@BusinessUnit varchar(50))
as 
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_InterstateRevenueLinksProtection]
    --' ---------------------------
    --' Purpose: Get the difference between Interstate Links required and Actual links and thus implement Revenue Protection-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/12/2014    AB      1.00    Created the procedure                             --AB20141205

    --'=====================================================================
--drop table #tempPrepaid
select code,
      Category,
      RevenueCategory,
      Description 
into #tempPrepaid from cpsqldwh01.DWH.dbo.Dimproduct where revenuetype='Prepaid'




select RevenueRecognisedDate,
       Labelnumber,
	   Linklabelnumber,
	   Labelprefix,
	   CouponType,
	   convert(varchar(50),'') as RevenueType,
	   BU,
	   case when BU ='Adelaide' then 'SA'
	        when BU='Melbourne' then 'VIC'
			when BU in ('Sydney','CENTRAL COAST','CANBERRA')  then 'NSW'
			when BU='Perth' then 'WA'
			when BU in ('Brisbane','GOLD COAST','Sunshine Coast') then 'QLD'
			else 'Unknown' end as BUState,
	   Accountcode,
	   Accountname,
	   convert(varchar(100),'') as PickupDriverNumber,
	   convert(varchar(100),'') as PickupDriverBranch,
	   convert(varchar(100),'') as  DeliveryDriverNumber,
	   convert(varchar(100),'') as DeliveryDriverBranch,
	  -- isnull(FirstscandriverBranch,'Unknown') as FirstscandriverBranch,
	 --  isnull(FirstScanDriverNumber,'') as FirstScanDriverNumber,
	  -- isnull(OtherScanDriverBranch,'Unknown') as OtherScanDriverBranch,
	 --  isnull(OtherscanDrivernumber,'Unknown') as OtherscanDrivernumber,
	  convert(int,'') as PickupPostcode,
	   convert(varchar(200),'') as Pickupsuburb,
	   convert(int,'') as DestinationPostcode,
	   convert(varchar(200),'') as Destinationsuburb,
	   convert(varchar(20),'') as FromZone,
	   convert(varchar(20),'') as ToZone,
	   convert(varchar(50),'') as LinkCoupon,
	   convert(varchar(50),'') as LinkRevenueCategory,
	   convert(varchar(50),'') as LinkCategory,
	   convert(varchar(100),'') as LinkDescription,
	   convert(float,0) as cd_dead_weight,
	   convert(int,0) as LinksCount,
	   convert(int,0) as LinksRequired,
	   convert(int,0) as Difference,
	   convert(float,0) as RevenueDifference,
       RevenueAmount
into #temp from [cp-sql01].Revenue.dbo.v_PrepaidRevenueReporting p join #tempPrepaid tp on tp.code=p.labelprefix 
where   RevenueRecognisedDate between @StartDate and @EndDate and tp.RevenueCategory='Primary'

  CREATE CLUSTERED INDEX #TempTrackingEventsidx ON #Temp(LabelNumber)

update #temp set PickupDriverNumber=d.code,PickupDriverBranch=b.Name
from #temp t join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= t.labelnumber join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join scannergateway.dbo.driver d on d.id=te.driverid
left join scannergateway.dbo.branch b on b.id=d.BranchId
where te.eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'


update #temp set DeliveryDriverNumber=d.code,DeliveryDriverBranch=b.Name
from #temp t join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= t.labelnumber join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join scannergateway.dbo.driver d on d.id=te.driverid
left join scannergateway.dbo.branch b on b.id=d.BranchId
where te.eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')


update #temp set FromZone= isnull(d.pricingzone,'') from #temp  t 
left join cpsqldwh01.DWH.dbo.dimcontractor  d on t.PickupDriverNumber=d.DriverNumber and replace(t.PickupDriverBranch,'Gold Coast','GoldCoast')=d.Branch 

update #temp set ToZone= isnull(d1.pricingzone,'') from #temp  t 
left join cpsqldwh01.DWH.dbo.dimcontractor  d1 on t.DeliveryDriverNumber=d1.DriverNumber and replace(t.DeliveryDriverBranch,'Gold Coast','GoldCoast')=d1.Branch 


select distinct t.*,case when ltrim(rtrim(additionaltext1)) like '%Link Coupon%' then substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1)))) else '' end as Linkcoupontemp
into #templink from #temp  t left join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= t.labelnumber left join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join #tempPrepaid tp on tp.code=left(case when ltrim(rtrim(additionaltext1)) like '%Link Coupon%' then substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1)))) else '' end ,3)
where te.eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and tp.Category in ('Link','RP TRACKER')



/* For Primary coupons linked to RP's pickup is on RP's hence get the RP coupon number and get pickup details*/
--------------------------------------------------------------
update #temp set PickupDriverNumber=d.code,PickupDriverBranch=b.Name
from #temp t join #templink tl on tl.labelnumber=t.labelnumber join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= tl.Linkcoupontemp join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join scannergateway.dbo.driver d on d.id=te.driverid
left join scannergateway.dbo.branch b on b.id=d.BranchId
where te.eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'
and t.PickupDriverNumber='' and t.PickupDriverBranch=''



update #temp set FromZone= isnull(d.pricingzone,'') from #temp  t 
left join cpsqldwh01.DWH.dbo.dimcontractor  d on t.PickupDriverNumber=d.DriverNumber and replace(t.PickupDriverBranch,'Gold Coast','GoldCoast')=d.Branch 
where t.FromZone=''

-----------------------------------------------------

update #templink set LinkRevenueCategory=isnull(tp.RevenueCategory,''),LinkCategory=isnull(tp.Category,''),LinkDescription=isnull(tp.Description,'') from #templink t left join #tempPrepaid tp on tp.code=left(ltrim(rtrim(linkcoupontemp)),3)
where tp.Category = 'Link'

update #templink set linkscount=case when linkcoupon is null or linkcoupon='' then 0 else 1 end where LinkCategory = 'Link'

update #templink set linkscount=case when Linkdescription like '%Metro Link%' then 1
                                                         when  Linkdescription like '%Ezylink 1%' then 1
                                                         when  Linkdescription like '%Ezylink 2%' then 2
                                                         when  Linkdescription like '%Ezylink 3%' then 3
                                                         when  Linkdescription like '%Ezylink 5%' then 5
                                                         when  Linkdescription like '%Ezylink 10%' then 10
                                                        else Linkscount end  where LinkCategory = 'Link'
														


select RevenueRecognisedDate,
       Labelnumber,
	   CouponType,
	   BU,
	   BUState,
	   Accountcode,
	   Accountname,
	   PickupDriverNumber,
	   PickupDriverBranch,
	   DeliveryDriverNumber,
	   DeliveryDriverBranch,
	   cd_dead_weight,
	   FromZone,
	   ToZone,
	   sum(LinksCount) as LinksCounttemp,
	    convert(int,0) as LinksRequired,
	   Difference,
	  RevenueDifference
    into #templinkfinal 
	from #templink
 where LinkCategory = 'Link'
	   group by 
	   RevenueRecognisedDate,
       Labelnumber,
	   CouponType,
	   BU,
	   BUState,
	   Accountcode,
	   Accountname,
	   PickupDriverNumber,
	   PickupDriverBranch,
	   DeliveryDriverNumber,
	   DeliveryDriverBranch,
	   cd_dead_weight,
	   FromZone,
	   ToZone,
	    Difference,
	  RevenueDifference		

	  
				
update #temp set cd_dead_weight= isnull(cs.cd_deadweight,''),RevenueType='EDI'  from #temp t  join cppledi.dbo.cdcoupon c on t.labelnumber=c.cc_coupon 
                                       join cppledi.dbo.consignment cs on cs.cd_id=c.cc_consignment 

---Only for IRP's get the FromZone from EDI,for rest get from Pickup Driver----
update #temp set PickupPostcode=cs.cd_pickup_postcode,PickupSuburb=cs.cd_pickup_suburb,FromZone=[DWH].[dbo].[fn_GetETAZone](cd_pickup_postcode,cd_pickup_suburb)
                                     from #temp t left  join cppledi.dbo.cdcoupon c on t.labelnumber=c.cc_coupon 
                                                  left join cppledi.dbo.consignment cs on cs.cd_id=c.cc_consignment 
									 where RevenueType='EDI' and CouponType='IRP'
--------------------------------------------------------------------------------

--------For Prepaid Manifested in Gateway get the Tozone from Location Master(based on Pc and Suburb),for rest from DriverNumber-------

update #temp set DestinationPostcode=cs.cd_delivery_postcode,DestinationSuburb=cs.cd_delivery_suburb,ToZone=[DWH].[dbo].[fn_GetETAZone](cd_delivery_postcode,cd_delivery_suburb)
                                 from #temp t left  join cppledi.dbo.cdcoupon c on t.labelnumber=c.cc_coupon 
                                      left join cppledi.dbo.consignment cs on cs.cd_id=c.cc_consignment 
									  where RevenueType='EDI'
-----------------------------------------------------------------------------------------------------------


update #temp set FromZone=p.FromZone
from #temp t join [DWH].[dbo].[PrePaidPrefixFromZoneMapping] p on p.prefix=t.labelprefix
where t.FromZone=''


update #temp set linkscount=isnull(t1.LinksCounttemp,'')
from #temp t left join #templinkfinal t1 on t.labelnumber=t1.labelnumber

						 
update #temp set LinksRequired= isnull(case when cd_dead_weight<=15 then WB1count
            when cd_dead_weight>=16 and cd_dead_weight<=24 then WB2count
            when cd_dead_weight>=25  then WB3count
            else WB1count end,'') from #temp t left join cpsqldwh01.couponcalculator.dbo.couponcalculator c on c.fromzone=t.fromzone and c.tozone=t.tozone
where t.coupontype not like '%Sat%'

---For Satchels,irrespective of FromZone the count is same across anyzone to a particular zone----------

update #temp set LinksRequired=SatchelCount
from #temp t join cpsqldwh01.couponcalculator.dbo.couponcalculator c on c.tozone=t.tozone
where coupontype like '%Sat%'

------------------------------------------------------------------------------------------------------------


update #temp set difference=LinksRequired-LinksCount
update #temp set RevenueDifference=difference*3.90


If(@BusinessUnit='ALL')
select  RevenueRecognisedDate,
       Labelnumber,
	   CouponType,
	   BU,
	   BUState,
	   Accountcode,
	   Accountname,
	    PickupDriverNumber,
	   PickupDriverBranch,
	   DeliveryDriverNumber,
	   DeliveryDriverBranch,
	   cd_dead_weight,
	   FromZone,
	   ToZone,
	   LinksCount,
	  LinksRequired,
	  Difference,
	  RevenueDifference 
	  from #temp 
	  WHERE difference>0
	  else

	  select  RevenueRecognisedDate,
       Labelnumber,
	   CouponType,
	   BU,
	   BUState,
	   Accountcode,
	   Accountname,
	   PickupDriverNumber,
	   PickupDriverBranch,
	   DeliveryDriverNumber,
	   DeliveryDriverBranch,
	   cd_dead_weight,
	   FromZone,
	   ToZone,
	   LinksCount,
	  LinksRequired,
	  Difference,
	  RevenueDifference 
	  from #temp
	  where BUState in (select * from [DWH].[dbo].[fn_GetMultipleValues](@BusinessUnit)) 
	  and  difference>0
	

end
GO
