SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO









CREATE Procedure [dbo].[sp_Rpt_PrepaidRevenueProtection_bkup_AH_11022020]
as 
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_InterstateRevenueLinksProtection]
    --' ---------------------------
    --' Purpose: Get the difference between Interstate Links required and Actual links and thus implement Revenue Protection-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/12/2014    AB      1.00    Created the procedure                             --AB20141205

    --'=====================================================================
--drop table #tempPrepaid

select code,
      Category,
      RevenueCategory,
      Description 
into #tempPrepaid from cpsqldwh01.DWH.dbo.Dimproduct where revenuetype='Prepaid'

declare @StartDate date
set @StartDate=convert(date,dateadd("d",-(datepart("weekday",getdate())+13),getdate())) --13

declare @EndDate date
set @EndDate=dateadd("d",6,@StartDate)



select RevenueRecognisedDate,
       Labelnumber,
	   Linklabelnumber,
	   Labelprefix,
	   CouponType,
	   convert(varchar(50),'') as RevenueType,
	   BU,
	   case when BU ='Adelaide' then 'SA'
	        when BU='Melbourne' then 'VIC'
			when BU in ('Sydney','CENTRAL COAST','CANBERRA','Coffs Harbour')  then 'NSW'
			when BU='Perth' then 'WA'
			when BU in ('Brisbane','GOLD COAST','Sunshine Coast') then 'QLD'
			when BU='DARWIN' then 'NT'
			else 'Unknown' end as BUState,
	   Accountcode,
	   Accountname,
	   convert(varchar(100),'') as PickupDriverNumber,
	   convert(varchar(100),'') as PickupDriverBranch,
	   convert(varchar(100),'') as  DeliveryDriverNumber,
	   convert(varchar(100),'') as DeliveryDriverBranch,
	   convert(datetime,'') as PickupEventDatetime,
	   convert(varchar(100),'') as  PickupReportingDriver,
	   convert(varchar(100),'') as PickupReportingDriverBranch,
	   convert(datetime,'') as DeliveryEventDatetime,
	  -- isnull(FirstscandriverBranch,'Unknown') as FirstscandriverBranch,
	 --  isnull(FirstScanDriverNumber,'') as FirstScanDriverNumber,
	  -- isnull(OtherScanDriverBranch,'Unknown') as OtherScanDriverBranch,
	 --  isnull(OtherscanDrivernumber,'Unknown') as OtherscanDrivernumber,
	  convert(int,'') as PickupPostcode,
	   convert(varchar(200),'') as Pickupsuburb,
	   convert(int,'') as DestinationPostcode,
	   convert(varchar(200),'') as Destinationsuburb,
	   convert(varchar(20),'') as FromZone,
	   convert(varchar(20),'') as ToZone,
	   convert(varchar(50),'') as LinkCoupon,
	   convert(varchar(50),'') as LinkRevenueCategory,
	   convert(varchar(50),'') as LinkCategory,
	   convert(varchar(100),'') as LinkDescription,
	   convert(varchar(100),'') as ConsignmentId,
	   convert(int,0) as Itemcount,
	   convert(float,0) as cd_dead_weight,
	   convert(int,0) as LinksCount,
	   convert(int,0) as LinksRequired,
	   convert(int,0) as Difference,
	   convert(Decimal(10,2),0) as RevenueDifference,
       RevenueAmount
into #temp from Revenue.dbo.v_PrepaidRevenueReporting p join #tempPrepaid tp on tp.code=p.labelprefix 
where 
--labelnumber not in (select label from [cp-sql01].Revenue.dbo.PrepaidRevenueRecognisedExceptions) and
 RevenueRecognisedDate between @StartDate and @Enddate and tp.RevenueCategory='Primary' and left(ltrim(rtrim(Labelnumber)),3)<>'639' and left(ltrim(rtrim(Labelnumber)),3)<>'295' and firstscandatetime>dateadd("d",-21,@StartDate)



  CREATE CLUSTERED INDEX #TempTrackingEventsidx ON #Temp(LabelNumber)

  update #temp set PickupEventDatetime=(Select min(eventdatetime) from scannergateway.dbo.trackingevent te(NOLOCK) where te.sourcereference=labelnumber and  te.eventtypeid in ('98EBB899-A15E-4826-8D05-516E744C466C'))
 from scannergateway.dbo.trackingevent te(NOLOCK) where te.sourcereference=labelnumber and  te.eventtypeid in ('98EBB899-A15E-4826-8D05-516E744C466C')



 update #temp set DeliveryEventDatetime=(Select min(eventdatetime) from scannergateway.dbo.trackingevent te(NOLOCK) where te.sourcereference=labelnumber and  te.eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'))
 from scannergateway.dbo.trackingevent te(NOLOCK) where te.sourcereference=labelnumber and  te.eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')

update #temp set PickupDriverNumber=d.code,PickupDriverBranch=b.Name
from #temp t join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= t.labelnumber join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join scannergateway.dbo.driver d on d.id=te.driverid
left join scannergateway.dbo.branch b on b.id=d.BranchId
where te.eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C' and te.EventDateTime=PickupEventDatetime

--select * from trackingevent where sourcereference='20300162330' andand


update #temp set DeliveryDriverNumber=d.code,DeliveryDriverBranch=b.Name
from #temp t join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= t.labelnumber join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join scannergateway.dbo.driver d on d.id=te.driverid
left join scannergateway.dbo.branch b on b.id=d.BranchId
where te.eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2') and te.eventdatetime=DeliveryEventDatetime

update #temp set PickupReportingDriver=PickupDriverNumber,PickupReportingDriverBranch=PickupDriverBranch
from #temp

update #temp set FromZone= isnull(d.pricingzone,'') from #temp  t 
join cosmos.dbo.driver  d on t.PickupDriverNumber=d.DriverNumber and replace(t.PickupDriverBranch,'Gold Coast','GoldCoast')=d.Branch 
where d.isactive=1

update #temp set ToZone= isnull(d1.pricingzone,'') from #temp  t 
 join cosmos.dbo.driver  d1 on t.DeliveryDriverNumber=d1.DriverNumber and replace(t.DeliveryDriverBranch,'Gold Coast','GoldCoast')=d1.Branch 
 where d1.isactive=1


 update #temp set FromZone= isnull(d.pricingzone,'') from #temp  t 
join cosmos.dbo.driver  d on t.PickupDriverNumber=d.DriverNumber and replace(t.PickupDriverBranch,'Gold Coast','GoldCoast')=d.Branch 
where fromzone=''

update #temp set ToZone= isnull(d1.pricingzone,'') from #temp  t 
 join cosmos.dbo.driver  d1 on t.DeliveryDriverNumber=d1.DriverNumber and replace(t.DeliveryDriverBranch,'Gold Coast','GoldCoast')=d1.Branch 
 where tozone=''

select distinct t.*,case when ltrim(rtrim(additionaltext1)) like '%Link Coupon%' then substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1)))) else '' end as Linkcoupontemp,tp.Category
into #templink from #temp  t left join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= t.labelnumber left join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join #tempPrepaid tp on tp.code=left(case when ltrim(rtrim(additionaltext1)) like '%Link Coupon%' then substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1)))) else '' end ,3)
where te.eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and (tp.Category in ('Link','RP TRACKER') or tp.RevenueCategory='Primary')





/* For Primary coupons linked to RP's pickup is on RP's hence get the RP coupon number and get pickup details*/
--------------------------------------------------------------
--select t.labelnumber,min(eventdatetime) as PickupDatetime
--into #tempPickup
--from #temp t join #templink tl on tl.labelnumber=t.labelnumber join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= tl.Linkcoupontemp join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id 
--where te.eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'
--group by t.labelnumber

Select t.* into #tempdeletedRP
from #temp t join #templink tl on tl.labelnumber=t.labelnumber where tl.category='RP TRACKER'

delete t from #temp t join #templink tl on tl.labelnumber=t.labelnumber where tl.category='RP TRACKER'


--update #temp set PickupDriverNumber=d.code,PickupDriverBranch=b.Name
--from #temp t join #templink tl on tl.labelnumber=t.labelnumber join scannergateway.dbo.label l(NOLOCK) on  l.labelnumber= tl.Linkcoupontemp join scannergateway.dbo.trackingevent te(NOLOCK) on te.labelid=l.id left join scannergateway.dbo.driver d on d.id=te.driverid
--left join scannergateway.dbo.branch b on b.id=d.BranchId
--join #tempPickup p on p.PickupDatetime=te.EventDateTime
--where te.eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'
--and t.PickupDriverNumber='' and t.PickupDriverBranch=''


-- update #temp set FromZone= isnull(d.pricingzone,'') from #temp  t 
--join cosmos.dbo.driver  d on t.PickupDriverNumber=d.DriverNumber and replace(t.PickupDriverBranch,'Gold Coast','GoldCoast')=d.Branch 
--where fromzone=''
-------------------------------------------------------------------------

-----For RP's and IRP's Reporting should be done on Delivery driver and not PickupDriver--

--update #temp set PickupReportingDriver=t.DeliveryDriverNumber,PickupReportingDriverBranch=t.DeliveryDriverBranch
--from #temp t join #templink tl on t.labelnumber=tl.labelnumber where tl.Category='RP TRACKER'

--update #temp set PickupReportingDriver=DeliveryDriverNumber,PickupReportingDriverBranch=DeliveryDriverBranch
--from #temp t  where CouponType='IRP'

select * into #tempdeletedIRP from #temp where CouponType='IRP'

delete t from #temp t where CouponType='IRP'



-----------------------------------------------------

update #templink set LinkRevenueCategory=isnull(tp.RevenueCategory,''),LinkCategory=isnull(tp.Category,''),LinkDescription=isnull(tp.Description,'') from #templink t left join #tempPrepaid tp on tp.code=left(ltrim(rtrim(linkcoupontemp)),3)
where tp.Category = 'Link'

update #templink set linkscount=case when linkcoupon is null or linkcoupon='' then 0 else 1 end where LinkCategory = 'Link'

update #templink set linkscount=case when Linkdescription like '%Metro Link%' then 1
                                                         when  Linkdescription like '%Ezylink 1%' then 1
                                                         when  Linkdescription like '%Ezylink 2%' then 2
                                                         when  Linkdescription like '%Ezylink 3%' then 3
                                                         when  Linkdescription like '%Ezylink 5%' then 5
                                                         when  Linkdescription like '%Ezylink 10%' then 10
                                                        else Linkscount end  where LinkCategory = 'Link'

														


--drop table #templinkfinal

select RevenueRecognisedDate,
       Labelnumber,
	   CouponType,
	   BU,
	   BUState,
	   Accountcode,
	   Accountname,
	   PickupDriverNumber,
	   PickupDriverBranch,
	   DeliveryDriverNumber,
	   DeliveryDriverBranch,
	   cd_dead_weight,
	   FromZone,
	   ToZone,
	   sum(LinksCount) as LinksCounttemp,
	    convert(int,0) as LinksRequired,
	   Difference,
	  RevenueDifference
    into #templinkfinal 
	from #templink
 where LinkCategory = 'Link' or left(ltrim(rtrim(labelnumber)),3) in ('215','216','295','395','515','516','615','616','715','915')
 
 --or left(ltrim(rtrim(labelnumber)),3) in ('215','216','515','516','615','616','715','915')
	   group by 
	   RevenueRecognisedDate,
       Labelnumber,
	   CouponType,
	   BU,
	   BUState,
	   Accountcode,
	   Accountname,
	   PickupDriverNumber,
	   PickupDriverBranch,
	   DeliveryDriverNumber,
	   DeliveryDriverBranch,
	   cd_dead_weight,
	   FromZone,
	   ToZone,
	    Difference,
	  RevenueDifference		

	  
				
update #temp set cd_dead_weight= isnull(cs.cd_deadweight,''),RevenueType='EDI'  from #temp t  join cppledi.dbo.cdcoupon c on t.labelnumber=c.cc_coupon 
                                       join cppledi.dbo.consignment cs on cs.cd_id=c.cc_consignment 


---Only for IRP's get the FromZone from location Master,for rest get from Pickup Driver----
update #temp set PickupPostcode=cs.cd_pickup_postcode,PickupSuburb=cs.cd_pickup_suburb,FromZone=[DWH].[dbo].[fn_GetPricingZone](cd_pickup_postcode,cd_pickup_suburb)
                                     from #temp t left  join cppledi.dbo.cdcoupon c on t.labelnumber=c.cc_coupon 
                                                  left join cppledi.dbo.consignment cs on cs.cd_id=c.cc_consignment 
									 where RevenueType='EDI' and CouponType='IRP'
--------------------------------------------------------------------------------

--------For Prepaid Manifested in Gateway get the Tozone from Location Master(based on Pc and Suburb),for rest from DriverNumber-------

update #temp set DestinationPostcode=cs.cd_delivery_postcode,DestinationSuburb=cs.cd_delivery_suburb,ToZone=[DWH].[dbo].[fn_GetPricingZone](cd_delivery_postcode,cd_delivery_suburb)
                                 from #temp t left  join cppledi.dbo.cdcoupon c on t.labelnumber=c.cc_coupon 
                                      left join cppledi.dbo.consignment cs on cs.cd_id=c.cc_consignment 
									  where RevenueType='EDI'
-----------------------------------------------------------------------------------------------------------

--If FromZone is not yet filled,fil it from coupon prefix type--
update #temp set FromZone=p.FromZone
from #temp t join [DWH].[dbo].[PrePaidPrefixFromZoneMapping] p on p.prefix=t.labelprefix
where t.FromZone=''
-------------------------

update #temp set linkscount=isnull(t1.LinksCounttemp,'')
from #temp t left join #templinkfinal t1 on t.labelnumber=t1.labelnumber

update #temp set linkscount=linkscount+1 where left(ltrim(rtrim(labelnumber)),3) in ('215','216','295','395','515','516','615','616','715','915')

update #temp set LinksRequired= isnull(case when cd_dead_weight<=15 then WB1count
            when cd_dead_weight>15 and cd_dead_weight<=25 then WB2count
            when cd_dead_weight>25  then WB3count
            else WB1count end,'') from #temp t left join cpsqldwh01.couponcalculator.dbo.couponcalculator c on c.fromzone=t.fromzone and c.tozone=t.tozone
where t.coupontype not like '%Sat%' and (	WB1count<>'XXX' or WB2count<>'XXX' or WB3count<>'XXX')

---For Satchels,irrespective of FromZone the count is same across anyzone to a particular zone----------

update #temp set LinksRequired=SatchelCount
from #temp t join cpsqldwh01.couponcalculator.dbo.couponcalculator c on c.tozone=t.tozone
where coupontype like '%Sat%' and SatchelCount<>'XXX'



--drop table #tempdeleted

-------------Divide the weight by the number of items and then calculate links required for multiple Primary labels in one Consignment--(Applies to Prepaid MAnifested in GW only)

 
update #temp set Itemcount=cd_items from  #temp join cppledi.dbo.consignment cs on cs.cd_connote=labelnumber where  RevenueType='EDI'

--Select * from #temp where labelnumber='60706109734'

update #temp set Itemcount=cd_items from #temp join cpplEDI.dbo.cdcoupon on cc_coupon=labelnumber join cppledi.dbo.consignment cs on cs.cd_id=cc_consignment
where RevenueType='EDI' and ItemCount=0


select Labelnumber,cc_coupon,convert(int,0) as LinksCountnew,convert(float,0) as Deadweightnew,convert(int,0) as LinksRequirednew,Itemcount into #temp1 from #temp join cppledi.dbo.consignment cs on cs.cd_connote=labelnumber join cppledi.dbo.cdcoupon c on cs.cd_id=c.cc_consignment 
where Itemcount>1 and RevenueType='EDI'

--update #temp set cd_dead_weight=cd_dead_weight/t1.Itemcount  from #temp t join #temp1 t1 on t.labelnumber=t1.cc_coupon 

select t.* into #tempdeletedPrimarytoPrimary from #temp t join #temp1 t1 on t.Labelnumber=t1.cc_coupon

select t.* into #tempdeletedLinkstoPrimary from #temp t where Itemcount>1 and  RevenueType='EDI'

delete t from #temp t join #temp1 t1 on t.Labelnumber=t1.cc_coupon

delete t from #temp t where Itemcount>1 and  RevenueType='EDI'

-----------------------------------------------------------------------------------------------------------------------------------------------------------------

						 


/*
------------Aggregate all linked coupons by weight,Actual Links,Links Required and display as only one Primary and delete the rest for Prepaid Manifested in EDI----

update #temp1 set DeadweightNew=cd_dead_weight from #temp where #temp1.cc_Coupon=#temp.labelnumber

update #temp1 set LinksRequiredNew=LinksRequired from #temp where #temp.labelnumber=#temp1.cc_coupon

update #temp1 set LinksCountNew=LinksCount from #temp where #temp1.cc_Coupon=#temp.labelnumber

Select * into #temp2 from #temp1 where DeadweightNew=0 and LinksRequiredNew=0 and LinksCountNew=0

update #temp set cd_dead_weight=(Select sum(DeadweightNew) from  #temp inner join #temp1 on #temp1.labelnumber=#temp.labelnumber)
from #temp1 where #temp1.Labelnumber=#temp.labelnumber

update #temp set LinksCount=(Select sum(#temp1.LinksCountnew) from #temp1 where #temp1.labelnumber=#temp.labelnumber)
from #temp1 where #temp1.Labelnumber=#temp.labelnumber

update #temp set LinksRequired=(Select sum(LinksRequirednew) from #temp1 where #temp1.labelnumber=#temp.labelnumber)
from #temp1 where #temp1.Labelnumber=#temp.labelnumber


delete t from #temp t join #temp1 t1 on t.labelnumber=t1.cc_coupon where t1.labelnumber<>t1.cc_coupon

delete t from #temp t join #temp2 t1 on t.labelnumber=t1.labelnumber

select * from #temp where labelnumber='30203927745'
*/


-----For Primary coupon linke dto another Primary,get the list from tracking events,aggregate all the links required and actual links and display against one Primary coupon.Also, delete if any primary coupon linked is not with in the daterange we have taken as we didnt calculate the number of links for it-----------------------------
--drop table #tempprimary

Create table #tempPrimary(Labelnumber varchar(100),LinkcouponTemp varchar(100),LinksCountnew int,LinksRequirednew int)



Select distinct Labelnumber,LinkcouponTemp 
into #temp3 
from #templink where Category not  in ('Link','RP TRACKER') 

select distinct t.* into #tempdeleted1 from #temp t join #temp3 t1 on t.labelnumber=t1.labelnumber

delete t from #temp t join #temp3 t1 on t.labelnumber=t1.labelnumber

select distinct t.* into #tempdeleted2 from #temp t join #temp3 t1 on t.labelnumber=t1.linkcoupontemp

delete t from #temp t join #temp3 t1 on t.labelnumber=t1.linkcoupontemp

select * into #tempdeletedfinal from #tempdeleted1
union
select * from #tempdeletedPrimarytoPrimary
union 
select * from #tempdeletedLinkstoPrimary
union
select * from #tempdeleted2
union 
select * from #tempdeletedRP
union 
select * from #tempdeletedIRP

--truncate table PrepaidRevenueProtectionDeleted

Insert into PrepaidRevenueProtectionDeleted
select * ,getdate() as Date from #tempdeletedfinal
/*
Insert into #tempPrimary(linkcoupontemp)
select distinct labelnumber from #temp3

select distinct linkcoupontemp from #temp3


Insert into #tempPrimary (Labelnumber,LinkcouponTemp)
SELECT
  k.labelnumber,k.linkcoupontemp
FROM #temp3 k
  LEFT JOIN (SELECT
               t.Labelnumber
             FROM #temp3 t  
               INNER JOIN #temp3 r
                 ON (r.Labelnumber = t.LinkcouponTemp
                     AND t.Labelnumber = r.LinkcouponTemp)
             ) b
    ON b.Labelnumber = k.Labelnumber
WHERE b.Labelnumber IS NULL


Select t.labelnumber,t.LinkcouponTemp 
into #temp4 
from #temp3 t join #temp3 t1 on t.linkcoupontemp=t1.labelnumber and t1.linkcoupontemp=t.labelnumber
where t.labelnumber<>t.linkcoupontemp and t1.labelnumber<>t1.linkcoupontemp
order by t.labelnumber

Insert into #tempPrimary (Labelnumber,LinkcouponTemp)
select distinct Labelnumber,LinkcouponTemp
from
(select Labelnumber,LinkcouponTemp from #temp4 where LinkcouponTemp >= Labelnumber
 union 
 select LinkcouponTemp, Labelnumber from #temp4 where LinkcouponTemp < Labelnumber) v
group by Labelnumber,LinkcouponTemp


Insert into #TempPrimary(Labelnumber,LinkcouponTemp)
Select distinct labelnumber,labelnumber from #TempPrimary 

select * from #temp t right join #tempprimary t1 on t.labelnumber=t1.linkcoupontemp  where t.labelnumber='50604780170'

where t.labelnumber is null   #tempprimary

select * from #temp t where labelnumber=


update #tempPrimary set LinksRequiredNew=LinksRequired from #temp where #temp.labelnumber=#tempPrimary.LinkcouponTemp

update #tempPrimary set LinksCountNew=LinksCount from #temp where #tempPrimary.LinkcouponTemp=#temp.labelnumber

delete t from #temp t join #tempPrimary t1 on t.labelnumber=t1.labelnumber where LinksRequiredNew is null and LinksRequiredNew is null

update #temp set LinksCount=(Select sum(#tempPrimary.LinksCountnew) from #tempPrimary where #tempPrimary.labelnumber=#temp.labelnumber)
from #tempprimary where #tempprimary.Labelnumber=#temp.labelnumber

update #temp set LinksRequired=(Select sum(LinksRequirednew) from #tempPrimary where #tempPrimary.labelnumber=#temp.labelnumber)
from #tempprimary where #tempprimary.Labelnumber=#temp.labelnumber

delete t from #temp t join #tempPrimary t1 on t.labelnumber=t1.LinkcouponTemp where t1.labelnumber<>t1.LinkcouponTemp
*/
-------------------------SS Start-------------------------------------------------------

 select distinct sourcereference,substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1))))  as Linkcoupontemp,'' as Linkcnt into  #125 from #temp a
 inner join scannergateway..trackingevent te
 On(a.labelnumber = te.sourcereference)
 where additionaltext1 like '%125%' and additionaltext1 like '%link%'
 and len(substring(ltrim(rtrim(AdditionalText1)),13,len(ltrim(rtrim(AdditionalText1)))) ) = 11

Delete from #temp where LabelNumber in
 (select sourcereference from #125)

--select te.sourcereference,count(*) count1 into #125Cnt
--from  #125 a
-- inner join scannergateway..trackingevent te
-- On(a.Linkcoupontemp = te.sourcereference)
--where te.sourcereference like '125%' and additionaltext1 like '%link%' and len(te.sourcereference) = 11
--group by te.sourcereference


--update #125
--set linkcnt = count1
--from #125 a
--inner join #125Cnt b
--on(a.linkcoupontemp = b.sourcereference)
--where linkcnt <>'*'

--update #temp
--set LinksCount = Linkcnt
--from #temp a
--inner join #125 b
--on(a.labelnumber = b.sourcereference)
--where linkcnt <>'*'  and a.labelnumber in (select sourcereference from #125)

--select * from #temp where labelnumber = '50408917223' 
---------------------------------------------------------SS End------------------------------------------

update #temp set difference=LinksRequired-LinksCount
update #temp set RevenueDifference=difference*4.10
-- SS





Insert into PrepaidRevenueProtection([RevenueRecognisedDate]
      ,[Labelnumber]
      ,[CouponType]
      ,[BU]
      ,[BUState]
      ,[Accountcode]
      ,[Accountname]
      ,[PickupDriverNumber]
      ,[PickupDriverBranch]
      ,[PickupReportingDriver]
      ,[PickupReportingDriverBranch]
      ,[DeliveryDriverNumber]
      ,[DeliveryDriverBranch]
      ,[cd_dead_weight]
      ,[FromZone]
      ,[ToZone]
      ,[LinksCount]
      ,[LinksRequired]
      ,[Difference]
      ,[RevenueDifference])
	  
Select RevenueRecognisedDate,
       Labelnumber,
	   CouponType,
	   BU,
	   BUState,
	   Accountcode,
	   Accountname,
	    PickupDriverNumber,
	   PickupDriverBranch,
	   PickupReportingDriver,
	   PickupReportingDriverBranch,
	   DeliveryDriverNumber,
	   DeliveryDriverBranch,
	   cd_dead_weight,
	   FromZone,
	   ToZone,
	   LinksCount,
	  LinksRequired,
	  Difference,
	  RevenueDifference 
	  from #temp 
	  WHERE difference<>0 and coupontype<>'ONE-OFF' and revenuerecogniseddate not in (Select revenuerecogniseddate from PrepaidRevenueProtection)




	

end







GO
