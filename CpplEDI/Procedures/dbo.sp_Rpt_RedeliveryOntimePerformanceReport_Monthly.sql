SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_Rpt_RedeliveryOntimePerformanceReport_Monthly](@year int, @month int) as
begin

    --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_RedeliveryOntimePerformanceReport_Monthly]
    --' ---------------------------
    --' Purpose:Monthly Ontime Performance Report on Redeliveries-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 19 Nov 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 19/11/2014    AB      1.00    Created the procedure                             --AB20141119

    --'=====================================================================


SELECT [rl_redelivery_coupon] as RedeliveryCard
      ,[rl_tracked] as PrimaryCoupon 
      ,[rl_stamp] as RedeliveryBookedTime
      ,CONVERT(Datetime,Null) as DeliveryTime
      ,CONVERT (Varchar(20),'') as DeliveryEvent
      ,CONVERT (Varchar(10),'') as Ontime
      ,CONVERT (Varchar(20),'') as DeliveryDriver
      ,CONVERT (Varchar(20),'') as DeliveryBranch
      ,CONVERT (Varchar(20),'') as DeliveryDepot
	  ,CONVERT (Varchar(20),'') as DeliveryState
      ,[rl_delivery_date] as RedeliveryDate
      ,[rl_delivery_am_pm] as AMPM
      ,[rl_delivery_instructions] as RedeliveryInstructions
      ,[rl_delivery_name] as RedeliveryName
      ,[rl_delivery_addr1] as Address1
      ,[rl_delivery_suburb] as Suburb
      ,[rl_delivery_postcode] as Postcode 
  INTO #Temp1
  FROM [CpplEDI].[dbo].[redeliverylog]
  Where year(CONVERT(Date,[rl_delivery_date])) =@Year and month(CONVERT(Date,[rl_delivery_date])) =@Month

  delete  #temp1 from  #temp1 t join [DWH].[dbo].[DimProduct] p on p.code=left(ltrim(rtrim(PrimaryCoupon)),3)
  where ( len(PrimaryCoupon )=11 and isnumeric(PrimaryCoupon )=1)  and p.[RevenueCategory]<>'Primary'

  
  
--Drop Table #Tempscans
Select L.LabelNumber,E.Description,D.Code,D.BranchId,D.DepotId,T.EventDateTime , CONVERT (Varchar(20),'') as DeliveryBranch
      ,CONVERT (Varchar(20),'') as DeliveryDepot 
      into #TempScans
      from ScannerGateway.dbo.TrackingEvent T Join ScannerGateway.dbo.Label L on T.LabelId=L.id 
                                                 Join #Temp1 T1 on T1.PrimaryCoupon = L.LabelNumber
                                                 join ScannerGateway.dbo.EventType E on T.EventTypeId=E.id
                                                 Left Join ScannerGateway.dbo.Driver D on T.DriverId=D.ID
                                                 WHERE EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
UNION all
 Select L.LabelNumber,E.Description,D.Code,D.BranchId,D.DepotId,T.EventDateTime,CONVERT (Varchar(20),'') as DeliveryBranch
      ,CONVERT (Varchar(20),'') as DeliveryDepot from ScannerGateway.dbo.TrackingEvent T Join ScannerGateway.dbo.Label L on T.LabelId=L.id 
                                                 Join #Temp1 T1 on T1.PrimaryCoupon = L.LabelNumber
                                                 join ScannerGateway.dbo.EventType E on T.EventTypeId=E.id
                                                    Left Join ScannerGateway.dbo.Driver D on T.DriverId=D.ID
                                                 WHERE EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' 
                                                  and EventDateTime> T1.RedeliveryBookedTime                                             



Update #TempScans SET DeliveryBranch = B.CosmosBranch  From #TempScans S  LEFT Join ScannerGateway.dbo.Branch B on S.BranchId=B.Id
Update #TempScans SET DeliveryBranch ='Unknown' where DeliveryBranch is NULL
Update #TempScans SET DeliveryDepot = ltrim(rtrim(B.Name))  From #TempScans S Join ScannerGateway.dbo.Depot B on ltrim(rtrim(S.DepotId))=ltrim(rtrim(B.Id))



Update #Temp1 SET  DeliveryDriver =S.Code,DeliveryBranch = S.DeliveryBranch,DeliveryDepot=S.DeliveryDepot ,DeliveryTime = S.EventDateTime,DeliveryEvent = S.Description
From #Temp1 T join #TempScans S on T.PrimaryCoupon = S.LabelNumber

Update #Temp1 set DeliveryState=case when Deliverybranch='Adelaide' then 'SA'
when Deliverybranch='Brisbane' or Deliverybranch='Goldcoast' or Deliverybranch='Gold coast'  then 'QLD'
when Deliverybranch='Melbourne' then 'VIC'
when Deliverybranch='Perth' then 'WA'
when Deliverybranch='Sydney' then 'NSW'
else 'Unknown'
end

update #temp1 set Deliverystate=p.[state] from #temp1 t join [cpsqldwh01].[CouponCalculator].[dbo].[PostCodes] p on ltrim(rtrim(t.postcode))=ltrim(rtrim(p.postcode)) and ltrim(rtrim(t.suburb))=ltrim(rtrim(p.suburb))
where Deliverystate='Unknown'


Update #Temp1 SET Ontime = CaSE  WHEN DeliveryTime IS Null  Then 'N' WHEN  CONVERT(Date,DeliveryTime) > RedeliveryDate THEN 'N' ELSE 'Y' END

Select datepart(yyyy,RedeliveryDate)*100+datepart(mm,RedeliveryDate) as [MonthKey] ,DeliveryState,OnTime,count(*) as TotalCount,convert(int,0) as CountbyState,convert(decimal(12,1),0) as Percentage into #temp2 from #Temp1
group by datepart(yyyy,RedeliveryDate)*100+datepart(mm,RedeliveryDate),DeliveryState,OnTime


Select DeliveryState,sum(TotalCount) as CountbyState into #temp3 from #Temp2
group by DeliveryState


update #temp2 set countbystate=t.CountbyState from #temp2 t1 join #temp3 t   on t.deliverystate=t1.deliverystate

update #temp2 set percentage=Convert(decimal(12,1),TotalCount)*100 / Convert(decimal(12,1),Isnull((countbystate),0))  




--update #temp2 set percentage=Convert(decimal(12,2),TotalCount)*100 / Convert(decimal(12,2),Isnull((Select SUM(TotalCount) from #Temp2),0))  



select * from #temp2
end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_RedeliveryOntimePerformanceReport_Monthly]
	TO [ReportUser]
GO
