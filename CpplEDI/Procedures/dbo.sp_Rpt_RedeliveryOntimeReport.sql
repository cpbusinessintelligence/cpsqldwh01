SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_Rpt_RedeliveryOntimeReport](@date date) as
begin

    --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_RedeliveryOntimeReport]
    --' ---------------------------
    --' Purpose: Ontime Report on Redeliveries-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 13 Nov 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 13/11/2014    AB      1.00    Created the procedure                             --AB20141113

    --'=====================================================================


SELECT [rl_redelivery_coupon] as RedeliveryCard
      ,[rl_tracked] as PrimaryCoupon 
      ,[rl_stamp] as RedeliveryBookedTime
      ,CONVERT(Datetime,Null) as DeliveryTime
      ,CONVERT (Varchar(20),'') as DeliveryEvent
      ,CONVERT (Varchar(10),'') as Ontime
      ,CONVERT (Varchar(20),'') as DeliveryDriver
      ,CONVERT (Varchar(20),'') as DeliveryBranch
      ,CONVERT (Varchar(20),'') as DeliveryDepot
	  ,CONVERT (Varchar(20),'') as DeliveryState
      ,[rl_delivery_date] as RedeliveryDate
	  ,case when [rl_delivery_style]='A' then 'ATL' when [rl_delivery_style]='S' then 'Signature' else [rl_delivery_style] end as Redeliverystyle
      ,[rl_delivery_am_pm] as AMPM
      ,[rl_delivery_instructions] as RedeliveryInstructions
      ,[rl_delivery_name] as RedeliveryName
      ,[rl_delivery_addr1] as Address1
	  ,[rl_delivery_addr2] as Address2
      ,[rl_delivery_suburb] as Suburb
      ,[rl_delivery_postcode] as Postcode 
  INTO #Temp1
  FROM [CpplEDI].[dbo].[redeliverylog] r 
  --join [DWH].[dbo].[DimProduct] p on p.code=left(ltrim(rtrim([rl_tracked])),3)
   Where CONVERT(Date,[rl_delivery_date]) = @Date 
  --and ( len([rl_tracked])=11 and isnumeric([rl_tracked])=1) and p.Revenuetype='Prepaid' and p.[RevenueCategory]='Primary'
 
  
delete  #temp1 from  #temp1 t join [DWH].[dbo].[DimProduct] p on p.code=left(ltrim(rtrim(PrimaryCoupon)),3)
  where ( len(PrimaryCoupon )=11 and isnumeric(PrimaryCoupon )=1)  and p.[RevenueCategory]<>'Primary'

  

--Drop Table #Temp2
Select L.LabelNumber,E.Description,D.Code,D.BranchId,D.DepotId,T.EventDateTime , CONVERT (Varchar(20),'') as DeliveryBranch
      ,CONVERT (Varchar(20),'') as DeliveryDepot 
      into #TempScans
      from ScannerGateway.dbo.TrackingEvent T Join ScannerGateway.dbo.Label L on T.LabelId=L.id 
                                                 Join #Temp1 T1 on T1.PrimaryCoupon = L.LabelNumber
                                                 join ScannerGateway.dbo.EventType E on T.EventTypeId=E.id
                                                 Left Join ScannerGateway.dbo.Driver D on T.DriverId=D.ID
                                                 WHERE EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
UNION all
 Select L.LabelNumber,E.Description,D.Code,D.BranchId,D.DepotId,T.EventDateTime,CONVERT (Varchar(20),'') as DeliveryBranch
      ,CONVERT (Varchar(20),'') as DeliveryDepot from ScannerGateway.dbo.TrackingEvent T Join ScannerGateway.dbo.Label L on T.LabelId=L.id 
                                                 Join #Temp1 T1 on T1.PrimaryCoupon = L.LabelNumber
                                                 join ScannerGateway.dbo.EventType E on T.EventTypeId=E.id
                                                    Left Join ScannerGateway.dbo.Driver D on T.DriverId=D.ID
                                                 WHERE EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' 
                                                  and EventDateTime> T1.RedeliveryBookedTime                                             



Update #TempScans SET DeliveryBranch = B.CosmosBranch  From #TempScans S Join ScannerGateway.dbo.Branch B on S.BranchId=B.Id
Update #TempScans SET DeliveryDepot = B.Name  From #TempScans S Join ScannerGateway.dbo.Depot B on ltrim(rtrim(S.DepotId))=ltrim(rtrim(B.Id))

Update #Temp1 SET  DeliveryDriver =S.Code,DeliveryBranch = S.DeliveryBranch,DeliveryDepot=S.DeliveryDepot ,DeliveryTime = S.EventDateTime,DeliveryEvent = S.Description
From #Temp1 T join #TempScans S on T.PrimaryCoupon = S.LabelNumber

Update #Temp1 set DeliveryState=case when Deliverybranch='Adelaide' then 'SA'
when Deliverybranch='Brisbane' or Deliverybranch='Goldcoast' or Deliverybranch='Gold coast'  then 'QLD'
when Deliverybranch='Melbourne' then 'VIC'
when Deliverybranch='Perth' then 'WA'
when Deliverybranch='Sydney' then 'NSW'
else 'Unknown'
end

update #temp1 set Deliverystate=p.[state] from #temp1 t join [cpsqldwh01].[CouponCalculator].[dbo].[PostCodes] p on ltrim(rtrim(t.postcode))=ltrim(rtrim(p.postcode)) and ltrim(rtrim(t.suburb))=ltrim(rtrim(p.suburb))
where Deliverystate='Unknown'


Update #Temp1 SET Ontime = CaSE  WHEN DeliveryTime IS Null  Then 'N' WHEN  CONVERT(Date,DeliveryTime) > RedeliveryDate THEN 'N' ELSE 'Y' END

Select * from #Temp1

end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_RedeliveryOntimeReport]
	TO [ReportUser]
GO
