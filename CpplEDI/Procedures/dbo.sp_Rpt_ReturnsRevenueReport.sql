SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Rpt_ReturnsRevenueReport] @StartDate Date, @EndDate Date
AS
	BEGIN

       --SET FMTONLY OFF 
       SET NOCOUNT ON;


--DECLARE @StartDate DateTime
--DECLARE @EndDate DateTime

--set @StartDate='01-07-2020'
--Print @StartDate
--SET @StartDate=dateadd(dd, datediff(dd,0,@Date),0)- 1 + '15:00:00' 
--SEt @EndDate=(dateadd(dd, datediff(dd,0,@Date),0)) + '14:59:59' 

select 

TC.ConsignmentCode,
PB.AccountCode [Account Number],
TC.CreatedDateTime,
TC.RateCardID,
TC.CustomerRefNo,
TC.ConsignmentPreferPickupDate [Booking ready date],
TC.ConsignmentPreferPickupTime [Booking ready time],
 --ConsignmentPreferPickupTime  [Booking ready time],
PB.billingdate,
PB.BilledFreightCharge,
CN.cd_last_status,
CASE WHEN PB.ServiceCode in ('RTHP','RTDP') THEN  'Direct pickup'   WHEN PB.ServiceCode in ('RTRN') THEN 'Drop off' ELSE '' END AS Return_method
	

from pronto.[dbo].[ProntoBilling] (nolock) PB   Join CPPLEDI.dbo.Consignment (nolock) CN on  CN.cd_connote = PB.ConsignmentReference 
inner join [EzyFreight].[dbo].[tblConsignment] (nolock) TC on PB.ConsignmentReference = TC.ConsignmentCode 
										   join [EzyFreight].[dbo].tblStatus TS on TC.ConsignmentStatus = TS.StatusID
 
where  PB.ServiceCode in ('RTDP', 'RTHP', 'RTRN') and convert(date,TC.CreatedDateTime) between @StartDate and @EndDate 
END
GO
