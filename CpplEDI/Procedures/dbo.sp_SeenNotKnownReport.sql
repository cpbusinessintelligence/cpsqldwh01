SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[sp_SeenNotKnownReport] (@CouponPrefix Varchar(1000),@Startdate Datetime,@Enddate datetime)
as

    --'=====================================================================
    --' CP -Stored Procedure -[sp_SeenNotKnownReport] 'bgr','2019-05-25','2019-06-01'
    --' ---------------------------
    --' Purpose: Get all SNK-----
    --' Developer: Sinshith (Couriers Please Pty Ltd)
    --' Date: 13 Dec 2018
    --' Copyright: 
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --

    --'=====================================================================

Begin
If @CouponPrefix <> 'BGR'
Begin
--declare @CouponPrefix nVarchar(1000),@Startdate Datetime,@Enddate datetime
declare @param2 nvarchar(1000)
--set @CouponPrefix = '0009349968%'' or uc_coupon like ''0019349968%'' or uc_coupon like ''0089349968%'' or uc_coupon like ''0099349968%'' or uc_coupon like ''CPAP97Z'
--set @CouponPrefix = '0019349968'
select @param2 = ''''+ @CouponPrefix + '%'''
--print @param2
declare @print3 nvarchar(max)
set @print3 = '
SELECT [uc_coupon] as [Coupon]
      ,[uc_action] as [Status]
      ,[uc_stamp] as [DateTime]
      ,[uc_branch] as [Branch]
  FROM [CpplEDI].[dbo].[unknowncoupons]
  where uc_coupon like ' + @param2 +

  
  ' and [uc_stamp] >= '''+ convert(varchar(20),@Startdate,23)+ ''' and   [uc_stamp] <= ''' + convert(varchar(20),@Enddate,23)

  + ''' ORDER BY [uc_stamp] DESC'

    EXECUTE  sp_executesql @print3
	end


	--print @print3
	If @CouponPrefix = 'BGR'
	Begin
	declare @param3 varchar(100)
set @param2 = '%' + @CouponPrefix + '%'

SELECT [uc_coupon] as [Coupon]
      ,[uc_action] as [Status]
      ,[uc_stamp] as [DateTime]
      ,[uc_branch] as [Branch]
  FROM [CpplEDI].[dbo].[unknowncoupons]
  where uc_coupon like @param2
  and [uc_stamp] >= @Startdate and   [uc_stamp] <= @Enddate

  ORDER BY [uc_stamp] DESC

  End


End


GO
GRANT EXECUTE
	ON [dbo].[sp_SeenNotKnownReport]
	TO [ReportUser]
GO
