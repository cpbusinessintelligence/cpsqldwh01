SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[sp_SeenNotKnownReport_SS28052019] (@CouponPrefix Varchar(100),@Startdate Datetime,@Enddate datetime)
as

    --'=====================================================================
    --' CP -Stored Procedure -[sp_SeenNotKnownReport] '003404','2018-12-05','2018-12-11'
    --' ---------------------------
    --' Purpose: Get all SNK-----
    --' Developer: Sinshith (Couriers Please Pty Ltd)
    --' Date: 13 Dec 2018
    --' Copyright: 
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --

    --'=====================================================================

Begin
declare @param2 varchar(100)
set @param2 = '%' + @CouponPrefix + '%'

SELECT [uc_coupon] as [Coupon]
      ,[uc_action] as [Status]
      ,[uc_stamp] as [DateTime]
      ,[uc_branch] as [Branch]
  FROM [CpplEDI].[dbo].[unknowncoupons]
  where uc_coupon like @param2
  and [uc_stamp] >= @Startdate and   [uc_stamp] <= @Enddate

  ORDER BY [uc_stamp] DESC

End

GO
