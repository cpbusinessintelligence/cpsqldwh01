SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc dbo.sp_ccRewighLoad
As
Begin

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ccreweighhold') DROP TABLE [ccreweighhold];

CREATE TABLE [ccreweighhold] (

 [crh_coupon] char(32) NOT NULL DEFAULT '',

  [crh_coupon_id] int NOT NULL,

  [crh_stamp] datetime default NULL,

  [crh_checked] char(1) DEFAULT 'N',

  [crh_location] char(16) DEFAULT '',

  [crh_deadweight] FLOAT(25) DEFAULT '0',

  [crh_dimension0] FLOAT(25) DEFAULT '0',

  [crh_dimension1] FLOAT(25) DEFAULT '0',

  [crh_dimension2] FLOAT(25) DEFAULT '0',

  [crh_volume] FLOAT(25) DEFAULT '0',

  PRIMARY KEY ([crh_coupon]),

) ;



INSERT INTO ccreweighhold select * from openquery(MySQLMain,'select * from cpplEDI.ccreweighhold');INSERT INTO Temp_tablesload values('ccreweighhold',@@Error,(select * from openquery(MySQLMain,'select count(*) from cpplEDI.ccreweighhold')),(select count(*
) from ccreweighhold));

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ccreweigh_load') DROP TABLE [ccreweigh_load];
SELECT * INTO CpplEDI.dbo.ccreweigh_load
FROM openquery(MySQLMain, 'SELECT cd.* FROM cpplEDI.ccreweigh cd  where ccr_stamp>=NOW() - INTERVAL 2 MONTH ;')

MERGE ccreweigh AS cd
USING (select * from ccreweigh_load) AS cs ON cs.[ccr_coupon_id]=cd.[ccr_coupon_id]
when matched then UPDATE 
SET [ccr_coupon] = cs.ccr_coupon
,[ccr_coupon_id] = cs.ccr_coupon_id
,[ccr_stamp] = cs.ccr_stamp
,[ccr_location] = cs.ccr_location
,[ccr_deadweight] = cs.ccr_deadweight
,[ccr_dimension0] = cs.ccr_dimension0
,[ccr_dimension1] = cs.ccr_dimension1
,[ccr_dimension2] = cs.ccr_dimension2
,[ccr_volume] = cs.ccr_volume
when not matched then INSERT values([ccr_coupon]
           ,[ccr_coupon_id]
           ,[ccr_stamp]
           ,[ccr_location]
           ,[ccr_deadweight]
           ,[ccr_dimension0]
           ,[ccr_dimension1]
           ,[ccr_dimension2]
           ,[ccr_volume]);

		   End
GO
