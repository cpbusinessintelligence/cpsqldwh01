SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc sp_rptRedemption(@AccountCode varchar(2000), @StartDate Date, @EndDate Date) AS 

BEGIN

--sp_rptRedemption '112685656,113066070','2018-10-01','2018-10-06'
	

	SET NOCOUNT ON;

	--Declare @AccountCode Varchar(100)
	--set @AccountCode = '112685656'

SELECT * INTO #temp 

FROM [dbo].[Split](@AccountCode)



SELECT d.Accountcode into #temp3 FROM #temp t jOIN Pronto..ProntoDebtor d ON t.Item= d.Accountcode

--Declare @StartDate Datetime
--Declare @EndDate Datetime

--set @StartDate = '2018-10-11 00:00:00'
--set @EndDate = '2018-10-16 00:00:00'

select 
cd_connote,
cd_date,
cc_coupon,
cd_id,
cd_items,
cd_pricecode,
c_name,
cd_account,
cd_deliver_payment_actual,
cd_pickup_payment_actual,
Convert(Float,CASE 

         WHEN c.[cd_deadweight] > (c.[cd_volume] * 250 ) THEN  c.[cd_deadweight]

         ELSE c.[cd_volume] * 250

         END) AS ChargeableWeight,
Pickz.z_name as ConsPickupZone,
Deliz.z_name as ConsDeliverZone,

DPick.b_name as DriverPickupBranch,
DDeli.b_name as DriverDeliverBranch

into #Test 
from cpplEDI.dbo.consignment C
Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
inner join [companyaccount] Acc
On(C.cd_company_id = Acc.ca_company_id)
inner join [companies] Com
On(Acc.ca_company_id = Com.c_id)

 LEFT JOIN agentinfo agp ON agp.ai_id = c.cd_pickup_record_no
        LEFT JOIN zones Pickz ON Pickz.z_id = agp.zoneid

		 LEFT JOIN agentinfo agd ON agd.ai_id = c.cd_delivery_record_no
        LEFT JOIN zones Deliz ON Deliz.z_id = agd.zoneid


left join Branchs DPick
On(c.cd_pickup_driver_branch = DPick.B_id)
left join Branchs DDeli
On(c.cd_deliver_driver_branch = DDeli.B_id)

INNER JOIN #temp3 as f

  ON(c.cd_account = f.Accountcode)

--where 
--  cd_date between @StartDate and @EndDate  
  --and 
  
  --cd_connote  = 'CPAG35Z0101552'
order by cd_date desc

--drop table #Pickup
select c_name as CustomerName,
cd_connote as ConsignmentNumber,
cd_date as ConsignmentDate,
cd_account,
cd_pricecode,
cd_items as Items,
Min(Te.EventDatetime) PickupDate,
Et.Description as Status,
Dr.Code PickupDriverNum,
Dr.ProntoDriverCode as PickupProntoDrivercode,
ChargeableWeight,
cd_pickup_payment_actual,

ConsPickupZone,
ConsDeliverZone,


DriverPickupBranch,
DriverDeliverBranch
into #Pickup
from #Test Tes
join ScannerGateway..TrackingEvent
Te
on Tes.cc_coupon = Te.SourceReference
join ScannerGateway..Eventtype Et
On(Te.EventTypeId = Et.Id)
join ScannerGateway..[Driver] Dr
On(Te.DriverId = Dr.Id)
join [CpplEDI].[dbo].[cdaudit] Aud
On(Tes.cd_id = Aud.[ca_consignment])
where EventTypeId in ( '98EBB899-A15E-4826-8D05-516E744C466C')
 --and  cd_connote = 'CPAFXLC2319929'
 Group by
 c_name,
 cd_connote,
 cd_date,
 Et.Description,
 Dr.Code,
 Dr.ProntoDriverCode,
 Aud.ca_stamp,
 ca_account,
 cd_pickup_payment_actual,
 cd_account,
 cd_pricecode,

 ConsPickupZone,
ConsDeliverZone,

DriverPickupBranch,
DriverDeliverBranch,
ChargeableWeight,
cd_items


--Drop table #Delivery
 select 
 c_name as CustomerName,
 cd_connote as ConsignmentNumber,
 cd_date as ConsignmentDate,
 Min(Te.EventDatetime) DeliveredDate ,
 Dr.Code DeliveredDriverNum,
 Dr.ProntoDriverCode as DelProntoDrivercode,
 Et.Description as Status,
 cd_deliver_payment_actual
 into #Delivery
 from #Test Tes
join ScannerGateway..TrackingEvent
Te
on Tes.cc_coupon = Te.SourceReference
join ScannerGateway..Eventtype Et
On(Te.EventTypeId = Et.Id)
join ScannerGateway..[Driver] Dr
On(Te.DriverId = Dr.Id)
join [CpplEDI].[dbo].[cdaudit] Aud
On(Tes.cd_id = Aud.[ca_consignment])

where EventTypeId in ( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3')
 --and  cd_connote = 'CPAFXLC2319929'
 Group by
 c_name,
 cd_connote,
 cd_date,
 Et.Description,
 Dr.Code,
 Dr.ProntoDriverCode,
 Aud.ca_stamp,
 ca_account,
  cd_deliver_payment_actual

  --Drop table #final
select distinct 
Pick.CustomerName,
Pick.cd_account as AccountNumber,
Pick.cd_pricecode as Service,
Pick.ConsignmentNumber,
Pick.ConsignmentDate,
PickupDate,
Items,
ChargeableWeight,
PickupDriverNum,
DriverPickupBranch,
PickupProntoDrivercode,
 ConsPickupZone,
cd_pickup_payment_actual as PickupPayment,
ConsDeliverZone,
DeliveredDate,
DeliveredDriverNum,
DriverDeliverBranch,
DelProntoDrivercode,
cd_deliver_payment_actual as DeliverPayment

 into #Final
from 
#Pickup as Pick
Left join #Delivery as Del
on(Pick.ConsignmentNumber = Del.ConsignmentNumber)

--select * from #Final

--Drop table #FinalList

Select Fi.*,BillingDate into #FinalList from  CPsqLDWH01.pRONTO.[dbo].[ProntoBilling] Pb
inner join #Final Fi
On(Pb.ConsignmentReference = Fi.ConsignmentNumber)
where BillingDate between @StartDate and @EndDate

 
 Select * from #FinalList A
 
 INNER JOIN #temp3 as B

  ON(A.AccountNumber = B.Accountcode)



--drop table #Final

End
GO
GRANT EXECUTE
	ON [dbo].[sp_rptRedemption]
	TO [ReportUser]
GO
