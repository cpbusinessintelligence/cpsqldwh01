SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE z_SP_RptExtendedDIFOTReport_RSComponents  @AccountCode varchar(20),@StartDate date, @EndDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Select   cd_Connote ,
         cd_id,
         cc_coupon,
         cd_Account,
              cd_items, 
               cd_date ,
              convert(decimal(12,2),0) as KPI,
              convert(varchar(30),'') as DeliveryType,
              convert(varchar(30),'') as DeliverySubType,
              cd_pickup_suburb ,
              cd_pickup_postcode, 
         convert(varchar(20),'') as PickupZone, 
               cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
        convert(varchar(20),'') as DeliveryState,
         convert(varchar(20),'') as DeliveryZone,
              convert(varchar(20),'') as NetworkCategoryID,
              convert(varchar(20),'') as NetworkCategory,
              convert(varchar(20),'') as FromETA,
              convert(varchar(20),'') as ToETA,
                convert(varchar(20),'') as ETA,
            convert(Datetime,Null)  as ETADate,
              convert(varchar(20),'') as StatusID,
              convert(varchar(50),'') as StatusDescription,
              convert(Datetime,Null)  as PickupDate,
              convert(Datetime,Null)  as OutForDeliveryDate,
              convert(Datetime,Null)  as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard,
              convert(Datetime,Null)  as DeliveryDate
into #Temp1
from cpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
Where  cd_account = @AccountCode --'112814702' 
and cd_date >= @StartDate --'2017-04-01' 
and cd_date <= @EndDate  --'2017-04-30' 

Update #Temp1 SET AttemptedDeliveryCard = replace(additionaltext1,'Link Coupon ',''), AttemptedDeliveryDate = E.EventDateTime  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference Where
eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA' or additionaltext1 like 'Link Coupon 191%') 
 
Update #Temp1 Set DeliveryType =  'DEPOT Redelivery' Where (AttemptedDeliveryCard like '191%' OR AttemptedDeliveryCard like '%DPCNA') 
 Update #Temp1 Set DeliveryType =  'POPSHOP Redelivery' Where (AttemptedDeliveryCard like 'NHCL%' OR AttemptedDeliveryCard like '%RTCNA') 
 Update #Temp1 Set DeliveryType =  'POPSTATION Redelivery' Where  AttemptedDeliveryCard like '%SLCNA' 
 Update #Temp1 Set DeliverySubType = Case WHEN ISnull(SPInstruction,'') = '' THEN 'Signature' ELSE 'Authority To Leave' END     From #Temp1 T join EzYFreight.dbo.tblRedelivery R on T.cc_coupon= R.LableNumber Where IsProcessed =1
Update #Temp1 Set DeliverySubType = 'No Actions by Customer'  Where DeliveryType =  'DEPOT Redelivery' and DeliverySubType =''

 Update #Temp1 SET DeliveryType = 'Redirection', DeliverySubType  = R.SelectedDeliveryOption  From #Temp1 T join EzYFreight.dbo.tblRedirectedConsignment R on T.cd_connote= R.ConsignmentCode Where IsProcessed =1
Update #Temp1 SET DeliveryType = 'Normal Delivery' where DeliveryType = ''

Update #Temp1 SET PickupZone= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_pickup_postcode = P.PostCode and T.cd_pickup_suburb = P.Suburb
Update #Temp1 SET DeliveryZOne= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode and T.cd_delivery_suburb = P.Suburb
Update #Temp1 SET DeliveryState= P.State From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode
Update #Temp1 SET DeliveryZOne= P.ETAZone From #Temp1 T join PerformanceReporting.[dbo].[PostCodes] P on T.cd_delivery_postcode = P.PostCode where DeliveryZone = ''

Update #Temp1 SET FromETA  = E.FromETA ,
                  ToETA = E.ToETA,
                             ETA = E.ETA,
                             NetworkCategory = E.PrimaryNetworkCategory

   From #Temp1 T join  PerformanceReporting.[dbo].ETACalculator E on T.PickupZone  = E.FromZone and T.DeliveryZone = E.ToZone 


 --  Select * from #Temp1 where DeliveryZone = ''

Update #Temp1 SET PickupDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.PickupDate is null and E.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' 

Update #Temp1 SET OutForDeliveryDate =  E.EventDateTime  From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.OutForDeliveryDate is null and E.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' 
          
 Update #Temp1 SET AttemptedDeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.AttemptedDeliveryDate is null and E.EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' 

Update #Temp1 SET DeliveryDate =  E.EventDateTime  
                          From #Temp1 T join ScannerGateway.[dbo].[Label] L On T.cc_coupon = L.LabelNumber
                                                                                  join ScannerGateway.dbo.TrackingEvent E on L.ID =    E.LabelId
                                                              Where T.DeliveryDate is null and E.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'

Delete #Temp1 Where ToETa ='XXX'
Update #Temp1 SET ETADate  = PerformanceReporting.[dbo].[fn_CalculateDomesticWeekendsandPublicHolidays] (PickupDate,PickupZone,DeliveryZone,CASE WHEN DeliveryType =  'Redirection' and DeliverySubType <> 'Authority To Leave' THEN ToETA+1 Else toETA END) where PickupDate is not null 
Update #Temp1 SET StatusDescription =  'No Activity' WHere  PickupDate is null and OutForDeliveryDate is null and AttemptedDeliveryDate is null and DeliveryDate is null 
Update #Temp1 Set StatusDescription = 'Cant Calculate' where PickupDate is null  or ETA is null
Update #Temp1 SET StatusDescription =  CASE  WHEN (AttemptedDeliveryDate is null and DeliveryDate is null and  DateDiff(day, Getdate(),ETADate) >=0) THEN 'In Progress' ELSE 'Not On Time' ENd WHere  StatusDescription = '' and AttemptedDeliveryDate is null and DeliveryDate is null 
Update #Temp1 Set StatusDescription = CASE when DateDiff(day, Isnull(AttemptedDeliveryDate,DeliveryDate),ETADate)>=0 Then 'On Time' ELse 'Not On Time' END where  StatusDescription = '' 
Update #Temp1 SET KPI = Case NetworkCategory WHEN 'Metro' Then 95.00 ELSE 90.00 end
Select * from #Temp1

END
GO
GRANT EXECUTE
	ON [dbo].[z_SP_RptExtendedDIFOTReport_RSComponents]
	TO [ReportUser]
GO
