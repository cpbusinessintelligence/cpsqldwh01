SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].['Query Results$'] (
		[tracking_id]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[pickup_date]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[name]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[company]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[address_line1]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[address_line2]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[suburb]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[state_code]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[postcode]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].['Query Results$'] SET (LOCK_ESCALATION = TABLE)
GO
