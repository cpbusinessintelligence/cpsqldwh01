SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountToConsignmentRange] (
		[Sno]                       [int] IDENTITY(1, 1) NOT NULL,
		[Accountname]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentStartRange]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Createdby]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Createddatetime]           [datetime] NULL,
		[Editedby]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]            [datetime] NULL
)
GO
ALTER TABLE [dbo].[AccountToConsignmentRange]
	ADD
	CONSTRAINT [DF__AccountTo__Creat__6DB26575]
	DEFAULT ('Admin') FOR [Createdby]
GO
ALTER TABLE [dbo].[AccountToConsignmentRange]
	ADD
	CONSTRAINT [DF__AccountTo__Creat__6EA689AE]
	DEFAULT (getdate()) FOR [Createddatetime]
GO
ALTER TABLE [dbo].[AccountToConsignmentRange]
	ADD
	CONSTRAINT [DF__AccountTo__Edite__6F9AADE7]
	DEFAULT ('Admin') FOR [Editedby]
GO
ALTER TABLE [dbo].[AccountToConsignmentRange]
	ADD
	CONSTRAINT [DF__AccountTo__Edite__708ED220]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[AccountToConsignmentRange] SET (LOCK_ESCALATION = TABLE)
GO
