SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Agents List for Exile Soft Integration] (
		[AgentName]     [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Agents List for Exile Soft Integration] SET (LOCK_ESCALATION = TABLE)
GO
