SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AmwayTransitTime] (
		[Postcode]         [float] NULL,
		[Suburb]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[State]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Carrier]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Transit Time]     [float] NULL
)
GO
ALTER TABLE [dbo].[AmwayTransitTime] SET (LOCK_ESCALATION = TABLE)
GO
