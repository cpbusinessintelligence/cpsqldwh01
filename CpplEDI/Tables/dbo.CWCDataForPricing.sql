SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CWCDataForPricing] (
		[DwsTimeStamp]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MachineLocation]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Machineid]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Barcodes]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CubeLength]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CubeHeight]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Weight]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MeasuredWeight]      [float] NULL,
		[MeasuredVolume]      [float] NULL
)
GO
ALTER TABLE [dbo].[CWCDataForPricing] SET (LOCK_ESCALATION = TABLE)
GO
