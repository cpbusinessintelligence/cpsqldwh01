SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CWC_AgentOverRide] (
		[CWCAgentID]                        [int] NOT NULL,
		[AgentShortName]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AgentLongName]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[GatewayCode]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TrackingBranch]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AgentState]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RechargeCategory]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SpecialDriverRechargeCategory]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RevenueCategory]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AgentStyle]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NSWCWCFlag]                        [bit] NULL,
		[VICCWCFlag]                        [bit] NULL,
		[QLDCWCFlag]                        [bit] NULL,
		[SACWCFlag]                         [bit] NULL,
		[AgentFuelLevy]                     [float] NULL,
		[Add_LH_Flag]                       [bit] NULL,
		[AddWho]                            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddWhen]                           [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[CWC_AgentOverRide] SET (LOCK_ESCALATION = TABLE)
GO
