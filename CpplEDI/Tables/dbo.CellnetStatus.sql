SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CellnetStatus] (
		[Status]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionInformation]     [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Code]                     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Description]              [varchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CellnetStatus] SET (LOCK_ESCALATION = TABLE)
GO
