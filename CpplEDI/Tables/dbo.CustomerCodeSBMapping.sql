SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerCodeSBMapping] (
		[SBCustomerCode]        [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[GatewayUniqueCode]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[GatewayCompanyId]      [int] NULL,
		CONSTRAINT [PK__Customer__453846D1C6A471CF]
		PRIMARY KEY
		CLUSTERED
		([SBCustomerCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[CustomerCodeSBMapping] SET (LOCK_ESCALATION = TABLE)
GO
