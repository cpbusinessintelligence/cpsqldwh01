SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerStatusUpdateStaging_Seko] (
		[sno]                  [int] IDENTITY(1, 1) NOT NULL,
		[Consignment]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActualStatus]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]       [datetime] NULL,
		[MappedStatusCode]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[PODName]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]      [varchar](2000) COLLATE Latin1_General_CI_AS NULL,
		[Reference]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isProcessed]          [bit] NULL,
		[createdDate]          [datetime] NULL,
		[UpdatedDate]          [datetime] NULL
)
GO
ALTER TABLE [dbo].[CustomerStatusUpdateStaging_Seko]
	ADD
	CONSTRAINT [DF__CustomerS__isPro__35355C4D]
	DEFAULT ((0)) FOR [isProcessed]
GO
ALTER TABLE [dbo].[CustomerStatusUpdateStaging_Seko]
	ADD
	CONSTRAINT [DF__CustomerS__creat__36298086]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[CustomerStatusUpdateStaging_Seko]
	ADD
	CONSTRAINT [DF__CustomerS__Updat__371DA4BF]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[CustomerStatusUpdateStaging_Seko] SET (LOCK_ESCALATION = TABLE)
GO
