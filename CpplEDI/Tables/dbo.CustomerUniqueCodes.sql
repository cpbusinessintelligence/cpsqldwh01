SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerUniqueCodes] (
		[UniqueCustomerCode]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerName]           [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__Customer__DE71736143FC9E03]
		PRIMARY KEY
		CLUSTERED
		([UniqueCustomerCode], [CustomerName])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[CustomerUniqueCodes] SET (LOCK_ESCALATION = TABLE)
GO
