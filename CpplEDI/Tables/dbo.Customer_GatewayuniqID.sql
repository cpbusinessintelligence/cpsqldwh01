SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer_GatewayuniqID] (
		[Company]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Prefix]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Customer_GatewayuniqID] SET (LOCK_ESCALATION = TABLE)
GO
