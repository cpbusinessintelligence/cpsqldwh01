SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DIFOT_InternalStaffDetails] (
		[ConsignmentNumber]              [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountCode]                    [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]                    [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[NoofItems]                      [int] NULL,
		[ConsignmentDate]                [date] NULL,
		[PickupSuburb]                   [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostcode]                 [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[PickupZone]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupState]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Address1]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Address2]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Address3]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostCode]               [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategory]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkID]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FromETA]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToETA]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETA]                            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                        [date] NULL,
		[StatusID]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Status]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Category]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                    [char](32) COLLATE Latin1_General_CI_AS NULL,
		[FirstActivityDatetime]          [datetime] NULL,
		[FirstScanType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                     [datetime] NULL,
		[OutForDeliveryDate]             [datetime] NULL,
		[AttemptedDeliveryDate]          [datetime] NULL,
		[AttemptedDeliveryCard]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]                   [datetime] NULL,
		[PickupScannedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryScannedBy]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ConsolidateScannedAt]           [datetime] NULL,
		[WeekEndingDate]                 [datetime] NULL,
		[InterstateConnectivity]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]                    [datetime] NULL
)
GO
ALTER TABLE [dbo].[DIFOT_InternalStaffDetails] SET (LOCK_ESCALATION = TABLE)
GO
