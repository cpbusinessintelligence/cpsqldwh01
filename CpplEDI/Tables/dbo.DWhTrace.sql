SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DWhTrace] (
		[RowNumber]        [int] IDENTITY(0, 1) NOT NULL,
		[EventClass]       [int] NULL,
		[TextData]         [ntext] COLLATE Latin1_General_CI_AS NULL,
		[Duration]         [bigint] NULL,
		[SPID]             [int] NULL,
		[DatabaseID]       [int] NULL,
		[DatabaseName]     [nvarchar](128) COLLATE Latin1_General_CI_AS NULL,
		[ObjectType]       [int] NULL,
		[LoginName]        [nvarchar](128) COLLATE Latin1_General_CI_AS NULL,
		[BinaryData]       [image] NULL,
		CONSTRAINT [PK__DWhTrace__AAAC09D8F0BA1651]
		PRIMARY KEY
		CLUSTERED
		([RowNumber])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DWhTrace] SET (LOCK_ESCALATION = TABLE)
GO
