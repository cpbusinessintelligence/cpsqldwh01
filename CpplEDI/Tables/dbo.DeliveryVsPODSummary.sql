SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryVsPODSummary] (
		[ReportDate]           [date] NULL,
		[CustomerETADate]      [date] NULL,
		[WorkingDayDiff]       [int] NULL,
		[AgentState]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AgentName]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentCount]     [int] NULL,
		[RecievedCount]        [int] NULL,
		[DeliveredCount]       [int] NULL,
		[PODCount]             [int] NULL,
		[PercenttoData]        [decimal](12, 4) NULL,
		[PercenttoRecd]        [decimal](12, 4) NULL,
		[PercenttoPOD]         [decimal](12, 4) NULL
)
GO
ALTER TABLE [dbo].[DeliveryVsPODSummary] SET (LOCK_ESCALATION = TABLE)
GO
