SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExtendedDIFOTRSComponents] (
		[Consignment]               [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[Account]                   [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[NoofItems]                 [int] NULL,
		[ConsignmentDate]           [date] NULL,
		[PickupSuburb]              [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostcode]            [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[Category]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SubCategory]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Exceptions]                [varchar](120) COLLATE Latin1_General_CI_AS NULL,
		[PickupZone]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Address1]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Address2]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]            [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostCode]          [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategory]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETA]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                   [date] NULL,
		[Status]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                [datetime] NULL,
		[OutForDeliveryDate]        [datetime] NULL,
		[AttemptedDeliveryDate]     [datetime] NULL,
		[AttemptedDeliveryCard]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]              [datetime] NULL,
		[Total]                     [int] NULL
)
GO
ALTER TABLE [dbo].[ExtendedDIFOTRSComponents] SET (LOCK_ESCALATION = TABLE)
GO
