SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExtendedDIFOT_RSCo] (
		[MonthKey]                  [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[StatusDescription]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ACT]                       [decimal](12, 2) NULL,
		[NSW]                       [decimal](12, 2) NULL,
		[QLD]                       [decimal](12, 2) NULL,
		[VIC]                       [decimal](12, 2) NULL,
		[WA]                        [decimal](12, 2) NULL,
		[SA]                        [decimal](12, 2) NULL,
		[NT]                        [decimal](12, 2) NULL,
		[TotalByCustomer]           [decimal](12, 2) NULL,
		[TotalOnTimeByCustomer]     [decimal](12, 2) NULL,
		[PercentageByCustomer]      [decimal](12, 2) NULL
)
GO
ALTER TABLE [dbo].[ExtendedDIFOT_RSCo] SET (LOCK_ESCALATION = TABLE)
GO
