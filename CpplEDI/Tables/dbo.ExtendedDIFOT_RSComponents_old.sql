SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExtendedDIFOT_RSComponents_old] (
		[monthkey]             [int] NULL,
		[MonthName]            [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[account]              [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]        [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[KPI]                  [numeric](3, 1) NOT NULL,
		[performancecount]     [int] NULL,
		[total]                [int] NULL,
		[performance]          [decimal](12, 2) NULL
)
GO
ALTER TABLE [dbo].[ExtendedDIFOT_RSComponents_old] SET (LOCK_ESCALATION = TABLE)
GO
