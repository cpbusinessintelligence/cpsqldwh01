SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FailedDeliveries_Trev] (
		[Date]                     [datetime] NULL,
		[Time]                     [datetime] NULL,
		[Description]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Credit]                   [money] NULL,
		[Debit]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Account]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store Name]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store DLB]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store Suburb]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store State]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store Postcode]           [float] NULL,
		[Store Type]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment Number]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender Suburb]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender State]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender Postcode]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Destination Suburb]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Destination State]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Destination Postcode]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Destination Country]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Total Weight (kg)]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FailedDeliveries_Trev] SET (LOCK_ESCALATION = TABLE)
GO
