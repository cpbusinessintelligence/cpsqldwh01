SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gatewayuniquecode] (
		[Company_Name]     [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[Accounts]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Unique_code]      [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Company_Code]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Gatewayuniquecode] SET (LOCK_ESCALATION = TABLE)
GO
