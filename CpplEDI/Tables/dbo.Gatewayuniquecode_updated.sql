SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gatewayuniquecode_updated] (
		[Unique_Code]      [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[company_Code]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Company_name]     [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[Account]          [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Gatewayuniquecode_updated] SET (LOCK_ESCALATION = TABLE)
GO
