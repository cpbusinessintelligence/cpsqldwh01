SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_TestQuery] (
		[Consignment reference]     [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment date]          [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[JP_TestQuery] SET (LOCK_ESCALATION = TABLE)
GO
