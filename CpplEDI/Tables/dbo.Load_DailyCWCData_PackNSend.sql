SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_DailyCWCData_PackNSend] (
		[ConsignmentDate]              [date] NULL,
		[ConsignmentNumber]            [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[Label]                        [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[CustomerName]                 [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                  [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredServiceCode]          [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredWeight]               [float] NULL,
		[MeasuredWeightLabel]          [decimal](7, 2) NULL,
		[DeclaredCubicWeight]          [decimal](7, 2) NULL,
		[MeasuredCubicWeightLabel]     [decimal](7, 2) NULL,
		[ItemQuantity]                 [int] NULL,
		[MeasuredTime]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Length]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Width]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Height]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProcessedDate]                [datetime] NULL,
		[IsSent]                       [int] NULL
)
GO
CREATE NONCLUSTERED INDEX [idx_Load_DailyCWCData_PackNSend_ConsignmentNumber]
	ON [dbo].[Load_DailyCWCData_PackNSend] ([ConsignmentNumber])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Load_DailyCWCData_PackNSend] SET (LOCK_ESCALATION = TABLE)
GO
