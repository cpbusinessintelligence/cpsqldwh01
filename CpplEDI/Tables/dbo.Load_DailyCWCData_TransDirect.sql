SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_DailyCWCData_TransDirect] (
		[ConsignmentDate]              [date] NULL,
		[ConsignmentNumber]            [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[Label]                        [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[CustomerName]                 [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredServiceCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredWeight]               [float] NULL,
		[MeasuredWeightLabel]          [decimal](7, 2) NULL,
		[DeclaredCubicWeight]          [decimal](7, 2) NULL,
		[MeasuredCubicWeightLabel]     [decimal](7, 2) NULL,
		[ItemQuantity]                 [int] NULL,
		[MeasuredTime]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Length]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Width]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Height]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProcessedDate]                [datetime] NULL,
		[IsSent]                       [int] NULL
)
GO
ALTER TABLE [dbo].[Load_DailyCWCData_TransDirect] SET (LOCK_ESCALATION = TABLE)
GO
