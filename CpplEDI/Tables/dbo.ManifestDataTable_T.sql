SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManifestDataTable_T] (
		[Record type]                     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Consignment reference]           [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[Consingnment date]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Manifest reference]              [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Manifest date]                   [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Service]                         [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[Account Code]                    [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[Sender Name]                     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[Sender address1]                 [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Sender address2]                 [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Sender locality]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Sender State]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Sender Postcode]                 [int] NULL,
		[Receiver name]                   [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address 1]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address  2]             [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Receiver locality]               [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Receiver state]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Receiver postcode]               [int] NULL,
		[Customer reference]              [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Release ASN]                     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Return Authorisation Number]     [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Customer other reference 1]      [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Customer other reference 2]      [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Customer other reference 3]      [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Customer other reference 4]      [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Special instructions]            [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[item quantity]                   [int] NULL,
		[Declared weight]                 [float] NULL,
		[Measured Weight]                 [float] NOT NULL,
		[Declared volume]                 [float] NULL,
		[Measured volume]                 [float] NOT NULL,
		[Price override]                  [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Insurance category]              [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Declared value]                  [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Insurance Price Override]        [float] NULL,
		[Test Flag]                       [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Dangerous goods flag]            [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Release Not Before]              [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Release Not After]               [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Logistics Units]                 [int] NULL
)
GO
ALTER TABLE [dbo].[ManifestDataTable_T] SET (LOCK_ESCALATION = TABLE)
GO
