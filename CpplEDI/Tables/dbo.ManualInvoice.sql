SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManualInvoice] (
		[LabelNumber]     [nvarchar](32) COLLATE Latin1_General_CI_AS NULL,
		[Company]         [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Status]          [nvarchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ManualInvoice] SET (LOCK_ESCALATION = TABLE)
GO
