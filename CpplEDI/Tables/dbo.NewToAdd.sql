SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewToAdd] (
		[AccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[NewToAdd] SET (LOCK_ESCALATION = TABLE)
GO
