SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NowGoConsignemnt] (
		[labelnumber]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[datescanned]           [date] NULL,
		[category]              [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[NowGoConsignemnt] SET (LOCK_ESCALATION = TABLE)
GO
