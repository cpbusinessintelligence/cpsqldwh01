SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpsAccountsforReporting] (
		[AccountCode]       [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddedBy]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddedDateTime]     [datetime] NULL,
		[AccountName]       [varchar](250) COLLATE Latin1_General_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [idx_pAccountCode]
	ON [dbo].[OpsAccountsforReporting] ([AccountCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpsAccountsforReporting] SET (LOCK_ESCALATION = TABLE)
GO
