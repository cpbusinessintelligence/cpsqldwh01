SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PDG_RelationshipDefinitions] (
		[Id]                   [uniqueidentifier] NOT NULL,
		[PK_Table]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FK_Table]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PK_Id]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FK_Id]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RelationshipName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__PDG_Rela__3214EC075F85B894]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[PDG_RelationshipDefinitions] SET (LOCK_ESCALATION = TABLE)
GO
