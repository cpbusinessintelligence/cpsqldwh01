SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParameterMissedpickup] (
		[ID]                [int] IDENTITY(1, 1) NOT NULL,
		[AccountNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsAPI]             [bit] NULL,
		[CreatedDate]       [datetime] NULL,
		[CreatedBy]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDate]       [datetime] NULL,
		[UpdatedBy]         [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ParameterMissedpickup]
	ADD
	CONSTRAINT [DF__Parameter__Creat__68792F7F]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ParameterMissedpickup]
	ADD
	CONSTRAINT [DF__Parameter__Updat__696D53B8]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[ParameterMissedpickup] SET (LOCK_ESCALATION = TABLE)
GO
