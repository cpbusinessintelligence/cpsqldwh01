SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrepaidRevenueProtectionDeleted] (
		[RevenueRecognisedDate]           [date] NULL,
		[Labelnumber]                     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Linklabelnumber]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Labelprefix]                     [char](3) COLLATE Latin1_General_CI_AS NULL,
		[CouponType]                      [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[RevenueType]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BU]                              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BUState]                         [varchar](7) COLLATE Latin1_General_CI_AS NOT NULL,
		[Accountcode]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Accountname]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupDriverNumber]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupDriverBranch]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriverNumber]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriverBranch]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupEventDatetime]             [datetime] NULL,
		[PickupReportingDriver]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupReportingDriverBranch]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryEventDatetime]           [datetime] NULL,
		[PickupPostcode]                  [int] NULL,
		[Pickupsuburb]                    [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPostcode]             [int] NULL,
		[Destinationsuburb]               [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LinkCoupon]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LinkRevenueCategory]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LinkCategory]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LinkDescription]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentId]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Itemcount]                       [int] NULL,
		[cd_dead_weight]                  [float] NULL,
		[LinksCount]                      [int] NULL,
		[LinksRequired]                   [int] NULL,
		[Difference]                      [int] NULL,
		[RevenueDifference]               [float] NULL,
		[RevenueAmount]                   [numeric](13, 2) NULL,
		[Date]                            [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[PrepaidRevenueProtectionDeleted] SET (LOCK_ESCALATION = TABLE)
GO
