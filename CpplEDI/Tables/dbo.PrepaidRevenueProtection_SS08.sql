SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrepaidRevenueProtection_SS08] (
		[RevenueRecognisedDate]           [date] NULL,
		[Labelnumber]                     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CouponType]                      [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[BU]                              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BUState]                         [varchar](7) COLLATE Latin1_General_CI_AS NOT NULL,
		[Accountcode]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Accountname]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupDriverNumber]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupDriverBranch]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupReportingDriver]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupReportingDriverBranch]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriverNumber]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriverBranch]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[cd_dead_weight]                  [float] NULL,
		[FromZone]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LinksCount]                      [int] NULL,
		[LinksRequired]                   [int] NULL,
		[Difference]                      [int] NULL,
		[RevenueDifference]               [float] NULL,
		[Createdby]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Createddatetime]                 [datetime] NULL,
		[Editedby]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Editeddatetime]                  [datetime] NULL
)
GO
ALTER TABLE [dbo].[PrepaidRevenueProtection_SS08] SET (LOCK_ESCALATION = TABLE)
GO
