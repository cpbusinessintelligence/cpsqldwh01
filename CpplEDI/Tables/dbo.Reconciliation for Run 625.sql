SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Reconciliation for Run 625] (
		[Coupon]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Date]       [datetime2](7) NOT NULL,
		[Time]       [datetime2](7) NOT NULL,
		[Action]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Reconciliation for Run 625] SET (LOCK_ESCALATION = TABLE)
GO
