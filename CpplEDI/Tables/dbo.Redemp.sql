SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemp] (
		[AccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Redemp] SET (LOCK_ESCALATION = TABLE)
GO
