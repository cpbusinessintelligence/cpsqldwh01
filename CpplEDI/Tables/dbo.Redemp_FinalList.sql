SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemp_FinalList] (
		[Service]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cg_present]               [int] NULL,
		[cg_baseprice]             [int] NULL,
		[cg_kgincluded]            [int] NULL,
		[cg_kgprice]               [int] NULL,
		[cg_pickup_payamount]      [int] NULL,
		[cg_pickup_paybreak]       [int] NULL,
		[cg_pickup_kgpre]          [int] NULL,
		[cg_pickup_kgpost]         [int] NULL,
		[cg_deliver_payamount]     [int] NULL,
		[cg_deliver_paybreak]      [int] NULL,
		[cg_deliver_kgpre]         [int] NULL,
		[cg_deliver_kgpost]        [int] NULL,
		[FromState]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToState]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SPECIALREDEMPTION]        [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Redemp_FinalList] SET (LOCK_ESCALATION = TABLE)
GO
