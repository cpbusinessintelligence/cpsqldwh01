SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemp_Master] (
		[CODE]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WEIGHT ITEM]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DESCRIPTION]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[STATE]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[STD PUP]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SPEC_PUP]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DEL]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Redemp_Master] SET (LOCK_ESCALATION = TABLE)
GO
