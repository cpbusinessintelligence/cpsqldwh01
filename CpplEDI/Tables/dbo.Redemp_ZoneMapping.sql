SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemp_ZoneMapping] (
		[Zone]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Priority]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CP Zone ]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WorksheetZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Redemp_ZoneMapping] SET (LOCK_ESCALATION = TABLE)
GO
