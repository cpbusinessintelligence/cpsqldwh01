SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RedemptionReport] (
		[AccountNumber]         [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]     [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmnetDate]       [smalldatetime] NULL,
		[FromZone]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueOriginZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupContractor]      [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]            [date] NULL,
		[PickupRctiDate]        [date] NULL,
		[PickupRctiAmount]      [money] NULL
)
GO
ALTER TABLE [dbo].[RedemptionReport] SET (LOCK_ESCALATION = TABLE)
GO
