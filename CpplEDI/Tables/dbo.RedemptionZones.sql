SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RedemptionZones] (
		[Service]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Valid/ Not Valid]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[On Hub List]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RedemptionZones] SET (LOCK_ESCALATION = TABLE)
GO
