SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReturnCons_Jp_BUP_20200604] (
		[ConsignmentCode]     [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[CustomerRefNo]       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[RateCardID]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime2](3) NOT NULL
)
GO
ALTER TABLE [dbo].[ReturnCons_Jp_BUP_20200604] SET (LOCK_ESCALATION = TABLE)
GO
