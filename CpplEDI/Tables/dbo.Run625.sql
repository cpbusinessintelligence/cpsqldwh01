SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Run625] (
		[Coupon]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Time]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Action]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Run625] SET (LOCK_ESCALATION = TABLE)
GO
