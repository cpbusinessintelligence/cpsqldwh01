SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student] (
		[id]             [int] IDENTITY(1, 1) NOT NULL,
		[fn]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ln]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[course]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TeachersId]     [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Student]
	WITH CHECK
	ADD CONSTRAINT [FK__Student__Teacher__281718E9]
	FOREIGN KEY ([TeachersId]) REFERENCES [dbo].[Teacher] ([id])
ALTER TABLE [dbo].[Student]
	CHECK CONSTRAINT [FK__Student__Teacher__281718E9]

GO
ALTER TABLE [dbo].[Student] SET (LOCK_ESCALATION = TABLE)
GO
