SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T2] (
		[cd_id]                    [int] NOT NULL,
		[cd_account]               [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_connote]               [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[cd_date]                  [smalldatetime] NULL,
		[cd_customer_eta]          [smalldatetime] NULL,
		[cd_pickup_addr0]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_suburb]         [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_postcode]       [int] NULL,
		[cd_pickup_state]          [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_suburb]       [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_postcode]     [int] NULL,
		[cd_deliver_State]         [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[cd_items]                 [int] NULL,
		[cc_coupon]                [char](32) COLLATE Latin1_General_CI_AS NULL,
		[Id]                       [uniqueidentifier] NULL,
		[EventType]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EventDatetime]            [datetime] NULL,
		[Driver]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ProntoID]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[T2] SET (LOCK_ESCALATION = TABLE)
GO
