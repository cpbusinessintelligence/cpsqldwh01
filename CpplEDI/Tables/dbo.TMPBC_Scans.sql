SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TMPBC_Scans] (
		[Barcode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Company]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Recoverable]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Depot]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Charge Rate]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[TMPBC_Scans] SET (LOCK_ESCALATION = TABLE)
GO
