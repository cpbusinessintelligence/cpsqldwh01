SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TST] (
		[Trans  Type]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Cons  Type]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Weeks]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupon No ]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Account]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[comments]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[condate]         [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[TST] SET (LOCK_ESCALATION = TABLE)
GO
