SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Teacher_Reloaded] (
		[id]     [int] NOT NULL,
		[fn]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ln]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Teacher_Reloaded] SET (LOCK_ESCALATION = TABLE)
GO
