SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp] (
		[Label]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Connote]            [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[Driver]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Branch]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanType]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanDateTime]       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostcode]     [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[PickupSuburb]       [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[At1]                [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Temp] SET (LOCK_ESCALATION = TABLE)
GO
