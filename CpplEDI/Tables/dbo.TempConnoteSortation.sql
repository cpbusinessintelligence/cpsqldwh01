SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempConnoteSortation] (
		[cd_connote]     [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[TempConnoteSortation] SET (LOCK_ESCALATION = TABLE)
GO
