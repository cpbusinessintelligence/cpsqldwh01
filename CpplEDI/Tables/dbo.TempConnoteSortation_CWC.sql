SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempConnoteSortation_CWC] (
		[cd_connote]         [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[Measuredweight]     [float] NULL,
		[measuredvolume]     [float] NULL
)
GO
ALTER TABLE [dbo].[TempConnoteSortation_CWC] SET (LOCK_ESCALATION = TABLE)
GO
