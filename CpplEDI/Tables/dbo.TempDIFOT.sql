SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempDIFOT] (
		[Consignment]                    [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Items]                          [int] NULL,
		[cd_date]                        [datetime] NULL,
		[cd_pickup_suburb]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_postcode]             [int] NULL,
		[Category]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SubCategory]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Exceptions]                     [varchar](220) COLLATE Latin1_General_CI_AS NULL,
		[PickupZone]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr0]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr1]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr2]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_suburb]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_postcode]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategory]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FromETA]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToETA]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETA]                            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                        [datetime] NULL,
		[StatusID]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[StatusDescription]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cc_coupon]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                     [datetime] NULL,
		[OutForDeliveryDate]             [datetime] NULL,
		[AttemptedDeliveryDate]          [datetime] NULL,
		[AttemptedDeliveryCard]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]                   [datetime] NULL,
		[Total]                          [int] NULL,
		[PickupScannedBy]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryScannedBy]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryScannedBy]              [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[TempDIFOT] SET (LOCK_ESCALATION = TABLE)
GO
