SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempScanners] (
		[ScannerNumber]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Name]              [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
CREATE CLUSTERED INDEX [TempScannersidx]
	ON [dbo].[TempScanners] ([ScannerNumber])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[TempScanners] SET (LOCK_ESCALATION = TABLE)
GO
