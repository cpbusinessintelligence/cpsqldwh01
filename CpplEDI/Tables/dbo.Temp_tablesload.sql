SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_tablesload] (
		[Tablename]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[error]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MySQLCount]     [int] NULL,
		[SQLCount]       [int] NULL
)
GO
ALTER TABLE [dbo].[Temp_tablesload] SET (LOCK_ESCALATION = TABLE)
GO
