SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_tablesload_Test] (
		[Tablename]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[error]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MySQLCount]          [int] NULL,
		[SQLCount]            [int] NULL,
		[CreatedDatetime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[Temp_tablesload_Test]
	ADD
	CONSTRAINT [DF_Createddatetime]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[Temp_tablesload_Test] SET (LOCK_ESCALATION = TABLE)
GO
