SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_tablesload_tes] (
		[Tablename]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[error]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MySQLCount]     [int] NULL,
		[SQLCount]       [int] NULL,
		[LogDate]        [date] NULL
)
GO
ALTER TABLE [dbo].[Temp_tablesload_tes]
	ADD
	CONSTRAINT [DF__Temp_tabl__LogDa__01FFA734]
	DEFAULT (getdate()) FOR [LogDate]
GO
ALTER TABLE [dbo].[Temp_tablesload_tes] SET (LOCK_ESCALATION = TABLE)
GO
