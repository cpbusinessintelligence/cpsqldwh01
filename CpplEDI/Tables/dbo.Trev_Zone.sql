SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Trev_Zone] (
		[Consignment ref]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[StatusDes]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment date]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MMM-YY]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Manifest ref]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Manisfest Date]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Service]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Acct code]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Territory]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Branch]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Acct name]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender Add 1]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender Add 2]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender Locality]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP Zone]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender State]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender PC]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Rec'vr Name]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Rec'vr Add 1]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Rec'vr Add 2]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Rec'vr Locality]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Rec'vr State]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Rec'vr PC]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Cust ref]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Quantity]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredWeight]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MeasuredWeight]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredVolume]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MeasuredVolume]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Trev_Zone] SET (LOCK_ESCALATION = TABLE)
GO
