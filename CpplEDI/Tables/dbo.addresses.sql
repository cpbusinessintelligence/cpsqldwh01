SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[addresses] (
		[a_id]             [int] NOT NULL,
		[a_company_id]     [int] NULL,
		[a_user]           [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_pickup]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_delivery]       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_code]           [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr0]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr1]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr2]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr3]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_suburb]         [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[a_postcode]       [int] NULL,
		[a_contact]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_phone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[a_email]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__addresse__566AFA9AA0E00B25]
		PRIMARY KEY
		CLUSTERED
		([a_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_com__73477389]
	DEFAULT ('0') FOR [a_company_id]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_use__743B97C2]
	DEFAULT ('') FOR [a_user]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_pic__752FBBFB]
	DEFAULT ('N') FOR [a_pickup]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_del__7623E034]
	DEFAULT ('N') FOR [a_delivery]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_cod__7718046D]
	DEFAULT (NULL) FOR [a_code]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_add__780C28A6]
	DEFAULT ('') FOR [a_addr0]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_add__79004CDF]
	DEFAULT ('') FOR [a_addr1]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_add__79F47118]
	DEFAULT ('') FOR [a_addr2]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_add__7AE89551]
	DEFAULT ('') FOR [a_addr3]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_sub__7BDCB98A]
	DEFAULT ('') FOR [a_suburb]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_pos__7CD0DDC3]
	DEFAULT ('0') FOR [a_postcode]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_con__7DC501FC]
	DEFAULT ('') FOR [a_contact]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_pho__7EB92635]
	DEFAULT ('') FOR [a_phone]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__a_ema__7FAD4A6E]
	DEFAULT ('') FOR [a_email]
GO
ALTER TABLE [dbo].[addresses]
	WITH NOCHECK
	ADD CONSTRAINT [FK_addresses_company]
	FOREIGN KEY ([a_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[addresses]
	NOCHECK CONSTRAINT [FK_addresses_company]

GO
ALTER TABLE [dbo].[addresses] SET (LOCK_ESCALATION = TABLE)
GO
