SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[addresses_Archive2017] (
		[a_id]             [int] NULL,
		[a_company_id]     [int] NULL,
		[a_user]           [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_pickup]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_delivery]       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_code]           [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr0]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr1]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr2]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_addr3]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_suburb]         [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[a_postcode]       [int] NULL,
		[a_contact]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[a_phone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[a_email]          [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[addresses_Archive2017] SET (LOCK_ESCALATION = TABLE)
GO
