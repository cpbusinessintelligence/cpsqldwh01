SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[agedbalances] (
		[a_company_id]     [int] NOT NULL,
		[a_current]        [float] NULL,
		[a_7days]          [float] NULL,
		[a_14days]         [float] NULL,
		[a_21days]         [float] NULL,
		[a_28days]         [float] NULL,
		[a_60days]         [float] NULL,
		[a_90days]         [float] NULL,
		[a_over]           [float] NULL,
		[a_total]          [float] NULL,
		[a_asat]           [smalldatetime] NULL,
		CONSTRAINT [PK__agedbala__560806CCC4F8FDEA]
		PRIMARY KEY
		CLUSTERED
		([a_company_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_com__0289B719]
	DEFAULT ('0') FOR [a_company_id]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_cur__037DDB52]
	DEFAULT ('0') FOR [a_current]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_7da__0471FF8B]
	DEFAULT ('0') FOR [a_7days]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_14d__056623C4]
	DEFAULT ('0') FOR [a_14days]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_21d__065A47FD]
	DEFAULT ('0') FOR [a_21days]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_28d__074E6C36]
	DEFAULT ('0') FOR [a_28days]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_60d__0842906F]
	DEFAULT ('0') FOR [a_60days]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_90d__0936B4A8]
	DEFAULT ('0') FOR [a_90days]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_ove__0A2AD8E1]
	DEFAULT ('0') FOR [a_over]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_tot__0B1EFD1A]
	DEFAULT ('0') FOR [a_total]
GO
ALTER TABLE [dbo].[agedbalances]
	ADD
	CONSTRAINT [DF__agedbalan__a_asa__0C132153]
	DEFAULT (NULL) FOR [a_asat]
GO
ALTER TABLE [dbo].[agedbalances]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agedbalances_company]
	FOREIGN KEY ([a_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agedbalances]
	NOCHECK CONSTRAINT [FK_agedbalances_company]

GO
ALTER TABLE [dbo].[agedbalances] SET (LOCK_ESCALATION = TABLE)
GO
