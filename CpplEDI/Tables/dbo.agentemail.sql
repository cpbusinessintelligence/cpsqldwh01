SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[agentemail] (
		[ae_agent]           [int] NOT NULL,
		[ae_description]     [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[ae_email]           [varchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[ae_class]           [int] NOT NULL,
		[ae_style]           [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[agentemail]
	ADD
	CONSTRAINT [DF__agentemai__ae_ag__0DFB69C5]
	DEFAULT ('0') FOR [ae_agent]
GO
ALTER TABLE [dbo].[agentemail]
	ADD
	CONSTRAINT [DF__agentemai__ae_de__0EEF8DFE]
	DEFAULT ('') FOR [ae_description]
GO
ALTER TABLE [dbo].[agentemail]
	ADD
	CONSTRAINT [DF__agentemai__ae_em__0FE3B237]
	DEFAULT ('') FOR [ae_email]
GO
ALTER TABLE [dbo].[agentemail]
	ADD
	CONSTRAINT [DF__agentemai__ae_cl__10D7D670]
	DEFAULT ('0') FOR [ae_class]
GO
ALTER TABLE [dbo].[agentemail]
	ADD
	CONSTRAINT [DF__agentemai__ae_st__11CBFAA9]
	DEFAULT ('N') FOR [ae_style]
GO
ALTER TABLE [dbo].[agentemail]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentemail_agent]
	FOREIGN KEY ([ae_agent]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentemail]
	NOCHECK CONSTRAINT [FK_agentemail_agent]

GO
ALTER TABLE [dbo].[agentemail]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentemail_class]
	FOREIGN KEY ([ae_class]) REFERENCES [dbo].[companyclass] ([cc_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentemail]
	NOCHECK CONSTRAINT [FK_agentemail_class]

GO
ALTER TABLE [dbo].[agentemail] SET (LOCK_ESCALATION = TABLE)
GO
