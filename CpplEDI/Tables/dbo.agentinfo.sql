SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[agentinfo] (
		[zoning_id]             [int] NOT NULL,
		[postcode]              [int] NULL,
		[suburb]                [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[state]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[agent_id]              [int] NOT NULL,
		[alt_agent_id]          [int] NOT NULL,
		[zoneid]                [int] NULL,
		[subzoneid]             [int] NULL,
		[branch]                [int] NULL,
		[depot]                 [int] NULL,
		[cppldriver]            [int] NULL,
		[ai_id]                 [int] NOT NULL,
		[ai_eta_zone]           [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_code]          [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[a_imported]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_nsw]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_act]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_vic]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_tas]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_qld]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_sa]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_nt]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_wa]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_suffix]        [char](8) COLLATE Latin1_General_CI_AS NULL,
		[ai_eta_range_code]     [char](12) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__agentinf__0372DAEEA3740CEA]
		PRIMARY KEY
		CLUSTERED
		([ai_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__zonin__14A86754]
	DEFAULT ('0') FOR [zoning_id]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__postc__159C8B8D]
	DEFAULT (NULL) FOR [postcode]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__subur__1690AFC6]
	DEFAULT (NULL) FOR [suburb]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__state__1784D3FF]
	DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__agent__1878F838]
	DEFAULT ('0') FOR [agent_id]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__alt_a__196D1C71]
	DEFAULT ('0') FOR [alt_agent_id]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__zonei__1A6140AA]
	DEFAULT ('0') FOR [zoneid]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__subzo__1B5564E3]
	DEFAULT ('0') FOR [subzoneid]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__branc__1C49891C]
	DEFAULT (NULL) FOR [branch]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__depot__1D3DAD55]
	DEFAULT (NULL) FOR [depot]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__cppld__1E31D18E]
	DEFAULT ('0') FOR [cppldriver]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_et__1F25F5C7]
	DEFAULT ('') FOR [ai_eta_zone]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__201A1A00]
	DEFAULT (NULL) FOR [ai_sort_code]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__a_imp__210E3E39]
	DEFAULT ('N') FOR [a_imported]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__22026272]
	DEFAULT ('') FOR [ai_sort_nsw]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__22F686AB]
	DEFAULT ('') FOR [ai_sort_act]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__23EAAAE4]
	DEFAULT ('') FOR [ai_sort_vic]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__24DECF1D]
	DEFAULT ('') FOR [ai_sort_tas]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__25D2F356]
	DEFAULT ('') FOR [ai_sort_qld]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__26C7178F]
	DEFAULT ('') FOR [ai_sort_sa]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__27BB3BC8]
	DEFAULT ('') FOR [ai_sort_nt]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__28AF6001]
	DEFAULT ('') FOR [ai_sort_wa]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__29A3843A]
	DEFAULT ('') FOR [ai_sort_suffix]
GO
ALTER TABLE [dbo].[agentinfo]
	ADD
	CONSTRAINT [DF__agentinfo__ai_et__2A97A873]
	DEFAULT ('') FOR [ai_eta_range_code]
GO
ALTER TABLE [dbo].[agentinfo]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo_zoning_id]
	FOREIGN KEY ([zoning_id]) REFERENCES [dbo].[zoning] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo]
	NOCHECK CONSTRAINT [FK_agentinfo_zoning_id]

GO
ALTER TABLE [dbo].[agentinfo]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo_driver_cppldriver]
	FOREIGN KEY ([cppldriver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo]
	NOCHECK CONSTRAINT [FK_agentinfo_driver_cppldriver]

GO
ALTER TABLE [dbo].[agentinfo]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo_branch]
	FOREIGN KEY ([branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo]
	NOCHECK CONSTRAINT [FK_agentinfo_branch]

GO
ALTER TABLE [dbo].[agentinfo]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo_subzoneid]
	FOREIGN KEY ([subzoneid]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo]
	NOCHECK CONSTRAINT [FK_agentinfo_subzoneid]

GO
ALTER TABLE [dbo].[agentinfo]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo_agent]
	FOREIGN KEY ([agent_id]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo]
	NOCHECK CONSTRAINT [FK_agentinfo_agent]

GO
ALTER TABLE [dbo].[agentinfo]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo_zoneid]
	FOREIGN KEY ([zoneid]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo]
	NOCHECK CONSTRAINT [FK_agentinfo_zoneid]

GO
CREATE NONCLUSTERED INDEX [idx_agentinfo_etazone]
	ON [dbo].[agentinfo] ([postcode], [suburb], [ai_eta_zone])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[agentinfo] SET (LOCK_ESCALATION = TABLE)
GO
