SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[agentinfo2] (
		[ai_id]          [int] NOT NULL,
		[agent_id]       [int] NOT NULL,
		[pricing_id]     [char](4) COLLATE Latin1_General_CI_AS NOT NULL,
		[postcode]       [int] NULL,
		[suburb]         [char](32) COLLATE Latin1_General_CI_AS NULL,
		[state]          [char](10) COLLATE Latin1_General_CI_AS NULL,
		[metro]          [int] NULL,
		[links]          [int] NULL,
		[zone]           [char](10) COLLATE Latin1_General_CI_AS NULL,
		[zoneid]         [int] NULL,
		[depot]          [int] NULL,
		[branch]         [int] NULL,
		CONSTRAINT [PK__agentinf__0372DAEEBD349C53]
		PRIMARY KEY
		CLUSTERED
		([ai_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__agent__2D74151E]
	DEFAULT ('0') FOR [agent_id]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__prici__2E683957]
	DEFAULT ('') FOR [pricing_id]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__postc__2F5C5D90]
	DEFAULT (NULL) FOR [postcode]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__subur__305081C9]
	DEFAULT (NULL) FOR [suburb]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__state__3144A602]
	DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__metro__3238CA3B]
	DEFAULT (NULL) FOR [metro]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__links__332CEE74]
	DEFAULT (NULL) FOR [links]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo2__zone__342112AD]
	DEFAULT (NULL) FOR [zone]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__zonei__351536E6]
	DEFAULT ('0') FOR [zoneid]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__depot__36095B1F]
	DEFAULT (NULL) FOR [depot]
GO
ALTER TABLE [dbo].[agentinfo2]
	ADD
	CONSTRAINT [DF__agentinfo__branc__36FD7F58]
	DEFAULT (NULL) FOR [branch]
GO
ALTER TABLE [dbo].[agentinfo2]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo2_branch]
	FOREIGN KEY ([branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo2]
	NOCHECK CONSTRAINT [FK_agentinfo2_branch]

GO
ALTER TABLE [dbo].[agentinfo2]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo2_agent]
	FOREIGN KEY ([agent_id]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo2]
	NOCHECK CONSTRAINT [FK_agentinfo2_agent]

GO
ALTER TABLE [dbo].[agentinfo2]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agentinfo2_zoneid]
	FOREIGN KEY ([zoneid]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agentinfo2]
	NOCHECK CONSTRAINT [FK_agentinfo2_zoneid]

GO
ALTER TABLE [dbo].[agentinfo2] SET (LOCK_ESCALATION = TABLE)
GO
