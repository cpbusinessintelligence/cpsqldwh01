SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[agentinfo3] (
		[zoning_id]          [int] NOT NULL,
		[postcode]           [int] NULL,
		[suburb]             [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[state]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[agent_id]           [int] NOT NULL,
		[zoneid]             [int] NULL,
		[subzoneid]          [int] NULL,
		[branch]             [int] NULL,
		[depot]              [int] NULL,
		[cppldriver]         [int] NULL,
		[ai_id]              [int] NOT NULL,
		[ai_eta_zone]        [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_code]       [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[a_imported]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_nsw]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_act]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_vic]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_tas]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_qld]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_sa]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_nt]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_wa]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_suffix]     [char](8) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__zonin__38E5C7CA]
	DEFAULT ('0') FOR [zoning_id]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__postc__39D9EC03]
	DEFAULT (NULL) FOR [postcode]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__subur__3ACE103C]
	DEFAULT (NULL) FOR [suburb]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__state__3BC23475]
	DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__agent__3CB658AE]
	DEFAULT ('0') FOR [agent_id]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__zonei__3DAA7CE7]
	DEFAULT ('0') FOR [zoneid]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__subzo__3E9EA120]
	DEFAULT ('0') FOR [subzoneid]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__branc__3F92C559]
	DEFAULT (NULL) FOR [branch]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__depot__4086E992]
	DEFAULT (NULL) FOR [depot]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__cppld__417B0DCB]
	DEFAULT ('0') FOR [cppldriver]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_id__426F3204]
	DEFAULT ('0') FOR [ai_id]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_et__4363563D]
	DEFAULT ('') FOR [ai_eta_zone]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__44577A76]
	DEFAULT (NULL) FOR [ai_sort_code]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__a_imp__454B9EAF]
	DEFAULT ('N') FOR [a_imported]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__463FC2E8]
	DEFAULT ('') FOR [ai_sort_nsw]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__4733E721]
	DEFAULT ('') FOR [ai_sort_act]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__48280B5A]
	DEFAULT ('') FOR [ai_sort_vic]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__491C2F93]
	DEFAULT ('') FOR [ai_sort_tas]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__4A1053CC]
	DEFAULT ('') FOR [ai_sort_qld]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__4B047805]
	DEFAULT ('') FOR [ai_sort_sa]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__4BF89C3E]
	DEFAULT ('') FOR [ai_sort_nt]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__4CECC077]
	DEFAULT ('') FOR [ai_sort_wa]
GO
ALTER TABLE [dbo].[agentinfo3]
	ADD
	CONSTRAINT [DF__agentinfo__ai_so__4DE0E4B0]
	DEFAULT ('') FOR [ai_sort_suffix]
GO
ALTER TABLE [dbo].[agentinfo3] SET (LOCK_ESCALATION = TABLE)
GO
