SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[agents] (
		[a_id]                     [int] NOT NULL,
		[a_shortname]              [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[a_name]                   [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[a_supplier_code]          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_podrequired]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_cppl]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_querybranch]            [int] NULL,
		[a_driver]                 [int] NULL,
		[a_label_email]            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[a_label_printer]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[a_booking_branch]         [int] NULL,
		[a_booking_email]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[a_returns_direct]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_ezyfreight_direct]      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_import_direct]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_auth_owner]             [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_auth_state]             [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_auth_company]           [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_ogm_trigger]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_ogm_data]               [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[a_ogm_runs]               [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_split_returns]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_style]                  [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_ap]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_apinfo]                 [char](16) COLLATE Latin1_General_CI_AS NULL,
		[a_sms]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_state]                  [char](80) COLLATE Latin1_General_CI_AS NULL,
		[a_direct_method]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_direct_additional0]     [char](64) COLLATE Latin1_General_CI_AS NULL,
		[a_direct_additional1]     [char](64) COLLATE Latin1_General_CI_AS NULL,
		[a_direct_additional2]     [char](64) COLLATE Latin1_General_CI_AS NULL,
		[a_direct_additional3]     [char](64) COLLATE Latin1_General_CI_AS NULL,
		[a_direct_additional4]     [char](64) COLLATE Latin1_General_CI_AS NULL,
		[a_disabled]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__agents__566AFA9AF047DEA0]
		PRIMARY KEY
		CLUSTERED
		([a_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_shortn__0B89116E]
	DEFAULT ('') FOR [a_shortname]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_name__0C7D35A7]
	DEFAULT ('') FOR [a_name]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_suppli__0D7159E0]
	DEFAULT ('') FOR [a_supplier_code]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_podreq__0E657E19]
	DEFAULT ('N') FOR [a_podrequired]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_cppl__0F59A252]
	DEFAULT ('N') FOR [a_cppl]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_queryb__104DC68B]
	DEFAULT (NULL) FOR [a_querybranch]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_driver__1141EAC4]
	DEFAULT ('0') FOR [a_driver]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_label___12360EFD]
	DEFAULT (NULL) FOR [a_label_email]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_label___132A3336]
	DEFAULT (NULL) FOR [a_label_printer]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_bookin__141E576F]
	DEFAULT (NULL) FOR [a_booking_branch]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_bookin__15127BA8]
	DEFAULT (NULL) FOR [a_booking_email]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_return__16069FE1]
	DEFAULT ('N') FOR [a_returns_direct]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_ezyfre__16FAC41A]
	DEFAULT ('N') FOR [a_ezyfreight_direct]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_import__17EEE853]
	DEFAULT ('N') FOR [a_import_direct]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_auth_o__18E30C8C]
	DEFAULT ('') FOR [a_auth_owner]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_auth_s__19D730C5]
	DEFAULT ('') FOR [a_auth_state]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_auth_c__1ACB54FE]
	DEFAULT ('') FOR [a_auth_company]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_ogm_tr__1BBF7937]
	DEFAULT ('D') FOR [a_ogm_trigger]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_ogm_da__1CB39D70]
	DEFAULT ('') FOR [a_ogm_data]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_ogm_ru__1DA7C1A9]
	DEFAULT ('YNNNNNNNNNNNNNNN') FOR [a_ogm_runs]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_split___1E9BE5E2]
	DEFAULT ('N') FOR [a_split_returns]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_style__1F900A1B]
	DEFAULT ('') FOR [a_style]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_ap__20842E54]
	DEFAULT ('N') FOR [a_ap]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_apinfo__2178528D]
	DEFAULT ('') FOR [a_apinfo]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_direct__226C76C6]
	DEFAULT ('C') FOR [a_direct_method]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_direct__23609AFF]
	DEFAULT ('') FOR [a_direct_additional0]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_direct__2454BF38]
	DEFAULT ('') FOR [a_direct_additional1]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_direct__2548E371]
	DEFAULT ('') FOR [a_direct_additional2]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_direct__263D07AA]
	DEFAULT ('') FOR [a_direct_additional3]
GO
ALTER TABLE [dbo].[agents]
	ADD
	CONSTRAINT [DF__agents__a_direct__27312BE3]
	DEFAULT ('') FOR [a_direct_additional4]
GO
ALTER TABLE [dbo].[agents]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agents_driver]
	FOREIGN KEY ([a_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agents]
	NOCHECK CONSTRAINT [FK_agents_driver]

GO
ALTER TABLE [dbo].[agents]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agents_booking_branch]
	FOREIGN KEY ([a_booking_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agents]
	NOCHECK CONSTRAINT [FK_agents_booking_branch]

GO
ALTER TABLE [dbo].[agents]
	WITH NOCHECK
	ADD CONSTRAINT [FK_agents_querybranch]
	FOREIGN KEY ([a_querybranch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[agents]
	NOCHECK CONSTRAINT [FK_agents_querybranch]

GO
ALTER TABLE [dbo].[agents] SET (LOCK_ESCALATION = TABLE)
GO
