SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[apconnote] (
		[ac_id]                       [int] NOT NULL,
		[ac_connote]                  [char](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[ac_date]                     [smalldatetime] NULL,
		[ac_consignment]              [int] NOT NULL,
		[ac_sender_name]              [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_sender_addr1]             [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_sender_addr2]             [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_sender_suburb]            [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_sender_postcode]          [char](10) COLLATE Latin1_General_CI_AS NULL,
		[ac_receiver_name]            [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_receiver_addr1]           [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_receiver_addr2]           [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_receiver_suburb]          [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ac_receiver_postcode]        [char](10) COLLATE Latin1_General_CI_AS NULL,
		[ac_atl]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ac_special_instructions]     [char](120) COLLATE Latin1_General_CI_AS NULL,
		[ac_manifest_stamp]           [datetime] NULL,
		[ac_manifest_id]              [int] NULL,
		[ac_sender_location]          [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ac_receiver_location]        [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ac_service]                  [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ac_ap_shipment]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__apconnot__A36ABBF467FE861C]
		PRIMARY KEY
		CLUSTERED
		([ac_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_da__50BD515B]
	DEFAULT (NULL) FOR [ac_date]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_se__51B17594]
	DEFAULT ('') FOR [ac_sender_name]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_se__52A599CD]
	DEFAULT ('') FOR [ac_sender_addr1]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_se__5399BE06]
	DEFAULT ('') FOR [ac_sender_addr2]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_se__548DE23F]
	DEFAULT ('') FOR [ac_sender_suburb]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_se__55820678]
	DEFAULT ('') FOR [ac_sender_postcode]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_re__56762AB1]
	DEFAULT ('') FOR [ac_receiver_name]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_re__576A4EEA]
	DEFAULT ('') FOR [ac_receiver_addr1]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_re__585E7323]
	DEFAULT ('') FOR [ac_receiver_addr2]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_re__5952975C]
	DEFAULT ('') FOR [ac_receiver_suburb]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_re__5A46BB95]
	DEFAULT ('') FOR [ac_receiver_postcode]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_at__5B3ADFCE]
	DEFAULT ('N') FOR [ac_atl]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_sp__5C2F0407]
	DEFAULT ('') FOR [ac_special_instructions]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_ma__5D232840]
	DEFAULT (NULL) FOR [ac_manifest_stamp]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_ma__5E174C79]
	DEFAULT ('0') FOR [ac_manifest_id]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_se__5F0B70B2]
	DEFAULT ('') FOR [ac_sender_location]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_re__5FFF94EB]
	DEFAULT ('') FOR [ac_receiver_location]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_se__60F3B924]
	DEFAULT ('') FOR [ac_service]
GO
ALTER TABLE [dbo].[apconnote]
	ADD
	CONSTRAINT [DF__apconnote__ac_ap__61E7DD5D]
	DEFAULT ('') FOR [ac_ap_shipment]
GO
ALTER TABLE [dbo].[apconnote] SET (LOCK_ESCALATION = TABLE)
GO
