SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[apinfo] (
		[ai_sender_id]                    [char](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[ai_sender_name]                  [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ai_sender_addr1]                 [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ai_sender_addr2]                 [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ai_sender_suburb]                [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ai_sender_postcode]              [int] NULL,
		[ai_sender_record_no]             [int] NULL,
		[ai_connote_prefix]               [char](8) COLLATE Latin1_General_CI_AS NULL,
		[ai_connote_id]                   [int] NULL,
		[ai_connote_start]                [int] NULL,
		[ai_connote_finish]               [int] NULL,
		[ai_merchant_location_id]         [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ai_manifest_filename_prefix]     [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ai_manifest_id]                  [int] NULL,
		[ai_merchant_id]                  [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ai_charge_code]                  [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ai_charge_description]           [char](40) COLLATE Latin1_General_CI_AS NULL,
		[ai_transaction_id]               [int] NULL,
		[ai_post_account]                 [char](20) COLLATE Latin1_General_CI_AS NULL,
		[ai_lodgement_facility]           [char](80) COLLATE Latin1_General_CI_AS NULL,
		[ai_manifest_username]            [char](32) COLLATE Latin1_General_CI_AS NULL,
		[ai_transfer_host]                [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[ai_transfer_username]            [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[ai_transfer_password]            [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[ai_transfer_path]                [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[ai_special_agent_id]             [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[ai_sender_location]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__apinfo__F67A77AB4F2A78AF]
		PRIMARY KEY
		CLUSTERED
		([ai_sender_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_sende__64C44A08]
	DEFAULT ('') FOR [ai_sender_name]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_sende__65B86E41]
	DEFAULT ('') FOR [ai_sender_addr1]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_sende__66AC927A]
	DEFAULT ('') FOR [ai_sender_addr2]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_sende__67A0B6B3]
	DEFAULT ('') FOR [ai_sender_suburb]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_sende__6894DAEC]
	DEFAULT ('0') FOR [ai_sender_postcode]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_sende__6988FF25]
	DEFAULT ('0') FOR [ai_sender_record_no]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_conno__6A7D235E]
	DEFAULT ('') FOR [ai_connote_prefix]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_conno__6B714797]
	DEFAULT ('1000000') FOR [ai_connote_id]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_conno__6C656BD0]
	DEFAULT ('1000000') FOR [ai_connote_start]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_conno__6D599009]
	DEFAULT ('9999999') FOR [ai_connote_finish]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_merch__6E4DB442]
	DEFAULT ('') FOR [ai_merchant_location_id]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_manif__6F41D87B]
	DEFAULT ('') FOR [ai_manifest_filename_prefix]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_manif__7035FCB4]
	DEFAULT ('0') FOR [ai_manifest_id]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_merch__712A20ED]
	DEFAULT ('') FOR [ai_merchant_id]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_charg__721E4526]
	DEFAULT ('') FOR [ai_charge_code]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_charg__7312695F]
	DEFAULT ('') FOR [ai_charge_description]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_trans__74068D98]
	DEFAULT ('0') FOR [ai_transaction_id]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_post___74FAB1D1]
	DEFAULT ('') FOR [ai_post_account]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_lodge__75EED60A]
	DEFAULT ('') FOR [ai_lodgement_facility]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_manif__76E2FA43]
	DEFAULT ('') FOR [ai_manifest_username]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_trans__77D71E7C]
	DEFAULT (NULL) FOR [ai_transfer_host]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_trans__78CB42B5]
	DEFAULT (NULL) FOR [ai_transfer_username]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_trans__79BF66EE]
	DEFAULT (NULL) FOR [ai_transfer_password]
GO
ALTER TABLE [dbo].[apinfo]
	ADD
	CONSTRAINT [DF__apinfo__ai_speci__7AB38B27]
	DEFAULT (NULL) FOR [ai_special_agent_id]
GO
ALTER TABLE [dbo].[apinfo] SET (LOCK_ESCALATION = TABLE)
GO
