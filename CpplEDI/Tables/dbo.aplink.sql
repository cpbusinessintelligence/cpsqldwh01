SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aplink] (
		[al_id]              [int] NOT NULL,
		[al_cpl_connote]     [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[al_cpl_label]       [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[al_cpl_id]          [int] NULL,
		[al_ap_connote]      [char](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[al_ap_label]        [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[al_ap_no]           [int] NOT NULL,
		[al_ap_id]           [int] NULL,
		[al_status]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[al_weight]          [float] NULL,
		[al_printed]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[al_ap_full]         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[al_ap_item_id]      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__aplink__84248F834DCD52FA]
		PRIMARY KEY
		CLUSTERED
		([al_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[aplink]
	ADD
	CONSTRAINT [DF__aplink__al_weigh__006C647D]
	DEFAULT ('0') FOR [al_weight]
GO
ALTER TABLE [dbo].[aplink]
	ADD
	CONSTRAINT [DF__aplink__al_print__016088B6]
	DEFAULT ('N') FOR [al_printed]
GO
ALTER TABLE [dbo].[aplink]
	ADD
	CONSTRAINT [DF__aplink__al_ap_fu__0254ACEF]
	DEFAULT ('') FOR [al_ap_full]
GO
ALTER TABLE [dbo].[aplink]
	ADD
	CONSTRAINT [DF__aplink__al_ap_it__0348D128]
	DEFAULT ('') FOR [al_ap_item_id]
GO
ALTER TABLE [dbo].[aplink]
	ADD
	CONSTRAINT [DF__aplink__al_cpl_i__7D8FF7D2]
	DEFAULT ('0') FOR [al_cpl_id]
GO
ALTER TABLE [dbo].[aplink]
	ADD
	CONSTRAINT [DF__aplink__al_ap_id__7E841C0B]
	DEFAULT ('0') FOR [al_ap_id]
GO
ALTER TABLE [dbo].[aplink]
	ADD
	CONSTRAINT [DF__aplink__al_statu__7F784044]
	DEFAULT ('N') FOR [al_status]
GO
ALTER TABLE [dbo].[aplink] SET (LOCK_ESCALATION = TABLE)
GO
