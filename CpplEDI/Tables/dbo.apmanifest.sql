SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[apmanifest] (
		[am_id]               [int] NOT NULL,
		[am_sending_id]       [char](16) COLLATE Latin1_General_CI_AS NULL,
		[am_receiving_id]     [char](16) COLLATE Latin1_General_CI_AS NULL,
		[am_submitted]        [datetime] NULL,
		[am_lodged]           [datetime] NULL,
		[am_created]          [datetime] NULL,
		[am_connotes]         [int] NULL,
		[am_labels]           [int] NULL,
		[am_transmitted]      [datetime] NULL,
		[am_local_id]         [int] NULL,
		[am_received]         [datetime] NULL,
		[am_approved]         [datetime] NULL,
		[am_cancelled]        [datetime] NULL,
		[am_order_id]         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__apmanife__B95A8ED0D69A4810]
		PRIMARY KEY
		CLUSTERED
		([am_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_se__06253DD3]
	DEFAULT ('') FOR [am_sending_id]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_re__0719620C]
	DEFAULT ('') FOR [am_receiving_id]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_su__080D8645]
	DEFAULT (NULL) FOR [am_submitted]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_lo__0901AA7E]
	DEFAULT (NULL) FOR [am_lodged]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_cr__09F5CEB7]
	DEFAULT (NULL) FOR [am_created]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_co__0AE9F2F0]
	DEFAULT ('0') FOR [am_connotes]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_la__0BDE1729]
	DEFAULT ('0') FOR [am_labels]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_tr__0CD23B62]
	DEFAULT (NULL) FOR [am_transmitted]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_lo__0DC65F9B]
	DEFAULT ('0') FOR [am_local_id]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_re__0EBA83D4]
	DEFAULT (NULL) FOR [am_received]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_ap__0FAEA80D]
	DEFAULT (NULL) FOR [am_approved]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_ca__10A2CC46]
	DEFAULT (NULL) FOR [am_cancelled]
GO
ALTER TABLE [dbo].[apmanifest]
	ADD
	CONSTRAINT [DF__apmanifes__am_or__1196F07F]
	DEFAULT ('') FOR [am_order_id]
GO
ALTER TABLE [dbo].[apmanifest] SET (LOCK_ESCALATION = TABLE)
GO
