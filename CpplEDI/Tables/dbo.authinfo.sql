SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[authinfo] (
		[ai_id]       [int] NOT NULL,
		[ai_type]     [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[ai_name]     [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__authinfo__0372DAEEE37D7CF2]
		PRIMARY KEY
		CLUSTERED
		([ai_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[authinfo]
	ADD
	CONSTRAINT [DF__authinfo__ai_typ__14735D2A]
	DEFAULT ('') FOR [ai_type]
GO
ALTER TABLE [dbo].[authinfo]
	ADD
	CONSTRAINT [DF__authinfo__ai_nam__15678163]
	DEFAULT ('') FOR [ai_name]
GO
ALTER TABLE [dbo].[authinfo] SET (LOCK_ESCALATION = TABLE)
GO
