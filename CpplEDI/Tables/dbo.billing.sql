SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[billing] (
		[b_id]               [int] NOT NULL,
		[b_company]          [int] NOT NULL,
		[b_date]             [smalldatetime] NULL,
		[b_stamp]            [datetime] NULL,
		[b_consignments]     [int] NULL,
		[b_bookins]          [int] NULL,
		[b_total]            [float] NULL,
		CONSTRAINT [PK__billing__4E29C30D092561E6]
		PRIMARY KEY
		CLUSTERED
		([b_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[billing]
	ADD
	CONSTRAINT [DF__billing__b_compa__1843EE0E]
	DEFAULT ('0') FOR [b_company]
GO
ALTER TABLE [dbo].[billing]
	ADD
	CONSTRAINT [DF__billing__b_date__19381247]
	DEFAULT (NULL) FOR [b_date]
GO
ALTER TABLE [dbo].[billing]
	ADD
	CONSTRAINT [DF__billing__b_stamp__1A2C3680]
	DEFAULT (NULL) FOR [b_stamp]
GO
ALTER TABLE [dbo].[billing]
	ADD
	CONSTRAINT [DF__billing__b_consi__1B205AB9]
	DEFAULT ('0') FOR [b_consignments]
GO
ALTER TABLE [dbo].[billing]
	ADD
	CONSTRAINT [DF__billing__b_booki__1C147EF2]
	DEFAULT ('0') FOR [b_bookins]
GO
ALTER TABLE [dbo].[billing]
	ADD
	CONSTRAINT [DF__billing__b_total__1D08A32B]
	DEFAULT ('0') FOR [b_total]
GO
ALTER TABLE [dbo].[billing]
	WITH NOCHECK
	ADD CONSTRAINT [FK_billing_company]
	FOREIGN KEY ([b_company]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[billing]
	NOCHECK CONSTRAINT [FK_billing_company]

GO
ALTER TABLE [dbo].[billing] SET (LOCK_ESCALATION = TABLE)
GO
