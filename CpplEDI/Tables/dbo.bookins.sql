SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bookins] (
		[bi_id]                    [int] NOT NULL,
		[bi_company_id]            [int] NOT NULL,
		[bi_import_id]             [int] NULL,
		[bi_warehouse_id]          [int] NOT NULL,
		[bi_date]                  [smalldatetime] NULL,
		[bi_connote]               [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[bi_charge]                [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[bi_units]                 [int] NULL,
		[bi_units_entries]         [int] NULL,
		[bi_deadweight]            [float] NULL,
		[bi_volume]                [float] NULL,
		[bi_reference]             [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[bi_sender_addr0]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_sender_addr1]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_sender_addr2]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_sender_addr3]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_sender_suburb]         [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[bi_sender_postcode]       [int] NULL,
		[bi_receiver_addr0]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_receiver_addr1]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_receiver_addr2]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_receiver_addr3]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_receiver_suburb]       [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[bi_receiver_postcode]     [int] NULL,
		[bi_pricecode]             [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[bi_special]               [varchar](80) COLLATE Latin1_General_CI_AS NULL,
		[bi_not_before]            [smalldatetime] NULL,
		[bi_not_after]             [smalldatetime] NULL,
		[bi_revised_date]          [smalldatetime] NULL,
		[bi_revised_time]          [time](7) NULL,
		[bi_received]              [smalldatetime] NULL,
		[bi_released]              [smalldatetime] NULL,
		[bi_comments]              [varchar](80) COLLATE Latin1_General_CI_AS NULL,
		[bi_label]                 [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[bi_scan_received]         [datetime] NULL,
		[bi_scan_released]         [datetime] NULL,
		[bi_billing_id]            [int] NULL,
		[bi_billing_date]          [smalldatetime] NULL,
		[bi_export_id]             [int] NULL,
		[bi_export2_id]            [int] NULL,
		[bi_agent_pod_name]        [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[bi_agent_pod_stamp]       [datetime] NULL,
		[bi_agent_pod_entry]       [datetime] NULL,
		CONSTRAINT [PK__bookins__5F352EC73DD4289C]
		PRIMARY KEY
		CLUSTERED
		([bi_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_comp__1FE50FD6]
	DEFAULT ('0') FOR [bi_company_id]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_impo__20D9340F]
	DEFAULT (NULL) FOR [bi_import_id]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_ware__21CD5848]
	DEFAULT ('0') FOR [bi_warehouse_id]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_date__22C17C81]
	DEFAULT (NULL) FOR [bi_date]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_conn__23B5A0BA]
	DEFAULT ('') FOR [bi_connote]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_char__24A9C4F3]
	DEFAULT ('') FOR [bi_charge]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_unit__259DE92C]
	DEFAULT ('1') FOR [bi_units]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_unit__26920D65]
	DEFAULT ('0') FOR [bi_units_entries]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_dead__2786319E]
	DEFAULT (NULL) FOR [bi_deadweight]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_volu__287A55D7]
	DEFAULT (NULL) FOR [bi_volume]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_refe__296E7A10]
	DEFAULT ('') FOR [bi_reference]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_send__2A629E49]
	DEFAULT ('') FOR [bi_sender_addr0]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_send__2B56C282]
	DEFAULT ('') FOR [bi_sender_addr1]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_send__2C4AE6BB]
	DEFAULT ('') FOR [bi_sender_addr2]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_send__2D3F0AF4]
	DEFAULT ('') FOR [bi_sender_addr3]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_send__2E332F2D]
	DEFAULT ('') FOR [bi_sender_suburb]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_send__2F275366]
	DEFAULT ('0') FOR [bi_sender_postcode]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rece__301B779F]
	DEFAULT ('') FOR [bi_receiver_addr0]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rece__310F9BD8]
	DEFAULT ('') FOR [bi_receiver_addr1]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rece__3203C011]
	DEFAULT ('') FOR [bi_receiver_addr2]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rece__32F7E44A]
	DEFAULT ('') FOR [bi_receiver_addr3]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rece__33EC0883]
	DEFAULT ('') FOR [bi_receiver_suburb]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rece__34E02CBC]
	DEFAULT ('0') FOR [bi_receiver_postcode]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_pric__35D450F5]
	DEFAULT ('BOOKIN') FOR [bi_pricecode]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_spec__36C8752E]
	DEFAULT ('') FOR [bi_special]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_not___37BC9967]
	DEFAULT (NULL) FOR [bi_not_before]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_not___38B0BDA0]
	DEFAULT (NULL) FOR [bi_not_after]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_revi__39A4E1D9]
	DEFAULT (NULL) FOR [bi_revised_date]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_revi__3A990612]
	DEFAULT ('00:00:00') FOR [bi_revised_time]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rece__3B8D2A4B]
	DEFAULT (NULL) FOR [bi_received]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_rele__3C814E84]
	DEFAULT (NULL) FOR [bi_released]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_comm__3D7572BD]
	DEFAULT (NULL) FOR [bi_comments]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_labe__3E6996F6]
	DEFAULT (NULL) FOR [bi_label]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_scan__3F5DBB2F]
	DEFAULT (NULL) FOR [bi_scan_received]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_scan__4051DF68]
	DEFAULT (NULL) FOR [bi_scan_released]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_bill__414603A1]
	DEFAULT ('0') FOR [bi_billing_id]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_bill__423A27DA]
	DEFAULT (NULL) FOR [bi_billing_date]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_expo__432E4C13]
	DEFAULT ('0') FOR [bi_export_id]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_expo__4422704C]
	DEFAULT ('0') FOR [bi_export2_id]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_agen__45169485]
	DEFAULT (NULL) FOR [bi_agent_pod_name]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_agen__460AB8BE]
	DEFAULT (NULL) FOR [bi_agent_pod_stamp]
GO
ALTER TABLE [dbo].[bookins]
	ADD
	CONSTRAINT [DF__bookins__bi_agen__46FEDCF7]
	DEFAULT (NULL) FOR [bi_agent_pod_entry]
GO
ALTER TABLE [dbo].[bookins]
	WITH NOCHECK
	ADD CONSTRAINT [FK_bookins_export]
	FOREIGN KEY ([bi_export_id]) REFERENCES [dbo].[exports] ([e_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[bookins]
	NOCHECK CONSTRAINT [FK_bookins_export]

GO
ALTER TABLE [dbo].[bookins]
	WITH NOCHECK
	ADD CONSTRAINT [FK_bookins_export2]
	FOREIGN KEY ([bi_export2_id]) REFERENCES [dbo].[exports] ([e_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[bookins]
	NOCHECK CONSTRAINT [FK_bookins_export2]

GO
ALTER TABLE [dbo].[bookins]
	WITH NOCHECK
	ADD CONSTRAINT [FK_bookins_import]
	FOREIGN KEY ([bi_import_id]) REFERENCES [dbo].[imports] ([i_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[bookins]
	NOCHECK CONSTRAINT [FK_bookins_import]

GO
ALTER TABLE [dbo].[bookins]
	WITH NOCHECK
	ADD CONSTRAINT [FK_bookins_billing]
	FOREIGN KEY ([bi_billing_id]) REFERENCES [dbo].[billing] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[bookins]
	NOCHECK CONSTRAINT [FK_bookins_billing]

GO
ALTER TABLE [dbo].[bookins]
	WITH NOCHECK
	ADD CONSTRAINT [FK_bookins_warehouse]
	FOREIGN KEY ([bi_warehouse_id]) REFERENCES [dbo].[warehouse] ([w_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[bookins]
	NOCHECK CONSTRAINT [FK_bookins_warehouse]

GO
ALTER TABLE [dbo].[bookins]
	WITH NOCHECK
	ADD CONSTRAINT [FK_bookins_company]
	FOREIGN KEY ([bi_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[bookins]
	NOCHECK CONSTRAINT [FK_bookins_company]

GO
ALTER TABLE [dbo].[bookins] SET (LOCK_ESCALATION = TABLE)
GO
