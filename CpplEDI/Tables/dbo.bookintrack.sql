SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[bookintrack] (
		[bt_id]         [int] NOT NULL,
		[bt_bookin]     [int] NOT NULL,
		CONSTRAINT [PK__bookintr__73478FBE282712BB]
		PRIMARY KEY
		CLUSTERED
		([bt_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[bookintrack]
	ADD
	CONSTRAINT [DF__bookintra__bt_bo__49DB49A2]
	DEFAULT ('0') FOR [bt_bookin]
GO
ALTER TABLE [dbo].[bookintrack] SET (LOCK_ESCALATION = TABLE)
GO
