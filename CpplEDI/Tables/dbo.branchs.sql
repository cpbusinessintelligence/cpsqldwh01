SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[branchs] (
		[b_id]                      [int] NOT NULL,
		[b_name]                    [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[b_trackhost]               [varchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[b_trackport]               [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[b_multitrack]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[b_depot]                   [int] NULL,
		[b_shortname]               [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[b_internal_name]           [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[b_emmcode]                 [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[b_in_difot]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[b_description_metro]       [char](128) COLLATE Latin1_General_CI_AS NULL,
		[b_description_country]     [char](128) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__branchs__4E29C30DC40118DF]
		PRIMARY KEY
		CLUSTERED
		([b_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_name__4CB7B64D]
	DEFAULT ('') FOR [b_name]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_track__4DABDA86]
	DEFAULT ('') FOR [b_trackhost]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_track__4E9FFEBF]
	DEFAULT ('') FOR [b_trackport]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_multi__4F9422F8]
	DEFAULT ('N') FOR [b_multitrack]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_depot__50884731]
	DEFAULT (NULL) FOR [b_depot]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_short__517C6B6A]
	DEFAULT ('') FOR [b_shortname]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_inter__52708FA3]
	DEFAULT (NULL) FOR [b_internal_name]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_emmco__5364B3DC]
	DEFAULT (NULL) FOR [b_emmcode]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_in_di__5458D815]
	DEFAULT ('N') FOR [b_in_difot]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_descr__554CFC4E]
	DEFAULT ('') FOR [b_description_metro]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_descr__56412087]
	DEFAULT ('') FOR [b_description_country]
GO
ALTER TABLE [dbo].[branchs]
	WITH NOCHECK
	ADD CONSTRAINT [FK_branchs_depot]
	FOREIGN KEY ([b_depot]) REFERENCES [dbo].[depot] ([d_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[branchs]
	NOCHECK CONSTRAINT [FK_branchs_depot]

GO
CREATE NONCLUSTERED INDEX [idx_Branchs_EmmcodeId]
	ON [dbo].[branchs] ([b_id], [b_emmcode])
	INCLUDE ([b_name], [b_shortname])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[branchs] SET (LOCK_ESCALATION = TABLE)
GO
