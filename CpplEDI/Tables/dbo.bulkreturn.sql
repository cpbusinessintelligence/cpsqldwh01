SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bulkreturn] (
		[br_bulk]            [int] NOT NULL,
		[br_bulkbarcode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[br_internal]        [int] NULL,
		[br_barcode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[br_stamp]           [datetime] NULL,
		[br_weight]          [int] NULL,
		[br_volume]          [decimal](10, 3) NULL
)
GO
ALTER TABLE [dbo].[bulkreturn] SET (LOCK_ESCALATION = TABLE)
GO
