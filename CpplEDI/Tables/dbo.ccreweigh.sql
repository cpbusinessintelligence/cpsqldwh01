SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ccreweigh] (
		[ccr_coupon]         [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[ccr_coupon_id]      [int] NOT NULL,
		[ccr_stamp]          [datetime] NULL,
		[ccr_location]       [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ccr_deadweight]     [float] NULL,
		[ccr_dimension0]     [float] NULL,
		[ccr_dimension1]     [float] NULL,
		[ccr_dimension2]     [float] NULL,
		[ccr_volume]         [float] NULL,
		CONSTRAINT [PK__ccreweig__00108C257C358E97]
		PRIMARY KEY
		CLUSTERED
		([ccr_coupon_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ccreweigh]
	ADD
	CONSTRAINT [DF__ccreweigh__ccr_s__29CDBF1F]
	DEFAULT (NULL) FOR [ccr_stamp]
GO
ALTER TABLE [dbo].[ccreweigh]
	ADD
	CONSTRAINT [DF__ccreweigh__ccr_l__2AC1E358]
	DEFAULT ('') FOR [ccr_location]
GO
ALTER TABLE [dbo].[ccreweigh]
	ADD
	CONSTRAINT [DF__ccreweigh__ccr_d__2BB60791]
	DEFAULT ('0') FOR [ccr_deadweight]
GO
ALTER TABLE [dbo].[ccreweigh]
	ADD
	CONSTRAINT [DF__ccreweigh__ccr_d__2CAA2BCA]
	DEFAULT ('0') FOR [ccr_dimension0]
GO
ALTER TABLE [dbo].[ccreweigh]
	ADD
	CONSTRAINT [DF__ccreweigh__ccr_d__2D9E5003]
	DEFAULT ('0') FOR [ccr_dimension1]
GO
ALTER TABLE [dbo].[ccreweigh]
	ADD
	CONSTRAINT [DF__ccreweigh__ccr_d__2E92743C]
	DEFAULT ('0') FOR [ccr_dimension2]
GO
ALTER TABLE [dbo].[ccreweigh]
	ADD
	CONSTRAINT [DF__ccreweigh__ccr_v__2F869875]
	DEFAULT ('0') FOR [ccr_volume]
GO
ALTER TABLE [dbo].[ccreweigh] SET (LOCK_ESCALATION = TABLE)
GO
