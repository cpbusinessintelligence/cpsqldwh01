SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ccreweigh_load] (
		[ccr_coupon]         [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[ccr_coupon_id]      [int] NOT NULL,
		[ccr_stamp]          [datetime2](7) NULL,
		[ccr_location]       [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ccr_deadweight]     [float] NULL,
		[ccr_dimension0]     [float] NULL,
		[ccr_dimension1]     [float] NULL,
		[ccr_dimension2]     [float] NULL,
		[ccr_volume]         [float] NULL
)
GO
ALTER TABLE [dbo].[ccreweigh_load] SET (LOCK_ESCALATION = TABLE)
GO
