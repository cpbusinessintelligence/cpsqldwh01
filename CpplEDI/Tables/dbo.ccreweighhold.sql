SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ccreweighhold] (
		[crh_coupon]         [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[crh_coupon_id]      [int] NOT NULL,
		[crh_stamp]          [datetime] NULL,
		[crh_checked]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[crh_location]       [char](16) COLLATE Latin1_General_CI_AS NULL,
		[crh_deadweight]     [float] NULL,
		[crh_dimension0]     [float] NULL,
		[crh_dimension1]     [float] NULL,
		[crh_dimension2]     [float] NULL,
		[crh_volume]         [float] NULL,
		CONSTRAINT [PK__ccreweig__E5E69EB85443E4F8]
		PRIMARY KEY
		CLUSTERED
		([crh_coupon])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_c__591D8D32]
	DEFAULT ('') FOR [crh_coupon]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_s__5A11B16B]
	DEFAULT (NULL) FOR [crh_stamp]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_c__5B05D5A4]
	DEFAULT ('N') FOR [crh_checked]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_l__5BF9F9DD]
	DEFAULT ('') FOR [crh_location]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_d__5CEE1E16]
	DEFAULT ('0') FOR [crh_deadweight]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_d__5DE2424F]
	DEFAULT ('0') FOR [crh_dimension0]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_d__5ED66688]
	DEFAULT ('0') FOR [crh_dimension1]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_d__5FCA8AC1]
	DEFAULT ('0') FOR [crh_dimension2]
GO
ALTER TABLE [dbo].[ccreweighhold]
	ADD
	CONSTRAINT [DF__ccreweigh__crh_v__60BEAEFA]
	DEFAULT ('0') FOR [crh_volume]
GO
ALTER TABLE [dbo].[ccreweighhold] SET (LOCK_ESCALATION = TABLE)
GO
