SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdadditional] (
		[ca_consignment]     [int] NOT NULL,
		[ca_atl]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ca_dg]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__cdadditi__64D017294269E002]
		PRIMARY KEY
		CLUSTERED
		([ca_consignment])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[cdadditional]
	ADD
	CONSTRAINT [DF__cdadditio__ca_at__5011E59A]
	DEFAULT ('N') FOR [ca_atl]
GO
ALTER TABLE [dbo].[cdadditional]
	ADD
	CONSTRAINT [DF__cdadditio__ca_dg__510609D3]
	DEFAULT ('N') FOR [ca_dg]
GO
GRANT DELETE
	ON [dbo].[cdadditional]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[cdadditional]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[cdadditional]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[cdadditional]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[cdadditional] SET (LOCK_ESCALATION = TABLE)
GO
