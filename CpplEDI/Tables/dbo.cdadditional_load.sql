SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdadditional_load] (
		[ca_consignment]     [int] NOT NULL,
		[ca_atl]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ca_dg]              [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[cdadditional_load] SET (LOCK_ESCALATION = TABLE)
GO
