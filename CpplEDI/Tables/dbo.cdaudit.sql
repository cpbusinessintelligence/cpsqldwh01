SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdaudit] (
		[ca_id]              [int] NOT NULL,
		[ca_consignment]     [int] NOT NULL,
		[ca_stamp]           [datetime] NULL,
		[ca_account]         [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[ca_info]            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__cdaudit__0875B1F8BF96B376]
		PRIMARY KEY
		CLUSTERED
		([ca_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[cdaudit]
	ADD
	CONSTRAINT [DF__cdaudit__ca_cons__639B1BA5]
	DEFAULT ('0') FOR [ca_consignment]
GO
ALTER TABLE [dbo].[cdaudit]
	ADD
	CONSTRAINT [DF__cdaudit__ca_stam__648F3FDE]
	DEFAULT (NULL) FOR [ca_stamp]
GO
ALTER TABLE [dbo].[cdaudit]
	ADD
	CONSTRAINT [DF__cdaudit__ca_acco__65836417]
	DEFAULT ('') FOR [ca_account]
GO
ALTER TABLE [dbo].[cdaudit]
	ADD
	CONSTRAINT [DF__cdaudit__ca_info__66778850]
	DEFAULT ('') FOR [ca_info]
GO
ALTER TABLE [dbo].[cdaudit]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdaudit_ca_consignment]
	FOREIGN KEY ([ca_consignment]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdaudit]
	NOCHECK CONSTRAINT [FK_cdaudit_ca_consignment]

GO
CREATE NONCLUSTERED INDEX [idx_cdaudit_consignmentstamp]
	ON [dbo].[cdaudit] ([ca_consignment], [ca_stamp])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[cdaudit] SET (LOCK_ESCALATION = TABLE)
GO
