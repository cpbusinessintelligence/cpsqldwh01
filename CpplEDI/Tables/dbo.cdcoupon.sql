SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdcoupon] (
		[cc_id]                         [int] NOT NULL,
		[cc_company_id]                 [int] NOT NULL,
		[cc_consignment]                [int] NOT NULL,
		[cc_coupon]                     [char](32) COLLATE Latin1_General_CI_AS NULL,
		[cc_activity_stamp]             [datetime] NULL,
		[cc_pickup_stamp]               [datetime] NULL,
		[cc_accept_stamp]               [datetime] NULL,
		[cc_indepot_stamp]              [datetime] NULL,
		[cc_transfer_stamp]             [datetime] NULL,
		[cc_deliver_stamp]              [datetime] NULL,
		[cc_failed_stamp]               [datetime] NULL,
		[cc_activity_driver]            [int] NULL,
		[cc_pickup_driver]              [int] NULL,
		[cc_accept_driver]              [int] NULL,
		[cc_indepot_driver]             [int] NULL,
		[cc_transfer_driver]            [int] NULL,
		[cc_transfer_to]                [int] NULL,
		[cc_toagent_driver]             [int] NULL,
		[cc_toagent_stamp]              [datetime] NULL,
		[cc_toagent_name]               [char](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_deliver_driver]             [int] NULL,
		[cc_failed_driver]              [int] NULL,
		[cc_deliver_pod]                [char](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_failed]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_exception_stamp]            [datetime] NULL,
		[cc_exception_code]             [char](8) COLLATE Latin1_General_CI_AS NULL,
		[cc_unit_type]                  [char](8) COLLATE Latin1_General_CI_AS NULL,
		[cc_internal]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_link_coupon]                [char](32) COLLATE Latin1_General_CI_AS NULL,
		[cc_dirty]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_last_status]                [char](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_last_driver]                [int] NULL,
		[cc_last_stamp]                 [datetime] NULL,
		[cc_last_info]                  [char](32) COLLATE Latin1_General_CI_AS NULL,
		[cc_accept_driver_branch]       [int] NULL,
		[cc_activity_driver_branch]     [int] NULL,
		[cc_deliver_driver_branch]      [int] NULL,
		[cc_failed_driver_branch]       [int] NULL,
		[cc_indepot_driver_branch]      [int] NULL,
		[cc_last_driver_branch]         [int] NULL,
		[cc_pickup_driver_branch]       [int] NULL,
		[cc_toagent_driver_branch]      [int] NULL,
		[cc_tranfer_driver_branch]      [int] NULL,
		CONSTRAINT [PK__cdcoupon__9F1E187B699DCD7D]
		PRIMARY KEY
		CLUSTERED
		([cc_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_com__3433F969]
	DEFAULT ('0') FOR [cc_company_id]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_con__35281DA2]
	DEFAULT ('0') FOR [cc_consignment]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_cou__361C41DB]
	DEFAULT ('') FOR [cc_coupon]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_act__37106614]
	DEFAULT (NULL) FOR [cc_activity_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_pic__38048A4D]
	DEFAULT (NULL) FOR [cc_pickup_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_acc__38F8AE86]
	DEFAULT (NULL) FOR [cc_accept_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_ind__39ECD2BF]
	DEFAULT (NULL) FOR [cc_indepot_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_tra__3AE0F6F8]
	DEFAULT (NULL) FOR [cc_transfer_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_del__3BD51B31]
	DEFAULT (NULL) FOR [cc_deliver_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_fai__3CC93F6A]
	DEFAULT (NULL) FOR [cc_failed_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_act__3DBD63A3]
	DEFAULT ('0') FOR [cc_activity_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_pic__3EB187DC]
	DEFAULT ('0') FOR [cc_pickup_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_acc__3FA5AC15]
	DEFAULT ('0') FOR [cc_accept_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_ind__4099D04E]
	DEFAULT ('0') FOR [cc_indepot_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_tra__418DF487]
	DEFAULT ('0') FOR [cc_transfer_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_tra__428218C0]
	DEFAULT ('0') FOR [cc_transfer_to]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_toa__43763CF9]
	DEFAULT ('0') FOR [cc_toagent_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_toa__446A6132]
	DEFAULT (NULL) FOR [cc_toagent_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_toa__455E856B]
	DEFAULT ('') FOR [cc_toagent_name]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_del__4652A9A4]
	DEFAULT ('0') FOR [cc_deliver_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_fai__4746CDDD]
	DEFAULT ('0') FOR [cc_failed_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_del__483AF216]
	DEFAULT ('') FOR [cc_deliver_pod]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_fai__492F164F]
	DEFAULT ('N') FOR [cc_failed]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_exc__4A233A88]
	DEFAULT (NULL) FOR [cc_exception_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_exc__4B175EC1]
	DEFAULT ('') FOR [cc_exception_code]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_uni__4C0B82FA]
	DEFAULT ('') FOR [cc_unit_type]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_int__4CFFA733]
	DEFAULT ('N') FOR [cc_internal]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_lin__4DF3CB6C]
	DEFAULT ('') FOR [cc_link_coupon]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_dir__4EE7EFA5]
	DEFAULT ('Y') FOR [cc_dirty]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_las__4FDC13DE]
	DEFAULT ('') FOR [cc_last_status]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_las__50D03817]
	DEFAULT ('0') FOR [cc_last_driver]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_las__51C45C50]
	DEFAULT (NULL) FOR [cc_last_stamp]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_las__52B88089]
	DEFAULT (NULL) FOR [cc_last_info]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_acc__53ACA4C2]
	DEFAULT (NULL) FOR [cc_accept_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_act__54A0C8FB]
	DEFAULT (NULL) FOR [cc_activity_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_del__5594ED34]
	DEFAULT (NULL) FOR [cc_deliver_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_fai__5689116D]
	DEFAULT (NULL) FOR [cc_failed_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_ind__577D35A6]
	DEFAULT (NULL) FOR [cc_indepot_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_las__587159DF]
	DEFAULT (NULL) FOR [cc_last_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_pic__59657E18]
	DEFAULT (NULL) FOR [cc_pickup_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_toa__5A59A251]
	DEFAULT (NULL) FOR [cc_toagent_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	ADD
	CONSTRAINT [DF__cdcoupon__cc_tra__5B4DC68A]
	DEFAULT (NULL) FOR [cc_tranfer_driver_branch]
GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_activity]
	FOREIGN KEY ([cc_activity_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_activity]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_accept]
	FOREIGN KEY ([cc_accept_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_accept]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_last]
	FOREIGN KEY ([cc_last_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_last]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_toagent]
	FOREIGN KEY ([cc_toagent_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_toagent]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_consignment]
	FOREIGN KEY ([cc_consignment]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_consignment]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_deliver]
	FOREIGN KEY ([cc_deliver_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_deliver]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_failed]
	FOREIGN KEY ([cc_failed_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_failed]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_indepot]
	FOREIGN KEY ([cc_indepot_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_indepot]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_pickup]
	FOREIGN KEY ([cc_pickup_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_pickup]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_driver_transfer]
	FOREIGN KEY ([cc_transfer_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_driver_transfer]

GO
ALTER TABLE [dbo].[cdcoupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdcoupon_company]
	FOREIGN KEY ([cc_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdcoupon]
	NOCHECK CONSTRAINT [FK_cdcoupon_company]

GO
CREATE NONCLUSTERED INDEX [IX_cdcoupon_consignment]
	ON [dbo].[cdcoupon] ([cc_consignment])
	INCLUDE ([cc_id], [cc_company_id], [cc_coupon])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_cdcoupon_coupon]
	ON [dbo].[cdcoupon] ([cc_coupon])
	INCLUDE ([cc_id], [cc_company_id], [cc_consignment])
	ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[cdcoupon]
	TO [DataFactoryUser]
GO
GRANT DELETE
	ON [dbo].[cdcoupon]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[cdcoupon]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[cdcoupon]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[cdcoupon]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[cdcoupon] SET (LOCK_ESCALATION = TABLE)
GO
