SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdcoupon_Incrementalload] (
		[cc_id]                         [int] NULL,
		[cc_company_id]                 [int] NOT NULL,
		[cc_consignment]                [int] NOT NULL,
		[cc_coupon]                     [char](32) COLLATE Latin1_General_CI_AS NULL,
		[cc_activity_stamp]             [datetime2](7) NULL,
		[cc_pickup_stamp]               [datetime2](7) NULL,
		[cc_accept_stamp]               [datetime2](7) NULL,
		[cc_indepot_stamp]              [datetime2](7) NULL,
		[cc_transfer_stamp]             [datetime2](7) NULL,
		[cc_deliver_stamp]              [datetime2](7) NULL,
		[cc_failed_stamp]               [datetime2](7) NULL,
		[cc_activity_driver]            [int] NULL,
		[cc_pickup_driver]              [int] NULL,
		[cc_accept_driver]              [int] NULL,
		[cc_indepot_driver]             [int] NULL,
		[cc_transfer_driver]            [int] NULL,
		[cc_transfer_to]                [int] NULL,
		[cc_toagent_driver]             [int] NULL,
		[cc_toagent_stamp]              [datetime2](7) NULL,
		[cc_toagent_name]               [char](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_deliver_driver]             [int] NULL,
		[cc_failed_driver]              [int] NULL,
		[cc_deliver_pod]                [char](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_failed]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_exception_stamp]            [datetime2](7) NULL,
		[cc_exception_code]             [char](8) COLLATE Latin1_General_CI_AS NULL,
		[cc_unit_type]                  [char](8) COLLATE Latin1_General_CI_AS NULL,
		[cc_internal]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_link_coupon]                [char](32) COLLATE Latin1_General_CI_AS NULL,
		[cc_dirty]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_last_status]                [char](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_last_driver]                [int] NULL,
		[cc_last_stamp]                 [datetime2](7) NULL,
		[cc_last_info]                  [char](32) COLLATE Latin1_General_CI_AS NULL,
		[cc_accept_driver_branch]       [int] NULL,
		[cc_activity_driver_branch]     [int] NULL,
		[cc_deliver_driver_branch]      [int] NULL,
		[cc_failed_driver_branch]       [int] NULL,
		[cc_indepot_driver_branch]      [int] NULL,
		[cc_last_driver_branch]         [int] NULL,
		[cc_pickup_driver_branch]       [int] NULL,
		[cc_toagent_driver_branch]      [int] NULL,
		[cc_tranfer_driver_branch]      [int] NULL
)
GO
ALTER TABLE [dbo].[cdcoupon_Incrementalload] SET (LOCK_ESCALATION = TABLE)
GO
