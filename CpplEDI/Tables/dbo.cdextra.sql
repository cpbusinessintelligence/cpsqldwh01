SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdextra] (
		[ce_consignment]     [int] NULL,
		[ce_key]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ce_value]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]        [datetime] NULL,
		[CreatedBy]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDate]        [datetime] NULL,
		[UpdatedBy]          [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[cdextra]
	ADD
	CONSTRAINT [DF__cdextra__Created__5F6232C2]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[cdextra]
	ADD
	CONSTRAINT [DF__cdextra__Created__605656FB]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[cdextra]
	ADD
	CONSTRAINT [DF__cdextra__Updated__614A7B34]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[cdextra]
	ADD
	CONSTRAINT [DF__cdextra__Updated__623E9F6D]
	DEFAULT ('Admin') FOR [UpdatedBy]
GO
GRANT ALTER
	ON [dbo].[cdextra]
	TO [DataFactoryUser]
GO
GRANT DELETE
	ON [dbo].[cdextra]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[cdextra]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[cdextra]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[cdextra]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[cdextra] SET (LOCK_ESCALATION = TABLE)
GO
