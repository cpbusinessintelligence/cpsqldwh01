SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdinternal] (
		[ci_id]              [int] NOT NULL,
		[ci_company_id]      [int] NOT NULL,
		[ci_consignment]     [int] NOT NULL,
		[ci_parent]          [int] NOT NULL,
		[ci_coupon]          [char](32) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__cdintern__4FE5E8DC587A507D]
		PRIMARY KEY
		CLUSTERED
		([ci_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[cdinternal]
	ADD
	CONSTRAINT [DF__cdinterna__ci_co__6953F4FB]
	DEFAULT ('0') FOR [ci_company_id]
GO
ALTER TABLE [dbo].[cdinternal]
	ADD
	CONSTRAINT [DF__cdinterna__ci_co__6A481934]
	DEFAULT ('0') FOR [ci_consignment]
GO
ALTER TABLE [dbo].[cdinternal]
	ADD
	CONSTRAINT [DF__cdinterna__ci_pa__6B3C3D6D]
	DEFAULT ('0') FOR [ci_parent]
GO
ALTER TABLE [dbo].[cdinternal]
	ADD
	CONSTRAINT [DF__cdinterna__ci_co__6C3061A6]
	DEFAULT ('') FOR [ci_coupon]
GO
ALTER TABLE [dbo].[cdinternal]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdinternal_ci_consignment]
	FOREIGN KEY ([ci_consignment]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdinternal]
	NOCHECK CONSTRAINT [FK_cdinternal_ci_consignment]

GO
ALTER TABLE [dbo].[cdinternal]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdinternal_ci_company]
	FOREIGN KEY ([ci_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdinternal]
	NOCHECK CONSTRAINT [FK_cdinternal_ci_company]

GO
ALTER TABLE [dbo].[cdinternal] SET (LOCK_ESCALATION = TABLE)
GO
