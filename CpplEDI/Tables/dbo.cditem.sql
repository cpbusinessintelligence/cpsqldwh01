SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cditem] (
		[ci_id]                 [int] NULL,
		[ci_consignment_id]     [int] NULL,
		[ci_order]              [int] NULL,
		[ci_count]              [int] NULL,
		[ci_per]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ci_description]        [char](64) COLLATE Latin1_General_CI_AS NULL,
		[ci_deadweight]         [float] NULL,
		[ci_x]                  [int] NULL,
		[ci_y]                  [int] NULL,
		[ci_z]                  [int] NULL,
		[ci_volume]             [float] NULL
)
GO
GRANT ALTER
	ON [dbo].[cditem]
	TO [DataFactoryUser]
GO
GRANT DELETE
	ON [dbo].[cditem]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[cditem]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[cditem]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[cditem]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[cditem] SET (LOCK_ESCALATION = TABLE)
GO
