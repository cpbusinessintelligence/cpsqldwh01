SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdref] (
		[cr_id]              [int] NOT NULL,
		[cr_company_id]      [int] NOT NULL,
		[cr_consignment]     [int] NOT NULL,
		[cr_reference]       [char](32) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__cdref__AB69D8CF52040E59]
		PRIMARY KEY
		CLUSTERED
		([cr_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[cdref]
	ADD
	CONSTRAINT [DF__cdref__cr_compan__5BEF2A98]
	DEFAULT ('0') FOR [cr_company_id]
GO
ALTER TABLE [dbo].[cdref]
	ADD
	CONSTRAINT [DF__cdref__cr_consig__5CE34ED1]
	DEFAULT ('0') FOR [cr_consignment]
GO
ALTER TABLE [dbo].[cdref]
	ADD
	CONSTRAINT [DF__cdref__cr_refere__5DD7730A]
	DEFAULT ('') FOR [cr_reference]
GO
ALTER TABLE [dbo].[cdref]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdref_cr_consignment]
	FOREIGN KEY ([cr_consignment]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdref]
	NOCHECK CONSTRAINT [FK_cdref_cr_consignment]

GO
ALTER TABLE [dbo].[cdref]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdref_company]
	FOREIGN KEY ([cr_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdref]
	NOCHECK CONSTRAINT [FK_cdref_company]

GO
CREATE NONCLUSTERED INDEX [idx_cdref_consignment]
	ON [dbo].[cdref] ([cr_consignment])
	INCLUDE ([cr_reference])
	ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[cdref]
	TO [DataFactoryUser]
GO
GRANT DELETE
	ON [dbo].[cdref]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[cdref]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[cdref]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[cdref]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[cdref] SET (LOCK_ESCALATION = TABLE)
GO
