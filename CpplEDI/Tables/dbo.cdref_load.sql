SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdref_load] (
		[cr_id]              [int] NULL,
		[cr_company_id]      [int] NOT NULL,
		[cr_consignment]     [int] NOT NULL,
		[cr_reference]       [char](32) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[cdref_load] SET (LOCK_ESCALATION = TABLE)
GO
