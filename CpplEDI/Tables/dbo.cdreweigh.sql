SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[cdreweigh] (
		[cr_consignment]      [int] NOT NULL,
		[cr_stamp]            [datetime] NULL,
		[cr_deadweight]       [float] NULL,
		[cr_volume]           [float] NULL,
		[cr_chargeweight]     [float] NULL,
		CONSTRAINT [PK__cdreweig__C64341AD80A46928]
		PRIMARY KEY
		CLUSTERED
		([cr_consignment])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[cdreweigh]
	ADD
	CONSTRAINT [DF__cdreweigh__cr_st__3CE09393]
	DEFAULT (NULL) FOR [cr_stamp]
GO
ALTER TABLE [dbo].[cdreweigh]
	ADD
	CONSTRAINT [DF__cdreweigh__cr_de__3DD4B7CC]
	DEFAULT ('0') FOR [cr_deadweight]
GO
ALTER TABLE [dbo].[cdreweigh]
	ADD
	CONSTRAINT [DF__cdreweigh__cr_vo__3EC8DC05]
	DEFAULT ('0') FOR [cr_volume]
GO
ALTER TABLE [dbo].[cdreweigh]
	ADD
	CONSTRAINT [DF__cdreweigh__cr_ch__3FBD003E]
	DEFAULT ('0') FOR [cr_chargeweight]
GO
ALTER TABLE [dbo].[cdreweigh] SET (LOCK_ESCALATION = TABLE)
GO
