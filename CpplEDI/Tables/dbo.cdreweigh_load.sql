SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[cdreweigh_load] (
		[cr_consignment]      [int] NOT NULL,
		[cr_stamp]            [datetime2](7) NULL,
		[cr_deadweight]       [float] NULL,
		[cr_volume]           [float] NULL,
		[cr_chargeweight]     [float] NULL
)
GO
ALTER TABLE [dbo].[cdreweigh_load] SET (LOCK_ESCALATION = TABLE)
GO
