SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cdsundry] (
		[cn_consignment]          [int] NOT NULL,
		[cn_status]               [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[cn_cost]                 [float] NULL,
		[cn_note]                 [text] COLLATE Latin1_General_CI_AS NULL,
		[cn_stamp]                [datetime] NULL,
		[cn_notified]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cn_delcared_value]       [float] NULL,
		[cn_insurance]            [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[cn_dangerous_goods]      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cn_email]                [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[cn_validation_info]      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[cn_releaseasn]           [char](25) COLLATE Latin1_General_CI_AS NULL,
		[cn_notbefore]            [smalldatetime] NULL,
		[cn_notafter]             [smalldatetime] NULL,
		[cn_ezyfreight_price]     [float] NULL,
		CONSTRAINT [PK__cdsundry__53E00ADAA33A4616]
		PRIMARY KEY
		CLUSTERED
		([cn_consignment])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_con__571A7566]
	DEFAULT ('0') FOR [cn_consignment]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_sta__580E999F]
	DEFAULT ('') FOR [cn_status]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_cos__5902BDD8]
	DEFAULT (NULL) FOR [cn_cost]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_sta__59F6E211]
	DEFAULT (NULL) FOR [cn_stamp]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_not__5AEB064A]
	DEFAULT ('N') FOR [cn_notified]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_del__5BDF2A83]
	DEFAULT (NULL) FOR [cn_delcared_value]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_ins__5CD34EBC]
	DEFAULT ('') FOR [cn_insurance]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_dan__5DC772F5]
	DEFAULT ('N') FOR [cn_dangerous_goods]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_ema__5EBB972E]
	DEFAULT ('') FOR [cn_email]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_val__5FAFBB67]
	DEFAULT ('') FOR [cn_validation_info]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_rel__60A3DFA0]
	DEFAULT ('') FOR [cn_releaseasn]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_not__619803D9]
	DEFAULT (NULL) FOR [cn_notbefore]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_not__628C2812]
	DEFAULT (NULL) FOR [cn_notafter]
GO
ALTER TABLE [dbo].[cdsundry]
	ADD
	CONSTRAINT [DF__cdsundry__cn_ezy__63804C4B]
	DEFAULT ('0') FOR [cn_ezyfreight_price]
GO
ALTER TABLE [dbo].[cdsundry]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cdsundry_cn_consignment]
	FOREIGN KEY ([cn_consignment]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cdsundry]
	NOCHECK CONSTRAINT [FK_cdsundry_cn_consignment]

GO
ALTER TABLE [dbo].[cdsundry] SET (LOCK_ESCALATION = TABLE)
GO
