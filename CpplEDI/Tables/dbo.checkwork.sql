SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[checkwork] (
		[zoning_id]          [int] NOT NULL,
		[postcode]           [int] NULL,
		[suburb]             [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[state]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[agent_id]           [int] NOT NULL,
		[zoneid]             [int] NULL,
		[subzoneid]          [int] NULL,
		[branch]             [int] NULL,
		[depot]              [int] NULL,
		[cppldriver]         [int] NULL,
		[ai_id]              [int] NOT NULL,
		[ai_eta_zone]        [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_code]       [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[a_imported]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_nsw]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_act]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_vic]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_tas]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_qld]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_sa]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_nt]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_wa]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ai_sort_suffix]     [char](8) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__checkwor__0372DAEE2CDB05AA]
		PRIMARY KEY
		CLUSTERED
		([ai_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__00375A53]
	DEFAULT ('') FOR [ai_sort_sa]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__012B7E8C]
	DEFAULT ('') FOR [ai_sort_nt]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__021FA2C5]
	DEFAULT ('') FOR [ai_sort_wa]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__0313C6FE]
	DEFAULT ('') FOR [ai_sort_suffix]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__zonin__6F0CCE51]
	DEFAULT ('0') FOR [zoning_id]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__postc__7000F28A]
	DEFAULT (NULL) FOR [postcode]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__subur__70F516C3]
	DEFAULT (NULL) FOR [suburb]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__state__71E93AFC]
	DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__agent__72DD5F35]
	DEFAULT ('0') FOR [agent_id]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__zonei__73D1836E]
	DEFAULT ('0') FOR [zoneid]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__subzo__74C5A7A7]
	DEFAULT ('0') FOR [subzoneid]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__branc__75B9CBE0]
	DEFAULT (NULL) FOR [branch]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__depot__76ADF019]
	DEFAULT (NULL) FOR [depot]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__cppld__77A21452]
	DEFAULT ('0') FOR [cppldriver]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_et__7896388B]
	DEFAULT ('') FOR [ai_eta_zone]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__798A5CC4]
	DEFAULT (NULL) FOR [ai_sort_code]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__a_imp__7A7E80FD]
	DEFAULT ('N') FOR [a_imported]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__7B72A536]
	DEFAULT ('') FOR [ai_sort_nsw]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__7C66C96F]
	DEFAULT ('') FOR [ai_sort_act]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__7D5AEDA8]
	DEFAULT ('') FOR [ai_sort_vic]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__7E4F11E1]
	DEFAULT ('') FOR [ai_sort_tas]
GO
ALTER TABLE [dbo].[checkwork]
	ADD
	CONSTRAINT [DF__checkwork__ai_so__7F43361A]
	DEFAULT ('') FOR [ai_sort_qld]
GO
ALTER TABLE [dbo].[checkwork] SET (LOCK_ESCALATION = TABLE)
GO
