SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chrisco] (
		[ch_id]               [int] NOT NULL,
		[ch_stamp]            [datetime] NULL,
		[ch_barcode]          [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[ch_code]             [int] NULL,
		[ch_podstamp]         [datetime] NULL,
		[ch_podname]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ch_location]         [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[ch_processstamp]     [datetime] NULL,
		[ch_success]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__chrisco__5130BD17AA18BB16]
		PRIMARY KEY
		CLUSTERED
		([ch_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_stam__05F033A9]
	DEFAULT (NULL) FOR [ch_stamp]
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_barc__06E457E2]
	DEFAULT ('') FOR [ch_barcode]
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_code__07D87C1B]
	DEFAULT ('1') FOR [ch_code]
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_pods__08CCA054]
	DEFAULT (NULL) FOR [ch_podstamp]
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_podn__09C0C48D]
	DEFAULT ('') FOR [ch_podname]
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_loca__0AB4E8C6]
	DEFAULT ('SYD') FOR [ch_location]
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_proc__0BA90CFF]
	DEFAULT (NULL) FOR [ch_processstamp]
GO
ALTER TABLE [dbo].[chrisco]
	ADD
	CONSTRAINT [DF__chrisco__ch_succ__0C9D3138]
	DEFAULT ('N') FOR [ch_success]
GO
ALTER TABLE [dbo].[chrisco] SET (LOCK_ESCALATION = TABLE)
GO
