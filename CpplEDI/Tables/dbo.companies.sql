SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companies] (
		[c_id]                                  [int] NOT NULL,
		[c_code]                                [varchar](8) COLLATE Latin1_General_CI_AS NOT NULL,
		[c_name]                                [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[c_zoning_id]                           [int] NOT NULL,
		[c_pricegroup_id]                       [int] NOT NULL,
		[c_class]                               [int] NOT NULL,
		[c_returns]                             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_return_connote_format]               [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[c_return_tracking_format]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[c_return_manifest_format]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[c_return_connote_id]                   [int] NULL,
		[c_return_tracking_id]                  [int] NULL,
		[c_return_manifest_id]                  [int] NULL,
		[c_default_addr0]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_default_addr1]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_default_addr2]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_default_addr3]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_default_suburb]                      [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[c_default_postcode]                    [int] NULL,
		[c_podrequired]                         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_active]                              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_update_style]                        [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[c_autoquery]                           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_cycle]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_trigger]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_group]                       [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[c_abn]                                 [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[c_gstreg]                              [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[c_asic]                                [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[c_salesrep_id]                         [int] NULL,
		[c_billing_name]                        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_addr0]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_addr1]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_addr2]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_addr3]                       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_suburb]                      [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[c_billing_postcode]                    [int] NULL,
		[c_billing_email]                       [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[c_default_paystyle]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_default_bank]                        [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[c_default_branch]                      [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[c_default_drawer]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[c_cosmos_transfer]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_auth_owner]                          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[c_auth_group]                          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[c_validation_id]                       [int] NULL,
		[c_ezyfreight_pricecode]                [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_volume]                   [float] NULL,
		[c_ezyfreight_weight]                   [int] NULL,
		[c_ezyfreight_pieces]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_zonemap]                  [int] NULL,
		[c_ezyfreight_connote_format]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_connote_id]               [int] NULL,
		[c_ezyfreight_consolidate]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_insurance]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_insurance_choices]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_header]                   [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_no_email]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_atl]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_returns_pricecode]                   [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[c_last_check]                          [int] NULL,
		[c_check_frequency]                     [int] NULL,
		[c_cosmos_code]                         [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[c_update_format]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_update_frequency]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_update_ftp_host]                     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[c_update_ftp_port]                     [int] NULL,
		[c_update_ftp_user]                     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[c_update_ftp_password]                 [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[c_update_ftp_directory]                [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[c_update_email]                        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[c_update_filename_format]              [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[c_update_id]                           [int] NULL,
		[c_update_passive]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_export_special]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_cubic_factor]                        [float] NULL,
		[c_difot_failedok]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_application_details]                 [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[c_notes]                               [text] COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_default_instructions]     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_no_auto_release]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_ezyfreight_cube_exempt]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_daily_deliveries_email]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[c_sms_on_pickup]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_alternate_agents]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_items_and_coupons]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_no_oversize_warning]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_obd_notify_mail]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_returns_available]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[c_booking_fixed_time]                  [int] NULL,
		[c_booking_from_first]                  [int] NULL,
		[c_booking_from_last]                   [int] NULL,
		[c_booking_auto_rebook]                 [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[c_booking_consolidated]                [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[c_labelless]                           [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__companie__213EE7742B10378F]
		PRIMARY KEY
		CLUSTERED
		([c_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__00D678D1]
	DEFAULT ('Q') FOR [c_default_paystyle]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__01CA9D0A]
	DEFAULT (NULL) FOR [c_default_bank]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__02BEC143]
	DEFAULT (NULL) FOR [c_default_branch]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__03B2E57C]
	DEFAULT (NULL) FOR [c_default_drawer]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_cos__04A709B5]
	DEFAULT ('N') FOR [c_cosmos_transfer]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_aut__059B2DEE]
	DEFAULT ('') FOR [c_auth_owner]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_aut__068F5227]
	DEFAULT ('') FOR [c_auth_group]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_val__07837660]
	DEFAULT (NULL) FOR [c_validation_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__08779A99]
	DEFAULT ('48') FOR [c_ezyfreight_pricecode]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__096BBED2]
	DEFAULT ('0') FOR [c_ezyfreight_volume]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__0A5FE30B]
	DEFAULT ('0') FOR [c_ezyfreight_weight]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__0B540744]
	DEFAULT ('N') FOR [c_ezyfreight_pieces]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__0C482B7D]
	DEFAULT ('0') FOR [c_ezyfreight_zonemap]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__0D3C4FB6]
	DEFAULT ('CP{ID}E%07d') FOR [c_ezyfreight_connote_format]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__0E3073EF]
	DEFAULT ('1') FOR [c_ezyfreight_connote_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__0F249828]
	DEFAULT ('N') FOR [c_ezyfreight_consolidate]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__1018BC61]
	DEFAULT ('0') FOR [c_ezyfreight_insurance]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__110CE09A]
	DEFAULT ('N') FOR [c_ezyfreight_insurance_choices]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__120104D3]
	DEFAULT ('N') FOR [c_ezyfreight_no_email]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__12F5290C]
	DEFAULT ('N') FOR [c_ezyfreight_atl]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__13E94D45]
	DEFAULT ('48') FOR [c_returns_pricecode]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_las__14DD717E]
	DEFAULT ('0') FOR [c_last_check]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_che__15D195B7]
	DEFAULT ('30') FOR [c_check_frequency]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_cos__16C5B9F0]
	DEFAULT ('') FOR [c_cosmos_code]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__17B9DE29]
	DEFAULT ('N') FOR [c_update_format]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__18AE0262]
	DEFAULT ('D') FOR [c_update_frequency]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__19A2269B]
	DEFAULT ('') FOR [c_update_ftp_host]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__1A964AD4]
	DEFAULT ('21') FOR [c_update_ftp_port]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__1B8A6F0D]
	DEFAULT ('') FOR [c_update_ftp_user]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__1C7E9346]
	DEFAULT ('') FOR [c_update_ftp_password]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__1D72B77F]
	DEFAULT ('/') FOR [c_update_ftp_directory]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__1E66DBB8]
	DEFAULT ('') FOR [c_update_email]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__1F5AFFF1]
	DEFAULT ('CPPL%05d.csv') FOR [c_update_filename_format]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__204F242A]
	DEFAULT ('1') FOR [c_update_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__21434863]
	DEFAULT ('N') FOR [c_update_passive]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_exp__22376C9C]
	DEFAULT ('N') FOR [c_export_special]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_cub__232B90D5]
	DEFAULT ('0') FOR [c_cubic_factor]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_dif__241FB50E]
	DEFAULT ('N') FOR [c_difot_failedok]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_app__2513D947]
	DEFAULT ('') FOR [c_application_details]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__2607FD80]
	DEFAULT ('') FOR [c_ezyfreight_default_instructions]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__26FC21B9]
	DEFAULT ('N') FOR [c_ezyfreight_no_auto_release]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ezy__27F045F2]
	DEFAULT ('N') FOR [c_ezyfreight_cube_exempt]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_dai__28E46A2B]
	DEFAULT ('') FOR [c_daily_deliveries_email]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_sms__29D88E64]
	DEFAULT ('N') FOR [c_sms_on_pickup]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_alt__2ACCB29D]
	DEFAULT ('N') FOR [c_alternate_agents]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ite__2BC0D6D6]
	DEFAULT ('N') FOR [c_items_and_coupons]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_no___2CB4FB0F]
	DEFAULT ('N') FOR [c_no_oversize_warning]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_obd__2DA91F48]
	DEFAULT ('N') FOR [c_obd_notify_mail]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__2E9D4381]
	DEFAULT ('N') FOR [c_returns_available]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_boo__2F9167BA]
	DEFAULT ('0') FOR [c_booking_fixed_time]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_boo__30858BF3]
	DEFAULT ('0') FOR [c_booking_from_first]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_boo__3179B02C]
	DEFAULT ('0') FOR [c_booking_from_last]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_boo__326DD465]
	DEFAULT ('N') FOR [c_booking_auto_rebook]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_boo__3361F89E]
	DEFAULT ('N') FOR [c_booking_consolidated]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_lab__34561CD7]
	DEFAULT ('N') FOR [c_labelless]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_cod__5D8D3C94]
	DEFAULT ('') FOR [c_code]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_nam__5E8160CD]
	DEFAULT (NULL) FOR [c_name]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_zon__5F758506]
	DEFAULT ('0') FOR [c_zoning_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_pri__6069A93F]
	DEFAULT ('0') FOR [c_pricegroup_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_cla__615DCD78]
	DEFAULT ('1') FOR [c_class]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__6251F1B1]
	DEFAULT ('N') FOR [c_returns]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__634615EA]
	DEFAULT ('') FOR [c_return_connote_format]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__643A3A23]
	DEFAULT ('') FOR [c_return_tracking_format]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__652E5E5C]
	DEFAULT ('') FOR [c_return_manifest_format]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__66228295]
	DEFAULT (NULL) FOR [c_return_connote_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__6716A6CE]
	DEFAULT (NULL) FOR [c_return_tracking_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_ret__680ACB07]
	DEFAULT ('1') FOR [c_return_manifest_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__68FEEF40]
	DEFAULT (NULL) FOR [c_default_addr0]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__69F31379]
	DEFAULT (NULL) FOR [c_default_addr1]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__6AE737B2]
	DEFAULT (NULL) FOR [c_default_addr2]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__6BDB5BEB]
	DEFAULT (NULL) FOR [c_default_addr3]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__6CCF8024]
	DEFAULT (NULL) FOR [c_default_suburb]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_def__6DC3A45D]
	DEFAULT (NULL) FOR [c_default_postcode]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_pod__6EB7C896]
	DEFAULT ('Y') FOR [c_podrequired]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_act__6FABECCF]
	DEFAULT ('N') FOR [c_active]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_upd__70A01108]
	DEFAULT (NULL) FOR [c_update_style]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_aut__71943541]
	DEFAULT ('N') FOR [c_autoquery]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7288597A]
	DEFAULT ('W') FOR [c_billing_cycle]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__737C7DB3]
	DEFAULT ('D') FOR [c_billing_trigger]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7470A1EC]
	DEFAULT (NULL) FOR [c_billing_group]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_abn__7564C625]
	DEFAULT (NULL) FOR [c_abn]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_gst__7658EA5E]
	DEFAULT (NULL) FOR [c_gstreg]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_asi__774D0E97]
	DEFAULT (NULL) FOR [c_asic]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_sal__784132D0]
	DEFAULT (NULL) FOR [c_salesrep_id]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__79355709]
	DEFAULT (NULL) FOR [c_billing_name]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7A297B42]
	DEFAULT (NULL) FOR [c_billing_addr0]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7B1D9F7B]
	DEFAULT (NULL) FOR [c_billing_addr1]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7C11C3B4]
	DEFAULT (NULL) FOR [c_billing_addr2]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7D05E7ED]
	DEFAULT (NULL) FOR [c_billing_addr3]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7DFA0C26]
	DEFAULT (NULL) FOR [c_billing_suburb]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7EEE305F]
	DEFAULT (NULL) FOR [c_billing_postcode]
GO
ALTER TABLE [dbo].[companies]
	ADD
	CONSTRAINT [DF__companies__c_bil__7FE25498]
	DEFAULT (NULL) FOR [c_billing_email]
GO
ALTER TABLE [dbo].[companies]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companies_class]
	FOREIGN KEY ([c_class]) REFERENCES [dbo].[companyclass] ([cc_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companies]
	NOCHECK CONSTRAINT [FK_companies_class]

GO
ALTER TABLE [dbo].[companies]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companies_pricegroup]
	FOREIGN KEY ([c_pricegroup_id]) REFERENCES [dbo].[pricegroup] ([pg_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companies]
	NOCHECK CONSTRAINT [FK_companies_pricegroup]

GO
ALTER TABLE [dbo].[companies]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companies_manifest]
	FOREIGN KEY ([c_zoning_id]) REFERENCES [dbo].[zoning] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companies]
	NOCHECK CONSTRAINT [FK_companies_manifest]

GO
ALTER TABLE [dbo].[companies]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companies_salesrep]
	FOREIGN KEY ([c_salesrep_id]) REFERENCES [dbo].[salesrep] ([sr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companies]
	NOCHECK CONSTRAINT [FK_companies_salesrep]

GO
CREATE NONCLUSTERED INDEX [idx_companies_cclass]
	ON [dbo].[companies] ([c_id], [c_class])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[companies] SET (LOCK_ESCALATION = TABLE)
GO
