SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companyaccount] (
		[ca_company_id]     [int] NOT NULL,
		[ca_order]          [int] NOT NULL,
		[ca_match]          [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[ca_account]        [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[companyaccount]
	ADD
	CONSTRAINT [DF__companyac__ca_co__363E6549]
	DEFAULT ('0') FOR [ca_company_id]
GO
ALTER TABLE [dbo].[companyaccount]
	ADD
	CONSTRAINT [DF__companyac__ca_or__37328982]
	DEFAULT ('0') FOR [ca_order]
GO
ALTER TABLE [dbo].[companyaccount]
	ADD
	CONSTRAINT [DF__companyac__ca_ma__3826ADBB]
	DEFAULT ('') FOR [ca_match]
GO
ALTER TABLE [dbo].[companyaccount]
	ADD
	CONSTRAINT [DF__companyac__ca_ac__391AD1F4]
	DEFAULT ('') FOR [ca_account]
GO
ALTER TABLE [dbo].[companyaccount]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companyaccount_company]
	FOREIGN KEY ([ca_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companyaccount]
	NOCHECK CONSTRAINT [FK_companyaccount_company]

GO
ALTER TABLE [dbo].[companyaccount] SET (LOCK_ESCALATION = TABLE)
GO
