SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[companyagentoverride] (
		[ao_company_id]     [int] NOT NULL,
		[ao_agent_id]       [int] NOT NULL
)
GO
ALTER TABLE [dbo].[companyagentoverride]
	ADD
	CONSTRAINT [DF__companyag__ao_co__3B031A66]
	DEFAULT ('0') FOR [ao_company_id]
GO
ALTER TABLE [dbo].[companyagentoverride]
	ADD
	CONSTRAINT [DF__companyag__ao_ag__3BF73E9F]
	DEFAULT ('0') FOR [ao_agent_id]
GO
ALTER TABLE [dbo].[companyagentoverride]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companyagentoverride_company]
	FOREIGN KEY ([ao_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companyagentoverride]
	NOCHECK CONSTRAINT [FK_companyagentoverride_company]

GO
ALTER TABLE [dbo].[companyagentoverride]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companyagentoverride_agent]
	FOREIGN KEY ([ao_agent_id]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companyagentoverride]
	NOCHECK CONSTRAINT [FK_companyagentoverride_agent]

GO
ALTER TABLE [dbo].[companyagentoverride] SET (LOCK_ESCALATION = TABLE)
GO
