SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companyclass] (
		[cc_id]                         [int] NOT NULL,
		[cc_shortname]                  [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[cc_longname]                   [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[cc_pickup_portion]             [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[cc_delivery_portion]           [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[cc_export_prefix]              [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_export_date]                [smalldatetime] NULL,
		[cc_export_id]                  [int] NULL,
		[cc_export_required]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_export_ftp_host]            [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_export_ftp_user]            [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_export_ftp_password]        [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_export_ftp_directory]       [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_ogm]                        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_direct_book_ezyfreight]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_export2_ftp_host]           [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_export2_ftp_user]           [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_export2_ftp_password]       [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_export2_ftp_directory]      [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[cc_export2_required]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_export2_date]               [smalldatetime] NULL,
		[cc_export2_id]                 [int] NULL,
		[cc_export2_prefix]             [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cc_export2_daily]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_pay_prefix]                 [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[cc_base_prefix]                [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[cc_label_name]                 [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[cc_image_file]                 [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[cc_invoice_reprint]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cc_book_pickups]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__companyc__9F1E187B979B6C2E]
		PRIMARY KEY
		CLUSTERED
		([cc_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_sh__0F799DE3]
	DEFAULT ('') FOR [cc_shortname]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_lo__106DC21C]
	DEFAULT ('') FOR [cc_longname]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_pi__1161E655]
	DEFAULT ('') FOR [cc_pickup_portion]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_de__12560A8E]
	DEFAULT ('') FOR [cc_delivery_portion]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__134A2EC7]
	DEFAULT ('') FOR [cc_export_prefix]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__143E5300]
	DEFAULT (NULL) FOR [cc_export_date]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__15327739]
	DEFAULT ('1') FOR [cc_export_id]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__16269B72]
	DEFAULT ('N') FOR [cc_export_required]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__171ABFAB]
	DEFAULT (NULL) FOR [cc_export_ftp_host]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__180EE3E4]
	DEFAULT (NULL) FOR [cc_export_ftp_user]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__1903081D]
	DEFAULT (NULL) FOR [cc_export_ftp_password]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__19F72C56]
	DEFAULT (NULL) FOR [cc_export_ftp_directory]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_og__1AEB508F]
	DEFAULT ('N') FOR [cc_ogm]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_di__1BDF74C8]
	DEFAULT ('N') FOR [cc_direct_book_ezyfreight]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__1CD39901]
	DEFAULT (NULL) FOR [cc_export2_ftp_host]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__1DC7BD3A]
	DEFAULT (NULL) FOR [cc_export2_ftp_user]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__1EBBE173]
	DEFAULT (NULL) FOR [cc_export2_ftp_password]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__1FB005AC]
	DEFAULT (NULL) FOR [cc_export2_ftp_directory]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__20A429E5]
	DEFAULT ('N') FOR [cc_export2_required]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__21984E1E]
	DEFAULT (NULL) FOR [cc_export2_date]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__228C7257]
	DEFAULT ('1') FOR [cc_export2_id]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__23809690]
	DEFAULT ('') FOR [cc_export2_prefix]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ex__2474BAC9]
	DEFAULT ('N') FOR [cc_export2_daily]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_pa__2568DF02]
	DEFAULT ('') FOR [cc_pay_prefix]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_ba__265D033B]
	DEFAULT ('') FOR [cc_base_prefix]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_la__27512774]
	DEFAULT ('') FOR [cc_label_name]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_im__28454BAD]
	DEFAULT ('image-POD.ps') FOR [cc_image_file]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_in__29396FE6]
	DEFAULT ('N') FOR [cc_invoice_reprint]
GO
ALTER TABLE [dbo].[companyclass]
	ADD
	CONSTRAINT [DF__companycl__cc_bo__2A2D941F]
	DEFAULT ('N') FOR [cc_book_pickups]
GO
CREATE NONCLUSTERED INDEX [idx_companyclass_shortname]
	ON [dbo].[companyclass] ([cc_id], [cc_shortname])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[companyclass] SET (LOCK_ESCALATION = TABLE)
GO
