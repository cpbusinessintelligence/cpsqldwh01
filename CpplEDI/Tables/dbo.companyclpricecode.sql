SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companyclpricecode] (
		[clp_company_id]     [int] NOT NULL,
		[clp_order]          [int] NOT NULL,
		[clp_pricecode]      [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[companyclpricecode]
	ADD
	CONSTRAINT [DF__companycl__clp_c__2C15DC91]
	DEFAULT ('0') FOR [clp_company_id]
GO
ALTER TABLE [dbo].[companyclpricecode]
	ADD
	CONSTRAINT [DF__companycl__clp_o__2D0A00CA]
	DEFAULT ('0') FOR [clp_order]
GO
ALTER TABLE [dbo].[companyclpricecode]
	ADD
	CONSTRAINT [DF__companycl__clp_p__2DFE2503]
	DEFAULT ('') FOR [clp_pricecode]
GO
ALTER TABLE [dbo].[companyclpricecode] SET (LOCK_ESCALATION = TABLE)
GO
