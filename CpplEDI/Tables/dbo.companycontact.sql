SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companycontact] (
		[cc_company_id]     [int] NOT NULL,
		[cc_order]          [int] NOT NULL,
		[cc_style]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cc_name]           [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cc_phone]          [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[cc_email]          [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[companycontact]
	ADD
	CONSTRAINT [DF__companyco__cc_co__2FE66D75]
	DEFAULT ('0') FOR [cc_company_id]
GO
ALTER TABLE [dbo].[companycontact]
	ADD
	CONSTRAINT [DF__companyco__cc_or__30DA91AE]
	DEFAULT ('0') FOR [cc_order]
GO
ALTER TABLE [dbo].[companycontact]
	ADD
	CONSTRAINT [DF__companyco__cc_st__31CEB5E7]
	DEFAULT (NULL) FOR [cc_style]
GO
ALTER TABLE [dbo].[companycontact]
	ADD
	CONSTRAINT [DF__companyco__cc_na__32C2DA20]
	DEFAULT (NULL) FOR [cc_name]
GO
ALTER TABLE [dbo].[companycontact]
	ADD
	CONSTRAINT [DF__companyco__cc_ph__33B6FE59]
	DEFAULT (NULL) FOR [cc_phone]
GO
ALTER TABLE [dbo].[companycontact]
	ADD
	CONSTRAINT [DF__companyco__cc_em__34AB2292]
	DEFAULT (NULL) FOR [cc_email]
GO
ALTER TABLE [dbo].[companycontact]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companycontact_company]
	FOREIGN KEY ([cc_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companycontact]
	NOCHECK CONSTRAINT [FK_companycontact_company]

GO
ALTER TABLE [dbo].[companycontact] SET (LOCK_ESCALATION = TABLE)
GO
