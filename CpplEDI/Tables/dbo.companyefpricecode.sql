SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companyefpricecode] (
		[cep_company_id]     [int] NOT NULL,
		[cep_order]          [int] NOT NULL,
		[cep_pricecode]      [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[companyefpricecode]
	ADD
	CONSTRAINT [DF__companyef__cep_c__36936B04]
	DEFAULT ('0') FOR [cep_company_id]
GO
ALTER TABLE [dbo].[companyefpricecode]
	ADD
	CONSTRAINT [DF__companyef__cep_o__37878F3D]
	DEFAULT ('0') FOR [cep_order]
GO
ALTER TABLE [dbo].[companyefpricecode]
	ADD
	CONSTRAINT [DF__companyef__cep_p__387BB376]
	DEFAULT ('') FOR [cep_pricecode]
GO
ALTER TABLE [dbo].[companyefpricecode]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companyefpricecode_company]
	FOREIGN KEY ([cep_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companyefpricecode]
	NOCHECK CONSTRAINT [FK_companyefpricecode_company]

GO
ALTER TABLE [dbo].[companyefpricecode] SET (LOCK_ESCALATION = TABLE)
GO
