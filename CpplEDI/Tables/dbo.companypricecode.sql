SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companypricecode] (
		[cp_company_id]       [int] NOT NULL,
		[cp_order]            [int] NOT NULL,
		[cp_match]            [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[cp_pricecode]        [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[cp_cubic_factor]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cp_style]            [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[companypricecode]
	ADD
	CONSTRAINT [DF__companypr__cp_co__3A63FBE8]
	DEFAULT ('0') FOR [cp_company_id]
GO
ALTER TABLE [dbo].[companypricecode]
	ADD
	CONSTRAINT [DF__companypr__cp_or__3B582021]
	DEFAULT ('0') FOR [cp_order]
GO
ALTER TABLE [dbo].[companypricecode]
	ADD
	CONSTRAINT [DF__companypr__cp_ma__3C4C445A]
	DEFAULT ('') FOR [cp_match]
GO
ALTER TABLE [dbo].[companypricecode]
	ADD
	CONSTRAINT [DF__companypr__cp_pr__3D406893]
	DEFAULT ('') FOR [cp_pricecode]
GO
ALTER TABLE [dbo].[companypricecode]
	ADD
	CONSTRAINT [DF__companypr__cp_cu__3E348CCC]
	DEFAULT ('N') FOR [cp_cubic_factor]
GO
ALTER TABLE [dbo].[companypricecode]
	ADD
	CONSTRAINT [DF__companypr__cp_st__3F28B105]
	DEFAULT ('I') FOR [cp_style]
GO
ALTER TABLE [dbo].[companypricecode]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companypricecode_company]
	FOREIGN KEY ([cp_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companypricecode]
	NOCHECK CONSTRAINT [FK_companypricecode_company]

GO
ALTER TABLE [dbo].[companypricecode] SET (LOCK_ESCALATION = TABLE)
GO
