SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companyremapagent] (
		[ra_company_id]        [int] NOT NULL,
		[ra_order]             [int] NOT NULL,
		[ra_from_agent_id]     [int] NOT NULL,
		[ra_to_agent_id]       [int] NOT NULL,
		[ra_pickup]            [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[ra_delivery]          [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[ra_import_only]       [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[companyremapagent]
	ADD
	CONSTRAINT [DF__companyre__ra_co__4110F977]
	DEFAULT ('0') FOR [ra_company_id]
GO
ALTER TABLE [dbo].[companyremapagent]
	ADD
	CONSTRAINT [DF__companyre__ra_or__42051DB0]
	DEFAULT ('0') FOR [ra_order]
GO
ALTER TABLE [dbo].[companyremapagent]
	ADD
	CONSTRAINT [DF__companyre__ra_fr__42F941E9]
	DEFAULT ('0') FOR [ra_from_agent_id]
GO
ALTER TABLE [dbo].[companyremapagent]
	ADD
	CONSTRAINT [DF__companyre__ra_to__43ED6622]
	DEFAULT ('0') FOR [ra_to_agent_id]
GO
ALTER TABLE [dbo].[companyremapagent]
	ADD
	CONSTRAINT [DF__companyre__ra_pi__44E18A5B]
	DEFAULT ('N') FOR [ra_pickup]
GO
ALTER TABLE [dbo].[companyremapagent]
	ADD
	CONSTRAINT [DF__companyre__ra_de__45D5AE94]
	DEFAULT ('N') FOR [ra_delivery]
GO
ALTER TABLE [dbo].[companyremapagent]
	ADD
	CONSTRAINT [DF__companyre__ra_im__46C9D2CD]
	DEFAULT ('N') FOR [ra_import_only]
GO
ALTER TABLE [dbo].[companyremapagent]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companyremapagent_company]
	FOREIGN KEY ([ra_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companyremapagent]
	NOCHECK CONSTRAINT [FK_companyremapagent_company]

GO
ALTER TABLE [dbo].[companyremapagent]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companyremapagent_ra_to_agent]
	FOREIGN KEY ([ra_to_agent_id]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companyremapagent]
	NOCHECK CONSTRAINT [FK_companyremapagent_ra_to_agent]

GO
ALTER TABLE [dbo].[companyremapagent]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companyremapagent_ra_from_agent]
	FOREIGN KEY ([ra_from_agent_id]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companyremapagent]
	NOCHECK CONSTRAINT [FK_companyremapagent_ra_from_agent]

GO
ALTER TABLE [dbo].[companyremapagent] SET (LOCK_ESCALATION = TABLE)
GO
