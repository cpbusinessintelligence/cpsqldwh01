SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companysscc] (
		[cs_company_id]     [int] NOT NULL,
		[cs_order]          [int] NOT NULL,
		[cs_match]          [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[companysscc]
	ADD
	CONSTRAINT [DF__companyss__cs_co__48B21B3F]
	DEFAULT ('0') FOR [cs_company_id]
GO
ALTER TABLE [dbo].[companysscc]
	ADD
	CONSTRAINT [DF__companyss__cs_or__49A63F78]
	DEFAULT ('0') FOR [cs_order]
GO
ALTER TABLE [dbo].[companysscc]
	ADD
	CONSTRAINT [DF__companyss__cs_ma__4A9A63B1]
	DEFAULT ('') FOR [cs_match]
GO
ALTER TABLE [dbo].[companysscc]
	WITH NOCHECK
	ADD CONSTRAINT [FK_companysscc_company]
	FOREIGN KEY ([cs_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[companysscc]
	NOCHECK CONSTRAINT [FK_companysscc_company]

GO
ALTER TABLE [dbo].[companysscc] SET (LOCK_ESCALATION = TABLE)
GO
