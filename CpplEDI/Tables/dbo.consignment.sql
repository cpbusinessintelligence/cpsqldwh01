SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[consignment] (
		[cd_id]                            [int] NOT NULL,
		[cd_company_id]                    [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_id]                      [int] NULL,
		[cd_import_id]                     [int] NULL,
		[cd_ogm_id]                        [int] NULL,
		[cd_manifest_id]                   [int] NULL,
		[cd_connote]                       [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_eta_date]                      [smalldatetime] NULL,
		[cd_eta_earliest]                  [smalldatetime] NULL,
		[cd_customer_eta]                  [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_record_no]              [int] NULL,
		[cd_pickup_confidence]             [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_email]                [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_postcode]             [int] NULL,
		[cd_delivery_record_no]            [int] NULL,
		[cd_delivery_confidence]           [int] NULL,
		[cd_delivery_contact]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_contact_phone]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_stats_branch]                  [int] NULL,
		[cd_stats_depot]                   [int] NULL,
		[cd_pickup_branch]                 [int] NULL,
		[cd_pickup_pay_branch]             [int] NULL,
		[cd_deliver_branch]                [int] NULL,
		[cd_deliver_pay_branch]            [int] NULL,
		[cd_special_driver]                [int] NULL,
		[cd_pickup_revenue]                [float] NULL,
		[cd_deliver_revenue]               [float] NULL,
		[cd_pickup_billing]                [float] NULL,
		[cd_deliver_billing]               [float] NULL,
		[cd_pickup_charge]                 [float] NULL,
		[cd_pickup_charge_actual]          [float] NULL,
		[cd_deliver_charge]                [float] NULL,
		[cd_deliver_payment_actual]        [float] NULL,
		[cd_pickup_payment]                [float] NULL,
		[cd_pickup_payment_actual]         [float] NULL,
		[cd_deliver_payment]               [float] NULL,
		[cd_deliver_charge_actual]         [float] NULL,
		[cd_special_payment]               [float] NULL,
		[cd_insurance_billing]             [float] NULL,
		[cd_items]                         [int] NULL,
		[cd_coupons]                       [int] NULL,
		[cd_references]                    [int] NULL,
		[cd_rating_id]                     [int] NULL,
		[cd_chargeunits]                   [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_dimension0]                    [float] NULL,
		[cd_dimension1]                    [float] NULL,
		[cd_dimension2]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_volume_automatic]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_import_deadweight]             [float] NULL,
		[cd_import_volume]                 [float] NULL,
		[cd_measured_deadweight]           [float] NULL,
		[cd_measured_volume]               [float] NULL,
		[cd_billing_id]                    [int] NULL,
		[cd_billing_date]                  [smalldatetime] NULL,
		[cd_export_id]                     [int] NULL,
		[cd_export2_id]                    [int] NULL,
		[cd_pickup_pay_date]               [smalldatetime] NULL,
		[cd_delivery_pay_date]             [smalldatetime] NULL,
		[cd_transfer_pay_date]             [smalldatetime] NULL,
		[cd_activity_stamp]                [datetime] NULL,
		[cd_activity_driver]               [int] NULL,
		[cd_pickup_stamp]                  [datetime] NULL,
		[cd_pickup_driver]                 [int] NULL,
		[cd_pickup_pay_driver]             [int] NULL,
		[cd_pickup_count]                  [int] NULL,
		[cd_pickup_notified]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_accept_stamp]                  [datetime] NULL,
		[cd_accept_driver]                 [int] NULL,
		[cd_accept_count]                  [int] NULL,
		[cd_accept_notified]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_indepot_notified]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_indepot_stamp]                 [datetime] NULL,
		[cd_indepot_driver]                [int] NULL,
		[cd_indepot_count]                 [int] NULL,
		[cd_transfer_notified]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_failed_stamp]                  [datetime] NULL,
		[cd_failed_driver]                 [int] NULL,
		[cd_deliver_stamp]                 [datetime] NULL,
		[cd_deliver_driver]                [int] NULL,
		[cd_deliver_pay_driver]            [int] NULL,
		[cd_deliver_count]                 [int] NULL,
		[cd_deliver_pod]                   [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_deliver_notified]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_pay_notified]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_deliver_pay_notified]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_printed]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_returns]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_release]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_release_stamp]                 [datetime] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_agent]                  [int] NULL,
		[cd_delivery_agent]                [int] NULL,
		[cd_agent_pod]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_pod_desired]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_pod_name]                [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_pod_stamp]               [datetime] NULL,
		[cd_agent_pod_entry]               [datetime] NULL,
		[cd_completed]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_cancelled]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_cancelled_stamp]               [datetime] NULL,
		[cd_cancelled_by]                  [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_test]                          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_dirty]                         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_transfer_stamp]                [datetime] NULL,
		[cd_transfer_driver]               [int] NULL,
		[cd_transfer_count]                [int] NULL,
		[cd_transfer_to]                   [int] NULL,
		[cd_toagent_notified]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_toagent_stamp]                 [datetime] NULL,
		[cd_toagent_driver]                [int] NULL,
		[cd_toagent_count]                 [int] NULL,
		[cd_toagent_name]                  [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_status]                   [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_notified]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_info]                     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_driver]                   [int] NULL,
		[cd_last_stamp]                    [datetime] NULL,
		[cd_last_count]                    [int] NULL,
		[cd_accept_driver_branch]          [int] NULL,
		[cd_activity_driver_branch]        [int] NULL,
		[cd_deliver_driver_branch]         [int] NULL,
		[cd_deliver_pay_driver_branch]     [int] NULL,
		[cd_failed_driver_branch]          [int] NULL,
		[cd_indepot_driver_branch]         [int] NULL,
		[cd_last_driver_branch]            [int] NULL,
		[cd_pickup_driver_branch]          [int] NULL,
		[cd_pickup_pay_driver_branch]      [int] NULL,
		[cd_special_driver_branch]         [int] NULL,
		[cd_toagent_driver_branch]         [int] NULL,
		[cd_transfer_driver_branch]        [int] NULL,
		CONSTRAINT [PK__consignm__D551B536CC82C0AC]
		PRIMARY KEY
		CLUSTERED
		([cd_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__005F4FA8]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__015373E1]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__0247981A]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__033BBC53]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__042FE08C]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__052404C5]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_in__061828FE]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_in__070C4D37]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_in__08007170]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_in__08F495A9]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_tr__09E8B9E2]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_fa__0ADCDE1B]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_fa__0BD10254]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__0CC5268D]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__0DB94AC6]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__0EAD6EFF]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__0FA19338]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__1095B771]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__1189DBAA]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__127DFFE3]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__1372241C]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pr__14664855]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_re__155A6C8E]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_re__164E90C7]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_re__1742B500]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pr__1836D939]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_in__192AFD72]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__1A1F21AB]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__1B1345E4]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ag__1C076A1D]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ag__1CFB8E56]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ag__1DEFB28F]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ag__1EE3D6C8]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ag__1FD7FB01]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_co__20CC1F3A]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ca__21C04373]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ca__22B467AC]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ca__23A88BE5]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_te__249CB01E]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_di__2590D457]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_tr__2684F890]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_tr__27791CC9]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_tr__286D4102]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_tr__2961653B]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_to__2A558974]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_to__2B49ADAD]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_to__2C3DD1E6]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_to__2D31F61F]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_to__2E261A58]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_la__2F1A3E91]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_la__300E62CA]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_la__31028703]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_la__31F6AB3C]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_co__322BB566]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_la__32EACF75]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__331FD99F]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_la__33DEF3AE]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ag__3413FDD8]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__34D317E7]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_im__35082211]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__35C73C20]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_og__35FC464A]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__36BB6059]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ma__36F06A83]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__37AF8492]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_co__37E48EBC]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_fa__38A3A8CB]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_da__38D8B2F5]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_in__3997CD04]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_co__39CCD72E]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_la__3A8BF13D]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_et__3AC0FB67]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__3B801576]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_et__3BB51FA0]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__3C7439AF]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_cu__3CA943D9]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_sp__3D685DE8]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__3D9D6812]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_to__3E5C8221]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__3E918C4B]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_tr__3F50A65A]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__3F85B084]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__4079D4BD]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__416DF8F6]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__42621D2F]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__43564168]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__444A65A1]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__453E89DA]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__4632AE13]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4726D24C]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__481AF685]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__490F1ABE]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4A033EF7]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4AF76330]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4BEB8769]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4CDFABA2]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4DD3CFDB]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4EC7F414]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__4FBC184D]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__50B03C86]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_sp__51A460BF]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_st__529884F8]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_st__538CA931]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__5480CD6A]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__5574F1A3]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__566915DC]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__575D3A15]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_sp__58515E4E]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__59458287]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__5A39A6C0]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__5B2DCAF9]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__5C21EF32]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__5D16136B]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__5E0A37A4]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__5EFE5BDD]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__5FF28016]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__60E6A44F]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__61DAC888]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__62CEECC1]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__63C310FA]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_sp__64B73533]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_in__65AB596C]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_it__669F7DA5]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_co__6793A1DE]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_re__6887C617]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ra__697BEA50]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ch__6A700E89]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__6B6432C2]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_di__6C5856FB]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_di__6D4C7B34]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_di__6E409F6D]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_vo__6F34C3A6]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_vo__7028E7DF]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_im__711D0C18]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_im__72113051]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_me__7305548A]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_me__73F978C3]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_bi__74ED9CFC]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_bi__75E1C135]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ex__76D5E56E]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ex__77CA09A7]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__78BE2DE0]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_de__79B25219]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_tr__7AA67652]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__7B9A9A8B]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_ac__7C8EBEC4]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__7D82E2FD]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__7E770736]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[consignment]
	ADD
	CONSTRAINT [DF__consignme__cd_pi__7F6B2B6F]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_toagent]
	FOREIGN KEY ([cd_toagent_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_toagent]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_activity]
	FOREIGN KEY ([cd_activity_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_activity]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_deliverypay]
	FOREIGN KEY ([cd_deliver_pay_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_deliverypay]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_special]
	FOREIGN KEY ([cd_special_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_special]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_pickup_agent]
	FOREIGN KEY ([cd_pickup_agent]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_pickup_agent]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_manifest]
	FOREIGN KEY ([cd_manifest_id]) REFERENCES [dbo].[manifest] ([m_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_manifest]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_ogmanifest]
	FOREIGN KEY ([cd_ogm_id]) REFERENCES [dbo].[ogmanifest] ([ogm_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_ogmanifest]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_export2]
	FOREIGN KEY ([cd_export2_id]) REFERENCES [dbo].[exports] ([e_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_export2]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_branch_deliver]
	FOREIGN KEY ([cd_deliver_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_branch_deliver]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_lastdriver]
	FOREIGN KEY ([cd_last_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_lastdriver]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_import]
	FOREIGN KEY ([cd_import_id]) REFERENCES [dbo].[imports] ([i_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_import]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_branch_stats]
	FOREIGN KEY ([cd_stats_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_branch_stats]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_export]
	FOREIGN KEY ([cd_export_id]) REFERENCES [dbo].[exports] ([e_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_export]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_transfer]
	FOREIGN KEY ([cd_transfer_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_transfer]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_agent]
	FOREIGN KEY ([cd_agent_id]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_agent]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_stats_depot]
	FOREIGN KEY ([cd_stats_depot]) REFERENCES [dbo].[depot] ([d_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_stats_depot]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_indepot]
	FOREIGN KEY ([cd_indepot_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_indepot]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignemnt_driver_pickup]
	FOREIGN KEY ([cd_pickup_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignemnt_driver_pickup]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_branch_pickuppay]
	FOREIGN KEY ([cd_pickup_pay_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_branch_pickuppay]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_deliver]
	FOREIGN KEY ([cd_deliver_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_deliver]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_branch_deliverpay]
	FOREIGN KEY ([cd_deliver_pay_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_branch_deliverpay]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_pickuppay]
	FOREIGN KEY ([cd_pickup_pay_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_pickuppay]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_accept]
	FOREIGN KEY ([cd_accept_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_accept]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_company]
	FOREIGN KEY ([cd_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_company]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_delivery_agent]
	FOREIGN KEY ([cd_delivery_agent]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_delivery_agent]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_branch_pickup]
	FOREIGN KEY ([cd_pickup_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_branch_pickup]

GO
ALTER TABLE [dbo].[consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_consignment_driver_failed]
	FOREIGN KEY ([cd_failed_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[consignment]
	NOCHECK CONSTRAINT [FK_consignment_driver_failed]

GO
CREATE NONCLUSTERED INDEX [IX_Consignment_cd_date]
	ON [dbo].[consignment] ([cd_date])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170914-185417]
	ON [dbo].[consignment] ([cd_connote])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_consignment_idcd_account]
	ON [dbo].[consignment] ([cd_account])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_consignment_manifest_id]
	ON [dbo].[consignment] ([cd_manifest_id], [cd_company_id])
	INCLUDE ([cd_id], [cd_connote])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_consignment_customer_eta]
	ON [dbo].[consignment] ([cd_customer_eta] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_consignment_idmanifestimportdate]
	ON [dbo].[consignment] ([cd_id], [cd_import_id], [cd_manifest_id], [cd_date], [cd_consignment_date], [cd_company_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_consignment_connote]
	ON [dbo].[consignment] ([cd_connote])
	INCLUDE ([cd_deliver_stamp], [cd_delivery_agent], [cd_agent_pod], [cd_agent_pod_stamp])
	ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[consignment]
	TO [DataFactoryUser]
GO
GRANT CONTROL
	ON [dbo].[consignment]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[consignment]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[consignment]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[consignment] SET (LOCK_ESCALATION = TABLE)
GO
