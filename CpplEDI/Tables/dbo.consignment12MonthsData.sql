SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[consignment12MonthsData] (
		[cd_id]                            [int] NOT NULL,
		[cd_company_id]                    [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_id]                      [int] NULL,
		[cd_import_id]                     [int] NULL,
		[cd_ogm_id]                        [int] NULL,
		[cd_manifest_id]                   [int] NULL,
		[cd_connote]                       [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_eta_date]                      [smalldatetime] NULL,
		[cd_eta_earliest]                  [smalldatetime] NULL,
		[cd_customer_eta]                  [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_record_no]              [int] NULL,
		[cd_pickup_confidence]             [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_email]                [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_postcode]             [int] NULL,
		[cd_delivery_record_no]            [int] NULL,
		[cd_delivery_confidence]           [int] NULL,
		[cd_delivery_contact]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_contact_phone]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_stats_branch]                  [int] NULL,
		[cd_stats_depot]                   [int] NULL,
		[cd_pickup_branch]                 [int] NULL,
		[cd_pickup_pay_branch]             [int] NULL,
		[cd_deliver_branch]                [int] NULL,
		[cd_deliver_pay_branch]            [int] NULL,
		[cd_special_driver]                [int] NULL,
		[cd_pickup_revenue]                [float] NULL,
		[cd_deliver_revenue]               [float] NULL,
		[cd_pickup_billing]                [float] NULL,
		[cd_deliver_billing]               [float] NULL,
		[cd_pickup_charge]                 [float] NULL,
		[cd_pickup_charge_actual]          [float] NULL,
		[cd_deliver_charge]                [float] NULL,
		[cd_deliver_payment_actual]        [float] NULL,
		[cd_pickup_payment]                [float] NULL,
		[cd_pickup_payment_actual]         [float] NULL,
		[cd_deliver_payment]               [float] NULL,
		[cd_deliver_charge_actual]         [float] NULL,
		[cd_special_payment]               [float] NULL,
		[cd_insurance_billing]             [float] NULL,
		[cd_items]                         [int] NULL,
		[cd_coupons]                       [int] NULL,
		[cd_references]                    [int] NULL,
		[cd_rating_id]                     [int] NULL,
		[cd_chargeunits]                   [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_dimension0]                    [float] NULL,
		[cd_dimension1]                    [float] NULL,
		[cd_dimension2]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_volume_automatic]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_import_deadweight]             [float] NULL,
		[cd_import_volume]                 [float] NULL,
		[cd_measured_deadweight]           [float] NULL,
		[cd_measured_volume]               [float] NULL,
		[cd_billing_id]                    [int] NULL,
		[cd_billing_date]                  [smalldatetime] NULL,
		[cd_export_id]                     [int] NULL,
		[cd_export2_id]                    [int] NULL,
		[cd_pickup_pay_date]               [smalldatetime] NULL,
		[cd_delivery_pay_date]             [smalldatetime] NULL,
		[cd_transfer_pay_date]             [smalldatetime] NULL,
		[cd_activity_stamp]                [datetime] NULL,
		[cd_activity_driver]               [int] NULL,
		[cd_pickup_stamp]                  [datetime] NULL,
		[cd_pickup_driver]                 [int] NULL,
		[cd_pickup_pay_driver]             [int] NULL,
		[cd_pickup_count]                  [int] NULL,
		[cd_pickup_notified]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_accept_stamp]                  [datetime] NULL,
		[cd_accept_driver]                 [int] NULL,
		[cd_accept_count]                  [int] NULL,
		[cd_accept_notified]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_indepot_notified]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_indepot_stamp]                 [datetime] NULL,
		[cd_indepot_driver]                [int] NULL,
		[cd_indepot_count]                 [int] NULL,
		[cd_transfer_notified]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_failed_stamp]                  [datetime] NULL,
		[cd_failed_driver]                 [int] NULL,
		[cd_deliver_stamp]                 [datetime] NULL,
		[cd_deliver_driver]                [int] NULL,
		[cd_deliver_pay_driver]            [int] NULL,
		[cd_deliver_count]                 [int] NULL,
		[cd_deliver_pod]                   [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_deliver_notified]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_pay_notified]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_deliver_pay_notified]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_printed]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_returns]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_release]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_release_stamp]                 [datetime] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_agent]                  [int] NULL,
		[cd_delivery_agent]                [int] NULL,
		[cd_agent_pod]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_pod_desired]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_pod_name]                [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_agent_pod_stamp]               [datetime] NULL,
		[cd_agent_pod_entry]               [datetime] NULL,
		[cd_completed]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_cancelled]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_cancelled_stamp]               [datetime] NULL,
		[cd_cancelled_by]                  [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_test]                          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_dirty]                         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_transfer_stamp]                [datetime] NULL,
		[cd_transfer_driver]               [int] NULL,
		[cd_transfer_count]                [int] NULL,
		[cd_transfer_to]                   [int] NULL,
		[cd_toagent_notified]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_toagent_stamp]                 [datetime] NULL,
		[cd_toagent_driver]                [int] NULL,
		[cd_toagent_count]                 [int] NULL,
		[cd_toagent_name]                  [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_status]                   [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_notified]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_info]                     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_last_driver]                   [int] NULL,
		[cd_last_stamp]                    [datetime] NULL,
		[cd_last_count]                    [int] NULL,
		[cd_accept_driver_branch]          [int] NULL,
		[cd_activity_driver_branch]        [int] NULL,
		[cd_deliver_driver_branch]         [int] NULL,
		[cd_deliver_pay_driver_branch]     [int] NULL,
		[cd_failed_driver_branch]          [int] NULL,
		[cd_indepot_driver_branch]         [int] NULL,
		[cd_last_driver_branch]            [int] NULL,
		[cd_pickup_driver_branch]          [int] NULL,
		[cd_pickup_pay_driver_branch]      [int] NULL,
		[cd_special_driver_branch]         [int] NULL,
		[cd_toagent_driver_branch]         [int] NULL,
		[cd_transfer_driver_branch]        [int] NULL
)
GO
CREATE NONCLUSTERED INDEX [idx_consignment12MonthsData_connote]
	ON [dbo].[consignment12MonthsData] ([cd_connote])
	INCLUDE ([cd_deliver_stamp], [cd_delivery_agent], [cd_agent_pod], [cd_agent_pod_stamp])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_consignment12MonthsData_idcd_account]
	ON [dbo].[consignment12MonthsData] ([cd_account])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_consignment12MonthsData_idmanifestimportdate]
	ON [dbo].[consignment12MonthsData] ([cd_id], [cd_import_id], [cd_manifest_id], [cd_date], [cd_consignment_date], [cd_company_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_consignment12MonthsData_cd_date]
	ON [dbo].[consignment12MonthsData] ([cd_date])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_consignment12MonthsData_customer_eta]
	ON [dbo].[consignment12MonthsData] ([cd_customer_eta] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_consignment12MonthsData_manifest_id]
	ON [dbo].[consignment12MonthsData] ([cd_manifest_id], [cd_company_id])
	INCLUDE ([cd_id], [cd_connote])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-consignment12MonthsData-20170914-185417]
	ON [dbo].[consignment12MonthsData] ([cd_connote])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[consignment12MonthsData] SET (LOCK_ESCALATION = TABLE)
GO
