SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[consignmentstatus] (
		[cs_code]            [varchar](8) COLLATE Latin1_General_CI_AS NOT NULL,
		[cs_description]     [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[cs_dhl_code]        [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[cs_failure]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__consignm__457BF54F430AD045]
		PRIMARY KEY
		CLUSTERED
		([cs_code])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[consignmentstatus]
	ADD
	CONSTRAINT [DF__consignme__cs_co__4D76D05C]
	DEFAULT ('') FOR [cs_code]
GO
ALTER TABLE [dbo].[consignmentstatus]
	ADD
	CONSTRAINT [DF__consignme__cs_de__4E6AF495]
	DEFAULT ('') FOR [cs_description]
GO
ALTER TABLE [dbo].[consignmentstatus]
	ADD
	CONSTRAINT [DF__consignme__cs_dh__4F5F18CE]
	DEFAULT (NULL) FOR [cs_dhl_code]
GO
ALTER TABLE [dbo].[consignmentstatus]
	ADD
	CONSTRAINT [DF__consignme__cs_fa__50533D07]
	DEFAULT ('N') FOR [cs_failure]
GO
ALTER TABLE [dbo].[consignmentstatus] SET (LOCK_ESCALATION = TABLE)
GO
