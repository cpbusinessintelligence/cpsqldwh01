SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cpplcpngrid] (
		[ccg_pricegroup]            [int] NOT NULL,
		[ccg_pricecode]             [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[ccg_fromzone]              [int] NOT NULL,
		[ccg_tozone]                [int] NOT NULL,
		[ccg_present]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ccg_baseprice]             [float] NULL,
		[ccg_15kg]                  [float] NULL,
		[ccg_25kg]                  [float] NULL,
		[ccg_pickup_payamount]      [float] NULL,
		[ccg_pickup_15kg]           [float] NULL,
		[ccg_pickup_25kg]           [float] NULL,
		[ccg_deliver_payamount]     [float] NULL,
		[ccg_deliver_15kg]          [float] NULL,
		[ccg_deliver_25kg]          [float] NULL
)
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_p__523B8579]
	DEFAULT ('0') FOR [ccg_pricegroup]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_p__532FA9B2]
	DEFAULT ('') FOR [ccg_pricecode]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_f__5423CDEB]
	DEFAULT ('0') FOR [ccg_fromzone]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_t__5517F224]
	DEFAULT ('0') FOR [ccg_tozone]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_p__560C165D]
	DEFAULT ('N') FOR [ccg_present]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_b__57003A96]
	DEFAULT ('0') FOR [ccg_baseprice]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_1__57F45ECF]
	DEFAULT ('0') FOR [ccg_15kg]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_2__58E88308]
	DEFAULT ('0') FOR [ccg_25kg]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_p__59DCA741]
	DEFAULT ('0') FOR [ccg_pickup_payamount]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_p__5AD0CB7A]
	DEFAULT ('0') FOR [ccg_pickup_15kg]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_p__5BC4EFB3]
	DEFAULT ('0') FOR [ccg_pickup_25kg]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_d__5CB913EC]
	DEFAULT ('0') FOR [ccg_deliver_payamount]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_d__5DAD3825]
	DEFAULT ('0') FOR [ccg_deliver_15kg]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	ADD
	CONSTRAINT [DF__cpplcpngr__ccg_d__5EA15C5E]
	DEFAULT ('0') FOR [ccg_deliver_25kg]
GO
ALTER TABLE [dbo].[cpplcpngrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cpplcpngrid_ccg_fromzone]
	FOREIGN KEY ([ccg_fromzone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cpplcpngrid]
	NOCHECK CONSTRAINT [FK_cpplcpngrid_ccg_fromzone]

GO
ALTER TABLE [dbo].[cpplcpngrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cpplcpngrid_ccg_tozone]
	FOREIGN KEY ([ccg_tozone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cpplcpngrid]
	NOCHECK CONSTRAINT [FK_cpplcpngrid_ccg_tozone]

GO
ALTER TABLE [dbo].[cpplcpngrid] SET (LOCK_ESCALATION = TABLE)
GO
