SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cpplgrid] (
		[cg_pricegroup]            [int] NOT NULL,
		[cg_pricecode]             [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[cg_fromzone]              [int] NOT NULL,
		[cg_tozone]                [int] NOT NULL,
		[cg_present]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[cg_baseprice]             [float] NULL,
		[cg_kgincluded]            [float] NULL,
		[cg_kgprice]               [float] NULL,
		[cg_pickup_payamount]      [float] NULL,
		[cg_pickup_paybreak]       [float] NULL,
		[cg_pickup_kgpre]          [float] NULL,
		[cg_pickup_kgpost]         [float] NULL,
		[cg_deliver_payamount]     [float] NULL,
		[cg_deliver_paybreak]      [float] NULL,
		[cg_deliver_kgpre]         [float] NULL,
		[cg_deliver_kgpost]        [float] NULL
)
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_pri__6089A4D0]
	DEFAULT ('0') FOR [cg_pricegroup]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_pri__617DC909]
	DEFAULT ('') FOR [cg_pricecode]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_fro__6271ED42]
	DEFAULT ('0') FOR [cg_fromzone]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_toz__6366117B]
	DEFAULT ('0') FOR [cg_tozone]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_pre__645A35B4]
	DEFAULT ('N') FOR [cg_present]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_bas__654E59ED]
	DEFAULT ('0') FOR [cg_baseprice]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_kgi__66427E26]
	DEFAULT ('0') FOR [cg_kgincluded]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_kgp__6736A25F]
	DEFAULT ('0') FOR [cg_kgprice]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_pic__682AC698]
	DEFAULT ('0') FOR [cg_pickup_payamount]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_pic__691EEAD1]
	DEFAULT ('25') FOR [cg_pickup_paybreak]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_pic__6A130F0A]
	DEFAULT ('0') FOR [cg_pickup_kgpre]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_pic__6B073343]
	DEFAULT ('0') FOR [cg_pickup_kgpost]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_del__6BFB577C]
	DEFAULT ('0') FOR [cg_deliver_payamount]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_del__6CEF7BB5]
	DEFAULT ('25') FOR [cg_deliver_paybreak]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_del__6DE39FEE]
	DEFAULT ('0') FOR [cg_deliver_kgpre]
GO
ALTER TABLE [dbo].[cpplgrid]
	ADD
	CONSTRAINT [DF__cpplgrid__cg_del__6ED7C427]
	DEFAULT ('0') FOR [cg_deliver_kgpost]
GO
ALTER TABLE [dbo].[cpplgrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cpplgrid_cg_tozone]
	FOREIGN KEY ([cg_tozone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cpplgrid]
	NOCHECK CONSTRAINT [FK_cpplgrid_cg_tozone]

GO
ALTER TABLE [dbo].[cpplgrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_cpplgrid_cg_fromzone]
	FOREIGN KEY ([cg_fromzone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[cpplgrid]
	NOCHECK CONSTRAINT [FK_cpplgrid_cg_fromzone]

GO
ALTER TABLE [dbo].[cpplgrid] SET (LOCK_ESCALATION = TABLE)
GO
