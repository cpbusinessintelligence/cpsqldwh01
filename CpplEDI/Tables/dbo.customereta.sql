SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customereta] (
		[ce_from]     [char](12) COLLATE Latin1_General_CI_AS NULL,
		[ce_to]       [char](12) COLLATE Latin1_General_CI_AS NULL,
		[ce_days]     [float] NULL
)
GO
ALTER TABLE [dbo].[customereta]
	ADD
	CONSTRAINT [DF__customere__ce_fr__70C00C99]
	DEFAULT ('') FOR [ce_from]
GO
ALTER TABLE [dbo].[customereta]
	ADD
	CONSTRAINT [DF__customere__ce_to__71B430D2]
	DEFAULT ('') FOR [ce_to]
GO
ALTER TABLE [dbo].[customereta]
	ADD
	CONSTRAINT [DF__customere__ce_da__72A8550B]
	DEFAULT ('1') FOR [ce_days]
GO
CREATE NONCLUSTERED INDEX [idx_customereta_FromTo]
	ON [dbo].[customereta] ([ce_from], [ce_to])
	INCLUDE ([ce_days])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[customereta] SET (LOCK_ESCALATION = TABLE)
GO
