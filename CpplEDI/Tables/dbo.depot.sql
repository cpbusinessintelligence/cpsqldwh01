SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[depot] (
		[d_id]           [int] NOT NULL,
		[d_code]         [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[d_branch]       [int] NULL,
		[d_name]         [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[d_suburb]       [int] NULL,
		[d_address0]     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[d_address1]     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[d_contact]      [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[d_style]        [tinyint] NULL,
		CONSTRAINT [PK__depot__D95F582B18DFB0C0]
		PRIMARY KEY
		CLUSTERED
		([d_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_code__7584C1B6]
	DEFAULT (NULL) FOR [d_code]
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_branch__7678E5EF]
	DEFAULT (NULL) FOR [d_branch]
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_name__776D0A28]
	DEFAULT (NULL) FOR [d_name]
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_suburb__78612E61]
	DEFAULT (NULL) FOR [d_suburb]
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_address__7955529A]
	DEFAULT (NULL) FOR [d_address0]
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_address__7A4976D3]
	DEFAULT (NULL) FOR [d_address1]
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_contact__7B3D9B0C]
	DEFAULT (NULL) FOR [d_contact]
GO
ALTER TABLE [dbo].[depot]
	ADD
	CONSTRAINT [DF__depot__d_style__7C31BF45]
	DEFAULT ('3') FOR [d_style]
GO
ALTER TABLE [dbo].[depot]
	WITH NOCHECK
	ADD CONSTRAINT [FK_depot_branch]
	FOREIGN KEY ([d_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[depot]
	NOCHECK CONSTRAINT [FK_depot_branch]

GO
ALTER TABLE [dbo].[depot]
	WITH NOCHECK
	ADD CONSTRAINT [FK_depot_suburb]
	FOREIGN KEY ([d_suburb]) REFERENCES [dbo].[suburb] ([s_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[depot]
	NOCHECK CONSTRAINT [FK_depot_suburb]

GO
ALTER TABLE [dbo].[depot] SET (LOCK_ESCALATION = TABLE)
GO
