SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dhlinfo] (
		[dhl_awb]              [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[dhl_origin]           [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		[dhl_route]            [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[dhl_service_area]     [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		[dhl_location]         [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		[dhl_userid]           [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[dhl_completed]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[dhl_consignment]      [int] NULL,
		[dhl_cycle]            [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__dhlinfo__0C76E3CB3A7456AC]
		PRIMARY KEY
		CLUSTERED
		([dhl_awb])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_ori__00025029]
	DEFAULT (NULL) FOR [dhl_origin]
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_rou__00F67462]
	DEFAULT (NULL) FOR [dhl_route]
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_ser__01EA989B]
	DEFAULT (NULL) FOR [dhl_service_area]
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_loc__02DEBCD4]
	DEFAULT (NULL) FOR [dhl_location]
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_use__03D2E10D]
	DEFAULT (NULL) FOR [dhl_userid]
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_com__04C70546]
	DEFAULT ('N') FOR [dhl_completed]
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_con__05BB297F]
	DEFAULT (NULL) FOR [dhl_consignment]
GO
ALTER TABLE [dbo].[dhlinfo]
	ADD
	CONSTRAINT [DF__dhlinfo__dhl_awb__7F0E2BF0]
	DEFAULT ('') FOR [dhl_awb]
GO
ALTER TABLE [dbo].[dhlinfo] SET (LOCK_ESCALATION = TABLE)
GO
