SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dhlroute] (
		[dr_route]               [char](8) COLLATE Latin1_General_CI_AS NOT NULL,
		[dr_agent]               [int] NULL,
		[dr_pickup_suburb]       [char](64) COLLATE Latin1_General_CI_AS NULL,
		[dr_pickup_postcode]     [int] NULL,
		[dr_branch]              [int] NULL,
		[dr_company_id]          [int] NULL,
		CONSTRAINT [PK__dhlroute__737CA9F74EE62A60]
		PRIMARY KEY
		CLUSTERED
		([dr_route])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[dhlroute]
	ADD
	CONSTRAINT [DF__dhlroute__dr_age__0897962A]
	DEFAULT ('0') FOR [dr_agent]
GO
ALTER TABLE [dbo].[dhlroute]
	ADD
	CONSTRAINT [DF__dhlroute__dr_pic__098BBA63]
	DEFAULT (NULL) FOR [dr_pickup_suburb]
GO
ALTER TABLE [dbo].[dhlroute]
	ADD
	CONSTRAINT [DF__dhlroute__dr_pic__0A7FDE9C]
	DEFAULT (NULL) FOR [dr_pickup_postcode]
GO
ALTER TABLE [dbo].[dhlroute]
	ADD
	CONSTRAINT [DF__dhlroute__dr_bra__0B7402D5]
	DEFAULT (NULL) FOR [dr_branch]
GO
ALTER TABLE [dbo].[dhlroute]
	ADD
	CONSTRAINT [DF__dhlroute__dr_com__0C68270E]
	DEFAULT ('21') FOR [dr_company_id]
GO
ALTER TABLE [dbo].[dhlroute] SET (LOCK_ESCALATION = TABLE)
GO
