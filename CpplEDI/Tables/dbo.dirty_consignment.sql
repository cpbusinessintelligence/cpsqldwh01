SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[dirty_consignment] (
		[dc_id]             [int] NOT NULL,
		[dc_company_id]     [int] NOT NULL,
		CONSTRAINT [PK__dirty_co__33FDC9751DBD9C89]
		PRIMARY KEY
		CLUSTERED
		([dc_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[dirty_consignment]
	ADD
	CONSTRAINT [DF__dirty_con__dc_id__0F4493B9]
	DEFAULT ('0') FOR [dc_id]
GO
ALTER TABLE [dbo].[dirty_consignment]
	ADD
	CONSTRAINT [DF__dirty_con__dc_co__1038B7F2]
	DEFAULT ('0') FOR [dc_company_id]
GO
ALTER TABLE [dbo].[dirty_consignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_dirty_consignment_company]
	FOREIGN KEY ([dc_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[dirty_consignment]
	NOCHECK CONSTRAINT [FK_dirty_consignment_company]

GO
ALTER TABLE [dbo].[dirty_consignment] SET (LOCK_ESCALATION = TABLE)
GO
