SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[dirty_coupon] (
		[dc_id]             [int] NOT NULL,
		[dc_company_id]     [int] NOT NULL,
		CONSTRAINT [PK__dirty_co__33FDC97544569096]
		PRIMARY KEY
		CLUSTERED
		([dc_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[dirty_coupon]
	ADD
	CONSTRAINT [DF__dirty_cou__dc_id__1315249D]
	DEFAULT ('0') FOR [dc_id]
GO
ALTER TABLE [dbo].[dirty_coupon]
	ADD
	CONSTRAINT [DF__dirty_cou__dc_co__140948D6]
	DEFAULT ('0') FOR [dc_company_id]
GO
ALTER TABLE [dbo].[dirty_coupon]
	WITH NOCHECK
	ADD CONSTRAINT [FK_dirty_coupon_company]
	FOREIGN KEY ([dc_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[dirty_coupon]
	NOCHECK CONSTRAINT [FK_dirty_coupon_company]

GO
ALTER TABLE [dbo].[dirty_coupon] SET (LOCK_ESCALATION = TABLE)
GO
