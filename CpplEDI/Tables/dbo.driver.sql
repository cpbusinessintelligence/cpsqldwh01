SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[driver] (
		[dr_id]            [int] NOT NULL,
		[dr_branch]        [int] NULL,
		[dr_number]        [int] NULL,
		[dr_name]          [char](64) COLLATE Latin1_General_CI_AS NULL,
		[dr_depot]         [int] NULL,
		[dr_warehouse]     [char](16) COLLATE Latin1_General_CI_AS NULL,
		[dr_agentname]     [char](16) COLLATE Latin1_General_CI_AS NULL,
		[dr_typecode]      [char](2) COLLATE Latin1_General_CI_AS NULL,
		[dr_type]          [char](32) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__driver__01AE93E23C163465]
		PRIMARY KEY
		CLUSTERED
		([dr_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_branc__16E5B581]
	DEFAULT (NULL) FOR [dr_branch]
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_numbe__17D9D9BA]
	DEFAULT (NULL) FOR [dr_number]
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_name__18CDFDF3]
	DEFAULT (NULL) FOR [dr_name]
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_depot__19C2222C]
	DEFAULT (NULL) FOR [dr_depot]
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_wareh__1AB64665]
	DEFAULT (NULL) FOR [dr_warehouse]
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_agent__1BAA6A9E]
	DEFAULT ('') FOR [dr_agentname]
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_typec__1C9E8ED7]
	DEFAULT (NULL) FOR [dr_typecode]
GO
ALTER TABLE [dbo].[driver]
	ADD
	CONSTRAINT [DF__driver__dr_type__1D92B310]
	DEFAULT (NULL) FOR [dr_type]
GO
ALTER TABLE [dbo].[driver]
	WITH NOCHECK
	ADD CONSTRAINT [FK_driver_branch]
	FOREIGN KEY ([dr_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[driver]
	NOCHECK CONSTRAINT [FK_driver_branch]

GO
CREATE NONCLUSTERED INDEX [idx_Driver_NumberBranch]
	ON [dbo].[driver] ([dr_branch], [dr_number])
	INCLUDE ([dr_id], [dr_name])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[driver] SET (LOCK_ESCALATION = TABLE)
GO
