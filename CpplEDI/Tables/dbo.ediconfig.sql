SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ediconfig] (
		[ec_parameter]       [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
		[ec_style]           [char](16) COLLATE Latin1_General_CI_AS NULL,
		[ec_section]         [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[ec_description]     [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[ec_value]           [varchar](256) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ediconfig]
	ADD
	CONSTRAINT [DF__ediconfig__ec_st__1F7AFB82]
	DEFAULT ('string') FOR [ec_style]
GO
ALTER TABLE [dbo].[ediconfig]
	ADD
	CONSTRAINT [DF__ediconfig__ec_se__206F1FBB]
	DEFAULT ('') FOR [ec_section]
GO
ALTER TABLE [dbo].[ediconfig]
	ADD
	CONSTRAINT [DF__ediconfig__ec_de__216343F4]
	DEFAULT ('') FOR [ec_description]
GO
ALTER TABLE [dbo].[ediconfig]
	ADD
	CONSTRAINT [DF__ediconfig__ec_va__2257682D]
	DEFAULT ('') FOR [ec_value]
GO
ALTER TABLE [dbo].[ediconfig] SET (LOCK_ESCALATION = TABLE)
GO
