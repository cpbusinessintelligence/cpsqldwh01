SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[enquiry] (
		[srno]              [int] NOT NULL,
		[pname]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[pcompanyno]        [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[couponno]          [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[type]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[status]            [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[sdatetime]         [datetime] NULL,
		[edatetime]         [datetime] NULL,
		[dhours]            [int] NOT NULL,
		[dmin]              [int] NOT NULL,
		[pdriver]           [int] NOT NULL,
		[pdriverbranch]     [int] NULL,
		[contact]           [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[padd1]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[padd2]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[padd3]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[psub]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[ppcode]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[dadd1]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[dadd2]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[dadd3]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[dsub]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[dpcode]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[dname]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[dcompanyno]        [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[ddriver]           [int] NOT NULL,
		[ddriverbranch]     [int] NULL,
		[name]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[branch]            [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[callyn]            [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[lastcall]          [datetime] NOT NULL,
		[goodd]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[csr]               [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__enquiry__36B150C6E762A7BF]
		PRIMARY KEY
		CLUSTERED
		([srno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__pname__4BE691FF]
	DEFAULT ('') FOR [pname]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__pcompan__4CDAB638]
	DEFAULT ('') FOR [pcompanyno]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__couponn__4DCEDA71]
	DEFAULT ('') FOR [couponno]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__type__4EC2FEAA]
	DEFAULT ('') FOR [type]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__status__4FB722E3]
	DEFAULT ('') FOR [status]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__sdateti__50AB471C]
	DEFAULT (NULL) FOR [sdatetime]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__edateti__519F6B55]
	DEFAULT (NULL) FOR [edatetime]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dhours__52938F8E]
	DEFAULT ('0') FOR [dhours]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dmin__5387B3C7]
	DEFAULT ('0') FOR [dmin]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__pdriver__547BD800]
	DEFAULT ('0') FOR [pdriver]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__pdriver__556FFC39]
	DEFAULT ('0') FOR [pdriverbranch]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__contact__56642072]
	DEFAULT ('') FOR [contact]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__padd1__575844AB]
	DEFAULT ('') FOR [padd1]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__padd2__584C68E4]
	DEFAULT ('') FOR [padd2]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__padd3__59408D1D]
	DEFAULT ('') FOR [padd3]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__psub__5A34B156]
	DEFAULT ('') FOR [psub]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__ppcode__5B28D58F]
	DEFAULT ('') FOR [ppcode]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dadd1__5C1CF9C8]
	DEFAULT ('') FOR [dadd1]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dadd2__5D111E01]
	DEFAULT ('') FOR [dadd2]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dadd3__5E05423A]
	DEFAULT ('') FOR [dadd3]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dsub__5EF96673]
	DEFAULT ('') FOR [dsub]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dpcode__5FED8AAC]
	DEFAULT ('') FOR [dpcode]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dname__60E1AEE5]
	DEFAULT ('') FOR [dname]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__dcompan__61D5D31E]
	DEFAULT ('') FOR [dcompanyno]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__ddriver__62C9F757]
	DEFAULT ('0') FOR [ddriver]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__ddriver__63BE1B90]
	DEFAULT ('0') FOR [ddriverbranch]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__name__64B23FC9]
	DEFAULT ('') FOR [name]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__branch__65A66402]
	DEFAULT ('') FOR [branch]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__callyn__669A883B]
	DEFAULT ('0') FOR [callyn]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__lastcal__678EAC74]
	DEFAULT ('1900-01-01 00:00:00') FOR [lastcall]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__goodd__6882D0AD]
	DEFAULT ('') FOR [goodd]
GO
ALTER TABLE [dbo].[enquiry]
	ADD
	CONSTRAINT [DF__enquiry__csr__6976F4E6]
	DEFAULT ('') FOR [csr]
GO
ALTER TABLE [dbo].[enquiry]
	WITH NOCHECK
	ADD CONSTRAINT [FK_enquiry_driver_pdriver]
	FOREIGN KEY ([pdriver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[enquiry]
	NOCHECK CONSTRAINT [FK_enquiry_driver_pdriver]

GO
ALTER TABLE [dbo].[enquiry]
	WITH NOCHECK
	ADD CONSTRAINT [FK_enquiry_driver_pdriverbranch]
	FOREIGN KEY ([pdriverbranch]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[enquiry]
	NOCHECK CONSTRAINT [FK_enquiry_driver_pdriverbranch]

GO
ALTER TABLE [dbo].[enquiry]
	WITH NOCHECK
	ADD CONSTRAINT [FK_enquiry_driver_ddriver]
	FOREIGN KEY ([ddriver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[enquiry]
	NOCHECK CONSTRAINT [FK_enquiry_driver_ddriver]

GO
ALTER TABLE [dbo].[enquiry]
	WITH NOCHECK
	ADD CONSTRAINT [FK_enquiry_driver_ddriverbranch]
	FOREIGN KEY ([ddriverbranch]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[enquiry]
	NOCHECK CONSTRAINT [FK_enquiry_driver_ddriverbranch]

GO
ALTER TABLE [dbo].[enquiry] SET (LOCK_ESCALATION = TABLE)
GO
