SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[enquiry_temp] (
		[srno]              [int] NOT NULL,
		[pname]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[pcompanyno]        [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[couponno]          [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[type]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[status]            [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[sdatetime]         [datetime] NULL,
		[edatetime]         [datetime] NULL,
		[dhours]            [int] NOT NULL,
		[dmin]              [int] NOT NULL,
		[pdriver]           [int] NOT NULL,
		[pdriverbranch]     [int] NULL,
		[contact]           [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[padd1]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[padd2]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[padd3]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[psub]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[ppcode]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[dadd1]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[dadd2]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[dadd3]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[dsub]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[dpcode]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[dname]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[dcompanyno]        [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[ddriver]           [int] NOT NULL,
		[ddriverbranch]     [int] NULL,
		[name]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[branch]            [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[callyn]            [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[lastcall]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[goodd]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[csr]               [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__enquiry___36B150C6D26673CB]
		PRIMARY KEY
		CLUSTERED
		([srno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[enquiry_temp] SET (LOCK_ESCALATION = TABLE)
GO
