SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[enquiryd] (
		[srno]        [int] NOT NULL,
		[date]        [smalldatetime] NULL,
		[time]        [time](7) NOT NULL,
		[csr]         [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[details]     [text] COLLATE Latin1_General_CI_AS NOT NULL,
		[action]      [text] COLLATE Latin1_General_CI_AS NOT NULL,
		[ensrno]      [int] NOT NULL,
		CONSTRAINT [PK__enquiryd__36B150C611CF440D]
		PRIMARY KEY
		CLUSTERED
		([srno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[enquiryd]
	ADD
	CONSTRAINT [DF__enquiryd__date__2533D4D8]
	DEFAULT (NULL) FOR [date]
GO
ALTER TABLE [dbo].[enquiryd]
	ADD
	CONSTRAINT [DF__enquiryd__time__2627F911]
	DEFAULT ('00:00:00') FOR [time]
GO
ALTER TABLE [dbo].[enquiryd]
	ADD
	CONSTRAINT [DF__enquiryd__csr__271C1D4A]
	DEFAULT ('') FOR [csr]
GO
ALTER TABLE [dbo].[enquiryd]
	ADD
	CONSTRAINT [DF__enquiryd__ensrno__28104183]
	DEFAULT ('0') FOR [ensrno]
GO
ALTER TABLE [dbo].[enquiryd] SET (LOCK_ESCALATION = TABLE)
GO
