SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[enquiryd_Archive2017] (
		[srno]        [int] NULL,
		[date]        [smalldatetime] NULL,
		[time]        [time](7) NULL,
		[csr]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[details]     [text] COLLATE Latin1_General_CI_AS NULL,
		[action]      [text] COLLATE Latin1_General_CI_AS NULL,
		[ensrno]      [int] NULL
)
GO
ALTER TABLE [dbo].[enquiryd_Archive2017] SET (LOCK_ESCALATION = TABLE)
GO
