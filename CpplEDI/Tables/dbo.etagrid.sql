SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[etagrid] (
		[eg_from]          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[eg_to]            [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[eg_postcode]      [int] NULL,
		[eg_suburb]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[eg_send]          [int] NOT NULL,
		[eg_leg1]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[eg_leg1_days]     [int] NULL,
		[eg_leg2]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[eg_leg2_days]     [int] NULL,
		[eg_leg3]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[eg_leg3_days]     [int] NULL,
		[eg_to_days]       [int] NULL
)
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_from__29F889F5]
	DEFAULT ('') FOR [eg_from]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_to__2AECAE2E]
	DEFAULT ('') FOR [eg_to]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_post__2BE0D267]
	DEFAULT ('0') FOR [eg_postcode]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_subu__2CD4F6A0]
	DEFAULT ('') FOR [eg_suburb]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_send__2DC91AD9]
	DEFAULT ('0') FOR [eg_send]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_leg1__2EBD3F12]
	DEFAULT ('') FOR [eg_leg1]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_leg1__2FB1634B]
	DEFAULT ('0') FOR [eg_leg1_days]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_leg2__30A58784]
	DEFAULT ('') FOR [eg_leg2]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_leg2__3199ABBD]
	DEFAULT ('0') FOR [eg_leg2_days]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_leg3__328DCFF6]
	DEFAULT ('') FOR [eg_leg3]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_leg3__3381F42F]
	DEFAULT ('0') FOR [eg_leg3_days]
GO
ALTER TABLE [dbo].[etagrid]
	ADD
	CONSTRAINT [DF__etagrid__eg_to_d__34761868]
	DEFAULT ('0') FOR [eg_to_days]
GO
ALTER TABLE [dbo].[etagrid] SET (LOCK_ESCALATION = TABLE)
GO
