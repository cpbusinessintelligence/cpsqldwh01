SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[etastate] (
		[es_zone]      [char](8) COLLATE Latin1_General_CI_AS NULL,
		[es_state]     [char](8) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[etastate]
	ADD
	CONSTRAINT [DF__etastate__es_zon__365E60DA]
	DEFAULT (NULL) FOR [es_zone]
GO
ALTER TABLE [dbo].[etastate]
	ADD
	CONSTRAINT [DF__etastate__es_sta__37528513]
	DEFAULT (NULL) FOR [es_state]
GO
ALTER TABLE [dbo].[etastate] SET (LOCK_ESCALATION = TABLE)
GO
