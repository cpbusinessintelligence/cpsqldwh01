SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[exceptionmap] (
		[em_id]            [int] NOT NULL,
		[em_style]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[em_string]        [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[em_status]        [varchar](8) COLLATE Latin1_General_CI_AS NOT NULL,
		[em_fullmatch]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__exceptio__ABD24D7F9327AE47]
		PRIMARY KEY
		CLUSTERED
		([em_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[exceptionmap]
	ADD
	CONSTRAINT [DF__exception__em_st__3EF3A6DB]
	DEFAULT ('D') FOR [em_style]
GO
ALTER TABLE [dbo].[exceptionmap]
	ADD
	CONSTRAINT [DF__exception__em_st__3FE7CB14]
	DEFAULT ('') FOR [em_string]
GO
ALTER TABLE [dbo].[exceptionmap]
	ADD
	CONSTRAINT [DF__exception__em_st__40DBEF4D]
	DEFAULT ('') FOR [em_status]
GO
ALTER TABLE [dbo].[exceptionmap]
	ADD
	CONSTRAINT [DF__exception__em_fu__41D01386]
	DEFAULT ('N') FOR [em_fullmatch]
GO
ALTER TABLE [dbo].[exceptionmap] SET (LOCK_ESCALATION = TABLE)
GO
