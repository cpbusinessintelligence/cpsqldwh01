SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[exports] (
		[e_id]               [int] NOT NULL,
		[e_stamp]            [datetime] NULL,
		[e_filename]         [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[e_consignments]     [int] NULL,
		[e_bookins]          [int] NULL,
		[e_class]            [int] NULL,
		[e_2nd]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__exports__3E2ED64A63CF4B99]
		PRIMARY KEY
		CLUSTERED
		([e_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[exports]
	ADD
	CONSTRAINT [DF__exports__e_stamp__44AC8031]
	DEFAULT (NULL) FOR [e_stamp]
GO
ALTER TABLE [dbo].[exports]
	ADD
	CONSTRAINT [DF__exports__e_filen__45A0A46A]
	DEFAULT ('') FOR [e_filename]
GO
ALTER TABLE [dbo].[exports]
	ADD
	CONSTRAINT [DF__exports__e_consi__4694C8A3]
	DEFAULT ('0') FOR [e_consignments]
GO
ALTER TABLE [dbo].[exports]
	ADD
	CONSTRAINT [DF__exports__e_booki__4788ECDC]
	DEFAULT ('0') FOR [e_bookins]
GO
ALTER TABLE [dbo].[exports]
	ADD
	CONSTRAINT [DF__exports__e_class__487D1115]
	DEFAULT ('0') FOR [e_class]
GO
ALTER TABLE [dbo].[exports]
	ADD
	CONSTRAINT [DF__exports__e_2nd__4971354E]
	DEFAULT ('N') FOR [e_2nd]
GO
ALTER TABLE [dbo].[exports] SET (LOCK_ESCALATION = TABLE)
GO
