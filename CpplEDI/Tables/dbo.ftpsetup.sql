SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ftpsetup] (
		[fs_user]            [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[fs_password]        [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[fs_description]     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[fs_uid]             [int] NULL,
		[fs_gid]             [int] NULL,
		[fs_dir]             [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__ftpsetup__656F05D15AA1CBD3]
		PRIMARY KEY
		CLUSTERED
		([fs_user])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ftpsetup]
	ADD
	CONSTRAINT [DF__ftpsetup__fs_use__4C4DA1F9]
	DEFAULT ('') FOR [fs_user]
GO
ALTER TABLE [dbo].[ftpsetup]
	ADD
	CONSTRAINT [DF__ftpsetup__fs_pas__4D41C632]
	DEFAULT ('') FOR [fs_password]
GO
ALTER TABLE [dbo].[ftpsetup]
	ADD
	CONSTRAINT [DF__ftpsetup__fs_des__4E35EA6B]
	DEFAULT (NULL) FOR [fs_description]
GO
ALTER TABLE [dbo].[ftpsetup]
	ADD
	CONSTRAINT [DF__ftpsetup__fs_uid__4F2A0EA4]
	DEFAULT ('32767') FOR [fs_uid]
GO
ALTER TABLE [dbo].[ftpsetup]
	ADD
	CONSTRAINT [DF__ftpsetup__fs_gid__501E32DD]
	DEFAULT ('32767') FOR [fs_gid]
GO
ALTER TABLE [dbo].[ftpsetup]
	ADD
	CONSTRAINT [DF__ftpsetup__fs_dir__51125716]
	DEFAULT ('/tmp') FOR [fs_dir]
GO
ALTER TABLE [dbo].[ftpsetup] SET (LOCK_ESCALATION = TABLE)
GO
