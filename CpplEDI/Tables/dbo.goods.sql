SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[goods] (
		[g_id]              [int] NOT NULL,
		[g_company_id]      [int] NULL,
		[g_user]            [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[g_description]     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[g_deadweight]      [float] NULL,
		[g_dimension0]      [float] NULL,
		[g_dimension1]      [float] NULL,
		[g_dimension2]      [float] NULL,
		[g_volume]          [float] NULL,
		CONSTRAINT [PK__goods__49FB61C4670D1F44]
		PRIMARY KEY
		CLUSTERED
		([g_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_company__53EEC3C1]
	DEFAULT ('0') FOR [g_company_id]
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_user__54E2E7FA]
	DEFAULT ('') FOR [g_user]
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_descrip__55D70C33]
	DEFAULT ('') FOR [g_description]
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_deadwei__56CB306C]
	DEFAULT (NULL) FOR [g_deadweight]
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_dimensi__57BF54A5]
	DEFAULT (NULL) FOR [g_dimension0]
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_dimensi__58B378DE]
	DEFAULT (NULL) FOR [g_dimension1]
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_dimensi__59A79D17]
	DEFAULT (NULL) FOR [g_dimension2]
GO
ALTER TABLE [dbo].[goods]
	ADD
	CONSTRAINT [DF__goods__g_volume__5A9BC150]
	DEFAULT (NULL) FOR [g_volume]
GO
ALTER TABLE [dbo].[goods]
	WITH NOCHECK
	ADD CONSTRAINT [FK_goods_company]
	FOREIGN KEY ([g_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[goods]
	NOCHECK CONSTRAINT [FK_goods_company]

GO
ALTER TABLE [dbo].[goods] SET (LOCK_ESCALATION = TABLE)
GO
