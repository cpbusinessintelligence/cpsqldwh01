SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[groupname] (
		[groupname]       [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[description]     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__groupnam__ED1647CDE1DC5C2E]
		PRIMARY KEY
		CLUSTERED
		([groupname])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[groupname]
	ADD
	CONSTRAINT [DF__groupname__group__5D782DFB]
	DEFAULT ('') FOR [groupname]
GO
ALTER TABLE [dbo].[groupname]
	ADD
	CONSTRAINT [DF__groupname__descr__5E6C5234]
	DEFAULT (NULL) FOR [description]
GO
ALTER TABLE [dbo].[groupname] SET (LOCK_ESCALATION = TABLE)
GO
