SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[groups] (
		[account]     [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[groups]      [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__groups__5FC00A1BD66798AE]
		PRIMARY KEY
		CLUSTERED
		([account], [groups])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[groups]
	ADD
	CONSTRAINT [DF__groups__account__6148BEDF]
	DEFAULT ('') FOR [account]
GO
ALTER TABLE [dbo].[groups]
	ADD
	CONSTRAINT [DF__groups__groups__623CE318]
	DEFAULT ('') FOR [groups]
GO
ALTER TABLE [dbo].[groups] SET (LOCK_ESCALATION = TABLE)
GO
