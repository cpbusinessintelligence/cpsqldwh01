SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[guessSSCC] (
		[gs_portion]        [char](7) COLLATE Latin1_General_CI_AS NULL,
		[gs_name]           [char](32) COLLATE Latin1_General_CI_AS NULL,
		[gs_company_id]     [int] NULL
)
GO
ALTER TABLE [dbo].[guessSSCC]
	ADD
	CONSTRAINT [DF__guessSSCC__gs_po__64252B8A]
	DEFAULT (NULL) FOR [gs_portion]
GO
ALTER TABLE [dbo].[guessSSCC]
	ADD
	CONSTRAINT [DF__guessSSCC__gs_na__65194FC3]
	DEFAULT (NULL) FOR [gs_name]
GO
ALTER TABLE [dbo].[guessSSCC]
	ADD
	CONSTRAINT [DF__guessSSCC__gs_co__660D73FC]
	DEFAULT (NULL) FOR [gs_company_id]
GO
ALTER TABLE [dbo].[guessSSCC]
	WITH NOCHECK
	ADD CONSTRAINT [FK_guessSSCC_company]
	FOREIGN KEY ([gs_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[guessSSCC]
	NOCHECK CONSTRAINT [FK_guessSSCC_company]

GO
ALTER TABLE [dbo].[guessSSCC] SET (LOCK_ESCALATION = TABLE)
GO
