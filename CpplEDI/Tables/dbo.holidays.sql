SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[holidays] (
		[h_id]          [int] NOT NULL,
		[h_state]       [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[h_style]       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[h_holiday]     [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[h_name]        [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__holidays__430F8EDB74ECE40E]
		PRIMARY KEY
		CLUSTERED
		([h_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[holidays]
	ADD
	CONSTRAINT [DF__holidays__h_stat__68E9E0A7]
	DEFAULT ('ALL') FOR [h_state]
GO
ALTER TABLE [dbo].[holidays]
	ADD
	CONSTRAINT [DF__holidays__h_styl__69DE04E0]
	DEFAULT ('S') FOR [h_style]
GO
ALTER TABLE [dbo].[holidays]
	ADD
	CONSTRAINT [DF__holidays__h_holi__6AD22919]
	DEFAULT ('') FOR [h_holiday]
GO
ALTER TABLE [dbo].[holidays]
	ADD
	CONSTRAINT [DF__holidays__h_name__6BC64D52]
	DEFAULT ('') FOR [h_name]
GO
ALTER TABLE [dbo].[holidays] SET (LOCK_ESCALATION = TABLE)
GO
