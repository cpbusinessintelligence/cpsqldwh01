SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iccompany] (
		[ic_importconfig]     [int] NOT NULL,
		[ic_order]            [int] NOT NULL,
		[ic_match]            [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[ic_company_id]       [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[iccompany]
	ADD
	CONSTRAINT [DF__iccompany__ic_im__6DAE95C4]
	DEFAULT ('0') FOR [ic_importconfig]
GO
ALTER TABLE [dbo].[iccompany]
	ADD
	CONSTRAINT [DF__iccompany__ic_or__6EA2B9FD]
	DEFAULT ('0') FOR [ic_order]
GO
ALTER TABLE [dbo].[iccompany]
	ADD
	CONSTRAINT [DF__iccompany__ic_ma__6F96DE36]
	DEFAULT ('') FOR [ic_match]
GO
ALTER TABLE [dbo].[iccompany]
	ADD
	CONSTRAINT [DF__iccompany__ic_co__708B026F]
	DEFAULT ('') FOR [ic_company_id]
GO
ALTER TABLE [dbo].[iccompany] SET (LOCK_ESCALATION = TABLE)
GO
