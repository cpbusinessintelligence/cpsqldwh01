SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[icfile] (
		[if_importconfig]     [int] NOT NULL,
		[if_order]            [int] NOT NULL,
		[if_match]            [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[icfile]
	ADD
	CONSTRAINT [DF__icfile__if_impor__72734AE1]
	DEFAULT ('0') FOR [if_importconfig]
GO
ALTER TABLE [dbo].[icfile]
	ADD
	CONSTRAINT [DF__icfile__if_order__73676F1A]
	DEFAULT ('0') FOR [if_order]
GO
ALTER TABLE [dbo].[icfile]
	ADD
	CONSTRAINT [DF__icfile__if_match__745B9353]
	DEFAULT ('') FOR [if_match]
GO
ALTER TABLE [dbo].[icfile] SET (LOCK_ESCALATION = TABLE)
GO
