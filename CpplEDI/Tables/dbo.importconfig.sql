SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[importconfig] (
		[ic_id]                          [int] NOT NULL,
		[ic_description]                 [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[ic_company_id]                  [int] NOT NULL,
		[ic_mechanism]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_style]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_format]                      [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[ic_default_insurance]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_force_default_insurance]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_unique_connote]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_unique_timeout]              [int] NULL,
		[ic_update_connote]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_update_timeout]              [int] NULL,
		[ic_import_to_ezyfreight]        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_validate_import]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_ftp_host]                    [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[ic_ftp_user]                    [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[ic_ftp_password]                [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[ic_ftp_directory]               [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[ic_ftp_archive]                 [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[ic_frequency]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_default_addr0]               [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ic_default_addr1]               [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ic_default_addr2]               [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ic_default_addr3]               [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ic_default_suburb]              [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[ic_default_postcode]            [int] NULL,
		[ic_default_always]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_default_atl]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_lastlog]                     [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[ic_laststamp]                   [datetime] NULL,
		[ic_lastok]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_lastfile]                    [varchar](128) COLLATE Latin1_General_CI_AS NULL,
		[ic_lastfilestamp]               [datetime] NULL,
		[ic_default_weight]              [float] NULL,
		[ic_default_volume]              [float] NULL,
		[ic_default_per_item]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_book_pickups]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_email_recipient]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_missing_alert]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ic_missing_time]                [int] NULL,
		[ic_missing_email]               [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ic_missing_name]                [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ic_missing_checked]             [datetime] NULL,
		CONSTRAINT [PK__importco__28FBF46C987468ED]
		PRIMARY KEY
		CLUSTERED
		([ic_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__1D5DA8E6]
	DEFAULT ('') FOR [ic_description]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_co__1E51CD1F]
	DEFAULT ('0') FOR [ic_company_id]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_me__1F45F158]
	DEFAULT ('F') FOR [ic_mechanism]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_st__203A1591]
	DEFAULT ('C') FOR [ic_style]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_fo__212E39CA]
	DEFAULT ('') FOR [ic_format]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__22225E03]
	DEFAULT ('0') FOR [ic_default_insurance]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_fo__2316823C]
	DEFAULT ('0') FOR [ic_force_default_insurance]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_un__240AA675]
	DEFAULT ('N') FOR [ic_unique_connote]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_un__24FECAAE]
	DEFAULT ('366') FOR [ic_unique_timeout]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_up__25F2EEE7]
	DEFAULT ('N') FOR [ic_update_connote]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_up__26E71320]
	DEFAULT ('14') FOR [ic_update_timeout]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_im__27DB3759]
	DEFAULT ('N') FOR [ic_import_to_ezyfreight]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_va__28CF5B92]
	DEFAULT ('N') FOR [ic_validate_import]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_ft__29C37FCB]
	DEFAULT (NULL) FOR [ic_ftp_host]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_ft__2AB7A404]
	DEFAULT (NULL) FOR [ic_ftp_user]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_ft__2BABC83D]
	DEFAULT (NULL) FOR [ic_ftp_password]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_ft__2C9FEC76]
	DEFAULT (NULL) FOR [ic_ftp_directory]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_ft__2D9410AF]
	DEFAULT (NULL) FOR [ic_ftp_archive]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_fr__2E8834E8]
	DEFAULT ('A') FOR [ic_frequency]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__2F7C5921]
	DEFAULT ('') FOR [ic_default_addr0]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__30707D5A]
	DEFAULT ('') FOR [ic_default_addr1]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__3164A193]
	DEFAULT ('') FOR [ic_default_addr2]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__3258C5CC]
	DEFAULT ('') FOR [ic_default_addr3]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__334CEA05]
	DEFAULT ('') FOR [ic_default_suburb]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__34410E3E]
	DEFAULT ('0') FOR [ic_default_postcode]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__35353277]
	DEFAULT ('N') FOR [ic_default_always]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__362956B0]
	DEFAULT ('N') FOR [ic_default_atl]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_la__371D7AE9]
	DEFAULT (NULL) FOR [ic_lastlog]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_la__38119F22]
	DEFAULT (NULL) FOR [ic_laststamp]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_la__3905C35B]
	DEFAULT ('Y') FOR [ic_lastok]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_la__39F9E794]
	DEFAULT (NULL) FOR [ic_lastfile]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_la__3AEE0BCD]
	DEFAULT (NULL) FOR [ic_lastfilestamp]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__3BE23006]
	DEFAULT ('0') FOR [ic_default_weight]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__3CD6543F]
	DEFAULT ('0') FOR [ic_default_volume]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_de__3DCA7878]
	DEFAULT ('N') FOR [ic_default_per_item]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_bo__3EBE9CB1]
	DEFAULT ('N') FOR [ic_book_pickups]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_em__3FB2C0EA]
	DEFAULT ('N') FOR [ic_email_recipient]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_mi__40A6E523]
	DEFAULT ('N') FOR [ic_missing_alert]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_mi__419B095C]
	DEFAULT ('0') FOR [ic_missing_time]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_mi__428F2D95]
	DEFAULT ('') FOR [ic_missing_email]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_mi__438351CE]
	DEFAULT ('') FOR [ic_missing_name]
GO
ALTER TABLE [dbo].[importconfig]
	ADD
	CONSTRAINT [DF__importcon__ic_mi__44777607]
	DEFAULT (NULL) FOR [ic_missing_checked]
GO
ALTER TABLE [dbo].[importconfig]
	WITH NOCHECK
	ADD CONSTRAINT [FK_importconfig_company]
	FOREIGN KEY ([ic_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[importconfig]
	NOCHECK CONSTRAINT [FK_importconfig_company]

GO
ALTER TABLE [dbo].[importconfig] SET (LOCK_ESCALATION = TABLE)
GO
