SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[imports] (
		[i_id]               [int] NOT NULL,
		[i_style]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[i_company]          [int] NULL,
		[i_stamp]            [datetime] NULL,
		[i_filename]         [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[i_consignments]     [int] NULL,
		[i_references]       [int] NULL,
		[i_coupons]          [int] NULL,
		[i_bookins]          [int] NULL,
		[i_date]             [smalldatetime] NULL,
		[i_manifest_id]      [int] NULL,
		CONSTRAINT [PK__imports__98F919BA6D3A590F]
		PRIMARY KEY
		CLUSTERED
		([i_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_style__6D1A1A2E]
	DEFAULT ('C') FOR [i_style]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_compa__6E0E3E67]
	DEFAULT (NULL) FOR [i_company]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_stamp__6F0262A0]
	DEFAULT (NULL) FOR [i_stamp]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_filen__6FF686D9]
	DEFAULT (NULL) FOR [i_filename]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_consi__70EAAB12]
	DEFAULT (NULL) FOR [i_consignments]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_refer__71DECF4B]
	DEFAULT (NULL) FOR [i_references]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_coupo__72D2F384]
	DEFAULT (NULL) FOR [i_coupons]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_booki__73C717BD]
	DEFAULT (NULL) FOR [i_bookins]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_date__74BB3BF6]
	DEFAULT (NULL) FOR [i_date]
GO
ALTER TABLE [dbo].[imports]
	ADD
	CONSTRAINT [DF__imports__i_manif__75AF602F]
	DEFAULT ('0') FOR [i_manifest_id]
GO
ALTER TABLE [dbo].[imports]
	WITH NOCHECK
	ADD CONSTRAINT [FK_imports_i_consignments]
	FOREIGN KEY ([i_consignments]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[imports]
	NOCHECK CONSTRAINT [FK_imports_i_consignments]

GO
ALTER TABLE [dbo].[imports]
	WITH NOCHECK
	ADD CONSTRAINT [FK_imports_company]
	FOREIGN KEY ([i_company]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[imports]
	NOCHECK CONSTRAINT [FK_imports_company]

GO
ALTER TABLE [dbo].[imports] SET (LOCK_ESCALATION = TABLE)
GO
