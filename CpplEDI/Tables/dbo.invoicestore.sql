SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[invoicestore] (
		[is_id]            [int] NOT NULL,
		[is_account]       [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[is_invoiceno]     [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[is_date]          [smalldatetime] NULL,
		[is_imported]      [datetime] NULL,
		CONSTRAINT [PK__invoices__ADF81AD3323972CD]
		PRIMARY KEY
		CLUSTERED
		([is_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[invoicestore]
	ADD
	CONSTRAINT [DF__invoicest__is_ac__7737FFFE]
	DEFAULT ('') FOR [is_account]
GO
ALTER TABLE [dbo].[invoicestore]
	ADD
	CONSTRAINT [DF__invoicest__is_in__782C2437]
	DEFAULT ('') FOR [is_invoiceno]
GO
ALTER TABLE [dbo].[invoicestore]
	ADD
	CONSTRAINT [DF__invoicest__is_da__79204870]
	DEFAULT (NULL) FOR [is_date]
GO
ALTER TABLE [dbo].[invoicestore]
	ADD
	CONSTRAINT [DF__invoicest__is_im__7A146CA9]
	DEFAULT (NULL) FOR [is_imported]
GO
ALTER TABLE [dbo].[invoicestore] SET (LOCK_ESCALATION = TABLE)
GO
