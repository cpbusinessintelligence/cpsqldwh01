SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[irp] (
		[irp_id]                         [int] NOT NULL,
		[irp_consignment]                [int] NOT NULL,
		[irp_delivery_branch]            [int] NOT NULL,
		[irp_delivery_phone]             [char](32) COLLATE Latin1_General_CI_AS NULL,
		[irp_delivery_details]           [char](255) COLLATE Latin1_General_CI_AS NULL,
		[irp_delivery_suburb_string]     [char](40) COLLATE Latin1_General_CI_AS NULL,
		[irp_delivery_suburb]            [int] NULL,
		[irp_pickup_branch]              [int] NOT NULL,
		[irp_pickup_phone]               [char](32) COLLATE Latin1_General_CI_AS NULL,
		[irp_pickup_details]             [char](255) COLLATE Latin1_General_CI_AS NULL,
		[irp_pickup_suburb_string]       [char](40) COLLATE Latin1_General_CI_AS NULL,
		[irp_pickup_suburb]              [int] NULL,
		[irp_pickup_contact]             [char](32) COLLATE Latin1_General_CI_AS NULL,
		[irp_pickup_location]            [char](32) COLLATE Latin1_General_CI_AS NULL,
		[irp_pickup_date]                [smalldatetime] NULL,
		[irp_pickup_time]                [time](7) NULL,
		[irp_job_no]                     [int] NULL,
		[irp_job_date]                   [smalldatetime] NULL,
		[irp_coupon]                     [char](32) COLLATE Latin1_General_CI_AS NULL,
		[irp_coupon_id]                  [int] NULL,
		[irp_tracker]                    [char](32) COLLATE Latin1_General_CI_AS NULL,
		[irp_tracker_id]                 [int] NULL,
		[irp_dimension0]                 [int] NULL,
		[irp_dimension1]                 [int] NULL,
		[irp_dimension2]                 [int] NULL,
		[irp_cube]                       [int] NULL,
		[irp_deadweight]                 [int] NULL,
		[irp_deleted]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__irp__249BBCD2661BDA17]
		PRIMARY KEY
		CLUSTERED
		([irp_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_deliver__00C16A38]
	DEFAULT (NULL) FOR [irp_delivery_suburb_string]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_deliver__01B58E71]
	DEFAULT ('0') FOR [irp_delivery_suburb]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___02A9B2AA]
	DEFAULT ('0') FOR [irp_pickup_branch]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___039DD6E3]
	DEFAULT ('') FOR [irp_pickup_phone]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___0491FB1C]
	DEFAULT ('') FOR [irp_pickup_details]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___05861F55]
	DEFAULT (NULL) FOR [irp_pickup_suburb_string]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___067A438E]
	DEFAULT ('0') FOR [irp_pickup_suburb]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___076E67C7]
	DEFAULT ('') FOR [irp_pickup_contact]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___08628C00]
	DEFAULT ('') FOR [irp_pickup_location]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___0956B039]
	DEFAULT (NULL) FOR [irp_pickup_date]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_pickup___0A4AD472]
	DEFAULT ('00:00:00') FOR [irp_pickup_time]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_job_no__0B3EF8AB]
	DEFAULT ('0') FOR [irp_job_no]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_job_dat__0C331CE4]
	DEFAULT (NULL) FOR [irp_job_date]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_coupon__0D27411D]
	DEFAULT ('') FOR [irp_coupon]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_coupon___0E1B6556]
	DEFAULT ('0') FOR [irp_coupon_id]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_tracker__0F0F898F]
	DEFAULT ('') FOR [irp_tracker]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_tracker__1003ADC8]
	DEFAULT ('0') FOR [irp_tracker_id]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_dimensi__10F7D201]
	DEFAULT ('0') FOR [irp_dimension0]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_dimensi__11EBF63A]
	DEFAULT ('0') FOR [irp_dimension1]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_dimensi__12E01A73]
	DEFAULT ('0') FOR [irp_dimension2]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_cube__13D43EAC]
	DEFAULT ('0') FOR [irp_cube]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_deadwei__14C862E5]
	DEFAULT ('0') FOR [irp_deadweight]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_deleted__15BC871E]
	DEFAULT ('N') FOR [irp_deleted]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_consign__7CF0D954]
	DEFAULT ('0') FOR [irp_consignment]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_deliver__7DE4FD8D]
	DEFAULT ('0') FOR [irp_delivery_branch]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_deliver__7ED921C6]
	DEFAULT ('') FOR [irp_delivery_phone]
GO
ALTER TABLE [dbo].[irp]
	ADD
	CONSTRAINT [DF__irp__irp_deliver__7FCD45FF]
	DEFAULT ('') FOR [irp_delivery_details]
GO
ALTER TABLE [dbo].[irp]
	WITH NOCHECK
	ADD CONSTRAINT [FK_irp_irp_pickup_branch]
	FOREIGN KEY ([irp_pickup_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[irp]
	NOCHECK CONSTRAINT [FK_irp_irp_pickup_branch]

GO
ALTER TABLE [dbo].[irp]
	WITH NOCHECK
	ADD CONSTRAINT [FK_irp_irp_consignment]
	FOREIGN KEY ([irp_consignment]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[irp]
	NOCHECK CONSTRAINT [FK_irp_irp_consignment]

GO
ALTER TABLE [dbo].[irp]
	WITH NOCHECK
	ADD CONSTRAINT [FK_irp_irp_delivery]
	FOREIGN KEY ([irp_delivery_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[irp]
	NOCHECK CONSTRAINT [FK_irp_irp_delivery]

GO
ALTER TABLE [dbo].[irp] SET (LOCK_ESCALATION = TABLE)
GO
