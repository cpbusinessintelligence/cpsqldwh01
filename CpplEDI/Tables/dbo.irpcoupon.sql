SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[irpcoupon] (
		[ic_irp_id]        [int] NOT NULL,
		[ic_coupon]        [char](32) COLLATE Latin1_General_CI_AS NULL,
		[ic_coupon_id]     [int] NULL,
		[ic_tracker]       [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[irpcoupon]
	ADD
	CONSTRAINT [DF__irpcoupon__ic_ir__17A4CF90]
	DEFAULT ('0') FOR [ic_irp_id]
GO
ALTER TABLE [dbo].[irpcoupon]
	ADD
	CONSTRAINT [DF__irpcoupon__ic_co__1898F3C9]
	DEFAULT ('') FOR [ic_coupon]
GO
ALTER TABLE [dbo].[irpcoupon]
	ADD
	CONSTRAINT [DF__irpcoupon__ic_co__198D1802]
	DEFAULT ('0') FOR [ic_coupon_id]
GO
ALTER TABLE [dbo].[irpcoupon]
	ADD
	CONSTRAINT [DF__irpcoupon__ic_tr__1A813C3B]
	DEFAULT ('Y') FOR [ic_tracker]
GO
ALTER TABLE [dbo].[irpcoupon] SET (LOCK_ESCALATION = TABLE)
GO
