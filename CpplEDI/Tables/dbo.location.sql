SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[location] (
		[l_number]       [int] NOT NULL,
		[l_branch]       [char](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[l_name]         [char](128) COLLATE Latin1_General_CI_AS NULL,
		[l_custid]       [int] NULL,
		[l_custname]     [char](128) COLLATE Latin1_General_CI_AS NULL,
		[l_update]       [datetime] NULL,
		CONSTRAINT [PK__location__03F2B475613C8947]
		PRIMARY KEY
		CLUSTERED
		([l_branch], [l_number])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[location]
	ADD
	CONSTRAINT [DF__location__l_numb__4D0CBC08]
	DEFAULT ('0') FOR [l_number]
GO
ALTER TABLE [dbo].[location]
	ADD
	CONSTRAINT [DF__location__l_bran__4E00E041]
	DEFAULT ('') FOR [l_branch]
GO
ALTER TABLE [dbo].[location]
	ADD
	CONSTRAINT [DF__location__l_name__4EF5047A]
	DEFAULT ('') FOR [l_name]
GO
ALTER TABLE [dbo].[location]
	ADD
	CONSTRAINT [DF__location__l_cust__4FE928B3]
	DEFAULT ('0') FOR [l_custid]
GO
ALTER TABLE [dbo].[location]
	ADD
	CONSTRAINT [DF__location__l_cust__50DD4CEC]
	DEFAULT ('') FOR [l_custname]
GO
ALTER TABLE [dbo].[location]
	ADD
	CONSTRAINT [DF__location__l_upda__51D17125]
	DEFAULT ('0000-00-00 00:00:00') FOR [l_update]
GO
ALTER TABLE [dbo].[location] SET (LOCK_ESCALATION = TABLE)
GO
