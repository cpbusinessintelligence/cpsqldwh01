SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[login] (
		[username]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[password]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[branch]       [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[level]        [int] NOT NULL,
		CONSTRAINT [PK__login__F3DBC573DB97F04E]
		PRIMARY KEY
		CLUSTERED
		([username])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[login]
	ADD
	CONSTRAINT [DF__login__username__4753E2B2]
	DEFAULT ('') FOR [username]
GO
ALTER TABLE [dbo].[login]
	ADD
	CONSTRAINT [DF__login__password__484806EB]
	DEFAULT ('') FOR [password]
GO
ALTER TABLE [dbo].[login]
	ADD
	CONSTRAINT [DF__login__branch__493C2B24]
	DEFAULT ('') FOR [branch]
GO
ALTER TABLE [dbo].[login]
	ADD
	CONSTRAINT [DF__login__level__4A304F5D]
	DEFAULT ('0') FOR [level]
GO
ALTER TABLE [dbo].[login] SET (LOCK_ESCALATION = TABLE)
GO
