SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[manifest] (
		[m_id]               [int] NOT NULL,
		[m_date]             [smalldatetime] NULL,
		[m_company_id]       [int] NOT NULL,
		[m_description]      [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[m_comments]         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[m_consignments]     [int] NULL,
		[m_stamp]            [datetime] NULL,
		[m_class]            [int] NOT NULL,
		[m_import_id]        [int] NULL,
		[m_released]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[m_printed]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__manifest__7C8D7D294630D700]
		PRIMARY KEY
		CLUSTERED
		([m_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_date__54ADDDD0]
	DEFAULT (NULL) FOR [m_date]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_comp__55A20209]
	DEFAULT ('0') FOR [m_company_id]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_desc__56962642]
	DEFAULT ('') FOR [m_description]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_comm__578A4A7B]
	DEFAULT ('') FOR [m_comments]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_cons__587E6EB4]
	DEFAULT ('0') FOR [m_consignments]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_stam__597292ED]
	DEFAULT (NULL) FOR [m_stamp]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_clas__5A66B726]
	DEFAULT ('0') FOR [m_class]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_impo__5B5ADB5F]
	DEFAULT (NULL) FOR [m_import_id]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_rele__5C4EFF98]
	DEFAULT ('N') FOR [m_released]
GO
ALTER TABLE [dbo].[manifest]
	ADD
	CONSTRAINT [DF__manifest__m_prin__5D4323D1]
	DEFAULT ('N') FOR [m_printed]
GO
ALTER TABLE [dbo].[manifest]
	WITH NOCHECK
	ADD CONSTRAINT [FK_manifest_company]
	FOREIGN KEY ([m_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[manifest]
	NOCHECK CONSTRAINT [FK_manifest_company]

GO
ALTER TABLE [dbo].[manifest]
	WITH NOCHECK
	ADD CONSTRAINT [FK_manifest_m_consignments]
	FOREIGN KEY ([m_consignments]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[manifest]
	NOCHECK CONSTRAINT [FK_manifest_m_consignments]

GO
CREATE NONCLUSTERED INDEX [idx_manifest_idreleasedstampdate]
	ON [dbo].[manifest] ([m_id], [m_date], [m_stamp], [m_released])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[manifest] SET (LOCK_ESCALATION = TABLE)
GO
