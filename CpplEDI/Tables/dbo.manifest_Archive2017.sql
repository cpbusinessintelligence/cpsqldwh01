SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[manifest_Archive2017] (
		[m_id]               [int] NULL,
		[m_date]             [smalldatetime] NULL,
		[m_company_id]       [int] NULL,
		[m_description]      [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[m_comments]         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[m_consignments]     [int] NULL,
		[m_stamp]            [datetime] NULL,
		[m_class]            [int] NULL,
		[m_import_id]        [int] NULL,
		[m_released]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[m_printed]          [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[manifest_Archive2017] SET (LOCK_ESCALATION = TABLE)
GO
