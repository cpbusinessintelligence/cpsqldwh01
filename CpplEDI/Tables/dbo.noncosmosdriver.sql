SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[noncosmosdriver] (
		[nc_branch]          [int] NOT NULL,
		[nc_contractor]      [int] NOT NULL,
		[nc_description]     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__noncosmo__3957D8399EEBAD5C]
		PRIMARY KEY
		CLUSTERED
		([nc_branch], [nc_contractor])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[noncosmosdriver]
	ADD
	CONSTRAINT [DF__noncosmos__nc_br__601F907C]
	DEFAULT ('0') FOR [nc_branch]
GO
ALTER TABLE [dbo].[noncosmosdriver]
	ADD
	CONSTRAINT [DF__noncosmos__nc_co__6113B4B5]
	DEFAULT ('0') FOR [nc_contractor]
GO
ALTER TABLE [dbo].[noncosmosdriver]
	ADD
	CONSTRAINT [DF__noncosmos__nc_de__6207D8EE]
	DEFAULT (NULL) FOR [nc_description]
GO
ALTER TABLE [dbo].[noncosmosdriver]
	WITH NOCHECK
	ADD CONSTRAINT [FK_noncosmosdriver_nc_branch]
	FOREIGN KEY ([nc_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[noncosmosdriver]
	NOCHECK CONSTRAINT [FK_noncosmosdriver_nc_branch]

GO
ALTER TABLE [dbo].[noncosmosdriver] SET (LOCK_ESCALATION = TABLE)
GO
