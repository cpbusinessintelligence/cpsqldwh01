SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ogmanifest] (
		[ogm_id]          [int] NOT NULL,
		[ogm_date]        [smalldatetime] NULL,
		[ogm_stamp]       [datetime] NULL,
		[ogm_class]       [int] NOT NULL,
		[ogm_agent]       [int] NOT NULL,
		[ogm_count]       [int] NULL,
		[ogm_returns]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__ogmanife__47AC10B2CDAA10E1]
		PRIMARY KEY
		CLUSTERED
		([ogm_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ogmanifest]
	ADD
	CONSTRAINT [DF__ogmanifes__ogm_d__64E44599]
	DEFAULT (NULL) FOR [ogm_date]
GO
ALTER TABLE [dbo].[ogmanifest]
	ADD
	CONSTRAINT [DF__ogmanifes__ogm_s__65D869D2]
	DEFAULT (NULL) FOR [ogm_stamp]
GO
ALTER TABLE [dbo].[ogmanifest]
	ADD
	CONSTRAINT [DF__ogmanifes__ogm_c__66CC8E0B]
	DEFAULT ('0') FOR [ogm_class]
GO
ALTER TABLE [dbo].[ogmanifest]
	ADD
	CONSTRAINT [DF__ogmanifes__ogm_a__67C0B244]
	DEFAULT ('0') FOR [ogm_agent]
GO
ALTER TABLE [dbo].[ogmanifest]
	ADD
	CONSTRAINT [DF__ogmanifes__ogm_c__68B4D67D]
	DEFAULT ('0') FOR [ogm_count]
GO
ALTER TABLE [dbo].[ogmanifest]
	ADD
	CONSTRAINT [DF__ogmanifes__ogm_r__69A8FAB6]
	DEFAULT ('N') FOR [ogm_returns]
GO
ALTER TABLE [dbo].[ogmanifest]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ogmanifest_ogm_agent]
	FOREIGN KEY ([ogm_agent]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[ogmanifest]
	NOCHECK CONSTRAINT [FK_ogmanifest_ogm_agent]

GO
ALTER TABLE [dbo].[ogmanifest] SET (LOCK_ESCALATION = TABLE)
GO
