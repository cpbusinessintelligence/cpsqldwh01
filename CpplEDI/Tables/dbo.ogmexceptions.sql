SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ogmexceptions] (
		[oge_id]                 [int] NOT NULL,
		[oge_ogm_id]             [int] NOT NULL,
		[oge_consignment_id]     [int] NOT NULL,
		[oge_description]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__ogmexcep__07D4819EF3E355CD]
		PRIMARY KEY
		CLUSTERED
		([oge_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ogmexceptions]
	ADD
	CONSTRAINT [DF__ogmexcept__oge_o__6C856761]
	DEFAULT ('0') FOR [oge_ogm_id]
GO
ALTER TABLE [dbo].[ogmexceptions]
	ADD
	CONSTRAINT [DF__ogmexcept__oge_c__6D798B9A]
	DEFAULT ('0') FOR [oge_consignment_id]
GO
ALTER TABLE [dbo].[ogmexceptions]
	ADD
	CONSTRAINT [DF__ogmexcept__oge_d__6E6DAFD3]
	DEFAULT (NULL) FOR [oge_description]
GO
ALTER TABLE [dbo].[ogmexceptions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ogmexceptions_oge_ogm_id]
	FOREIGN KEY ([oge_ogm_id]) REFERENCES [dbo].[ogmanifest] ([ogm_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[ogmexceptions]
	NOCHECK CONSTRAINT [FK_ogmexceptions_oge_ogm_id]

GO
ALTER TABLE [dbo].[ogmexceptions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ogmexceptions_oge_consignment_id]
	FOREIGN KEY ([oge_consignment_id]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[ogmexceptions]
	NOCHECK CONSTRAINT [FK_ogmexceptions_oge_consignment_id]

GO
ALTER TABLE [dbo].[ogmexceptions] SET (LOCK_ESCALATION = TABLE)
GO
