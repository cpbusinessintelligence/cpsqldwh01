SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[pcodebranch] (
		[pb_start]      [int] NOT NULL,
		[pb_end]        [int] NOT NULL,
		[pb_branch]     [int] NOT NULL,
		CONSTRAINT [PK__pcodebra__87E16D739EB1C7B9]
		PRIMARY KEY
		CLUSTERED
		([pb_start])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[pcodebranch]
	ADD
	CONSTRAINT [DF__pcodebran__pb_st__3ED3AB4A]
	DEFAULT ('0') FOR [pb_start]
GO
ALTER TABLE [dbo].[pcodebranch]
	ADD
	CONSTRAINT [DF__pcodebran__pb_en__3FC7CF83]
	DEFAULT ('0') FOR [pb_end]
GO
ALTER TABLE [dbo].[pcodebranch]
	ADD
	CONSTRAINT [DF__pcodebran__pb_br__40BBF3BC]
	DEFAULT ('0') FOR [pb_branch]
GO
ALTER TABLE [dbo].[pcodebranch]
	WITH NOCHECK
	ADD CONSTRAINT [FK_pcodebranch_pb_branch]
	FOREIGN KEY ([pb_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[pcodebranch]
	NOCHECK CONSTRAINT [FK_pcodebranch_pb_branch]

GO
ALTER TABLE [dbo].[pcodebranch] SET (LOCK_ESCALATION = TABLE)
GO
