SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pickupconsignment] (
		[pc_id]                    [int] NOT NULL,
		[pc_pickup]                [int] NULL,
		[pc_consignment]           [int] NULL,
		[pc_connote]               [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[pc_reference]             [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[pc_delivery_addr0]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[pc_delivery_addr1]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[pc_delivery_addr2]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[pc_delivery_addr3]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[pc_delivery_suburb]       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[pc_delivery_postcode]     [int] NULL,
		[pc_delivery_contact]      [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[pc_delivery_phone]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[pc_items]                 [int] NULL,
		[pc_weight]                [int] NULL,
		[pc_volume]                [float] NULL,
		CONSTRAINT [PK__pickupco__1D3A69C001BD9484]
		PRIMARY KEY
		CLUSTERED
		([pc_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_pi__2FD15EDC]
	DEFAULT (NULL) FOR [pc_pickup]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_co__30C58315]
	DEFAULT (NULL) FOR [pc_consignment]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_co__31B9A74E]
	DEFAULT (NULL) FOR [pc_connote]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_re__32ADCB87]
	DEFAULT (NULL) FOR [pc_reference]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__33A1EFC0]
	DEFAULT (NULL) FOR [pc_delivery_addr0]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__349613F9]
	DEFAULT (NULL) FOR [pc_delivery_addr1]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__358A3832]
	DEFAULT (NULL) FOR [pc_delivery_addr2]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__367E5C6B]
	DEFAULT (NULL) FOR [pc_delivery_addr3]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__377280A4]
	DEFAULT (NULL) FOR [pc_delivery_suburb]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__3866A4DD]
	DEFAULT (NULL) FOR [pc_delivery_postcode]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__395AC916]
	DEFAULT (NULL) FOR [pc_delivery_contact]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_de__3A4EED4F]
	DEFAULT (NULL) FOR [pc_delivery_phone]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_it__3B431188]
	DEFAULT (NULL) FOR [pc_items]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_we__3C3735C1]
	DEFAULT (NULL) FOR [pc_weight]
GO
ALTER TABLE [dbo].[pickupconsignment]
	ADD
	CONSTRAINT [DF__pickupcon__pc_vo__3D2B59FA]
	DEFAULT (NULL) FOR [pc_volume]
GO
ALTER TABLE [dbo].[pickupconsignment] SET (LOCK_ESCALATION = TABLE)
GO
