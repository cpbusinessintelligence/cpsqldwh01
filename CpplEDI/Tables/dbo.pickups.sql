SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pickups] (
		[p_id]                  [int] NOT NULL,
		[p_pending]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[p_style]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[p_cosmos_branch]       [int] NULL,
		[p_agent]               [int] NULL,
		[p_cosmos_code]         [char](32) COLLATE Latin1_General_CI_AS NULL,
		[p_company]             [int] NULL,
		[p_company_name]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_pickup_addr0]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_pickup_addr1]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_pickup_addr2]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_pickup_addr3]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_pickup_suburb]       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_pickup_postcode]     [int] NULL,
		[p_pickup_contact]      [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_pickup_phone]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[p_created_stamp]       [datetime] NULL,
		[p_updated_stamp]       [datetime] NULL,
		[p_release_stamp]       [datetime] NULL,
		[p_sent_stamp]          [datetime] NULL,
		[p_confirmed_stamp]     [datetime] NULL,
		[p_consignments]        [int] NULL,
		[p_items]               [int] NULL,
		[p_weight]              [int] NULL,
		[p_volume]              [float] NULL,
		[p_cosmos_date]         [date] NULL,
		[p_cosmos_job]          [int] NULL,
		[p_accept_stamp]        [datetime] NULL,
		[p_pickup_stamp]        [datetime] NULL,
		[p_futile_stamp]        [datetime] NULL,
		CONSTRAINT [PK__pickups__82E06B912C60FCA0]
		PRIMARY KEY
		CLUSTERED
		([p_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_pendi__1240FBF5]
	DEFAULT ('Y') FOR [p_pending]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_style__1335202E]
	DEFAULT ('N') FOR [p_style]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_cosmo__14294467]
	DEFAULT (NULL) FOR [p_cosmos_branch]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_agent__151D68A0]
	DEFAULT (NULL) FOR [p_agent]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_cosmo__16118CD9]
	DEFAULT (NULL) FOR [p_cosmos_code]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_compa__1705B112]
	DEFAULT (NULL) FOR [p_company]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_compa__17F9D54B]
	DEFAULT (NULL) FOR [p_company_name]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__18EDF984]
	DEFAULT (NULL) FOR [p_pickup_addr0]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__19E21DBD]
	DEFAULT (NULL) FOR [p_pickup_addr1]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__1AD641F6]
	DEFAULT (NULL) FOR [p_pickup_addr2]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__1BCA662F]
	DEFAULT (NULL) FOR [p_pickup_addr3]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__1CBE8A68]
	DEFAULT (NULL) FOR [p_pickup_suburb]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__1DB2AEA1]
	DEFAULT (NULL) FOR [p_pickup_postcode]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__1EA6D2DA]
	DEFAULT (NULL) FOR [p_pickup_contact]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__1F9AF713]
	DEFAULT (NULL) FOR [p_pickup_phone]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_creat__208F1B4C]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_created_stamp]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_updat__21833F85]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_updated_stamp]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_relea__227763BE]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_release_stamp]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_sent___236B87F7]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_sent_stamp]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_confi__245FAC30]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_confirmed_stamp]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_consi__2553D069]
	DEFAULT (NULL) FOR [p_consignments]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_items__2647F4A2]
	DEFAULT (NULL) FOR [p_items]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_weigh__273C18DB]
	DEFAULT (NULL) FOR [p_weight]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_volum__28303D14]
	DEFAULT (NULL) FOR [p_volume]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_cosmo__2924614D]
	DEFAULT ('0000-00-00') FOR [p_cosmos_date]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_cosmo__2A188586]
	DEFAULT ('0') FOR [p_cosmos_job]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_accep__2B0CA9BF]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_accept_stamp]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_picku__2C00CDF8]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_pickup_stamp]
GO
ALTER TABLE [dbo].[pickups]
	ADD
	CONSTRAINT [DF__pickups__p_futil__2CF4F231]
	DEFAULT ('0000-00-00 00:00:00') FOR [p_futile_stamp]
GO
ALTER TABLE [dbo].[pickups] SET (LOCK_ESCALATION = TABLE)
GO
