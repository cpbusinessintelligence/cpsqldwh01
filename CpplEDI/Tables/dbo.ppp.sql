SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ppp] (
		[Trans Date]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Branch]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Order No]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tr bo suffix]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Invoice]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Ref ]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 7]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Details]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Amount]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Batch]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Age]              [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ppp] SET (LOCK_ESCALATION = TABLE)
GO
