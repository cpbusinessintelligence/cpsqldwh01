SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[price] (
		[p_id]                   [int] NOT NULL,
		[p_name]                 [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[p_action]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[p_target]               [char](2) COLLATE Latin1_General_CI_AS NULL,
		[p_style]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[p_cpplonly]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[p_primary_cost]         [float] NULL,
		[p_primary_break]        [float] NULL,
		[p_secondary_cost]       [float] NULL,
		[p_secondary_break]      [float] NULL,
		[p_cubic_conversion]     [float] NULL,
		CONSTRAINT [PK__price__82E06B91F52DCB6C]
		PRIMARY KEY
		CLUSTERED
		([p_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_name__43986067]
	DEFAULT (NULL) FOR [p_name]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_action__448C84A0]
	DEFAULT ('N') FOR [p_action]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_target__4580A8D9]
	DEFAULT ('AD') FOR [p_target]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_style__4674CD12]
	DEFAULT ('F') FOR [p_style]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_cpplonl__4768F14B]
	DEFAULT ('N') FOR [p_cpplonly]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_primary__485D1584]
	DEFAULT ('0') FOR [p_primary_cost]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_primary__495139BD]
	DEFAULT ('0') FOR [p_primary_break]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_seconda__4A455DF6]
	DEFAULT ('0') FOR [p_secondary_cost]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_seconda__4B39822F]
	DEFAULT ('0') FOR [p_secondary_break]
GO
ALTER TABLE [dbo].[price]
	ADD
	CONSTRAINT [DF__price__p_cubic_c__4C2DA668]
	DEFAULT ('250') FOR [p_cubic_conversion]
GO
ALTER TABLE [dbo].[price] SET (LOCK_ESCALATION = TABLE)
GO
