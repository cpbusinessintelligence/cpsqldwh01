SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pricecodes] (
		[pc_code]                         [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[pc_name]                         [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[pc_validation_id]                [int] NULL,
		[pc_consolidation_code]           [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[pc_default_weight]               [int] NULL,
		[pc_default_cube]                 [float] NULL,
		[pc_default_instruction]          [char](80) COLLATE Latin1_General_CI_AS NULL,
		[pc_default_item_description]     [char](80) COLLATE Latin1_General_CI_AS NULL,
		[pc_airfreight]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pc_label_description]            [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[pc_returns_available]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pc_atl_service]                  [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pc_labelless]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__pricecod__20A61766C8FAB725]
		PRIMARY KEY
		CLUSTERED
		([pc_code])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_co__4F0A1313]
	DEFAULT ('') FOR [pc_code]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_na__4FFE374C]
	DEFAULT ('') FOR [pc_name]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_va__50F25B85]
	DEFAULT (NULL) FOR [pc_validation_id]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_co__51E67FBE]
	DEFAULT ('') FOR [pc_consolidation_code]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_de__52DAA3F7]
	DEFAULT ('0') FOR [pc_default_weight]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_de__53CEC830]
	DEFAULT ('0') FOR [pc_default_cube]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_de__54C2EC69]
	DEFAULT ('') FOR [pc_default_instruction]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_de__55B710A2]
	DEFAULT ('') FOR [pc_default_item_description]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_ai__56AB34DB]
	DEFAULT ('N') FOR [pc_airfreight]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_la__579F5914]
	DEFAULT ('') FOR [pc_label_description]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_re__58937D4D]
	DEFAULT ('N') FOR [pc_returns_available]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_at__5987A186]
	DEFAULT ('N') FOR [pc_atl_service]
GO
ALTER TABLE [dbo].[pricecodes]
	ADD
	CONSTRAINT [DF__pricecode__pc_la__5A7BC5BF]
	DEFAULT ('N') FOR [pc_labelless]
GO
ALTER TABLE [dbo].[pricecodes] SET (LOCK_ESCALATION = TABLE)
GO
