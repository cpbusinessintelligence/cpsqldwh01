SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pricegroup] (
		[pg_id]         [int] NOT NULL,
		[pg_name]       [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[pg_style]      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pg_parent]     [int] NULL,
		CONSTRAINT [PK__pricegro__B056F8C34F87F02B]
		PRIMARY KEY
		CLUSTERED
		([pg_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[pricegroup]
	ADD
	CONSTRAINT [DF__pricegrou__pg_na__5D58326A]
	DEFAULT ('') FOR [pg_name]
GO
ALTER TABLE [dbo].[pricegroup]
	ADD
	CONSTRAINT [DF__pricegrou__pg_st__5E4C56A3]
	DEFAULT ('S') FOR [pg_style]
GO
ALTER TABLE [dbo].[pricegroup]
	ADD
	CONSTRAINT [DF__pricegrou__pg_pa__5F407ADC]
	DEFAULT (NULL) FOR [pg_parent]
GO
ALTER TABLE [dbo].[pricegroup] SET (LOCK_ESCALATION = TABLE)
GO
