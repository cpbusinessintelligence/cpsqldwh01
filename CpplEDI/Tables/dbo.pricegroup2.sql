SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pricegroup2] (
		[pg_id]         [int] NOT NULL,
		[pg_name]       [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[pg_style]      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pg_parent]     [int] NULL
)
GO
ALTER TABLE [dbo].[pricegroup2]
	ADD
	CONSTRAINT [DF__pricegrou__pg_id__6128C34E]
	DEFAULT ('0') FOR [pg_id]
GO
ALTER TABLE [dbo].[pricegroup2]
	ADD
	CONSTRAINT [DF__pricegrou__pg_na__621CE787]
	DEFAULT ('') FOR [pg_name]
GO
ALTER TABLE [dbo].[pricegroup2]
	ADD
	CONSTRAINT [DF__pricegrou__pg_st__63110BC0]
	DEFAULT ('S') FOR [pg_style]
GO
ALTER TABLE [dbo].[pricegroup2]
	ADD
	CONSTRAINT [DF__pricegrou__pg_pa__64052FF9]
	DEFAULT (NULL) FOR [pg_parent]
GO
ALTER TABLE [dbo].[pricegroup2] SET (LOCK_ESCALATION = TABLE)
GO
