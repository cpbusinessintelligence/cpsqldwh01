SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pricezonemap] (
		[pz_zonemap]       [int] NOT NULL,
		[pz_pricecode]     [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[pz_fromzone]      [int] NOT NULL,
		[pz_tozone]        [int] NOT NULL,
		[pz_mapped]        [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[pricezonemap]
	ADD
	CONSTRAINT [DF__pricezone__pz_zo__65ED786B]
	DEFAULT ('0') FOR [pz_zonemap]
GO
ALTER TABLE [dbo].[pricezonemap]
	ADD
	CONSTRAINT [DF__pricezone__pz_pr__66E19CA4]
	DEFAULT ('') FOR [pz_pricecode]
GO
ALTER TABLE [dbo].[pricezonemap]
	ADD
	CONSTRAINT [DF__pricezone__pz_fr__67D5C0DD]
	DEFAULT ('0') FOR [pz_fromzone]
GO
ALTER TABLE [dbo].[pricezonemap]
	ADD
	CONSTRAINT [DF__pricezone__pz_to__68C9E516]
	DEFAULT ('0') FOR [pz_tozone]
GO
ALTER TABLE [dbo].[pricezonemap]
	ADD
	CONSTRAINT [DF__pricezone__pz_ma__69BE094F]
	DEFAULT ('') FOR [pz_mapped]
GO
ALTER TABLE [dbo].[pricezonemap]
	WITH NOCHECK
	ADD CONSTRAINT [FK_pricezonemap_pz_zonemap]
	FOREIGN KEY ([pz_zonemap]) REFERENCES [dbo].[zonemaps] ([zm_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[pricezonemap]
	NOCHECK CONSTRAINT [FK_pricezonemap_pz_zonemap]

GO
ALTER TABLE [dbo].[pricezonemap]
	WITH NOCHECK
	ADD CONSTRAINT [FK_pricezonemap_pz_fromzone]
	FOREIGN KEY ([pz_fromzone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[pricezonemap]
	NOCHECK CONSTRAINT [FK_pricezonemap_pz_fromzone]

GO
ALTER TABLE [dbo].[pricezonemap]
	WITH NOCHECK
	ADD CONSTRAINT [FK_pricezonemap_pz_tozone]
	FOREIGN KEY ([pz_tozone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[pricezonemap]
	NOCHECK CONSTRAINT [FK_pricezonemap_pz_tozone]

GO
ALTER TABLE [dbo].[pricezonemap] SET (LOCK_ESCALATION = TABLE)
GO
