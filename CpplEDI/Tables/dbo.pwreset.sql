SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pwreset] (
		[pr_id]               [int] NOT NULL,
		[pr_userid]           [int] NOT NULL,
		[pr_username]         [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[pr_email]            [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
		[pr_create_stamp]     [datetime] NULL,
		[pr_unique_id]        [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[pr_state]            [int] NULL,
		[pr_reset_stamp]      [datetime] NULL,
		CONSTRAINT [PK__pwreset__47B09F8E47627F71]
		PRIMARY KEY
		CLUSTERED
		([pr_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[pwreset]
	ADD
	CONSTRAINT [DF__pwreset__pr_crea__3A2EF1BE]
	DEFAULT (NULL) FOR [pr_create_stamp]
GO
ALTER TABLE [dbo].[pwreset]
	ADD
	CONSTRAINT [DF__pwreset__pr_stat__3B2315F7]
	DEFAULT ('0') FOR [pr_state]
GO
ALTER TABLE [dbo].[pwreset]
	ADD
	CONSTRAINT [DF__pwreset__pr_rese__3C173A30]
	DEFAULT (NULL) FOR [pr_reset_stamp]
GO
ALTER TABLE [dbo].[pwreset] SET (LOCK_ESCALATION = TABLE)
GO
