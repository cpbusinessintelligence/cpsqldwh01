SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rate_data] (
		[rd_id]          [int] NOT NULL,
		[rd_rating]      [int] NULL,
		[rd_action]      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[rd_style]       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[rd_data_id]     [int] NULL,
		CONSTRAINT [PK__rate_dat__D3D01ED21F6BE492]
		PRIMARY KEY
		CLUSTERED
		([rd_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[rate_data]
	ADD
	CONSTRAINT [DF__rate_data__rd_ra__6C9A75FA]
	DEFAULT (NULL) FOR [rd_rating]
GO
ALTER TABLE [dbo].[rate_data]
	ADD
	CONSTRAINT [DF__rate_data__rd_ac__6D8E9A33]
	DEFAULT ('N') FOR [rd_action]
GO
ALTER TABLE [dbo].[rate_data]
	ADD
	CONSTRAINT [DF__rate_data__rd_st__6E82BE6C]
	DEFAULT ('F') FOR [rd_style]
GO
ALTER TABLE [dbo].[rate_data]
	ADD
	CONSTRAINT [DF__rate_data__rd_da__6F76E2A5]
	DEFAULT (NULL) FOR [rd_data_id]
GO
ALTER TABLE [dbo].[rate_data] SET (LOCK_ESCALATION = TABLE)
GO
