SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rating] (
		[r_id]              [int] NOT NULL,
		[r_company_id]      [int] NOT NULL,
		[r_name]            [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[r_description]     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__rating__C4762327AD032F4B]
		PRIMARY KEY
		CLUSTERED
		([r_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[rating]
	ADD
	CONSTRAINT [DF__rating__r_compan__714A1C7E]
	DEFAULT ('0') FOR [r_company_id]
GO
ALTER TABLE [dbo].[rating]
	ADD
	CONSTRAINT [DF__rating__r_name__723E40B7]
	DEFAULT (NULL) FOR [r_name]
GO
ALTER TABLE [dbo].[rating]
	ADD
	CONSTRAINT [DF__rating__r_descri__733264F0]
	DEFAULT (NULL) FOR [r_description]
GO
ALTER TABLE [dbo].[rating]
	WITH NOCHECK
	ADD CONSTRAINT [FK_rating_company]
	FOREIGN KEY ([r_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[rating]
	NOCHECK CONSTRAINT [FK_rating_company]

GO
ALTER TABLE [dbo].[rating] SET (LOCK_ESCALATION = TABLE)
GO
