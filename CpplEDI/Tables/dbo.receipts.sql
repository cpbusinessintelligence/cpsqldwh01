SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[receipts] (
		[r_id]                 [int] NOT NULL,
		[r_company_id]         [int] NOT NULL,
		[r_user_id]            [int] NOT NULL,
		[r_date]               [smalldatetime] NULL,
		[r_creation_stamp]     [datetime] NULL,
		[r_exclusive]          [float] NULL,
		[r_gst]                [float] NULL,
		[r_total]              [float] NULL,
		[r_unallocated]        [float] NULL,
		[r_type]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[r_style]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[r_closed]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[r_reversed]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[r_bank]               [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[r_branch]             [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[r_drawer]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[r_no]                 [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[r_reference]          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[r_comment]            [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__receipts__C47623273CB97DC1]
		PRIMARY KEY
		CLUSTERED
		([r_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_reve__008C600E]
	DEFAULT ('N') FOR [r_reversed]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_bank__01808447]
	DEFAULT (NULL) FOR [r_bank]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_bran__0274A880]
	DEFAULT (NULL) FOR [r_branch]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_draw__0368CCB9]
	DEFAULT (NULL) FOR [r_drawer]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_no__045CF0F2]
	DEFAULT (NULL) FOR [r_no]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_refe__0551152B]
	DEFAULT (NULL) FOR [r_reference]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_comm__06453964]
	DEFAULT (NULL) FOR [r_comment]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_comp__760ED19B]
	DEFAULT ('0') FOR [r_company_id]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_user__7702F5D4]
	DEFAULT ('0') FOR [r_user_id]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_date__77F71A0D]
	DEFAULT (NULL) FOR [r_date]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_crea__78EB3E46]
	DEFAULT (NULL) FOR [r_creation_stamp]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_excl__79DF627F]
	DEFAULT ('0') FOR [r_exclusive]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_gst__7AD386B8]
	DEFAULT ('0') FOR [r_gst]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_tota__7BC7AAF1]
	DEFAULT ('0') FOR [r_total]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_unal__7CBBCF2A]
	DEFAULT ('0') FOR [r_unallocated]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_type__7DAFF363]
	DEFAULT ('') FOR [r_type]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_styl__7EA4179C]
	DEFAULT ('') FOR [r_style]
GO
ALTER TABLE [dbo].[receipts]
	ADD
	CONSTRAINT [DF__receipts__r_clos__7F983BD5]
	DEFAULT ('N') FOR [r_closed]
GO
ALTER TABLE [dbo].[receipts]
	WITH NOCHECK
	ADD CONSTRAINT [FK_receipts_company]
	FOREIGN KEY ([r_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[receipts]
	NOCHECK CONSTRAINT [FK_receipts_company]

GO
ALTER TABLE [dbo].[receipts] SET (LOCK_ESCALATION = TABLE)
GO
