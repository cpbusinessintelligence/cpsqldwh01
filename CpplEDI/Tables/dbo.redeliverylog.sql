SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redeliverylog] (
		[rl_id]                        [int] IDENTITY(1, 1) NOT NULL,
		[rl_redelivery_coupon]         [char](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[rl_tracked]                   [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[rl_stamp]                     [datetime] NULL,
		[rl_delivery_date]             [date] NULL,
		[rl_delivery_am_pm]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[rl_delivery_style]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[rl_delivery_instructions]     [char](120) COLLATE Latin1_General_CI_AS NULL,
		[rl_delivery_name]             [char](60) COLLATE Latin1_General_CI_AS NULL,
		[rl_delivery_addr1]            [char](60) COLLATE Latin1_General_CI_AS NULL,
		[rl_delivery_addr2]            [char](60) COLLATE Latin1_General_CI_AS NULL,
		[rl_delivery_suburb]           [char](40) COLLATE Latin1_General_CI_AS NULL,
		[rl_delivery_postcode]         [char](10) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__redelive__A107D412B5B9D61E]
		PRIMARY KEY
		CLUSTERED
		([rl_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_st__2A0D988E]
	DEFAULT ('0000-00-00 00:00:00') FOR [rl_stamp]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__2B01BCC7]
	DEFAULT ('0000-00-00') FOR [rl_delivery_date]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__2BF5E100]
	DEFAULT ('') FOR [rl_delivery_am_pm]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__2CEA0539]
	DEFAULT ('') FOR [rl_delivery_style]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__2DDE2972]
	DEFAULT ('') FOR [rl_delivery_instructions]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__2ED24DAB]
	DEFAULT ('') FOR [rl_delivery_name]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__2FC671E4]
	DEFAULT ('') FOR [rl_delivery_addr1]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__30BA961D]
	DEFAULT ('') FOR [rl_delivery_addr2]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__31AEBA56]
	DEFAULT ('') FOR [rl_delivery_suburb]
GO
ALTER TABLE [dbo].[redeliverylog]
	ADD
	CONSTRAINT [DF__redeliver__rl_de__32A2DE8F]
	DEFAULT ('') FOR [rl_delivery_postcode]
GO
ALTER TABLE [dbo].[redeliverylog] SET (LOCK_ESCALATION = TABLE)
GO
