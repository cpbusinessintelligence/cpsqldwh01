SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[return_info] (
		[r_id]              [int] NOT NULL,
		[r_company_id]      [int] NOT NULL,
		[r_consignment]     [int] NOT NULL,
		CONSTRAINT [PK__return_i__C4762327603E1373]
		PRIMARY KEY
		CLUSTERED
		([r_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[return_info]
	ADD
	CONSTRAINT [DF__return_in__r_com__0921A60F]
	DEFAULT ('0') FOR [r_company_id]
GO
ALTER TABLE [dbo].[return_info]
	ADD
	CONSTRAINT [DF__return_in__r_con__0A15CA48]
	DEFAULT ('0') FOR [r_consignment]
GO
ALTER TABLE [dbo].[return_info]
	WITH NOCHECK
	ADD CONSTRAINT [FK_return_info_company]
	FOREIGN KEY ([r_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[return_info]
	NOCHECK CONSTRAINT [FK_return_info_company]

GO
ALTER TABLE [dbo].[return_info]
	WITH NOCHECK
	ADD CONSTRAINT [FK_return_info_r_consignment]
	FOREIGN KEY ([r_consignment]) REFERENCES [dbo].[consignment] ([cd_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[return_info]
	NOCHECK CONSTRAINT [FK_return_info_r_consignment]

GO
ALTER TABLE [dbo].[return_info] SET (LOCK_ESCALATION = TABLE)
GO
