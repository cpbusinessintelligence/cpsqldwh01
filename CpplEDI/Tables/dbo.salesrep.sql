SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesrep] (
		[sr_id]             [int] NOT NULL,
		[sr_code]           [varchar](8) COLLATE Latin1_General_CI_AS NOT NULL,
		[sr_name]           [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[sr_addr0]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[sr_addr1]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[sr_addr2]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[sr_addr3]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[sr_suburb]         [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[sr_postcode]       [int] NULL,
		[sr_phone]          [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[sr_started]        [smalldatetime] NULL,
		[sr_terminated]     [smalldatetime] NULL,
		CONSTRAINT [PK__salesrep__5C9E98B94E15DDDE]
		PRIMARY KEY
		CLUSTERED
		([sr_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_cod__0CF236F3]
	DEFAULT ('') FOR [sr_code]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_nam__0DE65B2C]
	DEFAULT ('') FOR [sr_name]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_add__0EDA7F65]
	DEFAULT (NULL) FOR [sr_addr0]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_add__0FCEA39E]
	DEFAULT (NULL) FOR [sr_addr1]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_add__10C2C7D7]
	DEFAULT (NULL) FOR [sr_addr2]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_add__11B6EC10]
	DEFAULT (NULL) FOR [sr_addr3]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_sub__12AB1049]
	DEFAULT (NULL) FOR [sr_suburb]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_pos__139F3482]
	DEFAULT ('0') FOR [sr_postcode]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_pho__149358BB]
	DEFAULT (NULL) FOR [sr_phone]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_sta__15877CF4]
	DEFAULT (NULL) FOR [sr_started]
GO
ALTER TABLE [dbo].[salesrep]
	ADD
	CONSTRAINT [DF__salesrep__sr_ter__167BA12D]
	DEFAULT (NULL) FOR [sr_terminated]
GO
ALTER TABLE [dbo].[salesrep] SET (LOCK_ESCALATION = TABLE)
GO
