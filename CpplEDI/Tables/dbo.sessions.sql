SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sessions] (
		[id]         [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[access]     [int] NULL,
		[data]       [text] COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__sessions__3213E83F991AFA6B]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[sessions]
	ADD
	CONSTRAINT [DF__sessions__id__19580DD8]
	DEFAULT ('') FOR [id]
GO
ALTER TABLE [dbo].[sessions]
	ADD
	CONSTRAINT [DF__sessions__access__1A4C3211]
	DEFAULT (NULL) FOR [access]
GO
ALTER TABLE [dbo].[sessions] SET (LOCK_ESCALATION = TABLE)
GO
