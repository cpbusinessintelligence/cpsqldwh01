SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sortationdriver] (
		[s_branch]          [int] NOT NULL,
		[s_contractor]      [int] NOT NULL,
		[s_description]     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__sortatio__2C77E711AFDF1457]
		PRIMARY KEY
		CLUSTERED
		([s_branch], [s_contractor])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[sortationdriver]
	ADD
	CONSTRAINT [DF__sortation__s_bra__1D289EBC]
	DEFAULT ('0') FOR [s_branch]
GO
ALTER TABLE [dbo].[sortationdriver]
	ADD
	CONSTRAINT [DF__sortation__s_con__1E1CC2F5]
	DEFAULT ('0') FOR [s_contractor]
GO
ALTER TABLE [dbo].[sortationdriver]
	ADD
	CONSTRAINT [DF__sortation__s_des__1F10E72E]
	DEFAULT (NULL) FOR [s_description]
GO
ALTER TABLE [dbo].[sortationdriver]
	WITH NOCHECK
	ADD CONSTRAINT [FK_sortationdriver_s_branch]
	FOREIGN KEY ([s_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[sortationdriver]
	NOCHECK CONSTRAINT [FK_sortationdriver_s_branch]

GO
ALTER TABLE [dbo].[sortationdriver] SET (LOCK_ESCALATION = TABLE)
GO
