SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[suburb] (
		[s_id]           [int] NOT NULL,
		[s_cosmosid]     [int] NULL,
		[s_branch]       [int] NULL,
		[s_depot]        [int] NULL,
		[s_driver]       [int] NULL,
		[s_postcode]     [int] NULL,
		[s_name]         [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[s_state]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[s_zone]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__suburb__2F3684F4A838CB45]
		PRIMARY KEY
		CLUSTERED
		([s_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_cosmos__21ED53D9]
	DEFAULT (NULL) FOR [s_cosmosid]
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_branch__22E17812]
	DEFAULT (NULL) FOR [s_branch]
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_depot__23D59C4B]
	DEFAULT (NULL) FOR [s_depot]
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_driver__24C9C084]
	DEFAULT (NULL) FOR [s_driver]
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_postco__25BDE4BD]
	DEFAULT (NULL) FOR [s_postcode]
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_name__26B208F6]
	DEFAULT (NULL) FOR [s_name]
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_state__27A62D2F]
	DEFAULT (NULL) FOR [s_state]
GO
ALTER TABLE [dbo].[suburb]
	ADD
	CONSTRAINT [DF__suburb__s_zone__289A5168]
	DEFAULT (NULL) FOR [s_zone]
GO
ALTER TABLE [dbo].[suburb]
	WITH NOCHECK
	ADD CONSTRAINT [FK_suburb_driver_driver]
	FOREIGN KEY ([s_driver]) REFERENCES [dbo].[driver] ([dr_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[suburb]
	NOCHECK CONSTRAINT [FK_suburb_driver_driver]

GO
ALTER TABLE [dbo].[suburb]
	WITH NOCHECK
	ADD CONSTRAINT [FK_suburn_depot]
	FOREIGN KEY ([s_depot]) REFERENCES [dbo].[depot] ([d_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[suburb]
	NOCHECK CONSTRAINT [FK_suburn_depot]

GO
ALTER TABLE [dbo].[suburb]
	WITH NOCHECK
	ADD CONSTRAINT [FK_suburb_s_branch]
	FOREIGN KEY ([s_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[suburb]
	NOCHECK CONSTRAINT [FK_suburb_s_branch]

GO
ALTER TABLE [dbo].[suburb] SET (LOCK_ESCALATION = TABLE)
GO
