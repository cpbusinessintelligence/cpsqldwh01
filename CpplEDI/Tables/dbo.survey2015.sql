SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[survey2015] (
		[s_user]         [char](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[s_answered]     [date] NULL,
		[s_skipped]      [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[s_q1]           [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[s_q2]           [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[s_q3]           [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[s_q4]           [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[s_q5]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__survey20__13B4F3FA9B724823]
		PRIMARY KEY
		CLUSTERED
		([s_user])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[survey2015]
	ADD
	CONSTRAINT [DF__survey201__s_ski__2B76BE13]
	DEFAULT ('N') FOR [s_skipped]
GO
ALTER TABLE [dbo].[survey2015]
	ADD
	CONSTRAINT [DF__survey2015__s_q1__2C6AE24C]
	DEFAULT ('') FOR [s_q1]
GO
ALTER TABLE [dbo].[survey2015]
	ADD
	CONSTRAINT [DF__survey2015__s_q2__2D5F0685]
	DEFAULT ('') FOR [s_q2]
GO
ALTER TABLE [dbo].[survey2015]
	ADD
	CONSTRAINT [DF__survey2015__s_q3__2E532ABE]
	DEFAULT ('') FOR [s_q3]
GO
ALTER TABLE [dbo].[survey2015]
	ADD
	CONSTRAINT [DF__survey2015__s_q4__2F474EF7]
	DEFAULT ('') FOR [s_q4]
GO
ALTER TABLE [dbo].[survey2015] SET (LOCK_ESCALATION = TABLE)
GO
