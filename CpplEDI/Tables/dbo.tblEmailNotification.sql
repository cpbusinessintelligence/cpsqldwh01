SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailNotification] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]                 [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]              [bit] NOT NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__IsAct__7B82800E]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Creat__7C76A447]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Creat__7D6AC880]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Updat__7E5EECB9]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Updat__7F5310F2]
	DEFAULT ('TS') FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[tblEmailNotification] SET (LOCK_ESCALATION = TABLE)
GO
