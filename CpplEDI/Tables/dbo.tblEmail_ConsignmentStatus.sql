SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmail_ConsignmentStatus] (
		[ID]                        [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]                     [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[EventDateTime]             [datetime] NULL,
		[EventType]                 [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]                  [bit] NOT NULL,
		[EmailNotificationFlag]     [bit] NOT NULL,
		[CreatedDateTime]           [datetime] NULL,
		[CreatedBy]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblEmail_ConsignmentStatus]
	ADD
	CONSTRAINT [DF__tblEmail___Email__013B5964]
	DEFAULT ((0)) FOR [EmailNotificationFlag]
GO
ALTER TABLE [dbo].[tblEmail_ConsignmentStatus]
	ADD
	CONSTRAINT [DF__tblEmail___Creat__022F7D9D]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblEmail_ConsignmentStatus]
	ADD
	CONSTRAINT [DF__tblEmail___Creat__0323A1D6]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblEmail_ConsignmentStatus]
	ADD
	CONSTRAINT [DF__tblEmail___Updat__0417C60F]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblEmail_ConsignmentStatus]
	ADD
	CONSTRAINT [DF__tblEmail___Updat__050BEA48]
	DEFAULT ('TS') FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[tblEmail_ConsignmentStatus] SET (LOCK_ESCALATION = TABLE)
GO
