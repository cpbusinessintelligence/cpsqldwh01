SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DIFOTAccountNumbers_ToExcelLoad] (
		[ID]                [int] IDENTITY(1, 1) NOT NULL,
		[AccountNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FilePath]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[PackageName]       [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]          [int] NULL,
		[IsFinished]        [int] NULL
)
GO
ALTER TABLE [dbo].[tbl_DIFOTAccountNumbers_ToExcelLoad]
	ADD
	CONSTRAINT [DF__tbl_DIFOT__IsFin__69572AB5]
	DEFAULT ((0)) FOR [IsFinished]
GO
ALTER TABLE [dbo].[tbl_DIFOTAccountNumbers_ToExcelLoad] SET (LOCK_ESCALATION = TABLE)
GO
