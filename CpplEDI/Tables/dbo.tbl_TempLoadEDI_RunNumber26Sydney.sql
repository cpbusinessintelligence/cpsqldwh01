SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempLoadEDI_RunNumber26Sydney] (
		[cd_connote]        [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[EventDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_TempLoadEDI_RunNumber26Sydney] SET (LOCK_ESCALATION = TABLE)
GO
