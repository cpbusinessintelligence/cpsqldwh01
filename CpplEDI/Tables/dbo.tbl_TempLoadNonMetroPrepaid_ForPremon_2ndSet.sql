SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempLoadNonMetroPrepaid_ForPremon_2ndSet] (
		[LabelNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[EventDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_TempLoadNonMetroPrepaid_ForPremon_2ndSet] SET (LOCK_ESCALATION = TABLE)
GO
