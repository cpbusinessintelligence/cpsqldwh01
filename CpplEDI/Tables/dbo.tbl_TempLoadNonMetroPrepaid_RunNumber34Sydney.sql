SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempLoadNonMetroPrepaid_RunNumber34Sydney] (
		[LabelNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[EventDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_TempLoadNonMetroPrepaid_RunNumber34Sydney] SET (LOCK_ESCALATION = TABLE)
GO
