SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempPODMissingLabels_BE] (
		[LabelNumber]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EventDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_TempPODMissingLabels_BE] SET (LOCK_ESCALATION = TABLE)
GO
