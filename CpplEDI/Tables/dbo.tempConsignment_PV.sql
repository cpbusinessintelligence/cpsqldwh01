SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tempConsignment_PV] (
		[cd_id]                    [int] NOT NULL,
		[cd_date]                  [smalldatetime] NULL,
		[cd_consignment_date]      [smalldatetime] NULL,
		[cd_eta_date]              [smalldatetime] NULL,
		[cd_eta_earliest]          [smalldatetime] NULL,
		[cd_customer_eta]          [smalldatetime] NULL,
		[cd_billing_date]          [smalldatetime] NULL,
		[cd_pickup_pay_date]       [smalldatetime] NULL,
		[cd_delivery_pay_date]     [smalldatetime] NULL,
		[cd_transfer_pay_date]     [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[tempConsignment_PV] SET (LOCK_ESCALATION = TABLE)
GO
