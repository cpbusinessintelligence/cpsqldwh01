SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tempsuburb] (
		[suburb]       [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[postcode]     [int] NULL
)
GO
ALTER TABLE [dbo].[tempsuburb] SET (LOCK_ESCALATION = TABLE)
GO
