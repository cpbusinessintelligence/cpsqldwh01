SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_Driver257Scans] (
		[LabelNumber]     [varchar](250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_Driver257Scans] SET (LOCK_ESCALATION = TABLE)
GO
