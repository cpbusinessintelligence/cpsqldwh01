SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_DriverExclProntoIds_PV] (
		[Branch]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]           [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[ContractorType]         [varchar](2) COLLATE Latin1_General_CI_AS NULL,
		[Barcode_Contractor]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DepotNo]                [varchar](2) COLLATE Latin1_General_CI_AS NULL,
		[ProntoId]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[isActive]               [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[EffectiveDate]          [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Include_Ignore]         [varchar](10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_DriverExclProntoIds_PV] SET (LOCK_ESCALATION = TABLE)
GO
