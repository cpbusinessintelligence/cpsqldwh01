SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_DriverExclusion_PV] (
		[nc_branch]          [varchar](2) COLLATE Latin1_General_CI_AS NULL,
		[nc_contractor]      [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[nc_description]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[branch]             [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_DriverExclusion_PV] SET (LOCK_ESCALATION = TABLE)
GO
