SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_EDILoad_SA25_V2.0] (
		[Consignment]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Supplier]            [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ManifestDate]        [date] NULL,
		[Address]             [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Items]               [int] NULL,
		[Deadweight]          [decimal](18, 2) NULL,
		[Volume]              [decimal](18, 2) NULL,
		[PayDate]             [date] NULL,
		[Style]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Value]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Class]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender Postcode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_EDILoad_SA25_V2.0] SET (LOCK_ESCALATION = TABLE)
GO
