SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_FailedDefer2] (
		[cd_connote]                     [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[LableNumber]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WorkflowType]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Outcome]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Attribute]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AttributeValue]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverExtRef]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverRunNumber]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EventDateTime]                  [datetime] NULL,
		[cd_pickup_branch]               [int] NULL,
		[cd_pickup_driver]               [int] NULL,
		[cd_pickup_pay_driver]           [int] NULL,
		[cd_deliver_branch]              [int] NULL,
		[cd_deliver_driver]              [int] NULL,
		[cd_deliver_pay_driver]          [int] NULL,
		[PickupContractor]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                     [date] NULL,
		[PickupRctiDate]                 [date] NULL,
		[PickupRctiAmount]               [money] NULL,
		[PickupBranch]                   [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryContractor]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]                   [date] NULL,
		[DeliveryRctiDate]               [date] NULL,
		[DeliveryRctiAmount]             [money] NULL,
		[Deliverbranch]                  [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[cd_deliver_driver_ProntoId]     [varchar](5) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_FailedDefer2] SET (LOCK_ESCALATION = TABLE)
GO
