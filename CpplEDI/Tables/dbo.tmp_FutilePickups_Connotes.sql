SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_FutilePickups_Connotes] (
		[ORIGINSTATE]             [nvarchar](5) COLLATE Latin1_General_CI_AS NULL,
		[CONSIGNMENTDATE]         [date] NULL,
		[CONSIGNMENTNUMBER]       [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ORIGINPOSTCODE]          [int] NULL,
		[ORIGINSUBURB]            [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DESTINATIONSTATE]        [nvarchar](5) COLLATE Latin1_General_CI_AS NULL,
		[DESTINATIONPOSTCODE]     [int] NULL,
		[DESTINATIONSUBURB]       [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DECLAREDWEIGHT]          [decimal](18, 2) NULL,
		[COURIERCOST]             [decimal](18, 2) NULL
)
GO
ALTER TABLE [dbo].[tmp_FutilePickups_Connotes] SET (LOCK_ESCALATION = TABLE)
GO
