SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_PV_FailedDefer2] (
		[LableNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WorkflowType]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Outcome]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Attribute]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AttributeValue]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverExtRef]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverRunNumber]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Branch]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EventDateTime]       [datetime] NULL
)
GO
ALTER TABLE [dbo].[tmp_PV_FailedDefer2] SET (LOCK_ESCALATION = TABLE)
GO
