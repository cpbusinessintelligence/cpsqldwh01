SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_PV_SendleCWC_Debug_20201005] (
		[ConsignmentDate]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Label]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredServiceCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredWeight]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MeasuredWeightLabel]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredCubicWeight]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MeasuredCubicWeightLabel]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ItemQuantity]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Length]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Width]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Height]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MeasuredTime]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_PV_SendleCWC_Debug_20201005] SET (LOCK_ESCALATION = TABLE)
GO
