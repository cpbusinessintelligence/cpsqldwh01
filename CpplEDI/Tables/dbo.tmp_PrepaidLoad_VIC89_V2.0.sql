SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_PrepaidLoad_VIC89_V2.0] (
		[Column0]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column1]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column2]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column3]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column4]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column5]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column6]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column7]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Column8]     [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_PrepaidLoad_VIC89_V2.0] SET (LOCK_ESCALATION = TABLE)
GO
