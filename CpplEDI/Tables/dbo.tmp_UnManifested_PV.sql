SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_UnManifested_PV] (
		[id]                    [int] IDENTITY(1, 1) NOT NULL,
		[TRADE_NAME]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DATE_ACTL_DESPCH]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DATE_INVOICED]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CHRG_CODE]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CNSGMT_ID]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CNSGNEE_NAME]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date/TimeDataSent]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ManifestNumber]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_UnManifested_PV] SET (LOCK_ESCALATION = TABLE)
GO
