SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[trackcompany] (
		[tc_track]          [int] NOT NULL,
		[tc_company_id]     [int] NOT NULL
)
GO
ALTER TABLE [dbo].[trackcompany]
	ADD
	CONSTRAINT [DF__trackcomp__tc_tr__312F9769]
	DEFAULT ('0') FOR [tc_track]
GO
ALTER TABLE [dbo].[trackcompany]
	ADD
	CONSTRAINT [DF__trackcomp__tc_co__3223BBA2]
	DEFAULT ('0') FOR [tc_company_id]
GO
ALTER TABLE [dbo].[trackcompany]
	WITH NOCHECK
	ADD CONSTRAINT [FK_trackcompany_company]
	FOREIGN KEY ([tc_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[trackcompany]
	NOCHECK CONSTRAINT [FK_trackcompany_company]

GO
ALTER TABLE [dbo].[trackcompany] SET (LOCK_ESCALATION = TABLE)
GO
