SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[trackkey] (
		[t_id]              [int] NOT NULL,
		[t_key]             [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[t_description]     [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[t_includeeta]      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[t_limited]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[t_api]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[t_message]         [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
		[t_last_used]       [datetime] NULL,
		[t_use_count]       [int] NULL,
		CONSTRAINT [PK__trackkey__E579775F669C7149]
		PRIMARY KEY
		CLUSTERED
		([t_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_key__3500284D]
	DEFAULT ('') FOR [t_key]
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_desc__35F44C86]
	DEFAULT ('') FOR [t_description]
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_incl__36E870BF]
	DEFAULT ('N') FOR [t_includeeta]
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_limi__37DC94F8]
	DEFAULT ('N') FOR [t_limited]
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_api__38D0B931]
	DEFAULT ('N') FOR [t_api]
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_mess__39C4DD6A]
	DEFAULT ('') FOR [t_message]
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_last__3AB901A3]
	DEFAULT (NULL) FOR [t_last_used]
GO
ALTER TABLE [dbo].[trackkey]
	ADD
	CONSTRAINT [DF__trackkey__t_use___3BAD25DC]
	DEFAULT ('0') FOR [t_use_count]
GO
ALTER TABLE [dbo].[trackkey] SET (LOCK_ESCALATION = TABLE)
GO
