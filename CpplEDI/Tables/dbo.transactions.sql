SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactions] (
		[t_id]                 [int] NOT NULL,
		[t_company_id]         [int] NOT NULL,
		[t_date]               [smalldatetime] NULL,
		[t_creation_stamp]     [datetime] NULL,
		[t_exclusive]          [float] NULL,
		[t_gst]                [float] NULL,
		[t_total]              [float] NULL,
		[t_unallocated]        [float] NULL,
		[t_type]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[t_billing_id]         [int] NULL,
		[t_parent_id]          [int] NULL,
		[t_receipt_id]         [int] NULL,
		[t_open]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__transact__E579775F3186F6F1]
		PRIMARY KEY
		CLUSTERED
		([t_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_com__3E899287]
	DEFAULT ('0') FOR [t_company_id]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_dat__3F7DB6C0]
	DEFAULT (NULL) FOR [t_date]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_cre__4071DAF9]
	DEFAULT (NULL) FOR [t_creation_stamp]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_exc__4165FF32]
	DEFAULT ('0') FOR [t_exclusive]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_gst__425A236B]
	DEFAULT ('0') FOR [t_gst]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_tot__434E47A4]
	DEFAULT ('0') FOR [t_total]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_una__44426BDD]
	DEFAULT ('0') FOR [t_unallocated]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_typ__45369016]
	DEFAULT ('') FOR [t_type]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_bil__462AB44F]
	DEFAULT (NULL) FOR [t_billing_id]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_par__471ED888]
	DEFAULT (NULL) FOR [t_parent_id]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_rec__4812FCC1]
	DEFAULT (NULL) FOR [t_receipt_id]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__t_ope__490720FA]
	DEFAULT ('N') FOR [t_open]
GO
ALTER TABLE [dbo].[transactions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_transactions_company]
	FOREIGN KEY ([t_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[transactions]
	NOCHECK CONSTRAINT [FK_transactions_company]

GO
ALTER TABLE [dbo].[transactions] SET (LOCK_ESCALATION = TABLE)
GO
