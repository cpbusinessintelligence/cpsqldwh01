SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[type] (
		[ename]            [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[ediscription]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__type__0E8FC1F122265B66]
		PRIMARY KEY
		CLUSTERED
		([ename])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[type]
	ADD
	CONSTRAINT [DF__type__ename__4BE38DA5]
	DEFAULT ('') FOR [ename]
GO
ALTER TABLE [dbo].[type]
	ADD
	CONSTRAINT [DF__type__ediscripti__4CD7B1DE]
	DEFAULT ('') FOR [ediscription]
GO
ALTER TABLE [dbo].[type] SET (LOCK_ESCALATION = TABLE)
GO
