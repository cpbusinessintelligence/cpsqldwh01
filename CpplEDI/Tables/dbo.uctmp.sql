SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[uctmp] (
		[uc_coupon]         [char](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[uc_action]         [char](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[uc_stamp]          [datetime] NULL,
		[uc_company_id]     [int] NOT NULL,
		[uc_contractor]     [int] NULL,
		[uc_branch]         [char](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[uctmp]
	ADD
	CONSTRAINT [DF__uctmp__uc_coupon__4EBFFA50]
	DEFAULT ('') FOR [uc_coupon]
GO
ALTER TABLE [dbo].[uctmp]
	ADD
	CONSTRAINT [DF__uctmp__uc_action__4FB41E89]
	DEFAULT ('') FOR [uc_action]
GO
ALTER TABLE [dbo].[uctmp]
	ADD
	CONSTRAINT [DF__uctmp__uc_stamp__50A842C2]
	DEFAULT (NULL) FOR [uc_stamp]
GO
ALTER TABLE [dbo].[uctmp]
	ADD
	CONSTRAINT [DF__uctmp__uc_compan__519C66FB]
	DEFAULT ('0') FOR [uc_company_id]
GO
ALTER TABLE [dbo].[uctmp]
	ADD
	CONSTRAINT [DF__uctmp__uc_contra__52908B34]
	DEFAULT ('0') FOR [uc_contractor]
GO
ALTER TABLE [dbo].[uctmp]
	ADD
	CONSTRAINT [DF__uctmp__uc_branch__5384AF6D]
	DEFAULT ('') FOR [uc_branch]
GO
ALTER TABLE [dbo].[uctmp] SET (LOCK_ESCALATION = TABLE)
GO
