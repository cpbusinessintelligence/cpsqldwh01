SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[unknowncoupons] (
		[uc_coupon]         [char](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[uc_action]         [char](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[uc_stamp]          [datetime] NULL,
		[uc_company_id]     [int] NOT NULL,
		[uc_contractor]     [int] NULL,
		[uc_branch]         [char](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[unknowncoupons]
	ADD
	CONSTRAINT [DF__unknownco__uc_co__556CF7DF]
	DEFAULT ('') FOR [uc_coupon]
GO
ALTER TABLE [dbo].[unknowncoupons]
	ADD
	CONSTRAINT [DF__unknownco__uc_ac__56611C18]
	DEFAULT ('') FOR [uc_action]
GO
ALTER TABLE [dbo].[unknowncoupons]
	ADD
	CONSTRAINT [DF__unknownco__uc_st__57554051]
	DEFAULT (NULL) FOR [uc_stamp]
GO
ALTER TABLE [dbo].[unknowncoupons]
	ADD
	CONSTRAINT [DF__unknownco__uc_co__5849648A]
	DEFAULT ('0') FOR [uc_company_id]
GO
ALTER TABLE [dbo].[unknowncoupons]
	ADD
	CONSTRAINT [DF__unknownco__uc_co__593D88C3]
	DEFAULT ('0') FOR [uc_contractor]
GO
ALTER TABLE [dbo].[unknowncoupons]
	ADD
	CONSTRAINT [DF__unknownco__uc_br__5A31ACFC]
	DEFAULT ('') FOR [uc_branch]
GO
ALTER TABLE [dbo].[unknowncoupons]
	WITH NOCHECK
	ADD CONSTRAINT [FK_unknowncoupons_company]
	FOREIGN KEY ([uc_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[unknowncoupons]
	NOCHECK CONSTRAINT [FK_unknowncoupons_company]

GO
ALTER TABLE [dbo].[unknowncoupons] SET (LOCK_ESCALATION = TABLE)
GO
