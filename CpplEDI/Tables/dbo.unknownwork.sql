SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[unknownwork] (
		[uc_coupon]         [char](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[uc_action]         [char](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[uc_stamp]          [datetime] NULL,
		[uc_company_id]     [int] NOT NULL,
		[uc_contractor]     [int] NULL,
		[uc_branch]         [char](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[unknownwork]
	ADD
	CONSTRAINT [DF__unknownwo__uc_co__5C19F56E]
	DEFAULT ('') FOR [uc_coupon]
GO
ALTER TABLE [dbo].[unknownwork]
	ADD
	CONSTRAINT [DF__unknownwo__uc_ac__5D0E19A7]
	DEFAULT ('') FOR [uc_action]
GO
ALTER TABLE [dbo].[unknownwork]
	ADD
	CONSTRAINT [DF__unknownwo__uc_st__5E023DE0]
	DEFAULT (NULL) FOR [uc_stamp]
GO
ALTER TABLE [dbo].[unknownwork]
	ADD
	CONSTRAINT [DF__unknownwo__uc_co__5EF66219]
	DEFAULT ('0') FOR [uc_company_id]
GO
ALTER TABLE [dbo].[unknownwork]
	ADD
	CONSTRAINT [DF__unknownwo__uc_co__5FEA8652]
	DEFAULT ('0') FOR [uc_contractor]
GO
ALTER TABLE [dbo].[unknownwork]
	ADD
	CONSTRAINT [DF__unknownwo__uc_br__60DEAA8B]
	DEFAULT ('') FOR [uc_branch]
GO
ALTER TABLE [dbo].[unknownwork]
	WITH NOCHECK
	ADD CONSTRAINT [FK_unknownwork_company]
	FOREIGN KEY ([uc_company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[unknownwork]
	NOCHECK CONSTRAINT [FK_unknownwork_company]

GO
ALTER TABLE [dbo].[unknownwork] SET (LOCK_ESCALATION = TABLE)
GO
