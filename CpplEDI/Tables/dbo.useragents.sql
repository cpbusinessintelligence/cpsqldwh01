SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[useragents] (
		[user_id]      [int] NOT NULL,
		[agent_id]     [int] NOT NULL
)
GO
ALTER TABLE [dbo].[useragents]
	ADD
	CONSTRAINT [DF__useragent__user___62C6F2FD]
	DEFAULT ('0') FOR [user_id]
GO
ALTER TABLE [dbo].[useragents]
	ADD
	CONSTRAINT [DF__useragent__agent__63BB1736]
	DEFAULT ('0') FOR [agent_id]
GO
ALTER TABLE [dbo].[useragents]
	WITH NOCHECK
	ADD CONSTRAINT [FK_useragents_agent]
	FOREIGN KEY ([agent_id]) REFERENCES [dbo].[agents] ([a_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[useragents]
	NOCHECK CONSTRAINT [FK_useragents_agent]

GO
ALTER TABLE [dbo].[useragents] SET (LOCK_ESCALATION = TABLE)
GO
