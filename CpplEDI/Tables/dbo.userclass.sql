SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userclass] (
		[uc_user]               [int] NOT NULL,
		[uc_class]              [int] NOT NULL,
		[uc_email_manifest]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[uc_email]              [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[userclass]
	ADD
	CONSTRAINT [DF__userclass__uc_us__65A35FA8]
	DEFAULT ('0') FOR [uc_user]
GO
ALTER TABLE [dbo].[userclass]
	ADD
	CONSTRAINT [DF__userclass__uc_cl__669783E1]
	DEFAULT ('0') FOR [uc_class]
GO
ALTER TABLE [dbo].[userclass]
	ADD
	CONSTRAINT [DF__userclass__uc_em__678BA81A]
	DEFAULT ('N') FOR [uc_email_manifest]
GO
ALTER TABLE [dbo].[userclass]
	ADD
	CONSTRAINT [DF__userclass__uc_em__687FCC53]
	DEFAULT (NULL) FOR [uc_email]
GO
ALTER TABLE [dbo].[userclass] SET (LOCK_ESCALATION = TABLE)
GO
