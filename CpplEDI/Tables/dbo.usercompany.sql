SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usercompany] (
		[user_id]            [int] NOT NULL,
		[company_id]         [int] NOT NULL,
		[email_manifest]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[email]              [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[usercompany]
	ADD
	CONSTRAINT [DF__usercompa__user___6A6814C5]
	DEFAULT ('0') FOR [user_id]
GO
ALTER TABLE [dbo].[usercompany]
	ADD
	CONSTRAINT [DF__usercompa__compa__6B5C38FE]
	DEFAULT ('0') FOR [company_id]
GO
ALTER TABLE [dbo].[usercompany]
	ADD
	CONSTRAINT [DF__usercompa__email__6C505D37]
	DEFAULT ('N') FOR [email_manifest]
GO
ALTER TABLE [dbo].[usercompany]
	ADD
	CONSTRAINT [DF__usercompa__email__6D448170]
	DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[usercompany]
	WITH NOCHECK
	ADD CONSTRAINT [FK_usercompany_company]
	FOREIGN KEY ([company_id]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[usercompany]
	NOCHECK CONSTRAINT [FK_usercompany_company]

GO
ALTER TABLE [dbo].[usercompany] SET (LOCK_ESCALATION = TABLE)
GO
