SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users] (
		[id]                           [int] NOT NULL,
		[account]                      [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[salt]                         [char](64) COLLATE Latin1_General_CI_AS NULL,
		[md5]                          [char](64) COLLATE Latin1_General_CI_AS NULL,
		[fullname]                     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[email]                        [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[lastlogin]                    [datetime] NULL,
		[userlevel]                    [int] NULL,
		[flags]                        [int] NULL,
		[pReturns]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pAgents]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pAdmin]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pPricing]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pMaintenance]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pBilling]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pTracking]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pBookin]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pFinancial]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pSupervisor]                  [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pPODEntry]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pConnote]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pUndelivered]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pInvoiceReprint]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pSplitDeliveries]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pDaily]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pTRAP]                        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pMonitor]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pNoStats]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightConsignment]       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightLabels]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightPickup]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightImport]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightImportAddress]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightPricing]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pStaff]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pControlledReport]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[fCSV]                         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[contractor_number]            [int] NULL,
		[contractor_branch]            [int] NULL,
		[fContractor]                  [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEMM]                         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[emm_branch]                   [int] NULL,
		[pwchanged]                    [smalldatetime] NULL,
		[pwexempt]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[disabled]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pAPConnote]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEbay]                        [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEditConsignments]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightItems]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pITAdmin]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pRedirect]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[u_additional_permissions]     [varchar](264) COLLATE Latin1_General_CI_AS NULL,
		[fCSVStyle]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[loginattempts]                [int] NULL,
		[attemptstamp]                 [datetime] NULL,
		CONSTRAINT [PK__users__3213E83FFE7459D3]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pStaff__00224BBA]
	DEFAULT ('N') FOR [pStaff]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pControll__01166FF3]
	DEFAULT ('N') FOR [pControlledReport]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__fCSV__020A942C]
	DEFAULT ('N') FOR [fCSV]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__contracto__02FEB865]
	DEFAULT ('0') FOR [contractor_number]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__contracto__03F2DC9E]
	DEFAULT ('0') FOR [contractor_branch]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__fContract__04E700D7]
	DEFAULT ('N') FOR [fContractor]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEMM__05DB2510]
	DEFAULT ('N') FOR [pEMM]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__emm_branc__06CF4949]
	DEFAULT ('0') FOR [emm_branch]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pwchanged__07C36D82]
	DEFAULT (NULL) FOR [pwchanged]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pwexempt__08B791BB]
	DEFAULT ('N') FOR [pwexempt]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__disabled__09ABB5F4]
	DEFAULT ('N') FOR [disabled]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pAPConnot__0A9FDA2D]
	DEFAULT ('N') FOR [pAPConnote]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEbay__0B93FE66]
	DEFAULT ('N') FOR [pEbay]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEditCons__0C88229F]
	DEFAULT ('N') FOR [pEditConsignments]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEzyFreig__0D7C46D8]
	DEFAULT ('N') FOR [pEzyFreightItems]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pITAdmin__0E706B11]
	DEFAULT ('N') FOR [pITAdmin]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pRedirect__0F648F4A]
	DEFAULT ('N') FOR [pRedirect]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__account__60A9A061]
	DEFAULT ('') FOR [account]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__salt__619DC49A]
	DEFAULT ('') FOR [salt]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__md5__6291E8D3]
	DEFAULT (NULL) FOR [md5]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__fullname__63860D0C]
	DEFAULT (NULL) FOR [fullname]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__email__647A3145]
	DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__lastlogin__656E557E]
	DEFAULT (NULL) FOR [lastlogin]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__userlevel__666279B7]
	DEFAULT ('0') FOR [userlevel]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__flags__67569DF0]
	DEFAULT ('0') FOR [flags]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pReturns__684AC229]
	DEFAULT ('N') FOR [pReturns]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pAgents__693EE662]
	DEFAULT ('N') FOR [pAgents]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pAdmin__6A330A9B]
	DEFAULT ('N') FOR [pAdmin]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pPricing__6B272ED4]
	DEFAULT ('N') FOR [pPricing]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pMaintena__6C1B530D]
	DEFAULT ('N') FOR [pMaintenance]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pBilling__6D0F7746]
	DEFAULT ('N') FOR [pBilling]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pTracking__6E039B7F]
	DEFAULT ('N') FOR [pTracking]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pBookin__6EF7BFB8]
	DEFAULT ('N') FOR [pBookin]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pFinancia__6FEBE3F1]
	DEFAULT ('N') FOR [pFinancial]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pSupervis__70E0082A]
	DEFAULT ('N') FOR [pSupervisor]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pPODEntry__71D42C63]
	DEFAULT ('N') FOR [pPODEntry]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pConnote__72C8509C]
	DEFAULT ('N') FOR [pConnote]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pUndelive__73BC74D5]
	DEFAULT ('N') FOR [pUndelivered]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pInvoiceR__74B0990E]
	DEFAULT ('N') FOR [pInvoiceReprint]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pSplitDel__75A4BD47]
	DEFAULT ('N') FOR [pSplitDeliveries]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pDaily__7698E180]
	DEFAULT ('N') FOR [pDaily]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pTRAP__778D05B9]
	DEFAULT ('N') FOR [pTRAP]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pMonitor__788129F2]
	DEFAULT ('N') FOR [pMonitor]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pNoStats__79754E2B]
	DEFAULT ('N') FOR [pNoStats]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEzyFreig__7A697264]
	DEFAULT ('N') FOR [pEzyFreightConsignment]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEzyFreig__7B5D969D]
	DEFAULT ('N') FOR [pEzyFreightLabels]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEzyFreig__7C51BAD6]
	DEFAULT ('N') FOR [pEzyFreightPickup]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEzyFreig__7D45DF0F]
	DEFAULT ('N') FOR [pEzyFreightImport]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEzyFreig__7E3A0348]
	DEFAULT ('N') FOR [pEzyFreightImportAddress]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__pEzyFreig__7F2E2781]
	DEFAULT ('N') FOR [pEzyFreightPricing]
GO
ALTER TABLE [dbo].[users]
	WITH NOCHECK
	ADD CONSTRAINT [FK_users_contractor]
	FOREIGN KEY ([contractor_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[users]
	NOCHECK CONSTRAINT [FK_users_contractor]

GO
ALTER TABLE [dbo].[users]
	WITH NOCHECK
	ADD CONSTRAINT [FK_users_emm]
	FOREIGN KEY ([emm_branch]) REFERENCES [dbo].[branchs] ([b_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[users]
	NOCHECK CONSTRAINT [FK_users_emm]

GO
ALTER TABLE [dbo].[users] SET (LOCK_ESCALATION = TABLE)
GO
