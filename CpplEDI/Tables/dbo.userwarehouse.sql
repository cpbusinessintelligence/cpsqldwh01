SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userwarehouse] (
		[uw_user_id]            [int] NOT NULL,
		[uw_warehouse_id]       [int] NOT NULL,
		[uw_email_manifest]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[uw_email]              [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[userwarehouse]
	ADD
	CONSTRAINT [DF__userwareh__uw_us__6F2CC9E2]
	DEFAULT ('0') FOR [uw_user_id]
GO
ALTER TABLE [dbo].[userwarehouse]
	ADD
	CONSTRAINT [DF__userwareh__uw_wa__7020EE1B]
	DEFAULT ('0') FOR [uw_warehouse_id]
GO
ALTER TABLE [dbo].[userwarehouse]
	ADD
	CONSTRAINT [DF__userwareh__uw_em__71151254]
	DEFAULT ('N') FOR [uw_email_manifest]
GO
ALTER TABLE [dbo].[userwarehouse]
	ADD
	CONSTRAINT [DF__userwareh__uw_em__7209368D]
	DEFAULT (NULL) FOR [uw_email]
GO
ALTER TABLE [dbo].[userwarehouse] SET (LOCK_ESCALATION = TABLE)
GO
