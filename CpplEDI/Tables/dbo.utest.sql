SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[utest] (
		[id]                         [int] NOT NULL,
		[account]                    [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[password]                   [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[encrypted]                  [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[md5]                        [char](64) COLLATE Latin1_General_CI_AS NULL,
		[fullname]                   [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		[email]                      [varchar](256) COLLATE Latin1_General_CI_AS NULL,
		[lastlogin]                  [datetime] NULL,
		[userlevel]                  [int] NULL,
		[flags]                      [int] NULL,
		[pReturns]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pAgents]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pAdmin]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pPricing]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pMaintenance]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pBilling]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pTracking]                  [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pBookin]                    [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pFinancial]                 [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pSupervisor]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pPODEntry]                  [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pConnote]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pUndelivered]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pInvoiceReprint]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pSplitDeliveries]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pDaily]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pTRAP]                      [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pMonitor]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pNoStats]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightConsignment]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightLabels]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEzyFreightPickup]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pStaff]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pControlledReport]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[fCSV]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[contractor_number]          [int] NULL,
		[contractor_branch]          [int] NULL,
		[fContractor]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pEMM]                       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[emm_branch]                 [int] NULL,
		[pwchanged]                  [smalldatetime] NULL,
		[pwexempt]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[disabled]                   [char](1) COLLATE Latin1_General_CI_AS NULL,
		[pAPConnote]                 [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pPricing__005755E4]
	DEFAULT ('N') FOR [pPricing]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pMaintena__014B7A1D]
	DEFAULT ('N') FOR [pMaintenance]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pBilling__023F9E56]
	DEFAULT ('N') FOR [pBilling]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pTracking__0333C28F]
	DEFAULT ('N') FOR [pTracking]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pBookin__0427E6C8]
	DEFAULT ('N') FOR [pBookin]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pFinancia__051C0B01]
	DEFAULT ('N') FOR [pFinancial]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pSupervis__06102F3A]
	DEFAULT ('N') FOR [pSupervisor]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pPODEntry__07045373]
	DEFAULT ('N') FOR [pPODEntry]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pConnote__07F877AC]
	DEFAULT ('N') FOR [pConnote]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pUndelive__08EC9BE5]
	DEFAULT ('N') FOR [pUndelivered]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pInvoiceR__09E0C01E]
	DEFAULT ('N') FOR [pInvoiceReprint]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pSplitDel__0AD4E457]
	DEFAULT ('N') FOR [pSplitDeliveries]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pDaily__0BC90890]
	DEFAULT ('N') FOR [pDaily]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pTRAP__0CBD2CC9]
	DEFAULT ('N') FOR [pTRAP]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pMonitor__0DB15102]
	DEFAULT ('N') FOR [pMonitor]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pNoStats__0EA5753B]
	DEFAULT ('N') FOR [pNoStats]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pEzyFreig__0F999974]
	DEFAULT ('N') FOR [pEzyFreightConsignment]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pEzyFreig__108DBDAD]
	DEFAULT ('N') FOR [pEzyFreightLabels]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pEzyFreig__1181E1E6]
	DEFAULT ('N') FOR [pEzyFreightPickup]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pStaff__1276061F]
	DEFAULT ('N') FOR [pStaff]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pControll__136A2A58]
	DEFAULT ('N') FOR [pControlledReport]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__fCSV__145E4E91]
	DEFAULT ('N') FOR [fCSV]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__contracto__155272CA]
	DEFAULT ('0') FOR [contractor_number]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__contracto__16469703]
	DEFAULT ('0') FOR [contractor_branch]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__fContract__173ABB3C]
	DEFAULT ('N') FOR [fContractor]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pEMM__182EDF75]
	DEFAULT ('N') FOR [pEMM]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__emm_branc__192303AE]
	DEFAULT ('0') FOR [emm_branch]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pwchanged__1A1727E7]
	DEFAULT (NULL) FOR [pwchanged]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pwexempt__1B0B4C20]
	DEFAULT ('N') FOR [pwexempt]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__disabled__1BFF7059]
	DEFAULT ('N') FOR [disabled]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pAPConnot__1CF39492]
	DEFAULT ('N') FOR [pAPConnote]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__id__73F17EFF]
	DEFAULT ('0') FOR [id]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__account__74E5A338]
	DEFAULT ('') FOR [account]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__password__75D9C771]
	DEFAULT ('') FOR [password]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__encrypted__76CDEBAA]
	DEFAULT ('') FOR [encrypted]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__md5__77C20FE3]
	DEFAULT (NULL) FOR [md5]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__fullname__78B6341C]
	DEFAULT (NULL) FOR [fullname]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__email__79AA5855]
	DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__lastlogin__7A9E7C8E]
	DEFAULT (NULL) FOR [lastlogin]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__userlevel__7B92A0C7]
	DEFAULT ('0') FOR [userlevel]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__flags__7C86C500]
	DEFAULT ('0') FOR [flags]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pReturns__7D7AE939]
	DEFAULT ('N') FOR [pReturns]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pAgents__7E6F0D72]
	DEFAULT ('N') FOR [pAgents]
GO
ALTER TABLE [dbo].[utest]
	ADD
	CONSTRAINT [DF__utest__pAdmin__7F6331AB]
	DEFAULT ('N') FOR [pAdmin]
GO
ALTER TABLE [dbo].[utest] SET (LOCK_ESCALATION = TABLE)
GO
