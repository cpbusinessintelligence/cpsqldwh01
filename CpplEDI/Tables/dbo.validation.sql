SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[validation] (
		[v_id]                              [int] NOT NULL,
		[v_minimum_deadweight_required]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_minimum_deadweight_value]        [float] NULL,
		[v_minimum_volume_required]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_minimum_volume_value]            [float] NULL,
		[v_minimum_ratio_required]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_minimum_ratio_value]             [float] NULL,
		[v_minimum_items_required]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_minimum_items_value]             [int] NULL,
		[v_maximum_deadweight_required]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_maximum_deadweight_value]        [float] NULL,
		[v_maximum_volume_required]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_maximum_volume_value]            [float] NULL,
		[v_maximum_ratio_required]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_maximum_ratio_value]             [float] NULL,
		[v_maximum_items_required]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_maximum_items_value]             [int] NULL,
		[v_no_suburb_validation]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[v_quote_check]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__validati__AD3D844175DF94C8]
		PRIMARY KEY
		CLUSTERED
		([v_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__1FD0013D]
	DEFAULT ('N') FOR [v_minimum_deadweight_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__20C42576]
	DEFAULT (NULL) FOR [v_minimum_deadweight_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__21B849AF]
	DEFAULT ('N') FOR [v_minimum_volume_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__22AC6DE8]
	DEFAULT ('0') FOR [v_minimum_volume_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__23A09221]
	DEFAULT ('N') FOR [v_minimum_ratio_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__2494B65A]
	DEFAULT ('0') FOR [v_minimum_ratio_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__2588DA93]
	DEFAULT ('N') FOR [v_minimum_items_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_min__267CFECC]
	DEFAULT ('0') FOR [v_minimum_items_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__27712305]
	DEFAULT ('N') FOR [v_maximum_deadweight_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__2865473E]
	DEFAULT (NULL) FOR [v_maximum_deadweight_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__29596B77]
	DEFAULT ('N') FOR [v_maximum_volume_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__2A4D8FB0]
	DEFAULT ('0') FOR [v_maximum_volume_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__2B41B3E9]
	DEFAULT ('N') FOR [v_maximum_ratio_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__2C35D822]
	DEFAULT ('0') FOR [v_maximum_ratio_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__2D29FC5B]
	DEFAULT ('N') FOR [v_maximum_items_required]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_max__2E1E2094]
	DEFAULT ('0') FOR [v_maximum_items_value]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_no___2F1244CD]
	DEFAULT ('N') FOR [v_no_suburb_validation]
GO
ALTER TABLE [dbo].[validation]
	ADD
	CONSTRAINT [DF__validatio__v_quo__30066906]
	DEFAULT ('N') FOR [v_quote_check]
GO
ALTER TABLE [dbo].[validation] SET (LOCK_ESCALATION = TABLE)
GO
