SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[warehouse] (
		[w_id]              [int] NOT NULL,
		[w_name]            [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[w_description]     [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__warehous__1198F2A36EEB86AB]
		PRIMARY KEY
		CLUSTERED
		([w_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[warehouse]
	ADD
	CONSTRAINT [DF__warehouse__w_nam__32E2D5B1]
	DEFAULT ('') FOR [w_name]
GO
ALTER TABLE [dbo].[warehouse]
	ADD
	CONSTRAINT [DF__warehouse__w_des__33D6F9EA]
	DEFAULT (NULL) FOR [w_description]
GO
ALTER TABLE [dbo].[warehouse] SET (LOCK_ESCALATION = TABLE)
GO
