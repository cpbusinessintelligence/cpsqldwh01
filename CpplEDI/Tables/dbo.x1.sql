SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[x1] (
		[ccg_pricegroup]            [int] NOT NULL,
		[ccg_pricecode]             [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
		[ccg_fromzone]              [int] NOT NULL,
		[ccg_tozone]                [int] NOT NULL,
		[ccg_present]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[ccg_baseprice]             [float] NULL,
		[ccg_15kg]                  [float] NULL,
		[ccg_25kg]                  [float] NULL,
		[ccg_pickup_payamount]      [float] NULL,
		[ccg_pickup_15kg]           [float] NULL,
		[ccg_pickup_25kg]           [float] NULL,
		[ccg_deliver_payamount]     [float] NULL,
		[ccg_deliver_15kg]          [float] NULL,
		[ccg_deliver_25kg]          [float] NULL
)
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_pricegro__35BF425C]
	DEFAULT ('0') FOR [ccg_pricegroup]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_pricecod__36B36695]
	DEFAULT ('') FOR [ccg_pricecode]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_fromzone__37A78ACE]
	DEFAULT ('0') FOR [ccg_fromzone]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_tozone__389BAF07]
	DEFAULT ('0') FOR [ccg_tozone]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_present__398FD340]
	DEFAULT ('N') FOR [ccg_present]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_basepric__3A83F779]
	DEFAULT ('0') FOR [ccg_baseprice]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_15kg__3B781BB2]
	DEFAULT ('0') FOR [ccg_15kg]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_25kg__3C6C3FEB]
	DEFAULT ('0') FOR [ccg_25kg]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_pickup_p__3D606424]
	DEFAULT ('0') FOR [ccg_pickup_payamount]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_pickup_1__3E54885D]
	DEFAULT ('0') FOR [ccg_pickup_15kg]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_pickup_2__3F48AC96]
	DEFAULT ('0') FOR [ccg_pickup_25kg]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_deliver___403CD0CF]
	DEFAULT ('0') FOR [ccg_deliver_payamount]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_deliver___4130F508]
	DEFAULT ('0') FOR [ccg_deliver_15kg]
GO
ALTER TABLE [dbo].[x1]
	ADD
	CONSTRAINT [DF__x1__ccg_deliver___42251941]
	DEFAULT ('0') FOR [ccg_deliver_25kg]
GO
ALTER TABLE [dbo].[x1] SET (LOCK_ESCALATION = TABLE)
GO
