SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_tmpCdconnote_Test] (
		[cd_connote]     [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[z_tmpCdconnote_Test] SET (LOCK_ESCALATION = TABLE)
GO
