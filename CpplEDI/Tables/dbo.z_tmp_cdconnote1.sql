SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_tmp_cdconnote1] (
		[cd_connote]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[z_tmp_cdconnote1] SET (LOCK_ESCALATION = TABLE)
GO
