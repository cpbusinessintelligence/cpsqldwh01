SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zonegrid] (
		[zp_fromzone]          [int] NOT NULL,
		[zp_tozone]            [int] NOT NULL,
		[zp_price]             [int] NOT NULL,
		[zp_pricegroup_id]     [int] NOT NULL,
		[zp_multiplier]        [int] NULL,
		[zp_pricecode]         [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__zonegrid__DD40DED79F824F23]
		PRIMARY KEY
		CLUSTERED
		([zp_fromzone], [zp_tozone], [zp_price], [zp_pricegroup_id], [zp_pricecode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[zonegrid]
	ADD
	CONSTRAINT [DF__zonegrid__zp_fro__450185EC]
	DEFAULT ('0') FOR [zp_fromzone]
GO
ALTER TABLE [dbo].[zonegrid]
	ADD
	CONSTRAINT [DF__zonegrid__zp_toz__45F5AA25]
	DEFAULT ('0') FOR [zp_tozone]
GO
ALTER TABLE [dbo].[zonegrid]
	ADD
	CONSTRAINT [DF__zonegrid__zp_pri__46E9CE5E]
	DEFAULT ('0') FOR [zp_price]
GO
ALTER TABLE [dbo].[zonegrid]
	ADD
	CONSTRAINT [DF__zonegrid__zp_pri__47DDF297]
	DEFAULT ('0') FOR [zp_pricegroup_id]
GO
ALTER TABLE [dbo].[zonegrid]
	ADD
	CONSTRAINT [DF__zonegrid__zp_mul__48D216D0]
	DEFAULT ('1') FOR [zp_multiplier]
GO
ALTER TABLE [dbo].[zonegrid]
	ADD
	CONSTRAINT [DF__zonegrid__zp_pri__49C63B09]
	DEFAULT ('STD') FOR [zp_pricecode]
GO
ALTER TABLE [dbo].[zonegrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zonegrid_zp_price]
	FOREIGN KEY ([zp_price]) REFERENCES [dbo].[price] ([p_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zonegrid]
	NOCHECK CONSTRAINT [FK_zonegrid_zp_price]

GO
ALTER TABLE [dbo].[zonegrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zonegrid_zp_pricegroup_id]
	FOREIGN KEY ([zp_pricegroup_id]) REFERENCES [dbo].[pricegroup] ([pg_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zonegrid]
	NOCHECK CONSTRAINT [FK_zonegrid_zp_pricegroup_id]

GO
ALTER TABLE [dbo].[zonegrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zonegrid_zp_fromzone]
	FOREIGN KEY ([zp_fromzone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zonegrid]
	NOCHECK CONSTRAINT [FK_zonegrid_zp_fromzone]

GO
ALTER TABLE [dbo].[zonegrid]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zonegrid_zp_tozone]
	FOREIGN KEY ([zp_tozone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zonegrid]
	NOCHECK CONSTRAINT [FK_zonegrid_zp_tozone]

GO
ALTER TABLE [dbo].[zonegrid] SET (LOCK_ESCALATION = TABLE)
GO
