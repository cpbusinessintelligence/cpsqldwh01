SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zonemaps] (
		[zm_id]       [int] NOT NULL,
		[zm_name]     [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK__zonemaps__FBEBD920EF1E7D7A]
		PRIMARY KEY
		CLUSTERED
		([zm_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[zonemaps]
	ADD
	CONSTRAINT [DF__zonemaps__zm_nam__4CA2A7B4]
	DEFAULT ('') FOR [zm_name]
GO
ALTER TABLE [dbo].[zonemaps] SET (LOCK_ESCALATION = TABLE)
GO
