SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zoneprice] (
		[zp_zone]           [int] NOT NULL,
		[zp_price]          [int] NOT NULL,
		[zp_company]        [int] NOT NULL,
		[zp_multiplier]     [int] NULL,
		[zp_pricecode]      [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__zonepric__BFD796BA409DCC3C]
		PRIMARY KEY
		CLUSTERED
		([zp_zone], [zp_price], [zp_company])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[zoneprice]
	ADD
	CONSTRAINT [DF__zoneprice__zp_zo__4F7F145F]
	DEFAULT ('0') FOR [zp_zone]
GO
ALTER TABLE [dbo].[zoneprice]
	ADD
	CONSTRAINT [DF__zoneprice__zp_pr__50733898]
	DEFAULT ('0') FOR [zp_price]
GO
ALTER TABLE [dbo].[zoneprice]
	ADD
	CONSTRAINT [DF__zoneprice__zp_co__51675CD1]
	DEFAULT ('0') FOR [zp_company]
GO
ALTER TABLE [dbo].[zoneprice]
	ADD
	CONSTRAINT [DF__zoneprice__zp_mu__525B810A]
	DEFAULT ('1') FOR [zp_multiplier]
GO
ALTER TABLE [dbo].[zoneprice]
	ADD
	CONSTRAINT [DF__zoneprice__zp_pr__534FA543]
	DEFAULT ('STD') FOR [zp_pricecode]
GO
ALTER TABLE [dbo].[zoneprice]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zoneprice_zp_zone]
	FOREIGN KEY ([zp_zone]) REFERENCES [dbo].[zones] ([z_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zoneprice]
	NOCHECK CONSTRAINT [FK_zoneprice_zp_zone]

GO
ALTER TABLE [dbo].[zoneprice]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zoneprice_company]
	FOREIGN KEY ([zp_company]) REFERENCES [dbo].[companies] ([c_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zoneprice]
	NOCHECK CONSTRAINT [FK_zoneprice_company]

GO
ALTER TABLE [dbo].[zoneprice]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zoneprice_zp_price]
	FOREIGN KEY ([zp_price]) REFERENCES [dbo].[price] ([p_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zoneprice]
	NOCHECK CONSTRAINT [FK_zoneprice_zp_price]

GO
ALTER TABLE [dbo].[zoneprice] SET (LOCK_ESCALATION = TABLE)
GO
