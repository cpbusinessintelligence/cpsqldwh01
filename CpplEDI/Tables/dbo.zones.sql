SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zones] (
		[z_id]               [int] NOT NULL,
		[z_code]             [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[z_name]             [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[z_warehouse_id]     [int] NOT NULL,
		[z_level]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[z_order]            [int] NULL,
		[z_color]            [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__zones__977743E6B60ACD2C]
		PRIMARY KEY
		CLUSTERED
		([z_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[zones]
	ADD
	CONSTRAINT [DF__zones__z_code__562C11EE]
	DEFAULT (NULL) FOR [z_code]
GO
ALTER TABLE [dbo].[zones]
	ADD
	CONSTRAINT [DF__zones__z_name__57203627]
	DEFAULT (NULL) FOR [z_name]
GO
ALTER TABLE [dbo].[zones]
	ADD
	CONSTRAINT [DF__zones__z_warehou__58145A60]
	DEFAULT ('0') FOR [z_warehouse_id]
GO
ALTER TABLE [dbo].[zones]
	ADD
	CONSTRAINT [DF__zones__z_level__59087E99]
	DEFAULT ('3') FOR [z_level]
GO
ALTER TABLE [dbo].[zones]
	ADD
	CONSTRAINT [DF__zones__z_order__59FCA2D2]
	DEFAULT ('1000') FOR [z_order]
GO
ALTER TABLE [dbo].[zones]
	ADD
	CONSTRAINT [DF__zones__z_color__5AF0C70B]
	DEFAULT (NULL) FOR [z_color]
GO
ALTER TABLE [dbo].[zones]
	WITH NOCHECK
	ADD CONSTRAINT [FK_zones_warehouse]
	FOREIGN KEY ([z_warehouse_id]) REFERENCES [dbo].[warehouse] ([w_id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[zones]
	NOCHECK CONSTRAINT [FK_zones_warehouse]

GO
ALTER TABLE [dbo].[zones] SET (LOCK_ESCALATION = TABLE)
GO
