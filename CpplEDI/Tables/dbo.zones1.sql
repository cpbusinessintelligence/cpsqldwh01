SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zones1] (
		[z_id]               [int] NOT NULL,
		[z_code]             [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[z_name]             [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[z_warehouse_id]     [int] NOT NULL,
		[z_level]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[z_order]            [int] NULL,
		[z_color]            [varchar](64) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__zones1__977743E695BD8079]
		PRIMARY KEY
		CLUSTERED
		([z_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[zones1]
	ADD
	CONSTRAINT [DF__zones1__z_code__5DCDB4FD]
	DEFAULT (NULL) FOR [z_code]
GO
ALTER TABLE [dbo].[zones1]
	ADD
	CONSTRAINT [DF__zones1__z_name__5EC1D936]
	DEFAULT (NULL) FOR [z_name]
GO
ALTER TABLE [dbo].[zones1]
	ADD
	CONSTRAINT [DF__zones1__z_wareho__5FB5FD6F]
	DEFAULT ('0') FOR [z_warehouse_id]
GO
ALTER TABLE [dbo].[zones1]
	ADD
	CONSTRAINT [DF__zones1__z_level__60AA21A8]
	DEFAULT ('3') FOR [z_level]
GO
ALTER TABLE [dbo].[zones1]
	ADD
	CONSTRAINT [DF__zones1__z_order__619E45E1]
	DEFAULT ('1000') FOR [z_order]
GO
ALTER TABLE [dbo].[zones1]
	ADD
	CONSTRAINT [DF__zones1__z_color__62926A1A]
	DEFAULT (NULL) FOR [z_color]
GO
ALTER TABLE [dbo].[zones1] SET (LOCK_ESCALATION = TABLE)
GO
