SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Consignment]
AS

	Select * From [consignment_Archive_18-19]  	
	Union
	Select * From [consignment_Archive_19-20] 
	Union
	Select * From [consignment]

GO
