SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Coupon]
AS

	Select * From [cdcoupon_Archive_18-19]
	Union
	Select * From [cdcoupon_Archive_19-20] 
	Union
	Select * From [cdcoupon]

GO
