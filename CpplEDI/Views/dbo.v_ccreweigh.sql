SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[v_ccreweigh]
AS
SELECT 'M' as [Source]
      ,[ccr_coupon]
      ,[ccr_stamp]
      ,[ccr_location]
      ,[ccr_deadweight]
      ,[ccr_dimension0]
      ,[ccr_dimension1]
      ,[ccr_dimension2]
      ,[ccr_volume]
  FROM [cpplEDI].[dbo].[ccreweigh]

Union all

SELECT 'H' as [Source]
      ,[crh_coupon]
      ,[crh_stamp]
      ,[crh_location]
      ,[crh_deadweight]
      ,[crh_dimension0]
      ,[crh_dimension1]
      ,[crh_dimension2]
      ,[crh_volume]
  FROM [cpplEDI].[dbo].[ccreweighhold]



GO
