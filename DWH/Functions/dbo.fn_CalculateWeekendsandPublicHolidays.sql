SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_CalculateWeekendsandPublicHolidays](@Date date,@Zone varchar(20),@BusinessDays Integer) returns date as

begin

declare @outputdate date =@Date



--declare @outputdate date ='2015-03-24'

--declare @BusinessDays int=8

--declare @Zone varchar(20)='WA4'



WHILE @BusinessDays > 0

BEGIN

 --Select  @outputdate = case when datename(dw,@outputdate) = 'Friday' then  dateadd(day,3,@outputdate) end

	--                           when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 

 --                              when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 

	--						   ELSE dateadd(day,1,@outputdate) end



Select  @outputdate =dateadd(day,1,@outputdate)



WHILE (SELECT count(*) FROM DWH.[dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))<>0

   BEGIN

     Select  @outputdate = dateadd(day,1,@outputdate)

	END

 Select  @BusinessDays = @BusinessDays -1

END







--WHILE (SELECT count(*) FROM DWH.[dbo].[PublicHolidays_Temp] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))<>0

--   BEGIN

--     Select  @outputdate = dateadd(day,1,@outputdate)

--   END



return @outputdate

end
GO
GRANT EXECUTE
	ON [dbo].[fn_CalculateWeekendsandPublicHolidays]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_CalculateWeekendsandPublicHolidays]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_CalculateWeekendsandPublicHolidays]
	TO [couriersplease\Harley.BoydSkinner]
GO
