SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CalculateWeekendsandPublicHolidays_backup_AK20150130](@Date date,@Zone varchar(20),@BusinessDays Integer) returns date as
begin
declare @outputdate date = @date


WHILE @BusinessDays > 0
BEGIN
    Select  @outputdate = case when datename(dw,@outputdate) = 'Friday' then dateadd(day,3,@outputdate)
	                           when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 
                               when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 
							   ELSE dateadd(day,1,@outputdate) end
    Select  @BusinessDays = @BusinessDays -1
	
END

IF EXISTS (SELECT 1 FROM DWH.[dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	 Select  @outputdate = case when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 
                               when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 
							   ELSE @outputdate end
	  IF EXISTS (SELECT 1 FROM DWH.[dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))
	   BEGIN
		 Select  @outputdate = dateadd(day,1,@outputdate)
	     Select  @outputdate = case when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 
                               when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 
							   ELSE @outputdate end
	  END
   END

return @outputdate
end

GO
GRANT EXECUTE
	ON [dbo].[fn_CalculateWeekendsandPublicHolidays_backup_AK20150130]
	TO [COURIERSPLEASE\lynn.wong]
GO
