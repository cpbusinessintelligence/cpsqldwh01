SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create function [dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups](@Date date,@Zone varchar(20),@BusinessDays Integer) returns date as
begin
declare @outputdate date =@Date
--declare @BusinessDays Integer = 3
--declare @Zone varchar(20)='ADLM'

WHILE @BusinessDays > 0
BEGIN
    Select  @outputdate = case when datename(dw,@outputdate) = 'Friday' then dateadd(day,3,@outputdate)
	                           when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 
                               when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 
							   ELSE dateadd(day,1,@outputdate) end
IF EXISTS (SELECT 1 FROM DWH.[dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	 end
    Select  @BusinessDays = @BusinessDays -1
	
END
IF  datename(dw,@outputdate) = 'Saturday' or datename(dw,@outputdate) = 'Sunday'  
   BEGIN

 Select  @outputdate =  case when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 
                             when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 
							 else @outputdate end
	END

IF EXISTS (SELECT 1 FROM DWH.[dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	 Select  @outputdate = case when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 
                               when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 
							   ELSE @outputdate end
	  IF EXISTS (SELECT 1 FROM DWH.[dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))
	   BEGIN
		 Select  @outputdate = dateadd(day,1,@outputdate)
	     Select  @outputdate = case when datename(dw,@outputdate) = 'Saturday' then dateadd(day,2,@outputdate) 
                               when datename(dw,@outputdate) = 'Sunday'  THEN  dateadd(day,1,@outputdate) 
							   ELSE @outputdate end
	  END
   END

return @outputdate
end


GO
GRANT EXECUTE
	ON [dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_CalculateWeekendsandPublicHolidaysforMisPickups]
	TO [couriersplease\Harley.BoydSkinner]
GO
