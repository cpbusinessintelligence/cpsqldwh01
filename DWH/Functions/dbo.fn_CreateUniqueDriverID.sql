SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CreateUniqueDriverID] (@sBranch varchar(20),@sRunNumber varchar(20),@sProntoID varchar(20))
returns varchar(20)
as
begin
	Declare @sOutput as char(20) ='' 
	Select @sOutput = Rtrim(Ltrim(CASE @sBranch WHEN 'Adelaide' THEN 'ADL' WHEN 'Brisbane' THEN 'BNE' WHEN 'Gold Coast' THEN 'GLC' WHEN 'Goldcoast' THEN 'GLC' WHEN 'Sydney' THEN 'SYD' WHEN 'Melbourne' THEN 'MEL' WHEN 'Perth' THEN 'PER' WHEN 'Nkope' THEN 'NKP' ELSE 'XXX' END 
	                  +  dbo.fn_prefillZeros(ISnull(@sRunNumber,'XXXX'),4)
					  +  ISnull(@sProntoID,'XXXX')))

	Return @sOutput
end

GO
GRANT EXECUTE
	ON [dbo].[fn_CreateUniqueDriverID]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_CreateUniqueDriverID]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_CreateUniqueDriverID]
	TO [couriersplease\Harley.BoydSkinner]
GO
