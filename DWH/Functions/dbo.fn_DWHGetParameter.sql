SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_DWHGetParameter] (@sCode varchar(100))
returns varchar(800)
as
begin
	Declare @sParmValue varchar(800)

	Select 
		@sParmValue = [Value] 
	From 
		DWHParameters
	Where 
		[Code]=@sCode

	If @sParmValue Is Null
		Select @sParmValue=''

	Return @sParmValue
end

GO
GRANT EXECUTE
	ON [dbo].[fn_DWHGetParameter]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_DWHGetParameter]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_DWHGetParameter]
	TO [couriersplease\Harley.BoydSkinner]
GO
