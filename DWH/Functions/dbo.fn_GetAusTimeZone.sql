SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create function fn_GetAusTimeZone(@State varchar(3)) returns datetime as
begin

declare @Output datetime

--Create table #temp(State varchar(20),StdTimeZone decimal(4,2),DSTimeZone decimal(4,2))
--Insert into #temp values('NSW',10,11),('TAS',10,11),('VIC',10,11),('QLD',10,10),('WA',8,8),('SA',9.5,10.5),('NT',9.5,9.5)

If (datediff(Hour,GetUtcDate(),getdate()))<>10
select @Output=dateadd(minute,((DSTimeZone)*60),getUtcdate())
from TempTimeZone where state=@State 
ELSE
select @Output=dateadd(minute,((StdTimeZone)*60),getUtcdate()) 
from TempTimeZone where state=@State 

return @Output

end
GO
GRANT EXECUTE
	ON [dbo].[fn_GetAusTimeZone]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetAusTimeZone]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetAusTimeZone]
	TO [couriersplease\Harley.BoydSkinner]
GO
