SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetContractorTypefromCode] (@sContarctorType varchar(20))
returns varchar(100)
as
begin
	Declare @sOutput as varchar(100) ='' 
	Select @sOutput = CASE @sContarctorType WHEN 'C' THEN 'Contractor'
	                                        WHEN 'D' THEN 'Driver'
											WHEN 'V' THEN 'Vacant'
											WHEN 'M' THEN 'Management'
											WHEN 'S' THEN 'Second Scanner'
											WHEN 'O' THEN 'Other'
											WHEN 'B' THEN 'Barcode'
											WHEN 'X' THEN 'Unknown'
											ELSE @sContarctorType END

	Return @sOutput
end

GO
GRANT EXECUTE
	ON [dbo].[fn_GetContractorTypefromCode]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetContractorTypefromCode]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetContractorTypefromCode]
	TO [couriersplease\Harley.BoydSkinner]
GO
