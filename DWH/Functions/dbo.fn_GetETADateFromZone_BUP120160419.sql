SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create function [dbo].[fn_GetETADateFromZone_BUP120160419] (@RevenueType varchar(20),@AccountCode varchar(20),@Prefix varchar(20), @Pickup DateTime,@PickupDriver varchar(20) , @sFromZone varchar(20), @sToZone varchar(20))
returns dateTime
as
begin
   Declare @OutputDateTime as DateTime
   Declare @PickUpDate as Date = Convert(Date,@Pickup)
   Declare @PickupTime As Time = Convert(Time,@Pickup)
   Declare @BusinessDays  as Varchar(10) 
   Declare @DeliveryTime As Varchar(20)
   IF( Select Count(*) From [DWH].[dbo].[PerformanceReportingExceptions] WHere ProductType = @RevenueType 
                                                                           and [Prefix/AccountCode]= CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END ) >0
	  BEGIN
         Select @BusinessDays = ISNULL((Select CASE [1stPickupCutOff] WHEN 'XXX' THEN [2ndAllowedDays] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOff]) THEN [2ndAllowedDays] ELSE [1stAllowedDays] END) END  
                                                          From  [DWH].[dbo].[PerformanceReportingExceptions]
                                                          Where ProductType = @RevenueType
																and [Prefix/AccountCode]= CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END
	  															and [PickupBranch] = (SELECT [Branch] FROM [DWH].[dbo].[DimContractor] where DriverID = @PickupDriver)),'XXX')
         Select @DeliveryTime = ISNULL((Select CASE [1stPickupCutOff] WHEN 'XXX' THEN [2ndDeliveryOff] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOff]) THEN [2ndDeliveryOff] ELSE [1stDeliveryCutOff] END) END  
                                                          From  [DWH].[dbo].[PerformanceReportingExceptions]
                                                          Where ProductType = @RevenueType
																and [Prefix/AccountCode]= CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END
	  															and [PickupBranch] = (SELECT [Branch] FROM [DWH].[dbo].[DimContractor] where DriverID = @PickupDriver)),'XXX') 
	  END
	  ELSE 
	  BEGIN
         Select @BusinessDays   =  ISNULL((Select CASE [1stPickupCutOffTime] WHEN 'XXX' THEN [2ndAllowedDays] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOffTime]) THEN [2ndAllowedDays] ELSE [1stAllowedDays] END) END  
                                                          From  [DWH].[dbo].[ETACalculator] 
                                                          Where [FromZone] = @sFromZone and [ToZone] = @sToZone),'XXX')
         Select @DeliveryTime   =  ISNULL((Select CASE [1stPickupCutOffTime] WHEN 'XXX' THEN [2ndDeliveryCutOffTime] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOffTime]) THEN [2ndDeliveryCutOffTime] ELSE [1stDeliveryCutOffTime] END) END  
                                                          From  [DWH].[dbo].[ETACalculator] 
                                                          Where [FromZone] = @sFromZone and [ToZone] = @sToZone),'XXX') 
	  END
   IF @BusinessDays <> 'XXX'
        IF ISnull(@DeliveryTime,'XXX') <> 'XXX'
          Select @OutputDateTime = CAST(CAST([dbo].[fn_CalculateWeekendsandPublicHolidays](@Pickup,@sToZone,@BusinessDays)AS DATE) AS DATETIME) +  CAST(CAST(@DeliveryTime AS TIME) AS DATETIME)
		ELSE 
		  Select @OutputDateTime = CAST(CAST([dbo].[fn_CalculateWeekendsandPublicHolidays](@Pickup,@sToZone,@BusinessDays)AS DATE) AS DATETIME) +  CAST(CAST('23:59:59' AS TIME) AS DATETIME)
   Return @OutputDateTime
end

GO
