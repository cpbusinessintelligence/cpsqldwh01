SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetETADateFromZone_BUP_JP] (@RevenueType varchar(20),@AccountCode varchar(20),@Prefix varchar(20), @Pickup DateTime,@PickupDriver varchar(20), @sFromZone varchar(20), @sToZone varchar(20))
returns date
as
begin
   Declare @OutputDate as Date
   Declare @PickUpDate as Date = Convert(Date,@Pickup)
   Declare @PickupTime As Time = Convert(Time,@Pickup)
   Declare @BusinessDays  as Varchar(10) 
   IF( Select Count(*) From [DWH].[dbo].[PerformanceReportingExceptions] WHere ProductType = @RevenueType 
                                                                           and [Prefix/AccountCode]= CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END ) >0
      Select @BusinessDays = ISNULL((Select CASE [1stPickupCutOff] WHEN 'XXX' THEN [2ndAllowedDays] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOff]) THEN [2ndAllowedDays] ELSE [1stAllowedDays] END) END  
                                                          From  [DWH].[dbo].[PerformanceReportingExceptions]
                                                          Where ProductType = @RevenueType
																and [Prefix/AccountCode]= CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END
																and [PickupBranch] = (SELECT [Branch] FROM [DWH].[dbo].[DimContractor] where DriverID = @PickupDriver)),'XXX')
   ELSE 
      Select @BusinessDays   =  ISNULL((Select CASE [1stPickupCutOff] WHEN 'XXX' THEN [2ndAllowedDays] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOff]) THEN [2ndAllowedDays] ELSE [1stAllowedDays] END) END  
                                                          From  [DWH].[dbo].[ETACalculator] 
                                                          Where [FromZone] = @sFromZone and [ToZone] = @sToZone),'XXX')

   IF @BusinessDays <> 'XXX'
      Select @OutputDate= [dbo].[fn_CalculateWeekendsandPublicHolidays](@Pickup,@sToZone,@BusinessDays)
  
   Return @OutputDate
end

GO
