SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_GetETAZone](@PostCode varchar(20) ,@Suburb varchar(20)) returns varchar(800) as
begin

		Return ISNULL((Select [ETAZone]  FROM DWH.dbo.Postcodes 
                            Where Postcode =  CASE LEn(Rtrim(Ltrim(@PostCode))) WHEN 3 THEN '0'+Rtrim(Ltrim(@PostCode)) ELSE  Rtrim(Ltrim(@PostCode)) END 
								  and Suburb = Rtrim(Ltrim(@Suburb))),'')

end

GO
GRANT EXECUTE
	ON [dbo].[fn_GetETAZone]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetETAZone]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetETAZone]
	TO [couriersplease\Harley.BoydSkinner]
GO
