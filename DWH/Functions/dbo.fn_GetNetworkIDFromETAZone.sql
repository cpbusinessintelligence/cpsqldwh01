SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetNetworkIDFromETAZone] (@sFromZone varchar(20), @sToZone varchar(20))
returns integer
as
begin
   Declare @OutputID as integer = 0
   IF @sFromZone <> '' and @sToZone <> ''
        Select @OutputID =  ISNULL((Select n.NetworkCategoryId From  [DWH].[dbo].[ETACalculator] e join [dbo].[NetworkCategory] n on n.[NetworkCategory]=e.PrimaryNetworkCategory and n.[SecondaryNetworkCategory]=e.SecondaryNetworkCategory
                                                          Where [FromZone] = @sFromZone and [ToZone] = @sToZone),0)

 
    Return @OutputID
end

GO
GRANT EXECUTE
	ON [dbo].[fn_GetNetworkIDFromETAZone]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetNetworkIDFromETAZone]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetNetworkIDFromETAZone]
	TO [couriersplease\Harley.BoydSkinner]
GO
