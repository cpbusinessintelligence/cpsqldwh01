SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetNetworkIdFromDrivers] ( @sPickupDriver varchar(20), @sDeliveryDriver varchar(20))
returns int
as
begin
   Declare @Output as int
/****** Script for SelectTopNRows command from SSMS  ******/
     Return(SELECT [NetworkCategoryID]
        FROM [DWH].[dbo].[NetworkCategory] N Join DWH.dbo.DimContractor C on N.OriginDepotCode = C.DepotCode 
		                                     Join  DWH.dbo.DimContractor C2 on N.DestinationDepotCode = C2.DepotCode 
		Where C.DriverID = @sPickupDriver and C2.DriverID = @sDeliveryDriver )
  
end

GO
GRANT EXECUTE
	ON [dbo].[fn_GetNetworkIdFromDrivers]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetNetworkIdFromDrivers]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetNetworkIdFromDrivers]
	TO [couriersplease\Harley.BoydSkinner]
GO
