SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetStatusCodefromDescription] (@sDescription varchar(200))
returns varchar(50)
as
begin
	
	Return Isnull((SELECT [Code]
        FROM [DWH].[dbo].[StatusCodes] where Description = @sDescription),'')
end

GO
GRANT EXECUTE
	ON [dbo].[fn_GetStatusCodefromDescription]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetStatusCodefromDescription]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetStatusCodefromDescription]
	TO [couriersplease\Harley.BoydSkinner]
GO
