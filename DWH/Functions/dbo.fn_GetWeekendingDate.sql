SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetWeekendingDate] (@Date Date)
returns Date
as
begin

	Return (Select  COnvert(Date,WeekEndingdate)  from [dbo].[DimCalendarDate] Where Date =@Date)
end

GO
GRANT EXECUTE
	ON [dbo].[fn_GetWeekendingDate]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetWeekendingDate]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_GetWeekendingDate]
	TO [couriersplease\Harley.BoydSkinner]
GO
