SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PrefillZeros]
 (@Input varchar(20), @length int)
   returns varchar(20)
as
begin
       Declare @Strlen  Int
       Declare @Output varchar(25) = ''
       Select @Strlen = len(@Input)
       
       if @Strlen = @length
              Select @Output = @Input
          
       if  @Strlen > @length
              Select @Output =  Left(@Input,@length)  
       if @Strlen < @length
         Begin
           while Len(@Input) < @Length
           BEGIN 
             Select @input ='0'+Ltrim( @Input)
             Select @Output = @input
           END
         End
		Return @Output
end
GO
GRANT EXECUTE
	ON [dbo].[fn_PrefillZeros]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_PrefillZeros]
	TO [SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[fn_PrefillZeros]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[fn_PrefillZeros]
	TO [couriersplease\Harley.BoydSkinner]
GO
