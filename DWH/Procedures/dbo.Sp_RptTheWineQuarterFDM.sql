SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <30th Jan,2017>
-- Description:	<The Wine Quarter Manifeast File>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptTheWineQuarterFDM] (@From Date, @To Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    
   SELECT distinct
       cd_connote as [Connote No]
	  ,isnull(l.LabelNumber,'') LabelNumber
	,c.cd_account
      ,cast(cd_date as Date) as Date	
	  ,'DEL' as Type
	  ,cd_delivery_addr0 as [Receiver Address 0]
	  ,cd_delivery_addr1 as [Receiver Address 1]
	  ,cd_delivery_addr2 as [Receiver Address 2]	
	  , cd_delivery_suburb as [Receiver Suburb]
	  , cd_delivery_postcode as [Receiver Postcode]
	  , cd_pickup_count as [No of items]
	  ,cd.cc_coupon as [Item Number]
	  ,r.cr_reference as [Reference]
	  ,cd_import_deadweight as Weight
	  , cd_volume as Cube
	  , cd_pickup_contact as [Sender Name]
	  , cd_pickup_addr0 as [Sender Address 0]	
	  , cd_pickup_addr1 as [Sender Address 1]	 
	  , cd_pickup_addr2 as [Sender Address 2]	
	    , cd_pickup_suburb as [Sender Suburb]
	  , cd_pickup_postcode as [Sender Postcode]
	  ,'' as [Sender State]
	  ,'' as Service
	  ,cd_company_id as ID
	  , com.c_code as [Sender Code]
	  ,cd_special_instructions as [Special Instructions]
	  ,'' as [Contact Name]
	  ,cd_delivery_contact_phone as [Contact Phone]
	  ,cd_delivery_email as [Contact email]
	  ,'' [ATL Field]
	  ,cc_coupon coupon
	   FROM [CpplEDI].[dbo].[consignment] c left join  [CpplEDI].dbo.cdcoupon cd on c.cd_id= cd.cc_consignment
	   left join [ScannerGateway].dbo.Label l on l.LabelNumber = cd.cc_coupon	
	   left join [CpplEDI].[dbo].[cdref] r on r.cr_consignment=cd_id 
	   left join [CpplEDI].dbo.companies com on com.c_id= cd_company_id
	   where c.cd_date between @From and @To
	   and c.cd_account = '112988894'
	   and l.LabelNumber is not null
	  
END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptTheWineQuarterFDM]
	TO [ReportUser]
GO
