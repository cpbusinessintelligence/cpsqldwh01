SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[Z1sp_RptDailyDepotDriversPerformanceOverview]
(@DepotName Varchar(50),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[[sp_RptDailyDepotDriversPerformanceOverview]]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
    SELECT Isnull(State,'Unknown') as State,Isnull(DepotName,'Unknown') as DepotName, ISNULL(DriverID,'') as Driver
       ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = @Date  and [OnTimeStatus] Not in ('A','AE','NE','E','X') and DepotName = @DepotName
  Group by Isnull(State,'Unknown')
          ,Isnull(Branch,'Unknown')
		  ,Isnull(DepotName,'Unknown')
		  , ISNULL(DriverID,'') 
		  ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.Driver = #Temp1.Driver)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0


  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [DWH].[dbo].[TargetKPI] K on T.[State] = K.[State] 
     Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'





	  SELECT  'Achieved' as ComplianceStatus
	      , Isnull(State,'Unknown') as State
	      ,ISNULL(DriverID,'') as Driver
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
		
  INTO #Temp2 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C     (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = @Date and [OnTimeStatus] not in ('A','AE','NE','E','X') and D.DepotName = @DepotName
  Group By Isnull(State,'Unknown'),ISNULL(DriverID,'')
   Union all
  SELECT   'Not Achieved' as ComplianceStatus
           , Isnull(State,'Unknown') as State
 	      ,ISNULL(DriverID,'') as Driver
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
	  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
   WHere C.Date = @Date and [OnTimeStatus] not in ('A','AE','NE','E','X') and  D.DepotName = @DepotName
   Group By Isnull(State,'Unknown'),ISNULL(DriverID,'')
   Update #Temp2 SET TotalPercentage = Convert(decimal(12,2),([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount])*100)/Convert(decimal(12,2),[PerformanceCount]*3)
   Update #Temp2 SET TargetKPI =K.Target *100 From #Temp1 T join [DWH].[dbo].[TargetKPI] K on T.[State] = K.[State] 
       Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='SCAN-COMP'


   Select DriverID,
          Branch,
		  DepotName,
		  DriverNumber,
		  ProntoID, 
		  Convert(decimal(12,2),0) as OnTimePercentage,
		  Convert(decimal(12,2),0) as ScanningPercentage,
		  Convert(int,0) as Deliveries,
		  Convert(int,0) as Pickups,
		  Convert(int,0) as Total,
		  Convert(int,0) as NoTime
INTO #TempFinal
from [DWH].[dbo].[DimContractor] D (NOLOCK) Where DepotName = @DepotName 

Update #TempFinal SET OnTimePercentage  =T.PerformanceKPI ,DEliveries= T.Total From #TempFinal F Join #Temp1 T on F.DepotName=T.DepotName and T.OnTimeStatus = 'Y' and F.DriverID = T.Driver
Update #TempFinal SET ScanningPercentage  =T.TotalPercentage From #TempFinal F Join #Temp2 T on  T.ComplianceStatus = 'Achieved' and F.DriverID = T.Driver
Update #TempFinal SET NoTime = N.NoTimeCount   From #TempFinal T join [DWH].[dbo].[DailyNoTimeReporting] N on T.DriverID = N.OnBoardDriver
 
Select * from #TempFinal WHere OnTimePercentage<>0 order by OnTimePercentage + ScanningPercentage desc
end


GO
