SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[Z1sp_RptDailyDepotETAwithNoDeliveryOverview]
(@Depot Varchar(30),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyBranchETAwithNoDeliveryOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  SELECT Isnull(D.Branch,'Unknown') as Branch
   ,Isnull(D.DepotName,'Unknown') as DepotName
          , SUM([ETANoDeliveryCount]) as [ETANoDeliveryCount]
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyETANoDeliveryReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[ETADayID]= C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[PickupDriver] = D.DriverID
  WHere C.Date = @Date  and D.Branch = @Depot
  GROUP By  Isnull(D.Branch,'Unknown')
   ,Isnull(D.DepotName,'Unknown')

  Select DepotName,[ETANoDeliveryCount] from #Temp1

end
GO
GRANT EXECUTE
	ON [dbo].[Z1sp_RptDailyDepotETAwithNoDeliveryOverview]
	TO [ReportUser]
GO
