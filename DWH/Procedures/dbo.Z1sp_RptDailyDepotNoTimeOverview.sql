SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[Z1sp_RptDailyDepotNoTimeOverview]
(@Depot Varchar(30),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyBranchNoTimeOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  SELECT Isnull(D.Branch,'Unknown') as Branch
  ,Isnull(D.DepotName,'Unknown') as DepotName
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyNoTimeReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
                                                     JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere C.Date = @Date  And D.Branch=@Depot
  GROUP By  Isnull(D.Branch,'Unknown'),Isnull(D.DepotName,'Unknown')
  Select DepotName ,NoTimeCount from #Temp1
end
GO
GRANT EXECUTE
	ON [dbo].[Z1sp_RptDailyDepotNoTimeOverview]
	TO [ReportUser]
GO
