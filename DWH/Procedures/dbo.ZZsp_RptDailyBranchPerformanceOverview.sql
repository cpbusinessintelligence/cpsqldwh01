SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[ZZsp_RptDailyBranchPerformanceOverview]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyBranchPerformanceOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
   SELECT Isnull(State,'Unknown') as State,Isnull(Branch,'Unknown') as Branch
       ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
	    ,Convert(Varchar(20),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = @Date  and [OnTimeStatus] Not in ('A','AE','NE','X','E')
  Group by Isnull(State,'Unknown'),Isnull(Branch,'Unknown'),CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END

  Update #Temp1 SET [Descr] =S.description From #Temp1 T join [dbo].[StatusCodes] S on T.OnTimeStatus=S.code
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.Branch = #Temp1.Branch)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0

--SELECT Convert(Varchar(20),'PERFORMANCE') as [Category]
--      ,[Code]
--      ,[Description]
--      ,[SortOrder],Convert(Int,0) as Total
--	  ,Convert(decimal(12,2),0) as Percentage
--  into #TempStatusCodes   
--  FROM [DWH].[dbo].[StatusCodes] Where Code not in ('A','AE','CN','CY') 

 
--  Update #TempStatusCodes SET  Total  = T.PerformanceCount , 
--       Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1),0)) 
--  From  #TempStatusCodes S Join #Temp1 T on S.[Code] = T.[OnTimeStatus] 
--  Where S.Category = 'PERFORMANCE'
Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [DWH].[dbo].[TargetKPI] K on T.[State] = K.[State] 
Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1  Where Branch not in ( 'Perth','Unknown')
  order by PerformanceKPI desc
end
GO
GRANT EXECUTE
	ON [dbo].[ZZsp_RptDailyBranchPerformanceOverview]
	TO [ReportUser]
GO
