SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[ZZsp_RptDailyBranchPerformanceSummary]
(@Branch varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalPerformanceandComplianceOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  SELECT CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		,SUM([PODComplainceCount]) as [PODComplainceCount]
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id       
                                                          JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                            
  WHere C.Date = @Date and D.Branch=@Branch
    and [OnTimeStatus] not in ('A','AE','NE','X','E')
  Group by CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END

  SELECT Convert(Varchar(20),'PERFORMANCE') as [Category]
      ,[Code]
      ,[Description]
      ,[SortOrder],Convert(Int,0) as Total
	  ,Convert(decimal(12,2),0) as Percentage
  into #TempStatusCodes   
  FROM [DWH].[dbo].[StatusCodes] Where Code not in ('A','AE','CN','CY','NE','X','E') 

 
  Update #TempStatusCodes SET  Total  = T.PerformanceCount , 
                               Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1),0)) 
  From  #TempStatusCodes S Join #Temp1 T on S.[Code] = T.[OnTimeStatus] 
  Where S.Category = 'PERFORMANCE'

 
  Select * from #TempStatusCodes order by [SortOrder] ASc

end
GO
GRANT EXECUTE
	ON [dbo].[ZZsp_RptDailyBranchPerformanceSummary]
	TO [ReportUser]
GO
