SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[Z_sp_LoadDailyETANoDeliveryReporting]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadDailyNoTimeReporting]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
   Declare @CalendarDayId as Integer = ISnull((SELECT [Id] FROM [DWH].[dbo].[DimCalendarDate] where Date = @Date),0)
   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)
   
INSERT INTO [dbo].[DailyETANoDeliveryReporting]
           ([ETADayID]
           ,[RevenueType]
           ,[PickupDriver]
           ,[PickupETAZone]
           ,[DeliveryETAZone]
           ,[BUCode]
           ,[ETANoDeliveryCount]
           ,[AddWho]
           ,[AddDateTime])
   Select @CalendarDayId
        , RevenueType
		, Isnull(PickupScannedBy,'')
		 ,[PickupETAZone]
		 ,[DeliveryETAZone]
		 ,[BUCode]
		 ,COUNT(*)
		 ,'Sys'
		 ,Getdate()
   From [DWH].[dbo].[PrimaryLabels] 
   Where Convert(date,ETADate) = @Date and ((AttemptedDeliveryDateTime is null and DeliveryDateTime is null) OR COnvert(date,[DeliveryDateTime]) >@EndDate OR Convert(Date,AttemptedDeliveryDateTime) > @EndDate)
   Group by RevenueType,
          Isnull(PickupScannedBy,''),
		   [PickupETAZone],
		   [DeliveryETAZone],
		    [BUCode]
end

GO
