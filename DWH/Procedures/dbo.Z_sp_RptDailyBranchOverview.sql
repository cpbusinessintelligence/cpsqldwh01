SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[Z_sp_RptDailyBranchOverview]
(@Branch varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
Select 1 
Select 2
Select 3
Select 4/0
end
GO
