SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[cppl_UpdateDWH_Reload](@Date date)
as
begin

exec [sp_LoadConsignmentsandLabels] @Date
print '1 done'
exec [sp_LoadTrackingEvents] @Date
print '2 done'
exec [sp_CalculateETAandPerformance] @Date
print '3 done'
exec [sp_LoadDailyPerformanceReporting] @Date
print '4 done'
EXEC [sp_LoadDailyNoTimeReporting] @Date
print '5 done'

end
GO
GRANT EXECUTE
	ON [dbo].[cppl_UpdateDWH_Reload]
	TO [COURIERSPLEASE\lynn.wong]
GO
