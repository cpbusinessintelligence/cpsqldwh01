SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_CalculateETAandPerformance] 
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_CalculateETAandPerformance]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================


   Update DWH.[dbo].[PrimaryLabels] SET PerformanceProcessed = 1 , 
                                        PerformanceReportingDate = @Date
		 WHere (AttemptedDeliveryDateTime is not null or DeliveryDateTime is not null) 
		     and  isnull(PerformanceProcessed,0)=0 
			 and  Convert(date,Isnull(AttemptedDeliveryDateTime,DeliveryDateTime)) = @Date

   	SELECT RevenueType
	  ,Accountcode
      ,[LabelNumber]
      ,[PickupScannedBy]
      ,[PickupDateTime]
	  ,PickupETAZone as PickUpZone
	  ,AttemptedDeliveryDateTime
	  ,AttemptedDeliveryScannedBy
      ,DeliveryDateTime
	  ,DeliveryScannedBy
	  ,DeliveryETAZone as DeliveryZone
      ,[ETADate]
	  ,DeliveryContractorType
	  ,NetworkCategoryID 
      ,[OnTimeStatus]
      ,[PerformanceReportingDate]
	  ,PerformanceProcessed
	  ,[LabelCreatedDateTime]
  Into #TempLabels
  FROM [DWH].[dbo].[PrimaryLabels] (NOLOCK)
  Where  PerformanceProcessed = 1 
      and PerformanceReportingDate = @Date 



  Update #TempLabels SET PickUpZone =C.ETAZone  
             From #TempLabels L Join [DWH].[dbo].[DimContractor]  C on L.PickupScannedBy = C.DriverID 
			 Where Isnull(PickUpZone,'')= ''

  Update #TempLabels SET DeliveryZone =C.ETAZone,
       DeliveryContractorType = Case  C.ContractorCategory WHEN 'Agent' THEN 'Agent' ELSE '' END  
		     From #TempLabels L Join [DWH].[dbo].[DimContractor]  C on Isnull(L.AttemptedDeliveryScannedBy,L.DeliveryScannedBy) = C.DriverID   and ISnull(DeliveryZone,'') =''

  Update #TempLabels SET DeliveryContractorType = 'Agent',
                         [OnTimeStatus] = [dbo].[fn_GetStatusCodefromDescription] ('Agent Manifested')
             From #TempLabels P Join CpplEdi.dbo.consignment C on P.LabelNumber = C.cd_connote 
			 where P.RevenueType = 'Prepaid'
		
  Update #TempLabels SET ETADate = [dbo].[fn_GetETADateFromZone](RevenueType,Accountcode,LEft(LabelNumber,3),Convert(datetime,[PickupDateTime]),PickupScannedBy,PickUpZone,DeliveryZone) 
             where [PickupDateTime] is not null and ETADate is null
  Update #TempLabels SET ETADate = [dbo].[fn_GetETADateFromZone](RevenueType,Accountcode,LEft(LabelNumber,3),Convert(datetime,[LabelCreatedDateTime]),'',PickUpZone,DeliveryZone) 
             where [PickupDateTime] is  null and RevenueType = 'EDI' and ETADate is null


  Update #TempLabels SET [OnTimeStatus] = CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
                                            THEN  [dbo].[fn_GetStatusCodefromDescription] ('OnTime with ConDate')  ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not OnTime with ConDate')  END
             Where  Isnull([OnTimeStatus],'') ='' and  Etadate is not null and  [PickupDateTime] is  null and RevenueType = 'EDI'


  Update #TempLabels SET [OnTimeStatus] = CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
	            THEN [dbo].[fn_GetStatusCodefromDescription] ('OnTime') ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not OnTime')  END
              Where  Isnull([OnTimeStatus],'') ='' and  Etadate is not null 

  Update  #TempLabels SET [OnTimeStatus] =  [dbo].[fn_GetStatusCodefromDescription] ('Agent Manifested') Where DeliveryContractorType = 'Agent' and [OnTimeStatus] <>  [dbo].[fn_GetStatusCodefromDescription] ('Agent Manifested')
  Update  #TempLabels SET [OnTimeStatus] =[dbo].[fn_GetStatusCodefromDescription] ('Unknown') 
             Where ETADate is null and  Isnull([OnTimeStatus],'') =''

  Update [DWH].[dbo].[PrimaryLabels] SET  PickupETAZone =T.PickUpZone,
                                          DeliveryETAZone = T.DeliveryZone,
										  ETADate=T.ETADate,
										  DeliveryContractorType = T.DeliveryContractorType,
										  [OnTimeStatus] = T.OnTimeStatus
              From #TempLabels T Join [DWH].[dbo].[PrimaryLabels] P on T.LabelNumber = P.LabelNumber
-------------------------------------
-------------14 Day Exception--------
-------------------------------------

  Update DWH.[dbo].[PrimaryLabels] SET PerformanceProcessed = 1 , 
                                       PerformanceReportingDate = @Date ,
									   [OnTimeStatus] =  [dbo].[fn_GetStatusCodefromDescription] ('CPPL No POD') 
             WHere  isnull(PerformanceProcessed,0)=0 
			      and  [LabelCreatedDateTime] <= Dateadd(Day,-14,@Date)
				  and ISnull([OnTimeStatus],'') = ''
  Update  DWH.[dbo].[PrimaryLabels] SET DeliveryContractorType = 'Agent',
                                        [OnTimeStatus] = [dbo].[fn_GetStatusCodefromDescription] ('Agent No POD') 
             From DWH.[dbo].[PrimaryLabels] P Join CpplEdi.dbo.consignment C on P.LabelNumber = C.cd_connote 
             WHere  PerformanceProcessed = 1 
			      and PerformanceReportingDate = @Date 
				  and ISnull([OnTimeStatus],'') = [dbo].[fn_GetStatusCodefromDescription] ('CPPL No POD') 
 Update  DWH.[dbo].[PrimaryLabels] SET [OnTimeStatus] = [dbo].[fn_GetStatusCodefromDescription] ('No Activity') 
             WHere PerformanceReportingDate = @Date 
			      and PickupDateTime is null 
			      and ISnull([OnTimeStatus],'') = [dbo].[fn_GetStatusCodefromDescription] ('CPPL No POD') 
				  and DeliveryDateTime is null 
				  and AttemptedDeliveryDateTime is null           
				  and TransferDateTime is null  
				  and HandoverDateTime is null 
				  and OutForDeliverDateTime is null 
				  and InDepotDateTime is null


----------------------------------------------
---Calculate Network Category
---------------------------------------
  Update DWH.[dbo].[PrimaryLabels] SET [NetworkCategoryID] = [dbo].[fn_GetNetworkIDFromETAZone]([PickupETAZone],[DeliveryETAZone]) 
          Where   PerformanceReportingDate = @Date 
		    and PerformanceProcessed = 1
----------------------------------------------
---Calculate ETA fro non processed EDI Labels
---------------------------------------
  Update DWH.[dbo].[PrimaryLabels] SET ETADate = [dbo].[fn_GetETADateFromZone](RevenueType,Accountcode,LEft(LabelNumber,3),Convert(datetime,[PickupDateTime]),PickupScannedBy,PickupETAZone,DeliveryETAZone) 
             where [PickupDateTime] is not null and ETADate is not null and RevenueType = 'EDI'

end
GO
