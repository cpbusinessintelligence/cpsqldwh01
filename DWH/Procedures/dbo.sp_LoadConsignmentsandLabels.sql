SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_LoadConsignmentsandLabels]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadConsignmentsandLabels]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================

   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)

   --------------------------
   ----- PREPAID-------------
   --------------------------
   --Getting all Valid Prepaid Coupon Type into memory
   
   SELECT [Code]
          ,[Category]
         ,[RevenueCategory]
	INTO #TempPrepaidProducts
    FROM [DWH].[dbo].[DimProduct] where RevenueType = 'Prepaid'
 PRINT 'Loaded all Products int tempTable'

 --Getting all  Prepaid Coupons which had activity

   SELECT  LTRIM(RTRIM(A.SerialNumber)) as SerialNumber,
           LEFT(LTRIM(RTRIM(A.SerialNumber)), 3) as Prefix,
		   MIN(ActivityDateTime) as ActivityDateTime
   INTO #TempActivityImport
   FROM Cosmos.dbo.ActivityImport  A(NOLOCK)
   WHERE  CONVERT(date, A.ActivityDateTime) >= @StartDate
		 AND  CONVERT(date, A.ActivityDateTime) <= @EndDate 
		 AND  ISNUMERIC(A.SerialNumber) = 1
		 AND  LEN(LTRIM(RTRIM(A.SerialNumber))) = 11	                 
   Group By LTRIM(RTRIM(A.SerialNumber))
 PRINT 'Loaded all Coupons which had Activity into temp table'

   
   INSERT INTO [DWH].[dbo].[Consignment]
                ([RevenueType],[ProductType],[ServiceType],[ConNote],[ConNoteCreatedDate],[ItemCount],[CouponCount],[AddWho],[AddDateTime])
	      Select 'Prepaid',T.Prefix,'', SerialNumber,ActivityDateTime,1,1,'Sys',Getdate()
	      from #TempActivityImport T Join #TempPrepaidProducts P on T.Prefix=P.[Code]
	       where P.RevenueCategory = 'PRIMARY'
		      AND NOT EXISTS (SELECT 1  FROM [DWH].[dbo].[Consignment] C  WHERE C.ConNote = LTRIM(RTRIM(T.SerialNumber)))
   PRINT 'Inserted all Prepaid Coupons into Consigment Table'  


	INSERT INTO [dbo].[PrimaryLabels]
           ([RevenueType],[LabelNumber],[AddWho],[LabelCreatedDateTime],[AddDateTime])
	   Select 'Prepaid', SerialNumber,'Sys',ActivityDateTime,Getdate()
	   from #TempActivityImport T Join #TempPrepaidProducts P on T.Prefix=P.[Code]
	   where P.RevenueCategory = 'PRIMARY'
		      AND NOT EXISTS (SELECT 1  FROM [DWH].[dbo].[PrimaryLabels] C  WHERE C.LabelNumber = LTRIM(RTRIM(T.SerialNumber)))
   PRINT 'Inserted all Prepaid Coupons into PrimaryLabels Table'  
			  
	INSERT INTO [DWH].[dbo].[SecondaryLabels]
           ([SecondaryLabelNumber],[SecondaryLabelPrefix],[LabelCreatedDateTime] ,[AddWho],[AddDateTime])
        Select SerialNumber,Left(SerialNumber,3),ActivityDateTime,'Sys',Getdate()
	    from #TempActivityImport T Join #TempPrepaidProducts P on T.Prefix=P.[Code]
	     where P.RevenueCategory <> 'PRIMARY'
		      AND NOT EXISTS (SELECT 1  FROM [DWH].[dbo].[SecondaryLabels] S  WHERE S.SecondaryLabelNumber = LTRIM(RTRIM(T.SerialNumber)))
   PRINT 'Inserted all Prepaid Coupons into SecondaryLabels Table'  

--------------------
------------EDI-----
-------------------
   
   INSERT INTO [DWH].[dbo].[Consignment]([GWConsignmentID],[RevenueType],[ProductType],[ServiceType],[ConNote],[CustomerConNoteDate]
							      ,[ConNoteCreatedDate],[CustomerETADate],[AccountCode],[PickupAddress] 
								  ,[ItemCount],[CouponCount],[AddWho],[AddDateTime])
                           SELECT  cd_id,'EDI',[cd_pricecode],'',Rtrim(Ltrim([cd_connote])),CONVERT(Date,[cd_consignment_date]) 
						          ,CONVERT(Date,[cd_date]),CONVERT(Date,cd_customer_eta),[cd_account],[cd_pickup_addr0]
                                  ,[cd_items],[cd_coupons],'Sys',Getdate()
                           FROM [CpplEDI].[dbo].[consignment] P
						   WHERE CONVERT(Date,[cd_date]) >= @StartDate 
						     and CONVERT(Date,[cd_date]) <=@EndDate
							 AND NOT EXISTS (SELECT 1  FROM [DWH].[dbo].[Consignment] C  WHERE C.ConNote = LTRIM(RTRIM(P.cd_connote)))
	   PRINT 'Inserted all EDI Cons into Consigment Table'  					      

   INSERT INTO [DWH].[dbo].[PrimaryLabels]([GWConsignmentID],[RevenueType],[AccountCode],[LabelNumber],[DeliveryContractorType],[PickupETAZone],[DeliveryETAZone],[AddWho],[LabelCreatedDateTime],[AddDateTime])
						  SELECT C.cd_id,'EDI',C.cd_account,Rtrim(Ltrim(P.cc_coupon)),CASE A.a_cppl WHEN 'N' THEN 'Agent' ELSE '' END,[dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_pickup_postcode])),Rtrim(Ltrim(C.[cd_pickup_suburb]))) 
						  , Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_delivery_postcode])),Rtrim(Ltrim(C.[cd_delivery_suburb]))),'')
						   ,'Sys',C.[cd_date],getdate()
                           FROM [CpplEDI].[dbo].[consignment] C Join CpplEDI.dbo.cdcoupon P On C.cd_id = P.cc_consignment
						                                        Left Join CpplEDI.dbo.agents A on C.cd_agent_id = A.A_id
						   WHERE CONVERT(Date,[cd_date]) >= @StartDate 
						     and CONVERT(Date,[cd_date]) <=@EndDate
							 and Not (ISnumeric([cd_connote])=1 and len([cd_connote]) =11)
                             AND NOT EXISTS (SELECT 1  FROM [DWH].[dbo].[PrimaryLabels] C  WHERE C.LabelNumber = LTRIM(RTRIM(P.cc_coupon)))
    PRINT 'Inserted all EDI Labels into PrimaryLabels Table'  

 Update [DWH].[dbo].[PrimaryLabels] SET [DeliveryETAZone] = Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_delivery_postcode])),Rtrim(Ltrim(C.[cd_delivery_addr2]))),'')  
                FROM [DWH].[dbo].[PrimaryLabels] P  Join [CpplEDI].[dbo].[consignment] C on P.[GWConsignmentID] = C.Cd_id 
                Where Isnull(P.[DeliveryETAZone],'') = '' and P.[RevenueType] = 'EDI' and P.[AddDateTime] >= @StartDate and P.[AddDateTime] <=@EndDate
  PRINT 'Calculated [DeliveryETAZone] for EDI from DeliveryAddress2 '  
 
 Update [DWH].[dbo].[PrimaryLabels] SET [DeliveryETAZone] = Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_delivery_postcode])),Rtrim(Ltrim(C.[cd_delivery_addr3]))),'')  
                FROM [DWH].[dbo].[PrimaryLabels] P  Join [CpplEDI].[dbo].[consignment] C on P.[GWConsignmentID] = C.Cd_id 
                Where ISnull(P.[DeliveryETAZone],'') = '' and P.[RevenueType] = 'EDI' and P.[AddDateTime] >= @StartDate and P.[AddDateTime] <=@EndDate
  PRINT 'Calculated [DeliveryETAZone] for EDI from DeliveryAddress3 '  

 Update [DWH].[dbo].[PrimaryLabels] SET PickupETAZone = Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_pickup_postcode])),Rtrim(Ltrim(C.[cd_pickup_addr2]))),'')  
                FROM [DWH].[dbo].[PrimaryLabels] P  Join [CpplEDI].[dbo].[consignment] C on P.[GWConsignmentID] = C.Cd_id 
                Where Isnull(P.PickupETAZone,'') = '' and P.[RevenueType] = 'EDI' and  P.[AddDateTime] >= @StartDate and P.[AddDateTime] <=@EndDate
  PRINT 'Calculated [PickupETAZone] for EDI from [cd_pickup_addr2] '  
end
GO
