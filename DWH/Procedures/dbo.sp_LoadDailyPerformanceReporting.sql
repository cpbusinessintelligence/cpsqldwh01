SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_LoadDailyPerformanceReporting]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadDailyPerformanceReporting]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
   Declare @CalendarDayId as Integer = ISnull((SELECT [Id] FROM [DWH].[dbo].[DimCalendarDate] where Date = @Date),0)
   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)
   
   INSERT INTO [DWH].[dbo].[DailyPerformanceReporting]
                 ([PerformanceDayID],[RevenueType],[DeliveryDriver],[PickUpDriver],[OnTimeStatus]
				 ,[PickupETAZone],[DeliveryETAZone],[NetworkCategoryID],[BUCode]
				 ,[PerformanceCount],[OnBoardComplainceCount],[DeliveryComplainceCount],
				 [PODComplainceCount],[AddWho],[AddDateTime])
   Select @CalendarDayId
        , RevenueType
		, Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'')
		 ,ISNull(PickupSCannedBy,'')
		 ,[OnTimeStatus]
		 ,[PickupETAZone]
		 ,[DeliveryETAZone]
		 ,[NetworkCategoryID]
		 ,[BUCode]
		 ,COUNT(*)
		 ,SUM(CASE WHEN [OutForDeliverDateTime] is not null THEN 1 ELSE 0 END )
		 ,SUM(CASE WHEN  [DeliveryDateTime] is not Null or AttemptedDeliveryDateTime is not null THEN 1 ELSE 0 END)
		 ,SUM(CASE WHEN AttemptedDeliveryDateTime is not null OR ISNULL(IsPODPresent,'N') = 'Y' THEN 1 ELSE 0 END)
		 ,'Sys'
		 ,Getdate()
   From [DWH].[dbo].[PrimaryLabels] 
   Where [PerformanceReportingDate] = @Date
     and [PerformanceProcessed] =1
   Group by RevenueType,
           Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),''),
		   ISNull(PickupSCannedBy,''),
		   [OnTimeStatus],
		   [PickupETAZone],
		   [DeliveryETAZone],
		   [NetworkCategoryID],
		   [BUCode]
end
GO
