SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_LoadDimAgent] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_LoadDimAgent]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	

  MERGE DWH.dbo.[DimAgent] AS DimA
			USING (SELECT [a_id]
						 ,[a_shortname]
						 ,[a_name]
						 ,[a_supplier_code]
						 ,[a_podrequired]
						 ,[a_cppl]
						 ,[a_querybranch]
						 ,[b_name]
						 ,[a_driver]
						 ,[a_state]
					FROM [CpplEDI].[dbo].[agents] A Left Join [CpplEDI].[dbo].[branchs] B on A.a_querybranch = B.b_id)
		    AS SD	ON DimA.[AgentID] = SD.[a_id] 
            WHEN MATCHED THEN UPDATE SET   [AgentCode] = SD.[a_shortname]
			                              ,[AgentName] = SD.[a_name]
										  ,[SupplierCode] =SD.[a_supplier_code]
							     		  ,[QueryBranch]=SD.[b_name]
										  ,[State]=SD.[a_state]
										  ,[IsCPPLContractor] = SD.[a_cppl]
										  ,[IsPODRequired]=SD.[a_podrequired]
										  ,[DriverNumber]=SD.[a_driver]
            WHEN NOT MATCHED THEN INSERT  ([AgentID],[AgentCode],[AgentName],[SupplierCode],[QueryBranch]
									      ,[State],[IsCPPLContractor],[IsPODRequired],[DriverNumber])
							       VALUES (SD.[a_id],SD.[a_shortname],SD.[a_name],SD.[a_supplier_code],SD.[b_name]
								           ,SD.[a_state],SD.[a_cppl],SD.[a_podrequired],SD.[a_driver]);





end
GO
