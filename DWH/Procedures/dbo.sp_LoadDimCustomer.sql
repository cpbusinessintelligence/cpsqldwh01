SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_LoadDimCustomer] as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadDimCustomer]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	

  MERGE DWH.dbo.DimCustomer AS DimC
			USING (SELECT CASE WHEN [Territory] like 'X%' THEN 'PREPAID' ELSE 'EDI' END as RevenueType
			             ,[Accountcode]
					     ,[BillTo] as ParentCompany
						 ,ISNULL(UPPER([Shortname]),'') as CompanyName
						 ,[Warehouse]
						 ,ISnull([TermDisc],'') as TermDiscount
						 ,[Territory]
						 ,ISNULL([MarketingFlag],'') as [MarketingFlag]
						 ,[AverageDaysToPay]
						 ,[Postcode]
						 ,[Locality]
						 ,[RepCode]
						 ,Replace([RepName],'Not Applicable','') as RepName
						 ,[OriginalRepCode]
						 ,Replace([OriginalRepName],'Not Applicable','') as OriginalRepName
						 ,[LastSale]
					 FROM [Pronto].[dbo].[ProntoDebtor])
		    AS SD	ON DimC.[Accountcode] = SD.[Accountcode] and DimC.RevenueType = SD.RevenueType
            WHEN MATCHED THEN UPDATE SET   [ParentCompany]= SD.[ParentCompany]
			                              ,[CompanyName] = SD.[CompanyName]
										  ,[Warehouse] =SD.[Warehouse]
							     		  ,TermDiscount=SD.TermDiscount
										  ,[Territory]=SD.[Territory]
										  ,[MarketingFlag] = SD.[MarketingFlag]
										  ,[AverageDaysToPay]=SD.[AverageDaysToPay]
										  ,[Postcode]=SD.[Postcode]
										  ,[Locality]=SD.[Locality]
										  ,[RepName]=SD.[RepName]
										  ,[OriginalRepCode]=SD.[OriginalRepCode]
										  ,[OriginalRepName]=SD.[OriginalRepName]
										  ,[LastSale]=SD.[LastSale]


            WHEN NOT MATCHED THEN INSERT  ([RevenueType],[Accountcode],[ParentCompany] ,[CompanyName],[Warehouse]
										  ,[TermDiscount],[Territory],[MarketingFlag],[AverageDaysToPay],[Postcode]
										  ,[Locality],[RepCode],[RepName],[OriginalRepCode],[OriginalRepName],[LastSale])
							       VALUES (SD.[RevenueType],SD.[Accountcode],SD.[ParentCompany],SD.[CompanyName],SD.[Warehouse]
										  ,SD.[TermDiscount],SD.[Territory],SD.[MarketingFlag],SD.[AverageDaysToPay],SD.[Postcode]
										  ,SD.[Locality],SD.[RepCode],SD.[RepName],SD.[OriginalRepCode],SD.[OriginalRepName],SD.[LastSale]);

end
GO
