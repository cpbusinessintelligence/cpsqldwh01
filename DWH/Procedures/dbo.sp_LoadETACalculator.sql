SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_LoadETACalculator] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_LoadETACalculator]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	

   Truncate table [DWH].[dbo].[ETACalculator]
   INSERT INTO [DWH].[dbo].[ETACalculator]
           ([ETACalculatorID]
           ,[FromZone]
           ,[ToZone]
           ,[ETA]
		   ,FromETA
		   ,ToETA) 
	SELECT  [ETAID]
		   ,[FromZone]
		   ,[ToZone]
           ,[ETA]
		   ,(case when  charindex('-',[ETA])=0 then [ETA] else (substring([ETA],1,(charindex('-',[ETA])-1))) end) as FromETA
		   ,(case when  charindex('-',[ETA])=0 then [ETA] else (substring([ETA],charindex('-',[ETA])+1,(len([ETA])-(charindex('-',[ETA]))))) end) as ToETA
    FROM [CouponCalculator].[dbo].[ETACalculator]




end
GO
