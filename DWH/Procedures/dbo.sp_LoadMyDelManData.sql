SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_LoadMyDelManData] 

	-- Parameters for the tblMyDelMan_API
	@ReqID varchar(250),
	@DateOfRequest varchar(250),
	@ConsignmentNumber varchar(100),
	@ConStatus varchar(100),
	@JSONResponse varchar(MAX),
	--@LabelNumber varchar(100),
	--@ReceivedAtDepotDateSql varchar(100),
	--@LabelStatus varchar(250),
	--@Rejected varchar(100),
	--@RejectReason varchar(200),
	--@RejectNote varchar(200),
	--@IconName varchar(100),
	--@DeliveryId varchar(100),
	--@DeliveredBy varchar(100),
	--@CollectedDateSql varchar(100),
	--@CollectedDateString varchar(100),
	--@DeliveredDateSql varchar(100),
	--@DeliveredDateString varchar(100),
	--@ReturnedDateSql varchar(100),
	--@ReturnedDateString varchar(100),
	--@DeliveredLat varchar(100),
	--@DeliveredLon varchar(100),
	--@POD varchar(MAX),
	--@SignedBy varchar(100),
	--@CustomerAcceptedDamagedGoods varchar(100),
	--@DeliveryConfirmed varchar(100),
	--@DeliveryStatus varchar(100),
	--@CardDelivery varchar(100),
	--@FailedReason varchar(100),
	--@DeliveryNote varchar(100),
	--@NoScanPickup varchar(100),
	--@NoScanDelivery varchar(100),
	--@MultiConsignmentDeliveryId varchar(100),
	@IsProcessed bit

	---- Parameters for the tblMyDelMan_PhotoPOD
	--@PhotoPodDeliveryId varchar(100),
	--@PhotoPodDeliveredBy varchar(100),
	--@PhotoPodRowNumber	varchar(100),
	--@PhotoPOD  varchar(Max),
	--@PhotoPodIsProcessed bit,

	---- Parameters for the tblMyDelManJSONResponse
	


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	BEGIN TRANSACTION MyDelMan
	BEGIN TRY
	-- Insert Data into tblMyDelMan_API

    INSERT INTO [dbo].[tblMyDelMan_Consignmnets_API]
           ([ReqID]
		   ,[DateOfRequest]
           ,[ConsignmentNumber]
           ,[ConStatus]
		   ,[JSONResponse]
           ,[IsProcessed])
     VALUES

			  ( @ReqID,
			    @DateOfRequest,
				@ConsignmentNumber,
				@ConStatus,
				@JSONResponse,
				@IsProcessed )

	COMMIT TRANSACTION MyDelMan
		PRINT 'tblMyDelMan_Consignment- Data Inserted successfully' 
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION MyDelMan
		PRINT 'MyDelMan Consignment TRANSACTION IS ROLLBACK' 
	END CATCH
	-- Insert Data into tblMyDelMan_PhotoPOD

	--INSERT INTO [dbo].[tblMyDelMan_PhotoPOD]
 --          ([ReqID]
 --          ,[DeliveryId]
 --          ,[DeliveredBy]
 --          ,[RowNumber]
 --          ,[PhotoPOD]
 --          ,[IsProcessed])
 --    VALUES
	--		(@ReqID,
	--		 @PhotoPodDeliveryId,
	--		 @PhotoPodDeliveredBy,
	--		 @PhotoPodRowNumber,
	--		 @PhotoPOD,
	--		 @PhotoPodIsProcessed )

		
END	
	

GO
