SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_LoadMyDelManDeliveriesData] 

	-- Parameters for the tblMyDelMan_Deliveries
    @ReqID varchar(250),
	@DateOfRequest varchar(250),
	@ConsignmentNumber varchar(100),
	@DeliveryId varchar(100),
	@DeliveredBy varchar(100),
	@CollectedDateSql varchar(100),
	@CollectedDateString varchar(100),
	@DeliveredDateSql varchar(100),
	@DeliveredDateString varchar(100),
	@ReturnedDateSql varchar(100),
	@ReturnedDateString varchar(100),
	@DeliveredLat varchar(100),
	@DeliveredLon varchar(100),
	@POD varchar(MAX),
	@SignedBy varchar(100),
	@CustomerAcceptedDamagedGoods varchar(100),
	@DeliveryConfirmed varchar(100),
	@DeliveryStatus varchar(100),
	@CardDelivery varchar(100),
	@FailedReason varchar(100),
	@DeliveryNote varchar(100),
	@NoScanPickup varchar(100),
	@NoScanDelivery varchar(100),
	@MultiConsignmentDeliveryId varchar(100),
	@IsProcessed bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	BEGIN TRANSACTION MyDelMan_Deliveries
	BEGIN TRY
	-- Insert Data into tblMyDelMan_API

    INSERT INTO [dbo].[tblMyDelMan_Deliveries]
           ([ReqID]
		   ,[DateOfRequest]
		   ,[ConsignmentNumber]
           ,[DeliveryId]
           ,[DeliveredBy]
           ,[CollectedDateSql]
           ,[CollectedDateString]
           ,[DeliveredDateSql]
           ,[DeliveredDateString]
           ,[ReturnedDateSql]
           ,[ReturnedDateString]
           ,[DeliveredLat]
           ,[DeliveredLon]
           ,[POD]
           ,[SignedBy]
           ,[CustomerAcceptedDamagedGoods]
           ,[DeliveryConfirmed]
           ,[DeliveryStatus]
           ,[CardDelivery]
           ,[FailedReason]
           ,[DeliveryNote]
           ,[NoScanPickup]
           ,[NoScanDelivery]
           ,[MultiConsignmentDeliveryId]
           ,[IsProcessed])
     VALUES

			  ( @ReqID,
			    @DateOfRequest,
				@ConsignmentNumber,
				@DeliveryId,
				@DeliveredBy,
				@CollectedDateSql,
				@CollectedDateString,
				@DeliveredDateSql,
				@DeliveredDateString,
				@ReturnedDateSql,
				@ReturnedDateString,
				@DeliveredLat,
				@DeliveredLon,
				@POD,
				@SignedBy,
				@CustomerAcceptedDamagedGoods,
				@DeliveryConfirmed,
				@DeliveryStatus,
				@CardDelivery,
				@FailedReason,
				@DeliveryNote,
				@NoScanPickup,
				@NoScanDelivery,
				@MultiConsignmentDeliveryId,
				@IsProcessed )

	COMMIT TRANSACTION MyDelMan_Deliveries
		PRINT 'tblMyDelMan_Deliveries- Data Inserted successfully' 
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION MyDelMan_Deliveries
		PRINT 'MyDelMan TRANSACTION IS ROLLBACK' 
	END CATCH
	-- Insert Data into tblMyDelMan_PhotoPOD

	--INSERT INTO [dbo].[tblMyDelMan_PhotoPOD]
 --          ([ReqID]
 --          ,[DeliveryId]
 --          ,[DeliveredBy]
 --          ,[RowNumber]
 --          ,[PhotoPOD]
 --          ,[IsProcessed])
 --    VALUES
	--		(@ReqID,
	--		 @PhotoPodDeliveryId,
	--		 @PhotoPodDeliveredBy,
	--		 @PhotoPodRowNumber,
	--		 @PhotoPOD,
	--		 @PhotoPodIsProcessed )

		
END	
	


GO
