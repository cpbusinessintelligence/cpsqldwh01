SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_LoadMyDelManItemData] 

	-- Parameters for the tblMyDelMan_API
	@ReqID varchar(250),
	@DateOfRequest varchar(250),
	@ConsignmentNumber varchar(100),
	@LabelNumber varchar(100),
	@ReceivedAtDepotDateSql varchar(100),
	@LabelStatus varchar(250),
	@Rejected varchar(100),
	@RejectReason varchar(200),
	@RejectNote varchar(200),
	@IconName varchar(100),
	@DeliveryId varchar(100)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	BEGIN TRANSACTION MyDelManItem
	BEGIN TRY
	-- Insert Data into tblMyDelMan_API

    --INSERT INTO [dbo].[tblMyDelMan_API]
    --       ([ReqID]
    --       ,[ConsignmentNumber]
    --       ,[ConStatus]
    --       ,[LabelNumber]
    --       ,[ReceivedAtDepotDateSql]
    --       ,[LabelStatus]
    --       ,[Rejected]
    --       ,[RejectReason]
    --       ,[RejectNote]
    --       ,[IconName]
    --       ,[DeliveryId]
    --       ,[DeliveredBy]
    --       ,[CollectedDateSql]
    --       ,[CollectedDateString]
    --       ,[DeliveredDateSql]
    --       ,[DeliveredDateString]
    --       ,[ReturnedDateSql]
    --       ,[ReturnedDateString]
    --       ,[DeliveredLat]
    --       ,[DeliveredLon]
    --       ,[POD]
    --       ,[SignedBy]
    --       ,[CustomerAcceptedDamagedGoods]
    --       ,[DeliveryConfirmed]
    --       ,[DeliveryStatus]
    --       ,[CardDelivery]
    --       ,[FailedReason]
    --       ,[DeliveryNote]
    --       ,[NoScanPickup]
    --       ,[NoScanDelivery]
    --       ,[MultiConsignmentDeliveryId]
    --       ,[IsProcessed])
    -- VALUES

			 -- ( @ReqID,
				--@ConsignmentNumber,
				--@ConStatus,
				--@LabelNumber,
				--@ReceivedAtDepotDateSql,
				--@LabelStatus,
				--@Rejected,
				--@RejectReason,
				--@RejectNote,
				--@IconName,
				--@DeliveryId,
				--@DeliveredBy,
				--@CollectedDateSql,
				--@CollectedDateString,
				--@DeliveredDateSql,
				--@DeliveredDateString,
				--@ReturnedDateSql,
				--@ReturnedDateString,
				--@DeliveredLat,
				--@DeliveredLon,
				--@POD,
				--@SignedBy,
				--@CustomerAcceptedDamagedGoods,
				--@DeliveryConfirmed,
				--@DeliveryStatus,
				--@CardDelivery,
				--@FailedReason,
				--@DeliveryNote,
				--@NoScanPickup,
				--@NoScanDelivery,
				--@MultiConsignmentDeliveryId,
				--@IsProcessed )
	INSERT INTO [dbo].[tblMyDelMan_Item]
           ([ReqID]
		   ,[DateOfRequest]
           ,[ConsignmentNumber]
           ,[LabelNumber]
           ,[ReceivedAtDepotDateSql]
           ,[LabelStatus]
           ,[Rejected]
           ,[RejectReason]
           ,[RejectNote]
           ,[IconName]
           ,[DeliveryId])
     VALUES
           (@ReqID,
		        @DateOfRequest,
				@ConsignmentNumber,
				@LabelNumber,
				@ReceivedAtDepotDateSql,
				@LabelStatus,
				@Rejected,
				@RejectReason,
				@RejectNote,
				@IconName,
				@DeliveryId)
	COMMIT TRANSACTION MyDelManItem
		PRINT 'tblMyDelMan_Item- Data Inserted successfully' 
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION MyDelManItem
		PRINT 'MyDelMan TRANSACTION IS ROLLBACK' 
	END CATCH
	-- Insert Data into tblMyDelMan_PhotoPOD

	--INSERT INTO [dbo].[tblMyDelMan_PhotoPOD]
 --          ([ReqID]
 --          ,[DeliveryId]
 --          ,[DeliveredBy]
 --          ,[RowNumber]
 --          ,[PhotoPOD]
 --          ,[IsProcessed])
 --    VALUES
	--		(@ReqID,
	--		 @PhotoPodDeliveryId,
	--		 @PhotoPodDeliveredBy,
	--		 @PhotoPodRowNumber,
	--		 @PhotoPOD,
	--		 @PhotoPodIsProcessed )

		
END	
	


GO
