SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_LoadMyDelManPhotoPodData] 


	-- Parameters for the tblMyDelMan_PhotoPOD
	@DateOfRequest varchar(100),
	@ReqID varchar(100),
	@ConsignmentNumber varchar(100),
	@PhotoPodDeliveryId varchar(100),
	@PhotoPodDeliveredBy varchar(100),
	@PhotoPodRowNumber	varchar(100),
	@PhotoPOD  varchar(Max),
	@PhotoPodIsProcessed bit


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	BEGIN TRANSACTION MyDelMan_PhotoPOD
	BEGIN TRY
	-- Insert Data into tblMyDelMan_PhotoPOD

INSERT INTO [dbo].[tblMyDelMan_PhotoPOD]
           ([DateOfRequest]
           ,[ReqID]
		   ,[ConsignmentNumber]
           ,[DeliveryId]
           ,[DeliveredBy]
           ,[RowNumber]
           ,[PhotoPOD]
           ,[IsProcessed]
)
     VALUES

			  (
			    @DateOfRequest,
				@ReqID,
				@ConsignmentNumber,
				@PhotoPodDeliveryId,
				@PhotoPodDeliveredBy,
				@PhotoPodRowNumber,
				@PhotoPOD,
				@PhotoPodIsProcessed)

	COMMIT TRANSACTION MyDelMan_PhotoPOD
		PRINT 'tblMyDelMan_PhotoPOD- Data Inserted successfully' 
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION MyDelMan_PhotoPOD
		PRINT 'MyDelMan TRANSACTION IS ROLLBACK' 
	END CATCH
	-- Insert Data into tblMyDelMan_PhotoPOD

	--INSERT INTO [dbo].[tblMyDelMan_PhotoPOD]
 --          ([ReqID]
 --          ,[DeliveryId]
 --          ,[DeliveredBy]
 --          ,[RowNumber]
 --          ,[PhotoPOD]
 --          ,[IsProcessed])
 --    VALUES
	--		(@ReqID,
	--		 @PhotoPodDeliveryId,
	--		 @PhotoPodDeliveredBy,
	--		 @PhotoPodRowNumber,
	--		 @PhotoPOD,
	--		 @PhotoPodIsProcessed )

		
END	
	



GO
