SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_LoadTrackingEvents] --'2014-10-01'
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadTrackingEvents]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================

   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)

   SELECT Rtrim(Ltrim(L.LabelNumber)) as LabelNumber
         ,[EventDateTime]
		 ,E.Description
		 ,D.ProntoDriverCode
		 ,B.CosmosBranch
		 ,D.Code
		 ,[ExceptionReason]
		 ,LTRIM(RTRIM(REPLACE([AdditionalText1], 'Link Coupon ', ''))) AS AdditionalText  
		 ,AdditionalText2
		 ,[NumberOfItems]
		 ,[SourceReference]
		 ,[CosmosSignatureId]
  INTO #TempTrackingEvents
  FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id
                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id
												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 
											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1
  WHERE [EventDateTime] >=@StartDate and  [EventDateTime] <=@EndDate 
  CREATE CLUSTERED INDEX #TempTrackingEventsidx ON #TempTrackingEvents(Description,LabelNumber)
  PRINT 'Loaded all Scanning Eventts into TempTable'

 ------------------------
 -------Pickup-----------
 ------------------------
  Update DWH.[dbo].[PrimaryLabels] SET PickupDateTime = T.EventDateTime,
                                       PickupScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 
		  WHere  T.Description = 'Pickup'
PRINT 'Updated DIrect Pickup Scans'
 ------------------------
 -------Handover----------
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET [HandoverDateTime] = T.EventDateTime,
                                        [HandoverScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Handover' 
PRINT 'Updated DIrect Handover Scans'
 ------------------------
 -------InDepot----------
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET InDepotDateTime = T.EventDateTime,
                                       InDepotScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'In Depot' 
PRINT 'Updated DIrect InDepot Scans'

 ------------------------
 -------Transfer----------
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET TransferDateTime = T.EventDateTime,
                                        TransferScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Transfer' 
PRINT 'Updated DIrect Transfer Scans'
 ------------------------
 -------OutForDelivery---
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET OutForDeliverDateTime = T.EventDateTime,OutforDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 
		  WHere  T.Description = 'Out For Delivery'
PRINT 'Updated DIrect Out For Delivery Scans'
 ----------------------------
 -------Attempted Delivery---
 ----------------------------
   Update DWH.[dbo].[PrimaryLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,
                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Attempted Delivery'
PRINT 'Updated DIrect Attempted Delivery Scans'
 ------------------------
 -------Delivered----------
 ------------------------
  Update DWH.[dbo].[PrimaryLabels] SET [DeliveryDateTime] = T.EventDateTime,
                                       DeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),
									   [IsPODPresent] =  CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0) THEN 'Y' ELSE 'N' END,
									   [PODDateTime] = CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0) THEN T.EventDateTime ELSE Null END
          From  #TempTrackingEvents T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Delivered' 
PRINT 'Updated DIrect Delivered Scans'			

---------------------------
----Secondary Labels-------
----------------------------

  Select T.* ,
         P.Category  as SecondaryCouponCategory ,
		 Convert(Varchar(20),'') as PrimaryCouponCategory
     Into #TempLinkCoupons
     from #TempTrackingEvents T Join [DWH].dbo.SecondaryLabels S on T.LabelNumber =S.SecondaryLabelNumber
                                Join [DWH].dbo.DimProduct P on Left(LabelNumber,3) = P.Code and P.RevenueType = 'Prepaid'
     where t.Description = 'Link Scan' 

   Update #TempLinkCoupons SET PrimaryCouponCategory =P.RevenueCategory  
                           From #TempLinkCoupons T Join   [DWH].dbo.DimProduct P on Left(AdditionalText,3) = P.Code and P.RevenueType = 'Prepaid'


   Update [DWH].dbo.SecondaryLabels SET PrimaryLabelNumber =T.AdditionalText,
                                        ScanDateTime = T.EventDateTime,
										[ScannedBy] = dbo.fn_CreateUniqueDriverID(T.CosmosBranch,T.Code,T.ProntoDriverCode)
                                 From #TempLinkCoupons T Join [DWH].dbo.SecondaryLabels S on T.LabelNumber = S.SecondaryLabelNumber
                                 Where T.PrimaryCouponCategory ='PRIMARY' 
   
     Update [DWH].dbo.SecondaryLabels SET PrimaryLabelNumber =T.AdditionalText,
	                                      ScanDateTime = T.EventDateTime,
										  [ScannedBy] = dbo.fn_CreateUniqueDriverID(T.CosmosBranch,T.Code,T.ProntoDriverCode)
                                 From #TempLinkCoupons T Join [DWH].dbo.SecondaryLabels S on T.LabelNumber = S.SecondaryLabelNumber
                                 Where T.PrimaryCouponCategory ='' and Isnull(S.PrimaryLabelNumber,'') = ''
PRINT 'Updated all Secondary Scans'	

 Drop Table #TempTrackingEvents
-------------------------------------
-- Fill Tracking Events based on Secondary Labels
------------------------------------------
SELECT [SecondaryLabelNumber]
      ,[PrimaryLabelNumber]
  into #TempSecondaryLabels
  FROM [DWH].[dbo].[SecondaryLabels]
  Where PrimaryLabelNumber <>'' and [LabelCreatedDateTime]>= Dateadd(Day,-7,@Date) and LabelCreatedDateTime <= @Date

 PRINT 'Loaded  all Secondary Scans which is linked to Primary fro the last 7 days'	

      SELECT Rtrim(Ltrim(L.LabelNumber)) as SecondaryLabelNumber
	     ,SL.PrimaryLabelNumber as LabelNumber
         ,[EventDateTime]
		 ,E.Description
		 ,D.ProntoDriverCode
		 ,B.CosmosBranch
		 ,D.Code
		 ,[ExceptionReason]
		 ,LTRIM(RTRIM(REPLACE([AdditionalText1], 'Link Coupon ', ''))) AS AdditionalText  
		 ,AdditionalText2
		 ,[NumberOfItems]
		 ,[SourceReference]
		 ,[CosmosSignatureId]
  INTO #TempTrackingEvents2
  FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id
                                                Join #TempSecondaryLabels SL on L.LabelNumber =SL.SecondaryLabelNumber
                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id
												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 
											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1
										Where E.Description <> 'Link Scan'

  CREATE CLUSTERED INDEX #TempTrackingEvents2idx ON #TempTrackingEvents2(Description,LabelNumber)

   PRINT 'Loaded  all Scanning Events fro the secondary labels'	

 ------------------------
 -------Pickup-----------
 ------------------------
  Update DWH.[dbo].[PrimaryLabels] SET PickupDateTime = T.EventDateTime,
                                       PickupScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents2 T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 
		  WHere  T.Description = 'Pickup' and PickupDateTime is null
PRINT 'Updated Secondary Pickup Scans'
 ------------------------
 -------Handover----------
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET [HandoverDateTime] = T.EventDateTime,
                                        [HandoverScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents2 T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Handover' and HandoverDateTime is null
PRINT 'Updated Secondary Handover Scans'
 ------------------------
 -------InDepot----------
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET InDepotDateTime = T.EventDateTime,
                                       InDepotScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents2 T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'In Depot'  and InDepotDateTime is null
PRINT 'Updated Secondary InDepot Scans'

 ------------------------
 -------Transfer----------
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET TransferDateTime = T.EventDateTime,
                                        TransferScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents2 T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Transfer' and  TransferDateTime is null
PRINT 'Updated Secondary Transfer Scans'
 ------------------------
 -------OutForDelivery---
 ------------------------
   Update DWH.[dbo].[PrimaryLabels] SET OutForDeliverDateTime = T.EventDateTime,OutforDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents2 T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 
		  WHere  T.Description = 'Out For Delivery' and OutForDeliverDateTime is null
PRINT 'Updated Secondary OutForDelivery Scans'
 ----------------------------
 -------Attempted Delivery---
 ----------------------------
   Update DWH.[dbo].[PrimaryLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,
                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)
          From  #TempTrackingEvents2 T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Attempted Delivery' and AttemptedDeliveryDateTime is null
PRINT 'Updated Secondary Attempted Delivery Scans'
 ------------------------
 -------Delivered----------
 ------------------------
  Update DWH.[dbo].[PrimaryLabels] SET [DeliveryDateTime] = T.EventDateTime,
                                       DeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),
									   [IsPODPresent] =  CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0) THEN 'Y' ELSE 'N' END,
									   [PODDateTime] = CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0) THEN T.EventDateTime ELSE Null END
          From  #TempTrackingEvents2 T join DWH.[dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber
		  WHere  T.Description = 'Delivered'  and [DeliveryDateTime] is null

PRINT 'Updated Secondary Delivered Scans'

	
end

GO
