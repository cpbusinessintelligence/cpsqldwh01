SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_ReloadTablesandCalculateETA]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_ReloadTablesandCalculateETA]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================

   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)


   Delete
   FROM [DWH].[dbo].[PrimaryLabels] WHere LabelCreatedDateTime >= @StartDate and LabelCreatedDateTime <= @EndDate

   Update [DWH].[dbo].[PrimaryLabels] SET [OnTimeStatus] = Null ,
                                          PerformanceProcessed = 0 , 
										  PerformanceReportingDate = Null ,
										  ETADate = Null
						Where PerformanceReportingDate = @Date

  Delete [DWH].[dbo].[DailyPerformanceReporting]
  FROM [DWH].[dbo].[DailyPerformanceReporting] P Join DWH.dbo.DimCalendarDate C on  P.PerformanceDayID = C.Id 
  Where C.Date = @Date

  Delete [DWH].[dbo].[DailyNoTimeReporting]
  FROM [DWH].[dbo].[DailyNoTimeReporting] P Join DWH.dbo.DimCalendarDate C on  P.NoTimeReportingDayID = C.Id 
  Where C.Date = @Date

  --  Delete 
  --FROM [DWH].[dbo].[DailyNoTimeReportingDetail]  where PerformanceReportingDate = @Date
  

 end
GO
