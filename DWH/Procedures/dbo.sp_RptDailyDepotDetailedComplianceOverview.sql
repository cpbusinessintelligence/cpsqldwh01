SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDailyDepotDetailedComplianceOverview] 
(@Depot Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyDepotDetailedComplianceOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================




    SELECT  'Achieved' as ComplianceStatus
		  ,C.Date
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C     (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = @Date and [OnTimeStatus] not in ('A','AE','NE','X','E','CY','CN') and D.DepotName = @Depot
  Group by    C.Date
  Union all
  SELECT   'Not Achieved' as ComplianceStatus       
	      ,C.Date
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  		  ,Convert(decimal(12,2),0) as TargetKPI
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = @Date and [OnTimeStatus] not in ('A','AE','NE','X','E','CY','CN') and D.DepotName = @Depot
  Group by  C.Date

  Update #Temp1 SET OnBoardPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]))/Convert(decimal(12,2),[PerformanceCount])
                Where [PerformanceCount]>0
	
  Update #Temp1 SET DeliveryPercentage = Convert(decimal(12,2),100*([DeliveryComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0
  
  Update #Temp1 SET PODPercentage = Convert(decimal(12,2),100*([PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0


  
  
  Update #Temp1 SET TotalPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 
                Where [PerformanceCount]>0


  Select * from #Temp1  

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyDepotDetailedComplianceOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyDepotDetailedComplianceOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyDepotDetailedComplianceOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
