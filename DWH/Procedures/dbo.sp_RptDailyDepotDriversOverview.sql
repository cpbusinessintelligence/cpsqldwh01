SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDailyDepotDriversOverview]
(@Depot Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[[sp_RptDailyDepotDriversOverview]]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================


    SELECT  'Achieved' as ComplianceStatus
		  ,D.DriverID
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  --,Convert(decimal(12,2),0) as OnBoardPercentage
		  --,Convert(decimal(12,2),0) as DeliveryPercentage
		  --,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
  INTO #TempComp
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C     (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = @Date and [OnTimeStatus] not in ('A','AE','NE','X','E','CY','CN') and D.DepotName = @Depot
  Group by    D.DriverID
  Union all
  SELECT   'Not Achieved' as ComplianceStatus       
	      ,D.DriverID
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  --,Convert(decimal(12,2),0) as OnBoardPercentage
		  --,Convert(decimal(12,2),0) as DeliveryPercentage
		  --,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  		  ,Convert(decimal(12,2),0) as TargetKPI
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = @Date and [OnTimeStatus] not in ('A','AE','NE','X','E','CY','CN') and D.DepotName = @Depot
  Group by D.DriverID

  --Update #TempComp SET OnBoardPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]))/Convert(decimal(12,2),[PerformanceCount])
  --              Where [PerformanceCount]>0
	
  --Update #TempComp SET DeliveryPercentage = Convert(decimal(12,2),100*([DeliveryComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
  --              Where [PerformanceCount]>0
  
  --Update #TempComp SET PODPercentage = Convert(decimal(12,2),100*([PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
  --              Where [PerformanceCount]>0


  
  
  Update #TempComp SET TotalPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 
                Where [PerformanceCount]>0






   SELECT Isnull(DeliveryDriver,'Unknown') as DeliveryDriver
       ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
	    ,Convert(Varchar(20),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = @Date  and [OnTimeStatus] Not in ('A','AE','NE','X','E','CY','CN')and D.DepotName =@Depot
  Group by Isnull(DeliveryDriver,'Unknown')
          ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END
  Update #Temp1 SET [Descr] =S.description From #Temp1 T join [dbo].[StatusCodes] S on T.OnTimeStatus=S.code
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DeliveryDriver = #Temp1.DeliveryDriver)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  


--  Select top 20 * INto #TempFinal from #Temp1 WHere OnTimeStatus = 'Y' and PerformanceCOunt>20 and DeliveryDriver not like 'PER%' Order by PerformanceKPI asc


 
  Select Distinct DeliveryDriver,Convert(int,0) as OnTimeCount,Convert(int,0) as NotOnTimeCount,Convert(int,0) as TotalCount,Convert(decimal(12,2),0) as PerformanceKPI,Convert(decimal(12,2),0) as ComplianceKPI into #TempFinal From #Temp1
  Update #TempFinal SEt OnTimeCount =T1.PerformanceCount,PerformanceKPI=T1.PerformanceKPI From #TempFinal TF Join #TEMP1 t1 ON TF.DeliveryDriver = T1.DeliveryDriver Where T1.OnTimeStatus = 'Y'
  Update #TempFinal SEt NotOnTimeCount =T1.PerformanceCount From #TempFinal TF Join #TEMP1 t1 ON TF.DeliveryDriver = T1.DeliveryDriver Where T1.OnTimeStatus = 'N'
  Update #TempFinal SET TotalCount = OnTimeCount+NotOnTimeCount
   Update #TempFinal SEt ComplianceKPI =Tc.TotalPercentage From #TempFinal TF Join #TempComp tC ON TF.DeliveryDriver = TC.DriverID Where TC.ComplianceStatus = 'Achieved'


  Select * from #TempFinal where totalcount>10 order by PerformanceKPI asc

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyDepotDriversOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyDepotDriversOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyDepotDriversOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
