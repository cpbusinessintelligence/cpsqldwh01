SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_RptDailyDepotOnTimePerformanceBasedOnScans]
(@Date Date , @State varchar(20))
 as
begin

     SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyDepotPerformanceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
   SELECT Isnull(State,'Unknown') as State,Isnull(Branch,'Unknown') as Branch,Isnull(DepotName,'Unknown') as DepotName
       ,[OnTimeStatus]
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  PerformanceReporting.[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = @Date  and [OnTimeStatus] in (1,2) and state = @State
  Group by Isnull(State,'Unknown')
          ,Isnull(Branch,'Unknown')
		  ,Isnull(DepotName,'Unknown')
		  ,[OnTimeStatus]
  Update #Temp1 SET [Descr] =S.StatusDescription From #Temp1 T join PerformanceReporting.[dbo].DimStatus S on T.OnTimeStatus=S.id
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DepotName = #Temp1.DepotName)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0


  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [DWH].[dbo].[TargetKPI] K on T.[State] = K.[State] 
     Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 Where DepotName not in ('Unknown')
  order by PerformanceKPI desc

end
GO
