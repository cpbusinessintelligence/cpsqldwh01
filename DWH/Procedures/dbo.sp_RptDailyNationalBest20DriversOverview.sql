SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDailyNationalBest20DriversOverview]
(@Date Date )
 as
begin
     SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNoTimeOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
    SELECT Isnull(DeliveryDriver,'Unknown') as DeliveryDriver
       ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
	    ,Convert(Varchar(20),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = @Date  and [OnTimeStatus] Not in ('A','AE','NE','X','E','CY','CN')
  Group by Isnull(DeliveryDriver,'Unknown'),CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END
  Update #Temp1 SET [Descr] =S.description From #Temp1 T join [dbo].[StatusCodes] S on T.OnTimeStatus=S.code
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DeliveryDriver = #Temp1.DeliveryDriver)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

  Select top 20 * from #Temp1   order by PerformanceKPI Desc,[PerformanceCount] desc
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyNationalBest20DriversOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyNationalBest20DriversOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyNationalBest20DriversOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
