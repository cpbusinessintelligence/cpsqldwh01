SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDailyNationalWorst20NoTimeDrivers]
(@Date Date )
 as
begin

     SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNoTimeOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  SELECT Isnull(D.DriverID,'Unknown') as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyNoTimeReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere C.Date = @Date  
  GROUP By  Isnull(D.DriverID,'Unknown')
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc

 

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyNationalWorst20NoTimeDrivers]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyNationalWorst20NoTimeDrivers]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyNationalWorst20NoTimeDrivers]
	TO [couriersplease\Harley.BoydSkinner]
GO
