SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDisplayAllDepots]

 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyDepotPerformanceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  
Select distinct DepotName ,State from [PerformanceReporting].[dbo].[DimContractor] Order by 1
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDisplayAllDepots]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDisplayAllDepots]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDisplayAllDepots]
	TO [couriersplease\Harley.BoydSkinner]
GO
