SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

 CREATE Procedure sp_RptDriversVsETAZone(@State varchar(50), @Runnumber int) as
 begin
  

       --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_DriversVsETAZone]
    --' ---------------------------
    --' Purpose: Drivers Vs ETAZone-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 04 Mar 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 04/03/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

select distinct Branch,
                  DriverNumber,
                  ProntoId,
				  [DWH].[dbo].[fn_CreateUniqueDriverID] (Branch,DriverNumber,ProntoId) as DriverID
                  into #temp
from cosmos.dbo.driver where isactive=1

If @Runnumber<>''
SELECT c.[DriverID]
      ,c.[DriverNumber]
      ,c.[ProntoID]
      ,[DepotCode]
      ,[DepotName]
      ,c.[Branch]
      ,[State]
      ,(case when [ContractorType]='C' then 'Contractor' when [ContractorType]='B' then 'Barcode' when [ContractorType]='S' then 'Second Scanner' when [ContractorType]='O' then 'Other' when [ContractorType]='V' then 'Vacant Run' when [ContractorType]='M' then 'Management Run' else ContractorType end) as Contractortype
      ,[DriverName]
      ,[ETAZone]
      ,[PricingZone]
      ,[ContractorCategory]
  FROM #temp t join [DWH].[dbo].[DimContractor] c on c.DriverID=t.DriverID where state=@State and c.drivernumber=@Runnumber
else 
SELECT c.[DriverID]
      ,c.[DriverNumber]
      ,c.[ProntoID]
      ,[DepotCode]
      ,[DepotName]
      ,c.[Branch]
      ,[State]
      ,(case when [ContractorType]='C' then 'Contractor' when [ContractorType]='B' then 'Barcode' when [ContractorType]='S' then 'Second Scanner' when [ContractorType]='O' then 'Other' when [ContractorType]='V' then 'Vacant Run' when [ContractorType]='M' then 'Management Run' else ContractorType end) as Contractortype
      ,[DriverName]
      ,[ETAZone]
      ,[PricingZone]
      ,[ContractorCategory]
  FROM #temp t join [DWH].[dbo].[DimContractor] c on c.DriverID=t.DriverID where state=@State
  end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriversVsETAZone]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriversVsETAZone]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriversVsETAZone]
	TO [couriersplease\Harley.BoydSkinner]
GO
