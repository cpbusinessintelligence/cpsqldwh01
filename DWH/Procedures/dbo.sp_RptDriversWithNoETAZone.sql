SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_RptDriversWithNoETAZone] as
 begin
  

       --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDriversWithNoETAZone]
    --' ---------------------------
    --' Purpose: Drivers Vs ETAZone-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 04 Mar 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 04/03/2015    AK      1.00    Created the procedure                            

    --'=====================================================================


select distinct Branch,
                  DriverNumber,
                  ProntoId,
				  [DWH].[dbo].[fn_CreateUniqueDriverID] (Branch,DriverNumber,ProntoId) as DriverID
                  into #temp
from cosmos.dbo.driver where isactive=1


SELECT c.[DriverID]
      ,c.[DriverNumber]
      ,c.[ProntoID]
      ,c.[DepotCode]
      ,c.[DepotName]
      ,c.[Branch]
      ,c.[State]
      ,c.[ContractorType]
      ,c.[DriverName]
      ,c.[ETAZone]
      ,c.[PricingZone]
      ,c.[ContractorCategory]
  FROM #temp t join [DWH].[dbo].[DimContractor] c on c.DriverID=t.DriverID  where ETAZone='' and c.driverid not like 'NKP%' 


  end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriversWithNoETAZone]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriversWithNoETAZone]
	TO [SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriversWithNoETAZone]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDriversWithNoETAZone]
	TO [couriersplease\Harley.BoydSkinner]
GO
