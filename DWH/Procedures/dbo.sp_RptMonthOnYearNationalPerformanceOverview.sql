SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptMonthOnYearNationalPerformanceOverview]
(@Year Integer )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptMonthOnYearNationalPerformanceOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
 
	CREATE TABLE #TempFinal
	(
		[ShortMonth] varchar(10),
		[MonthName] varchar(20),
		[MonthNumber] int,
		[State] varchar(20),
		[TotalCount] int,
		[TotalCountCompletedScans] int,
		[TotalCountConsignmentData] int,
		[OnTimeCountCompletedScans] int,
		[NotOnTimeCountCompletedScans] int,
		[OnTimeCountConsignmentData] int,
		[NotOnTimeCountConsignmentData] int,
		[TotalOnTimePercentage] decimal(12,2),
		[TotalNotOnTimePercentage] decimal(12,2),
		[TotalOnTimePercentageCompletedScans] decimal(12,2),
		[TotalNotOnTimePercentageCompletedScans] decimal(12,2),
		[TotalOnTimePercentageConsignmentData] decimal(12,2),
		[TotalNotOnTimePercentageConsignmentData] decimal(12,2),
		[ComplianceAchivedPercentage] decimal(12,2),
		[ComplianceNotAchivedPercentage] decimal(12,2),
	)

	Insert into  #TempFinal Select 'JAN','January',1,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'FEB','February',2,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'MAR','March',3,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'APR','April',4,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'MAY','May',5,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'JUN','June',6,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'JUL','July',7,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'AUG','August',8,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'SEP','September',9,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'OCT','October',10,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'NOV','November',11,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	Insert into  #TempFinal Select 'DEC','December',12,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

    SELECT  C.CalendarMonthNumber
	      ,'Achieved' as ComplianceStatus
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		
  INTO #Temp2
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C     (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.CalendarYear = @Year  and [OnTimeStatus] not in ('A','AE','NE','X','E','CY','CN')
  Group by C.CalendarMonthNumber
   Union all
  SELECT   C.CalendarMonthNumber
           ,'Not Achieved' as ComplianceStatus
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
	  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.CalendarYear = @Year  and [OnTimeStatus] not in ('A','AE','NE','X','E','CY','CN')
  Group by C.CalendarMonthNumber
  Update #Temp2 SET TotalPercentage = Convert(decimal(12,2),([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount])*100)/Convert(decimal(12,2),[PerformanceCount]*3)


   SELECT C.CalendarMonthNumber, [OnTimeStatus] 
        ,SUM([PerformanceCount]) as [PerformanceCount]
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id                                                   
  WHere C.CalendarYear = @Year 
    and [OnTimeStatus] not in ('A','AE','NE','X','E')
  Group by C.CalendarMonthNumber, [OnTimeStatus] 

  Update #TempFinal SET [OnTimeCountCompletedScans] =T1.PerformanceCount   From #TempFinal TF Join #Temp1 T1 on TF.MonthNumber=T1.CalendarMonthNumber WHere T1.CalendarMonthNumber=TF.MonthNumber and T1.OnTimeStatus='Y'
  Update #TempFinal SET [NotOnTimeCountCompletedScans] =T1.PerformanceCount   From #TempFinal TF Join #Temp1 T1 on TF.MonthNumber=T1.CalendarMonthNumber WHere T1.CalendarMonthNumber=TF.MonthNumber and T1.OnTimeStatus='N'
  Update #TempFinal SET [OnTimeCountConsignmentData] =T1.PerformanceCount   From #TempFinal TF Join #Temp1 T1 on TF.MonthNumber=T1.CalendarMonthNumber WHere T1.CalendarMonthNumber=TF.MonthNumber and T1.OnTimeStatus='CY'
  Update #TempFinal SET [NotOnTimeCountConsignmentData] =T1.PerformanceCount   From #TempFinal TF Join #Temp1 T1 on TF.MonthNumber=T1.CalendarMonthNumber WHere T1.CalendarMonthNumber=TF.MonthNumber and T1.OnTimeStatus='CN'
  Update #TempFinal SET [ComplianceAchivedPercentage] =T2.TotalPercentage   From #TempFinal TF Join #Temp2 T2 on TF.MonthNumber=T2.CalendarMonthNumber WHere T2.CalendarMonthNumber=TF.MonthNumber and T2.ComplianceStatus= 'Achieved'
  Update #TempFinal SET [ComplianceNotAchivedPercentage] =T2.TotalPercentage   From #TempFinal TF Join #Temp2 T2 on TF.MonthNumber=T2.CalendarMonthNumber WHere T2.CalendarMonthNumber=TF.MonthNumber and T2.ComplianceStatus= 'Not Achieved'
  
  Update #TempFinal SET TotalCount =[OnTimeCountCompletedScans]+[NotOnTimeCountCompletedScans]+ [OnTimeCountConsignmentData]+[NotOnTimeCountConsignmentData],
                       [TotalCountCompletedScans]=[OnTimeCountCompletedScans]+[NotOnTimeCountCompletedScans],
					   [TotalCountConsignmentData]=[OnTimeCountConsignmentData]+[NotOnTimeCountConsignmentData]
  Update #TempFinal SET [TotalOnTimePercentage] = CASE WHEN [TotalCount] >0 THEN Convert(Decimal(12,2),[OnTimeCountCompletedScans]+OnTimeCountConsignmentData)*100.0/([TotalCount]) ELSE 0 END,
					    [TotalNotOnTimePercentage] = CASE WHEN [TotalCount] >0 THEN Convert(Decimal(12,2),[NotOnTimeCountCompletedScans]+NotOnTimeCountConsignmentData)*100.0/([TotalCount]) ELSE 0 END,
					    [TotalOnTimePercentageCompletedScans]= CASE WHEN TotalCountCompletedScans >0 THEN Convert(Decimal(12,2),[OnTimeCountCompletedScans])*100.0/(TotalCountCompletedScans) ELSE 0 END,
					    [TotalNotOnTimePercentageCompletedScans] = CASE WHEN TotalCountCompletedScans >0 THEN Convert(Decimal(12,2),[NotOnTimeCountCompletedScans])*100.0/(TotalCountCompletedScans) ELSE 0 END,
					    [TotalOnTimePercentageConsignmentData]= CASE WHEN TotalCountConsignmentData >0 THEN Convert(Decimal(12,2),[OnTimeCountConsignmentData])*100.0/(TotalCountConsignmentData) ELSE 0 END,
					    [TotalNotOnTimePercentageConsignmentData] = CASE WHEN TotalCountConsignmentData >0 THEN Convert(Decimal(12,2),[NotOnTimeCountConsignmentData])*100.0/(TotalCountConsignmentData) ELSE 0 END
 
  Select * from #Tempfinal


end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptMonthOnYearNationalPerformanceOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptMonthOnYearNationalPerformanceOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptMonthOnYearNationalPerformanceOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
