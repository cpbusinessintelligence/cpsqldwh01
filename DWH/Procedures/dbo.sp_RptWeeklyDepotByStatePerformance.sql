SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--[sp_RptWeeklyDepotByStatePerformance] 'Sydney Zone4','2020-05-11'
CREATE proc [dbo].[sp_RptWeeklyDepotByStatePerformance]
(@Depot Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[[[sp_RptWeeklyDepotByStatePerformance]]]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================

	Declare @WeekendingDate as Date = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
	Declare @State As Varchar(20) = ISnull((SELECT Max(State) FROM [PerformanceReporting].[dbo].[DimContractor] WHere DepotName = @Depot),'')

    SELECT Isnull(State,'Unknown') as State,Isnull(Branch,'Unknown') as Branch,Isnull(DepotName,'Unknown') as DepotName
       ,CASE S.[Statuscode] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE S.[Statuscode] END as [OnTimeStatus]
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
	JOIN [PerformanceReporting].[dbo].[DimStatus] S (NOLOCK) on P.[OnTimeStatus] = S.Id
	LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                                      
  Where C.WeekEndingdate = @WeekendingDate and D.State = @State and S.Statuscode Not in ('A','AE','NE','X','E','CY','CN')
  Group by Isnull(State,'Unknown')
          ,Isnull(Branch,'Unknown')
		  ,Isnull(DepotName,'Unknown')
		  ,CASE S.[Statuscode] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE S.[Statuscode] END
  
  Update #Temp1 SET [Descr] =S.description From #Temp1 T join [PerformanceReporting].[dbo].[DimStatus] S on T.OnTimeStatus = S.Statuscode
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DepotName = #Temp1.DepotName)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
     Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 Where DepotName not in ('Unknown')
  order by PerformanceKPI desc

End
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotByStatePerformance]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotByStatePerformance]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotByStatePerformance]
	TO [couriersplease\Harley.BoydSkinner]
GO
