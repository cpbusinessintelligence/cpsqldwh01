SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


--sp_RptWeeklyDepotDetailedComplianceOverview 'Sydney Zone4','2020-05-11'
CREATE proc [dbo].[sp_RptWeeklyDepotDetailedComplianceOverview] 
(@Depot Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyDepotDetailedComplianceOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================


	Declare @WeekendingDate as Date = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

	SELECT  'Achieved' as ComplianceStatus
		  ,C.WeekEndingdate
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
	JOIN [PerformanceReporting].[dbo].[DimStatus] S (NOLOCK) on P.[OnTimeStatus] = S.Id
	LEFT JOIN [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.WeekEndingdate = @WeekendingDate and S.Statuscode not in ('A','AE','NE','X','E','CY','CN') and D.DepotName = @Depot
  Group by    C.WeekEndingdate
  Union all
  SELECT   'Not Achieved' as ComplianceStatus       
	      ,C.WeekEndingdate
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		,Convert(decimal(12,2),0) as TargetKPI
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
	JOIN [PerformanceReporting].[dbo].[DimStatus] S (NOLOCK) on P.[OnTimeStatus] = S.Id
	LEFT JOIN [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.WeekEndingdate = @WeekendingDate and S.Statuscode not in ('A','AE','NE','X','E','CY','CN') and D.DepotName = @Depot
  Group by  C.WeekEndingdate

  Update #Temp1 SET OnBoardPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]))/Convert(decimal(12,2),[PerformanceCount])
                Where [PerformanceCount]>0
  Update #Temp1 SET DeliveryPercentage = Convert(decimal(12,2),100*([DeliveryComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0
  Update #Temp1 SET PODPercentage = Convert(decimal(12,2),100*([PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0
  Update #Temp1 SET TotalPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 
                Where [PerformanceCount]>0

  Select * from #Temp1  

End
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotDetailedComplianceOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotDetailedComplianceOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotDetailedComplianceOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
