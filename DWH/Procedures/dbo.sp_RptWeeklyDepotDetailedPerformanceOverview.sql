SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


--[sp_RptWeeklyDepotDetailedPerformanceOverview] 'Sydney Zone4','2020-05-11'
CREATE proc [dbo].[sp_RptWeeklyDepotDetailedPerformanceOverview]
(@Depot Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyDepotDetailedPerformanceOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
   Declare @WeekendingDate as Date = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
   SELECT Isnull(State,'Unknown') as State
       ,Isnull(DepotName,'Unknown') as Depot
       ,CASE S.[Statuscode] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE S.[Statuscode] END as [OnTimeStatus]
	    ,Convert(Varchar(50),'') as [Descr]
		,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
	JOIN [PerformanceReporting].[dbo].[DimStatus] S (NOLOCK) on P.[OnTimeStatus] = S.Id
	LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID     
  WHere C.WeekEndingdate = @WeekendingDate  and S.Statuscode Not in ('A','AE','NE','X','E') and D.DepotName =@Depot
  Group by Isnull(State,'Unknown'),Isnull(DepotName,'Unknown'),CASE S.[Statuscode] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [Statuscode] END 

  Update #Temp1 SET [Descr] = S.description From #Temp1 T join [PerformanceReporting].[dbo].[DimStatus] S on T.OnTimeStatus = S.Statuscode
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.Depot = #Temp1.Depot)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 
  Order by PerformanceKPI desc

End
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotDetailedPerformanceOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotDetailedPerformanceOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotDetailedPerformanceOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
