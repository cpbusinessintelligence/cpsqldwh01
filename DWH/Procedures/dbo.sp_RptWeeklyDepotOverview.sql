SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--[sp_RptWeeklyDepotOverview] 'Sydney Zone4','2020-05-11'
CREATE proc [dbo].[sp_RptWeeklyDepotOverview]
(@Depot varchar(30),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWeeklyDepotOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================

	  Declare @WeekendingDate as Date = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

      SELECT [OnTimeStatus]
            ,SUM([PerformanceCount]) as [PerformanceCount]
      INTO #Temp1 
	  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
		JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id     
	    JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                         
	  WHERE  C.WeekEndingdate = @WeekendingDate and D.DepotName= @Depot
	  GROUP BY [OnTimeStatus]

      SELECT StatusCode [Code]
            ,[Description]
            ,Id
			,Convert(Int,0) as Total
	        ,Convert(decimal(12,2),0) as Percentage
      INTO #TempStatusCodes    
      FROM [PerformanceReporting].[dbo].[DimStatus]
 
      Update #TempStatusCodes 
		SET Total  = T.PerformanceCount 
			,Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1 WHere [OnTimeStatus] in (1,2,3,4)),0)) 
      From  #TempStatusCodes S Join #Temp1 T on S.[Id] = T.[OnTimeStatus] 

	  Update #TempStatusCodes SET Percentage = 0 where Code not in ('Y','N','CY','CN')

      Select * From #TempStatusCodes 
	  Order by Id 
End
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
