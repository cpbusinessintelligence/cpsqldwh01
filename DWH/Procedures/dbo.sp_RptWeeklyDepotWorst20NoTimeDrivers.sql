SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


--[sp_RptWeeklyDepotWorst20NoTimeDrivers] 'Sydney Zone4','2020-05-11'
CREATE proc [dbo].[sp_RptWeeklyDepotWorst20NoTimeDrivers]
(@Depot Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyStateWorst20NoTimeDrivers]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  Declare @WeekendingDate as Date = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
  SELECT Isnull(D.DriverID,'Unknown') as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
    LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere C.WeekEndingdate = @WeekendingDate and D.DepotName =  @Depot
  GROUP By  Isnull(D.DriverID,'Unknown')
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc
  
End
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotWorst20NoTimeDrivers]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotWorst20NoTimeDrivers]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyDepotWorst20NoTimeDrivers]
	TO [couriersplease\Harley.BoydSkinner]
GO
