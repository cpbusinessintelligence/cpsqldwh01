SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptWeeklyMainNetworkPerformanceOverview]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWeeklyMainNetworkPerformanceOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  Declare @WeekendingDate as Date = (Select WeekEndingdate from [DWH].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
  SELECT isnull(N.[NetworkCategory],'Unknown') as [NetworkCategory]
	    ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
		,Convert(int,0) as SortOrder
        ,Convert(Varchar(20),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[NetworkCategory] N (NOLOCK) on P.[NetworkCategoryID] = N.NetworkCategoryID
                                                      
  WHere C.WeekEndingdate = @WeekendingDate  and [OnTimeStatus] Not in ('A','AE','NE','X','E','CY','CN')
  Group by isnull(N.[NetworkCategory],'Unknown'),CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END
  Update #Temp1 SET SortOrder = CASE [NetworkCategory] WHEN 'METRO' THEN 1 WHEN 'INTERSTATE' THEN 3 WHEN 'INTRASTATE' THEN 4 WHEN 'REGIONAL' THEN 2 ELSE 5 END 
  Update #Temp1 SET [Descr] =S.description From #Temp1 T join [dbo].[StatusCodes] S on T.OnTimeStatus=S.code
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.NetworkCategory = #Temp1.NetworkCategory)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0



 
  Select * from #Temp1 order by SortOrder desc

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyMainNetworkPerformanceOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyMainNetworkPerformanceOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyMainNetworkPerformanceOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
