SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptWeeklyNationalOverview]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWeeklyNationalOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	  Declare @WeekendingDate as Date = (Select WeekEndingdate from [DWH].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

      SELECT [OnTimeStatus]
            ,SUM([PerformanceCount]) as [PerformanceCount]
      INTO #Temp1 
	  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id                                                    
	  WHere C.WeekEndingdate = @WeekendingDate
	  Group by [OnTimeStatus]

      SELECT [Code]
            ,[Description]
            ,[SortOrder]
			,Convert(Int,0) as Total
	        ,Convert(decimal(12,2),0) as Percentage
      into #TempStatusCodes    
      FROM [DWH].[dbo].[StatusCodes]
 
      Update #TempStatusCodes SET Total  = T.PerformanceCount , 
                                  Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1),0)) 
      From  #TempStatusCodes S Join #Temp1 T on S.[Code] = T.[OnTimeStatus] 

      Select * 
	  from #TempStatusCodes 
	  order by [SortOrder] asc

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyNationalOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyNationalOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyNationalOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
