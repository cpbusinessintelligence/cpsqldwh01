SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--[sp_RptWeeklyNationalPerfandCompByStateOverview] '2020-05-11'
CREATE proc [dbo].[sp_RptWeeklyNationalPerfandCompByStateOverview]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWeeklyNationalPerfandCompByStateOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
   Declare @WeekendingDate as Date = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
   SELECT Isnull(State,'Unknown') as State
       ,CASE S.[Statuscode] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE S.[Statuscode] END as [OnTimeStatus]
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
	JOIN [PerformanceReporting].[dbo].[DimStatus] S (NOLOCK) on P.[OnTimeStatus] = S.Id
	LEFT JOIN [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                                   
  Where C.WeekEndingdate = @WeekendingDate  and S.Statuscode Not in ('A','AE','NE','X','E','CY','CN')
  Group by Isnull(State,'Unknown'),CASE S.Statuscode WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE S.Statuscode END

  Update #Temp1 SET [Descr] =S.description From #Temp1 T join [PerformanceReporting].[dbo].[DimStatus] S on T.OnTimeStatus = S.Statuscode
  Update #Temp1 Set Total = (Select SUM([PerformanceCount]) from #Temp1 T2 Where T2.State = #Temp1.State)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
       Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  SELECT  'Achieved' as ComplianceStatus
          ,Isnull(D.State,'Unknown') as State
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
  INTO #Temp2 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C     (NOLOCK) on  P.[PerformanceDayID] = C.Id
    JOIN [PerformanceReporting].[dbo].[DimStatus] S (NOLOCK) on P.[OnTimeStatus] = S.Id
	LEFT JOIN [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID												                                             
  Where C.WeekEndingdate = @WeekendingDate and S.Statuscode not in ('A','AE','NE','X','E','CY','CN')
  Group by Isnull(D.State,'Unknown') 	
  Union all
  SELECT   'Not Achieved' as ComplianceStatus
		  ,Isnull(D.State,'Unknown')  as State
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
	JOIN [PerformanceReporting].[dbo].[DimStatus] S (NOLOCK) on P.[OnTimeStatus] = S.Id
	LEFT JOIN [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.WeekEndingdate = @WeekendingDate and S.Statuscode not in ('A','AE','NE','X','E','CY','CN')
  Group by Isnull(D.State,'Unknown') 	

  Update #Temp2 SET TotalPercentage = Convert(decimal(12,2),([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount])*100)/Convert(decimal(12,2),[PerformanceCount]*3)


CREATE TABLE #TempFinal
(
    [State] varchar(100),
    TotalCount int,
	OnTimeCount int,
	NotOnTimeCount int,
	OnTimePercentage decimal(12,2),
	TargetOnTimePercentage decimal(12,2),
	TotalCompliancePercentage decimal(12,2),
)

Insert into  #TempFinal Select 'NSW',0,0,0,0,0,0
Insert into  #TempFinal Select 'QLD',0,0,0,0,0,0
Insert into  #TempFinal Select 'VIC',0,0,0,0,0,0
Insert into  #TempFinal Select 'SA',0,0,0,0,0,0

Update #TempFinal SET  OnTimeCount =T.PerformanceCount,TOtalCOunt= T.Total,OnTimePercentage= T.PerformanceKPI,TargetOnTimePercentage = T.TargetKPI From #TempFinal F Join  #Temp1 T on F.State = T.State and T.OnTimeStatus = 'Y'
Update #TempFinal SET  notOnTimeCount =T.PerformanceCount From #TempFinal F Join  #Temp1 T on F.State = T.State and T.OnTimeStatus = 'N'
Update #TempFinal SET  TotalCompliancePercentage =T.TotalPercentage From #TempFinal F Join  #Temp2 T on F.State = T.State and T.ComplianceStatus = 'Achieved'

Select * from #TempFinal
--Open t

End
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyNationalPerfandCompByStateOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyNationalPerfandCompByStateOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyNationalPerfandCompByStateOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
