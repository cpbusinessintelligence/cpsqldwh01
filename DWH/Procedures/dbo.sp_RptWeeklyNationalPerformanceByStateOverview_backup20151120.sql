SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


Create proc [dbo].[sp_RptWeeklyNationalPerformanceByStateOverview_backup20151120]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalPerformanceByStateOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  Declare @WeekendingDate as Date = (Select WeekEndingdate from [DWH].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
  SELECT Isnull(State,'Unknown') as State
       ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
	    ,Convert(Varchar(20),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.WeekEndingdate = @WeekendingDate  and [OnTimeStatus] Not in ('A','AE','NE','X','E','CY','CN')
  Group by Isnull(State,'Unknown'),CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END

  Update #Temp1 SET [Descr] =S.description From #Temp1 T join [dbo].[StatusCodes] S on T.OnTimeStatus=S.code
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.State = #Temp1.State)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0


  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [DWH].[dbo].[TargetKPI] K on T.[State] = K.[State] 
Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1  Where State not in ('Unknown', 'WA')
  order by PerformanceKPI desc
end

GO
