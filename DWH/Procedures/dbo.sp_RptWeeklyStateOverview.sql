SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptWeeklyStateOverview]
(@State varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyStateOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================

	  Declare @WeekendingDate as Date = (Select WeekEndingdate from [DWH].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
      SELECT [OnTimeStatus]
            ,SUM([PerformanceCount]) as [PerformanceCount]
      INTO #Temp1 
	  FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id     
	                                                          JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                         
	  WHere C.WeekEndingdate = @WeekendingDate and D.State= @State
	  Group by [OnTimeStatus]

      SELECT [Code]
            ,[Description]
            ,[SortOrder]
			,Convert(Int,0) as Total
	        ,Convert(decimal(12,2),0) as Percentage
      into #TempStatusCodes    
      FROM [DWH].[dbo].[StatusCodes]
 
      Update #TempStatusCodes SET Total  = T.PerformanceCount , 
                                  Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1 WHere [OnTimeStatus] in ('Y','N','CY','CN')),0)) 
      From  #TempStatusCodes S Join #Temp1 T on S.[Code] = T.[OnTimeStatus] 

	  Update #TempStatusCodes SET Percentage = 0 where Code not in ('Y','N','CY','CN')

      Select * 
	  from #TempStatusCodes 
	  order by [SortOrder] desc

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyStateOverview]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyStateOverview]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyStateOverview]
	TO [couriersplease\Harley.BoydSkinner]
GO
