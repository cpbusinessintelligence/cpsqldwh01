SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptWeeklyStateWorst20NoTimeDrivers]
(@State Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptWeeklyStateWorst20NoTimeDrivers]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
  Declare @WeekendingDate as Date = (Select WeekEndingdate from [DWH].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
  SELECT Isnull(D.DriverID,'Unknown') as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [DWH].[dbo].[DailyNoTimeReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
                                                          LEFT JOIN  [DWH].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere C.WeekEndingdate = @WeekendingDate and D.State =  @State
  GROUP By  Isnull(D.DriverID,'Unknown')
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc

 

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyStateWorst20NoTimeDrivers]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyStateWorst20NoTimeDrivers]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWeeklyStateWorst20NoTimeDrivers]
	TO [couriersplease\Harley.BoydSkinner]
GO
