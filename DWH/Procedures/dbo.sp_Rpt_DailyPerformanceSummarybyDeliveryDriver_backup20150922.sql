SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[sp_Rpt_DailyPerformanceSummarybyDeliveryDriver_backup20150922](@Depot varchar(100),@Date date) as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_OnTimePerformanceSummary]
    --' ---------------------------
    --' Purpose: OnTime Performance summary-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 29 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/12/2014    AK      1.00    Created the procedure                            

    --'=====================================================================

SELECT DeliveryDriver
      ,C.Date as Date
	  ,case left(ltrim(rtrim(DeliveryDriver)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as Branch
	  ,D.DepotCode
	  ,D.DepotName
	  ,substring(ltrim(rtrim(DeliveryDriver)),4,4) as Runno
	  ,right(ltrim(rtrim(DeliveryDriver)),4) as ProntoID
	  ,[PerformanceCount]
      ,case when [OnTimeStatus] in ('Y') then PerformanceCount else 0 end as OnTimeCount
	  ,case when [OnTimeStatus] in ('N') then PerformanceCount else 0 end as NotOnTimeCount
	  ,[OnBoardComplainceCount]
      ,[DeliveryComplainceCount]
	  ,PODComplainceCount
	  ,convert(decimal(12,2),0) as ComplaincePercentage
	  ,convert(int,0) as NoTimeCount
into #temp
FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID  
													   where c.date =@Date and 	D.DepotName=@Depot	and OnTimeStatus not in ('A','AE','NE','X','E','CY','CN')			   

select c.date as Date,
       [OnBoardDriver],
	   D.DepotName,
	   sum(NotimeCount) as NoTimeCount 
into #temp1 
from [DWH].[dbo].[DailyNoTimeReporting] p JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id  Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[OnBoardDriver]=D.DriverID  
where  c.date =@Date and 	D.DepotName=@Depot		 
group by c.date,[OnBoardDriver], D.DepotName


													   
select DeliveryDriver as Driver
      ,Date
	  ,Branch
	  ,DepotName
	  ,Runno
	  ,ProntoID
	  ,sum([PerformanceCount]) as PerformanceCount
      ,sum(OnTimeCount) as OnTimeCount
	  ,sum(NotOnTimeCount) as NotOnTimeCount
	  ,sum([OnBoardComplainceCount]) as OnBoardComplainceCount
      ,sum([DeliveryComplainceCount]) as DeliveryComplainceCount
	  ,sum(PODComplainceCount) as PODComplainceCount
	  ,ComplaincePercentage
	  ,NoTimeCount
	  into #temp2 from #temp
	  group by DeliveryDriver
              ,Date
	          ,Branch
			  ,DepotName
	          ,Runno
	          ,ProntoID
			  ,NoTimeCount
			  ,ComplaincePercentage
			  

update #temp2 set NoTimeCount=t1.NotimeCount from #temp2 t join #temp1 t1 on t1.[OnBoardDriver]=t.Driver
update #temp2 set ComplaincePercentage=Convert(decimal(12,2),100*([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 

select * from #temp2 	

end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DailyPerformanceSummarybyDeliveryDriver_backup20150922]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DailyPerformanceSummarybyDeliveryDriver_backup20150922]
	TO [COURIERSPLEASE\lynn.wong]
GO
