SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_Rpt_DailyPerformanceSummarybyPickupDriver](@Depot varchar(100),@Date date) as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_DailyPerformanceSummarybyPickupDriver]
    --' ---------------------------
    --' Purpose: OnTime Performance summary-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 16 Feb 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 16/02/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

SELECT PickupDriver
      ,C.Date as Date
	  ,case left(ltrim(rtrim(PickupDriver)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as Branch
	  ,D.DepotCode
	  ,D.DepotName
	  ,substring(ltrim(rtrim(PickupDriver)),4,4) as Runno
	  ,right(ltrim(rtrim(PickupDriver)),4) as ProntoID
	  ,[PerformanceCount]
      ,case when [OnTimeStatus] in ('Y') then PerformanceCount else 0 end as OnTimeCount
	  ,case when [OnTimeStatus] in ('N') then PerformanceCount else 0 end as NotOnTimeCount
	  ,[OnBoardComplainceCount]
      ,[DeliveryComplainceCount]
	  ,PODComplainceCount
	  ,convert(decimal(12,2),0) as CompliancePercentage
into #temp
FROM [DWH].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[PickupDriver]=D.DriverID  
													   where c.date=@Date and 	D.DepotName=@Depot	and OnTimeStatus not in ('A','AE','NE','X','E','CY','CN')			   




													   
select PickupDriver as Driver
      ,Date
	  ,Branch
	  ,DepotName
	  ,Runno
	  ,ProntoID
	  ,sum([PerformanceCount]) as PerformanceCount
      ,sum(OnTimeCount) as OnTimeCount
	  ,sum(NotOnTimeCount) as NotOnTimeCount
	  ,sum([OnBoardComplainceCount]) as OnBoardComplianceCount
      ,sum([DeliveryComplainceCount]) as DeliveryComplianceCount
	  ,sum(PODComplainceCount) as PODComplianceCount
	  ,CompliancePercentage
	  into #temp2 from #temp
	  group by PickupDriver
              ,Date
	          ,Branch
			  ,DepotName
	          ,Runno
	          ,ProntoID
			  ,CompliancePercentage
			  

update #temp2 set CompliancePercentage=Convert(decimal(12,2),100*([OnBoardComplianceCount]+[DeliveryComplianceCount]+[PODComplianceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 

select * from #temp2 	

end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DailyPerformanceSummarybyPickupDriver]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DailyPerformanceSummarybyPickupDriver]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DailyPerformanceSummarybyPickupDriver]
	TO [couriersplease\Harley.BoydSkinner]
GO
