SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Rpt_DrilldownNoTimeDetailByDrivernumber](@Driver varchar(200),@Date date,@Depot varchar(200)) as
  begin
       --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_DrilldownNoTimeDetailByDrivernumber]
    --' ---------------------------
    --' Purpose:No Time Detail Report-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 31 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 31/12/2014    AK      1.00    Created the procedure                            

    --'=====================================================================
--Select [RevenueType]
--      ,[OnBoardDriver]
--      ,isnull([PickupETAZone],'') as PickupETAZone
--      ,isnull([DeliveryETAZone],'') as DeliveryETAZone
--      ,isnull([BUCode],'') as BUCode
--      ,[NoTimeCount]
--into #temp
--from [DWH].[dbo].[DailyNoTimeReporting] p JOIN [DWH].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id  Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[OnBoardDriver]=D.DriverID  
--where c.date=@Date and 	[OnBoardDriver]=@Driver


SELECT [LabelNumber]
      --,[GWConsignmentID]
      ,[RevenueType]
	  ,isnull([PickupDateTime],'') as PickupScan
      ,isnull([PickupScannedBy],'') as PickupDrivernumber
      ,case left(ltrim(rtrim(PickupScannedBy)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as PickupBranch
	  ,isnull(C.DepotName,'') as PickupDepot
      ,isnull([AttemptedDeliveryDateTime],'') as AttemptScan
	  ,isnull(C1.DepotName,'') as OnBoardDepot
	  ,isnull([OutForDeliverDateTime],'') as OnBoardScan
      ,isnull([DeliveryDateTime],'') as DeliveryScan
	  ,Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'') as DeliveryDriver
	  ,isnull(C2.DepotName,'') as DeliveryDepot
      ,datediff("mi",[PickupDateTime],isnull([DeliveryDateTime],[AttemptedDeliveryDateTime]))/convert(decimal(4,2),60) as DeliveryHours
      ,isnull([PickupETAZone],'') as PickupETAZone
      ,isnull([DeliveryETAZone],'') as DeliveryETAZone
      ,isnull(replace([1stDeliveryCutOff],'XXX',''),'') as [1stDeliveryCutOff]
      ,isnull(replace([1stAllowedDays],'XXX',''),'') as [1stAllowedDays]
      ,isnull(replace([2ndDeliverycutOff],'XXX',''),'') as [2ndDeliveryCutOff]
      ,isnull(replace([2ndAllowedDays],'XXX',''),'') as [2ndAllowedDays]
into #temp
  From [DWH].[dbo].[PrimaryLabels] P 
  Join DWH.dbo.DimContractor C1 on P.OutforDeliveryScannedBy = C1.DriverID
  left Join DWH.dbo.DimContractor C on P.PickupScannedBy = C.DriverID
  
  left Join DWH.dbo.DimContractor C2 on Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'') = C2.DriverID
	 left join  [dbo].[ETACalculator] e on e.FromZone=[PickupETAZone] and e.[ToZone]=[DeliveryETAZone]
   Where Convert(date,OutForDeliverDateTime) = @Date and P.OutforDeliveryScannedBy=@Driver and C1.DepotName=@Depot
   --and  Isnull(OutforDeliveryScannedBy,'') like 'ADL%' 
   and C1.ContractorType not in ('D','B') and C1.ContractorCategory <> 'Agent'
           and ((AttemptedDeliveryDateTime is null 
		   and DeliveryDateTime is null)   OR COnvert(date,[DeliveryDateTime]) >@Date OR Convert(Date,AttemptedDeliveryDateTime) > @Date)
	
--select [LabelNumber]
--      --,[GWConsignmentID]
--      ,[RevenueType]
--	  ,isnull([PickupDateTime],'') as PickupScan
--      ,isnull([PickupScannedBy],'') as PickupDrivernumber
--      ,case left(ltrim(rtrim(PickupScannedBy)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as PickupBranch
--	  ,isnull(C.DepotName,'') as PickupDepot
--	  ,Isnull(OutforDeliveryScannedBy,'') as OnBoardDriver
--      ,isnull([OutForDeliverDateTime],'') as OnBoardDatetime
--      ,isnull([PickupETAZone],'') as PickupETAZone
--      ,isnull([DeliveryETAZone],'') as DeliveryETAZone
--	  into #temp
--From [DWH].[dbo].[PrimaryLabels] P Join DWH.dbo.DimContractor C on P.OutforDeliveryScannedBy = C.DriverID
--   Where Convert(date,OutForDeliverDateTime) = @Date    and Isnull(OutforDeliveryScannedBy,'')=@Driver and C.ContractorType not in ('D','B') and ContractorCategory <> 'Agent'
--           and ((AttemptedDeliveryDateTime is null 
--		   and DeliveryDateTime is null)   OR COnvert(date,[DeliveryDateTime]) >@Date OR Convert(Date,AttemptedDeliveryDateTime) > @Date)
		 
select * from #temp   order by RevenueType


end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DrilldownNoTimeDetailByDrivernumber]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DrilldownNoTimeDetailByDrivernumber]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DrilldownNoTimeDetailByDrivernumber]
	TO [couriersplease\Harley.BoydSkinner]
GO
