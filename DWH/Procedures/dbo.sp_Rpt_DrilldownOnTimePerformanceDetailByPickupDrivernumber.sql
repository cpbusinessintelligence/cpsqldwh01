SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_Rpt_DrilldownOnTimePerformanceDetailByPickupDrivernumber](@Driver varchar(200),@Date date,@Depot varchar(100),@OnTime varchar(100)) as
begin
     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_DrilldownOnTimePerformanceDetailByDrivernumber]
    --' ---------------------------
    --' Purpose: Drilldown Report for OnTimePerformanceDetail By Drivernumber-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 29 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/12/2014    AK      1.00    Created the procedure                            

    --'=====================================================================
SELECT [LabelNumber]
      ,[GWConsignmentID]
      ,[RevenueType]
	  ,isnull([PickupDateTime],'') as PickupScan
      ,isnull([PickupScannedBy],'') as PickupDrivernumber
      ,case left(ltrim(rtrim(PickupScannedBy)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as PickupBranch
	  ,isnull(D.DepotName,'') as PickupDepot
      ,isnull([AttemptedDeliveryDateTime],'') as AttemptScan
      ,isnull([DeliveryDateTime],'') as DeliveryScan
	  ,Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]) as DeliveryDriverNumber
	  ,case left(ltrim(rtrim(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]))),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as DeliveryBranch
	  ,isnull(D1.DepotName,'') as DeliveryDepot
      ,datediff("mi",[PickupDateTime],isnull([AttemptedDeliveryDateTime],[DeliveryDateTime]))/convert(decimal(4,2),60) as DeliveryHours
      ,isnull([PickupETAZone],'') as PickupETAZone
      ,isnull([DeliveryETAZone],'') as DeliveryETAZone
      ,isnull(replace([1stDeliveryCutOffTime],'XXX',''),'') as [1stDeliveryCutOff]
      ,isnull(replace([1stAllowedDays],'XXX',''),'') as [1stAllowedDays]
      ,isnull(replace([2ndDeliverycutOffTime],'XXX',''),'') as [2ndDeliveryCutOff]
      ,isnull(replace([2ndAllowedDays],'XXX',''),'') as [2ndAllowedDays]
	  ,case when OnTimeStatus='Y' then 'On Time' when OnTimeStatus='N' then 'Not On Time' else OnTimeStatus end as OnTimeStatus
	  ,case when OnTimeStatus in ('N') then 'Number of deliverydays exceeds allowed days' else '' end as FailureReason
	 -- ,convert(varchar(100),'') as ConsignmentNumber
	 into #temp

  FROM [DWH].[dbo].[PrimaryLabels] p  Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[PickupScannedBy]=D.DriverID  
  Left Join [DWH].[dbo].[DimContractor] D1  (NOLOCK) on Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy])=D1.DriverID  
  left join  [dbo].[ETACalculator] e on e.FromZone=[PickupETAZone] and e.[ToZone]=[DeliveryETAZone]
  --where Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]) ='ADL0004A207' and Performancereportingdate='2014-11-21' and D1.DepotName='Adelaide'
where Isnull(P.[PickupScannedBy],'') =@Driver and Performancereportingdate=@Date and D.DepotName=@Depot and OnTimeStatus in ('N','Y')

--Update #temp set ConsignmentNumber=Connote from #temp t join [dbo].[Consignment] c on t.GWConsignmentid=c.GWConsignmentid 

select * from #temp where OnTimeStatus in (select * from  [dbo].[fn_GetMultipleValues](@OnTime)) order by OnTimeStatus desc
end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DrilldownOnTimePerformanceDetailByPickupDrivernumber]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DrilldownOnTimePerformanceDetailByPickupDrivernumber]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DrilldownOnTimePerformanceDetailByPickupDrivernumber]
	TO [couriersplease\Harley.BoydSkinner]
GO
