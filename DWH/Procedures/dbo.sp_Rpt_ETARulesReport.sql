SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_Rpt_ETARulesReport](@state varchar(50)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_ETARulesReport] 'nsw'
    --' ---------------------------
    --' Purpose: Get ETA Rules-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 29 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/12/2014    AK      1.00    Created the procedure                            

    --'=====================================================================

select distinct state,ETAZone into #temp from [DWH].[dbo].[PostCodes]

SELECT t.state as State
      ,[FromZone]
      ,[ToZone]
	  ,replace([ETA],'XXX','') as ETA
      ,n.NetworkCategory as NetworkCategory
      ,replace([1stPickupCutOffTime],'XXX','') as [1stPickupCutOff]
      ,replace([1stDeliveryCutOffTime],'XXX','') as [1stDeliveryCutOff]
      ,replace([1stAllowedDays],'XXX','') as [1stAllowedDays]
      ,replace([2ndPickupCutOffTime],'XXX','') as [2ndPickupCutOff]
      ,replace([2ndDeliveryCutOffTime],'XXX','') as [2ndDeliveryCutOff]
      ,replace([2ndAllowedDays],'XXX','') as [2ndAllowedDays]
	  ,case when n.NetworkCategory='Metro' then 1 when n.NetworkCategory='Intrastate' then 2 when n.NetworkCategory='Regional' then 3 when n.NetworkCategory='Interstate' then 4 else 10 end as sortorder
	  into #temp1
  FROM #temp t join ETACalculator e on t.ETAZone=e.FromZone join [dbo].[NetworkCategory] n on n.[NetworkCategory]=e.PrimaryNetworkCategory
 
 If @State='ALL'
 select * from #temp1 order by sortorder,State
else
select * from #temp1  where state=@state order by sortorder
  end

GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_ETARulesReport]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_ETARulesReport]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_ETARulesReport]
	TO [couriersplease\Harley.BoydSkinner]
GO
