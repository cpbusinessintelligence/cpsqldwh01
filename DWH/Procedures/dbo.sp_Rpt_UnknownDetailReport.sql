SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_Rpt_UnknownDetailReport](@State varchar(200),@Date date) as
begin
     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_DrilldownOnTimePerformanceDetailByDrivernumber]
    --' ---------------------------
    --' Purpose: Drilldown Report for OnTimePerformanceDetail By Drivernumber-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 29 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/12/2014    AK      1.00    Created the procedure                            

    --'=====================================================================
SELECT D1.state
      ,[LabelNumber]
      ,[GWConsignmentID]
      ,[RevenueType]
	  ,isnull(Accountcode,'') as Accountcode
	  ,isnull([PickupDateTime],'') as PickupScan
      ,isnull([PickupScannedBy],'') as PickupDrivernumber
      ,case left(ltrim(rtrim(PickupScannedBy)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as PickupBranch
	  ,isnull(D.DepotName,'') as PickupDepot
      ,isnull([AttemptedDeliveryDateTime],'') as AttemptScan
      ,isnull([DeliveryDateTime],'') as DeliveryScan
	  ,isnull([DeliveryScannedBy],'') as DeliveryDrivernumber
	  ,case left(ltrim(rtrim(DeliveryScannedBy)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as DeliveryBranch
	  ,isnull(D1.DepotName,'') as DeliveryDepot
      ,datediff("mi",[PickupDateTime],isnull([DeliveryDateTime],[AttemptedDeliveryDateTime]))/convert(decimal(4,2),60) as DeliveryHours
      ,isnull([PickupETAZone],'') as PickupETAZone
      ,isnull([DeliveryETAZone],'') as DeliveryETAZone
	  ,isnull(ETADate,'') as ETADate
	  ,isnull(replace([1stPickupCutOff],'XXX',''),'') as [1stPickupCutOff]
      ,isnull(replace([1stDeliveryCutOff],'XXX',''),'') as [1stDeliveryCutOff]
      ,isnull(replace([1stAllowedDays],'XXX',''),'') as [1stAllowedDays]
	  ,isnull(replace([2ndPickupCutOff],'XXX',''),'') as [2ndPickupCutOff]
      ,isnull(replace([2ndDeliverycutOff],'XXX',''),'') as [2ndDeliveryCutOff]
      ,isnull(replace([2ndAllowedDays],'XXX',''),'') as [2ndAllowedDays]
	  ,OnTimeStatus
	  ,convert(varchar(500),'') as ReasonforUnknown
	 -- ,case when OnTimeStatus in ('N','CN') then 'Number of deliverydays exceeds allowed days' else '' end as FailureReason
	 -- ,convert(varchar(100),'') as ConsignmentNumber
	 into #temp

  FROM [DWH].[dbo].[PrimaryLabels] p  Left Join [DWH].[dbo].[DimContractor] D  (NOLOCK) on P.[PickupScannedBy]=D.DriverID  
  Left Join [DWH].[dbo].[DimContractor] D1  (NOLOCK) on P.[DeliveryScannedBy]=D1.DriverID  
  left join  [dbo].[ETACalculator] e on ltrim(rtrim(e.FromZone))=ltrim(rtrim([PickupETAZone])) and ltrim(rtrim(e.[ToZone]))=ltrim(rtrim([DeliveryETAZone]))
  --where Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]) ='ADL0004A207' and Performancereportingdate='2014-11-21' and D1.DepotName='Adelaide'
where  OnTimeStatus ='X'  and PerformanceReportingDate=@Date

--Update #temp set ConsignmentNumber=Connote from #temp t join [dbo].[Consignment] c on t.GWConsignmentid=c.GWConsignmentid 

Update #temp set ReasonforUnknown=case when ETAdate is null or ETAdate='1900-01-01 00:00:00.000' then case when PickupDrivernumber='' then 'Pickup Driver is not known' when PickupETAZone='' then 'PickupETAZone is not known' when DeliveryETAZone='' then 'DeliveryETAZone is not known' when Pickupscan is null or Pickupscan='1900-01-01 00:00:00.000' then 'Pickup scan date is not present' else 'Unknown' end else '' end

If @State='ALL'
select isnull(State,'Unknown') as State
      ,[LabelNumber]
      ,[RevenueType]
	  ,Accountcode
	  ,PickupScan
      ,PickupDrivernumber
      ,PickupBranch
	  ,PickupDepot
      ,AttemptScan
      ,DeliveryScan
	  ,DeliveryBranch
	  ,DeliveryDepot
      ,DeliveryHours
      ,PickupETAZone
      ,DeliveryETAZone
	  ,ETADate
	  ,[1stPickupCutOff]
      ,[1stDeliveryCutOff]
      ,[1stAllowedDays]
	  ,[2ndPickupCutOff]
      ,[2ndDeliveryCutOff]
      ,[2ndAllowedDays]
	  ,OnTimeStatus
	  ,ReasonforUnknown
	  from #temp order by State,RevenueType
else
	  select State
     ,[LabelNumber]
      ,[RevenueType]
	  ,Accountcode
	  ,PickupScan
      ,PickupDrivernumber
      ,PickupBranch
	  ,PickupDepot
      ,AttemptScan
      ,DeliveryScan
	  ,DeliveryDrivernumber
	  ,DeliveryBranch
	  ,DeliveryDepot
      ,DeliveryHours
      ,PickupETAZone
      ,DeliveryETAZone
	  ,ETADate
	  ,[1stPickupCutOff]
      ,[1stDeliveryCutOff]
      ,[1stAllowedDays]
	  ,[2ndPickupCutOff]
      ,[2ndDeliveryCutOff]
      ,[2ndAllowedDays]
	  ,OnTimeStatus
	  ,ReasonforUnknown
	   from #temp where state=@State order by State,RevenueType
end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_UnknownDetailReport]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_UnknownDetailReport]
	TO [COURIERSPLEASE\lynn.wong]
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_UnknownDetailReport]
	TO [couriersplease\Harley.BoydSkinner]
GO
