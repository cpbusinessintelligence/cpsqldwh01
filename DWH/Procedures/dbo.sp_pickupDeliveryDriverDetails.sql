SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procedure [dbo].[sp_pickupDeliveryDriverDetails]  as
begin

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select  Label as LabelNumber,
                 Customer,
				 Status,
               --RevenueRecogniseddate,
			   convert(datetime,'') as PUDateTime,
			   convert(varchar(100),'') as PUBranch,
			   convert(varchar(100),'') as PUDriver,
			   convert(varchar(100),'') as PUETAZone,
			   convert(datetime,'') as AttDelDateTime,
			   convert(varchar(100),'') as AttDelBranch,
			   convert(varchar(100),'') as AttDelDriver,
			   convert(varchar(100),'') as AttDelETAZone,
			   convert(varchar(100),'') as RedeliveryCard,
			    convert(datetime,'') as DelDateTime,
			   convert(varchar(100),'') as DelBranch,
			   convert(varchar(100),'') as DelDriver,
			   convert(varchar(100),'') as DelETAZone
 into #temp2 from cpsqldwh01.[DWH].[dbo].[RsCompnentsLabelDetails]  

 --drop table #temp

 --Select * from vw_trackingevent where sourcereference='13502308821 '

 Update #temp2 set PUDateTime=eventdatetime,
			      PUBranch=b.name,
			      PUDriver=d.code,
			      PUETAZone=b1.etazone
from #temp2 join [CPSQLDWH01].scannergateway.dbo.vw_trackingevent with (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join [CPSQLDWH01].scannergateway.dbo.driver d with (NOLOCK) on d.id=driverid
		   join [CPSQLDWH01].scannergateway.dbo.branch b with (NOLOCK) on b.id=d.branchid
		   left join [CPSQLDWH01].cosmos.dbo.driver b1 with (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'



Update #temp2 set AttDelDateTime=eventdatetime,
			     AttDelBranch=b.name,
			     AttDelDriver=d.code,
			     AttDelETAZone=b1.etazone
				-- TransferredtoAgent=isnull(Additionaltext2,'')
from #temp2 join [CPSQLDWH01].scannergateway.dbo.vw_trackingevent with  (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join [CPSQLDWH01].scannergateway.dbo.driver d with (NOLOCK) on d.id=driverid
		   join [CPSQLDWH01].scannergateway.dbo.branch b with (NOLOCK) on b.id=d.branchid
		   left join [CPSQLDWH01].cosmos.dbo.driver b1 with (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'


Update #temp2 set RedeliveryCard=replace(additionaltext1,'Link Coupon ','') 
from  #temp2 join [CPSQLDWH01].scannergateway.dbo.vw_trackingevent with (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
where eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and (additionaltext1 like 'Link Coupon 191%' or additionaltext1 like 'Link Coupon NH%')


Update #temp2 set DelDateTime=eventdatetime,
			     DelBranch=b.name,
			     DelDriver=d.code,
			     DelETAZone=b1.etazone
				-- TransferredtoAgent=isnull(Additionaltext2,'')
from #temp2 join [CPSQLDWH01].scannergateway.dbo.vw_trackingevent with (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join [CPSQLDWH01].scannergateway.dbo.driver d with (NOLOCK) on d.id=driverid
		   join [CPSQLDWH01].scannergateway.dbo.branch b with (NOLOCK) on b.id=d.branchid
		   left join [CPSQLDWH01].cosmos.dbo.driver b1 WITH (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3'

Select LabelNumber,Customer,isnull(Status,'') as Status, replace(FORMAT(PUDateTime,'dd/MM/yyyy h:mm:ss tt'),'01/01/1900 12:00:00 AM','') as PUDateTime,isnull(PUBranch,'') as PUBranch,isnull(PUDriver, '') as PUDriver,
 isnull(PUETAZone,'') as PUETAZone,replace(format(AttDelDateTime,'dd/MM/yyyy hh:mm:ss tt'),'01/01/1900 12:00:00 AM','') as AttDelDateTime,isnull(AttDelBranch,'') as AttDelBranch,isnull(AttDelDriver,'') as AttDelDriver,
isnull(AttDelETAZone,'') as AttDelETAZone,isnull(RedeliveryCard,'') as RedeliveryCard,replace(format(DelDateTime,'dd/MM/yyyy hh:mm:ss tt'),'01/01/1900 12:00:00 AM','') as DelDateTime,isnull(DelBranch,'') as DelBranch,
isnull(DelDriver,'') as DelDriver,isnull(DelETAZone,'') as DelETAZone from #temp2


--drop table #temp2



END

GO
