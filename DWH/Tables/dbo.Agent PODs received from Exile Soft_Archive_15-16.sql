SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Agent PODs received from Exile Soft_Archive_15-16] (
		[Date]                  [datetime] NULL,
		[ConsignmentNumber]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[filename]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[foldername]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Agent PODs received from Exile Soft_Archive_15-16] SET (LOCK_ESCALATION = TABLE)
GO
