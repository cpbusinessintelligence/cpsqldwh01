SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractorCategory] (
		[ContractorCategoryID]     [int] IDENTITY(1, 1) NOT NULL,
		[ETAZone]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ZoneName]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ZoneServiceCategory]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Region]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                   [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Category]                 [varchar](70) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ContractorCategory]
		PRIMARY KEY
		CLUSTERED
		([ContractorCategoryID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ContractorCategory] SET (LOCK_ESCALATION = TABLE)
GO
