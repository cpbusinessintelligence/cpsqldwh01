SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponDetailsWithRevenueWithIn2years] (
		[SerialNumber]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StartSerialNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LastSold]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Prefix]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountLastSale]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Comment]               [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Age]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SheetName]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RevenueAmount]         [numeric](38, 19) NULL
)
GO
ALTER TABLE [dbo].[CouponDetailsWithRevenueWithIn2years] SET (LOCK_ESCALATION = TABLE)
GO
