SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DWHParameters_Archive] (
		[DWHParametersID]     [int] IDENTITY(1, 1) NOT NULL,
		[Code]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]         [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
		[Value]               [varchar](800) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]         [datetime] NOT NULL,
		[EditWho]             [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[EditDateTime]        [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[DWHParameters_Archive] SET (LOCK_ESCALATION = TABLE)
GO
