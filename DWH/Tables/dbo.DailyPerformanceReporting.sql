SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyPerformanceReporting] (
		[PerformanceReportingID]      [int] IDENTITY(1, 1) NOT NULL,
		[PerformanceDayID]            [int] NOT NULL,
		[RevenueType]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryDriver]              [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupDriver]                [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[OnTimeStatus]                [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupETAZone]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]           [int] NULL,
		[BUCode]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceCount]            [int] NOT NULL,
		[OnBoardComplainceCount]      [int] NOT NULL,
		[DeliveryComplainceCount]     [int] NOT NULL,
		[PODComplainceCount]          [int] NOT NULL,
		[AddWho]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                 [datetime] NULL,
		CONSTRAINT [PK_DailyPerformanceReporting]
		PRIMARY KEY
		CLUSTERED
		([PerformanceReportingID])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_DailyPerformanceReporting]
	ON [dbo].[DailyPerformanceReporting] ([PerformanceDayID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[DailyPerformanceReporting] SET (LOCK_ESCALATION = TABLE)
GO
