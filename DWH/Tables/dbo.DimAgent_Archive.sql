SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimAgent_Archive] (
		[Id]                   [int] IDENTITY(1, 1) NOT NULL,
		[AgentID]              [int] NOT NULL,
		[AgentCode]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AgentName]            [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[SupplierCode]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[QueryBranch]          [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsCPPLContractor]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsPODRequired]        [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[DriverNumber]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DimAgent_Archive] SET (LOCK_ESCALATION = TABLE)
GO
