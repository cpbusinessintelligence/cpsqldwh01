SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimContractor] (
		[Id]                     [int] IDENTITY(1, 1) NOT NULL,
		[DriverID]               [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DriverNumber]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntoID]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DepotCode]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DepotName]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Branch]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ContractorType]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DriverName]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[ETAZone]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[PricingZone]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ContractorCategory]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Region]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Address1]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address2]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address3]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PhoneNumber]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[MobileNumber]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FaxNumber]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CreditLimit]            [decimal](12, 2) NULL,
		[StartDate]              [date] NULL,
		[EndDate]                [date] NULL,
		[ArchiveDate]            [date] NULL,
		CONSTRAINT [UQ__DimContr__F1B1CD2581FEDB2A]
		UNIQUE
		NONCLUSTERED
		([DriverID])
		ON [PRIMARY],
		CONSTRAINT [PK__DimContr__3214EC0769507B2F]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimContractor] SET (LOCK_ESCALATION = TABLE)
GO
