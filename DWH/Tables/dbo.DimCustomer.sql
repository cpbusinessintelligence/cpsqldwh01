SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimCustomer] (
		[RevenueType]          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Accountcode]          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ParentCompany]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[CompanyName]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TermDiscount]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MarketingFlag]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AverageDaysToPay]     [int] NULL,
		[Postcode]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Locality]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepName]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepCode]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastSale]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoDebtor]
		PRIMARY KEY
		CLUSTERED
		([Accountcode], [RevenueType])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimCustomer] SET (LOCK_ESCALATION = TABLE)
GO
