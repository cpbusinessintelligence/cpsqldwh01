SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ETACalculator] (
		[ETAID]                                  [int] IDENTITY(1, 1) NOT NULL,
		[FromZone]                               [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]                                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[PrimaryNetworkCategory]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryNetworkCategory]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ETA]                                    [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[FromETA]                                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ToETA]                                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[1stPickupCutOffTime]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stDeliveryCutOffTime]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stAllowedDays]                         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[2ndPickupCutOffTime]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndDeliveryCutOffTime]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndAllowedDays]                         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AIR_ETA]                                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AIR_FromETA]                            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AIR_ToETA]                              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AIR_1stDeliveryCutOffTime]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AIR_1stAllowedDays]                     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AIR_2ndDeliveryCutOffTime]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AIR_2ndAllowedDays]                     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SameDay_ETA]                            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SameDay_FromETA]                        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SameDay_ToETA]                          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SameDay_1stDeliveryCutOffTime]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SameDay_1stAllowedDays]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SameDay_2ndDeliveryCutOffTime]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SameDay_2ndAllowedDays]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AddWho]                                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]                            [datetime] NOT NULL,
		[EditWho]                                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]                           [datetime] NULL,
		[Domestic_OffPeak_FromETA]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Domestic_OffPeak_ToETA]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Domestic_OffPeak_1stDeliveryCutOff]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Domestic_OffPeak_1stAllowedDays]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Domestic_OffPeak_2ndDeliveryCutOff]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Domestic_OffPeak_2ndAllowedDays]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Domestic_OffPeak_ETA]                   [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK1_ETACalculator]
		PRIMARY KEY
		CLUSTERED
		([ETAID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ETACalculator]
	ADD
	CONSTRAINT [DF1_ETACalculator_AddDateTime]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[ETACalculator]
	ADD
	CONSTRAINT [DF1_ETACalculator_EditDateTime]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
ALTER TABLE [dbo].[ETACalculator] SET (LOCK_ESCALATION = TABLE)
GO
