SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ETACalculator_OldSchema] (
		[ETACalculatorID]       [int] NOT NULL,
		[FromZone]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ETA]                   [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[FromETA]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToETA]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategory]       [int] NOT NULL,
		[1stPickupCutOff]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stDeliveryCutOff]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stAllowedDays]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndPickupCutOff]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndDeliveryCutOff]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndAllowedDays]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ETACalculator]
		PRIMARY KEY
		CLUSTERED
		([ETACalculatorID])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_ETACalculator]
	ON [dbo].[ETACalculator_OldSchema] ([FromZone], [ToZone])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX1_ETACalculator]
	ON [dbo].[ETACalculator_OldSchema] ([FromZone], [ToZone])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ETACalculator_OldSchema] SET (LOCK_ESCALATION = TABLE)
GO
