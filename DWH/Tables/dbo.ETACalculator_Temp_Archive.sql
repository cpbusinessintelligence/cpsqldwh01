SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ETACalculator_Temp_Archive] (
		[ETACalculatorID]     [int] NOT NULL,
		[FromZone]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ETA]                 [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[FromETA]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToETA]               [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ETACalculator_Temp_Archive] SET (LOCK_ESCALATION = TABLE)
GO
