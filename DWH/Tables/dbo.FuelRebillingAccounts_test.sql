SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FuelRebillingAccounts_test] (
		[AccountCode]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ContactName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Amount]              [int] NULL,
		[IsProcessed]         [bit] NULL,
		[CreatedDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[FuelRebillingAccounts_test]
	ADD
	CONSTRAINT [DF_FuelRebillingAccounts_test_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[FuelRebillingAccounts_test]
	ADD
	CONSTRAINT [DF_FuelRebillingAccounts_test_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[FuelRebillingAccounts_test] SET (LOCK_ESCALATION = TABLE)
GO
