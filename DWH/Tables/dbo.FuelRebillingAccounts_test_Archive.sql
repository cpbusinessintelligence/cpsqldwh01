SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FuelRebillingAccounts_test_Archive] (
		[AccountCode]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ContactName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Amount]              [int] NULL,
		[IsProcessed]         [bit] NULL,
		[CreatedDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[FuelRebillingAccounts_test_Archive]
	ADD
	CONSTRAINT [DF__FuelRebil__IsPro__168449D3]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[FuelRebillingAccounts_test_Archive]
	ADD
	CONSTRAINT [DF__FuelRebil__Creat__17786E0C]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[FuelRebillingAccounts_test_Archive] SET (LOCK_ESCALATION = TABLE)
GO
