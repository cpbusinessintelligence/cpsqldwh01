SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FuelSurcharge_Archive] (
		[AccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Amount]          [int] NULL,
		[Email]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Status]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ContactName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FuelSurcharge_Archive] SET (LOCK_ESCALATION = TABLE)
GO
