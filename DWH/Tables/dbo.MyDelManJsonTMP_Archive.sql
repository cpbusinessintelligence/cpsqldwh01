SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MyDelManJsonTMP_Archive] (
		[Date]                             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[JSONResponse]                     [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[ReqID]                            [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConStatus]                        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReceivedAtDepotDateSql]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LabelStatus]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Rejected]                         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RejectReason]                     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[RejectNote]                       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[IconName]                         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryId]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredBy]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectedDateSql]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectedDateString]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredDateSql]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredDateString]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReturnedDateSql]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReturnedDateString]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredLat]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredLon]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[POD]                              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[SignedBy]                         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAcceptedDamagedGoods]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryConfirmed]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryStatus]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CardDelivery]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FailedReason]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryNote]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NoScanPickup]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NoScanDelivery]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MultiConsignmentDeliveryId]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RowNumber]                        [int] NULL,
		[PhotoPOD]                         [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]                      [bit] NOT NULL,
		[CreatedBy]                        [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]                  [datetime] NOT NULL,
		[UpdatedBy]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]                     [datetime] NULL
)
GO
ALTER TABLE [dbo].[MyDelManJsonTMP_Archive]
	ADD
	CONSTRAINT [DF__MyDelManJ__IsPro__26EFBBC6]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[MyDelManJsonTMP_Archive]
	ADD
	CONSTRAINT [DF__MyDelManJ__Creat__27E3DFFF]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[MyDelManJsonTMP_Archive]
	ADD
	CONSTRAINT [DF__MyDelManJ__Creat__28D80438]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[MyDelManJsonTMP_Archive]
	ADD
	CONSTRAINT [DF__MyDelManJ__EditD__29CC2871]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
ALTER TABLE [dbo].[MyDelManJsonTMP_Archive] SET (LOCK_ESCALATION = TABLE)
GO
