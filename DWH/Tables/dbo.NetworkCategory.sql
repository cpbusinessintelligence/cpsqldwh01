SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NetworkCategory] (
		[NetworkCategoryID]            [int] IDENTITY(1, 1) NOT NULL,
		[NetworkCategory]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryNetworkCategory]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AddWho]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                  [datetime] NULL,
		[EditWho]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]                 [datetime] NULL,
		CONSTRAINT [PK__NetworkC__9578FC7F31CD8CC8]
		PRIMARY KEY
		CLUSTERED
		([NetworkCategoryID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[NetworkCategory] SET (LOCK_ESCALATION = TABLE)
GO
