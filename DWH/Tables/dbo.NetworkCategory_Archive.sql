SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NetworkCategory_Archive] (
		[NetworkCategoryID]            [int] IDENTITY(1, 1) NOT NULL,
		[NetworkCategory]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryNetworkCategory]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AddWho]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                  [datetime] NULL,
		[EditWho]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]                 [datetime] NULL
)
GO
ALTER TABLE [dbo].[NetworkCategory_Archive] SET (LOCK_ESCALATION = TABLE)
GO
