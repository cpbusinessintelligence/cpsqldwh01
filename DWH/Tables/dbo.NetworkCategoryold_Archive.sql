SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NetworkCategoryold_Archive] (
		[NetworkCategoryID]              [int] IDENTITY(1, 1) NOT NULL,
		[RevenueType]                    [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[OriginDepotCode]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[OriginDepotName]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[OriginDepotState]               [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationDepotCode]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationDepotName]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationDepotState]          [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[NetworkCategoryMain]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[NetworkCategorySecondary]       [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[NetworkCategoryDescription]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                    [datetime] NULL,
		[EditWho]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]                   [datetime] NULL
)
GO
ALTER TABLE [dbo].[NetworkCategoryold_Archive] SET (LOCK_ESCALATION = TABLE)
GO
