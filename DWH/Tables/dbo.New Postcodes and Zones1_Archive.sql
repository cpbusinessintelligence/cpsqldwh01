SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[New Postcodes and Zones1_Archive] (
		[Postcode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pcsuburb]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pcsuburbstate]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NEW ETA ZONE]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Agent]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Alternate Delivery Agent]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Agent Runcode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NEW ETA ZONE1]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Agent1]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentRunCode1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Alternate Delivery Agent1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 13]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 14]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[New Postcodes and Zones1_Archive] SET (LOCK_ESCALATION = TABLE)
GO
