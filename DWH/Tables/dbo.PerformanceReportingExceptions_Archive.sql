SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PerformanceReportingExceptions_Archive] (
		[ProductType]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Prefix/AccountCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Desscription]           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[PickupBranch]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ReportTypeFlag]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddtoETADays]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AllowedDays]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AllowFromFlag]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stPickupCutOff]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stDeliveryCutOff]      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stAllowedDays]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndPickupCutOff]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndDeliveryOff]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndAllowedDays]         [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PerformanceReportingExceptions_Archive] SET (LOCK_ESCALATION = TABLE)
GO
