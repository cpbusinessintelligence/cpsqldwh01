SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrePaidPrefixFromZoneMapping_Archive] (
		[PREFIX]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DESCRIPTION]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BRANCH]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PrePaidPrefixFromZoneMapping_Archive] SET (LOCK_ESCALATION = TABLE)
GO
