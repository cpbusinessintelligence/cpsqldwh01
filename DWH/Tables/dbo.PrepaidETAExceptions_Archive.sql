SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrepaidETAExceptions_Archive] (
		[PrepaidETAExceptionsID]     [int] IDENTITY(1, 1) NOT NULL,
		[CouponPrefix]               [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CouponName]                 [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ExceptionReason]            [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddtoETADays]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AllowedDays]                [int] NULL,
		[1stPickupCutOff]            [time](7) NULL,
		[1stDeliveryCutOff]          [time](7) NULL,
		[1stAllowedDays]             [float] NULL,
		[2ndPickupCutOff]            [time](7) NULL,
		[2ndDeliveryOff]             [time](7) NULL,
		[2ndAllowedDays]             [float] NULL,
		[AddWho]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                [datetime] NULL,
		[EditWho]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]               [datetime] NULL
)
GO
ALTER TABLE [dbo].[PrepaidETAExceptions_Archive] SET (LOCK_ESCALATION = TABLE)
GO
