SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrimaryLabels_Archive15-16] (
		[PrimaryLabelsID]                [int] NOT NULL,
		[GWConsignmentID]                [int] NULL,
		[RevenueType]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                    [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupScannedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupDateTime]                 [datetime] NULL,
		[InDepotScannedBy]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[InDepotDateTime]                [datetime] NULL,
		[HandoverScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[HandoverDateTime]               [datetime] NULL,
		[TransferScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[TransferDateTime]               [datetime] NULL,
		[OutforDeliveryScannedBy]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[OutForDeliverDateTime]          [datetime] NULL,
		[AttemptedDeliveryScannedBy]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryDateTime]      [datetime] NULL,
		[DeliveryScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDateTime]               [datetime] NULL,
		[IsPODPresent]                   [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[PODDateTime]                    [datetime] NULL,
		[DeliveryContractorType]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupETAZone]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]              [int] NULL,
		[ETADate]                        [datetime] NULL,
		[OnTimeStatus]                   [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceReportingDate]       [date] NULL,
		[PerformanceProcessed]           [int] NULL,
		[AddWho]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelCreatedDateTime]           [datetime] NULL,
		[AddDateTime]                    [datetime] NULL
)
GO
ALTER TABLE [dbo].[PrimaryLabels_Archive15-16] SET (LOCK_ESCALATION = TABLE)
GO
