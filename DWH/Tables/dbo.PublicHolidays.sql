SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PublicHolidays] (
		[PublicHolidaysID]     [int] IDENTITY(1, 1) NOT NULL,
		[Date]                 [date] NOT NULL,
		[Description]          [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
		[Zone]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddWho]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]          [datetime] NULL,
		[EditWho]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]         [datetime] NULL,
		CONSTRAINT [PK_PublicHolidays]
		PRIMARY KEY
		CLUSTERED
		([PublicHolidaysID])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_PublicHolidays]
	ON [dbo].[PublicHolidays] ([Date], [Zone])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[PublicHolidays] SET (LOCK_ESCALATION = TABLE)
GO
