SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RsCompnentsLabelDetails] (
		[Label]        [nchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Customer]     [nchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Status]       [nchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RsCompnentsLabelDetails] SET (LOCK_ESCALATION = TABLE)
GO
