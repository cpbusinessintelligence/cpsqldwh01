SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RsCompnentsLabelDetails_Archive] (
		[Label]        [nchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Customer]     [nchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Status]       [nchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RsCompnentsLabelDetails_Archive] SET (LOCK_ESCALATION = TABLE)
GO
