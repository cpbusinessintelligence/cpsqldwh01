SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesRepList] (
		[Rep]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Location]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SalesRepList] SET (LOCK_ESCALATION = TABLE)
GO
