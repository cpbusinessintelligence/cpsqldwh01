SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SecondaryLabels] (
		[SecondaryLabelsID]        [int] IDENTITY(1, 1) NOT NULL,
		[RevenueType]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryLabelNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SecondaryLabelPrefix]     [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PrimaryLabelNumber]       [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[ScanDateTime]             [datetime] NULL,
		[ScannedBy]                [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[LabelCreatedDateTime]     [datetime] NULL,
		[AddWho]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]              [datetime] NULL,
		CONSTRAINT [PK_SecondaryLabels]
		PRIMARY KEY
		CLUSTERED
		([SecondaryLabelsID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SecondaryLabels] SET (LOCK_ESCALATION = TABLE)
GO
