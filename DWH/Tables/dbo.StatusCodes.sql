SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatusCodes] (
		[StatusCodesID]     [int] IDENTITY(1, 1) NOT NULL,
		[Code]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]       [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
		[SortOrder]         [int] NOT NULL,
		[AddWho]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]       [datetime] NOT NULL,
		[EditWho]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[EditDateTime]      [datetime] NOT NULL,
		CONSTRAINT [PK_StatusCodes]
		PRIMARY KEY
		CLUSTERED
		([StatusCodesID])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_StatusCodes]
	ON [dbo].[StatusCodes] ([Code])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[StatusCodes] SET (LOCK_ESCALATION = TABLE)
GO
