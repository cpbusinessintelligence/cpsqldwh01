SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TargetKPI_Archive] (
		[TargetKPIID]           [int] IDENTITY(1, 1) NOT NULL,
		[MonthKey]              [int] NOT NULL,
		[Type]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Target]                [decimal](12, 4) NOT NULL,
		[TargetLowerCutOff]     [decimal](12, 4) NOT NULL,
		[AddWho]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]           [datetime] NULL,
		[EditWho]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]          [datetime] NULL
)
GO
ALTER TABLE [dbo].[TargetKPI_Archive] SET (LOCK_ESCALATION = TABLE)
GO
