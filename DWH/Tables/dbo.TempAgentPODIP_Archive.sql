SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempAgentPODIP_Archive] (
		[filename]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[TempAgentPODIP_Archive] SET (LOCK_ESCALATION = TABLE)
GO
