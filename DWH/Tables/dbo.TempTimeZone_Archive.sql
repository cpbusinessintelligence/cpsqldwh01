SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempTimeZone_Archive] (
		[State]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[StdTimeZone]     [decimal](4, 2) NULL,
		[DSTimeZone]      [decimal](4, 2) NULL
)
GO
ALTER TABLE [dbo].[TempTimeZone_Archive] SET (LOCK_ESCALATION = TABLE)
GO
