SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Weekends_Archive] (
		[Pid]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WeekEndingdate]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Zone]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Addwho]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[adddatetime]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[editwho]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[editdatetime]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 8]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 9]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 10]          [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Weekends_Archive] SET (LOCK_ESCALATION = TABLE)
GO
