SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFMSConsignments] (
		[ID]                        [int] IDENTITY(1, 1) NOT NULL,
		[FMSClinet]                 [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentNumber]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryNote]              [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[Status]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusDate]                [datetime] NULL,
		[EstimatedDeliveryDate]     [datetime] NULL,
		[DeliveryDate]              [datetime] NULL,
		[POD]                       [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[BranchID]                  [bigint] NULL,
		[IsProcessed]               [bit] NOT NULL,
		[CreatedBy]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]           [datetime] NOT NULL,
		[UpdatedBy]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]           [datetime] NULL,
		CONSTRAINT [PK__tblFMSCo__3214EC27A4FB6104]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblFMSConsignments]
	ADD
	CONSTRAINT [DF__tblFMSCon__IsPro__0ADD8CFD]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblFMSConsignments]
	ADD
	CONSTRAINT [DF__tblFMSCon__Creat__0BD1B136]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblFMSConsignments]
	ADD
	CONSTRAINT [DF__tblFMSCon__Creat__0CC5D56F]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblFMSConsignments]
	ADD
	CONSTRAINT [DF__tblFMSCon__Updat__0DB9F9A8]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblFMSConsignments] SET (LOCK_ESCALATION = TABLE)
GO
