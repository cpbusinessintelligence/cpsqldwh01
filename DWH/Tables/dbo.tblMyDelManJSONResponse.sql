SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelManJSONResponse] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[DateOfRequest]       [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[Date]                [datetime] NULL,
		[JSONResponse]        [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedBy]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[UpdatedBy]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__6F9F86DC]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__7093AB15]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__7187CF4E]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse] SET (LOCK_ESCALATION = TABLE)
GO
