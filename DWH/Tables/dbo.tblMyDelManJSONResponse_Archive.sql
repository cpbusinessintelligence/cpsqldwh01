SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelManJSONResponse_Archive] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[DateOfRequest]       [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[Date]                [datetime] NULL,
		[JSONResponse]        [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedBy]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[UpdatedBy]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__7ADC2F5E]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__7BD05397]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__7CC477D0]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelManJSONResponse_Archive] SET (LOCK_ESCALATION = TABLE)
GO
