SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelManStatusMapping] (
		[Event]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Event_Type]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Exception_Reason]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedBy]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[UpdatedBy]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblMyDelManStatusMapping]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__78BEDCC2]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelManStatusMapping]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__79B300FB]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelManStatusMapping]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__7AA72534]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelManStatusMapping] SET (LOCK_ESCALATION = TABLE)
GO
