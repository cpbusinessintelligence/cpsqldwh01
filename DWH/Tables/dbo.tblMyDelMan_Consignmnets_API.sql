SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelMan_Consignmnets_API] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[DateOfRequest]         [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[ReqID]                 [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConStatus]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[JSONResponse]          [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsProcessed]           [bit] NOT NULL,
		[CreatedBy]             [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[UpdatedBy]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		CONSTRAINT [PK__tblMyDel__3214EC272CF3185F]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblMyDelMan_Consignmnets_API]
	ADD
	CONSTRAINT [DF__tblMyDelM__IsPro__6F357288]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblMyDelMan_Consignmnets_API]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__702996C1]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelMan_Consignmnets_API]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__711DBAFA]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_Consignmnets_API]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__7211DF33]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_Consignmnets_API] SET (LOCK_ESCALATION = TABLE)
GO
