SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelMan_Deliveries] (
		[ReqID]                            [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[DateOfRequest]                    [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentNumber]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryId]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredBy]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectedDateSql]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectedDateString]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredDateSql]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredDateString]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReturnedDateSql]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReturnedDateString]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredLat]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredLon]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[POD]                              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[SignedBy]                         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAcceptedDamagedGoods]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryConfirmed]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryStatus]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CardDelivery]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FailedReason]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryNote]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NoScanPickup]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NoScanDelivery]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MultiConsignmentDeliveryId]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]                      [bit] NOT NULL,
		[CreatedBy]                        [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]                  [datetime] NOT NULL,
		[UpdatedBy]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]                  [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__00CA12DE]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__01BE3717]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__02B25B50]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries]
	ADD
	CONSTRAINT [DF__tblMyDelM__IsPro__7FD5EEA5]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries] SET (LOCK_ESCALATION = TABLE)
GO
