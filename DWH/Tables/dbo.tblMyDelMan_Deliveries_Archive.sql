SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelMan_Deliveries_Archive] (
		[ReqID]                            [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[DateOfRequest]                    [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentNumber]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryId]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredBy]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectedDateSql]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectedDateString]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredDateSql]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredDateString]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReturnedDateSql]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReturnedDateString]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredLat]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredLon]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[POD]                              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[SignedBy]                         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAcceptedDamagedGoods]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryConfirmed]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryStatus]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CardDelivery]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FailedReason]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryNote]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NoScanPickup]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NoScanDelivery]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MultiConsignmentDeliveryId]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]                      [bit] NOT NULL,
		[CreatedBy]                        [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]                  [datetime] NOT NULL,
		[UpdatedBy]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]                  [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__IsPro__40AF8DC9]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__41A3B202]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__4297D63B]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__438BFA74]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_Deliveries_Archive] SET (LOCK_ESCALATION = TABLE)
GO
