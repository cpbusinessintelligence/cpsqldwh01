SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelMan_Item_Archive] (
		[DateOfRequest]              [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[ItemID]                     [int] IDENTITY(1, 1) NOT NULL,
		[ReqID]                      [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReceivedAtDepotDateSql]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LabelStatus]                [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Rejected]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RejectReason]               [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[RejectNote]                 [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[IconName]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryId]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedBy]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]            [datetime] NOT NULL,
		[UpdatedBy]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]            [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblMyDelMan_Item_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__3138400F]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelMan_Item_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__322C6448]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_Item_Archive] SET (LOCK_ESCALATION = TABLE)
GO
