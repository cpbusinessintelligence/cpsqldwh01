SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelMan_PhotoPOD] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[DateOfRequest]         [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[ReqID]                 [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryId]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredBy]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RowNumber]             [int] NULL,
		[PhotoPOD]              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]           [bit] NOT NULL,
		[CreatedBy]             [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[UpdatedBy]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		CONSTRAINT [PK__tblMyDel__3214EC27CD8AB5F8]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD]
	ADD
	CONSTRAINT [DF__tblMyDelM__IsPro__058EC7FB]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__0682EC34]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__0777106D]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__086B34A6]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD] SET (LOCK_ESCALATION = TABLE)
GO
