SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMyDelMan_PhotoPOD_Archive] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[DateOfRequest]         [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[ReqID]                 [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryId]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveredBy]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RowNumber]             [int] NULL,
		[PhotoPOD]              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]           [bit] NOT NULL,
		[CreatedBy]             [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[UpdatedBy]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__IsPro__4668671F]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__475C8B58]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Creat__4850AF91]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD_Archive]
	ADD
	CONSTRAINT [DF__tblMyDelM__Updat__4944D3CA]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblMyDelMan_PhotoPOD_Archive] SET (LOCK_ESCALATION = TABLE)
GO
