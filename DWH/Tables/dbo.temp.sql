SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp] (
		[Snumber]         [int] IDENTITY(1, 1) NOT NULL,
		[minPostcode]     [int] NULL,
		[maxPostcode]     [int] NULL,
		[ETAZone]         [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[temp] SET (LOCK_ESCALATION = TABLE)
GO
