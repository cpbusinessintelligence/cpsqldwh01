SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE function [dbo].[fn_CalculateInsuranceforAPIConsignments](@Consignment varchar(100)) returns decimal(12,2) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - fn_CalculateInsuranceforAPIConsignments
	    --' ---------------------------
    --' Purpose: fn_CalculateInsuranceforAPIConsignments-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 30 May 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 30/05/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

declare @DHLFuelsurcharge decimal(12,2)= 18.25 -- 15.50 Mauy 2018 
declare @SavFuelsurcharge decimal(12,2)=12.50
declare @insurance decimal(12,2)

Select @insurance=case when (ic.rate+c.NetSubTotal)>=150 then 0.01*(ic.rate+(case when c.ratecardid like 'SAV%' then @SavFuelsurcharge*ic.rate/100 else @DHLFuelsurcharge*ic.rate/100 end)+c.netsubtotal) else 0 end    
from tblconsignment c join tbladdress a  on a.addressid=pickupid
                      join tbladdress a1 on a1.addressid=destinationid
					  join cpsqlops01.couponcalculator.dbo.intcountry i on i.countryname=a.country
					  join cpsqlops01.couponcalculator.dbo.intzone z on i.zone=z.zonename
					  --case when a.country='AUSTRALIA' then a1.country else a.country end
					  join cpsqlops01.couponcalculator.dbo.intcountry i1 on i1.countryname=a1.country
					  join cpsqlops01.couponcalculator.dbo.intzone z1 on i1.zone=z1.zonename
					  --case when a.country='AUSTRALIA' then a1.country else a.country end
					  join cpsqlops01.couponcalculator.dbo.intratecalculation ic on c.ratecardid=ic.servicecode and (CASE WHEN c.TotalWeight > c.TotalVolume THEN c.TotalWeight ELSE c.TotalVolume END)  >= fromweight and  (CASE WHEN c.TotalWeight > c.TotalVolume THEN c.TotalWeight ELSE c.TotalVolume END)<=toweight
					                                                                                            and fromzoneid=z.zonecode and tozoneid=z1.zonecode
where isinsurance=1 and isinternational=1 and c.ConsignmentCode=@Consignment

Return @Insurance;
end



GO
