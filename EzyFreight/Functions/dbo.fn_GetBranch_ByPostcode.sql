SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GetBranch_ByPostcode]
(
	@Postcode varchar(5)
)
RETURNS varchar(20)
AS
BEGIN

	DECLARE @ret varchar(15);
	SELECT @ret = Null;
	
	SELECT @ret = CASE 
					WHEN @postcode In ('2372','2460','2463','2469','2470','2472','2474','2475','2476','2478','2479','2480','2481','2484','2485','2486','2487','2488') 
						THEN 'GOLDCOAST'
					WHEN @postcode In ('4207','4208','4209','4210','4211','4212','4213','4214','4215','4216','4217','4218','4219','4220','4221','4222','4223','4224','4225','4226','4227','4228','4229','4230','4272','4287','4270','4271','4275') 
						THEN 'GOLDCOAST'  
					WHEN @postcode like '2%' THEN 'SYDNEY'
					WHEN (@postcode like '3%' Or @postcode like '7%') THEN 'MELBOURNE'
					WHEN @postcode like '4%' THEN 'BRISBANE' 
					WHEN (@postcode like '5%' Or @postcode like '8%' OR  @postcode like '08%') THEN 'ADELAIDE'
					WHEN @postcode like '6%' THEN 'PERTH'
				ELSE ''  END
	RETURN @ret;
	
END
GO
