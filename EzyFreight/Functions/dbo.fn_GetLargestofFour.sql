SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE function [dbo].[fn_GetLargestofFour] (@WtValue1 decimal(12,3),@WtValue2 decimal(12,3),@VolValue3 decimal(12,3),@VolValue4 decimal(12,3))

returns decimal(12,2)

as

begin

	Declare @dReturn as decimal(12,3)

	Declare @WtValueDec1  decimal(12,3) =  Convert(decimal(12,3),Isnull(@WtValue1,0));

	Declare @WtValueDec2  decimal(12,3) =  Convert(decimal(12,3),Isnull(@WtValue2,0));

	Declare @VolValueDec1  decimal(12,3)=  Convert(decimal(12,3),Isnull(@VolValue3,0))*250.00;

	Declare @VolValueDec2  decimal(12,3)=  Convert(decimal(12,3),Isnull(@VolValue4,0))*250.00;



	Declare @Greater1  decimal(12,3)=  Case when  @WtValueDec1> @WtValueDec2 Then @WtValueDec1 else @WtValueDec2 End

	Declare @Greater2  decimal(12,3)=  Case when  @VolValueDec1> @VolValueDec2 Then @VolValueDec1 else @VolValueDec2 End





	Return Case when @Greater1>@Greater2 THEN @Greater1 ELSE @Greater2 END

end








GO
