SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.fn_XmlToJson_Get
(
@XmlData XML
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
RETURN
 (SELECT STUFF( 
  (SELECT
   *
   FROM 
    (SELECT
      ',{'+ 
        STUFF(
          (SELECT
            ','+char(13)+'"'+
             COALESCE(b.c.value('local-name(.)', 'NVARCHAR(MAX)'),'')+'":"'+ b.c.value('text()[1]','NVARCHAR(MAX)') +'"'
           FROM x.a.nodes('*') b(c) FOR XML PATH(''),TYPE).value('(./text())[1]','NVARCHAR(MAX)'),1,1,'')
       +'}'
     FROM @XmlData.nodes('/root/*') x(a)) JSON(theLine) 
    FOR XML PATH(''),TYPE).value('.','NVARCHAR(MAX)' )
   ,1,1,''))
END
GO
