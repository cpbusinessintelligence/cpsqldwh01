SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[MaintenancePlan_EzyFreight]
As
Begin
	--REORGANIZE

	ALTER INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] REORGANIZE  



	ALTER INDEX [PK__aspnet_M__1788CC4D037D7AA8] ON [dbo].[aspnet_Membership] REORGANIZE  



	ALTER INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] REORGANIZE  



	ALTER INDEX [PK__aspnet_U__1788CC4D082037AE] ON [dbo].[aspnet_Users] REORGANIZE  



	ALTER INDEX [PK_EventLogMaster] ON [dbo].[EventLog] REORGANIZE  



	ALTER INDEX [PK_EventLogConfig] ON [dbo].[EventLogConfig] REORGANIZE  



	ALTER INDEX [PK_EventLogTypes] ON [dbo].[EventLogTypes] REORGANIZE  



	ALTER INDEX [PK_Parameters] ON [dbo].[Parameters] REORGANIZE  



	ALTER INDEX [PK__Relation__3214EC07DAC8130A] ON [dbo].[RelationshipDefinitions] REORGANIZE  



	ALTER INDEX [IX_RoleGroupName] ON [dbo].[RoleGroups] REORGANIZE  



	ALTER INDEX [PK_RoleGroups] ON [dbo].[RoleGroups] REORGANIZE  



	ALTER INDEX [IX_RoleName] ON [dbo].[Roles] REORGANIZE  



	ALTER INDEX [PK_Roles] ON [dbo].[Roles] REORGANIZE  



	ALTER INDEX [PK_tblActivityLogging] ON [dbo].[tblActivityLogging] REORGANIZE  



	ALTER INDEX [PK_tblAddress] ON [dbo].[tblAddress] REORGANIZE  



	ALTER INDEX [PK_tblAPIEnquiry] ON [dbo].[tblAPIEnquiry] REORGANIZE  



	ALTER INDEX [PK_tblAPIEnquiryNotes] ON [dbo].[tblAPIEnquiryNotes] REORGANIZE  



	ALTER INDEX [PK_tblAPIEnquiryStatus] ON [dbo].[tblAPIEnquiryStatus] REORGANIZE  



	ALTER INDEX [PK_tblBooking] ON [dbo].[tblBooking] REORGANIZE  



	ALTER INDEX [PK_tblCancelledConsignment] ON [dbo].[tblCancelledConsignment] REORGANIZE  



	ALTER INDEX [PK_tblClient] ON [dbo].[tblClient] REORGANIZE  



	ALTER INDEX [PK_tblClientCPPLServiceAccess] ON [dbo].[tblClientCPPLServiceAccess] REORGANIZE  



	ALTER INDEX [PK_tblCommunity] ON [dbo].[tblCommunity] REORGANIZE  



	ALTER INDEX [IX_tblCompany_1] ON [dbo].[tblCompany] REORGANIZE  



	ALTER INDEX [PK_tblCompany] ON [dbo].[tblCompany] REORGANIZE  



	ALTER INDEX [PK_tblCompanyUsers] ON [dbo].[tblCompanyUsers] REORGANIZE  



	ALTER INDEX [IX_tblConsignment_ConsignmentCode] ON [dbo].[tblConsignment] REORGANIZE  



	ALTER INDEX [PK_tblConsignment] ON [dbo].[tblConsignment] REORGANIZE  



	ALTER INDEX [TUC_tblConsignment_1] ON [dbo].[tblConsignment] REORGANIZE  



	ALTER INDEX [PK_tblConsignmentImage] ON [dbo].[tblConsignmentImage] REORGANIZE  



	ALTER INDEX [PK_tblConsignmentService] ON [dbo].[tblConsignmentService] REORGANIZE  



	ALTER INDEX [PK_tblConsignmentStaging] ON [dbo].[tblConsignmentStaging] REORGANIZE  



	ALTER INDEX [PK_TBLContactUs] ON [dbo].[tblContactUs] REORGANIZE  



	ALTER INDEX [PK_tblCPPLServices] ON [dbo].[tblCPPLServices] REORGANIZE  



	ALTER INDEX [PK_tblCustomDeclaration] ON [dbo].[tblCustomDeclaration] REORGANIZE  



	ALTER INDEX [PK_tblCustomerRate] ON [dbo].[tblCustomerRate] REORGANIZE  



	ALTER INDEX [PK_tblDHLBarCodeImage] ON [dbo].[tblDHLBarCodeImage] REORGANIZE  



	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REORGANIZE  



	ALTER INDEX [IX_tblEzyNetCustomer] ON [dbo].[tblEzyNetCustomer] REORGANIZE  



	ALTER INDEX [PK_tblEzyNetCustomer] ON [dbo].[tblEzyNetCustomer] REORGANIZE  



	ALTER INDEX [PK_tblFranchise] ON [dbo].[tblFranchise] REORGANIZE  



	ALTER INDEX [PK_tblFranchiseList] ON [dbo].[tblFranchiseList] REORGANIZE  



	ALTER INDEX [PK_tblFreightType] ON [dbo].[tblFreightType] REORGANIZE  



	ALTER INDEX [PK_tblInvoice] ON [dbo].[tblInvoice] REORGANIZE  



	ALTER INDEX [PK_tblInvoiceImage] ON [dbo].[tblInvoiceImage] REORGANIZE  



	ALTER INDEX [PK_tblItemLabel] ON [dbo].[tblItemLabel] REORGANIZE  



	ALTER INDEX [PK_tblItemLabelImage] ON [dbo].[tblItemLabelImage] REORGANIZE  



	ALTER INDEX [PK_tblnsuranceCatery] ON [dbo].[tblnsuranceCatery] REORGANIZE  



	ALTER INDEX [TUC_tblnsuranceCatery_InsuranceCide] ON [dbo].[tblnsuranceCatery] REORGANIZE  



	ALTER INDEX [PK_tblOpenAccount] ON [dbo].[tblOpenAccount] REORGANIZE  



	ALTER INDEX [PK_tblQuote] ON [dbo].[tblQuote] REORGANIZE  



	ALTER INDEX [PK_tblRateCard] ON [dbo].[tblRateCard] REORGANIZE  



	ALTER INDEX [TUC_tblRateCard_1] ON [dbo].[tblRateCard] REORGANIZE  



	ALTER INDEX [PK_tblRateCardDetail] ON [dbo].[tblRateCardDetail] REORGANIZE  



	ALTER INDEX [PK_tblRecharge] ON [dbo].[tblRecharge] REORGANIZE  



	ALTER INDEX [PK_tblRedelivery] ON [dbo].[tblRedelivery] REORGANIZE  



	ALTER INDEX [PK_tblRedirectedConsignment] ON [dbo].[tblRedirectedConsignment] REORGANIZE  



	ALTER INDEX [PK_tblRedirectedItemLabel] ON [dbo].[tblRedirectedItemLabel] REORGANIZE  



	ALTER INDEX [PK_tblRefund] ON [dbo].[tblRefund] REORGANIZE  



	ALTER INDEX [PK_tblSalesOrder] ON [dbo].[tblSalesOrder] REORGANIZE  



	ALTER INDEX [PK_tblSalesOrderDetail] ON [dbo].[tblSalesOrderDetail] REORGANIZE  



	ALTER INDEX [PK_tblService] ON [dbo].[tblService] REORGANIZE  



	ALTER INDEX [PK_tblState] ON [dbo].[tblState] REORGANIZE  



	ALTER INDEX [PK_tblStatus] ON [dbo].[tblStatus] REORGANIZE  



	ALTER INDEX [PK_tblTracking] ON [dbo].[tblTracking] REORGANIZE  



	ALTER INDEX [PK_tblTrackingStaging] ON [dbo].[tblTrackingStaging] REORGANIZE  



	ALTER INDEX [PK_tblZone] ON [dbo].[tblZone] REORGANIZE  



	ALTER INDEX [TUC_tblZone_1] ON [dbo].[tblZone] REORGANIZE  



	ALTER INDEX [PK_UserRoles] ON [dbo].[UserRoles] REORGANIZE  



	ALTER INDEX [IX_Users] ON [dbo].[Users] REORGANIZE  



	ALTER INDEX [PK_Users] ON [dbo].[Users] REORGANIZE  



	---REBUILD



	ALTER INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] REBUILD  



	ALTER INDEX [PK__aspnet_M__1788CC4D037D7AA8] ON [dbo].[aspnet_Membership] REBUILD  



	ALTER INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] REBUILD  



	ALTER INDEX [PK__aspnet_U__1788CC4D082037AE] ON [dbo].[aspnet_Users] REBUILD  



	ALTER INDEX [PK_EventLogMaster] ON [dbo].[EventLog] REBUILD  



	ALTER INDEX [PK_EventLogConfig] ON [dbo].[EventLogConfig] REBUILD  



	ALTER INDEX [PK_EventLogTypes] ON [dbo].[EventLogTypes] REBUILD  



	ALTER INDEX [PK_Parameters] ON [dbo].[Parameters] REBUILD  



	ALTER INDEX [PK__Relation__3214EC07DAC8130A] ON [dbo].[RelationshipDefinitions] REBUILD  



	ALTER INDEX [IX_RoleGroupName] ON [dbo].[RoleGroups] REBUILD  



	ALTER INDEX [PK_RoleGroups] ON [dbo].[RoleGroups] REBUILD  



	ALTER INDEX [IX_RoleName] ON [dbo].[Roles] REBUILD  



	ALTER INDEX [PK_Roles] ON [dbo].[Roles] REBUILD  



	ALTER INDEX [PK_tblActivityLogging] ON [dbo].[tblActivityLogging] REBUILD  



	ALTER INDEX [PK_tblAddress] ON [dbo].[tblAddress] REBUILD  



	ALTER INDEX [PK_tblAPIEnquiry] ON [dbo].[tblAPIEnquiry] REBUILD  



	ALTER INDEX [PK_tblAPIEnquiryNotes] ON [dbo].[tblAPIEnquiryNotes] REBUILD  



	ALTER INDEX [PK_tblAPIEnquiryStatus] ON [dbo].[tblAPIEnquiryStatus] REBUILD  



	ALTER INDEX [PK_tblBooking] ON [dbo].[tblBooking] REBUILD  



	ALTER INDEX [PK_tblCancelledConsignment] ON [dbo].[tblCancelledConsignment] REBUILD  



	ALTER INDEX [PK_tblClient] ON [dbo].[tblClient] REBUILD  



	ALTER INDEX [PK_tblClientCPPLServiceAccess] ON [dbo].[tblClientCPPLServiceAccess] REBUILD  



	ALTER INDEX [PK_tblCommunity] ON [dbo].[tblCommunity] REBUILD  



	ALTER INDEX [IX_tblCompany_1] ON [dbo].[tblCompany] REBUILD  



	ALTER INDEX [PK_tblCompany] ON [dbo].[tblCompany] REBUILD  



	ALTER INDEX [PK_tblCompanyUsers] ON [dbo].[tblCompanyUsers] REBUILD  



	ALTER INDEX [IX_tblConsignment_ConsignmentCode] ON [dbo].[tblConsignment] REBUILD  



	ALTER INDEX [PK_tblConsignment] ON [dbo].[tblConsignment] REBUILD  



	ALTER INDEX [TUC_tblConsignment_1] ON [dbo].[tblConsignment] REBUILD  



	ALTER INDEX [PK_tblConsignmentImage] ON [dbo].[tblConsignmentImage] REBUILD  



	ALTER INDEX [PK_tblConsignmentService] ON [dbo].[tblConsignmentService] REBUILD  



	ALTER INDEX [PK_tblConsignmentStaging] ON [dbo].[tblConsignmentStaging] REBUILD  



	ALTER INDEX [PK_TBLContactUs] ON [dbo].[tblContactUs] REBUILD  



	ALTER INDEX [PK_tblCPPLServices] ON [dbo].[tblCPPLServices] REBUILD  



	ALTER INDEX [PK_tblCustomDeclaration] ON [dbo].[tblCustomDeclaration] REBUILD  



	ALTER INDEX [PK_tblCustomerRate] ON [dbo].[tblCustomerRate] REBUILD  



	ALTER INDEX [PK_tblDHLBarCodeImage] ON [dbo].[tblDHLBarCodeImage] REBUILD  



	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REBUILD  



	ALTER INDEX [IX_tblEzyNetCustomer] ON [dbo].[tblEzyNetCustomer] REBUILD  



	ALTER INDEX [PK_tblEzyNetCustomer] ON [dbo].[tblEzyNetCustomer] REBUILD  



	ALTER INDEX [PK_tblFranchise] ON [dbo].[tblFranchise] REBUILD  



	ALTER INDEX [PK_tblFranchiseList] ON [dbo].[tblFranchiseList] REBUILD  



	ALTER INDEX [PK_tblFreightType] ON [dbo].[tblFreightType] REBUILD  



	ALTER INDEX [PK_tblInvoice] ON [dbo].[tblInvoice] REBUILD  



	ALTER INDEX [PK_tblInvoiceImage] ON [dbo].[tblInvoiceImage] REBUILD  



	ALTER INDEX [PK_tblItemLabel] ON [dbo].[tblItemLabel] REBUILD  



	ALTER INDEX [PK_tblItemLabelImage] ON [dbo].[tblItemLabelImage] REBUILD  



	ALTER INDEX [PK_tblnsuranceCatery] ON [dbo].[tblnsuranceCatery] REBUILD  



	ALTER INDEX [TUC_tblnsuranceCatery_InsuranceCide] ON [dbo].[tblnsuranceCatery] REBUILD  



	ALTER INDEX [PK_tblOpenAccount] ON [dbo].[tblOpenAccount] REBUILD  



	ALTER INDEX [PK_tblQuote] ON [dbo].[tblQuote] REBUILD  



	ALTER INDEX [PK_tblRateCard] ON [dbo].[tblRateCard] REBUILD  



	ALTER INDEX [TUC_tblRateCard_1] ON [dbo].[tblRateCard] REBUILD  



	ALTER INDEX [PK_tblRateCardDetail] ON [dbo].[tblRateCardDetail] REBUILD  



	ALTER INDEX [PK_tblRecharge] ON [dbo].[tblRecharge] REBUILD  



	ALTER INDEX [PK_tblRedelivery] ON [dbo].[tblRedelivery] REBUILD  



	ALTER INDEX [PK_tblRedirectedConsignment] ON [dbo].[tblRedirectedConsignment] REBUILD  



	ALTER INDEX [PK_tblRedirectedItemLabel] ON [dbo].[tblRedirectedItemLabel] REBUILD  



	ALTER INDEX [PK_tblRefund] ON [dbo].[tblRefund] REBUILD  



	ALTER INDEX [PK_tblSalesOrder] ON [dbo].[tblSalesOrder] REBUILD  



	ALTER INDEX [PK_tblSalesOrderDetail] ON [dbo].[tblSalesOrderDetail] REBUILD  



	ALTER INDEX [PK_tblService] ON [dbo].[tblService] REBUILD  



	ALTER INDEX [PK_tblState] ON [dbo].[tblState] REBUILD  



	ALTER INDEX [PK_tblStatus] ON [dbo].[tblStatus] REBUILD  



	ALTER INDEX [PK_tblTracking] ON [dbo].[tblTracking] REBUILD  



	ALTER INDEX [PK_tblTrackingStaging] ON [dbo].[tblTrackingStaging] REBUILD  



	ALTER INDEX [PK_tblZone] ON [dbo].[tblZone] REBUILD  



	ALTER INDEX [TUC_tblZone_1] ON [dbo].[tblZone] REBUILD  



	ALTER INDEX [PK_UserRoles] ON [dbo].[UserRoles] REBUILD  



	ALTER INDEX [IX_Users] ON [dbo].[Users] REBUILD  



	ALTER INDEX [PK_Users] ON [dbo].[Users] REBUILD  

	---Update Stat


	UPDATE STATISTICS [dbo].[AgentParam] 
	



	UPDATE STATISTICS [dbo].[Anand] 
	



	UPDATE STATISTICS [dbo].[aspnet_Membership] 
	



	UPDATE STATISTICS [dbo].[aspnet_Users] 
	



	UPDATE STATISTICS [dbo].[AssignShipmentEzy2ShipResponse] 
	



	UPDATE STATISTICS [dbo].[AssignShipmentNZResponse] 
	



	UPDATE STATISTICS [dbo].[BillingStaging] 
	



	UPDATE STATISTICS [dbo].[BillingStaging1] 
	



	UPDATE STATISTICS [dbo].[BillingStaging2] 
	



	UPDATE STATISTICS [dbo].[BillingTemp] 
	



	UPDATE STATISTICS [dbo].[BillingWeb] 
	



	UPDATE STATISTICS [dbo].[ConsumerDropOffApiResponse] 
	



	UPDATE STATISTICS [dbo].[CreateShipmentEzy2ShipResponse] 
	



	UPDATE STATISTICS [dbo].[DHLLocationcodes] 
	



	UPDATE STATISTICS [dbo].[EventLog] 
	



	UPDATE STATISTICS [dbo].[EventLogConfig] 
	



	UPDATE STATISTICS [dbo].[EventLogTypes] 
	



	UPDATE STATISTICS [dbo].[Example_File] 
	



	UPDATE STATISTICS [dbo].[EzyParcelAssignShipments] 
	



	UPDATE STATISTICS [dbo].[EzyParcelAssignShipmentsStaging] 
	



	UPDATE STATISTICS [dbo].[EzysendtoEzyTrakFilename] 
	



	UPDATE STATISTICS [dbo].[EzysendtoEzyTrakHeaderFile] 
	



	UPDATE STATISTICS [dbo].[GetShipmentLabelEzy2ShipResponse] 
	



	UPDATE STATISTICS [dbo].[HubbedWikiTemp] 
	



	UPDATE STATISTICS [dbo].[HubbedWikiTemp1] 
	



	UPDATE STATISTICS [dbo].[LianaRedemptionOctober] 
	



	UPDATE STATISTICS [dbo].[LianaRedemptionOctober_2510] 
	



	UPDATE STATISTICS [dbo].[MailchimpStaging] 
	



	UPDATE STATISTICS [dbo].[NumberofActiveConnections] 
	



	UPDATE STATISTICS [dbo].[NZPost_staging] 
	



	UPDATE STATISTICS [dbo].[Parameters] 
	



	UPDATE STATISTICS [dbo].[PaymentReferenceStaging] 
	



	UPDATE STATISTICS [dbo].[ProntoDetails] 
	



	UPDATE STATISTICS [dbo].[Redemp_FinalList] 
	



	UPDATE STATISTICS [dbo].[RelationshipDefinitions] 
	



	UPDATE STATISTICS [dbo].[response] 
	



	UPDATE STATISTICS [dbo].[RoleGroups] 
	



	UPDATE STATISTICS [dbo].[Roles] 
	



	UPDATE STATISTICS [dbo].[SSIParameter] 
	



	UPDATE STATISTICS [dbo].[tblActivityLogging] 
	



	UPDATE STATISTICS [dbo].[tblAddress] 
	



	UPDATE STATISTICS [dbo].[tblAddress_bjkup_AH] 
	



	UPDATE STATISTICS [dbo].[tblAgentExecLog] 
	



	UPDATE STATISTICS [dbo].[tblAPIEnquiry] 
	



	UPDATE STATISTICS [dbo].[tblAPIEnquiryNotes] 
	



	UPDATE STATISTICS [dbo].[tblAPIEnquiryStatus] 
	



	UPDATE STATISTICS [dbo].[tblBooking] 
	



	UPDATE STATISTICS [dbo].[tblBookingFromHubbed] 
	



	UPDATE STATISTICS [dbo].[tblBranchAddresses] 
	



	UPDATE STATISTICS [dbo].[tblBranchAddresses_backup] 
	



	UPDATE STATISTICS [dbo].[tblCancelledConsignment] 
	



	UPDATE STATISTICS [dbo].[tblClient] 
	



	UPDATE STATISTICS [dbo].[tblClientCPPLServiceAccess] 
	



	UPDATE STATISTICS [dbo].[tblCommunity] 
	



	UPDATE STATISTICS [dbo].[tblCompany] 
	



	UPDATE STATISTICS [dbo].[tblCompany_SS12042019] 
	



	UPDATE STATISTICS [dbo].[tblCompanyTestUser] 
	



	UPDATE STATISTICS [dbo].[tblCompanyUsers] 
	



	UPDATE STATISTICS [dbo].[tblConsignment] 
	



	UPDATE STATISTICS [dbo].[tblConsignment_bkup_AH] 
	



	UPDATE STATISTICS [dbo].[tblConsignment_ExceptionTest] 
	



	UPDATE STATISTICS [dbo].[tblConsignment_Seko] 
	



	UPDATE STATISTICS [dbo].[tblConsignment_Seko1] 
	



	UPDATE STATISTICS [dbo].[tblConsignmentImage] 
	



	UPDATE STATISTICS [dbo].[tblConsignmentService] 
	



	UPDATE STATISTICS [dbo].[tblConsignmentStaging] 
	



	UPDATE STATISTICS [dbo].[tblContactUs] 
	



	UPDATE STATISTICS [dbo].[tblCPPLServices] 
	



	UPDATE STATISTICS [dbo].[tblCustomDeclaration] 
	



	UPDATE STATISTICS [dbo].[tblCustomerRate] 
	



	UPDATE STATISTICS [dbo].[tblDHLBarCodeImage] 
	



	UPDATE STATISTICS [dbo].[tblDHLShiptoAddresses] 
	



	UPDATE STATISTICS [dbo].[tblDHLStaging] 
	



	UPDATE STATISTICS [dbo].[tblEmailNotification] 
	



	UPDATE STATISTICS [dbo].[tblErrorLog] 
	



	UPDATE STATISTICS [dbo].[tblEzyNetCustomer] 
	



	UPDATE STATISTICS [dbo].[tblFranchise] 
	



	UPDATE STATISTICS [dbo].[tblFranchiseList] 
	



	UPDATE STATISTICS [dbo].[tblFreightType] 
	



	UPDATE STATISTICS [dbo].[tblInsurance] 
	



	UPDATE STATISTICS [dbo].[tblInsuranceCatery] 
	



	UPDATE STATISTICS [dbo].[tblInvoice] 
	



	UPDATE STATISTICS [dbo].[tblInvoiceImage] 
	



	UPDATE STATISTICS [dbo].[tblItemLabel] 
	



	UPDATE STATISTICS [dbo].[tblItemLabel_bkup_AH] 
	



	UPDATE STATISTICS [dbo].[tblItemLabelImage] 
	



	UPDATE STATISTICS [dbo].[tblnsuranceCatery] 
	



	UPDATE STATISTICS [dbo].[tblOpenAccount] 
	



	UPDATE STATISTICS [dbo].[tblQuote] 
	



	UPDATE STATISTICS [dbo].[tblRateCard] 
	



	UPDATE STATISTICS [dbo].[tblRateCardDetail] 
	



	UPDATE STATISTICS [dbo].[tblRecharge] 
	



	UPDATE STATISTICS [dbo].[tblRedelivery] 
	



	UPDATE STATISTICS [dbo].[tblRedirectedConsignment] 
	



	UPDATE STATISTICS [dbo].[tblRedirectedItemLabel] 
	



	UPDATE STATISTICS [dbo].[tblRefund] 
	



	UPDATE STATISTICS [dbo].[tblRefund_Archive] 
	



	UPDATE STATISTICS [dbo].[tblRefund_backupHB07082018] 
	



	UPDATE STATISTICS [dbo].[tblSalesOrder] 
	



	UPDATE STATISTICS [dbo].[tblSalesOrder_Archive] 
	



	UPDATE STATISTICS [dbo].[tblSalesOrderDetail] 
	



	UPDATE STATISTICS [dbo].[tblSalesOrderDetail_Archive] 
	



	UPDATE STATISTICS [dbo].[tblService] 
	



	UPDATE STATISTICS [dbo].[tblState] 
	



	UPDATE STATISTICS [dbo].[tblStatus] 
	



	UPDATE STATISTICS [dbo].[tblTracking] 
	



	UPDATE STATISTICS [dbo].[tblTrackingStaging] 
	



	UPDATE STATISTICS [dbo].[TblWebsiteFailedValidationLog] 
	



	UPDATE STATISTICS [dbo].[TblWebsiteFailedValidationLog_Raj] 
	



	UPDATE STATISTICS [dbo].[TblWebsiteFailedValidationLog_Test] 
	



	UPDATE STATISTICS [dbo].[tblZone] 
	



	UPDATE STATISTICS [dbo].[tempLabelNumber] 
	



	UPDATE STATISTICS [dbo].[UserRoles] 
	



	UPDATE STATISTICS [dbo].[Users] 
	



	UPDATE STATISTICS [dbo].[USPostcodeChart] 
	



	UPDATE STATISTICS [dbo].[WebBil] 
	



	UPDATE STATISTICS [dbo].[website labels SNK] 
	



	UPDATE STATISTICS [dbo].[Website_Validations] 
	



	UPDATE STATISTICS [dbo].[WebsiteActivityLog] 
	

	--Shrink DB

	DBCC SHRINKDATABASE(N'EzyFreight')

End
GO
