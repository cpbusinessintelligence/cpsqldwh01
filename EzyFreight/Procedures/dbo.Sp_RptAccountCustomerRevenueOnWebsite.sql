SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptAccountCustomerRevenueOnWebsite] 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT Convert(Date, [CreatedDateTime]) as [Consignment Created Date]
      ,[ConsignmentCode]
         , Case  when ClientCode is null Then 'Website' Else 'API' END as RevenueType
          ,[RateCardID] as RateCardCode
      ,Convert(Varchar(50),'')  as ProductType
         , UserID
         , Convert(Varchar(50),'')  as AccountNumber
         , Convert(Varchar(50),'')  as CompanyName
         , Convert(Varchar(50),'')  as ProntoBilledDate
         , Convert(Varchar(50),'')  as BilledFreightCharge
         , Convert(Varchar(50),'')  as BilledFuelSurCharge
         , Convert(Varchar(50),'')  as BilledTransportCharge
           
 
  into #temp1  
  FROM [EzyFreight].[dbo].[tblConsignment]
  where IsAccountCustomer =1
  and [CreatedDateTime] >= '2017-03-01 00:02:47.230' and isprocessed =1
  and [CreatedDateTime] <= '2018-03-31 23:53:44.537'
 
  --Drop Table #Temp1
 
  Update #Temp1 Set AccountNumber =C.AccountNumber, CompanyName = C.CompanyName
        From #Temp1 T join [dbo].[tblCompanyUsers] U on T.UserID= U.UserID
 
                                                   Join [dbo].[tblCompany] C on U.CompanyID = C.CompanyID
 
 
 
 
Delete  from #Temp1 where UserID is null
 
Delete  from #Temp1 where AccountNumber =  'WD00000006'
 
Update #Temp1 SET ProductType =P.pc_name  From #Temp1 T Join [CpplEDI].[dbo].[pricecodes] P on T.RateCardCode =P.pc_code
 
Update #Temp1 SET ProductType =  'International Express' WHere RateCardCode like 'EX%'
Update #Temp1 SET ProductType =  'International Saver' WHere RateCardCode like 'SA%'
 
Update #Temp1 SET ProntoBilledDate =B.AccountingDate, BilledFreightCharge =  B.BilledFreightCharge, BilledFuelSurCharge = B.BilledFuelSurcharge, BilledTransportCharge = B.BilledTransportCharge
 
    From #Temp1 T join Pronto.[dbo].[ProntoBilling] B on T.[ConsignmentCode] =  B.ConsignmentReference

 Update #temp1  Set ProntoBilledDate =' Not yet Billed', BilledFreightCharge = ' Not yet Billed', BilledFuelSurCharge =' Not yet Billed', BilledTransportCharge = ' Not yet Billed'
Where ProntoBilledDate = ''

select t.*,p.Territory from #temp1 t join pronto.dbo.ProntoDebtor p on t.AccountNumber=p.Accountcode

  
 
END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptAccountCustomerRevenueOnWebsite]
	TO [ReportUser]
GO
