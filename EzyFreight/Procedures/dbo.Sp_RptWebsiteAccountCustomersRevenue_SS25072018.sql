SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptWebsiteAccountCustomersRevenue_SS25072018]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Convert(Date, [CreatedDateTime]) as [Consignment Created Date]
      ,[ConsignmentCode]
         , Case  when ClientCode is null Then 'Website' Else 'API' END as RevenueType
          ,[RateCardID] as RateCardCode
      ,Convert(Varchar(50),'')  as ProductType
         , UserID
		 , PickupID
		 , DestinationID
		 , IsInternational
         , Convert(Varchar(50),'')  as AccountNumber
         , Convert(Varchar(50),'')  as CompanyName

		 ,Convert(Varchar(50),'')  as ServiceName
		 ,Convert(Varchar(50),'')  as PickupState
		 ,Convert(Varchar(50),'')  as DeliveryState


         , Convert(Varchar(50),'')  as ProntoBilledDate
         , Convert(Varchar(50),'')  as BilledFreightCharge
         , Convert(Varchar(50),'')  as BilledFuelSurCharge
         , Convert(Varchar(50),'')  as BilledTransportCharge
          , Convert(Varchar(50),'')  as Territory
  into #temp1  
  FROM [EzyFreight].[dbo].[tblConsignment] with (nolock)
  where IsAccountCustomer =1
  and [CreatedDateTime] >= '2016-03-01 00:53:44.537' and isprocessed =1
  and [CreatedDateTime] <= '2018-03-31 23:53:44.537'
  and 
   [RateCardID] not in ('P0','L66','P10','X33','Y30','I77','L77','I22','X0A','I10','I66','L44','L22','L11'

,'I44','P5','Y31','P3','L55','Y33','X31','P1','I55','X5A','P20','X35','X30','Y35'
,'L11','P25','Y0A','X3A','I50','I33','X1A','Y5A','Y3A','Y1A'
)

  --Drop Table #Temp1

  Update #Temp1 Set AccountNumber =C.AccountNumber, CompanyName = C.CompanyName
        From #Temp1 T join [dbo].[tblCompanyUsers] U on T.UserID= U.UserID

                                                   Join [dbo].[tblCompany] C on U.CompanyID = C.CompanyID


--Select distinct RateCardCode from #Temp1 where RateCardCode not in ('P0','L66','P10','X33','Y30','I77','L77','I22','X0A','I10'

--,'I44','P5','Y31','P3','L55','Y33','X31','P1','I55','X5A','P20','X35','X30','Y35'
--,'L11','P25','Y0A','X3A','I50','I33','X1A','',''
--)


Delete  from #Temp1 where UserID is null

Delete  from #Temp1 where AccountNumber =  'WD00000006'

Delete from #temp1 where CompanyName = 'Sample Customer'

Update #Temp1 SET ProductType =   case 
when RateCardCode in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' 
when RateCardCode in ('SDC','PDC') then 'Australian City Express Same Day' 
when RateCardCode in ('REC','PEC') then 'Domestic Saver' 
when  RateCardCode like 'EXP%' then 'International Priority' 
when RateCardCode like 'SAV%' then 'International Saver' 
when RateCardCode in ('SGE','GEC') then 'Gold Domestic'  
when RateCardCode in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  
when RateCardCode in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' 
when  RateCardCode in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else 'Others' end 





Update #Temp1 SET ProntoBilledDate =B.AccountingDate, BilledFreightCharge =  B.BilledFreightCharge, BilledFuelSurCharge = B.BilledFuelSurcharge, BilledTransportCharge = B.BilledTransportCharge

    From #Temp1 T join Pronto.[dbo].[ProntoBilling] B on T.[ConsignmentCode] =  B.ConsignmentReference

 Update #temp1  Set ProntoBilledDate =' Not yet Billed', BilledFreightCharge = ' Not yet Billed', BilledFuelSurCharge =' Not yet Billed', BilledTransportCharge = ' Not yet Billed'
Where ProntoBilledDate = ''

  Update #Temp1 SET Territory = D.Territory   From #Temp1 T Join  Pronto.dbo.ProntoDebtor D on T.AccountNumber= D.AccountCode

  Update #temp1 SET ServiceName = Case when Addr1.Country = 'Australia' and IsInternational = 1 Then 'Export' When Addr1.Country <> 'Australia' and IsInternational = 1 Then 'Import'  Else Null End,
                    PickupState = Case when Addr1.StateName is Null Then S.StateName  else Addr1.StateName end,
					DeliveryState = Case when Addr2.StateName is Null Then St.StateName  else Addr2.StateName end
					From #Temp1 Con
                    LEFT join tblAddress Addr1
on (Con.PickupID = Addr1.AddressID)
LEFT join tblAddress Addr2
on (Con.DestinationID = Addr2.AddressID) 
Left join tblState S
on (Addr1.StateID = S.StateID)
Left join tblState St
on (Addr2.StateID = S.StateID)

Select * from #temp1

END


GO
