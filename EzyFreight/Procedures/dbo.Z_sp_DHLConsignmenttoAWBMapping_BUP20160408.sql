SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Z_sp_DHLConsignmenttoAWBMapping_BUP20160408] as

begin


     --'=====================================================================
    --' CP -Stored Procedure - sp_DHLConsignmenttoAWBMapping
    --' ---------------------------
    --' Purpose: DHLConsignmenttoAWBMapping-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 20 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 20/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Select convert(varchar(50),[AWBNumber]) AS AWBNumber,
	   c.Consignmentcode,
	   l.LabelNumber,
	   [StatusDateTime],
       [ScanEvent],
       [ContractorCode],
       [ExceptionReason]
	   from [dbo].[tblDHLStaging] s left join [dbo].[tblDHLBarCodeImage] b on b.AWBCode=s.AWBNumber
	                                left join [dbo].[tblConsignment] c on c.ConsignmentID=b.ConsignmentID
									left join [dbo].[tblItemLabel] l on l.ConsignmentID=c.ConsignmentID
									 and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
end
GO
