SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Z_sp_ExportConsignmentDetailsfromEzySendtoEzyTrak] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_ExportConsignmentDetailsfromEzySendtoEzyTrak
    --' ---------------------------
    --' Purpose: To get all consignment details for  consignments Going To NZ-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 13 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 13/08/2015    AB      1.00    Created the procedure                             --AB20150813

    --'=====================================================================


--Truncate table EzysendtoEzyTrakFilename
--Insert into EzysendtoEzyTrakFilename
--values('IDCPDOM'+convert(varchar(20),year(getdate()))+right('00'+convert(varchar(20),month(getdate())),2)+right('00'+convert(varchar(20),day(getdate())),2)+convert(varchar(8),replace(replace(convert(varchar(20),cast(getdate() as time)),':',''),'.','')))

declare @Varcharop varchar(max)

set @varcharop=(SELECT (Select  'H' As RecordType,
        [FileName],
		convert(date,getdate()) as ExtractDate,
		convert(time,getdate()) as ExtractTime,
		'E2S' as Interfacesystem
		From EzysendtoEzyTrakFilename
FOR 
XML PATH('FileHeader'),
TYPE
),
(SELECT 
(SELECT top 2  'D' As RecordType,
	           'PO' as CustomerIndicator,
			   '123' as CustomerAccountNumber,
			   l.LabelNumber as SPItemNumber,
			   '' as SenderReferenceNumber,
			   c.RateCardId as ServiceType,
			   isnull(ReasonforExport,'O') as ItemType,
			   '' as StockType ,
	           c.TotalWeight as ItemWeight,
			   c.NetSubTotal as ItemDeclaredValue,
			   'AUD' as ItemDeclaredValueCurrency,
			   isnull(a.CountryCode,'CP') as SenderCountry,
			   isnull(a1.countrycode,'CP') as Destinationcountry,
			   a1.FirstName+' '+a1.LastName as RecipientName,
               replace(a1.address1,',','') as RecipientAddress1,
               replace(a1.address2,',','') as RecipientAddress2,
			   '' as RecipientAddress3,
			   '' as RecipientAddress4,
			   a1.suburb as RecipientCity,
			   a1.Postcode as RecipientPostalCode,
			   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as Recipientstate,
			   replace(a1.phone,' ','') as RecipientContactNumber,
			   a1.FirstName+' '+a1.LastName  as RecipientContactPerson,
			   a1.Email as Recipientemail,
			   '' as RecipientIdentificationNumber,
               a.FirstName+' '+a.LastName as SenderName,
               replace(a.address1,',','') as SenderAddress1,
               replace(a.address2,',','') as SenderAddress2,
			   '' as SenderAddress3,
			   '' as SenderAddress4,
			   a.suburb as SenderCity,
			   a.Postcode as SenderPostalCode,
			   isnull(s.StateCode,isnull(a.statename,'Unknown')) as SenderState,
			   replace(a.phone,' ','') as SenderContactNumber,
			   a.FirstName+' '+a.LastName as SenderContactPerson,
			   a.Email as SenderEmailAddress,
			   '' as Undeliverable,
			   l.Length as Length,
			   l.width as Breadth,
			   l.height as Height,
			   l.CubicWeight as VolumetricWeight,
			   'N' as InsuranceCategory,
			   c.[NoOfItems] as NumberOfPackages,
			   0 as InsuredValue,
			   0 as InsurancePremium,
			   ReasonforExport as ContentCategory,
			  case when ReasonforExport='S' then 'Sample' when ReasonforExport='M' then 'Merchandise' when ReasonforExport='D' then 'Document' when ReasonforExport='O' then 'Others' else '' end as ContentCategoryDescription,
			  0 as CODValue,
			  '' as CODPaymentmode,
			  isnull(REPLACE(REPLACE(REPLACE(REPLACE(isnull(specialinstruction,''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '),'') as DeliveryInstructions,
			  ConsignmentPreferPickupDate as CollectionDate,
			  '' as ManifestNumber,
			  'Singpost' as Distributor,
			  '' as DispatchMode,
			  '' as DispatchType,
			  '' as PPINumber,
			  '' as PPIDescription ,
			  'DOM' as ServiceSubType,
			  'AUD' as InsuredValueCurrency,
			   1 as ToProcess,
			  '' as Remarks,
			   '' as ReleaseOnholdItem,
			   c.ConsignmentPreferPickupTime as CollectionTimeFrom,
			   c.ConsignmentPreferPickupTime as CollectionTimeTo
			
		  from
           [EzyFreight].[dbo].[tblConsignment] c left join  [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=c.consignmentid
			                                              left join  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  left JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=c.consignmentid
														  where  convert(date,c.Createddatetime)='2015-08-11' FOR XML PATH('ItemDetail'),Type) FOR XML PATH('RecordDetail'),Type),
(SELECT 'T' As RecordType,
				'3' As NumberOfLines FOR XML PATH('FileTrailer'),Type)
FOR XML PATH(''),
ROOT('Structure'))

select @varcharop as col1

end
GO
GRANT EXECUTE
	ON [dbo].[Z_sp_ExportConsignmentDetailsfromEzySendtoEzyTrak]
	TO [SSISUser]
GO
