SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Z_sp_ExportConsignmentstoGateway_backup20151006]   as
begin
   
   
     --'=====================================================================
    --' CP -Stored Procedure - sp_ExportConsignmentstoGateway
    --' ---------------------------
    --' Purpose: ExportConsignmentstoGateway-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 11 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 11/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

	Declare @Temp table(Consignmentid int,CONSIGNMENTNUMBER varchar(100),CONSIGNMENTDATE varchar(20),[MANIFESTFILENAME] varchar(50),[MANIFESTFILEDATE] date,[SERVICECODE] varchar(20),
	                    [ACCOUNTNUMBER] varchar(50),[SENDERNAME] varchar(200),[SENDERADDRESS1] varchar(500),[SENDERADDRESS2] varchar(500),[SENDERLOCATION] varchar(50),[SENDERSTATE] varchar(50),
						[SENDERPOSTCODE] varchar(20),[RECEIVERNAME] varchar(200),[RECEIVERADDRESS1] varchar(500),[RECEIVERADDRESS2] varchar(500),[RECEIVERLOCATION] varchar(50),[RECEIVERSTATE] varchar(50),
						[RECEIVERPOSTCODE] varchar(20),CONTACT varchar(50),phonenumber varchar(30),[PRIMARYREFERENCE] varchar(50),[RELEASEASN] varchar(50),[RETURNAUTHORITYNUMBER] varchar(50),
						[OTHERREFERENCE1] varchar(50),[OTHERREFERENCE2] varchar(50),[OTHERREFERENCE3] varchar(50),[OTHERREFERENCE4] varchar(50),[SPECIALINSTRUCTIONS] varchar(500),
						[TOTALLOGISTICSUNITS] int,[TOTALDEADWEIGHT] decimal(12,2),[TOTALVOLUME] decimal(12,4),[INSURANCECATEGORY] varchar(10),[DECLAREDVALUE] decimal(12,2),Testflag bit,
						[DANGEROUSGOODSFLAG] bit,[NOTBEFOREDATE] varchar(20),[NOTAFTERDATE] varchar(20))

						Insert into @Temp
	SELECT    CONSIGNMENT.Consignmentid,
	            consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
            ,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
           ,a.FirstName+' '+a.LastName as [SENDERNAME]
           ,a.address1 as [SENDERADDRESS1]
           ,a.address2 as [SENDERADDRESS2]
           ,a.suburb AS [SENDERLOCATION]
           ,ISNULL(a.stateName,'') as [SENDERSTATE]
           ,a.Postcode as [SENDERPOSTCODE]
           ,a1.FirstName+' '+a1.LastName as [RECEIVERNAME]
           ,a1.address1 as [RECEIVERADDRESS1]
           ,a1.address2 as [RECEIVERADDRESS2]
           ,a1.suburb as [RECEIVERLOCATION]
           ,ISNULL(a1.stateName,'') as [RECEIVERSTATE]
           ,a1.Postcode as [RECEIVERPOSTCODE]
		   ,a1.FirstName AS [CONTACT]
		   ,a1.phone as [PHONENUMBER]
           ,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
           ,convert(varchar(50),'') as [RELEASEASN] 
           ,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
           ,convert(varchar(50),'') as [OTHERREFERENCE1]
           ,convert(varchar(50),'') as [OTHERREFERENCE2]
           ,convert(varchar(50),'') as [OTHERREFERENCE3]
           ,convert(varchar(50),'') as [OTHERREFERENCE4]
           ,SpecialInstruction as [SPECIALINSTRUCTIONS]
           ,NoOfItems as [TOTALLOGISTICSUNITS]
           ,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
           ,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
		   ,'' AS [INSURANCECATEGORY]
		   ,NetSubTotal AS [DECLAREDVALUE]
		   ,'0' AS [TESTFLAG]
		   ,DangerousGoods AS [DANGEROUSGOODSFLAG]
		   ,'' AS [NOTBEFOREDATE]
		   ,'' AS [NOTAFTERDATE]
	       from 
          [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] CONSIGNMENT
			                                              left join  [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
														  where  EDIDataprocessed=0 and CONSIGNMENT.isprocessed=1


	
declare @xmlop xml
declare @varcharop varchar(max)
set @xmlop=  
            (SELECT
			 CONSIGNMENT.CONSIGNMENTNUMBER
			,CONSIGNMENTDATE
			,MANIFESTFILENAME
			,MANIFESTFILEDATE
			,SERVICECODE
            ,ACCOUNTNUMBER--should be 999999999?
           ,SENDERNAME
           ,SENDERADDRESS1
           ,SENDERADDRESS2
           ,SENDERLOCATION
           ,SENDERSTATE
           ,SENDERPOSTCODE
           ,RECEIVERNAME
           ,RECEIVERADDRESS1
           ,RECEIVERADDRESS2
           ,RECEIVERLOCATION
           ,RECEIVERSTATE
           ,RECEIVERPOSTCODE
		   ,CONTACT
		   ,PHONENUMBER
           ,PRIMARYREFERENCE
           ,RELEASEASN
           ,RETURNAUTHORITYNUMBER
           ,OTHERREFERENCE1
           ,OTHERREFERENCE2
           ,OTHERREFERENCE3
           ,OTHERREFERENCE4
           ,SPECIALINSTRUCTIONS
           ,TOTALLOGISTICSUNITS
           ,TOTALDEADWEIGHT--Declaredweight/Measuredweight
           ,TOTALVOLUME--Declaredvolume/measurevolume
		   ,INSURANCECATEGORY
		   ,DECLAREDVALUE
		   ,TESTFLAG
		   ,DANGEROUSGOODSFLAG
		   ,NOTBEFOREDATE
		   ,NOTAFTERDATE
           ,ITEM.LABELNUMBER
		   ,'CTN' AS [UNITTYPE]
		   ,'0' AS [LABELTESTFLAG]
		   ,'1' AS [INTERNALLABEL]
		   ,'0' AS [LINKLABEL]

		   --Date?
			 FROM @temp CONSIGNMENT left join  [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] ITEM on ITEM.consignmentid=CONSIGNMENT.consignmentid
			                                            
														  for xml AUTO ,ROOT('CPPLPODManifest'), ELEMENTS );

														 -- for XML AUTO ,ROOT('CPPLPODManifest'),ELEMENTS XSINIL)
														 -- select @xmlop;
														   --output to 'c:\\sales.xml' FORMAT TEXT;


--select @xmlop
set @varcharop=convert(varchar(max),@xmlop)
select @varcharop as col1
	--bcp @xmlop queryout c:\department.txt -c –T	
update  [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] set EDIDataprocessed=1 from @temp t join [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] c on t.Consignmentid=c.consignmentid


	end			
GO
