SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Z_sp_ExtractDomesticAccountCustomerBillingFilesToPronto_backup20151209](@Startdate date,@Enddate date) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_ExtractDomesticAccountCustomerBillingFilesToPronto] 
	    --' ---------------------------
    --' Purpose: ExtractDomesticAccountCustomerBillingFilesToPronto-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 22 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 22/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================
	
	If @StartDate=''
select @Startdate=convert(date,dateadd(DAY,-(datepart("dw",getdate()))-5,getdate()))


If @EndDate=''
select @EndDate=convert(date,dateadd(DAY,-(datepart("dw",getdate()))+1,getdate()))


  Select  Consignmentid
         ,'C' as RecordType
         ,ConsignmentCode as [Consignment Reference]
		 ,convert(date,c.CreatedDateTime) as [Consignment date]
         ,convert(varchar(50),'') as [Manifest reference]
         ,convert(varchar(50),'') as [Manifest date]
		 ,RateCardID as [Service]
         ,convert(varchar(100),AccountNumber) as [Account code]
         ,a.FirstName+' '+a.LastName as [Sender name]
         ,replace(a.address1,',','') as [Sender address 1]
         ,replace(a.address2,',','') as [Sender address 2]
         ,a.suburb AS [Sender locality]
         ,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [Sender State]
         ,a.Postcode as [Sender postcode]
         ,a1.FirstName+' '+a1.LastName as [Receiver name]
         ,a1.address1 as [Receiver address 1]
         ,a1.address2 as [Receiver address 2]
         ,a1.suburb as [Receiver locality]
         ,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [Receiver state]
         ,a1.Postcode as [Receiver postcode]
         ,isnull(CustomerRefNo,'') as [Customer reference]
         ,convert(varchar(50),'') as [Release ASN] 
         ,convert(varchar(50),'') as [Return Authorisation Number]
         ,convert(varchar(50),'') as [Customer other reference 1]
         ,convert(varchar(50),'') as [Customer other reference 2]
         ,convert(varchar(50),'') as [Customer other reference 3]
         ,convert(varchar(50),'') as [Customer other reference 4]
         ,isnull(REPLACE(REPLACE(REPLACE(REPLACE(isnull(specialinstruction,''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '),'') as [Special instructions]
         ,NoOfItems as [Item quantity]
         ,isnull(TotalWeight,0) as [Declared weight]
         ,isnull(TotalMeasureWeight,0) as [Measured weight]
          ,isnull(TotalVolume/250,0) as [Declared volume]
         ,isnull(TotalMeasureVolume/250,0) as [Measured volume]
         ,0.00 as [Price override]
         ,convert(varchar(50),'') as [Insurance category]
         ,isnull(NetSubTotal,0) as [Declared value]
         ,convert(varchar(50),'') as [Insurance Price Override]
         ,0 as [Test Flag]
         ,DangerousGoods as [Dangerous goods flag]
         ,convert(varchar(50),'') as [Release Not Before]
         ,convert(varchar(50),'') as [Release Not After]
         ,NoOfItems as [Logistics Units]
         ,1 as [IsProcessed]
         ,0 as [IsDeleted]
         ,0 as [HasError]
         ,0 as [ErrorCoded]
         ,'Admin' as [AddWho]
         ,getdate() as [AddDateTime]
         ,'Admin' as [EditWho]
         ,getdate() as [EditDateTime]
FROM [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c left join [cpsqlweb01]. [EzyFreight].[dbo].[tblCompanyUsers] cu on cu.UserID=c.userid
                                            left join [cpsqlweb01]. [EzyFreight].[dbo].[tblCompany] tc on tc.companyid=cu.companyid
                                            left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
where c.isbilling=0 and isinternational=0  and IsAccountCustomer=1 
and convert(date,c.CreatedDateTime)  between @StartDate and @EndDate
--and cu.userid<>1920 and cu.userid<>2368
and tc.AccountNumber<>'WD00000060' and tc.AccountNumber<>'WD00000006' and tc.AccountNumber<>'WD00000926'

--and convert(date,c.CreatedDateTime) between '2015-08-31' and '2015-09-06'



end
GO
