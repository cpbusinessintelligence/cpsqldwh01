SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Z_sp_RptGetRedirectedItemReport_BUP20161010](@StartDate date,@EndDate date) as
begin
     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptGetRedirectedItemReport]
    --' ---------------------------
    --' Purpose: sp_RptGetRedirectedItemReport-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 16 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 05/10/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

	Select [ConsignmentCode]
	       ,[SelectedDeliveryOption]
		   ,convert(date,c.createddatetime) as RedirectedDate
		   ,isnull(a.FirstName,'')+' '+isnull(a.LastNAme,'') as Sender
		   ,isnull(a.Address1,'') as SenderAddress1
		   ,isnull(a.Address2 ,'') as SenderAddress2
		   ,isnull(a.Suburb,'') as SenderSuburb
		   ,isnull(a.StateName,s.statecode) as SenderState
		   ,isnull(a.Postcode,'') as SenderPostcode
		   ,isnull(a.Email,'') as SenderEmail
		   ,isnull(a.Phone,a.Mobile) as SenderPhone
		   ,isnull(a1.FirstName,'')+' '+isnull(a1.LastNAme,'') as CurrentReceiver
		   ,isnull(a1.Address1,'') as CurrentReceiverAddress1
		   ,isnull(a1.Address2 ,'') as CurrentReceiverAddress2
		   ,isnull(a1.Suburb,'') as CurrentReceiverSuburb
		   ,isnull(a1.StateName,s1.statecode) as CurrentReceiverState
		   ,isnull(a1.Postcode,'') as CurrentReceiverPostcode
		   ,isnull(a1.Email,'') as CurrentReceiverEmail
		   ,isnull(a1.Phone,a1.Mobile) as CurrentReceiverPhone
		   ,isnull(a2.FirstName,'')+' '+isnull(a2.LastNAme,'') as NewReceiver
		   ,isnull(a2.Address1,'') as NewReceiverAddress1
		   ,isnull(a2.Address2 ,'') as NewReceiverAddress2
		   ,isnull(a2.Suburb,'') as NewReceiverSuburb
		   ,isnull(a2.StateName,s2.statecode) as NewReceiverState
		   ,isnull(a2.Postcode,'') as NewReceiverPostcode
		   ,isnull(a2.Email,'') as NewReceiverEmail
		   ,isnull(a2.Phone,a2.Mobile) as NewReceiverPhone
		   ,[TotalWeight]
           ,[TotalVolume]
           ,[ServiceType]
           ,[RateCardID]
           ,[CurrentETA]
           ,[NewETA]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[Terms]
           ,[ATL]
           ,[ConfirmATLInsuranceVoid]
           ,[ConfirmDeliveryAddress]
           ,[IsProcessed]
           ,[SortCode]
           ,[CalculatedTotal]
           ,[CreditCardSurcharge]
           ,[NetTotal]
from [dbo].[tblRedirectedConsignment]  c 
                                            --join [dbo].[tblRedirectedItemLabel] l on c.[ReConsignmentID]=l.[ReConsignmentID]
	                                         join [dbo].[tbladdress] a on a.addressid=c.pickupaddressid
											left join [dbo].[tblState] s on s.stateid=a.StateID
											 join [dbo].[tbladdress] a1 on a1.addressid=c.[CurrentDeliveryAddressID]
											 left join [dbo].[tblState] s1 on s1.stateid=a1.StateID
											 join [dbo].[tbladdress] a2 on a2.addressid=c.[NewDeliveryAddressID]
		       								 left join [dbo].[tblState] s2 on s2.stateid=a2.StateID
											 where convert(date,c.createddatetime) between @StartDate and @EndDate
	end
GO
