SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Z_sp_RptInternationalRegularshippersBUP20160218](@Date Date) as
begin

 --'=====================================================================
    --' CP -Stored Procedure -[sp_RptInternationalRegularshippers]
    --' ---------------------------
    --' Purpose: InternationalRegularshippers----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 18 Nov 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 18/11/2015   AB      1.00    Created the procedure                             --AB20151108

    --'=====================================================================


select userid,count(*) as count
into #temp
 from tblconsignment c where isinternational=1 and userid<>-1 and c.isprocessed=1
group by userid
having count(*)>=3

select companyname,count(*) as count
into #temp1
from tblconsignment c   join [dbo].[tblAddress] a on c.pickupid=a.addressid  
where isinternational=1 and c.userid=-1 and c.isprocessed=1
group by companyname

--Select * from #temp1

--select * from #temp

Select convert(date,c.createddatetime) as Date,
       isnull(a.companyname,'') as BusinessName,
       a.FirstName+' '+a.LastNAme as SenderName,
       a.address1 as Senderaddress1,
       a.address2 as SenderAddress2,
			a.email as Senderemail,
			a.phone as SenderPhone,
			a.suburb as SenderSuburb,
			a.Postcode as SenderPostcode,
			isnull(s.StateCode,isnull(a.statename,'Unknown')) as SenderState,
			case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end as SenderCountry,
			count(*)

from #temp t join tblconsignment c   on c.userid=t.userid  left    join [dbo].[tblAddress] a on c.pickupid=a.addressid  
                                              left    join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                                    left    join  [dbo].[tblState] s on s.stateid=a.stateid
													left    join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
													left join [dbo].[Users] u on c.userid=u.userid
													left join  [dbo].[tblcompanyusers] u1 on u1.userid=c.userid
											        left join  [dbo].[tblcompany] p on u1.companyid=p.companyid
													where isnull(p.AccountNumber,'')<>'WD00000060' and isnull(p.AccountNumber,'')<>'WD00000006' and isnull(p.AccountNumber,'')<>'WD00000926' and isinternational=1 and convert(date,c.createddatetime)>=@Date
													--and c.isprocessed=1
		
		group by      convert(date,c.createddatetime),
                       isnull(a.companyname,''),
                        a.FirstName+' '+a.LastNAme ,
                        a.address1,
                        a.address2 ,
			            a.email ,
			            a.phone ,
			            a.suburb ,
			            a.Postcode ,
			            isnull(s.StateCode,isnull(a.statename,'Unknown')),
			            case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end


union

Select convert(date,c.createddatetime) as Date,
       isnull(a.companyname,'') as BusinessName,
       a.FirstName+' '+a.LastNAme as SenderName,
       a.address1 as Senderaddress1,
       a.address2 as SenderAddress2,
			a.email as Senderemail,
			a.phone as SenderPhone,
			a.suburb as SenderSuburb,
			a.Postcode as SenderPostcode,
			isnull(s.StateCode,isnull(a.statename,'Unknown')) as SenderState,
			case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end as SenderCountry,
			count(*)

from #temp1 t 
join  [dbo].[tblAddress] a on t.companyname=a.companyname 
              join  tblconsignment c   on   c.contactid=a.addressid  
			                                 -- left    join [dbo].[tblAddress] a on c.pickupid=a.addressid 
                                              left    join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                                    left    join  [dbo].[tblState] s on s.stateid=a.stateid
													left    join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
													left join [dbo].[Users] u on c.userid=u.userid
													left join  [dbo].[tblcompanyusers] u1 on u1.userid=c.userid
											        left join  [dbo].[tblcompany] p on u1.companyid=p.companyid
													where isnull(p.AccountNumber,'')<>'WD00000060' and isnull(p.AccountNumber,'')<>'WD00000006' and isnull(p.AccountNumber,'')<>'WD00000926' and isinternational=1 and convert(date,c.createddatetime)>=@Date
													--and c.isprocessed=1
		
		group by      convert(date,c.createddatetime),
                       isnull(a.companyname,''),
                        a.FirstName+' '+a.LastNAme ,
                        a.address1,
                        a.address2 ,
			            a.email ,
			            a.phone ,
			            a.suburb ,
			            a.Postcode ,
			            isnull(s.StateCode,isnull(a.statename,'Unknown')),
			            case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end
end
GO
