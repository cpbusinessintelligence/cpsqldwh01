SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Z_sp_RptNewWebsiteOneOffs](@FromDate date,@Todate date) as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptNewWebsiteOneOffs
    --' ---------------------------
    --' Purpose: All customers who traded-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================



SELECT consignmentcode,
       consignmentid,
	   ratecardid,
	   convert(date,c.createddatetime) as Date,
	   isnull(s.StateCode,isnull(a.statename,'Unknown')) as Statecode,
	   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as DestinationState,
	   count(*) as CustomersTraded,

	   case when isaccountcustomer=1 then c.CalculatedTotal else convert(decimal(12,2),0) end as GrossTotal,
	   case when isaccountcustomer=1 then c.CalculatedGST else convert(decimal(12,2),0) end as GST,
	   case when isaccountcustomer=1 then c.CalculatedTotal+c.CalculatedGST else convert(decimal(12,2),0) end as [NetTotal],
	   isaccountcustomer
       into #temp
	    FROM [EzyFreight].[dbo].[tblconsignment] c left join [dbo].[tblAddress] a on c.pickupid=a.addressid  
	                                              left join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                                  left join  [dbo].[tblState] s on s.stateid=a.stateid
												  left join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
	where  convert(date,c.createddatetime) between @Fromdate and @Todate
	   group by consignmentcode,
       consignmentid,
	   ratecardid,
	   convert(date,c.createddatetime) ,
       datepart(hour,c.Createddatetime) ,
	   isnull(s.StateCode,isnull(a.statename,'Unknown')) ,
	   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) ,
	   isaccountcustomer,
	   c.CalculatedTotal,
	   c.CalculatedGST


Update #temp set [GrossTotal]=(Select sum(GrossTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid and #temp.GrossTotal=0


Update #temp set [GST]=(Select sum(GST) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid  and #temp.GST=0


Update #temp set [NetTotal]=(Select sum(NetTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid and #temp.NetTotal=0


SELECT  ltrim(rtrim(Statecode)),
       case when ltrim(rtrim(Statecode)) in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 1  else 0 end as ExAustralia ,
       ltrim(rtrim(DestinationState)),
       case when RateCardID in ('EXP','SAV','EXPA') then 'International' else 'Domestic' end as ServiceArea,
       case when RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate'  when RateCardID in ('SDC','PDC') then 'Australian City Express SameDay'  when RateCardID in ('REC','PEC') then 'Domestic Saver' when  RateCardID in ( 'EXP','EXPA') then 'International Express' when RateCardID = 'SAV' then 'International Saver' else '' end as ServiceType,
    --  isnull(c.customerRefNo,'') as CustomerReferenceNo,
       Date,
	   sum(CustomersTraded) as CustomersTraded,
	   sum(GrossTotal) as GrossTotal,
	   sum(GST) as GST,
	   sum(NetTotal) as NetTotal
	   from #temp
	   group by ltrim(rtrim(Statecode)),
       case when ltrim(rtrim(Statecode)) in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 1  else 0 end  ,
       ltrim(rtrim(DestinationState)),
       case when RateCardID in ('EXP','SAV','EXPA') then 'International' else 'Domestic' end ,
       case when RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate'  when RateCardID in ('SDC','PDC') then 'Australian City Express SameDay'  when RateCardID in ('REC','PEC') then 'Domestic Saver' when  RateCardID in ( 'EXP','EXPA') then 'International Express' when RateCardID = 'SAV' then 'International Saver' else '' end ,
    --  isnull(c.customerRefNo,'') as CustomerReferenceNo,
       Date











end
GO
GRANT EXECUTE
	ON [dbo].[Z_sp_RptNewWebsiteOneOffs]
	TO [ReportUser]
GO
