SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Z_sp_RptNewWebsiteOneOffs_BUP20160620](@FromDate date,@Todate date) as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptNewWebsiteOneOffs
    --' ---------------------------
    --' Purpose: All customers who traded-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

SELECT isnull(s.StateCode,isnull(a.statename,'Unknown')) as Statecode,
       case when isnull(s.StateCode,isnull(a.statename,'Unknown')) in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 1  else 0 end as ExAustralia ,
       isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as DestinationState,
       case when  c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end as ServiceArea,
       case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else RateCardDescription end as ServiceType,
    --  isnull(c.customerRefNo,'') as CustomerReferenceNo,
       convert(date,c.createddatetime) as Date,
	   count(distinct c.consignmentid) as JobsBooked,
	   sum(GrossTotal) as GrossTotal,
	   sum(GST) as GST,
	   sum(NetTotal) as NetTotal
        into #temp
       FROM [EzyFreight].[dbo].[tblconsignment] c left join [dbo].[tblAddress] a on c.pickupid=a.addressid  
	                                              left join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                                  left join  [dbo].[tblState] s on s.stateid=a.stateid
												  left join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
	                                              left join [EzyFreight].[dbo].[tblsalesOrder] s2 on s2.referenceNo=c.consignmentid 
												  left join [cpsqlops01].[couponcalculator].[dbo].[Ratecard] r on r.ratecardcode=c.ratecardid
	where  convert(date,c.createddatetime) between @Fromdate and @Todate and isnull(isaccountcustomer,0)<>1  and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
	   --<dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())
  group by isnull(s.StateCode,isnull(a.statename,'Unknown')) ,
           case when isnull(s.StateCode,isnull(a.statename,'Unknown')) in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 1  else 0 end ,
           isnull(s1.StateCode,isnull(a1.statename,'Unknown')),
            case when  c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end,
       case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else RateCardDescription end ,
           c.RateCardID,
      --     isnull(c.customerRefNo,'') ,
          convert(date,c.createddatetime),
          datepart(hour,c.Createddatetime)
  order by 1,2,3,4 asc



  Select Statecode,
         DestinationState,
		 ExAustralia,
         ServiceArea,
         ServiceType,
		 Date,
		 sum(JobsBooked) as CustomersTraded,
		 sum(GrossTotal) as GrossTotal,
		 sum(GST) as GST,
		 sum(NetTotal) as NetTotal
		 from #temp
		 group by Statecode,
		          DestinationState,
				  ExAustralia,
		          ServiceArea,
                  ServiceType,
		          Date
				  order by Date





end
GO
