SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE procedure [dbo].[Z_sp_RptSalesReportbyNation](@StartDate Date,@EndDate Date) as
begin
     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptNationwidesalesReport]
    --' ---------------------------
    --' Purpose: NationwidesalesReport-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 12 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 12/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================


SELECT 
      [ConsignmentCode]
	  ,convert(date,c.CreatedDateTime) as [Consignment date]
      ,a.FirstName+' '+a.LastName as [Sender name]
      ,isnull(a.address1,'') as [Sender address 1]
      ,isnull(a.address2,'') as [Sender address 2]
      ,isnull(a.suburb,'') AS [Sender locality]
      ,isnull(a.stateName,'') as [Sender State]
	  ,ISNULL(A.COUNTRY,'') as [Sender Country]
      ,a.Postcode as [Sender postcode]
      ,a1.FirstName+' '+a1.LastName as [Receiver name]
      ,isnull(a1.address1,'') as [Receiver address 1]
      ,isnull(a1.address2,'') as [Receiver address 2]
      ,isnull(a1.suburb,'') as [Receiver locality]
      ,isnull(a1.stateName,'') as [Receiver state]
	  ,ISNULL(A1.COUNTRY,'') as [Receiver Country]
      ,a1.Postcode as [Receiver postcode]
      ,[TotalWeight]
     -- ,[TotalMeasureWeight]
      ,[TotalVolume]
     -- ,[TotalMeasureVolume]
      ,[NoOfItems]
      ,[RateCardID]
      ,isnull([InternalServiceCode],'') as [InternalServiceCode]
      ,isnull([NetSubTotal],0) as NetSubtotal
      ,isnull([IsATl],0) as IsATL
      ,isnull([IsReturnToSender],0) as Isreturntosender
 --Is it only for Aus?
 --NetSubTotal?
  FROM [EzyFreight].[dbo].[tblConsignment] c left join [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
   									        left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											where convert(Date,c.CreatedDateTime)>=@StartDate and convert(Date,c.CreatedDateTime)<=@EndDate
											 and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
 end
GO
GRANT EXECUTE
	ON [dbo].[Z_sp_RptSalesReportbyNation]
	TO [ReportUser]
GO
