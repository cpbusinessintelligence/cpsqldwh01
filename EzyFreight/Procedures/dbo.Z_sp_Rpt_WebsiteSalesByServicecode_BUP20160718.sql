SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Z_sp_Rpt_WebsiteSalesByServicecode_BUP20160718](@StartDate date,@EndDate date) as
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_WebsiteSalesByServicecode]
    --' ---------------------------
    --' Purpose: sp_Rpt_WebsiteSalesByServicecode-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 02 Nov 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 02/11/2015   AB      1.00    Created the procedure                             --AB20151102

    --'=====================================================================

Select  consignmentcode,
       consignmentid,
	   Convert(date,c.createddatetime) as [Date],
	   case when isnull(isaccountcustomer,0)=1 then p.accountnumber else 'Adhoc' end as AccountNo,
	   case when isnull(isaccountcustomer,0)=1 then p.companyname else a2.CompanyName end as AccountName,
	  case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else RateCardDescription end as ProductType,
	  c.Ratecardid,
	  	   case when isinternational=0 then isnull(s.StateCode,isnull(a.statename,'Unknown')) else isnull(a.countrycode,isnull(a.country,'')) end as [From],
	   case when isinternational=0 then isnull(s1.StateCode,isnull(a1.statename,'Unknown')) else isnull(a1.countrycode,isnull(a1.country,'')) end as [To],
	 --   isnull(s.StateCode,isnull(a.statename,'Unknown'))  as [State],
	   c.NoOfItems,
	   case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end as Totalweight,
	   case when isnull(isaccountcustomer,0)=1 then c.CalculatedTotal else convert(decimal(12,2),0) end as [Gross Total],
	    case when isnull(isaccountcustomer,0)=1 then c.CalculatedGST else convert(decimal(12,2),0) end as [GST],
	   case when isnull(isaccountcustomer,0)=1 then c.CalculatedTotal+c.CalculatedGST else convert(decimal(12,2),0) end as [Net Total],
	   convert(decimal(12,2),0) as Fuelcharge

	   into #temp

  FROM [EzyFreight].[dbo].[tblConsignment] c  left join [dbo].[tblAddress] a on c.pickupid=a.addressid  
                                              left join  [dbo].[tblState] s on s.stateid=a.stateid
											  left join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                              left join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
											  left join  [dbo].[tblcompanyusers] u on u.userid=c.userid
											  left join  [dbo].[tblcompany] p on u.companyid=p.companyid
											  left  join [dbo].[tblAddress] a2 on c.contactid=a2.addressid 
											  --left join [dbo].[users] u1 on u1.userid=c.userid
											  left join [cpsqlops01].[couponcalculator].[dbo].[Ratecard] r on r.ratecardcode=c.ratecardid
													where Convert(date,c.CreatedDatetime) 
													between @StartDate and @EndDate and isnull(p.AccountNumber,'') not in (Select [Accountcode] from  [EzyFreight].[dbo].[tblCompanyTestUser])
													--between @StartDate and @EndDate 
													 and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
							

Update #temp set [Gross Total]=(Select sum(GrossTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid  and #temp.[Gross Total]=0


Update #temp set [GST]=(Select sum(GST) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid  and #temp.[GST]=0


Update #temp set [Net Total]=(Select sum(NetTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid  and #temp.[Net Total]=0


 Update #temp set Fuelcharge=isnull(s2.Fuelcharge,0) from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
    where description not in ('Reweight charge','Credit Card Surcharge','Insurance','Admin Fee')

	Select [Date],
	       AccountNo,
	       AccountName,
		   Ratecardid,
	       ProductType,
	       [From],
		   [To],
		   sum(NoOfItems) as TotalItems,
		    count(*) as [Consignment Count],
	  -- sum(NoOfItems) as Multipieceshipments,
	   sum(Totalweight) as [TotalWeight],
	   sum([Gross Total]) as [GrossTotal],
	   sum([GST]) as [GST],
	   sum([Net Total]) as [NetTotal]
	   from #temp
	   group by [Date],
	          AccountNo,
	       AccountName,
		   Ratecardid,
	       ProductType,
	       [From],
		   [To]
	  end     
GO
