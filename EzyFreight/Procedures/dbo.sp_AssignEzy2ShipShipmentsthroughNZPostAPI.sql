SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_AssignEzy2ShipShipmentsthroughNZPostAPI] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_CreateEzy2ShipShipmentsthroughNZAPI]
    --' ---------------------------
    --' Purpose: NZ Post API -----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 21 Dec 2016                         

    --'=====================================================================


 SELECT  c.ConsignmentID,ConsignmentCode, cd.ItemDescription as Description, l.LabelNumber,p.Address1 as PicAddress1,p.Address2 as PicAddress2,p.Country as PicCountryCode, 
 p.Email as PicEmail, p.FirstName as PicFirstName, p.LastName as PicLastName, p.Phone as PicPhone, p.postCode as PicPhostCode, CONVERT(varchar(50), isnull(p.StateID,'')) as PicStateID, 
 p.Suburb as PicSuburb,d.Address1 as DesAddress1,d.Address2 as DesAddress2,d.Country as DesCountryCode, d.Email as DesEmail, d.FirstName as DesFirstName, 
 d.LastName as DesLastName, d.Phone as DesPhone,d.postCode as DesPostCode, CONVERT(varchar(50), isnull(d.StateID,'')) as DesStateID, d.Suburb as DesSuburb,CONVERT(int,c.totalWeight) as PhysicalWeight,
 c.NoOfItems as Quantity ,c.ReasonForExport,CONVERT(int,c.totalWeight) as totalWeight

 FROM [EzyFreight].[dbo].tblConsignment c with (NOLOCK)  left join [EzyFreight].[dbo].tblItemLabel l on c.consignmentID = l.ConsignmentID
 left join [EzyFreight].[dbo].[tblCustomDeclaration] cd on c.ConsignmentID = cd.ConsignmentID
 left join [EzyFreight].[dbo].[tblAddress] p on c.PickupID = p.AddressID
 left join [EzyFreight].[dbo].[tblAddress] d on c.DestinationID = d.AddressID
 where
 consignmentCode like 'CPWSAV%' and 
 d.CountryCode='NZ' and 
 c.NoOfItems =1 and 
 isShipmentSent2NZPost =0 and  
 RateCardID like 'SAV%' and 
 c.createdDateTime >'2017-02-25'
 --c.ConsignmentCode = 'CPWSAVW000003495'


end



GO
