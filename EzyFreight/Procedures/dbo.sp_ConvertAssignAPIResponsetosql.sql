SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_ConvertAssignAPIResponsetosql as
begin



declare @FilePath varchar(200)
Set @FilePath='E:\ETL Processing\Assign Shipments to EzyParcel API\AssignAPIResponse.xml'

  DECLARE @x XML, @sql NVARCHAR(MAX);

SELECT @sql = N'SELECT @x = CONVERT(xml,BulkColumn) 
 FROM OPENROWSET(BULK ''' + @FilePath + ''' ,SINGLE_BLOB) as p1';

EXEC sp_executesql @sql, N'@x XML OUTPUT', @x OUTPUT;

truncate table EzyParcelAssignShipmentsStaging
  INSERT INTO EzyParcelAssignShipmentsStaging(xmlresponse)
select @x 

DECLARE @x1 xml
select @x1 = xmlresponse from EzyParcelAssignShipmentsStaging;


WITH XMLNAMESPACES  (
    'https://quantium-staging.oss.neopost-id.com/modules/oss/api/ship/index?sz_Client=Quantium/AssignShipments' as ns1,
    'http://www.w3.org/2001/XMLSchema-instance' AS xsi,
    'http://schemas.xmlsoap.org/soap/envelope/' AS e
)

Insert into [EzyParcelAssignShipments](code,message,manifestnumber,ShipmentNumber)
SELECT
 T.c.value('code[1]','VARCHAR(200)') ,
  T.c.value('message[1]','VARCHAR(200)') ,
   T.c.value('ManifestNumber[1]','VARCHAR(200)') ,
    T.c.value('ShipmentNumber[1]','VARCHAR(200)') 

from  @x1.nodes('e:Envelope/e:Body/ns1:assignShipmentsResponse/response/ShipmentAssigns') T(c)

end
GO
GRANT EXECUTE
	ON [dbo].[sp_ConvertAssignAPIResponsetosql]
	TO [SSISUser]
GO
