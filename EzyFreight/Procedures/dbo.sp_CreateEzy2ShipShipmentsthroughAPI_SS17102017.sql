SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO













CREATE procedure [dbo].[sp_CreateEzy2ShipShipmentsthroughAPI_SS17102017] as

--EXEC [dbo].[sp_CreateEzy2ShipShipmentsthroughAPI] 

begin



     --'=====================================================================

    --' CP -Stored Procedure - [sp_CreateEzy2ShipShipmentsthroughAPI]

    --' ---------------------------

    --' Purpose: sp_CreateEzy2ShipShipmentsthroughAPI-----

    --' Developer: Jobin Philip (Couriers Please Pty Ltd)

    --' Date: 04 Nov 2016

    --' Copyright: 2014 Couriers Please Pty Ltd

    --' Change Log: 

    --' Date          Who     Ver     Reason                                            

    --' ----          ---     ---     -----                                            

    --' 04/11/2016    AK      1.00    Created the procedure                            



    --'=====================================================================





Select c.consignmentid,

      --d.subtotal,

      convert(varchar(50), sum(cast ((case when d.SubTotal='Nan' then 0 else d.SubTotal end) as decimal(12,2)))) as DeclaredValue,

       min(replace((case when c.consignmentid=83991 then 'a' else [ItemDescription] end),'"','')) as [ItemDescription],

	   min(d.CountryofOrigin),

	   HSCode,

	  convert(varchar(50), c.[TotalWeight]) as [TotalWeight] ,

	  convert(varchar(50), c.NoOfItems) as NoOfItems ,

	  convert(varchar(50), l.[Length]) as [Length],

	   convert(varchar(50),l.[Width] ) as [Width],

	   convert(varchar(50),l.[Height]) as [Height], 

	   convert(varchar(50),[CubicWeight]) as [CubicWeight],

	    convert(varchar(50),c.[ReasonForExport] ) as CatgeoryofShipment,

	   [RateCardID] as ServiceCode,

	   l.labelnumber as SenderRef,



	   replace(a.FirstName+' '+a.LastNAme,'"','') as ContactContactNAme,

	   a.Phone as ContactPhone,

	   a.Email as ContactEmail,

	   a.countrycode as ContactCountrycode,

	   replace(a.Address1,'"','') as ContactAddress1,

	   replace(a.Address2,'"','') as ContactAddress2,

	   replace(a.CompanyName,'"','') as ContactCompanyName,

	   case when isnull(a.IsBusiness,0)=1 then 'True' else 'false' end as ContactIsBusiness,

	   a.Postcode as ContactPostcode,

	   a.StateName as ContactState,

	   a.Suburb as ContactSuburb,

	   replace(a.FirstName,'"','') as ContactFirstName,

	   replace(a.LastName,'"','') as ContactLastName,





	   replace(a2.FirstName+' '+a2.LastNAme,'"','') as DestinationContactNAme,

	   a2.Phone as DestinationPhone,

	   a2.Email as DestinationEmail,

	   a2.countrycode as DestinationCountrycode,

	   replace(a2.Address1,'"','') as DestinationAddress1,

	   replace(a2.Address2,'"','') as DestinationAddress2,

	   replace(a2.CompanyName,'"','') as DestinationCompanyName,

	   case when isnull(a2.IsBusiness,0)=1 then 'True' else 'false' end as DestinationIsBusiness,

	   a2.Postcode as DestinationPostcode,

	   case when a2.country='Singapore' then  'Singapore' else a2.StateName end as DestinationState,

	    case when a2.country='Singapore' then  'Singapore' else a2.Suburb end as DestinationSuburb,

	   replace(a2.FirstName,'"','') as DestinationFirstName,

	   replace(a2.LastName,'"','') as DestinationLastName,





	   replace(a1.FirstName+' '+a1.LastNAme,'"','') as PickupContactNAme,

	   a1.Phone as PickupPhone,

	   a1.Email as PickupEmail,

	   a1.countrycode as PickupCountrycode,

	   replace(a1.Address1,'"','') as PickupAddress1,

	   replace(a1.Address2,'"','') as PickupAddress2,

	   replace(a1.CompanyName,'"','') as PickupCompanyName,

	   case when isnull(a1.IsBusiness,0)=1 then 'True' else 'false' end as PickupIsBusiness,

	   a1.Postcode as PickupPostcode,

	   a1.StateName as PickupState,

	   a1.Suburb as PickupSuburb,

	   replace(a1.FirstName,'"','') as PickupFirstName,

	   replace(a1.LastName,'"','') as PickupLastName





from tblconsignment c join tblcustomdeclaration d on c.consignmentid=d.consignmentid

                      join tblitemlabel l on l.consignmentid=c.consignmentid

					  join tbladdress a on a.addressid=contactid

					  join tbladdress a1 on a1.addressid=pickupid

					  join tbladdress a2 on a2.addressid=destinationid

where c.ratecardid like '%SAV%' and a2.CountryCode<>'NZ' 
--and labelnumber in ('CPWSAV900019131','CPWSAV900019130')



--and convert(Date,c.createddatetime)>=convert(date,dateadd(day,-6,getdate())) 

--and c.isCreateEzy2ShipShipment=0 
and l.labelnumber='CPWSAV900019237'


--and c.consignmentcode='CPWSAV900010769'

--and c.ConsignmentID in (99829,

--98801,

--98644)

--and l.labelnumber='CPWSAV900018956'


--and l.labelnumber = 'CPWSAV900018988'

group by c.consignmentid,

      --  d.subtotal,

	  -- d.CountryofOrigin,

	   HSCode,

	  convert(varchar(50), c.[TotalWeight]),

	  convert(varchar(50), c.NoOfItems),

	  convert(varchar(50), l.[Length]),

	   convert(varchar(50),l.[Width] ) ,

	   convert(varchar(50),l.[Height]), 

	   convert(varchar(50),[CubicWeight]) ,

	   convert(varchar(50),c.[ReasonForExport] ),

	   [RateCardID] ,

	   l.labelnumber ,



	  replace(a.FirstName+' '+a.LastNAme,'"','') ,

	   a.Phone ,

	   a.Email ,

	   a.countrycode ,

	   replace(a.Address1,'"','') ,

	   replace(a.Address2,'"','') ,

	   replace(a.CompanyName,'"','') ,

	   case when isnull(a.IsBusiness,0)=1 then 'True' else 'false' end ,

	   a.Postcode ,

	   a.StateName ,

	   a.Suburb,

	   replace(a.FirstName,'"','') ,

	   replace(a.LastName,'"','') ,





	  replace( a2.FirstName+' '+a2.LastNAme,'"','') ,

	   a2.Phone,

	   a2.Email ,

	   a2.countrycode ,

	   replace(a2.Address1,'"','') ,

	   replace(a2.Address2,'"','') ,

	   replace(a2.CompanyName,'"','') ,

	   case when isnull(a2.IsBusiness,0)=1 then 'True' else 'false' end ,

	   a2.Postcode ,

	   a2.StateName ,

	   a2.Suburb ,
	   case when a2.country='Singapore' then  'Singapore' else a2.StateName end ,

	    case when a2.country='Singapore' then  'Singapore' else a2.Suburb end ,

	   replace(a2.FirstName,'"','') ,

	   replace(a2.LastName,'"','') ,





	   replace(a1.FirstName+' '+a1.LastNAme,'"','') ,

	   a1.Phone ,

	   a1.Email ,

	   a1.countrycode ,

	   replace(a1.Address1,'"','') ,

	   replace(a1.Address2,'"','') ,

	   replace(a1.CompanyName,'"',''),

	   case when isnull(a1.IsBusiness,0)=1 then 'True' else 'false' end ,

	   a1.Postcode ,

	   a1.StateName ,

	   a1.Suburb ,

	   replace(a1.FirstName,'"','') ,

	   replace(a1.LastName,'"','')



end











GO
