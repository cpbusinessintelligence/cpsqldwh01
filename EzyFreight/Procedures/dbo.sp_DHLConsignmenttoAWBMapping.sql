SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DHLConsignmenttoAWBMapping] as

begin


     --'=====================================================================
    --' CP -Stored Procedure - sp_DHLConsignmenttoAWBMapping
    --' ---------------------------
    --' Purpose: DHLConsignmenttoAWBMapping-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 20 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 20/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Select convert(varchar(50),[AWBNumber]) AS AWBNumber,
	   c.Consignmentcode,
	   l.LabelNumber,
	   [StatusDateTime],
       [ScanEvent],
       [ContractorCode],
       case when [ExceptionReason]='' then  'From '+isnull(l1.[Location Area Name],'') else [ExceptionReason] end as ExceptionReason
	   from [dbo].[tblDHLStaging] s (NOLOCK)
	   left join [dbo].[tblDHLBarCodeImage] b (NOLOCK) on b.AWBCode=s.AWBNumber
	   left join [dbo].[DHLLocationcodes] l1 (NOLOCK) on l1.[Location Code]=[DHLLocationcode]
	   left join [dbo].[tblConsignment] c (NOLOCK) on c.ConsignmentID=b.ConsignmentID
	   left join [dbo].[tblItemLabel] l (NOLOCK) on l.ConsignmentID=c.ConsignmentID
	   and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
end
GO
GRANT EXECUTE
	ON [dbo].[sp_DHLConsignmenttoAWBMapping]
	TO [SSISUser]
GO
