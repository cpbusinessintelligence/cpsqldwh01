SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

						CREATE Proc [dbo].[sp_DeliveryReport] 
						(
						@StartDate Datetime,
						@EndDate Datetime,
						@AccountNumber Varchar(200)
						)

						As
						Begin
						
						SELECT   CONSIGNMENT.Consignmentid,
	            consignmentcode as CONSIGNMENTNUMBER,
			Labelnumber,
			st.StatusDescription [Latest Activity],
			case when cd_deliver_stamp is not null then 'Delivered'
			when cd_deliver_stamp is  null then cd_last_status
			end  [Gateway Status],
			Length,
			width,
			Height,
			Cubicweight,
			CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
            --,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
			,[ACCOUNTNUMBER]
			,case when isnull(a.companyname,'')='' then a.FirstName+' '+a.LastName else isnull(a.companyname,'') end as [SENDERNAME]
			--,case when isnull(a.companyname,'')='' then a.FirstName+' '+a.LastName else isnull(a.companyname,'')+','+a.FirstName+' '+a.LastName end as [SENDERNAME]
			--,case when c.companyid in (1339,1340) then isnull(a.CompanyName,'') else isnull(c.companyname,'')+'-'+'API' end as [SENDERNAME]
           --,isnull(c.companyname,'')+'-'+'API' as [SENDERNAME]
		   ,a.FirstName+' '+a.LastName as [SENDERADDRESS1]
           ,isnull(a.address1,'')+' '+isnull(a.address2,'') as [SENDERADDRESS2]
          ,case when isinternational=1 then (case when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
		                                          when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
												  else  I.Category COLLATE SQL_Latin1_General_CP1_CI_AS end)
                                       else a.suburb end AS [SENDERLOCATION]
           ,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [SENDERSTATE]
		  ,case when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' else a.PostCode end as [SENDERPOSTCODE]
          -- ,case when  isnull(s.StateCode,isnull(a.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then
		        --                            case when I.zone='Zone0' then '0100' when I.zone='Zone1' then '0101' when I.zone='Zone2' then '0102' when I.zone='Zone3' then '0103' when I.zone='Zone4' then '0104' when I.zone='Zone5' then '0105' else 'Unknown' end 
										--else a.postcode end	as [SENDERPOSTCODE]
		   ,case when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName else isnull(a1.companyname,'') end  as [RECEIVERNAME]
           ,isnull(a1.address1,'') as  [RECEIVERADDRESS1]
           ,isnull(a1.address2,'') as [RECEIVERADDRESS2]
           ,case when isinternational=1 then (case when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
		                                          when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
												  else  I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS end) 
										else a1.suburb end as [RECEIVERLOCATION]
           ,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [RECEIVERSTATE]
		   ,case when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' else a1.PostCode end as [RECEIVERPOSTCODE] 
          -- ,case when   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 
		        --                            case when I1.zone='Zone0' then '0100' when I1.zone='Zone1' then '0101' when I1.zone='Zone2' then '0102' when I1.zone='Zone3' then '0103' when I1.zone='Zone4' then '0104' when I1.zone='Zone5' then '0105' else 'Unknown' end 
										--else a1.postcode end	as [RECEIVERPOSTCODE] 
		   ,a1.FirstName AS [CONTACT]
		   ,a1.phone as [PHONENUMBER]
           ,SpecialInstruction as [SPECIALINSTRUCTIONS]
           ,NoOfItems as [TOTALLOGISTICSUNITS]
           ,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
           ,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
		   ,NetSubTotal AS [DECLAREDVALUE]
		   ,DangerousGoods AS [DANGEROUSGOODSFLAG] 
		   
		   into #temp1

	       from 
           [EzyFreight].[dbo].[tblConsignment] CONSIGNMENT  with (Nolock)
		   inner join tblitemlabel itm
		   On(CONSIGNMENT.Consignmentid =itm.consignmentid)
			                                              left join  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  left JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
														  	left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I ON I.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a.country   COLLATE SQL_Latin1_General_CP1_CI_AS
											                left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I1 ON I1.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a1.country   COLLATE SQL_Latin1_General_CP1_CI_AS
			                                             left JOIN [EzyFreight].[dbo].[tblcompanyusers] u on u.userid=CONSIGNMENT.userid
														 left JOIN [EzyFreight].[dbo].[tblcompany] c on c.companyid=u.companyid
														 left join [EzyFreight].[dbo].[tblstatus] st on CONSIGNMENT.consignmentstatus = st.statusID
														 
														left join cppledi.[dbo].[consignment] (nolock) con on con.cd_connote=consignmentcode
														 
														 where CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) between @StartDate and @EndDate
														
														--case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end  = '113075030'
														 
														 select * from #temp1 where  AccountNumber  = @AccountNumber
														 order by 1 


														 End


	--exec sp_DeliveryReport '03-06-2020','04-06-2020',113129415				
GO
GRANT EXECUTE
	ON [dbo].[sp_DeliveryReport]
	TO [ReportUser]
GO
