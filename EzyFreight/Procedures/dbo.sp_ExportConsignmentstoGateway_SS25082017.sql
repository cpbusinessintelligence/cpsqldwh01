SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_ExportConsignmentstoGateway_SS25082017]   as
begin
   
   
     --'=====================================================================
    --' CP -Stored Procedure - sp_ExportConsignmentstoGateway
    --' ---------------------------
    --' Purpose: ExportConsignmentstoGateway-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 11 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 11/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

--BEGIN TRAN
	Declare @Temp table(Consignmentid int,CONSIGNMENTNUMBER varchar(100),CONSIGNMENTDATE varchar(20),[MANIFESTFILENAME] varchar(50),[MANIFESTFILEDATE] date,[SERVICECODE] varchar(20),
	                    [ACCOUNTNUMBER] varchar(50),[SENDERNAME] varchar(200),[SENDERADDRESS1] varchar(500),[SENDERADDRESS2] varchar(500),[SENDERLOCATION] varchar(50),[SENDERSTATE] varchar(50),
						[SENDERPOSTCODE] varchar(20),[RECEIVERNAME] varchar(200),[RECEIVERADDRESS1] varchar(500),[RECEIVERADDRESS2] varchar(500),[RECEIVERLOCATION] varchar(50),[RECEIVERSTATE] varchar(50),
						[RECEIVERPOSTCODE] varchar(20),CONTACT varchar(50),phonenumber varchar(30),[PRIMARYREFERENCE] varchar(50),[RELEASEASN] varchar(50),[RETURNAUTHORITYNUMBER] varchar(50),
						[OTHERREFERENCE1] varchar(50),[OTHERREFERENCE2] varchar(50),[OTHERREFERENCE3] varchar(50),[OTHERREFERENCE4] varchar(50),[SPECIALINSTRUCTIONS] varchar(500),
						[TOTALLOGISTICSUNITS] int,[TOTALDEADWEIGHT] decimal(12,2),[TOTALVOLUME] decimal(12,4),[INSURANCECATEGORY] varchar(10),[DECLAREDVALUE] decimal(12,2),Testflag bit,
						[DANGEROUSGOODSFLAG] bit,[NOTBEFOREDATE] varchar(20),[NOTAFTERDATE] varchar(20))

						Insert into @Temp


							SELECT   CONSIGNMENT.Consignmentid,
	            consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
            ,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
           ,a.FirstName+' '+a.LastName as [SENDERNAME]
           ,a.address1 as [SENDERADDRESS1]
           ,a.address2 as [SENDERADDRESS2]
          ,case when isinternational=1 then (case when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
		                                          when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
												  else  I.Category COLLATE SQL_Latin1_General_CP1_CI_AS end)
                                       else a.suburb end AS [SENDERLOCATION]
           ,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [SENDERSTATE]
		  ,case when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' else a.PostCode end as [SENDERPOSTCODE]
          -- ,case when  isnull(s.StateCode,isnull(a.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then
		        --                            case when I.zone='Zone0' then '0100' when I.zone='Zone1' then '0101' when I.zone='Zone2' then '0102' when I.zone='Zone3' then '0103' when I.zone='Zone4' then '0104' when I.zone='Zone5' then '0105' else 'Unknown' end 
										--else a.postcode end	as [SENDERPOSTCODE]
		   ,case when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName else isnull(a1.companyname,'')+','+a1.FirstName+' '+a1.LastName end  as [RECEIVERNAME]
           ,a1.address1 as  [RECEIVERADDRESS1]
           ,a1.address2 as [RECEIVERADDRESS2]
           ,case when isinternational=1 then (case when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
		                                          when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
												  else  I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS end) 
										else a1.suburb end as [RECEIVERLOCATION]
           ,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [RECEIVERSTATE]
		   ,case when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' else a1.PostCode end as [RECEIVERPOSTCODE] 
          -- ,case when   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 
		        --                            case when I1.zone='Zone0' then '0100' when I1.zone='Zone1' then '0101' when I1.zone='Zone2' then '0102' when I1.zone='Zone3' then '0103' when I1.zone='Zone4' then '0104' when I1.zone='Zone5' then '0105' else 'Unknown' end 
										--else a1.postcode end	as [RECEIVERPOSTCODE] 
		   ,a1.FirstName AS [CONTACT]
		   ,a1.phone as [PHONENUMBER]
           ,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
           ,convert(varchar(50),'') as [RELEASEASN] 
           ,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
           ,convert(varchar(50),'') as [OTHERREFERENCE1]
           ,convert(varchar(50),'') as [OTHERREFERENCE2]
           ,convert(varchar(50),'') as [OTHERREFERENCE3]
           ,convert(varchar(50),'') as [OTHERREFERENCE4]
           ,SpecialInstruction as [SPECIALINSTRUCTIONS]
           ,NoOfItems as [TOTALLOGISTICSUNITS]
           ,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
           ,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
		   ,'' AS [INSURANCECATEGORY]
		   ,NetSubTotal AS [DECLAREDVALUE]
		   ,'0' AS [TESTFLAG]
		   ,DangerousGoods AS [DANGEROUSGOODSFLAG]
		   ,'' AS [NOTBEFOREDATE]
		   ,'' AS [NOTAFTERDATE]
	       from 
           [EzyFreight].[dbo].[tblConsignment] CONSIGNMENT
			                                              left join  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  left JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
														  	left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I ON I.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a.country   COLLATE SQL_Latin1_General_CP1_CI_AS
											                left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I1 ON I1.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a1.country   COLLATE SQL_Latin1_General_CP1_CI_AS

							  where 
														   isinternational=1 and isprocessed=1 
														  and EDIDataprocessed=0
														   and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
														  -- and convert(date,CONSIGNMENT.createddatetime) >='2015-09-01' 
														  and consignmentcode not in ('CPWEXP000000000','CPWSAV000000000','CPWEXP000000006','CPWEXP000000007','CPWEXP000000008')
														  and isnull(clientcode,'')<>'CPAPI'

Insert into @Temp


							SELECT   CONSIGNMENT.Consignmentid,
	            consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
            ,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
           ,isnull(c.companyname,'')+'-'+'API' as [SENDERNAME]
		   ,a.FirstName+' '+a.LastName as [SENDERADDRESS1]
           ,isnull(a.address1,'')+' '+isnull(a.address2,'') as [SENDERADDRESS2]
          ,case when isinternational=1 then (case when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
		                                          when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
												  else  I.Category COLLATE SQL_Latin1_General_CP1_CI_AS end)
                                       else a.suburb end AS [SENDERLOCATION]
           ,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [SENDERSTATE]
		  ,case when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' else a.PostCode end as [SENDERPOSTCODE]
          -- ,case when  isnull(s.StateCode,isnull(a.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then
		        --                            case when I.zone='Zone0' then '0100' when I.zone='Zone1' then '0101' when I.zone='Zone2' then '0102' when I.zone='Zone3' then '0103' when I.zone='Zone4' then '0104' when I.zone='Zone5' then '0105' else 'Unknown' end 
										--else a.postcode end	as [SENDERPOSTCODE]
		   ,case when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName else isnull(a1.companyname,'')+','+a1.FirstName+' '+a1.LastName end  as [RECEIVERNAME]
           ,a1.address1 as  [RECEIVERADDRESS1]
           ,a1.address2 as [RECEIVERADDRESS2]
           ,case when isinternational=1 then (case when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
		                                          when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
												  else  I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS end) 
										else a1.suburb end as [RECEIVERLOCATION]
           ,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [RECEIVERSTATE]
		   ,case when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' else a1.PostCode end as [RECEIVERPOSTCODE] 
          -- ,case when   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 
		        --                            case when I1.zone='Zone0' then '0100' when I1.zone='Zone1' then '0101' when I1.zone='Zone2' then '0102' when I1.zone='Zone3' then '0103' when I1.zone='Zone4' then '0104' when I1.zone='Zone5' then '0105' else 'Unknown' end 
										--else a1.postcode end	as [RECEIVERPOSTCODE] 
		   ,a1.FirstName AS [CONTACT]
		   ,a1.phone as [PHONENUMBER]
           ,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
           ,convert(varchar(50),'') as [RELEASEASN] 
           ,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
           ,convert(varchar(50),'') as [OTHERREFERENCE1]
           ,convert(varchar(50),'') as [OTHERREFERENCE2]
           ,convert(varchar(50),'') as [OTHERREFERENCE3]
           ,convert(varchar(50),'') as [OTHERREFERENCE4]
           ,SpecialInstruction as [SPECIALINSTRUCTIONS]
           ,NoOfItems as [TOTALLOGISTICSUNITS]
           ,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
           ,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
		   ,'' AS [INSURANCECATEGORY]
		   ,NetSubTotal AS [DECLAREDVALUE]
		   ,'0' AS [TESTFLAG]
		   ,DangerousGoods AS [DANGEROUSGOODSFLAG]
		   ,'' AS [NOTBEFOREDATE]
		   ,'' AS [NOTAFTERDATE]
	       from 
           [EzyFreight].[dbo].[tblConsignment] CONSIGNMENT
			                                              left join  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  left JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
														  	left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I ON I.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a.country   COLLATE SQL_Latin1_General_CP1_CI_AS
											                left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I1 ON I1.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a1.country   COLLATE SQL_Latin1_General_CP1_CI_AS
			                                             left JOIN [EzyFreight].[dbo].[tblcompanyusers] u on u.userid=CONSIGNMENT.userid
														 left JOIN [EzyFreight].[dbo].[tblcompany] c on c.companyid=u.companyid

														
														 ----where isnull(clientcode,'')='CPAPI'
														  where 
														   isinternational=1 and isprocessed=1 
														  and EDIDataprocessed=0
														   and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
														  -- and convert(date,CONSIGNMENT.createddatetime) >='2015-09-01' 
														  and consignmentcode not in ('CPWEXP000000000','CPWSAV000000000','CPWEXP000000006','CPWEXP000000007','CPWEXP000000008')
														   and isnull(clientcode,'')='CPAPI'


    Insert into @Temp

	SELECT    CONSIGNMENT.Consignmentid,
	            consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
            ,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
           ,a.FirstName+' '+a.LastName as [SENDERNAME]
           ,a.address1 as [SENDERADDRESS1]
           ,a.address2 as [SENDERADDRESS2]
           ,a.suburb AS [SENDERLOCATION]
           ,ISNULL(a.stateName,'') as [SENDERSTATE]
           ,a.Postcode as [SENDERPOSTCODE]
          ,case when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName else isnull(a1.companyname,'')+','+a1.FirstName+' '+a1.LastName end as [RECEIVERNAME]
           ,a1.address1 as  [RECEIVERADDRESS1]
           ,a1.address2 as [RECEIVERADDRESS2]
           ,a1.suburb as [RECEIVERLOCATION]
           ,ISNULL(a1.stateName,'') as [RECEIVERSTATE]
           ,a1.Postcode as [RECEIVERPOSTCODE]
		   ,a1.FirstName AS [CONTACT]
		   ,a1.phone as [PHONENUMBER]
           ,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
           ,convert(varchar(50),'') as [RELEASEASN] 
           ,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
           ,convert(varchar(50),'') as [OTHERREFERENCE1]
           ,convert(varchar(50),'') as [OTHERREFERENCE2]
           ,convert(varchar(50),'') as [OTHERREFERENCE3]
           ,convert(varchar(50),'') as [OTHERREFERENCE4]
           ,SpecialInstruction as [SPECIALINSTRUCTIONS]
           ,NoOfItems as [TOTALLOGISTICSUNITS]
           ,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
           ,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
		   ,'' AS [INSURANCECATEGORY]
		   ,NetSubTotal AS [DECLAREDVALUE]
		   ,'0' AS [TESTFLAG]
		   ,DangerousGoods AS [DANGEROUSGOODSFLAG]
		   ,'' AS [NOTBEFOREDATE]
		   ,'' AS [NOTAFTERDATE]
	       from 
           [EzyFreight].[dbo].[tblConsignment] CONSIGNMENT
			                                              left join  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  left JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid

														 
											
														 where  
														 EDIDataprocessed=0 
														 and CONSIGNMENT.isprocessed=1  and CONSIGNMENT.isinternational=0
														  and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
	                                                   and isnull(clientcode,'')<>'CPAPI'

	
    Insert into @Temp

	SELECT    CONSIGNMENT.Consignmentid,
	            consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
            ,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
           ,isnull(c.companyname,'')+'-'+'API' as [SENDERNAME]
		   ,a.FirstName+' '+a.LastName as [SENDERADDRESS1]
           ,isnull(a.address1,'')+' '+isnull(a.address2,'') as [SENDERADDRESS2]
           ,a.suburb AS [SENDERLOCATION]
           ,ISNULL(a.stateName,'') as [SENDERSTATE]
           ,a.Postcode as [SENDERPOSTCODE]
          ,case when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName else isnull(a1.companyname,'')+','+a1.FirstName+' '+a1.LastName end as [RECEIVERNAME]
           ,a1.address1 as  [RECEIVERADDRESS1]
           ,a1.address2 as [RECEIVERADDRESS2]
           ,a1.suburb as [RECEIVERLOCATION]
           ,ISNULL(a1.stateName,'') as [RECEIVERSTATE]
           ,a1.Postcode as [RECEIVERPOSTCODE]
		   ,a1.FirstName AS [CONTACT]
		   ,a1.phone as [PHONENUMBER]
           ,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
           ,convert(varchar(50),'') as [RELEASEASN] 
           ,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
           ,convert(varchar(50),'') as [OTHERREFERENCE1]
           ,convert(varchar(50),'') as [OTHERREFERENCE2]
           ,convert(varchar(50),'') as [OTHERREFERENCE3]
           ,convert(varchar(50),'') as [OTHERREFERENCE4]
           ,SpecialInstruction as [SPECIALINSTRUCTIONS]
           ,NoOfItems as [TOTALLOGISTICSUNITS]
           ,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
           ,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
		   ,'' AS [INSURANCECATEGORY]
		   ,NetSubTotal AS [DECLAREDVALUE]
		   ,'0' AS [TESTFLAG]
		   ,DangerousGoods AS [DANGEROUSGOODSFLAG]
		   ,'' AS [NOTBEFOREDATE]
		   ,'' AS [NOTAFTERDATE]
	       from 
           [EzyFreight].[dbo].[tblConsignment] CONSIGNMENT
			                                              left join  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  left JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
											                        left JOIN [EzyFreight].[dbo].[tblcompanyusers] u on u.userid=CONSIGNMENT.userid
														 left JOIN [EzyFreight].[dbo].[tblcompany] c on c.companyid=u.companyid
														

	
	-- where isnull(clientcode,'')='CPAPI'
														 where 
														  EDIDataprocessed=0 
														 and CONSIGNMENT.isprocessed=1  and CONSIGNMENT.isinternational=0
														  and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
	                                                        and isnull(clientcode,'')='CPAPI'
	--and CONSIGNMENT.isinternational=0                    


	--select * from  [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] where isprocessed=0 and isinternational=0


	
declare @xmlop xml
declare @varcharop varchar(max)
set @xmlop=  
            (SELECT
			 CONSIGNMENT.CONSIGNMENTNUMBER
			,CONSIGNMENTDATE
			,MANIFESTFILENAME
			,MANIFESTFILEDATE
			,SERVICECODE
            ,ACCOUNTNUMBER--should be 999999999?
           ,SENDERNAME
           ,SENDERADDRESS1
           ,SENDERADDRESS2
           ,SENDERLOCATION
           ,SENDERSTATE
           ,SENDERPOSTCODE
           ,RECEIVERNAME
           ,RECEIVERADDRESS1
           ,RECEIVERADDRESS2
           ,RECEIVERLOCATION
           ,RECEIVERSTATE
           ,RECEIVERPOSTCODE
		   ,CONTACT
		   ,PHONENUMBER
           ,PRIMARYREFERENCE
           ,RELEASEASN
           ,RETURNAUTHORITYNUMBER
           ,OTHERREFERENCE1
           ,OTHERREFERENCE2
           ,OTHERREFERENCE3
           ,OTHERREFERENCE4
           ,SPECIALINSTRUCTIONS
           ,TOTALLOGISTICSUNITS
           ,TOTALDEADWEIGHT--Declaredweight/Measuredweight
           ,TOTALVOLUME--Declaredvolume/measurevolume
		   ,INSURANCECATEGORY
		   ,DECLAREDVALUE
		   ,TESTFLAG
		   ,DANGEROUSGOODSFLAG
		   ,NOTBEFOREDATE
		   ,NOTAFTERDATE
           ,ITEM.LABELNUMBER
		   ,'CTN' AS [UNITTYPE]
		   ,'0' AS [LABELTESTFLAG]
		   ,'1' AS [INTERNALLABEL]
		   ,'0' AS [LINKLABEL]

		   --Date?
			 FROM @temp CONSIGNMENT  join  [EzyFreight].[dbo].[tblItemLabel] ITEM on ITEM.consignmentid=CONSIGNMENT.consignmentid
			                                            -- FOR XML PATH('CPPLPODManifest'));


														  for xml AUTO ,ROOT('CPPLPODManifest'), ELEMENTS );

--														 -- for XML AUTO ,ROOT('CPPLPODManifest'),ELEMENTS XSINIL)
--														 -- select @xmlop;
--														   --output to 'c:\\sales.xml' FORMAT TEXT;


	--													   SET @xmlop = (SELECT 
	--(SELECT 
	--		 CONSIGNMENT.CONSIGNMENTNUMBER
	--		,CONSIGNMENTDATE
	--		,MANIFESTFILENAME
	--		,MANIFESTFILEDATE
	--		,SERVICECODE
 --           ,ACCOUNTNUMBER--should be 999999999?
 --          ,SENDERNAME
 --          ,SENDERADDRESS1
 --          ,SENDERADDRESS2
 --          ,SENDERLOCATION
 --          ,SENDERSTATE
 --          ,SENDERPOSTCODE
 --          ,RECEIVERNAME
 --          ,RECEIVERADDRESS1
 --          ,RECEIVERADDRESS2
 --          ,RECEIVERLOCATION
 --          ,RECEIVERSTATE
 --          ,RECEIVERPOSTCODE
	--	   ,CONTACT
	--	   ,PHONENUMBER
 --          ,PRIMARYREFERENCE
 --          ,RELEASEASN
 --          ,RETURNAUTHORITYNUMBER
 --          ,OTHERREFERENCE1
 --          ,OTHERREFERENCE2
 --          ,OTHERREFERENCE3
 --          ,OTHERREFERENCE4
 --          ,SPECIALINSTRUCTIONS
 --          ,TOTALLOGISTICSUNITS
 --          ,TOTALDEADWEIGHT--Declaredweight/Measuredweight
 --          ,TOTALVOLUME--Declaredvolume/measurevolume
	--	   ,INSURANCECATEGORY
	--	   ,DECLAREDVALUE
	--	   ,TESTFLAG
	--	   ,DANGEROUSGOODSFLAG
	--	   ,NOTBEFOREDATE
	--	   ,NOTAFTERDATE
 --          ,ITEM.LABELNUMBER
	--	   ,'CTN' AS [UNITTYPE]
	--	   ,'0' AS [LABELTESTFLAG]
	--	   ,'1' AS [INTERNALLABEL]
	--	   ,'0' AS [LINKLABEL]

	--	   --Date?
	--		 FROM @temp CONSIGNMENT left join  [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] ITEM on ITEM.consignmentid=CONSIGNMENT.consignmentid
	--		 FOR XML PATH('CONSIGNMENT'), TYPE)
	--FOR XML PATH('CPPLPODManifest'));


--Select * from [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] c where consignmentid in (Select Consignmentid from @Temp)
----set @varcharop=convert(varchar(max),@xmlop)
----select @varcharop as col1
	--bcp @xmlop queryout c:\department.txt -c –T	

	
--update  [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] set EDIDataprocessed=1 from @temp t join [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] c on t.Consignmentid=c.consignmentid
select @xmlop as col1
update   [EzyFreight].[dbo].[tblConsignment] set EDIDataprocessed=1 where consignmentid in (Select c.Consignmentid from @Temp  c join  [EzyFreight].[dbo].[tblItemLabel] ITEM on ITEM.consignmentid=c.consignmentid)


--COMMIT TRAN

	end			

GO
