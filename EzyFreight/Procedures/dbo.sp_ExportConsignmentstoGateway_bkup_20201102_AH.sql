SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[sp_ExportConsignmentstoGateway_bkup_20201102_AH]
AS
BEGIN  
    --'=====================================================================
    --' CP -Stored Procedure - sp_ExportConsignmentstoGateway
    --' ---------------------------
    --' Purpose: ExportConsignmentstoGateway-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 11 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 11/07/2015    AK      1.00    Created the procedure                            
	--' 25/08/2017    SS              Added Sender name fix for Fleshlight
	--  12/02/2018    SS              Added the fix for failing validation
	--	19/06/2020	  PV			  Added Booking details 	
    --'=====================================================================

--BEGIN TRAN
	Declare @Temp table
	(
		Consignmentid int,CONSIGNMENTNUMBER varchar(100),CONSIGNMENTDATE varchar(20),[MANIFESTFILENAME] varchar(50),[MANIFESTFILEDATE] date,[SERVICECODE] varchar(20),
	    [ACCOUNTNUMBER] varchar(50),[SENDERNAME] varchar(200),[SENDERADDRESS1] varchar(500),[SENDERADDRESS2] varchar(500),[SENDERLOCATION] varchar(50),[SENDERSTATE] varchar(50),
		[SENDERPOSTCODE] varchar(20),[RECEIVERNAME] varchar(200),[RECEIVERADDRESS1] varchar(500),[RECEIVERADDRESS2] varchar(500),[RECEIVERLOCATION] varchar(50),[RECEIVERSTATE] varchar(50),
		[RECEIVERPOSTCODE] varchar(20),CONTACT varchar(50),phonenumber varchar(30),[PRIMARYREFERENCE] varchar(50),[RELEASEASN] varchar(50),[RETURNAUTHORITYNUMBER] varchar(50),
		[OTHERREFERENCE1] varchar(50),[OTHERREFERENCE2] varchar(50),[OTHERREFERENCE3] varchar(50),[OTHERREFERENCE4] varchar(50),[SPECIALINSTRUCTIONS] varchar(500),
		[TOTALLOGISTICSUNITS] int,[TOTALDEADWEIGHT] decimal(12,2),[TOTALVOLUME] decimal(12,4),[INSURANCECATEGORY] varchar(10),[DECLAREDVALUE] decimal(12,2),Testflag bit,
		[DANGEROUSGOODSFLAG] bit,[NOTBEFOREDATE] varchar(20),[NOTAFTERDATE] varchar(20),[ATLFLAG] int, BOOKINGID varchar(20), BOOKINGBRANCH varchar(500), BOOKINGDATE varchar(10), UNIQUECPPLCODE nvarchar(50)
	)

	Insert Into @Temp
	Select	CONSIGNMENT.Consignmentid
			,consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
			,case	when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') 
					else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
			,a.FirstName+' '+a.LastName as [SENDERNAME]
			,isnull(a.address1,'') as [SENDERADDRESS1]
			,isnull(a.address2,'') as [SENDERADDRESS2]
			,case	when isinternational=1 then 
						(case	when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
								when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
								else  I.Category COLLATE SQL_Latin1_General_CP1_CI_AS end)
					else a.suburb end AS [SENDERLOCATION]
			,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [SENDERSTATE]
			,case	when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' 
					else a.PostCode end as [SENDERPOSTCODE]
			-- ,case when  isnull(s.StateCode,isnull(a.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then
			--                            case when I.zone='Zone0' then '0100' when I.zone='Zone1' then '0101' when I.zone='Zone2' then '0102' when I.zone='Zone3' then '0103' when I.zone='Zone4' then '0104' when I.zone='Zone5' then '0105' else 'Unknown' end 
			--else a.postcode end	as [SENDERPOSTCODE]
			,case	when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName 
					else isnull(a1.companyname,'')+','+a1.FirstName+' '+a1.LastName end  as [RECEIVERNAME]
			,isnull(a1.address1,'') as  [RECEIVERADDRESS1]
			,isnull(a1.address2,'') as [RECEIVERADDRESS2]
			,case	when isinternational=1 then 
						(case	when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
								when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
								else  I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS end) 
					else a1.suburb end as [RECEIVERLOCATION]
			,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [RECEIVERSTATE]
			,case	when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' 
					else a1.PostCode end as [RECEIVERPOSTCODE] 
			-- ,case when   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 
			--                            case when I1.zone='Zone0' then '0100' when I1.zone='Zone1' then '0101' when I1.zone='Zone2' then '0102' when I1.zone='Zone3' then '0103' when I1.zone='Zone4' then '0104' when I1.zone='Zone5' then '0105' else 'Unknown' end 
			--else a1.postcode end	as [RECEIVERPOSTCODE] 
			,a1.FirstName AS [CONTACT]
			,a1.phone as [PHONENUMBER]
			,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
			,convert(varchar(50),'') as [RELEASEASN] 
			,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
			,convert(varchar(50),'') as [OTHERREFERENCE1]
			,convert(varchar(50),'') as [OTHERREFERENCE2]
			,convert(varchar(50),'') as [OTHERREFERENCE3]
			,convert(varchar(50),'') as [OTHERREFERENCE4]
			,SpecialInstruction as [SPECIALINSTRUCTIONS]
			,NoOfItems as [TOTALLOGISTICSUNITS]
			,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
			,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
			,'' AS [INSURANCECATEGORY]
			,NetSubTotal AS [DECLAREDVALUE]
			,'0' AS [TESTFLAG]
			,DangerousGoods AS [DANGEROUSGOODSFLAG]
			,'' AS [NOTBEFOREDATE]
			,'' AS [NOTAFTERDATE]
			,IsATl AS [ATLFLAG]
			,IsNull(tb.BookingRefNo,'') [BookingID]
			,[dbo].[fn_GetBranch_ByPostcode](tb.PickupPostCode)  [BookingBranch]
			,IsNull(Convert(varchar(10),tb.CreatedDateTime,120),'') [BookingDate]
			--,Convert(varchar(10),'') As [UNIQUECPPLCODE]
			,IsNull(c.UniqueCpplCode,'')  As [UNIQUECPPLCODE]
	FROM 
		[EzyFreight].[dbo].[tblConsignment] CONSIGNMENT  with (Nolock)
			LEFT JOIN  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
			LEFT JOIN [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
			LEFT JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
			LEFT JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I ON I.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a.country   COLLATE SQL_Latin1_General_CP1_CI_AS
			LEFT JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I1 ON I1.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a1.country   COLLATE SQL_Latin1_General_CP1_CI_AS
			LEFT JOIN [EzyFreight].[dbo].[tblBooking] tb On tb.ConsignmentID = CONSIGNMENT.ConsignmentID
			LEFT JOIN [EzyFreight].[dbo].[tblcompanyusers] u ON u.userid=CONSIGNMENT.userid
			LEFT JOIN [EzyFreight].[dbo].[tblcompany] c ON c.companyid=u.companyid			
	WHERE 
			isinternational=1 and CONSIGNMENT.isprocessed=1 
			and EDIDataprocessed=0
			and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
			-- and convert(date,CONSIGNMENT.createddatetime) >='2015-09-01' 
			and consignmentcode not in ('CPWEXP000000000','CPWSAV000000000','CPWEXP000000006','CPWEXP000000007','CPWEXP000000008')
			and isnull(clientcode,'')<>'CPAPI'
	
	-----------
	Insert into @Temp

	SELECT	CONSIGNMENT.Consignmentid
			,consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
			,case	when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') 
					else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
			,case	when isnull(a.companyname,'')='' then a.FirstName+' '+a.LastName 
					else isnull(a.companyname,'') end as [SENDERNAME]
			--,case when isnull(a.companyname,'')='' then a.FirstName+' '+a.LastName else isnull(a.companyname,'')+','+a.FirstName+' '+a.LastName end as [SENDERNAME]
			--,case when c.companyid in (1339,1340) then isnull(a.CompanyName,'') else isnull(c.companyname,'')+'-'+'API' end as [SENDERNAME]
			--,isnull(c.companyname,'')+'-'+'API' as [SENDERNAME]
			,a.FirstName+' '+a.LastName as [SENDERADDRESS1]
			,isnull(a.address1,'')+' '+isnull(a.address2,'') as [SENDERADDRESS2]
			,case	when isinternational=1 then 
						(case	when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
								when I.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
								else  I.Category COLLATE SQL_Latin1_General_CP1_CI_AS end)
					else a.suburb end AS [SENDERLOCATION]
			,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [SENDERSTATE]
			,case	when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' 
					when I.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' 
					else a.PostCode end as [SENDERPOSTCODE]
			-- ,case when  isnull(s.StateCode,isnull(a.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then
			--                            case when I.zone='Zone0' then '0100' when I.zone='Zone1' then '0101' when I.zone='Zone2' then '0102' when I.zone='Zone3' then '0103' when I.zone='Zone4' then '0104' when I.zone='Zone5' then '0105' else 'Unknown' end 
			--else a.postcode end	as [SENDERPOSTCODE]
			,case	when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName 
					else isnull(a1.companyname,'') end  as [RECEIVERNAME]
			,isnull(a1.address1,'') as  [RECEIVERADDRESS1]
			,isnull(a1.address2,'') as [RECEIVERADDRESS2]
			,case	when isinternational=1 then 
						(case	when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row1' then  'REST OF THE WORLD 1'  
								when I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS = 'Row2' then  'REST OF THE WORLD 2' 
								else  I1.Category COLLATE SQL_Latin1_General_CP1_CI_AS end) 
					else a1.suburb end as [RECEIVERLOCATION]
			,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [RECEIVERSTATE]
			,case	when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS ='Zone0' then '100' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone1' then '101' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone2' then '102' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone3' then '103' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone4' then '104' 
					when I1.zone COLLATE SQL_Latin1_General_CP1_CI_AS='Zone5' then '105' 
					else a1.PostCode end as [RECEIVERPOSTCODE] 
			-- ,case when   isnull(s1.StateCode,isnull(a1.statename,'Unknown')) not in ('ACT','NSW','WA','VIC','QLD','NT','SA','Unknown') then 
			--                            case when I1.zone='Zone0' then '0100' when I1.zone='Zone1' then '0101' when I1.zone='Zone2' then '0102' when I1.zone='Zone3' then '0103' when I1.zone='Zone4' then '0104' when I1.zone='Zone5' then '0105' else 'Unknown' end 
			--else a1.postcode end	as [RECEIVERPOSTCODE] 
			,a1.FirstName AS [CONTACT]
			,a1.phone as [PHONENUMBER]
			,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
			,convert(varchar(50),'') as [RELEASEASN] 
			,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
			,convert(varchar(50),'') as [OTHERREFERENCE1]
			,convert(varchar(50),'') as [OTHERREFERENCE2]
			,convert(varchar(50),'') as [OTHERREFERENCE3]
			,convert(varchar(50),'') as [OTHERREFERENCE4]
			,SpecialInstruction as [SPECIALINSTRUCTIONS]
			,NoOfItems as [TOTALLOGISTICSUNITS]
			,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
			,case when isinternational=0 then Totalvolume/250 else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
			,'' AS [INSURANCECATEGORY]
			,NetSubTotal AS [DECLAREDVALUE]
			,'0' AS [TESTFLAG]
			,DangerousGoods AS [DANGEROUSGOODSFLAG]
			,'' AS [NOTBEFOREDATE]
			,'' AS [NOTAFTERDATE]
			,IsATl AS [ATLFLAG]
			,IsNull(tb.BookingRefNo,'') [BookingID]
			,[dbo].[fn_GetBranch_ByPostcode](tb.PickupPostCode)  [BookingBranch]
			,IsNull(Convert(varchar(10),tb.CreatedDateTime,120),'') [BookingDate]
			--,Convert(varchar(10),'') As [UNIQUECPPLCODE]
			,IsNull(c.UniqueCpplCode,'')  As [UNIQUECPPLCODE]
	FROM
		[EzyFreight].[dbo].[tblConsignment] CONSIGNMENT  WITH (NOLOCK)
			LEFT JOIN [EzyFreight].[dbo].[tblAddress] a ON a.addressid=pickupid
			LEFT JOIN [EzyFreight].[dbo].[tblAddress] a1 ON a1.addressid=destinationid
			LEFT JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t ON t.ConsignmentID=CONSIGNMENT.consignmentid
			LEFT JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I ON I.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a.country   COLLATE SQL_Latin1_General_CP1_CI_AS
			LEFT JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I1 ON I1.countryname COLLATE SQL_Latin1_General_CP1_CI_AS=a1.country   COLLATE SQL_Latin1_General_CP1_CI_AS
			LEFT JOIN [EzyFreight].[dbo].[tblcompanyusers] u ON u.userid=CONSIGNMENT.userid
			LEFT JOIN [EzyFreight].[dbo].[tblcompany] c ON c.companyid=u.companyid
			LEFT JOIN [EzyFreight].[dbo].[tblBooking] tb ON tb.ConsignmentID = CONSIGNMENT.ConsignmentID
			--LEFT JOIN [CpplEDI].[dbo].[Gatewayuniquecode_updated] GU ON ltrim(GU.Account) = c.AccountNumber								
		----where isnull(clientcode,'')='CPAPI'
	WHERE
			isinternational=1 and CONSIGNMENT.isprocessed=1 
			and EDIDataprocessed=0
			and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
			-- and convert(date,CONSIGNMENT.createddatetime) >='2015-09-01' 
			and consignmentcode not in ('CPWAGVBK000054910','CPWAGVP5000012073','CPWAGVBK000054910','CPWDS3000000451','CPWNPU000061612','CPWAGVBK000068178','CPWAGSBK000084115','CPWAGSP5000047229','CPWAGSP5000047229','CPWAGSBK000084115','CPWAGVP5000012073','CPWAGVBK000054910','CPWAGSP5000047229','CPWASP25000090873','CPWASP25000091010') 
			and isnull(clientcode,'')='CPAPI'
	
	---------
    Insert into @Temp

	SELECT
			CONSIGNMENT.Consignmentid,
			consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE]
			--,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
			, Case	when AccountNumber is null then [dbo].[fn_GetParameter]( 'Domestic') 
					else AccountNumber end as [ACCOUNTNUMBER]
			,isnull(a.FirstName,'')+' '+isnull(a.LastName,'') as [SENDERNAME]
			,isnull(a.address1,'') as [SENDERADDRESS1]
			,isnull(a.address2,'') as [SENDERADDRESS2]
			,isnull(a.suburb,'') AS [SENDERLOCATION]
			,ISNULL(a.stateName,'') as [SENDERSTATE]
			,a.Postcode as [SENDERPOSTCODE]
			,case	when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName 
					else isnull(a1.companyname,'')+','+a1.FirstName+' '+a1.LastName end as [RECEIVERNAME]
			,isnull(a1.address1,'') as  [RECEIVERADDRESS1]
			,isnull(a1.address2,'') as [RECEIVERADDRESS2]
			,a1.suburb as [RECEIVERLOCATION]
			,ISNULL(a1.stateName,'') as [RECEIVERSTATE]
			,a1.Postcode as [RECEIVERPOSTCODE]
			,a1.FirstName AS [CONTACT]
			,a1.phone as [PHONENUMBER]
			,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
			,convert(varchar(50),'') as [RELEASEASN] 
			,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
			,convert(varchar(50),'') as [OTHERREFERENCE1]
			,convert(varchar(50),'') as [OTHERREFERENCE2]
			,convert(varchar(50),'') as [OTHERREFERENCE3]
			,convert(varchar(50),'') as [OTHERREFERENCE4]
			,SpecialInstruction as [SPECIALINSTRUCTIONS]
			,NoOfItems as [TOTALLOGISTICSUNITS]
			,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
			,case	when isinternational=0 then Totalvolume/250 
					else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
			,'' AS [INSURANCECATEGORY]
			,NetSubTotal AS [DECLAREDVALUE]
			,'0' AS [TESTFLAG]
			,DangerousGoods AS [DANGEROUSGOODSFLAG]
			,'' AS [NOTBEFOREDATE]
			,'' AS [NOTAFTERDATE]
			,IsATl AS [ATLFLAG]
			,IsNull(tb.BookingRefNo,'') [BookingID]
			,[dbo].[fn_GetBranch_ByPostcode](tb.PickupPostCode)  [BookingBranch]
			,IsNull(Convert(varchar(10),tb.CreatedDateTime,120),'') [BookingDate]
			--,Convert(varchar(10),'') As [UNIQUECPPLCODE]
			,IsNull(c.UniqueCpplCode,'')  As [UNIQUECPPLCODE]
	FROM 
		[EzyFreight].[dbo].[tblConsignment] CONSIGNMENT With (Nolock)
			LEFT JOIN [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
			LEFT JOIN [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
			LEFT JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
			LEFT JOIN [EzyFreight].[dbo].[tblcompanyusers] u on u.userid=CONSIGNMENT.userid
			LEFT JOIN [EzyFreight].[dbo].[tblcompany] c on c.companyid=u.companyid
			LEFT JOIN [EzyFreight].[dbo].[tblBooking] tb On tb.ConsignmentID = CONSIGNMENT.ConsignmentID			
			--LEFT JOIN [CpplEDI].[dbo].[Gatewayuniquecode_updated] GU ON ltrim(GU.Account) = c.AccountNumber
	WHERE
			EDIDataprocessed=0 
			and CONSIGNMENT.isprocessed=1  and CONSIGNMENT.isinternational=0
			and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
			and isnull(clientcode,'')<>'CPAPI'
	
	-------			
	Insert into @Temp
	SELECT    
			CONSIGNMENT.Consignmentid
			,consignmentcode as CONSIGNMENTNUMBER
			,CONVERT(varchar(20), CONVERT(date, CONSIGNMENT.createddatetime), 120) AS [CONSIGNMENTDATE]
			,'Test' AS [MANIFESTFILENAME]
			,convert(date,getdate()) aS [MANIFESTFILEDATE]
			,isnull(RateCardId,'') AS [SERVICECODE] 
			--,case when isinternational=0 then [dbo].[fn_GetParameter]( 'Domestic') else  [dbo].[fn_GetParameter]( 'International') end as [ACCOUNTNUMBER]--should be 999999999?
			,case	when AccountNumber is null then [dbo].[fn_GetParameter]( 'Domestic') 
					else AccountNumber end as [ACCOUNTNUMBER]
			--,case when c.companyid in (1339,1340) then isnull(a.CompanyName,'') else isnull(c.companyname,'')+'-'+'API' end as [SENDERNAME]
			--,isnull(c.companyname,'')+'-'+'API' as [SENDERNAME]
			,case	when isnull(a.companyname,'')='' then a.FirstName+' '+a.LastName 
					else isnull(a.companyname,'') end as [SENDERNAME]
			,a.FirstName+' '+a.LastName as [SENDERADDRESS1]
			,isnull(a.address1,'')+' '+isnull(a.address2,'') as [SENDERADDRESS2]
			,a.suburb AS [SENDERLOCATION]
			,ISNULL(a.stateName,'') as [SENDERSTATE]
			,a.Postcode as [SENDERPOSTCODE]
			,case	when isnull(a1.companyname,'')='' then a1.FirstName+' '+a1.LastName 
					else isnull(a1.companyname,'') end as [RECEIVERNAME]
			,isnull(a1.address1,'') as  [RECEIVERADDRESS1]
			,isnull(a1.address2,'') as [RECEIVERADDRESS2]
			,isnull(a1.suburb,'') as [RECEIVERLOCATION]
			,ISNULL(a1.stateName,'') as [RECEIVERSTATE]
			,a1.Postcode as [RECEIVERPOSTCODE]
			,a1.FirstName AS [CONTACT]
			,a1.phone as [PHONENUMBER]
			,ISNULL(t.AWBCODE,'') as [PRIMARYREFERENCE]
			,convert(varchar(50),'') as [RELEASEASN] 
			,convert(varchar(50),'') as [RETURNAUTHORITYNUMBER]
			,convert(varchar(50),'') as [OTHERREFERENCE1]
			,convert(varchar(50),'') as [OTHERREFERENCE2]
			,convert(varchar(50),'') as [OTHERREFERENCE3]
			,convert(varchar(50),'') as [OTHERREFERENCE4]
			,SpecialInstruction as [SPECIALINSTRUCTIONS]
			,NoOfItems as [TOTALLOGISTICSUNITS]
			,Totalweight as [TOTALDEADWEIGHT]--Declaredweight/Measuredweight
			,case	when isinternational=0 then Totalvolume/250 
					else Totalvolume/200 end as [TOTALVOLUME]--Declaredvolume/measurevolume
			,'' AS [INSURANCECATEGORY]
			,NetSubTotal AS [DECLAREDVALUE]
			,'0' AS [TESTFLAG]
			,DangerousGoods AS [DANGEROUSGOODSFLAG]
			,'' AS [NOTBEFOREDATE]
			,'' AS [NOTAFTERDATE]
			,IsATl AS [ATLFLAG]
			,IsNull(tb.BookingRefNo,'') [BookingID]
			,[dbo].[fn_GetBranch_ByPostcode](tb.PickupPostCode)  [BookingBranch]
			,IsNull(Convert(varchar(10),tb.CreatedDateTime,120),'') [BookingDate]
			--,Convert(varchar(10),'') As [UNIQUECPPLCODE]
			,IsNull(c.UniqueCpplCode,'')  As [UNIQUECPPLCODE]
	FROM
		[EzyFreight].[dbo].[tblConsignment] CONSIGNMENT  with (Nolock)
			LEFT JOIN [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
			LEFT JOIN [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
			LEFT JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
			LEFT JOIN [EzyFreight].[dbo].[tblDHLBarCodeImage] t on t.ConsignmentID=CONSIGNMENT.consignmentid
			LEFT JOIN [EzyFreight].[dbo].[tblcompanyusers] u on u.userid=CONSIGNMENT.userid
			LEFT JOIN [EzyFreight].[dbo].[tblcompany] c on c.companyid=u.companyid
			LEFT JOIN [EzyFreight].[dbo].[tblBooking] tb On tb.ConsignmentID = CONSIGNMENT.ConsignmentID			
			--LEFT JOIN [CpplEDI].[dbo].[Gatewayuniquecode_updated] GU ON ltrim(GU.Account) = c.AccountNumber
	WHERE 
			EDIDataprocessed=0 
			and CONSIGNMENT.isprocessed=1  and CONSIGNMENT.isinternational=0
			and (case when convert(date,CONSIGNMENT.createddatetime)>'2016-01-31' then CONSIGNMENT.isprocessed else 1 end)=1
			and isnull(clientcode,'')='CPAPI'

------------
	Insert into dbo.TblWebsiteFailedValidationLog(ConsignmentNumber,IsProcessed)
	Select CONSIGNMENTNUMBER,0 
	From @temp
	Where  RECEIVERNAME = ''  or RECEIVERADDRESS1 = '' or RECEIVERLOCATION  = ''
			-- or RECEIVERSTATE  = ''
			or RECEIVERPOSTCODE  = ''

-----------
	Delete From @temp 
	Where  RECEIVERNAME = ''  or RECEIVERADDRESS1 = '' or RECEIVERLOCATION = ''
           --or RECEIVERSTATE = ''
           or RECEIVERPOSTCODE  = ''

----------
	UPDATE @temp SET RECEIVERNAME = REPLACE( RECEIVERNAME, CHAR(0x0b), '' )
	
---------
	Declare @xmlop xml
	Declare @varcharop varchar(max)
	
	Set @xmlop	=  
		(	SELECT 
				CONSIGNMENT.CONSIGNMENTNUMBER
				,CONSIGNMENTDATE
				,MANIFESTFILENAME
				,MANIFESTFILEDATE
				,SERVICECODE
				,ACCOUNTNUMBER--should be 999999999?
				,SENDERNAME
				,SENDERADDRESS1
				,SENDERADDRESS2
				,SENDERLOCATION
				,SENDERSTATE
				,SENDERPOSTCODE
				,RECEIVERNAME
				,RECEIVERADDRESS1
				,RECEIVERADDRESS2
				,RECEIVERLOCATION
				,RECEIVERSTATE
				,RECEIVERPOSTCODE
				,CONTACT
				,PHONENUMBER
				,PRIMARYREFERENCE
				,RELEASEASN
				,RETURNAUTHORITYNUMBER
				,OTHERREFERENCE1
				,OTHERREFERENCE2
				,OTHERREFERENCE3
				,OTHERREFERENCE4
				,SPECIALINSTRUCTIONS
				,TOTALLOGISTICSUNITS
				,TOTALDEADWEIGHT--Declaredweight/Measuredweight
				,TOTALVOLUME--Declaredvolume/measurevolume
				,INSURANCECATEGORY
				,DECLAREDVALUE
				,TESTFLAG
				,DANGEROUSGOODSFLAG
				,NOTBEFOREDATE
				,NOTAFTERDATE
				,ITEM.LABELNUMBER
				,'CTN' AS [UNITTYPE]
				,'0' AS [LABELTESTFLAG]
				,'1' AS [INTERNALLABEL]
				,'0' AS [LINKLABEL]
				, [ATLFLAG]
				,[BOOKINGID]
				,[BOOKINGBRANCH]
				,[BOOKINGDATE]
				,[UNIQUECPPLCODE]
				--Date?
			FROM @temp CONSIGNMENT
				JOIN  [EzyFreight].[dbo].[tblItemLabel] ITEM on ITEM.consignmentid=CONSIGNMENT.consignmentid
			                                            -- FOR XML PATH('CPPLPODManifest'));
			WHERE 												
					CONSIGNMENT.consignmentid not in (630415,972680) 
					and consignment.CONSIGNMENTNUMBER not in (
						'CPWAGVBK000054910',
						'CPWAGVP5000012073',
						'CPWAGVBK000054910',
						'CPWDS3000000451',
						'CPWNPU000061612',
						'CPWAGVBK000068178',
						'CPWAGSBK000084115',
						'CPWAGSP5000047229',
						'CPWAGSP5000047229',
						'CPWAGSBK000084115',
						'CPWAGVP5000012073',
						'CPWAGVBK000054910',
						'CPWAGSP5000047229',
						'CPWASP25000090873',
						'CPWASP25000091010',
						'CPWAGSP5000050095',
						'CPWAC2000027187',
						'CPWAGSP5000050095',
						'CPWAGSP5000050106',
						'CPWAGSP5000050118',
						'CPWASP25000095632',
						'CPWAGSP5000050166',
						'CPWAGSP5000050095',
						'CPWAGSP5000050106',
						'CPWAGSP5000050118',
						'CPWASP25000095632',
						'CPWAGSP5000050166',
						'CPWASP25000096040',
						'CPWASP25000096301',
						'CPWAGSP5000050434',
						'CPWAGSP5000050095',
						'CPWAGSP5000050106',
						'CPWAGSP5000050118',
						'CPWASP25000095632',
						'CPWAGSP5000050166',
						'CPWASP25000096040',
						'CPWASP25000096301',
						'CPWAGSP5000050434',
						'CPWAGVP5000027238',
						'CPWAGVBK000071909'
					) 
					--and  consignment.CONSIGNMENTNUMBER  in
					--(
					--'CPWAGVP5000027238',
					--'CPWAGVBK000071909'
					--)
		FOR XML AUTO, ROOT('CPPLPODManifest'), ELEMENTS 
		);

------------------
	--update  [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] set EDIDataprocessed=1 from @temp t join [cpsqlweb01]. [EzyFreight].[dbo].[tblConsignment] c on t.Consignmentid=c.consignmentid
	select @xmlop as col1

	--Added by SS, Added Recievername from XML should match with the temp table
	Select
		Col.query('./CONSIGNMENTNUMBER').value('.', 'varchar(300)') as CONSIGNMENTNUMBER,
		Col.query('./RECEIVERNAME').value('.', 'varchar(300)') as RECEIVERNAME
	Into #Tempcolumn
	From @xmlop.nodes('/CPPLPODManifest/CONSIGNMENT') Tab(Col)

------------------------
	Update [EzyFreight].[dbo].[tblConsignment] 
		Set EDIDataprocessed=1 
	From [EzyFreight].[dbo].[tblConsignment]  Cons
		INNER JOIN @Temp C
			On(Cons.consignmentid = C.Consignmentid)
		JOIN [EzyFreight].[dbo].[tblItemLabel] ITEM 
			On (ITEM.consignmentid=c.consignmentid )
		JOIN #Tempcolumn Temp
			On(Cons.Consignmentcode = Temp.CONSIGNMENTNUMBER)
		JOIN @Temp D
			On(Temp.RECEIVERNAME = D.RECEIVERNAME)
--COMMIT TRAN

End
GO
