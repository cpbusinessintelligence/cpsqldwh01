SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_ExportCustomerInformationtoPronto]
as
begin
  
  
  --'=====================================================================
    --' CP -Stored Procedure -sp_ExportCustomerInformationtoPronto
    --' ---------------------------
    --' Purpose: sp_ExportCustomerInformationtoPronto-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 07 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 07/09/2015    AB      1.00    Created the procedure                             --AB20150907

    --'=====================================================================
  
---For Existing EDI customers,Domestic is charged based on existing EDI account.For international,a new account with WI Prefix needs to be created.For Domestic nothing else needs to be done.(hence no WD account for existing EDI customers)


 Declare  @Temp table(ChildAccount varchar(10),Parentaccount varchar(10),[Name(Internal)] varchar(100),[Name(External)] varchar(100),
                    Address1 varchar(100),Address2 varchar(100),Address3 varchar(100),Address4 varchar(100),Address5 varchar(100),Address6 varchar(100),
					Postcode int,Phone varchar(100),ABN varchar(100),[Credit Limit code] varchar(10),[Credit Limit $ amt] varchar(50),
		 [Account Status] varchar(50),[Clearing] varchar(10),[GST Exempt Ref] varchar(100),[Price Level] int,[Warehouse] varchar(10),[Territory] varchar(10),[Rep] varchar(20),
		 [Terms Code] varchar(10),[Settlement Disc Code] varchar(50) ,[Cust Type] varchar(50),[Industry] varchar(50),[Marketing Flag] varchar(50),
		 [Company Flag] varchar(50),[Price Cat] varchar(50),[Currency] varchar(50),[Delivery Code] varchar(50),[Deliv Seq] int,
		 [Master Account] varchar(50),[Tarriff Acc] varchar(50),[Acc Fee] varchar(50),[Part Ship] varchar(50),[Order Priority] int,
		 [Price by Bill To]  varchar(50),[Sign Ind] varchar(50),[Bill Control] varchar(50),[Mail Control] varchar(50),[Freight Code] varchar(50),[Industry Sub] varchar(50),[Re Weigh] varchar(50),[Card Disc] int,[Pay Surcharge] varchar(50),[Email for Statement] 
varchar(50),
		  [Contact Name 1] varchar(50),[Contact 1 Phone] varchar(50),[Contact 1 Fax] varchar(50),[Contact 1 Email] varchar(50),[Contact 1 send Invoice] varchar(50),[Contact 1 send Elec Inv] varchar(50),
		-- [Contact Name 2] varchar(50),[Contact 2 Phone] varchar(50),[Contact 2 Fax] varchar(50), [Contact 2 Email] varchar(50),[Contact 2 send Invoice] varchar(50),[Contact 2 send Elec Inv] varchar(50),
		--[Contact Name 3] varchar(50),[Contact 3 Phone] varchar(50),[Contact 3 Fax] varchar(50), [Contact 3 Email] varchar(50), [Contact 3 send Invoice] varchar(50),[Contact 3 send Elec Inv] varchar(50),
        [Bank BSB] varchar(50),[Bank Account Number] varchar(50),[New/Update] varchar(50),Rank int)

 Insert into @Temp
 Select distinct AccountNumber as ChildAccount,
    -- case when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end as ChildAccount,
     AccountNumber as Parentaccount,
         convert(varchar(100), c.CompanyName) as [Name(Internal)],
		 convert(varchar(100),c.CompanyName) as [Name(External)],
		 convert(varchar(100),a.address1) as Address1,
		 convert(varchar(100),a.address2) as Address2,
		 convert(varchar(100),a.suburb) as Address3,
		  convert(varchar(100),isnull(s.statecode,a.statename)) as Address4,
		 '' as Address5,
		 '' as Address6,
		 a.Postcode as Postcode,
		 a.phone as Phone,
		 c.ABN as ABN,
		 'Z' as [Credit Limit Code],
		 1000.00 as [Credit Limit $ amt],
		 '' as [Account Status],
		 'O' as [Clearing],
		 '' as [GST Exempt Ref],
		 0 as [Price Level],
		 '01' as [Warehouse],
		 'CHQ' as [Territory],
		 '000' as [Rep],
		 'W' as [Terms Code],
		 '' as [Settlement Disc Code],
		 '' as [Cust Type],
		 '' as [Industry],
		 '' as [Marketing Flag],
		 '' as [Company Flag],
		 '' as [Price Cat],
		 '' as [Currency],
		 '' as [Delivery Code],
		 0 as [Deliv Seq],
		  '' as [Master Account],
		 '' as [Tarriff Acc],
		 '' as [Acc Fee],
		 'Y' as [Part Ship],
		 0 as [Order Priority],
		 'Y' as [Price by Bill To],
		 'N' as [Sign Ind],
		  '' as [Bill Control],
		 '' as [Mail Control],
		 '' as [Freight Code],
		 '' as [Industry Sub],
		 'Y' as [Re Weigh],
		 0 as [Card Disc],
		 '' as [Pay Surcharge],
		 convert(varchar(50),u.Email) as [Email for Statement],
		 convert(varchar(50),u.Firstname+''+u.LastName) as [Contact Name 1],
		 '' as [Contact 1 Phone],
		 '' as [Contact 1 Fax],
		 convert(varchar(50),u.Email) as [Contact 1 Email],
		 'Y' as [Contact 1 send Invoice],
		 'Y' as [Contact 1 send Elec Inv],
		 -- convert(varchar(50),'') as [Contact Name 2],
		 --'' as [Contact 2 Phone],
		 --'' as [Contact 2 Fax],
		 --convert(varchar(50),'') as [Contact 2 Email],
		 --'Y' as [Contact 2 send Invoice],
		 --'Y' as [Contact 2 send Elec Inv],
		 --convert(varchar(50),'')  as [Contact Name 3],
		 --'' as [Contact 3 Phone],
		 --'' as [Contact 3 Fax],
		 --convert(varchar(50),'') as [Contact 3 Email],
		 --'Y' as [Contact 3 send Invoice],
		 --'Y' as [Contact 3 send Elec Inv],
		 '' as [Bank BSB],
		 '' as [Bank Account Number],
		 'N' as [New/Update],
		  RANK() OVER ( PARTITION BY AccountNumber order by cu.userid ) as Rank
         FROM [cpsqlweb01].[EzyFreight].[dbo].[tblCompany] c left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=c.addressid1
                                                    left join [cpsqlweb01].[EzyFreight].[dbo].[tblstate] s on s.StateID=a.stateid
                                       left join [cpsqlweb01].[EzyFreight].[dbo].[tblCompanyUsers] cu on c.companyid=cu.companyid
									   left join [EzyFreight].[dbo].[Users] u on cu.userid=u.userid
									  -- where accountnumber='WD00000170'
									   where 
									   
									  c.IsProntoExtracted=0 and  accountnumber is not null and accountnumber like 'WD%' and c.CompanyName is not null and accountnumber<>''
									   --  and accountnumber='WD00003031'
									  
									   
 Insert into @Temp

Select distinct 

  --AccountNumber as ChildAccount,
     convert(varchar(10),case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end) as ChildAccount,
     AccountNumber as Parentaccount,
          convert(varchar(100),c.CompanyName) as [Name(Internal)],
		  convert(varchar(100),c.CompanyName) as [Name(External)],
		 convert(varchar(100),a.address1) as Address1,
		 convert(varchar(100),a.address2) as Address2,
		 convert(varchar(100),a.suburb) as Address3,
		  convert(varchar(100),isnull(s.statecode,a.statename)) as Address4,
		 '' as Address5,
		 '' as Address6,
		 a.Postcode as Postcode,
		 a.phone as Phone,
		 c.ABN as ABN,
		 'Z' as [Credit Limit Code],
		 1000.00 as [Credit Limit $ amt],
		 '' as [Account Status],
		 'O' as [Clearing],
		 'I' as [GST Exempt Ref],
		 0 as [Price Level],
		 '01' as [Warehouse],
		 'CHQ' as [Territory],
		 '000' as [Rep],
		 'W' as [Terms Code],
		 '' as [Settlement Disc Code],
		 '' as [Cust Type],
		 '' as [Industry],
		 '' as [Marketing Flag],
		 '' as [Company Flag],
		 '' as [Price Cat],
		 '' as [Currency],
		 '' as [Delivery Code],
		 0 as [Deliv Seq],
		  '' as [Master Account],
		 '' as [Tarriff Acc],
		 '' as [Acc Fee],
		 'Y' as [Part Ship],
		 0 as [Order Priority],
		 'Y' as [Price by Bill To],
		 'N' as [Sign Ind],
		  '' as [Bill Control],
		 '' as [Mail Control],
		 '' as [Freight Code],
		 '' as [Industry Sub],
		 'Y' as [Re Weigh],
		 0 as [Card Disc],
		 '' as [Pay Surcharge],
		 convert(varchar(50),u.Email) as [Email for Statement],
		 convert(varchar(50),u.Firstname+''+u.LastName) as [Contact Name 1],
		 '' as [Contact 1 Phone],
		 '' as [Contact 1 Fax],
		 convert(varchar(50),u.Email) as [Contact 1 Email],
		 'Y' as [Contact 1 send Invoice],
		 'Y' as [Contact 1 send Elec Inv],
		 -- convert(varchar(50),'') as [Contact Name 2],
		 --'' as [Contact 2 Phone],
		 --'' as [Contact 2 Fax],
		 --convert(varchar(50),'') as [Contact 2 Email],
		 --'Y' as [Contact 2 send Invoice],
		 --'Y' as [Contact 2 send Elec Inv],
		 --	 	 convert(varchar(50),'')  as [Contact Name 3],
		 --'' as [Contact 3 Phone],
		 --'' as [Contact 3 Fax],
		 --convert(varchar(50),'') as [Contact 3 Email],
		 --'Y' as [Contact 3 send Invoice],
		 --'Y' as [Contact 3 send Elec Inv],
		 '' as [Bank BSB],
		 '' as [Bank Account Number],
		 'N' as [New/Update],
		 RANK() OVER ( PARTITION BY case when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI')  when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) else '' end order by cu.userid) as Rank



FROM [cpsqlweb01].[EzyFreight].[dbo].[tblCompany] c left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=c.addressid1
                                                    left join [cpsqlweb01].[EzyFreight].[dbo].[tblstate] s on s.StateID=a.stateid
                                       left join [cpsqlweb01].[EzyFreight].[dbo].[tblCompanyUsers] cu on c.companyid=cu.companyid
									   left join [EzyFreight].[dbo].[Users] u on cu.userid=u.userid
									  where 
									  --c.CompanyID =3031
									  --c.AccountNumber='WD00003031'
									  -- And 
									    c.IsProntoExtracted=0 and accountnumber is not null and c.CompanyName is not null and  c.CompanyName<>'000 Test Company' and accountnumber<>''
									


--Declare @Temp1 table(ChildAccount varchar(20),ContactName1 varchar(100),Contact1Email varchar(100))

--Insert into @Temp1
--Select ChildAccount,[Contact Name 1],[Contact 1 Email]
--from @temp where rank=2




--Declare @Temp2 table(ChildAccount varchar(20),ContactName1 varchar(100),Contact1Email varchar(100))

--Insert into @Temp2
--Select ChildAccount,[Contact Name 1],[Contact 1 Email]
--from @temp where rank=3


--Update @Temp set [Contact Name 2]=t1.ContactName1,[Contact 2 Email]=t1.Contact1Email from @Temp t join @temp1 t1 on t.childaccount=t1.childaccount where rank=1


--Update @Temp set [Contact Name 3]=t1.ContactName1,[Contact 2 Email]=t1.Contact1Email from @Temp t join @temp2 t1 on t.childaccount=t1.childaccount where rank=1

--Update @Temp set [Contact 1 send Invoice]='',[Contact 1 send Elec Inv]='' where [Contact 1 Email]='' or [Contact 1 Email] is null

--Update @Temp set [Contact 2 send Invoice]='',[Contact 2 send Elec Inv]='' where [Contact 2 Email]='' or [Contact 2 Email] is null

--Update @Temp set [Contact 3 send Invoice]='',[Contact 3 send Elec Inv]='' where [Contact 3 Email]='' or [Contact 3 Email] is null

Select * from @Temp 
--where rank=1
									  
end

GO
GRANT EXECUTE
	ON [dbo].[sp_ExportCustomerInformationtoPronto]
	TO [SSISUser]
GO
