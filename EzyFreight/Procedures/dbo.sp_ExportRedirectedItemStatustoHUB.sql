SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ExportRedirectedItemStatustoHUB] as
begin

   
     --'=====================================================================
    --' CP -Stored Procedure - sp_ExportRedirectedItemStatustoHUB
    --' ---------------------------
    --' Purpose: sp_ExportRedirectedItemStatustoHUB-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 15 Aug 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 15/08/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

  Select distinct  rc.uniqueid,
       ri.[LabelNumber],
       rc.[ConsignmentCode],
	   convert(varchar(19),rc.createddatetime) as createddatetime,
	   'Redirected' as ScanEvent,
	   9408 as Contractor,
	   SelectedDeliveryOption as ExceptionReason
	  -- convert(varchar(500),'http://cpsqldev02:8060/Tools/RedirectedConsignment?Id='+rc.[ConsignmentCode]) as ExceptionReason
 from [dbo].[tblRedirectedConsignment]  rc join [dbo].[tblRedirectedItemLabel] ri on rc.reconsignmentid=ri.reconsignmentid
where isscannerGWdataprocessed=0


end
GO
