SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[sp_ExtractDomesticAccountAPICustomerBillingFilesToPronto_1](@Startdate date,@Enddate date)
as
begin
Select  distinct c.Consignmentid
         ,'C' as RecordType
		 ,ConsignmentCode as [Consignment Reference]
         --,ConsignmentCode+'_R' as [Consignment Reference]
		 ,convert(date,c.CreatedDateTime) as [Consignment date]
         ,convert(varchar(50),'') as [Manifest reference]
         ,convert(varchar(50),'') as [Manifest date]
		 ,RateCardID as [Service]
         ,convert(varchar(100),AccountNumber) as [Account code]
         ,a.FirstName+' '+a.LastName as [Sender name]
         ,replace(a.address1,',','') as [Sender address 1]
         ,replace(a.address2,',','') as [Sender address 2]
         ,a.suburb AS [Sender locality]
         ,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [Sender State]
         ,a.Postcode as [Sender postcode]
         ,a1.FirstName+' '+a1.LastName as [Receiver name]
         ,a1.address1 as [Receiver address 1]
         ,a1.address2 as [Receiver address 2]
         ,a1.suburb as [Receiver locality]
         ,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [Receiver state]
         ,a1.Postcode as [Receiver postcode]
         ,isnull(CustomerRefNo,'') as [Customer reference]
         ,convert(varchar(50),'') as [Release ASN] 
         ,convert(varchar(50),'') as [Return Authorisation Number]
         ,convert(varchar(50),'') as [Customer other reference 1]
         ,convert(varchar(50),'') as [Customer other reference 2]
         ,convert(varchar(50),'') as [Customer other reference 3]
         ,convert(varchar(50),'') as [Customer other reference 4]
         ,isnull(REPLACE(REPLACE(REPLACE(REPLACE(isnull(specialinstruction,''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '),'') as [Special instructions]
         ,NoOfItems as [Item quantity]
         ,isnull(TotalWeight,0) as [Declared weight]
         ,isnull(TotalMeasureWeight,0) as [Measured weight]
          ,isnull(TotalVolume/250,0) as [Declared volume]
         ,isnull(TotalMeasureVolume/250,0) as [Measured volume]
         ,0.00 as [Price override]
         ,convert(varchar(50),'') as [Insurance category]
         ,isnull(NetSubTotal,0) as [Declared value]
		 ,case when isnull(isinsurance,0)=1 then isnull(insuranceamount,0) else 0 end as [Insurance Price Override]
         --,convert(varchar(50),'') as [Insurance Price Override]
         ,0 as [Test Flag]
         ,DangerousGoods as [Dangerous goods flag]
         ,convert(varchar(50),'') as [Release Not Before]
         ,convert(varchar(50),'') as [Release Not After]
         ,NoOfItems as [Logistics Units]
         ,1 as [IsProcessed]
         ,0 as [IsDeleted]
         ,0 as [HasError]
         ,0 as [ErrorCoded]
         ,'Admin' as [AddWho]
         ,getdate() as [AddDateTime]
         ,'Admin' as [EditWho]
         ,getdate() as [EditDateTime]
 From  cpsqlweb01.[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) 
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblCompanyUsers] cu (NOLOCK) on cu.UserID=c.userid
                                            left join  cpsqlweb01.[EzyFreight].[dbo].[tblCompany] tc (NOLOCK) on tc.companyid=cu.companyid
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on a.addressid=pickupid
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											left JOIN cpsqlweb01.[EzyFreight].[dbo].[tblState] S (NOLOCK) ON S.StateID=a.StateID
											left JOIN cpsqlweb01.[EzyFreight].[dbo].[tblState] S1(NOLOCK)  ON S1.StateID=a1.StateID
where tc.AccountNumber  not in (Select [Accountcode] from  cpsqlweb01.[EzyFreight].[dbo].[tblCompanyTestUser])
                and isnull(c.clientcode,'') ='CPAPI'
				and tc.AccountNumber<>''
				and c.isbilling=0
				and c.isinternational=0 
				and isnull(c.IsAccountCustomer,0)=1   
				and (tc.IsBillingOnActivity =0 or tc.IsBillingOnActivity is null)
                -- and    convert(date,c.createddatetime) > @EndDate
				--and convert(date,c.createddatetime) <= @EndDate  

UNION ALL
				 
  Select  distinct c.Consignmentid
         ,'C' as RecordType
		 ,ConsignmentCode as [Consignment Reference]
         --,ConsignmentCode+'_R' as [Consignment Reference]
		 ,convert(date,c.CreatedDateTime) as [Consignment date]
         ,convert(varchar(50),'') as [Manifest reference]
         ,convert(varchar(50),'') as [Manifest date]
		 ,RateCardID as [Service]
         ,convert(varchar(100),AccountNumber) as [Account code]
         ,a.FirstName+' '+a.LastName as [Sender name]
         ,replace(a.address1,',','') as [Sender address 1]
         ,replace(a.address2,',','') as [Sender address 2]
         ,a.suburb AS [Sender locality]
         ,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [Sender State]
         ,a.Postcode as [Sender postcode]
         ,a1.FirstName+' '+a1.LastName as [Receiver name]
         ,a1.address1 as [Receiver address 1]
         ,a1.address2 as [Receiver address 2]
         ,a1.suburb as [Receiver locality]
         ,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [Receiver state]
         ,a1.Postcode as [Receiver postcode]
         ,isnull(CustomerRefNo,'') as [Customer reference]
         ,convert(varchar(50),'') as [Release ASN] 
         ,convert(varchar(50),'') as [Return Authorisation Number]
         ,convert(varchar(50),'') as [Customer other reference 1]
         ,convert(varchar(50),'') as [Customer other reference 2]
         ,convert(varchar(50),'') as [Customer other reference 3]
         ,convert(varchar(50),'') as [Customer other reference 4]
         ,isnull(REPLACE(REPLACE(REPLACE(REPLACE(isnull(specialinstruction,''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '),'') as [Special instructions]
         ,NoOfItems as [Item quantity]
         ,isnull(TotalWeight,0) as [Declared weight]
         ,isnull(TotalMeasureWeight,0) as [Measured weight]
          ,isnull(TotalVolume/250,0) as [Declared volume]
         ,isnull(TotalMeasureVolume/250,0) as [Measured volume]
         ,0.00 as [Price override]
         ,convert(varchar(50),'') as [Insurance category]
         ,isnull(NetSubTotal,0) as [Declared value]
		 ,case when isnull(isinsurance,0)=1 then isnull(insuranceamount,0) else 0 end as [Insurance Price Override]
         --,convert(varchar(50),'') as [Insurance Price Override]
         ,0 as [Test Flag]
         ,DangerousGoods as [Dangerous goods flag]
         ,convert(varchar(50),'') as [Release Not Before]
         ,convert(varchar(50),'') as [Release Not After]
         ,NoOfItems as [Logistics Units]
         ,1 as [IsProcessed]
         ,0 as [IsDeleted]
         ,0 as [HasError]
         ,0 as [ErrorCoded]
         ,'Admin' as [AddWho]
         ,getdate() as [AddDateTime]
         ,'Admin' as [EditWho]
         ,getdate() as [EditDateTime]
 From  cpsqlweb01.[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) 
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblCompanyUsers] cu (NOLOCK) on cu.UserID=c.userid
                                            left join  cpsqlweb01.[EzyFreight].[dbo].[tblCompany] tc (NOLOCK) on tc.companyid=cu.companyid
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on a.addressid=pickupid
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											left JOIN cpsqlweb01.[EzyFreight].[dbo].[tblState] S (NOLOCK) ON S.StateID=a.StateID
											left JOIN cpsqlweb01.[EzyFreight].[dbo].[tblState] S1(NOLOCK)  ON S1.StateID=a1.StateID
where tc.AccountNumber  not in (Select [Accountcode] from  cpsqlweb01.[EzyFreight].[dbo].[tblCompanyTestUser])
                and isnull(c.clientcode,'') ='CPAPI'
				and tc.AccountNumber<>''
				and c.isbilling=0
				and c.isinternational=0 
				and isnull(c.IsAccountCustomer,0)=1   
				and tc.IsBillingOnActivity = 1
				and ConsignmentStatus in (2,3,4)
                --and convert(date,c.createddatetime) > @EndDate
				 --and    convert(date,c.createddatetime) <= @EndDate  

				 end
GO
