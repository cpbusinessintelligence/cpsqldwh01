SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[sp_ExtractInternationalAccountCustomerBillingFilesToPronto_Change](@Startdate date,@Enddate date) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_ExtractInternationalAccountCustomerBillingFilesToPronto]
    --' ---------------------------
    --' Purpose: ExtractInternationalAccountCustomerBillingFilesToPronto-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 07 October 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 07/10/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

	--SET DATEFIRST 1;
--Declare @Temp table([CountryCode] varchar(20),[CountryName] varchar(100),[Category] varchar(100),[Zone] varchar(20))

--Insert into  @Temp
--Select 	[CountryCode]
--       ,[CountryName]
--       ,[Category]
--       ,[Zone]
-- from [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] 


-- alter  @Temp 
-- alter column [CountryName] varchar(20)  COLLATE SQL_Latin1_General_CP1_CI_AS

If @StartDate=''
select @Startdate=convert(date,dateadd(DAY,-(datepart("dw",getdate()))-5,getdate()))


If @EndDate=''
select @EndDate=convert(date,dateadd(DAY,-(datepart("dw",getdate()))+1,getdate()))

print @StartDate
print @EndDate



  Select  C.Consignmentid
         ,'C' as RecordType
         ,ConsignmentCode as [Consignment Reference]
		 ,convert(date,c.CreatedDateTime) as [Consignment date]
         ,convert(varchar(50),'') as [Manifest reference]
         ,convert(varchar(50),'') as [Manifest date]
		 ,RateCardId+convert(varchar(5),[DWH].[dbo].[fn_PrefillZeros](case when (convert(decimal(12,1),case when (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end)%0.5=0 then (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) else   (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end -((case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) %0.5))+0.5 end))>=30 then 31.0 else (convert(decimal(12,1),case when (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end)%0.5=0 then (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) else   (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end -((case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) %0.5))+0.5 end)) end,5)) as [Service]
         ,convert(varchar(100), case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) else replace(AccountNumber,'WD','WI') end) as [Account code]
         ,a.FirstName+' '+a.LastName as [Sender name]
         ,replace(a.address1,',','') as [Sender address 1]
         ,replace(a.address2,',','') as [Sender address 2]
         ,a.suburb AS [Sender locality]

		 --SS
         --,isnull(s.StateCode,isnull(a.statename,'Unknown')) as [Sender State]
		 ,'NSW' as [Sender State]

		 ,case when I.zone='Zone0' then '0100' when I.zone='Zone1' then '0101' when I.zone='Zone2' then '0102' when I.zone='Zone3' then '0103' when I.zone='Zone4' then '0104' when I.zone='Zone5' then '0105' else 'Unknown' end as SenderPostcode
		 ,a.Country as SenderCountry
		 ,case when I.zone='Zone0' then 'IN00' when I.zone='Zone1' then 'IN01' when I.zone='Zone2' then 'IN02' when I.zone='Zone3' then 'IN03' when I.zone='Zone4' then 'IN04' when I.zone='Zone5' then 'IN05' else 'Unknown' end as FromZone
         ,a1.FirstName+' '+a1.LastName as [Receiver name]
         ,a1.address1 as [Receiver address 1]
         ,a1.address2 as [Receiver address 2]
         ,a1.suburb as [Receiver locality]

		 --SS
         --,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as [Receiver state]
		 ,'NSW' as [Receiver state]

         ,case when I1.zone='Zone0' then '0100' when I1.zone='Zone1' then '0101' when I1.zone='Zone2' then '0102' when I1.zone='Zone3' then '0103' when I1.zone='Zone4' then '0104' when I1.zone='Zone5' then '0105' else 'Unknown' end as ReceiverPostcode
		 ,a1.country as RecieverCountry
		 ,case when I1.zone='Zone0' then 'IN00' when I1.zone='Zone1' then 'IN01' when I1.zone='Zone2' then 'IN02' when I1.zone='Zone3' then 'IN03' when I1.zone='Zone4' then 'IN04' when I1.zone='Zone5' then 'IN05' else 'Unknown' end as ToZone
         ,isnull(CustomerRefNo,'') as [Customer reference]
         ,convert(varchar(50),'') as [Release ASN] 
         ,convert(varchar(50),'') as [Return Authorisation Number]
         ,convert(varchar(50),'') as [Customer other reference 1]
         ,convert(varchar(50),'') as [Customer other reference 2]
         ,convert(varchar(50),'') as [Customer other reference 3]
         ,convert(varchar(50),'') as [Customer other reference 4]
         ,isnull(REPLACE(REPLACE(REPLACE(REPLACE(isnull(specialinstruction,''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '),'') as [Special instructions]
         ,1 as [Item quantity]
         ,isnull(TotalWeight,0) as [Declared weight]
         ,isnull(TotalMeasureWeight,0) as [Measured weight]
          ,isnull(TotalVolume/200,0) as [Declared volume]
         ,isnull(TotalMeasureVolume/200,0) as [Measured volume]
		 ,case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end as BilledWeight
		 ,convert(varchar(5),[DWH].[dbo].[fn_PrefillZeros](convert(decimal(12,1),case when (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end)%0.5=0 then (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) else   (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end -((case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) %0.5))+0.5 end),5)) as Toweight
        -- ,convert(decimal(12,1),case when (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) between I.Fromweight and I.Toweight  then I.Toweight else '' end) as Toweight
		 ,RateCardId+convert(varchar(5),[DWH].[dbo].[fn_PrefillZeros](case when (convert(decimal(12,1),case when (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end)%0.5=0 then (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) else   (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end -((case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) %0.5))+0.5 end))>=30 then 31.0 else (convert(decimal(12,1),case when (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end)%0.5=0 then (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) else   (case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end -((case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end) %0.5))+0.5 end)) end,5)) as RateCardId
		 ,convert(varchar(50),'') as [Price override]
         ,convert(varchar(50),'') as [Insurance category]
         ,isnull(NetSubTotal,0) as [Declared value]
		-- ,3.40 as [Insurance Price Override]
		,case when isnull(isinsurance,0)=1 then isnull(insuranceamount,0) else 0 end as [Insurance Price Override]
        -- ,convert(varchar(50),'') as [Insurance Price Override]
         ,0 as [Test Flag]
         ,DangerousGoods as [Dangerous goods flag]
         ,convert(varchar(50),'') as [Release Not Before]
         ,convert(varchar(50),'') as [Release Not After]
         ,1 as [Logistics Units]
         ,1 as [IsProcessed]
         ,0 as [IsDeleted]
         ,0 as [HasError]
         ,0 as [ErrorCoded]
         ,'Admin' as [AddWho]
         ,getdate() as [AddDateTime]
         ,'Admin' as [EditWho]
         ,getdate() as [EditDateTime]
FROM  cpsqlweb01.[EzyFreight].[dbo].[tblConsignment] c  (NOLOCK) left join cpsqlweb01. [EzyFreight].[dbo].[tblCompanyUsers] cu (NOLOCK) on cu.UserID=c.userid
                                            left join  cpsqlweb01.[EzyFreight].[dbo].[tblCompany] tc (NOLOCK) on tc.companyid=cu.companyid
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on a.addressid=pickupid
                                            left join cpsqlweb01.[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											left JOIN cpsqlweb01.[EzyFreight].[dbo].[tblState] S (NOLOCK) ON S.StateID=a.StateID
											left JOIN cpsqlweb01.[EzyFreight].[dbo].[tblState] S1 (NOLOCK) ON S1.StateID=a1.StateID
											left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I ON I.countryname=a.country  COLLATE Latin1_General_CI_AS
											left JOIN [cpsqlops01].[CouponCalculator].[dbo].[IntCountry] I1 ON I1.countryname=a1.country  COLLATE Latin1_General_CI_AS

--and cu.userid<>1920 and cu.userid<>2368
where tc.AccountNumber not in (Select [Accountcode] from  cpsqlweb01.[EzyFreight].[dbo].[tblCompanyTestUser])
-----='WD00000006'
and isnull(clientcode,'') <> 'CPAPI' and tc.AccountNumber<>'' and
 c.isbilling=0 and 
 c.isinternational=1 and isnull(c.IsAccountCustomer,0)=1   
  and    convert(date,c.createddatetime) <= @EndDate   





end

GO
