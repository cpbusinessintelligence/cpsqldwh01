SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create PROC [dbo].[sp_EzyFreight_Create_FK_Constraints] AS

BEGIN


      --'=====================================================================
    --' CP -Stored Procedure -[EzyFreight_Create_FK_Constraints]
    --' ---------------------------
    --' Purpose: Creates FK Constraints-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015   AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================


	DECLARE @PK_Table varchar(100)
			,@FK_Table varchar(100)
			,@PK_Id varchar(100)
			,@FK_Id varchar(100)
			,@RelationshipName varchar(100)
			,@SqlStatement varchar(1000)

	DECLARE Relationships_Cursor CURSOR FOR
	SELECT [PK_Table]
		  ,[FK_Table]
		  ,[PK_Id]
		  ,[FK_Id]
		  ,[RelationshipName]
	  FROM [dbo].[RelationshipDefinitions]
	  WHERE [PK_Table] IS NOT NULL
		  AND [FK_Table] IS NOT NULL
		  AND [PK_Id] IS NOT NULL
		  AND [FK_Id] IS NOT NULL
		  AND [RelationshipName] IS NOT NULL;
	OPEN Relationships_Cursor;

	FETCH NEXT FROM Relationships_Cursor
	INTO @PK_Table, @FK_Table, @PK_Id, @FK_Id, @RelationshipName;

	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			SET @SqlStatement = '
			IF OBJECT_ID(''' + @RelationshipName + ''', ''F'')IS NULL
			ALTER TABLE ' + @FK_Table + ' WITH NOCHECK ADD CONSTRAINT 
			' + @RelationshipName + ' FOREIGN KEY (' + @FK_Id + ') REFERENCES ' + @PK_Table + '
			(' + @PK_Id + ') ON UPDATE NO ACTION ON DELETE NO ACTION NOT FOR REPLICATION;

			ALTER TABLE ' + @FK_Table + ' NOCHECK CONSTRAINT ' + @RelationshipName + ';
			'
			BEGIN TRANSACTION;
				BEGIN TRY
					EXEC(@SqlStatement);
					COMMIT TRANSACTION;
					PRINT 'Constraint ' + @RelationshipName + ' created.';
				END TRY
				BEGIN CATCH
					SELECT ERROR_MESSAGE() AS ErrorMessage, @SqlStatement AS SqlStatement;
					PRINT '** FAILD to create Constraint ' + @RelationshipName;
					IF @@TRANCOUNT > 0
						ROLLBACK TRANSACTION;
				END CATCH
			FETCH NEXT FROM Relationships_Cursor
				INTO @PK_Table, @FK_Table, @PK_Id, @FK_Id, @RelationshipName;
		END;
	CLOSE Relationships_Cursor;
	DEALLOCATE Relationships_Cursor;

	END
GO
