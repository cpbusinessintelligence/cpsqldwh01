SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GetLabelsforAssignShipmenttoEzyParcelAPI] as 
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_GetLabelsforAssignShipmenttoEzyParcelAPI]
    --' ---------------------------
    --' Purpose: sp_GetLabelsforAssignShipmenttoEzyParcelAPI-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 05 May 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 05/05/2016    AK      1.00    Created the procedure                            

    --'=====================================================================




Select  labelnumber
from [EzyFreight].[dbo].[tblConsignment]	c     left join	[EzyFreight].[dbo].[tblItemlabel] l on l.ConsignmentID=c.ConsignmentID
                                                  left join [EzyFreight].[dbo].[tblAddress] a on  a.addressid=pickupid
                                                  left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid


 where RateCardID like 'SAV%' 
 and  a1.countrycode='US'  and c.userid=1920


end
GO
GRANT EXECUTE
	ON [dbo].[sp_GetLabelsforAssignShipmenttoEzyParcelAPI]
	TO [SSISUser]
GO
