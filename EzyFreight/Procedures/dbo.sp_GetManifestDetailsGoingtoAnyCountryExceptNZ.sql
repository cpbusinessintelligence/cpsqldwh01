SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_GetManifestDetailsGoingtoAnyCountryExceptNZ](@State varchar(30)) as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_Getezy2shipManifestDetailsGoingtoSG
    --' ---------------------------
    --' Purpose: To get all consignment details for  consignments Going To SG-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

Declare @Temp table(consignmentid int,[Line #] int identity(1,1),[House Bill] varchar(35),Consignor varchar(300),SenderState varchar(50),[Consignee First Name] varchar(35),[Consignee Last Name] varchar(35),[Consignee Addr. 1] varchar(35),[Consignee Addr. 2] varchar(35),[Consignee City] varchar(35),[Consignee State] varchar(35),[Consignee Postcode] varchar(10),
                    [Consignee Country] varchar(35),[Consignee Email] varchar(50),[Consignee Phone] varchar(50),[Goods Description] varchar(500),[Origin] varchar(10),[Dest] varchar(10),[Country Origin] varchar(10),value decimal(18,2),Currency varchar(20),Packs int,UnitPacksMeasure varchar(10),Weight decimal(18,2),Unitweightmeasure varchar(10),[ConsignmentNumber] varchar(35))


Insert into @Temp(consignmentid,
                  [House Bill],
				  Consignor,
				  SenderState,
				  [Consignee First Name],
				  [Consignee Last Name],
				  [Consignee Addr. 1],
				  [Consignee Addr. 2],
				  [Consignee City],
				  [Consignee State],
				  [Consignee Postcode],
                  [Consignee Country],
				  [Consignee Email],
				  [Consignee Phone],
				  [Goods Description],
				  [Origin],
				  [Dest],
				  [Country Origin],
				  value,
				  Currency,
				  Packs,
				  UnitPacksMeasure,
				  Weight,
				  Unitweightmeasure,
				 consignmentNumber)
SELECT c.consignmentid
     ,l.labelnumber
	 ,replace(a.FirstName+' '+a.LastNAme+' '+a.CompanyName+' '+a.Address1+' '+a.Address2+' '+a.Suburb+' '+a.StateName+' '+a.CountryCode,',',' ') as [Consignee Addr. 1]
	 ,isnull(case when a.statename='SYD' then 'NSW' else a.statename end,'') as SenderState
	 ,a1.FirstName
	 ,a1.LastNAme
     ,replace(a1.Address1 ,',',' ') as [Address1]
	 ,replace(a1.Address2,',',' ') as [Address2]
	 ,a1.suburb
	 ,a1.StateName
	 ,a1.PostCode
	 ,a1.CountryCode
	 ,a1.Email
	 ,a1.Phone
	 ,convert(varchar(500),'') as GoodsDescription
	 ,a.Countrycode
	 ,a1.countrycode
	 ,convert(varchar(10),'') as CountryofOrigin
	 ,convert(decimal(18,2),isnull(c.netsubtotal,0))  as UnitValue
	 ,'AUD'
	 ,NoOfItems
	 ,'PCS'
	 ,isnull(i.physicalweight,0) as Physicalweight
	 ,'KG'
	 ,[ConsignmentCode]

  FROM [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemlabel] l (NOLOCK) on l.ConsignmentID=c.ConsignmentID
                                                          left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on  a.addressid=pickupid
                                             left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											 left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] i(NOLOCK)  on i.consignmentid=c.consignmentid
											 where isinternational=1 and  a1.countrycode<>'NZ' 
											 and ratecardid like 'SAV%'  
											--and isnull(case when a.statename='SYD' then 'NSW' else a.statename end,'') in (select * from  [dbo].[fn_GetMultipleValues](@State)) 
											and  [CTIManifestExtracted]=0
								--	and l.labelnumber in ('CPWSAV900003111','CPWSAV900003101')
--'CPWSAV990001447', 
--'CPWSAV990001448', 
--'CPWSAV990001438', 
--'CPWSAV990001449', 
--'CPWSAV990001450' )

							--		 and c.consignmentid=3381
	order by c.consignmentid,l.Labelnumber COLLATE Latin1_General_CI_AS --and convert(date,c.CreatedDateTime)=convert(date,dateadd("dd",-1,getdate()))

Update @Temp set [Country Origin]=isnull(c.CountryOfOrigin,Origin),[Goods Description]=replace(c.ItemDescription,',','')  from @Temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration] c (NOLOCK) on c.consignmentid=t.consignmentid


select distinct t.* from @temp t join [scannergateway].[dbo].[trackingevent](NOLOCK) e on e.sourcereference=[House Bill]
	 where e.eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' 
	  and e.eventdatetime>=
	-- Convert(datetime, Convert(varchar(10), dateadd(day,-3,getdate()), 103) + ' 13:30:00', 103) and eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 13:29:00', 103)
	 Convert(datetime, Convert(varchar(10), dateadd(day,-4,getdate()), 103) + ' 13:55:00', 103) and e.eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 13:54:00', 103)
	-- Convert(datetime, Convert(varchar(10), dateadd(day,-2,getdate()), 103) + ' 13:30:00', 103) and e.eventdatetime<=Convert(datetime, Convert(varchar(10), dateadd(day,-1,getdate()), 103) + ' 13:29:00', 103)
	 and   e.additionaltext1<>'CPI0000002513' and e.additionaltext1<>'CPI0000012701' and  e.additionaltext1<>'CPI0000008101'  and e.additionaltext1 like 'CPI%'

end

GO
GRANT EXECUTE
	ON [dbo].[sp_GetManifestDetailsGoingtoAnyCountryExceptNZ]
	TO [SSISUser]
GO
