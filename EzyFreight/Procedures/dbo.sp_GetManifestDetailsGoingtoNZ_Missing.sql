SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_GetManifestDetailsGoingtoNZ_Missing] as
--(@Label varchar(max)) as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_GetManifestDetailsGoingtoNZ
    --' ---------------------------
    --' Purpose: To get all consignment details for  consignments Going To NZ-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

Declare @Temp table(consignmentid int,[Line #] int identity(1,1),[House Bill] varchar(35),Consignor varchar(300),SenderState varchar(50),[Consignee Name] varchar(35),[Consignee Addr. 1] varchar(35),[Consignee Addr. 2] varchar(35),[Consignee City] varchar(35),[Consignee State] varchar(35),[Consignee Postcode] varchar(10),
                    [Consignee Country] varchar(35),[Consignee Email] varchar(50),[Consignee Phone] varchar(50),[Goods Description] varchar(500),value decimal(18,2),Currency varchar(20),Weight decimal(18,2),Unitweightmeasure varchar(10),Packs int,UnitPacksMeasure varchar(10),Sendercountrycode varchar(10),[Country Origin] varchar(10),[Country Destination] varchar(10),Consignmentcode varchar(100),LabelNumber varchar(max))


Insert into @Temp(consignmentid,
                  [House Bill],
				  Consignor,
				  SenderState,
				  
				  [Consignee Name],
				  [Consignee Addr. 1],
				  [Consignee Addr. 2],
				  [Consignee City],
				  [Consignee State],
				  [Consignee Postcode],
                  [Consignee Country],
				  [Consignee Email],
				  [Consignee Phone],
				  [Goods Description],
				  value,
				  Currency,
				  Weight,
				  Unitweightmeasure,
				  Packs,
				  UnitPacksMeasure,
				  Sendercountrycode,
				  [Country Origin],
				  [Country Destination],
				  Consignmentcode,
				  LabelNumber)
SELECT c.consignmentid
     ,labelnumber
	 ,replace(a.FirstName+' '+a.LastNAme+' '+a.CompanyName+' '+a.Address1+' '+a.Address2+' '+a.Suburb+' '+a.StateName+' '+a.CountryCode,',',' ') as [Consignee Addr. 1]
	 ,isnull(case when a.statename='SYD' then 'NSW' else a.statename end,'') as SenderState
	 ,a1.FirstName+' '+a1.LastNAme
	 ,replace(a1.Address1 ,',',' ') as [Address1]
	 ,replace(a1.Address2,',',' ') as [Address2]
	 ,a1.suburb
	 ,a1.StateName
	 ,a1.PostCode
	 ,a1.CountryCode
	 ,a1.Email
	 ,a1.Phone
	 ,convert(varchar(500),'') as GoodsDescription
	-- ,isnull(i.UnitValue,0) as UnitValue
	,convert(decimal(18,2),isnull(c.netsubtotal,0)) as UnitValue
	 ,'AUD'
	 ,isnull(i.physicalweight,0) as Physicalweight
	 ,'KG'
	 ,NoOfItems
	 ,'PCS'
	 ,a.countrycode as sendercountrycode
	 ,convert(varchar(50),'') as CountryofOrigin
	 ,'NZ'
	 ,c.consignmentcode
	 ,labelnumber
  FROM [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on  a.addressid=pickupid
                                             left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											 left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] i (NOLOCK) on i.consignmentid=c.consignmentid
											 where isinternational=1 and  a1.countrycode='NZ' and ratecardid like 'SAV%'
											--and consignmentcode='CPWSAV000001992'
										--and ismanifested=0 
						and labelnumber in ('CPWSAV990002045','CPWSAV990002047','CPWSAV990002046','CPWSAV990002026','CPWSAV990001937','CPWSAV990002051','CPWSAV990002050','CPWSAV990002052','CPWSAV990002049','CPWSAV990002056','CPWSAV990002048')
						--in  (select * from  [dbo].[fn_GetMultipleValues](@Label)) 


											-- and isnull(case when a.statename='SYD' then 'NSW' else a.statename end,'')  in (select * from  [dbo].[fn_GetMultipleValues](@State)) 
											 --in ('VIC','QLD','SA')
											 
										--	 and consignmentcode in ('CPWSAV000000075','CPWSAV000000076','CPWSAV000000077')
 order by c.consignmentid,Labelnumber COLLATE Latin1_General_CI_AS 	
											 --and convert(date,c.CreatedDateTime)=convert(date,dateadd("dd",-1,getdate()))
 update @Temp set [Country Origin]=isnull(c.countryoforigin,sendercountrycode),[Goods Description]=replace(ltrim(rtrim(c.ItemDescription)),',','') from @temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration] c on c.consignmentid=t.consignmentid

 --select * from @Temp


select distinct t.*
--[Line #] ,[House Bill] ,Consignor ,SenderState ,[Consignee Name] ,[Consignee Addr. 1] ,[Consignee Addr. 2] ,[Consignee City] ,[Consignee State] ,[Consignee Postcode] ,
  --[Consignee Country] ,[Goods Description] ,value ,Currency ,Weight ,Unitweightmeasure ,Packs int,UnitPacksMeasure ,Sendercountrycode ,[Country Origin] ,[Country Destination] 
					 from @temp t join [scannergateway].[dbo].[trackingevent](NOLOCK) e on e.sourcereference=LabelNumber
	 where e.eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' 
	 and e.eventdatetime>=
	 --Convert(datetime, Convert(varchar(10), dateadd(day,-3,getdate()), 103) + ' 15:30:00', 103) and eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 15:29:00', 103)
	Convert(datetime, Convert(varchar(10), dateadd(day,-60,getdate()), 103) + ' 13:30:00', 103) and e.eventdatetime<=Convert(datetime,Convert(varchar(10), dateadd(day,-1,getdate()), 103) + ' 13:29:00', 103)
	--Convert(datetime, Convert(varchar(10), dateadd(day,-60,getdate()), 103) + ' 13:55:00', 103) and e.eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 13:54:00', 103)
 and e.additionaltext1<>'CPI0000002513' and e.additionaltext1 like 'CPZ%'




end

GO
