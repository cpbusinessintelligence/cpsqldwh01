SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_GetManifestDetailsGoingtoUS] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -[sp_GetManifestDetailsGoingtoUS]
    --' ---------------------------
    --' Purpose: To get all consignment details for  consignments Going To NZ-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 26 April 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 26/04/2016    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

Declare @Temp table(Consignmentid int,Labelnumber varchar(max),[Site ID] varchar(10),	ArrivalAirport varchar(20),	WaybillOriginator varchar(10),	AirlinePrefix varchar(50),	AWBSerialNumber	varchar(50), HouseAWB	varchar(50),MasterAWBIndicator varchar(50),
                   	OriginAirport varchar(10),	Pieces int,	WeightCode varchar(10),	Weight decimal(12,2),	Description varchar(max),FDAIndicator varchar(50),	ImportingCarrier varchar(50),	FlightNumber varchar(50),	
					ArrivalDay varchar(50),	ArrivalMonth varchar(50),	ShipperName varchar(50),	ShipperStreetAddress varchar(100),	ShipperCity	varchar(20),ShipperStateOrProvince varchar(20),	ShipperPostalCode varchar(10),
					ShipperCountry	varchar(10), ShipperTelephone varchar(50),	ConsigneeName varchar(50),	ConsigneeStreetAddress	varchar(50), ConsigneeCity varchar(50),	ConsigneeStateOrProvince varchar(50),
					ConsigneePostalCode varchar(50),	ConsigneeCountry varchar(50),ConsigneeTelephone	varchar(50),AmendmentFlag varchar(10),	AmendmentCode varchar(10),	AmendmentReason varchar(50),
					ptpDestination	varchar(50),ptpDestinationDay varchar(50),	ptpDestinationMonth	 varchar(50), BoardedPieces varchar(50),	BoardedWeightCode varchar(50),	BordedWeight varchar(50),
					PartialShipmentRef	varchar(50),BrokerCode varchar(50),	InbondDestination varchar(50),InbondDestinationType varchar(50),	BondedCarrierID varchar(50),	OnwardCarrier varchar(50),	BondedPremisesID varchar(50),
					TransferControlNumber varchar(50),	EntryType varchar(50),	EntryNumber varchar(50),	CountryOfOrigin	varchar(10),CustomsValue decimal(12,2),CurrencyCode varchar(50),HTSNumber varchar(50),ExpressRelease	varchar(50),[Bag number] int)



Insert into @Temp
(Consignmentid,
LabelNumber,
[Site ID],
ArrivalAirport,
WaybillOriginator,
AirlinePrefix,
AWBSerialNumber,
HouseAWB,
MasterAWBIndicator,
OriginAirport,
Pieces,
WeightCode,
Weight,
Description,
FDAIndicator,
ImportingCarrier,
FlightNumber,
ArrivalDay,
ArrivalMonth,
ShipperName,
ShipperStreetAddress,
ShipperCity,
ShipperStateOrProvince,
ShipperPostalCode,
ShipperCountry,
ShipperTelephone,
ConsigneeName,
ConsigneeStreetAddress,
ConsigneeCity,
ConsigneeStateOrProvince,
ConsigneePostalCode,
ConsigneeCountry,
ConsigneeTelephone,
AmendmentFlag,
AmendmentCode,
AmendmentReason,
ptpDestination,
ptpDestinationDay,
ptpDestinationMonth,
BoardedPieces,
BoardedWeightCode,
BordedWeight,
PartialShipmentRef,
BrokerCode,
InbondDestination,
InbondDestinationType,
BondedCarrierID,
OnwardCarrier,
BondedPremisesID,
TransferControlNumber,
EntryType,
EntryNumber,
CountryOfOrigin,
CustomsValue,
CurrencyCode,
HTSNumber,
ExpressRelease,
[Bag number])


SELECT 
c.Consignmentid,
i.labelnumber,
'' as [Site Id],
'' as ArrivalAirport,
convert(varchar(100),'') WaybillOriginator,
'',
'',
right(ltrim(rtrim(labelnumber)),11),
'',
'SYD',
1,
'K',
case when isnull(i.cubicweight,0)> isnull(i.physicalweight,0) then isnull(i.cubicweight,0)  else isnull(i.physicalweight,0) end,
convert(varchar(100),'') as Description,
'',
'',
'',
'',
'',
'Couriers Please',
replace('Rosehill Industrial Estate,Building C South,Level 1 5-7 Shirley Street,Rosehill',',',' '),
'Sydney',
'NSW',
'2142',
'AU',
'' as ShipperTelephone,
a1.FirstName+' '+a1.LastNAme,
replace(a1.Address1 ,',',' ')+ ' '+isnull(replace(a1.Address2,',',' '),'')  as [Address1],
a1.suburb,
a1.StateName,
a1.PostCode,
a1.CountryCode,
a1.Phone,
'A' as AmendmentFlag,
21 as AmendmentCode,
'',
'' as ptpDestination,
'' as ptpDestinationDay,
'' as ptpDestinationMonth,
'' as BoardedPieces,
'' as BoardedWeightCode,
'' as BordedWeight,
'' as PartialShipmentRef,
'' as BrokerCode,
'' as InbondDestination,
'' as InbondDestinationType,
'' as BondedCarrierID,
'' as OnwardCarrier,
'' as BondedPremisesID,
'' as TransferControlNumber,
'' as EntryType,
'' as EntryNumber,
'AU' as CountryOfOrigin,
convert(decimal(12,2),isnull(c.netsubtotal,0)),
'USD',
'',
'Y',
'' as [Bag number]
  FROM [cpsqldwh01].[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) left join [cpsqldwh01].[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on  a.addressid=pickupid
                                             left join [cpsqldwh01].[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											 left join [cpsqldwh01].[EzyFreight].[dbo].[tblItemLabel] i (NOLOCK) on i.consignmentid=c.consignmentid
											 where isinternational=1 and  a1.countrycode='US' and ratecardid like 'SAV%'
											--and consignmentcode='CPWSAV000001992'
											--and labelnumber='0405510200793102242463'
										     --and CTIManifestextracted=0 



 update @Temp set Description=replace(ltrim(rtrim(c.ItemDescription)),',','') from @temp t join [cpsqldwh01].[EzyFreight].[dbo].[tblCustomDeclaration] c on c.consignmentid=t.consignmentid

  update @Temp set CustomsValue=CustomsValue*Value from [CentralAdmin].[dbo].[Parameters] where code='USCurrencyConversion'

   update @Temp set ArrivalAirport =[Assigned Port]	from [dbo].[USPostcodeChart] where substring(ltrim(rtrim(ConsigneePostalCode)),1,3) = [Postal code]

  update @Temp set WaybillOriginator=case when ArrivalAirport='JFK' then 'E130' 
                                          when ArrivalAirport='LAX' then 'W650' else '' end  


--Select * from @Temp
select distinct t.*

	from @temp t join [scannergateway].[dbo].[trackingevent](NOLOCK) e on e.sourcereference=LabelNumber
	 where e.eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' 
	 and e.eventdatetime>=
Convert(datetime, Convert(varchar(10), dateadd(day,-1,getdate()), 103) + ' 14:00:00', 103) and e.eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 13:59:00', 103)
 and e.additionaltext1<>'CPI0000002513' and (e.additionaltext1 like 'CPI00000[1][56789]%' or e.additionaltext1 like 'CPI00000[2][012345]%')

end
GO
GRANT EXECUTE
	ON [dbo].[sp_GetManifestDetailsGoingtoUS]
	TO [SSISUser]
GO
