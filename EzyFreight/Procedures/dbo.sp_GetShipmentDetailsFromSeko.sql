SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[sp_GetShipmentDetailsFromSeko] 
AS

BEGIN



     --'=====================================================================

    --' CP -Stored Procedure - [sp_CreateEzy2ShipShipmentsthroughNZAPI]

    --' ---------------------------

    --' Purpose: NZ Post API -----

    --' Developer: Satya Gandu (Couriers Please Pty Ltd)

    --' Date: 21 Dec 2016                         



    --'=====================================================================

SELECT c.consignmentcode 
FROM [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on  a.addressid=pickupid
                                             left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											 left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] i (NOLOCK) on i.consignmentid=c.consignmentid
											 where 
											 isinternational=1 and  a1.countrycode='NZ' and ratecardid like 'SAV%'
									-- labelnumber='CPWSAV900010131'
										--and ismanifested=0 
										and C.CreatedDateTime >GEtdate() -30
										and c.ConsignmentStatus not in (4)

										--and ConsignmentCode = 'CPWSAVW000004012'


--SELECT A.ConsignmentCode
--FROM #TEMP1 A where ConsignmentCode = 'CPWSAV990020366'
--LEFT JOIN [SCANNERGATEWAY].DBO.[SEKOSTATUSRESPONSE] b
--ON(a.ConsignmentCode COLLATE Latin1_General_CI_AS = b.Connote)

END




GO
