SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_GetShipmentLabelsEzy2ShipShipmentsthroughAPI] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_CreateEzy2ShipShipmentsthroughAPI]
    --' ---------------------------
    --' Purpose: sp_CreateEzy2ShipShipmentsthroughAPI-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 04 Nov 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 04/11/2016    AK      1.00    Created the procedure                            

    --'=====================================================================


Select distinct c.consignmentid,
       ShipmentNumber
	   from [CreateShipmentEzy2ShipResponse] c
where isprocessed=0 and shipmentnumber is not null and shipmentnumber<>''


end


GO
