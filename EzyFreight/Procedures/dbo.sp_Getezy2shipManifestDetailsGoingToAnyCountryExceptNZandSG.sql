SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[sp_Getezy2shipManifestDetailsGoingToAnyCountryExceptNZandSG] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_Getezy2shipManifestDetailsGoingToAnyCountryExceptNZandSG
    --' ---------------------------
    --' Purpose: sp_Getezy2shipManifestDetailsGoingToAnyCountryExceptNZandSG-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

Declare @Temp table(conid int,[Cost Centre Code] varchar(40),[Send from Business Name] varchar(35),[Send from Contact person] varchar(35),[Send from address line 1] varchar(35),[Send from address line 2] varchar(35),[Send from address line 3] varchar(35),
                    [Send from Town] varchar(30),[Send from country] varchar(2),[Send from Postcode] varchar(10),[Sender VAT/GST number] varchar(50),[Send to Business Name] varchar(50),[Send to Contact person] varchar(35),[Send to address line 1] varchar(35),[Send to address line 2] varchar(35),[Send to address line 3] varchar(35),
					[Send to Town] varchar(30),[Send To State] varchar(30),[Send to country] varchar(2),[Send to Postcode] varchar(10),[Send to Phone] varchar(20),[Receiver VAT/GST number] varchar(50),[Sender Reference] varchar(20),[Category of Shipment] varchar(1),[If "Others"please describe] varchar(100),
					[Total Item physical weight (kg)] decimal(12,2),[Item length(cm)] decimal(12,2),[Item width(cm)] decimal(12,2),[Item height(cm)] decimal(12,2),Itemquantity int,Itemsinbox int,
					[Item description 1] varchar(50),[Item quantity 1] varchar(10),[Item weight 1(kg)] varchar(20),[Declare value 1(SGD)] varchar(50),[HS tariff number 1] varchar(6),[Country of origin 1] varchar(2),
					[Item description 2] varchar(50),[Item quantity 2] varchar(10),[Item weight 2(kg)] varchar(20),[Declare value 2(SGD)] varchar(50),[HS tariff number 2] varchar(6),[Country of origin 2] varchar(2),
					[Item description 3] varchar(50),[Item quantity 3] varchar(10),[Item weight 3(kg)] varchar(20),[Declare value 3(SGD)] varchar(50),[HS tariff number 3] varchar(6),[Country of origin 3] varchar(2),
					[Enhanced Liability Amount] varchar(10),[Invoice number] varchar(50),[Certificate number]varchar(50),[Export license number] varchar(50),[Service code] varchar(20))


					--ItemQuantity int,[Item No. 1 Physical weight(kg)] decimal(4,2),[Item No. 1 Length (cm)] decimal(4,2),
					--[Item No. 1 Width (cm)] decimal(4,2),[Item No. 1 Height (cm)] decimal(4,2),[Item No. 1 Item content] varchar(50),[Item No. 1 Declared Value (SGD)] decimal(4,2),[Item No. 2 Physical weight(kg)] decimal(4,2),[Item No. 2 Length (cm)] decimal(4,2),
					--[Item No. 2 Width (cm)] decimal(4,2),[Item No. 2 Height (cm)] decimal(4,2),[Item No. 2 Item content] varchar(50),[Item No. 2 Declared Value (SGD)] decimal(4,2),[Item No. 3 Physical weight(kg)] decimal(4,2),[Item No. 3 Length (cm)] decimal(4,2),
					--[Item No. 3 Width (cm)] decimal(4,2),[Item No. 3 Height (cm)] decimal(4,2),[Item No. 3 Item content] varchar(50),[Item No. 3 Declared Value (SGD)] decimal(4,2),[Service code] varchar(20),[Enhanced Liability Amount Item No. 1] decimal(4,2),[Enhanced Liability Amount Item No. 2] decimal(4,2)
	    --            ,[Enhanced Liability Amount Item No. 3] decimal(4,2))

Insert into @Temp
SELECT c.consignmentid as conid
       ,convert(varchar(40),'') as [Cost Centre Code] 
      ,'Couriers Please' as [Send from Business Name]
	  ,'Couriers Please' as [Send from Contact person]
	  ,'21 North Perimeter Road' as [Send from address line 1]
	  ,'#03-01 Airmail Transit Centre' as [Send from address line 2]
	  ,convert(varchar(35),'') as [Send from address line 3]
	  ,'Singapore' as [Send from Town]
	  ,'SG' as [Send from country]
	  ,'918112' as [Send from Postcode]
	  ,'' as [Sender VAT/GST]
	  ,convert(varchar(50),isnull(case when a1.companyname='' then a1.FirstName+' '+a1.LastNAme else a1.companyname end ,isnull(a1.FirstName+' '+a1.LastNAme,''))) as [Send to Business Name]
	  ,convert(varchar(35),isnull(a1.FirstName+' '+a1.LastNAme,'')) as [Send to Contact person]
	  ,convert(varchar(35),replace(isnull(a1.Address1,''),',',' ')) as [Send to address line 1]
	  ,convert(varchar(35),replace(isnull(a1.Address2,''),',',' ')) as [Send to address line 2]
	  ,convert(varchar(35),convert(varchar(35),'')) as [Send to address line 3]
	  ,convert(varchar(30),isnull(a1.suburb,'')) as [Send to Town]
	  ,convert(varchar(30),isnull(a1.StateName,'')) as [SendToState]
	  ,convert(varchar(2),isnull(a1.countrycode,'')) as [Send to country]
	  ,convert(varchar(10),isnull(a1.postcode,'')) as [Send to Postcode]
	  ,convert(varchar(20),isnull(a1.phone,'')) as [Send to Phone]
	  ,'' as [Receiver VAT/GST]
	  ,convert(varchar(20),l.labelnumber) as [Sender Reference]
	  ,case when ReasonforExport in ('S','D','M','O','G') then ReasonforExport else '' end as CategoryofShipment
	  ,convert(varchar(100),isnull(case when ReasonforExport='O' then NatureOfGoods else '' end,'')) as Ifotherspleasespecify
	  ,convert(decimal(12,2),isnull(c.totalmeasureweight,c.totalweight))
	  ,convert(decimal(12,2),0) as Length
	  ,convert(decimal(12,2),0) as Width	
	  ,convert(decimal(12,2),0) as Height
      ,convert(int,NoOfItems) as ItemQuantity
	  ,convert(int,0) as Itemsinbox
	  ,convert(varchar(50),'') as [Item description 1]
	  ,convert(varchar(10),'') as [Item Quantity 1]
	  ,convert(varchar(20),'') as [Item weight 1(kg)]
	  ,convert(varchar(10),'') as [Declare value 1(SGD)]
	  ,convert(varchar(50),'') as [HS tariff number 1]
	  ,convert(varchar(2),'') as [Country of Origin 1]
	  ,convert(varchar(50),'') as [Item description 2]
	  ,convert(varchar(10),'') as [Item Quantity 2]
	  ,convert(varchar(20),'') as [Item weight 2(kg)]
	  ,convert(varchar(10),'') as [Declare value 2(SGD)]
	  ,convert(varchar(50),'') as [HS tariff number 2]
	  ,convert(varchar(2),'') as [Country of Origin 2]
	  ,convert(varchar(50),'') as [Item description 3]
	  ,convert(varchar(10),'') as [Item Quantity 3]
	  ,convert(varchar(20),'') as [Item weight 3(kg)]
	  ,convert(varchar(10),'') as [Declare value 3(SGD)]
	  ,convert(varchar(50),'') as [HS tariff number 3]
	  ,convert(varchar(2),'') as [Country of Origin 3]
	  ,convert(varchar(10),'') as [Enhanced Liability Amount]
	  ,convert(varchar(50),'') as [Invoice Number]
	  ,convert(varchar(50),'') as [Certificate number]
	  ,convert(varchar(50),'') as [Export licence number]
	  ,'TRSTWC' as [Service code]
  FROM [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemlabel] l (NOLOCK) on l.ConsignmentID=c.ConsignmentID
                                             left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on  a.addressid=pickupid
                                             left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											 where isinternational=1  and ratecardid like 'SAV%' 
											 and a1.countrycode  not in ('SG','NZ')
											 								
																			--SS
--																		and labelnumber in ('CPWSAV900018928', 
--'CPWSAV900018947', 
--'CPWSAV900018949', 
--'CPWSAV900018966',
--'CPWSAV900018952')

											--and IsManifested=0
 order by c.consignmentid,Labelnumber COLLATE Latin1_General_CI_AS 	
											 --where c.consignmentid in (961,962)
											-- where convert(date,c.CreatedDateTime)>=convert(date,dateadd("dd",-3,getdate()))
											 
Declare @Temp1 table([CustomDeclarationId] int,[ConsignmentID] int,[ItemDescription] varchar(50),[ItemInBox] int,[SubTotal] varchar(50),[HSCode] varchar(50),[CountryofOrigin] varchar(50),currency varchar(10),RANK int)
--Declare @Temp1 table([CustomDeclarationId] int,[ConsignmentID] int,[ItemDescription] nvarchar(max),[ItemInBox] nvarchar(max),[SubTotal] nvarchar(max),[HSCode]  nvarchar(max),[CountryofOrigin] nvarchar(max),currency  nvarchar(max),RANK int)

Insert into @Temp1
SELECT [CustomDeclarationId]
      ,[ConsignmentID]
      ,convert(varchar(50),[ItemDescription])
      ,[ItemInBox]
      ,convert(varchar(50),replace([SubTotal],'Nan',0))
      ,[HSCode]
      ,[CountryofOrigin]
      ,[Currency]
	  ,rank() over(partition by consignmentid order by CustomdeclarationID asc) as rank
  FROM @Temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration]	c (NOLOCK) on t.conid=c.consignmentid									 
											 
											 
update @Temp set [Item length(cm)]=l.length from @Temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] l (NOLOCK) on l.ConsignmentID=t.Conid
update @Temp set [Item Width(cm)]=l.width from @Temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] l (NOLOCK) on l.ConsignmentID=t.Conid 
update @Temp set [Item Height(cm)]=l.height from @Temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] l  (NOLOCK) on l.ConsignmentID=t.Conid


update @Temp set Itemsinbox=(Select count(*) from   [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration] d (NOLOCK) where d.consignmentid=conid)
--from  [dbo].[tblCustomDeclaration] d where t.consignmentid=d.consignmentid

--Select * from @Temp1

update @Temp set [Invoice Number]=s.invoiceno from @Temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblSalesOrder] s (NOLOCK) on s.ReferenceNo=t.conid 

update @Temp set [Item description 1]=[Itemdescription] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=1
update @Temp set [Item description 2]=[Itemdescription] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=2
update @Temp set [Item description 3]=[Itemdescription] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=3

update @Temp set [Item Quantity 1]=[ItemInBox] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=1
update @Temp set [Item Quantity 2]=[ItemInBox] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=2
update @Temp set [Item Quantity 3]=[ItemInBox] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=3

update @Temp set [Item weight 1(kg)]=[Total Item physical weight (kg)]/Itemsinbox where Itemsinbox<>0
update @Temp set [Item weight 2(kg)]=[Total Item physical weight (kg)]/Itemsinbox where Itemsinbox=2
update @Temp set [Item weight 3(kg)]=[Total Item physical weight (kg)]/Itemsinbox where Itemsinbox=3

update @Temp set [Declare value 1(SGD)]=[Subtotal] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=1
update @Temp set [Declare value 2(SGD)]=[Subtotal] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=2
update @Temp set [Declare value 3(SGD)]=[Subtotal] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=3


update @Temp set [Country of Origin 1]=[CountryofOrigin] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=1
update @Temp set [Country of Origin 2]=[CountryofOrigin] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=2
update @Temp set [Country of Origin 3]=[CountryofOrigin] from @Temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=3

--Select * from @Temp

--update @Temp set [Item Quantity 1]=replace(convert(varchar(10),[Item Quantity 1]),'0','') where [Item Quantity 1]=0
--update @Temp set [Item Quantity 2]=replace(convert(varchar(10),[Item Quantity 2]),'0','') where [Item Quantity 2]=0
--update @Temp set [Item Quantity 3]=replace(convert(varchar(10),[Item Quantity 3]),'0','') where [Item Quantity 3]=0


--update @Temp set [Item Quantity 1]=case when convert(varchar(10),[Item Quantity 1])='0' then '' else  convert(varchar(10),[Item Quantity 1]) end where [Item Quantity 1]=0
--update @Temp set [Item Quantity 2]=case when convert(varchar(10),[Item Quantity 2])='0' then '' else  convert(varchar(10),[Item Quantity 2]) end where [Item Quantity 2]=0
--update @Temp set [Item Quantity 3]=case when convert(varchar(10),[Item Quantity 3])='0' then '' else  convert(varchar(10),[Item Quantity 3]) end where [Item Quantity 3]=0

--update @Temp set [Item weight 1(kg)]=case when convert(varchar(50),[Item weight 1(kg)])='0.00' then '' else  convert(varchar(50),[Item weight 1(kg)]) end where [Item weight 1(kg)]=0
--update @Temp set [Item weight 2(kg)]=case when convert(varchar(50),[Item weight 2(kg)])='0.00' then '' else  convert(varchar(50),[Item weight 2(kg)]) end where [Item weight 2(kg)]=0
--update @Temp set [Item weight 3(kg)]=case when convert(varchar(50),[Item weight 3(kg)])='0.00' then '' else  convert(varchar(50),[Item weight 3(kg)]) end where [Item weight 3(kg)]=0

--update @Temp set [Declare value 1(SGD)]=case when convert(varchar(50),[Declare value 1(SGD)])='0.00' then '' else  convert(varchar(50),[Declare value 1(SGD)]) end where [Declare value 1(SGD)]=0
--update @Temp set [Declare value 2(SGD)]=case when convert(varchar(50),[Declare value 2(SGD)])='0.00' then '' else  convert(varchar(50),[Declare value 2(SGD)]) end where [Declare value 2(SGD)]=0
--update @Temp set [Declare value 3(SGD)]=case when convert(varchar(50),[Declare value 3(SGD)])='0.00' then '' else  convert(varchar(50),[Declare value 3(SGD)]) end where [Declare value 3(SGD)]=0





Select distinct t.* from @Temp t join [scannergateway].[dbo].[trackingevent](NOLOCK) e on e.sourcereference=[Sender Reference]
	 where e.eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' 
and eventdatetime>=Convert(datetime, Convert(varchar(10), dateadd(day,-1,getdate()), 103) + ' 15:30:00', 103) and eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 15:29:00', 103)
	 --and eventdatetime>=Convert(datetime, Convert(varchar(10), dateadd(day,-2,getdate()), 103) + ' 15:30:00', 103) and eventdatetime<=Convert(datetime, Convert(varchar(10), dateadd(day,-1,getdate()), 103) +  ' 15:29:00', 103)
	 and e.additionaltext1<>'CPI0000002513' and e.additionaltext1<>'CPI0000012701' and  e.additionaltext1<>'CPI0000008101' and e.additionaltext1 like 'CPI%'


end


GO
GRANT EXECUTE
	ON [dbo].[sp_Getezy2shipManifestDetailsGoingToAnyCountryExceptNZandSG]
	TO [SSISUser]
GO
