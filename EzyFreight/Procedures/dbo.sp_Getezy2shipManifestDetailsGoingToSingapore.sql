SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_Getezy2shipManifestDetailsGoingToSingapore] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_Getezy2shipManifestDetailsGoingToSingapore
    --' ---------------------------
    --' Purpose: To get all consignment details for  consignments Going To Singapore-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

Declare @Temp table(conid int,[Cost Centre Code] varchar(40),[Send from Business Name] varchar(35),[Send from Contact person] varchar(35),[Send from address line 1] varchar(35),[Send from address line 2] varchar(35),[Send from address line 3] varchar(35),
                    [Send from Town] varchar(35),[Send from country] varchar(35),[Send from Postcode] varchar(10),[Send to Business Name] varchar(35),[Send to Contact person] varchar(35),[Send to address line 1] varchar(35),[Send to address line 2] varchar(35),[Send to address line 3] varchar(35),
					[Send to Town] varchar(35),[Send to country] varchar(35),[Send to Postcode] varchar(10),[Send to Phone] varchar(20),[Send to Email Address] varchar(50),[Sender Reference] varchar(20) collate Latin1_General_CI_AS,ItemQuantity int,Itemsinbox int,Totalphysicalweight decimal(12,2),[Item No. 1 Physical weight(kg)] varchar(50),[Item No. 1 Length (cm)] varchar(10),
					[Item No. 1 Width (cm)] varchar(10),[Item No. 1 Height (cm)] varchar(10),[Item No. 1 Item content] varchar(50),[Item No. 1 Declared Value (SGD)] varchar(50),[Item No. 2 Physical weight(kg)] varchar(50),[Item No. 2 Length (cm)] varchar(10),
					[Item No. 2 Width (cm)] varchar(10),[Item No. 2 Height (cm)] varchar(10),[Item No. 2 Item content] varchar(50),[Item No. 2 Declared Value (SGD)] varchar(50),[Item No. 3 Physical weight(kg)] varchar(50),[Item No. 3 Length (cm)] varchar(10),
					[Item No. 3 Width (cm)] varchar(10),[Item No. 3 Height (cm)] varchar(10),[Item No. 3 Item content] varchar(50),[Item No. 3 Declared Value (SGD)] varchar(50),[Service code] varchar(20),[Enhanced Liability Amount Item No. 1] varchar(10),[Enhanced Liability Amount Item No. 2] varchar(10)
	                ,[Enhanced Liability Amount Item No. 3] varchar(10))

Insert into @Temp
SELECT c.consignmentid
       ,convert(varchar(40),'') as [Cost Centre Code] 
      ,'Couriers Please' as [Send from Business Name]
	  ,'Couriers Please'  as [Send from Contact person]
	  ,'21 North Perimeter Road' as [Send from address line 1]
	  ,'#03-01 Airmail Transit Centre' as [Send from address line 2]
	  ,convert(varchar(35),'') as [Send from address line 3]
	  ,'Singapore' as [Send from Town]
	  ,'SG' as [Send from country]
	  ,'918112' as [Send from Postcode]
	  ,CONVERT(varchar(35),isnull(case when a1.companyname='' then a1.FirstName+' '+a1.LastNAme else a1.companyname end ,isnull(a1.FirstName+' '+a1.LastNAme,''))) as [Send to Business Name]
	  ,convert(varchar(35),isnull(a1.FirstName+' '+a1.LastNAme,'')) as [Send to Contact person]
	  ,convert(varchar(35),replace(isnull(a1.Address1,''),',',' ')) as [Send to address line 1]
	  ,convert(varchar(35),replace(isnull(a1.Address2,''),',',' ')) as [Send to address line 2]
	  ,convert(varchar(35),'') as [Send to address line 3]
	  ,isnull(a1.suburb,'') as [Send to Town]
	  ,isnull(a1.country,'') as [Send to country]
	  ,isnull(a1.postcode,'') as [Send to Postcode]
	  ,isnull(a1.phone,'') as [Send to Phone]
	  ,isnull(a1.email,'') as [Send to Email Address]
	  ,l.labelnumber as [Sender Reference]
	  ,NoOfItems as ItemQuantity
	  ,convert(int,0) as Itemsinbox
	  ,isnull(c.totalmeasureweight,c.totalweight)
	  ,convert(varchar(50),'') as [Item No. 1 Physical weight(kg)]
	  ,l.length as [Item No. 1 Length (cm)]
	  ,l.width as [Item No. 1 Width (cm)]
	  ,l.height as [Item No. 1 Height (cm)]
	  ,convert(varchar(50),'') as [Item No. 1 Item content]
	  ,convert(varchar(50),'') as [Item No. 1 Declared Value (SGD)]
	  ,convert(varchar(50),'') as [Item No. 2 Physical weight(kg)]
	  ,convert(varchar(10),'') as [Item No. 2 Length (cm)]
	  ,convert(varchar(10),'') as [Item No. 2 Width (cm)]
	  ,convert(varchar(10),'') as [Item No. 2 Height (cm)]
	  ,convert(varchar(50),'') as [Item No. 2 Item content]
	  ,convert(varchar(50),'') as [Item No. 2 Declared Value (SGD)]
	  ,convert(varchar(50),'') as [Item No. 3 Physical weight(kg)]
	  ,convert(varchar(10),'') as [Item No. 3 Length (cm)]
	  ,convert(varchar(10),'') as [Item No. 3 Width (cm)]
	  ,convert(varchar(10),'') as [Item No. 3 Height (cm)]
	  ,convert(varchar(50),'') as [Item No. 3 Item content]
	  ,convert(varchar(50),'') as [Item No. 3 Declared Value (SGD)]
	  ,'TRSTIC' as [Service code]
	  ,convert(varchar(10),'') as [Enhanced Liability Amount Item No. 1]
	  ,convert(varchar(10),'') as [Enhanced Liability Amount Item No. 2]
	  ,convert(varchar(10),'') as [Enhanced Liability Amount Item No. 3]
   FROM [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c (NOLOCK) left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemlabel] l (NOLOCK) on l.ConsignmentID=c.ConsignmentID
                                                      --      join [scannergateway].[dbo].[trackingevent](NOLOCK) e on e.sourcereference=labelnumber
                                                          left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a (NOLOCK) on  a.addressid=pickupid
                                             left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 (NOLOCK) on a1.addressid=destinationid
											-- where c.consignmentid=1660
											 where isinternational=1 and  a1.countrycode='SG' and ratecardid like 'SAV%' 
--											 and l.labelnumber in ('CPWSAV990002220',
--'CPWSAV990002226',
--'CPWSAV990002222')

--'CPWSAV900002789'
											 and ismanifested=0
	 order by c.consignmentid,Labelnumber COLLATE Latin1_General_CI_AS 										
											 --and convert(date,c.CreatedDateTime)=convert(date,dateadd("dd",-1,getdate()))

Declare @Temp1 table([CustomDeclarationId] int,[ConsignmentID] int,[ItemDescription] varchar(50),[ItemInBox] int,[SubTotal] varchar(50),[HSCode] varchar(50),[CountryofOrigin] varchar(50),currency varchar(10),RANK int)


Insert into @Temp1
SELECT [CustomDeclarationId]
      ,c.[ConsignmentID]
      ,convert(varchar(50),[ItemDescription])
      ,[ItemInBox]
      ,isnull(replace([SubTotal],'Nan',0),0)
      ,[HSCode]
      ,[CountryofOrigin]
      ,[Currency]
	  ,rank() over(partition by consignmentid order by CustomdeclarationID asc) as rank
  FROM @Temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration]	c (NOLOCK) on t.conid=c.consignmentid

--select * from [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration] where consignmentid in (2722,
--5022,
--6444,
--6489,
--6589,
--7137,
--8100,
--8238,
--11220,
--12615,
--12654,
--13820,
--13937,
--14267,
--18760,
--18764,
--19894,
--19992,
--21508,
--22266,
--22267,
--27357,
--31975,
--32852,
--32853,
--32854,
--32917,
--32928,
--32953,
--32955,
--33205,
--34947,
--37203,
--37204,
--37279,
--37524,
--38357,
--38943,
--38964,
--39634,
--39636,
--39637)

-- not(isnumeric([SubTotal]))=1

update @temp set Itemsinbox=(Select count(*) from   [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration] d (NOLOCK) where d.consignmentid=conid)

--select * from @temp

Update @temp set [Item No. 1 Physical weight(kg)]=[TotalPhysicalweight]

 --Update @temp set [Item No. 1 Physical weight(kg)]=[TotalPhysicalweight]/Itemsinbox where Itemsinbox<>0
 --Update @temp set [Item No. 2 Physical weight(kg)]=[TotalPhysicalweight]/Itemsinbox where Itemsinbox=2
 --Update @temp set [Item No. 3 Physical weight(kg)]=[TotalPhysicalweight]/Itemsinbox where Itemsinbox=3


 Declare @Temp2 table(Conid int,ItemDescriptionConcat varchar(max));
 
WITH CTE ( Consignmentid,ItemDescriptionConcatenated, ItemDescription, length )
          AS ( SELECT Consignmentid, CAST( '' AS VARCHAR(8000) ), CAST( '' AS VARCHAR(8000) ), 0
                 FROM @Temp1
                GROUP BY Consignmentid
                UNION ALL
               SELECT p.Consignmentid, CAST( c.ItemDescriptionConcatenated +
                      CASE WHEN length = 0 THEN '' ELSE '  ' END + p.ItemDescription AS VARCHAR(8000) ),
                      CAST( p.ItemDescription AS VARCHAR(8000)), length + 1
                 FROM CTE c
                INNER JOIN @Temp1 p
                   ON c.Consignmentid = p.Consignmentid
                WHERE p.ItemDescription > c.ItemDescription )

Insert into @Temp2
SELECT Consignmentid, ItemDescriptionConcatenated
      FROM ( SELECT Consignmentid, ItemDescriptionConcatenated,
                    RANK() OVER ( PARTITION BY Consignmentid ORDER BY length DESC )
               FROM CTE ) D ( Consignmentid, ItemDescriptionConcatenated, rank )
     WHERE rank = 1 ;

--select * from @Temp2

 Update @temp set [Item No. 1 Item content]=convert(varchar(50),[ItemDescriptionConcat]) from @temp t join @Temp2 t1 on t.conid=t1.conid 

 Update @temp set [Item No. 1 Declared Value (SGD)]= [NetSubtotal] from @temp t join [cpsqlweb01].[EzyFreight].[dbo].[tblconsignment] t1 (NOLOCK) on t.conid=t1.consignmentid 
 
 --where rank=1
 --Update @temp set [Item No. 2 Declared Value (SGD)]= [Subtotal] from @temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=2
 --Update @temp set [Item No. 3 Declared Value (SGD)]= [Subtotal] from @temp t join @Temp1 t1 on t.conid=t1.consignmentid where rank=3

 --Select * from @Temp
  --Update @temp set [Item No. 1 Length (cm)]= [length] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid 
  --Update @temp set [Item No. 1 Width (cm)]=[width] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid 
  --Update @temp set [Item No. 1 Height (cm)]= [height] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid 

 --alter table @Temp
 --alter column  SenderReference varchar(max) collate Latin1_General_CI_AS

 
    select distinct t.* from @temp t join [scannergateway].[dbo].[trackingevent](NOLOCK) e on e.sourcereference=[Sender Reference]
	 where e.eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' 
	 and e.eventdatetime>=
	-- Convert(datetime, Convert(varchar(10), dateadd(day,-2,getdate()), 103) + ' 15:30:00', 103) and eventdatetime<=Convert(datetime, Convert(varchar(10), dateadd(day,-1,getdate()), 103) + ' 15:29:00', 103)
   Convert(datetime, Convert(varchar(10), dateadd(day,-1,getdate()), 103) + ' 15:30:00', 103) and e.eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 15:29:00', 103)
	 and   e.additionaltext1<>'CPI0000002513' and e.additionaltext1<>'CPI0000012701' and  e.additionaltext1<>'CPI0000008101' and e.additionaltext1 like 'CPI%'
end
GO
GRANT EXECUTE
	ON [dbo].[sp_Getezy2shipManifestDetailsGoingToSingapore]
	TO [SSISUser]
GO
