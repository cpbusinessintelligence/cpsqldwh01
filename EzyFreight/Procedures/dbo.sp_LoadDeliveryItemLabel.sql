SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_LoadDeliveryItemLabel] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadDeliveryItemLabel
    --' ---------------------------
    --' Purpose: sp_LoadDeliveryItemLabel-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 21 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

IF(datepart(hour,getdate())<>'01')
begin

Insert into  [cpsqlezt01].[EzyTrak Integration].[dbo]. DeliveryItemLabel(
SourceSystem,
ProcessingCode,
ItemNumber,
--SiblingItemNumber,
CNACardNumber,
SLCNACardNumber,
RTCNACardNumber,
Weight,
ServiceType,
--AmountToCollect,
TrafficIndicator,
ActualItemNumber,
BillToCustomerID,
BillToCustomerAccountNumber,
--BillToCustomerCostCentreCode,
BillToName,
DeliveryToCompanyName,
DeliveryToName,
DeliveryToRecipientID,
DeliveryToPhone,
DeliveryToMail,
DeliveryToAddress1,
DeliveryToAddress2,
DeliveryToAddress3,
DeliveryToCity,
DeliveryToAddressZip,
DeliveryToState,
DeliveryToCountryCode,
DeliveryToCountryName,
DeliveryToPreferredFromTime,
DeliveryToPreferredToTime,
Instructions,
IsATL,
IsRedelivery,
DLB,
IsProcessed)

Select 
       'CP_WEB',
       'N' as ProcessingCode,
	   Labelnumber,
--	   '',
       '',
	   '',
	   '',
	   l.physicalweight,
	   c.ratecardid,
	   'LCL',
	   '',
	   '112995527',
	   '112995527',
	   'Retail' as CustomerName,
	  a1.firstname+''+a1.lastname,
       a1.firstname+''+a1.lastname,
	   '',
	    a1.phone, 
	    a1.email,
	   a1.address1,
	    a1.address2,
	   '',
	   a1.suburb,
	   a1.postcode,
	   isnull(s1.StateCode,isnull(a1.statename,'Unknown')),
       a1.countrycode,
	   a1.country,
	  convert(varchar(20),ConsignmentPreferPickupDate)+'T'+convert(varchar(12),cast(ConsignmentPreferPickupTime as Time)),
	  convert(varchar(20),dateadd("day",4,ConsignmentPreferPickupDate))+'T'+convert(varchar(12),cast(ConsignmentPreferPickupTime as Time)),
	   replace(isnull(specialinstruction,''),',',''),
	   IsATL,
	   0,
	   '',
	   0
from  [EzyFreight].[dbo].[tblConsignment] c left join [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=c.consignmentid
                                            left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											left Join [DWH].[dbo].[Postcodes] p on p.postcode=a.postcode and p.suburb=a.suburb
where 
--convert(date,c.createddatetime)= convert(date,getdate()-1)
c.createddatetime>= dateadd(minute,-30,getdate()) and c.isinternational=0
and not exists(select ItemNumber from [cpsqlezt01].[EzyTrak Integration].[dbo].DeliveryItemLabel where Itemnumber=l.labelnumber)  and labelnumber not like '%CNA'

----select labelnumber from  [cpsqldwh01].[EzyFreight].[dbo].[tblConsignment] c left join [cpsqldwh01].[EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=c.consignmentid



--Insert into [cpsqlezt01].[EzyTrak Integration].[dbo]. DeliveryItemLabel(
--SourceSystem,
--ProcessingCode,
--ItemNumber,
----SiblingItemNumber,
--CNACardNumber,
--SLCNACardNumber,
--RTCNACardNumber,
--Weight,
--ServiceType,
----AmountToCollect,
--TrafficIndicator,
--ActualItemNumber,
--BillToCustomerID,
--BillToCustomerAccountNumber,
----BillToCustomerCostCentreCode,
--BillToName,
--DeliveryToCompanyName,
--DeliveryToName,
--DeliveryToRecipientID,
--DeliveryToPhone,
--DeliveryToMail,
--DeliveryToAddress1,
--DeliveryToAddress2,
--DeliveryToAddress3,
--DeliveryToCity,
--DeliveryToAddressZip,
--DeliveryToState,
--DeliveryToCountryCode,
--DeliveryToCountryName,
--DeliveryToPreferredFromTime,
--DeliveryToPreferredToTime,
--Instructions,
--IsATL,
--IsRedelivery,
--DLB,
--IsProcessed)

--Select top 20 
--       'CP_EDIGW',
--       'N' as ProcessingCode,
--	   cd.cc_coupon,
----	   '',
--       '',
--	   '',
--	   '',
--	   cd_deadweight,
--	   cd_pricecode,
--	   'LCL',
--	   '',
--	   cd_account,
--	   cd_account,
--	   ShortName as CustomerName,
--	   cd_delivery_contact,
--       cd_delivery_contact,
--	   '',
--	   cd_delivery_contact_phone, 
--	   cd_delivery_email,
--	   cd_delivery_addr1,
--	   cd_delivery_addr2,
--	   cd_delivery_addr3,
--	   cd_Delivery_suburb,
--	   cd_delivery_postcode,
--	   case when b.b_name ='Adelaide' then 'SA' 
--	        when b.b_name ='Brisbane' then 'QLD'
--			when b.b_name ='Gold Coast' then 'QLD'
--			when b.b_name ='Melbourne' then 'VIC'
--			when b.b_name ='Sydney' then 'NSW'
--			when b.b_name ='Perth' then 'WA'
--            when b.b_name ='Tasmania' then 'TAS'
--			when b.b_name ='Northern Kope' then 'NT'
--			when b.b_name ='Canberra' then 'ACT'
--			else 'Unknown' end,
--       'AU',
--	   'Australia',
--	  convert(varchar(20),cd_date)+' '+convert(varchar(20),cast(cd_Date as Time)),
--	  convert(varchar(20),dateadd("day",4,cd_Date))+' '+convert(varchar(20),cast(cd_date as Time)),
--	   replace(isnull(cd_special_instructions,''),',',''),
--	   0,
--	   0,
--	   '',
--	   0

--from  [cpsqldwh01].[cpplEDI].[dbo].[Consignment] c left join [cpsqldwh01].[cpplEDI].[dbo].[cdcoupon] cd on cc_consignment=cd_id
--                                                   left join  [cpsqldwh01].[Pronto].[dbo].[ProntoDebtor] d on d.accountcode=cd_account
--												   left join  [cpsqldwh01].[cpplEDI].[dbo].[Branchs] b on b.b_id=c.cd_deliver_branch
--												   where convert(date,cd_date)=convert(date,getdate()-1)
--												   --where c.cd_Date>= dateadd(hour,-1,getdate()) 
--												   and not exists(select ItemNumber from [cpsqlezt01].[EzyTrak Integration].[dbo].DeliveryItemLabel where Itemnumber=cd.cc_coupon)
--												   and cd.cc_coupon not like '%CNA'
											

----select * from  [cpsqldwh01].[cpplEDI].[dbo].[Consignment] where cd_connote like '191%'

----select * from [cpsqldwh01].[scannergateway].[dbo].[trackingevent] where additionaltext1 like '%191%'

----Link Coupon 19100734702


Insert into [cpsqlezt01].[EzyTrak Integration].[dbo]. DeliveryItemLabel(
SourceSystem,
ProcessingCode,
ItemNumber,
--SiblingItemNumber,
CNACardNumber,
SLCNACardNumber,
RTCNACardNumber,
Weight,
ServiceType,
--AmountToCollect,
TrafficIndicator,
ActualItemNumber,
BillToCustomerID,
BillToCustomerAccountNumber,
--BillToCustomerCostCentreCode,
BillToName,
DeliveryToCompanyName,
DeliveryToName,
DeliveryToRecipientID,
DeliveryToPhone,
DeliveryToMail,
DeliveryToAddress1,
DeliveryToAddress2,
DeliveryToAddress3,
DeliveryToCity,
DeliveryToAddressZip,
DeliveryToState,
DeliveryToCountryCode,
DeliveryToCountryName,
DeliveryToPreferredFromTime,
DeliveryToPreferredToTime,
courierid,
Instructions,
IsATL,
IsRedelivery,
DLB,
IsProcessed)

Select  
       'CP_CPN',
       'N' as ProcessingCode,
	   l.labelnumber,
--	   '',
       '',
	   '',
	   '',
	    0,
	   '',
	   'LCL',
	   '',
	   '112526900',
	   '112526900',
	   '',
	   '',
       '',
	   '',
	   '', 
	   '',
	   '',
	   '',
	   '',
	   'Sydney',
	   '2000',
	   'NSW',
       'AU',
	   'Australia',
	  convert(varchar(20),'2015-09-30')+'T'+convert(varchar(12),cast('7:15 am' as Time)),
	  convert(varchar(20),'2015-09-30')+'T'+convert(varchar(12),cast('7:15 am' as Time)),
	   '',
	   '',
	   0,
	   0,
	   '',
	   0
 from  [cpsqldwh01].[ScannerGateway].[dbo].[Label] l (NOLOCK)
where l.createddate>= dateadd(minute,-30,getdate()) 
and isnumeric(labelnumber)=1 and len(labelnumber)=11 
and not exists(select ItemNumber from [cpsqlezt01].[EzyTrak Integration].[dbo]. DeliveryItemLabel where Itemnumber=labelnumber) 
and labelnumber not like '%CNA' 
 and  labelnumber like '605%'




--------Get Redelivery labels and update------

Select l.labelnumber,convert(varchar(100),'') as Sourcereference,convert(varchar(100),'') as Driver
into #temp
from [ScannerGateway].[dbo].[Label] l (NOLOCK)
where labelnumber like '%CNA' and l.createddate>= dateadd(minute,-30,getdate())
and (not exists (select CNACardNumber from DeliveryItemLabel where labelnumber=CNACardNumber) or not exists (select SLCNACardNumber from [cpsqlezt01].[EzyTrak Integration].[dbo]. DeliveryItemLabel  where labelnumber=SLCNACardNumber) or not exists (select RTCNACardNumber from DeliveryItemLabel where labelnumber=RTCNACardNumber))

--drop table #temp1

Update #temp set Sourcereference=te.sourcereference,Driver=[dbo].[fn_CreateUniqueDriverID](b.name,d.code,prontodrivercode)  from [ScannerGateway].[dbo].[TrackingEvent] te  join [ScannerGateway].[dbo].[Driver] d on d.id=te.driverid  join [ScannerGateway].[dbo].[Branch] b on b.id=d.branchid where ltrim(rtrim(replace(te.additionaltext1,'Link Coupon ','')))=labelnumber
and te.eventdatetime>= dateadd(minute,-30,getdate())

--Select labelnumber,te.sourcereference into #temp1 from #temp join [cpsqldwh01].[ScannerGateway].[dbo].[TrackingEvent] te on ltrim(rtrim(replace(te.additionaltext1,'Link Coupon ','')))=labelnumber
--where te.eventdatetime>= dateadd(hour,-1,getdate())


--select * from #temp DeliveryItemLabel

Update [cpsqlezt01].[EzyTrak Integration].[dbo]. DeliveryItemLabel set CNACardNumber=labelnumber,isprocessed=0,ProcessingCode='U',courierid=driver from #temp where #temp.sourcereference=ItemNumber and len(labelnumber)=10 and labelnumber like '%CNA'

Update [cpsqlezt01].[EzyTrak Integration].[dbo].DeliveryItemLabel set SLCNACardNumber=labelnumber,isprocessed=0,ProcessingCode='U',courierid=driver from #temp where #temp.sourcereference=ItemNumber and len(labelnumber)=12 and labelnumber like '%SLCNA'

Update [cpsqlezt01].[EzyTrak Integration].[dbo].DeliveryItemLabel set RTCNACardNumber=labelnumber,isprocessed=0,ProcessingCode='U',courierid=driver from #temp where #temp.sourcereference=ItemNumber and len(labelnumber)=12 and labelnumber like '%RTCNA'

--Update DeliveryItemLabel set courierid=[dbo].[fn_CreateUniqueDriverID](b.name,d.code,prontodrivercode)  from #temp join [cpsqldwh01].[ScannerGateway].[dbo].[Driver] d on d.id=Driver  join [cpsqldwh01].[ScannerGateway].[dbo].[Branch] b on b.id=d.branchid
--where driver is not null

end

end


GO
