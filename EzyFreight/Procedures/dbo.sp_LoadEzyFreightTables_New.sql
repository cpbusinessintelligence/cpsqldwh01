SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadEzyFreightTables_New] as
begin

truncate table [dbo].[aspnet_Membership] 
Insert into [dbo].[aspnet_Membership] 
SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[aspnet_Membership]with (NOLOCK)

Truncate table aspnet_Users
Insert into [aspnet_Users]
SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[aspnet_Users] with (NOLOCK)

Truncate table EventLog;
Insert into EventLog
SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[EventLog] with (NOLOCK)

Truncate table [EventLogConfig];
Insert into EventLogConfig
SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[EventLogConfig] with (NOLOCK)

Truncate table [EventLogTypes];
Insert into EventLogTypes
  SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[EventLogTypes] with (NOLOCK)

Truncate table [Parameters]
Insert into [Parameters]
SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[Parameters] with (NOLOCK)

Truncate table RoleGroups
Insert into RoleGroups
SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[RoleGroups] with (NOLOCK)

Truncate table [dbo].[Roles]
Insert into Roles
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[Roles] with (NOLOCK)

Truncate table [dbo].[tblActivityLogging]
Insert into [dbo].[tblActivityLogging]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblActivityLogging] with (NOLOCK)

Truncate table [dbo].[tblAddress]
Insert into [dbo].[tblAddress]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblAddress] with (NOLOCK)

Truncate table [dbo].[tblBooking]
Insert into [dbo].[tblBooking]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblBooking] with (NOLOCK)

--Truncate table [dbo].[tblBranchAddresses]
--Insert into [dbo].[tblBranchAddresses]
--Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblBranchAddresses]

Truncate table [dbo].[tblClient]
Insert into [dbo].[tblClient]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblClient] with (NOLOCK)

Truncate table [dbo].[tblClientCPPLServiceAccess]
Insert into  [dbo].[tblClientCPPLServiceAccess]
Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblClientCPPLServiceAccess] with (NOLOCK)

Truncate table [dbo].[tblCommunity]
Insert into [dbo].[tblCommunity]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCommunity] with (NOLOCK)

Truncate table [dbo].[tblCompany]
Insert into  [dbo].[tblCompany]
Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblCompany] with (NOLOCK)

Truncate table [dbo].[tblCompanyUsers]
Insert into  [dbo].[tblCompanyUsers]
Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblCompanyUsers] with (NOLOCK)



select * into #tblConsignmentTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblConsignment] with (NOLOCK) where DatePart("m", CreatedDateTime) = DatePart("m", DateAdd("m", -1, getdate()))
and DatePart("yyyy", CreatedDateTime) = DatePart("yyyy", DateAdd("m", -1, getdate()))

DELETE [dbo].[tblConsignment] from [dbo].[tblConsignment] cd (NOLOCK)
join #tblConsignmentTMP cs (NOLOCK) on cd.ConsignmentID = cs.ConsignmentID
where cd.ConsignmentID = cs.ConsignmentID


INSERT INTO tblConsignment ([ConsignmentID]
      ,[ConsignmentCode]
      ,[UserID]
      ,[IsRegUserConsignment]
      ,[PickupID]
      ,[DestinationID]
      ,[ContactID]
      ,[TotalWeight]
      ,[TotalMeasureWeight]
      ,[TotalVolume]
      ,[TotalMeasureVolume]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[CustomerRefNo]
      ,[ConsignmentPreferPickupDate]
      ,[ConsignmentPreferPickupTime]
      ,[ClosingTime]
      ,[DangerousGoods]
      ,[Terms]
      ,[RateCardID]
      ,[LastActivity]
      ,[LastActiivityDateTime]
      ,[ConsignmentStatus]
      ,[ProntoDataExtracted]
      ,[IsBilling]
      ,[IsManifested]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[IsInternational]
      ,[IsDocument]
      ,[IsSignatureReq]
      ,[IfUndelivered]
      ,[ReasonForExport]
      ,[TypeOfExport]
      ,[Currency]
      ,[IsInsurance]
      ,[IsIdentity]
      ,[IdentityType]
      ,[IdentityNo]
      ,[Country-ServiceArea-FacilityCode]
      ,[InternalServiceCode]
      ,[NetSubTotal]
      ,[IsATl]
      ,[IsReturnToSender]
      ,[HasReadInsuranceTc]
      ,[NatureOfGoods]
      ,[OriginServiceAreaCode]
      ,[ProductShortName]
      ,[SortCode]
      ,[ETA]
      ,[CTIManifestExtracted]
      ,[isprocessed]
      ,[IsAccountCustomer]
      ,[InsuranceAmount]
      ,[CourierPickupDate]
      ,[CalculatedTotal]
      ,[CalculatedGST]
      ,[ClientCode] 
      ,PromotionCode
      ,IsHubApiProcessed)
Select cs.ConsignmentID
      ,cs.ConsignmentCode
      ,cs.UserID
      ,cs.IsRegUserConsignment
      ,cs.PickupID
      ,cs.DestinationID
      ,cs.ContactID
      ,cs.TotalWeight
      ,cs.TotalMeasureWeight
      ,cs.TotalVolume
      ,cs.TotalMeasureVolume
      ,cs.NoOfItems
      ,cs.SpecialInstruction
      ,cs.CustomerRefNo
      ,cs.ConsignmentPreferPickupDate
      ,cs.ConsignmentPreferPickupTime
      ,cs.ClosingTime
      ,cs.DangerousGoods
      ,cs.Terms
      ,cs.RateCardID
      ,cs.LastActivity
      ,cs.LastActiivityDateTime
      ,cs.ConsignmentStatus
      ,cs.ProntoDataExtracted
      ,cs.IsBilling
      ,cs.IsManifested
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
      ,cs.IsInternational
      ,cs.IsDocument
      ,cs.IsSignatureReq
      ,cs.IfUndelivered
      ,cs.ReasonForExport
      ,cs.TypeOfExport
      ,cs.Currency
      ,cs.IsInsurance
      ,cs.IsIdentity
      ,cs.IdentityType
      ,cs.IdentityNo
      ,cs.[Country-ServiceArea-FacilityCode]
      ,cs.InternalServiceCode
      ,cs.NetSubTotal
      ,cs.IsATl
      ,cs.IsReturnToSender
      ,cs.HasReadInsuranceTc
      ,cs.NatureOfGoods
      ,cs.OriginServiceAreaCode
      ,cs.ProductShortName
      ,cs.SortCode
      ,cs.ETA
  ,cs.CTIManifestExtracted
  ,cs.isprocessed
      ,cs.IsAccountCustomer
      ,cs.InsuranceAmount
      ,cs.CourierPickupDate
      ,cs.CalculatedTotal
      ,cs.CalculatedGST
  ,cs.clientcode
  ,cs.PromotionCode
  ,0
from #tblConsignmentTMP cs (NOLOCK)
--where not exists (select 1 from tblConsignment cd (NOLOCK) where cd.ConsignmentID = cs.ConsignmentID);

Truncate table [EzyFreight].[dbo].[tblConsignmentImage]
Insert into [EzyFreight].[dbo].[tblConsignmentImage]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblConsignmentImage] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblConsignmentService]
Insert into  [EzyFreight].[dbo].[tblConsignmentService]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblConsignmentService] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblConsignmentStaging]
Insert into [EzyFreight].[dbo].[tblConsignmentStaging]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblConsignmentStaging] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblContactUs]
Insert into [EzyFreight].[dbo].[tblContactUs]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblContactUs] with (NOLOCK)


Truncate table [EzyFreight].[dbo].[tblCPPLServices]
Insert into [EzyFreight].[dbo].[tblCPPLServices]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCPPLServices] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblCustomDeclaration]
Insert into  [EzyFreight].[dbo].[tblCustomDeclaration]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCustomDeclaration] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblCustomerRate]
Insert into [EzyFreight].[dbo].[tblCustomerRate]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCustomerRate] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblDHLBarCodeImage]
Insert into [EzyFreight].[dbo].[tblDHLBarCodeImage]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblDHLBarCodeImage] with (NOLOCK)


Truncate table [EzyFreight].[dbo].[tblErrorLog]
Insert into [EzyFreight].[dbo].[tblErrorLog]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblErrorLog] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblEzyNetCustomer]
Insert into  [EzyFreight].[dbo].[tblEzyNetCustomer]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblEzyNetCustomer] with (NOLOCK)
--where phoneno='243855228'

--Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblEzyNetCustomer] where phoneno like '%243855228%'

Truncate table [EzyFreight].[dbo].[tblFranchise]
Insert into [EzyFreight].[dbo].[tblFranchise]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblFranchise] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblFranchiseList]
Insert into [EzyFreight].[dbo].[tblFranchiseList]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblFranchiseList] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblFreightType]
Insert into [EzyFreight].[dbo].[tblFreightType]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblFreightType] with (NOLOCK)


Truncate table [EzyFreight].[dbo].[tblInsurance]
Insert into [EzyFreight].[dbo].[tblInsurance]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblInsurance] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblInvoice]
Insert into  [EzyFreight].[dbo].[tblInvoice]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblInvoice] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblInvoiceImage]
Insert into [EzyFreight].[dbo].[tblInvoiceImage]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblInvoiceImage] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblItemLabel]
Insert into [EzyFreight].[dbo].[tblItemLabel]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblItemLabel] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblItemLabelImage]
Insert into [EzyFreight].[dbo].[tblItemLabelImage]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblItemLabelImage] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblnsuranceCategory]
Insert into  [EzyFreight].[dbo].[tblnsuranceCategory]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblnsuranceCategory] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblOpenAccount]
Insert into [EzyFreight].[dbo].[tblOpenAccount]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblOpenAccount] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblQuote]
Insert into [EzyFreight].[dbo].[tblQuote]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblQuote] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblRateCard]
Insert into [EzyFreight].[dbo].[tblRateCard]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRateCard] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblRateCardDetail]
Insert into  [EzyFreight].[dbo].[tblRateCardDetail]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRateCardDetail] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblRecharge]
Insert into [EzyFreight].[dbo].[tblRecharge]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRecharge] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblRedelivery]
Insert into [EzyFreight].[dbo].[tblRedelivery]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRedelivery] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblRefund]
Insert into  [EzyFreight].[dbo].[tblRefund]
Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblRefund] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblSalesOrder]
Insert into [EzyFreight].[dbo].[tblSalesOrder]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblSalesOrder] with (NOLOCK)

Truncate table [dbo].[tblSalesOrderDetail]
Insert into  [EzyFreight].[dbo].[tblSalesOrderDetail]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblSalesOrderDetail] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblService]
Insert into [EzyFreight].[dbo].[tblService]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblService] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblState]
Insert into  [EzyFreight].[dbo].[tblState]
Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblState] with (NOLOCK)


Truncate table [EzyFreight].[dbo].[tblStatus]
Insert into [EzyFreight].[dbo].[tblStatus]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblStatus] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblTracking]
Insert into  [EzyFreight].[dbo].[tblTracking]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblTracking] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblTrackingStaging]
Insert into  [EzyFreight].[dbo].[tblTrackingStaging]
Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblTrackingStaging] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblZone]
Insert into  [EzyFreight].[dbo].[tblZone]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblZone] with (NOLOCK)


Truncate table [EzyFreight].[dbo].[UserRoles]
Insert into  [EzyFreight].[dbo].[UserRoles]
Select * from [CPSQLWEB01].[EzyFreight].[dbo].[UserRoles] with (NOLOCK)



Truncate table [EzyFreight].[dbo].[Users]
Insert into  [EzyFreight].[dbo].[Users]
Select * from  [CPSQLWEB01].[CPPLWeb_8_3].[dbo].[Users] with (NOLOCK)

Truncate table [EzyFreight].[dbo].[tblAPIEnquiry]
Insert into  [EzyFreight].[dbo].[tblAPIEnquiry]
Select * from  [CPSQLWEB01].[EzyFreight].[dbo].[tblAPIEnquiry] with (NOLOCK)

--Truncate table [dbo].[tblRedirectedConsignment]
--Insert into  [dbo].[tblRedirectedConsignment]
----Select * from  [CPSQLWEB01].[EzyFreight].[dbo].[tblRedirectedConsignment]
--Select *,0 as isscannerGWdataprocessed from  [CPSQLWEB01].[EzyFreight].[dbo].[tblRedirectedConsignment]


--Truncate table [dbo].[tblRedirectedItemLabel]
--Insert into  [dbo].[tblRedirectedItemLabel]
--Select * from  [CPSQLWEB01].[EzyFreight].[dbo].[tblRedirectedItemLabel]

--------------------------------------------------------[dbo].[tblRedirectedConsignment]-----------------------------------------------------

select * into #tblRedirectedConsignmentTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedConsignment] cr with (NOLOCK)  
where DatePart("m", cr.CreatedDateTime) = DatePart("m", DateAdd("m", -1, getdate()))
and DatePart("yyyy", cr.CreatedDateTime) = DatePart("yyyy", DateAdd("m", -1, getdate()))

DELETE [EzyFreight].[dbo].[tblRedirectedConsignment] from [dbo].[tblRedirectedConsignment] cd (NOLOCK) 
JOIN #tblRedirectedConsignmentTMP cs (NOLOCK) 
ON cd.[ReConsignmentID]=cs.[ReConsignmentID]
where cd.[ReConsignmentID]=cs.[ReConsignmentID]


INSERT INTO [tblRedirectedConsignment](ReConsignmentID
      ,UniqueID
      ,ConsignmentCode
      ,SelectedDeliveryOption
      ,PickupAddressID
      ,CurrentDeliveryAddressID
      ,NewDeliveryAddressID
      ,TotalWeight
      ,TotalVolume
      ,ServiceType
      ,RateCardID
      ,CurrentETA
      ,NewETA
      ,NoOfItems
      ,ConsignmentStatus
      ,SpecialInstruction
      ,Terms
      ,ATL
      ,ConfirmATLInsuranceVoid
      ,ConfirmDeliveryAddress
      ,IsProcessed
      ,SortCode
      ,PaymentRefNo
      ,MerchantReferenceCode
      ,SubscriptionID
      ,AuthorizationCode
      ,CalculatedTotal
      ,CreditCardSurcharge
      ,NetTotal
         ,CreatedDateTime
      ,CreatedBy
      ,UpdatedDateTTime
      ,UpdatedBy)
Select cs.ReConsignmentID
      ,cs.UniqueID
      ,cs.ConsignmentCode
      ,cs.SelectedDeliveryOption
      ,cs.PickupAddressID
      ,cs.CurrentDeliveryAddressID
      ,cs.NewDeliveryAddressID
      ,cs.TotalWeight
      ,cs.TotalVolume
      ,cs.ServiceType
      ,cs.RateCardID
      ,cs.CurrentETA
      ,cs.NewETA
      ,cs.NoOfItems
      ,cs.ConsignmentStatus
      ,cs.SpecialInstruction
      ,cs.Terms
      ,cs.ATL
      ,cs.ConfirmATLInsuranceVoid
      ,cs.ConfirmDeliveryAddress
      ,cs.IsProcessed
      ,cs.SortCode
      ,cs.PaymentRefNo
      ,cs.MerchantReferenceCode
      ,cs.SubscriptionID
      ,cs.AuthorizationCode
      ,cs.CalculatedTotal
      ,cs.CreditCardSurcharge
      ,cs.NetTotal
         ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
from #tblRedirectedConsignmentTMP cs (NOLOCK)
--where not exists (select 1 from [tblRedirectedConsignment] cd (NOLOCK) where cd.[ReConsignmentID]=cs.[ReConsignmentID]);

---------------------------------------------------------------[dbo].[tblRedirectedItemLabel]----------------------------------------------------------

select * into #tblRedirectedItemLabelTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedItemLabel] cr with (NOLOCK) 
where DatePart("m", cr.CreatedDateTime) = DatePart("m", DateAdd("m", -1, getdate()))
and DatePart("yyyy", cr.CreatedDateTime) = DatePart("yyyy", DateAdd("m", -1, getdate()))

DELETE [EzyFreight].[dbo].[tblRedirectedItemLabel] from [dbo].[tblRedirectedItemLabel] cd (NOLOCK) 
JOIN #tblRedirectedItemLabelTMP cs (NOLOCK) 
ON cd.[ReItemLabelID]=cs.[ReItemLabelID]
where cd.[ReItemLabelID]=cs.[ReItemLabelID]


INSERT INTO [tblRedirectedItemLabel] 
      (cs.ReItemLabelID
      ,cs.ReConsignmentID
      ,cs.LabelNumber
      ,cs.Length
      ,cs.Width
      ,cs.Height
      ,cs.CubicWeight
      ,cs.PhysicalWeight
      ,cs.LastActivity
      ,cs.LastActivityDateTime
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy)
select cs.ReItemLabelID
      ,cs.ReConsignmentID
      ,cs.LabelNumber
      ,cs.Length
      ,cs.Width
      ,cs.Height
      ,cs.CubicWeight
      ,cs.PhysicalWeight
      ,cs.LastActivity
      ,cs.LastActivityDateTime
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
      from #tblRedirectedItemLabelTMP cs (NOLOCK)
--where not exists (select 1 from [tblRedirectedItemLabel] cd (NOLOCK) where cd.[ReItemLabelID]=cs.[ReItemLabelID]);
         

end


GO
