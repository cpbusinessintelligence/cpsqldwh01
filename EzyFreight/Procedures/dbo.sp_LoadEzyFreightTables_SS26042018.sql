SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[sp_LoadEzyFreightTables_SS26042018] as

begin



  --'=====================================================================

    --' CP -Stored Procedure -sp_LoadEzyFreightTables

    --' ---------------------------

    --' Purpose: LoadEzyFreightTables-----

    --' Developer: Abhigna (Couriers Please Pty Ltd)

    --' Date: 08 July 2015

    --' Copyright: 2014 Couriers Please Pty Ltd

    --' Change Log: 

    --' Date          Who     Ver     Reason                                            Bookmark

    --' ----          ---     ---     -----                                             -------

    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708



    --'=====================================================================



truncate table [dbo].[aspnet_Membership]

Insert into [dbo].[aspnet_Membership]

SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[aspnet_Membership]



Truncate table aspnet_Users

Insert into [aspnet_Users]

SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[aspnet_Users]



Truncate table EventLog;

Insert into EventLog

SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[EventLog];



Truncate table [EventLogConfig];

Insert into EventLogConfig

SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[EventLogConfig]



Truncate table [EventLogTypes];

 Insert into EventLogTypes

  SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[EventLogTypes];



Truncate table [Parameters]

Insert into [Parameters]

SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[Parameters]



Truncate table RoleGroups

Insert into RoleGroups

SELECT * FROM [CPSQLWEB01].[EzyFreight].[dbo].[RoleGroups]



Truncate table [dbo].[Roles]

Insert into Roles

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[Roles]



Truncate table [dbo].[tblActivityLogging]

Insert into [dbo].[tblActivityLogging]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblActivityLogging]



Truncate table [dbo].[tblAddress]

Insert into [dbo].[tblAddress]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblAddress]



ALTER TABLE  [dbo].[tblBooking] NOCHECK CONSTRAINT ALL

Truncate table [dbo].[tblBooking]

Insert into [dbo].[tblBooking]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblBooking]

ALTER TABLE  [dbo].[tblBooking]  CHECK CONSTRAINT ALL



--Truncate table [dbo].[tblBranchAddresses]

--Insert into [dbo].[tblBranchAddresses]

--Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblBranchAddresses]





Truncate table [dbo].[tblClient]

Insert into [dbo].[tblClient]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblClient]



Truncate table [dbo].[tblClientCPPLServiceAccess]

Insert into  [dbo].[tblClientCPPLServiceAccess]

Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblClientCPPLServiceAccess]



Truncate table [dbo].[tblCommunity]

Insert into [dbo].[tblCommunity]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCommunity]





Truncate table [dbo].[tblCompany]

Insert into  [dbo].[tblCompany]([CompanyID]
      ,[AccountNumber]
      ,[CompanyName]
      ,[ParentAccountNo]
      ,[ABN]
      ,[AddressID1]
      ,[PostalAddressID]
      ,[ParentCompanyName]
      ,[BilingAccountNo]
      ,[IsProntoExtracted]
      ,[RateCardCategory]
      ,[IntRateCardCategory]
      ,[SameBillingAddress]
      ,[BillingFname]
      ,[BillingLname]
      ,[BllingEmail]
      ,[BillingPhone]
      ,[BillingAddress]
      ,[CreditLimit]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[IsExistingCustomer]
      ,[Branch]
      ,[ExistingAccountNumber]
      ,[CouponNo]
      ,[IsRegularShipper]
      ,[ShipperCreatedBy]
      ,[ShipperCreatedDateTime]
      ,[ShipperUpdatedBy]
      ,[ShipperUpdatedDateTime]
      ,[IsDirectDebitDone]
      ,[IsAggregatorPricing])

Select [CompanyID]
      ,[AccountNumber]
      ,[CompanyName]
      ,[ParentAccountNo]
      ,[ABN]
      ,[AddressID1]
      ,[PostalAddressID]
      ,[ParentCompanyName]
      ,[BilingAccountNo]
      ,[IsProntoExtracted]
      ,[RateCardCategory]
      ,[IntRateCardCategory]
      ,[SameBillingAddress]
      ,[BillingFname]
      ,[BillingLname]
      ,[BllingEmail]
      ,[BillingPhone]
      ,[BillingAddress]
      ,[CreditLimit]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[IsExistingCustomer]
      ,[Branch]
      ,[ExistingAccountNumber]
      ,[CouponNo]
      ,[IsRegularShipper]
      ,[ShipperCreatedBy]
      ,[ShipperCreatedDateTime]
      ,[ShipperUpdatedBy]
      ,[ShipperUpdatedDateTime]
      ,[IsDirectDebitDone]
      ,[IsAggregatorPricing]
 from [CPSQLWEB01].[EzyFreight]. [dbo].[tblCompany]







Truncate table [dbo].[tblCompanyUsers]

Insert into  [dbo].[tblCompanyUsers]

Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblCompanyUsers]



MERGE tblConsignment AS cd

USING (select [ConsignmentID]

      ,[ConsignmentCode]

      ,[UserID]

      ,[IsRegUserConsignment]

      ,[PickupID]

      ,[DestinationID]

      ,[ContactID]

      ,[TotalWeight]

      ,[TotalMeasureWeight]

      ,[TotalVolume]

      ,[TotalMeasureVolume]

      ,[NoOfItems]

      ,[SpecialInstruction]

      ,[CustomerRefNo]

      ,[ConsignmentPreferPickupDate]

      ,[ConsignmentPreferPickupTime]

      ,[ClosingTime]

      ,[DangerousGoods]

      ,[Terms]

      ,[RateCardID]

      ,[LastActivity]

      ,[LastActiivityDateTime]

      ,[ConsignmentStatus]

     -- ,[EDIDataProcessed]

      ,[ProntoDataExtracted]

      ,[IsBilling]

      ,[IsManifested]

      ,[CreatedDateTime]

      ,[CreatedBy]

      ,[UpdatedDateTTime]

      ,[UpdatedBy]

      ,[IsInternational]

      ,[IsDocument]

      ,[IsSignatureReq]

      ,[IfUndelivered]

      ,[ReasonForExport]

      ,[TypeOfExport]

      ,[Currency]

      ,[IsInsurance]

      ,[IsIdentity]

      ,[IdentityType]

      ,[IdentityNo]

      ,[Country-ServiceArea-FacilityCode]

      ,[InternalServiceCode]

      ,[NetSubTotal]

      ,[IsATl]

      ,[IsReturnToSender]

      ,[HasReadInsuranceTc]

      ,[NatureOfGoods]

      ,[OriginServiceAreaCode]

      ,[ProductShortName]

      ,[SortCode]

      ,[ETA]

      ,[CTIManifestExtracted]

      ,[isprocessed]

      ,[IsAccountCustomer]

      ,[InsuranceAmount]

      ,[CourierPickupDate]

      ,[CalculatedTotal]

      ,[CalculatedGST]

      ,[ClientCode] 

	  ,PromotionCode

	  ,InsuranceCategory

	  ,0 as IsHubApiProcessed from [cpsqlweb01].[Ezyfreight].[dbo].[tblConsignment] ) AS cs ON cs.consignmentcode=cd.consignmentcode  collate SQL_Latin1_General_CP1_CI_AS

when matched then update set 

       [ConsignmentID]=cs.ConsignmentID

      ,[ConsignmentCode]=cs.ConsignmentCode

      ,[UserID]=cs.UserID

      ,[IsRegUserConsignment]=cs.IsRegUserConsignment

      ,[PickupID]=cs.PickupID

      ,[DestinationID]=cs.DestinationID

      ,[ContactID]=cs.ContactID

      ,[TotalWeight]=cs.TotalWeight

      ,[TotalMeasureWeight]=cs.TotalMeasureWeight

      ,[TotalVolume]=cs.TotalVolume

      ,[TotalMeasureVolume]=cs.TotalMeasureVolume

      ,[NoOfItems]=cs.NoOfItems

      ,[SpecialInstruction]=cs.SpecialInstruction

      ,[CustomerRefNo]=cs.CustomerRefNo

      ,[ConsignmentPreferPickupDate]=cs.ConsignmentPreferPickupDate

      ,[ConsignmentPreferPickupTime]=cs.ConsignmentPreferPickupTime

      ,[ClosingTime]=cs.ClosingTime

      ,[DangerousGoods]=cs.DangerousGoods

      ,[Terms]=cs.Terms

      ,[RateCardID]=cs.RateCardID

      ,[LastActivity]=cs.LastActivity

      ,[LastActiivityDateTime]=cs.LastActiivityDateTime

      ,[ConsignmentStatus]=cs.ConsignmentStatus

     -- ,[EDIDataProcessed]=cs.EDIDataProcessed

      ,[ProntoDataExtracted]=cs.ProntoDataExtracted

      ,[IsBilling]=cs.IsBilling

      ,[IsManifested]=cs.IsManifested

      ,[CreatedDateTime]=cs.CreatedDateTime

      ,[CreatedBy]=cs.CreatedBy

      ,[UpdatedDateTTime]=cs.UpdatedDateTTime

      ,[UpdatedBy]=cs.UpdatedBy

      ,[IsInternational]=cs.IsInternational

      ,[IsDocument]=cs.IsDocument

      ,[IsSignatureReq]=cs.IsSignatureReq

      ,[IfUndelivered]=cs.IfUndelivered

      ,[ReasonForExport]=cs.ReasonForExport

      ,[TypeOfExport]=cs.TypeOfExport

      ,[Currency]=cs.Currency

      ,[IsInsurance]=cs.IsInsurance

      ,[IsIdentity]=cs.IsIdentity

      ,[IdentityType]=cs.IdentityType

      ,[IdentityNo]=cs.IdentityNo

      ,[Country-ServiceArea-FacilityCode]=cs.[Country-ServiceArea-FacilityCode]

      ,[InternalServiceCode]=cs.InternalServiceCode

      ,[NetSubTotal]=cs.NetSubTotal

      ,[IsATl]=cs.IsATl

      ,[IsReturnToSender]=cs.IsReturnToSender

      ,[HasReadInsuranceTc]=cs.HasReadInsuranceTc

      ,[NatureOfGoods]=cs.NatureOfGoods

      ,[OriginServiceAreaCode]=cs.OriginServiceAreaCode

      ,[ProductShortName]=cs.ProductShortName

      ,[SortCode]=cs.SortCode

      ,[ETA]=cs.ETA

	  ,[CTIManifestExtracted]=cs.CTIManifestExtracted

	  ,isprocessed=cs.isprocessed

      ,[IsAccountCustomer]=cs.IsAccountCustomer

      ,[InsuranceAmount]=cs.InsuranceAmount

      ,[CourierPickupDate]=cs.CourierPickupDate

      ,[CalculatedTotal]=cs.CalculatedTotal

      ,[CalculatedGST]=cs.CalculatedGST

	  ,clientcode=cs.clientcode

      ,PromotionCode=cs.PromotionCode

	  ,InsuranceCategory = cs.InsuranceCategory

	  ,IsHubApiProcessed= cs.IsHubApiProcessed

														

when not matched then INSERT values(cs.ConsignmentID

      ,cs.ConsignmentCode

      ,cs.UserID

      ,cs.IsRegUserConsignment

      ,cs.PickupID

      ,cs.DestinationID

      ,cs.ContactID

      ,cs.TotalWeight

      ,cs.TotalMeasureWeight

      ,cs.TotalVolume

      ,cs.TotalMeasureVolume

      ,cs.NoOfItems

      ,cs.SpecialInstruction

      ,cs.CustomerRefNo

      ,cs.ConsignmentPreferPickupDate

      ,cs.ConsignmentPreferPickupTime

      ,cs.ClosingTime

      ,cs.DangerousGoods

      ,cs.Terms

      ,cs.RateCardID

      ,cs.LastActivity

      ,cs.LastActiivityDateTime

      ,cs.ConsignmentStatus

      ,0

      ,cs.ProntoDataExtracted

      ,cs.IsBilling

      ,cs.IsManifested

      ,cs.CreatedDateTime

      ,cs.CreatedBy

      ,cs.UpdatedDateTTime

      ,cs.UpdatedBy

      ,cs.IsInternational

      ,cs.IsDocument

      ,cs.IsSignatureReq

      ,cs.IfUndelivered

      ,cs.ReasonForExport

      ,cs.TypeOfExport

      ,cs.Currency

      ,cs.IsInsurance

      ,cs.IsIdentity

      ,cs.IdentityType

      ,cs.IdentityNo

      ,cs.[Country-ServiceArea-FacilityCode]

      ,cs.InternalServiceCode

      ,cs.NetSubTotal

      ,cs.IsATl

      ,cs.IsReturnToSender

      ,cs.HasReadInsuranceTc

      ,cs.NatureOfGoods

      ,cs.OriginServiceAreaCode

      ,cs.ProductShortName

      ,cs.SortCode

      ,cs.ETA

	  ,cs.CTIManifestExtracted

	  ,cs.isprocessed

      ,cs.IsAccountCustomer

      ,cs.InsuranceAmount

      ,cs.CourierPickupDate

      ,cs.CalculatedTotal

      ,cs.CalculatedGST

	  ,cs.clientcode

	  ,0

	  ,0

	  ,cs.PromotionCode
	  ,0
	  ,cs.InsuranceCategory

	  );





Truncate table [dbo].[tblConsignmentImage]

Insert into [dbo].[tblConsignmentImage]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblConsignmentImage]



Truncate table [dbo].[tblConsignmentService]

Insert into  [dbo].[tblConsignmentService]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblConsignmentService]



Truncate table [dbo].[tblConsignmentStaging]

Insert into [dbo].[tblConsignmentStaging]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblConsignmentStaging]



Truncate table [dbo].[tblContactUs]

Insert into [dbo].[tblContactUs]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblContactUs]





Truncate table [dbo].[tblCPPLServices]

Insert into [dbo].[tblCPPLServices]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCPPLServices]



Truncate table [dbo].[tblCustomDeclaration]

Insert into  [dbo].[tblCustomDeclaration]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCustomDeclaration]



Truncate table [dbo].[tblCustomerRate]

Insert into [dbo].[tblCustomerRate]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblCustomerRate]



Truncate table [dbo].[tblDHLBarCodeImage]

Insert into [dbo].[tblDHLBarCodeImage]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblDHLBarCodeImage]





Truncate table [dbo].[tblErrorLog]

Insert into [dbo].[tblErrorLog]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblErrorLog]



Truncate table [dbo].[tblEzyNetCustomer]

Insert into  [dbo].[tblEzyNetCustomer]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblEzyNetCustomer] 

--where phoneno='243855228'



--Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblEzyNetCustomer] where phoneno like '%243855228%'



Truncate table [dbo].[tblFranchise]

Insert into [dbo].[tblFranchise]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblFranchise]



Truncate table [dbo].[tblFranchiseList]

Insert into [dbo].[tblFranchiseList]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblFranchiseList]



Truncate table [dbo].[tblFreightType]

Insert into [dbo].[tblFreightType]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblFreightType]





Truncate table [dbo].[tblInsurance]

Insert into [dbo].[tblInsurance]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblInsurance]



Truncate table [dbo].[tblInvoice]

Insert into  [dbo].[tblInvoice]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblInvoice]



Truncate table [dbo].[tblInvoiceImage]

Insert into [dbo].[tblInvoiceImage]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblInvoiceImage]



Truncate table [dbo].[tblItemLabel]

Insert into [dbo].[tblItemLabel]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblItemLabel]



Truncate table [dbo].[tblItemLabelImage]

Insert into [dbo].[tblItemLabelImage]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblItemLabelImage]



Truncate table [dbo].[tblInsuranceCategory]

Insert into  [dbo].[tblInsuranceCategory]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblInsuranceCategory]



Truncate table [dbo].[tblOpenAccount]

Insert into [dbo].[tblOpenAccount]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblOpenAccount]



Truncate table [dbo].[tblQuote]

Insert into [dbo].[tblQuote]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblQuote]



Truncate table [dbo].[tblRateCard]

Insert into [dbo].[tblRateCard]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRateCard]



Truncate table [dbo].[tblRateCardDetail]

Insert into  [dbo].[tblRateCardDetail]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRateCardDetail]



Truncate table [dbo].[tblRecharge]

Insert into [dbo].[tblRecharge]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRecharge]



Truncate table [dbo].[tblRedelivery]

Insert into [dbo].[tblRedelivery]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblRedelivery]



Truncate table [dbo].[tblRefund]

Insert into  [dbo].[tblRefund]

Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblRefund]





Truncate table [dbo].[tblSalesOrder]

Insert into [dbo].[tblSalesOrder]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblSalesOrder]



Truncate table [dbo].[tblSalesOrderDetail]

Insert into  [dbo].[tblSalesOrderDetail]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblSalesOrderDetail]



Truncate table [dbo].[tblService]

Insert into [dbo].[tblService]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblService]



Truncate table [dbo].[tblState]

Insert into  [dbo].[tblState]

Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblState]





Truncate table [dbo].[tblStatus]

Insert into [dbo].[tblStatus]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblStatus]



Truncate table [dbo].[tblTracking]

Insert into  [dbo].[tblTracking]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblTracking]



Truncate table [dbo].[tblTrackingStaging]

Insert into  [dbo].[tblTrackingStaging]

Select * from [CPSQLWEB01].[EzyFreight]. [dbo].[tblTrackingStaging]



Truncate table [dbo].[tblZone]

Insert into  [dbo].[tblZone]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[tblZone]





Truncate table [dbo].[UserRoles]

Insert into  [dbo].[UserRoles]

Select * from [CPSQLWEB01].[EzyFreight].[dbo].[UserRoles]







Truncate table [dbo].[Users]

Insert into  [dbo].[Users]

Select * from  [CPSQLWEB01].[CPPLWeb_8_3].[dbo].[Users]



Truncate table [dbo].[tblAPIEnquiry]

Insert into  [dbo].[tblAPIEnquiry]

Select * from  [CPSQLWEB01].[EzyFreight].[dbo].[tblAPIEnquiry]



--Truncate table [dbo].[tblRedirectedConsignment]

--Insert into  [dbo].[tblRedirectedConsignment]

----Select * from  [CPSQLWEB01].[EzyFreight].[dbo].[tblRedirectedConsignment]

--Select *,0 as isscannerGWdataprocessed from  [CPSQLWEB01].[EzyFreight].[dbo].[tblRedirectedConsignment]





--Truncate table [dbo].[tblRedirectedItemLabel]

--Insert into  [dbo].[tblRedirectedItemLabel]

--Select * from  [CPSQLWEB01].[EzyFreight].[dbo].[tblRedirectedItemLabel]









	   MERGE [tblRedirectedConsignment] AS cd

USING (select ReConsignmentID

      ,UniqueID

      ,ConsignmentCode

      ,SelectedDeliveryOption

      ,PickupAddressID

      ,CurrentDeliveryAddressID

      ,NewDeliveryAddressID

      ,TotalWeight

      ,TotalVolume

      ,ServiceType

      ,RateCardID

      ,CurrentETA

      ,NewETA

      ,NoOfItems

      ,ConsignmentStatus

      ,SpecialInstruction

      ,Terms

      ,ATL

      ,ConfirmATLInsuranceVoid

      ,ConfirmDeliveryAddress

      ,IsProcessed

      ,SortCode

      ,PaymentRefNo

      ,MerchantReferenceCode

      ,SubscriptionID

      ,AuthorizationCode

      ,CalculatedTotal

      ,CreditCardSurcharge

      ,NetTotal

	  ,CreatedDateTime

      ,CreatedBy

      ,UpdatedDateTTime

      ,UpdatedBy from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedConsignment]  where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))) 

AS cs ON cs.[ReConsignmentID]=cd.[ReConsignmentID]

when matched then update set 



       [ReConsignmentID]=cs.[ReConsignmentID]

      ,[UniqueID]=cs.[UniqueID]

      ,[ConsignmentCode]=cs.[ConsignmentCode]

      ,[SelectedDeliveryOption]=cs.[SelectedDeliveryOption]

      ,[PickupAddressID]=cs.[PickupAddressID]

      ,[CurrentDeliveryAddressID]=cs.[CurrentDeliveryAddressID]

      ,[NewDeliveryAddressID]=cs.[NewDeliveryAddressID]

      ,[TotalWeight]=cs.[TotalWeight]

      ,[TotalVolume]=cs.[TotalVolume]

      ,[ServiceType]=cs.[ServiceType]

      ,[RateCardID]=cs.[RateCardID]

      ,[CurrentETA]=cs.[CurrentETA]

      ,[NewETA]=cs.[NewETA]

      ,[NoOfItems]=cs.[NoOfItems]

      ,[ConsignmentStatus]=cs.[ConsignmentStatus]

      ,[SpecialInstruction]=cs.[SpecialInstruction]

      ,[Terms]=cs.[Terms]

      ,[ATL]=cs.[ATL]

      ,[ConfirmATLInsuranceVoid]=cs.[ConfirmATLInsuranceVoid]

      ,[ConfirmDeliveryAddress]=cs.[ConfirmDeliveryAddress]

      ,[IsProcessed]=cs.[IsProcessed]

      ,[SortCode]=cs.[SortCode]

      ,[PaymentRefNo]=cs.[PaymentRefNo]

      ,[MerchantReferenceCode]=cs.[MerchantReferenceCode]

      ,[SubscriptionID]=cs.[SubscriptionID]

      ,[AuthorizationCode]=cs.[AuthorizationCode]

      ,[CalculatedTotal]=cs.[CalculatedTotal]

      ,[CreditCardSurcharge]=cs.[CreditCardSurcharge]

      ,[NetTotal]=cs.[NetTotal]

      ,[CreatedDateTime]=cs.[CreatedDateTime]

      ,[CreatedBy]=cs.[CreatedBy]

	  ,UpdatedDateTTime=cs.UpdatedDateTTime

      ,[UpdatedBy]=cs.[UpdatedBy]

      

 

when not matched then INSERT values(

	   cs.ReConsignmentID

      ,cs.UniqueID

      ,cs.ConsignmentCode

      ,cs.SelectedDeliveryOption

      ,cs.PickupAddressID

      ,cs.CurrentDeliveryAddressID

      ,cs.NewDeliveryAddressID

      ,cs.TotalWeight

      ,cs.TotalVolume

      ,cs.ServiceType

      ,cs.RateCardID

      ,cs.CurrentETA

      ,cs.NewETA

      ,cs.NoOfItems

      ,cs.ConsignmentStatus

      ,cs.SpecialInstruction

      ,cs.Terms

      ,cs.ATL

      ,cs.ConfirmATLInsuranceVoid

      ,cs.ConfirmDeliveryAddress

      ,cs.IsProcessed

      ,cs.SortCode

      ,cs.PaymentRefNo

      ,cs.MerchantReferenceCode

      ,cs.SubscriptionID

      ,cs.AuthorizationCode

      ,cs.CalculatedTotal

      ,cs.CreditCardSurcharge

      ,cs.NetTotal

	  ,cs.CreatedDateTime

      ,cs.CreatedBy

      ,cs.UpdatedDateTTime

      ,cs.UpdatedBy

	  ,0);





	  

	   MERGE [tblRedirectedItemLabel] AS cd

USING (select * from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedItemLabel]  where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))) 

AS cs ON cs.[ReItemLabelID]=cd.[ReItemLabelID]

when matched then update set 



       [ReItemLabelID]=cs.[ReItemLabelID]

      ,[ReConsignmentID]=cs.[ReConsignmentID]

      ,[LabelNumber]=cs.[LabelNumber]

      ,[Length]=cs.[Length]

      ,[Width]=cs.[Width]

      ,[Height]=cs.[Height]

      ,[CubicWeight]=cs.[CubicWeight]

      ,[PhysicalWeight]=cs.[PhysicalWeight]

      ,[LastActivity]=cs.[LastActivity]

      ,[LastActivityDateTime]=cs.[LastActivityDateTime]

      ,[CreatedDateTime]=cs.[CreatedDateTime]

      ,[CreatedBy]=cs.[CreatedBy]

	  ,UpdatedDateTime=cs.UpdatedDateTime

      ,[UpdatedBy]=cs.[UpdatedBy]

      

 

when not matched then INSERT values(

	  cs.ReItemLabelID

      ,cs.ReConsignmentID

      ,cs.LabelNumber

      ,cs.Length

      ,cs.Width

      ,cs.Height

      ,cs.CubicWeight

      ,cs.PhysicalWeight

      ,cs.LastActivity

      ,cs.LastActivityDateTime

	  ,cs.CreatedDateTime

      ,cs.CreatedBy

      ,cs.UpdatedDateTime

      ,cs.UpdatedBy);







end




GO
