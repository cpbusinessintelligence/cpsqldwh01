SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadIncrementalEzyFreightTables_New] as
begin

---------------------------------------------[dbo].[tblConsignment]----------------------------------------------------------

select * into #tblConsignmentTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblConsignment] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))

DElETE dbo.tblConsignment from dbo.tblConsignment cd (NOLOCK)
         join #tblConsignmentTMP cs (NOLOCK) on cd.ConsignmentID = cs.ConsignmentID
         where cd.ConsignmentID = cs.ConsignmentID

INSERT INTO tblConsignment ([ConsignmentID]
      ,[ConsignmentCode]
      ,[UserID]
      ,[IsRegUserConsignment]
      ,[PickupID]
      ,[DestinationID]
      ,[ContactID]
      ,[TotalWeight]
      ,[TotalMeasureWeight]
      ,[TotalVolume]
      ,[TotalMeasureVolume]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[CustomerRefNo]
      ,[ConsignmentPreferPickupDate]
      ,[ConsignmentPreferPickupTime]
      ,[ClosingTime]
      ,[DangerousGoods]
      ,[Terms]
      ,[RateCardID]
      ,[LastActivity]
      ,[LastActiivityDateTime]
      ,[ConsignmentStatus]
      ,[ProntoDataExtracted]
      ,[IsBilling]
      ,[IsManifested]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[IsInternational]
      ,[IsDocument]
      ,[IsSignatureReq]
      ,[IfUndelivered]
      ,[ReasonForExport]
      ,[TypeOfExport]
      ,[Currency]
      ,[IsInsurance]
      ,[IsIdentity]
      ,[IdentityType]
      ,[IdentityNo]
      ,[Country-ServiceArea-FacilityCode]
      ,[InternalServiceCode]
      ,[NetSubTotal]
      ,[IsATl]
      ,[IsReturnToSender]
      ,[HasReadInsuranceTc]
      ,[NatureOfGoods]
      ,[OriginServiceAreaCode]
      ,[ProductShortName]
      ,[SortCode]
      ,[ETA]
      ,[CTIManifestExtracted]
      ,[isprocessed]
      ,[IsAccountCustomer]
      ,[InsuranceAmount]
      ,[CourierPickupDate]
      ,[CalculatedTotal]
      ,[CalculatedGST]
      ,[ClientCode] 
      ,PromotionCode
      ,IsHubApiProcessed)
Select cs.ConsignmentID
      ,cs.ConsignmentCode
      ,cs.UserID
      ,cs.IsRegUserConsignment
      ,cs.PickupID
      ,cs.DestinationID
      ,cs.ContactID
      ,cs.TotalWeight
      ,cs.TotalMeasureWeight
      ,cs.TotalVolume
      ,cs.TotalMeasureVolume
      ,cs.NoOfItems
      ,cs.SpecialInstruction
      ,cs.CustomerRefNo
      ,cs.ConsignmentPreferPickupDate
      ,cs.ConsignmentPreferPickupTime
      ,cs.ClosingTime
      ,cs.DangerousGoods
      ,cs.Terms
      ,cs.RateCardID
      ,cs.LastActivity
      ,cs.LastActiivityDateTime
      ,cs.ConsignmentStatus
      ,cs.ProntoDataExtracted
      ,cs.IsBilling
      ,cs.IsManifested
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
      ,cs.IsInternational
      ,cs.IsDocument
      ,cs.IsSignatureReq
      ,cs.IfUndelivered
      ,cs.ReasonForExport
      ,cs.TypeOfExport
      ,cs.Currency
      ,cs.IsInsurance
      ,cs.IsIdentity
      ,cs.IdentityType
      ,cs.IdentityNo
      ,cs.[Country-ServiceArea-FacilityCode]
      ,cs.InternalServiceCode
      ,cs.NetSubTotal
      ,cs.IsATl
      ,cs.IsReturnToSender
      ,cs.HasReadInsuranceTc
      ,cs.NatureOfGoods
      ,cs.OriginServiceAreaCode
      ,cs.ProductShortName
      ,cs.SortCode
      ,cs.ETA
      ,cs.CTIManifestExtracted
      ,cs.isprocessed
      ,cs.IsAccountCustomer
      ,cs.InsuranceAmount
      ,cs.CourierPickupDate
      ,cs.CalculatedTotal
      ,cs.CalculatedGST
      ,cs.clientcode
      ,cs.PromotionCode
      ,0
from #tblConsignmentTMP cs (NOLOCK)
--where not exists (select 1 from tblConsignment cd (NOLOCK) where cd.ConsignmentID = cs.ConsignmentID);

-------------------------------------------------------------[dbo].[tblConsignmentstaging]---------------------------------------------

select * into #tblConsignmentstagingTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblConsignmentstaging] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))

Delete [dbo].[tblConsignmentstaging] from [dbo].[tblConsignmentstaging] cd (NOLOCK)
	  join #tblConsignmentstagingTMP cs (NOLOCK)
	  ON cd.ConsignmentStagingID=cs.ConsignmentStagingID 
	  where cd.ConsignmentStagingID=cs.ConsignmentStagingID 

INSERT INTO [dbo].[tblConsignmentstaging]([ConsignmentStagingID]
      ,[UserID]
      ,[IsRegUserConsignment]
      ,[PickupID]
      ,[DestinationID]
      ,[ContactID]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[CustomerRefNo]
      ,[PickupDate]
      ,[PreferPickupTime]
      ,[ClosingTime]
      ,[DangerousGoods]
      ,[Terms]
      ,[ServiceID]
      ,[RateCardID]
      ,[IsProcessed]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[ConsignmentId]
      ,[IsDocument]
      ,[IsSignatureReq]
      ,[IfUndelivered]
      ,[ReasonForExport]
      ,[TypeOfExport]
      ,[Currency]
      ,[IsInsurance]
      ,[IsIdentity]
      ,[IdentityType]
      ,[IdentityNo]
      ,[XMLRequest]
      ,[XMLResponce]
      ,[IsATl]
      ,[IsReturnToSender]
      ,[HasReadInsuranceTc]
      ,[NatureOfGoods]
      ,[SortCode]
      ,[XMLRequestPU]
      ,[XMLResponcePU]
      ,[ETA]
      ,[PaymentRefNo]
      ,[XMLRequestPU_DHL]
      ,[XMLResponcePU_DHL]
      ,[XMLRequestPU_NZPost]
      ,[XMLResponcePU_NZPost]
      ,[MerchantReferenceCode]
      ,[SubscriptionID]
      ,[AuthorizationCode]
      ,[IsAccountCustomer]
      ,[InsuranceAmount]
      ,[CourierPickupDate]
      ,[CalculatedTotal]
      ,[CalculatedGST]
      ,[ClientCode]
      ,[USPSRefNo]
      ,[PromotionCode])
Select cs.ConsignmentStagingID
      ,cs.UserID
      ,cs.IsRegUserConsignment
      ,cs.PickupID
      ,cs.DestinationID
      ,cs.ContactID
      ,cs.TotalWeight
      ,cs.TotalVolume
      ,cs.NoOfItems
      ,cs.SpecialInstruction
      ,cs.CustomerRefNo
      ,cs.PickupDate
      ,cs.PreferPickupTime
      ,cs.ClosingTime
      ,cs.DangerousGoods
      ,cs.Terms
      ,cs.ServiceID
      ,cs.RateCardID
      ,cs.IsProcessed
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
      ,cs.ConsignmentId
      ,cs.IsDocument
      ,cs.IsSignatureReq
      ,cs.IfUndelivered
      ,cs.ReasonForExport
      ,cs.TypeOfExport
      ,cs.Currency
      ,cs.IsInsurance
      ,cs.IsIdentity
      ,cs.IdentityType
      ,cs.IdentityNo
      ,cs.XMLRequest
      ,cs.XMLResponce
      ,cs.IsATl
      ,cs.IsReturnToSender
      ,cs.HasReadInsuranceTc
      ,cs.NatureOfGoods
      ,cs.SortCode
      ,cs.XMLRequestPU
      ,cs.XMLResponcePU
      ,cs.ETA
      ,cs.PaymentRefNo
      ,cs.XMLRequestPU_DHL
      ,cs.XMLResponcePU_DHL
      ,cs.XMLRequestPU_NZPost
      ,cs.XMLResponcePU_NZPost
  ,cs.MerchantReferenceCode
  ,cs.SubscriptionID
  ,cs.AuthorizationCode
  ,cs.IsAccountCustomer
  ,cs.InsuranceAmount
  ,cs.CourierPickupDate 
  ,cs.CalculatedTotal
  ,cs.CalculatedGST
  ,cs.clientcode
  ,cs.USPSRefNo
  ,cs.PromotionCode
from #tblConsignmentstagingTMP cs (NOLOCK)
--where not exists (select 1 from tblConsignmentstaging cd (NOLOCK) where cd.ConsignmentStagingID=cs.ConsignmentStagingID);

----------------------------------------------------------[dbo].[tblBooking]-------------------------------------------------------------
select * into #tblBookingTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblBooking] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))

Delete [tblBooking] from [tblBooking] cd (NOLOCK)
	  join [#tblBookingTMP] cs (NOLOCK)
	  on  cd.BookingID=cs.BookingID
	  where cd.BookingID=cs.BookingID

INSERT INTO [dbo].[tblBooking]
([BookingID]
      ,[PhoneNumber]
      ,[ContactName]
      ,[ContactEmail]
      ,[PickupDate]
      ,[PickupTime]
      ,[OrderCoupons]
      ,[PickupFromCustomer]
      ,[PickupName]
      ,[PickupAddress1]
      ,[PickupAddress2]
      ,[PickupSuburb]
      ,[PickupPhoneNo]
      ,[ContactDetails]
      ,[PickupFrom]
      ,[IsProcessed]
      ,[BookingRefNo]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[Branch]
      ,[PickupEmail]
      ,[PickupPostCode]
      ,[Costom]
      ,[CostomId]
      ,[ConsignmentID])
Select cs.BookingID
      ,cs.PhoneNumber
      ,cs.ContactName
      ,cs.ContactEmail
      ,cs.PickupDate
      ,cs.PickupTime
      ,cs.OrderCoupons
      ,cs.PickupFromCustomer
      ,cs.PickupName
      ,cs.PickupAddress1
      ,cs.PickupAddress2
      ,cs.PickupSuburb
      ,cs.PickupPhoneNo
      ,cs.ContactDetails
      ,cs.PickupFrom
      ,cs.IsProcessed
      ,cs.BookingRefNo
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
      ,cs.Branch
      ,cs.PickupEmail
      ,cs.PickupPostCode
      ,cs.Costom
      ,cs.CostomId
	  ,cs.consignmentid
	  from #tblBookingTMP cs (NOLOCK)
--where not exists (select 1 from tblBooking cd (NOLOCK) where cd.BookingID=cs.BookingID);

------------------------------------------------------[dbo].[tblContactUs]----------------------------------------------------------------------

select * into #tblContactUsTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblContactUs] with (NOLOCK) where convert(date,[CreatedDate])>=convert(date,dateadd("dd",-2,getdate()))

DELETE [dbo].[tblContactUs] from [dbo].[tblContactUs] cd (NOLOCK)
	  join #tblContactUsTMP cs (NOLOCK)
	  ON cd.ID=cs.ID
	  where cd.ID=cs.ID

INSERT INTO [dbo].[tblContactUs]
      ([Id]
      ,[FirstName]
      ,[LastName]
      ,[Email]
      ,[Phone]
      ,[Suburb]
      ,[PostCode]
      ,[StateID]
      ,[Subject]
      ,[HowCanIHelpYou]
      ,[IsSubscribe]
      ,[CreatedDate]
      ,[TrackingNo]
      ,[AddressLine1]
      ,[AddressLine2])
Select cs.Id
      ,cs.FirstName
      ,cs.LastName
      ,cs.Email
      ,cs.Phone
      ,cs.Suburb
      ,cs.PostCode
      ,cs.StateID
      ,cs.Subject
      ,cs.HowCanIHelpYou
      ,cs.IsSubscribe
      ,cs.CreatedDate
	  ,cs.TrackingNo
	  ,cs.AddressLine1
	  ,cs.AddressLine2
	  from #tblContactUsTMP cs (NOLOCK)
	  --where not exists (select 1 from tblContactUs cd (NOLOCK) where cd.ID=cs.ID);

---------------------------------------------------------------[dbo].[tblSalesOrder]---------------------------------------------------

 select * into #tblSalesOrderTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblSalesOrder] with (NOLOCK) where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))
  
DELETE [dbo].[tblSalesOrder] from  [dbo].[tblSalesOrder] cd (NOLOCK)
	  join  #tblSalesOrderTMP cs (NOLOCK)
	  ON cd.salesOrderID=cs.SalesOrderID 
	  where cd.salesOrderID=cs.SalesOrderID 
	  
INSERT INTO [dbo].[tblSalesOrder] 
([SalesOrderID]
      ,[ReferenceNo]
      ,[UserID]
      ,[NoofItems]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[RateCardID]
      ,[GrossTotal]
      ,[GST]
      ,[NetTotal]
      ,[SalesOrderStatus]
      ,[InvoiceNo]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[ConsignmentCode]
      ,[PromotionDiscount]
      ,[DiscountedTotal]
      ,[SalesOrderType])
Select 	cs.SalesOrderID
      ,cs.ReferenceNo
      ,cs.UserID
      ,cs.NoofItems
      ,cs.TotalWeight
      ,cs.TotalVolume
      ,cs.RateCardID
      ,cs.GrossTotal
      ,cs.GST
      ,cs.NetTotal
      ,cs.SalesOrderStatus
      ,cs.InvoiceNo
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.consignmentcode
	  ,cs.[PromotionDiscount]	
	  ,cs.[DiscountedTotal]
	  ,cs.[SalesOrderType]
	  from #tblSalesOrderTMP cs (NOLOCK)
	  --where not exists (select 1 from tblSalesOrder cd (NOLOCK) where cd.salesOrderID=cs.SalesOrderID);

-----------------------------------------------------[dbo].[tblSalesOrderDetail]-------------------------------------------------

select * into #tblSalesOrderDetailTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblSalesOrderDetail] with (NOLOCK) where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))
  
Delete [dbo].[tblSalesOrderDetail] from [dbo].[tblSalesOrderDetail] cd (NOLOCK)
	  join #tblSalesOrderDetailTMP cs (NOLOCK)
	  ON cd.salesOrderdetailID=cs.SalesOrderdetailID
	  where cd.salesOrderdetailID=cs.SalesOrderdetailID

INSERT INTO [dbo].[tblSalesOrderDetail] 
([SalesOrderDetailID]
      ,[SalesOrderID]
      ,[Description]
      ,[LineNo]
      ,[Weight]
      ,[Volume]
      ,[FreightCharge]
      ,[FuelCharge]
      ,[InsuranceCharge]
      ,[ServiceCharge]
      ,[Total]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy])
Select cs.SalesOrderDetailID
      ,cs.SalesOrderID
      ,cs.Description
      ,cs.[LineNo]
      ,cs.[Weight]
      ,cs.Volume
      ,cs.FreightCharge
      ,cs.FuelCharge
      ,cs.InsuranceCharge
      ,cs.ServiceCharge
      ,cs.Total
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  from #tblSalesOrderDetailTMP cs (NOLOCK)
 --where not exists (select 1 from tblSalesOrderDetail cd (NOLOCK) where cd.salesOrderdetailID=cs.SalesOrderdetailID);

 ------------------------------------------------------------[dbo].[tblRecharge]---------------------------------------------------------------------------------

 select * into #tblRechargeTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblRecharge] with (NOLOCK) where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))

Delete [dbo].[tblRecharge] from [dbo].[tblRecharge] cd (NOLOCK)
	  join #tblRechargeTMP cs (NOLOCK)
	  ON cd.ID=cs.ID
	  where cd.ID=cs.ID

INSERT INTO [dbo].[tblRecharge]
([Id]
      ,[ConsignmentID]
      ,[AdminFee]
      ,[Difference]
      ,[ETA]
      ,[ExtensionData]
      ,[GrossInvoiceAmount]
      ,[GSTonGrossInvoiceAmount]
      ,[NetTotal]
      ,[NewCalculatedTotal]
      ,[NewWeightforCalculation]
      ,[ReweighStatus]
      ,[ReweighStatusDescription]
      ,[ServiceCode]
      ,[ServiceDescription]
      ,[SignatureOption]
      ,[IsSignatureReq]
      ,[OrijnalCalculatedTotal]
      ,[PaymentRefNo]
      ,[MerchantReferenceCode]
      ,[IsProcessed]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy])
Select cs.Id
      ,cs.ConsignmentID
      ,cs.AdminFee
      ,cs.Difference
      ,cs.ETA
      ,cs.ExtensionData
      ,cs.GrossInvoiceAmount
      ,cs.GSTonGrossInvoiceAmount
      ,cs.NetTotal
      ,cs.NewCalculatedTotal
      ,cs.NewWeightforCalculation
      ,cs.ReweighStatus
      ,cs.ReweighStatusDescription
      ,cs.ServiceCode
      ,cs.ServiceDescription
      ,cs.SignatureOption
      ,cs.IsSignatureReq
      ,cs.OrijnalCalculatedTotal
      ,cs.PaymentRefNo
      ,cs.MerchantReferenceCode
      ,cs.IsProcessed
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  from #tblRechargeTMP cs (NOLOCK)
 --where not exists (select 1 from [dbo].[tblRecharge] cd (NOLOCK) where cd.ID=cs.ID);

 ----------------------------------------------------------------[dbo].[tblAddress]------------------------------------------------------

 select * into #tblAddressTMP from [cpsqlweb01].[Ezyfreight].[dbo].[tblAddress] with (NOLOCK) where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))

 Delete [dbo].[tblAddress] from [dbo].[tblAddress] cd (NOLOCK)
	  join #tblAddressTMP cs (NOLOCK)
	  ON cd.AddressID=cs.AddressID
	  where cd.AddressID=cs.AddressID

 INSERT INTO [dbo].[tblAddress]([AddressID]
      ,[UserID]
      ,[FirstName]
      ,[LastName]
      ,[CompanyName]
      ,[Email]
      ,[Address1]
      ,[Address2]
      ,[Suburb]
      ,[StateName]
      ,[StateID]
      ,[PostCode]
      ,[Phone]
      ,[Mobile]
      ,[Country]
      ,[CountryCode]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[IsRegisterAddress]
      ,[IsDeleted]
      ,[IsBusiness]
      ,[IsSubscribe]
      ,[IsEDIUser])
Select cs.AddressID
      ,cs.UserID
      ,cs.FirstName
      ,cs.LastName
      ,cs.CompanyName
      ,cs.Email
      ,cs.Address1
      ,cs.Address2
      ,cs.Suburb
      ,cs.StateName
      ,cs.StateID
      ,cs.PostCode
      ,cs.Phone
      ,cs.Mobile
      ,cs.Country
      ,cs.CountryCode
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
      ,cs.IsRegisterAddress
      ,cs.IsDeleted
      ,cs.IsBusiness
      ,cs.IsSubscribe
	  ,cs.IsEDIUser
	  from #tblAddressTMP cs (NOLOCK)
---------------------------------------------------------tblCustomDeclaration----------------------------------------------------------
  select * into #tblCustomDeclarationTMP from [cpsqlweb01].[Ezyfreight].[dbo].tblCustomDeclaration with (NOLOCK) where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))
  
 Delete [dbo].tblCustomDeclaration from [dbo].tblCustomDeclaration cd (NOLOCK)
	  join #tblCustomDeclarationTMP cs (NOLOCK)
	  ON cd.[CustomDeclarationId]=cs.[CustomDeclarationId]
	  where cd.[CustomDeclarationId]=cs.[CustomDeclarationId]  

  INSERT INTO [dbo].tblCustomDeclaration
  ([CustomDeclarationId]
      ,[ConsignmentID]
      ,[ItemDescription]
      ,[ItemInBox]
      ,[UnitPrice]
      ,[SubTotal]
      ,[HSCode]
      ,[CountryofOrigin]
      ,[Currency]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy])
  Select cs.CustomDeclarationId
      ,cs.ConsignmentID
      ,cs.ItemDescription
      ,cs.ItemInBox
      ,cs.UnitPrice
      ,cs.SubTotal
      ,cs.HSCode
      ,cs.CountryofOrigin
      ,cs.Currency
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
	  from #tblCustomDeclarationTMP CS (NOLOCK)

-----------------------------------------------------------[dbo].tblRefund--------------------------------------------------

select * into #tblRefundTMP from [cpsqlweb01].[Ezyfreight].[dbo].tblRefund with (NOLOCK) where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))

 Delete [dbo].tblRefund from [dbo].tblRefund cd (NOLOCK)
	  join #tblRefundTMP cs (NOLOCK)
	  ON cd.[Id]=cs.[Id]
	  where cd.[Id]=cs.[Id]

INSERT INTO [dbo].tblRefund
      ([Id]
      ,[ConsignmentID]
      ,[Reason]
      ,[Amount]
      ,[PaymentRefNo]
      ,[AuthorizationCode]
      ,[IsProcess]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[MerchantReferenceCode]
      ,[RefundCategory])
Select cs.Id
      ,cs.ConsignmentID
      ,cs.Reason
      ,cs.Amount
      ,cs.PaymentRefNo
      ,cs.AuthorizationCode
      ,cs.IsProcess
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
      ,cs.MerchantReferenceCode
	  ,cs.RefundCategory
	  from #tblRefundTMP CS (NOLOCK)
--------------------------------------------------------------[dbo].[Users]----------------------------------------------------------

select * into #UsersTMP from [cpsqlweb01].[CPPLWeb_8_3].[dbo].[Users] with (NOLOCK) where CreatedOnDate>=convert(date,dateadd(HOUR,-5,getdate()))

 Delete [dbo].[Users] from [dbo].[Users] cd (NOLOCK)
	  join #UsersTMP cs (NOLOCK)
	  ON cd.[UserID]=cs.[UserID]
	  where cd.[UserID]=cs.[UserID]

INSERT INTO [dbo].[Users]
([UserID]
      ,[Username]
      ,[FirstName]
      ,[LastName]
      ,[IsSuperUser]
      ,[AffiliateId]
      ,[Email]
      ,[DisplayName]
      ,[UpdatePassword]
      ,[LastIPAddress]
      ,[IsDeleted]
      ,[CreatedByUserID]
      ,[CreatedOnDate]
      ,[LastModifiedByUserID]
      ,[LastModifiedOnDate]
      ,[PasswordResetToken]
      ,[PasswordResetExpiration]
      ,[LowerEmail])
SELECT cs.UserID
      ,cs.Username
      ,cs.FirstName
      ,cs.LastName
      ,cs.IsSuperUser
      ,cs.AffiliateId
      ,cs.Email
      ,cs.DisplayName
      ,cs.UpdatePassword
      ,cs.LastIPAddress
      ,cs.IsDeleted
      ,cs.CreatedByUserID
      ,cs.CreatedOnDate
      ,cs.LastModifiedByUserID
      ,cs.LastModifiedOnDate
      ,cs.PasswordResetToken
      ,cs.PasswordResetExpiration
      ,cs.LowerEmail
	  from #UsersTMP CS (NOLOCK)
----------------------------------------------------------------[dbo].[tblcompany]-------------------------------------------------

select * into #tblcompanyTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblcompany] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate())) 
 
 
 Delete [dbo].[tblcompany]  from [dbo].[tblcompany]  cd (NOLOCK)
	  join #tblcompanyTMP cs (NOLOCK)
	  ON cd.[CompanyID]=cs.[CompanyID]
	  where cd.[CompanyID]=cs.[CompanyID]

INSERT INTO [dbo].[tblcompany]
([CompanyID]
      ,[AccountNumber]
      ,[CompanyName]
      ,[ParentAccountNo]
      ,[ABN]
      ,[AddressID1]
      ,[PostalAddressID]
      ,[ParentCompanyName]
      ,[BilingAccountNo]
      ,[IsProntoExtracted]
      ,[RateCardCategory]
      ,[IntRateCardCategory]
      ,[SameBillingAddress]
      ,[BillingFname]
      ,[BillingLname]
      ,[BllingEmail]
      ,[BillingPhone]
      ,[BillingAddress]
      ,[CreditLimit]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[IsExistingCustomer]
      ,[Branch]
      ,[ExistingAccountNumber]
      ,[CouponNo]
      ,[IsRegularShipper]
      ,[ShipperCreatedBy]
      ,[ShipperCreatedDateTime]
      ,[ShipperUpdatedBy]
      ,[ShipperUpdatedDateTime]
      ,[IsDirectDebitDone])
select cs.CompanyID
      ,cs.AccountNumber
      ,cs.CompanyName
      ,cs.ParentAccountNo
      ,cs.ABN
      ,cs.AddressID1
      ,cs.PostalAddressID
      ,cs.ParentCompanyName
      ,cs.BilingAccountNo
      ,cs.IsProntoExtracted
      ,cs.RateCardCategory
      ,cs.IntRateCardCategory
      ,cs.SameBillingAddress
      ,cs.BillingFname
      ,cs.BillingLname
      ,cs.BllingEmail
      ,cs.BillingPhone
      ,cs.BillingAddress
      ,cs.CreditLimit
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.IsExistingCustomer
      ,cs.Branch
      ,cs.ExistingAccountNumber
      ,cs.CouponNo
	  ,cs.IsRegularShipper
	  ,cs.ShipperCreatedBy
	  ,cs.ShipperCreatedDateTime
	  ,cs.ShipperUpdatedBy
	  ,cs.ShipperUpdatedDateTime
	  ,cs.IsDirectDebitDone
	  from #tblcompanyTMP cs (NOLOCK)
-------------------------------------------------------------------[dbo].[tblcompanyUsers]---------------------------------------------

select * into #tblcompanyUsersTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblcompanyUsers] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate())) 

 Delete [dbo].[tblcompanyUsers]  from [dbo].[tblcompanyUsers] cd (NOLOCK)
	  join #tblcompanyUsersTMP cs (NOLOCK)
	  ON cd.[CompanyUsersID]=cs.[CompanyUsersID]
	  where cd.[CompanyUsersID]=cs.[CompanyUsersID]

INSERT INTO [dbo].[tblcompanyUsers]
([CompanyUsersID]
      ,[UserID]
      ,[CompanyID]
      ,[IsUserDisabled]
      ,[ReasonSubject]
      ,[ReasonDesciption]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[PromotionalCode])
Select cs.CompanyUsersID
      ,cs.UserID
      ,cs.CompanyID
      ,cs.IsUserDisabled
      ,cs.ReasonSubject
      ,cs.ReasonDesciption
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.PromotionalCode from #tblcompanyUsersTMP cs (NOLOCK)

------------------------------------------------------------------[dbo].tblRedelivery--------------------------------------------------

select * into #tblRedeliveryTMP from [cpsqlweb01].[ezyfreight].[dbo].tblRedelivery with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))

 Delete [dbo].tblRedelivery  from [dbo].tblRedelivery cd (NOLOCK)
	  join #tblRedeliveryTMP cs (NOLOCK)
	  ON cd.[RedeliveryID]=cs.[RedeliveryID]
	  where cd.[RedeliveryID]=cs.[RedeliveryID]


INSERT INTO [dbo].tblRedelivery 
([RedeliveryID]
      ,[RedeliveryType]
      ,[CardReferenceNumber]
      ,[LableNumber]
      ,[ConsignmentCode]
      ,[Branch]
      ,[State]
      ,[AttemptedRedeliveryTime]
      ,[SenderName]
      ,[NumberOfItem]
      ,[DestinationName]
      ,[DestinationAddress]
      ,[DestinationSuburb]
      ,[DestinationPostCode]
      ,[SelectedETADate]
      ,[PreferDeliverTimeSlot]
      ,[PreferDeliverTime]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[IsProcessed]
      ,[Firstname]
      ,[Lastname]
      ,[Email]
      ,[PhoneNumber]
      ,[SPInstruction])

Select cs.RedeliveryID
	   ,cs.[RedeliveryType]
      ,cs.CardReferenceNumber
      ,cs.LableNumber
      ,cs.ConsignmentCode
      ,cs.Branch
      ,cs.State
      ,cs.AttemptedRedeliveryTime
      ,cs.SenderName
      ,cs.NumberOfItem
      ,cs.DestinationName
      ,cs.DestinationAddress
      ,cs.DestinationSuburb
      ,cs.DestinationPostCode
      ,cs.SelectedETADate
      ,cs.PreferDeliverTimeSlot
      ,cs.PreferDeliverTime
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
      ,cs.IsProcessed
      ,cs.Firstname
      ,cs.Lastname
      ,cs.Email
      ,cs.PhoneNumber
      ,cs.SPInstruction 
	  from #tblRedeliveryTMP cs (NOLOCK)

-----------------------------------------------------------------[dbo].tblItemLabel------------------------------------------------------

select * into #tblItemLabelTMP from[cpsqlweb01].[ezyfreight].[dbo].tblItemLabel with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))

 Delete [dbo].tblItemLabel  from [dbo].tblItemLabel cd (NOLOCK)
	  join #tblItemLabelTMP cs (NOLOCK)
	  ON cd.[ItemLabelID]=cs.[ItemLabelID]
	  where cd.[ItemLabelID]=cs.[ItemLabelID]

INSERT INTO [dbo].tblItemLabel
      ([ItemLabelID]
      ,[ConsignmentID]
      ,[LabelNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[CubicWeight]
      ,[MeasureLength]
      ,[MeasureWidth]
      ,[MeasureHeight]
      ,[MeasureCubicWeight]
      ,[PhysicalWeight]
      ,[MeasureWeight]
      ,[DeclareVolume]
      ,[LastActivity]
      ,[LastActivityDateTime]
      ,[UnitValue]
      ,[Quantity]
      ,[CountryOfOrigin]
      ,[Description]
      ,[HSTariffNumber]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[DHLBarCode])
Select  [ItemLabelID]
      ,[ConsignmentID]
      ,[LabelNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[CubicWeight]
      ,[MeasureLength]
      ,[MeasureWidth]
      ,[MeasureHeight]
      ,[MeasureCubicWeight]
      ,[PhysicalWeight]
      ,[MeasureWeight]
      ,[DeclareVolume]
      ,[LastActivity]
      ,[LastActivityDateTime]
      ,[UnitValue]
      ,[Quantity]
      ,[CountryOfOrigin]
      ,[Description]
      ,[HSTariffNumber]
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.DHLBarCode from #tblItemLabelTMP cs (NOLOCK)

--------------------------------------------------------------------------[dbo].[tblDHLBarCodeImage]-------------------------------------------------------------

select * into #tblDHLBarCodeImageTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblDHLBarCodeImage]  with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))

 Delete [dbo].[tblDHLBarCodeImage]  from [dbo].[tblDHLBarCodeImage]  cd (NOLOCK)
	  join #tblDHLBarCodeImageTMP cs (NOLOCK)
	  ON cd.[ID]=cs.[ID]
	  where cd.[ID]=cs.[ID]


INSERT INTO [dbo].[tblDHLBarCodeImage] 
      ([Id]
      ,[ConsignmentID]
      ,[AWBCode]
      ,[AWBBarCode]
      ,[OriginDestncode]
      ,[OriginDestnBarcode]
      ,[ClientIDCode]
      ,[ClientIDBarCode]
      ,[DHLRoutingCode]
      ,[DHLRoutingBarCode]
      ,[CreatedDateTime]
      ,[CreatedBy])
Select [Id]
      ,[ConsignmentID]
      ,[AWBCode]
      ,[AWBBarCode]
      ,[OriginDestncode]
      ,[OriginDestnBarcode]
      ,[ClientIDCode]
      ,[ClientIDBarCode]
      ,[DHLRoutingCode]
      ,[DHLRoutingBarCode]
      ,[CreatedDateTime]
      ,[CreatedBy]
from #tblDHLBarCodeImageTMP (NOLOCK)

---------------------------------------------------------------------------[dbo].[tblRedirectedConsignment]-------------------------------------------------------
select * into #tblRedirectedConsignmentTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedConsignment] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))

 Delete [dbo].[tblRedirectedConsignment]  from [dbo].[tblRedirectedConsignment]  cd (NOLOCK)
	  join #tblRedirectedConsignmentTMP cs (NOLOCK)
	  ON cd.[ReConsignmentID]=cs.[ReConsignmentID]
	  where cd.[ReConsignmentID]=cs.[ReConsignmentID]

INSERT INTO [dbo].[tblRedirectedConsignment]
([ReConsignmentID]
      ,[UniqueID]
      ,[ConsignmentCode]
      ,[SelectedDeliveryOption]
      ,[PickupAddressID]
      ,[CurrentDeliveryAddressID]
      ,[NewDeliveryAddressID]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[ServiceType]
      ,[RateCardID]
      ,[CurrentETA]
      ,[NewETA]
      ,[NoOfItems]
      ,[ConsignmentStatus]
      ,[SpecialInstruction]
      ,[Terms]
      ,[ATL]
      ,[ConfirmATLInsuranceVoid]
      ,[ConfirmDeliveryAddress]
      ,[IsProcessed]
      ,[SortCode]
      ,[PaymentRefNo]
      ,[MerchantReferenceCode]
      ,[SubscriptionID]
      ,[AuthorizationCode]
      ,[CalculatedTotal]
      ,[CreditCardSurcharge]
      ,[NetTotal]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[isscannerGWdataprocessed])
Select cs.ReConsignmentID
      ,cs.UniqueID
      ,cs.ConsignmentCode
      ,cs.SelectedDeliveryOption
      ,cs.PickupAddressID
      ,cs.CurrentDeliveryAddressID
      ,cs.NewDeliveryAddressID
      ,cs.TotalWeight
      ,cs.TotalVolume
      ,cs.ServiceType
      ,cs.RateCardID
      ,cs.CurrentETA
      ,cs.NewETA
      ,cs.NoOfItems
      ,cs.ConsignmentStatus
      ,cs.SpecialInstruction
      ,cs.Terms
      ,cs.ATL
      ,cs.ConfirmATLInsuranceVoid
      ,cs.ConfirmDeliveryAddress
      ,cs.IsProcessed
      ,cs.SortCode
      ,cs.PaymentRefNo
      ,cs.MerchantReferenceCode
      ,cs.SubscriptionID
      ,cs.AuthorizationCode
      ,cs.CalculatedTotal
      ,cs.CreditCardSurcharge
      ,cs.NetTotal
	  ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
	  ,0
	  from #tblRedirectedConsignmentTMP cs (NOLOCK)

-----------------------------------------------------------------[dbo].[tblRedirectedItemLabel]--------------------------------------

select * into #tblRedirectedItemLabelTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedItemLabel] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))

Delete [dbo].[tblRedirectedItemLabel]   from [dbo].[tblRedirectedItemLabel]   cd (NOLOCK)
	  join #tblRedirectedItemLabelTMP cs (NOLOCK)
	  ON cd.[ReItemLabelID]=cs.[ReItemLabelID]
	  where cd.[ReItemLabelID]=cs.[ReItemLabelID]


INSERT INTO [dbo].[tblRedirectedItemLabel]
      ([ReItemLabelID]
      ,[ReConsignmentID]
      ,[LabelNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[CubicWeight]
      ,[PhysicalWeight]
      ,[LastActivity]
      ,[LastActivityDateTime]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy])
Select cs.ReItemLabelID
      ,cs.ReConsignmentID
      ,cs.LabelNumber
      ,cs.Length
      ,cs.Width
      ,cs.Height
      ,cs.CubicWeight
      ,cs.PhysicalWeight
      ,cs.LastActivity
      ,cs.LastActivityDateTime
	  ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  from #tblRedirectedItemLabelTMP cs (NOLOCK)

--------------------------------------------------------------------------------- [dbo].[tblAPIEnquiry]-------------------------------

select * into #tblAPIEnquiryTMP from [cpsqlweb01].[ezyfreight].[dbo].[tblAPIEnquiry] with (NOLOCK) where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))


Delete [dbo].[tblAPIEnquiry] from [dbo].[tblAPIEnquiry] cd (NOLOCK)
	  join #tblAPIEnquiryTMP cs (NOLOCK)
	  ON cd.[ID]=cs.[ID]
	  where cd.[ID]=cs.[ID]


INSERT INTO [dbo].[tblAPIEnquiry]
      ([ID]
      ,[CompanyName]
      ,[Address]
      ,[Suburb]
      ,[StateID]
      ,[PostCode]
      ,[Country]
      ,[Website]
      ,[AccountNumber]
      ,[RequesterFname]
      ,[RequesterLname]
      ,[TechContactName]
      ,[TechContactEmail]
      ,[TechContactNumber]
      ,[ShoppingCartUsed]
      ,[Comments]
      ,[CreatedDateTime]
      ,[RequesterEmail]
      ,[ERPInUse])

Select cs.ID
      ,cs.CompanyName
      ,cs.Address
      ,cs.Suburb
      ,cs.StateID
      ,cs.PostCode
      ,cs.Country
      ,cs.Website
      ,cs.AccountNumber
      ,cs.RequesterFname
      ,cs.RequesterLname
      ,cs.TechContactName
      ,cs.TechContactEmail
      ,cs.TechContactNumber
      ,cs.ShoppingCartUsed
      ,cs.Comments
	  ,cs.CreatedDateTime
	  ,cs.RequesterEmail
	  ,cs.ERPInUse
	  from #tblAPIEnquiryTMP cs (NOLOCK)

END







GO
