SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_LoadIncrementalEzyFreightTables_SS05022018] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_LoadEzyFreightTables
    --' ---------------------------
    --' Purpose: LoadEzyFreightTables-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

--select * from [cpsqlweb01].[Ezyfreight].[dbo].tblConsignment where consignmentcode='CPWPEC000020664'


--select * from [cpsqlweb01].[Ezyfreight].[dbo].tblConsignment where consignmentid=65138
--select * from tblconsignment where consignmentcode='CPWPEC000020664'

MERGE tblConsignment AS cd
USING (select [ConsignmentID]
      ,[ConsignmentCode]
      ,[UserID]
      ,[IsRegUserConsignment]
      ,[PickupID]
      ,[DestinationID]
      ,[ContactID]
      ,[TotalWeight]
      ,[TotalMeasureWeight]
      ,[TotalVolume]
      ,[TotalMeasureVolume]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[CustomerRefNo]
      ,[ConsignmentPreferPickupDate]
      ,[ConsignmentPreferPickupTime]
      ,[ClosingTime]
      ,[DangerousGoods]
      ,[Terms]
      ,[RateCardID]
      ,[LastActivity]
      ,[LastActiivityDateTime]
      ,[ConsignmentStatus]
     -- ,[EDIDataProcessed]
      ,[ProntoDataExtracted]
      ,[IsBilling]
      ,[IsManifested]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[IsInternational]
      ,[IsDocument]
      ,[IsSignatureReq]
      ,[IfUndelivered]
      ,[ReasonForExport]
      ,[TypeOfExport]
      ,[Currency]
      ,[IsInsurance]
      ,[IsIdentity]
      ,[IdentityType]
      ,[IdentityNo]
      ,[Country-ServiceArea-FacilityCode]
      ,[InternalServiceCode]
      ,[NetSubTotal]
      ,[IsATl]
      ,[IsReturnToSender]
      ,[HasReadInsuranceTc]
      ,[NatureOfGoods]
      ,[OriginServiceAreaCode]
      ,[ProductShortName]
      ,[SortCode]
      ,[ETA]
      ,[CTIManifestExtracted]
      ,[isprocessed]
      ,[IsAccountCustomer]
      ,[InsuranceAmount]
      ,[CourierPickupDate]
      ,[CalculatedTotal]
      ,[CalculatedGST]
      ,[ClientCode]
	  ,PromotionCode
	  ,0 as IsHubApiProcessed from [cpsqlweb01].[Ezyfreight].[dbo].[tblConsignment] where isnull(UpdatedDateTTime,CreatedDateTime)>=dateadd(hour,-5,getdate())) AS cs ON cs.consignmentcode=cd.consignmentcode  collate SQL_Latin1_General_CP1_CI_AS
when matched then update set 
       [ConsignmentID]=cs.ConsignmentID
      ,[ConsignmentCode]=cs.ConsignmentCode
      ,[UserID]=cs.UserID
      ,[IsRegUserConsignment]=cs.IsRegUserConsignment
      ,[PickupID]=cs.PickupID
      ,[DestinationID]=cs.DestinationID
      ,[ContactID]=cs.ContactID
      ,[TotalWeight]=cs.TotalWeight
      ,[TotalMeasureWeight]=cs.TotalMeasureWeight
      ,[TotalVolume]=cs.TotalVolume
      ,[TotalMeasureVolume]=cs.TotalMeasureVolume
      ,[NoOfItems]=cs.NoOfItems
      ,[SpecialInstruction]=cs.SpecialInstruction
      ,[CustomerRefNo]=cs.CustomerRefNo
      ,[ConsignmentPreferPickupDate]=cs.ConsignmentPreferPickupDate
      ,[ConsignmentPreferPickupTime]=cs.ConsignmentPreferPickupTime
      ,[ClosingTime]=cs.ClosingTime
      ,[DangerousGoods]=cs.DangerousGoods
      ,[Terms]=cs.Terms
      ,[RateCardID]=cs.RateCardID
      ,[LastActivity]=cs.LastActivity
      ,[LastActiivityDateTime]=cs.LastActiivityDateTime
      ,[ConsignmentStatus]=cs.ConsignmentStatus
     -- ,[EDIDataProcessed]=cs.EDIDataProcessed
      ,[ProntoDataExtracted]=cs.ProntoDataExtracted
      ,[IsBilling]=cs.IsBilling
      ,[IsManifested]=cs.IsManifested
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTTime]=cs.UpdatedDateTTime
      ,[UpdatedBy]=cs.UpdatedBy
      ,[IsInternational]=cs.IsInternational
      ,[IsDocument]=cs.IsDocument
      ,[IsSignatureReq]=cs.IsSignatureReq
      ,[IfUndelivered]=cs.IfUndelivered
      ,[ReasonForExport]=cs.ReasonForExport
      ,[TypeOfExport]=cs.TypeOfExport
      ,[Currency]=cs.Currency
      ,[IsInsurance]=cs.IsInsurance
      ,[IsIdentity]=cs.IsIdentity
      ,[IdentityType]=cs.IdentityType
      ,[IdentityNo]=cs.IdentityNo
      ,[Country-ServiceArea-FacilityCode]=cs.[Country-ServiceArea-FacilityCode]
      ,[InternalServiceCode]=cs.InternalServiceCode
      ,[NetSubTotal]=cs.NetSubTotal
      ,[IsATl]=cs.IsATl
      ,[IsReturnToSender]=cs.IsReturnToSender
      ,[HasReadInsuranceTc]=cs.HasReadInsuranceTc
      ,[NatureOfGoods]=cs.NatureOfGoods
      ,[OriginServiceAreaCode]=cs.OriginServiceAreaCode
      ,[ProductShortName]=cs.ProductShortName
      ,[SortCode]=cs.SortCode
      ,[ETA]=cs.ETA
	  ,[CTIManifestExtracted]=cs.CTIManifestExtracted
	  ,isprocessed=cs.isprocessed
      ,[IsAccountCustomer]=cs.IsAccountCustomer
      ,[InsuranceAmount]=cs.InsuranceAmount
      ,[CourierPickupDate]=cs.CourierPickupDate
      ,[CalculatedTotal]=cs.CalculatedTotal
      ,[CalculatedGST]=cs.CalculatedGST
	  ,clientcode=cs.clientcode
      ,PromotionCode=cs.PromotionCode
	  ,IsHubApiProcessed=cs.IsHubApiProcessed
														
when not matched then INSERT values(cs.ConsignmentID
      ,cs.ConsignmentCode
      ,cs.UserID
      ,cs.IsRegUserConsignment
      ,cs.PickupID
      ,cs.DestinationID
      ,cs.ContactID
      ,cs.TotalWeight
      ,cs.TotalMeasureWeight
      ,cs.TotalVolume
      ,cs.TotalMeasureVolume
      ,cs.NoOfItems
      ,cs.SpecialInstruction
      ,cs.CustomerRefNo
      ,cs.ConsignmentPreferPickupDate
      ,cs.ConsignmentPreferPickupTime
      ,cs.ClosingTime
      ,cs.DangerousGoods
      ,cs.Terms
      ,cs.RateCardID
      ,cs.LastActivity
      ,cs.LastActiivityDateTime
      ,cs.ConsignmentStatus
      ,0
      ,cs.ProntoDataExtracted
      ,cs.IsBilling
      ,cs.IsManifested
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
      ,cs.IsInternational
      ,cs.IsDocument
      ,cs.IsSignatureReq
      ,cs.IfUndelivered
      ,cs.ReasonForExport
      ,cs.TypeOfExport
      ,cs.Currency
      ,cs.IsInsurance
      ,cs.IsIdentity
      ,cs.IdentityType
      ,cs.IdentityNo
      ,cs.[Country-ServiceArea-FacilityCode]
      ,cs.InternalServiceCode
      ,cs.NetSubTotal
      ,cs.IsATl
      ,cs.IsReturnToSender
      ,cs.HasReadInsuranceTc
      ,cs.NatureOfGoods
      ,cs.OriginServiceAreaCode
      ,cs.ProductShortName
      ,cs.SortCode
      ,cs.ETA
	  ,cs.CTIManifestExtracted
	  ,cs.isprocessed
      ,cs.IsAccountCustomer
      ,cs.InsuranceAmount
      ,cs.CourierPickupDate
      ,cs.CalculatedTotal
      ,cs.CalculatedGST
	  ,cs.clientcode
	  ,0
	  ,0
	  ,cs.PromotionCode
	  ,cs.IsHubApiProcessed);


	  
	
MERGE tblConsignmentstaging AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].[tblConsignmentstaging] where isnull(UpdatedDateTTime,CreatedDateTime)>=dateadd(hour,-5,getdate())) AS cs ON cs.ConsignmentStagingID=cd.ConsignmentStagingID
when matched then update set 
       [ConsignmentStagingID]=cs.ConsignmentStagingID
      ,[UserID]=cs.UserID
      ,[IsRegUserConsignment]=cs.IsRegUserConsignment
      ,[PickupID]=cs.PickupID
      ,[DestinationID]=cs.DestinationID
      ,[ContactID]=cs.ContactID
      ,[TotalWeight]=cs.TotalWeight
      ,[TotalVolume]=cs.TotalVolume
      ,[NoOfItems]=cs.NoOfItems
      ,[SpecialInstruction]=cs.SpecialInstruction
      ,[CustomerRefNo]=cs.CustomerRefNo
      ,[PickupDate]=cs.PickupDate
      ,[PreferPickupTime]=cs.PreferPickupTime
      ,[ClosingTime]=cs.ClosingTime
      ,[DangerousGoods]=cs.DangerousGoods
      ,[Terms]=cs.Terms
      ,[ServiceID]=cs.ServiceID
      ,[RateCardID]=cs.RateCardID
      ,[IsProcessed]=cs.IsProcessed
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTTime]=cs.UpdatedDateTTime
      ,[UpdatedBy]=cs.UpdatedBy
      ,[ConsignmentId]=cs.ConsignmentId
      ,[IsDocument]=cs.IsDocument
      ,[IsSignatureReq]=cs.IsSignatureReq
      ,[IfUndelivered]=cs.IfUndelivered
      ,[ReasonForExport]=cs.ReasonForExport
      ,[TypeOfExport]=cs.TypeOfExport
      ,[Currency]=cs.Currency
      ,[IsInsurance]=cs.IsInsurance
      ,[IsIdentity]=cs.IsIdentity
      ,[IdentityType]=cs.IdentityType
      ,[IdentityNo]=cs.IdentityNo
      ,[XMLRequest]=cs.XMLRequest
      ,[XMLResponce]=cs.XMLResponce
      ,[IsATl]=cs.IsATl
      ,[IsReturnToSender]=cs.IsReturnToSender
      ,[HasReadInsuranceTc]=cs.HasReadInsuranceTc
      ,[NatureOfGoods]=cs.NatureOfGoods
      ,[SortCode]=cs.SortCode
      ,[XMLRequestPU]=cs.XMLRequestPU
      ,[XMLResponcePU]=cs.XMLResponcePU
      ,[ETA]=cs.ETA
      ,[PaymentRefNo]=cs.PaymentRefNo
      ,[XMLRequestPU_DHL]=cs.XMLRequestPU_DHL
      ,[XMLResponcePU_DHL]=cs.XMLResponcePU_DHL
      ,[XMLRequestPU_NZPost]=cs.XMLRequestPU_NZPost
      ,[XMLResponcePU_NZPost]=cs.XMLResponcePU_NZPost
	  ,[MerchantReferenceCode] =cs.MerchantReferenceCode
	,[SubscriptionID]=cs.SubscriptionID
	,[AuthorizationCode] =cs.AuthorizationCode
	,[IsAccountCustomer] =cs.IsAccountCustomer
	  ,[InsuranceAmount] =cs.InsuranceAmount
	  ,[CourierPickupDate] =cs.CourierPickupDate
	  ,[CalculatedTotal]=cs.CalculatedTotal
	  ,[CalculatedGST] =cs.CalculatedGST
	  ,clientcode=cs.clientcode
	  ,[USPSRefNo]=cs.[USPSRefNo]
	  ,[PromotionCode]=cs.[PromotionCode]														
when not matched then INSERT values( cs.ConsignmentStagingID
      ,cs.UserID
      ,cs.IsRegUserConsignment
      ,cs.PickupID
      ,cs.DestinationID
      ,cs.ContactID
      ,cs.TotalWeight
      ,cs.TotalVolume
      ,cs.NoOfItems
      ,cs.SpecialInstruction
      ,cs.CustomerRefNo
      ,cs.PickupDate
      ,cs.PreferPickupTime
      ,cs.ClosingTime
      ,cs.DangerousGoods
      ,cs.Terms
      ,cs.ServiceID
      ,cs.RateCardID
      ,cs.IsProcessed
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
      ,cs.ConsignmentId
      ,cs.IsDocument
      ,cs.IsSignatureReq
      ,cs.IfUndelivered
      ,cs.ReasonForExport
      ,cs.TypeOfExport
      ,cs.Currency
      ,cs.IsInsurance
      ,cs.IsIdentity
      ,cs.IdentityType
      ,cs.IdentityNo
      ,cs.XMLRequest
      ,cs.XMLResponce
      ,cs.IsATl
      ,cs.IsReturnToSender
      ,cs.HasReadInsuranceTc
      ,cs.NatureOfGoods
      ,cs.SortCode
      ,cs.XMLRequestPU
      ,cs.XMLResponcePU
      ,cs.ETA
      ,cs.PaymentRefNo
      ,cs.XMLRequestPU_DHL
      ,cs.XMLResponcePU_DHL
      ,cs.XMLRequestPU_NZPost
      ,cs.XMLResponcePU_NZPost
	  ,cs.MerchantReferenceCode
	  ,cs.SubscriptionID
	  ,cs.AuthorizationCode
	  ,cs.IsAccountCustomer
	  ,cs.InsuranceAmount
	  ,cs.CourierPickupDate 
	  ,cs.CalculatedTotal
	  ,cs.CalculatedGST
	  ,cs.clientcode
	  ,cs.USPSRefNo
	  ,cs.PromotionCode) ;




	  	
MERGE tblBooking AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].[tblBooking] where isnull(UpdatedDateTime,CreatedDateTime)>=dateadd(hour,-5,getdate())) AS cs ON cs.BookingID=cd.BookingID
when matched then update set 
       [BookingID]=cs.BookingID
      ,[PhoneNumber]=cs.PhoneNumber
      ,[ContactName]=cs.ContactName
      ,[ContactEmail]=cs.ContactEmail
      ,[PickupDate]=cs.PickupDate
      ,[PickupTime]=cs.PickupTime
      ,[OrderCoupons]=cs.OrderCoupons
      ,[PickupFromCustomer]=cs.PickupFromCustomer
      ,[PickupName]=cs.PickupName
      ,[PickupAddress1]=cs.PickupAddress1
      ,[PickupAddress2]=cs.PickupAddress2
      ,[PickupSuburb]=cs.PickupSuburb
      ,[PickupPhoneNo]=cs.PickupPhoneNo
      ,[ContactDetails]=cs.ContactDetails
      ,[PickupFrom]=cs.PickupFrom
      ,[IsProcessed]=cs.IsProcessed
      ,[BookingRefNo]=cs.BookingRefNo
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
      ,[Branch]=cs.Branch
      ,[PickupEmail]=cs.PickupEmail
      ,[PickupPostCode]=cs.PickupPostCode
      ,[Costom]=cs.Costom
      ,[CostomId]=cs.CostomId
	  ,[Consignmentid]=cs.consignmentid
														
when not matched then INSERT values(cs.BookingID
      ,cs.PhoneNumber
      ,cs.ContactName
      ,cs.ContactEmail
      ,cs.PickupDate
      ,cs.PickupTime
      ,cs.OrderCoupons
      ,cs.PickupFromCustomer
      ,cs.PickupName
      ,cs.PickupAddress1
      ,cs.PickupAddress2
      ,cs.PickupSuburb
      ,cs.PickupPhoneNo
      ,cs.ContactDetails
      ,cs.PickupFrom
      ,cs.IsProcessed
      ,cs.BookingRefNo
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
      ,cs.Branch
      ,cs.PickupEmail
      ,cs.PickupPostCode
      ,cs.Costom
      ,cs.CostomId
	  ,cs.consignmentid);


	  
MERGE tblContactUs AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].[tblContactUs] where convert(date,[CreatedDate])>=convert(date,dateadd("dd",-2,getdate()))) AS cs ON cs.ID=cd.ID
when matched then update set 
       [Id]=cs.Id
      ,[FirstName]=cs.FirstName
      ,[LastName]=cs.LastName
      ,[Email]=cs.Email
      ,[Phone]=cs.Phone
      ,[Suburb]=cs.Suburb
      ,[PostCode]=cs.PostCode
      ,[StateID]=cs.StateID
      ,[Subject]=cs.Subject
      ,[HowCanIHelpYou]=cs.HowCanIHelpYou
      ,[IsSubscribe]=cs.IsSubscribe
      ,[CreatedDate]=cs.CreatedDate
	  ,[TrackingNo]=cs.TrackingNo
	  ,[AddressLine1]=cs.AddressLine1
	  ,[AddressLine2]=cs.AddressLine2
														
when not matched then INSERT values( cs.Id
      ,cs.FirstName
      ,cs.LastName
      ,cs.Email
      ,cs.Phone
      ,cs.Suburb
      ,cs.PostCode
      ,cs.StateID
      ,cs.Subject
      ,cs.HowCanIHelpYou
      ,cs.IsSubscribe
      ,cs.CreatedDate
	  ,cs.TrackingNo
	  ,cs.AddressLine1
	  ,cs.AddressLine2);


	  MERGE tblSalesOrder AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].[tblSalesOrder] where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))) AS cs ON cs.salesOrderID=cd.SalesOrderID
when matched then update set 
       [SalesOrderID]=cs.SalesOrderID
      ,[ReferenceNo]=cs.ReferenceNo
      ,[UserID]=cs.UserID
      ,[NoofItems]=cs.NoofItems
      ,[TotalWeight]=cs.TotalWeight
      ,[TotalVolume]=cs.TotalVolume
      ,[RateCardID]=cs.RateCardID
      ,[GrossTotal]=cs.GrossTotal
      ,[GST]=cs.GST
      ,[NetTotal]=cs.NetTotal
      ,[SalesOrderStatus]=cs.SalesOrderStatus
      ,[InvoiceNo]=cs.InvoiceNo
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
	  ,consignmentcode=cs.consignmentcode
	  ,[PromotionDiscount]=cs.[PromotionDiscount]	
	  ,[DiscountedTotal]=cs.[DiscountedTotal]
	  ,[SalesOrderType]=cs.[SalesOrderType]
														
when not matched then INSERT values( cs.SalesOrderID
      ,cs.ReferenceNo
      ,cs.UserID
      ,cs.NoofItems
      ,cs.TotalWeight
      ,cs.TotalVolume
      ,cs.RateCardID
      ,cs.GrossTotal
      ,cs.GST
      ,cs.NetTotal
      ,cs.SalesOrderStatus
      ,cs.InvoiceNo
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.consignmentcode
	  ,cs.[PromotionDiscount]	
	  ,cs.[DiscountedTotal]
	  ,cs.[SalesOrderType]);


	  MERGE tblSalesOrderDetail AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].[tblSalesOrderDetail] where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))) AS cs ON cs.salesOrderdetailID=cd.SalesOrderdetailID
when matched then update set 
      [SalesOrderDetailID]=cs.SalesOrderDetailID
      ,[SalesOrderID]=cs.SalesOrderID
      ,[Description]=cs.Description
      ,[LineNo]=cs.[LineNo]
      ,[Weight]=cs.[Weight]
      ,[Volume]=cs.Volume
      ,[FreightCharge]=cs.FreightCharge
      ,[FuelCharge]=cs.FuelCharge
      ,[InsuranceCharge]=cs.InsuranceCharge
      ,[ServiceCharge]=cs.ServiceCharge
      ,[Total]=cs.Total
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
														
when not matched then INSERT values(cs.SalesOrderDetailID
      ,cs.SalesOrderID
      ,cs.Description
      ,cs.[LineNo]
      ,cs.[Weight]
      ,cs.Volume
      ,cs.FreightCharge
      ,cs.FuelCharge
      ,cs.InsuranceCharge
      ,cs.ServiceCharge
      ,cs.Total
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy);
	 
	  MERGE tblRecharge AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].[tblRecharge] where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))) AS cs ON cs.ID=cd.ID
when matched then update set 
       [Id]=cs.Id
      ,[ConsignmentID]=cs.ConsignmentID
      ,[AdminFee]=cs.AdminFee
      ,[Difference]=cs.Difference
      ,[ETA]=cs.ETA
      ,[ExtensionData]=cs.ExtensionData
      ,[GrossInvoiceAmount]=cs.GrossInvoiceAmount
      ,[GSTonGrossInvoiceAmount]=cs.GSTonGrossInvoiceAmount
      ,[NetTotal]=cs.NetTotal
      ,[NewCalculatedTotal]=cs.NewCalculatedTotal
      ,[NewWeightforCalculation]=cs.NewWeightforCalculation
      ,[ReweighStatus]=cs.ReweighStatus
      ,[ReweighStatusDescription]=cs.ReweighStatusDescription
      ,[ServiceCode]=cs.ServiceCode
      ,[ServiceDescription]=cs.ServiceDescription
      ,[SignatureOption]=cs.SignatureOption
      ,[IsSignatureReq]=cs.IsSignatureReq
      ,[OrijnalCalculatedTotal]=cs.OrijnalCalculatedTotal
      ,[PaymentRefNo]=cs.PaymentRefNo
      ,[MerchantReferenceCode]=cs.MerchantReferenceCode
      ,[IsProcessed]=cs.IsProcessed
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
														
when not matched then INSERT values( cs.Id
      ,cs.ConsignmentID
      ,cs.AdminFee
      ,cs.Difference
      ,cs.ETA
      ,cs.ExtensionData
      ,cs.GrossInvoiceAmount
      ,cs.GSTonGrossInvoiceAmount
      ,cs.NetTotal
      ,cs.NewCalculatedTotal
      ,cs.NewWeightforCalculation
      ,cs.ReweighStatus
      ,cs.ReweighStatusDescription
      ,cs.ServiceCode
      ,cs.ServiceDescription
      ,cs.SignatureOption
      ,cs.IsSignatureReq
      ,cs.OrijnalCalculatedTotal
      ,cs.PaymentRefNo
      ,cs.MerchantReferenceCode
      ,cs.IsProcessed
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy);


	   MERGE tblAddress AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].[tblAddress] where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))) AS cs ON cs.AddressID=cd.AddressID
when matched then update set 
       [AddressID]=cs.AddressID
      ,[UserID]=cs.UserID
      ,[FirstName]=cs.FirstName
      ,[LastName]=cs.LastName
      ,[CompanyName]=cs.CompanyName
      ,[Email]=cs.Email
      ,[Address1]=cs.Address1
      ,[Address2]=cs.Address2
      ,[Suburb]=cs.Suburb
      ,[StateName]=cs.StateName
      ,[StateID]=cs.StateID
      ,[PostCode]=cs.PostCode
      ,[Phone]=cs.Phone
      ,[Mobile]=cs.Mobile
      ,[Country]=cs.Country
      ,[CountryCode]=cs.CountryCode
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
      ,[IsRegisterAddress]=cs.IsRegisterAddress
      ,[IsDeleted]=cs.IsDeleted
      ,[IsBusiness]=cs.IsBusiness
      ,[IsSubscribe]=cs.IsSubscribe
	  ,[IsEDIUser]=cs.IsEDIUser
														
when not matched then INSERT values(   cs.AddressID
      ,cs.UserID
      ,cs.FirstName
      ,cs.LastName
      ,cs.CompanyName
      ,cs.Email
      ,cs.Address1
      ,cs.Address2
      ,cs.Suburb
      ,cs.StateName
      ,cs.StateID
      ,cs.PostCode
      ,cs.Phone
      ,cs.Mobile
      ,cs.Country
      ,cs.CountryCode
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
      ,cs.IsRegisterAddress
      ,cs.IsDeleted
      ,cs.IsBusiness
      ,cs.IsSubscribe
	  ,cs.IsEDIUser);

	  MERGE tblCustomDeclaration AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].tblCustomDeclaration where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))) AS cs ON cs.[CustomDeclarationId]=cd.[CustomDeclarationId]
when matched then update set 
      [CustomDeclarationId]=cs.[CustomDeclarationId]
      ,[ConsignmentID]=cs.[ConsignmentID]
      ,[ItemDescription]=cs.[ItemDescription]
      ,[ItemInBox]=cs.[ItemInBox]
      ,[UnitPrice]=cs.[UnitPrice]
      ,[SubTotal]=cs.[SubTotal]
      ,[HSCode]=cs.[HSCode]
      ,[CountryofOrigin]=cs.[CountryofOrigin]
      ,[Currency]=cs.[Currency]
      ,[CreatedDateTime]=cs.[CreatedDateTime]
      ,[CreatedBy]=cs.[CreatedBy]
      ,[UpdatedDateTTime]=cs.[UpdatedDateTTime]
      ,[UpdatedBy]=cs.[UpdatedBy]
														
when not matched then INSERT values(cs.CustomDeclarationId
      ,cs.ConsignmentID
      ,cs.ItemDescription
      ,cs.ItemInBox
      ,cs.UnitPrice
      ,cs.SubTotal
      ,cs.HSCode
      ,cs.CountryofOrigin
      ,cs.Currency
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy);
	  	

		  MERGE tblRefund AS cd
USING (select * from [cpsqlweb01].[Ezyfreight].[dbo].tblRefund where [CreatedDateTime]>=convert(date,dateadd(HOUR,-5,getdate()))) AS cs ON cs.[Id]=cd.[Id]
when matched then update set 
     [Id]=cs.[Id]
      ,[ConsignmentID]=cs.[ConsignmentID]
      ,[Reason]=cs.[Reason]
      ,[Amount]=cs.[Amount]
      ,[PaymentRefNo]=cs.[PaymentRefNo]
      ,[AuthorizationCode]=cs.[AuthorizationCode]
      ,[IsProcess]=cs.[IsProcess]
      ,[CreatedDateTime]=cs.[CreatedDateTime]
      ,[CreatedBy]=cs.[CreatedBy]
      ,[UpdatedDateTTime]=cs.[UpdatedDateTTime]
      ,[UpdatedBy]=cs.[UpdatedBy]
      ,[MerchantReferenceCode]=cs.[MerchantReferenceCode]
	  ,RefundCategory =cs.RefundCategory 
														
when not matched then INSERT values(cs.Id
      ,cs.ConsignmentID
      ,cs.Reason
      ,cs.Amount
      ,cs.PaymentRefNo
      ,cs.AuthorizationCode
      ,cs.IsProcess
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
      ,cs.MerchantReferenceCode
	  ,cs.RefundCategory );	
  

  	  	

MERGE Users AS cd
USING (select * from [cpsqlweb01].[CPPLWeb_8_3].[dbo].[Users] where CreatedOnDate>=convert(date,dateadd(HOUR,-5,getdate()))) AS cs ON cs.[UserID]=cd.[UserID]
when matched then update set 
       [UserID]=cs.[UserID]
      ,[Username]=cs.[Username]
      ,[FirstName]=cs.[FirstName]
      ,[LastName]=cs.[LastName]
      ,[IsSuperUser]=cs.[IsSuperUser]
      ,[AffiliateId]=cs.[AffiliateId]
      ,[Email]=cs.[Email]
      ,[DisplayName]=cs.[DisplayName]
      ,[UpdatePassword]=cs.[UpdatePassword]
      ,[LastIPAddress]=cs.[LastIPAddress]
      ,[IsDeleted]=cs.[IsDeleted]
      ,[CreatedByUserID]=cs.[CreatedByUserID]
      ,[CreatedOnDate]=cs.[CreatedOnDate]
      ,[LastModifiedByUserID]=cs.[LastModifiedByUserID]
      ,[LastModifiedOnDate]=cs.[LastModifiedOnDate]
      ,[PasswordResetToken]=cs.[PasswordResetToken]
      ,[PasswordResetExpiration]=cs.[PasswordResetExpiration]
      ,[LowerEmail]=cs.[LowerEmail]
														
when not matched then INSERT values(cs.UserID
      ,cs.Username
      ,cs.FirstName
      ,cs.LastName
      ,cs.IsSuperUser
      ,cs.AffiliateId
      ,cs.Email
      ,cs.DisplayName
      ,cs.UpdatePassword
      ,cs.LastIPAddress
      ,cs.IsDeleted
      ,cs.CreatedByUserID
      ,cs.CreatedOnDate
      ,cs.LastModifiedByUserID
      ,cs.LastModifiedOnDate
      ,cs.PasswordResetToken
      ,cs.PasswordResetExpiration
      ,cs.LowerEmail);	




MERGE tblcompany AS cd
USING (select * from [cpsqlweb01].[ezyfreight].[dbo].[tblcompany]  where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate())) or isnull(updateddatetime,'')>=convert(date,dateadd(HOUR,-5,getdate())))
AS cs ON cs.[CompanyID]=cd.[CompanyID]
when matched then update set 

 CompanyID=cs.CompanyID,
  AccountNumber=cs.AccountNumber,
  CompanyName=cs.CompanyName,
  ParentAccountNo=cs.ParentAccountNo,
   ABN=cs.ABN,
   AddressID1=cs.AddressID1,
   PostalAddressID=cs.PostalAddressID,
     ParentCompanyName=cs.ParentCompanyName,
     BilingAccountNo=cs.BilingAccountNo,
     IsProntoExtracted=cs.IsProntoExtracted,
     RateCardCategory=cs.RateCardCategory,
     [IntRateCardCategory]=cs.IntRateCardCategory
      ,[SameBillingAddress]=cs.SameBillingAddress
      ,[BillingFname]=cs.BillingFname
      ,[BillingLname]=cs.BillingLname
      ,[BllingEmail]=cs.BllingEmail
      ,[BillingPhone]=cs.BillingPhone
      ,[BillingAddress]=cs.BillingAddress
      ,[CreditLimit]=cs.CreditLimit
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
	  ,[IsExistingCustomer]=cs.IsExistingCustomer
      ,[Branch]=cs.Branch
      ,[ExistingAccountNumber]=cs.ExistingAccountNumber
      ,[CouponNo]=cs.CouponNo
	  ,[IsRegularShipper]=cs.IsRegularShipper
	  ,[ShipperCreatedBy]=cs.ShipperCreatedBy
	  ,[ShipperCreatedDateTime]=cs.ShipperCreatedDateTime
	  ,[ShipperUpdatedBy]=cs.ShipperUpdatedBy
	  ,[ShipperUpdatedDateTime]=cs.ShipperUpdatedDateTime
      , IsDirectDebitDone=cs.IsDirectDebitDone
	  ,IsAggregatorPricing= cs.IsAggregatorPricing



when not matched then INSERT values(
	  cs.CompanyID
      ,cs.AccountNumber
      ,cs.CompanyName
      ,cs.ParentAccountNo
      ,cs.ABN
      ,cs.AddressID1
      ,cs.PostalAddressID
      ,cs.ParentCompanyName
      ,cs.BilingAccountNo
      ,cs.IsProntoExtracted
      ,cs.RateCardCategory
      ,cs.IntRateCardCategory
      ,cs.SameBillingAddress
      ,cs.BillingFname
      ,cs.BillingLname
      ,cs.BllingEmail
      ,cs.BillingPhone
      ,cs.BillingAddress
      ,cs.CreditLimit
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.IsExistingCustomer
      ,cs.Branch
      ,cs.ExistingAccountNumber
      ,cs.CouponNo
	  ,cs.IsRegularShipper
	  ,cs.ShipperCreatedBy
	  ,cs.ShipperCreatedDateTime
	  ,cs.ShipperUpdatedBy
	  ,cs.ShipperUpdatedDateTime
	  ,cs.IsDirectDebitDone
	  ,cs.IsAggregatorPricing
	  );



MERGE tblcompanyUsers AS cd
USING (select * from [cpsqlweb01].[ezyfreight].[dbo].[tblcompanyUsers]  where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate())) or isnull(updateddatetime,'')>=convert(date,dateadd(HOUR,-5,getdate())))
AS cs ON cs.[CompanyUsersID]=cd.[CompanyUsersID]
when matched then update set 

 CompanyUsersID=cs.CompanyUsersID,
    UserID=  cs.UserID,
  CompanyID=cs.CompanyID,
[IsUserDisabled]=cs.IsUserDisabled
      ,[ReasonSubject]=cs.ReasonSubject
      ,[ReasonDesciption]=cs.ReasonDesciption
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
	  ,[PromotionalCode]=cs.PromotionalCode
 
when not matched then INSERT values(
	   cs.CompanyUsersID,
      cs.UserID,
      cs.CompanyID,
cs.IsUserDisabled
      ,cs.ReasonSubject
      ,cs.ReasonDesciption
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.PromotionalCode);



MERGE tblRedelivery AS cd
USING (select * from [cpsqlweb01].[ezyfreight].[dbo].tblRedelivery  where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))) 
AS cs ON cs.[RedeliveryID]=cd.[RedeliveryID]
when matched then update set 

       [RedeliveryID]=cs.RedeliveryID
	   ,[RedeliveryType]=cs.[RedeliveryType]
      ,[CardReferenceNumber]=cs.CardReferenceNumber
      ,[LableNumber]=cs.LableNumber
      ,[ConsignmentCode]=cs.ConsignmentCode
      ,[Branch]=cs.Branch
      ,[State]=cs.State
      ,[AttemptedRedeliveryTime]=cs.AttemptedRedeliveryTime
      ,[SenderName]=cs.SenderName
      ,[NumberOfItem]=cs.NumberOfItem
      ,[DestinationName]=cs.DestinationName
      ,[DestinationAddress]=cs.DestinationAddress
      ,[DestinationSuburb]=cs.DestinationSuburb
      ,[DestinationPostCode]=cs.DestinationPostCode
      ,[SelectedETADate]=cs.SelectedETADate
      ,[PreferDeliverTimeSlot]=cs.PreferDeliverTimeSlot
      ,[PreferDeliverTime]=cs.PreferDeliverTime
      ,[CreatedDateTime]=cs.CreatedDateTime
      ,[CreatedBy]=cs.CreatedBy
      ,[UpdatedDateTime]=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.UpdatedBy
      ,[IsProcessed]=cs.IsProcessed
      ,[Firstname]=cs.Firstname
      ,[Lastname]=cs.Lastname
      ,[Email]=cs.Email
      ,[PhoneNumber]=cs.PhoneNumber
      ,[SPInstruction]=cs.SPInstruction
 
when not matched then INSERT values(
	   cs.RedeliveryID
	   ,cs.[RedeliveryType]
      ,cs.CardReferenceNumber
      ,cs.LableNumber
      ,cs.ConsignmentCode
      ,cs.Branch
      ,cs.State
      ,cs.AttemptedRedeliveryTime
      ,cs.SenderName
      ,cs.NumberOfItem
      ,cs.DestinationName
      ,cs.DestinationAddress
      ,cs.DestinationSuburb
      ,cs.DestinationPostCode
      ,cs.SelectedETADate
      ,cs.PreferDeliverTimeSlot
      ,cs.PreferDeliverTime
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
      ,cs.IsProcessed
      ,cs.Firstname
      ,cs.Lastname
      ,cs.Email
      ,cs.PhoneNumber
      ,cs.SPInstruction);
	 

	 MERGE tblItemLabel AS cd
USING (select * from [cpsqlweb01].[ezyfreight].[dbo].tblItemLabel  where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))) 
AS cs ON cs.[ItemLabelID]=cd.[ItemLabelID]
when matched then update set 

       [ItemLabelID]=cs.[ItemLabelID]
      ,[ConsignmentID]=cs.ConsignmentID
      ,[LabelNumber]=cs.LabelNumber
      ,[Length]=cs.Length
      ,[Width]=cs.Width
      ,[Height]=cs.Height
      ,[CubicWeight]=cs.CubicWeight
      ,[MeasureLength]=cs.MeasureLength
      ,[MeasureWidth]=cs.MeasureWidth
      ,[MeasureHeight]=cs.MeasureHeight
      ,[MeasureCubicWeight]=cs.MeasureCubicWeight
      ,[PhysicalWeight]=cs.PhysicalWeight
      ,[MeasureWeight]=cs.MeasureWeight
      ,[DeclareVolume]=cs.DeclareVolume
      ,[LastActivity]=cs.LastActivity
      ,[LastActivityDateTime]=cs.LastActivityDateTime
      ,[UnitValue]=cs.UnitValue
      ,[Quantity]=cs.Quantity
      ,[CountryOfOrigin]=cs.CountryOfOrigin
      ,[Description]=cs.Description
      ,[HSTariffNumber]=cs.HSTariffNumber
	  ,[DHLBarCode]=cs.DHLBarCode
 
when not matched then INSERT values(
	  [ItemLabelID]
      ,[ConsignmentID]
      ,[LabelNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[CubicWeight]
      ,[MeasureLength]
      ,[MeasureWidth]
      ,[MeasureHeight]
      ,[MeasureCubicWeight]
      ,[PhysicalWeight]
      ,[MeasureWeight]
      ,[DeclareVolume]
      ,[LastActivity]
      ,[LastActivityDateTime]
      ,[UnitValue]
      ,[Quantity]
      ,[CountryOfOrigin]
      ,[Description]
      ,[HSTariffNumber]
      ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy
	  ,cs.DHLBarCode);
	 

	 MERGE [tblDHLBarCodeImage] AS cd
USING (select * from [cpsqlweb01].[ezyfreight].[dbo].[tblDHLBarCodeImage]  where CreatedDateTime>=convert(date,dateadd(HOUR,-5,getdate()))) 
AS cs ON cs.[ID]=cd.[ID]
when matched then update set 

       [Id]=cs.[Id]
      ,[ConsignmentID]=cs.ConsignmentID
      ,[AWBCode]=cs.[AWBCode]
      ,[AWBBarCode]=cs.[AWBBarCode]
      ,[OriginDestncode]=cs.[OriginDestncode]
      ,[OriginDestnBarcode]=cs.[OriginDestnBarcode]
      ,[ClientIDCode]=cs.[ClientIDCode]
      ,[ClientIDBarCode]=cs.[ClientIDBarCode]
      ,[DHLRoutingCode]=cs.[DHLRoutingCode]
      ,[DHLRoutingBarCode]=cs.[DHLRoutingBarCode]
      ,[CreatedDateTime]=cs.[CreatedDateTime]
      ,[CreatedBy]=cs.[CreatedBy]
      
 
when not matched then INSERT values(
	   [Id]
      ,[ConsignmentID]
      ,[AWBCode]
      ,[AWBBarCode]
      ,[OriginDestncode]
      ,[OriginDestnBarcode]
      ,[ClientIDCode]
      ,[ClientIDBarCode]
      ,[DHLRoutingCode]
      ,[DHLRoutingBarCode]
      ,[CreatedDateTime]
      ,[CreatedBy]);


	   MERGE [tblRedirectedConsignment] AS cd
USING (select ReConsignmentID
      ,UniqueID
      ,ConsignmentCode
      ,SelectedDeliveryOption
      ,PickupAddressID
      ,CurrentDeliveryAddressID
      ,NewDeliveryAddressID
      ,TotalWeight
      ,TotalVolume
      ,ServiceType
      ,RateCardID
      ,CurrentETA
      ,NewETA
      ,NoOfItems
      ,ConsignmentStatus
      ,SpecialInstruction
      ,Terms
      ,ATL
      ,ConfirmATLInsuranceVoid
      ,ConfirmDeliveryAddress
      ,IsProcessed
      ,SortCode
      ,PaymentRefNo
      ,MerchantReferenceCode
      ,SubscriptionID
      ,AuthorizationCode
      ,CalculatedTotal
      ,CreditCardSurcharge
      ,NetTotal
	  ,CreatedDateTime
      ,CreatedBy
      ,UpdatedDateTTime
      ,UpdatedBy from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedConsignment]  where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))) 
AS cs ON cs.[ReConsignmentID]=cd.[ReConsignmentID]
when matched then update set 

       [ReConsignmentID]=cs.[ReConsignmentID]
      ,[UniqueID]=cs.[UniqueID]
      ,[ConsignmentCode]=cs.[ConsignmentCode]
      ,[SelectedDeliveryOption]=cs.[SelectedDeliveryOption]
      ,[PickupAddressID]=cs.[PickupAddressID]
      ,[CurrentDeliveryAddressID]=cs.[CurrentDeliveryAddressID]
      ,[NewDeliveryAddressID]=cs.[NewDeliveryAddressID]
      ,[TotalWeight]=cs.[TotalWeight]
      ,[TotalVolume]=cs.[TotalVolume]
      ,[ServiceType]=cs.[ServiceType]
      ,[RateCardID]=cs.[RateCardID]
      ,[CurrentETA]=cs.[CurrentETA]
      ,[NewETA]=cs.[NewETA]
      ,[NoOfItems]=cs.[NoOfItems]
      ,[ConsignmentStatus]=cs.[ConsignmentStatus]
      ,[SpecialInstruction]=cs.[SpecialInstruction]
      ,[Terms]=cs.[Terms]
      ,[ATL]=cs.[ATL]
      ,[ConfirmATLInsuranceVoid]=cs.[ConfirmATLInsuranceVoid]
      ,[ConfirmDeliveryAddress]=cs.[ConfirmDeliveryAddress]
      ,[IsProcessed]=cs.[IsProcessed]
      ,[SortCode]=cs.[SortCode]
      ,[PaymentRefNo]=cs.[PaymentRefNo]
      ,[MerchantReferenceCode]=cs.[MerchantReferenceCode]
      ,[SubscriptionID]=cs.[SubscriptionID]
      ,[AuthorizationCode]=cs.[AuthorizationCode]
      ,[CalculatedTotal]=cs.[CalculatedTotal]
      ,[CreditCardSurcharge]=cs.[CreditCardSurcharge]
      ,[NetTotal]=cs.[NetTotal]
      ,[CreatedDateTime]=cs.[CreatedDateTime]
      ,[CreatedBy]=cs.[CreatedBy]
	  ,UpdatedDateTTime=cs.UpdatedDateTTime
      ,[UpdatedBy]=cs.[UpdatedBy]
      
 
when not matched then INSERT values(
	   cs.ReConsignmentID
      ,cs.UniqueID
      ,cs.ConsignmentCode
      ,cs.SelectedDeliveryOption
      ,cs.PickupAddressID
      ,cs.CurrentDeliveryAddressID
      ,cs.NewDeliveryAddressID
      ,cs.TotalWeight
      ,cs.TotalVolume
      ,cs.ServiceType
      ,cs.RateCardID
      ,cs.CurrentETA
      ,cs.NewETA
      ,cs.NoOfItems
      ,cs.ConsignmentStatus
      ,cs.SpecialInstruction
      ,cs.Terms
      ,cs.ATL
      ,cs.ConfirmATLInsuranceVoid
      ,cs.ConfirmDeliveryAddress
      ,cs.IsProcessed
      ,cs.SortCode
      ,cs.PaymentRefNo
      ,cs.MerchantReferenceCode
      ,cs.SubscriptionID
      ,cs.AuthorizationCode
      ,cs.CalculatedTotal
      ,cs.CreditCardSurcharge
      ,cs.NetTotal
	  ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTTime
      ,cs.UpdatedBy
	  ,0);


	  
	   MERGE [tblRedirectedItemLabel] AS cd
USING (select * from [cpsqlweb01].[ezyfreight].[dbo].[tblRedirectedItemLabel]  where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))) 
AS cs ON cs.[ReItemLabelID]=cd.[ReItemLabelID]
when matched then update set 

       [ReItemLabelID]=cs.[ReItemLabelID]
      ,[ReConsignmentID]=cs.[ReConsignmentID]
      ,[LabelNumber]=cs.[LabelNumber]
      ,[Length]=cs.[Length]
      ,[Width]=cs.[Width]
      ,[Height]=cs.[Height]
      ,[CubicWeight]=cs.[CubicWeight]
      ,[PhysicalWeight]=cs.[PhysicalWeight]
      ,[LastActivity]=cs.[LastActivity]
      ,[LastActivityDateTime]=cs.[LastActivityDateTime]
      ,[CreatedDateTime]=cs.[CreatedDateTime]
      ,[CreatedBy]=cs.[CreatedBy]
	  ,UpdatedDateTime=cs.UpdatedDateTime
      ,[UpdatedBy]=cs.[UpdatedBy]
      
 
when not matched then INSERT values(
	  cs.ReItemLabelID
      ,cs.ReConsignmentID
      ,cs.LabelNumber
      ,cs.Length
      ,cs.Width
      ,cs.Height
      ,cs.CubicWeight
      ,cs.PhysicalWeight
      ,cs.LastActivity
      ,cs.LastActivityDateTime
	  ,cs.CreatedDateTime
      ,cs.CreatedBy
      ,cs.UpdatedDateTime
      ,cs.UpdatedBy);


	   MERGE [tblAPIEnquiry] AS cd
USING (select * from [cpsqlweb01].[ezyfreight].[dbo].[tblAPIEnquiry]  where CreatedDateTime>=convert(date,dateadd(HOUR,-24,getdate()))) 
AS cs ON cs.[ID]=cd.[ID]
when matched then update set 

       [ID]=cs.[ID]
      ,[CompanyName]=cs.[CompanyName]
      ,[Address]=cs.[Address]
      ,[Suburb]=cs.[Suburb]
      ,[StateID]=cs.[StateID]
      ,[PostCode]=cs.[PostCode]
      ,[Country]=cs.[Country]
      ,[Website]=cs.[Website]
      ,[AccountNumber]=cs.[AccountNumber]
      ,[RequesterFname]=cs.[RequesterFname]
      ,[RequesterLname]=cs.[RequesterLname]
      ,[TechContactName]=cs.[TechContactName]
      ,[TechContactEmail]=cs.[TechContactEmail]
      ,[TechContactNumber]=cs.[TechContactNumber]
      ,[ShoppingCartUsed]=cs.[ShoppingCartUsed]
      ,[Comments]=cs.[Comments]
      ,[CreatedDateTime]=cs.[CreatedDateTime]
	  ,RequesterEmail=cs.RequesterEmail
	  ,ERPInUse=cs.ERPInUse

 
when not matched then INSERT values(
	 cs.ID
      ,cs.CompanyName
      ,cs.Address
      ,cs.Suburb
      ,cs.StateID
      ,cs.PostCode
      ,cs.Country
      ,cs.Website
      ,cs.AccountNumber
      ,cs.RequesterFname
      ,cs.RequesterLname
      ,cs.TechContactName
      ,cs.TechContactEmail
      ,cs.TechContactNumber
      ,cs.ShoppingCartUsed
      ,cs.Comments
	  ,cs.CreatedDateTime
	  ,cs.RequesterEmail
	  ,cs.ERPInUse);


	

--Insert into NumberofActiveConnections
--    SELECT 
--    'CPPLWeb', 
--    COUNT(*) as NumberOfConnections,
--    loginame as LoginName,
--	getdate() as Date
--   FROM
--  [cpsqlweb01].[master].[dbo]. [sysprocesses]
--  where loginame='CpplWebUser                                                                                                                     '
--GROUP BY 
--    loginame


end


















GO
