SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadMailchimpsubscription] as
begin


     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadMailchimpsubscription
    --' ---------------------------
    --' Purpose: LoadMailchimpsubscription-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 16 Oct 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 16/10/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Insert into MailchimpStaging([Addressid]
      ,[EmailAddress]
      ,[APIKey]
      ,[ListId]
      ,[IsRegisterAddress]
      ,[IsSubscribe]
	  ,IsProcessed)

Select Addressid,
       Email,
	   '0cd92b62725eaf2a97c9c31db32c602f-us10',
	   'a2dcfd39fa',
	    IsRegisterAddress,
	    Issubscribe,
	    1
	   from tbladdress where issubscribe=1

Update MailchimpStaging set issubscribe=0 where addressid in (select addressid from tbladdress where issubscribe=0)


declare @x xml
set @x=(select * from MailchimpStaging for XML PATH,root )

declare @x1 varchar(max)
 
set @x1=(select dbo.fn_XmlToJson_Get(@x) )

select @x1 as col1

end


GO
