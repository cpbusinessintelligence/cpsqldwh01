SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [dbo].[sp_ParentVsChildAccountCustomers]  @State varchar(20)

as
 begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_ParentVsChildAccountCustomers
    --' ---------------------------
    --' Purpose: sp_ParentVsChildAccountCustomers-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 18 July 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 18/07/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

	If (@State)='ALL'
	
 Select isnull(s.statecode,case  when a.postcode like '2%' then 'NSW' 
                                           when a.postcode like '3%' then 'VIC'
										   when a.postcode like '4%' then 'QLD'
										   when a.postcode like '5%' then 'SA'
										   when a.postcode like '6%' then 'WA'
										   when a.postcode like '7%' then 'TAS'
										   when a.postcode like '08%' then 'NT'
										   else '' end) as State,convert(varchar(10),case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end) as ChildAccount,
        Accountnumber,
		case when isnumeric(Accountnumber)=1 then 'SingleSignOn' when isnumeric(Accountnumber)<>1  then 'Website Account' else '' end as AccountType,
		convert(varchar(100), c.CompanyName) as CompanyName,
		convert(Date,c.createddatetime) as Date
		from tblcompany c  join tblAddress a on c.AddressID1=a.AddressID
		join tblstate s on a.stateid= s.stateId
		
		where  Accountnumber is not null
		order by convert(Date,c.createddatetime)  desc

	
else
 
 Select s.statecode,convert(varchar(10),case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end) as ChildAccount,
        Accountnumber,
		case when isnumeric(Accountnumber)=1 then 'SingleSignOn' when isnumeric(Accountnumber)<>1  then 'Website Account' else '' end as AccountType,
		convert(varchar(100), c.CompanyName) as CompanyName,
		convert(Date,c.createddatetime) as Date
		from tblcompany c  join tblAddress a on c.AddressID1=a.AddressID
		join tblstate s on a.stateid= s.stateId
		
		where  s.statecode = (@State) and Accountnumber is not null
		order by convert(Date,c.createddatetime)  desc

	



		end



		
GO
GRANT EXECUTE
	ON [dbo].[sp_ParentVsChildAccountCustomers]
	TO [ReportUser]
GO
