SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptAccountCustomerConsignmentDetails](@StartDate date,@EndDate date,@ChildAccountcode varchar(100),@ParentAccountcode varchar(100),@Accountname varchar(100)) as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptAccountCustomerConsignmentDetails
    --' ---------------------------
    --' Purpose: sp_RptAccountCustomerConsignmentDetails-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Feb 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/02/2016    AB      1.00    Created the procedure                             --AB20160209

    --'=====================================================================

SELECT convert(date,ci.createddatetime) as Date

      ,case when isinternational=1 then convert(varchar(10),case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end) else Accountnumber end as Accountcode
      ,[BillTo]
	  ,c.AccountNumber
      ,p.shortname
      ,[Warehouse]
      ,[TermDisc]
      ,[Territory]
      ,[RepCode]
      ,[IndustryCode]
      ,[CustType]
      ,[MarketingFlag]
      ,[AverageDaysToPay]
      ,[IndustrySubGroup]
      ,p.[Postcode]
      ,[Locality]
      ,[OriginalRepCode]
      ,[LastSale]
      ,p.ABN
      ,[RepName]
      ,[CrmAccount]
      ,[CrmParent]
      ,[CrmType]
      ,[CrmRegion]
      ,[OriginalRepName]
	  ,ci.consignmentcode
	  ,a.FirstName+' '+a.LastNAme as SenderName
      ,a.address1 as Senderaddress1
      ,a.address2 as SenderAddress2
	  ,a.email as Senderemail
	  ,a.phone as SenderPhone
	  ,a.suburb as SenderSuburb
	  ,a.Postcode as SenderPostcode
	  ,isnull(s.StateCode,isnull(a.statename,'Unknown')) as SenderState
	  ,case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end as SenderCountry
	  ,a1.FirstName+' '+a1.LastNAme as ReceiverName
      ,a1.address1 as Receiveraddress1
      ,a1.address2 as ReceiverAddress2
	  ,a1.email as Receiveremail
	  ,a1.phone as ReceiverPhone
	  ,a1.suburb as ReceiverSuburb
	  ,a1.Postcode as ReceiverPostcode
	  ,isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as ReceiverState
	  ,case when isnull(a1.countrycode,a1.country) is null then 'Australia' else isnull(a1.country,a1.countrycode) end as ReceiverCountry
	  ,calculatedtotal
	  ,calculatedgst
--into #temp
  FROM ezyfreight.[dbo].[tblconsignment] ci join ezyfreight.[dbo].[tblCompanyUsers] cu on cu.userid=ci.userid
                                            join ezyfreight.dbo.tblcompany c on c.companyid=cu.CompanyID
                                           left join [Pronto].[dbo].[ProntoDebtor] p on p.accountcode=case when isinternational=1 then convert(varchar(10),case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end) else Accountnumber end
                                            join tbladdress a on a.addressid=ci.pickupid
											join tbladdress a1 on a1.addressid=ci.destinationid
											left    join  [dbo].[tblState] s on s.stateid=a.stateid
											left    join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
  where 
  --accountcode not like 'WI%' 
  --and c.AccountNumber<>'WD00000060' and c.AccountNumber<>'WD00000006' and c.AccountNumber<>'WD00000926'
  isnull(ci.IsAccountCustomer,0)=1
  and convert(date,ci.createddatetime) between @StartDate and @EndDate 
  and  (ltrim(rtrim(c.companyname)) like '%'+@Accountname+'%' or isnull(@Accountname,'')='') and (case when isinternational=1 then convert(varchar(10),case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end) else Accountnumber end like '%'+@ChildAccountcode+'%' or isnull(@ChildAccountcode,'')='')
  and  (ltrim(rtrim(p.billto)) like '%'+@ParentAccountcode+'%' or isnull(@ParentAccountcode,'')='')
  and (case when convert(date,ci.createddatetime)>'2016-01-31' then ci.isprocessed else 1 end)=1
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptAccountCustomerConsignmentDetails]
	TO [ReportUser]
GO
