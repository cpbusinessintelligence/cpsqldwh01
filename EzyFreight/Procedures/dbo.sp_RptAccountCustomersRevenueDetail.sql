SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptAccountCustomersRevenueDetail](@StartDate date,@EndDate date) as
begin
     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptAccountCustomersRevenue]]
    --' ---------------------------
    --' Purpose: AccountCustomersRevenue-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 12 Nov 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 12/11/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

select convert(date,c.createddatetime) as Date,
       p.companyName,
       case when isinternational=0 then 'Domestic' else 'International' end as ProductType,
	   sum(calculatedtotal) as GrossTotal,
	   sum(calculatedgst) as GST,
	   isnull(sum(insuranceamount),0) as Insurance,
	   count(*) as CountofConsignments
 FROM [EzyFreight].[dbo].[tblConsignment]  c left join  [dbo].[tblcompanyusers] u on u.userid=c.userid
					                              left join  [dbo].[tblcompany] p on u.companyid=p.companyid
												   where isnull(isaccountcustomer,0)=1 
  and convert(date,c.createddatetime) between @StartDate and @EndDate and isnull(p.AccountNumber,'') not in (Select [Accountcode] from  [dbo].[tblCompanyTestUser])
   and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
  group by convert(date,c.createddatetime),p.companyName,case when isinternational=0 then 'Domestic' else 'International' end 
   order by convert(date,c.createddatetime)

   end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptAccountCustomersRevenueDetail]
	TO [ReportUser]
GO
