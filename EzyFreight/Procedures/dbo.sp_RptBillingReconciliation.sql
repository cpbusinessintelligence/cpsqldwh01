SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptBillingReconciliation](@StartDate date ,@EndDate date) as
begin

   --'=====================================================================
    --' CP -Stored Procedure -[sp_RptBillingReconciliation] 
    --' ---------------------------
    --' Purpose: sp_RptBillingReconciliation-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 11 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 11/07/2015    AB      1.00    Created the procedure                             --AB20150711

    --'=====================================================================


--Select ConsignmentCode,
--       SalesOrderId,
--	   s.NoOfItems,
--	   s.TotalWeight,
--	   s.TotalVolume,
--	   s.RateCardId,
--	   ts.Statusdescription as SalesOrderStatus,
--	   InvoiceNo,
--	   (TotalFreightExGST) as TotalFreightExGST,
--	   i.GST,
--	   TotalFreightInclGST,
--	   ts1.StatusDescription as InvoiceStatus,
--	   PaymentRefNo,
--	   AuthorizationCode,
--	   p.billedFreightCharge as ProntoBilledFreight,
--	   p.BilledFuelSurcharge as ProntoBilledFuel,
--	   p.BilledTransportCharge as ProntoBilledTransport,
--	   convert(float, TotalFreightExGST-isnull(p.billedFreightCharge,0)) as Variance,
--	   case when p.billedFreightCharge is NULL then 'U' else 'B'  end as ProntoBilledFlag
--       --Any flag required in filter like isbilled?
--	   from [EzyFreight].[dbo].[tblConsignment] c left join [EzyFreight].[dbo].[tblSalesOrder] s on ReferenceNo=consignmentid
--	                                           left join [EzyFreight].[dbo].[tblInvoice] i on i.InvoiceNumber=s.invoiceno
--		    								   left join [Pronto].[dbo].[Prontobilling] p on p.consignmentreference=consignmentcode
--											   left join [EzyFreight].[dbo].[tblStatus] ts on ts.statusid=salesorderstatus
--											   left join  [EzyFreight].[dbo].[tblStatus] ts1 on ts1.statusid=invoicestatus
--											   where convert(date,i.createddatetime)>=@StartDate and convert(date,i.createddatetime)<=@EndDate
SELECT
       --isnull(s.StateCode,isnull(a.statename,'Unknown')) as Statecode,
       Consignmentid,
	   Consignmentcode,
	   p.prizezone as RevenueOriginZone,
	   p1.prizezone as RevenueDestinationZone,
	   isnull(s.StateCode,isnull(a.statename,'Unknown')) as OriginState,
	    isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as DestinationState,
	   --a.postcode,
	   --a.Suburb1,
	   --a1.postcode,
	   --a1.suburb,
	   case when isinternational=1 and isnull(s.StateCode,isnull(a.statename,'Unknown')) in ('NSW','NT','QLD','TAS','WA','SA','VIC') then 'INTERNATIONAL OUTBOUND' 
	        when isinternational=1 and isnull(s1.StateCode,isnull(a1.statename,'Unknown')) in ('NSW','NT','QLD','TAS','WA','SA','VIC') then 'INTERNATIONAL INBOUND' else '' end as NetworkCategory,
	   'Parcels' as ServiceCategory,
       case when c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end as ServiceArea,
      case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Air SME'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RAL','RSH','RAH','RSM','RAM') then 'Domestic Road to Air'  else '' end as ServiceType,
    --  isnull(c.customerRefNo,'') as CustomerReferenceNo,
       convert(date,c.createddatetime) as Date,
	  -- count(distinct c.consignmentid) as JobsBooked,
	   pb.billedFreightCharge as ProntoBilledFreight,
       pb.BilledFuelSurcharge as ProntoBilledFuel,
--	   pb.BilledTransportCharge as ProntoBilledTransport
	   convert(decimal(12,2),0) as WebFreightCharge,
	   convert(decimal(12,2),0) as WebFuelCharge,
	 --  sum(GST) as GST,
	   convert(decimal(12,2),0) as WebReCharge,
	   convert(decimal(4,2),0) as WebCrCardCharge,
	   convert(decimal(4,2),0) as WebAdminFee,
	   convert(decimal(4,2),0) as WebInsurance,
	   case when pb.billedFreightCharge is NULL then 'U' else 'B'  end as ProntoBilledFlag
	  -- sum(NetTotal) as NetTotal
        into #temp
       FROM [EzyFreight].[dbo].[tblconsignment] c   left join [Pronto].[dbo].[Prontobilling] pb on replace(pb.ConsignmentReference,'_B','')=c.consignmentcode 
	                                              left join [dbo].[tblAddress] a on c.pickupid=a.addressid
	                                              left join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid   
                                                  left join  [dbo].[tblState] s on s.stateid=a.stateid
												  left join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
												  left join [DWH].[dbo].[Postcodes] p on p.postcode=a.postcode and p.suburb=a.suburb
												  left join [DWH].[dbo].[Postcodes] p1 on p1.postcode=a1.postcode and p1.suburb=a1.suburb
						
	                                          --    left join [EzyFreight].[dbo].[tblsalesOrder] s1 on s1.referenceNo=c.consignmentid 
												
	where convert(date,c.createddatetime) between @StartDate and @EndDate and isnull(isaccountcustomer,0)<>1
	 and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
	--and isinternational=0
	--between 
--'2015-07-31' and '2015-08-09'
	   --<dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())
 -- group by 
 --         --isnull(s.StateCode,isnull(a.statename,'Unknown')) ,
 --         consignmentId,
 --          case when c.RateCardID in ('EXP','SAV') then 'International' else 'Domestic' end,
 --           case when c.RateCardID in ('SDC','PDC') then 'Same Day' when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'ACE Interstate' when c.RateCardID in ('REC','PEC') then 'Domestic Saver/RoadExpress' when  c.RateCardID = 'EXP' then 'International Express' when c.RateCardID = 'SAV' then 'International Saver' else '' end ,
 --          c.RateCardID,
 --     --     isnull(c.customerRefNo,'') ,
 --         convert(date,c.createddatetime),
 --         datepart(hour,c.Createddatetime)
 ---- order by 1,2,3,4 asc

    Update #temp set WebFreightCharge=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description not in ('Reweight charge','Credit Card Surcharge','Insurance','Admin Fee')

     Update #temp set WebRecharge=s2.FreightCharge from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
     where description='Reweight charge'

	 Update #temp set WebFuelCharge=isnull(s2.Fuelcharge,0) from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
    where description not in ('Reweight charge','Credit Card Surcharge','Insurance','Admin Fee')


  Update #temp set WebCrCardCharge=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description='Credit Card Surcharge'

  
  Update #temp set WebInsurance=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description='Insurance'

    
  Update #temp set WebAdminFee=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description='Admin Fee'

   Update #temp set NetworkCategory=CASE WHEN (LTRIM(RTRIM(pb.RevenueOriginZone)) = LTRIM(RTRIM(pb.RevenueDestinationZone))
							AND LTRIM(RTRIM(pb.RevenueOriginZone)) IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
													'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL'))
						THEN 'METRO'
					ELSE
						CASE WHEN (pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
														'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND ((ISNULL(Originstate, 'XX') = ISNULL(DestinationState, 'ZZ'))
											OR (ISNULL(Originstate, 'XX') IN ('ACT','NSW')
												AND ISNULL(DestinationState, 'ZZ') IN ('ACT','NSW'))))
							THEN 'REGIONAL'
						ELSE
							CASE WHEN ((ISNULL(Originstate, 'XX') = ISNULL(DestinationState, 'ZZ'))
											OR (ISNULL(Originstate, '') IN ('ACT','NSW')
												AND ISNULL(DestinationState, 'ZZ') IN ('ACT','NSW')))
								THEN 'INTRASTATE'
							ELSE
								'INTERSTATE'
							END
						END
					END
					from #temp pb where networkcategory=''

Select  Consignmentid,
	   Consignmentcode,
	   RevenueOriginZone,
	   RevenueDestinationZone,
	   OriginState,
	   DestinationState,
	   NetworkCategory,
	   ServiceCategory,
       ServiceArea,
       ServiceType,
       Date,
	   isnull(ProntoBilledFreight,0) as ProntoBilledFreight,
       isnull(ProntoBilledFuel,0) as ProntoBilledFuel,
--	   pb.BilledTransportCharge as ProntoBilledTransport
	   isnull(WebFreightCharge,0) as WebFreightCharge,
	   isnull(WebFuelCharge,0) as WebFuelCharge,
	   isnull(WebReCharge,0) as WebReCharge,
	   isnull(WebCrCardCharge,0) as WebCrCardCharge,
	   isnull(WebAdminFee,0) as WebAdminFee,
	   isnull(WebInsurance,0) as WebInsurance,
	   ProntobilledFlag,
	   convert(float,isnull(ProntoBilledFreight,0)-isnull(WebFreightCharge,0) ) as FreightVariance,
	   convert(float,isnull(ProntoBilledFuel,0)-isnull(WebFuelCharge,0)) as FuelVariance
	    from #temp

  --Select --Statecode,
  --       Date,
  --       ServiceArea,
  --       ServiceType,
		-- Case when Originstate in ('NSW','NT','QLD','TAS','WA','SA','VIC') then OriginState else Destinationstate end as State,
		-- NetworkCategory,
		-- ServiceCategory,
		-- count(*) as CustomersTraded,
		-- sum(WebFreightcharge) as WebFreightCharge,
		-- sum(WebFuelcharge) as WebFuelcharge,
		-- sum(WebRecharge) as WebReCharge,
		-- sum(WebAdminFee) as WebAdminFee,
		-- sum(WebInsurance) as WebInsurance,
		-- sum(WebCrcardCharge) as WebCrcardCharge,
		--  sum(ProntoBilledFreight) as ProntoBilledFreight,
		-- sum(ProntoBilledFuel) as ProntoBilledFuel

		-- from #temp
		-- group by Date,
		--          --Statecode,
		--          ServiceArea,
  --                ServiceType, 
		--		  Case when Originstate in ('NSW','NT','QLD','TAS','WA','SA','VIC') then OriginState else Destinationstate end,
		--		  NetworkCategory,
		--          ServiceCategory
		--		  order by Date



end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptBillingReconciliation]
	TO [ReportUser]
GO
