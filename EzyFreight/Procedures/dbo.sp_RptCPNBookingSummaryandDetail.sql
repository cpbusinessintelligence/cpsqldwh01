SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_RptCPNBookingSummaryandDetail] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptCPNBookingSummaryandDetail
    --' ---------------------------
    --' Purpose: Get Booking details for Coupons-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

SELECT Branch,
       convert(date,createddatetime) as Date,
       datepart(hour,Createddatetime) as Hour,
	   count(*) as PrepaidCPNRequest
	   into #temp
  FROM [EzyFreight].[dbo].[tblBooking] c where pickupname  not like '%Ezysend%' and isprocessed=1 and convert(date,c.createddatetime)=convert(date,getdate())
  --c.createddatetime<=dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())
  group by Branch,
          convert(date,createddatetime),
          datepart(hour,Createddatetime)
  order by 2,3,1 asc

Select case when Branch='Sydney' then 'NSW' when Branch='Brisbane' then 'QLD' when Branch='Gold Coast' then 'QLD' when Branch='Melbourne' then 'VIC'  when Branch='Adelaide' then 'SA' else 'Unknown' end as State,
	   Date,
       Hour,
	   sum(PrepaidCPNRequest) as PrepaidCPNRequest
	   from #temp
		group by case when Branch='Sydney' then 'NSW' when Branch='Brisbane' then 'QLD' when Branch='Gold Coast' then 'QLD' when Branch='Melbourne' then 'VIC'  when Branch='Adelaide' then 'SA' else 'Unknown' end,
				 Date,
                 Hour
        order by 2,3,1 asc


end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptCPNBookingSummaryandDetail]
	TO [ReportUser]
GO
