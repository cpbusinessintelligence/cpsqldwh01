SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


Create Procedure [dbo].[sp_RptContactusReport](@State varchar(20),@StartDate date,@EndDate date) as
begin
  --'=====================================================================
    --' CP -Stored Procedure -[sp_RptContactusReport]
    --' ---------------------------
    --' Purpose: Website Dashboard Summary-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

If(@State='ALL')
SELECT [Id]
      ,[FirstName]
      ,[LastName]
      ,[Email]
      ,[Phone]
      ,[Suburb]
      ,[PostCode]
      ,s.StateCode
      ,[Subject]
      , REPLACE(REPLACE(REPLACE(REPLACE(isnull([HowCanIHelpYou],''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' ') as HowcanIhelpYou
      ,[IsSubscribe]
      ,[CreatedDate]
  ,isnull(TrackingNo,'') as TrackingNo
	  ,isnull(addressline1,'') as AddressLine1
	  ,isnull(addressline2,'') as AddressLine2
  FROM [EzyFreight].[dbo].[tblContactUs] c join tblstate s on s.stateid=c.stateid where convert(date,createddate) between @StartDate and @EndDate

else

SELECT [Id]
      ,[FirstName]
      ,[LastName]
      ,[Email]
      ,[Phone]
      ,[Suburb]
      ,[PostCode]
      ,s.StateCode
      ,[Subject]
      , REPLACE(REPLACE(REPLACE(REPLACE(isnull([HowCanIHelpYou],''), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' ') as HowcanIhelpYou
      ,[IsSubscribe]
      ,[CreatedDate]
        ,isnull(TrackingNo,'') as TrackingNo
	  ,isnull(addressline1,'') as AddressLine1
	  ,isnull(addressline2,'') as AddressLine2
  FROM [EzyFreight].[dbo].[tblContactUs] c join tblstate s on s.stateid=c.stateid where statecode=@State and convert(date,createddate) between @StartDate and @EndDate
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptContactusReport]
	TO [ReportUser]
GO
