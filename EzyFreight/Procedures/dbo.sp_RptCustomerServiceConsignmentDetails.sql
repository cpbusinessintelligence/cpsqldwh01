SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptCustomerServiceConsignmentDetails](@StartDate date,@EndDate date)
as
begin
     --'=====================================================================
    --' CP -Stored Procedure - sp_RptCustomerServiceConsignmentDetails
    --' ---------------------------
    --' Purpose: sp_RptCustomerServiceConsignmentDetails-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 14 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 14/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

select Consignmentcode,
       isnull(s.StateCode,isnull(a.statename,'Unknown')) as Origin,
	   case when c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then isnull(a1.countrycode,a1.country) else isnull(s1.StateCode,isnull(a1.statename,'Unknown')) end as Destination,
	   isnull(a.companyname,(a.FirstName+' '+a.LastName)) as Sender,
	   isnull(a1.companyname,(a1.FirstName+' '+a1.LastName)) as Receiver,
	   convert(date,c.createddatetime) as BookingDate,
	   ConsignmentPreferPickupDate,
	   b.BookingRefNo as BookingJobNumber,
	   TotalWeight,
	   ETA,
	   l.labelnumber as TrackingNumber,
       case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else RateCardDescription end as ProductType
       FROM [EzyFreight].[dbo].[tblConsignment] c 
                                            left join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=c.consignmentid
											left join [EzyFreight].[dbo].[tblBooking] b on b.consignmentid=c.consignmentid
                                            left join [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											 left join  [dbo].[tblcompanyusers] u on u.userid=c.userid
											  left join  [dbo].[tblcompany] p on u.companyid=p.companyid
											 left join [cpsqlops01].[couponcalculator].[dbo].[Ratecard] r on r.ratecardcode=c.ratecardid
 where convert(date,c.createddatetime) between @Startdate and @EndDate and isnull(p.AccountNumber,'') not in (Select [Accountcode] from  [dbo].[tblCompanyTestUser])
  and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
 end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptCustomerServiceConsignmentDetails]
	TO [ReportUser]
GO
