SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_RptCustomersOnShoppingCart] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptCustomersOnShoppingCart
    --' ---------------------------
    --' Purpose: All customers on shopping cart-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

SELECT case when  c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end as ServiceArea,
     --   isnull(customerRefNo,'') as CustomerReferenceNo,
       convert(date,createddatetime) as Date,
       datepart(hour,Createddatetime) as Hour,
	   count(*) as CustomersOnshoppingCart
	   --into #temp
  FROM [EzyFreight].[dbo].[tblconsignmentstaging] c where  convert(date,c.createddatetime)=convert(date,getdate())
  --createddatetime<=dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())
  group by case when  c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end,
      --      isnull(customerRefNo,'') ,
          convert(date,createddatetime),
          datepart(hour,Createddatetime)
  order by 1,2,3 asc




end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptCustomersOnShoppingCart]
	TO [ReportUser]
GO
