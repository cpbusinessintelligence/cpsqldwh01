SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_RptCustomersTraded] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptCustomersTraded
    --' ---------------------------
    --' Purpose: All customers who traded-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

SELECT consignmentcode,
       consignmentid,
	   ratecardid,
	   convert(date,c.createddatetime) as Date,
       datepart(hour,c.Createddatetime) as Hour,
	   count(*) as CustomersTraded,
	   case when isaccountcustomer=1 then c.CalculatedTotal else convert(decimal(12,2),0) end as GrossTotal,
	   case when isaccountcustomer=1 then c.CalculatedGST else convert(decimal(12,2),0) end as GST,
	   case when isaccountcustomer=1 then c.CalculatedTotal+c.CalculatedGST else convert(decimal(12,2),0) end as [NetTotal],
	   isaccountcustomer
       into #temp
	   FROM [EzyFreight].[dbo].[tblConsignment] c left join  [dbo].[tblcompanyusers] u on u.userid=c.userid
					                              left join  [dbo].[tblcompany] p on u.companyid=p.companyid
	   where Convert(date,c.CreatedDatetime) =CONVERT(date,GETDATE()) and c.isprocessed=1
	   --'2015-11-06'
	   --convert(date,getdate()) 
	   and isnull(p.AccountNumber,'') not in (Select [Accountcode] from  [dbo].[tblCompanyTestUser])
	   group by consignmentcode,
       consignmentid,
	   ratecardid,
	   convert(date,c.createddatetime) ,
       datepart(hour,c.Createddatetime) ,
	   isaccountcustomer,
	   c.CalculatedTotal,
	   c.CalculatedGST


Update #temp set [GrossTotal]=(Select sum(GrossTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid and #temp.GrossTotal=0


Update #temp set [GST]=(Select sum(GST) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid  and #temp.GST=0


Update #temp set [NetTotal]=(Select sum(NetTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid and #temp.NetTotal=0


select case when  RateCardID like 'EXP%' or RateCardID like 'SAV%' then 'International' else 'Domestic' end as ServiceArea,
        case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak'  else '' end as ServiceType,
	   Date,
	   Hour,
	   sum(CustomersTraded) as CustomersTraded,
	   sum(GrossTotal) as GrossTotal,
	   sum(GST) as GST,
	   sum(NetTotal) as NetTotal
	   from #temp c  
	   group by case when RateCardID like 'EXP%' or RateCardID like 'SAV%' then 'International' else 'Domestic' end ,
       case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else '' end ,
	   Date,
	   Hour






end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptCustomersTraded]
	TO [ReportUser]
GO
