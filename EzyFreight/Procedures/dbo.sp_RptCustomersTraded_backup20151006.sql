SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[sp_RptCustomersTraded_backup20151006] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptCustomersTraded
    --' ---------------------------
    --' Purpose: All customers who traded-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

SELECT case when c.RateCardID in ('EXP','SAV') then 'International' else 'Domestic' end as ServiceArea,
       case when c.RateCardID in ('SDC','CE3','CE5','PDC','PE3','PE5') then 'Australian City Express' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID = 'EXP' then 'Express' when c.RateCardID = 'SAV' then 'Saver' else '' end as ServiceType,
    --  isnull(c.customerRefNo,'') as CustomerReferenceNo,
       convert(date,c.createddatetime) as Date,
       datepart(hour,c.Createddatetime) as Hour,
	   count(distinct c.consignmentid) as CustomersTraded,
	   sum(GrossTotal) as GrossTotal,
	   sum(GST) as GST,
	   sum(NetTotal) as NetTotal
        into #temp
       FROM [EzyFreight].[dbo].[tblconsignment] c left join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceNo=c.consignmentid where  convert(date,c.createddatetime)=convert(date,getdate())
	   --<dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())
  group by case when c.RateCardID in ('EXP','SAV') then 'International' else 'Domestic' end,
           case when c.RateCardID in ('SDC','CE3','CE5','PDC','PE3','PE5') then 'Australian City Express' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID = 'EXP' then 'Express' when c.RateCardID = 'SAV' then 'Saver' else '' end ,
           c.RateCardID,
      --     isnull(c.customerRefNo,'') ,
          convert(date,c.createddatetime),
          datepart(hour,c.Createddatetime)
  order by 1,2,3,4 asc



  Select ServiceArea,
         ServiceType,
		 Date,
		 Hour,
		 sum(CustomersTraded) as CustomersTraded,
		 sum(GrossTotal) as GrossTotal,
		 sum(GST) as GST,
		 sum(NetTotal) as NetTotal
		 from #temp
		 group by ServiceArea,
         ServiceType,
		 Date,
		 Hour





end
GO
