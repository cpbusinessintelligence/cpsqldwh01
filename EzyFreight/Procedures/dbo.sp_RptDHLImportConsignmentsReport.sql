SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptDHLImportConsignmentsReport](@StartDate Date,@EndDate Date)
as begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_RptWebsiteErrorReport
    --' ---------------------------
    --' Purpose: WebsiteErrorReport-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 20 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 20/08/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

	Select isnull(c.ConsignmentCode,'Unknown') as ConsignmentCode,
	       a.address1 as Address1,
		   a.Address2 as Address2,
		   a.Suburb as Suburb,
		   a.Postcode as Postcode,
		   a.phone as Phone,
		   a.Mobile as Mobile,
		   isnull(s1.StateCode,isnull(a.statename,'Unknown')) as Statecode,
		   s.PickupDate as PreferredPickupDate,
		   s.PreferPickupTime as PreferredPickupTime,
		   case when ( isnull([XMLResponcePU_DHL],'')  like '%<ActionNote>Success</ActionNote>%' or [XMLResponcePU_DHL] is  null) then 'Success'  else 'Failure' end as [Status],
		   case when  [XMLResponcePU_DHL] not like '%<ActionNote>Success</ActionNote>%' then Replace(REPLACE(REPLACE(REPLACE(REPLACE(substring(XMLResponcePU_DHL,charindex('<ConditionData>',XMLResponcePU_DHL,1)+15,(charindex('</ConditionData>',XMLResponcePU_DHL,1)-(charindex('<ConditionData>',XMLResponcePU_DHL,1)+15))), CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '),char(9),'') else '' end as ReasonforFailure
	 FROM [EzyFreight].[dbo].[tblConsignmentStaging] s
	  left join [EzyFreight].[dbo].[tblConsignment] c on c.consignmentid=s.consignmentid
	  left join [EzyFreight].[dbo].[tblAddress] a on s.contactid=a.AddressID
	  left join [EzyFreight].[dbo].[tblState] s1 on s1.stateid=a.stateid
  where s.RateCardID like 'EXP%' and Isnull(s.isprocessed,1)=1 and convert(date,s.CreatedDateTime) between @StartDate and @EndDate
   and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
  --and [XMLResponcePU_DHL] not like '%<ActionNote>Success</ActionNote>%'  



end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDHLImportConsignmentsReport]
	TO [ReportUser]
GO
