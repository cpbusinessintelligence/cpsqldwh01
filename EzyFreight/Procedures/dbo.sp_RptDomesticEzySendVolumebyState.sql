SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptDomesticEzySendVolumebyState] as
begin


SELECT isnull(s.StateCode,isnull(a.statename,'Unknown')) as Statecode,
       datepart(HOUR,c.createddatetime) as HOUR,
       count(*) as DomesticEzyFreightVolume
 FROM [EzyFreight].[dbo].[tblConsignment] c left join [dbo].[tblAddress] a on c.pickupid=a.addressid  
                                           left  join  [dbo].[tblState] s on s.stateid=a.stateid
  where  c.RateCardID not like 'EXP%' and c.RateCardID not like 'SAV%'  and c.isinternational=0 and convert(date,c.createddatetime)=convert(date,getdate())
  --c.createddatetime<=dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())
  group by isnull(s.StateCode,isnull(a.statename,'Unknown')),
           datepart(HOUR,c.createddatetime)

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptDomesticEzySendVolumebyState]
	TO [ReportUser]
GO
