SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_RptEzy2ShipInternationalSalesbyCustomer](@StartDate date,@EndDate date) as 
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_RptEzy2ShipInternationalSalesbyCustomer]
    --' ---------------------------
    --' Purpose: Ezy2Ship InternationalSalesbyCustomer-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 06 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 06/08/2015   AB      1.00    Created the procedure                             --AB20150806

    --'=====================================================================



Select  consignmentcode,
       consignmentid,
	   case when c.userid=-1 then a.FirstName+' '+a.LastName else u.displayname end as [Customer Name],
	   case when c.userid=-1 then a.email else u.email end as [Customer Email],
	   case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' else RatecardDescription end as Product,
	   c.NoOfItems,
	   case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end as Totalweight,
	      case when isnull(isaccountcustomer,0)=1 then c.CalculatedTotal else convert(decimal(12,2),0) end as [Gross Total],
	    case when isnull(isaccountcustomer,0)=1 then c.CalculatedGST else convert(decimal(12,2),0) end as [GST],
	   case when isnull(isaccountcustomer,0)=1 then c.CalculatedTotal+c.CalculatedGST else convert(decimal(12,2),0) end as [Net Total],
	   convert(decimal(12,2),0) as [Weight per shipment - WPS (Kg)],
	   convert(decimal(12,2),0) as [Revenue per shipment - RPS (AUD)],
	   convert(decimal(12,2),0) as [Revenue per Kilo - RPK (AUD)],
	   case when c.NoOfItems>1 then 1 else 0 end as Multipieceshipments
	   into #temp

  FROM [EzyFreight].[dbo].[tblConsignment] c 
		                                           left    join [dbo].[tblAddress] a on c.contactid=a.addressid  
                                                    left    join  [dbo].[tblState] s on s.stateid=a.stateid
													left join [dbo].[Users] u on c.userid=u.userid
													left join  [dbo].[tblcompanyusers] u1 on u1.userid=c.userid
											        left join  [dbo].[tblcompany] p on u1.companyid=p.companyid
											        left join [cpsqlops01].[couponcalculator].[dbo].[Ratecard] r on r.ratecardcode=c.ratecardid
													where Convert(date,c.CreatedDatetime) 
													between @StartDate and @EndDate and isinternational=1 and isnull(p.AccountNumber,'') not in (Select [Accountcode] from  [dbo].[tblCompanyTestUser])
													--between @StartDate and @EndDate and isinternational=0
													 and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1

Update #temp set [Gross Total]=(Select sum(GrossTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid 

Update #temp set [GST]=(Select sum(GST) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid 


Update #temp set [Net Total]=(Select sum(NetTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid 

Select [Customer Name] as [Customer Name],
       Product,
	   count(*) as [Total Shipments],
	--   sum(NoOfItems) as Multipieceshipments,
	   sum(Totalweight) as [Total Weight (Kg)],
	   sum([Gross Total]) as [Total Revenue (AUD)],
	   sum([GST]) as [GST],
	   sum([Net Total]) as [Net Total],
	   [Weight per shipment - WPS (Kg)],
	   [Revenue per shipment - RPS (AUD)],
	   [Revenue per Kilo - RPK (AUD)],
	   sum(Multipieceshipments) as Multipieceshipments

into #temp1
from #temp group by [Customer Name],
                    Product,
					[Weight per shipment - WPS (Kg)],
	   [Revenue per shipment - RPS (AUD)],
	   [Revenue per Kilo - RPK (AUD)]

Update #temp1 set [Weight per shipment - WPS (Kg)]=[Total Weight (Kg)]/[Total Shipments]

Update #temp1 set [Revenue per shipment - RPS (AUD)]=[Total Revenue (AUD)]/[Total Shipments]

Update #temp1 set [Revenue per Kilo - RPK (AUD)]=[Total Revenue (AUD)]/[Total Weight (Kg)]

select * from #temp1 order by 1,2,3
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptEzy2ShipInternationalSalesbyCustomer]
	TO [ReportUser]
GO
