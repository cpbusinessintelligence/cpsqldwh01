SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptEzyFreightSalesbyProduct](@StartDate date,@EndDate date) as
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_RptEzyFreightSalesbyProduct]
    --' ---------------------------
    --' Purpose: Ezy2Ship Overall Sales-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 06 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 06/08/2015   AB      1.00    Created the procedure                             --AB20150806

    --'=====================================================================


	Select  consignmentcode,
       consignmentid,
	  case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else RateCardDescription end as ProductType,
     --  Convert(date,c.CreatedDatetime) as Date,
	   case when c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end as ServiceArea,
	   case when isinternational=0 then isnull(s.StateCode,isnull(a.statename,'Unknown')) else isnull(a.countrycode,isnull(a.country,'')) end as [From],
	   case when isinternational=0 then isnull(s1.StateCode,isnull(a1.statename,'Unknown')) else isnull(a1.countrycode,isnull(a1.country,'')) end as [To],
	   c.NoOfItems,
	   case when (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end)> (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) then (case when isnull(TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(TotalWeight,0) end) else (case when isnull(TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(TotalVolume,0) end) end as Totalweight,
	   Case when TypeofExport='Return' then 1 else 0 end as IsReturn,
	   Case when Isinsurance=1 then 1 else 0 end as IsInsurance,
	   	   case when isnull(isaccountcustomer,0)=1 then c.CalculatedTotal else convert(decimal(12,2),0) end as [Gross Total],
	    case when isnull(isaccountcustomer,0)=1 then c.CalculatedGST else convert(decimal(12,2),0) end as [GST],
	   case when isnull(isaccountcustomer,0)=1 then c.CalculatedTotal+c.CalculatedGST else convert(decimal(12,2),0) end as [Net Total],
	   convert(decimal(12,2),0) as [Weight per shipment - WPS (Kg)],
	   convert(decimal(12,2),0) as [Revenue per shipment - RPS (AUD)],
	   convert(decimal(12,2),0) as [Revenue per Kilo - RPK (AUD)],
	   case when c.NoOfItems>1 then 1 else 0 end as Multipieceshipments 
	   into #temp

  FROM [EzyFreight].[dbo].[tblConsignment] c  left join [dbo].[tblAddress] a on c.pickupid=a.addressid  
                                              left join  [dbo].[tblState] s on s.stateid=a.stateid
											  left join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                              left join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
											  left join [cpsqlops01].[couponcalculator].[dbo].[Ratecard] r on r.ratecardcode=c.ratecardid
											  left join [cpsqlweb01]. [EzyFreight].[dbo].[tblCompanyUsers] cu on cu.UserID=c.userid
                                              left join [cpsqlweb01]. [EzyFreight].[dbo].[tblCompany] tc on tc.companyid=cu.companyid

													where Convert(date,c.CreatedDatetime) 
													--between '2015-08-10' and '2015-08-12' 
													between @StartDate and @EndDate  and isnull(tc.AccountNumber,'') not in (Select [Accountcode] from  [cpsqlweb01].[EzyFreight].[dbo].[tblCompanyTestUser]) and   (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1


Update #temp set [Gross Total]=(Select sum(GrossTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid 


Update #temp set [GST]=(Select sum(GST) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid 


Update #temp set [Net Total]=(Select sum(NetTotal) from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid)
from [EzyFreight].[dbo].[tblsalesOrder] where referenceNo=consignmentid 

Select  ProductType,
	   ServiceArea,
	   [From],
	   [To],
	   sum(Isreturn) as Isreturn,
	   sum(IsInsurance) as IsInsurance,
	   count(*) as [Total Shipments],
	  -- sum(NoOfItems) as Multipieceshipments,
	   sum(Totalweight) as [Total Weight (Kg)],
	   sum([Gross Total]) as [Total Revenue (AUD)],
	   sum([GST]) as [GST],
	   sum([Net Total]) as [Net Total],
	   [Weight per shipment - WPS (Kg)],
	   [Revenue per shipment - RPS (AUD)],
	   [Revenue per Kilo - RPK (AUD)],
	   sum(Multipieceshipments) as Multipieceshipments
	   into #temp1
from #temp group by ProductType,
	   ServiceArea,
	   [From],
	   [To],
		[Weight per shipment - WPS (Kg)],
	   [Revenue per shipment - RPS (AUD)],
	   [Revenue per Kilo - RPK (AUD)]

Update #temp1 set [Weight per shipment - WPS (Kg)]=[Total Weight (Kg)]/[Total Shipments]

Update #temp1 set [Revenue per shipment - RPS (AUD)]=[Total Revenue (AUD)]/[Total Shipments]

Update #temp1 set [Revenue per Kilo - RPK (AUD)]=[Total Revenue (AUD)]/[Total Weight (Kg)]

select * from #temp1

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptEzyFreightSalesbyProduct]
	TO [ReportUser]
GO
