SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure sp_RptGetConsignmentCountForRedirectionAccounts(@StartDate date,@EndDate date) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptGetConsignmentCountForRedirectionAccounts] 
	    --' ---------------------------
    --' Purpose: sp_RptGetConsignmentCountForRedirectionAccounts-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 20 Oct 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 20/10/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

select cd_connote as Connote,cd_account as Account,AccountName
into #temp
from cpplEDI.dbo.consignment (NOLOCK) join Redirection.dbo.tblredirectionaccounts NOLOCK on cd_account=accountnumber
where cd_date between @StartDate and @EndDate


Select AccountName,count(*) as count
from #temp
group by AccountName
order by AccountName


end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptGetConsignmentCountForRedirectionAccounts]
	TO [ReportUser]
GO
