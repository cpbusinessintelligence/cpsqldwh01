SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--drop table #temp1

CREATE procedure [dbo].[sp_RptGetDHLMAnifest] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptGetDHLMAnifest]
    --' ---------------------------
    --' Purpose: GetDHLMAnifest-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 16 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 16/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Select distinct sourcereference,convert(date,eventdatetime) as PickupDate,convert(int,0) as Consignmentid into #temp
from scannergateway.dbo.trackingevent
where eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' and additionaltext1 like 'CPL%' and convert(date,eventdatetime)=convert(date,dateadd("dd",0,getdate()))

alter table #temp
alter column sourcereference varchar(100) collate SQL_Latin1_General_CP1_CI_AS

Update #temp set Consignmentid=l.consignmentid from #temp t join [cpsqlweb01].ezyfreight.dbo.tblDHLBarCodeImage l on AWBCode=t.sourcereference

Update #temp set Consignmentid=l.consignmentid from #temp t join [cpsqlweb01].ezyfreight.dbo.tblItemlabel l on l.labelnumber=t.sourcereference
where t.consignmentid=0

--Select distinct l.consignmentid,convert(date,eventdatetime) as PickupDate
--into #temp from scannergateway.dbo.trackingevent join [cpsqlweb01].ezyfreight.dbo.tblitemlabel l on labelnumber=sourcereference
--where additionaltext1='lm' and eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF'


Select distinct c.Consignmentid,
         ConsignmentCode
         ,a1.FirstName+' '+a1.LastName +CHAR(13) + CHAR(10) +a1.Phone as [Receiver name/TelNo]
		 ,a1.FirstName+' '+a1.LastName as ReceiverName
         ,a1.address1+' '+a1.address2+' '+a1.suburb+' '+convert(varchar(10),a1.Postcode)+' '+isnull(a1.StateName,'')+' '+isnull(a1.Country,'') as [Receiver address]
		 ,ConsignmentPreferPickupDate
		 ,a.FirstName+' '+a.LastName as [Sender name]
		 ,a.address1+' '+a.address2+' '+a.suburb+' '+convert(varchar(10),a.Postcode)+' '+s.StateName+a.Country as [Sender address]
		 ,a.phone as shippertelno
		 ,convert(int,0) as TotalShipments
		 ,convert(int,0) as ToatlPieces
		 ,NoOfItems as PiecesinConsignment
		 ,case when ReasonforExport ='D' then ReasonforExport else 'P' end as Productcode
		 ,convert(varchar(10),isnull(c.Netsubtotal,0))+' '+c.Currency as Value
		,convert(varchar(20),Totalweight)+' '+'KG' as Weight
		 ,convert(varchar(100),l.LabelNumber) as Labelnumber 
		 ,convert(varchar(max),'') as Labelconcatenated
		 ,convert(varchar(500),'') as Description
		 ,b.AWBCode as Airwaybillno
       ,t.PickupDate
	--	 ,RANK() OVER 
   -- (PARTITION BY c.consignmentcode ORDER BY l.itemlabelid asc) AS Rank
		 into #temp1
         FROM #temp t                       join [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c on c.consignmentid=t.consignmentid
                                            left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [cpsqlweb01].[EzyFreight].[dbo].[tblDHLBarCodeImage] b on b.ConsignmentID=c.ConsignmentID
											left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] l on l.ConsignmentID=c.ConsignmentID
											where c.ConsignmentCode like 'CPWEXP%'
											--and convert(date,c.createddatetime)>=convert(date,dateadd("dd",-5,getdate()))
											order by consignmentcode


----Update #temp set Value=(select sum(isnull(subtotal,0)) from [dbo].[tblCustomDeclaration] c where #temp.consignmentid=c.consignmentid)
----										from [dbo].[tblCustomDeclaration] c	where #temp.consignmentid=c.consignmentid


----Update #temp set Value=convert(varchar(15),value)+' '+currency from #temp t join [dbo].[tblCustomDeclaration] c on #temp.consignmentid=c.consignmentid



--Select  c.Consignmentid,
--         ConsignmentCode
--         ,a1.FirstName+' '+a1.LastName +CHAR(13) + CHAR(10) +a1.Phone as [Receiver name/TelNo]
--		 ,a1.FirstName+' '+a1.LastName as ReceiverName
--         ,a1.address1+' '+a1.address2+' '+a1.suburb+' '+convert(varchar(10),a1.Postcode)+' '+s1.StateName+a1.Country as [Receiver address]
--		 ,ConsignmentPreferPickupDate
--		 ,a.FirstName+' '+a.LastName as [Sender name]
--		 ,a.address1+' '+a.address2+' '+a.suburb+' '+convert(varchar(10),a.Postcode)+' '+s.StateName+a.Country as [Sender address]
--		 ,a.phone as shippertelno
--		 ,convert(int,0) as TotalShipments
--		 ,convert(int,0) as ToatlPieces
--		 ,NoOfItems as PiecesinConsignment
--		 ,convert(varchar(10),isnull(c.Netsubtotal,0))+' '+c.Currency as Value
--		,convert(varchar(20),Totalweight)+' '+'KG' as Weight
--		 ,l.LabelNumber 
--		 ,convert(varchar(max),'') as Labelconcatenated
--		 ,convert(varchar(50),'') as Description
--		 ,b.AWBCode as Airwaybillno
--		 ,'18/07/2015' as PickupDate
--into #temp1
--from [EzyFreight].[dbo].[tblConsignment] c 
--                                            left join [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
--                                            left join [EzyFreight].[dbo].[tblDHLBarCodeImage] b on b.ConsignmentID=c.ConsignmentID
--											left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
--											left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
--											left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
--											left join [EzyFreight].[dbo].[tblItemLabel] l on l.ConsignmentID=c.ConsignmentID
--											where c.ConsignmentCode like 'CPWEXP%'
--											and convert(date,c.createddatetime)>=convert(date,dateadd("dd",-5,getdate()))
--											order by consignmentcode

											
Update #temp1 set description=Itemdescription
from #temp1 t join [cpsqlweb01].[EzyFreight].[dbo].[tblCustomDeclaration] c on c.consignmentid=t.consignmentid;

			--select * from #temp1 where consignmentcode='CPWEXP000000160'
alter table #temp1
alter column Labelnumber varchar(8000) ;
										




WITH CTE ( Consignmentcode,LabelConcatenated, Labelnumber, length )
          AS ( SELECT Consignmentcode, CAST( '' AS VARCHAR(8000) ), CAST( '' AS VARCHAR(8000) ), 0
                 FROM #temp1
                GROUP BY Consignmentcode
                UNION ALL
               SELECT p.Consignmentcode, CAST( c.LabelConcatenated +
                      CASE WHEN length = 0 THEN '' ELSE ', ' END + p.LabelNumber AS VARCHAR(8000) ),
                      CAST( p.Labelnumber AS VARCHAR(8000)), length + 1
                 FROM CTE c
                INNER JOIN #temp1 p
                   ON c.Consignmentcode = p.Consignmentcode
                WHERE p.Labelnumber > c.Labelnumber )

SELECT Consignmentcode, LabelConcatenated
into #temp2
      FROM ( SELECT Consignmentcode, LabelConcatenated,
                    RANK() OVER ( PARTITION BY Consignmentcode ORDER BY length DESC )
               FROM CTE ) D ( Consignmentcode, LabelConcatenated, rank )
     WHERE rank = 1 ;

update #temp1 set labelconcatenated=t2.labelconcatenated from #temp2 t2 where #temp1.consignmentcode=t2.consignmentcode


Select distinct  Consignmentid,
         ConsignmentCode
         ,[Receiver name/TelNo]
		 ,ReceiverName
         ,[Receiver address]
		 ,ConsignmentPreferPickupDate
		 ,[Sender name]
		 ,[Sender address]
		 ,shippertelno
		 ,TotalShipments
		 ,ToatlPieces
		 ,PiecesinConsignment
		 ,Productcode
		 ,Value
		,Weight
		 ,Labelconcatenated
		 ,Description
		 ,Airwaybillno
		 
        from #temp1 

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptGetDHLMAnifest]
	TO [ReportUser]
GO
