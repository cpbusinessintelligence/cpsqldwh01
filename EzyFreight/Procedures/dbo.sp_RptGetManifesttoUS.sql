SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--drop table #temp2

CREATE procedure [dbo].[sp_RptGetManifesttoUS](@Manifest varchar(50)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptGetSGMAnifest]
    --' ---------------------------
    --' Purpose: GetDHLMAnifest-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 16 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 16/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================


Select distinct sourcereference,convert(date,eventdatetime) as ManifestDate,convert(int,0) as Consignmentid into #temp
from scannergateway.dbo.trackingevent
where eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' and additionaltext1=@Manifest 

alter table #temp
alter column sourcereference varchar(100) 
--collate SQL_Latin1_General_CP1_CI_AS

Update #temp set Consignmentid=l.consignmentid from #temp t join [cpsqldwh01].ezyfreight.dbo.tblitemlabel l on labelnumber=t.sourcereference


Select  c.Consignmentid,
         ConsignmentCode,
		 l.labelnumber as HAWB,
		 NoOfItems as Itemquantity,
		 convert(decimal(12,2),Totalweight) as Weight
		 ,convert(varchar(200),'') as Description
       ,t.ManifestDate
		 into #temp1
         FROM #temp t                       join [cpsqldwh01].[EzyFreight].[dbo].[tblConsignment] c on c.consignmentid=t.consignmentid
                                            left join [cpsqldwh01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [cpsqldwh01].[EzyFreight].[dbo].[tblDHLBarCodeImage] b on b.ConsignmentID=c.ConsignmentID
											left join [cpsqldwh01].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [cpsqldwh01].[EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [cpsqldwh01].[EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											left join [cpsqldwh01].[EzyFreight].[dbo].[tblItemLabel] l on l.ConsignmentID=c.ConsignmentID
											order by consignmentcode


											
Update #temp1 set description=convert(varchar(50),Itemdescription)
from #temp1 t join [cpsqldwh01]. [EzyFreight].[dbo].[tblCustomDeclaration] c on c.consignmentid=t.consignmentid;

			--select * from #temp1 where consignmentcode='CPWEXP000000160'

									

Select * from #temp1 where consignmentid<>11498
--where (@Manifest  like 'CPI00000[1][56789]%' or @Manifest like 'CPI00000[2][012345]%') and consignmentcode <>'CPWPE3000000121'

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptGetManifesttoUS]
	TO [ReportUser]
GO
