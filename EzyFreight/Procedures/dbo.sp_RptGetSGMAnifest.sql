SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--drop table #temp2

CREATE procedure [dbo].[sp_RptGetSGMAnifest](@Manifest varchar(50)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptGetSGMAnifest]
    --' ---------------------------
    --' Purpose: GetDHLMAnifest-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 16 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 16/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================


Select distinct sourcereference,convert(date,eventdatetime) as ManifestDate,convert(int,0) as Consignmentid into #temp
from scannergateway.dbo.trackingevent
where eventtypeid='F47CABB2-55AA-4F19-B5EE-C2754268D1AF' and additionaltext1=@Manifest 

alter table #temp
alter column sourcereference varchar(100) collate SQL_Latin1_General_CP1_CI_AS

Update #temp set Consignmentid=l.consignmentid from #temp t join [cpsqlweb01].ezyfreight.dbo.tblitemlabel l on labelnumber=t.sourcereference


Select  c.Consignmentid,
         ConsignmentCode,
		 l.labelnumber as HAWB,
		 NoOfItems as Itemquantity,
		 convert(decimal(12,2),Totalweight) as Weight
		 ,convert(varchar(200),'') as Description
       ,t.ManifestDate
		 into #temp1
         FROM #temp t                       join [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c on c.consignmentid=t.consignmentid
                                            left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [cpsqlweb01].[EzyFreight].[dbo].[tblDHLBarCodeImage] b on b.ConsignmentID=c.ConsignmentID
											left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [cpsqlweb01].[EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											left join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] l on l.ConsignmentID=c.ConsignmentID
											order by consignmentcode

----Update #temp set Value=(select sum(isnull(subtotal,0)) from [dbo].[tblCustomDeclaration] c where #temp.consignmentid=c.consignmentid)
----										from [dbo].[tblCustomDeclaration] c	where #temp.consignmentid=c.consignmentid


----Update #temp set Value=convert(varchar(15),value)+' '+currency from #temp t join [dbo].[tblCustomDeclaration] c on #temp.consignmentid=c.consignmentid


--Select  c.Consignmentid,
--         ConsignmentCode
--		 ,NoOfItems as Itemquantity
--		,convert(decimal(12,2),Totalweight) as Weight
--		 ,convert(varchar(50),'') as Description
--       , convert(date,getdate()) as ManifestDate
--		 into #temp1
--         FROM                               [EzyFreight].[dbo].[tblConsignment] c 
--                                            left join [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
--                                            left join [EzyFreight].[dbo].[tblDHLBarCodeImage] b on b.ConsignmentID=c.ConsignmentID
--											left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
--											left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
--											left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
--											left join [EzyFreight].[dbo].[tblItemLabel] l on l.ConsignmentID=c.ConsignmentID
--											where a1.countrycode='SG'
--											--and convert(date,c.createddatetime)>=convert(date,dateadd("dd",-5,getdate()))
--											order by consignmentcode

											
Update #temp1 set description=convert(varchar(50),Itemdescription)
from #temp1 t join [cpsqlweb01]. [EzyFreight].[dbo].[tblCustomDeclaration] c on c.consignmentid=t.consignmentid;

			--select * from #temp1 where consignmentcode='CPWEXP000000160'

									

Select * from #temp1 where consignmentcode <>'CPWPE3000000121'

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptGetSGMAnifest]
	TO [ReportUser]
GO
