SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptInternationalRegularshippers](@Date Date) as
begin

 --'=====================================================================
    --' CP -Stored Procedure -[sp_RptInternationalRegularshippers]
    --' ---------------------------
    --' Purpose: InternationalRegularshippers----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 18 Nov 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 18/11/2015   AB      1.00    Created the procedure                             --AB20151108

    --'=====================================================================

--drop table #temp0

select distinct a.companyname,a.userid,count(*) as count
into #temp
 from tblconsignment c join [dbo].[tblAddress] a on c.userid=a.userid and c.contactid=a.addressid  where  convert(date,c.createddatetime)>=@Date and isinternational=1 and c.userid<>-1 and c.isprocessed=1  and isnull(companyname,'')<>''
 --and a.countrycode='AU'
group by a.companyname,a.userid
having count(*)>=3

--select * from  tblconsignment c join [dbo].[tblAddress] a on c.userid=a.userid   where isinternational=1 and c.userid<>-1 and c.isprocessed=1 and a.companyname like '%derwent%' and  a.countrycode='AU'

--select * from #temp where companyname like '%derwent%'

--Update #temp set userid=a.userid from [dbo].[tblAddress] a where a.companyname=#temp.companyname

--select * from tbladdress a where userid=53 a.companyname like '%derwent%' and coutrycode 

select companyname,count(*) as count
into #temp1
from tblconsignment c   join [dbo].[tblAddress] a on c.contactid=a.addressid  
where isinternational=1 and c.userid=-1 and c.isprocessed=1 and  convert(date,c.createddatetime)>=@Date and isnull(companyname,'')<>''
group by companyname
having count(*)>=3

--select * from tbladdress where companyname like '%zenith%'

--select * from tblconsignment where userid=4959 and isinternational=1

--Select * from #temp1

--select * from #temp order by 1

Select convert(date,c.createddatetime) as Date,
       isnull(a.companyname,'') as BusinessName,
       a.FirstName+' '+a.LastNAme as SenderName,
       a.address1 as Senderaddress1,
       a.address2 as SenderAddress2,
			a.email as Senderemail,
			a.phone as SenderPhone,
			a.suburb as SenderSuburb,
			a.Postcode as SenderPostcode,
			isnull(s.StateCode,isnull(a.statename,'Unknown')) as SenderState,
			case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end as SenderCountry,
			count(*)

from #temp t join   tblconsignment c   on c.userid=t.userid  left    join [dbo].[tblAddress] a on c.contactid=a.addressid  and t.CompanyName=a.CompanyName
                                              left    join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                                    left    join  [dbo].[tblState] s on s.stateid=a.stateid
													left    join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
													left join [dbo].[Users] u on c.userid=u.userid
													left join  [dbo].[tblcompanyusers] u1 on u1.userid=c.userid
											        left join  [dbo].[tblcompany] p on u1.companyid=p.companyid
													where isnull(p.AccountNumber,'') not in (Select [Accountcode] from  [EzyFreight].[dbo].[tblCompanyTestUser]) and isinternational=1 
													and convert(date,c.createddatetime)>=@Date
													and c.isprocessed=1 and a.userid is not null
		
		group by      convert(date,c.createddatetime),
                       isnull(a.companyname,''),
                        a.FirstName+' '+a.LastNAme ,
                        a.address1,
                        a.address2 ,
			            a.email ,
			            a.phone ,
			            a.suburb ,
			            a.Postcode ,
			            isnull(s.StateCode,isnull(a.statename,'Unknown')),
			            case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end


union

Select convert(date,c.createddatetime) as Date,
       isnull(a.companyname,'') as BusinessName,
       a.FirstName+' '+a.LastNAme as SenderName,
       a.address1 as Senderaddress1,
       a.address2 as SenderAddress2,
			a.email as Senderemail,
			a.phone as SenderPhone,
			a.suburb as SenderSuburb,
			a.Postcode as SenderPostcode,
			isnull(s.StateCode,isnull(a.statename,'Unknown')) as SenderState,
			case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end as SenderCountry,
			count(*)

from #temp1 t 
join  [dbo].[tblAddress] a on t.companyname=a.companyname 
              join  tblconsignment c   on   c.contactid=a.addressid  and (c.userid=-1 or c.userid is null)
			                                 -- left    join [dbo].[tblAddress] a on c.pickupid=a.addressid 
                                              left    join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid  
                                                    left    join  [dbo].[tblState] s on s.stateid=a.stateid
													left    join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
													left join [dbo].[Users] u on c.userid=u.userid
													left join  [dbo].[tblcompanyusers] u1 on u1.userid=c.userid
											        left join  [dbo].[tblcompany] p on u1.companyid=p.companyid
													where isnull(p.AccountNumber,'') not in (Select Accountcode from tblCompanyTestUser) and isinternational=1 and convert(date,c.createddatetime)>=@Date
													and c.isprocessed=1
		
		group by      convert(date,c.createddatetime),
                       isnull(a.companyname,''),
                        a.FirstName+' '+a.LastNAme ,
                        a.address1,
                        a.address2 ,
			            a.email ,
			            a.phone ,
			            a.suburb ,
			            a.Postcode ,
			            isnull(s.StateCode,isnull(a.statename,'Unknown')),
			            case when isnull(a.countrycode,a.country) is null then 'Australia' else isnull(a.country,a.countrycode) end
end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptInternationalRegularshippers]
	TO [ReportUser]
GO
