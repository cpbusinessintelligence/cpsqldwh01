SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE procedure [dbo].[sp_RptNewWebAccountCustomers](@StartDate date,@EndDate Date) as
begin


     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptNewWebAccountCustomers]
    --' ---------------------------
    --' Purpose: sp_RptNewWebAccountCustomers-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 17 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 17/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================
	
SELECT  c.[CompanyID]
      ,isnull([AccountNumber],'Unknown') as AccountNumber
      ,c.[CompanyName]
      ,[ParentAccountNo]
      ,[ABN]
	  ,a.address1 as Address1
	  ,a.address2 as Address2
	  ,a.suburb as Suburb
	  ,a.Postcode as Postcode
	  ,isnull(s.statecode,a.statename) as State
	  ,a.phone as Phone 
	      ,[ParentCompanyName]
      ,[BilingAccountNo]
      ,[IsProntoExtracted]
      ,[RateCardCategory]
      ,[IntRateCardCategory]
      ,[SameBillingAddress]
      ,[BillingFname]
      ,[BillingLname]
      ,[BllingEmail]
      ,[BillingPhone]
      ,[BillingAddress]
      ,[CreditLimit]
	  ,case when isnull(IsExistingCustomer,0)=1 then 'Existing Customer' else 'New Customer' end as CustomerType
      ,c.[CreatedDateTime]
 FROM [EzyFreight].[dbo].[tblCompany] c left join [EzyFreight].[dbo].[tblAddress] a on a.addressid=c.addressid1
                                                    left join [EzyFreight].[dbo].[tblstate] s on s.StateID=a.stateid
                                       left join [EzyFreight].[dbo].[tblCompanyUsers] cu on c.companyid=cu.companyid
									   where convert(Date,c.createddatetime) between @StartDate and @EndDate
									   and  isnull(c.Accountnumber,'') not in (Select [Accountcode] from  [EzyFreight].[dbo].[tblCompanyTestUser])
									   --and accountnumber is not null
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptNewWebAccountCustomers]
	TO [ReportUser]
GO
