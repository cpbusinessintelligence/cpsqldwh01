SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-09-30
-- Description:	
-- =============================================

CREATE PROCEDURE sp_RptRecurringOneOffs
@FromDate Date = NULL
,@ToDate Date = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		p.CompanyName [PickupBusinessName]
		,p.FirstName +' '+ p.LastName [PickupContactName]
		,p.Email [PickupEmail]
		,p.Phone [PickupPhone]
		,p.Address1 + ' ' + p.Address2 [PickupAddress]
		,p.Suburb + ' ' + p.PostCode [PickupSuburb]
		,d.CompanyName [DeliveryBusinessName]
		,d.FirstName +' '+ d.LastName [DeliveryContactName]
		,d.Email [DeliveryEmail]
		,d.Phone [DeliveryPhone]
		,d.Address1 + ' ' + d.Address2 [DeleiveryAddress]
		,d.Suburb + ' ' + d.PostCode [DeliverySuburb]
		,c.ConsignmentCode
		,Count(i.ItemLabelID) [ItemQty]
		--distinct c.ConsignmentCode
	FROM [EzyFreight].[dbo].[tblconsignment] c (nolock) --1377401
		Join dbo.tblItemLabel i (nolock) on c.ConsignmentID = i.ConsignmentID  --801273
		left join [dbo].[tblAddress] p on c.pickupid=p.addressid  --801273
		left join [dbo].[tblAddress] d on c.destinationid=d.addressid --8801273		
		--left join [dbo].[tblAddress] s on c.ContactID=s.addressid --8801273
	WHERE --c.CreatedDateTime > GETDATE()-365
	c.CreatedDateTime >= @FromDate
	and  c.CreatedDateTime  <=@ToDate
	GROUP BY
		p.CompanyName
		,p.FirstName +' '+ p.LastName 	
		,p.Email 
		,p.Phone
		,p.Address1 + ' ' + p.Address2 
		,p.Suburb + ' ' + p.PostCode 
		,d.CompanyName 
		,d.FirstName +' '+ d.LastName
		,d.Email 
		,d.Phone 
		,d.Address1 + ' ' + d.Address2 
		,d.Suburb + ' ' + d.PostCode  
		,c.ConsignmentCode
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptRecurringOneOffs]
	TO [ReportUser]
GO
