SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


Create Procedure [dbo].[sp_RptRedeliveryReportbyBranch](@Branch varchar(50),@StartDate date,@EndDate date) as
begin
  --'=====================================================================
    --' CP -Stored Procedure -[sp_RptContactusReport]
    --' ---------------------------
    --' Purpose: Website Dashboard Summary-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================
SELECT [CardReferenceNumber]
      ,[LableNumber]
      ,[ConsignmentCode]
      ,[Branch]
      ,case when Branch='Adelaide' then 'SA' when branch='Melbourne' then 'VIC' when branch='Brisbane' then 'QLD' when branch='goldcoast' then 'QLD' when branch='sydney' then 'NSW' when branch='Perth' then 'WA' else branch end as state
      ,[AttemptedRedeliveryTime]
      ,[SenderName]
      ,[NumberOfItem]
      ,[DestinationName]
      ,[DestinationAddress]
      ,[DestinationSuburb]
      ,[DestinationPostCode]
      ,[SelectedETADate]
      ,[PreferDeliverTimeSlot]
      ,[PreferDeliverTime]
      ,[CreatedDateTime]

      ,[IsProcessed]
      ,[Firstname]
      ,[Lastname]
      ,[Email]
      ,[PhoneNumber]
      ,[SPInstruction]
  FROM [EzyFreight].[dbo].[tblRedelivery]
  where isprocessed = 1 and Branch=@Branch and convert(date,createddatetime) between @Startdate and @EndDate
end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptRedeliveryReportbyBranch]
	TO [ReportUser]
GO
