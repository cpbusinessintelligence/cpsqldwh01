SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_RptTotalEnquiries] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -[sp_RptTotalEnquiries]
    --' ---------------------------
    --' Purpose: Total Enquiries-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

SELECT [Subject],
       StateCode,
	   CreatedDate,
	   count(*) as CustomerEnquiries
	   --into #temp
  FROM [EzyFreight].[dbo].[tblcontactus] c left join [dbo].[tblState] s on s.stateid=c.stateid where convert(date,createddate)=convert(date,getdate())
  group by [Subject],
           StateCode,
	       CreatedDate
  order by 1,2,3 asc




end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptTotalEnquiries]
	TO [ReportUser]
GO
