SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure sp_RptUserDetailsReport (@UserName varchar(200),@FirstName  varchar(200),@LastName varchar(200),@Email varchar(200)) as 
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptUserDetailsReport
    --' ---------------------------
    --' Purpose: sp_RptUserDetailsReport-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 20 Feb 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 20/02/2016    AB      1.00    Created the procedure                             --AB20160220

    --'=====================================================================


select Username,
       FirstName,
	   LastName,
	   Email,
	   case when Authorised=1 then 'Authorised' else 'UnAuthorised' end as Authority 
	   from [cpsqlweb01].[CPPLWeb_8_3].[dbo].[users] u left join [cpsqlweb01].[CPPLWeb_8_3].[dbo].[userportals] up on u.userid=up.userid
where (username like '%'+@UserName+'%' or isnull(@username,'')='') and (FirstName like '%'+@FirstName+'%' or isnull(@FirstName,'')='') and (LastName like '%'+@LastName+'%' or isnull(@LastName,'')='') and (Email like '%'+@Email+'%' or isnull(@Email,'')='')
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptUserDetailsReport]
	TO [ReportUser]
GO
