SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 
 
 Create procedure sp_RptWebsiteAccountCustomersActualRevenue(@StartDate date,@EndDate date) as
 begin

    --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWebsiteAccountCustomersActualRevenue] 
    --' ---------------------------
    --' Purpose: sp_RptWebsiteAccountCustomersActualRevenue-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/01/2017    AB      1.00    Created the procedure                             --AB20170103

    --'=====================================================================
 
 select convert(date,c.createddatetime) as CreatedDate,
        c.consignmentcode,
		case when isnull(clientcode,'')='CPAPI' then 'API' else 'Website'  end as RevenueType,
		p.AccountNumber,
		p.companyname,
		pb.AccountingDate,
		pb.BilledFreightCharge,
		pb.BilledFuelSurcharge,
		pb.billedtransportcharge
 
 
 
 FROM [EzyFreight].[dbo].[tblConsignment]  c  join  [dbo].[tblcompanyusers] u on u.userid=c.userid
					                               join  [dbo].[tblcompany] p on u.companyid=p.companyid
												  left join Pronto.dbo.ProntoBilling pb on pb.consignmentreference=c.consignmentcode
												   where isnull(c.isaccountcustomer,0)=1 
  and convert(date,c.createddatetime) between '2016-04-01' and '2017-02-26' and isnull(p.AccountNumber,'') not in (Select [Accountcode] from  [dbo].[tblCompanyTestUser])
   and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
   order by convert(date,c.createddatetime)


   end
GO
