SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_RptWebsiteDashboardSummary] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWebsiteDashboard]
    --' ---------------------------
    --' Purpose: Website Dashboard Summary-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================

	--drop table WebsiteActivityLog
	--Create table WebsiteActivityLog(Activity varchar(200),Count int,sortorder int)

	Truncate table WebsiteActivityLog

	Insert into WebsiteActivityLog
	Select 'Prepaid CPN Request',count(*),1 from [EzyFreight].[dbo].[tblBooking] c where pickupname  not like '%Ezysend%' and isprocessed=1 and convert(date,c.createddatetime)=convert(date,getdate())
	---createddatetime<=dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())


	Insert into WebsiteActivityLog
	Select 'Total Customers*', count(*),5 as CustomersOnshoppingCart FROM [EzyFreight].[dbo].[tblconsignmentstaging] c where  convert(date,c.createddatetime)=convert(date,getdate()) 
	--createddatetime<=dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())

	
	Insert into WebsiteActivityLog
	Select 'Traded Volume', count(distinct c.consignmentid),3 as CustomersTraded
	  -- sum(GrossTotal) as GrossTotal,
	   --sum(GST) as GST,
	 -- sum(NetTotal) as NetTotal
      FROM [EzyFreight].[dbo].[tblconsignment] c where  convert(date,c.createddatetime)=convert(date,getdate()) and c.isprocessed=1 
	  
	  --left join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceNo=c.consignmentid 
	  --c.createddatetime<=dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())


	  Insert into WebsiteActivityLog
	Select 'Lost Customers', 1 as LostCustomers,4
	
	
	--,4 FROM [EzyFreight].[dbo].[tblconsignmentstaging] c left join cpsqlweb01. [EzyFreight].[dbo].[tblconsignment] c1  on c.consignmentid=c1.consignmentid
	-- where  convert(date,c.createddatetime)=CONVERT(date, DATEADD(day, - 1, GETDATE()))  and (c1.consignmentid is null)
	
	
	--where   convert(date,c.createddatetime)=CONVERT(date, DATEADD(day, - 1, GETDATE()))  and isprocessed=0
	--createddatetime<=dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate()) and isprocessed=0

	  Insert into WebsiteActivityLog
	Select 'Customer Enquiries', count(*) as CustomerEnquiries,2 FROM [EzyFreight].[dbo].[tblcontactus] c left join [dbo].[tblState] s on s.stateid=c.stateid where convert(date,createddate)=convert(date,getdate())

	Update WebsiteActivityLog set Count=(select count from WebsiteActivityLog where activity='Total Customers*')-(select count from WebsiteActivityLog where activity='Traded Volume')
	where activity='Lost Customers' 

	Select * from  WebsiteActivityLog order by sortorder asc
	
	end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWebsiteDashboardSummary]
	TO [ReportUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[sp_RptWebsiteDashboardSummary]
	TO [ReportUser]
GO
