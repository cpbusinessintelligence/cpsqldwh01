SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_RptWebsiteDashboardSummarySalesByProduct] as
begin
  --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWebsiteDashboard]
    --' ---------------------------
    --' Purpose: Website Dashboard Summary-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 30 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 30/07/2015    AB      1.00    Created the procedure                             --AB20150730

    --'=====================================================================


SELECT       case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'ACE Interstate' when c.RateCardID in ('SDC','PDC') then 'ACE Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Off Peak' else  RateCardDescription end AS ServiceType, 
             COUNT(DISTINCT c.ConsignmentID) AS JobsBooked, 
             SUM(s2.GrossTotal) AS GrossTotal, 
			 SUM(s2.GST) AS GST, 
			 SUM(s2.NetTotal) AS NetTotal
FROM            tblConsignment AS c LEFT OUTER JOIN
                         tblAddress AS a ON c.PickupID = a.AddressID LEFT OUTER JOIN
                         tblAddress AS a1 ON c.DestinationID = a1.AddressID LEFT OUTER JOIN
                         tblState AS s ON s.StateID = a.StateID LEFT OUTER JOIN
                         tblState AS s1 ON s1.StateID = a1.StateID LEFT OUTER JOIN
                         tblSalesOrder AS s2 ON s2.ReferenceNo = c.ConsignmentID  left join 
						 [cpsqlops01].[couponcalculator].[dbo].[Ratecard] r on r.ratecardcode=c.ratecardid
						 left join [dbo].[tblCompanyUsers] cu on cu.UserID=c.userid
                                              left join [dbo].[tblCompany] tc on tc.companyid=cu.companyid
WHERE        (CONVERT(date, c.CreatedDateTime) = CONVERT(date,  GETDATE()))  and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
             and isnull(tc.accountnumber,'') not in (Select [Accountcode] from  [EzyFreight].[dbo].[tblCompanyTestUser])
GROUP BY case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'ACE Interstate' when c.RateCardID in ('SDC','PDC') then 'ACE Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Off Peak' else RateCardDescription end
ORDER BY ServiceType

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptWebsiteDashboardSummarySalesByProduct]
	TO [ReportUser]
GO
