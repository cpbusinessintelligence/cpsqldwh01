SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_RptWebsiteMonthlyAdhocRevenueSummary_SS04072018](@Month int) as
begin

  --'=====================================================================
    --' CP -Stored Procedure -[sp_RptWebsiteMonthlyAdhocRevenueSummary]
    --' ---------------------------
    --' Purpose: [sp_RptWebsiteMonthlyAdhocRevenueSummary]-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 31 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 31/08/2015    AB      1.00    Created the procedure                             --AB20150831

    --'=====================================================================
SELECT distinct 
      convert(date,c.createddatetime) as Date,
       --isnull(s.StateCode,isnull(a.statename,'Unknown')) as Statecode,
       c.Consignmentid,
	   c.Consignmentcode,
	   c.NoOfItems,
	   p.prizezone as RevenueOriginZone,
	   p1.prizezone as RevenueDestinationZone,
	   z.warehouse,
	   isnull(s.StateCode,isnull(a.statename,'Unknown')) as OriginState,
	    isnull(s1.StateCode,isnull(a1.statename,'Unknown')) as DestinationState,
	   case when isinternational=1 and isnull(s.StateCode,isnull(a.statename,'Unknown')) in ('ACT','NSW','NT','QLD','TAS','WA','SA','VIC') then 'INTERNATIONAL OUTBOUND' 
	        when isinternational=1 and isnull(s1.StateCode,isnull(a1.statename,'Unknown')) in ('ACT','NSW','NT','QLD','TAS','WA','SA','VIC') then 'INTERNATIONAL INBOUND' else '' end as NetworkCategory,
	   'Parcels' as ServiceCategory,
       case when c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end as ServiceArea,
      case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else RateCardDescription end  as ServiceType,
	   convert(decimal(12,3),case when (case when isnull(c.TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(c.TotalWeight,0) end)> (case when isnull(c.TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(c.TotalVolume,0) end) then (case when isnull(c.TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(c.TotalWeight,0) end) else (case when isnull(c.TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(c.TotalVolume,0) end) end) as BillableWeight,
	   convert(decimal(12,2),0) as FreightCharge,
	   convert(decimal(12,2),0) as FuelCharge,
	   convert(decimal(12,2),0) as ReCharge,
	   convert(decimal(12,2),0) as CrCardCharge,
	   convert(decimal(12,2),0) as AdminFee,
	   convert(decimal(12,2),0) as Insurance,
	   sum(s2.GST) as GST,
	   count(distinct consignmentid) as Consignmentcount,
	   isnull(c.Netsubtotal,0) as Declaredvalue,
	   convert(varchar(100),'') as Invoice1,
	   convert(varchar(100),'') as Invoice2
        into #temp
       FROM [EzyFreight].[dbo].[tblconsignment] c left join [dbo].[tblAddress] a on c.pickupid=a.addressid
	                                              left join [dbo].[tblAddress] a1 on c.destinationid=a1.addressid   
                                                  left join  [dbo].[tblState] s on s.stateid=a.stateid
												  left join  [dbo].[tblState] s1 on s1.stateid=a1.stateid
												  left join [DWH].[dbo].[Postcodes] p on p.postcode=a.postcode and p.suburb=a.suburb
												  left join [DWH].[dbo].[Postcodes] p1 on p1.postcode=a1.postcode and p1.suburb=a1.suburb
	                                             left join [EzyFreight].[dbo].[tblsalesOrder] s2 on s2.referenceNo=c.consignmentid 
												 left join [cpsqlops01].[couponcalculator].[dbo].[Ratecard] r on r.ratecardcode=c.ratecardid
												 left join [DWH].[dbo].[ZonetoWarehousemap] z on z.zone= Case when isnull(s.StateCode,isnull(a.statename,'Unknown')) in ('ACT','NSW','NT','QLD','TAS','WA','SA','VIC') then p.prizezone else p1.prizezone end
	where  year(convert(date,c.createddatetime))*100+month(convert(date,c.createddatetime))=@Month and isnull(isaccountcustomer,0)<>1
	and  isnull(isaccountcustomer,0)<>1 
	and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
	group by
	convert(date,c.createddatetime),
	 c.Consignmentid,
	   c.Consignmentcode,
	   c.NoOfItems,
	   p.prizezone,
	   p1.prizezone ,
	   z.warehouse,
	   isnull(s.StateCode,isnull(a.statename,'Unknown')),
	    isnull(s1.StateCode,isnull(a1.statename,'Unknown')),
	   case when isinternational=1 and isnull(s.StateCode,isnull(a.statename,'Unknown')) in ('ACT','NSW','NT','QLD','TAS','WA','SA','VIC') then 'INTERNATIONAL OUTBOUND' 
	        when isinternational=1 and isnull(s1.StateCode,isnull(a1.statename,'Unknown')) in ('ACT','NSW','NT','QLD','TAS','WA','SA','VIC') then 'INTERNATIONAL INBOUND' else '' end ,
       case when  c.RateCardID like 'EXP%' or c.RateCardID like 'SAV%' then 'International' else 'Domestic' end ,
        case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'Australian City Express Interstate' when c.RateCardID in ('SDC','PDC') then 'Australian City Express Same Day' when c.RateCardID in ('REC','PEC') then 'Domestic Saver' when  c.RateCardID like 'EXP%' then 'International Priority' when c.RateCardID like 'SAV%' then 'International Saver' when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else RateCardDescription  end,
	   convert(decimal(12,3),case when (case when isnull(c.TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(c.TotalWeight,0) end)> (case when isnull(c.TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(c.TotalVolume,0) end) then (case when isnull(c.TotalWeight,0)<isnull(TotalMeasureWeight,0) then isnull(TotalMeasureWeight,0) else isnull(c.TotalWeight,0) end) else (case when isnull(c.TotalVolume,0)<isnull(TotalMeasureVolume,0) then isnull(TotalMeasureVolume,0) else isnull(c.TotalVolume,0) end) end),
	   isnull(c.Netsubtotal,0)
	--and isinternational=0
	--between 
--'2015-07-31' and '2015-08-09'
	   --<dateadd(minute,-(datepart(minute,getdate())+1),getdate()) and convert(date,c.createddatetime)=convert(date,getdate())
 -- group by 
 --         --isnull(s.StateCode,isnull(a.statename,'Unknown')) ,
 --         consignmentId,
 --          case when c.RateCardID in ('EXP','SAV') then 'International' else 'Domestic' end,
 --           case when c.RateCardID in ('SDC','PDC') then 'Same Day' when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'ACE Interstate' when c.RateCardID in ('REC','PEC') then 'Domestic Saver/RoadExpress' when  c.RateCardID = 'EXP' then 'International Express' when c.RateCardID = 'SAV' then 'International Saver' else '' end ,
 --          c.RateCardID,
 --     --     isnull(c.customerRefNo,'') ,
 --         convert(date,c.createddatetime),
 --         datepart(hour,c.Createddatetime)
 ---- order by 1,2,3,4 asc

   Update #temp set warehouse= z.warehouse from #temp left join [DWH].[dbo].[ZonetoWarehousemap] z on z.zone=  RevenueOriginZone  where #temp.warehouse='' or #temp.warehouse is null
  
  Update #temp set warehouse= z.warehouse from #temp left join [DWH].[dbo].[ZonetoWarehousemap] z on z.zone=  RevenueDestinationZone  where #temp.warehouse='' or #temp.warehouse is null


    Update #temp set FreightCharge=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description not in ('Reweight charge','Credit Card Surcharge','Insurance','Admin Fee')

     Update #temp set Recharge=s2.FreightCharge from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
     where description='Reweight charge'

	 Update #temp set Fuelcharge=isnull(s2.Fuelcharge,0) from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
    where description not in ('Reweight charge','Credit Card Surcharge','Insurance','Admin Fee')

  Update #temp set CrCardCharge=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description='Credit Card Surcharge'

  
  Update #temp set Insurance=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description='Insurance'

    
  Update #temp set AdminFee=s2.FreightCharge   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description='Admin Fee'


    Update #temp set Invoice1=s.InvoiceNo  from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description not  in ('Admin Fee','ReweightCharge')


    Update #temp set Invoice2=s.InvoiceNo   from #temp t join [EzyFreight].[dbo].[tblsalesOrder] s on s.referenceno=t.consignmentid join [EzyFreight].[dbo].[tblsalesOrderdetail] s2 on s2.salesorderid=s.salesorderid
  where description in ('Admin Fee','ReweightCharge')

   Update #temp set NetworkCategory=CASE WHEN (LTRIM(RTRIM(pb.RevenueOriginZone)) = LTRIM(RTRIM(pb.RevenueDestinationZone))
							AND LTRIM(RTRIM(pb.RevenueOriginZone)) IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
													'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL'))
						THEN 'METRO'
					ELSE
						CASE WHEN (pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
														'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND ((ISNULL(Originstate, 'XX') = ISNULL(DestinationState, 'ZZ'))
											OR (ISNULL(Originstate, 'XX') IN ('ACT','NSW')
												AND ISNULL(DestinationState, 'ZZ') IN ('ACT','NSW'))))
							THEN 'REGIONAL'
						ELSE
							CASE WHEN ((ISNULL(Originstate, 'XX') = ISNULL(DestinationState, 'ZZ'))
											OR (ISNULL(Originstate, '') IN ('ACT','NSW')
												AND ISNULL(DestinationState, 'ZZ') IN ('ACT','NSW')))
								THEN 'INTRASTATE'
							ELSE
								'INTERSTATE'
							END
						END
					END
					from #temp pb where networkcategory=''

--Select * from #temp

  Select --Statecode,
         Date,
         ServiceArea,
         ServiceType,
		 Case when Originstate in ('ACT','NSW','NT','QLD','TAS','WA','SA','VIC') then OriginState else Destinationstate end as State,
		 NetworkCategory,
		 ServiceCategory,
		 Warehouse,
		 sum(NoOfItems) as TotalItems,
		 sum(Billableweight) as Billableweight,
		 sum(Consignmentcount) as Consignmentcount,
		 sum(Freightcharge) as FreightCharge,
		 sum(Fuelcharge) as Fuelcharge,
		 sum(Recharge) as ReCharge,
		 sum(AdminFee) as AdminFee,
		 sum(Insurance) as Insurance,
		 sum(CrcardCharge) as CrcardCharge,
		 sum(GST) as GST,
		 sum(Declaredvalue) as DeclaredValue
		 from #temp
		 group by Date,
		          --Statecode,
		          ServiceArea,
                  ServiceType, 
				  Case when Originstate in ('ACT','NSW','NT','QLD','TAS','WA','SA','VIC') then OriginState else Destinationstate end,
				  NetworkCategory,
		          ServiceCategory,
				  Warehouse
				  order by Date
end

GO
