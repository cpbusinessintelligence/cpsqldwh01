SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptWebsiteRefundReport](@StartDate date,@EndDate date)
as begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_RptWebsiteRefundReport
    --' ---------------------------
    --' Purpose: WebsiteRefundReport-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 20 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 20/08/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Select convert(date,r.CreatedDateTime) as Date,
       c.consignmentid,
       c.ConsignmentCode,
	   Reason,
	   Amount as RefundAmount,
	   convert(decimal(12,2),sum(s.NetTotal)) as SaleAmount,
	   convert(varchar(100),'') as RefundType,
       PaymentRefNo,
	  -- u.FirstName+' '+u.LastName as [User],
	  u.DisplayName as [User],
	   u.username as UserName,
	   case when u.DisplayName='Janay Tafili' then 'QLD'
	        when u.DisplayName='SHARON WALTON' then 'QLD'
			when u.DisplayName='Harold Hill' then 'NSW'
			when u.DisplayName='sandra sidra' then 'NSW'
			when u.DisplayName='anita vella' then 'NSW'
			when u.DisplayName='Dharshi Bubb' then 'VIC'
			when u.DisplayName='Rita Ilai' then 'VIC'
			when u.DisplayName='Lina Versace' then 'SA'
			when u.DisplayName='Lisa Tedstone' then 'SA'
			else 'Unknown' end as State,

	   case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'ACE Interstate' 
	        when c.RateCardID in ('SDC','PDC') then 'ACE Same Day' 
			when c.RateCardID in ('REC','PEC') then 'Domestic Saver' 
			when  c.RateCardID like 'EXP%' then 'International Priority' 
			when c.RateCardID like 'SAV%' then 'International Saver' 
			when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  
			when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  
			when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' 
			when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else c.RateCardID end as ProductType
	   into #temp

from [EzyFreight].[dbo].[tblRefund] r join [EzyFreight].[dbo].[tblconsignment] c on r.ConsignmentID=c.ConsignmentID
                                      join [EzyFreight].[dbo].[tblsalesOrder] s on s.ReferenceNo=c.ConsignmentID
                                      join CPSQLWEB02.CPPLWEB_9_2_Admin.dbo.Users u on u.UserID=r.CreatedBy
where isprocess=1 
and convert(date,r.createddatetime) between @StartDate and @EndDate
and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
group by convert(date,r.CreatedDateTime),
       c.consignmentid,
       c.ConsignmentCode,
	   Reason,
	   Amount,
       PaymentRefNo,
	   u.DisplayName,
	   u.username,
	   case when u.DisplayName='Janay Tafili' then 'QLD'
	        when u.DisplayName='SHARON WALTON' then 'QLD'
			when u.DisplayName='Harold Hill' then 'NSW'
			when u.DisplayName='sandra sidra' then 'NSW'
			when u.DisplayName='anita vella' then 'NSW'
			when u.DisplayName='Dharshi Bubb' then 'VIC'
			when u.DisplayName='Rita Ilai' then 'VIC'
			when u.DisplayName='Lina Versace' then 'SA'
			when u.DisplayName='Lisa Tedstone' then 'SA'
			else 'Unknown' end,
			 case when c.RateCardID in ('CE3','CE5','PE3','PE5') then 'ACE Interstate' 
	        when c.RateCardID in ('SDC','PDC') then 'ACE Same Day' 
			when c.RateCardID in ('REC','PEC') then 'Domestic Saver' 
			when  c.RateCardID like 'EXP%' then 'International Priority' 
			when c.RateCardID like 'SAV%' then 'International Saver' 
			when c.RateCardID in ('SGE','GEC') then 'Gold Domestic'  
			when c.RateCardID in ('DSL','DAL','DSH','DAH','DSM','DAM') then 'Domestic Priority'  
			when c.RateCardID in ('ASL','ACL','ASH','ACH','ASM','ACM') then 'Domestic Air Consolidator' 
			when c.RateCardID in ('RSL','RCL','RSH','RCH','RSM','RCM') then 'Domestic Off Peak' else c.RateCardID end

Update #temp set RefundType=case when RefundAmount-SaleAmount=0 then 'Full Refund' else 'Partial Refund' end

select * from #temp

end

--select * from  [EzyFreight].[dbo].[tblRefund] 

--select * into tblRefund_backupHB07082018 from tblRefund

--select * from [dbo].[tblRefund_Archive] where CreatedDateTime >= '2017-12-01 08:56:11.410' and createddatetime <='2018-01-31 15:55:12.113'
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWebsiteRefundReport]
	TO [ReportUser]
GO
