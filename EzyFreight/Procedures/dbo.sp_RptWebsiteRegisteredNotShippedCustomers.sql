SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptWebsiteRegisteredNotShippedCustomers] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptWebsiteRegisteredNotShippedCustomers]
    --' ---------------------------
    --' Purpose: sp_RptWebsiteRegisteredNotShippedCustomers-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 27 Jan 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 27/01/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

Select distinct convert(date,a.CreatedDateTime) as Date
      ,case when p.companyid is null and ( u.userid is not null and u.userid<>-1)   then 'Adhoc Registered' 
	        when p.companyid is null  and ( u.userid is  null or u.userid=-1) then 'Adhoc Non-Registered' 
	        when p.companyid is not null and (p.Branch is not NULL and p.existingaccountnumber is not NULL) then 'Prepaid Account Registered'
			when p.companyid is not null and isnumeric(accountnumber)=1 then 'EDI SingleSignOn'
			when p.companyid is not null and isnumeric(accountnumber)<>1 and not (p.Branch is not NULL and p.existingaccountnumber is not NULL) then 'New Account Registered'
			else 'Account Other' end as [UserIDCategory]
      ,u.userid
      ,isnull(a.[FirstName],'') as FirstName
      ,isnull(a.[LastName],'') as LastName
      ,isnull(a.[CompanyName],'') as CompanyName
      ,isnull(a.[Email],'') as Email
      ,isnull(a.[Address1],'') as Address1
      ,isnull(a.[Address2],'') as Address2
      ,isnull(a.[Suburb],'') as Suburb
      ,isnull(a.[StateName],s.statecode) as State
     -- ,a.[StateID],'') as 
      ,isnull(a.[PostCode],'') as PostCode
      ,isnull(a.[Phone],'') as Phone
      ,isnull(a.[Mobile],'') as Mobile
      ,case when isnull(a.[StateName],s.statecode) in ('New South Wales','Queensland','Victoria','South Australia','Australian Capital Territory','Western Australia','Tasmania','NSW','QLD','SA','TAS','VIC','WA','ACT','NT') then 'AUSTRALIA' else isnull(a.[Country],'') end as Country
      ,case when isnull(a.[StateName],s.statecode) in ('New South Wales','Queensland','Victoria','South Australia','Australian Capital Territory','Western Australia','Tasmania','NSW','QLD','SA','TAS','VIC','WA','ACT','NT') then 'AU' else isnull(a.[Countrycode],'') end as Countrycode
  --  into #temp  
 from users u left join tblconsignment c on c.userid=u.userid
             left join tbladdress a on a.userid=u.userid
			 left join tblstate s on s.stateid=isnull(a.stateid,'')
			 
             left join ezyfreight.[dbo].[tblCompanyUsers] cu on cu.userid=u.userid
			 left join ezyfreight.dbo.tblcompany p on p.companyid=cu.CompanyID
			 where c.userid is null and convert(date,a.CreatedDateTime) is not null and a.firstname is not null
			 and a.IsRegisterAddress=1 and a.address1 <>'123 Middle Road' 
			 and convert(date,a.CreatedDateTime)>'2015-07-31'  and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
			 order by convert(date,a.CreatedDateTime)


--update #temp set [UserIDCategory]=case when 
--from #temp u join ezyfreight.[dbo].[tblCompanyUsers] cu on cu.userid=u.userid
--		   join ezyfreight.dbo.tblcompany p on p.companyid=cu.CompanyID



end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWebsiteRegisteredNotShippedCustomers]
	TO [ReportUser]
GO
