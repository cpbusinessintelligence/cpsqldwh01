SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptWebsiteSalesbyRep](@StartDate date,@EndDate date) as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_RptWebsiteSalesbyRep
    --' ---------------------------
    --' Purpose: sp_RptWebsiteSalesbyRep-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 07 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 15/12/2015    AB      1.00    Created the procedure                             --AB20151215

    --'=====================================================================


SELECT   case when isinternational=1 then convert(varchar(10),case when isnumeric(accountnumber)=1 then 'WI'+convert(varchar(8),substring(Accountnumber,2,len(Accountnumber))) when Accountnumber like 'WD%' then replace(Accountnumber,'WD','WI') else '' end) else Accountnumber end as Accountcode
      ,[BillTo]
	  ,c.AccountNumber
      ,[Shortname]
      ,[Warehouse]
      ,[TermDisc]
      ,[Territory]
      ,[RepCode]
      ,[IndustryCode]
      ,[CustType]
      ,[MarketingFlag]
      ,[AverageDaysToPay]
      ,[IndustrySubGroup]
      ,[Postcode]
      ,[Locality]
      ,[OriginalRepCode]
      ,[LastSale]
      ,p.ABN
      ,[RepName]
      ,[CrmAccount]
      ,[CrmParent]
      ,[CrmType]
      ,[CrmRegion]
      ,[OriginalRepName]
	  ,ci.consignmentcode
	  ,convert(date,ci.createddatetime) as Date
	  ,calculatedtotal
	  ,calculatedgst
	  ,isnull(isaccountcustomer,0) as isaccountcustomer
	  	  into #temp
  FROM [Pronto].[dbo].[ProntoDebtor] p join ezyfreight.dbo.tblcompany c on c.accountnumber=p.accountcode
                                       join ezyfreight.[dbo].[tblCompanyUsers] cu on cu.companyid=c.companyid
									   join ezyfreight.[dbo].[tblconsignment] ci on cu.userid=ci.userid
  where accountcode not like 'WI%' and c.AccountNumber not in (Select [Accountcode] from  [EzyFreight].[dbo].[tblCompanyTestUser])
  and convert(date,ci.createddatetime) between @StartDate and @EndDate  and (case when convert(date,ci.createddatetime)>'2016-01-31' then ci.isprocessed else 1 end)=1


Select Repcode,Repname,Accountcode,BillTo,count(*) as Count,sum(calculatedtotal) as CalculatedTotal,sum(calculatedgst) as GST
from #temp
group by  Repcode,Repname,[OriginalRepCode],[OriginalRepName],Accountcode,BillTo

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptWebsiteSalesbyRep]
	TO [ReportUser]
GO
