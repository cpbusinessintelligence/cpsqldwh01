SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptWebsiteShippedNotRegisteredcustomers] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptWebsiteShippedNotRegisteredcustomers]
    --' ---------------------------
    --' Purpose: sp_RptWebsiteShippedNotRegisteredcustomers-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 27 Jan 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 27/01/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

Select distinct convert(date,c.CreatedDateTime) as Date
      ,'Adhoc Non-Registered' as  [UserIDCategory]
     -- ,u.userid
      ,isnull(a.[FirstName],'') as FirstName
      ,isnull(a.[LastName],'') as LastName
      ,isnull(a.[CompanyName],'') as CompanyName
      ,isnull(a.[Email],'') as Email
      ,isnull(a.[Address1],'') as Address1
      ,isnull(a.[Address2],'') as Address2
      ,isnull(a.[Suburb],'') as Suburb
      ,isnull(isnull(a.[StateName],s.statecode),'') as State
     -- ,a.[StateID],'') as 
      ,isnull(a.[PostCode],'') as PostCode
      ,isnull(a.[Phone],'') as Phone
      ,isnull(a.[Mobile],'') as Mobile
      ,case when isnull(a.[StateName],s.statecode) in ('New South Wales','Queensland','Victoria','South Australia','Australian Capital Territory','Western Australia','Tasmania','NSW','QLD','SA','TAS','VIC','WA','ACT','NT') then 'AUSTRALIA' else isnull(a.[Country],'') end as Country
      ,case when isnull(a.[StateName],s.statecode) in ('New South Wales','Queensland','Victoria','South Australia','Australian Capital Territory','Western Australia','Tasmania','NSW','QLD','SA','TAS','VIC','WA','ACT','NT') then 'AU' else isnull(a.[Countrycode],'') end as Countrycode
      
     -- ,a.[CreatedBy]
from  tblconsignment c  
             --left join users u on c.userid=u.userid
             left join tbladdress a on a.addressid=c.contactid
			 left join tblstate s on s.stateid=isnull(a.stateid,'')
			-- left join ezyfreight.[dbo].[tblCompanyUsers] cu on cu.userid=u.userid
			-- left join ezyfreight.dbo.tblcompany p on p.companyid=cu.CompanyID
			 where  c.userid=-1 or c.userid is null and convert(date,a.CreatedDateTime) is not null and a.firstname is not null
			 and convert(date,c.CreatedDateTime)>'2015-07-31' and (case when convert(date,c.createddatetime)>'2016-01-31' then c.isprocessed else 1 end)=1
			  order by convert(date,c.CreatedDateTime)


end


GO
GRANT EXECUTE
	ON [dbo].[sp_RptWebsiteShippedNotRegisteredcustomers]
	TO [ReportUser]
GO
