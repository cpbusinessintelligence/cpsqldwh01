SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_boomerang_pickupRPT](@StartDate Date,@EndDate Date)
AS
BEGIN
	SELECT 
com.CompanyName [Customer],
con.cd_special_instructions [Additional Instrunctions],
c.CreatedDateTime [Booking Date],
c.ConsignmentCode [Con Number],
b.BookingRefNo [Cosmos Job Number],
case RateCardID when 'RTDP' then 'Direct Pick up' 
	when 'RTRN' then 'Hubbed Drop off' 
	when 'RTHP' then 'Hubbed Drop off' end [Pick up or drop off],

ConsignmentPreferPickupDate [Booking ready date],
ConsignmentPreferPickupTime [Booking ready time],
b.ContactName [Sender contact],
a.CompanyName [Drop point name],
c.RateCardID [Service Code],
isnull(a.Address1, '') + isnull(a.Address2, '') [Sender Address],
a.Suburb [Sender Suburb],
a.stateName [State],
a.Phone [Sender phone],
a.Email [Sender email],
customerrefno [Customer Reference]


  FROM [EzyFreight].[dbo].[tblConsignment] c 
--  inner join tblEzyNetCustomer con on c.CustomerRefNo =con.customerid
  left join tblAddress a on c.pickupid=a.addressid
  left join tbladdress ab on c.DestinationID=ab.AddressID
  left join tblBooking b on c.ConsignmentID=b.ConsignmentID
  left join tblCompanyUsers u on u.userid=c.userid
  left join tblCompany com on com.companyid=u.companyid
  left join cpplEDI.dbo.consignment (nolock) con on con.cd_connote=c.ConsignmentCode
 --inner join Cosmos.dbo.Booking bo on bo.BookingId=b.BookingRefNo
--left  join tblEzyNetCustomer cus on cus.CustomerID=BookingCustomerId
  where RateCardID in ('RTRN','RTDP','RTHP')
  and (convert(date,c.CreatedDateTime)>=@Startdate  and convert(date,c.CreatedDateTime)<=@Enddate )
END
GO
GRANT EXECUTE
	ON [dbo].[sp_boomerang_pickupRPT]
	TO [ReportUser]
GO
