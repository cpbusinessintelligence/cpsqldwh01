SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentParam] (
		[Num]               [int] IDENTITY(1, 1) NOT NULL,
		[AgentName]         [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[URL]               [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[FtpName]           [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[UserName]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FtpPassword]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[fileExtension]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]       [datetime] NULL,
		[UpdatedDate]       [datetime] NULL
)
GO
ALTER TABLE [dbo].[AgentParam]
	ADD
	CONSTRAINT [DF__AgentPara__Creat__335CB8BA]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[AgentParam]
	ADD
	CONSTRAINT [DF__AgentPara__Updat__3450DCF3]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[AgentParam] SET (LOCK_ESCALATION = TABLE)
GO
