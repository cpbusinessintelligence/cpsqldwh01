SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignShipmentEzy2ShipResponse] (
		[CodeStatus]                 [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Message]                    [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[ShipmentNumber]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Reference]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ManifestNumber]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[URLneoPod]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[URLTrackTrace]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectionDocketNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[createddatetime]            [datetime] NULL,
		[createdby]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[consignmentid]              [int] NULL
)
GO
ALTER TABLE [dbo].[AssignShipmentEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__AssignShi__creat__3E0C23FB]
	DEFAULT (getdate()) FOR [createddatetime]
GO
ALTER TABLE [dbo].[AssignShipmentEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__AssignShi__creat__3F004834]
	DEFAULT ('Admin') FOR [createdby]
GO
ALTER TABLE [dbo].[AssignShipmentEzy2ShipResponse] SET (LOCK_ESCALATION = TABLE)
GO
