SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignShipmentNZResponse] (
		[Code]                [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Status]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Description]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[Tracking_Code]       [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[UUID]                [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[createddatetime]     [datetime] NULL,
		[createdby]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[consignmentid]       [int] NOT NULL,
		[Request]             [varchar](5000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AssignShipmentNZResponse]
	ADD
	CONSTRAINT [DF__AssignShi__creat__64C6C9C0]
	DEFAULT (getdate()) FOR [createddatetime]
GO
ALTER TABLE [dbo].[AssignShipmentNZResponse]
	ADD
	CONSTRAINT [DF__AssignShi__creat__65BAEDF9]
	DEFAULT ('Admin') FOR [createdby]
GO
ALTER TABLE [dbo].[AssignShipmentNZResponse] SET (LOCK_ESCALATION = TABLE)
GO
