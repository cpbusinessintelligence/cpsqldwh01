SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BillingStaging1] (
		[Consignmentid]     [int] NULL,
		[LabelNumber]       [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[BillingStaging1] SET (LOCK_ESCALATION = TABLE)
GO
