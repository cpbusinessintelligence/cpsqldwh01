SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BillingWeb] (
		[Consignmentid]                   [int] NOT NULL,
		[RecordType]                      [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Consignment Reference]           [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Consignment date]                [date] NULL,
		[Manifest reference]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Manifest date]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]                         [varchar](55) COLLATE Latin1_General_CI_AS NULL,
		[Account code]                    [varchar](800) COLLATE Latin1_General_CI_AS NULL,
		[Sender name]                     [varchar](101) COLLATE Latin1_General_CI_AS NULL,
		[Sender address 1]                [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[Sender address 2]                [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[Sender locality]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Sender State]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Sender postcode]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SenderCountry]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Receiver name]                   [varchar](101) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address 1]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address 2]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Receiver locality]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Receiver state]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Receiver postcode]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RecieverCountry]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer reference]              [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[Release ASN]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Return Authorisation Number]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 3]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 4]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Special instructions]            [varchar](8000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Item quantity]                   [int] NOT NULL,
		[Declared weight]                 [numeric](10, 4) NOT NULL,
		[Measured weight]                 [numeric](10, 4) NOT NULL,
		[Declared volume]                 [numeric](14, 8) NOT NULL,
		[Measured volume]                 [numeric](14, 8) NOT NULL,
		[BilledWeight]                    [numeric](7, 3) NULL,
		[Toweight]                        [numeric](7, 3) NULL,
		[RateCardId]                      [varchar](55) COLLATE Latin1_General_CI_AS NULL,
		[Price override]                  [int] NULL,
		[Insurance category]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Declared value]                  [numeric](10, 2) NULL,
		[Insurance Price Override]        [numeric](13, 5) NULL,
		[Test Flag]                       [int] NOT NULL,
		[Dangerous goods flag]            [bit] NOT NULL,
		[Release Not Before]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Release Not After]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Logistics Units]                 [int] NOT NULL,
		[IsProcessed]                     [int] NOT NULL,
		[IsDeleted]                       [int] NOT NULL,
		[HasError]                        [int] NOT NULL,
		[ErrorCoded]                      [int] NOT NULL,
		[AddWho]                          [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]                     [datetime] NOT NULL,
		[EditWho]                         [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[EditDateTime]                    [datetime] NOT NULL,
		[IsprocessedPB]                   [bit] NULL
)
GO
ALTER TABLE [dbo].[BillingWeb]
	ADD
	CONSTRAINT [DF_temp_x]
	DEFAULT ((0)) FOR [IsprocessedPB]
GO
ALTER TABLE [dbo].[BillingWeb] SET (LOCK_ESCALATION = TABLE)
GO
