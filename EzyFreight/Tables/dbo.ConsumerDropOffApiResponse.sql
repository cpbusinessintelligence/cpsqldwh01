SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConsumerDropOffApiResponse] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ResponseCode]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Request]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]         [datetime] NULL,
		[IsProcessed]         [bit] NULL
)
GO
ALTER TABLE [dbo].[ConsumerDropOffApiResponse]
	ADD
	CONSTRAINT [DefDatetime]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[ConsumerDropOffApiResponse]
	ADD
	CONSTRAINT [DF__ConsumerD__IsPro__748070A1]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[ConsumerDropOffApiResponse] SET (LOCK_ESCALATION = TABLE)
GO
