SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreateShipmentEzy2ShipResponse] (
		[Consignmentid]              [int] NULL,
		[CodeStatus]                 [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Message]                    [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[ShipmentNumber]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Reference]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ManifestNumber]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[URLneoPod]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[URLTrackTrace]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectionDocketNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]                [bit] NULL,
		[createddatetime]            [datetime] NULL,
		[createdby]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CreateShipmentEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__CreateShi__isass__41DCB4DF]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[CreateShipmentEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__CreateShi__creat__42D0D918]
	DEFAULT (getdate()) FOR [createddatetime]
GO
ALTER TABLE [dbo].[CreateShipmentEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__CreateShi__creat__43C4FD51]
	DEFAULT ('Admin') FOR [createdby]
GO
ALTER TABLE [dbo].[CreateShipmentEzy2ShipResponse] SET (LOCK_ESCALATION = TABLE)
GO
