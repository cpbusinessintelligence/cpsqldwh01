SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DHLLocationcodes] (
		[Location Code]                                                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Location Area Name]                                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Country Code]                                                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Country Name]                                                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Difference from UCT (hours) not including daylight saving]     [float] NULL,
		[Time Zone Code]                                                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Time Zone Name]                                                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DHLLocationcodes] SET (LOCK_ESCALATION = TABLE)
GO
