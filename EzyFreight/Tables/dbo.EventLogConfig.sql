SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventLogConfig] (
		[ID]                                [int] NOT NULL,
		[LogTypeKey]                        [nvarchar](35) COLLATE Latin1_General_CI_AS NULL,
		[LogTypePortalID]                   [int] NULL,
		[LoggingIsActive]                   [bit] NOT NULL,
		[KeepMostRecent]                    [int] NOT NULL,
		[EmailNotificationIsActive]         [bit] NOT NULL,
		[NotificationThreshold]             [int] NULL,
		[NotificationThresholdTime]         [int] NULL,
		[NotificationThresholdTimeType]     [int] NULL,
		[MailFromAddress]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[MailToAddress]                     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_EventLogConfig]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[EventLogConfig]
	WITH NOCHECK
	ADD CONSTRAINT [FK_EventLogConfig_EventLogTypes]
	FOREIGN KEY ([LogTypeKey]) REFERENCES [dbo].[EventLogTypes] ([LogTypeKey])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[EventLogConfig]
	NOCHECK CONSTRAINT [FK_EventLogConfig_EventLogTypes]

GO
ALTER TABLE [dbo].[EventLogConfig] SET (LOCK_ESCALATION = TABLE)
GO
