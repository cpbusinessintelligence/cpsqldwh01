SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventLogTypes] (
		[LogTypeKey]              [nvarchar](35) COLLATE Latin1_General_CI_AS NOT NULL,
		[LogTypeFriendlyName]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LogTypeDescription]      [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[LogTypeOwner]            [nvarchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[LogTypeCSSClass]         [nvarchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_EventLogTypes]
		PRIMARY KEY
		CLUSTERED
		([LogTypeKey])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[EventLogTypes] SET (LOCK_ESCALATION = TABLE)
GO
