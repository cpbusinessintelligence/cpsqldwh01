SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EzyParcelAssignShipments] (
		[code]                [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[message]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[manifestnumber]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[ShipmentNumber]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ErrorMessage]        [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EzyParcelAssignShipments]
	ADD
	CONSTRAINT [DF__EzyParcel__Creat__4EAB096A]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[EzyParcelAssignShipments]
	ADD
	CONSTRAINT [DF__EzyParcel__Creat__4F9F2DA3]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EzyParcelAssignShipments]
	ADD
	CONSTRAINT [DF__EzyParcel__Edite__509351DC]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[EzyParcelAssignShipments]
	ADD
	CONSTRAINT [DF__EzyParcel__Edite__51877615]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[EzyParcelAssignShipments] SET (LOCK_ESCALATION = TABLE)
GO
