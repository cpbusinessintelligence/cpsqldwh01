SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EzysendtoEzyTrakFilename] (
		[FileName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EzysendtoEzyTrakFilename] SET (LOCK_ESCALATION = TABLE)
GO
