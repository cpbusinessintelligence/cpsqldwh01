SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EzysendtoEzyTrakHeaderFile] (
		[RecordType]          [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[FileName]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ExtractDate]         [date] NULL,
		[ExtractTime]         [time](7) NULL,
		[Interfacesystem]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[RecordTypeDummy]     [varchar](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EzysendtoEzyTrakHeaderFile] SET (LOCK_ESCALATION = TABLE)
GO
