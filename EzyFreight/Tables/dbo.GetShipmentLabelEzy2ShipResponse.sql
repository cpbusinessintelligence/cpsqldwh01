SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GetShipmentLabelEzy2ShipResponse] (
		[ShipmentNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CodeStatus]          [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Message]             [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[createddatetime]     [datetime] NULL,
		[createdby]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[consignmentid]       [int] NULL,
		[isassigned]          [bit] NULL
)
GO
ALTER TABLE [dbo].[GetShipmentLabelEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__GetShipme__isass__209BBCA5]
	DEFAULT ((0)) FOR [isassigned]
GO
ALTER TABLE [dbo].[GetShipmentLabelEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__GetShipme__creat__218FE0DE]
	DEFAULT (getdate()) FOR [createddatetime]
GO
ALTER TABLE [dbo].[GetShipmentLabelEzy2ShipResponse]
	ADD
	CONSTRAINT [DF__GetShipme__creat__22840517]
	DEFAULT ('Admin') FOR [createdby]
GO
ALTER TABLE [dbo].[GetShipmentLabelEzy2ShipResponse] SET (LOCK_ESCALATION = TABLE)
GO
