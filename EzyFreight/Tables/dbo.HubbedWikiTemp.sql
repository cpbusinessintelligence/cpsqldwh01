SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HubbedWikiTemp] (
		[RecordType]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment Reference]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment date]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Manifest reference]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Manifest date]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Service]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Account code]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender name]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender address 1]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender address 2]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender locality]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender State]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sender postcode]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Receiver name]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address 1]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Receiver address 2]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Receiver locality]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Receiver state]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Receiver postcode]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Customer reference]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Release ASN]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Return Authorisation Number]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 1]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 2]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 3]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Customer other reference 4]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Special instructions]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Item quantity]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Declared weight]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Measured weight]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Declared volume]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Measured volume]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Price override]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Insurance category]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Declared value]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Insurance Price Override]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Test Flag]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Dangerous goods flag]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Release Not Before]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Release Not After]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Logistics Units]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[HubbedWikiTemp] SET (LOCK_ESCALATION = TABLE)
GO
