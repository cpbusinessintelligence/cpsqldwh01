SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LianaRedemptionOctober] (
		[Trans# Type]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Cons# Type]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date/Time]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Branch]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Account]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Reason]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LianaRedemptionOctober] SET (LOCK_ESCALATION = TABLE)
GO
