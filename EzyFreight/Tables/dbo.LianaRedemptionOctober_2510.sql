SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LianaRedemptionOctober_2510] (
		[Consignment]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LianaRedemptionOctober_2510] SET (LOCK_ESCALATION = TABLE)
GO
