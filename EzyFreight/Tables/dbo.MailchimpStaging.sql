SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailchimpStaging] (
		[Sno]                   [bigint] IDENTITY(1, 1) NOT NULL,
		[Addressid]             [int] NULL,
		[EmailAddress]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[APIKey]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ListId]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsRegisterAddress]     [bit] NULL,
		[IsSubscribe]           [bit] NULL,
		[Response]              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]           [bit] NULL,
		[Createdby]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]       [datetime] NULL,
		[Editedby]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]        [datetime] NULL
)
GO
ALTER TABLE [dbo].[MailchimpStaging]
	ADD
	CONSTRAINT [DF__Mailchimp__ispro__61674175]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[MailchimpStaging]
	ADD
	CONSTRAINT [DF__Mailchimp__Creat__625B65AE]
	DEFAULT ('Admin') FOR [Createdby]
GO
ALTER TABLE [dbo].[MailchimpStaging]
	ADD
	CONSTRAINT [DF__Mailchimp__Creat__634F89E7]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[MailchimpStaging]
	ADD
	CONSTRAINT [DF__Mailchimp__Edite__6443AE20]
	DEFAULT ('Admin') FOR [Editedby]
GO
ALTER TABLE [dbo].[MailchimpStaging]
	ADD
	CONSTRAINT [DF__Mailchimp__Edite__6537D259]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[MailchimpStaging] SET (LOCK_ESCALATION = TABLE)
GO
