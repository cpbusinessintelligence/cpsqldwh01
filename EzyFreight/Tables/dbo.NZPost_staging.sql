SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NZPost_staging] (
		[Code]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Desc]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNo]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentStatus]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Picked]                [datetime] NULL,
		[Delivered]             [datetime] NULL,
		[Tracking]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventDt]               [datetime] NULL,
		[EventCode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventDesc]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventLocation]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventPart]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedUtc]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[NZPost_staging] SET (LOCK_ESCALATION = TABLE)
GO
