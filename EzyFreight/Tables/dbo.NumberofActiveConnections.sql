SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NumberofActiveConnections] (
		[DBName]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NumberofConnections]     [int] NULL,
		[LoginName]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Date]                    [datetime] NULL
)
GO
ALTER TABLE [dbo].[NumberofActiveConnections] SET (LOCK_ESCALATION = TABLE)
GO
