SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentReferenceStaging] (
		[SNo]                 [int] IDENTITY(1, 1) NOT NULL,
		[Batchid]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Merchantid]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Batchdate]           [date] NULL,
		[Requestid]           [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[MerchantRefNo]       [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[TransRefNo]          [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[Paymentmethod]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Currency]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Amount]              [decimal](12, 2) NULL,
		[Transactiontype]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PaymentReferenceStaging] SET (LOCK_ESCALATION = TABLE)
GO
