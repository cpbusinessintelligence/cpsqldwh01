SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProntoDetails] (
		[ProntoDetailsID]           [int] IDENTITY(1, 1) NOT NULL,
		[sale]                      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[driverWareHouseNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SaleDateTime]              [datetime] NULL,
		[barcode]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[coupon]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[accountCode]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[customerPhoneNumber]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[invoiceNumber]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[transactionType]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[consignmentType]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[insuranceCode]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[insurancevalue]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]               [datetime] NULL,
		[UpdatedDate]               [datetime] NULL
)
GO
ALTER TABLE [dbo].[ProntoDetails]
	ADD
	CONSTRAINT [DF__ProntoDet__Creat__0F80164E]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ProntoDetails]
	ADD
	CONSTRAINT [DF__ProntoDet__Updat__10743A87]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[ProntoDetails] SET (LOCK_ESCALATION = TABLE)
GO
