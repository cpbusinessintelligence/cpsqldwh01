SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemp_FinalList] (
		[Service]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[cg_present]               [float] NULL,
		[cg_baseprice]             [float] NULL,
		[cg_kgincluded]            [float] NULL,
		[cg_kgprice]               [float] NULL,
		[cg_pickup_payamount]      [float] NULL,
		[cg_pickup_paybreak]       [float] NULL,
		[cg_pickup_kgpre]          [float] NULL,
		[cg_pickup_kgpost]         [float] NULL,
		[cg_deliver_payamount]     [float] NULL,
		[cg_deliver_paybreak]      [float] NULL,
		[cg_deliver_kgpre]         [float] NULL,
		[cg_deliver_kgpost]        [float] NULL
)
GO
ALTER TABLE [dbo].[Redemp_FinalList] SET (LOCK_ESCALATION = TABLE)
GO
