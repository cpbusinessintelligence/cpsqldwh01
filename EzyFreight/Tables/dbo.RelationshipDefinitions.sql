SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RelationshipDefinitions] (
		[Id]                   [int] IDENTITY(1, 1) NOT NULL,
		[PK_Table]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FK_Table]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PK_Id]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FK_Id]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RelationshipName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[t1]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[t2]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__Relation__3214EC07DAC8130A]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[RelationshipDefinitions] SET (LOCK_ESCALATION = TABLE)
GO
