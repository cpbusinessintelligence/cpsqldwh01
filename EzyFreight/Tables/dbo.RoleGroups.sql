SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoleGroups] (
		[RoleGroupID]              [int] NOT NULL,
		[RoleGroupName]            [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]              [nvarchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedByUserID]          [int] NULL,
		[CreatedOnDate]            [datetime] NULL,
		[LastModifiedByUserID]     [int] NULL,
		[LastModifiedOnDate]       [datetime] NULL,
		CONSTRAINT [IX_RoleGroupName]
		UNIQUE
		NONCLUSTERED
		([RoleGroupName])
		ON [PRIMARY],
		CONSTRAINT [PK_RoleGroups]
		PRIMARY KEY
		CLUSTERED
		([RoleGroupID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[RoleGroups] SET (LOCK_ESCALATION = TABLE)
GO
