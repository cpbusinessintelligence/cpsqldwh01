SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SSIParameter] (
		[AgentName]        [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[StatusColumn]     [bit] NULL,
		[CreatedDate]      [datetime] NULL,
		[UpdatedDate]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[SSIParameter]
	ADD
	CONSTRAINT [DF__SSIParame__Creat__30804C0F]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SSIParameter]
	ADD
	CONSTRAINT [DF__SSIParame__Updat__31747048]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[SSIParameter] SET (LOCK_ESCALATION = TABLE)
GO
