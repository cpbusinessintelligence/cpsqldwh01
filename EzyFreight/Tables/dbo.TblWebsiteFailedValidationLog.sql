SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblWebsiteFailedValidationLog] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]           [datetime] NULL,
		[CreatedBy]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDate]           [datetime] NULL,
		[UpdatedBy]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]           [int] NULL
)
GO
ALTER TABLE [dbo].[TblWebsiteFailedValidationLog]
	ADD
	CONSTRAINT [DF__TblWebsit__Creat__6A62D4E1]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TblWebsiteFailedValidationLog]
	ADD
	CONSTRAINT [DF__TblWebsit__Updat__6B56F91A]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[TblWebsiteFailedValidationLog] SET (LOCK_ESCALATION = TABLE)
GO
