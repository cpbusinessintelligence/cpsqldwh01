SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblWebsiteFailedValidationLog_Raj] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]           [datetime] NULL,
		[CreatedBy]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDate]           [datetime] NULL,
		[UpdatedBy]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]           [int] NULL
)
GO
ALTER TABLE [dbo].[TblWebsiteFailedValidationLog_Raj]
	ADD
	CONSTRAINT [DF__TblWebsit__Creat__1ACED1D7]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TblWebsiteFailedValidationLog_Raj]
	ADD
	CONSTRAINT [DF__TblWebsit__Updat__1BC2F610]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[TblWebsiteFailedValidationLog_Raj] SET (LOCK_ESCALATION = TABLE)
GO
