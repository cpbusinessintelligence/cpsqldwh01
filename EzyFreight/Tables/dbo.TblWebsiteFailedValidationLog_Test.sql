SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblWebsiteFailedValidationLog_Test] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]           [datetime] NULL,
		[CreatedBy]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDate]           [datetime] NULL,
		[UpdatedBy]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]           [int] NULL
)
GO
ALTER TABLE [dbo].[TblWebsiteFailedValidationLog_Test] SET (LOCK_ESCALATION = TABLE)
GO
