SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USPostcodeChart] (
		[Postal code]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[LAX]               [float] NULL,
		[JFK]               [float] NULL,
		[Assigned port]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[USPostcodeChart] SET (LOCK_ESCALATION = TABLE)
GO
