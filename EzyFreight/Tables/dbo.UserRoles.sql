SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserRoles] (
		[UserRoleID]               [int] NOT NULL,
		[UserID]                   [int] NOT NULL,
		[RoleID]                   [int] NOT NULL,
		[ExpiryDate]               [datetime] NULL,
		[IsTrialUsed]              [bit] NULL,
		[EffectiveDate]            [datetime] NULL,
		[CreatedByUserID]          [int] NULL,
		[CreatedOnDate]            [datetime] NULL,
		[LastModifiedByUserID]     [int] NULL,
		[LastModifiedOnDate]       [datetime] NULL,
		[Status]                   [int] NOT NULL,
		[IsOwner]                  [bit] NOT NULL,
		CONSTRAINT [PK_UserRoles]
		PRIMARY KEY
		CLUSTERED
		([UserRoleID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[UserRoles]
	ADD
	CONSTRAINT [CK_UserRoles_RoleId]
	CHECK
	([RoleId]>=(0))
GO
ALTER TABLE [dbo].[UserRoles]
CHECK CONSTRAINT [CK_UserRoles_RoleId]
GO
ALTER TABLE [dbo].[UserRoles]
	ADD
	CONSTRAINT [DF_UserRoles_Status]
	DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[UserRoles]
	ADD
	CONSTRAINT [DF_UserRoles_IsOwner]
	DEFAULT ((0)) FOR [IsOwner]
GO
ALTER TABLE [dbo].[UserRoles]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_Users_UserRoles]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[UserRoles]
	NOCHECK CONSTRAINT [FK_FK_Users_UserRoles]

GO
ALTER TABLE [dbo].[UserRoles]
	WITH NOCHECK
	ADD CONSTRAINT [FK_UserRoles_Roles]
	FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([RoleID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[UserRoles]
	NOCHECK CONSTRAINT [FK_UserRoles_Roles]

GO
ALTER TABLE [dbo].[UserRoles] SET (LOCK_ESCALATION = TABLE)
GO
