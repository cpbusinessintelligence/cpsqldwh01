SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WebsiteActivityLog] (
		[Activity]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Count]         [int] NULL,
		[sortorder]     [int] NULL
)
GO
GRANT ALTER
	ON [dbo].[WebsiteActivityLog]
	TO [ReportUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[WebsiteActivityLog]
	TO [ReportUser]
GO
ALTER TABLE [dbo].[WebsiteActivityLog] SET (LOCK_ESCALATION = TABLE)
GO
