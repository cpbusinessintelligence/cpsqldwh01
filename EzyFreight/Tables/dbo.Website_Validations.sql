SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Website_Validations] (
		[Consignments]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CP]               [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Website_Validations] SET (LOCK_ESCALATION = TABLE)
GO
