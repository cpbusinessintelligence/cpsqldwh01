SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aspnet_Membership] (
		[UserId]                                     [uniqueidentifier] NOT NULL,
		[Password]                                   [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[PasswordFormat]                             [int] NOT NULL,
		[PasswordSalt]                               [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[MobilePIN]                                  [nvarchar](16) COLLATE Latin1_General_CI_AS NULL,
		[Email]                                      [nvarchar](256) COLLATE Latin1_General_CI_AS NULL,
		[LoweredEmail]                               [nvarchar](256) COLLATE Latin1_General_CI_AS NULL,
		[PasswordQuestion]                           [nvarchar](256) COLLATE Latin1_General_CI_AS NULL,
		[PasswordAnswer]                             [nvarchar](128) COLLATE Latin1_General_CI_AS NULL,
		[IsApproved]                                 [bit] NOT NULL,
		[IsLockedOut]                                [bit] NOT NULL,
		[CreateDate]                                 [datetime] NOT NULL,
		[LastLoginDate]                              [datetime] NOT NULL,
		[LastPasswordChangedDate]                    [datetime] NOT NULL,
		[LastLockoutDate]                            [datetime] NOT NULL,
		[FailedPasswordAttemptCount]                 [int] NOT NULL,
		[FailedPasswordAttemptWindowStart]           [datetime] NOT NULL,
		[FailedPasswordAnswerAttemptCount]           [int] NOT NULL,
		[FailedPasswordAnswerAttemptWindowStart]     [datetime] NOT NULL,
		[Comment]                                    [ntext] COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__aspnet_M__1788CC4D037D7AA8]
		PRIMARY KEY
		NONCLUSTERED
		([UserId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[aspnet_Membership]
	ADD
	CONSTRAINT [DF__aspnet_Me__Passw__0BD1B136]
	DEFAULT ((0)) FOR [PasswordFormat]
GO
ALTER TABLE [dbo].[aspnet_Membership]
	WITH NOCHECK
	ADD CONSTRAINT [FK__aspnet_Me__UserI__0ADD8CFD]
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[aspnet_Membership]
	NOCHECK CONSTRAINT [FK__aspnet_Me__UserI__0ADD8CFD]

GO
CREATE CLUSTERED INDEX [aspnet_Membership_index]
	ON [dbo].[aspnet_Membership] ([LoweredEmail])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Membership] SET (LOCK_ESCALATION = TABLE)
GO
