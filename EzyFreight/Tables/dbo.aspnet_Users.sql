SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aspnet_Users] (
		[UserId]               [uniqueidentifier] NOT NULL,
		[UserName]             [nvarchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
		[LoweredUserName]      [nvarchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
		[MobileAlias]          [nvarchar](16) COLLATE Latin1_General_CI_AS NULL,
		[IsAnonymous]          [bit] NOT NULL,
		[LastActivityDate]     [datetime] NOT NULL,
		CONSTRAINT [PK__aspnet_U__1788CC4D082037AE]
		PRIMARY KEY
		NONCLUSTERED
		([UserId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[aspnet_Users]
	ADD
	CONSTRAINT [DF__aspnet_Us__UserI__7AA72534]
	DEFAULT (newid()) FOR [UserId]
GO
ALTER TABLE [dbo].[aspnet_Users]
	ADD
	CONSTRAINT [DF__aspnet_Us__Mobil__7B9B496D]
	DEFAULT ('NULL') FOR [MobileAlias]
GO
ALTER TABLE [dbo].[aspnet_Users]
	ADD
	CONSTRAINT [DF__aspnet_Us__IsAno__7C8F6DA6]
	DEFAULT ((0)) FOR [IsAnonymous]
GO
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2]
	ON [dbo].[aspnet_Users] ([LastActivityDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Users] SET (LOCK_ESCALATION = TABLE)
GO
