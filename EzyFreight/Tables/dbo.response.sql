SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[response] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ResponseCode]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Request]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]         [datetime] NULL,
		[IsProcessed]         [bit] NULL
)
GO
ALTER TABLE [dbo].[response] SET (LOCK_ESCALATION = TABLE)
GO
