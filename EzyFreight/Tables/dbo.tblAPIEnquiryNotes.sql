SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAPIEnquiryNotes] (
		[Id]               [int] IDENTITY(1, 1) NOT NULL,
		[APIEnquiryID]     [int] NULL,
		[Text]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[To]               [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[CC]               [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[BCC]              [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Subject]          [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Attachments]      [nvarchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CreatedTime]      [datetime] NULL,
		[UpdatedTime]      [datetime] NULL,
		CONSTRAINT [PK_tblAPIEnquiryNotes]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblAPIEnquiryNotes]
	ADD
	CONSTRAINT [DF_tblAPIEnquiryNotes_CreatedTime]
	DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[tblAPIEnquiryNotes]
	ADD
	CONSTRAINT [DF_tblAPIEnquiryNotes_UpdatedTime]
	DEFAULT (getdate()) FOR [UpdatedTime]
GO
ALTER TABLE [dbo].[tblAPIEnquiryNotes] SET (LOCK_ESCALATION = TABLE)
GO
