SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAPIEnquiryStatus] (
		[Id]               [int] IDENTITY(1, 1) NOT NULL,
		[APIEnquiryID]     [int] NULL,
		[CreatedTime]      [datetime] NULL,
		[UpdatedTime]      [datetime] NULL,
		[Status]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Category]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[VendorUsed]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblAPIEnquiryStatus]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblAPIEnquiryStatus]
	ADD
	CONSTRAINT [DF__tblAPIEnq__Statu__4166E83C]
	DEFAULT ('Not Started') FOR [Status]
GO
ALTER TABLE [dbo].[tblAPIEnquiryStatus]
	ADD
	CONSTRAINT [DF_tblAPIEnquiryStatus_UpdatedTime]
	DEFAULT (getdate()) FOR [UpdatedTime]
GO
ALTER TABLE [dbo].[tblAPIEnquiryStatus]
	ADD
	CONSTRAINT [DF__tblAPIEnq__Categ__425B0C75]
	DEFAULT ('None') FOR [Category]
GO
ALTER TABLE [dbo].[tblAPIEnquiryStatus]
	ADD
	CONSTRAINT [DF_tblAPIEnquiryStatus_CreatedTime]
	DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[tblAPIEnquiryStatus] SET (LOCK_ESCALATION = TABLE)
GO
