SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblActivityLogging] (
		[Id]                [int] NOT NULL,
		[CPPLServiceID]     [int] NOT NULL,
		[ClientID]          [int] NOT NULL,
		[CreateDate]        [datetime] NOT NULL,
		[Comment]           [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblActivityLogging]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblActivityLogging] SET (LOCK_ESCALATION = TABLE)
GO
