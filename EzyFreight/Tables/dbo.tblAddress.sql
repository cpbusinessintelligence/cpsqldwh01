SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddress] (
		[AddressID]             [int] NOT NULL,
		[UserID]                [int] NULL,
		[FirstName]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastName]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompanyName]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]                 [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Address1]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address2]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StateName]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StateID]               [int] NULL,
		[PostCode]              [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Phone]                 [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Mobile]                [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Country]               [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CountryCode]           [nvarchar](10) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		[IsRegisterAddress]     [bit] NULL,
		[IsDeleted]             [bit] NULL,
		[IsBusiness]            [bit] NULL,
		[IsSubscribe]           [bit] NULL,
		[IsEDIUser]             [bit] NULL,
		CONSTRAINT [PK_tblAddress]
		PRIMARY KEY
		CLUSTERED
		([AddressID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblAddress]
	ADD
	CONSTRAINT [DF__tblAddres__Creat__49C3F6B7]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblAddress]
	ADD
	CONSTRAINT [DF_tblAddress_IsRegisterAddress]
	DEFAULT ((0)) FOR [IsRegisterAddress]
GO
ALTER TABLE [dbo].[tblAddress]
	ADD
	CONSTRAINT [DF_tblAddress_IdDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tblAddress] SET (LOCK_ESCALATION = TABLE)
GO
