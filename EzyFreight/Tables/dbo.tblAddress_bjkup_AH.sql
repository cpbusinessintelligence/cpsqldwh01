SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddress_bjkup_AH] (
		[AddressID]             [int] NOT NULL,
		[UserID]                [int] NULL,
		[FirstName]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastName]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompanyName]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]                 [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Address1]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address2]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StateName]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StateID]               [int] NULL,
		[PostCode]              [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Phone]                 [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Mobile]                [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Country]               [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CountryCode]           [nvarchar](10) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		[IsRegisterAddress]     [bit] NULL,
		[IsDeleted]             [bit] NULL,
		[IsBusiness]            [bit] NULL,
		[IsSubscribe]           [bit] NULL,
		[IsEDIUser]             [bit] NULL
)
GO
ALTER TABLE [dbo].[tblAddress_bjkup_AH] SET (LOCK_ESCALATION = TABLE)
GO
