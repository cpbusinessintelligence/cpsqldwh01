SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAgentExecLog] (
		[Id]              [int] IDENTITY(1, 1) NOT NULL,
		[AgentName]       [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[PackageName]     [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[StatusDesc]      [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL,
		[UpdatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblAgentExecLog]
	ADD
	CONSTRAINT [DF__tblAgentE__Creat__5670EACD]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tblAgentExecLog]
	ADD
	CONSTRAINT [DF__tblAgentE__Updat__57650F06]
	DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[tblAgentExecLog] SET (LOCK_ESCALATION = TABLE)
GO
