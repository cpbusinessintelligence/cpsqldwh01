SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBooking] (
		[BookingID]              [int] NOT NULL,
		[PhoneNumber]            [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ContactName]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ContactEmail]           [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]             [date] NOT NULL,
		[PickupTime]             [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[OrderCoupons]           [bit] NULL,
		[PickupFromCustomer]     [bit] NULL,
		[PickupName]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress1]         [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress2]         [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupSuburb]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupPhoneNo]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ContactDetails]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupFrom]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]            [bit] NULL,
		[BookingRefNo]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [int] NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		[Branch]                 [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupEmail]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostCode]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Costom]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CostomId]               [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentID]          [int] NULL,
		CONSTRAINT [PK_tblBooking]
		PRIMARY KEY
		CLUSTERED
		([BookingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblBooking]
	ADD
	CONSTRAINT [DEF_tblBooking_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblBooking] SET (LOCK_ESCALATION = TABLE)
GO
