SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBookingFromHubbed] (
		[AgentName]            [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupTime]           [datetime] NULL,
		[PickupSuburb]         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostcode]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupCompany]        [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress1]       [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress2]       [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress3]       [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[PickupPhone]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]       [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostcode]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Sendername]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress1]     [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress2]     [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[BookingRefNumber]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]          [bit] NULL,
		[BookingRequest]       [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[BookingResponse]      [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]          [datetime] NULL,
		[CreatedBy]            [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDate]          [datetime] NULL,
		[UpdatedBy]            [varchar](300) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblBookingFromHubbed]
	ADD
	CONSTRAINT [DF__tblBookin__Creat__05601889]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tblBookingFromHubbed] SET (LOCK_ESCALATION = TABLE)
GO
