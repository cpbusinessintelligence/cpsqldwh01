SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBranchAddresses] (
		[Id]          [int] NOT NULL,
		[Company]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Branch]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address]     [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[State]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Country]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblBranchAddresses] SET (LOCK_ESCALATION = TABLE)
GO
