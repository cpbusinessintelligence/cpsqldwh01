SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCancelledConsignment] (
		[Id]                  [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]         [bit] NULL,
		[CreatedBy]           [int] NULL,
		[CreatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		CONSTRAINT [PK_tblCancelledConsignment]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCancelledConsignment] SET (LOCK_ESCALATION = TABLE)
GO
