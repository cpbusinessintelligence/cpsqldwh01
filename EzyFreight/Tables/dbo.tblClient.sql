SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClient] (
		[ClientID]            [int] NOT NULL,
		[ClientName]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ClientCode]          [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBY]           [int] NULL,
		CONSTRAINT [PK_tblClient]
		PRIMARY KEY
		CLUSTERED
		([ClientID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblClient] SET (LOCK_ESCALATION = TABLE)
GO
