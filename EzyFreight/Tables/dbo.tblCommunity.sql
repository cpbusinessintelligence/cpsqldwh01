SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCommunity] (
		[ID]                      [int] NOT NULL,
		[CommunityInitiative]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[StartDate]               [date] NULL,
		[EndDate]                 [date] NULL,
		[FirstName]               [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[LastName]                [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[CompanyName]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Email]                   [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Address]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                   [int] NULL,
		[Suburb]                  [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]                [nvarchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Mobile]                  [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Website]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[PurposeOfInitiative]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[WorkTogether]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		CONSTRAINT [PK_tblCommunity]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCommunity]
	ADD
	CONSTRAINT [DF_tblCommunity_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblCommunity] SET (LOCK_ESCALATION = TABLE)
GO
