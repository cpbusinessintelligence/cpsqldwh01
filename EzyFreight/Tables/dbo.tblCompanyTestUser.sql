SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCompanyTestUser] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[Accountcode]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[createddatetime]     [datetime] NULL,
		[editeddatetime]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblCompanyTestUser]
	ADD
	CONSTRAINT [DF__TestUsert__creat__55422D09]
	DEFAULT (getdate()) FOR [createddatetime]
GO
ALTER TABLE [dbo].[tblCompanyTestUser]
	ADD
	CONSTRAINT [DF__TestUsert__edite__56365142]
	DEFAULT (getdate()) FOR [editeddatetime]
GO
ALTER TABLE [dbo].[tblCompanyTestUser] SET (LOCK_ESCALATION = TABLE)
GO
