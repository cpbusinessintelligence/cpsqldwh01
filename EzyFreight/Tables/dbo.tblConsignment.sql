SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignment] (
		[ConsignmentID]                        [int] NOT NULL,
		[ConsignmentCode]                      [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[UserID]                               [int] NULL,
		[IsRegUserConsignment]                 [bit] NOT NULL,
		[PickupID]                             [int] NULL,
		[DestinationID]                        [int] NULL,
		[ContactID]                            [int] NULL,
		[TotalWeight]                          [decimal](10, 4) NULL,
		[TotalMeasureWeight]                   [decimal](10, 4) NULL,
		[TotalVolume]                          [decimal](10, 4) NULL,
		[TotalMeasureVolume]                   [decimal](10, 4) NULL,
		[NoOfItems]                            [int] NOT NULL,
		[SpecialInstruction]                   [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CustomerRefNo]                        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentPreferPickupDate]          [date] NOT NULL,
		[ConsignmentPreferPickupTime]          [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ClosingTime]                          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DangerousGoods]                       [bit] NOT NULL,
		[Terms]                                [bit] NOT NULL,
		[RateCardID]                           [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LastActivity]                         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[LastActiivityDateTime]                [datetime] NULL,
		[ConsignmentStatus]                    [int] NULL,
		[EDIDataProcessed]                     [bit] NULL,
		[ProntoDataExtracted]                  [bit] NULL,
		[IsBilling]                            [bit] NULL,
		[IsManifested]                         [bit] NULL,
		[CreatedDateTime]                      [datetime] NOT NULL,
		[CreatedBy]                            [int] NULL,
		[UpdatedDateTTime]                     [datetime] NULL,
		[UpdatedBy]                            [int] NULL,
		[IsInternational]                      [bit] NULL,
		[IsDocument]                           [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsSignatureReq]                       [bit] NULL,
		[IfUndelivered]                        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ReasonForExport]                      [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[TypeOfExport]                         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Currency]                             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsInsurance]                          [bit] NULL,
		[IsIdentity]                           [bit] NULL,
		[IdentityType]                         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IdentityNo]                           [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Country-ServiceArea-FacilityCode]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[InternalServiceCode]                  [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[NetSubTotal]                          [decimal](10, 2) NULL,
		[IsATl]                                [bit] NULL,
		[IsReturnToSender]                     [bit] NULL,
		[HasReadInsuranceTc]                   [bit] NULL,
		[NatureOfGoods]                        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[OriginServiceAreaCode]                [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ProductShortName]                     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[SortCode]                             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ETA]                                  [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CTIManifestExtracted]                 [bit] NULL,
		[isprocessed]                          [bit] NULL,
		[IsAccountCustomer]                    [bit] NULL,
		[InsuranceAmount]                      [decimal](10, 2) NULL,
		[CourierPickupDate]                    [datetime] NULL,
		[CalculatedTotal]                      [decimal](18, 2) NULL,
		[CalculatedGST]                        [decimal](18, 2) NULL,
		[ClientCode]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isCreateEzy2ShipShipment]             [bit] NULL,
		[isShipmentSent2NZPost]                [bit] NULL,
		[PromotionCode]                        [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[IsHubApiProcessed]                    [bit] NULL,
		[InsuranceCategory]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [TUC_tblConsignment_1]
		UNIQUE
		NONCLUSTERED
		([ConsignmentCode])
		ON [PRIMARY],
		CONSTRAINT [PK_tblConsignment]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF_tblConsignment_IsBilling]
	DEFAULT ((0)) FOR [IsBilling]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF_tblConsignment_IsManifested]
	DEFAULT ((0)) FOR [IsManifested]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF_tblConsignment_IsInternational]
	DEFAULT ((0)) FOR [IsInternational]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__ispro__2B0043CC]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__IsAcc__2200E977]
	DEFAULT ((0)) FOR [IsAccountCustomer]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__isCre__3FF46C6D]
	DEFAULT ((0)) FOR [isCreateEzy2ShipShipment]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__isShi__7B15FD92]
	DEFAULT ((0)) FOR [isShipmentSent2NZPost]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__IsHub__6724DED5]
	DEFAULT ((0)) FOR [IsHubApiProcessed]
GO
ALTER TABLE [dbo].[tblConsignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment]
	FOREIGN KEY ([PickupID]) REFERENCES [dbo].[tblAddress] ([AddressID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblConsignment]
	NOCHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment]

GO
ALTER TABLE [dbo].[tblConsignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment_1]
	FOREIGN KEY ([DestinationID]) REFERENCES [dbo].[tblAddress] ([AddressID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblConsignment]
	NOCHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment_1]

GO
ALTER TABLE [dbo].[tblConsignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment_2]
	FOREIGN KEY ([ContactID]) REFERENCES [dbo].[tblAddress] ([AddressID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblConsignment]
	NOCHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment_2]

GO
ALTER TABLE [dbo].[tblConsignment]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblStatus_tblConsignment]
	FOREIGN KEY ([ConsignmentStatus]) REFERENCES [dbo].[tblStatus] ([StatusID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblConsignment]
	NOCHECK CONSTRAINT [FK_FK_tblStatus_tblConsignment]

GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblConsignment_ConsignmentCode]
	ON [dbo].[tblConsignment] ([ConsignmentCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblConsignment] SET (LOCK_ESCALATION = TABLE)
GO
