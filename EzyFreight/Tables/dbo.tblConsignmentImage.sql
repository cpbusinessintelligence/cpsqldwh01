SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignmentImage] (
		[Id]                  [int] NOT NULL,
		[ConsignmentID]       [int] NOT NULL,
		[CommercialImage]     [varbinary](max) NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblConsignmentImage]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblConsignmentImage]
	ADD
	CONSTRAINT [DF_tblConsignmentImage_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblConsignmentImage]
	WITH NOCHECK
	ADD CONSTRAINT [FK_tblConsignmentImage_tblConsignment]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblConsignmentImage]
	NOCHECK CONSTRAINT [FK_tblConsignmentImage_tblConsignment]

GO
ALTER TABLE [dbo].[tblConsignmentImage] SET (LOCK_ESCALATION = TABLE)
GO
