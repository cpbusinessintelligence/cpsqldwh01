SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblConsignmentService] (
		[ServiceConsignmentID]     [int] NOT NULL,
		[ServiceID]                [int] NOT NULL,
		[ConsignmentID]            [int] NOT NULL,
		[CreatedDateTime]          [datetime] NOT NULL,
		[CreatedBy]                [int] NOT NULL,
		[UpdatedDateTime]          [datetime] NULL,
		[UpdatedBy]                [int] NULL,
		CONSTRAINT [PK_tblConsignmentService]
		PRIMARY KEY
		CLUSTERED
		([ServiceConsignmentID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblConsignmentService]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblConsignment_tblConsignmentService]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblConsignmentService]
	NOCHECK CONSTRAINT [FK_FK_tblConsignment_tblConsignmentService]

GO
ALTER TABLE [dbo].[tblConsignmentService]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblService_tblConsignmentService]
	FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[tblService] ([ServiceID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblConsignmentService]
	NOCHECK CONSTRAINT [FK_FK_tblService_tblConsignmentService]

GO
ALTER TABLE [dbo].[tblConsignmentService] SET (LOCK_ESCALATION = TABLE)
GO
