SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignment_Seko1] (
		[sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isDelivered]         [bit] NULL
)
GO
ALTER TABLE [dbo].[tblConsignment_Seko1]
	ADD
	CONSTRAINT [DF__tblConsig__isDel__250215BD]
	DEFAULT ((0)) FOR [isDelivered]
GO
ALTER TABLE [dbo].[tblConsignment_Seko1] SET (LOCK_ESCALATION = TABLE)
GO
