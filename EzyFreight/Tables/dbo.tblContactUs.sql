SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContactUs] (
		[Id]                 [int] NOT NULL,
		[FirstName]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastName]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Email]              [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Phone]              [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StateID]            [int] NULL,
		[Subject]            [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[HowCanIHelpYou]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsSubscribe]        [bit] NULL,
		[CreatedDate]        [datetime] NULL,
		[TrackingNo]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[AddressLine1]       [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[AddressLine2]       [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsProccessed]       [bit] NULL,
		[EMMRequest]         [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[EMMResponse]        [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[EnquiryNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[servicedeskid]      [int] NULL,
		CONSTRAINT [PK_TBLContactUs]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblContactUs]
	ADD
	CONSTRAINT [DF_TBLContactUs_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tblContactUs] SET (LOCK_ESCALATION = TABLE)
GO
