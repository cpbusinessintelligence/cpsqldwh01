SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomerRate] (
		[CustomerRateID]     [int] NOT NULL,
		[UserID]             [int] NOT NULL,
		[RateCardID]         [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]           [bit] NOT NULL,
		[CreatedDate]        [datetime] NOT NULL,
		[CreatedBy]          [int] NOT NULL,
		[UpdatedDate]        [datetime] NULL,
		[UpdatedBy]          [int] NULL,
		CONSTRAINT [PK_tblCustomerRate]
		PRIMARY KEY
		CLUSTERED
		([CustomerRateID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCustomerRate]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblRateCard_tblCustomerRate]
	FOREIGN KEY ([RateCardID]) REFERENCES [dbo].[tblRateCard] ([RateCardID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblCustomerRate]
	NOCHECK CONSTRAINT [FK_FK_tblRateCard_tblCustomerRate]

GO
ALTER TABLE [dbo].[tblCustomerRate]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_Users_tblCustomerRate]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblCustomerRate]
	NOCHECK CONSTRAINT [FK_FK_Users_tblCustomerRate]

GO
ALTER TABLE [dbo].[tblCustomerRate] SET (LOCK_ESCALATION = TABLE)
GO
