SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDHLBarCodeImage] (
		[Id]                     [int] NOT NULL,
		[ConsignmentID]          [int] NULL,
		[AWBCode]                [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[AWBBarCode]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[OriginDestncode]        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[OriginDestnBarcode]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ClientIDCode]           [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ClientIDBarCode]        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[DHLRoutingCode]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[DHLRoutingBarCode]      [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]        [datetime] NULL,
		[CreatedBy]              [int] NULL,
		CONSTRAINT [PK_tblDHLBarCodeImage]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	ADD
	CONSTRAINT [DF_tblDHLBarCodeImage_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	ADD
	CONSTRAINT [DF_tblDHLBarCodeImage_CreatedBy]
	DEFAULT ((-1)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	WITH NOCHECK
	ADD CONSTRAINT [FK_tblDHLBarCodeImage_tblConsignment]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	NOCHECK CONSTRAINT [FK_tblDHLBarCodeImage_tblConsignment]

GO
ALTER TABLE [dbo].[tblDHLBarCodeImage] SET (LOCK_ESCALATION = TABLE)
GO
