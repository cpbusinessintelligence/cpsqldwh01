SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDHLStaging] (
		[AWBNumber]           [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ScanEvent]           [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[ContractorCode]      [int] NULL,
		[ExceptionReason]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]         [bit] NULL,
		[DHLLocationcode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblDHLStaging]
	ADD
	CONSTRAINT [DF__tblDHLSta__ispro__0B8863CA]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[tblDHLStaging] SET (LOCK_ESCALATION = TABLE)
GO
