SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailNotification] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Email]                 [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]              [bit] NOT NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[UpdatedDateTime]       [datetime] NOT NULL,
		[UpdatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__IsAct__5B1FC5FA]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Creat__5C13EA33]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Creat__5D080E6C]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Updat__5DFC32A5]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblEmailNotification]
	ADD
	CONSTRAINT [DF__tblEmailN__Updat__5EF056DE]
	DEFAULT ('TS') FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[tblEmailNotification] SET (LOCK_ESCALATION = TABLE)
GO
