SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEzyNetCustomer] (
		[CustomerID]          [int] NOT NULL,
		[PhoneNo]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Branch]              [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[FirstName]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Lastname]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Email]               [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[CreatedBy]           [int] NULL,
		[CreatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		CONSTRAINT [IX_tblEzyNetCustomer]
		UNIQUE
		NONCLUSTERED
		([PhoneNo], [Branch])
		ON [PRIMARY],
		CONSTRAINT [PK_tblEzyNetCustomer]
		PRIMARY KEY
		CLUSTERED
		([CustomerID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblEzyNetCustomer]
	ADD
	CONSTRAINT [DF_tblEzyNetCustomer_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblEzyNetCustomer] SET (LOCK_ESCALATION = TABLE)
GO
