SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFranchiseList] (
		[FranchiseID]         [int] NOT NULL,
		[Branch]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RunNo]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Area]                [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[GrossIncome]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SellingPrice]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SellWithVan]         [bit] NULL,
		[RunDescription]      [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[VanDescription]      [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ContactName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PhoneNo]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsArchive]           [bit] NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [int] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblFranchiseList]
		PRIMARY KEY
		CLUSTERED
		([FranchiseID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblFranchiseList] SET (LOCK_ESCALATION = TABLE)
GO
