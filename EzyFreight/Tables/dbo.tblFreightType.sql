SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFreightType] (
		[FreightTypeID]       [int] NOT NULL,
		[FreightType]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblFreightType]
		PRIMARY KEY
		CLUSTERED
		([FreightTypeID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblFreightType] SET (LOCK_ESCALATION = TABLE)
GO
