SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInsurance] (
		[InsuranceID]              [int] NOT NULL,
		[InsuranceCategoryID]      [int] NOT NULL,
		[InsuranceDetail]          [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[InsuranceChargeExGST]     [numeric](19, 4) NULL,
		[GST]                      [numeric](19, 4) NULL,
		[InsuranceCharge]          [numeric](19, 4) NOT NULL,
		[IsArchive]                [bit] NOT NULL,
		[CreatedDateTime]          [datetime] NOT NULL,
		[CreatedBy]                [int] NOT NULL,
		[UpdatedDateTime]          [datetime] NULL,
		[UpdatedBy]                [int] NULL
)
GO
ALTER TABLE [dbo].[tblInsurance]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblnsuranceCategory_tblInsurance]
	FOREIGN KEY ([InsuranceCategoryID]) REFERENCES [dbo].[tblnsuranceCategory] ([InsuranceCategoryID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblInsurance]
	NOCHECK CONSTRAINT [FK_FK_tblnsuranceCategory_tblInsurance]

GO
ALTER TABLE [dbo].[tblInsurance] SET (LOCK_ESCALATION = TABLE)
GO
