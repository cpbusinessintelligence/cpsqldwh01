SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInsuranceCategory] (
		[InsuranceCategoryID]         [int] NOT NULL,
		[InsuranceCategoryCode]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[InsuranceCategoryDetail]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]             [datetime] NOT NULL,
		[CreatedBy]                   [int] NOT NULL,
		[UpdatedDateTime]             [datetime] NULL,
		[UpdatedBy]                   [int] NULL
)
GO
ALTER TABLE [dbo].[tblInsuranceCategory] SET (LOCK_ESCALATION = TABLE)
GO
