SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblItemLabelImage] (
		[Id]                    [int] NOT NULL,
		[ItemLabelID]           [int] NOT NULL,
		[LableImage]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ArchiveLableImage]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		CONSTRAINT [PK_tblItemLabelImage]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblItemLabelImage]
	ADD
	CONSTRAINT [DF_tblItemLabelImage_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblItemLabelImage]
	WITH NOCHECK
	ADD CONSTRAINT [FK_tblItemLabelImage_tblItemLabel]
	FOREIGN KEY ([ItemLabelID]) REFERENCES [dbo].[tblItemLabel] ([ItemLabelID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblItemLabelImage]
	NOCHECK CONSTRAINT [FK_tblItemLabelImage_tblItemLabel]

GO
ALTER TABLE [dbo].[tblItemLabelImage] SET (LOCK_ESCALATION = TABLE)
GO
