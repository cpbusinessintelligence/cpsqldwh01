SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblQuote] (
		[QuoteID]                 [int] NOT NULL,
		[FreightType]             [int] NOT NULL,
		[Length]                  [decimal](4, 2) NOT NULL,
		[Width]                   [decimal](4, 2) NOT NULL,
		[Height]                  [decimal](4, 2) NOT NULL,
		[PhysicalWeight]          [decimal](4, 2) NOT NULL,
		[VolumeWeight]            [decimal](4, 2) NOT NULL,
		[PickupPostcode]          [int] NOT NULL,
		[DestinationPostCode]     [int] NOT NULL,
		[QuoteResult]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ETA]                     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		[CreatedBy]               [int] NOT NULL,
		[UpdatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [int] NULL,
		CONSTRAINT [PK_tblQuote]
		PRIMARY KEY
		CLUSTERED
		([QuoteID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblQuote] SET (LOCK_ESCALATION = TABLE)
GO
