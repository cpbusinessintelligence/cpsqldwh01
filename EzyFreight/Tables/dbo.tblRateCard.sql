SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCard] (
		[RateCardID]              [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RateCardCode]            [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[RateCardDescription]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]                [bit] NOT NULL,
		[IsArchieve]              [bit] NOT NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		[CreatedBy]               [int] NOT NULL,
		[UpdatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [int] NULL,
		CONSTRAINT [TUC_tblRateCard_1]
		UNIQUE
		NONCLUSTERED
		([RateCardCode])
		ON [PRIMARY],
		CONSTRAINT [PK_tblRateCard]
		PRIMARY KEY
		CLUSTERED
		([RateCardID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRateCard]
	ADD
	CONSTRAINT [DEF_tblRateCard_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRateCard] SET (LOCK_ESCALATION = TABLE)
GO
