SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCardDetail] (
		[RateCardDetailID]           [int] NOT NULL,
		[RateCardID]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RoundingWeight]             [int] NULL,
		[FromZoneCode]               [int] NOT NULL,
		[ToZoneCode]                 [int] NOT NULL,
		[FreightCharge]              [decimal](19, 4) NOT NULL,
		[FuelSurCharge]              [decimal](19, 4) NOT NULL,
		[ContractorPickupCost]       [decimal](19, 4) NOT NULL,
		[ContractorDeliveryCost]     [decimal](19, 4) NOT NULL,
		[CreatedDateTime]            [datetime] NOT NULL,
		[CreatedBy]                  [int] NOT NULL,
		[UpdatedDateTime]            [datetime] NULL,
		[UpdatedBy]                  [int] NULL,
		CONSTRAINT [PK_tblRateCardDetail]
		PRIMARY KEY
		CLUSTERED
		([RateCardDetailID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRateCardDetail]
	ADD
	CONSTRAINT [DEF_tblRateCardDetail_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRateCardDetail]
	ADD
	CONSTRAINT [DEF_tblRateCardDetail_CreatedBy]
	DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblRateCardDetail]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblRateCard_tblRateCardDetail]
	FOREIGN KEY ([RateCardID]) REFERENCES [dbo].[tblRateCard] ([RateCardID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblRateCardDetail]
	NOCHECK CONSTRAINT [FK_FK_tblRateCard_tblRateCardDetail]

GO
ALTER TABLE [dbo].[tblRateCardDetail]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblZone_tblRateCardDetail]
	FOREIGN KEY ([FromZoneCode]) REFERENCES [dbo].[tblZone] ([ZoneID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblRateCardDetail]
	NOCHECK CONSTRAINT [FK_FK_tblZone_tblRateCardDetail]

GO
ALTER TABLE [dbo].[tblRateCardDetail]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblZone_tblRateCardDetail_1]
	FOREIGN KEY ([ToZoneCode]) REFERENCES [dbo].[tblZone] ([ZoneID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblRateCardDetail]
	NOCHECK CONSTRAINT [FK_FK_tblZone_tblRateCardDetail_1]

GO
ALTER TABLE [dbo].[tblRateCardDetail] SET (LOCK_ESCALATION = TABLE)
GO
