SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedelivery] (
		[RedeliveryID]                [int] NOT NULL,
		[RedeliveryType]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CardReferenceNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LableNumber]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentCode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                       [int] NOT NULL,
		[AttemptedRedeliveryTime]     [datetime] NOT NULL,
		[SenderName]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NumberOfItem]                [int] NULL,
		[DestinationName]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationAddress]          [varchar](200) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationSuburb]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationPostCode]         [int] NOT NULL,
		[SelectedETADate]             [date] NOT NULL,
		[PreferDeliverTimeSlot]       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[PreferDeliverTime]           [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]             [datetime] NOT NULL,
		[CreatedBy]                   [int] NOT NULL,
		[UpdatedDateTime]             [datetime] NULL,
		[UpdatedBy]                   [int] NULL,
		[IsProcessed]                 [bit] NULL,
		[Firstname]                   [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Lastname]                    [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Email]                       [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PhoneNumber]                 [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SPInstruction]               [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[SourceSystem]                [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblRedelivery]
		PRIMARY KEY
		CLUSTERED
		([RedeliveryID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRedelivery]
	ADD
	CONSTRAINT [DF_tblRedelivery_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRedelivery]
	ADD
	CONSTRAINT [DF_tblRedelivery_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblRedelivery] SET (LOCK_ESCALATION = TABLE)
GO
