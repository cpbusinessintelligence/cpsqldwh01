SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedirectedItemLabel] (
		[ReItemLabelID]            [int] NOT NULL,
		[ReConsignmentID]          [int] NOT NULL,
		[LabelNumber]              [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[Length]                   [decimal](10, 2) NULL,
		[Width]                    [decimal](10, 2) NULL,
		[Height]                   [decimal](10, 2) NULL,
		[CubicWeight]              [decimal](10, 2) NULL,
		[PhysicalWeight]           [decimal](10, 2) NULL,
		[LastActivity]             [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[LastActivityDateTime]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]          [datetime] NOT NULL,
		[CreatedBy]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[UpdatedDateTime]          [datetime] NULL,
		[UpdatedBy]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblRedirectedItemLabel]
		PRIMARY KEY
		CLUSTERED
		([ReItemLabelID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRedirectedItemLabel]
	ADD
	CONSTRAINT [DEF_tblRedirectedItemLabel_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRedirectedItemLabel]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblRedirectedConsignment_tblRedirectedItemLabel]
	FOREIGN KEY ([ReConsignmentID]) REFERENCES [dbo].[tblRedirectedConsignment] ([ReConsignmentID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblRedirectedItemLabel]
	NOCHECK CONSTRAINT [FK_FK_tblRedirectedConsignment_tblRedirectedItemLabel]

GO
ALTER TABLE [dbo].[tblRedirectedItemLabel] SET (LOCK_ESCALATION = TABLE)
GO
