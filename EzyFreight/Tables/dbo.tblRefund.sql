SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRefund] (
		[Id]                        [int] NOT NULL,
		[ConsignmentID]             [int] NULL,
		[Reason]                    [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                    [decimal](18, 2) NULL,
		[PaymentRefNo]              [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AuthorizationCode]         [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsProcess]                 [bit] NULL,
		[CreatedDateTime]           [datetime] NULL,
		[CreatedBy]                 [int] NULL,
		[UpdatedDateTTime]          [datetime] NULL,
		[UpdatedBy]                 [int] NULL,
		[MerchantReferenceCode]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RefundCategory]            [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblRefund]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRefund]
	ADD
	CONSTRAINT [DF_tblRefund_IsProcess]
	DEFAULT ((0)) FOR [IsProcess]
GO
ALTER TABLE [dbo].[tblRefund]
	ADD
	CONSTRAINT [DF_tblRefund_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRefund]
	WITH NOCHECK
	ADD CONSTRAINT [FK_tblRefund_tblRefund]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblRefund]
	NOCHECK CONSTRAINT [FK_tblRefund_tblRefund]

GO
ALTER TABLE [dbo].[tblRefund] SET (LOCK_ESCALATION = TABLE)
GO
