SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRefund_backupHB07082018] (
		[Id]                        [int] NOT NULL,
		[ConsignmentID]             [int] NULL,
		[Reason]                    [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                    [decimal](18, 2) NULL,
		[PaymentRefNo]              [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AuthorizationCode]         [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsProcess]                 [bit] NULL,
		[CreatedDateTime]           [datetime] NULL,
		[CreatedBy]                 [int] NULL,
		[UpdatedDateTTime]          [datetime] NULL,
		[UpdatedBy]                 [int] NULL,
		[MerchantReferenceCode]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[RefundCategory]            [varchar](150) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblRefund_backupHB07082018] SET (LOCK_ESCALATION = TABLE)
GO
