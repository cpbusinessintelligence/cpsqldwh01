SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSalesOrder] (
		[SalesOrderID]          [int] NOT NULL,
		[ReferenceNo]           [int] NOT NULL,
		[UserID]                [int] NULL,
		[NoofItems]             [int] NOT NULL,
		[TotalWeight]           [decimal](10, 4) NOT NULL,
		[TotalVolume]           [decimal](10, 4) NOT NULL,
		[RateCardID]            [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[GrossTotal]            [decimal](19, 4) NOT NULL,
		[GST]                   [decimal](19, 4) NOT NULL,
		[NetTotal]              [decimal](19, 4) NOT NULL,
		[SalesOrderStatus]      [int] NULL,
		[InvoiceNo]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		[ConsignmentCode]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PromotionDiscount]     [decimal](19, 4) NULL,
		[DiscountedTotal]       [decimal](19, 4) NULL,
		[SalesOrderType]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblSalesOrder]
		PRIMARY KEY
		CLUSTERED
		([SalesOrderID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblSalesOrder]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblConsignment_tblSalesOrder]
	FOREIGN KEY ([ReferenceNo]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblSalesOrder]
	NOCHECK CONSTRAINT [FK_FK_tblConsignment_tblSalesOrder]

GO
ALTER TABLE [dbo].[tblSalesOrder]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblStatus_tblSalesOrder]
	FOREIGN KEY ([SalesOrderStatus]) REFERENCES [dbo].[tblStatus] ([StatusID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblSalesOrder]
	NOCHECK CONSTRAINT [FK_FK_tblStatus_tblSalesOrder]

GO
ALTER TABLE [dbo].[tblSalesOrder] SET (LOCK_ESCALATION = TABLE)
GO
