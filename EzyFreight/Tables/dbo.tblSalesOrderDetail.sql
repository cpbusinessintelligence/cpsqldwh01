SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSalesOrderDetail] (
		[SalesOrderDetailID]     [int] NOT NULL,
		[SalesOrderID]           [int] NOT NULL,
		[Description]            [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
		[LineNo]                 [int] NOT NULL,
		[Weight]                 [decimal](10, 4) NULL,
		[Volume]                 [decimal](10, 4) NULL,
		[FreightCharge]          [decimal](19, 4) NOT NULL,
		[FuelCharge]             [decimal](19, 4) NULL,
		[InsuranceCharge]        [decimal](19, 4) NULL,
		[ServiceCharge]          [decimal](19, 4) NULL,
		[Total]                  [decimal](19, 4) NOT NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [int] NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		CONSTRAINT [PK_tblSalesOrderDetail]
		PRIMARY KEY
		CLUSTERED
		([SalesOrderDetailID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblSalesOrderDetail]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblSalesOrder_tblSalesOrderDetail]
	FOREIGN KEY ([SalesOrderID]) REFERENCES [dbo].[tblSalesOrder] ([SalesOrderID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblSalesOrderDetail]
	NOCHECK CONSTRAINT [FK_FK_tblSalesOrder_tblSalesOrderDetail]

GO
ALTER TABLE [dbo].[tblSalesOrderDetail] SET (LOCK_ESCALATION = TABLE)
GO
