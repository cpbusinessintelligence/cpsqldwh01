SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSalesOrderDetail_Archive] (
		[SalesOrderDetailID]     [int] NULL,
		[SalesOrderID]           [int] NULL,
		[Description]            [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[LineNo]                 [int] NULL,
		[Weight]                 [decimal](10, 4) NULL,
		[Volume]                 [decimal](10, 4) NULL,
		[FreightCharge]          [decimal](19, 4) NULL,
		[FuelCharge]             [decimal](19, 4) NULL,
		[InsuranceCharge]        [decimal](19, 4) NULL,
		[ServiceCharge]          [decimal](19, 4) NULL,
		[Total]                  [decimal](19, 4) NULL,
		[CreatedDateTime]        [datetime] NULL,
		[CreatedBy]              [int] NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL
)
GO
ALTER TABLE [dbo].[tblSalesOrderDetail_Archive] SET (LOCK_ESCALATION = TABLE)
GO
