SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblService] (
		[ServiceID]           [int] NOT NULL,
		[ServiceType]         [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceCharge]       [decimal](4, 2) NOT NULL,
		[IsArchive]           [bit] NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblService]
		PRIMARY KEY
		CLUSTERED
		([ServiceID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblService] SET (LOCK_ESCALATION = TABLE)
GO
