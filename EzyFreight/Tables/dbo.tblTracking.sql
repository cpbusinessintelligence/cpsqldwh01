SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTracking] (
		[TrackingD]            [int] NOT NULL,
		[ConsignmentID]        [int] NOT NULL,
		[LabelID]              [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[TrackingActivity]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TrackingDatetime]     [datetime] NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NULL,
		[UpdatedDateTTime]     [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblTracking]
		PRIMARY KEY
		CLUSTERED
		([TrackingD])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblTracking] SET (LOCK_ESCALATION = TABLE)
GO
