SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblZone] (
		[ZoneID]              [int] NOT NULL,
		[ZoneCode]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ZoneName]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[StateID]             [int] NOT NULL,
		[IsActive]            [bit] NOT NULL,
		[IsArchive]           [bit] NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [TUC_tblZone_1]
		UNIQUE
		NONCLUSTERED
		([ZoneCode])
		ON [PRIMARY],
		CONSTRAINT [PK_tblZone]
		PRIMARY KEY
		CLUSTERED
		([ZoneID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblZone]
	ADD
	CONSTRAINT [DEF_tblZone_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblZone]
	WITH NOCHECK
	ADD CONSTRAINT [FK_FK_tblState_tblZone]
	FOREIGN KEY ([StateID]) REFERENCES [dbo].[tblState] ([StateID])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[tblZone]
	NOCHECK CONSTRAINT [FK_FK_tblState_tblZone]

GO
ALTER TABLE [dbo].[tblZone] SET (LOCK_ESCALATION = TABLE)
GO
