SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblnsuranceCategory] (
		[InsuranceCategoryID]         [int] NOT NULL,
		[InsuranceCategoryCode]       [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[InsuranceCategoryDetail]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]             [datetime] NOT NULL,
		[CreatedBy]                   [int] NOT NULL,
		[UpdatedDateTime]             [datetime] NULL,
		[UpdatedBy]                   [int] NULL,
		CONSTRAINT [TUC_tblnsuranceCategory_InsuranceCide]
		UNIQUE
		NONCLUSTERED
		([InsuranceCategoryCode])
		ON [PRIMARY],
		CONSTRAINT [PK_tblnsuranceCategory]
		PRIMARY KEY
		CLUSTERED
		([InsuranceCategoryID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblnsuranceCategory] SET (LOCK_ESCALATION = TABLE)
GO
