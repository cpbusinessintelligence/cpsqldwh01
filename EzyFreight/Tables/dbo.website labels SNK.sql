SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[website labels SNK] (
		[ConsignmnetNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Driver]                [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[website labels SNK] SET (LOCK_ESCALATION = TABLE)
GO
