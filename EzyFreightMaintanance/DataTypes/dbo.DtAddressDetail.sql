CREATE TYPE [dbo].[DtAddressDetail]
AS TABLE (
		[FirstName]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastName]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyName]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address1]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address2]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsBusiness]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
