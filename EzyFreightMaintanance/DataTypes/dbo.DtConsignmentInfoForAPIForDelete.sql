CREATE TYPE [dbo].[DtConsignmentInfoForAPIForDelete]
AS TABLE (
		[ConsignmentId]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentCode]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentStagingId]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemsList]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
