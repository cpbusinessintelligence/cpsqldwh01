CREATE TYPE [dbo].[DtDeliveryChoices]
AS TABLE (
		[Category]                      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryChoiceID]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sortingcode]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryChoiceName]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryChoiceDescription]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OperationHours]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address1]                      [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address2]                      [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address3]                      [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                         [varchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country]                       [varchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedBy]                     [int] NULL,
		[Latitude]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Longtitude]                    [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsRedeliveryAvailable]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
