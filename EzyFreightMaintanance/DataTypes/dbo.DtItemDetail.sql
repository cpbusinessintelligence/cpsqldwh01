CREATE TYPE [dbo].[DtItemDetail]
AS TABLE (
		[Length]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Width]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Height]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PhysicalWeight]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Quantity]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CubicWeight]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
