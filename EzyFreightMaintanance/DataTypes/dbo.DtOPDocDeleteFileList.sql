CREATE TYPE [dbo].[DtOPDocDeleteFileList]
AS TABLE (
		[FileName]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
