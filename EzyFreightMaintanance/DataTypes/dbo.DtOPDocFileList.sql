CREATE TYPE [dbo].[DtOPDocFileList]
AS TABLE (
		[OperationDocName]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OperationDocAttachment]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
