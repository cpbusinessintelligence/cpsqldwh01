CREATE TYPE [dbo].[LabelNoArray]
AS TABLE (
		[LabelNumber]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
