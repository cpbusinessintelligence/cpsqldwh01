CREATE TYPE [dbo].[dtAddressDetail_Redirected]
AS TABLE (
		[StrAddressId]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrfirstName]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrlastName]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrCompanyName]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrEmail]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrAddress1]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrAddress2]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrSuburb]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrStateId]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrPostalCode]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrPhone]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrMobile]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrIsBusiness]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrIsRegisterAddress]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StrIsSubscribe]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryCode]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
