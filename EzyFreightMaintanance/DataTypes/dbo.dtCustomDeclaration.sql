CREATE TYPE [dbo].[dtCustomDeclaration]
AS TABLE (
		[ItemDescription]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemInBox]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnitPrice]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubTotal]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HSCode]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryofOrigin]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
