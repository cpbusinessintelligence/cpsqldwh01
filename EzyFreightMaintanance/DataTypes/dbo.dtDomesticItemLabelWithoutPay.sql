CREATE TYPE [dbo].[dtDomesticItemLabelWithoutPay]
AS TABLE (
		[strItemLabelID]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLength]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strWidth]                    [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strHeight]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCubicWeight]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strPhysicalWeight]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strMeasureWeight]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strDeclareVolume]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLastActivity]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLastActivityDateTime]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLabelNumber]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strQuantity]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strMeasureLength]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strMeasureWidth]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strMeasureHeight]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
