CREATE TYPE [dbo].[dtItemLabelCreateShipment]
AS TABLE (
		[length]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[width]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[height]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[physicalWeight]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[quantity]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCreatedBy]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLabelNumber]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strClientCode]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strServiceCode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strServiceName]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
