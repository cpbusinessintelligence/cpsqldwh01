CREATE TYPE [dbo].[dtItemLabelDHL_International]
AS TABLE (
		[strLength]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strWidth]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strHeight]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strPhysicalWeight]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Quantity]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strDHLBarCode]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLabelNumber]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CubicWeight]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryOfOrigin]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCreatedBy]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
