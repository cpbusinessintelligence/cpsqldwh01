CREATE TYPE [dbo].[dtItemLabelSing]
AS TABLE (
		[strItemLabelID]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLength]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strWidth]                    [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strHeight]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCubicWeight]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strPhysicalWeight]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strMeasureWeight]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strDeclareVolume]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLastActivity]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLastActivityDateTime]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCreatedBy]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strUpdatedBy]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLabelNumber]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryOfOrigin]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Quantity]                    [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalAmount]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HSTariffNumber]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnitValue]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
