CREATE TYPE [dbo].[dtItemLabelSing_International]
AS TABLE (
		[length]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[width]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[height]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[physicalWeight]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[quantity]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strDHLBarCode]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLabelNumber]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CubicWeight]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryofOrigin]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCreatedBy]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
