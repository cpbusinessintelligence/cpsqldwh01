CREATE TYPE [dbo].[dtItemLabel_Common_For_ALL]
AS TABLE (
		[strItemLabelID]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLength]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strWidth]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strHeight]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCubicWeight]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strPhysicalWeight]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLabelNumber]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryOfOrigin]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Quantity]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalAmount]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HSTariffNumber]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnitValue]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
