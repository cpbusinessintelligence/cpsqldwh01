CREATE TYPE [dbo].[dtProntoInvoiceList]
AS TABLE (
		[InvoiceNo]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InvoiceDate]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GrossTotal]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GST]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelSurCharge]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Insurance]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OtherCharge]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NetTotal]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalOwingAmt]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PaidAmount]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MessageFromPronto]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sola_territory]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
