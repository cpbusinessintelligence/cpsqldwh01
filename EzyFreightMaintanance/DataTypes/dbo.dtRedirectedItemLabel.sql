CREATE TYPE [dbo].[dtRedirectedItemLabel]
AS TABLE (
		[CubicWeight]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Height]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActivity]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActivityDateTime]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Length]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PhysicalWeight]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Width]                    [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
