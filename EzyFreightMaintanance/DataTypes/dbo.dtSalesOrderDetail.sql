CREATE TYPE [dbo].[dtSalesOrderDetail]
AS TABLE (
		[strDescription]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strFreightCharge]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strFuelCharge]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strInsuranceCharge]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strServiceCharge]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strCreatedBy]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strPhysicalWeight]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strDeclareVolume]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLineNo]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
