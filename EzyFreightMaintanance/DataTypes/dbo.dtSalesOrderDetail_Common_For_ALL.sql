CREATE TYPE [dbo].[dtSalesOrderDetail_Common_For_ALL]
AS TABLE (
		[strDescription]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strFreightCharge]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strFuelCharge]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strInsuranceCharge]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strServiceCharge]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strPhysicalWeight]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strDeclareVolume]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[strLineNo]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
