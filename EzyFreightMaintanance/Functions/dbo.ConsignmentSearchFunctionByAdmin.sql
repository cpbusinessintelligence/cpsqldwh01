SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ConsignmentSearchFunctionByAdmin]
(
@ConsignmentNo varchar(max)=null,
@Sender varchar(max)=null,
@Receiver varchar(max)=null,
@ConsignmentFrom date=null,
@ConsignmentTo date=null,
@ConsignmentType varchar(max)=null
)
RETURNS TABLE
AS
RETURN
(


SELECT top 100   
	
 CASE 
	 WHEN  (addPikup.Country    LIKE '%'+ @Sender +'%' OR addDest.Country    LIKE '%'+ @Receiver +'%' )  then 1 
     WHEN  (addPikup.StateID    LIKE '%'+ @Sender +'%'  or         addDest.StateID    LIKE '%'+ @Receiver +'%' )then 2 
     WHEN  (addPikup.StateName    LIKE '%'+ @Sender +'%'  or   addDest.StateName    LIKE '%'+ @Receiver +'%' )then 2 
	 WHEN  (addPikup.PostCode    LIKE '%'+ @Sender +'%'  or   addDest.PostCode    LIKE '%'+ @Receiver +'%') then 3
	 WHEN  (addPikup.Suburb    LIKE '%'+ @Sender +'%'  or   addDest.Suburb    LIKE '%'+ @Receiver +'%') then 4
     else 5 end as PikupOrder ,

	consign.*,	FORMAT(consign.CreatedDateTime,'dd/MM/yyyy HH:mm') AS CreatedDateTime1,
	addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
	addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, addDest.Country AS DestCountry, addDest.StateName AS DestStateName, addDest.CountryCode AS DestCountryCode, 
	addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName,  addPikup.Suburb AS PickSuburab, addPikup.Country AS PickCountry,  addPikup.StateName AS PickStateName, addDest.CountryCode AS PickCountryCode, 
	addPikup.PostCode AS PickPostCode,
	addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate
	,case when  tblCompany.IsRegularShipper  = 1 then 'Regular' else 'Adhoc' end as Shipper 
	
FROM tblCompany 
INNER JOIN tblCompanyUsers CU ON tblCompany.CompanyID = CU.CompanyID 
RIGHT OUTER JOIN dbo.tblConsignment AS consign  with (nolock) 
INNER JOIN dbo.tblAddress AS addPikup with (nolock)  ON consign.PickupID = addPikup.AddressID 
INNER JOIN dbo.tblAddress AS addDest  with (nolock) ON consign.DestinationID = addDest.AddressID 
INNER JOIN dbo.tblAddress AS addContct  with (nolock) ON consign.ContactID = addContct.AddressID 
LEFT OUTER JOIN dbo.tblStatus with (nolock) ON   consign.ConsignmentStatus = dbo.tblStatus.StatusID ON CU.UserID = consign.UserID
WHERE     
(consign.IsProcessed is null or consign.IsProcessed =1) and

(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateID  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Suburb  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.PostCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Country  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CountryCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) 
and
 (
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateID  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Suburb  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.PostCode  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Country  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CountryCode  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' 

) and

 (
 isnull(@ConsignmentType,'null1') = case when (consign.RateCardID = 'PEC' OR consign.RateCardID = 'REC' ) then  case when @ConsignmentType IS  null  then 'null1' else 'Saver' end  
                        when ( consign.RateCardID = 'SDC'OR consign.RateCardID = 'PDC'OR consign.RateCardID = 'CE3'OR consign.RateCardID = 'CE5'OR consign.RateCardID = 'PE3'OR consign.RateCardID = 'PE5') then case when @ConsignmentType IS  null  then 'null1' else 'ACE' end  
                        when (consign.RateCardID like 'SAV%' OR consign.RateCardID like 'EXP%') then  case when @ConsignmentType IS  null  then 'null1' else 'International' end  
                        when (consign.RateCardID = 'DAH' OR consign.RateCardID = 'DAM' OR consign.RateCardID = 'DAL' OR consign.RateCardID = 'ACH' OR consign.RateCardID = 'ACM' OR consign.RateCardID = 'ACL' OR consign.RateCardID = 'ASH' OR consign.RateCardID = 'ASM' OR consign.RateCardID = 'ASL' OR consign.RateCardID = 'DSH' OR consign.RateCardID = 'DSM' OR consign.RateCardID = 'DSL') then  case when @ConsignmentType IS  null  then 'null1' else 'Priority' end  
                        when (consign.RateCardID = 'RCH' OR consign.RateCardID = 'RCM' OR consign.RateCardID = 'RCL' OR consign.RateCardID = 'RSH' OR consign.RateCardID = 'RSM' OR consign.RateCardID = 'RSL') then  case when @ConsignmentType IS  null  then 'null1' else 'OffPeak' end  
                        
						else 'null1' 
                        end 
)

and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) 
and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)
) 
and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL  with (nolock) where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL  with (nolock) where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 )
 Order by CreatedDatetime Desc


)
GO
