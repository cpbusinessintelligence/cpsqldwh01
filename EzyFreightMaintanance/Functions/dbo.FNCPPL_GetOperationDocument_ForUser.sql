SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FNCPPL_GetOperationDocument_ForUser]
(
@Subject varchar(100) = null,
@Description varchar(100) = null,
@FromDate Date = null,
@ToDate Date = null,
@State varchar(4) = null,
@Favourite bit = 0

)
RETURNS TABLE
AS
RETURN
(

Select * 
, STUFF((SELECT ',' + OperationDocName
            FROM CPPLWeb_8_3_Admin..[tblOperationDocumentFiles] where OperationDocId = D.OperationDocId
            FOR XML PATH('')
            ), 1, 1, '') as OperationDocName
,STUFF((SELECT ',' + OperationDocAttachment
            FROM CPPLWeb_8_3_Admin..[tblOperationDocumentFiles] where OperationDocId = D.OperationDocId
            FOR XML PATH('')
            ), 1, 1, '') as OperationDocAttachment
from CPPLWeb_8_3_Admin..tblOperationDocuments D
Where (CASE WHEN @Subject IS null THEN isnull(@Subject,'') else D.OperationDocSubject  END LIKE '%'+isnull(@Subject,'')+'%' and
CASE WHEN @Description IS null THEN isnull(@Description,'') else D.OperationDocDescription  END LIKE '%'+isnull(@Description,'')+'%' and
CASE WHEN @State IS null THEN isnull(@State,'') else D.OperationDocState  END LIKE '%'+isnull(@State,'')+'%' and

(

CASE WHEN @FromDate IS null 
	THEN 
		CASE WHEN isnull( D.UpdatedDateTime ,'') <> ''
			then  
				CASE WHEN D.UpdatedDateTime > D.CreatedDateTime 
				then  CAST( D.UpdatedDateTime as DATE) 
				else  CAST( D.CreatedDateTime as DATE) 
				end 
			else CAST( D.CreatedDateTime as DATE) 
		end 
	else @FromDate END <= CASE WHEN isnull( D.UpdatedDateTime ,'') <> ''
			then  
				CASE WHEN D.UpdatedDateTime > D.CreatedDateTime 
				then  CAST( D.UpdatedDateTime as DATE) 
				else  CAST( D.CreatedDateTime as DATE) 
				end 
			else CAST( D.CreatedDateTime as DATE) 
		end 
)and 
(

CASE WHEN @ToDate IS null 
	THEN 
		CASE WHEN isnull( D.UpdatedDateTime ,'') <> ''
			then  
				CASE WHEN D.UpdatedDateTime > D.CreatedDateTime 
				then  CAST( D.UpdatedDateTime as DATE) 
				else  CAST( D.CreatedDateTime as DATE) 
				end 
			else CAST( D.CreatedDateTime as DATE) 
		end 
	else @ToDate END >= CASE WHEN isnull( D.UpdatedDateTime ,'') <> ''
			then  
				CASE WHEN D.UpdatedDateTime > D.CreatedDateTime 
				then  CAST( D.UpdatedDateTime as DATE) 
				else  CAST( D.CreatedDateTime as DATE) 
				end 
			else CAST( D.CreatedDateTime as DATE) 
		end 
)
) and
CASE WHEN @Favourite IS null THEN isnull(@Favourite,D.IsFavourite) else D.IsFavourite  END =@Favourite
)

GO
