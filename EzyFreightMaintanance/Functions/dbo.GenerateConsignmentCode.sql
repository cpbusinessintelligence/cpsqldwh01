SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[GenerateConsignmentCode]( @ConsignmentCode VARCHAR(40)= NULL)
RETURNS varchar(15)
BEGIN
DECLARE @ConsignmentCode9Digit NVARCHAR(30), @AppendConsignmentCode varchar(10) = null
select @AppendConsignmentCode = dbo.fn_scalar_appendConsignmentCode()

SELECT @ConsignmentCode9Digit = Max (RIGHT(Replace(consignmentcode,'-TEST',''), 9)) + 1 FROM   tblconsignment 
WHERE LEFT(consignmentcode, LEN(@ConsignmentCode)) = @ConsignmentCode 
SELECT @ConsignmentCode9Digit = RIGHT('000000000'+ Replace(@ConsignmentCode9Digit, '-','') , 9) 

IF( @ConsignmentCode9Digit IS NULL ) 
SET @ConsignmentCode9Digit = '000000000' 


RETURN (@ConsignmentCode9Digit + isnull(@AppendConsignmentCode,''))
END
GO
