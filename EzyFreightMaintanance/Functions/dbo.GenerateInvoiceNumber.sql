SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[GenerateInvoiceNumber]( @InvoiceNumber nvarchar(5)= NULL)
RETURNS varchar(15)
BEGIN

declare @InvoiceCode7Digit nvarchar(7)
select @InvoiceCode7Digit = max (RIGHT(InvoiceNumber, 7))+1  from tblInvoice where left(InvoiceNumber,5) = @InvoiceNumber
SELECT @InvoiceCode7Digit =  RIGHT('0000000' + replace(@InvoiceCode7Digit,'-',''),7)

if(@InvoiceCode7Digit is null)
set @InvoiceCode7Digit = '0000000'

RETURN (@InvoiceCode7Digit)
END
GO
