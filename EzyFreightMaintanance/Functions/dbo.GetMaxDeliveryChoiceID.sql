SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetMaxDeliveryChoiceID]( @RowNo int )
RETURNS varchar(15)
BEGIN
    RETURN (select isnull(max(ID),0) + @RowNo as MaxDeliveryChoiceID from [vw_DeliveryChoices])
END
GO
