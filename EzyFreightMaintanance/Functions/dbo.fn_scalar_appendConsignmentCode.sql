SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_scalar_appendConsignmentCode]()
RETURNS varchar(10) 
AS
BEGIN
declare @RetValue nvarchar(10) = null 

if EXISTS(select @@servername  WHERE @@servername LIKE 'CPSQLWEB01%')
BEGIN
	set @RetValue  = ''
END
ELSE
	set @RetValue = '-TEST'

RETURN @RetValue
END
GO
