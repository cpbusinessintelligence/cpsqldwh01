SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[GetAddressDetailByUserID]
@UserID int ,
@ConsignmentType nvarchar(50) = null
As
Begin
if(isnull(@ConsignmentType,'')='domestic')
Begin
SELECT [AddressID],REPLACE(lower(rtrim(ltrim(ISNULL(FirstName,''))) + rtrim(ltrim(ISNULL(LastName,'')))
		+ rtrim(ltrim(ISNULL(CompanyName,''))) + rtrim(ltrim(ISNULL(Email,'')))
		+ rtrim(ltrim(ISNULL(Address1,''))) + rtrim(ltrim(ISNULL(Address2,'')))
		+ rtrim(ltrim(ISNULL(Suburb,''))) + rtrim(ltrim(ISNULL(StateName,'')))+ rtrim(ltrim(ISNULL(PostCode,''))) + rtrim(ltrim(ISNULL(Phone,'')))
		+ rtrim(ltrim(ISNULL(Country,''))) + rtrim(ltrim(ISNULL(CountryCode,'')))
		),' ','') as AddressDetail
		FROM [dbo].[tblAddress]
		where [UserID] = @UserID and (IsDeleted<>1 or IsDeleted is null) and isnull(Country,'') = '' and isnull(countrycode,'') = ''
		--And IsRegisterAddress = 0
		Order by 1 desc
end
if(isnull(@ConsignmentType,'')='international')
begin

SELECT [AddressID],REPLACE(lower(rtrim(ltrim(ISNULL(FirstName,''))) + rtrim(ltrim(ISNULL(LastName,'')))
		+ rtrim(ltrim(ISNULL(CompanyName,''))) + rtrim(ltrim(ISNULL(Email,'')))
		+ rtrim(ltrim(ISNULL(Address1,''))) + rtrim(ltrim(ISNULL(Address2,'')))
		+ rtrim(ltrim(ISNULL(Suburb,''))) + rtrim(ltrim(ISNULL(StateName,'')))+ rtrim(ltrim(ISNULL(PostCode,''))) + rtrim(ltrim(ISNULL(Phone,'')))
		+ rtrim(ltrim(ISNULL(Country,''))) + rtrim(ltrim(ISNULL(CountryCode,'')))
		),' ','') as AddressDetail
		FROM [dbo].[tblAddress]
		where [UserID] = @UserID and (IsDeleted<>1 or IsDeleted is null)  and isnull([Country],'') <> '' and isnull([countrycode],'') <> ''
		--And IsRegisterAddress = 0
		Order by 1 desc


end

End
GO
