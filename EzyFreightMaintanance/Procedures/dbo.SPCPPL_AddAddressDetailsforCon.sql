SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_AddAddressDetailsforCon]

@userId int=null,
@firstName nvarchar(50)=null,
@lastName nvarchar(50)=null,
@companyName nvarchar(100)=null,
@email nvarchar(250)=null,
@address1 nvarchar(200)=null,
@address2 nvarchar(200)=null,
@suburb nvarchar(100)=null,
@stateId nvarchar(max)=null,
@postalCode int=null,
@phone nvarchar(20)=null,
@mobile int=null,
@createdBy int=null,

@isBusiness bit=false,
@isRegisterAddress bit=false,
@IsSubscribe bit=false

AS
BEGIN


DECLARE @AddID int
DECLARE @stateIdbyCode int

SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @stateId

	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe] )
	 VALUES
      ( @userId,@firstName ,@lastName ,@companyName,@email,@address1,@address2,@suburb,@stateIdbyCode,
      @postalCode,@phone,@mobile,@isRegisterAddress,GETDATE(), @createdBy ,@isBusiness,@IsSubscribe)
        SET @AddID = SCOPE_IDENTITY()
			select @AddID

END
GO
