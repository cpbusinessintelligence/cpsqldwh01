SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[SPCPPL_AddConsXMLPUByConsignmentID]
@ConsignmentStagingID int,
@XMLRequest nvarchar(max),
@XMLResponce nvarchar(max)
AS
BEGIN

declare @ConsignmentStagingIDNew int

select  @ConsignmentStagingIDNew  = 
   ConsignmentStagingID
  FROM  [dbo].[tblConsignmentStaging]
  
   where 
  [consignmentId] =@ConsignmentStagingID
  
update tblConsignmentStaging set 
XMLRequestPU=@XMLRequest,
XMLResponcePU = @XMLResponce where 
ConsignmentStagingID = @ConsignmentStagingIDNew

END
GO
