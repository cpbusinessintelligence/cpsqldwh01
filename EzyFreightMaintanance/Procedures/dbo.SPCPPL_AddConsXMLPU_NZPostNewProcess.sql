SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SPCPPL_AddConsXMLPU_NZPostNewProcess]
@ConsignmentStagingID int,
@XMLRequest nvarchar(max),
@XMLResponce nvarchar(max)
AS
BEGIN

update tblConsignmentStaging set 
XMLRequestPU_NZPost=@XMLRequest,
XMLResponcePU_NZPost = @XMLResponce where 
ConsignmentId = @ConsignmentStagingID

END
GO
