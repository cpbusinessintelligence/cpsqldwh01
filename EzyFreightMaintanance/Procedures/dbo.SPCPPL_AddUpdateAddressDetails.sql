SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_AddUpdateAddressDetails]

@AddressId int=null,
@userId       int=null,
@firstName     nvarchar(50)=null,
@lastName      nvarchar(50)=null,
@companyName   nvarchar(100)=null,
@email         nvarchar(250)=null,
@address1      nvarchar(200)=null,
@address2      nvarchar(200)=null,
@suburb       nvarchar(100)=null,
@stateId      nvarchar(max)=null,
@postalCode   int=null,
@phone        nvarchar(20)=null,
@mobile       nvarchar(20)=null,
@isBusiness bit=null,
@UpdatedBy    int=null,
@createdBy    int=null,
@isRegisterAddress bit=null,
@IsSubscribe bit=null,
 @Country  nvarchar(100)=null,
  @CountryCode   nvarchar(20)=null,
@IsEDIUser bit=null


AS
BEGIN
DECLARE @stateIdbyCode int

SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @stateId
  
if(@AddressId is null)
Begin

 IF EXISTS (SELECT * FROM [tblAddress] WHERE [UserID]=@userId and [IsRegisterAddress]=1 and @isRegisterAddress=1)
Begin
  UPDATE [dbo].[tblAddress] SET 
				 [FirstName]=case when @firstName IS null then [FirstName] else @firstName end,
				 [LastName]= case when @lastName IS null then [LastName] else @lastName end,
				 [CompanyName]=case when @companyName IS null then [CompanyName] else @companyName end,
				 [Email]= case when @email IS null then [Email] else @email end,
				 [Address1]= case when @address1 IS null then [Address1] else @address1 end,
				 [Address2]=  case when @address2 IS null then [Address2] else @address2 end,
				 [Suburb]= case when @suburb IS null then [Suburb] else @suburb end,
				 [StateID]= case when @stateIdbyCode IS null then [StateID] else @stateIdbyCode end,
				 [PostCode]= case when @postalCode IS null then [PostCode] else @postalCode end,
				 [Phone]= case when @phone IS null then [Phone] else @phone end,
				 [Mobile]= case when @mobile IS null then [Mobile] else @mobile end,
				 [isBusiness]= case when @isBusiness IS null then [isBusiness] else @isBusiness end,
				 [IsRegisterAddress] = case when @isRegisterAddress IS null then [IsRegisterAddress] else @isRegisterAddress end,
				 [IsSubscribe] = case when @IsSubscribe IS null then [IsSubscribe] else @IsSubscribe end,
				 [IsEDIUser] = case when @IsEDIUser IS null then [IsEDIUser] else @IsEDIUser end,
				 [UpdatedDateTime]=GETDATE(),
				 [UpdatedBy]=@UpdatedBy
		WHERE [UserID]=@userId and [IsRegisterAddress]=1
end
else 
begin
       	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe]
	 ,Country,CountryCode,IsEDIUser )
	 VALUES
      ( @userId,@firstName ,@lastName ,@companyName,@email,@address1,@address2,@suburb,@stateIdbyCode,
      @postalCode,@phone,@mobile,@isRegisterAddress,GETDATE(), @createdBy ,@isBusiness,@IsSubscribe
      ,@Country,@CountryCode,@IsEDIUser)
end
  
end


else
Begin
         UPDATE [dbo].[tblAddress] SET 
				 [FirstName]=case when @firstName IS null then [FirstName] else @firstName end,
				 [LastName]= case when @lastName IS null then [LastName] else @lastName end,
				 [CompanyName]=case when @companyName IS null then [CompanyName] else @companyName end,
				 [Email]= case when @email IS null then [Email] else @email end,
				 [Address1]= case when @address1 IS null then [Address1] else @address1 end,
				 [Address2]=  case when @address2 IS null then [Address2] else @address2 end,
				 [Suburb]= case when @suburb IS null then [Suburb] else @suburb end,
				 [StateID]= case when @stateIdbyCode IS null then [StateID] else @stateIdbyCode end,
				 [PostCode]= case when @postalCode IS null then [PostCode] else @postalCode end,
				 [Phone]= case when @phone IS null then [Phone] else @phone end,
				 [Mobile]= case when @mobile IS null then [Mobile] else @mobile end,
				 [isBusiness]= case when @isBusiness IS null then [isBusiness] else @isBusiness end,
				 [IsRegisterAddress] = case when @isRegisterAddress IS null then [IsRegisterAddress] else @isRegisterAddress end,
				 [IsSubscribe] = case when @IsSubscribe IS null then [IsSubscribe] else @IsSubscribe end,
				 [IsEDIUser] = case when @IsEDIUser IS null then [IsEDIUser] else @IsEDIUser end,
				 [UpdatedDateTime]=GETDATE(),
				 [UpdatedBy]=@UpdatedBy
		Where AddressId=@AddressId  
end
 
END
 
GO
