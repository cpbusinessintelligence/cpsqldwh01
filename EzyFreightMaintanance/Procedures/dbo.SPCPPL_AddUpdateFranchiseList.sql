SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_AddUpdateFranchiseList]

           @FranchiseID int =null,
           @Branch varchar(20) =null,
           @RunNo varchar(20) =null,
           @Area varchar(max) =null,
           @GrossIncome varchar(100) =null,
           @SellingPrice varchar(50) =null,
           @SellWithVan bit =null,
           @RunDescription varchar(max) =null,
           @VanDescription varchar(max) =null,
           @ContactName varchar(100) =null,
           @PhoneNo varchar(100) =null,
         
           @CreatedBy int =null,
           @UpdatedBy int =null,
           @IsArchive bit = null


AS
BEGIN
 
  
if(@FranchiseID is null)

	Begin
			  INSERT INTO [dbo].[tblFranchiseList]
				   ([Branch]
				   ,[RunNo]
				   ,[Area]
				   ,[GrossIncome]
				   ,[SellingPrice]
				   ,[SellWithVan]
				   ,[RunDescription]
				   ,[VanDescription]
				   ,[ContactName]
				   ,[PhoneNo]
				   ,[CreatedBy]
                   ,[CreatedDateTime]
                   ,[IsArchive]
					)
			 VALUES
				   (@Branch ,
				   @RunNo ,
				   @Area  ,
				   @GrossIncome  ,
				   @SellingPrice  ,
				   @SellWithVan  ,
				   @RunDescription  ,
				   @VanDescription  ,
				   @ContactName  ,
				   @PhoneNo ,
				   @CreatedBy,
				   GETDATE()
				   ,@IsArchive
				    )
	end
else
	Begin
			 UPDATE [dbo].[tblFranchiseList] SET 
					 [Branch]=case when @Branch IS null then [Branch] else @Branch end,
					 [RunNo]= case when @RunNo IS null then [RunNo] else @RunNo end,
					 [Area]=case when @Area IS null then [Area] else @Area end,
					 [GrossIncome]= case when @GrossIncome IS null then [GrossIncome] else @GrossIncome end,
					 [SellingPrice]= case when @SellingPrice IS null then [SellingPrice] else @SellingPrice end,
					 [SellWithVan]=  case when @SellWithVan IS null then [SellWithVan] else @SellWithVan end,
					 [RunDescription]= case when @RunDescription IS null then [RunDescription] else @RunDescription end,
					 [VanDescription]= case when @VanDescription  IS null then [VanDescription] else @VanDescription  end,
					 [ContactName]= case when @ContactName IS null then [ContactName] else @ContactName end,
					 [PhoneNo]= case when @PhoneNo IS null then [PhoneNo] else @PhoneNo end,
					 [IsArchive]= case when @IsArchive IS null then [IsArchive] else @IsArchive end,
					 [UpdatedDateTime]=GETDATE(),
					 [UpdatedBy]=@UpdatedBy
			Where FranchiseID=@FranchiseID
	end
 
END
 
GO
