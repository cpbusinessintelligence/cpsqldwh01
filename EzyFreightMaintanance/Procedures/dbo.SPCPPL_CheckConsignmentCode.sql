SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SPCPPL_CheckConsignmentCode]

@ConsignmentCode varchar(40) = null
 
AS
BEGIN
select top 5 * from [tblConsignment] WITH (NOLOCK)  where [ConsignmentCode] = @ConsignmentCode and IsProcessed = 1 
END
GO
