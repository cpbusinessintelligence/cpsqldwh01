SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[SPCPPL_CheckForAccess]
 @ClientCode varchar(40) = null,
 @ServiceName varchar(100) = null,
  @ServiceCode varchar(50)=null
AS
BEGIN
SELECT     CSA.ClientID, CSA.CPPLServiceID, C.ClientCode, S.ServiceCode
FROM         dbo.tblClientCPPLServiceAccess AS CSA INNER JOIN
                      dbo.tblClient AS C ON CSA.ClientID = C.ClientID INNER JOIN
                      dbo.tblCPPLServices AS S ON CSA.CPPLServiceID = S.CPPLServiceID
WHERE     (C.ClientCode = @ClientCode) AND (S.ServiceCode = @ServiceCode)AND (S.ServiceName = @ServiceName)


--INSERT INTO [dbo].[tblActivityLogging]
--           ([CPPLServiceID]
--           ,[ClientID]
--           ,[CreateDate]
--           ,[Comment])
     
--           ((SELECT  top (1)  CSA.CPPLServiceID,   CSA.ClientID,  getDate(), 'Call web Service' 
--FROM         dbo.tblClientCPPLServiceAccess AS CSA INNER JOIN
--                      dbo.tblClient AS C ON CSA.ClientID = C.ClientID INNER JOIN
--                      dbo.tblCPPLServices AS S ON CSA.CPPLServiceID = S.CPPLServiceID
--WHERE     (C.ClientCode = @ClientCode) AND (S.ServiceCode = @ServiceCode)))
END
-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
