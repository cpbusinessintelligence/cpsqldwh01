SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CheckPaidProntoInvoices]

@AccountNo varchar(50) = null ,
@dtProntoInvoice  [dtProntoInvoiceList] readonly


AS
BEGIN

Select * from tblProntoReceipt R
Inner Join tblProntoInvoice I on I.ReceiptNo = R.ReceiptNo
where AccountNo = @AccountNo and isnull(PaymentRefNo,'') <> ''  and InvoiceNo in (select InvoiceNo from @dtProntoInvoice) 
and isnull(IsPaymentProcessed,0) = 1 and isnull(IsReceiptProcessed,0) = 1


End
GO
