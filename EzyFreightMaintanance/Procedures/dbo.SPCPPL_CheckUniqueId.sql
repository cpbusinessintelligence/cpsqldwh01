SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SPCPPL_CheckUniqueId]

@UniqueID varchar(40) = null
 
AS
BEGIN
select * from [tblRedirectedConsignment] where [UniqueID] = @UniqueID and IsProcessed = 1 
END
GO
