SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[SPCPPL_ConsignmentSearchITAdmin_V2]
@ConsignmentNo varchar(max)=null,
@Sender varchar(max)=null,
@Receiver varchar(max)=null,
@ConsignmentFrom date=null,
@ConsignmentTo date=null,
@ConsignmentType varchar(max)=null

As Begin
select * from [ConsignmentSearchFunctionByAdmin] (@ConsignmentNo,@Sender,@Receiver,@ConsignmentFrom,@ConsignmentTo,@ConsignmentType)
End
GO
