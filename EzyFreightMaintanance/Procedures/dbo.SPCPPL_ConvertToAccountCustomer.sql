SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_ConvertToAccountCustomer]
@AccountNumber nvarchar(20) = null,
@CompanyName varchar(50) = null,
@ParentAccountNo varchar(20) = null,
@ABN varchar(20) = null,
@AddressID1 int = null,
@ParentCompanyName varchar(50) = null,
@BilingAccountNo nvarchar(20) = null,
@IsProntoExtracted bit = null,
@RateCardCategory nvarchar(20) = null,
@SameBillingAddress bit = null,
@BillingFname nvarchar(50) = null,
@BillingLname nvarchar(50) = null,
@BllingEmail nvarchar(50) = null,
@BillingPhone nvarchar(20) = null,
@BillingAddress nvarchar(100) = null,
@CreditLimit decimal(15,2) = null,
@CreatedBy  int = -1 ,
@IsExistingCustomer  bit = null ,
@Branch  varchar(20) = null,
@ExistingAccountNumber  varchar(20) = null,
@CouponNo  varchar(20) = null,
@AddresscompanyName nvarchar(100)=null,
@AddressisBusiness bit=0,
@UserID int = null,
@PromotionalCode nvarchar(max) = null
           
      
AS
BEGIN
begin tran
BEGIN TRY
DECLARE @stateIdbyCode int

-------------------------------------------------
if isnull(@AddressID1,0) <> 0
begin
		Update [dbo].[tblAddress] set [CompanyNAme] = @CompanyName, [isBusiness] = @AddressisBusiness,
		updatedby = @CreatedBy, updateddatetime = getdate()
		where Addressid = @AddressID1
end

DECLARE @CompanyIDret int
		
if exists(select * from [tblCompany] where [ABN] = @ABN)
begin
		select @CompanyIDret= CompanyID from [tblCompany] where [ABN] = @ABN
end
else
begin
	declare @AccountNumber9Digit nvarchar(9)

--	select @AccountNumber9Digit = max ((CompanyID))+1  from dbo.tblCompany 
--	SELECT @AccountNumber9Digit =  RIGHT('00000000' + replace(@AccountNumber9Digit,'-',''), 8)
--	SELECT @AccountNumber9Digit

--if(@AccountNumber9Digit is null)
--				set @AccountNumber9Digit = '00000000'
--				set @AccountNumber9Digit = 'W'+@AccountNumber9Digit

set @AccountNumber9Digit = null


SELECT @AccountNumber9Digit

INSERT INTO  [dbo].[tblCompany]
					   ([AccountNumber]
					   ,[CompanyName]
					   ,[ParentAccountNo]
					   ,[ABN]
					   ,[AddressID1]
					   ,[PostalAddressID]
					   ,[ParentCompanyName]
					   ,[BilingAccountNo]
					   ,[IsProntoExtracted]
					   ,[RateCardCategory]
					   ,[IntRateCardCategory]
					   ,SameBillingAddress
					   ,BillingFname
					   ,BillingLname
					   ,BllingEmail
					   ,BillingPhone
					   ,BillingAddress
					   ,CreditLimit
					   ,CreatedDateTime
					   ,CreatedBy
					   , IsExistingCustomer  
					   , Branch  
					   , ExistingAccountNumber   
					   , CouponNo  
					   )
				 VALUES
					   (
					   @AccountNumber9Digit  ,
					   @CompanyName  ,
					   @ParentAccountNo  ,
					   @ABN  ,
					   @AddressID1  ,
					   @AddressID1,			--@PostalAddressID  ,
					   @ParentCompanyName  ,
					   @BilingAccountNo  ,
					   @IsProntoExtracted  ,
					   'DEF',  
					   'DEF' 
					   ,@SameBillingAddress
					   ,@BillingFname
					   ,@BillingLname
					   ,@BllingEmail
					   ,@BillingPhone
					   ,@BillingAddress
					   ,@CreditLimit
					   ,GETDATE()
					   ,@CreatedBy 
					   ,@IsExistingCustomer  
					   ,@Branch  
					   ,@ExistingAccountNumber  
					   ,@CouponNo   
					   )
SET @CompanyIDret = SCOPE_IDENTITY()

end
            

			
------------------------------------------------------------------------------------------------

DECLARE @CompanyUsersIDret int
			INSERT INTO [dbo].[tblCompanyUsers]
           ([UserID]
           ,[CompanyID] 
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[IsUserDisabled]
           ,[PromotionalCode])
     VALUES
           (
           @UserID  ,
           @CompanyIDret ,
           GETDATE(),
           @CreatedBy ,
           1,
           @PromotionalCode)

SET @CompanyUsersIDret = SCOPE_IDENTITY()
select @CompanyUsersIDret as CompanyUsersIDret, @CompanyIDret as CompanyIDret
 
		commit tran          
END TRY

BEGIN CATCH
	begin
		rollback tran
		INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
					,[FunctionInfo]
					,[ClientId])
		VALUES
					(cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
					,'[SPCPPL_ConvertToAccountCustomer]', 2)
	end
END CATCH  
End
GO
