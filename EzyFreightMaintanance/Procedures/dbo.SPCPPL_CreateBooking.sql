SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SPCPPL_CreateBooking]
                    @PhoneNumber varchar(50) = null ,
           @ContactName varchar(50) = null ,
           @ContactEmail varchar(250) = null ,
           @PickupDate date = null ,
           @PickupTime varchar(10) = null ,
           @OrderCoupons bit = null ,
           @PickupFromCustomer bit = null ,
           @PickupName varchar(100) = null ,
           @PickupAddress1 varchar(250) = null ,
           @PickupAddress2 varchar(250) = null ,
           @PickupSuburb varchar(50) = null ,
           @ContactDetails varchar(50) = null ,
           @PickupFrom varchar(50) = null ,
           @IsProcessed bit = null ,
           @BookingRefNo varchar(50) = null ,

        
           @CreatedBy int = null,
           
           @Branch nvarchar(250) = null,
           @PickupEmail nvarchar(MAX)= null,
           @PickupPostCode nvarchar(MAX)= null,
           @Costom   nvarchar(MAX)= null,
           @ConsignmentID   int= null,
           @CostomId nvarchar(MAX)= null

				
AS
BEGIN



   

  
DECLARE @PickupAddID int

					INSERT INTO [dbo].[tblBooking]
           ([PhoneNumber]
           ,[ContactName]
           ,[ContactEmail]
           ,[PickupDate]
           ,[PickupTime]
           ,[OrderCoupons]
           ,[PickupFromCustomer]
           ,[PickupName]
           ,[PickupAddress1]
           ,[PickupAddress2]
           ,[PickupSuburb]
           ,[ContactDetails]
           ,[PickupFrom]
           ,[IsProcessed]
           ,[BookingRefNo]
           ,[CreatedDateTime]
           ,[CreatedBy]
             ,[Branch]
           ,[PickupEmail]
           ,[PickupPostCode]
			,[Costom]  
			,[CostomId]
           ,[ConsignmentID]
          )
     VALUES
           (@PhoneNumber,
           @ContactName, 
           @ContactEmail,
           @PickupDate, 
           @PickupTime, 
           @OrderCoupons,
           @PickupFromCustomer,
           @PickupName, 
           @PickupAddress1, 
           @PickupAddress2, 
           @PickupSuburb, 
           @ContactDetails, 
           @PickupFrom,
           @IsProcessed, 
           @BookingRefNo, 
           GETDATE(), 
           @CreatedBy ,
            @Branch,
           @PickupEmail,
           @PickupPostCode,
			@Costom  ,
			@CostomId ,
			@ConsignmentID
           
           )
      SET @PickupAddID = SCOPE_IDENTITY()
			select @PickupAddID
				
End




GO
