SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SPCPPL_CreateBookingRP]
                    @PhoneNumber nvarchar(50) = null ,
           @ContactName varchar(50) = null ,
           @ContactEmail varchar(250) = null ,
           @PickupDate date = null ,
           @PickupTime varchar(10) = null ,
           @OrderCoupons bit = null ,
           @PickupFromCustomer bit = null ,
           @PickupName varchar(100) = null ,
          @PickupAddress1 varchar(250) = null ,
           @PickupAddress2 varchar(250) = null ,
           @PickupSuburb varchar(50) = null ,
           @ContactDetails varchar(50) = null ,
           @PickupFrom varchar(50) = null ,
           @IsProcessed bit = null ,
           @BookingRefNo varchar(50) = null ,

           @PickupPhoneNo nvarchar(50) = null,
           @CreatedBy int = null,
           
           @Branch nvarchar(250) = null,
           @PickupEmail nvarchar(MAX)= null,
           @PickupPostCode nvarchar(MAX)= null
           --,
           --@CompanyName nvarchar(MAX)= null
AS
BEGIN

--declare @ENCustPhoneNo int
--declare @ENCustBranch nvarchar(250)
--declare @ENCustFirstName nvarchar(250)
--declare @ENCustLastname nvarchar(250)
--declare @ENCustEmail nvarchar(250)

--   select @ENCustPhoneNo=PhoneNo ,
--   @ENCustBranch = Branch ,
--   @ENCustFirstName =FirstName,
--   @ENCustLastname=Lastname,
--   @ENCustEmail=Email
--    from tblEzyNetCustomer where [PhoneNo] = @PhoneNumber

  
DECLARE @PickupAddID int

					INSERT INTO [dbo].[tblBooking]
           ([PhoneNumber]
           ,[ContactName]
           ,[ContactEmail]
           ,[PickupDate]
           ,[PickupTime]
           ,[OrderCoupons]
           ,[PickupFromCustomer]
           ,[PickupName]
            ,[PickupAddress1]
           ,[PickupAddress2]
           ,[PickupSuburb]
           ,[ContactDetails]
           ,[PickupFrom]
           ,[IsProcessed]
           ,[BookingRefNo]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[Branch]
           ,[PickupEmail]
           ,[PickupPostCode]
      --     ,[ENCustPhoneNo]
      --,[ENCustBranch]
      --,[ENCustFirstName]
      --,[ENCustLastname]
      --,[ENCustEmail]
      ,[PickupPhoneNo]
      --,[CompanyName]
          )
     VALUES
           (@PhoneNumber,
           @ContactName, 
           @ContactEmail,
           @PickupDate, 
           @PickupTime, 
           @OrderCoupons,
           @PickupFromCustomer,
           @PickupName, 
            @PickupAddress1, 
           @PickupAddress2, 
           @PickupSuburb, 
           @ContactDetails, 
           @PickupFrom,
           @IsProcessed, 
           @BookingRefNo, 
           GETDATE(), 
           @CreatedBy ,
           @Branch,
           @PickupEmail,
           @PickupPostCode,
      --     @ENCustPhoneNo
      --,@ENCustBranch
      --,@ENCustFirstName
      --,@ENCustLastname
      --,@ENCustEmail
      @PickupPhoneNo
      --,
      --@CompanyName
           )
      SET @PickupAddID = SCOPE_IDENTITY()
			select @PickupAddID
				
End
GO
