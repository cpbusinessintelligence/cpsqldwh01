SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateCommunity]
            @CommunityInitiative nvarchar(max)  = null ,
            @StartDate date  = null ,
            @EndDate date  = null ,
            @FirstName nvarchar(max)  = null ,
            @LastName nvarchar(max)  = null ,
            @CompanyName nvarchar(max)  = null ,
            @Email nvarchar(max)  = null ,
            @Address nvarchar(max)  = null ,
          @Suburb varchar(100)  = null ,
           @State nvarchar(max) = null ,
           @PostCode varchar(10)  = null ,
            @Mobile nvarchar(max)  = null ,
            @Website nvarchar(max)  = null ,
            @PurposeOfInitiative nvarchar(max)  = null ,
            @WorkTogether nvarchar(max)  = null 
           
AS
BEGIN

   DECLARE @stateIdbyCode int
SELECT top 1  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @State

   

INSERT INTO [dbo].[tblCommunity]
           ([CommunityInitiative]
           ,[StartDate]
           ,[EndDate]
           ,[FirstName]
           ,[LastName]
           ,[CompanyName]
           ,[Email]
           ,[Address]
           ,[Suburb]
           ,[State]
           ,[PostCode]
           ,[Mobile]
           ,[Website]
           ,[PurposeOfInitiative]
           ,[WorkTogether]
         )
     VALUES
           (            @CommunityInitiative ,
            @StartDate ,
            @EndDate   ,
            @FirstName ,
            @LastName ,
            @CompanyName ,
            @Email ,
            @Address ,
             @Suburb    ,
           @stateIdbyCode   ,
           @PostCode   ,
            @Mobile ,
            @Website ,
            @PurposeOfInitiative ,
            @WorkTogether 
           )
				
End
GO
