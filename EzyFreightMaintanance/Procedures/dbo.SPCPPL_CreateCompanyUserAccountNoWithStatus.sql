SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


create  PROCEDURE [dbo].[SPCPPL_CreateCompanyUserAccountNoWithStatus]
        
@UserID int = null,
@ReasonSubject    nvarchar (max)=null,
@ReasonDesciption nvarchar (max)=null
           
      
AS
BEGIN
begin tran

 
			
				DECLARE @CompanyIDret int
		
 
                declare @CompanyID int 
 select @CompanyID =   CompanyID    from tblCompanyUsers where UserID =  @UserID
 
 
 update tblCompanyUsers 
 set IsUserDisabled = 0
 where UserID =  @UserID
 
 print @companyid


                declare @AccountNumber9Digit nvarchar(10)
 select @AccountNumber9Digit = @CompanyID
SELECT @AccountNumber9Digit =  RIGHT('00000000' + replace(@AccountNumber9Digit,'-',''), 8)
if(@AccountNumber9Digit is null)
set @AccountNumber9Digit = '00000000'
set @AccountNumber9Digit = 'WD'+@AccountNumber9Digit
 
 
 SELECT @AccountNumber9Digit
            	update   [dbo].[tblCompany] set
           [AccountNumber]= @AccountNumber9Digit where CompanyID  = @CompanyID
			
			
 
 
 INSERT INTO  [dbo].[tblCompanyStatusLog]
           ([CompanyID]
           ,[CompanyStatus]
           ,[StatusReason]
           ,[MoreDetails]
           ,[CreatedDateTime]
           ,[CreatedBy]
            )
     VALUES
           (@CompanyID
           ,1
           ,@ReasonSubject   
           ,@ReasonDesciption 
           ,getdate()
           ,@UserID
            )
 
 
 
 
		 
			 if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateCompanyUserAccountN]'
    , 2)
  end
  else
  commit tran          
            

END



GO
