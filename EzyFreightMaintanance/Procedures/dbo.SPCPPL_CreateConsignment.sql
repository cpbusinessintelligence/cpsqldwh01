SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[SPCPPL_CreateConsignment]
           @ConsignmentCode varchar(40)= null,
           @UserID int= null,
           @IsRegUserConsignment bit= null,
           --@PickupID int= null,
           --@DestinationID int= null,
           --@ContactID int= null,
           @TotalWeight decimal(4,2)= null,
           @TotalMeasureWeight decimal(4,2)= null,
           @TotalVolume decimal(4,2)= null,
           @TotalMeasureVolume decimal(4,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @LastActivity varchar(200)= null,
           @LastActiivityDateTime datetime= null,
           @ConsignmentStatus int= null,
           @EDIDataProcessed bit= null,
           @ProntoDataExtracted bit= null,

           @CreatedBy int= null,
      
           
           @PickupuserId int=null,
@PickupfirstName nvarchar(50)=null,
@PickuplastName nvarchar(50)=null,
@PickupcompanyName nvarchar(100)=null,
@Pickupemail nvarchar(250)=null,
@Pickupaddress1 nvarchar(200)=null,
@Pickupaddress2 nvarchar(200)=null,
@Pickupsuburb nvarchar(100)=null,
@PickupstateId int=null,
@PickuppostalCode int=null,
@Pickupphone int=null,
@Pickupmobile int=null,
@PickupcreatedBy int=null,
@PickupisBusiness bit=false,
@PickupisRegisterAddress bit=false,
           
           @DestinationuserId int=null,
@DestinationfirstName nvarchar(50)=null,
@DestinationlastName nvarchar(50)=null,
@DestinationcompanyName nvarchar(100)=null,
@Destinationemail nvarchar(250)=null,
@Destinationaddress1 nvarchar(200)=null,
@Destinationaddress2 nvarchar(200)=null,
@Destinationsuburb nvarchar(100)=null,
@DestinationstateId int=null,
@DestinationpostalCode int=null,
@Destinationphone int=null,
@Destinationmobile int=null,
@DestinationcreatedBy int=null,
@DestinationisBusiness bit=false,
@DestinationisRegisterAddress bit=false,

@ContactuserId int=null,
@ContactfirstName nvarchar(50)=null,
@ContactlastName nvarchar(50)=null,
@ContactcompanyName nvarchar(100)=null,
@Contactemail nvarchar(250)=null,
@Contactaddress1 nvarchar(200)=null,
@Contactaddress2 nvarchar(200)=null,
@Contactsuburb nvarchar(100)=null,
@ContactstateId int=null,
@ContactpostalCode int=null,
@Contactphone int=null,
@Contactmobile int=null,
@ContactcreatedBy int=null,
@ContactisBusiness bit=false,
@ContactisRegisterAddress bit=false

           
AS
BEGIN

-- begin trans
--BEGIN TRANSACTION
BEGIN TRAN

BEGIN TRY

   

  
DECLARE @PickupAddID int

						INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness] )
	 VALUES
      ( @PickupuserId,@PickupfirstName ,@PickuplastName ,@PickupcompanyName,@Pickupemail,@Pickupaddress1,@Pickupaddress2,@Pickupsuburb,@PickupstateId,
      @PickuppostalCode,@Pickupphone,@Pickupmobile,@PickupisRegisterAddress,GETDATE(), @PickupcreatedBy ,@PickupisBusiness)
      SET @PickupAddID = SCOPE_IDENTITY()
			select @PickupAddID
				
DECLARE @DestinationAddID int
			
				INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness] )
	 VALUES
      ( @DestinationuserId,@DestinationfirstName ,@DestinationlastName ,@DestinationcompanyName,@Destinationemail,@Destinationaddress1,@Destinationaddress2,@Destinationsuburb,@DestinationstateId,
      @DestinationpostalCode,@Destinationphone,@Destinationmobile,@DestinationisRegisterAddress,GETDATE(), @DestinationcreatedBy ,@DestinationisBusiness)
			  SET @DestinationAddID = SCOPE_IDENTITY()
			select @DestinationAddID
			
		
		DECLARE @ContactAddID int
			INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness] )
	 VALUES
      ( @ContactuserId,@ContactfirstName ,@ContactlastName ,@ContactcompanyName,@Contactemail,@Contactaddress1,@Contactaddress2,@Contactsuburb,@ContactstateId,
      @ContactpostalCode,@Contactphone,@Contactmobile,@ContactisRegisterAddress,GETDATE(), @ContactcreatedBy ,@ContactisBusiness)
			  SET @ContactAddID = SCOPE_IDENTITY()
			select @ContactAddID
			
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].[tblConsignment]
           ([ConsignmentCode]
           ,[UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalMeasureWeight]
           ,[TotalVolume]
           ,[TotalMeasureVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[ConsignmentPreferPickupDate]
           ,[ConsignmentPreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[LastActivity]
           ,[LastActiivityDateTime]
           ,[ConsignmentStatus]
           ,[EDIDataProcessed]
           ,[ProntoDataExtracted]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
     VALUES
           (@ConsignmentCode,
           @UserID,
           @IsRegUserConsignment,
           @PickupAddID,
           @DestinationAddID,
           @ContactAddID,
           @TotalWeight,
           @TotalMeasureWeight,
           @TotalVolume,
           @TotalMeasureVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           @LastActivity,
           @LastActiivityDateTime,
           @ConsignmentStatus,
           @EDIDataProcessed,
           @ProntoDataExtracted,
           GETDATE(),
           @CreatedBy) SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret
			
-- check for error
--IF @@ERROR <> 0 
--	GOTO ErrorHandler

---- commit transaction, and exit
--COMMIT TRANSACTION
--RETURN 

---- Error Handler
--ErrorHandler:

---- see if transaction is open
--IF @@TRANCOUNT > 0 
--BEGIN
---- rollback tran
--ROLLBACK TRANSACTION
--END

--ROLLBACK TRANSACTION
---- set failure values
--RETURN -1


 COMMIT TRAN

END TRY
BEGIN CATCH

  ROLLBACK TRAN
select -1
END CATCH
END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
