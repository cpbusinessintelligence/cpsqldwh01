SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[SPCPPL_CreateConsignmentStaging]
           @UserID int= null,
           @IsRegUserConsignment bit= null,
           @PickupID int= null,
           @DestinationID int= null,
           @ContactID int= null,
           @TotalWeight decimal(4,2)= null,
           @TotalVolume decimal(4,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @CreatedBy int= null,
           @ServiceID int= null

           
AS
BEGIN


			
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].tblConsignmentStaging
           (
            [UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[PickupDate]
           ,[PreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[ServiceID]
           ,[IsProcessed]
           )
     VALUES
           (
           @UserID,
           @IsRegUserConsignment,
              @PickupID,
           @DestinationID,
           @ContactID,
           @TotalWeight,
           @TotalVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           GETDATE(),
           @CreatedBy,
           @ServiceID,
           0
           ) SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret

END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
