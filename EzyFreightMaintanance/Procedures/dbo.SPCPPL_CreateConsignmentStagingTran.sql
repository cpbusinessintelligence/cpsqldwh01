SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateConsignmentStagingTran]
@UserID int= null,
@IsRegUserConsignment bit= null,
@TotalWeight decimal(19,4)= null,
@TotalVolume decimal(19,4)= null,
@NoOfItems int= null,
@SpecialInstruction varchar(500)= null,
@CustomerRefNo varchar(40)= null,
@ConsignmentPreferPickupDate date= null,
@ConsignmentPreferPickupTime varchar(20)= null,
@ClosingTime varchar(10)= null,
@DangerousGoods bit= null,
@Terms bit= null,
@RateCardID nvarchar(50)= null,
@CreatedBy int= null,
@ServiceID int= null,
--------------------------------------------------Pickup
@PickupAddressId int=null,
@PickupuserId int=null,
@PickupfirstName nvarchar(50)=null,
@PickuplastName nvarchar(50)=null,
@PickupcompanyName nvarchar(100)=null,
@Pickupemail nvarchar(250)=null,
@Pickupaddress1 nvarchar(200)=null,
@Pickupaddress2 nvarchar(200)=null,
@Pickupsuburb nvarchar(100)=null,
@PickupstateId nvarchar(max)=null,
@PickuppostalCode nvarchar(20)=null,
@Pickupphone nvarchar(20)=null,
@Pickupmobile nvarchar(20)=null,
@PickupcreatedBy int=null,
@PickupisBusiness bit=0,
@PickupisRegisterAddress bit=0,
@PickupIsSubscribe bit=0,
--------------------------------------------------Destination
@DestinationAddressId int=null,
@DestinationuserId int=null,
@DestinationfirstName nvarchar(50)=null,
@DestinationlastName nvarchar(50)=null,
@DestinationcompanyName nvarchar(100)=null,
@Destinationemail nvarchar(250)=null,
@Destinationaddress1 nvarchar(200)=null,
@Destinationaddress2 nvarchar(200)=null,
@Destinationsuburb nvarchar(100)=null,
@DestinationstateId nvarchar(max)=null,
@DestinationpostalCode nvarchar(20)=null,
@Destinationphone nvarchar(20)=null,
@Destinationmobile nvarchar(20)=null,
@DestinationcreatedBy int=null,
@DestinationisBusiness bit=0,
@DestinationisRegisterAddress bit=0,
@DestinationIsSubscribe bit=0,
--------------------------------------------------Contact
@ContactAddressId int=null,
@ContactuserId int=null,
@ContactfirstName nvarchar(50)=null,
@ContactlastName nvarchar(50)=null,
@ContactcompanyName nvarchar(100)=null,
@Contactemail nvarchar(250)=null,
@Contactaddress1 nvarchar(200)=null,
@Contactaddress2 nvarchar(200)=null,
@Contactsuburb nvarchar(100)=null,
@ContactstateId nvarchar(max)=null,
@ContactpostalCode nvarchar(20)=null,
@Contactphone nvarchar(20)=null,
@Contactmobile nvarchar(20)=null,
@ContactcreatedBy int=null,
@ContactisBusiness bit=0,
@ContactisRegisterAddress bit=0,
@ContactIsSubscribe bit=0,


@IsSameAsDestination bit = 0,
@IsSameAsPickup bit = 0,

@IsSignatureReq bit = 0,
@IsDocument nvarchar(max) = null,
@NatureOfGoods nvarchar(max) = null,
@SortCode nvarchar(max) = null,
@ETA nvarchar(max) = null,
@IsATL               bit = null,
@GrossTotal decimal(18,2) = null,
@GST  decimal(18,2)  = null,
@PromotionCode varchar(6) = null
      
AS
BEGIN
Begin Try
begin tran

DECLARE @stateIdbyCode int

----------------------------------------Pickup----------------------------------------------------------

SELECT  @stateIdbyCode=[StateID] FROM [dbo].[tblState]where [StateCode] = @PickupstateId
DECLARE @PickupAddID int 

if (isnull(@PickupAddressId,'')<>'')
begin
set @PickupAddID = @PickupAddressId 
end
else

begin
	INSERT INTO [dbo].[tblAddress]
		 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
			[PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[stateName] )
	VALUES
		  ( @PickupuserId,@PickupfirstName ,@PickuplastName ,@PickupcompanyName,@Pickupemail,@Pickupaddress1,@Pickupaddress2,@Pickupsuburb, @stateIdbyCode ,
		@PickuppostalCode,@Pickupphone,@Pickupmobile,@PickupisRegisterAddress,GETDATE(), @PickupcreatedBy ,@PickupisBusiness,@PickupIsSubscribe,@PickupstateId)
SET @PickupAddID = SCOPE_IDENTITY()
end


----------------------------------------Destination----------------------------------------------------------
SELECT  @stateIdbyCode=[StateID]
      
FROM [dbo].[tblState]where [StateCode] = @DestinationstateId
DECLARE @DestinationAddID int

if (isnull(@DestinationAddressId,'')<>'')
begin
Set @DestinationAddID = @DestinationAddressId 
end
else

begin
	INSERT INTO [dbo].[tblAddress]
		 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
		[PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[stateName] )
	VALUES
		( @DestinationuserId,@DestinationfirstName ,@DestinationlastName ,@DestinationcompanyName,@Destinationemail,@Destinationaddress1,@Destinationaddress2,@Destinationsuburb, @stateIdbyCode ,
		@DestinationpostalCode,@Destinationphone,@Destinationmobile,@DestinationisRegisterAddress,GETDATE(), @DestinationcreatedBy ,@DestinationisBusiness,@DestinationIsSubscribe,@DestinationstateId)
SET @DestinationAddID = SCOPE_IDENTITY()
end



----------------------------------------Contact----------------------------------------------------------

DECLARE @ContactAddID int

IF (@IsSameAsDestination=1)
   set @ContactAddID = @DestinationAddID
ELSE 
   BEGIN
      IF (@IsSameAsPickup=1)
   set @ContactAddID = @PickupAddID
   ELSE
        begin
        SELECT  @stateIdbyCode=[StateID] FROM [dbo].[tblState]where [StateCode] = @ContactstateId

if (isnull(@ContactAddressId,'')<>'')
begin
set @ContactAddID = @ContactAddressId 
end
else

begin
	INSERT INTO [dbo].[tblAddress]
		( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
		[PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[stateName])
	VALUES
		( @ContactuserId,@ContactfirstName ,@ContactlastName ,@ContactcompanyName,@Contactemail,@Contactaddress1,@Contactaddress2,@Contactsuburb,@stateIdbyCode ,
		@ContactpostalCode,@Contactphone,@Contactmobile,@ContactisRegisterAddress,GETDATE(), @ContactcreatedBy ,@ContactisBusiness,@ContactIsSubscribe,@ContactstateId)
SET @ContactAddID = SCOPE_IDENTITY()
end
end
END ;

---------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE @ConsignmentIDret int
INSERT INTO [dbo].tblConsignmentStaging
           (
            [UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[PickupDate]
           ,[PreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[ServiceID]
           ,[IsProcessed]
            ,[IsDocument]
           ,IsSignatureReq
           ,NatureOfGoods
           ,SortCode
           ,ETA
           ,IsATL
           ,CalculatedTotal
           ,CalculatedGST 
		   ,PromotionCode
           )
VALUES
           (
           @UserID,
           @IsRegUserConsignment,
             @PickupAddID,
           @DestinationAddID,
           @ContactAddID,
           @TotalWeight,
           @TotalVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           GETDATE(),
           @CreatedBy,
           @ServiceID,
           0,
          @IsDocument   ,
        @IsSignatureReq    
        ,@NatureOfGoods
        ,@SortCode
        ,@ETA
        ,@IsATL
        ,@GrossTotal
        ,@GST 
		,@PromotionCode
           ) SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret
			
   COMMIT TRAN 

   END TRY
		BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SPCPPL_CreateConsignmentStagingTran', 2)
		end
		END CATCH
            
			

END
GO
