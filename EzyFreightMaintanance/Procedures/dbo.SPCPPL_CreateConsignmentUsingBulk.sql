SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateConsignmentUsingBulk] 
  
  --@TotalMeasureWeight decimal(10,2)= null, 
  --@TotalMeasureVolume decimal(10,2)= null, 
  @ProntoDataExtracted   BIT= NULL, 
  @LastActivity          VARCHAR(200)= NULL, 
  @LastActiivityDateTime DATETIME= NULL, 
  @EDIDataProcessed      BIT= NULL, 
  --@ConsignmentStatus int= null, 
  @ConsignmentCode       VARCHAR(40)= NULL, 
  @ConsignmentStagingID  INT= NULL, 
  @GrossTotal            DECIMAL(19, 4), 
  @GST                   DECIMAL(19, 4), 
  @NetTotal              DECIMAL(19, 4), 
  @dtItemLabel           DTITEMLABEL readonly, 
  @dtSalesOrderDetail    DTSALESORDERDETAIL readonly, 
  @TotalFreightExGST     DECIMAL(19, 4) = NULL, 
  @TotalFreightInclGST   DECIMAL(19, 4) = NULL, 
  @InvoiceStatus         INT = NULL, 
  @SendToPronto          BIT = NULL, 
  @PaymentRefNo          NVARCHAR(100)=NULL, 
  @merchantReferenceCode NVARCHAR(max)=NULL, 
  @SubscriptionID        NVARCHAR(max)=NULL, 
  @AuthorizationCode     NVARCHAR(100)=NULL 
AS 
  BEGIN 
  BEGIN TRY
      BEGIN TRAN 

      DECLARE @UserID                      INT= NULL, 
              @IsRegUserConsignment        BIT= NULL, 
              @PickupID                    INT= NULL, 
              @DestinationID               INT= NULL, 
              @ContactID                   INT= NULL, 
              @TotalWeight                 DECIMAL(10, 2)= NULL, 
              @TotalVolume                 DECIMAL(10, 2)= NULL, 
              @NoOfItems                   INT= NULL, 
              @SpecialInstruction          VARCHAR(500)= NULL, 
              @CustomerRefNo               VARCHAR(40)= NULL, 
              @ConsignmentPreferPickupDate DATE= NULL, 
              @ConsignmentPreferPickupTime VARCHAR(20)= NULL, 
              @ClosingTime                 VARCHAR(10)= NULL, 
              @DangerousGoods              BIT= NULL, 
              @Terms                       BIT= NULL, 
              @RateCardID                  NVARCHAR(50)= NULL, 
              @CreatedBy                   INT= NULL, 
              @IsSignatureReq              BIT = 0, 
              @IsATl                       BIT=NULL, 
              @IsDocument                  BIT = 0, 
              @SortCode                    NVARCHAR(max) = NULL, 
              @NatureOfGoods               NVARCHAR(max) = NULL, 
              @ETA                         NVARCHAR(max) = NULL 

      SELECT @UserID = [userid], 
             @IsRegUserConsignment = [isreguserconsignment], 
             @PickupID = [pickupid], 
             @DestinationID = [destinationid], 
             @ContactID = [contactid], 
             @TotalWeight = [totalweight], 
             @TotalVolume = [totalvolume], 
             @NoOfItems = [noofitems], 
             @SpecialInstruction = [specialinstruction], 
             @CustomerRefNo = [customerrefno], 
             @ConsignmentPreferPickupDate = [pickupdate], 
             @ConsignmentPreferPickupTime = [preferpickuptime], 
             @ClosingTime = [closingtime], 
             @DangerousGoods = [dangerousgoods], 
             @Terms = [terms], 
             @RateCardID = [ratecardid], 
             @IsATl = [isatl], 
             @CreatedBy = [createdby], 
             @IsSignatureReq = [issignaturereq], 
             @IsDocument = [isdocument], 
             @SortCode = [sortcode], 
             @NatureOfGoods = natureofgoods, 
             @ETA = [eta] 
      FROM   [dbo].[tblconsignmentstaging] 
      WHERE  consignmentstagingid = @ConsignmentStagingID 

DECLARE @ConsignmentCode9Digit NVARCHAR(9) 

      SELECT @ConsignmentCode9Digit = Max (RIGHT(consignmentcode, 9)) + 1 
      FROM   tblconsignment WHERE  LEFT(consignmentcode, Len(@ConsignmentCode)) = @ConsignmentCode 
	  --WHERE  LEFT(consignmentcode, 6) = @ConsignmentCode 

      SELECT @ConsignmentCode9Digit = RIGHT('000000000' + Replace(@ConsignmentCode9Digit, '-', '') , 9) 

      --SELECT @ConsignmentCode9Digit 
      IF( @ConsignmentCode9Digit IS NULL ) 
        SET @ConsignmentCode9Digit = '000000000' 


      DECLARE @ConsignmentIDret INT 

      INSERT INTO [dbo].[tblconsignment] 
                  ([consignmentcode], 
                   [userid], 
                   [isreguserconsignment], 
                   [pickupid], 
                   [destinationid], 
                   [contactid], 
                   [totalweight] 
                   --,[TotalMeasureWeight] 
                   , 
                   [totalvolume] 
                   --,[TotalMeasureVolume] 
                   , 
                   [noofitems], 
                   [specialinstruction], 
                   [customerrefno], 
                   [consignmentpreferpickupdate], 
                   [consignmentpreferpickuptime], 
                   [closingtime], 
                   [dangerousgoods], 
                   [terms], 
                   [ratecardid], 
                   [lastactivity], 
                   [lastactiivitydatetime], 
                   [consignmentstatus], 
                   [edidataprocessed], 
                   [prontodataextracted], 
                   [createddatetime], 
                   [createdby], 
                   [isdocument], 
                   issignaturereq, 
                   sortcode, 
                   natureofgoods, 
                   eta, 
                   [isatl]) 
      VALUES      (@ConsignmentCode + @ConsignmentCode9Digit, 
                   @UserID, 
                   @IsRegUserConsignment, 
                   @PickupID, 
                   @DestinationID, 
                   @ContactID, 
                   @TotalWeight, 
                   --@TotalMeasureWeight, 
                   @TotalVolume, 
                   --@TotalMeasureVolume, 
                   @NoOfItems, 
                   @SpecialInstruction, 
                   @CustomerRefNo, 
                   @ConsignmentPreferPickupDate, 
                   @ConsignmentPreferPickupTime, 
                   @ClosingTime, 
                   @DangerousGoods, 
                   @Terms, 
                   @RateCardID, 
                   @LastActivity, 
                   @LastActiivityDateTime, 
                   1, 
                   0, 
                   0, 
                   Getdate(), 
                   @CreatedBy, 
                   @IsDocument, 
                   @IsSignatureReq, 
                   @SortCode, 
                   @NatureOfGoods, 
                   @ETA, 
                   @IsATl ) 

      SET @ConsignmentIDret = Scope_identity() 

      SELECT @ConsignmentIDret 

      UPDATE dbo.tblconsignmentstaging 
      SET    [isprocessed] = 1, 
             [updateddatettime] = Getdate(), 
             [updatedby] = @CreatedBy, 
             [consignmentid] = @ConsignmentIDret 
      WHERE  consignmentstagingid = @ConsignmentStagingID 

      DECLARE @SalesOrderIDret INT 

      INSERT INTO [dbo].[tblsalesorder] 
                  ([referenceno], 
                   [userid], 
                   [noofitems], 
                   [totalweight], 
                   [totalvolume], 
                   [ratecardid], 
                   [grosstotal], 
                   [gst], 
                   [nettotal], 
                   [salesorderstatus], 
                   [invoiceno], 
                   [createddatetime], 
                   [createdby]) 
      VALUES      ( @ConsignmentIDret, 
                    @UserID, 
                    @NoofItems, 
                    @TotalWeight, 
                    @TotalVolume, 
                    @RateCardID, 
                    @GrossTotal, 
                    @GST, 
                    @NetTotal, 
                    7, 
                    NULL, 
                    Getdate(), 
                    @UserID ) 

      SET @SalesOrderIDret = Scope_identity() 

      SELECT @SalesOrderIDret 

      SELECT [firstname], 
             [address1], 
             [address2], 
             [suburb], 
             [postcode] 
      FROM   [dbo].[tbladdress] 
      WHERE  [addressid] = @PickupID 

      SELECT [firstname], 
             [address1], 
             [address2], 
             [suburb], 
             [postcode] 
      FROM   [dbo].[tbladdress] 
      WHERE  [addressid] = @DestinationID 

      SELECT @ConsignmentPreferPickupDate 

      SELECT @PickupID, 
             @DestinationID, 
             @ContactID 

      SELECT * 
      FROM   @dtItemLabel 

      SELECT * 
      FROM   @dtSalesOrderDetail 

      ---------ItemLabel ------------------------ 
      INSERT INTO [dbo].[tblitemlabel] 
                  ([consignmentid], 
                   [labelnumber], 
                   [length], 
                   [width], 
                   [height], 
                   [cubicweight], 
                   [physicalweight], 
                   [measureweight], 
                   [declarevolume], 
                   [lastactivity], 
                   [lastactivitydatetime], 
                   [createddatetime], 
                   [createdby]) 
      SELECT @ConsignmentIDret, 
             @ConsignmentCode + @ConsignmentCode9Digit 
             + Cast( strlabelnumber AS NVARCHAR), 
             --case when strLength='' then null else  cast(strLength as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strlength) = 1 THEN 
               Cast(strlength AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strwidth) = 1 THEN 
               Cast(strwidth AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strheight) = 1 THEN 
               Cast(strheight AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strcubicweight) = 1 THEN Cast( 
               strcubicweight AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strphysicalweight) = 1 THEN Cast( 
               strphysicalweight AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strMeasureWeight='' then null else    cast(  strMeasureWeight as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strmeasureweight) = 1 THEN Cast( 
               strmeasureweight AS 
               DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strDeclareVolume='' then null else  cast(  strDeclareVolume as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strdeclarevolume) = 1 THEN Cast( 
               strdeclarevolume AS 
               DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             strlastactivity, 
             strlastactivitydatetime, 
             Getdate(), 
             strcreatedby 
      FROM   @dtItemLabel; 

      ---------SalesOrderDetail ------------------------ 
      INSERT INTO [dbo].[tblsalesorderdetail] 
                  ([salesorderid], 
                   [description], 
                   [lineno], 
                   [weight], 
                   [volume], 
                   [freightcharge], 
                   [fuelcharge], 
                   [insurancecharge], 
                   [servicecharge], 
                   [total], 
                   [createddatetime], 
                   [createdby]) 
      SELECT @SalesOrderIDret, 
             strdescription, 
             [strlineno], 
             --case when strPhysicalWeight='' then null else    cast( strPhysicalWeight as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strphysicalweight) = 1 THEN Cast( 
               strphysicalweight AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strDeclareVolume='' then null else    cast(  strDeclareVolume as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strdeclarevolume) = 1 THEN Cast( 
               strdeclarevolume AS 
               DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strFreightCharge='' then null else    cast(  strFreightCharge as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strfreightcharge) = 1 THEN Cast( 
               strfreightcharge AS 
               DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strFuelCharge='' then null else    cast(  strFuelCharge as decimal(19,4))end, 
             CASE 
               WHEN Isnumeric(strfuelcharge) = 1 THEN Cast(strfuelcharge AS 
                                                           DECIMAL(19, 4)) 
               ELSE NULL 
             END, 
             --case when strInsuranceCharge='' then null else    cast(   strInsuranceCharge as decimal(19,4))end,
             CASE 
               WHEN Isnumeric(strinsurancecharge) = 1 THEN Cast( 
               strinsurancecharge AS DECIMAL(19, 4)) 
               ELSE NULL 
             END, 
             --case when strServiceCharge='' then null else    cast(    strServiceCharge as decimal(19,4))end,
             CASE 
               WHEN Isnumeric(strservicecharge) = 1 THEN Cast( 
               strservicecharge AS 
               DECIMAL(19, 4)) 
               ELSE NULL 
             END, 
             --cast (strFreightCharge as decimal(19,4)) + 
             CASE WHEN Isnumeric(strfreightcharge)=1 THEN Cast(strfreightcharge 
             AS 
             DECIMAL(19 
             , 4))ELSE 0.0 END + 
             --cast (strFuelCharge as decimal(19,4))+ 
             CASE WHEN Isnumeric(strfuelcharge)=1 THEN Cast(strfuelcharge AS 
             DECIMAL( 
             19, 4)) 
             ELSE 0.0 END + 
             --cast (strInsuranceCharge as decimal(19,4))+ 
             CASE WHEN Isnumeric(strinsurancecharge)=1 THEN Cast( 
             strinsurancecharge AS 
             DECIMAL(19, 4))ELSE 0.0 END + 
             --cast (strServiceCharge as decimal(19,4)), 
             CASE WHEN Isnumeric(strservicecharge)=1 THEN Cast(strservicecharge 
             AS 
             DECIMAL(19 
             , 4))ELSE 0.0 END, 
             Getdate(), 
             strcreatedby 
      FROM   @dtSalesOrderDetail 

      ---------Invoice ------------------------ 
      DECLARE @InvoiceNumber NVARCHAR(5) 

      SET @InvoiceNumber= Substring(@ConsignmentCode, 1, 3) + 'IN' 

      DECLARE @InvoiceCode7Digit NVARCHAR(7) 

      SELECT @InvoiceCode7Digit = Max (RIGHT(invoicenumber, 7)) + 1 
      FROM   tblinvoice 
      WHERE  LEFT(invoicenumber, 5) = @InvoiceNumber 

      SELECT @InvoiceCode7Digit = RIGHT('0000000' 
                                        + Replace(@InvoiceCode7Digit, '-', ''), 
                                  7) 

      --SELECT @ConsignmentCode9Digit 
      IF( @InvoiceCode7Digit IS NULL ) 
        SET @InvoiceCode7Digit = '0000000' 

      --select @UserId5Digit+@ConsignmentCode8Digit 
      DECLARE @InvoiceID INT 

      INSERT INTO [dbo].[tblinvoice] 
                  ([invoicenumber], 
                   [userid], 
                   [pickupaddressid], 
                   [destinationaddressid], 
                   [contactaddressid], 
                   [totalfreightexgst], 
                   [gst], 
                   [totalfreightinclgst], 
                   [invoicestatus], 
                   [sendtopronto], 
                   [createddatetime], 
                   [createdby], 
                   [paymentrefno], 
                   [authorizationcode], 
                   [merchantreferencecode], 
                   [subscriptionid]) 
      VALUES      ( @InvoiceNumber + @InvoiceCode7Digit, 
                    @UserID, 
                    @PickupID, 
                    @DestinationID, 
                    @ContactID, 
                    --@TotalFreightExGST ,  
                    @GrossTotal, 
                    @GST, 
                    --@TotalFreightInclGST ,  
                    @NetTotal, 
                    11, 
                    @SendToPronto, 
                    Getdate(), 
                    @CreatedBY, 
                    @PaymentRefNo, 
                    @AuthorizationCode, 
                    @merchantReferenceCode, 
                    @SubscriptionID ) 

      SET @InvoiceID = Scope_identity() 

      --------Update Salse order ------- 
      UPDATE dbo.tblconsignmentstaging 
      SET    [paymentrefno] = @PaymentRefNo 
      WHERE  consignmentstagingid = @ConsignmentStagingID 

      --------Update Salse order ------- 
      UPDATE tblsalesorder 
      SET    invoiceno = @InvoiceNumber + @InvoiceCode7Digit, 
             salesorderstatus = 9 
      WHERE  salesorderid = @SalesOrderIDret 

      SELECT dbo.tblinvoice.* 
      FROM   dbo.tblinvoice 
      WHERE  dbo.tblinvoice.invoiceid = @InvoiceID 

      SELECT dbo.tblconsignment.* 
      FROM   dbo.tblconsignment 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentIDret 

      SELECT dbo.tblsalesorderdetail.*, 
             dbo.tblsalesorder.* 
      FROM   dbo.tblsalesorderdetail 
             INNER JOIN dbo.tblsalesorder 
                     ON dbo.tblsalesorderdetail.salesorderid = 
                        dbo.tblsalesorder.salesorderid 
      WHERE  ( dbo.tblsalesorder.referenceno = @ConsignmentIDret ) 

      DECLARE @PicupID INT 

      --DECLARE @DestinationID int 
      SELECT @PicupID = dbo.tblconsignment.pickupid, 
             @DestinationID = dbo.tblconsignment.destinationid 
      FROM   dbo.tblconsignment 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentIDret 

      SELECT @PicupID = dbo.tblconsignment.pickupid, 
             @DestinationID = dbo.tblconsignment.destinationid 
      FROM   dbo.tblconsignment 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentIDret 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             dbo.tblstate.statename AS StateID 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @PicupID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             dbo.tblstate.statename AS StateID 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @DestinationID 

      SELECT * 
      FROM   [dbo].[tblitemlabel] 
      WHERE  consignmentid = @ConsignmentIDret 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             dbo.tblstate.statename AS StateID 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @ContactID 

      --IF( @@ERROR <> 0 ) 
      --  BEGIN 
      --      ROLLBACK TRAN 

      --      INSERT INTO [dbo].[tblerrorlog] 
      --                  ([error], 
      --                   [functioninfo], 
      --                   [clientid]) 
      --      VALUES      (Error_line() + ' : ' + Error_message(), 
      --                   'Spcppl_createconsignmentusingbulk', 
      --                   2) 
      --  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
      --  END 
      --ELSE 

        COMMIT TRAN 

		END TRY
		BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'Spcppl_createconsignmentusingbulk'
				   , 2)
		end
		END CATCH
  END 
GO
