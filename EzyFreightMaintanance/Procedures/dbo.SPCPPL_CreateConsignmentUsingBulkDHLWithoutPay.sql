SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateConsignmentUsingBulkDHLWithoutPay] 
  
  @ProntoDataExtracted            BIT= NULL, 
  @LastActivity                   VARCHAR(200)= NULL, 
  @LastActiivityDateTime          DATETIME= NULL, 
  @EDIDataProcessed               BIT= NULL, 
  @ConsignmentCode                VARCHAR(40)= NULL, 
  @ConsignmentStagingID           INT= NULL, 
  @GrossTotal                     DECIMAL(19, 4), 
  @GST                            DECIMAL(19, 4), 
  @NetTotal                       DECIMAL(19, 4), 
  @dtItemLabel                    DTITEMLABELDHL readonly, 
  @dtCustomDeclaration            DTCUSTOMDECLARATION readonly, 
  @TotalFreightExGST              DECIMAL(19, 4) = NULL, 
  @TotalFreightInclGST            DECIMAL(19, 4) = NULL, 
  @InvoiceStatus                  INT = NULL, 
  @SendToPronto                   BIT = NULL, 
  @PaymentRefNo                   NVARCHAR(100)=NULL, 
  @merchantReferenceCode          NVARCHAR(max)=NULL, 
  @SubscriptionID                 NVARCHAR(max)=NULL, 
  @AuthorizationCode              NVARCHAR(100)=NULL, 
  @InvoiceImage                   VARCHAR(max)=NULL, 
  @LabelImage                     VARCHAR(max)=NULL, 
  @ItemCodeSing                   VARCHAR(max)=NULL, 
  @AWBBarCode                     VARCHAR(max)=NULL, 
  @OriginDestnBarcode             VARCHAR(max)=NULL, 
  @ClientIDBarCode                VARCHAR(max)=NULL, 
  @DHLRoutingBarCode              VARCHAR(max)=NULL, 
  @AWBCode                        VARCHAR(max)=NULL, 
  @OriginDestncode                VARCHAR(max)=NULL, 
  @ClientIDCode                   VARCHAR(max)=NULL, 
  @DHLRoutingCode                 VARCHAR(max)=NULL, 
  @CountryServiceAreaFacilityCode VARCHAR(max)=NULL, 
  @OriginServiceAreaCode          VARCHAR(max)=NULL, 
  @ProductShortName               VARCHAR(max)=NULL, 
  @InternalServiceCode            VARCHAR(max)=NULL, 
  @NetSubTotal                    DECIMAL(10, 2)=NULL, 
  @NatureOfGoods                  NVARCHAR(max)=NULL 
AS 
  BEGIN 
  BEGIN TRY
      BEGIN TRAN 

      DECLARE @UserID                      INT= NULL, 
              @IsRegUserConsignment        BIT= NULL, 
              @PickupID                    INT= NULL, 
              @DestinationID               INT= NULL, 
              @ContactID                   INT= NULL, 
              @TotalWeight                 DECIMAL(19, 4)= NULL, 
              @TotalVolume                 DECIMAL(19, 4)= NULL, 
              @NoOfItems                   INT= NULL, 
              @SpecialInstruction          VARCHAR(500)= NULL, 
              @CustomerRefNo               VARCHAR(40)= NULL, 
              @ConsignmentPreferPickupDate DATE= NULL, 
              @ConsignmentPreferPickupTime VARCHAR(20)= NULL, 
              @ClosingTime                 VARCHAR(10)= NULL, 
              @DangerousGoods              BIT= NULL, 
              @Terms                       BIT= NULL, 
              @RateCardID                  NVARCHAR(50)= NULL, 
              @CreatedBy                   INT= NULL, 
              @IsSignatureReq              BIT = 0, 
              @IsDocument                  NVARCHAR(max) = NULL, 
              @IfUndelivered               NVARCHAR(max)=NULL, 
              @ReasonForExport             NVARCHAR(max)=NULL, 
              @TypeOfExport                NVARCHAR(max)=NULL, 
              @Currency                    NVARCHAR(max)=NULL, 
              @IsInsurance                 BIT=NULL, 
              @IdentityNo                  NVARCHAR(max)=NULL, 
              @IdentityType                NVARCHAR(max)=NULL, 
              @IsIdentity                  BIT=NULL, 
              @IsATl                       BIT=NULL, 
              @IsReturnToSender            BIT=NULL, 
              @HasReadInsuranceTc          BIT=NULL, 
              @SortCode                    NVARCHAR(max) = NULL, 
              @ETA                         NVARCHAR(max) = NULL, 
              @InsuranceAmount             DECIMAL(10, 2) = NULL 

      SELECT @UserID = [userid], 
             @IsRegUserConsignment = [isreguserconsignment], 
             @PickupID = [pickupid], 
             @DestinationID = [destinationid], 
             @ContactID = [contactid], 
             @TotalWeight = [totalweight], 
             @TotalVolume = [totalvolume], 
             @NoOfItems = [noofitems], 
             @SpecialInstruction = [specialinstruction], 
             @CustomerRefNo = [customerrefno], 
             @ConsignmentPreferPickupDate = [pickupdate], 
             @ConsignmentPreferPickupTime = [preferpickuptime], 
             @ClosingTime = [closingtime], 
             @DangerousGoods = [dangerousgoods], 
             @Terms = [terms], 
             @RateCardID = [ratecardid], 
             @CreatedBy = [createdby], 
             @IsSignatureReq = [issignaturereq], 
             @IsDocument = [isdocument], 
             @IfUndelivered = [ifundelivered], 
             @ReasonForExport = [reasonforexport], 
             @TypeOfExport = [typeofexport], 
             @Currency = [currency], 
             @IsInsurance = [isinsurance], 
             @IdentityNo = [identityno], 
             @IdentityType = [identitytype], 
             @IsIdentity = [isidentity], 
             @IsATl = [isatl], 
             @IsReturnToSender = [isreturntosender], 
             @HasReadInsuranceTc = [hasreadinsurancetc], 
             @SortCode = [sortcode], 
             @NatureOfGoods = natureofgoods, 
             @ETA = [eta], 
             @InsuranceAmount = insuranceamount 
      FROM   [dbo].[tblconsignmentstaging] 
      WHERE  consignmentstagingid = @ConsignmentStagingID 


declare @GeneratedConsignmentCode nvarchar(30) 
ConsignmentCodeGeneration:
Set @GeneratedConsignmentCode = @ConsignmentCode + [dbo].GenerateConsignmentCode(@ConsignmentCode)

if exists(select * from tblconsignment where [ConsignmentCode] = @GeneratedConsignmentCode )
begin
Goto ConsignmentCodeGeneration;
end

------------Commented By Shubham 06/02/2017  Created New Function to get the consignment code

		--SELECT @ConsignmentCode9Digit = Max (RIGHT(consignmentcode, 9)) + 1 
		--FROM   tblconsignment WHERE  LEFT(consignmentcode, LEN(@ConsignmentCode)) = @ConsignmentCode 

		--SELECT @ConsignmentCode9Digit = RIGHT( 
		--'000000000' + Replace(@ConsignmentCode9Digit , '-', '') , 9) 
		--IF( @ConsignmentCode9Digit IS NULL ) 
		--SET @ConsignmentCode9Digit = '000000000' 
        


      
DECLARE @ConsignmentIDret INT 

      INSERT INTO [dbo].[tblconsignment] 
                  ([consignmentcode], 
                   [userid], 
                   [isreguserconsignment], 
                   [pickupid], 
                   [destinationid], 
                   [contactid], 
                   [totalweight] , 
                   [totalvolume] , 
                   [noofitems], 
                   [specialinstruction], 
                   [customerrefno], 
                   [consignmentpreferpickupdate], 
                   [consignmentpreferpickuptime], 
                   [closingtime], 
                   [dangerousgoods], 
                   [terms], 
                   [ratecardid], 
                   [lastactivity], 
                   [lastactiivitydatetime], 
                   [consignmentstatus], 
                   [edidataprocessed], 
                   [prontodataextracted], 
                   [createddatetime], 
                   [createdby], 
                   [isinternational], 
                   [isdocument], 
                   issignaturereq, 
                   [ifundelivered], 
                   [reasonforexport], 
                   [typeofexport], 
                   [currency], 
                   [isinsurance], 
                   [identityno], 
                   [identitytype], 
                   [isidentity], 
                   [country-servicearea-facilitycode], 
                   [internalservicecode], 
                   [netsubtotal], 
                   [isatl], 
                   [isreturntosender], 
                   [hasreadinsurancetc], 
                   [natureofgoods], 
                   [originserviceareacode], 
                   [productshortname], 
                   [sortcode], 
                   eta, 
                   isaccountcustomer, 
                   insuranceamount, 
                   calculatedtotal, 
                   calculatedgst, 
                   [isprocessed]) 
      VALUES      (@GeneratedConsignmentCode, --@ConsignmentCode + @ConsignmentCode9Digit, 
                   @UserID, 
                   @IsRegUserConsignment, 
                   @PickupID, 
                   @DestinationID, 
                   @ContactID, 
                   @TotalWeight, 
                   @TotalVolume, 
                   @NoOfItems, 
                   @SpecialInstruction, 
                   @CustomerRefNo, 
                   @ConsignmentPreferPickupDate, 
                   @ConsignmentPreferPickupTime, 
                   @ClosingTime, 
                   @DangerousGoods, 
                   @Terms, 
                   @RateCardID, 
                   @LastActivity, 
                   @LastActiivityDateTime, 
                   1, 
                   0, 
                   0, 
                   Getdate(), 
                   @CreatedBy, 
                   1, 
                   @IsDocument, 
                   @IsSignatureReq, 
                   @IfUndelivered, 
                   @ReasonForExport, 
                   @TypeOfExport, 
                   @Currency, 
                   @IsInsurance, 
                   @IdentityNo, 
                   @IdentityType, 
                   @IsIdentity, 
                   @CountryServiceAreaFacilityCode, 
                   @InternalServiceCode, 
                   @NetSubTotal, 
                   @IsATl, 
                   @IsReturnToSender, 
                   @HasReadInsuranceTc, 
                   @NatureOfGoods, 
                   @OriginServiceAreaCode, 
                   @ProductShortName, 
                   @SortCode, 
                   @ETA, 
                   1, 
                   @InsuranceAmount, 
                   @GrossTotal, 
                   @GST, 
                   1 ) 

      SET @ConsignmentIDret = Scope_identity() 

      ---------ItemLabel ------------------------ 
      INSERT INTO [dbo].[tblitemlabel] 
                  ([consignmentid], 
                   [labelnumber], 
                   [length], 
                   [width], 
                   [height], 
                   [cubicweight], 
                   [physicalweight], 
                   [measureweight], 
                   [declarevolume], 
                   [lastactivity], 
                   [lastactivitydatetime], 
                   [createddatetime], 
                   [createdby], 
                   [countryoforigin], 
                   [description], 
                   [hstariffnumber], 
                   [quantity], 
                   [unitvalue], 
                   [dhlbarcode]) 
      SELECT @ConsignmentIDret, 
             --@ItemCodeSing, 
             Cast(strlabelnumber AS NVARCHAR), 
             --case when strLength='' then null else  cast(strLength as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strlength) = 1 THEN 
               Cast(strlength AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strwidth) = 1 THEN 
               Cast(strwidth AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strheight) = 1 THEN 
               Cast(strheight AS DECIMAL(10, 2)) 
               ELSE NULL 
             END, 
             --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strcubicweight) = 1 THEN Cast( 
               strcubicweight AS DECIMAL(10, 4)) 
               ELSE NULL 
             END, 
             --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strphysicalweight) = 1 THEN Cast( 
               strphysicalweight AS DECIMAL(10, 4)) 
               ELSE NULL 
             END, 
             --case when strMeasureWeight='' then null else    cast(  strMeasureWeight as decimal(10,2))end,
             CASE 
               WHEN Isnumeric(strmeasureweight) = 1 THEN Cast( 
               strmeasureweight AS 
               DECIMAL(10, 4)) 
               ELSE NULL 
             END, 
             --case when strDeclareVolume='' then null else  cast(  strDeclareVolume as decimal(10,2))end, 
             CASE 
               WHEN Isnumeric(strdeclarevolume) = 1 THEN Cast( 
               strdeclarevolume AS 
               DECIMAL(10, 4)) 
               ELSE NULL 
             END, 
             strlastactivity, 
             strlastactivitydatetime, 
             Getdate(), 
             strcreatedby, 
             [countryoforigin], 
             [description], 
             [hstariffnumber], 
             [quantity], 
             [unitvalue], 
             [strdhlbarcode] 
      FROM   @dtItemLabel; 

      --------------------------------Item Image ---------------------- 
      DECLARE @ItemLabelID INT= NULL 

      SELECT @ItemLabelID = itemlabelid 
      FROM   [tblitemlabel] 
      WHERE  [consignmentid] = @ConsignmentIDret 

      INSERT INTO [dbo].[tblitemlabelimage] 
                  ([itemlabelid], 
                   [lableimage], 
                   [createdby]) 
      VALUES      (@ItemLabelID, 
                   @LabelImage, 
                   @UserID ) 

      ---------CustomDeclaration ------------------------ 
      INSERT INTO [dbo].tblcustomdeclaration 
                  ([consignmentid], 
                   itemdescription, 
                   iteminbox, 
                   unitprice, 
                   subtotal, 
                   hscode, 
                   countryoforigin, 
                   currency, 
                   createdby) 
      SELECT @ConsignmentIDret, 
             itemdescription, 
             iteminbox, 
             unitprice, 
             subtotal, 
             hscode, 
             countryoforigin, 
             currency, 
             @UserID 
      FROM   @dtCustomDeclaration 

      -----------------------insert [tblDHLBarCodeImage] ------- 
      IF( @AWBBarCode IS NOT NULL ) 
        BEGIN 
            INSERT INTO [dbo].[tbldhlbarcodeimage] 
                        ([consignmentid], 
                         [awbcode], 
                         [awbbarcode], 
                         [origindestncode], 
                         [origindestnbarcode], 
                         [clientidcode], 
                         [clientidbarcode], 
                         [dhlroutingcode], 
                         [dhlroutingbarcode], 
                         [createdby]) 
            VALUES      (@ConsignmentIDret, 
                         @AWBCode, 
                         @AWBBarCode, 
                         @OriginDestncode, 
                         @OriginDestnBarcode, 
                         @ClientIDCode, 
                         @ClientIDBarCode, 
                         @DHLRoutingCode, 
                         @DHLRoutingBarCode, 
                         @UserID) 
        END 

      ------------------------------------------------------------- 
      --------------------------------------------------------------------------------------------- 
      DECLARE @ConsignmentId INT = @ConsignmentIDret 

      --------------------------------------------Commented By Shubham 16/05/2016-------------------------- 
      --UPDATE dbo.tblConsignment 
      --SET [IsProcessed] =1, 
      --   [UpdatedDateTTime] = getDate(), 
      --   [UpdatedBy] = @UserID 
      --WHERE ConsignmentID = @ConsignmentId 
      ----------------------------------------------------------------------------------------------------------- 
      UPDATE dbo.[tblinvoice] 
      SET    [paymentrefno] = @PaymentRefNo, 
             [authorizationcode] = @AuthorizationCode, 
             [merchantreferencecode] = @merchantReferenceCode, 
             [subscriptionid] = @SubscriptionID, 
             [updateddatetime] = Getdate(), 
             [updatedby] = @UserID 
      WHERE  invoicenumber = (SELECT TOP 1 invoiceno 
                              FROM   dbo.tblsalesorder 
                              WHERE  referenceno = @ConsignmentId) 

      --------------------------------------------------Commented By Shubham 16/05/2016------------------------------------------ 
      --UPDATE dbo.tblConsignmentStaging 
      --  SET [IsProcessed] =1, 
      --     [UpdatedDateTTime] = getDate(), 
      --     [UpdatedBy] = @CreatedBy      , 
      --     [ConsignmentId]=@ConsignmentIDret, 
      --     IsAccountCustomer=1 
      --WHERE ConsignmentStagingID = @ConsignmentStagingID 
      ------------------------------------------Update Consignment Staging -------------------------------------------------------- 
      UPDATE dbo.tblconsignmentstaging 
      SET    [isprocessed] = 1, 
             [consignmentid] = @ConsignmentIDret, 
             isaccountcustomer = 1, 
             [paymentrefno] = @PaymentRefNo, 
             [authorizationcode] = @AuthorizationCode, 
             [merchantreferencecode] = @merchantReferenceCode, 
             [subscriptionid] = @SubscriptionID, 
             [updateddatettime] = Getdate(), 
             [updatedby] = @UserID 
      WHERE  consignmentstagingid = @ConsignmentStagingID 

      ----------------------------------------------------------------------------------------------------------------  
      DECLARE @PicupID INT 

      --SELECT     dbo.tblInvoice.* 
      --FROM         dbo.tblConsignment INNER JOIN 
      --                      dbo.tblSalesOrder ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo INNER JOIN
      --                      dbo.tblInvoice ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
      --                       where dbo.tblConsignment.ConsignmentID = @ConsignmentID 
      SELECT dbo.tblconsignment.*, 
             dbo.tblconsignment.[country-servicearea-facilitycode] AS 
             Country_ServiceArea_FacilityCode 
      FROM   dbo.tblconsignment 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentID 

      --SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.* 
      --FROM         dbo.tblSalesOrderDetail INNER JOIN 
      --                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
      --WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID) 
      SELECT @PicupID = dbo.tblconsignment.pickupid, 
             @DestinationID = dbo.tblconsignment.destinationid, 
             @ContactID = dbo.tblconsignment.contactid 
      FROM   dbo.tblconsignment 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @PicupID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @DestinationID 

      SELECT Cu.userid, 
             c.isregularshipper 
      FROM   tblcompany c 
             INNER JOIN tblcompanyusers Cu 
                     ON c.companyid = Cu.companyid 
      WHERE  ( c.isregularshipper = 1 ) 
             AND Cu.userid = @UserID 

   COMMIT TRAN 


		END TRY
		BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'Spcppl_createconsignmentusingbulkdhlwithoutpay', 2)
		end
		END CATCH
  END 

GO
