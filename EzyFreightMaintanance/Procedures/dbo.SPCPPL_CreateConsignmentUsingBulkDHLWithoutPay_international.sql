SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateConsignmentUsingBulkDHLWithoutPay_international] 
@ProntoDataExtracted            BIT= NULL,
@LastActivity                   VARCHAR(200)= NULL,
@LastActiivityDateTime          DATETIME= NULL,
@EDIDataProcessed               BIT= NULL,
@ConsignmentCode                VARCHAR(40)= NULL,
@ConsignmentStagingID           INT= NULL,
@GrossTotal                     DECIMAL(19, 4) = NULL,
@GST                            DECIMAL(19, 4) =NULL,
@NetTotal                       DECIMAL(19, 4)=NULL,
@dtItemLabel                    DTITEMLABELDHL_INTERNATIONAL readonly,
@dtCustomDeclaration            DTCUSTOMDECLARATION_INTERNATIONAL readonly,
@TotalFreightExGST              DECIMAL(19, 4) = NULL,
@TotalFreightInclGST            DECIMAL(19, 4) = NULL,
@InvoiceStatus                  INT = NULL,
@SendToPronto                   BIT = NULL,
@PaymentRefNo                   NVARCHAR(100)=NULL,
@merchantReferenceCode          NVARCHAR(max)=NULL,
@SubscriptionID                 NVARCHAR(max)=NULL,
@AuthorizationCode              NVARCHAR(100)=NULL,
@InvoiceImage                   VARCHAR(max)=NULL,
@LabelImage                     VARCHAR(max)=NULL,
@ItemCodeSing                   VARCHAR(max)=NULL,
@AWBBarCode                     VARCHAR(max)=NULL,
@OriginDestnBarcode             VARCHAR(max)=NULL,
@ClientIDBarCode                VARCHAR(max)=NULL,
@DHLRoutingBarCode              VARCHAR(max)=NULL,
@AWBCode                        VARCHAR(max)=NULL,
@OriginDestncode                VARCHAR(max)=NULL,
@ClientIDCode                   VARCHAR(max)=NULL,
@DHLRoutingCode                 VARCHAR(max)=NULL,
@CountryServiceAreaFacilityCode VARCHAR(max)=NULL,
@OriginServiceAreaCode          VARCHAR(max)=NULL,
@ProductShortName               VARCHAR(max)=NULL,
@InternalServiceCode            VARCHAR(max)=NULL,
@NetSubTotal                    DECIMAL(10, 2)=NULL,
@NatureOfGoods                  NVARCHAR(max)=NULL,
@ClientCode                     VARCHAR(50) = NULL
AS
  BEGIN
      BEGIN try
          BEGIN TRAN

          DECLARE @UserID                      INT= NULL,
                  @IsRegUserConsignment        BIT= NULL,
                  @PickupID                    INT= NULL,
                  @DestinationID               INT= NULL,
                  @ContactID                   INT= NULL,
                  @TotalWeight                 DECIMAL(19, 4)= NULL,
                  @TotalVolume                 DECIMAL(19, 4)= NULL,
                  @NoOfItems                   INT= NULL,
                  @SpecialInstruction          VARCHAR(500)= NULL,
                  @CustomerRefNo               VARCHAR(40)= NULL,
                  @ConsignmentPreferPickupDate DATE= NULL,
                  @ConsignmentPreferPickupTime VARCHAR(20)= NULL,
                  @ClosingTime                 VARCHAR(10)= NULL,
                  @DangerousGoods              BIT= NULL,
                  @Terms                       BIT= NULL,
                  @RateCardID                  NVARCHAR(50)= NULL,
                  @CreatedBy                   INT= NULL,
                  @IsSignatureReq              BIT = 0,
                  @IsDocument                  NVARCHAR(max) = NULL,
                  @IfUndelivered               NVARCHAR(max)=NULL,
                  @ReasonForExport             NVARCHAR(max)=NULL,
                  @TypeOfExport                NVARCHAR(max)=NULL,
                  @Currency                    NVARCHAR(max)=NULL,
                  @IsInsurance                 BIT=NULL,
                  @IdentityNo                  NVARCHAR(max)=NULL,
                  @IdentityType                NVARCHAR(max)=NULL,
                  @IsIdentity                  BIT=NULL,
                  @IsATl                       BIT=NULL,
                  @IsReturnToSender            BIT=NULL,
                  @HasReadInsuranceTc          BIT=NULL,
                  @SortCode                    NVARCHAR(max) = NULL,
                  @ETA                         NVARCHAR(max) = NULL,
                  @InsuranceAmount             DECIMAL(10, 2) = NULL

          SELECT @UserID = [userid],
                 @IsRegUserConsignment = [isreguserconsignment],
                 @PickupID = [pickupid],
                 @DestinationID = [destinationid],
                 @ContactID = [contactid],
                 @TotalWeight = [totalweight],
                 @TotalVolume = [totalvolume],
                 @NoOfItems = [noofitems],
                 @SpecialInstruction = [specialinstruction],
                 @CustomerRefNo = [customerrefno],
                 @ConsignmentPreferPickupDate = [pickupdate],
                 @ConsignmentPreferPickupTime = [preferpickuptime],
                 @ClosingTime = [closingtime],
                 @DangerousGoods = [dangerousgoods],
                 @Terms = [terms],
                 @RateCardID = [ratecardid],
                 @CreatedBy = [createdby],
                 @IsSignatureReq = [issignaturereq],
                 @IsDocument = [isdocument],
                 @IfUndelivered = [ifundelivered],
                 @ReasonForExport = [reasonforexport],
                 @TypeOfExport = [typeofexport],
                 @Currency = [currency],
                 @IsInsurance = [isinsurance],
                 @IdentityNo = [identityno],
                 @IdentityType = [identitytype],
                 @IsIdentity = [isidentity],
                 @IsATl = [isatl],
                 @IsReturnToSender = [isreturntosender],
                 @HasReadInsuranceTc = [hasreadinsurancetc],
                 @SortCode = [sortcode],
                 @NatureOfGoods = natureofgoods,
                 @ETA = [eta],
                 @InsuranceAmount = insuranceamount
          FROM   [dbo].[TBLCONSIGNMENTSTAGING]
          WHERE  consignmentstagingid = @ConsignmentStagingID

DECLARE @GeneratedConsignmentCode nvarchar(30)
ConsignmentCodeGeneration:
Set @GeneratedConsignmentCode = @ConsignmentCode + [dbo].GenerateConsignmentCode(@ConsignmentCode)

if exists(select * from tblconsignment where [ConsignmentCode] = @GeneratedConsignmentCode )
begin
Goto ConsignmentCodeGeneration;
end


------------Commented By Shubham 06/02/2017  Created New Function to get the consignment code
		--SELECT @ConsignmentCode9Digit = Max (RIGHT(consignmentcode, 9)) + 1
		--FROM   TBLCONSIGNMENT
		--WHERE  LEFT( consignmentcode, Len( @ConsignmentCode ) ) = @ConsignmentCode

		--SELECT @ConsignmentCode9Digit = RIGHT( '000000000'
		--+ Replace(@ConsignmentCode9Digit, '-', ''), 9 )

		--IF( @ConsignmentCode9Digit IS NULL )
		--SET @ConsignmentCode9Digit = '000000000'


          DECLARE @ConsignmentIDret INT

          INSERT INTO [dbo].[TBLCONSIGNMENT]
                      ([consignmentcode],
                       [userid],
                       [isreguserconsignment],
                       [pickupid],
                       [destinationid],
                       [contactid],
                       [totalweight],
                       [totalvolume],
                       [noofitems],
                       [specialinstruction],
                       [customerrefno],
                       [consignmentpreferpickupdate],
                       [consignmentpreferpickuptime],
                       [closingtime],
                       [dangerousgoods],
                       [terms],
                       [ratecardid],
                       [lastactivity],
                       [lastactiivitydatetime],
                       [consignmentstatus],
                       [edidataprocessed],
                       [prontodataextracted],
                       [createddatetime],
                       [createdby],
                       [isinternational],
                       [isdocument],
                       issignaturereq,
                       [ifundelivered],
                       [reasonforexport],
                       [typeofexport],
                       [currency],
                       [isinsurance],
                       [identityno],
                       [identitytype],
                       [isidentity],
                       [country-servicearea-facilitycode],
                       [internalservicecode],
                       [netsubtotal],
                       [isatl],
                       [isreturntosender],
                       [hasreadinsurancetc],
                       [natureofgoods],
                       [originserviceareacode],
                       [productshortname],
                       [sortcode],
                       eta,
                       isaccountcustomer,
                       insuranceamount,
                       calculatedtotal,
                       calculatedgst,
                       clientcode,
                       [isprocessed])
          VALUES      (@GeneratedConsignmentCode , --@ConsignmentCode + @ConsignmentCode9Digit,
                       @UserID,
                       @IsRegUserConsignment,
                       @PickupID,
                       @DestinationID,
                       @ContactID,
                       @TotalWeight,
                       @TotalVolume,
                       @NoOfItems,
                       @SpecialInstruction,
                       @CustomerRefNo,
                       @ConsignmentPreferPickupDate,
                       @ConsignmentPreferPickupTime,
                       @ClosingTime,
                       @DangerousGoods,
                       @Terms,
                       @RateCardID,
                       @LastActivity,
                       @LastActiivityDateTime,
                       1,
                       0,
                       0,
                       Getdate( ),
                       @CreatedBy,
                       1,
                       @IsDocument,
                       @IsSignatureReq,
                       @IfUndelivered,
                       @ReasonForExport,
                       @TypeOfExport,
                       @Currency,
                       @IsInsurance,
                       @IdentityNo,
                       @IdentityType,
                       @IsIdentity,
                       @CountryServiceAreaFacilityCode,
                       @InternalServiceCode,
                       @NetSubTotal,
                       @IsATl,
                       @IsReturnToSender,
                       @HasReadInsuranceTc,
                       @NatureOfGoods,
                       @OriginServiceAreaCode,
                       @ProductShortName,
                       @SortCode,
                       @ETA,
                       1,
                       @InsuranceAmount,
                       @GrossTotal,
                       @GST,
                       @ClientCode,
                       1 )

          SET @ConsignmentIDret = Scope_identity( )

--------------ItemLabel ------------------------

          INSERT INTO [dbo].[TBLITEMLABEL]
                      ([consignmentid],
                       [labelnumber],
                       [length],
                       [width],
                       [height],
                       [cubicweight],
                       [physicalweight],
                       [createddatetime],
                       [createdby],
                       [countryoforigin],
                       [quantity],
                       [dhlbarcode])
          SELECT @ConsignmentIDret,
                 --@ItemCodeSing,

                 Cast( strlabelnumber AS NVARCHAR ),
                 --case when strLength='' then null else  cast(strLength as decimal(10,2))end,

                 CASE
                   WHEN Isnumeric( strlength ) = 1 THEN Cast( strlength AS DECIMAL(10, 2))
                   ELSE NULL
                 END,
                 --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end,

                 CASE
                   WHEN Isnumeric( strwidth ) = 1 THEN Cast( strwidth AS DECIMAL(10, 2))
                   ELSE NULL
                 END,
                 --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,

                 CASE
                   WHEN Isnumeric( strheight ) = 1 THEN Cast( strheight AS DECIMAL(10, 2))
                   ELSE NULL
                 END,
                 --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end,

                 CASE
                   WHEN Isnumeric( cubicweight ) = 1 THEN Cast( cubicweight AS DECIMAL(10, 4))
                   ELSE NULL
                 END,
                 --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,

                 CASE
                   WHEN Isnumeric( strphysicalweight ) = 1 THEN Cast( strphysicalweight AS DECIMAL(10, 4))
                   ELSE NULL
                 END,
                 Getdate( ),
                 strcreatedby,
                 [countryoforigin],
                 [quantity],
                 [strdhlbarcode]
          FROM   @dtItemLabel;

--------------------------------Item Image ----------------------

          DECLARE @ItemLabelID INT= NULL

          SELECT @ItemLabelID = itemlabelid
          FROM   [TBLITEMLABEL]
          WHERE  [consignmentid] = @ConsignmentIDret

          INSERT INTO [dbo].[TBLITEMLABELIMAGE]
                      ([itemlabelid],
                       [lableimage],
                       [createdby])
          VALUES      (@ItemLabelID,
                       @LabelImage,
                       @UserID )

          ---------CustomDeclaration ------------------------
          INSERT INTO [dbo].TBLCUSTOMDECLARATION
                      ([consignmentid],
                       itemdescription,
                       iteminbox,
                       unitprice,
                       subtotal,
                       hscode,
                       countryoforigin,
                       currency,
                       createdby)
          SELECT @ConsignmentIDret,
                 itemdescription,
                 iteminbox,
                 unitprice,
                 subtotal,
                 hscode,
                 countryoforigin,
                 currency,
                 @UserID
          FROM   @dtCustomDeclaration

          -----------------------insert [tblDHLBarCodeImage] -------

          IF( @AWBBarCode IS NOT NULL ) BEGIN
                INSERT INTO [dbo].[TBLDHLBARCODEIMAGE]
                            ([consignmentid],
                             [awbcode],
                             [awbbarcode],
                             [origindestncode],
                             [origindestnbarcode],
                             [clientidcode],
                             [clientidbarcode],
                             [dhlroutingcode],
                             [dhlroutingbarcode],
                             [createdby])
                VALUES      (@ConsignmentIDret,
                             @AWBCode,
                             @AWBBarCode,
                             @OriginDestncode,
                             @OriginDestnBarcode,
                             @ClientIDCode,
                             @ClientIDBarCode,
                             @DHLRoutingCode,
                             @DHLRoutingBarCode,
                             @UserID)

            END

          DECLARE @ConsignmentId INT = @ConsignmentIDret

---------------------------------------------------------------------------------------------

          UPDATE dbo.[TBLINVOICE]
          SET    [paymentrefno] = @PaymentRefNo,
                 [authorizationcode] = @AuthorizationCode,
                 [merchantreferencecode] = @merchantReferenceCode,
                 [subscriptionid] = @SubscriptionID,
                 [updateddatetime] = Getdate( ),
                 [updatedby] = @UserID
          WHERE  invoicenumber =
                 ( SELECT TOP 1 invoiceno
                   FROM   dbo.TBLSALESORDER
                   WHERE  referenceno = @ConsignmentId )

          -----------------------------------Consignment Staging-----------------------------------

          UPDATE dbo.TBLCONSIGNMENTSTAGING
          SET    [isprocessed] = 1,
                 [updateddatettime] = Getdate( ),
                 [updatedby] = @CreatedBy,
                 [consignmentid] = @ConsignmentIDret,
                 isaccountcustomer = 1
          WHERE  consignmentstagingid = @ConsignmentStagingID


          ---------------------------------------------------------------------------------------------

          DECLARE @PicupID INT

          SELECT dbo.TBLCONSIGNMENT.*,
                 dbo.TBLCONSIGNMENT.[country-servicearea-facilitycode] AS Country_ServiceArea_FacilityCode
          FROM   dbo.TBLCONSIGNMENT
          WHERE  dbo.TBLCONSIGNMENT.consignmentid = @ConsignmentID

          SELECT @PicupID = dbo.TBLCONSIGNMENT.pickupid,
                 @DestinationID = dbo.TBLCONSIGNMENT.destinationid,
                 @ContactID = dbo.TBLCONSIGNMENT.contactid
          FROM   dbo.TBLCONSIGNMENT
          WHERE  dbo.TBLCONSIGNMENT.consignmentid = @ConsignmentID

          SELECT dbo.TBLADDRESS.addressid,
                 dbo.TBLADDRESS.userid,
                 dbo.TBLADDRESS.firstname,
                 dbo.TBLADDRESS.lastname,
                 dbo.TBLADDRESS.companyname,
                 dbo.TBLADDRESS.email,
                 dbo.TBLADDRESS.address1,
                 dbo.TBLADDRESS.address2,
                 dbo.TBLADDRESS.suburb,
                 dbo.TBLADDRESS.postcode,
                 dbo.TBLADDRESS.phone,
                 dbo.TBLADDRESS.mobile,
                 dbo.TBLADDRESS.createddatetime,
                 dbo.TBLADDRESS.createdby,
                 dbo.TBLADDRESS.updateddatetime,
                 dbo.TBLADDRESS.updatedby,
                 dbo.TBLADDRESS.isregisteraddress,
                 dbo.TBLADDRESS.isdeleted,
                 dbo.TBLADDRESS.isbusiness,
                 dbo.TBLADDRESS.issubscribe,
                 CASE
                   WHEN dbo.TBLSTATE.statecode IS NULL THEN dbo.TBLADDRESS.statename
                   ELSE dbo.TBLSTATE.statecode
                 END AS StateID,
                 country,
                 countrycode
          FROM   dbo.TBLADDRESS
                 LEFT JOIN dbo.TBLSTATE
                        ON dbo.TBLADDRESS.stateid = dbo.TBLSTATE.stateid
          WHERE  addressid = @PicupID

          SELECT dbo.TBLADDRESS.addressid,
                 dbo.TBLADDRESS.userid,
                 dbo.TBLADDRESS.firstname,
                 dbo.TBLADDRESS.lastname,
                 dbo.TBLADDRESS.companyname,
                 dbo.TBLADDRESS.email,
                 dbo.TBLADDRESS.address1,
                 dbo.TBLADDRESS.address2,
                 dbo.TBLADDRESS.suburb,
                 dbo.TBLADDRESS.postcode,
                 dbo.TBLADDRESS.phone,
                 dbo.TBLADDRESS.mobile,
                 dbo.TBLADDRESS.createddatetime,
                 dbo.TBLADDRESS.createdby,
                 dbo.TBLADDRESS.updateddatetime,
                 dbo.TBLADDRESS.updatedby,
                 dbo.TBLADDRESS.isregisteraddress,
                 dbo.TBLADDRESS.isdeleted,
                 dbo.TBLADDRESS.isbusiness,
                 dbo.TBLADDRESS.issubscribe,
                 CASE
                   WHEN dbo.TBLSTATE.statecode IS NULL THEN dbo.TBLADDRESS.statename
                   ELSE dbo.TBLSTATE.statecode
                 END AS StateID,
                 country,
                 countrycode
          FROM   dbo.TBLADDRESS
                 LEFT JOIN dbo.TBLSTATE
                        ON dbo.TBLADDRESS.stateid = dbo.TBLSTATE.stateid
          WHERE  addressid = @DestinationID

          SELECT cu.userid,
                 c.isregularshipper
          FROM   TBLCOMPANY c
                 INNER JOIN TBLCOMPANYUSERS cu
                         ON c.companyid = cu.companyid
          WHERE  ( c.isregularshipper = 1 ) AND
                 cu.userid = @UserID

COMMIT TRAN

      END try

      BEGIN catch
          BEGIN
              ROLLBACK TRAN

              INSERT INTO [dbo].[TBLERRORLOG]
                          ([error],
                           [functioninfo],
                           [clientid])
              VALUES      (Cast(Error_line() AS VARCHAR) + ' : '
                           + Error_message(),
                           'SPCPPL_CreateConsignmentUsingBulkDHLWithoutPay_international',
                           2)
          END
      END catch

  END
GO
