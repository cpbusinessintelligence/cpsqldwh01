SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateConsignment_Common_SP_For_ALL_International] 

@ProntoDataExtracted bit= NULL,
@LastActivity varchar(200)= NULL,
@LastActiivityDateTime datetime= NULL,
@EDIDataProcessed bit= NULL,
@ConsignmentCode varchar(40)= NULL,
@ConsignmentStagingID int= NULL,
@GrossTotal decimal(19,4),
@GST decimal(19,4),
@NetTotal decimal(19,4),
@dtItemLabel dtItemLabel_Common_For_ALL READONLY,
@dtCustomDeclaration dtCustomDeclaration READONLY,
@dtSalesOrderDetail dtSalesOrderDetail_Common_For_ALL READONLY,
@TotalFreightExGST decimal(19,4) = NULL,
@TotalFreightInclGST decimal(19,4) = NULL,
@InvoiceStatus int = NULL,
@SendToPronto bit = NULL,
@PaymentRefNo nvarchar(100)=NULL,
@merchantReferenceCode nvarchar(MAX)=NULL,
@SubscriptionID nvarchar(MAX)=NULL,
@AuthorizationCode nvarchar(100)=NULL,
@InvoiceImage varchar(MAX)=NULL,
@LabelImage varchar(MAX)=NULL,
@ItemCodeSing varchar(MAX)=NULL,
@AWBBarCode varchar(MAX)=NULL,
@OriginDestnBarcode varchar(MAX)=NULL,
@ClientIDBarCode varchar(MAX)=NULL,
@DHLRoutingBarCode varchar(MAX)=NULL,
@NetSubTotal decimal(10,2)=NULL,
@SenderReference nvarchar(MAX) = NULL,
@NatureOfGoods nvarchar(MAX)=NULL ,
@IsPayment bit = 0,
@PromotionDiscount decimal(10,2)=NULL,
@DiscountedTotal decimal(10,2)=NULL

AS 
BEGIN 
BEGIN TRY
BEGIN tran 

DECLARE @UserID int= NULL,
@IsRegUserConsignment bit= NULL,
@PickupID int= NULL,
@DestinationID int= NULL,
@ContactID int= NULL,
@TotalWeight decimal(19,4)= NULL,
@TotalVolume decimal(19,4)= NULL,
@NoOfItems int= NULL,
@SpecialInstruction varchar(500)= NULL,
@CustomerRefNo varchar(40)= NULL,
@ConsignmentPreferPickupDate date= NULL,
@ConsignmentPreferPickupTime varchar(20)= NULL,
@ClosingTime varchar(10)= NULL,
@DangerousGoods bit= NULL,
@Terms bit= NULL,
@RateCardID nvarchar(50)= NULL,
@CreatedBy int= NULL,
@IsSignatureReq bit = 0,
@IsDocument nvarchar(MAX) = NULL,
@IfUndelivered nvarchar(MAX)=NULL,
@ReasonForExport nvarchar(MAX)=NULL,
@TypeOfExport nvarchar(MAX)=NULL,
@Currency nvarchar(MAX)=NULL,
@IsInsurance bit=NULL,
@IdentityNo nvarchar(MAX)=NULL,
@IdentityType nvarchar(MAX)=NULL,
@IsIdentity bit=NULL,
@IsATl bit=NULL,
@IsReturnToSender bit=NULL,
@HasReadInsuranceTc bit=NULL,
@SortCode nvarchar(MAX) = NULL,
@ETA nvarchar(MAX) = NULL,
@InsuranceAmount decimal(10,2) = NULL,
@CountryCode nvarchar(MAX) = NULL,
@PromotionCode nvarchar(MAX) = NULL

SELECT  @UserID =[UserID] ,
		@IsRegUserConsignment=[IsRegUserConsignment] ,
		@PickupID=[PickupID] ,
		@DestinationID=[DestinationID] ,
		@ContactID=[ContactID] ,
		@TotalWeight=[TotalWeight] ,
		@TotalVolume=[TotalVolume] ,
		@NoOfItems=[NoOfItems] ,
		@SpecialInstruction=[SpecialInstruction] ,
		@CustomerRefNo=[CustomerRefNo] ,
		@ConsignmentPreferPickupDate=[PickupDate] ,
		@ConsignmentPreferPickupTime=[PreferPickupTime] ,
		@ClosingTime=[ClosingTime] ,
		@DangerousGoods=[DangerousGoods] ,
		@Terms=[Terms] ,
		@RateCardID=[RateCardID] ,
		@CreatedBy = [CreatedBy] ,
		@IsSignatureReq =[IsSignatureReq] ,
		@IsDocument =[IsDocument],
		@IfUndelivered =[IfUndelivered],
		@ReasonForExport =[ReasonForExport],
		@TypeOfExport =[TypeOfExport],
		@Currency =[Currency],
		@IsInsurance = [IsInsurance],
		@IdentityNo = [IdentityNo],
		@IdentityType= [IdentityType],
		@IsIdentity = [IsIdentity],
		@IsATl=[IsATl],
		@IsReturnToSender= [IsReturnToSender],
		@HasReadInsuranceTc = [HasReadInsuranceTc],
		@SortCode = [SortCode],
		@NatureOfGoods=NatureOfGoods,
		@ETA = [ETA],
		@InsuranceAmount=InsuranceAmount,
		@PromotionCode = [PromotionCode]

FROM [dbo].[tblConsignmentStaging]
WHERE ConsignmentStagingID = @ConsignmentStagingID 

declare @GeneratedConsignmentCode nvarchar(30) 
ConsignmentCodeGeneration:
Set @GeneratedConsignmentCode = @ConsignmentCode + [dbo].GenerateConsignmentCode(@ConsignmentCode)

if exists(select * from tblconsignment where [ConsignmentCode] = @GeneratedConsignmentCode )
begin
Goto ConsignmentCodeGeneration;
end

DECLARE @ConsignmentIDret int
INSERT INTO [dbo].[tblConsignment] ([ConsignmentCode] ,[UserID] ,[IsRegUserConsignment] ,[PickupID] ,[DestinationID] ,[ContactID] ,[TotalWeight] 
				,[TotalVolume] 
				,[NoOfItems] ,[SpecialInstruction] ,[CustomerRefNo] ,[ConsignmentPreferPickupDate] ,[ConsignmentPreferPickupTime] ,[ClosingTime] ,[DangerousGoods] ,[Terms] ,[RateCardID] ,[LastActivity] ,[LastActiivityDateTime] ,[ConsignmentStatus] ,[EDIDataProcessed] ,[ProntoDataExtracted] ,[CreatedDateTime] ,[CreatedBy] ,[IsInternational] ,[IsDocument] ,IsSignatureReq, [IfUndelivered], [ReasonForExport], [TypeOfExport], [Currency], [IsInsurance], [IdentityNo], [IdentityType], [IsIdentity], [NetSubTotal], [IsATl], [IsReturnToSender], [HasReadInsuranceTc], [NatureOfGoods], [SortCode] ,ETA, IsAccountCustomer ,InsuranceAmount ,CalculatedTotal 
				,CalculatedGST , [IsProcessed],[PromotionCode] )
VALUES (@GeneratedConsignmentCode,
				@UserID,@IsRegUserConsignment,@PickupID,@DestinationID,@ContactID,@TotalWeight,@TotalVolume,@NoOfItems,@SpecialInstruction,@CustomerRefNo,@ConsignmentPreferPickupDate,
				@ConsignmentPreferPickupTime,@ClosingTime,@DangerousGoods,@Terms,@RateCardID,@LastActivity,@LastActiivityDateTime,1,0,0,GETDATE(),@CreatedBy,1,
				@IsDocument,@IsSignatureReq ,@IfUndelivered,@ReasonForExport,@TypeOfExport,@Currency,@IsInsurance,@IdentityNo,@IdentityType,@IsIdentity,@NetSubTotal,@IsATl,
				@IsReturnToSender,@HasReadInsuranceTc,@NatureOfGoods,@SortCode ,@ETA,1 ,@InsuranceAmount ,@GrossTotal ,@GST , 1,@PromotionCode)
SET @ConsignmentIDret = SCOPE_IDENTITY() 

------------------Insert In Sales Order------------------
if (@IsPayment=1) 
begin
DECLARE @SalesOrderIDret int
INSERT INTO [dbo].[tblSalesOrder] ([ReferenceNo],[UserID],[NoofItems],[TotalWeight],[TotalVolume],[RateCardID],[GrossTotal],[GST],[NetTotal],[SalesOrderStatus],[InvoiceNo],[CreatedDateTime],[CreatedBy]
,[PromotionDiscount],[DiscountedTotal])
VALUES (@ConsignmentIDret,@UserID,@NoofItems,@TotalWeight,@TotalVolume,@RateCardID,@GrossTotal,@GST,@NetTotal,7,NULL,GETDATE(),@UserID,@PromotionDiscount,@DiscountedTotal)
SET @SalesOrderIDret = SCOPE_IDENTITY() 
end
---------------------Get Country Code--------------------------

SELECT @CountryCode=CountryCode FROM dbo.tblAddress
LEFT JOIN dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID WHERE AddressID =@DestinationID

------------------------------ItemLabel ------------------------

INSERT INTO [dbo].[tblItemLabel] ([ConsignmentID] ,[LabelNumber] ,[Length] ,[Width] ,[Height] ,[CubicWeight] ,[PhysicalWeight] ,[MeasureWeight] ,[DeclareVolume] ,[LastActivity] ,[LastActivityDateTime] ,[CreatedDateTime] ,[CreatedBy] ,[CountryOfOrigin] ,[Description] ,[HSTariffNumber] ,[Quantity] ,[UnitValue] )
SELECT @ConsignmentIDret,@ItemCodeSing,

CASE
WHEN ISNUMERIC(strLength)=1 THEN CAST(strLength AS decimal(10,2))
ELSE NULL
END,

CASE
WHEN ISNUMERIC(strWidth)=1 THEN CAST(strWidth AS decimal(10,2))
ELSE NULL
END,

CASE
WHEN ISNUMERIC(strHeight)=1 THEN CAST(strHeight AS decimal(10,2))
ELSE NULL
END,

CASE
WHEN ISNUMERIC(strCubicWeight)=1 THEN CAST(strCubicWeight AS decimal(10,4))
ELSE NULL
END,

CASE
WHEN ISNUMERIC(strPhysicalWeight)=1 THEN CAST(strPhysicalWeight AS decimal(10,4))
ELSE NULL
END,

NULL,	--CASE WHEN ISNUMERIC(strMeasureWeight)=1 THEN CAST(strMeasureWeight AS decimal(10,2)) ELSE NULL END,

NULL,	--CASE WHEN ISNUMERIC(strDeclareVolume)=1 THEN CAST(strDeclareVolume AS decimal(10,2)) ELSE NULL END,
NULL,	--strLastActivity,
NULL,	--strLastActivityDateTime,
GETDATE(),
@CreatedBy,
[CountryOfOrigin] ,
[Description] ,
[HSTariffNumber] ,
[Quantity] ,
[UnitValue]
FROM @dtItemLabel;

--------------------------------Item Image ----------------------
DECLARE @ItemLabelID int= NULL
SELECT @ItemLabelID = ItemLabelID FROM [tblItemLabel] WHERE [ConsignmentID] = @ConsignmentIDret

INSERT INTO [dbo].[tblItemLabelImage] ([ItemLabelID] ,[LableImage] ,[CreatedBy] )
VALUES (@ItemLabelID , @LabelImage , @UserID ) 

---------SalesOrderDetail ------------------------

if (@IsPayment=1) 
begin

INSERT INTO [dbo].[tblSalesOrderDetail] ([SalesOrderID],[Description],[LineNo],[Weight],[Volume],[FreightCharge],[FuelCharge],[InsuranceCharge],[ServiceCharge],[Total],[CreatedDateTime],[CreatedBy])
SELECT @SalesOrderIDret,
strDescription,
[strLineNo],
CASE
WHEN ISNUMERIC(strPhysicalWeight)=1 THEN CAST(strPhysicalWeight AS decimal(10,4))
ELSE NULL
END,
CASE
WHEN ISNUMERIC(strDeclareVolume)=1 THEN CAST(strDeclareVolume AS decimal(10,4))
ELSE NULL
END,
CASE
WHEN ISNUMERIC(strFreightCharge)=1 THEN CAST(strFreightCharge AS decimal(10,2))
ELSE NULL
END,
CASE
WHEN ISNUMERIC(strFuelCharge)=1 THEN CAST(strFuelCharge AS decimal(19,4))
ELSE NULL
END,
CASE
WHEN ISNUMERIC(strInsuranceCharge)=1 THEN CAST(strInsuranceCharge AS decimal(19,4))
ELSE NULL
END,
CASE
WHEN ISNUMERIC(strServiceCharge)=1 THEN CAST(strServiceCharge AS decimal(19,4))
ELSE NULL
END,
CASE
WHEN ISNUMERIC(strFreightCharge)=1 THEN CAST(strFreightCharge AS decimal(19,4))
ELSE 0.0
END + 
CASE
WHEN ISNUMERIC(strFuelCharge)=1 THEN CAST(strFuelCharge AS decimal(19,4))
ELSE 0.0
END + 
CASE
WHEN ISNUMERIC(strInsuranceCharge)=1 THEN CAST(strInsuranceCharge AS decimal(19,4))
ELSE 0.0
END + 
CASE
WHEN ISNUMERIC(strServiceCharge)=1 THEN CAST(strServiceCharge AS decimal(19,4))
ELSE 0.0
END,
getdate(),
@CreatedBy
FROM @dtSalesOrderDetail 

end
----------------------CustomDeclaration ------------------------

INSERT INTO [dbo].tblCustomDeclaration ([ConsignmentID] ,ItemDescription ,ItemInBox ,UnitPrice ,SubTotal ,HSCode ,CountryofOrigin ,Currency ,CreatedBy )
SELECT @ConsignmentIDret,ItemDescription,ItemInBox,UnitPrice,SubTotal,HSCode,CountryofOrigin,Currency,@UserID
FROM @dtCustomDeclaration 

-----------------------insert [tblDHLBarCodeImage] ---------------

IF(@AWBBarCode IS NOT NULL) BEGIN
INSERT INTO [dbo].[tblDHLBarCodeImage] ([ConsignmentID] ,[AWBBarCode] ,[OriginDestnBarcode] ,[ClientIDBarCode] ,[DHLRoutingBarCode] ,[CreatedBy])
VALUES (@ConsignmentIDret ,@AWBBarCode,@OriginDestnBarcode,@ClientIDBarCode,@DHLRoutingBarCode,@UserID) 
END 
---------Invoice ------------------------

if (@IsPayment=1) 
begin

		DECLARE @InvoiceNumber NVARCHAR(5) , @GeneratedInvoiceNo nvarchar(30) = null
		SET @InvoiceNumber= Substring(@ConsignmentCode, 1, 3) + 'IN' 

		InvoiceNoGeneration: 
		Set @GeneratedInvoiceNo = @InvoiceNumber + [dbo].[GenerateInvoiceNumber](@InvoiceNumber)

		if exists(select * from tblinvoice where [InvoiceNumber] = @GeneratedInvoiceNo )
		begin
		Goto InvoiceNoGeneration;
		end

DECLARE @InvoiceID int

INSERT INTO [dbo].[tblInvoice] ([InvoiceNumber],[UserID],[PickupAddressID],[DestinationAddressID],[ContactAddressID],[TotalFreightExGST],[GST],[TotalFreightInclGST],[InvoiceStatus],[SendToPronto],[CreatedDateTime],[CreatedBY],[PaymentRefNo],[AuthorizationCode],[merchantReferenceCode],[SubscriptionID]
			,[PromotionDiscount],[DiscountedTotal])
	VALUES (@GeneratedInvoiceNo, @UserID,@PickupID,@DestinationID,@ContactID,@GrossTotal,@GST,@NetTotal,11,@SendToPronto,GETDATE(),@CreatedBY,@PaymentRefNo,@AuthorizationCode,
			@merchantReferenceCode,@SubscriptionID,@PromotionDiscount,@DiscountedTotal)

SET @InvoiceID = SCOPE_IDENTITY() 


-----------------------Invoice image --------------

INSERT INTO [dbo].[tblInvoiceImage] ([InvoiceID],[InvoiceImage],[CreatedBy])
VALUES (@InvoiceID,@InvoiceImage,@UserID )

-----------------------insert [tblDHLBarCodeImage] -------
if(@AWBBarCode IS NOT NULL) BEGIN
INSERT INTO [dbo].[tblDHLBarCodeImage] ([ConsignmentID],[AWBBarCode],[OriginDestnBarcode],[ClientIDBarCode],[DHLRoutingBarCode],[CreatedBy])
			VALUES (@ConsignmentIDret,@AWBBarCode,@OriginDestnBarcode,@ClientIDBarCode,@DHLRoutingBarCode,@UserID) END 

--------Update Sales order -----------------------------

UPDATE tblSalesOrder SET InvoiceNo = @GeneratedInvoiceNo, SalesOrderStatus = 9 WHERE SalesOrderID= @SalesOrderIDret 


end

----------------------------------------------------------------------------------------------------------------------------------------------------------


DECLARE @ConsignmentId int = @ConsignmentIDret

UPDATE dbo.tblConsignmentstaging SET 
[ConsignmentId]=@ConsignmentIDret,[IsProcessed] = Case when @IsPayment=1 then 0 else 1 end,
USPSRefNo = case when @CountryCode = 'US' then @SenderReference else null end,
[UpdatedDateTTime] =GETDATE() ,
[UpdatedBy] = @CreatedBy
WHERE ConsignmentStagingID = @ConsignmentStagingID 

--UPDATE dbo.[tblInvoice]
--SET [PaymentRefNo] = @PaymentRefNo ,
--[AuthorizationCode]=@AuthorizationCode ,
--[merchantReferenceCode]=@merchantReferenceCode ,
--[SubscriptionID]=@SubscriptionID ,
--[UpdatedDateTime] =GETDATE() ,
--[UpdatedBy] = @UserID WHERE invoicenumber =
--(SELECT top 1 InvoiceNo
--FROM dbo.tblSalesOrder
--WHERE ReferenceNo=@ConsignmentId)


--UPDATE dbo.tblConsignmentstaging
--SET [IsProcessed] =1, [PaymentRefNo] = @PaymentRefNo ,
--[AuthorizationCode]=@AuthorizationCode ,
--[merchantReferenceCode]=@merchantReferenceCode ,
--[SubscriptionID]=@SubscriptionID ,
--[UpdatedDateTTime] =GETDATE() ,
--[UpdatedBy] = @UserID, 
--[ConsignmentId]=@ConsignmentIDret,
--IsAccountCustomer=1,
--USPSRefNo = case when @CountryCode = 'US' then @SenderReference else null end
--WHERE ConsignmentStagingID = @ConsignmentStagingID DECLARE @PicupID int 

DECLARE @PicupID int 
----------------------------------------------------------------------------------------------------------------------------------

SELECT dbo.tblConsignment.*,
dbo.tblConsignment.[Country-ServiceArea-FacilityCode] AS Country_ServiceArea_FacilityCode
FROM dbo.tblConsignment WHERE dbo.tblConsignment.ConsignmentID = @ConsignmentID 

SELECT @PicupID = dbo.tblConsignment.PickupID,
@DestinationID=dbo.tblConsignment.DestinationID,
@ContactID=dbo.tblConsignment.ContactID
FROM dbo.tblConsignment WHERE dbo.tblConsignment.ConsignmentID = @ConsignmentID

SELECT dbo.tblAddress.AddressID,
		dbo.tblAddress.UserID,
		dbo.tblAddress.FirstName,
		dbo.tblAddress.LastName,
		dbo.tblAddress.CompanyName,
		dbo.tblAddress.Email,
		dbo.tblAddress.Address1,
		dbo.tblAddress.Address2,
		dbo.tblAddress.Suburb,
		dbo.tblAddress.PostCode,
		dbo.tblAddress.Phone,
		dbo.tblAddress.Mobile,
		dbo.tblAddress.CreatedDateTime,
		dbo.tblAddress.CreatedBy,
		dbo.tblAddress.UpdatedDateTime,
		dbo.tblAddress.UpdatedBy,
		dbo.tblAddress.IsRegisterAddress,
		dbo.tblAddress.IsDeleted,
		dbo.tblAddress.IsBusiness,
		dbo.tblAddress.IsSubscribe,
		CASE WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName ELSE dbo.tblState.StateCode END AS StateID,
		Country,
		CountryCode
FROM dbo.tblAddress
LEFT JOIN dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID WHERE AddressID =@PicupID

SELECT dbo.tblAddress.AddressID,
		dbo.tblAddress.UserID,
		dbo.tblAddress.FirstName,
		dbo.tblAddress.LastName,
		dbo.tblAddress.CompanyName,
		dbo.tblAddress.Email,
		dbo.tblAddress.Address1,
		dbo.tblAddress.Address2,
		dbo.tblAddress.Suburb,
		dbo.tblAddress.PostCode,
		dbo.tblAddress.Phone,
		dbo.tblAddress.Mobile,
		dbo.tblAddress.CreatedDateTime,
		dbo.tblAddress.CreatedBy,
		dbo.tblAddress.UpdatedDateTime,
		dbo.tblAddress.UpdatedBy,
		dbo.tblAddress.IsRegisterAddress,
		dbo.tblAddress.IsDeleted,
		dbo.tblAddress.IsBusiness,
		dbo.tblAddress.IsSubscribe,
		CASE WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName ELSE dbo.tblState.StateCode END AS StateID,
		Country,
		CountryCode
FROM dbo.tblAddress
LEFT JOIN dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID WHERE AddressID =@DestinationID

SELECT Cu.UserID, c.IsRegularShipper FROM tblCompany c INNER JOIN tblCompanyUsers Cu ON c.CompanyID = Cu.CompanyID WHERE (c.IsRegularShipper = 1) AND Cu.UserID = @UserID

SELECT * FROM tblItemLabel WHERE ConsignmentID = @ConsignmentID 
-----------------------------------------------------------------------------------------------

COMMIT tran 

END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
           ,'SPCPPL_CreateConsignment_Common_SP_For_ALL_International'
           , 2)
end
END CATCH
END
GO
