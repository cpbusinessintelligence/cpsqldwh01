SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_CreateConsignmentnew]
           @ConsignmentCode varchar(40)= null,
           @UserID int= null,
           @IsRegUserConsignment bit= null,
           @PickupID int= null,
           @DestinationID int= null,
           @ContactID int= null,
           @TotalWeight decimal(4,2)= null,
           @TotalMeasureWeight decimal(4,2)= null,
           @TotalVolume decimal(4,2)= null,
           @TotalMeasureVolume decimal(4,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @LastActivity varchar(200)= null,
           @LastActiivityDateTime datetime= null,
           @ConsignmentStatus int= null,
           @EDIDataProcessed bit= null,
           @ProntoDataExtracted bit= null,

           @CreatedBy int= null
      
           
        

           
AS
BEGIN

declare @ConsignmentCode8Digit nvarchar(8)
 select @ConsignmentCode8Digit = max (RIGHT(ConsignmentCode, 8))+1  from tblConsignment
SELECT @ConsignmentCode8Digit =  RIGHT('00000000' + replace(@ConsignmentCode8Digit,'-',''), 8)
SELECT @ConsignmentCode8Digit
declare @UserId6Digit nvarchar(6)
if(@UserID is null or @UserID ='')
begin
SELECT @UserId6Digit =  RIGHT('00000000' + replace(ABS(Checksum(NewID()) ),'-',''), 6)
end
else
begin
SELECT @UserId6Digit =  RIGHT('00000000' + replace(@UserID,'-',''), 6)
end


			
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].[tblConsignment]
           ([ConsignmentCode]
           ,[UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalMeasureWeight]
           ,[TotalVolume]
           ,[TotalMeasureVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[ConsignmentPreferPickupDate]
           ,[ConsignmentPreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[LastActivity]
           ,[LastActiivityDateTime]
           ,[ConsignmentStatus]
           ,[EDIDataProcessed]
           ,[ProntoDataExtracted]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
     VALUES
           (@ConsignmentCode+@UserId6Digit+@ConsignmentCode8Digit,
           @UserID,
           @IsRegUserConsignment,
           @PickupID,
           @DestinationID,
           @ContactID,
           @TotalWeight,
           @TotalMeasureWeight,
           @TotalVolume,
           @TotalMeasureVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           @LastActivity,
           @LastActiivityDateTime,
           @ConsignmentStatus,
           @EDIDataProcessed,
           @ProntoDataExtracted,
           GETDATE(),
           @CreatedBy) SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret
			
END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
