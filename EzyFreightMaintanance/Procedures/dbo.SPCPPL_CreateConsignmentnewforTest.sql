SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_CreateConsignmentnewforTest]
      
                @TotalMeasureWeight decimal(4,2)= null,
   @TotalMeasureVolume decimal(4,2)= null,
   @ProntoDataExtracted bit= null,
   @LastActivity varchar(200)= null,
     @LastActiivityDateTime datetime= null,
  @EDIDataProcessed bit= null,
   --@ConsignmentStatus int= null,
    @ConsignmentCode varchar(40)= null,
  @ConsignmentStagingID int= null,
        
@GrossTotal decimal(19,4) ,
 @GST decimal(19,4) ,
           @NetTotal decimal(19,4) 
           
AS
BEGIN


declare    @UserID int= null,
           @IsRegUserConsignment bit= null,
           @PickupID int= null,
           @DestinationID int= null,
           @ContactID int= null,
           @TotalWeight decimal(4,2)= null,
           @TotalVolume decimal(4,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @CreatedBy int= null




SELECT 
      @UserID =[UserID]
      ,@IsRegUserConsignment=[IsRegUserConsignment]
      ,@PickupID=[PickupID]
      ,@DestinationID=[DestinationID]
      ,@ContactID=[ContactID]
      ,@TotalWeight=[TotalWeight]
      ,@TotalVolume=[TotalVolume]
      ,@NoOfItems=[NoOfItems]
      ,@SpecialInstruction=[SpecialInstruction]
      ,@CustomerRefNo=[CustomerRefNo]
      ,@ConsignmentPreferPickupDate=[PickupDate]
      ,@ConsignmentPreferPickupTime=[PreferPickupTime]
      ,@ClosingTime=[ClosingTime]
      ,@DangerousGoods=[DangerousGoods]
      ,@Terms=[Terms]
      ,@RateCardID=[RateCardID]
      ,@CreatedBy = [CreatedBy]
  FROM [dbo].[tblConsignmentStaging] where ConsignmentStagingID = @ConsignmentStagingID




declare @ConsignmentCode9Digit nvarchar(9)
 select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,6) = @ConsignmentCode
SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode9Digit
if(@ConsignmentCode9Digit is null)
set @ConsignmentCode9Digit = '000000000'
--SELECT @ConsignmentCode9Digit

--declare @ConsignmentCode9Digit nvarchar(9)
-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where ConsignmentCode = @ConsignmentCode
--SELECT @ConsignmentCode9Digit =  RIGHT('00000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode8Digit
--declare @UserId6Digit nvarchar(6)
--if(@UserID is null or @UserID ='')
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(ABS(Checksum(NewID()) ),'-',''), 6)
--end
--else
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(@UserID,'-',''), 6)
--end


			
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].[tblConsignment]
           ([ConsignmentCode]
           ,[UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalMeasureWeight]
           ,[TotalVolume]
           ,[TotalMeasureVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[ConsignmentPreferPickupDate]
           ,[ConsignmentPreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[LastActivity]
           ,[LastActiivityDateTime]
           ,[ConsignmentStatus]
           ,[EDIDataProcessed]
           ,[ProntoDataExtracted]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
     VALUES
           (@ConsignmentCode+@ConsignmentCode9Digit,
           @UserID,
           @IsRegUserConsignment,
           @PickupID,
           @DestinationID,
           @ContactID,
           @TotalWeight,
           @TotalMeasureWeight,
           @TotalVolume,
           @TotalMeasureVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           @LastActivity,
           @LastActiivityDateTime,
           1,
           @EDIDataProcessed,
           @ProntoDataExtracted,
           GETDATE(),
           @CreatedBy) SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret
			
			UPDATE dbo.tblConsignmentStaging
   SET [IsProcessed] =1,
      [UpdatedDateTTime] = getDate(),
      [UpdatedBy] = @CreatedBy
 WHERE ConsignmentStagingID = @ConsignmentStagingID
 
 
 
 DECLARE @SalesOrderIDret int
 INSERT INTO [dbo].[tblSalesOrder]
           ([ReferenceNo]
           ,[UserID]
           ,[NoofItems]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[RateCardID]
           ,[GrossTotal]
           ,[GST]
           ,[NetTotal]
           ,[SalesOrderStatus]
           ,[InvoiceNo]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
           VALUES
           ( 
           @ConsignmentIDret ,
           1  ,
           @NoofItems  ,
           @TotalWeight  ,
           @TotalVolume  ,
           @RateCardID  ,
           @GrossTotal ,
           @GST  ,
           @NetTotal  ,
           7  ,
           null ,
           GETDATE() ,
          1 
           )SET @SalesOrderIDret = SCOPE_IDENTITY()
			select @SalesOrderIDret
 
 
 SELECT  [FirstName]
     
      ,[Address1]
      ,[Address2]
      ,[Suburb]
      
      ,[PostCode]
    
  FROM [dbo].[tblAddress] where [AddressID]=@PickupID
  
  SELECT 
      [FirstName]
     
      ,[Address1]
      ,[Address2]
      ,[Suburb]
      
      ,[PostCode]
    
  FROM [dbo].[tblAddress] where [AddressID]=@DestinationID
 
 
 select @ConsignmentPreferPickupDate 
			
			
			SELECT 
     
      @PickupID
      ,@DestinationID
      ,@ContactID 
END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
