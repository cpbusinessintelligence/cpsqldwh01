SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SPCPPL_CreateContactUs]
           @FirstName nvarchar(50) =null ,
           @LastName nvarchar(50) =null ,
           @Email nvarchar(250) =null ,
           @Phone nvarchar(250) =null ,
           @Suburb nvarchar(50) =null ,
           @PostCode nvarchar(50) =null ,
           @StateCode nvarchar(50) =null ,
           @Subject nvarchar(250) =null ,
           @HowCanIHelpYou nvarchar(max) =null ,
           @AddressLine1 nvarchar(max) =null ,
           @AddressLine2 nvarchar(max) =null ,
           @IsSubscribe bit =null ,
           @TrackingNo nvarchar(max)= null

           
AS
BEGIN

if (@Email not  like '%johndoe@singpost.com%'
and not exists( select COUNT([Phone]) from [dbo].[tblContactUs] where @Phone = [Phone] and [CreatedDate] = CAST(GETDATE() as date) group by [Phone] having COUNT ([Phone])>=50)
--and not exists( select COUNT([TrackingNo]) from [dbo].[tblContactUs] where @TrackingNo = [TrackingNo] and [CreatedDate] = CAST(GETDATE() as date) group by [TrackingNo] having COUNT ([TrackingNo])>=5)
and not exists( select COUNT([Email]) from [dbo].[tblContactUs] where @Email = [Email] and [CreatedDate] = CAST(GETDATE() as date) group by [Email] having COUNT ([Email])>=50)


)
BEGIN
   

DECLARE @stateIdbyCode int
SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @StateCode

DECLARE @PickupAddID int

					INSERT INTO [dbo].[tblContactUs]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Phone]
           ,[Suburb]
           ,[PostCode]
           ,[StateID]
           ,[Subject]
           ,[HowCanIHelpYou]
           ,[IsSubscribe]
           ,[TrackingNo]
           ,[AddressLine1]
           ,[AddressLine2]
          )
     VALUES
           (@FirstName ,
           @LastName ,
           @Email ,
           @Phone ,
           @Suburb ,
           @PostCode ,
           @stateIdbyCode ,
           @Subject ,
           @HowCanIHelpYou ,
           @IsSubscribe ,
           @TrackingNo,
           @AddressLine1,
           @AddressLine2
           )
      SET @PickupAddID = SCOPE_IDENTITY()
			select @PickupAddID
	End

else 
begin
 select * from sqlinjection

end

				
End
GO
