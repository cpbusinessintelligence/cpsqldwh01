SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateEDICompanyTran]
           @AccountNumber nvarchar(20) = null,
           @CompanyName varchar(50) = null,
           @ParentAccountNo varchar(20) = null,
           @ABN varchar(20) = null,
           @AddressID1 int = null,
           @PostalAddressID int = null,
           @ParentCompanyName varchar(50) = null,
           @BilingAccountNo nvarchar(20) = null,
           @IsProntoExtracted bit = null,
           @RateCardCategory nvarchar(20) = null,
           	    @SameBillingAddress bit = null
					   ,@BillingFname nvarchar(50) = null
					   ,@BillingLname nvarchar(50) = null
					   ,@BllingEmail nvarchar(50) = null
					   ,@BillingPhone nvarchar(20) = null
					   ,@BillingAddress nvarchar(100) = null
					   ,@CreditLimit decimal(15,2) = null
					   
					   ,@CreatedBy  int = -1 
					   ,@IsExistingCustomer  bit = null 
					   ,@Branch  varchar(20) = null
					   ,@ExistingAccountNumber  varchar(20) = null
					   ,@CouponNo  varchar(20) = null
					 
 
-- ,@PostalAddressuserId int=null,
--@PostalAddressfirstName nvarchar(50)=null,
--@PostalAddresslastName nvarchar(50)=null,
--@PostalAddresscompanyName nvarchar(100)=null,
--@PostalAddressemail nvarchar(250)=null,
--@PostalAddressaddress1 nvarchar(200)=null,
--@PostalAddressaddress2 nvarchar(200)=null,
--@PostalAddresssuburb nvarchar(100)=null,
--@PostalAddressstateId nvarchar(max)=null,
--@PostalAddresspostalCode nvarchar(20)=null,
--@PostalAddressphone nvarchar(20)=null,
--@PostalAddressmobile nvarchar(20)=null,
--@PostalAddresscreatedBy int=null,
--@PostalAddressisBusiness bit=0,
--@PostalAddressisRegisterAddress bit=0,
--@PostalAddressIsSubscribe bit=0,


,@AddressuserId int=null,
@AddressfirstName nvarchar(50)=null,
@AddresslastName nvarchar(50)=null,
@AddresscompanyName nvarchar(100)=null,
@Addressemail nvarchar(250)=null,
@Addressaddress1 nvarchar(200)=null,
@Addressaddress2 nvarchar(200)=null,
@Addresssuburb nvarchar(100)=null,
@AddressstateId nvarchar(max)=null,
@AddresspostalCode nvarchar(20)=null,
@Addressphone nvarchar(20)=null,
@Addressmobile nvarchar(20)=null,
@AddresscreatedBy int=null,
@AddressisBusiness bit=0,
@AddressisRegisterAddress bit=0,
@AddressIsSubscribe bit=0 ,
@UserID int = null,

@AccountNumber9Digit nvarchar(max) = null
           
      
AS
BEGIN
begin tran

DECLARE @stateIdbyCode int

-------------------------------------------------
			SELECT top 1  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @AddressstateId or StateName = @AddressstateId
 

if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@AddresscompanyName)) and 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @AddressfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@AddresslastName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Addressaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Addressaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =  rtrim(ltrim(@AddresscompanyName)) and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Addresssuburb ))and 
rtrim(ltrim([StateID])) =  rtrim(ltrim(@stateIdbyCode)) and 
rtrim(ltrim([PostCode])) =  rtrim(ltrim(@AddresspostalCode)) and [UserID]=@AddressuserId)
begin
select @AddressID1 = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@AddresscompanyName)) and 
rtrim(ltrim([FirstName])) =rtrim(ltrim( @AddressfirstName ))and 
rtrim(ltrim([LastName] ))= rtrim(ltrim(@AddresslastName ))and 
rtrim(ltrim([Address1])) =rtrim(ltrim( @Addressaddress1)) and 
rtrim(ltrim([Address2])) =rtrim(ltrim( @Addressaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =rtrim(ltrim( @AddresscompanyName)) and 
rtrim(ltrim([Suburb] ))= rtrim(ltrim(@Addresssuburb ))and 
rtrim(ltrim([StateID])) = rtrim(ltrim(@stateIdbyCode ))and 
rtrim(ltrim([PostCode] ))=rtrim(ltrim( @AddresspostalCode)) and [UserID]=@AddressuserId
end
else

begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe] 
	 ,IsEDIUser)
	 VALUES
      ( @AddressuserId,@AddressfirstName ,@AddresslastName ,@AddresscompanyName,@Addressemail,@Addressaddress1,@Addressaddress2,@Addresssuburb,@stateIdbyCode,
      @AddresspostalCode,@Addressphone,@Addressmobile,@AddressisRegisterAddress,GETDATE(), @AddresscreatedBy ,@AddressisBusiness,@AddressIsSubscribe
      ,1)
        SET @AddressID1 = SCOPE_IDENTITY()
end
 
			--select @AddressID
		set	@PostalAddressID = @AddressID1
			-------------------------------------------------

			--select @PostalAddressID
			-------------------------------------------------
			---------------------
			
				DECLARE @CompanyIDret int
		
if exists(select * from [tblCompany] where [ABN] = @ABN)
begin
		
            
            select @CompanyIDret= CompanyID from [tblCompany] where [ABN] = @ABN
            
            end
            else
                begin
    --            declare @AccountNumber9Digit nvarchar(9)
				-- select @AccountNumber9Digit = max ( (CompanyID))+1  from dbo.tblCompany 
				--SELECT @AccountNumber9Digit =  RIGHT('00000000' + replace(@AccountNumber9Digit,'-',''), 8)
				-- SELECT @AccountNumber9Digit
				--if(@AccountNumber9Digit is null)
				--set @AccountNumber9Digit = '00000000'
				--set @AccountNumber9Digit = 'W'+@AccountNumber9Digit
				--set @AccountNumber9Digit = null
				 SELECT @AccountNumber9Digit
            				INSERT INTO  [dbo].[tblCompany]
					   ([AccountNumber]
					   ,[CompanyName]
					   ,[ParentAccountNo]
					   ,[ABN]
					   ,[AddressID1]
					   ,[PostalAddressID]
					   ,[ParentCompanyName]
					   ,[BilingAccountNo]
					   ,[IsProntoExtracted]
					   ,[RateCardCategory]
					   ,[IntRateCardCategory]
					   ,SameBillingAddress
					   ,BillingFname
					   ,BillingLname
					   ,BllingEmail
					   ,BillingPhone
					   ,BillingAddress
					   ,CreditLimit
					   ,CreatedDateTime
					   ,CreatedBy
					    , IsExistingCustomer  
					   , Branch  
					   , ExistingAccountNumber   
					   , CouponNo  
					   )
				 VALUES
					   (
					   @AccountNumber9Digit  ,
					   @CompanyName  ,
					   @ParentAccountNo  ,
					   @ABN  ,
					   @AddressID1  ,
					   @PostalAddressID  ,
					   @ParentCompanyName  ,
					   @BilingAccountNo  ,
					   @IsProntoExtracted  ,
					   'DEF',  
					   'DEF' 
					   ,@SameBillingAddress
					   ,@BillingFname
					   ,@BillingLname
					   ,@BllingEmail
					   ,@BillingPhone
					   ,@BillingAddress
					   ,@CreditLimit
					   ,GETDATE()
					   ,@CreatedBy 
					    ,@IsExistingCustomer  
					   ,@Branch  
					   ,@ExistingAccountNumber  
					   ,@CouponNo   
					   )
						SET @CompanyIDret = SCOPE_IDENTITY()
            end
            
            
            
			select @CompanyIDret
			
			
			--------------------------------
					DECLARE @CompanyUsersIDret int
			INSERT INTO [dbo].[tblCompanyUsers]
           ([UserID]
           ,[CompanyID] 
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[IsUserDisabled])
     VALUES
           (
           @UserID  ,
           @CompanyIDret ,
           GETDATE(),
            @CreatedBy,
            0 )
            SET @CompanyUsersIDret = SCOPE_IDENTITY()
			select @CompanyUsersIDret
			 if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateCompanyTran]'
    , 2)
  end
  else
  commit tran          
            

END

GO
