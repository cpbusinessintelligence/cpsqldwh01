SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateFranchise]
           @FirstName varchar(50)  = null ,
           @LastName varchar(50)  = null ,
           @CompanyName varchar(100)  = null ,
           @ABNRegistration varchar(100)  = null ,
           @Email varchar(250)  = null ,
           @Address1 varchar(100)  = null ,
           @Address2 varchar(100)  = null ,
           @SuburbPostCode varchar(100)  = null ,
           @Suburb varchar(100)  = null ,
           @State nvarchar(max) = null ,
           @PostCode varchar(10)  = null ,
           @Phone nvarchar(20)  = null ,
           @Mobile nvarchar(20)  = null ,
           @DateOfBirth date  = null ,
           @Reason nvarchar(max)  = null ,
           @ValidLicense nvarchar(max)  = null ,
           @CreatedBy int = -1

           
AS
BEGIN



   DECLARE @stateIdbyCode int
SELECT top 1  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @State

INSERT INTO  [dbo].[tblFranchise]
           ([FirstName]
           ,[LastName]
           ,[CompanyName]
           ,[ABN]
           ,[Email]
           ,[Address1]
           ,[Address2]
           ,[Suburb]
           ,[State]
           ,[PostCode]
           ,[Phone]
           ,[Mobile]
           ,[DateOfBirth]
           ,[Reason]
           ,[ValidLicense]
           ,[CreatedBy]
            )
     VALUES
           (           @FirstName   ,
           @LastName   ,
           @CompanyName   ,
           @ABNRegistration   ,
           @Email ,
           @Address1   ,
           @Address2   ,
           @Suburb    ,
           @stateIdbyCode   ,
           @PostCode   ,
           @Phone   ,
           @Mobile   ,
           @DateOfBirth   ,
           @Reason   ,
           @ValidLicense   ,
           @CreatedBy
           )
				
End
GO
