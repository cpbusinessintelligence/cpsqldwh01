SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateInvoice]
      
        
           @UserID int = null,
           @PickupAddressID int = null,
           @DestinationAddressID int = null,
           @ContactAddressID int = null,
           @TotalFreightExGST decimal(19,4) = null,
           @GST decimal(19,4) = null,
           @TotalFreightInclGST decimal(19,4) = null,
           @InvoiceStatus int = null,
           @SendToPronto bit = null,

           @CreatedBY int = null,
           @InvoiceNumber nvarchar(40)=null,
           @ConsignmentID int = null
  
           
AS
BEGIN








--declare @ConsignmentCode8Digit nvarchar(6)

-- select @ConsignmentCode8Digit = max (RIGHT(InvoiceNumber, 6))+1  from tblInvoice
-- if(@ConsignmentCode8Digit is null)
--set @ConsignmentCode8Digit = 000001
--SELECT @ConsignmentCode8Digit =  RIGHT('000000' + replace(@ConsignmentCode8Digit,'-',''), 6)
----SELECT @ConsignmentCode8Digit
--declare @UserId5Digit nvarchar(6)
--if(@UserID is null or @UserID ='')
--begin
--set @UserId5Digit = 'AHCIN'
--end
--else
--begin
--set @UserId5Digit = 'REGIN'
--end

declare @InvoiceCode7Digit nvarchar(7)
 select @InvoiceCode7Digit = max (RIGHT(InvoiceNumber, 7))+1  from tblInvoice where left(InvoiceNumber,8) = @InvoiceNumber
SELECT @InvoiceCode7Digit =  RIGHT('0000000' + replace(@InvoiceCode7Digit,'-',''),7)
--SELECT @ConsignmentCode9Digit
if(@InvoiceCode7Digit is null)
set @InvoiceCode7Digit = '0000000'


--select @UserId5Digit+@ConsignmentCode8Digit

				DECLARE @InvoiceID int
INSERT INTO [dbo].[tblInvoice]
           ([InvoiceNumber]
           ,[UserID]
           ,[PickupAddressID]
           ,[DestinationAddressID]
           ,[ContactAddressID]
           ,[TotalFreightExGST]
           ,[GST]
           ,[TotalFreightInclGST]
           ,[InvoiceStatus]
           ,[SendToPronto]
           ,[CreatedDateTime]
           ,[CreatedBY]
         )
     VALUES
           (       @InvoiceNumber+@InvoiceCode7Digit , 
           @UserID , 
           @PickupAddressID , 
           @DestinationAddressID , 
           @ContactAddressID , 
           @TotalFreightExGST , 
           @GST , 
           @TotalFreightInclGST , 
           @InvoiceStatus , 
           @SendToPronto , 
           GETDATE() , 
           @CreatedBY 
           )SET @InvoiceID = SCOPE_IDENTITY()
           
           SELECT     dbo.tblInvoice.*
FROM         dbo.tblInvoice where dbo.tblInvoice.InvoiceID = @InvoiceID
           
           SELECT     dbo.tblConsignment.*
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID


SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*
FROM         dbo.tblSalesOrderDetail INNER JOIN
                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID)

				DECLARE @PicupID int
				DECLARE @DestinationID int
				
				   SELECT   @PicupID = dbo.tblConsignment.PickupID,@DestinationID=dbo.tblConsignment.DestinationID
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID

select * from dbo.tblAddress where AddressID =@PicupID
select * from dbo.tblAddress where AddressID =@DestinationID
			
END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
