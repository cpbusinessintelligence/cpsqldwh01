SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create  PROCEDURE [dbo].[SPCPPL_CreateOpenAccountTran]
           --@AddressId int=null,
           @Position nvarchar(max)=null,
           @IsAccountPaymentContact bit=null,
           --@AccountAddressId int=null,
           --@BusinessAddressId int=null,
           @TradingName nvarchar(max)=null,
           @ACN nvarchar(max)=null,
           @IsRegisteredForGST bit=null,
           @CompanyWebsite nvarchar(max)=null,
           --@PostalAddressId int=null,
           @MonthlyDomesticVolume nvarchar(max)=null,
           @CompanyUsedDomesticShipping nvarchar(max)=null,
           @MonthlyInternationalVolume nvarchar(max)=null,
           @CompanyUsedInternationalShipping nvarchar(max)=null,
           @CreditRequested nvarchar(max)=null,
           @PaymentTermsRequested nvarchar(max)=null,
           @IsAgree bit=null,
           @IsAuthorize bit=null,
 @userId int=null,
           
@AddressfirstName nvarchar(50)=null,
@AddresslastName nvarchar(50)=null,
@Addressemail nvarchar(250)=null,
@Addressphone nvarchar(20)=null,
           
@AccountfirstName nvarchar(50)=null,
@AccountlastName nvarchar(50)=null,
@Accountemail nvarchar(250)=null,
@Accountphone nvarchar(20)=null,

@BusinesscompanyName nvarchar(100)=null,
@Businessaddress1 nvarchar(200)=null,
@Businessaddress2 nvarchar(200)=null,
@Businesssuburb nvarchar(100)=null,

@PostalcompanyName nvarchar(100)=null,
@Postaladdress1 nvarchar(200)=null,
@Postaladdress2 nvarchar(200)=null,
@Postalsuburb nvarchar(100)=null,

@IsAddressSameAsAccount bit = 0,
@IsBusinessSameAsPostal bit = 0 
           
AS
BEGIN
begin tran


-------------------------------------------------
DECLARE @AddressAddID int
	 

if exists(select * from [tblAddress] where 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @AddressfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@AddresslastName)) and 
rtrim(ltrim([Phone])) =  rtrim(ltrim(@Addressphone))  )
begin
select @AddressAddID = AddressID from [tblAddress] where 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @AddressfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@AddresslastName)) and 
rtrim(ltrim([Phone])) =  rtrim(ltrim(@Addressphone))  
end
else

begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],   [email],
	 [Phone] ,[CreatedDateTime],[CreatedBy])
	 VALUES
      ( @userId,@AddressfirstName ,@AddresslastName  ,@Addressemail, 
      @Addressphone ,GETDATE(), @userId  )
        SET @AddressAddID = SCOPE_IDENTITY()
end
			--select @AddressAddID
			-------------------------------------------------
			 
DECLARE @AccountAddID int

IF (@IsAddressSameAsAccount=1)
   set @AccountAddID = @AddressAddID
ELSE 
   BEGIN
if exists(select * from [tblAddress] where 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @AccountfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@AccountlastName)) and 
rtrim(ltrim([phone])) =  rtrim(ltrim(@Accountphone))  )
begin
select @AccountAddID = AddressID from [tblAddress] where 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @AccountfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@AccountlastName)) and 
rtrim(ltrim([phone])) =  rtrim(ltrim(@Accountphone))

end
else

begin

	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],  [Email] 
	  ,[Phone], [CreatedDateTime],[CreatedBy]  )
	 VALUES
      ( @userId,@AccountfirstName ,@AccountlastName  ,@Accountemail 
       ,@Accountphone, GETDATE(), @userId  )
        SET @AccountAddID = SCOPE_IDENTITY()
end
   end
			--select @AccountAddID
			-------------------------------------------------
			---------------------

DECLARE @BusinessAddID int


 
       


if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@BusinesscompanyName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Businessaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Businessaddress2 ))and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Businesssuburb ))   )
begin
select @BusinessAddID = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@BusinesscompanyName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Businessaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Businessaddress2 ))and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Businesssuburb ))
end
else

begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID],   [CompanyNAme],  [Address1],[Address2],[Suburb] 
	  ,[CreatedDateTime],[CreatedBy]   )
	 VALUES
      ( @userId, @BusinesscompanyName,  @Businessaddress1,@Businessaddress2,@Businesssuburb 
        ,GETDATE(), @userId  )
        SET @BusinessAddID = SCOPE_IDENTITY()
end
       

			--select @BusinessAddID
			
			-------------------------------------------------
			
			DECLARE @PostalAddID int


 
       

IF (@IsBusinessSameAsPostal=1)
   set @PostalAddID = @BusinessAddID
ELSE 
   BEGIN

if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@PostalcompanyName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Postaladdress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Postaladdress2 ))and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Postalsuburb ))   )
begin
select @PostalAddID = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@PostalcompanyName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Postaladdress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Postaladdress2 ))and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Postalsuburb ))
end
else

begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID],   [CompanyNAme],  [Address1],[Address2],[Suburb] 
	  ,[CreatedDateTime],[CreatedBy]   )
	 VALUES
      ( @userId, @PostalcompanyName,  @Postaladdress1,@Postaladdress2,@Postalsuburb 
        ,GETDATE(), @userId  )
        SET @PostalAddID = SCOPE_IDENTITY()
end
			
			
			end
			
			
				DECLARE @ConsignmentIDret int
		INSERT INTO [dbo].[tblOpenAccount]
           ([AddressId]
           ,[Position]
           ,[IsAccountPaymentContact]
           ,[AccountAddressId]
           ,[BusinessAddressId]
           ,[TradingName]
           ,[ACN]
           ,[IsRegisteredForGST]
           ,[CompanyWebsite]
           ,[PostalAddressId]
           ,[MonthlyDomesticVolume]
           ,[CompanyUsedDomesticShipping]
           ,[MonthlyInternationalVolume]
           ,[CompanyUsedInternationalShipping]
           ,[CreditRequested]
           ,[PaymentTermsRequested]
           ,[IsAgree]
           ,[IsAuthorize])
     VALUES
           (           @AddressAddID  ,
           @Position ,
           @IsAccountPaymentContact ,
           @AccountAddID  ,
           @BusinessAddID  ,
           @TradingName ,
           @ACN ,
           @IsRegisteredForGST ,
           @CompanyWebsite ,
           @PostalAddId  ,
           @MonthlyDomesticVolume ,
           @CompanyUsedDomesticShipping ,
           @MonthlyInternationalVolume ,
           @CompanyUsedInternationalShipping ,
           @CreditRequested ,
           @PaymentTermsRequested ,
           @IsAgree ,
           @IsAuthorize )
            SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret
			
			
			--------------------------------
			
			 if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateConsignmentStagingTran]'
           , 2)
  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
  end
  else
  commit tran          
            

END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
