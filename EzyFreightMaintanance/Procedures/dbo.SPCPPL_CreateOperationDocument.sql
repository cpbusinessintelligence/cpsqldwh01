SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_CreateOperationDocument]

@OperationDocSubject varchar(100)= null,
@OperationDocDescription varchar(500)= null,
@OperationDocState varchar(10)= null,
@OperationDocIsActive bit= 0,
@IsFavourite bit= 0,
@CreatedBy int= null,
@DtOPDocFileList [DtOPDocFileList] readonly

As Begin
Begin Try
BEGIN TRAN 

Declare @OperationDocId int

INSERT INTO CPPLWeb_8_3_Admin..tblOperationDocuments
           ([OperationDocSubject],[OperationDocDescription],[OperationDocState],[OperationDocIsActive],[IsFavourite],[CreatedBy],[CreatedDateTime])
     VALUES
           (@OperationDocSubject ,@OperationDocDescription ,@OperationDocState , @OperationDocIsActive ,@IsFavourite ,@CreatedBy ,Getdate())


SET @OperationDocId = Scope_identity() 

INSERT INTO CPPLWeb_8_3_Admin..[tblOperationDocumentFiles]
           ([OperationDocName],[OperationDocAttachment],[OperationDocId],[CreatedBy],[CreatedDateTime])
     (select [OperationDocName],[OperationDocAttachment],@OperationDocId,@CreatedBy, Getdate() from @DtOPDocFileList)

Select * from CPPLWeb_8_3_Admin..tblOperationDocuments where OperationDocId = @OperationDocId

COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SPCPPL_CreateOperationDocument', 2)
		end
		END CATCH

END 
GO
