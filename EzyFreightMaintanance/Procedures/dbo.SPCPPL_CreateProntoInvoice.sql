SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateProntoInvoice]

@ProntoAccountNo varchar(50) = null ,
@WebsiteAccountNo varchar(50) = null ,
@CompanyName varchar(50) = null ,
@ContactAddress nvarchar(500) = null ,
@ContactFname  varchar(50) = null ,
@ContactLname  varchar(50) = null ,
@ContactNo  varchar(50) = null ,
@EmailId varchar(150) = null ,

@CreditCardSurCharge decimal(18,2) = null ,
@GrossTotal decimal(18,2) = null ,
@GST decimal(18,2) = null ,
@FuelSurCharge decimal(18,2) = null ,
@Insurance decimal(18,2) = null ,
@OtherCharge decimal(18,2) = null ,
@NetTotal decimal(18,2) = null ,
@TotalOwingAmount decimal(18,2) = null ,
@PaidAmount decimal(18,2) = null ,
@CreatedBy varchar(50) = null ,

@dtProntoInvoice  [dtProntoInvoiceList] readonly


AS
BEGIN
begin tran
BEGIN TRY
DECLARE @ReceiptID int,
		@ReceiptNo varchar(50),
		@RecCode varchar(5)

set @RecCode = 'CPWRE' 
if (select count(*) FROM tblprontoreceipt) > 0 
begin
SELECT @ReceiptNo = MAX (RIGHT(ReceiptNo, 6))+1
FROM tblprontoreceipt WHERE left(ReceiptNo,LEN(@RecCode)) = @RecCode
SELECT @ReceiptNo = RIGHT('000000' + replace(@ReceiptNo,'-',''), 6) 
if(@ReceiptNo IS NULL)
SET @ReceiptNo = '000000' 
end
else
begin 
SET @ReceiptNo =  '000001'
end

--------------------------------Pronto Receipt Entry------------------------------------------------------------------------------------------------------
INSERT INTO [dbo].[tblProntoReceipt]
           ([ReceiptNo]
           ,[AccountNo]
           ,[WebsiteAccountNo]
           ,[GrossTotal]
		   ,[CreditCardSurCharge]
           ,[GST]
           ,[FuelSurCharge]
           ,[Insurance]
           ,[OtherCharge]
           ,[NetTotal]
		   ,[TotalOwingAmount]
           ,[PaidAmount]
           ,[CreatedDateTime]
           ,[CreatedBy])
			VALUES(
			@RecCode + @ReceiptNo,
			@ProntoAccountNo,
			@WebsiteAccountNo,
			@GrossTotal,
			@CreditCardSurCharge,
			@GST,
			@FuelSurCharge,
			@Insurance,
			@OtherCharge,
			@NetTotal,
			@TotalOwingAmount,
			@PaidAmount,
			GETDATE(),
			@CreatedBy)
   
SET @ReceiptID = SCOPE_IDENTITY()

-----------------------------------------------------------------------------------------------------------------------------------------

--------------------------------Pronto Invoice Entry-------------------------------------------------------------------------------------


INSERT INTO [dbo].[tblProntoInvoice]
           ([InvoiceNo]
           ,[InvoiceDate]
           ,[ProntoAccountNo]
           ,[WebsiteAccountNo]
           ,[CompanyName]
           ,[ContactFname]
           ,[ContactLname]
           ,[ContactAddress]
           ,[ContactNo]
		   ,[Email]
           ,[GrossTotal]
           ,[GST]
           ,[FuelSurCharge]
           ,[Insurance]
           ,[OtherCharge]
           ,[NetTotal]
		   ,[TotalOwingAmount]
           ,[PaidAmount]
           ,[IsPaymentProcessed]
           ,[IsReceiptProcessed]
           ,[MessageFromPronto]
           ,[CreatedDateTime]
           ,[CreatedBy],[ReceiptNo])
     Select [InvoiceNo] ,[InvoiceDate],@ProntoAccountNo,@WebsiteAccountNo,@CompanyName,@ContactFname,@ContactLname,@ContactAddress,@ContactNo,@EmailId,[GrossTotal],[GST]
	 ,[FuelSurCharge],[Insurance],[OtherCharge],[NetTotal],[TotalOwingAmt],[PaidAmount],0,0,[MessageFromPronto] 
			,getdate(),@CreatedBy, @RecCode + @ReceiptNo
			from @dtProntoInvoice
	 
	 
--------------------------------------------------------------------------------------------------------

select @RecCode + @ReceiptNo as ReceiptNo ,  @ReceiptID as ReceiptId

--------------------------------------------------------------------------------------------------------


commit tran          
END TRY
BEGIN CATCH
begin
rollback tran
INSERT INTO [dbo].[tblErrorLog]
		   ([Error]
			,[FunctionInfo]
			,[ClientId])
VALUES
            (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
		    ,'SPCPPL_CreateProntoInvoice', 2)

end
END CATCH  
End
GO
