SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateReWeightInvoice]
@ConsignmentID int = null,
@GrossTotal decimal(19,4) ,
@GST decimal(19,4) ,
@NetTotal decimal(19,4) ,
@dtSalesOrderDetail dtSalesOrderDetail READONLY,
@TotalFreightExGST decimal(19,4) = null,
@TotalFreightInclGST decimal(19,4) = null,
@SendToPronto bit = null,
@PaymentRefNo nvarchar(100)=null,
@AuthorizationCode nvarchar(100)=null,
@MerchantReferenceCode nvarchar(100)=null,
@SubscriptionID  nvarchar(max)= null,
@CreatedBy nvarchar(max)= null
           
AS
BEGIN
begin tran
Begin Try

declare    @UserID int= null,
           @IsRegUserConsignment bit= null,
           @PickupID int= null,
           @DestinationID int= null,
           @ContactID int= null,
           @TotalWeight decimal(10,2)= null,
           @TotalVolume decimal(10,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           --@CreatedBy int= null,
		   @IsSignatureReq bit = 0,
           @IsDocument bit = 0,
           @ConsignmentCode nvarchar(3)



SELECT 
      @UserID =[UserID]
      ,@IsRegUserConsignment=[IsRegUserConsignment]
      ,@PickupID=[PickupID]
      ,@DestinationID=[DestinationID]
      ,@ContactID=[ContactID]
      ,@TotalWeight=[TotalWeight]
      ,@TotalVolume=[TotalVolume]
      ,@NoOfItems=[NoOfItems]
      ,@SpecialInstruction=[SpecialInstruction]
      ,@CustomerRefNo=[CustomerRefNo]
      ,@ClosingTime=[ClosingTime]
      ,@DangerousGoods=[DangerousGoods]
      ,@Terms=[Terms]
      ,@RateCardID=[RateCardID]
      --,@CreatedBy = [CreatedBy]
      ,@IsSignatureReq =[IsSignatureReq]
      ,@IsDocument =[IsDocument]
      ,@ConsignmentCode=SUBSTRING(ConsignmentCode, 1, 3)
  FROM [dbo].[tblConsignment] where ConsignmentID = @ConsignmentID
 
 DECLARE @SalesOrderIDret int
 INSERT INTO [dbo].[tblSalesOrder]
           ([ReferenceNo]
           ,[UserID]
           ,[NoofItems]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[RateCardID]
           ,[GrossTotal]
           ,[GST]
           ,[NetTotal]
           ,[SalesOrderStatus]
           ,[InvoiceNo]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
           VALUES
           ( 
           @ConsignmentID ,
           @UserID  ,
           @NoofItems  ,
           @TotalWeight  ,
           @TotalVolume  ,
           @RateCardID  ,
           @GrossTotal ,
           @GST  ,
           @NetTotal  ,
           7  ,
           null ,
           GETDATE() ,
          @CreatedBy )

SET @SalesOrderIDret = SCOPE_IDENTITY()

---------SalesOrderDetail ------------------------
             
INSERT INTO [dbo].[tblSalesOrderDetail]
           ([SalesOrderID]
           ,[Description]
           ,[LineNo]
           ,[Weight]
           ,[Volume]
           ,[FreightCharge]
           ,[FuelCharge]
           ,[InsuranceCharge]
           ,[ServiceCharge]
           ,[Total]
           ,[CreatedDateTime]
           ,[CreatedBy])
           select @SalesOrderIDret,
           strDescription,
           [strLineNo],
           --case when strPhysicalWeight='' then null else    cast( strPhysicalWeight as decimal(10,2))end,
        case when  ISNUMERIC(strPhysicalWeight)=1 then CAST(strPhysicalWeight AS decimal(10,2))else null end ,
          --case when strDeclareVolume='' then null else    cast(  strDeclareVolume as decimal(10,2))end,
        case when  ISNUMERIC(strDeclareVolume)=1 then CAST(strDeclareVolume AS decimal(10,2))else null end ,
           --case when strFreightCharge='' then null else    cast(  strFreightCharge as decimal(10,2))end,
        case when  ISNUMERIC(strFreightCharge)=1 then CAST(strFreightCharge AS decimal(10,2))else null end ,
           --case when strFuelCharge='' then null else    cast(  strFuelCharge as decimal(19,4))end,
        case when  ISNUMERIC(strFuelCharge)=1 then CAST(strFuelCharge AS decimal(19,4))else null end ,
          --case when strInsuranceCharge='' then null else    cast(   strInsuranceCharge as decimal(19,4))end,
        case when  ISNUMERIC(strInsuranceCharge)=1 then CAST(strInsuranceCharge AS decimal(19,4))else null end ,
         --case when strServiceCharge='' then null else    cast(    strServiceCharge as decimal(19,4))end,
        case when  ISNUMERIC(strServiceCharge)=1 then CAST(strServiceCharge AS decimal(19,4))else null end ,
           --cast (strFreightCharge as decimal(19,4)) +
        case when  ISNUMERIC(strFreightCharge)=1 then CAST(strFreightCharge AS decimal(19,4))else 0.0 end +
           --cast (strFuelCharge as decimal(19,4))+
        case when  ISNUMERIC(strFuelCharge)=1 then CAST(strFuelCharge AS decimal(19,4))else 0.0 end +
           --cast (strInsuranceCharge as decimal(19,4))+
        case when  ISNUMERIC(strInsuranceCharge)=1 then CAST(strInsuranceCharge AS decimal(19,4))else 0.0 end +
           --cast (strServiceCharge as decimal(19,4)),
        case when  ISNUMERIC(strServiceCharge)=1 then CAST(strServiceCharge AS decimal(19,4))else 0.0 end,
           getdate(),
           strCreatedBy
           
from @dtSalesOrderDetail
            
            
------------------Invoice ------------------------
            
declare  @InvoiceNumber nvarchar(5), @GeneratedInvoiceNo nvarchar(30) = null
set @InvoiceNumber=  SUBSTRING(@ConsignmentCode, 1, 3)+'IN'
InvoiceNoGeneration: 
Set @GeneratedInvoiceNo = @InvoiceNumber + [dbo].[GenerateInvoiceNumber](@InvoiceNumber)

--declare @InvoiceCode7Digit nvarchar(7)
--select @InvoiceCode7Digit = max (RIGHT(InvoiceNumber, 7))+1  from tblInvoice where left(InvoiceNumber,5) = @InvoiceNumber
--SELECT @InvoiceCode7Digit =  RIGHT('0000000' + replace(@InvoiceCode7Digit,'-',''),7)

--if(@InvoiceCode7Digit is null)
--set @InvoiceCode7Digit = '0000000'

if exists(select * from tblinvoice where [InvoiceNumber] = @GeneratedInvoiceNo )
begin
Goto InvoiceNoGeneration;
end


DECLARE @InvoiceID int
INSERT INTO [dbo].[tblInvoice]
           ([InvoiceNumber]
           ,[UserID]
           ,[PickupAddressID]
           ,[DestinationAddressID]
           ,[ContactAddressID]
           ,[TotalFreightExGST]
           ,[GST]
           ,[TotalFreightInclGST]
           ,[InvoiceStatus]
           ,[SendToPronto]
           ,[CreatedDateTime]
           ,[CreatedBY]
           ,[PaymentRefNo]
           ,[MerchantReferenceCode]
           ,[SubscriptionID]
           ,[AuthorizationCode])
     VALUES
           (@GeneratedInvoiceNo,		--@InvoiceNumber+@InvoiceCode7Digit , 
           @UserID , 
           @PickupID,
		   @DestinationID,
		   @ContactID ,
           @GrossTotal,
           @GST , 
           @NetTotal,
           11 , 
           @SendToPronto , 
           GETDATE() , 
           @CreatedBY ,
           @PaymentRefNo,
           @MerchantReferenceCode,
           @SubscriptionID,
           @AuthorizationCode)

SET @InvoiceID = SCOPE_IDENTITY()
          
           
---------------- Update Salse order ---------------
           
update tblSalesOrder set InvoiceNo =  @GeneratedInvoiceNo , SalesOrderStatus = 9 where SalesOrderID=  @SalesOrderIDret
           
SELECT     dbo.tblInvoice.*
FROM         dbo.tblInvoice where dbo.tblInvoice.InvoiceID = @InvoiceID
           
SELECT     dbo.tblConsignment.*
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID


SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*
FROM         dbo.tblSalesOrderDetail 
INNER JOIN dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
WHERE     (dbo.tblSalesOrder.SalesOrderID=  @SalesOrderIDret)

DECLARE @PicupID int
				
SELECT   @PicupID = dbo.tblConsignment.PickupID,@DestinationID=dbo.tblConsignment.DestinationID
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID

SELECT   @PicupID = dbo.tblConsignment.PickupID,@DestinationID=dbo.tblConsignment.DestinationID
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID


SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID

SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID

SELECT *  FROM [dbo].[tblItemLabel] where ConsignmentID = @ConsignmentID

SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID
                       where AddressID =@ContactID


commit tran          
End Try
Begin Catch
	  rollback tran

	  INSERT INTO [dbo].[tblErrorLog]
			   ([Error]
			   ,[FunctionInfo]
			   ,[ClientId])
		 VALUES
			   (ERROR_LINE() +' : '+ERROR_MESSAGE()
			   ,'[SPCPPL_CreateReWeightInvoice]'
			   , 2)

End Catch
END
GO
