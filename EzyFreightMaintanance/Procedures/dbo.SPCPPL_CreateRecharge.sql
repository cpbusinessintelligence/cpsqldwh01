SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateRecharge]
      @ConsignmentID int = null,
           @AdminFee decimal(18,2) = null,
           @Difference decimal(18,2) = null,
           @ETA nvarchar(max) = null,
           @ExtensionData nvarchar(100) = null,
           @GrossInvoiceAmount decimal(18,2) = null,
           @GSTonGrossInvoiceAmount decimal(18,2) = null,
           @NetTotal decimal(18,2) = null,
           @NewCalculatedTotal decimal(18,2) = null,
           @NewWeightforCalculation decimal(18,2) = null,
           @ReweighStatus nvarchar(max) = null,
           @ReweighStatusDescription nvarchar(max) = null,
           @ServiceCode nvarchar(max) = null,
           @ServiceDescription nvarchar(max) = null,
           @SignatureOption nvarchar(max) = null,
           @IsSignatureReq bit = null,
           @OrijnalCalculatedTotal decimal(18,2) = null,
           @PaymentRefNo nvarchar(max) = null,
           @IsProcessed bit = null,
           
           @CreatedBy int = null,
           @UpdatedDateTime datetime = null,
           @UpdatedBy int = null,
           @MerchantReferenceCode nvarchar(max) = null
           
AS

begin

if exists(select * from [dbo].[tblRecharge] where [ConsignmentID] = @ConsignmentID)
   BEGIN
    update [dbo].[tblRecharge] set 
           [AdminFee]						  = case when @AdminFee IS  null then [AdminFee] else @AdminFee end,
           [Difference]					  = case when @Difference IS  null then [Difference] else @Difference end,
           [ETA]							  = case when @ETA IS  null then [ETA] else @ETA end,
           [ExtensionData]					  = case when @ExtensionData IS  null then [ExtensionData] else @ExtensionData end,
           [GrossInvoiceAmount]			  = case when @GrossInvoiceAmount IS  null then [GrossInvoiceAmount] else @GrossInvoiceAmount end,
           [GSTonGrossInvoiceAmount]		  = case when @GSTonGrossInvoiceAmount IS  null then [GSTonGrossInvoiceAmount] else @GSTonGrossInvoiceAmount end,
           [NetTotal]						  = case when @NetTotal IS  null then [NetTotal] else @NetTotal end,
           [NewCalculatedTotal]			  = case when @NewCalculatedTotal IS  null then [NewCalculatedTotal] else @NewCalculatedTotal end,
           [NewWeightforCalculation]		  = case when @NewWeightforCalculation IS  null then [NewWeightforCalculation] else @NewWeightforCalculation end,
           [ReweighStatus]					  = case when @ReweighStatus IS  null then [ReweighStatus] else @ReweighStatus end,
           [ReweighStatusDescription]		  = case when @ReweighStatusDescription IS  null then [ReweighStatusDescription] else @ReweighStatusDescription end,
           [ServiceCode]					  = case when @ServiceCode IS  null then [ServiceCode] else @ServiceCode end,
           [ServiceDescription]			  = case when @ServiceDescription IS  null then [ServiceDescription] else @ServiceDescription end,
           [SignatureOption]				  = case when @SignatureOption IS  null then [SignatureOption] else @SignatureOption end,
           [IsSignatureReq]				  = case when @IsSignatureReq IS  null then [IsSignatureReq] else @IsSignatureReq end,
           [OrijnalCalculatedTotal]		  = case when @OrijnalCalculatedTotal IS  null then [OrijnalCalculatedTotal] else @OrijnalCalculatedTotal end,
           [PaymentRefNo]					  = case when @PaymentRefNo IS  null then [PaymentRefNo] else @PaymentRefNo end,
           [IsProcessed]					  = case when @IsProcessed IS  null then [IsProcessed] else @IsProcessed end,
											  
 
           [UpdatedDateTime]				  = GETDATE(),
           [UpdatedBy]						  =   @UpdatedBy  ,
           [MerchantReferenceCode]			  = case when @MerchantReferenceCode IS  null then [MerchantReferenceCode] else @ConsignmentID end
           
           where 
                     [ConsignmentID]                   =  @ConsignmentID 
    
   END

 ELSE 
   BEGIN
       INSERT INTO   [dbo].[tblRecharge]
           ([ConsignmentID]
           ,[AdminFee]
           ,[Difference]
           ,[ETA]
           ,[ExtensionData]
           ,[GrossInvoiceAmount]
           ,[GSTonGrossInvoiceAmount]
           ,[NetTotal]
           ,[NewCalculatedTotal]
           ,[NewWeightforCalculation]
           ,[ReweighStatus]
           ,[ReweighStatusDescription]
           ,[ServiceCode]
           ,[ServiceDescription]
           ,[SignatureOption]
           ,[IsSignatureReq]
           ,[OrijnalCalculatedTotal]
           ,[PaymentRefNo]
           ,[IsProcessed]
           
           ,[CreatedBy]
            
           ,[MerchantReferenceCode])
     VALUES
         ( @ConsignmentID  ,
           @AdminFee  ,
           @Difference  ,
           @ETA  ,
           @ExtensionData ,  
           @GrossInvoiceAmount  ,
           @GSTonGrossInvoiceAmount  ,
           @NetTotal  ,
           @NewCalculatedTotal  ,
           @NewWeightforCalculation  ,
           @ReweighStatus  ,
           @ReweighStatusDescription  ,
           @ServiceCode  ,
           @ServiceDescription  ,
           @SignatureOption  ,
           @IsSignatureReq ,
           @OrijnalCalculatedTotal  ,
           @PaymentRefNo  ,
           @IsProcessed ,
          
           @CreatedBy  ,
          
           @MerchantReferenceCode 
           )
    END

end
GO
