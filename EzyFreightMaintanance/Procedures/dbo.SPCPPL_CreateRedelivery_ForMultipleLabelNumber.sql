SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_CreateRedelivery_ForMultipleLabelNumber]
	   @RedeliveryType varchar(100)  = null ,
	   @LabelNumber varchar(100)  = null,
           @CardReferenceNumber varchar(50)  = null ,
           @ConsignmentCode varchar(50)  = null ,
           @Branch varchar(50)  = null ,
           @StateCode varchar(50)  = null ,
           @AttemptedRedeliveryTime datetime  = null ,
           @SenderName varchar(50)  = null ,
           @NumberOfItem int  = null ,
           @DestinationName varchar(50)  = null ,
           @DestinationAddress varchar(200)  = null ,
           @DestinationSuburb varchar(50)  = null ,
           @DestinationPostCode int  = null ,
           @SelectedETADate date  = null ,
           @PreferDeliverTime varchar(40)  = null ,
           @PreferDeliverTimeSlot varchar(40)  = null ,
           @Firstname    nvarchar(250)  = null ,
           @Lastname     nvarchar(250)  = null ,
           @Email        nvarchar(250)  = null ,
           @PhoneNumber  nvarchar(20)  = null ,
           @SPInstruction  nvarchar(max)  = null ,
           @CreatedBy int  = null 
           
AS
BEGIN
BEGIN TRY
           
   DECLARE  @State int  = null
   
   SELECT @State=[StateID] FROM [dbo].[tblState] where [StateCode]=@StateCode

   DECLARE @RedeliveryId int
   
   INSERT INTO [dbo].[tblRedelivery]
           ([RedeliveryType]
		   ,[CardReferenceNumber]
           ,[LableNumber]
           ,[ConsignmentCode]
           ,[Branch]
           ,[State]
           ,[AttemptedRedeliveryTime]
           ,[SenderName]
           ,[NumberOfItem]
           ,[DestinationName]
           ,[DestinationAddress]
           ,[DestinationSuburb]
           ,[DestinationPostCode]
           ,[SelectedETADate]
           ,[PreferDeliverTime]
           ,[CreatedBy]
	       ,[Firstname]  
		   ,[Lastname]  
		   ,[Email]     
		   ,[PhoneNumber] 
           ,[PreferDeliverTimeSlot]
           ,[SPInstruction])
     VALUES
           (@RedeliveryType,
		   @CardReferenceNumber  ,
           @LabelNumber  ,
           @ConsignmentCode  ,
           @Branch  ,
           @State  ,
           @AttemptedRedeliveryTime ,
           @SenderName ,
           @NumberOfItem ,
           @DestinationName ,
           @DestinationAddress ,
           @DestinationSuburb ,
           @DestinationPostCode ,
           @SelectedETADate  ,
           @PreferDeliverTime ,
           @CreatedBy ,
		   @Firstname  , 
		   @Lastname    ,
		   @Email       ,
		   @PhoneNumber ,
		   @PreferDeliverTimeSlot,
		   @SPInstruction)

SET @RedeliveryId = SCOPE_IDENTITY()

select @RedeliveryId

END TRY

BEGIN CATCH

begin

	INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
	VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
	,'[SPCPPL_CreateRedelivery_ForMultipleLabelNumber]', 2)

end

END CATCH				
End
GO
