SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateRefund]
           @ConsignmentID int= null,
           @Reason nvarchar(max)= null,
           @Amount decimal(18,2)= null,
           @PaymentRefNo nvarchar(100)= null,
           @AuthorizationCode nvarchar(100)= null,
	   @RefundCategory nvarchar(150)= null,
           @CreatedBy int= null
AS
BEGIN
INSERT INTO [dbo].[tblRefund]
           ([ConsignmentID]
           ,[Reason]
           ,[Amount]
           ,[PaymentRefNo]
           ,[AuthorizationCode]
	   ,[RefundCategory]
           ,[CreatedBy]
            )
     VALUES
           (@ConsignmentID
           ,@Reason
           ,@Amount
           ,@PaymentRefNo
           ,@AuthorizationCode
	   ,@RefundCategory
           ,@CreatedBy
            )
END
GO
