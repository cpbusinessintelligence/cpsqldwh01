SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateRefund_ForRedirection]
           @ReConsignmentID int= null,
           @Reason nvarchar(max)= null,
           @Amount decimal(18,2)= null,
           @PaymentRefNo nvarchar(100)= null,
           @AuthorizationCode nvarchar(100)= null,
           @CreatedBy int= null
AS
BEGIN
INSERT INTO [dbo].[tblRedirectedRefund]
           ([ReConsignmentID]
           ,[Reason]
           ,[Amount]
           ,[PaymentRefNo]
           ,[AuthorizationCode]
           ,[CreatedBy]
            )
     VALUES
           (@ReConsignmentID
           ,@Reason
           ,@Amount
           ,@PaymentRefNo
           ,@AuthorizationCode
           ,@CreatedBy
            )
END
GO
