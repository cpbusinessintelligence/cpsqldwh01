SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateShipingUsingBulkWithOutPay] 
@TotalMeasureWeight    DECIMAL(10, 4)= NULL, 
@ProntoDataExtracted   BIT= NULL, 
@LastActivity          VARCHAR(200)= NULL, 
@LastActiivityDateTime DATETIME= NULL, 
@EDIDataProcessed      BIT= NULL, 
@ConsignmentCode       VARCHAR(40)= NULL, 
@ConsignmentStagingID  INT= NULL, 
@GrossTotal            DECIMAL(19, 4) = NULL, 
@GST                   DECIMAL(19, 4) = NULL, 
@NetTotal              DECIMAL(19, 4) = NULL, 
@dtItemLabel           DTITEMLABELCREATESHIPMENT_NEW readonly, 
@TotalFreightExGST     DECIMAL(19, 4) = NULL, 
@TotalFreightInclGST   DECIMAL(19, 4) = NULL, 
@InvoiceStatus         INT = NULL, 
@SendToPronto          BIT = NULL, 
@ClientCode            VARCHAR(50) = NULL 
AS 
  BEGIN 
    BEGIN TRY
      BEGIN TRAN 
      DECLARE @UserID                      INT= NULL, 
              @IsRegUserConsignment        BIT= NULL, 
              @PickupID                    INT= NULL, 
              @DestinationID               INT= NULL, 
              @ContactID                   INT= NULL, 
              @TotalWeight                 DECIMAL(19, 4)= NULL, 
              @TotalVolume                 DECIMAL(19, 4)= NULL, 
              @NoOfItems                   INT= NULL, 
              @SpecialInstruction          VARCHAR(500)= NULL, 
              @CustomerRefNo               VARCHAR(40)= NULL, 
              @ConsignmentPreferPickupDate DATE= NULL, 
              @ConsignmentPreferPickupTime VARCHAR(20)= NULL, 
              @ClosingTime                 VARCHAR(10)= NULL, 
              @DangerousGoods              BIT= NULL, 
              @Terms                       BIT= NULL, 
              @RateCardID                  NVARCHAR(50)= NULL, 
              @CreatedBy                   INT= NULL, 
              @IsSignatureReq              BIT = 0, 
              @IsATl                       BIT=NULL, 
              @IsDocument                  BIT = 0, 
              @SortCode                    NVARCHAR(max) = NULL, 
              @NatureOfGoods               NVARCHAR(max) = NULL, 
              @ETA                         NVARCHAR(max) = NULL 

      SELECT @UserID = [userid], 
             @IsRegUserConsignment = [isreguserconsignment], 
             @PickupID = [pickupid], 
             @DestinationID = [destinationid], 
             @ContactID = [contactid], 
             @TotalWeight = [totalweight], 
             @TotalVolume = [totalvolume], 
             @NoOfItems = [noofitems], 
             @SpecialInstruction = [specialinstruction], 
             @CustomerRefNo = [customerrefno], 
             @ConsignmentPreferPickupDate = [pickupdate], 
             @ConsignmentPreferPickupTime = [preferpickuptime], 
             @ClosingTime = [closingtime], 
             @DangerousGoods = [dangerousgoods], 
             @Terms = [terms], 
             @RateCardID = [ratecardid], 
             @IsATl = [isatl], 
             @CreatedBy = [createdby], 
             @IsSignatureReq = [issignaturereq], 
             @IsDocument = [isdocument], 
             @SortCode = [sortcode], 
             @NatureOfGoods = natureofgoods, 
             @ETA = [eta] 
      FROM   [dbo].[tblconsignmentstaging] 
      WHERE  consignmentstagingid = @ConsignmentStagingID 

declare @GeneratedConsignmentCode nvarchar(30) 
		ConsignmentCodeGeneration:
		Set @GeneratedConsignmentCode = @ConsignmentCode + [dbo].GenerateConsignmentCode(@ConsignmentCode)

		if exists(select * from tblconsignment where [ConsignmentCode] = @GeneratedConsignmentCode )
		begin
		Goto ConsignmentCodeGeneration;
		end

------------Commented By Shubham 06/02/2017  Created New Function to get the consignment code
	  
	  --DECLARE @ConsignmentCode9Digit NVARCHAR(9) 

      --SELECT @ConsignmentCode9Digit = Max (RIGHT(Replace(consignmentcode,'-T',''), 9)) + 1 
      --FROM   tblconsignment 
      --WHERE  LEFT(consignmentcode, LEN(@ConsignmentCode)) = @ConsignmentCode 
      --SELECT @ConsignmentCode9Digit = RIGHT('000000000'+ Replace(@ConsignmentCode9Digit, '-','') , 9) 
      --IF( @ConsignmentCode9Digit IS NULL ) 
      --  SET @ConsignmentCode9Digit = '000000000' 


DECLARE @ConsignmentIDret INT 

INSERT INTO [dbo].[tblconsignment] 
                  ([consignmentcode],[userid], [isreguserconsignment], [pickupid], [destinationid], [contactid], [totalweight], [totalvolume], 
					[noofitems], [specialinstruction], [customerrefno], [consignmentpreferpickupdate], [consignmentpreferpickuptime], [closingtime], 
					[dangerousgoods], [terms], [ratecardid], [lastactivity], [lastactiivitydatetime], [consignmentstatus], [edidataprocessed], 
                   [prontodataextracted], [createddatetime], [createdby], [isdocument], issignaturereq, sortcode, natureofgoods, eta, [isatl], 
                   [isprocessed], [isaccountcustomer], calculatedtotal, calculatedgst, clientcode) 

      VALUES      (@GeneratedConsignmentCode,			--@ConsignmentCode + @ConsignmentCode9Digit, 
                   @UserID, 
                   @IsRegUserConsignment, 
                   @PickupID, 
                   @DestinationID, 
                   @ContactID, 
                   @TotalWeight, 
                   @TotalVolume, 
                   @NoOfItems, 
                   @SpecialInstruction, 
                   @CustomerRefNo, 
                   @ConsignmentPreferPickupDate, 
                   @ConsignmentPreferPickupTime, 
                   @ClosingTime, 
                   @DangerousGoods, 
                   @Terms, 
                   @RateCardID, 
                   @LastActivity, 
                   @LastActiivityDateTime, 
                   1, 
                   0, 
                   0, 
                   Getdate(), 
                   @CreatedBy, 
                   @IsDocument, 
                   @IsSignatureReq, 
                   @SortCode, 
                   @NatureOfGoods, 
                   @ETA, 
                   @IsATl, 
                   1, 
                   1, 
                   @GrossTotal, 
                   @GST, 
                   @ClientCode ) 



      SET @ConsignmentIDret = Scope_identity() 



      UPDATE dbo.tblconsignmentstaging 

      SET    [isprocessed] = 1, 

             [updateddatettime] = Getdate(), 

             [updatedby] = @CreatedBy, 

             [consignmentid] = @ConsignmentIDret, 

             isaccountcustomer = 1 

      WHERE  consignmentstagingid = @ConsignmentStagingID 



      ---------ItemLabel ------------------------ 

      INSERT INTO [dbo].[tblitemlabel] 

                  ([consignmentid], 

                   [labelnumber], 

                   [length], 

                   [width], 

                   [height], 

                   [physicalweight], 

                   [quantity], 

                   [createddatetime], 

                   [createdby], 

                   [cubicweight]) 

      SELECT @ConsignmentIDret, 

             @GeneratedConsignmentCode 

             + Cast( strlabelnumber AS NVARCHAR),				--@ConsignmentCode + @ConsignmentCode9Digit 

             CASE 

               WHEN Isnumeric(length) = 1 THEN Cast(length AS DECIMAL(10, 2)) 

               ELSE NULL 

             END, 

             CASE 

               WHEN Isnumeric(width) = 1 THEN Cast(width AS DECIMAL(10, 2)) 

               ELSE NULL 

             END, 

             CASE 

               WHEN Isnumeric(height) = 1 THEN Cast(height AS DECIMAL(10, 2)) 

               ELSE NULL 

             END, 

             CASE 

               WHEN Isnumeric(physicalweight) = 1 THEN Cast( 

               physicalweight AS DECIMAL(10, 4)) 

               ELSE NULL 

             END, 

   CASE 

               WHEN Isnumeric(quantity) = 1 THEN 

               Cast(quantity AS DECIMAL(10, 2)) 

               ELSE NULL 

             END, 

             Getdate(), 

             strcreatedby, 

             CASE 

               WHEN Isnumeric(cubicweight) = 1 THEN 

               Cast(cubicweight AS DECIMAL(10, 4)) 

               ELSE NULL 

             END 

      FROM   @dtItemLabel; 



      SELECT dbo.tblconsignment.* 

      FROM   dbo.tblconsignment 

      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentIDret 



      DECLARE @PicupID INT 



      SELECT @PicupID = dbo.tblconsignment.pickupid, 

             @DestinationID = dbo.tblconsignment.destinationid 

      FROM   dbo.tblconsignment 

      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentIDret 



      SELECT dbo.tbladdress.addressid, 

             dbo.tbladdress.userid, 

             dbo.tbladdress.firstname, 

             dbo.tbladdress.lastname, 

             dbo.tbladdress.companyname, 

             dbo.tbladdress.email, 

             dbo.tbladdress.address1, 

             dbo.tbladdress.address2, 

             dbo.tbladdress.suburb, 

             dbo.tbladdress.postcode, 

             dbo.tbladdress.phone, 

             dbo.tbladdress.mobile, 

             dbo.tbladdress.createddatetime, 

             dbo.tbladdress.createdby, 

             dbo.tbladdress.updateddatetime, 

             dbo.tbladdress.updatedby, 

             dbo.tbladdress.isregisteraddress, 

             dbo.tbladdress.isdeleted, 

             dbo.tbladdress.isbusiness, 

             dbo.tbladdress.issubscribe, 

             dbo.tblstate.statename AS StateID 

      FROM   dbo.tbladdress 

             LEFT JOIN dbo.tblstate ON dbo.tbladdress.stateid = dbo.tblstate.stateid 

      WHERE  addressid = @PicupID 



      SELECT dbo.tbladdress.addressid, 

             dbo.tbladdress.userid, 

             dbo.tbladdress.firstname, 

             dbo.tbladdress.lastname, 

             dbo.tbladdress.companyname, 

             dbo.tbladdress.email, 

             dbo.tbladdress.address1, 

             dbo.tbladdress.address2, 

             dbo.tbladdress.suburb, 

             dbo.tbladdress.postcode, 

             dbo.tbladdress.phone, 

             dbo.tbladdress.mobile, 

             dbo.tbladdress.createddatetime, 

             dbo.tbladdress.createdby, 

             dbo.tbladdress.updateddatetime, 

             dbo.tbladdress.updatedby, 

             dbo.tbladdress.isregisteraddress, 

             dbo.tbladdress.isdeleted, 

             dbo.tbladdress.isbusiness, 

             dbo.tbladdress.issubscribe, 

             dbo.tblstate.statename AS StateID 

      FROM   dbo.tbladdress 

             LEFT JOIN dbo.tblstate 

                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 

      WHERE  addressid = @DestinationID 



      SELECT dbo.tbladdress.addressid, 

             dbo.tbladdress.userid, 

             dbo.tbladdress.firstname, 

             dbo.tbladdress.lastname, 

             dbo.tbladdress.companyname, 

             dbo.tbladdress.email, 

             dbo.tbladdress.address1, 

             dbo.tbladdress.address2, 

             dbo.tbladdress.suburb, 

             dbo.tbladdress.postcode, 

             dbo.tbladdress.phone, 

             dbo.tbladdress.mobile, 

             dbo.tbladdress.createddatetime, 

             dbo.tbladdress.createdby, 

             dbo.tbladdress.updateddatetime, 

             dbo.tbladdress.updatedby, 

             dbo.tbladdress.isregisteraddress, 

             dbo.tbladdress.isdeleted, 

             dbo.tbladdress.isbusiness, 

             dbo.tbladdress.issubscribe, 

             dbo.tblstate.statename AS StateID 

      FROM   dbo.tbladdress 

             LEFT JOIN dbo.tblstate 

                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 

      WHERE  addressid = @ContactID 



      SELECT * 

      FROM   [dbo].[tblitemlabel] 
      WHERE  consignmentid = @ConsignmentIDret 



      SELECT Cu.userid, 

             c.isregularshipper 

      FROM   tblcompany c 

             INNER JOIN tblcompanyusers Cu 

                     ON c.companyid = Cu.companyid 

      WHERE  ( c.isregularshipper = 1 ) 

             AND Cu.userid = @UserID 



      SELECT * FROM   @dtItemLabel 

COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SPCPPL_CreateShipingUsingBulkWithOutPay'
				   , 2)
		end
		END CATCH
  END 
GO
