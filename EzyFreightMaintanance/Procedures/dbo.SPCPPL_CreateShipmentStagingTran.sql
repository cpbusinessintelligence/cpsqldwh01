SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_CreateShipmentStagingTran] 
@UserID INT= NULL,
@IsRegUserConsignment BIT= NULL, 
--@PickupID int= null, 
--@DestinationID int= null, 
--@ContactID int= null, 
@TotalWeight VARCHAR(500)= NULL, 
@TotalVolume VARCHAR(500)= NULL, 
@NoOfItems INT= NULL, 
@SpecialInstruction VARCHAR(500)= NULL, 
@CustomerRefNo VARCHAR(40)= NULL, 
@ConsignmentPreferPickupDate  DATE= NULL, 
@ConsignmentPreferPickupTime  VARCHAR(20)= NULL, 
@ClosingTime                  VARCHAR(10)= NULL, 
@DangerousGoods               BIT= NULL, 
@Terms                        BIT= NULL, 
@RateCardID                   NVARCHAR(50)= NULL, 
@CreatedBy                    INT= NULL, 
@ServiceID                    INT= NULL, 
@PickupuserId                 INT=NULL, 
@PickupfirstName              NVARCHAR(50)=NULL, 
@PickuplastName               NVARCHAR(50)=NULL, 
@PickupcompanyName            NVARCHAR(100)=NULL, 
@Pickupemail                  NVARCHAR(250)=NULL, 
@Pickupaddress1               NVARCHAR(200)=NULL, 
@Pickupaddress2               NVARCHAR(200)=NULL, 
@Pickupsuburb                 NVARCHAR(100)=NULL, 
@PickupstateId                NVARCHAR(max)=NULL, 
@PickuppostalCode             NVARCHAR(20)=NULL, 
@Pickupphone                  NVARCHAR(20)=NULL, 
@Pickupmobile                 NVARCHAR(20)=NULL, 
@PickupcreatedBy              INT=NULL, 
@PickupisBusiness             BIT=0, 
@PickupisRegisterAddress      BIT=0, 
@PickupIsSubscribe            BIT=0, 
@DestinationuserId            INT=NULL, 
@DestinationfirstName         NVARCHAR(50)=NULL, 
@DestinationlastName          NVARCHAR(50)=NULL, 
@DestinationcompanyName       NVARCHAR(100)=NULL, 
@Destinationemail             NVARCHAR(250)=NULL, 
@Destinationaddress1          NVARCHAR(200)=NULL, 
@Destinationaddress2          NVARCHAR(200)=NULL, 
@Destinationsuburb            NVARCHAR(100)=NULL, 
@DestinationstateId           NVARCHAR(max)=NULL, 
@DestinationpostalCode        NVARCHAR(20)=NULL, 
@Destinationphone             NVARCHAR(20)=NULL, 
@Destinationmobile            NVARCHAR(20)=NULL, 
@DestinationcreatedBy         INT=NULL, 
@DestinationisBusiness        BIT=0, 
@DestinationisRegisterAddress BIT=0, 
@DestinationIsSubscribe       BIT=0, 
@ContactuserId                INT=NULL, 
@ContactfirstName             NVARCHAR(50)=NULL, 
@ContactlastName              NVARCHAR(50)=NULL, 
@ContactcompanyName           NVARCHAR(100)=NULL, 
@Contactemail                 NVARCHAR(250)=NULL, 
@Contactaddress1              NVARCHAR(200)=NULL, 
@Contactaddress2              NVARCHAR(200)=NULL, 
@Contactsuburb                NVARCHAR(100)=NULL, 
@ContactstateId               NVARCHAR(max)=NULL, 
@ContactpostalCode            NVARCHAR(20)=NULL, 
@Contactphone                 NVARCHAR(20)=NULL, 
@Contactmobile                NVARCHAR(20)=NULL, 
@ContactcreatedBy             INT=NULL, 
@ContactisBusiness            BIT=0, 
@ContactisRegisterAddress     BIT=0, 
@ContactIsSubscribe           BIT=0, 
@IsSameAsDestination          BIT = 0, 
@IsSameAsPickup               BIT = 0, 
@IsSignatureReq               BIT = 0, 
@IsDocument                   NVARCHAR(max) = NULL, 
@NatureOfGoods                NVARCHAR(max) = NULL, 
@SortCode                     NVARCHAR(max) = NULL, 
@ETA                          NVARCHAR(max) = NULL, 
@IsATL                        BIT = NULL, 
@GrossTotal                   DECIMAL(18, 2) = NULL, 
@GST                          DECIMAL(18, 2) = NULL, 
@accountNumber                NVARCHAR(100)=NULL, 
@ClientCode                   VARCHAR(50) = NULL 
AS 
BEGIN 
Begin Try
BEGIN TRAN 

DECLARE @stateIdbyCode INT 

------------------------------------------------- 
--SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  [statecode] = @DestinationstateId 
SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  lower([StateName]) = lower(@DestinationstateId) 

DECLARE @DestinationAddID INT 

IF EXISTS(SELECT * FROM   [tbladdress] WHERE Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@DestinationcompanyName) ) 
AND Rtrim(Ltrim([firstname])) = Rtrim(Ltrim(@DestinationfirstName)) 
AND Rtrim(Ltrim([lastname])) = Rtrim( Ltrim(@DestinationlastName)) 
AND Rtrim(Ltrim([address1])) = Rtrim( Ltrim(@Destinationaddress1)) 
AND Rtrim(Ltrim([address2])) = Rtrim( Ltrim(@Destinationaddress2)) 
AND Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@DestinationcompanyName)) 
AND Rtrim(Ltrim([suburb])) = Rtrim( Ltrim(@Destinationsuburb)) 
AND Rtrim(Ltrim([stateid])) = Rtrim(Ltrim(@stateIdbyCode) ) 
AND Rtrim(Ltrim([phone])) = Rtrim( Ltrim(@Destinationphone)) 
AND Rtrim(Ltrim([postcode])) = Rtrim(Ltrim(@DestinationpostalCode)) AND [userid] = @DestinationuserId) 
BEGIN 
SELECT @DestinationAddID = addressid FROM   [tbladdress] WHERE  Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@DestinationcompanyName)) 
AND Rtrim(Ltrim([firstname])) = Rtrim(Ltrim(@DestinationfirstName)) 
AND Rtrim(Ltrim([lastname])) = Rtrim( Ltrim(@DestinationlastName)) 
AND Rtrim(Ltrim([address1])) = Rtrim( Ltrim(@Destinationaddress1)) 
AND Rtrim(Ltrim([address2])) = Rtrim( Ltrim(@Destinationaddress2)) 
AND Rtrim(Ltrim([companyname])) = Rtrim( Ltrim(@DestinationcompanyName)) 
AND Rtrim(Ltrim([suburb])) = Rtrim(Ltrim(@Destinationsuburb)) 
AND Rtrim(Ltrim([stateid])) = Rtrim(Ltrim(@stateIdbyCode)) 
AND Rtrim(Ltrim([phone])) = Rtrim(Ltrim(@Destinationphone)) 
AND Rtrim(Ltrim([postcode])) = Rtrim(Ltrim(@DestinationpostalCode)) AND [userid] = @DestinationuserId 
END 
ELSE 
BEGIN 
INSERT INTO [dbo].[tbladdress] 
				([userid], 
				[firstname], 
				[lastname], 
				[companyname], 
				[email], 
				[address1], 
				[address2], 
				[suburb], 
				[stateid], 
				[postcode], 
				[phone], 
				[mobile], 
				[isregisteraddress], 
				[createddatetime], 
				[createdby], 
				[isbusiness], 
				[issubscribe], 
				[statename]) 
VALUES      ( @UserID, 
				@DestinationfirstName, 
				@DestinationlastName, 
				@DestinationcompanyName, 
				@Destinationemail, 
				@Destinationaddress1, 
				@Destinationaddress2, 
				@Destinationsuburb, 
				@stateIdbyCode, 
				@DestinationpostalCode, 
				@Destinationphone, 
				@Destinationmobile, 
				@DestinationisRegisterAddress, 
				Getdate(), 
				@DestinationcreatedBy, 
				@DestinationisBusiness, 
				@DestinationIsSubscribe, 
				@DestinationstateId) 

SET @DestinationAddID = Scope_identity() 
END 

--select @DestinationAddID 
------------------------------------------------- 
--SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  [statecode] = @PickupstateId 
SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  lower([StateName]) = lower(@PickupstateId) 
DECLARE @PickupAddID INT 

IF EXISTS(SELECT * FROM   [tbladdress] WHERE  Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@PickupcompanyName)) 
AND Rtrim(Ltrim([firstname])) = Rtrim(Ltrim(@PickupfirstName)) 
AND Rtrim(Ltrim([lastname])) = Rtrim(Ltrim(@PickuplastName)) 
AND Rtrim(Ltrim([address1])) = Rtrim(Ltrim(@Pickupaddress1)) 
AND Rtrim(Ltrim([address2])) = Rtrim(Ltrim(@Pickupaddress2)) 
AND Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@PickupcompanyName)) 
AND Rtrim(Ltrim([suburb])) = Rtrim(Ltrim(@Pickupsuburb)) 
AND Rtrim(Ltrim([stateid])) = Rtrim(Ltrim(@stateIdbyCode)) 
AND Rtrim(Ltrim([phone])) = Rtrim(Ltrim(@Pickupphone)) 
AND Rtrim(Ltrim([postcode])) = Rtrim(Ltrim(@PickuppostalCode)) AND [userid] = @PickupuserId) 
BEGIN 
SELECT @PickupAddID = addressid FROM   [tbladdress] WHERE  Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@PickupcompanyName) ) 
AND Rtrim(Ltrim([firstname])) = Rtrim(Ltrim(@PickupfirstName)) 
AND Rtrim(Ltrim([lastname])) = Rtrim(Ltrim(@PickuplastName)) 
AND Rtrim(Ltrim([address1])) = Rtrim(Ltrim(@Pickupaddress1)) 
AND Rtrim(Ltrim([address2])) = Rtrim(Ltrim(@Pickupaddress2)) 
AND Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@PickupcompanyName)) 
AND Rtrim(Ltrim([suburb])) = Rtrim(Ltrim(@Pickupsuburb)) 
AND Rtrim(Ltrim([stateid])) = Rtrim(Ltrim(@stateIdbyCode)) 
AND Rtrim(Ltrim([phone])) = Rtrim(Ltrim(@Pickupphone)) 
AND Rtrim(Ltrim([postcode])) = Rtrim(Ltrim(@PickuppostalCode) ) AND [userid] = @PickupuserId 
END 
ELSE 
BEGIN 
INSERT INTO [dbo].[tbladdress] 
				([userid], 
				[firstname], 
				[lastname], 
				[companyname], 
				[email], 
				[address1], 
				[address2], 
				[suburb], 
				[stateid], 
				[postcode], 
				[phone], 
				[mobile], 
				[isregisteraddress], 
				[createddatetime], 
				[createdby], 
				[isbusiness], 
				[issubscribe], 
				[statename]) 
VALUES      ( @UserID, 
				@PickupfirstName, 
				@PickuplastName, 
				@PickupcompanyName, 
				@Pickupemail, 
				@Pickupaddress1, 
				@Pickupaddress2, 
				@Pickupsuburb, 
				@stateIdbyCode, 
				@PickuppostalCode, 
				@Pickupphone, 
				@Pickupmobile, 
				@PickupisRegisterAddress, 
				Getdate(), 
				@PickupcreatedBy, 
				@PickupisBusiness, 
				@PickupIsSubscribe, 
				@PickupstateId) 

SET @PickupAddID = Scope_identity() 
END 

--select @PickupAddID 
---------------------------------------------------------------------- 
DECLARE @ContactAddID INT 

IF ( @IsSameAsDestination = 1 ) 
	SET @ContactAddID = @DestinationAddID 
ELSE 
BEGIN 
IF ( @IsSameAsPickup = 1 ) 
	SET @ContactAddID = @PickupAddID 
ELSE 
BEGIN 
--SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  [statecode] = @ContactstateId 
 SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  lower([StateName]) = lower(@ContactstateId) 
IF EXISTS(SELECT * FROM   [tbladdress] WHERE  Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@ContactcompanyName)) 
AND Rtrim(Ltrim([firstname])) = Rtrim(Ltrim(@ContactfirstName)) 
AND Rtrim(Ltrim([lastname])) = Rtrim( Ltrim(@ContactlastName)) 
AND Rtrim(Ltrim([address1])) = Rtrim( Ltrim(@Contactaddress1)) 
AND Rtrim(Ltrim([address2])) = Rtrim( Ltrim(@Contactaddress2)) 
AND Rtrim(Ltrim([companyname])) = Rtrim(Ltrim(@ContactcompanyName) ) 
AND Rtrim(Ltrim([suburb])) = Rtrim(Ltrim(@Contactsuburb)) 
AND Rtrim(Ltrim([stateid])) = Rtrim(Ltrim(@stateIdbyCode) ) 
AND Rtrim(Ltrim([phone])) = Rtrim( Ltrim(@Contactphone)) 
AND Rtrim(Ltrim([postcode])) = Rtrim(Ltrim(@ContactpostalCode)) AND [userid] = @ContactuserId) 
BEGIN 
SELECT @ContactAddID = addressid FROM   [tbladdress] WHERE  Rtrim(Ltrim([companyname])) = Rtrim( Ltrim(@ContactcompanyName)) 
AND Rtrim(Ltrim([firstname])) = Rtrim( Ltrim(@ContactfirstName)) 
AND Rtrim(Ltrim([lastname])) = Rtrim(Ltrim(@ContactlastName)) 
AND Rtrim(Ltrim([address1])) = Rtrim(Ltrim(@Contactaddress1)) 
AND Rtrim(Ltrim([address2])) = Rtrim(Ltrim(@Contactaddress2)) 
AND Rtrim(Ltrim([companyname])) =Rtrim(Ltrim(@ContactcompanyName)) 
AND Rtrim(Ltrim([suburb])) = Rtrim( Ltrim(@Contactsuburb)) 
AND Rtrim(Ltrim([stateid])) = Rtrim(Ltrim(@stateIdbyCode) ) 
AND Rtrim(Ltrim([phone])) = Rtrim(Ltrim(@Contactphone)) 
AND Rtrim(Ltrim([postcode])) = Rtrim( Ltrim(@ContactpostalCode)) AND [userid] = @ContactuserId 
END 
ELSE 
BEGIN 
INSERT INTO [dbo].[tbladdress] 
				([userid], 
				[firstname], 
				[lastname], 
				[companyname], 
				[email], 
				[address1], 
				[address2], 
				[suburb], 
				[stateid], 
				[postcode], 
				[phone], 
				[mobile], 
				[isregisteraddress], 
				[createddatetime], 
				[createdby], 
				[isbusiness], 
				[issubscribe], 
				[statename]) 
VALUES      ( @UserID, 
				@ContactfirstName, 
				@ContactlastName, 
				@ContactcompanyName, 
				@Contactemail, 
				@Contactaddress1, 
				@Contactaddress2, 
				@Contactsuburb, 
				@stateIdbyCode, 
				@ContactpostalCode, 
				@Contactphone, 
				@Contactmobile, 
				@ContactisRegisterAddress, 
				Getdate(), 
				@ContactcreatedBy, 
				@ContactisBusiness, 
				@ContactIsSubscribe, 
				@ContactstateId) 

SET @ContactAddID = Scope_identity() 
END 
END 
END; 

--select @ContactAddID 
------------------------------------------------- 
DECLARE @ConsignmentIDret INT 

IF ( Isnull(@SortCode, '') = '' ) 
BEGIN 
	SELECT TOP 1 @SortCode = sortcode FROM   locationmaster WHERE  postcode = @DestinationpostalCode 
END 

INSERT INTO [dbo].tblconsignmentstaging 
				([userid], 
				[isreguserconsignment], 
				[pickupid], 
				[destinationid], 
				[contactid], 
				[totalweight], 
				[totalvolume], 
				[noofitems], 
				[specialinstruction], 
				[customerrefno], 
				[pickupdate], 
				[preferpickuptime], 
				[closingtime], 
				[dangerousgoods], 
				[terms], 
				[ratecardid], 
				[createddatetime], 
				[createdby], 
				[serviceid], 
				[isprocessed], 
				[isdocument], 
				issignaturereq, 
				natureofgoods, 
				sortcode, 
				eta, 
				isatl, 
				calculatedtotal, 
				calculatedgst, 
				clientcode) 
VALUES      ( @UserID, 
				@IsRegUserConsignment, 
				@PickupAddID, 
				@DestinationAddID, 
				@ContactAddID, 
				@TotalWeight, 
				@TotalVolume, 
				@NoOfItems, 
				@SpecialInstruction, 
				@CustomerRefNo, 
				@ConsignmentPreferPickupDate, 
				@ConsignmentPreferPickupTime, 
				@ClosingTime, 
				@DangerousGoods, 
				@Terms, 
				@RateCardID, 
				Getdate(), 
				@CreatedBy, 
				@ServiceID, 
				0, 
				@IsDocument, 
				@IsSignatureReq, 
				@NatureOfGoods, 
				@SortCode, 
				@ETA, 
				@IsATL, 
				@GrossTotal, 
				@GST, 
				@ClientCode ) 

SET @ConsignmentIDret = Scope_identity() 

SELECT @ConsignmentIDret 

COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SPCPPL_CreateShipmentStagingTran', 2)
		end
		END CATCH

END 
GO
