SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_DeleteAddressById]

@AddressId int=null,
@userId int=null,
@UpdatedBy int =null
AS
BEGIN

if(@AddressId is null)
Begin
 UPDATE [dbo].[tblAddress] SET 
			
				 [IsDeleted]=1 ,
				 [UpdatedDateTime]=GETDATE(),
				 [UpdatedBy]=@UpdatedBy
		Where UserID=@UserID and IsRegisterAddress=1 
end
else
Begin
 UPDATE [dbo].[tblAddress] SET 
			
				 [IsDeleted]=1 ,
				 [UpdatedDateTime]=GETDATE(),
				 [UpdatedBy]=@UpdatedBy
		Where AddressID=@AddressId 
End


END
GO
