SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_DeleteDeliveryChoices]
@DtIds [DtDeliveryChoiceId] readonly 

As Begin 
Begin Try

Delete From [vw_DeliveryChoicesBeat] Where DeliveryChoiceId in (select [strId] COLLATE Latin1_General_CI_AS from @DtIds )

Delete from [dbo].[vw_DeliveryChoices] where [DeliveryChoiceID]  in (select [strId] COLLATE Latin1_General_CI_AS from @DtIds )
			
select [strId] from @DtIds

END TRY
		BEGIN CATCH
		begin
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_DeleteDeliveryChoices]', 2)
		end
		END CATCH

END 
GO
