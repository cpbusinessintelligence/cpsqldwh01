SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_DeleteDomesticBulkCreateShipment_FORAPI] 
@DtConsignmentIdList [DtConsignmentInfoForAPIForDelete] readonly,
@DtConsignmentStagingIdList [DtConsignmentInfoForAPIForDelete] readonly

As Begin

Begin Try
BEGIN TRAN 

update [tblConsignmentStaging] set consignmentid = null, isprocessed = 0 where ConsignmentStagingId in (Select ConsignmentStagingID from @DtConsignmentStagingIdList)

Delete from [tblitemlabel] where [consignmentid] in (Select ConsignmentID from @DtConsignmentIdList)

Delete from [tblconsignment] where [consignmentcode] in (Select consignmentcode from @DtConsignmentIdList)


COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_DeleteDomesticBulkCreateShipment_FORAPI]', 2)
		end
		END CATCH

END 
GO
