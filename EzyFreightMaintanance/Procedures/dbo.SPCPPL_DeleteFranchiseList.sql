SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_DeleteFranchiseList]

           @FranchiseID int =null,
      
           @UpdatedBy int =null


AS
BEGIN
 
			 UPDATE [dbo].[tblFranchiseList] SET 
 
					 [IsArchive]= 1,
					 [UpdatedDateTime]=GETDATE(),
					 [UpdatedBy]=@UpdatedBy
			Where FranchiseID=@FranchiseID
 
END
 
GO
