SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_DeleteOperationDocument]
@OperationDocId int,
@UpdatedBy int		   

As Begin
Begin Try
BEGIN TRAN 

update CPPLWeb_8_3_Admin..tblOperationDocuments set 
[OperationDocIsActive] = 0  ,
[UpdatedBy] = @UpdatedBy,
[UpdatedDateTime] = Getdate()
Where OperationDocId = @OperationDocId


COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SPCPPL_DeleteOperationDocument', 2)
		end
		END CATCH

END 
GO
