SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_DeleteProntoInvoiceFullDetailById]
           @ReceiptNo nvarchar(100) =null,
		   @ReceiptId int =null
AS
BEGIN
begin tran
BEGIN TRY
	
	delete FROM [dbo].[tblProntoInvoice] where ReceiptNo = @ReceiptNo
	delete FROM [dbo].[tblProntoReceipt] where ReceiptId = @ReceiptId and ReceiptNo = @ReceiptNo

commit tran          
END TRY
BEGIN CATCH
begin
rollback tran
INSERT INTO [dbo].[tblErrorLog]
		   ([Error]
			,[FunctionInfo]
			,[ClientId])
VALUES
            (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
		    ,'SPCPPL_DeleteProntoInvoiceFullDetailById', 2)

end
END CATCH  
End
GO
