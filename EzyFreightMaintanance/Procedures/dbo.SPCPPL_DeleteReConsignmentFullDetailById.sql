SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_DeleteReConsignmentFullDetailById]
      
           @ReConsignmentID int =null,
		   @salesOrderId int =null
           
AS
BEGIN
declare @ConsignmentCode varchar(50), @PickupId int, @CurrentDeliveryAddress int, @NewDeliveryAddress int
select  @ConsignmentCode = ConsignmentCode, @PickupId = PickupAddressID ,@CurrentDeliveryAddress = CurrentDeliveryAddressID ,@NewDeliveryAddress = NewDeliveryAddressID
from [tblRedirectedConsignment] where ReConsignmentID = @ReConsignmentID

	delete FROM [dbo].[TblAddress] where AddressID = @PickupId
	delete FROM [dbo].[TblAddress] where AddressID = @CurrentDeliveryAddress
	delete FROM [dbo].[TblAddress] where AddressID = @NewDeliveryAddress

	delete FROM [dbo].[tblRedirectedItemLabel] where ReConsignmentID = @ReConsignmentID
    
 
declare @InvoiceNo nvarchar(50)
            
select top 1 @SalesOrderID=SalesOrderID,@InvoiceNo=InvoiceNo from dbo.tblSalesOrder where  ConsignmentCode = @ConsignmentCode and SalesOrderId = @SalesOrderID
                 
	

	delete from dbo.tblSalesOrderDetail   where  SalesOrderID = @SalesOrderID
	        
    delete from dbo.tblInvoice where  InvoiceNumber = @InvoiceNo

	delete from dbo.tblSalesOrder  where  ConsignmentCode = @ConsignmentCode and SalesOrderID = @SalesOrderID


--declare @SalesOrderID1 int
--declare @InvoiceNo1 nvarchar(50)
--select   @SalesOrderID1=SalesOrderID,@InvoiceNo1=InvoiceNo from dbo.tblSalesOrder where  ConsignmentCode = @ConsignmentCode and  SalesOrderID <> @SalesOrderID
  
--	delete from dbo.tblSalesOrder    where  ConsignmentCode = @ConsignmentCode and  SalesOrderID <> @SalesOrderID
         
--	delete from dbo.tblSalesOrderDetail   where  SalesOrderID = @SalesOrderID1            
  
--    delete from dbo.tblInvoice    where  InvoiceNumber = @InvoiceNo1
             
	delete  FROM [tblRedirectedConsignment]    where ReConsignmentID = @ReConsignmentID
            
END
GO
