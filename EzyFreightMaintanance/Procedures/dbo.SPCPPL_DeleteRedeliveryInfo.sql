SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_DeleteRedeliveryInfo]
           @dtReDeliveryIds dtReDeliveryIds readonly
         
AS
BEGIN
BEGIN TRY
Begin Tran
Declare @ConsignmentCode varchar(50), @SalesOrderID int, @InvoiceNo nvarchar(50)

SELECT @ConsignmentCode = ConsignmentCode from [tblRedelivery] WHERE [RedeliveryID] in (Select [StrRedeliveryID] from @dtReDeliveryIds)

SELECT TOP 1 @SalesOrderID = SalesOrderID, @InvoiceNo = InvoiceNo  FROM dbo.tblSalesOrder WITH (NOLOCK)  WHERE ConsignmentCode = @ConsignmentCode order by 1 desc

Delete FROM [dbo].[tblRedeliveryItemLabel] WHERE RedeliveryConsignment in (Select [StrRedeliveryID] from @dtReDeliveryIds)

Delete from dbo.tblInvoice WHERE InvoiceNumber = @InvoiceNo 

Delete from dbo.tblSalesOrderDetail WHERE SalesOrderID = @SalesOrderID

Delete from dbo.tblSalesOrder WHERE ConsignmentCode = @ConsignmentCode and SalesOrderID = @SalesOrderID

select * from dtReDeliveryIds
Commit
END TRY

BEGIN CATCH

begin

    rollback tran
	INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
	VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
	,'[SPCPPL_DeleteRedeliveryInfo]', 2)

end

END CATCH

				
End

GO
