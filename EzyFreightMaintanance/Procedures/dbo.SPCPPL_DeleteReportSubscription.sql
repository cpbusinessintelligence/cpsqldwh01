SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_DeleteReportSubscription]
@SerialNo int

As
Begin

Delete From [dbo].[Subscriptions] where [serialnumber] = @SerialNo

end
GO
