SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_DomesticBulkCreateShipment_FORAPI] 
@ConsignmentStagingID nvarchar(100) = null,
@ConsignmentCode VARCHAR(40)= NULL, 
@DtItemDetail DtItemDetail readonly,
@CreatedBy INT= NULL 

As Begin

Begin Try
BEGIN TRAN 

------------------Pickup Address Detail-------------------------------------
Declare @ConsignmentIDRet nvarchar(30) = null,@GeneratedConsignmentCode nvarchar(30) = null
ConsignmentCodeGeneration:
Set @GeneratedConsignmentCode = @ConsignmentCode + [dbo].GenerateConsignmentCode(@ConsignmentCode)

if exists(select * from tblconsignment where [ConsignmentCode] = @GeneratedConsignmentCode )
begin
Goto ConsignmentCodeGeneration;
end

INSERT INTO [dbo].[tblconsignment] 
                  ([consignmentcode], [userid], [isreguserconsignment], [pickupid], [destinationid], [contactid], [totalweight], [totalvolume], 
				  [noofitems], [specialinstruction], [customerrefno], [consignmentpreferpickupdate], [consignmentpreferpickuptime], [closingtime], 
				  [dangerousgoods], [terms], [ratecardid], [consignmentstatus],[createddatetime], [createdby], [issignaturereq],[isatl], [sortcode], eta , 
				  [isprocessed], [isaccountcustomer], calculatedtotal, clientcode) 

		Select @GeneratedConsignmentCode,[UserID],[IsRegUserConsignment],[PickupID],[DestinationID],[ContactID],[TotalWeight],[TotalVolume],
				[NoOfItems],[SpecialInstruction],[CustomerRefNo],[PickupDate],[PreferPickupTime],[ClosingTime],
				[DangerousGoods],[Terms],[RateCardID],1,Getdate(),@CreatedBy,[IsSignatureReq],[IsATl],[SortCode],[ETA],
				1,[IsAccountCustomer],[CalculatedTotal],[ClientCode] 
		from tblconsignmentstaging where ConsignmentstagingID = @ConsignmentStagingID

SET @ConsignmentIDRet = Scope_identity()

----------------------------Item Label Entry----------------------------------------------

INSERT INTO [dbo].[tblitemlabel] 
                  ([consignmentid], [labelnumber], [length], [width], [height], [physicalweight], [quantity], [cubicweight],
				  [createddatetime], [createdby] ) 
      SELECT @ConsignmentIDret, @GeneratedConsignmentCode + [LabelNumber] ,
			CASE WHEN Isnumeric([length]) = 1 THEN Cast([length] AS DECIMAL(10, 2)) ELSE NULL END, 
			CASE WHEN Isnumeric([width]) = 1 THEN Cast([width] AS DECIMAL(10, 2)) ELSE NULL END, 
			CASE WHEN Isnumeric([height]) = 1 THEN Cast([height] AS DECIMAL(10, 2)) ELSE NULL END, 
			CASE WHEN Isnumeric([PhysicalWeight]) = 1 THEN Cast([PhysicalWeight] AS DECIMAL(10, 4)) ELSE NULL END, 
			CASE WHEN Isnumeric([Quantity]) = 1 THEN Cast([Quantity] AS DECIMAL(10, 2)) ELSE NULL END, 
			CASE WHEN Isnumeric([CubicWeight]) = 1 THEN Cast([CubicWeight] AS DECIMAL(10, 4)) ELSE NULL END, 
			Getdate(),@CreatedBy
	  From @DtItemDetail

----------------------------Update Staging----------------------------------------------

UPDATE dbo.tblconsignmentstaging 
      SET    [isprocessed] = 1, [updateddatettime] = Getdate(), [updatedby] = @CreatedBy, [consignmentid] = @ConsignmentIDRet, 
             isaccountcustomer = 1 
      WHERE  consignmentstagingid = @ConsignmentStagingID 
-----------------------------------------------------------------------------------

SELECT @ConsignmentIDRet as ConsignmentID, @GeneratedConsignmentCode as ConsignmentCode

COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_DomesticBulkCreateShipment_FORAPI]', 2)
		end

		SELECT 'ERROR' AS Error, @GeneratedConsignmentCode as ConsignmentCode, cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() AS ErrorDescription

		END CATCH

END 
GO
