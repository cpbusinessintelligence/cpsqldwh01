SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_EditConsignment]
     @ConsignmentID int=null,
      @TotalWeight decimal(10,2)=null,
           @TotalMeasureWeight decimal(10,2)=null,
           @TotalVolume decimal(10,2)=null,
           @TotalMeasureVolume decimal(10,2)=null,
           @NoOfItems int=null,
           @SpecialInstruction varchar(500)=null,
           @ConsignmentPreferPickupDate date=null,
           @ConsignmentPreferPickupTime varchar(20)=null,
           @ClosingTime varchar(10)=null,
           @LastActivity varchar(200)=null,
           @LastActiivityDateTime datetime=null,
           @UpdatedDateTTime datetime=null,
           @UpdatedBy int=null
           
AS
BEGIN
UPDATE [dbo].[tblConsignment]
   SET 
      [TotalWeight] = case when  @TotalWeight is null then [TotalWeight] else @TotalWeight end
      ,[TotalMeasureWeight] =case when   @TotalMeasureWeight is null then [TotalMeasureWeight] else @TotalMeasureWeight end
      ,[TotalVolume] =case when   @TotalVolume is null then [TotalVolume] else @TotalVolume end
      ,[TotalMeasureVolume] =case when   @TotalMeasureVolume is null then [TotalMeasureVolume] else @TotalMeasureVolume end
      ,[NoOfItems] =case when   @NoOfItems is null then [NoOfItems] else @NoOfItems end
      ,[SpecialInstruction] =case when   @SpecialInstruction is null then [SpecialInstruction] else @SpecialInstruction end
      ,[ConsignmentPreferPickupDate] =case when  @ConsignmentPreferPickupDate is null then [ConsignmentPreferPickupDate] else @ConsignmentPreferPickupDate end
      ,[ConsignmentPreferPickupTime] =case when   @ConsignmentPreferPickupTime is null then [ConsignmentPreferPickupTime] else @ConsignmentPreferPickupTime end
      ,[ClosingTime] =case when   @ClosingTime is null then [ClosingTime] else @ClosingTime end
      ,[LastActivity] =case when   @LastActivity is null then [LastActivity] else @LastActivity end
      ,[LastActiivityDateTime] =case when   @LastActiivityDateTime is null then [LastActiivityDateTime] else @LastActiivityDateTime end
      ,[UpdatedDateTTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
 WHERE ConsignmentID = @ConsignmentID
            
            
END
GO
