SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SPCPPL_EditItemLabel]
     @ItemLabelID int=null,
      @Quantity int=null,
           @MeasureLength decimal(10,2)=null,
           @MeasureWidth decimal(10,2)=null,
           @MeasureHeight decimal(10,2)=null,
           @MeasureWeight decimal(10,2)=null,
           @UpdatedBy int=null
           
AS
BEGIN
UPDATE [dbo].[tblItemLabel]
   SET 
      [Quantity] = case when  @Quantity is null then [Quantity] else @Quantity end
      ,[MeasureLength] =case when   @MeasureLength is null then [MeasureLength] else @MeasureLength end
      ,[MeasureWidth] =case when   @MeasureWidth is null then [MeasureWidth] else @MeasureWidth end
      ,[MeasureHeight] =case when   @MeasureHeight is null then [MeasureHeight] else @MeasureHeight end
      ,[MeasureWeight] =case when   @MeasureWeight is null then [MeasureWeight] else @MeasureWeight end
      ,[UpdatedDateTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
 WHERE ItemLabelID = @ItemLabelID
            
            
END
GO
