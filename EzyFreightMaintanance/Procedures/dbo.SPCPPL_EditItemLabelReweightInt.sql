SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_EditItemLabelReweightInt]
     @ItemLabelID int=null,
      @Quantity int=null,
           @MeasureLength decimal(10,2)=null,
           @MeasureWidth decimal(10,2)=null,
           @MeasureHeight decimal(10,2)=null,
           @MeasureWeight decimal(10,2)=null,
           @UpdatedBy int=null
           
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
begin tran
BEGIN TRY
UPDATE [dbo].[tblItemLabel] 
   SET 
      [Quantity] = case when  @Quantity is null then [Quantity] else @Quantity end
      ,[MeasureLength] =case when   @MeasureLength is null then [MeasureLength] else @MeasureLength end
      ,[MeasureWidth] =case when   @MeasureWidth is null then [MeasureWidth] else @MeasureWidth end
      ,[MeasureHeight] =case when   @MeasureHeight is null then [MeasureHeight] else @MeasureHeight end
      ,[MeasureWeight] =case when   @MeasureWeight is null then [MeasureWeight] else @MeasureWeight end
      ,[MeasureCubicWeight] = (@MeasureLength/100)*(@MeasureWidth/100)*(@MeasureHeight/100)*200
      ,[UpdatedDateTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
 WHERE ItemLabelID = @ItemLabelID
            
declare    @ConsignmentID int
			select @ConsignmentID =ConsignmentID from tblItemLabel with (nolock) where ItemLabelID = @ItemLabelID
            
declare    @SumMeasureCubicWeight decimal(10,2),@SumMeasureWeight decimal(10,2)

            SELECT   @SumMeasureCubicWeight= SUM( MeasureCubicWeight),@SumMeasureWeight=SUM(MeasureWeight)
			FROM         dbo.tblItemLabel  with (nolock) 
			WHERE     (ConsignmentID = @ConsignmentID)


update dbo.tblConsignment
set 
TotalMeasureWeight = @SumMeasureWeight,
TotalMeasureVolume = @SumMeasureCubicWeight
WHERE     (ConsignmentID = @ConsignmentID)


SELECT     dbo.tblConsignment.TotalMeasureWeight, dbo.tblConsignment.TotalMeasureVolume, dbo.tblAddress.PostCode AS FromPostCode, 
                      dbo.tblAddress.Suburb AS FromSuburb, tblAddress_1.Suburb AS ToSuburb, tblAddress_1.PostCode AS ToPostCode, 

dbo.tblConsignment.NoOfItems, 
                   dbo.tblInvoice.MerchantReferenceCode,       dbo.tblConsignment.ConsignmentID,   dbo.tblConsignment.RateCardID, 

				   Case When IsAccountCustomer = 1 then isnull(CalculatedTotal,0) else isnull(dbo.tblSalesOrder.GrossTotal,0) end as 

GrossTotal 
				   
				   ,@MeasureWeight as MeasureWeight, (@MeasureLength/100)*(@MeasureWidth/100)*(@MeasureHeight/100)*250 as MeasureCubicWeight
                      , dbo.tblAddress.Country AS FromCountry, tblAddress_1.Country AS ToCountry
                 , dbo.tblConsignment.IsSignatureReq, dbo.tblConsignment.IsDocument, dbo.tblInvoice.PaymentRefNo
				 , Case When IsAccountCustomer = 1 then isnull(InsuranceAmount,0) else isnull(sd.Total,0) end as InsuranceAmt
				 , Case When IsAccountCustomer = 1 then (isnull(CalculatedTotal,0)) else (isnull(dbo.tblSalesOrder.GrossTotal,0)-isnull

(sd.Total,0)) end as TotalWithoutINSAmt
FROM         dbo.tblConsignment  with (nolock) INNER JOIN
                      dbo.tblAddress  with (nolock) ON dbo.tblConsignment.PickupID = dbo.tblAddress.AddressID 
					  INNER JOIN dbo.tblAddress AS tblAddress_1  with (nolock) ON dbo.tblConsignment.DestinationID = 

tblAddress_1.AddressID 
					  Left JOIN dbo.tblSalesOrder  with (nolock) ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo  
					  Left JOIN tblSalesOrderDetail sd on sd.SalesorderID = dbo.tblSalesOrder.SalesorderID and [Description] = 

'Insurance'
					  left JOIN dbo.tblInvoice  with (nolock) ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
                      where (dbo.tblConsignment.ConsignmentID = @ConsignmentID)
  
   
commit tran          
            
END TRY
	BEGIN CATCH
		begin
		rollback tran
		INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
					,[FunctionInfo]
					,[ClientId])
		VALUES
					(cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
					,'SPCPPL_EditItemLabelReweightInt', 2)

		end

	END CATCH  
END 
GO
