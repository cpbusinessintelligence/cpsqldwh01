SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_EzyFreightDatabaseMaintenance_New]

@StartDate datetime = '2015-05-28'

As 
BEGIN
       BEGIN TRY
              BEGIN TRAN 

              --EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

              --------------------REMOVE CONSTRAINT TO SELECTED TABLE ---------------------

               ALTER TABLE tblAddress NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblConsignmentStaging NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblConsignment NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblItemLabel NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblSalesOrder NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblSalesOrderDetail NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblInvoice NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblRedirectedConsignment NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblRedirectedItemLabel NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblRecharge NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblDHLBarCodeImage NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblItemLabelImage NOCHECK CONSTRAINT ALL
			   ALTER TABLE tblRefund NOCHECK CONSTRAINT ALL

				  ------------COPY ADDRESS-----------------------

				  INSERT INTO tblAddress_Archive 
				  select * from tblAddress with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and IsRegisterAddress <> 1 and isnull([UserID],'-1') = -1;

				   ------------COPY ConsignmentStaging-----------------------


				  INSERT INTO tblConsignmentStaging_Archive 
				  select * from tblConsignmentStaging with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				   --------------COPY Consignment-----------------------

                  INSERT INTO tblConsignment_Archive 
                  select * from tblConsignment with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				  ------------COPY tblItemLabel-----------------------


				  INSERT INTO tblItemLabel_Archive 
				  select * from tblItemLabel  with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate ;


				  ------------COPY tblSalesOrder-----------------------

                           --INSERT INTO tblSalesOrder_Archive 
                           --select * from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(SalesOrderType,'') <> 'REDELIVERY'
                           --and isnull(ConsignmentCode,'') not in (select ConsignmentCode from tblRedirectedConsignment);

                  INSERT INTO tblSalesOrder_Archive 
                  select * from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				  ------------COPY tblSalesOrderDetail-----------------------

                           --INSERT INTO tblSalesOrderDetails_Archive
                           --select * from tblSalesOrderDetail SD with(NoLock) where  convert(Date, CreatedDateTime, 103) <= @StartDate 
                           --and SD.SalesOrderID in (select SalesOrderID from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(SalesOrderType,'') <> 'REDELIVERY'
                           --and isnull(ConsignmentCode,'') not in (select ConsignmentCode from tblRedirectedConsignment));

                 INSERT INTO tblSalesOrderDetails_Archive
                 select * from tblSalesOrderDetail SD with(NoLock) where  convert(Date, CreatedDateTime, 103) <= @StartDate 


				 ------------COPY tblInvoice-----------------------

				 --INSERT INTO tblInvoice_Archive
				 --select * from tblInvoice with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 
				 --and InvoiceNumber in (select InvoiceNo from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(SalesOrderType,'') <> 'REDELIVERY'
				 --and isnull(ConsignmentCode,'') not in (select ConsignmentCode from tblRedirectedConsignment));

				 INSERT INTO tblInvoice_Archive
				 select * from tblInvoice with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate           


				 -----------------REDIRECTION CONSIGNMENT---------------------
              

				 INSERT INTO tblRedirectedConsignment_Archive
				 select * from tblRedirectedConsignment with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate      


				 -----------------REDIRECTION ITEM LABEL---------------------
              

				 INSERT INTO tblRedirectedItemLabel_Archive
				 select * from tblRedirectedItemLabel with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate   

				 ---------------COPY tblRecharge-----------------------

                 INSERT INTO tblRecharge_Archive
                 select * from tblRecharge with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				 -----------COPY DHL Barcode Record In Archive-----------------------

				 INSERT INTO tblDHLBarCodeImage_Archive
				 select * from tblDHLBarCodeImage Bar with(NoLock) where convert(Date, Bar.CreatedDateTime, 103) <= @StartDate;

				  -----------COPY LabelImage Record In Archive-----------------------
				 INSERT INTO tblItemLabelImage_Archive
				 select * from tblItemLabelImage with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate;

				  --------------Add Table tblRefund Record In Archive-----------------------

                 INSERT INTO tblRefund_Archive
                 select * from tblRefund with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

			     -------------------DELETE ADDRESS DATA------------------------

					  DELETE FROM tblAddress WHERE IsRegisterAddress = 0 AND isnull([UserID],'-1') = -1 AND (IsEDIUser = 0 OR IsEDIUser IS NULL) AND convert(Date, CreatedDateTime, 103) <@StartDate

					  Delete from tblConsignmentStaging where convert(Date, CreatedDateTime, 103) <= @StartDate;

					  Delete  from tblConsignment  where convert(Date, CreatedDateTime, 103) <= @StartDate ;

					  Delete  from tblItemLabel  where convert(Date, CreatedDateTime, 103) <= @StartDate ;

					  Delete from tblSalesOrder where convert(Date, CreatedDateTime, 103) <= @StartDate

					  Delete from tblSalesOrderDetail  where convert(Date, CreatedDateTime, 103) <= @StartDate

					  Delete from tblInvoice where convert(Date, CreatedDateTime, 103) <= @StartDate

					  Delete from tblRedirectedConsignment where convert(Date, CreatedDateTime, 103) <= @StartDate

					  Delete from tblRedirectedItemLabel where convert(Date, CreatedDateTime, 103) <= @StartDate

					  Delete  from tblRecharge   where convert(Date, CreatedDateTime, 103) <= @StartDate;

					  Delete Bar from tblDHLBarCodeImage Bar where convert(Date, Bar.CreatedDateTime, 103) <= @StartDate

					  Delete from tblItemLabelImage where convert(Date, CreatedDateTime, 103) <= @StartDate 

					  Delete from tblRefund  where convert(Date, CreatedDateTime, 103) <= @StartDate 


				--------------------SET CONSTRAINT TO SELECTED TABLE ---------------------

					ALTER TABLE tblAddress CHECK CONSTRAINT ALL

					ALTER TABLE tblConsignmentStaging CHECK CONSTRAINT ALL

                    ALTER TABLE tblConsignment CHECK CONSTRAINT ALL

                    ALTER TABLE tblItemLabel CHECK CONSTRAINT ALL

					ALTER TABLE tblSalesOrder CHECK CONSTRAINT ALL
					
					ALTER TABLE tblSalesOrderDetail CHECK CONSTRAINT ALL
					
					ALTER TABLE tblInvoice CHECK CONSTRAINT ALL

					ALTER TABLE tblRedirectedConsignment CHECK CONSTRAINT ALL
					
					ALTER TABLE tblRedirectedItemLabel CHECK CONSTRAINT ALL

					ALTER TABLE tblRecharge CHECK CONSTRAINT ALL

					ALTER TABLE tblDHLBarCodeImage CHECK CONSTRAINT ALL     

					ALTER TABLE tblItemLabelImage CHECK CONSTRAINT ALL

					ALTER TABLE tblRefund CHECK CONSTRAINT ALL

                 --EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"


              select 'OK' as Result

              COMMIT TRAN 

       END TRY
              BEGIN CATCH
              begin
                     rollback tran
                     INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
                     VALUES
                              (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
                              ,'SPCPPL_EzyFreightDatabaseMaintenance', 2)

                              select cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as Result
              end
              END CATCH

END 
GO
