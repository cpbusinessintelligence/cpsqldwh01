SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetActiveFranchiseListWithoutPaging]
@Branch varchar(20)=null 

AS
BEGIN
 
		 
		SELECT * FROM dbo.tblFranchiseList
		where Branch  = case when @Branch is null then Branch else @Branch end 
		and IsArchive =	  0  
		order by createddatetime desc
				 
 
END
GO
