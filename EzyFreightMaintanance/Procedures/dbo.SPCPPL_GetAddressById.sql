SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetAddressById]
@AddressID int = null
AS
BEGIN
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT     dbo.tblAddress.*, dbo.tblState.StateCode
FROM         dbo.tblAddress LEFT OUTER JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where [AddressID]=@AddressID
END

---[SPCPPL_GetAdminZoneWiseUsers] 41,1,10,'FirstName','ASC',0
--- select * from tblAddress
GO
