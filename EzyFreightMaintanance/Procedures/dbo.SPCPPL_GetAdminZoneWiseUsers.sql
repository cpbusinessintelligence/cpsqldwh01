SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_GetAdminZoneWiseUsers]
@UserId int,
@PageIndex int=1,
@PageSize int=10,
 @SortColumn varchar(50) = null,
 @SortDir varchar(50)=null,
@ParamTotalRec_out int out
AS
BEGIN
--  SELECT Zone Wise User According to AdminUserID for state.
   set @ParamTotalRec_out = (select distinct count(*)  from tblAddress where StateID=
   (select stateID from tblAddress where UserID=@UserId and IsDeleted='false' and IsRegisterAddress='true')and  IsDeleted='false' and IsRegisterAddress='true')
				select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY tblInner.UserID DESC)  RowNum, * 
					FROM 
					(
						SELECT * FROM tblAddress
						where tblAddress.StateID=(select stateID from tblAddress where UserID=@UserId and IsDeleted='false' and IsRegisterAddress='true') 
						and IsDeleted='false' and IsRegisterAddress='true'
					) as tblInner 
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY 
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'DESC' THEN FirstName END  DESC,
	--CASE WHEN @SortColumn = 'TitleString' AND @SortDir = 'DESC' THEN TitleString END DESC,
  --CASE WHEN @SortColumn = 'AuthorString' AND @SortDir = 'ASC'  THEN AuthorString END,
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'ASC'  THEN FirstName END;
END

---[SPCPPL_GetAdminZoneWiseUsers] 41,1,10,'FirstName','ASC',0
--- select * from tblAddress
GO
