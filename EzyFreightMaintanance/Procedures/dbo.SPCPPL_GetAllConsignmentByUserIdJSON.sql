SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create  PROCEDURE [dbo].[SPCPPL_GetAllConsignmentByUserIdJSON]
@UserId int,
@PageIndex int=1,
 @SortColumn varchar(50) = null,
  @SortDir varchar(50)=null,
@PageSize int=10
--@ParamTotalRec_out int out
AS
BEGIN
--  SELECT Zone Wise User According to AdminUserID for state.
(select distinct count(*)  from tblConsignment where  UserID=@UserId)
				select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY tblInner.UserID DESC)  RowNum, * 
					FROM 
					(
						SELECT     consign.ConsignmentID, consign.ConsignmentCode, consign.UserID, consign.PickupID, consign.DestinationID, consign.ContactID, 
                      addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
                      addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, 
                      addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName, 
                      addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate
FROM         dbo.tblConsignment AS consign INNER JOIN
                      dbo.tblAddress AS addPikup ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     (consign.UserID =@UserId)
					) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN PickFirstName END DESC,
 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC'  THEN PickFirstName END;
  
  CASE WHEN  @SortDir = 'DESC' THEN @SortColumn END DESC,
  CASE WHEN @SortDir = 'ASC'  THEN @SortColumn END;


END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
