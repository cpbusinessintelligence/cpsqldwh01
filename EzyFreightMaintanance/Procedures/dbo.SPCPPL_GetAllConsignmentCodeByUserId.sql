SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[SPCPPL_GetAllConsignmentCodeByUserId]
@Username nvarchar(max)
 
AS
BEGIN
 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
						SELECT     consign.ConsignmentCode 
						,dbo.UNIX_TIMESTAMP(consign.CreatedDateTime) DateSubmitted
						,consign.CreatedDateTime DateSubmittednew
FROM         dbo.tblConsignment AS consign  with (nolock) inner join
[CPPLWeb].[dbo].[Users] as Ur with (nolock)  on
consign.UserID = Ur.[UserID]
WHERE     (Ur. Username  =@Username) and consign.IsInternational =1 and consign.IsProcessed =1
			

END
GO
