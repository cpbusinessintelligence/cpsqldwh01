SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetAllState]

AS
BEGIN
SELECT  [StateID]
      ,[StateCode]
      ,[StateName]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
  FROM [dbo].[tblState] order by [StateName]
END
----Select * from tblAddress where userid=28
GO
