SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPCPPL_GetAllUser_WithPaging]
@searchstr nvarchar(150) = null,
@searchtype nvarchar(50)= null,
@PageIndex int=1,
@PageSize int=10

AS
BEGIN
select * from (
SELECT ROW_NUMBER() OVER(ORDER BY UserID desc) as RowNum, * 
from
(
Select distinct Users.UserID,Users.FirstName,Users.LastName,Users.Username,Users.Email,UserPortals.Authorised from Users 
inner join UserPortals on users.userid=UserPortals.userid inner join Userroles on users.userid=Userroles.userid 
where  users.userid not in (select userid from userroles where roleid = 0) and Users.Issuperuser=0 and UserPortals.PortalID = 0
and (
 case when UPPER(@searchtype) != 'USERNAME' then isnull(@searchtype,'') else Users.Username end LIKE '%'+isnull(@searchstr,'')+'%' or
 case when UPPER(@searchtype) != 'EMAIL' then isnull(@searchtype,'') else Users.Email end LIKE '%'+isnull(@searchstr,'')+'%' or
 case when UPPER(@searchtype) != 'FIRSTNAME' then isnull(@searchtype,'') else Users.FirstName end LIKE '%'+isnull(@searchstr,'')+'%' or
 case when UPPER(@searchtype) != 'LASTNAME' then isnull(@searchtype,'') else Users.LastName end LIKE '%'+isnull(@searchstr,'')+'%' 
)
)as tblinner
)as tblOuter 
where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) or (@PageIndex=0 and @Pagesize=0)

order by tblOuter.RowNum



SELECT count(*) 
from
(
Select distinct Users.UserID,Users.FirstName,Users.LastName,Users.Username,Users.Email,UserPortals.Authorised from Users 
inner join UserPortals on users.userid=UserPortals.userid inner join Userroles on users.userid=Userroles.userid 
where  users.userid not in (select userid from userroles where roleid = 0) and Users.Issuperuser=0 and UserPortals.PortalID = 0
and (
 case when UPPER(@searchtype) != 'USERNAME' then isnull(@searchtype,'') else Users.Username end LIKE '%'+isnull(@searchstr,'')+'%' or
 case when UPPER(@searchtype) != 'EMAIL' then isnull(@searchtype,'') else Users.Email end LIKE '%'+isnull(@searchstr,'')+'%' or
 case when UPPER(@searchtype) != 'FIRSTNAME' then isnull(@searchtype,'') else Users.FirstName end LIKE '%'+isnull(@searchstr,'')+'%' or
 case when UPPER(@searchtype) != 'LASTNAME' then isnull(@searchtype,'') else Users.LastName end LIKE '%'+isnull(@searchstr,'')+'%' 
)
)as tblinner




END




GO
