SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_GetBookingAddressDetailJSON]
@PhoneNumber nvarchar(50)=null,
@CompanyName nvarchar(max)=null,
@Branch nvarchar(50)=null

AS
BEGIN

SELECT TOP 1 
    [ContactEmail]
      ,[PickupDate]
      ,[PickupTime]
      ,[OrderCoupons]
      ,[PickupFromCustomer]
      ,[PickupName]
       ,[PickupAddress1]
           ,[PickupAddress2]
      ,[PickupSuburb]
      ,[PickupPhoneNo]
      ,[ContactDetails]
      ,[PickupFrom]
      ,[IsProcessed]
      ,[BookingRefNo]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[Branch]
      ,[PickupEmail]
      ,[PickupPostCode]
     

  FROM [dbo].[tblBooking] where
    case when   ISNULL( [PickupName],'') ='' then '' else [PickupName] +',' end +
          case when  ISNULL( [PickupAddress1],'') ='' then '' else [PickupAddress1] +' ' end +
      case when  ISNULL( [PickupAddress2],'') ='' then '' else [PickupAddress2]  end +
             ISNULL( [PickupSuburb],'')=@CompanyName  and PickupFromCustomer =0

order by [CreatedDateTime] desc
END


GO
