SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_GetBookingAddressJSON]
@PhoneNumber nvarchar(50)=null,
@Branch nvarchar(50)=null

AS
BEGIN

SELECT  distinct
      
     
   case when   ISNULL( [PickupName],'') ='' then '' else [PickupName] +',' end +
      case when  ISNULL( [PickupAddress1],'') ='' then '' else [PickupAddress1] +' ' end +
      case when  ISNULL( [PickupAddress2],'') ='' then '' else [PickupAddress2]  end +
             ISNULL( [PickupSuburb],'') as Addre
  FROM [dbo].[tblBooking] where
  [PhoneNumber]=@PhoneNumber and 
[Branch] = @Branch and PickupFromCustomer =0

group by 
PickupName,[PickupSuburb],[PickupPostCode],[PickupAddress1],[PickupAddress2]
END


GO
