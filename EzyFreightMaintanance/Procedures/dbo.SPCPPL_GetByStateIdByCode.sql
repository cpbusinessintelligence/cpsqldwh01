SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_GetByStateIdByCode]
@StateCode nvarchar(max)
AS
BEGIN
SELECT TOP 1 [StateID]
    
  FROM [dbo].[tblState] where [StateCode] = @StateCode
END 

GO
