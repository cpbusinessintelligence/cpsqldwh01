SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[SPCPPL_GetCompanyDetail_BUP_15_09_2017]
 
@PageIndex int=1,
 @SortColumn varchar(50) = 'CreatedDateTime',
  @SortDir varchar(50)='DESC',
@PageSize int=10 ,

  @AccountNumber varchar(max)=null,
  @Company varchar(max)=null,
  @Branch varchar(max)=null 

AS
BEGIN






select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY 
  --CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN   ConsignmentCode END DESC,
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN    PickFirstName END DESC,
  --CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN  DestFirstName END DESC,
  --CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN   StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN CreatedDateTime END DESC,
  
  --  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN ConsignmentCode END  ,
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN PickFirstName END  ,
  --CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN DestFirstName END  ,
  --CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN CreatedDateTime END  )  RowNum, * 
					FROM 
					(




	SELECT top 100 percent        tblCompany.CompanyID, tblCompany.AccountNumber, tblCompany.CompanyName, tblCompany.ParentAccountNo, tblCompany.ABN, tblCompany.AddressID1, 
                      tblCompany.PostalAddressID, tblCompany.ParentCompanyName, tblCompany.BilingAccountNo, tblCompany.IsProntoExtracted, tblCompany.RateCardCategory, 
                      tblCompany.IntRateCardCategory, tblCompany.SameBillingAddress, tblCompany.BillingFname, tblCompany.BillingLname, tblCompany.BllingEmail, 
                      tblCompany.BillingPhone, tblCompany.BillingAddress, tblCompany.CreditLimit, tblCompany.IsExistingCustomer, tblCompany.Branch, 
                      tblCompany.ExistingAccountNumber, tblCompany.CouponNo, tblCompany.CreatedDateTime, tblCompany.CreatedBy, tblCompany.UpdatedDateTime, 
                      tblCompany.UpdatedBy, 
                    tblAddress.AddressID AS AddrAddressID, tblAddress.UserID AS AddrUserID, tblAddress.FirstName AS AddrFirstName, 
                      tblAddress.LastName AS AddrLastName, tblAddress.CompanyName AS AddrCompanyName, tblAddress.Email AS AddrEmail, tblAddress.Address1 AS AddrAddress1, 
                      tblAddress.Address2 AS AddrAddress2, tblAddress.Suburb AS AddrSuburb, 
                    case when   tblAddress.StateName is null then (select top 1 StateName from dbo.tblState where StateID = tblAddress.StateID) else  tblAddress.StateName end AS AddrStateName, 
                      
                      tblAddress.StateID AS AddrStateID, 
                      tblAddress.PostCode AS AddrPostCode, tblAddress.Phone AS AddrPhone, tblAddress.Mobile AS AddrMobile, tblAddress.Country AS AddrCountry, 
                      tblAddress.CountryCode AS AddrCountryCode, tblAddress.CreatedDateTime AS AddrCreatedDateTime, tblAddress.CreatedBy AS AddrCreatedBy, 
                      tblAddress.UpdatedDateTime AS AddrUpdatedDateTime, tblAddress.UpdatedBy AS AddrUpdatedBy, tblAddress.IsRegisterAddress AS AddrIsRegisterAddress, 
                      tblAddress.IsBusiness AS AddrIsBusiness, tblAddress.IsDeleted AS AddrIsDeleted, tblAddress.IsSubscribe AS AddrIsSubscribe, 
                      tblAddress.IsEDIUser AS AddrIsEDIUser,   tblPostalAddress.AddressID AS PAddrAddressID, tblPostalAddress.UserID AS PAddrUserID, 
                      tblPostalAddress.FirstName AS PAddrFirstName, tblPostalAddress.LastName AS PAddrLastName, tblPostalAddress.CompanyName AS PAddrCompanyName, 
                      tblPostalAddress.Email AS PAddrEmail, tblPostalAddress.Address1 AS PAddrAddress1, tblPostalAddress.Address2 AS PAddrAddress2, 
                      tblPostalAddress.Suburb AS PAddrSuburb,
                   case when       tblPostalAddress.StateName is null then (select top 1 StateName from dbo.tblState where StateID = tblPostalAddress.StateID) else tblPostalAddress.StateName end AS PAddrStateName,
 
                       
                        tblPostalAddress.StateID AS PAddrStateID, 
                      tblPostalAddress.PostCode AS PAddrPostCode, tblPostalAddress.Phone AS PAddrPhone, tblPostalAddress.Mobile AS PAddrMobile, 
                      tblPostalAddress.Country AS PAddrCountry, tblPostalAddress.CountryCode AS PAddrCountryCode, tblPostalAddress.CreatedDateTime AS PAddrCreatedDateTime, 
                      tblPostalAddress.CreatedBy AS PAddrCreatedBy, tblPostalAddress.UpdatedDateTime AS PAddrUpdatedDateTime, tblPostalAddress.UpdatedBy AS PAddrUpdatedBy, 
                      tblPostalAddress.IsRegisterAddress AS PAddrIsRegisterAddress, tblPostalAddress.IsBusiness AS PAddrIsBusiness, tblPostalAddress.IsDeleted AS PAddrIsDeleted, 
                      tblPostalAddress.IsSubscribe AS PAddrIsSubscribe, tblPostalAddress.IsEDIUser AS PAddrIsEDIUser 
                       
                     , (select top 1 UserID  from dbo.tblCompanyUsers where CompanyID=   tblCompany.CompanyID) as UserID
                     , (select top 1   CompanyStatus from dbo.tblCompanyStatusLog where CompanyID=   tblCompany.CompanyID order by CompanyStatusLogID desc) as IsUserDisabled
                      ,tblCompany.IsRegularShipper,
                      tblCompany.ShipperCreatedBy,
                      tblCompany.ShipperCreatedDateTime,
                      tblCompany.ShipperUpdatedBy,
                      tblCompany.ShipperUpdatedDateTime 
FROM         tblCompany INNER JOIN
                      tblAddress ON tblCompany.AddressID1 = tblAddress.AddressID INNER JOIN
                      tblAddress tblPostalAddress ON tblCompany.PostalAddressID = tblPostalAddress.AddressID 
   
WHERE      
 (tblCompany.CompanyID IN
                          (SELECT DISTINCT CompanyID
                            FROM          tblCompanyUsers mh))
                            and (
 CASE WHEN @AccountNumber IS null THEN isnull(@AccountNumber,'') else tblCompany.AccountNumber  END LIKE '%'+isnull(@AccountNumber,'')+'%' 
 --or
 --CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 --CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblCompany.CompanyName  END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.FirstName END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.LastName END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.StateName END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.Suburb END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.PostCode END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.StateID END LIKE '%'+isnull(@Company,'')+'%' 

) 
and
(
 CASE WHEN @Branch IS null THEN isnull(@Branch,'') else tblCompany.Branch  END LIKE '%'+isnull(@Branch,'')+'%' 
)


) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
		
  
  
SELECT    COUNT(*)
 

FROM         tblCompany INNER JOIN
                      tblAddress ON tblCompany.AddressID1 = tblAddress.AddressID INNER JOIN
                      tblAddress tblPostalAddress ON tblCompany.PostalAddressID = tblPostalAddress.AddressID  
WHERE      
(
 CASE WHEN @AccountNumber IS null THEN isnull(@AccountNumber,'') else tblCompany.AccountNumber  END LIKE '%'+isnull(@AccountNumber,'')+'%' 
 --or
 --CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 --CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblCompany.CompanyName  END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.FirstName END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.LastName END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.StateName END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.Suburb END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.PostCode END LIKE '%'+isnull(@Company,'')+'%' or
 CASE WHEN @Company IS null THEN isnull(@Company,'') else tblAddress.StateID END LIKE '%'+isnull(@Company,'')+'%' 

) 
and
(
 CASE WHEN @Branch IS null THEN isnull(@Branch,'') else tblCompany.Branch  END LIKE '%'+isnull(@Branch,'')+'%' 
)


  
END


GO
