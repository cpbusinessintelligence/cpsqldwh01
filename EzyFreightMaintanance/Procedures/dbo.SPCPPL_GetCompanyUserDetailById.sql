SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetCompanyUserDetailById]
      
@UserID int = null
           
      
AS
BEGIN
 declare  @CompanyID int 
 declare  @AddressID1 int 
 declare  @PostalAddressID int 
 
 
 SELECT  @CompanyID=[CompanyID]
  FROM  [dbo].[tblCompanyUsers] where UserID=@UserID
  
  
 SELECT @AddressID1=[AddressID1]
      ,@PostalAddressID=[PostalAddressID]
  FROM  [dbo].[tblCompany] where CompanyID =@CompanyID
  
  
  select * from tbladdress where addressId = @AddressID1
  select * from tbladdress where addressId = @PostalAddressID
  select * from [tblCompany] where CompanyID = @CompanyID
  
  
 
  
 
END
GO
