SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetConsignmentFullDetailByByConsignmentCode] 
@ConsignmentCode NVARCHAR(max) = NULL 
AS 
  BEGIN 
      DECLARE @DestinationID INT, 
              @ContactID     INT, 
              @PicupID       INT, 
              @ConsignmentID INT 

      SELECT TOP 1 @ConsignmentID = consignmentid 
      FROM   dbo.tblconsignment 
      WHERE  consignmentcode = @ConsignmentCode 

      SELECT Isnull(dbo.tblconsignment.isatl, 0) AS IsATl, 
             dbo.tblconsignment.*, 
             dbo.tblconsignment.[country-servicearea-facilitycode] AS 
             Country_ServiceArea_FacilityCode 
      FROM   dbo.tblconsignment WITH (nolock) 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentID 

      SELECT @PicupID = dbo.tblconsignment.pickupid, 
             @DestinationID = dbo.tblconsignment.destinationid, 
             @ContactID = dbo.tblconsignment.contactid 
      FROM   dbo.tblconsignment WITH (nolock) 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress WITH (nolock) 
             LEFT JOIN dbo.tblstate WITH (nolock) 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @PicupID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress WITH (nolock) 
             LEFT JOIN dbo.tblstate WITH (nolock) 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @DestinationID 

      SELECT * 
      FROM   [dbo].[tblitemlabel] WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress WITH (nolock) 
             LEFT JOIN dbo.tblstate WITH (nolock) 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @ContactID 

      SELECT * 
      FROM   tbldhlbarcodeimage WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 

      SELECT TOP 1 * 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 

      DECLARE @SalesOrderID INT, 
              @InvoiceNo    NVARCHAR(50) 

      SELECT TOP 1 @SalesOrderID = salesorderid, 
                   @InvoiceNo = invoiceno 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 

      SELECT * 
      FROM   dbo.tblsalesorderdetail WITH (nolock) 
      WHERE  salesorderid = @SalesOrderID 

      SELECT * 
      FROM   dbo.tblinvoice WITH (nolock) 
      WHERE  invoicenumber = @InvoiceNo 

      SELECT * 
      FROM   dbo.tblcustomdeclaration WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 

      SELECT * 
      FROM   dbo.tblitemlabelimage WITH (nolock) 
      WHERE  itemlabelid IN (SELECT itemlabelid 
                             FROM   [dbo].[tblitemlabel] WITH (nolock) 
                             WHERE  consignmentid = @ConsignmentID) 

      DECLARE @SalesOrderID1 INT 
      DECLARE @InvoiceNo1 NVARCHAR(50) 

      SELECT TOP 1 * 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 
             AND salesorderid <> @SalesOrderID 

      SELECT @SalesOrderID1 = salesorderid, 
             @InvoiceNo1 = invoiceno 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 
             AND salesorderid <> @SalesOrderID 

      SELECT * 
      FROM   dbo.tblsalesorderdetail WITH (nolock) 
      WHERE  salesorderid = @SalesOrderID1 

      SELECT * 
      FROM   dbo.tblinvoice WITH (nolock) 
      WHERE  invoicenumber = @InvoiceNo1 

      SELECT * 
      FROM   dbo.[tblbooking] WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 
  END 
GO
