SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetConsignmentFullDetailById]
@ConsignmentID INT = NULL 
AS 
  BEGIN 
      DECLARE @DestinationID INT, @ContactID INT, @PicupID INT 

      SELECT Isnull(dbo.tblconsignment.isatl, 0) AS IsATl,Isnull(dbo.tblconsignment.IsAccountCustomer, 0) AS IsAccountCustomer, isnull(dbo.tblconsignment.EDIDataProcessed,0) as EDIDataProcessed, isnull(dbo.tblconsignment.ProntoDataExtracted,0) as  ProntoDataExtracted
			, dbo.tblconsignment.*, dbo.tblconsignment.[country-servicearea-facilitycode] AS Country_ServiceArea_FacilityCode, 
             ts.statusdescription AS consignmentstatus_Detail , Case When IsAccountCustomer = 1 then InsuranceAmount else sd.Total end as InsuranceAmt
			 
      FROM   dbo.tblconsignment WITH (nolock) 
             INNER JOIN tblstatus ts ON ts.statusid = dbo.tblconsignment.consignmentstatus 
			 LEFT JOIN tblSalesorder so 
			 INNER JOIN tblSalesOrderDetail sd on sd.SalesorderID = so.SalesorderID and [Description] = 'Insurance'
			 on so.ReferenceNo = dbo.tblconsignment.ConsignmentID
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentID 

--------------------------------------------------------------------------------------------------------------------------------------------------------------

      SELECT @PicupID = dbo.tblconsignment.pickupid, 
             @DestinationID = dbo.tblconsignment.destinationid, 
             @ContactID = dbo.tblconsignment.contactid 
      FROM   dbo.tblconsignment WITH (nolock) 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             Replace(Replace(Substring(dbo.tbladdress.address1, Charindex('/*/', 
             dbo.tbladdress.address1), 
             Len(dbo.tbladdress.address1) + 1), 
             LEFT(dbo.tbladdress.address1, Iif( 
             Charindex(':', dbo.tbladdress.address1) > 0, 
             Charindex(':', 
             dbo.tbladdress.address1), 1) - 1), 'C/-'), ':', '') AS address1 
             --dbo.tblAddress.Address1        --Changed by shubham 04/04/2016 Required by Meharwan Sing 
             , 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             isnull(dbo.tbladdress.issubscribe,0) as issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END                                                 AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress WITH (nolock) 
             LEFT JOIN dbo.tblstate WITH (nolock) 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @PicupID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             Replace(Replace(Substring(dbo.tbladdress.address1, Charindex('/*/', 
             dbo.tbladdress.address1), 
             Len(dbo.tbladdress.address1) + 1), 
             LEFT(dbo.tbladdress.address1, Iif( 
             Charindex(':', dbo.tbladdress.address1) > 0, 
             Charindex(':', dbo.tbladdress. 
             address1 
             ), 1) - 1), 'C/-'), ':', '') AS address1 
             --dbo.tblAddress.Address1            --Changed by shubham 04/04/2016 Required by Meharwan Sing 
             , 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             isnull(dbo.tbladdress.issubscribe,0) as issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END                          AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress WITH (nolock) 
             LEFT JOIN dbo.tblstate WITH (nolock) 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @DestinationID 

      SELECT * 
      FROM   [dbo].[tblitemlabel] WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             Replace(Replace(Substring(dbo.tbladdress.address1, Charindex('/*/', 
             dbo.tbladdress.address1), 
             Len(dbo.tbladdress.address1) + 1), 
             LEFT(dbo.tbladdress.address1, Iif( 
             Charindex(':', dbo.tbladdress.address1) > 0, 
             Charindex(':', 
             dbo.tbladdress.address1), 1) - 1), 'C/-'), ':', '') AS address1 
             --dbo.tblAddress.Address1      --Changed by shubham 04/04/2016 Required by Meharwan Sing 
             , 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             isnull(dbo.tbladdress.issubscribe,0) as issubscribe, 
             CASE 
               WHEN dbo.tblstate.statecode IS NULL THEN dbo.tbladdress.statename 
               ELSE dbo.tblstate.statecode 
             END                                                 AS StateID, 
             country, 
             countrycode 
      FROM   dbo.tbladdress WITH (nolock) 
             LEFT JOIN dbo.tblstate WITH (nolock) 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @ContactID 

      SELECT * 
      FROM   tbldhlbarcodeimage WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 

      SELECT TOP 1 * 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 

      DECLARE @SalesOrderID INT, 
              @InvoiceNo    NVARCHAR(50) 

      SELECT TOP 1 @SalesOrderID = salesorderid, 
                   @InvoiceNo = invoiceno 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 

      SELECT * 
      FROM   dbo.tblsalesorderdetail WITH (nolock) 
      WHERE  salesorderid = @SalesOrderID 

      SELECT * 
      FROM   dbo.tblinvoice WITH (nolock) 
      WHERE  invoicenumber = @InvoiceNo 

      SELECT * 
      FROM   dbo.tblcustomdeclaration WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 

      SELECT * 
      FROM   dbo.tblitemlabelimage WITH (nolock) 
      WHERE  itemlabelid IN (SELECT itemlabelid 
                             FROM   [dbo].[tblitemlabel] WITH (nolock) 
                             WHERE  consignmentid = @ConsignmentID) 

      DECLARE @SalesOrderID1 INT 
      DECLARE @InvoiceNo1 NVARCHAR(50) 

      SELECT TOP 1 * 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 
             AND salesorderid <> @SalesOrderID 

      SELECT @SalesOrderID1 = salesorderid, 
             @InvoiceNo1 = invoiceno 
      FROM   dbo.tblsalesorder WITH (nolock) 
      WHERE  referenceno = @ConsignmentID 
             AND salesorderid <> @SalesOrderID 

      SELECT * 
      FROM   dbo.tblsalesorderdetail WITH (nolock) 
      WHERE  salesorderid = @SalesOrderID1 

      SELECT * 
      FROM   dbo.tblinvoice WITH (nolock) 
      WHERE  invoicenumber = @InvoiceNo1 

      SELECT * 
      FROM   dbo.[tblbooking] WITH (nolock) 
      WHERE  consignmentid = @ConsignmentID 

	  
	  SELECT * FROM dbo.TblRefund WITH (NOLOCK)
	  WHERE consignmentid = @ConsignmentID order by 1 desc
	
  END 
GO
