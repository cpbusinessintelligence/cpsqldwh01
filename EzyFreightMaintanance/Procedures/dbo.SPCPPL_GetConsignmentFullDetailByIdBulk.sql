SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetConsignmentFullDetailByIdBulk]
      
  
           @ConsignmentID int =null
           
AS
BEGIN

declare @DestinationID int ,
@ContactID int ,
@PicupID int ,
@UserID int 
   
--SELECT     dbo.tblInvoice.*
--FROM         dbo.tblConsignment INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo INNER JOIN
--                      dbo.tblInvoice ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
                      
--                       where dbo.tblConsignment.ConsignmentID = @ConsignmentID
           
           SELECT     dbo.tblConsignment.*, dbo.tblConsignment.[Country-ServiceArea-FacilityCode] as Country_ServiceArea_FacilityCode
FROM         dbo.tblConsignment  WITH (NOLOCK)   where dbo.tblConsignment.ConsignmentID = @ConsignmentID


--SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*
--FROM         dbo.tblSalesOrderDetail INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
--WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID)

				


			   SELECT   @PicupID = dbo.tblConsignment.PickupID,
			   @DestinationID=dbo.tblConsignment.DestinationID,
			   @UserID=dbo.tblConsignment.UserID
FROM         dbo.tblConsignment  WITH (NOLOCK)  where dbo.tblConsignment.ConsignmentID = @ConsignmentID


   SELECT    
			   @ContactID=AddressID
FROM         dbo.tblAddress  WITH (NOLOCK)  where UserID = @UserID and IsRegisterAddress = 1

            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress  WITH (NOLOCK)  left JOIN
                      dbo.tblState WITH (NOLOCK)   ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe,case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress WITH (NOLOCK)   left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID
SELECT *
  FROM [dbo].[tblItemLabel]  WITH (NOLOCK)  where ConsignmentID = @ConsignmentID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress  WITH (NOLOCK)  left JOIN
                      dbo.tblState  WITH (NOLOCK)  ON dbo.tblAddress.StateID = dbo.tblState.StateID
                       where AddressID =@ContactID



            select * from tblDHLBarCodeImage WITH (NOLOCK)   where  ConsignmentID = @ConsignmentID
  
  
            select top 1 * from dbo.tblSalesOrder WITH (NOLOCK)   where  ReferenceNo = @ConsignmentID
            
            declare @SalesOrderID int,
                    @InvoiceNo nvarchar(50)
            
              select top 1 @SalesOrderID=SalesOrderID,
               @InvoiceNo=InvoiceNo from dbo.tblSalesOrder  WITH (NOLOCK)  where  ReferenceNo = @ConsignmentID
            
            select * from dbo.tblSalesOrderDetail  WITH (NOLOCK)  where  SalesOrderID = @SalesOrderID
            select * from dbo.tblInvoice  WITH (NOLOCK)  where  InvoiceNumber = @InvoiceNo
           

            
            select * from dbo.tblCustomDeclaration  WITH (NOLOCK)  where  ConsignmentID = @ConsignmentID
            select * from dbo.tblItemLabelImage  WITH (NOLOCK)  where  ItemLabelID in (SELECT ItemLabelID
  FROM [dbo].[tblItemLabel]  WITH (NOLOCK)  where ConsignmentID = @ConsignmentID)
  
  
  
              declare @SalesOrderID1 int
              declare @InvoiceNo1 nvarchar(50)
 select top 1 * from dbo.tblSalesOrder  WITH (NOLOCK)  where  ReferenceNo = @ConsignmentID and  SalesOrderID <> @SalesOrderID 
     select   @SalesOrderID1=SalesOrderID,
               @InvoiceNo1=InvoiceNo from dbo.tblSalesOrder  WITH (NOLOCK)  where  ReferenceNo = @ConsignmentID and  SalesOrderID <> @SalesOrderID 
             select * from dbo.tblSalesOrderDetail where  SalesOrderID = @SalesOrderID1
            select * from dbo.tblInvoice  WITH (NOLOCK)  where  InvoiceNumber = @InvoiceNo1
            
            select * from dbo.[tblBooking]  WITH (NOLOCK)  where  ConsignmentID = @ConsignmentID
            
END

GO
