SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SPCPPL_GetCustomersZoneWise]
@StateId int,
@PageIndex int=1,
 @SortColumn varchar(50) = null,
  @SortDir varchar(50)=null,
@PageSize int=10
--@ParamTotalRec_out int out
AS
BEGIN


--  SELECT Zone Wise User According to AdminUserID for state.
(select distinct count(*)  from tblAddress where StateID=@StateId and (IsDeleted<>1 or IsDeleted is null))
				select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY tblInner.CreatedDateTime DESC)  RowNum, * 
					FROM 
					(SELECT     addPikup.* FROM         dbo.tblAddress AS addPikup WHERE     (addPikup.StateID =(@StateId)and IsRegisterAddress=1 and (IsDeleted<>1 or IsDeleted is null))
					) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY tblOuter.CreatedDateTime,
  CASE WHEN  @SortDir = 'DESC' THEN @SortColumn  END DESC,
  CASE WHEN  @SortDir = 'ASC'  THEN @SortColumn END
  ;

END



-- [SPCPPL_GetCustomersZoneWise] 1,1,'FirstName','ASC',10
GO
