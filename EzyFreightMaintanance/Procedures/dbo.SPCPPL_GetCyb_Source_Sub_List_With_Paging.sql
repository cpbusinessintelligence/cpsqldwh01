SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[SPCPPL_GetCyb_Source_Sub_List_With_Paging]
@SubscriptionId varchar(max)=null,
@MerchantRefCode varchar(max)=null,
@PaymentRefNo varchar(max)=null,
@AuthorizationCode varchar(max)=null,
@FromDate date=null,
@ToDate date=null
AS
BEGIN


select top 100 SubscriptionID, MerchantReferenceCode, PaymentRefNo, AuthorizationCode,FORMAT(createddatetime,'dd/MM/yyyy HH:mm')  as CreatedDate 
from [tblInvoice]  
where isnull(SubscriptionID,'') <>'' and SubscriptionID not in (SELECT SubscriptionID from [tblDeletedSubscription] where [Message] like '%100%')
and CASE WHEN isnull(@SubscriptionId,'') = '' THEN isnull(@SubscriptionId,'') else SubscriptionID  END like '%'+isnull(@SubscriptionId,'')+'%' 
and CASE WHEN isnull(@MerchantRefCode,'') = '' THEN isnull(@MerchantRefCode,'') else MerchantReferenceCode  END like '%'+isnull(@MerchantRefCode,'')+'%' 
and CASE WHEN isnull(@PaymentRefNo,'') = '' THEN isnull(@PaymentRefNo,'') else PaymentRefNo  END like '%'+isnull(@PaymentRefNo,'')+'%' 
and CASE WHEN isnull(@AuthorizationCode,'') = '' THEN isnull(@AuthorizationCode,'') else AuthorizationCode  END like '%'+isnull(@AuthorizationCode,'')+'%' 
and CASE WHEN isnull(@FromDate,'')='' THEN CAST( createddatetime as DATE) else @FromDate END <= CAST( createddatetime as DATE) 
and CASE WHEN isnull(@ToDate,'')='' THEN CAST( createddatetime as DATE) else @ToDate END >= CAST( createddatetime as DATE)
order by createddatetime desc
END
GO
