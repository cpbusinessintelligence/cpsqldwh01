SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetDeliveryChoices]
@strId int = null, 
@strCategory nvarchar(12) = null,
@strSuburb nvarchar(100) = null
AS
BEGIN

SELECT DISTINCT ID,DeliveryChoiceID,Category,DeliveryChoiceID,Sortingcode,DeliveryChoiceName,DeliveryChoiceDescription,[Operation Hours] as OperationHours,Address1,Address2,Address3,Suburb,PostCode,State,Country,CreatedDateTime,CreatedBy,ModifiedDateTime,ModifiedBy,Latitude,Longtitude,IsRedeliveryAvailable,
  STUFF((SELECT distinct ', ' + p1.BeatID
         FROM vw_DeliveryChoicesWithBeat p1
         WHERE p.DeliveryChoiceID = p1.DeliveryChoiceID
            FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'') BeatIds
FROM vw_DeliveryChoices p
where Case When isnull(@strId,'') <> '' then Id else '' end = isnull(@strId,'') and
(Case When isnull(@strCategory,'') <> '' then Category else '' end like '%'+ isnull(@strCategory,'') + '%' and
Case When isnull(@strSuburb,'') <> '' then Suburb +' ' + [State] + ' ' + PostCode  else '' end like '%'+ isnull(@strSuburb,'') + '%' )
order by CreatedDateTime desc, ID desc


--SELECT *, [Operation Hours] as OperationHours FROM vw_DeliveryChoices 
--where Case When isnull(@strId,'') <> '' then Id else '' end = isnull(@strId,'') and
--(Case When isnull(@strCategory,'') <> '' then Category else '' end like '%'+ isnull(@strCategory,'') + '%' and
--Case When isnull(@strSuburb,'') <> '' then Suburb +' ' + [State] + ' ' + PostCode  else '' end like '%'+ isnull(@strSuburb,'') + '%' )
--order by CreatedDateTime desc, ID desc

select * from [dbo].[vw_DeliveryChoicesBeat] where DeliveryChoiceID in (SELECT DeliveryChoiceID as OperationHours FROM vw_DeliveryChoices 
where Case When isnull(@strId,'') <> '' then Id else '' end = isnull(@strId,'') and
(Case When isnull(@strCategory,'') <> '' then Category else '' end like '%'+ isnull(@strCategory,'') + '%' and
Case When isnull(@strSuburb,'') <> '' then Suburb +' ' + [State] + ' ' + PostCode  else '' end like '%'+ isnull(@strSuburb,'') + '%' ))

END
GO
