SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetDuplicateBeatId]
@BeatID nvarchar(100) = null,
@DeliveryChoiceID nvarchar(50) = null

AS
BEGIN
		SELECT * FROM [dbo].[vw_DeliveryChoicesWithBeat]
		where DeliveryChoiceID <> @DeliveryChoiceID and BeatID = @BeatID and Category = 'POPStation'

END
GO
