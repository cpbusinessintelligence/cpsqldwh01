SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetDuplicateDeliveryChoiceId]
@ID int = null,
@DeliveryChoiceID nvarchar(50) = null

AS
BEGIN
if isnull(@ID,0) <> 0
begin		
		SELECT * FROM vw_DeliveryChoices 
		where DeliveryChoiceID = @DeliveryChoiceID and ID <> @ID
end
else
begin
		SELECT * FROM vw_DeliveryChoices 
		where DeliveryChoiceID = @DeliveryChoiceID
end
END
GO
