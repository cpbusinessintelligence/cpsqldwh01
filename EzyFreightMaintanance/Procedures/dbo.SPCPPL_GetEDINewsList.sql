SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetEDINewsList] 
@NewsId int = null

As Begin
-----------------------------------------------------------------------------
if(isnull(@NewsId,0) <> 0)
begin
	select [Title],[Description] from CPPLWeb_8_3..tblnews  
	where isnull(IsArchive,0) = 0 and (GetDate() between StartDate and EndDate) and WhereToDisplay = 'EDI'
	and NewsID = @NewsId
	order by 1 desc
end
-----------------------------------------------------------------------------------------------
else
begin
	select [Title],[Description] from CPPLWeb_8_3..tblnews  
	where isnull(IsArchive,0) = 0 and (GetDate() between StartDate and EndDate) and WhereToDisplay = 'EDI'
	order by 1 desc
end

END 
GO
