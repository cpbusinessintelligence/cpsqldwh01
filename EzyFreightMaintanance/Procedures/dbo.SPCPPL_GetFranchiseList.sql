SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetFranchiseList]
@Branch varchar(20)=null,
@PageIndex int=1,
@PageSize int=10 ,
@IsShowall bit=0 

AS
BEGIN
--  SELECT Zone Wise User According to AdminUserID for state.
  select distinct count(*)  from dbo.tblFranchiseList where Branch  = case when @Branch is null then Branch else @Branch end 
						
					and IsArchive =	case when @IsShowall = 1 then IsArchive else 0 end 
						
						--and IsArchive =0
			
			
			
				select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY tblInner.FranchiseID DESC)  RowNum, * 
					FROM 
					(
						SELECT * FROM dbo.tblFranchiseList
						where Branch  = case when @Branch is null then Branch else @Branch end 
							and IsArchive =	case when @IsShowall = 1 then IsArchive else 0 end 
						--and IsArchive =0
					) as tblInner 
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY 
 CreatedDateTime desc
 
END
GO
