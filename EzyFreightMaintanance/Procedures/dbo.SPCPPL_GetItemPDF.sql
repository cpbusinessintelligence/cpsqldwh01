SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetItemPDF]
      
  
           @ConsignmentID int =null
           
AS
BEGIN

declare @DestinationID int ,
@ContactID int ,
@PicupID int 
   
SELECT     dbo.tblInvoice.*
FROM         dbo.tblConsignment INNER JOIN
                      dbo.tblSalesOrder ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo INNER JOIN
                      dbo.tblInvoice ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
                      
                       where dbo.tblConsignment.ConsignmentID = @ConsignmentID
           
           SELECT     dbo.tblConsignment.*
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID


SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*
FROM         dbo.tblSalesOrderDetail INNER JOIN
                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID)

				


			   SELECT   @PicupID = dbo.tblConsignment.PickupID,
			   @DestinationID=dbo.tblConsignment.DestinationID,
			   @ContactID=dbo.tblConsignment.ContactID
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID




            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID
SELECT cast(([Length]*[Width]*[Height])/1000000 as decimal(8,2))as CubicWeight,*
  FROM [dbo].[tblItemLabel] where ConsignmentID = @ConsignmentID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID
                       where AddressID =@ContactID



            
  
           

            
            
END
GO
