SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_GetOperationDocument] 

@OperationDocId int = 0,
@Subject varchar(100) = null,
@Description varchar(100) = null,
@FromDate Date = null,
@ToDate Date = null,
@State varchar(4) = null,
@Favourite bit = null

As Begin
If (isnull(@OperationDocId,0)=0)
begin
Select * 
, STUFF((SELECT ',' + OperationDocName
            FROM CPPLWeb_8_3_Admin..[tblOperationDocumentFiles] where OperationDocId = D.OperationDocId
            FOR XML PATH('')
            ), 1, 1, '') as OperationDocName
,STUFF((SELECT ',' + OperationDocAttachment
            FROM CPPLWeb_8_3_Admin..[tblOperationDocumentFiles] where OperationDocId = D.OperationDocId
            FOR XML PATH('')
            ), 1, 1, '') as OperationDocAttachment,FORMAT(Case when isnull(UpdatedDateTime,'') <> '' then case when  UpdatedDateTime > CreatedDateTime then UpdatedDateTime 
else CreatedDateTime end else CreatedDateTime end,'dd/MM/yyyy') AS CreatedDate 

from CPPLWeb_8_3_Admin..tblOperationDocuments D
Where CASE WHEN @Subject IS null THEN isnull(@Subject,'') else D.OperationDocSubject  END LIKE '%'+isnull(@Subject,'')+'%' and
CASE WHEN @Description IS null THEN isnull(@Description,'') else D.OperationDocDescription  END LIKE '%'+isnull(@Description,'')+'%' and
CASE WHEN @State IS null THEN isnull(@State,'') else D.OperationDocState  END LIKE '%'+isnull(@State,'')+'%' and
CASE WHEN @Favourite IS null THEN isnull(@Favourite,'') else D.IsFavourite  END = isnull(@Favourite,'')  and
((CASE WHEN @FromDate IS null THEN CAST( D.CreatedDateTime as DATE) else @FromDate END <= CAST( D.CreatedDateTime as DATE))and 
(CASE WHEN @ToDate IS null THEN CAST( D.CreatedDateTime as DATE) else @ToDate END >= CAST( D.CreatedDateTime as DATE) ) or 
(CASE WHEN @FromDate IS null THEN CAST( D.UpdatedDateTime as DATE) else @FromDate END <= CAST( D.UpdatedDateTime as DATE))and 
(CASE WHEN @ToDate IS null THEN CAST( D.UpdatedDateTime as DATE) else @ToDate END >= CAST( D.UpdatedDateTime as DATE) ) )
order by case when isnull(D.UpdatedDateTime,'') <> '' then D.UpdatedDateTime else D.CreatedDateTime end desc
end
else
begin
Select * 
, STUFF((SELECT ',' + OperationDocName
            FROM CPPLWeb_8_3_Admin..[tblOperationDocumentFiles] where OperationDocId = D.OperationDocId
            FOR XML PATH('')
            ), 1, 1, '') as OperationDocName
,STUFF((SELECT ',' + OperationDocAttachment
            FROM CPPLWeb_8_3_Admin..[tblOperationDocumentFiles] where OperationDocId = D.OperationDocId
            FOR XML PATH('')
            ), 1, 1, '') as OperationDocAttachment ,FORMAT(Case when isnull(UpdatedDateTime,'') <> '' then case when  UpdatedDateTime > CreatedDateTime then UpdatedDateTime 
else CreatedDateTime end else CreatedDateTime end,'dd/MM/yyyy') AS CreatedDate 
from CPPLWeb_8_3_Admin..tblOperationDocuments D where D.OperationDocId = @OperationDocId  

order by case when isnull(D.UpdatedDateTime,'') <> '' then D.UpdatedDateTime else D.CreatedDateTime end desc
end

END 
GO
