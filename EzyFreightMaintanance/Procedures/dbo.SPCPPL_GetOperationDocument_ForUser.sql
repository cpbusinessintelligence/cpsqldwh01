SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_GetOperationDocument_ForUser] 
@Subject varchar(100) = null,
@Description varchar(100) = null,
@FromDate Date = null,
@ToDate Date = null,
@State varchar(4) = null,
@Favourite bit = null

As Begin

select * from (
select Distinct * from (
select * from (
select top 5 *,FORMAT(Case when isnull(UpdatedDateTime,'') <> '' then case when  UpdatedDateTime > CreatedDateTime then UpdatedDateTime 
else CreatedDateTime end else CreatedDateTime end,'dd/MM/yyyy') AS CreatedDate 

from Dbo.[FNCPPL_GetOperationDocument_ForUser](@Subject,@Description,@FromDate,@ToDate,@State,isnull(@Favourite,1))
where OperationDocIsActive = 1
order by case when isnull(UpdatedDateTime,'') <> '' then case when  UpdatedDateTime > CreatedDateTime then UpdatedDateTime else CreatedDateTime end else CreatedDateTime end desc
) as FirstRecord

union all 

select * from (
select TOP 10 * ,FORMAT(Case when isnull(UpdatedDateTime,'') <> '' then case when  UpdatedDateTime > CreatedDateTime then UpdatedDateTime 
else CreatedDateTime end else CreatedDateTime end,'dd/MM/yyyy') AS CreatedDate 
from Dbo.[FNCPPL_GetOperationDocument_ForUser](@Subject,@Description,@FromDate,@ToDate,@State,isnull(@Favourite,0))
where OperationDocIsActive = 1
order by case when isnull(UpdatedDateTime,'') <> '' then  case when  UpdatedDateTime > CreatedDateTime then UpdatedDateTime else CreatedDateTime end else CreatedDateTime end desc
) as SecondRecord
) As Distinctrecord 
) As Finalrecord
order by Finalrecord.IsFavourite Desc --,case when isnull(Finalrecord.UpdatedDateTime,'') <> '' then case when  Finalrecord.UpdatedDateTime > Finalrecord.CreatedDateTime then Finalrecord.UpdatedDateTime else Finalrecord.CreatedDateTime end else Finalrecord.CreatedDateTime end desc
,case when isnull(Finalrecord.UpdatedDateTime,'') <> '' then Finalrecord.UpdatedDateTime else Finalrecord.CreatedDateTime end Desc --,Finalrecord.UpdatedDateTime Desc

END 
GO
