SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SPCPPL_GetProntoInvoiceDetailListByAccNo]

@AccountNo nvarchar(50) = null
AS
BEGIN

if(@AccountNo is not null)
begin
select top 100  pc.* from tblprontoReceipt pr
inner join tblprontoInvoice pc on pc.ReceiptNo = pr.ReceiptNo
where  IsPaymentProcessed  = 1 and 
(case when isnull(@AccountNo,'')<>'' then pc.WebsiteAccountNo else '' end = isnull(@AccountNo,'')) 

order by createddatetime desc
end

end
GO
