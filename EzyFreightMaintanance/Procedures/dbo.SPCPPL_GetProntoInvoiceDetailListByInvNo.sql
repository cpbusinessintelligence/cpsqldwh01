SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SPCPPL_GetProntoInvoiceDetailListByInvNo]

@InvoiceNo nvarchar(50) = null
AS
BEGIN

if(@InvoiceNo is not null)
begin
select top 100  pc.* from tblprontoReceipt pr
inner join tblprontoInvoice pc on pc.ReceiptNo = pr.ReceiptNo
where  IsPaymentProcessed  = 1 and IsReceiptProcessed = 1 and
(case when isnull(@InvoiceNo,'')<>'' then ltrim(pc.InvoiceNo) else '' end = isnull(ltrim(@InvoiceNo),'')) 

order by createddatetime desc
end
end
GO
