SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------By Shubham 07/03/2016-----------------------------------------------------------------------------

CREATE  PROCEDURE [dbo].[SPCPPL_GetRateCardCategoryUsingAccountNo]
           @AccountNumber nvarchar(250)= null

as begin
		   select top 1 RateCardCategory from tblCompanyUsers cu
inner join tblCompany c on c.CompanyID = cu.CompanyID
where AccountNumber = @AccountNumber

end
GO
