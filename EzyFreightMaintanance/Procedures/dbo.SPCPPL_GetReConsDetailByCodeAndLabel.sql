SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetReConsDetailByCodeAndLabel] 
@SearchCode nvarchar(50) = NULL



AS

BEGIN
  DECLARE @DestinationID int,
          @ContactID int,
          @PicupID int,
          @CurrentDeliveryID int,
		  @ConsignmentCode_Local varchar(50),
		  @UniqueId varchar(50)

SELECT top 1 @ConsignmentCode_Local = ConsignmentCode, @UniqueId = UniqueId from [tblRedirectedConsignment]  IRC
Inner Join [tblRedirectedItemLabel] IL on IRC.ReconsignmentID= IL.ReConsignmentId
where IRC.ConsignmentCode = @SearchCode or IL.LabelNumber = @SearchCode


	SELECT top 1 IRC.*				
	FROM [tblRedirectedConsignment] IRC WITH (NOLOCK) 
	Inner Join [tblRedirectedItemLabel] IL on IRC.ReconsignmentID= IL.ReConsignmentId
	where IRC.ConsignmentCode = @SearchCode or IL.LabelNumber = @SearchCode


	SELECT @PicupID = PickupAddressID, @DestinationID = NewDeliveryAddressID, @ContactID = CurrentDeliveryAddressID
	FROM [tblRedirectedConsignment] IRC WITH (NOLOCK)
	Inner Join [tblRedirectedItemLabel] IL on IRC.ReconsignmentID= IL.ReConsignmentId
	where IRC.ConsignmentCode = @SearchCode or IL.LabelNumber = @SearchCode
  
  SELECT  dbo.tblAddress.AddressID,   dbo.tblAddress.UserID,
    dbo.tblAddress.FirstName,
    dbo.tblAddress.LastName,
    dbo.tblAddress.CompanyName,
    dbo.tblAddress.Email,
    REPLACE(REPLACE(SUBSTRING(dbo.tblAddress.Address1, CHARINDEX('/*/', dbo.tblAddress.Address1), LEN(dbo.tblAddress.Address1) + 1), LEFT(dbo.tblAddress.Address1, IIF(CHARINDEX(':', dbo.tblAddress.Address1) > 0, CHARINDEX(':', dbo.tblAddress.Address1), 1) - 1
    ), 'C/-'), ':', '') AS Address1,
    dbo.tblAddress.Address2,
    dbo.tblAddress.Suburb,
    dbo.tblAddress.PostCode,
    dbo.tblAddress.Phone,
    dbo.tblAddress.Mobile,
    dbo.tblAddress.CreatedDateTime,
    dbo.tblAddress.CreatedBy,
    dbo.tblAddress.UpdatedDateTime,
    dbo.tblAddress.UpdatedBy,
    dbo.tblAddress.IsRegisterAddress,
    dbo.tblAddress.IsDeleted,
    dbo.tblAddress.IsBusiness,
    dbo.tblAddress.IsSubscribe,
    CASE
      WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName
      ELSE dbo.tblState.StateCode
    END AS StateID,
    Country,
    CountryCode
  FROM dbo.tblAddress WITH (NOLOCK)
  LEFT JOIN dbo.tblState WITH (NOLOCK)
    ON dbo.tblAddress.StateID = dbo.tblState.StateID
  WHERE AddressID = @PicupID


  SELECT
    dbo.tblAddress.AddressID,
    dbo.tblAddress.UserID,
    dbo.tblAddress.FirstName,
    dbo.tblAddress.LastName,
    dbo.tblAddress.CompanyName,
    dbo.tblAddress.Email,
    REPLACE(REPLACE(SUBSTRING(dbo.tblAddress.Address1, CHARINDEX('/*/', dbo.tblAddress.Address1), LEN(dbo.tblAddress.Address1) + 1), LEFT(dbo.tblAddress.Address1, IIF(CHARINDEX(':', dbo.tblAddress.Address1) > 0, CHARINDEX(':', dbo.tblAddress.
    Address1), 1) - 1), 'C/-'), ':', '') AS Address1,
    dbo.tblAddress.Address2,
    dbo.tblAddress.Suburb,
    dbo.tblAddress.PostCode,
    dbo.tblAddress.Phone,
    dbo.tblAddress.Mobile,
    dbo.tblAddress.CreatedDateTime,
    dbo.tblAddress.CreatedBy,
    dbo.tblAddress.UpdatedDateTime,
    dbo.tblAddress.UpdatedBy,
    dbo.tblAddress.IsRegisterAddress,
    dbo.tblAddress.IsDeleted,
    dbo.tblAddress.IsBusiness,
    dbo.tblAddress.IsSubscribe,
    CASE
      WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName
      ELSE dbo.tblState.StateCode
    END AS StateID,
    Country,
    CountryCode
  FROM dbo.tblAddress WITH (NOLOCK)
  LEFT JOIN dbo.tblState WITH (NOLOCK)
    ON dbo.tblAddress.StateID = dbo.tblState.StateID
  WHERE AddressID = @DestinationID



  SELECT IL.* FROM [dbo].[tblRedirectedConsignment] IRC WITH (NOLOCK)
  Inner Join [tblRedirectedItemLabel] IL on IRC.ReconsignmentID= IL.ReConsignmentId
  where IRC.ConsignmentCode = @SearchCode or IL.LabelNumber = @SearchCode



  SELECT
    dbo.tblAddress.AddressID,
    dbo.tblAddress.UserID,
    dbo.tblAddress.FirstName,
    dbo.tblAddress.LastName,
    dbo.tblAddress.CompanyName,
    dbo.tblAddress.Email,
    REPLACE(REPLACE(SUBSTRING(dbo.tblAddress.Address1, CHARINDEX('/*/', dbo.tblAddress.Address1), LEN(dbo.tblAddress.Address1) + 1), LEFT(dbo.tblAddress.Address1, IIF(CHARINDEX(':', dbo.tblAddress.Address1) > 0, CHARINDEX(':', dbo.tblAddress.Address1), 1) - 1
    ), 'C/-'), ':', '') AS Address1,
    dbo.tblAddress.Address2,
    dbo.tblAddress.Suburb,
    dbo.tblAddress.PostCode,
    dbo.tblAddress.Phone,
    dbo.tblAddress.Mobile,
    dbo.tblAddress.CreatedDateTime,
    dbo.tblAddress.CreatedBy,
    dbo.tblAddress.UpdatedDateTime,
    dbo.tblAddress.UpdatedBy,
    dbo.tblAddress.IsRegisterAddress,
    dbo.tblAddress.IsDeleted,
    dbo.tblAddress.IsBusiness,
    dbo.tblAddress.IsSubscribe,
    CASE
      WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName
      ELSE dbo.tblState.StateCode
    END AS StateID,
    Country,
    CountryCode
  FROM dbo.tblAddress WITH (NOLOCK)
  LEFT JOIN dbo.tblState WITH (NOLOCK)
    ON dbo.tblAddress.StateID = dbo.tblState.StateID
  WHERE AddressID = @ContactID

  


SELECT TOP 1 * FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode_Local  order by 1 desc
  
DECLARE @SalesOrderID int,
        @InvoiceNo nvarchar(50)

  SELECT TOP 1 @SalesOrderID = SalesOrderID, @InvoiceNo = InvoiceNo
  FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode_Local order by 1 desc
  
  SELECT * FROM dbo.tblSalesOrderDetail WITH (NOLOCK)
  WHERE SalesOrderID = @SalesOrderID

  SELECT *  FROM dbo.tblInvoice WITH (NOLOCK)
  WHERE InvoiceNumber = @InvoiceNo 
  
   DECLARE @SalesOrderID1 int
  DECLARE @InvoiceNo1 nvarchar(50)
  
  SELECT TOP 1 *  FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode_Local AND SalesOrderID <> @SalesOrderID 

  SELECT @SalesOrderID1 = SalesOrderID, @InvoiceNo1 = InvoiceNo  FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode_Local AND SalesOrderID <> @SalesOrderID 

  SELECT * FROM dbo.tblSalesOrderDetail WITH (NOLOCK)
  WHERE SalesOrderID = @SalesOrderID1

  SELECT * FROM dbo.tblInvoice WITH (NOLOCK)
  WHERE InvoiceNumber = @InvoiceNo1
  
  

 END
GO
