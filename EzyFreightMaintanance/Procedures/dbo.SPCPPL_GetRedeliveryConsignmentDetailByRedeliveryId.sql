SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetRedeliveryConsignmentDetailByRedeliveryId] 
@dtReDeliveryIds dtReDeliveryIds readonly


AS

BEGIN
  DECLARE @PickupID int,
          @DestinationID int,
          @ContactID int,
          @CurrentDeliveryID int,
		  @ConsignmentCode varchar(50),
		  @UniqueId varchar(50)

SELECT @ConsignmentCode = ConsignmentCode from [tblRedelivery] WHERE [RedeliveryID] in (Select top 1 [StrRedeliveryID] from @dtReDeliveryIds)

	SELECT top 1 [TblRedeliveryConsignment].*				
	FROM [TblRedeliveryConsignment] WITH (NOLOCK)
	WHERE ConsignmentCode = @ConsignmentCode order by 1 desc

	SELECT top 1 @PickupID = PickupAddressID, @ContactID = CurrentDeliveryAddressID, @DestinationID = NewDeliveryAddressID
	FROM [TblRedeliveryConsignment] WITH (NOLOCK)
	WHERE ConsignmentCode = @ConsignmentCode order by 1 desc
  
  SELECT  dbo.tblAddress.AddressID,   dbo.tblAddress.UserID,
    dbo.tblAddress.FirstName,
    dbo.tblAddress.LastName,
    dbo.tblAddress.CompanyName,
    dbo.tblAddress.Email,
    REPLACE(REPLACE(SUBSTRING(dbo.tblAddress.Address1, CHARINDEX('/*/', dbo.tblAddress.Address1), LEN(dbo.tblAddress.Address1) + 1), LEFT(dbo.tblAddress.Address1, IIF(CHARINDEX(':', dbo.tblAddress.Address1) > 0, CHARINDEX(':', dbo.tblAddress.Address1), 1) - 1
    ), 'C/-'), ':', '') AS Address1,
    dbo.tblAddress.Address2,
    dbo.tblAddress.Suburb,
    dbo.tblAddress.PostCode,
    dbo.tblAddress.Phone,
    dbo.tblAddress.Mobile,
    dbo.tblAddress.CreatedDateTime,
    dbo.tblAddress.CreatedBy,
    dbo.tblAddress.UpdatedDateTime,
    dbo.tblAddress.UpdatedBy,
    dbo.tblAddress.IsRegisterAddress,
    dbo.tblAddress.IsDeleted,
    dbo.tblAddress.IsBusiness,
    dbo.tblAddress.IsSubscribe,
    CASE
      WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName
      ELSE dbo.tblState.StateCode
    END AS StateID,
    Country,
    CountryCode
  FROM dbo.tblAddress WITH (NOLOCK)
  LEFT JOIN dbo.tblState WITH (NOLOCK)
    ON dbo.tblAddress.StateID = dbo.tblState.StateID
  WHERE AddressID = @PicKupID


  SELECT    dbo.tblAddress.AddressID,    dbo.tblAddress.UserID,
    dbo.tblAddress.FirstName,
    dbo.tblAddress.LastName,
    dbo.tblAddress.CompanyName,
    dbo.tblAddress.Email,
    REPLACE(REPLACE(SUBSTRING(dbo.tblAddress.Address1, CHARINDEX('/*/', dbo.tblAddress.Address1), LEN(dbo.tblAddress.Address1) + 1), LEFT(dbo.tblAddress.Address1, IIF(CHARINDEX(':', dbo.tblAddress.Address1) > 0, CHARINDEX(':', dbo.tblAddress.
    Address1), 1) - 1), 'C/-'), ':', '') AS Address1,
    dbo.tblAddress.Address2,
    dbo.tblAddress.Suburb,
    dbo.tblAddress.PostCode,
    dbo.tblAddress.Phone,
    dbo.tblAddress.Mobile,
    dbo.tblAddress.CreatedDateTime,
    dbo.tblAddress.CreatedBy,
    dbo.tblAddress.UpdatedDateTime,
    dbo.tblAddress.UpdatedBy,
    dbo.tblAddress.IsRegisterAddress,
    dbo.tblAddress.IsDeleted,
    dbo.tblAddress.IsBusiness,
    dbo.tblAddress.IsSubscribe,
    CASE
      WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName
      ELSE dbo.tblState.StateCode
    END AS StateID,
    Country,
    CountryCode
  FROM dbo.tblAddress WITH (NOLOCK)
  LEFT JOIN dbo.tblState WITH (NOLOCK)
    ON dbo.tblAddress.StateID = dbo.tblState.StateID
  WHERE AddressID = @DestinationID
  
  
  SELECT    dbo.tblAddress.AddressID,    dbo.tblAddress.UserID,
    dbo.tblAddress.FirstName,
    dbo.tblAddress.LastName,
    dbo.tblAddress.CompanyName,
    dbo.tblAddress.Email,
    REPLACE(REPLACE(SUBSTRING(dbo.tblAddress.Address1, CHARINDEX('/*/', dbo.tblAddress.Address1), LEN(dbo.tblAddress.Address1) + 1), LEFT(dbo.tblAddress.Address1, IIF(CHARINDEX(':', dbo.tblAddress.Address1) > 0, CHARINDEX(':', dbo.tblAddress.Address1), 1) - 1
    ), 'C/-'), ':', '') AS Address1,
    dbo.tblAddress.Address2,
    dbo.tblAddress.Suburb,
    dbo.tblAddress.PostCode,
    dbo.tblAddress.Phone,
    dbo.tblAddress.Mobile,
    dbo.tblAddress.CreatedDateTime,
    dbo.tblAddress.CreatedBy,
    dbo.tblAddress.UpdatedDateTime,
    dbo.tblAddress.UpdatedBy,
    dbo.tblAddress.IsRegisterAddress,
    dbo.tblAddress.IsDeleted,
    dbo.tblAddress.IsBusiness,
    dbo.tblAddress.IsSubscribe,
    CASE
      WHEN dbo.tblState.StateCode IS NULL THEN dbo.tblAddress.StateName
      ELSE dbo.tblState.StateCode
    END AS StateID,
    Country,
    CountryCode
  FROM dbo.tblAddress WITH (NOLOCK)
  LEFT JOIN dbo.tblState WITH (NOLOCK)
    ON dbo.tblAddress.StateID = dbo.tblState.StateID
  WHERE AddressID = @ContactID

  
  SELECT * FROM [dbo].[tblRedeliveryItemLabel] WITH (NOLOCK)
  WHERE RedeliveryConsignment in (SELECT top 1 RedeliveryConsignmentID FROM [TblRedeliveryConsignment] WITH (NOLOCK) WHERE ConsignmentCode = @ConsignmentCode  order by 1 desc)

  

SELECT TOP 1 * FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode and ISNULL(SalesOrderType,'')  = 'REDELIVERY'  order by 1 desc
  
DECLARE @SalesOrderID int, @InvoiceNo nvarchar(50)

  SELECT TOP 1 @SalesOrderID = SalesOrderID, @InvoiceNo = InvoiceNo
  FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode and ISNULL(SalesOrderType,'') = 'REDELIVERY' order by 1 desc
  
  SELECT * FROM dbo.tblSalesOrderDetail WITH (NOLOCK)
  WHERE SalesOrderID = @SalesOrderID

  SELECT *  FROM dbo.tblInvoice WITH (NOLOCK)
  WHERE InvoiceNumber = @InvoiceNo 

  DECLARE @SalesOrderID1 int
  DECLARE @InvoiceNo1 nvarchar(50)
  
  SELECT TOP 1 *  FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode AND SalesOrderID <> @SalesOrderID AND ISNULL(SalesOrderType,'')  = 'REDELIVERY'

  SELECT @SalesOrderID1 = SalesOrderID, @InvoiceNo1 = InvoiceNo  FROM dbo.tblSalesOrder WITH (NOLOCK)
  WHERE ConsignmentCode = @ConsignmentCode AND SalesOrderID <> @SalesOrderID  AND ISNULL(SalesOrderType,'')  = 'REDELIVERY'

  SELECT * FROM dbo.tblSalesOrderDetail WITH (NOLOCK)
  WHERE SalesOrderID = @SalesOrderID1

  SELECT * FROM dbo.tblInvoice WITH (NOLOCK)
  WHERE InvoiceNumber = @InvoiceNo1

	SELECT [tblRedelivery].*				
	FROM [tblRedelivery] WITH (NOLOCK)
	WHERE [RedeliveryID] in (Select [StrRedeliveryID] from @dtReDeliveryIds)

 END
GO
