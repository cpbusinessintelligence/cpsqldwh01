SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPPL_GetRedirectedLabelDetail]

  @LabelNumber LabelNoArray readonly

As 
Begin

SELECT TRC.ConsignmentCode, TRL.LabelNumber, NewAddress.CompanyName, NewAddress.FirstName, NewAddress.LastName, NewAddress.Address1, NewAddress.Address2, NewAddress.Suburb
, NewAddress.Postcode, NewAddress.StateName,
TRC.SpecialInstruction + ' ' + OldAddress.Address1 + ' ' + OldAddress.Address2 As SpecialInstruction , 
Case when TRC.SelectedDeliveryOption = 'Neighbour Delivery' then 'Neighbour' else TRC.SelectedDeliveryOption end as SelectedDeliveryOption
FROM [tblRedirectedItemLabel] TRL
INNER JOIN [tblRedirectedConsignment] TRC ON TRC.ReConsignmentID = TRL.ReConsignmentID
INNER JOIN [tblAddress] NewAddress ON NewAddress.ADDRESSID = TRC.NewDeliveryAddressID
INNER JOIN [tblAddress] OldAddress ON OldAddress.ADDRESSID = TRC.CurrentDeliveryAddressID
WHERE TRL.LabelNumber in (select LabelNumber from @LabelNumber)

End
GO
