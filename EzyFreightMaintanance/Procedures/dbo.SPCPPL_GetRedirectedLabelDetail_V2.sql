SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_GetRedirectedLabelDetail_V2]

  @LabelNumber LabelNoArray readonly

As 
Begin

SELECT TRC.ConsignmentCode, TRL.LabelNumber,TRC.SpecialInstruction + ' ' + OldAddress.Address1 + ' ' + OldAddress.Address2 As SpecialInstruction 
, OldAddress.CompanyName as CurrentCompanyName, OldAddress.FirstName As CurrentFirstName, OldAddress.LastName As CurrentLastName
, OldAddress.Address1 As CurrentAddress1, OldAddress.Address2 As CurrentAddress2, OldAddress.Suburb As CurrentSuburb, OldAddress.Postcode As CurrentPostcode
, OldAddress.StateName As CurrentState
, NewAddress.CompanyName As NewCompanyName, NewAddress.FirstName As NewFirstName, NewAddress.LastName As NewLastName, NewAddress.Address1 As NewAddress1, 
NewAddress.Address2 As NewAddress2
, NewAddress.Suburb As NewSuburb, NewAddress.Postcode As NewPostcode, NewAddress.StateName As NewState , 
Case when TRC.SelectedDeliveryOption = 'Neighbour Delivery' then 'Neighbour' else TRC.SelectedDeliveryOption end as SelectedDeliveryOption
FROM [tblRedirectedItemLabel] TRL
INNER JOIN [tblRedirectedConsignment] TRC ON TRC.ReConsignmentID = TRL.ReConsignmentID
INNER JOIN [tblAddress] NewAddress ON NewAddress.ADDRESSID = TRC.NewDeliveryAddressID
INNER JOIN [tblAddress] OldAddress ON OldAddress.ADDRESSID = TRC.CurrentDeliveryAddressID
WHERE TRL.LabelNumber in (select LabelNumber from @LabelNumber)

End
GO
