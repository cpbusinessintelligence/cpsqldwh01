SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetRefundAmount]
           @ConsignmentID int= null
AS
BEGIN

SELECT     SUM(INV.TotalFreightInclGST) AS TotalFreightInclGST, SUM(RF.Amount)/so.nobe AS RefundAmount,so.nobe
FROM         (SELECT     SUM(Amount) AS Amount,  ConsignmentID
                       FROM          dbo.tblRefund AS RFD
                       WHERE      (ConsignmentID IS NOT NULL) AND (ConsignmentID = @ConsignmentID) and RFD.IsProcess = 1 and isnull(RFD.PaymentRefNo,'') <> ''
                       GROUP BY ConsignmentID) AS RF RIGHT OUTER JOIN
                      (SELECT   *,
                      (SELECT     COUNT(*) AS nobe
FROM         dbo.tblSalesOrder where ReferenceNo=@ConsignmentID
GROUP BY  ReferenceNo)as nobe
FROM         dbo.tblSalesOrder
) AS SO ON RF.ConsignmentID = SO.ReferenceNo RIGHT OUTER JOIN
                      dbo.tblInvoice AS INV ON SO.InvoiceNo = INV.InvoiceNumber
WHERE     (SO.ReferenceNo = @ConsignmentID)
GROUP BY SO.ReferenceNo,so.nobe
                     
           
END
GO
