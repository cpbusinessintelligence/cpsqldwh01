SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetRefundAmount_ForRedirection]
           @ReConsignmentID int= null
AS
BEGIN

SELECT SUM(INV.TotalFreightInclGST) AS TotalFreightInclGST, SUM(RF.Amount)/so.nobe AS RefundAmount,so.nobe
FROM (
SELECT SUM(Amount) AS Amount,  ReConsignmentID FROM dbo.tblRedirectedRefund AS RFD
WHERE (ReConsignmentID IS NOT NULL) AND (ReConsignmentID = @ReConsignmentID) and RFD.IsProcess = 1 and isnull(RFD.PaymentRefNo,'') <> ''
GROUP BY ReConsignmentID) AS RF RIGHT OUTER JOIN
(SELECT   *, (SELECT     COUNT(*) AS nobe FROM dbo.tblSalesOrder where ConsignmentCode in (select ConsignmentCode from [tblRedirectedConsignment] where 

ReconsignmentId = @ReConsignmentID )  
GROUP BY  ReferenceNo)as nobe
FROM dbo.tblSalesOrder ) AS SO ON  SO.ConsignmentCode  = (select ConsignmentCode from [tblRedirectedConsignment] where ReconsignmentId = RF.ReConsignmentID) 
RIGHT OUTER JOIN dbo.tblInvoice AS INV ON SO.InvoiceNo = INV.InvoiceNumber
WHERE (SO.ConsignmentCode in (select ConsignmentCode from [tblRedirectedConsignment] where ReconsignmentId = @ReConsignmentID ))
GROUP BY SO.ReferenceNo,so.nobe
                   
           
END
GO
