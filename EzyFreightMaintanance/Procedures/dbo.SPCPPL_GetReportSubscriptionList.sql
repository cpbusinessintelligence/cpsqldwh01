SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_GetReportSubscriptionList] 
@category nvarchar(20) = null,
@Reportname nvarchar(100) = null,
@Email nvarchar(200) = null,
@SerialNo int = 0,
@AccountNo varchar(100) = null

As 
Begin

SELECT * FROM [Subscriptions] 
Where Reportname in ('Freight Exception Report','Missed Pickup Report_Customer')
And  Case when isnull(@Reportname,'') <> '' then Reportname else '' end =isnull(@Reportname,'')
And  Case when isnull(@category,'') <> '' then category else '' end =isnull(@category,'')
And  (Case when isnull(@Email,'') <> '' then [To] else '' end like '%'+isnull(@Email,'')+'%' Or Case when isnull(@Email,'') <> '' then [CC] else '' end like '%'+isnull(@Email,'')+'%')
And  Case when isnull(@SerialNo,0) <> 0 then SerialNumber else 0 end =isnull(@SerialNo,0)
And  Case when isnull(@AccountNo,'') <> '' then Parameter1 else '' end =isnull(@AccountNo,'')

order by serialnumber Desc


End
GO
