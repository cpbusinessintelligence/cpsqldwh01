SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_GetReportSubscriptionReportnameList]


As 
Begin

SELECT Distinct [Reportname] as Reportname FROM [Subscriptions] 
Where Reportname in ('Freight Exception Report','Missed Pickup Report_Customer')


End
GO
