SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetUserAddressBookByUserId]
@UserId int,
@PageIndex int=1,
@PageSize int=10,
 @SortColumn varchar(50) = null,
 @SortDir varchar(50)=null
AS
BEGIN
--  SELECT Zone Wise User According to AdminUserID for state.
 (select distinct count(*)  from tblAddress where  UserID=@UserId and  (IsDeleted<>1 or IsDeleted is null) and isnull([Country],'') = '' and isnull([countrycode],'') = '' )



select * from 
(SELECT ROW_NUMBER() OVER(ORDER BY tblInner.CreatedDateTime DESC)  RowNum, * 
					FROM 
					(
select TOP 100 PERCENT * from (SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.StateID, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, 
dbo.tblAddress.Mobile, dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, 
dbo.tblAddress.IsRegisterAddress, dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblState.StateCode, dbo.tblAddress.Country, dbo.tblAddress.countrycode
FROM dbo.tblAddress
INNER JOIN dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID) as tadd
where ( tadd.UserID=@UserId )  And tadd.IsRegisterAddress =0
and (tadd.IsDeleted<>1 or tadd.IsDeleted is null)
and isnull(tadd.Country,'') = '' and isnull(tadd.countrycode,'') = ''

order by tadd.CreatedDateTime desc
					
					) as tblInner 
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				--ORDER BY 
				 order by CreatedDateTime desc,
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'DESC' THEN FirstName END  DESC,
	--CASE WHEN @SortColumn = 'TitleString' AND @SortDir = 'DESC' THEN TitleString END DESC,
  --CASE WHEN @SortColumn = 'AuthorString' AND @SortDir = 'ASC'  THEN AuthorString END,
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'ASC'  THEN FirstName END;
END
GO
