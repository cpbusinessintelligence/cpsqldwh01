SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetUserAddressBookByUserIdForInternational]
@UserId int = -5672,
@PageIndex int=1,
@PageSize int=10,
@SortColumn varchar(50) = null,
@SortDir varchar(50)=null
AS
BEGIN

----------Count
  select distinct count(*)  from tblAddress where  UserID=@UserId and  (IsDeleted<>1 or IsDeleted is null)
  and isnull([Country],'') <> '' and isnull([countrycode],'')<> ''
  And  IsRegisterAddress =0
 
----------Record
 select distinct * from 
(
SELECT ROW_NUMBER() OVER(ORDER BY tblInner.CreatedDateTime DESC)  RowNum, * 
FROM (
select distinct TOP 100 PERCENT * from (
SELECT  distinct   dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.StateID, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, 
                      dbo.tblAddress.Mobile, dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, 
                      dbo.tblAddress.IsRegisterAddress, dbo.tblAddress.IsDeleted,dbo.tblAddress.[countrycode],dbo.tblAddress.[Country], dbo.tblAddress.IsBusiness 
FROM dbo.tblAddress  
) as tadd
where ( tadd.UserID=@UserId )  And tadd.IsRegisterAddress =0 and (tadd.IsDeleted<>1 or tadd.IsDeleted is null) 
		and isnull(tadd.[Country],'') <> '' and isnull(tadd.[countrycode],'') <> ''
						
order by tadd.CreatedDateTime desc
					
) as tblInner 
)	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex * @PageSize) or (@PageIndex=0 and @Pagesize=0)

order by CreatedDateTime desc 
END
GO
