SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_GetUserAddressDetail]
@UserId int,
@ConsignmentType nvarchar(20)=null,
@FirstName nvarchar(50)=null,
@Address nvarchar(max)=null,
@Email nvarchar(50)=null,
@IsPickup nvarchar(10)=null
AS
BEGIN

----------------------------International------------------------------

if(lower(@ConsignmentType) = 'international')
Begin
select distinct TOP 50 * from (
SELECT  distinct   dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb,dbo.tblAddress.StateName, dbo.tblAddress.StateID, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, 
                      dbo.tblAddress.Mobile, dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, 
                      dbo.tblAddress.IsRegisterAddress, dbo.tblAddress.IsDeleted,dbo.tblAddress.[countrycode],dbo.tblAddress.[Country], dbo.tblAddress.IsBusiness 
FROM dbo.tblAddress 
) as tadd
where ( tadd.UserID=@UserId )  --And tadd.IsRegisterAddress = 0 
		and (tadd.IsDeleted<>1 or tadd.IsDeleted is null) 
		and isnull(tadd.[Country],'') <> '' and isnull(tadd.[countrycode],'') <> '' and				
		case when isnull(@FirstName,'') <> '' then  isnull(tadd.FirstName,'') +' '+ isnull(tadd.LastName,'') +' '+ isnull(tadd.CompanyName,'') else '' end like '%'+isnull(@FirstName,'')+'%' 
		and case when isnull(@Address,'') <> '' then  isnull(tadd.Suburb,'') +' '+ isnull(tadd.StateName,'') +' '+ isnull(tadd.PostCode,'') else '' end like '%'+isnull(@Address,'')+'%'
		and case when isnull(@Email,'') <> '' then  isnull(Email,'') else '' end like '%'+isnull(@Email,'')+'%'
order by tadd.CreatedDateTime desc
end

----------------------------domestic-------------------------------------

if(lower(@ConsignmentType) = 'domestic')
begin
if (lower(isnull(@IsPickup,'')) = 'true')
	begin
			select distinct TOP 50 * from (
			SELECT  distinct   dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
								  dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb
								  ,Case when isnull(dbo.tblAddress.StateName,'') = '' and isnull(dbo.tblAddress.StateID,'') <> '' then (select StateCode from tblstate where stateid = dbo.tblAddress.StateID)
								  else dbo.tblAddress.StateName end as StateName								  
								  , dbo.tblAddress.StateID, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, 
								  dbo.tblAddress.Mobile, dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, 
								  dbo.tblAddress.IsRegisterAddress, dbo.tblAddress.IsDeleted,dbo.tblAddress.[countrycode],dbo.tblAddress.[Country], dbo.tblAddress.IsBusiness 
			FROM dbo.tblAddress  
			) as tadd
			where ( tadd.UserID=@UserId )  --And tadd.IsRegisterAddress = 0 
					and (tadd.IsDeleted<>1 or tadd.IsDeleted is null) 
					and isnull(tadd.[Country],'') = '' and isnull(tadd.[countrycode],'') = '' 				
					and	isnull(tadd.Address1,'') +' '+ isnull(tadd.Address2,'') not like '%C/-%' 
					and case when isnull(@FirstName,'') <> '' then  isnull(tadd.FirstName,'') +' '+ isnull(tadd.LastName,'') +' '+ isnull(tadd.CompanyName,'') else '' end like '%'+isnull(@FirstName,'')+'%' 
					and case when isnull(@Address,'') <> '' then  isnull(tadd.Suburb,'') +' '+ isnull(tadd.StateName,'') +' '+ isnull(tadd.PostCode,'') else '' end like '%'+isnull(@Address,'')+'%'
					and case when isnull(@Email,'') <> '' then  isnull(Email,'') else '' end like '%'+isnull(@Email,'')+'%'
			order by tadd.CreatedDateTime desc
	end
else
	begin
			select distinct TOP 50 * from (
			SELECT  distinct   dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
								  dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb,
								  Case when isnull(dbo.tblAddress.StateName,'') = '' and isnull(dbo.tblAddress.StateID,'') <> '' then (select StateCode from tblstate where stateid = dbo.tblAddress.StateID)
								  else dbo.tblAddress.StateName end as StateName	
								  , dbo.tblAddress.StateID, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, 
								  dbo.tblAddress.Mobile, dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, 
								  dbo.tblAddress.IsRegisterAddress, dbo.tblAddress.IsDeleted,dbo.tblAddress.[countrycode],dbo.tblAddress.[Country], dbo.tblAddress.IsBusiness 
			FROM dbo.tblAddress  
			) as tadd
			where ( tadd.UserID=@UserId )  --And tadd.IsRegisterAddress = 0 
					and (tadd.IsDeleted<>1 or tadd.IsDeleted is null) 
					and isnull(tadd.[Country],'') = '' and isnull(tadd.[countrycode],'') = '' and				
					case when isnull(@FirstName,'') <> '' then  isnull(tadd.FirstName,'') +' '+ isnull(tadd.LastName,'') +' '+ isnull(tadd.CompanyName,'') else '' end like '%'+isnull(@FirstName,'')+'%' 
					and case when isnull(@Address,'') <> '' then  isnull(tadd.Suburb,'') +' '+ isnull(tadd.StateName,'') +' '+ isnull(tadd.PostCode,'') else '' end like '%'+isnull(@Address,'')+'%'
					and case when isnull(@Email,'') <> '' then  isnull(Email,'') else '' end like '%'+isnull(@Email,'')+'%'
			order by tadd.CreatedDateTime desc
	end
end
END
GO
