SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_GetUserContactDetailsByUserId_28_07_2017]
--@outParam_out  int  OUTPUT,
@userId int
AS
BEGIN
SELECT     dbo.tblAddress.*, dbo.tblState.StateCode
FROM         dbo.tblAddress INNER JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where [UserID]=@userId and IsRegisterAddress='true' and ISdeleted = 0
                      order by dbo.tblAddress.CreatedDateTime
END
----[SPCPPL_GetUserContactDetailsByUserId] 29
GO
