SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_InsertAPIEnquiry]

@CompanyName nvarchar(200) = null ,
@Address nvarchar(100) = null ,
@Suburb nvarchar(100) = null ,
@StateID int = null ,
@PostCode nvarchar(10) = null ,
@Country nvarchar(50) = null ,
@Website nvarchar(200) = null ,
@AccountNumber nvarchar(50) = null ,
@RequesterFname nvarchar(50) = null ,
@RequesterLname nvarchar(50) = null ,
@TechContactName nvarchar(50) = null ,
@TechContactEmail nvarchar(250) = null ,
@TechContactNumber nvarchar(50) = null ,
@ShoppingCartUsed nvarchar(150) = null ,
@Comments nvarchar(500) = null ,
@RequesterEmail varchar(250) = null ,
@ERPAccounting varchar(100) = null 

AS
BEGIN
INSERT INTO [dbo].[tblAPIEnquiry]
           ([CompanyName]
           ,[Address]
           ,[Suburb]
           ,[StateID]
           ,[PostCode]
           ,[Country]
           ,[Website]
           ,[AccountNumber]
           ,[RequesterFname]
           ,[RequesterLname]
           ,[TechContactName]
           ,[TechContactEmail]
           ,[TechContactNumber]
           ,[ShoppingCartUsed]
           ,[Comments]
	   ,[RequesterEmail]
	   ,[ERPInUse])
     VALUES
           (@CompanyName  ,
		@Address  ,
		@Suburb ,
		@StateID,
		@PostCode ,
		@Country ,
		@Website ,
		@AccountNumber ,
		@RequesterFname ,
		@RequesterLname ,
		@TechContactName ,
		@TechContactEmail ,
		@TechContactNumber ,
		@ShoppingCartUsed ,
		@Comments,
		@RequesterEmail,
		@ERPAccounting )



End
GO
