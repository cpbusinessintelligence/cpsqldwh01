SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SPCPPL_InsertFranchiseCampaign]

@FirstName varchar(50)= null ,

@LastName varchar(50)= null ,

@Suburb varchar(50)= null ,

@State varchar(50)= null ,

@Postcode nvarchar(10)= null ,

@PreferredArea nvarchar(250) = null,

@Location nvarchar(100) = null,

@Email nvarchar(250)= null ,

@Phone nvarchar(20)= null ,

@FranchiseRequirement nvarchar(1000)= null ,

@Comment varchar(500) = null,

@CreatedBy varchar(50) = null



AS

BEGIN

INSERT INTO [dbo].[tblFranchiseCampaign]

           ([FirstName]

           ,[LastName]

           ,[Suburb]

           ,[State]

           ,[Postcode]

	   ,[PreferredArea]

	   ,[Location]

           ,[Email]

           ,[Phone]

           ,[FranchiseRequirement]

           ,[Comment]

	   ,CreatedBy

	   ,CreatedDateTime)

     VALUES

           (@FirstName, 

           @LastName, 

           @Suburb, 

           @State, 

           @Postcode, 

	   @PreferredArea,

	   @Location,

           @Email, 

           @Phone, 

           @FranchiseRequirement, 

           @Comment,

	   @CreatedBy,

	   GetDate())



End
GO
