SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SPCPPL_InsertIRPRefund]

@Branch nvarchar(50)= null ,
@AccountNumber nvarchar(50)= null ,
@FranchiseNumber nvarchar(50)= null ,
@BusinessName varchar(100)= null ,
@FirstName varchar(50)= null ,
@LastName varchar(50)= null ,
@Email varchar(100)= null ,
@Phone nvarchar(20)= null ,
@StreetAddress1 varchar(50)= null ,
@StreetAddress2 varchar(50)= null ,
@Suburb varchar(50)= null ,
@State varchar(50)= null ,
@Postcode varchar(50)= null ,
@BSBNumber varchar(10)= null ,
@BankAccountNumber nvarchar(20)= null ,
@AccountName varchar(50)= null ,
@RemittanceEmail varchar(100)= null ,
@BookNo varchar(max)= null ,
@CreatedBy varchar(50)= null

AS
BEGIN
INSERT INTO [dbo].[tblIRPRefund]
           ([Branch]
           ,[AccountNumber]
           ,[FranchiseNumber]
           ,[BusinessName]
           ,[FirstName]
           ,[LastName]
           ,[Email]
           ,[Phone]
           ,[StreetAddress1]
           ,[StreetAddress2]
           ,[Suburb]
           ,[State]
           ,[Postcode]
           ,[BSBNumber]
           ,[BankAccountNumber]
           ,[AccountName]
           ,[RemittanceEmail]
           ,[BookNo]
           ,[CreatedDateTime]
           ,[CreatedBy])
     VALUES
           (@Branch,
           @AccountNumber, 
           @FranchiseNumber, 
           @BusinessName, 
           @FirstName, 
           @LastName, 
           @Email, 
           @Phone, 
           @StreetAddress1, 
           @StreetAddress2, 
           @Suburb, 
           @State, 
           @Postcode, 
           @BSBNumber, 
           @BankAccountNumber, 
           @AccountName, 
           @RemittanceEmail, 
           @BookNo, 
           getdate(), 
           @CreatedBy)


End
GO
