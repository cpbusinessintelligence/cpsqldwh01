SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SPCPPL_LogError]
@Error nvarchar(max) = null,
@FunctionInfo  nvarchar(max) = null,
 @ClientId  nvarchar(max) = null,
 @URL  nvarchar(max) = null,
@Request  nvarchar(max) = null

AS
BEGIN
INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[URL]
           ,[Request]
           ,[ClientId])
     VALUES
           (@Error
           ,@FunctionInfo
           ,@URL
           ,@Request
           , @ClientId)

END
GO
