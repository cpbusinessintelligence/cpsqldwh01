SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_RedeliveryConsignmentSearchITAdmin]
@RedeliveryConsignmentCode varchar(max)=null,
@Sender varchar(max)=null,
@Receiver varchar(max)=null,
@RedeliveryConsignmentFrom date=null,
@RedeliveryConsignmentTo date=null,
@RedeliveryType varchar(max)=null

AS
BEGIN
 

SELECT top 100   
CASE WHEN  (addPikup.Country    LIKE '%'+ @Sender +'%' OR addDest.Country    LIKE '%'+ @Receiver +'%' )  then 1 
WHEN  (addPikup.StateID    LIKE '%'+ @Sender +'%'  or         addDest.StateID    LIKE '%'+ @Receiver +'%' )then 2 
WHEN  (addPikup.StateName    LIKE '%'+ @Sender +'%'  or   addDest.StateName    LIKE '%'+ @Receiver +'%' )then 2 
WHEN  (addPikup.PostCode    LIKE '%'+ @Sender +'%'  or   addDest.PostCode    LIKE '%'+ @Receiver +'%') then 3
WHEN  (addPikup.Suburb    LIKE '%'+ @Sender +'%'  or   addDest.Suburb    LIKE '%'+ @Receiver +'%') then 4
else 5 end as PikupOrder , Red.*, FORMAT(Red.CreatedDateTime,'dd/MM/yyyy HH:mm') AS CreatedDateTime1,
addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, addDest.Country AS DestCountry, addDest.StateName AS DestStateName, addDest.CountryCode AS DestCountryCode, 
addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName,  addPikup.Suburb AS PickSuburab, addPikup.Country AS PickCountry,  addPikup.StateName AS PickStateName, addDest.CountryCode AS PickCountryCode, 
addPikup.PostCode AS PickPostCode,
addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate,consign.*
FROM dbo.tblRedelivery Red  with (nolock) 
Left Join TblRedeliveryConsignment consign on consign.ConsignmentCode = Red.ConsignmentCode
INNER JOIN dbo.tblAddress AS addPikup with (nolock)  ON consign.PickupAddressID = addPikup.AddressID 
INNER JOIN dbo.tblAddress AS addDest  with (nolock) ON consign.NewDeliveryAddressID = addDest.AddressID 
INNER JOIN dbo.tblAddress AS addContct  with (nolock) ON consign.CurrentDeliveryAddressID = addContct.AddressID 
LEFT OUTER JOIN dbo.tblStatus with (nolock) ON   consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     
(Red.IsProcessed is null or Red.IsProcessed =1) and
(
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateName  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateID  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Suburb  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.PostCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Country  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CountryCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' ) and
(
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateName  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateID  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Suburb  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.PostCode  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Country  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CountryCode  END LIKE '%'+isnull(@Sender,'')+'%' or
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' ) and
( CASE WHEN @RedeliveryType IS null THEN isnull(@RedeliveryType,'') else Red.RedeliveryType   END LIKE '%'+isnull(@RedeliveryType,'')+'%')and
(CASE WHEN @RedeliveryConsignmentFrom IS null THEN CAST( Red.CreatedDateTime as DATE) else @RedeliveryConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)) and
(CASE WHEN @RedeliveryConsignmentTo IS null THEN CAST( Red.CreatedDateTime as DATE) else @RedeliveryConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)) and
(CASE WHEN @RedeliveryConsignmentCode IS null THEN isnull(@RedeliveryConsignmentCode,'') else Red.ConsignmentCode  END LIKE '%'+isnull(@RedeliveryConsignmentCode,'')+'%' )
Order by Red.CreatedDateTime DESC
END
GO
