SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_SalesOrderDetail]

         
           @PhysicalWeight decimal(4,2)=null,
     
           @DeclareVolume decimal(4,2)=null,
      
           @CreatedBy int=null,
           
           
           @SalesOrderID int =null,
           @Description  varchar(50)=null,
           @LineNo  int=null,
           @FreightCharge decimal(19,4)=null,
           @FuelCharge decimal(19,4)=null,
           @InsuranceCharge  decimal(19,4)=null,
           @ServiceCharge  decimal(19,4)=null
AS
BEGIN

           
           
           
           
           INSERT INTO [dbo].[tblSalesOrderDetail]
           ([SalesOrderID]
           ,[Description]
           ,[LineNo]
           ,[Weight]
           ,[Volume]
           ,[FreightCharge]
           ,[FuelCharge]
           ,[InsuranceCharge]
           ,[ServiceCharge]
           ,[Total]
           ,[CreatedDateTime]
           ,[CreatedBy])
     VALUES
           (@SalesOrderID
           ,@Description
           ,@LineNo
           ,@PhysicalWeight
           ,@DeclareVolume
           ,@FreightCharge
           ,@FuelCharge
           ,@InsuranceCharge 
           ,@ServiceCharge
           ,cast (@FreightCharge as decimal(19,4)) +cast (@FuelCharge as decimal(19,4))+cast (@InsuranceCharge as decimal(19,4))+cast (@ServiceCharge as decimal(19,4))
           ,getdate()
           ,@CreatedBy )

END
GO
