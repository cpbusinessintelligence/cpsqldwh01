SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_SaveAddressDetails]
@outParam_out  int  OUTPUT,
@userId int,
@firstName nvarchar(50),
@lastName nvarchar(50),
@companyName nvarchar(100)=null,
@email nvarchar(250),
@address1 nvarchar(200),
@address2 nvarchar(200)=null,
@suburb nvarchar(100),
@stateId int,
@postalCode int,
@phone nvarchar(20),
@mobile int=null,
@createdBy int=null,
@updateBy int=null,
@isRegisterAddress bit=false

AS
BEGIN
--- IF Begin for Check user exist or not
IF EXISTS (SELECT UserID FROM  [dbo].[tblAddress] WHERE UserID = @userId)
Begin
 UPDATE [dbo].[tblAddress] SET [FirstName]=@firstName,[LastName]=@lastName,[CompanyName]=@companyName,
 [Email]=@email ,[Address1]=@address1,[Address2]=@address2,[Suburb]=@suburb,[StateID]=@stateId,[PostCode]=@postalCode,
 [Phone]=@phone,[Mobile]=@mobile,[UpdatedDateTime]=GETDATE(),[UpdatedBy]=@updateBy
Where UserID=@UserID and IsRegisterAddress='true' 
End
----==== Update if user exist
Else
Begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy] )
	 VALUES
      ( @userId,@firstName ,@lastName ,@companyName,@email,@address1,@address2,@suburb,@stateId,
      @postalCode,@phone,@mobile,@isRegisterAddress,GETDATE(), @createdBy )
        set @outParam_out =@@Identity;   
End
END
GO
