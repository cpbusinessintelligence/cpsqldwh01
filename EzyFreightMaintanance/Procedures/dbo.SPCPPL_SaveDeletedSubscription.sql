SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_SaveDeletedSubscription]

@PaymentRefNo  varchar(100) = null,
@AuthorizationCode  varchar(100) = null,
@MerchantReferenceCode  varchar(150) = null,
@SubscriptionID  varchar(150) = null,
@NewRequestID  varchar(150) = null,
@Message  varchar(max) = null,
@CreatedBY varchar(50) = null


As 
Begin
begin tran
BEGIN TRY
INSERT INTO [dbo].[tblDeletedSubscription]
           ([PaymentRefNo]
           ,[AuthorizationCode]
           ,[MerchantReferenceCode]
           ,[SubscriptionID]
           ,[NewRequestID]
           ,[Message]
           ,[CreatedBY])
     values(@PaymentRefNo ,
           @AuthorizationCode ,
           @MerchantReferenceCode ,
           @SubscriptionID ,
           @NewRequestID ,
           @Message ,
           @CreatedBY)

select scope_identity() as DeletedSubId

commit tran          
END TRY
BEGIN CATCH
begin
rollback tran
INSERT INTO [dbo].[tblErrorLog]
		   ([Error]
			,[FunctionInfo]
			,[ClientId])
VALUES
            (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
		    ,'[SPCPPL_SaveDeletedSubscription]', 2)

end

END CATCH 
End

GO
