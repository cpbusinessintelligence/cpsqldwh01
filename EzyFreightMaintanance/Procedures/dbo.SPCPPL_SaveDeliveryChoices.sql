SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_SaveDeliveryChoices]
@DtDeliveryChoice DtDeliveryChoices Readonly

As Begin 
Begin Try

INSERT INTO [dbo].[vw_DeliveryChoices]
           ([Category]
           ,[DeliveryChoiceID]
           ,[Sortingcode]
           ,[DeliveryChoiceName]
           ,[DeliveryChoiceDescription]
           ,[Operation Hours]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[Suburb]
           ,[PostCode]
           ,[State]
           ,[Country]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[Latitude]
           ,[Longtitude]
		   ,[IsRedeliveryAvailable])
            (select [Category] ,	[DeliveryChoiceID] ,	[Sortingcode] ,	[DeliveryChoiceName] ,	[DeliveryChoiceDescription] ,
			[OperationHours] ,	[Address1] ,	[Address2] ,	[Address3] ,	[Suburb] ,	[PostCode] ,	[State] ,[Country], Getdate(), [CreatedBy] 
			,[Latitude] ,[Longtitude],[IsRedeliveryAvailable] 
			From @DtDeliveryChoice)

select scope_identity() as Id

select * From @DtDeliveryChoice

END TRY
		BEGIN CATCH
		begin
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SaveDeliveryChoices', 2)
		end
		END CATCH

END 
GO
