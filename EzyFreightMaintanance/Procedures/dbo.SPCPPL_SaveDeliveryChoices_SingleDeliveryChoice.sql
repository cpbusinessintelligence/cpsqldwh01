SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SPCPPL_SaveDeliveryChoices_SingleDeliveryChoice]
@DtDeliveryChoice DtDeliveryChoicesList Readonly,
@DtBeatIdList [DtBeatIds] Readonly,
@CreatedBy int

As Begin 
Begin Try
Declare @DeliveryChoiceId nvarchar(50)=''

select @DeliveryChoiceId = [DeliveryChoiceID] From @DtDeliveryChoice

INSERT INTO [dbo].[vw_DeliveryChoices]
           ([Category]
           ,[DeliveryChoiceID]
           ,[Sortingcode]
           ,[DeliveryChoiceName]
           ,[DeliveryChoiceDescription]
           ,[Operation Hours]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[Suburb]
           ,[PostCode]
           ,[State]
           ,[Country]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[Latitude]
           ,[Longtitude]
		   ,[IsRedeliveryAvailable])
            (select [Category] ,	[DeliveryChoiceID] ,	[Sortingcode] ,	[DeliveryChoiceName] ,	[DeliveryChoiceDescription] ,
			[OperationHours] ,	[Address1] ,	[Address2] ,	[Address3] ,	[Suburb] ,	[PostCode] ,	[State] ,[Country], Getdate(), @CreatedBy 
			,[Latitude] ,[Longtitude],[IsRedeliveryAvailable] 
			From @DtDeliveryChoice)

select scope_identity() as Id

INSERT INTO [dbo].[vw_DeliveryChoicesBeat]
           ([BeatID]
		   ,[DeliveryChoiceID]
           ,[IsProcess]
           ,[CreatedDateTime]
           ,[CreatedBy])
		(select Upper([BeatIds]), @DeliveryChoiceId , 0, getdate(),@CreatedBy From @DtBeatIdList)

select * From @DtDeliveryChoice

END TRY
		BEGIN CATCH
		begin
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_SaveDeliveryChoices_SingleDeliveryChoice]', 2)
		end
		END CATCH

END 
GO
