SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_SaveReportSubscription]
@Category varchar(20)= null,
@ReportName varchar(100)= null,
@EmailTo varchar(8000) = null,
@EmailCC varchar(8000)= null,
@AccountNo varchar(100)

As
Begin

Declare @MaxSerialNo int 

SELECT @MaxSerialNo = max(isnull([serialnumber],0))+1 FROM [Subscriptions]


INSERT INTO [dbo].[Subscriptions]
           ([Category]
           ,[Reportname]
           ,[To]
           ,[CC]
           ,[BCC]
           ,[Parameter1]
		   ,[Parameter2]
		   ,[Parameter3]
		   ,[Parameter4]
		   ,[Parameter5]
           ,[IncludeReport]
           ,[IncludeLink]
           ,[RenderFormat]
           ,[shouldsendreport]
           ,[isactive]
           ,[sendorder]
           ,[serialnumber]
           ,[Subject]
           ,[isconsolidate])
     VALUES
           (@Category
           ,@Reportname
           ,@EmailTo
           ,@EmailCC
		   ,'Cpreporting@couriersplease.com.au'
           ,@AccountNo
		   ,''
		   ,''
		   ,''
		   ,''
           ,'TRUE'
           ,'FALSE'
           ,'Excel'
           ,1
           ,1
           ,1
           ,@MaxSerialNo
           ,''
           ,0)


select @MaxSerialNo as ID
end
GO
