SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_SaveRequestResponse]
@ConsignmentCode varchar(50) = null,
@VendorName varchar(50) = null,
@CPPLServiceName varchar(50) = null,
@CPPLServiceEndPoint varchar(max) = null,
@CPPLServiceRequest varchar(max) = null,
@VendorServiceName varchar(50) = null,
@VendorServiceEndPoint  varchar(max) = null,
@VendorServiceRequest varchar(max) = null,
@VendorServiceResponse varchar(max) = null,
@CPPLServiceResponse varchar(max) = null,
@CreatedBy varchar(50) = null

as 
Begin
Begin Try
INSERT INTO [dbo].[tblServiceRecords]
           ([ConsignmentCode],
		   [VendorName],
		   [CPPLServiceName],
		   [CPPLServiceEndPoint],
		   [CPPLServiceRequest],
		   [CPPLServiceResponse],
		   [VendorServiceName],
		   [VendorServiceEndPoint],
		   [VendorServiceRequest],
		   [VendorServiceResponse],
		   [CreatedDateTime],
		   [CreatedBy])
     VALUES
           (@ConsignmentCode,
			@VendorName,
			@CPPLServiceName,
			@CPPLServiceEndPoint,
			@CPPLServiceRequest,
			@CPPLServiceResponse,
			@VendorServiceName,
			@VendorServiceEndPoint,
			@VendorServiceRequest,
			@VendorServiceResponse,
			getdate(),
			@CreatedBy)

END TRY
Begin Catch

		INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
					,[FunctionInfo]
					,[ClientId])
		VALUES
					(cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
					,'SPCPPL_SaveRequestResponse', 2)


End Catch
End
GO
