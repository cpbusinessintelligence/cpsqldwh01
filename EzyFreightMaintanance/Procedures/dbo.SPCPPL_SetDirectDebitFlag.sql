SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_SetDirectDebitFlag]

@AccountNo varchar(50) = null 

AS
BEGIN
BEGIN TRY
---------------------Update direct debit flag ------------------------------------------------------

update tblcompany set IsDirectDebitDone = 1 where accountnumber = @AccountNo

----------------------------------------------------------------------------------------------------
END TRY
BEGIN CATCH
begin
INSERT INTO [dbo].[tblErrorLog]
		   ([Error]
			,[FunctionInfo]
			,[ClientId])
VALUES
            (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
		    ,'SPCPPL_SetDirectDebitFlag', 2)

end
END CATCH  
End
GO
