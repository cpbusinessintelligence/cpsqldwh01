SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateAddressDetails]

@AddressId int=null,
@userId       int=null,
@firstName     nvarchar(50)=null,
@lastName      nvarchar(50)=null,
@companyName   nvarchar(100)=null,
@email         nvarchar(250)=null,
@address1      nvarchar(200)=null,
@address2      nvarchar(200)=null,
@suburb       nvarchar(100)=null,
@stateId      nvarchar(max)=null,
@postalCode   int=null,
@phone        nvarchar(20)=null,
@mobile       nvarchar(20)=null,
@isBusiness bit=null,
@UpdatedBy    int=null,
@isRegisterAddress bit=null,
@IsSubscribe bit=null


AS
BEGIN
DECLARE @stateIdbyCode int

SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @stateId
  
if(@AddressId is null)
Begin
    UPDATE [dbo].[tblAddress] SET 
				 [FirstName]=case when @firstName IS null then [FirstName] else @firstName end,
				 [LastName]= case when @lastName IS null then [LastName] else @lastName end,
				 [CompanyName]=case when @companyName IS null then [CompanyName] else @companyName end,
				 [Email]= case when @email IS null then [Email] else @email end,
				 [Address1]= case when @address1 IS null then [Address1] else @address1 end,
				 [Address2]=  case when @address2 IS null then [Address2] else @address2 end,
				 [Suburb]= case when @suburb IS null then [Suburb] else @suburb end,
				 [StateID]= case when @stateIdbyCode IS null then [StateID] else @stateIdbyCode end,
				 [PostCode]= case when @postalCode IS null then [PostCode] else @postalCode end,
				 [Phone]= case when @phone IS null then [Phone] else @phone end,
				 [Mobile]= case when @mobile IS null then [Mobile] else @mobile end,
				 [isBusiness]= case when @isBusiness IS null then [isBusiness] else @isBusiness end,
				 [IsRegisterAddress] = case when @isRegisterAddress IS null then [IsRegisterAddress] else @isRegisterAddress end,
				 [IsSubscribe] = case when @IsSubscribe IS null then [IsSubscribe] else @IsSubscribe end,
				 [UpdatedDateTime]=GETDATE(),
				 [UpdatedBy]=@UpdatedBy
		Where UserID=@UserID and IsRegisterAddress=1
end
else
Begin
    UPDATE [dbo].[tblAddress] SET 
				 [FirstName]=case when @firstName IS null then [FirstName] else @firstName end,
				 [LastName]= case when @lastName IS null then [LastName] else @lastName end,
				 [CompanyName]=case when @companyName IS null then [CompanyName] else @companyName end,
				 [Email]= case when @email IS null then [Email] else @email end,
				 [Address1]= case when @address1 IS null then [Address1] else @address1 end,
				 [Address2]=  case when @address2 IS null then [Address2] else @address2 end,
				 [Suburb]= case when @suburb IS null then [Suburb] else @suburb end,
				 [StateID]= case when @stateIdbyCode IS null then [StateID] else @stateIdbyCode end,
				 [PostCode]= case when @postalCode IS null then [PostCode] else @postalCode end,
				 [Phone]= case when @phone IS null then [Phone] else @phone end,
				 [Mobile]= case when @mobile IS null then [Mobile] else @mobile end,
				 [isBusiness]= case when @isBusiness IS null then [isBusiness] else @isBusiness end,
				 [IsRegisterAddress] = case when @isRegisterAddress IS null then [IsRegisterAddress] else @isRegisterAddress end,
				 [IsSubscribe] = case when @IsSubscribe IS null then [IsSubscribe] else @IsSubscribe end,
				 [UpdatedDateTime]=GETDATE(),
				 [UpdatedBy]=@UpdatedBy
		Where AddressID=@AddressId 
end
-- exec [SPCPPL_UpdateAddressDetails] @userId=40,@UpdatedBy=1,@mobile=9179810979,@phone=9179810979,@postalCode=123,@stateId=1,@suburb=11,@address2=11,@address1=11,@email=11,@companyName=11,@lastName=11,@firstName=11

END
-- exec [SPCPPL_UpdateAddressDetails] l
--ALTER PROCEDURE [dbo].[SPCPPL_UpdateAddressDetails]

--@userId       int=52,
--@firstName     nvarchar(50)='frrr',
--@lastName      nvarchar(50)='yy',
--@companyName   nvarchar(100)='cc',
--@email         nvarchar(250)='ema',
--@address1      nvarchar(200)='add1',
--@address2      nvarchar(200)='aa21',
--@suburb       nvarchar(100)='1',
--@stateId      int=1,
--@postalCode   int=11,
--@phone        int=1234567895,
--@mobile       int=1111,
--@UpdatedBy    int=40
GO
