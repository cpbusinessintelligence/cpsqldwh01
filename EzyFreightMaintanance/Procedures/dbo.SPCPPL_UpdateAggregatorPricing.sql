SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_UpdateAggregatorPricing]
@CompanyID int,
@UpdatedBy int,
@IsAggregatorPricing bit = 0

As Begin

Update TblCompany set IsAggregatorPricing = @IsAggregatorPricing , UpdatedDateTime = Getdate() ,UpdatedBy = @UpdatedBy where CompanyID = @CompanyID

End
GO
