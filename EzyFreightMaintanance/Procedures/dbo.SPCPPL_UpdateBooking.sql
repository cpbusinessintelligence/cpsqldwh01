SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[SPCPPL_UpdateBooking]
           @BookingID int = null ,
           @PhoneNumber varchar(50) = null ,
           @ContactName varchar(50) = null ,
           @ContactEmail varchar(250) = null ,
           @PickupDate date = null ,
           @PickupTime varchar(10) = null ,
           @OrderCoupons bit = null ,
           @PickupFromCustomer bit = null ,
           @PickupName varchar(100) = null ,
           @PickupAddress1 varchar(250) = null ,
           @PickupAddress2 varchar(250) = null ,
           @PickupSuburb varchar(50) = null ,
           @ContactDetails varchar(50) = null ,
           @PickupFrom varchar(50) = null ,
           @IsProcessed bit = null ,
           @BookingRefNo varchar(50) = null ,
        
           @UpdatedBy int = null

           
AS
BEGIN

UPDATE [dbo].[tblBooking]
   SET [PhoneNumber] =case when @PhoneNumber is null then [PhoneNumber] else @PhoneNumber end ,
      [ContactName] = case when @ContactName is null  then [ContactName] else @ContactName end ,
      [ContactEmail] = case when @ContactEmail is null  then [ContactEmail] else @ContactEmail end ,
      [PickupDate] =case when  @PickupDate is null  then [PickupDate] else @PickupDate end ,
      [PickupTime] =case when   @PickupTime is null  then [PickupTime] else @PickupTime end ,
      [OrderCoupons] =case when   @OrderCoupons is null  then [OrderCoupons] else @OrderCoupons end ,
      [PickupFromCustomer] = case when  @PickupFromCustomer is null  then [PickupFromCustomer] else @PickupFromCustomer end ,
      [PickupName] = case when  @PickupName  is null then [PickupName] else @PickupName end ,
      [PickupAddress1] =case when   @PickupAddress1  is null then [PickupAddress1] else @PickupAddress1 end ,
      [PickupAddress2] =case when   @PickupAddress2  is null then [PickupAddress2] else @PickupAddress2 end ,
      [PickupSuburb] =case when   @PickupSuburb  is null then [PickupSuburb] else @PickupSuburb end ,
      [ContactDetails] =case when   @ContactDetails  is null then [ContactDetails] else @ContactDetails end ,
      [PickupFrom] =case when   @PickupFrom  is null then [PickupFrom] else @PickupFrom end ,
      [IsProcessed] =case when   @IsProcessed  is null then [IsProcessed] else @IsProcessed end ,
      [BookingRefNo] = case when  @BookingRefNo  is null then [BookingRefNo] else @BookingRefNo end ,
      [UpdatedDateTime] = getDate(),
      [UpdatedBy] = @UpdatedBy
 WHERE BookingID = @BookingID
				
End
GO
