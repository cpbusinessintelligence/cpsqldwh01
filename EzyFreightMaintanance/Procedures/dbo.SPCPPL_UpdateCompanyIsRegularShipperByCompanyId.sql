SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SPCPPL_UpdateCompanyIsRegularShipperByCompanyId]
@CompanyId          int = null ,
 
@IsRegularShipper   bit = null ,
@UserId    int = null 

           
AS
BEGIN

if exists(select * from [tblCompany] where 
[CompanyId] = @CompanyId
and [ShipperCreatedBy] is null
)
begin
UPDATE [dbo].[tblCompany]
   SET          
      
       [IsRegularShipper] =  @IsRegularShipper  
      ,[ShipperCreatedBy] =   @UserId   
      
      ,[ShipperCreatedDateTime]= GETDATE()
 WHERE [CompanyId] =          @CompanyId
 end
 else
 begin
           
 UPDATE [dbo].[tblCompany]
   SET          
      
       [IsRegularShipper] =  @IsRegularShipper  
      ,[ShipperUpdatedBy] =   @UserId   
      
      ,[ShipperUpdatedDateTime]= GETDATE()
 WHERE [CompanyId] =          @CompanyId
 end
END
GO
