SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SPCPPL_UpdateCompanyName]
@CompanyID           int = null ,
@UpdatedBy           int = null ,
 
 
@CompanyName    nvarchar(max) = null  

           
AS
BEGIN
UPDATE [dbo].[tblCompany]
   SET          
      
       [CompanyName] =  @CompanyName
   
      ,[UpdatedBy]  = @UpdatedBy
      ,[UpdatedDateTime]=GETDATE()
 WHERE [CompanyID] =          @CompanyID
 
            
END
GO
