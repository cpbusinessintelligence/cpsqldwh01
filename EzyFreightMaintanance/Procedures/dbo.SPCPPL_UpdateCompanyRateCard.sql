SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SPCPPL_UpdateCompanyRateCard]
@CompanyID           int = null ,
@UpdatedBy           int = null ,
 
 
@RateCardCategory    nvarchar(max) = null ,
@IntRateCardCategory  nvarchar(max) = null 

           
AS
BEGIN
UPDATE [dbo].[tblCompany]
   SET          
      
       [RateCardCategory] =  @RateCardCategory
      ,[IntRateCardCategory] =   @IntRateCardCategory
      ,[UpdatedBy]  = @UpdatedBy
      ,[UpdatedDateTime]=GETDATE()
 WHERE [CompanyID] =          @CompanyID
 
            
END
GO
