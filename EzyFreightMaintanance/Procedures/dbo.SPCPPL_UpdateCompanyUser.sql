SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateCompanyUser]

@UserID           int = null ,
@IsUserDisabled   bit = null ,
@ReasonSubject    nvarchar(max) = null ,
@ReasonDesciption  nvarchar(max) = null, 
@UpdatedBy int= null
           
AS
BEGIN

declare @CompanyID int 
select @CompanyID =   CompanyID    from tblCompanyUsers where UserID =  @UserID

UPDATE [dbo].[tblCompanyUsers]
   SET          
      
       [IsUserDisabled] =  @IsUserDisabled  
      ,[ReasonSubject] = case when  @ReasonSubject   IS null then [ReasonSubject] else @ReasonSubject end
      ,[ReasonDesciption]=case when  @ReasonDesciption IS null then [ReasonDesciption] else @ReasonDesciption end
      ,UpdatedBy=@UpdatedBy ,[UpdatedDateTime]=GETDATE()
 WHERE [UserID] =          @UserID 
 

 update   [dbo].[tblCompany] set  UpdatedBy = @UpdatedBy,UpdatedDateTime = getdate() where CompanyID  = @CompanyID

-- declare @CompanyID int
--  select top 1 @CompanyID = CompanyID from [dbo].[tblCompanyUsers] where [UserID] = @UserID
 
--INSERT INTO  [dbo].[tblCompanyStatusLog]
--           ([CompanyID]
--           ,[CompanyStatus]
--           ,[StatusReason]
--           ,[MoreDetails]
--           ,[CreatedDateTime]
--           ,[CreatedBy]
--           )
--     VALUES
--           ( @CompanyID 
--           ,@IsUserDisabled
--           ,@ReasonSubject
--           ,@ReasonDesciption
--           ,getdate()
--           ,@UserID
--           )
            
END
GO
