SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[SPCPPL_UpdateCompanyUserAccountByCompanyIdNoWithStatus]
        
@CreatedBy int = null,
@CompanyId int = null,
@ReasonSubject    nvarchar (max)=null,
@ReasonDesciption nvarchar (max)=null
           
      
AS
BEGIN
begin tran

 
			
				DECLARE @CompanyIDret int
		
 
               
 
 
 update tblCompanyUsers 
 set IsUserDisabled = 0,
   [UpdatedBy]=@CreatedBy,
           [UpdatedDateTime] = GETDATE()
 where CompanyId =  @CompanyId
 
 print @companyid


                declare @AccountNumber9Digit nvarchar(10)
 select @AccountNumber9Digit = @CompanyID
SELECT @AccountNumber9Digit =  RIGHT('00000000' + replace(@AccountNumber9Digit,'-',''), 8)
if(@AccountNumber9Digit is null)
set @AccountNumber9Digit = '00000000'
set @AccountNumber9Digit = 'WD'+@AccountNumber9Digit
 
 
 SELECT @AccountNumber9Digit
            	update   [dbo].[tblCompany] set
           [AccountNumber]= @AccountNumber9Digit ,
           [UpdatedBy]=@CreatedBy,
           [UpdatedDateTime] = GETDATE()
           where CompanyID  = @CompanyID
			
			
 
 
 INSERT INTO  [dbo].[tblCompanyStatusLog]
           ([CompanyID]
           ,[CompanyStatus]
           ,[StatusReason]
           ,[MoreDetails]
           ,[CreatedDateTime]
           ,[CreatedBy]
            )
     VALUES
           (@CompanyID
           ,1
           ,@ReasonSubject   
           ,@ReasonDesciption 
           ,getdate()
           ,@CreatedBy
            )
 
 
 
 
		 
			 if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateCompanyUserAccountN]'
    , 2)
  end
  else
  commit tran          
            

END


GO
