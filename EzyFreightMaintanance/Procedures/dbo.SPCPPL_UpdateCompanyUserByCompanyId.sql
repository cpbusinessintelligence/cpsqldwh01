SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateCompanyUserByCompanyId]

@CompanyId          int = null ,
@IsUserDisabled   bit = null ,
@ReasonSubject    nvarchar(max) = null ,
@ReasonDesciption  nvarchar(max) = null, 
@UserId int = null

           

AS

BEGIN

update   [dbo].[tblCompany] set  UpdatedBy = @UserId,UpdatedDateTime = getdate() where CompanyID  = @CompanyID

UPDATE [dbo].[tblCompanyUsers]

   SET          

      

       [IsUserDisabled] =  @IsUserDisabled  

      ,[ReasonSubject] =   @ReasonSubject   

      ,[ReasonDesciption]= @ReasonDesciption

      ,[UpdatedBy] = @UserId

      ,[UpdatedDateTime]= GETDATE()

 WHERE [CompanyId] =          @CompanyId

 

              



 

INSERT INTO  [dbo].[tblCompanyStatusLog]

           ([CompanyID]

           ,[CompanyStatus]

           ,[StatusReason]

           ,[MoreDetails]

           ,[CreatedDateTime]

           ,[CreatedBy]

            )

     VALUES

           ( @CompanyID 

           ,~@IsUserDisabled

           ,@ReasonSubject

           ,@ReasonDesciption

           ,getdate()

           ,@UserID

           )

END
GO
