SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateConsignmentPayment]
      
           @ConsignmentStagingId nvarchar(100)=null,
           @ConsignmentId int=null,
           @UpdatedBy int=null,
           @PaymentRefNo nvarchar(100)=null,
           @merchantReferenceCode nvarchar(max)=null,
           @SubscriptionID nvarchar(max)=null,
           @AuthorizationCode nvarchar(100)=null
           
AS
  
BEGIN
BEGIN TRY


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
begin tran


 ---------------------------Commented BY Shubham 12/05/2016------------------------------------------

	--		UPDATE dbo.tblConsignment
 --  SET [IsProcessed] =1,
 --     [UpdatedDateTTime] = getDate(),
 --     [UpdatedBy] = @UpdatedBy
     
 --WHERE ConsignmentID = @ConsignmentId
 -----------------------------------------------------------------------------------------------------

 	UPDATE dbo.[tblInvoice]
   SET [PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
      
 WHERE invoicenumber = (select top 1 InvoiceNo from dbo.tblSalesOrder where ReferenceNo=@ConsignmentId)


-------------------------Update Consignment Staging------------------------------------------------
 if (isnull(@ConsignmentStagingId,'') <> '')
 begin
  	UPDATE dbo.tblConsignmentstaging
 	set [IsProcessed] =1,[ConsignmentId]=@ConsignmentId,
			[PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
      where 
      --ConsignmentId = @ConsignmentId					---Commented by shubham 12/05/2016--
	  ConsignmentStagingId = @ConsignmentStagingId

end
else
 begin
 	UPDATE dbo.tblConsignmentstaging
 	set [IsProcessed] =1,[ConsignmentId]=@ConsignmentId,
[PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
      where 
      ConsignmentId = @ConsignmentId					---Commented by shubham 12/05/2016--
	  --ConsignmentStagingId = @ConsignmentStagingId

end

-----------------------------------------------------------------------------------------------------
      
declare @DestinationID int ,
@ContactID int ,
@PicupID int 
   
--SELECT     dbo.tblInvoice.*
--FROM         dbo.tblConsignment INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo INNER JOIN
--                      dbo.tblInvoice ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
                      
--                       where dbo.tblConsignment.ConsignmentID = @ConsignmentID
           
           SELECT     dbo.tblConsignment.*, dbo.tblConsignment.[Country-ServiceArea-FacilityCode] as Country_ServiceArea_FacilityCode
FROM         dbo.tblConsignment with(nolock) where dbo.tblConsignment.ConsignmentID = @ConsignmentID


--SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*
--FROM         dbo.tblSalesOrderDetail INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
--WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID)

				


			   SELECT   @PicupID = dbo.tblConsignment.PickupID,
			   @DestinationID=dbo.tblConsignment.DestinationID,
			   @ContactID=dbo.tblConsignment.ContactID
FROM         dbo.tblConsignment  with(nolock) where dbo.tblConsignment.ConsignmentID = @ConsignmentID




            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress  with(nolock) left JOIN
                      dbo.tblState  with(nolock) ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe,case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress  with(nolock) left JOIN
                      dbo.tblState  with(nolock) ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID
 
 



      select * from  [dbo].[tblItemLabel]  with(nolock) where ConsignmentID = @ConsignmentId


SELECT     Cu.UserID, c.IsRegularShipper
FROM         tblCompany c INNER JOIN
                      tblCompanyUsers Cu ON c.CompanyID = Cu.CompanyID
WHERE     (c.IsRegularShipper = 1)
and  Cu.UserID = @UpdatedBy



 COMMIT TRAN 

		END TRY
		BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SPCPPL_UpdateConsignmentPayment'
				   , 2)
		end
		END CATCH
  END 
GO
