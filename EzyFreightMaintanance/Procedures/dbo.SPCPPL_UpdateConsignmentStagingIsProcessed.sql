SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE  PROCEDURE [dbo].[SPCPPL_UpdateConsignmentStagingIsProcessed]
           @ConsignmentStagingID int = null 
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

UPDATE dbo.tblConsignmentStaging
   SET [IsProcessed] =1,
      [UpdatedDateTTime] = getDate(),
      [UpdatedBy] = CreatedBy
 WHERE ConsignmentStagingID = @ConsignmentStagingID
				
End



GO
