SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_UpdateDeliveryChoices]
@ID int,
@Category varchar(20)= null ,
@DeliveryChoiceID varchar(50)= null ,
@Sortingcode varchar(50)= null ,
@DeliveryChoiceName varchar(100)= null ,
@DeliveryChoiceDescription varchar(max)= null ,
@OperationHours nvarchar(max)= null ,
@Address1 varchar(max)= null ,
@Address2 varchar(max)= null ,
@Address3 varchar(max)= null ,
@Suburb varchar(50)= null ,
@PostCode varchar(50)= null ,
@State varchar(55)= null ,
@Country varchar(55)= null ,
@UpdatedBy int= null ,
@Latitude nvarchar(max)= null ,
@Longtitude nvarchar(max)= null ,
@IsRedeliveryAvailable bit= 0,
@DtDeletedBeatIdList [DtBeatIds] Readonly,
@DtNewAddedBeatIdList [DtBeatIds] Readonly

As Begin 
Begin Try

update [dbo].[vw_DeliveryChoices] set 
		   [Category] = @Category
           ,[DeliveryChoiceID] = @DeliveryChoiceID
           ,[Sortingcode] = @Sortingcode
           ,[DeliveryChoiceName] = @DeliveryChoiceName
           ,[DeliveryChoiceDescription] = @DeliveryChoiceDescription
           ,[Operation Hours] = @OperationHours
           ,[Address1] = @Address1
           ,[Address2] = @Address2
           ,[Address3] = @Address3
           ,[Suburb] = @Suburb
           ,[PostCode] = @PostCode
           ,[State] = @State
           ,[Country] = @Country
           ,[ModifiedDateTime] = Getdate()
           ,[ModifiedBy] = @UpdatedBy
           ,[Latitude] = @Latitude
           ,[Longtitude] = @Longtitude
		   ,[IsRedeliveryAvailable] = @IsRedeliveryAvailable
     where 
           [ID] = @ID

If((select Count(*) from @DtDeletedBeatIdList)>0)
	Begin

		Delete From [vw_DeliveryChoicesBeat] Where [BeatID] In ( Select [BeatIDs] Collate Latin1_General_CI_AS from @DtDeletedBeatIdList) and DeliveryChoiceId = @DeliveryChoiceId

	End 

If((select Count(*) from @DtNewAddedBeatIdList)>0)
	Begin

		INSERT INTO [dbo].[vw_DeliveryChoicesBeat]
           ([BeatID],[DeliveryChoiceID],[IsProcess],[CreatedDateTime],[CreatedBy])
		   (select Upper([BeatIds]), @DeliveryChoiceId , 0, getdate(),@UpdatedBy From @DtNewAddedBeatIdList)

	End 


	
select @ID as Id
select * from @DtNewAddedBeatIdList
select * from @DtDeletedBeatIdList
END TRY
		BEGIN CATCH
		begin
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_UpdateDeliveryChoices]', 2)
		end
		END CATCH


END 
GO
