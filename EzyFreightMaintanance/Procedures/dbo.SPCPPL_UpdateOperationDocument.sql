SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPPL_UpdateOperationDocument]
@OperationDocId int,
@OperationDocSubject varchar(100)= null,
@OperationDocDescription varchar(500)= null,
@OperationDocState varchar(10)= null,
@OperationDocIsActive bit= 0,
@IsFavourite bit= 0,
@UpdatedBy int= null,
@DtOPDocFileList [DtOPDocFileList] readonly,
@DtDeleteFileList [DtOPDocDeleteFileList] readonly

As Begin
Begin Try
BEGIN TRAN 


Update CPPLWeb_8_3_Admin..tblOperationDocuments set
           [OperationDocSubject] = @OperationDocSubject,
		   [OperationDocDescription] = @OperationDocDescription,
		   [OperationDocState] = @OperationDocState,
		   [OperationDocIsActive] = @OperationDocIsActive,
		   [IsFavourite] = @IsFavourite,
		   [UpdatedBy] = @UpdatedBy,
		   [UpdatedDateTime] = Getdate()
     Where OperationDocId = @OperationDocId

if (exists(select * from @DtOPDocFileList))
begin
INSERT INTO CPPLWeb_8_3_Admin..[tblOperationDocumentFiles]
           ([OperationDocName],[OperationDocAttachment],[OperationDocId],[CreatedBy],[CreatedDateTime],[UpdatedBy],[UpdatedDateTime])
     (select [OperationDocName],[OperationDocAttachment],@OperationDocId,@UpdatedBy, Getdate(),@UpdatedBy, Getdate() from @DtOPDocFileList)
end

if (exists(select * from @DtDeleteFileList))
begin
delete from CPPLWeb_8_3_Admin..[tblOperationDocumentFiles] where [OperationDocId] = @OperationDocId and [OperationDocAttachment] In (select [FileName] from @DtDeleteFileList)
end


Select * from CPPLWeb_8_3_Admin..tblOperationDocuments where OperationDocId = @OperationDocId

COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SPCPPL_UpdateOperationDocument', 2)
		end
		END CATCH

END 
GO
