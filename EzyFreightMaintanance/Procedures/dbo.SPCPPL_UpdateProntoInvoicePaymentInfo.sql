SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateProntoInvoicePaymentInfo]
     

		   @ReceiptNo nvarchar(100),
           @UpdatedBy int=null,
           @PaymentRefNo nvarchar(100)=null,
           @merchantReferenceCode nvarchar(max)=null,
           @SubscriptionID nvarchar(max)=null,
           @AuthorizationCode nvarchar(100)=null
		   

AS

BEGIN
BEGIN TRY
UPDATE dbo.[tblProntoInvoice]
			SET [PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTime] =GETDATE()
		   ,[UpdatedBy] = @UpdatedBy
		   ,IsPaymentProcessed = 1
 WHERE ReceiptNo = @ReceiptNo 

-----------------------------------------------------------------------------------------------------
END TRY
BEGIN CATCH
begin
INSERT INTO [dbo].[tblErrorLog]
		   ([Error]
			,[FunctionInfo]
			,[ClientId])
VALUES
            (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
		    ,'SPCPPL_UpdateProntoInvoicePaymentInfo', 2)

end
END CATCH       
              
END
GO
