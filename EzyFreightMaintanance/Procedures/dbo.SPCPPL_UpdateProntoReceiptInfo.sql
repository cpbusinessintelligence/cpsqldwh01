SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateProntoReceiptInfo]
@ReceiptNo nvarchar(100),
@InvoiceNo nvarchar(100),
@Messagefrompronto nvarchar(100)=null,
@UpdatedBy int=null,
@Flag_Receipt bit,
@ProntoComment nvarchar(100)
AS
BEGIN
BEGIN TRY
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
UPDATE dbo.[tblProntoInvoice]

			SET IsReceiptProcessed = @Flag_Receipt

           ,MessageFromPronto=@Messagefrompronto

		   ,Comment=@ProntoComment

		   ,[UpdatedDateTime] =GETDATE()

		   ,[UpdatedBy] = @UpdatedBy
		   
 WHERE ReceiptNo = @ReceiptNo and InvoiceNo = @InvoiceNo

-----------------------------------------------------------------------------------------------------
END TRY
BEGIN CATCH

begin
  INSERT INTO [dbo].[tblErrorLog]
           ([Error],[FunctionInfo],[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'SPCPPL_UpdateProntoInvoicePaymentInfo' , 2)
  
 End
   

END CATCH            
END
GO
