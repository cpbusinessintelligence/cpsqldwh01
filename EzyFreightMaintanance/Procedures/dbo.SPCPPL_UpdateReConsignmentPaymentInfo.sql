SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SPCPPL_UpdateReConsignmentPaymentInfo]
     

	   @ReconsignmentId nvarchar(100)=null,
           @ConsignmentCode nvarchar(100)=null,
           @UpdatedBy int=null,
           @PaymentRefNo nvarchar(100)=null,
           @merchantReferenceCode nvarchar(max)=null,
           @SubscriptionID nvarchar(max)=null,
           @AuthorizationCode nvarchar(100)=null,
	   @ConsignmentIDRet int = null,
	   @SalesOrderId int = null
           

AS

BEGIN


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

begin tran




 	UPDATE dbo.[tblInvoice]

	SET [PaymentRefNo] = @PaymentRefNo

           ,[AuthorizationCode]=@AuthorizationCode

           ,[merchantReferenceCode]=@merchantReferenceCode

           ,[SubscriptionID]=@SubscriptionID

           ,[UpdatedDateTime] =GETDATE()

	   ,[UpdatedBy] = @UpdatedBy
      

 WHERE invoicenumber = (select top 1 InvoiceNo from dbo.tblSalesOrder where ReferenceNo=@ConsignmentIDRet and SalesOrderId=@SalesOrderId)


-------------------------Update REConsignment------------------------------------------------

 

 	UPDATE dbo.[tblRedirectedConsignment]

	set [IsProcessed] =1,

	   [PaymentRefNo] = @PaymentRefNo

           ,[AuthorizationCode]=@AuthorizationCode

           ,[merchantReferenceCode]=@merchantReferenceCode

           ,[SubscriptionID]=@SubscriptionID

           ,[UpdatedDateTTime] =GETDATE()

	   ,[UpdatedBy] = @UpdatedBy

      where 
	  ConsignmentCode = @ConsignmentCode and ReconsignmentId = @ReconsignmentId

-----------------------------------------------------------------------------------------------------
            

  if(@@ERROR<>0)

  begin

  rollback tran

  

  INSERT INTO [dbo].[tblErrorLog]

           ([Error]

           ,[FunctionInfo]

           ,[ClientId])

     VALUES

           (ERROR_LINE() +' : '+ERROR_MESSAGE()

           ,'SPCPPL_UpdateReConsignmentPaymentInfo'

           , 2)

 
  end

  else

  commit tran          
              
END
GO
