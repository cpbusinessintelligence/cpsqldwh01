SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_UpdateRechargeIsProcess_26_07_2017]
           @ConsignmentID int= null,
           @PaymentRefNo nvarchar(50) = null,
		   @merchantRefCode nvarchar(50) = null,
           @IsProcess bit= null,
           @UpdatedBy int= null
AS
BEGIN
update [dbo].[tblRecharge]
set 
IsProcessed = @IsProcess, PaymentRefNo = @PaymentRefNo , MerchantReferenceCode = @merchantRefCode,
UpdatedBy = @UpdatedBy,
UpdatedDateTime=getDate()
where 

           [ConsignmentID]=@ConsignmentID
END
GO
