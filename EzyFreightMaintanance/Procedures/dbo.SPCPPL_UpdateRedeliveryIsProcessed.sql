SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[SPCPPL_UpdateRedeliveryIsProcessed]
           @RedeliveryID int = null ,
           @UpdatedBy int = null

           
AS
BEGIN

UPDATE [dbo].[tblRedelivery]
   SET [UpdatedBy] =case when @UpdatedBy is null then [UpdatedBy] else @UpdatedBy end ,
    
      [IsProcessed] =1,
 
      [UpdatedDateTime] = getDate()
 WHERE RedeliveryID = @RedeliveryID
				
End
GO
