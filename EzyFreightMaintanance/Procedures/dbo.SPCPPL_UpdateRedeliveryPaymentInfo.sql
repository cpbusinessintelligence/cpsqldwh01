SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateRedeliveryPaymentInfo]
		   @MerchantReferenceCode varchar(500)  = null ,
           @PaymentRefNo varchar(50)  = null ,
           @SubscriptionID nvarchar(50)  = null ,
           @AuthorizationCode varchar(50)  = null ,
		   @ConsignmentCode varchar(50)  = null ,
		   @RedeliveryConsignmentID int,
		   @UpdatedBy int
         
AS
BEGIN
BEGIN TRY
Begin Tran
UPDATE [dbo].[tblRedeliveryConsignment]
   SET [IsProcessed] = 1
      ,[PaymentRefNo] = @PaymentRefNo
      ,[AuthorizationCode] = @AuthorizationCode
      ,[SubscriptionID] = @SubscriptionID
      ,[MerchantReferenceCode] = @MerchantReferenceCode
	  ,UpdatedBy = @UpdatedBy, UpdatedDateTime = GetDate()
 WHERE [ConsignmentCode] = @ConsignmentCode and RedeliveryConsignmentID = @RedeliveryConsignmentID

 UPDATE [dbo].[tblinvoice]
   SET PaymentRefNo = @PaymentRefNo
      ,AuthorizationCode = @AuthorizationCode
      ,SubscriptionID = @SubscriptionID
      ,MerchantReferenceCode = @MerchantReferenceCode
	  ,UpdatedBy = @UpdatedBy, UpdatedDateTime = GetDate()
 WHERE InvoiceNumber in (select InvoiceNo from tblsalesorder where consignmentcode = @ConsignmentCode)

select @ConsignmentCode as consignmentcode, @RedeliveryConsignmentID as RedeliveryConsignmentID
Commit
END TRY

BEGIN CATCH

begin

    rollback tran
	INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
	VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
	,'[SPCPPL_UpdateRedeliveryPaymentInfo]', 2)

end

END CATCH

				
End
GO
