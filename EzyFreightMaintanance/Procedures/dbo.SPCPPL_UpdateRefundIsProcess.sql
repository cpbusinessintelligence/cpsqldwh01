SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateRefundIsProcess]
           @ConsignmentID int= null,
           
           @IsProcess bit= null,
           @PaymentRefNo nvarchar(max)= null,
           @AuthorizationCode nvarchar(max)= null,
           @UpdatedBy int= null
AS
BEGIN
	declare @Id int = null
	if	exists(select count(*) from [tblRefund] where  [ConsignmentID]=@ConsignmentID)
	begin
	select top 1 @Id = id from [tblRefund] where  [ConsignmentID]=@ConsignmentID order by 1 desc
	end

update [dbo].[tblRefund] set IsProcess = @IsProcess,
UpdatedBy = @UpdatedBy,
PaymentRefNo = @PaymentRefNo,
AuthorizationCode = @AuthorizationCode,
UpdatedDateTTime=getDate()
where [ConsignmentID]=@ConsignmentID and id = @Id
           
           
update [dbo].tblConsignment set ConsignmentStatus = 15,
UpdatedBy = @UpdatedBy,
UpdatedDateTTime=getDate()
where [ConsignmentID]=@ConsignmentID
           
END
GO
