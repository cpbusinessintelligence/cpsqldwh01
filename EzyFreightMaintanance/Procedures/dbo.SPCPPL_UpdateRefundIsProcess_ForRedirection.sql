SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateRefundIsProcess_ForRedirection]
           @ReConsignmentID int= null,
           @IsProcess bit= null,
           @PaymentRefNo nvarchar(max)= null,
           @AuthorizationCode nvarchar(max)= null,
           @UpdatedBy int= null
AS
BEGIN
declare @Id int = null
if	exists(select count(*) from [tblRedirectedRefund] where  [ReConsignmentID]=@ReConsignmentID)
begin
select top 1 @Id = id from [tblRedirectedRefund] where  [ReConsignmentID]=@ReConsignmentID order by 1 desc
end
update [dbo].[tblRedirectedRefund]
		set IsProcess = @IsProcess,UpdatedBy = @UpdatedBy,PaymentRefNo = @PaymentRefNo,AuthorizationCode = @AuthorizationCode,UpdatedDateTTime=getDate()
		where  [ReConsignmentID]=@ReConsignmentID and id = @Id

update [dbo].tblredirectedconsignment
		set ConsignmentStatus = 15,UpdatedBy = @UpdatedBy,UpdatedDateTTime=getDate()
		where [ReConsignmentID]=@ReConsignmentID
           
END
GO
