SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPPL_UpdateReportSubscription]
@SerialNo int,
@Category varchar(20)= null,
@ReportName varchar(100)= null,
@EmailTo varchar(8000) = null,
@EmailCC varchar(8000)= null,
@AccountNo varchar(100)

As
Begin

Update [dbo].[Subscriptions] set  [Category] = @Category ,[Reportname] = @Reportname,[To]=@EmailTo,
           [CC]=@EmailCC,[Parameter1] = @AccountNo
where [serialnumber] = @SerialNo
    

select @SerialNo as ID
end
GO
