SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_UpdateRequestResponseinReConsignment]
		   @ReconsignmentId nvarchar(100)=null,
           @ConsignmentCode nvarchar(100)=null,
           @UpdatedBy int=null,
           @RedirectionResponse nvarchar(100)=null

AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
begin tran
BEGIN TRY

-------------------------Update RE Consignment with Request and Response------------------------------------------------
	
	UPDATE dbo.[tblRedirectedConsignment]
 			set [RedirectionResponse] = @RedirectionResponse,
			[UpdatedDateTTime] =GETDATE(),
			[UpdatedBy] = @UpdatedBy
      where ConsignmentCode = @ConsignmentCode and ReconsignmentId = @ReconsignmentId

select @ReconsignmentId as ReConsignmentID
-----------------------------------------------------------------------------------------------------
commit tran      

END TRY
BEGIN CATCH
begin
rollback tran
INSERT INTO [dbo].[tblErrorLog]
		   ([Error]
			,[FunctionInfo]
			,[ClientId])
VALUES
            (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
		    ,'[SPCPPL_UpdateRequestResponseinReConsignment]', 2)

end

END CATCH  
      
END
GO
