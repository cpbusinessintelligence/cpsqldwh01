SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPPL_createItemLabel]
@ConsignmentID int=null,
           @LabelNumber varchar(100)=null,
           @Length decimal(5,2)=null,
           @Width decimal(5,2)=null,
           @Height decimal(5,2)=null,
           @CubicWeight decimal(4,2)=null,
           @PhysicalWeight decimal(4,2)=null,
           @MeasureWeight decimal(4,2)=null,
           @DeclareVolume decimal(4,2)=null,
           @LastActivity varchar(200)=null,
           @LastActivityDateTime varchar(200)=null,
           @CreatedBy int=null
           
           
      
AS
BEGIN

declare @lableCode8Digit nvarchar(2)
 select @lableCode8Digit = @LabelNumber
SELECT @lableCode8Digit =  RIGHT('00' + replace(@lableCode8Digit,'-',''),2)

declare @labelno nvarchar(19)

SELECT  @labelno=
      [ConsignmentCode] +@lableCode8Digit
     
  FROM [dbo].[tblConsignment] where [ConsignmentID]=@ConsignmentID

INSERT INTO [dbo].[tblItemLabel]
           ([ConsignmentID]
           ,[LabelNumber]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[CubicWeight]
           ,[PhysicalWeight]
           ,[MeasureWeight]
           ,[DeclareVolume]
           ,[LastActivity]
           ,[LastActivityDateTime]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
     VALUES
           (@ConsignmentID
           ,@labelno
           ,@Length
           ,@Width
           ,@Height
           ,@CubicWeight
           ,@PhysicalWeight
           ,@MeasureWeight
           ,@DeclareVolume
           ,@LastActivity
           ,@LastActivityDateTime
           ,GETDATE()
           ,@CreatedBy
           )
           
           
           
           


END
GO
