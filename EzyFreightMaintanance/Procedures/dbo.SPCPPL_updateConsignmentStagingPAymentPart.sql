SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPPL_updateConsignmentStagingPAymentPart]
@ConsignmentStagingID int,
@PaymentRefNo nvarchar(max) = null,
@merchantReferenceCode nvarchar(max)= null,
@SubscriptionID nvarchar(max)= null,
@AuthorizationCode nvarchar(max)= null
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

update tblConsignmentStaging set 
PaymentRefNo=@PaymentRefNo,
SubscriptionID=@SubscriptionID,
AuthorizationCode=@AuthorizationCode,
merchantReferenceCode = @merchantReferenceCode

 where 
ConsignmentStagingID = @ConsignmentStagingID

END
GO
