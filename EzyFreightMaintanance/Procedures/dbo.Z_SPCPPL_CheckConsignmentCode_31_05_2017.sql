SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[Z_SPCPPL_CheckConsignmentCode_31_05_2017]

@ConsignmentCode varchar(40) = null
 
AS
BEGIN
select top 5 * from [tblConsignment] where [ConsignmentCode] = @ConsignmentCode and IsProcessed = 1 
END

GO
