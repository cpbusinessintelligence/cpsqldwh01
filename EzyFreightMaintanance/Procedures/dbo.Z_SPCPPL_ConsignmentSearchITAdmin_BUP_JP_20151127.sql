SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_ConsignmentSearchITAdmin_BUP_JP_20151127]
@UserId int,
@PageIndex int=1,
 @SortColumn varchar(50) = null,
  @SortDir varchar(50)=null,
@PageSize int=10,

  @ConsignmentNo varchar(max)=null,
  @Sender varchar(max)=null,
  @Receiver varchar(max)=null,
  @ConsignmentFrom date=null,
  @ConsignmentTo date=null,
 @ConsignmentType varchar(max)=null
--@ParamTotalRec_out int out
AS
BEGIN
 





select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY PikupOrder desc,
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN   ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN    PickFirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN  DestFirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN   StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN PickFirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN DestFirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN CreatedDateTime END  )  RowNum, * 
					FROM 
					(




	SELECT top 100 percent  
	
 CASE 
	 WHEN  (addPikup.Country    LIKE '%'+ @Sender +'%' OR addDest.Country    LIKE '%'+ @Receiver +'%' )  then 1 
     WHEN  (addPikup.StateID    LIKE '%'+ @Sender +'%'  or         addDest.StateID    LIKE '%'+ @Receiver +'%' )then 2 
     WHEN  (addPikup.StateName    LIKE '%'+ @Sender +'%'  or   addDest.StateName    LIKE '%'+ @Receiver +'%' )then 2 
	 WHEN  (addPikup.PostCode    LIKE '%'+ @Sender +'%'  or   addDest.PostCode    LIKE '%'+ @Receiver +'%') then 3
	 WHEN  (addPikup.Suburb    LIKE '%'+ @Sender +'%'  or   addDest.Suburb    LIKE '%'+ @Receiver +'%') then 4
     else 5 end as PikupOrder ,
 
	 consign.*,
	 FORMAT(consign.CreatedDateTime,'dd/MM/yyyy HH:mm') AS CreatedDateTime1,
                      addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
                      addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, addDest.Country AS DestCountry, addDest.StateName AS DestStateName, addDest.CountryCode AS DestCountryCode, 
                      addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName,  addPikup.Suburb AS PickSuburab, addPikup.Country AS PickCountry,  addPikup.StateName AS PickStateName, addDest.CountryCode AS PickCountryCode, 
                      addPikup.PostCode AS PickPostCode,
                      addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate
FROM         dbo.tblConsignment AS consign INNER JOIN
                      dbo.tblAddress AS addPikup ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     

(consign.IsProcessed is null or consign.IsProcessed =1) and

(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateID  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Suburb  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.PostCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Country  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CountryCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateID  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Suburb  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.PostCode  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Country  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CountryCode  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' 

) 
 and

 (
 isnull(@ConsignmentType,'null1') = case when (consign.RateCardID = 'PEC' OR consign.RateCardID = 'REC' ) then  case when @ConsignmentType IS  null  then 'null1' else 'Saver' end  
                        when ( consign.RateCardID = 'SDC'OR consign.RateCardID = 'PDC'OR consign.RateCardID = 'CE3'OR consign.RateCardID = 'CE5'OR consign.RateCardID = 'PE3'OR consign.RateCardID = 'PE5') then case when @ConsignmentType IS  null  then 'null1' else 'ACE' end  
                        when (consign.RateCardID = 'SAV' OR consign.RateCardID = 'EXP') then  case when @ConsignmentType IS  null  then 'null1' else 'International' end  
                        when (consign.RateCardID = 'DAH' OR consign.RateCardID = 'DAM' OR consign.RateCardID = 'DAL' OR consign.RateCardID = 'ACH' OR consign.RateCardID = 'ACM' OR consign.RateCardID = 'ACL' OR consign.RateCardID = 'ASH' OR consign.RateCardID = 'ASM' OR consign.RateCardID = 'ASL' OR consign.RateCardID = 'DSH' OR consign.RateCardID = 'DSM' OR consign.RateCardID = 'DSL') then  case when @ConsignmentType IS  null  then 'null1' else 'Priority' end  
                        when (consign.RateCardID = 'RCH' OR consign.RateCardID = 'RCM' OR consign.RateCardID = 'RCL' OR consign.RateCardID = 'RSH' OR consign.RateCardID = 'RSM' OR consign.RateCardID = 'RSL') then  case when @ConsignmentType IS  null  then 'null1' else 'OffPeak' end  
                        
						else 'null1' 
                        end 
)

and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)


) and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
)

ORDER BY 
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN consign.ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN addPikup.FirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN addDest.FirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN dbo.tblStatus.StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN consign.CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN consign.ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN addPikup.FirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN addDest.FirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN dbo.tblStatus.StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN consign.CreatedDateTime END   
  
) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
		--		ORDER BY 
  ----CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN PickFirstName END DESC,
 
  ----CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC'  THEN PickFirstName END;
  --CreatedDateTime DESC,
  --CASE WHEN  @SortDir = 'DESC' THEN @SortColumn END DESC,
  --CASE WHEN @SortDir = 'ASC'  THEN @SortColumn END;
  
  
  SELECT    count( consign.CreatedDateTime)
FROM         dbo.tblConsignment AS consign INNER JOIN
                      dbo.tblAddress AS addPikup ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     
(consign.IsProcessed is null or consign.IsProcessed =1) and
(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.StateID  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Suburb  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.PostCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.Country  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CountryCode  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%'
) and

 (
CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.StateID  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Suburb  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.PostCode  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.Country  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CountryCode  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%'  

) and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)


)  and

 (
 isnull(@ConsignmentType,'null1') = case when (consign.RateCardID = 'PEC' OR consign.RateCardID = 'REC' ) then  case when @ConsignmentType IS  null  then 'null1' else 'Saver' end  
                        when ( consign.RateCardID = 'SDC'OR consign.RateCardID = 'PDC'OR consign.RateCardID = 'CE3'OR consign.RateCardID = 'CE5'OR consign.RateCardID = 'PE3'OR consign.RateCardID = 'PE5') then case when @ConsignmentType IS  null  then 'null1' else 'ACE' end  
                        when (consign.RateCardID = 'SAV' OR consign.RateCardID = 'EXP') then  case when @ConsignmentType IS  null  then 'null1' else 'International' end  
                        when (consign.RateCardID = 'DAH' OR consign.RateCardID = 'DAM' OR consign.RateCardID = 'DAL' OR consign.RateCardID = 'ACH' OR consign.RateCardID = 'ACM' OR consign.RateCardID = 'ACL' OR consign.RateCardID = 'ASH' OR consign.RateCardID = 'ASM' OR consign.RateCardID = 'ASL' OR consign.RateCardID = 'DSH' OR consign.RateCardID = 'DSM' OR consign.RateCardID = 'DSL') then  case when @ConsignmentType IS  null  then 'null1' else 'Priority' end  
                        when (consign.RateCardID = 'RCH' OR consign.RateCardID = 'RCM' OR consign.RateCardID = 'RCL' OR consign.RateCardID = 'RSH' OR consign.RateCardID = 'RSM' OR consign.RateCardID = 'RSL') then  case when @ConsignmentType IS  null  then 'null1' else 'OffPeak' end  
                        else 'null111' 
                        end 
)
and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
)

  
END







GO
