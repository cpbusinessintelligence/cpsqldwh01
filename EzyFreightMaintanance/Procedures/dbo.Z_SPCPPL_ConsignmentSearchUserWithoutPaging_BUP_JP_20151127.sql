SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create  PROCEDURE [dbo].[Z_SPCPPL_ConsignmentSearchUserWithoutPaging_BUP_JP_20151127]
@UserId int,
 
 @SortColumn varchar(50) = null,
  @SortDir varchar(50)=null,
 

  @ConsignmentNo varchar(max)=null,
  @Sender varchar(max)=null,
  @Receiver varchar(max)=null,
  @ConsignmentFrom date=null,
  @ConsignmentTo date=null

--@ParamTotalRec_out int out
AS
BEGIN






select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY 
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN   ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN    PickFirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN  DestFirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN   StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN PickFirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN DestFirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN CreatedDateTime END  )  RowNum, * 
					FROM 
					(




	SELECT top 100 percent   consign.*, 
                      addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
                      addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, 
                      addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName, 
                      addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate
FROM         dbo.tblConsignment AS consign INNER JOIN
                      dbo.tblAddress AS addPikup ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     (consign.UserID = @UserId) and 
(consign.IsProcessed is null or consign.IsProcessed =1) and
(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' 

) and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)


) and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
)

ORDER BY 
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN consign.ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN addPikup.FirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN addDest.FirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN dbo.tblStatus.StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN consign.CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN consign.ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN addPikup.FirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN addDest.FirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN dbo.tblStatus.StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN consign.CreatedDateTime END   

) as tblInner
				  )	as tblOuter 
				  
				ORDER BY 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN PickFirstName END DESC,
 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC'  THEN PickFirstName END;
  
  CASE WHEN  @SortDir = 'DESC' THEN @SortColumn END DESC,
  CASE WHEN @SortDir = 'ASC'  THEN @SortColumn END;
  
  
   

  
END


GO
