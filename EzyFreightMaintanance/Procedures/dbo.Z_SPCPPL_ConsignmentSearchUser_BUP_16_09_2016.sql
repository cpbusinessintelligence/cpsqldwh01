SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_ConsignmentSearchUser_BUP_16_09_2016]
@UserId int,
@PageIndex int=1,
@SortColumn varchar(50) = null,
@SortDir varchar(50)=null,
@PageSize int=10,
@ConsignmentNo varchar(max)=null,
@Sender varchar(max)=null,
@Receiver varchar(max)=null,
@ConsignmentFrom date=null,
@ConsignmentTo date=null

--@ParamTotalRec_out int out
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  


declare @AccountCustomer bit=0

if((select count(companyid) from [dbo].[tblCompanyUsers] where userid = @UserId) >0) begin
set @AccountCustomer = 1
end

if(@AccountCustomer=0)
begin

select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY 
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN   ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN    PickFirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN  DestFirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN   StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN PickFirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN DestFirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN CreatedDateTime END  )  RowNum, * 
					FROM 
					(




	SELECT top 100 percent   consign.*, 
                      addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
                      addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, 
                      addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName, 
                      addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate
FROM         dbo.tblConsignment AS consign  with (nolock) INNER JOIN
                      dbo.tblAddress AS addPikup  with (nolock) ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest  with (nolock) ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct  with (nolock) ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus  with (nolock) ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     (consign.UserID = @UserId) ---Changes Made By Shubham 11/05/2016

and (consign.IsProcessed is null or consign.IsProcessed =1) and
(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' 

) and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)


) and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL  with (nolock) where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL  with (nolock) where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
)

ORDER BY 
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN consign.ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN addPikup.FirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN addDest.FirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN dbo.tblStatus.StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN consign.CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN consign.ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN addPikup.FirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN addDest.FirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN dbo.tblStatus.StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN consign.CreatedDateTime END   

) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN PickFirstName END DESC,
 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC'  THEN PickFirstName END;
  
  CASE WHEN  @SortDir = 'DESC' THEN @SortColumn END DESC,
  CASE WHEN @SortDir = 'ASC'  THEN @SortColumn END;
  
  
  SELECT    count( consign.CreatedDateTime)
FROM         dbo.tblConsignment AS consign  with (nolock) INNER JOIN
                      dbo.tblAddress AS addPikup with (nolock)  ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest with (nolock)  ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct  with (nolock) ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus  with (nolock) ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     (consign.UserID = @UserId)  ---Changes Made By Shubham 11/05/2016
 

and (consign.IsProcessed is null or consign.IsProcessed =1) and
(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' 

) and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)


) and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL  with (nolock) where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL with (nolock)  where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
)

end


if(@AccountCustomer=1)
begin

select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY 
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN   ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN    PickFirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN  DestFirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN   StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN PickFirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN DestFirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN CreatedDateTime END  )  RowNum, * 
					FROM 
					(




	SELECT top 100 percent   consign.*, 
                      addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
                      addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, 
                      addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName, 
                      addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate
FROM         dbo.tblConsignment AS consign  with (nolock) INNER JOIN
                      dbo.tblAddress AS addPikup  with (nolock) ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest  with (nolock) ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct  with (nolock) ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus  with (nolock) ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE			 ---Changes Made By Shubham 11/05/2016
consign.UserID in  (select userid from [dbo].[tblCompanyUsers] where  companyid in (select companyid from [dbo].[tblCompanyUsers] where userid = @UserId))

and (consign.IsProcessed is null or consign.IsProcessed =1) and
(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' 

) and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)


) and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL  with (nolock) where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL  with (nolock) where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
)

ORDER BY 
  CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'DESC' THEN consign.ConsignmentCode END DESC,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN addPikup.FirstName END DESC,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'DESC' THEN addDest.FirstName END DESC,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'DESC' THEN dbo.tblStatus.StatusDescription END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN consign.CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'ConsignmentCode' AND @SortDir = 'ASC' THEN consign.ConsignmentCode END  ,
  CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC' THEN addPikup.FirstName END  ,
  CASE WHEN @SortColumn = 'DestFirstName' AND @SortDir = 'ASC' THEN addDest.FirstName END  ,
  CASE WHEN @SortColumn = 'StatusDescription' AND @SortDir = 'ASC' THEN dbo.tblStatus.StatusDescription END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN consign.CreatedDateTime END   

) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN PickFirstName END DESC,
 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC'  THEN PickFirstName END;
  
  CASE WHEN  @SortDir = 'DESC' THEN @SortColumn END DESC,
  CASE WHEN @SortDir = 'ASC'  THEN @SortColumn END;
  
  
  SELECT    count( consign.CreatedDateTime)
FROM         dbo.tblConsignment AS consign  with (nolock) INNER JOIN
                      dbo.tblAddress AS addPikup with (nolock)  ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest with (nolock)  ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct  with (nolock) ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus  with (nolock) ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     ---Changes Made By Shubham 11/05/2016
 consign.UserID in  (select userid from [dbo].[tblCompanyUsers] where  companyid in (select companyid from [dbo].[tblCompanyUsers] where userid = @UserId))

and (consign.IsProcessed is null or consign.IsProcessed =1) and
(
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.CompanyName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.LastName  END LIKE '%'+isnull(@Receiver,'')+'%' or
 CASE WHEN @Receiver IS null THEN isnull(@Receiver,'') else addDest.FirstName  END LIKE '%'+isnull(@Receiver,'')+'%' 

) and

 (
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.CompanyName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.LastName  END LIKE '%'+isnull(@Sender,'')+'%' or
 CASE WHEN @Sender IS null THEN isnull(@Sender,'') else addPikup.FirstName  END LIKE '%'+isnull(@Sender,'')+'%' 

) and

(
 CASE WHEN @ConsignmentFrom IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentFrom END <= CAST( consign.CreatedDateTime as DATE)


) and

(
 CASE WHEN @ConsignmentTo IS null THEN CAST( consign.CreatedDateTime as DATE) else @ConsignmentTo END >= CAST( consign.CreatedDateTime as DATE)


) and

(
CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else consign.ConsignmentCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' or
consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL  with (nolock) where

  CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.LabelNumber  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
 or
consign.ConsignmentID in (select IL.ConsignmentID from tblDHLBarCodeImage IL with (nolock)  where CASE WHEN @ConsignmentNo IS null THEN isnull(@ConsignmentNo,'') else IL.AWBCode  END LIKE '%'+isnull(@ConsignmentNo,'')+'%' 
 ) 
)

end


  
END
GO
