SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create PROCEDURE [dbo].[Z_SPCPPL_CreateCompanyTran_BUP]
           @AccountNumber nvarchar(20) = null,
           @CompanyName varchar(50) = null,
           @ParentAccountNo varchar(20) = null,
           @ABN varchar(20) = null,
         
           @ParentCompanyName varchar(50) = null,
           @BilingAccountNo nvarchar(20) = null,
           @IsProntoExtracted bit = null,
           @RateCardCategory nvarchar(20) = null,
 
 @PostalAddressuserId int=null,
@PostalAddressfirstName nvarchar(50)=null,
@PostalAddresslastName nvarchar(50)=null,
@PostalAddresscompanyName nvarchar(100)=null,
@PostalAddressemail nvarchar(250)=null,
@PostalAddressaddress1 nvarchar(200)=null,
@PostalAddressaddress2 nvarchar(200)=null,
@PostalAddresssuburb nvarchar(100)=null,
@PostalAddressstateId nvarchar(max)=null,
@PostalAddresspostalCode nvarchar(20)=null,
@PostalAddressphone nvarchar(20)=null,
@PostalAddressmobile nvarchar(20)=null,
@PostalAddresscreatedBy int=null,
@PostalAddressisBusiness bit=0,
@PostalAddressisRegisterAddress bit=0,
@PostalAddressIsSubscribe bit=0,


@AddressuserId int=null,
@AddressfirstName nvarchar(50)=null,
@AddresslastName nvarchar(50)=null,
@AddresscompanyName nvarchar(100)=null,
@Addressemail nvarchar(250)=null,
@Addressaddress1 nvarchar(200)=null,
@Addressaddress2 nvarchar(200)=null,
@Addresssuburb nvarchar(100)=null,
@AddressstateId nvarchar(max)=null,
@AddresspostalCode nvarchar(20)=null,
@Addressphone nvarchar(20)=null,
@Addressmobile nvarchar(20)=null,
@AddresscreatedBy int=null,
@AddressisBusiness bit=0,
@AddressisRegisterAddress bit=0,
@AddressIsSubscribe bit=0 ,
@UserID int = null
           
      
AS
BEGIN
begin tran

DECLARE @stateIdbyCode int , @AddressID1 int ,@PostalAddressID int

-------------------------------------------------
			SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @AddressstateId
 

if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@AddresscompanyName)) and 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @AddressfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@AddresslastName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Addressaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Addressaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =  rtrim(ltrim(@AddresscompanyName)) and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Addresssuburb ))and 
rtrim(ltrim([StateID])) =  rtrim(ltrim(@stateIdbyCode)) and 
rtrim(ltrim([PostCode])) =  rtrim(ltrim(@AddresspostalCode)) and [UserID]=@AddressuserId)
begin
select @AddressID1 = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@AddresscompanyName)) and 
rtrim(ltrim([FirstName])) =rtrim(ltrim( @AddressfirstName ))and 
rtrim(ltrim([LastName] ))= rtrim(ltrim(@AddresslastName ))and 
rtrim(ltrim([Address1])) =rtrim(ltrim( @Addressaddress1)) and 
rtrim(ltrim([Address2])) =rtrim(ltrim( @Addressaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =rtrim(ltrim( @AddresscompanyName)) and 
rtrim(ltrim([Suburb] ))= rtrim(ltrim(@Addresssuburb ))and 
rtrim(ltrim([StateID])) = rtrim(ltrim(@stateIdbyCode ))and 
rtrim(ltrim([PostCode] ))=rtrim(ltrim( @AddresspostalCode)) and [UserID]=@AddressuserId
end
else

begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe] )
	 VALUES
      ( @AddressuserId,@AddressfirstName ,@AddresslastName ,@AddresscompanyName,@Addressemail,@Addressaddress1,@Addressaddress2,@Addresssuburb,@stateIdbyCode,
      @AddresspostalCode,@Addressphone,@Addressmobile,@AddressisRegisterAddress,GETDATE(), @AddresscreatedBy ,@AddressisBusiness,@AddressIsSubscribe)
        SET @AddressID1 = SCOPE_IDENTITY()
end
 
			--select @AddressID
			
			-------------------------------------------------
							SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @PostalAddressstateId
 


if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@PostalAddresscompanyName)) and 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @PostalAddressfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@PostalAddresslastName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@PostalAddressaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@PostalAddressaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =  rtrim(ltrim(@PostalAddresscompanyName)) and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@PostalAddresssuburb ))and 
rtrim(ltrim([StateID])) =  rtrim(ltrim(@stateIdbyCode)) and 
rtrim(ltrim([PostCode])) =  rtrim(ltrim(@PostalAddresspostalCode)) and [UserID]=@PostalAddressuserId)
begin
select @PostalAddressID = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@PostalAddresscompanyName)) and 
rtrim(ltrim([FirstName])) =rtrim(ltrim( @PostalAddressfirstName ))and 
rtrim(ltrim([LastName] ))= rtrim(ltrim(@PostalAddresslastName ))and 
rtrim(ltrim([Address1])) =rtrim(ltrim( @PostalAddressaddress1)) and 
rtrim(ltrim([Address2])) =rtrim(ltrim( @PostalAddressaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =rtrim(ltrim( @PostalAddresscompanyName)) and 
rtrim(ltrim([Suburb] ))= rtrim(ltrim(@PostalAddresssuburb ))and 
rtrim(ltrim([StateID])) = rtrim(ltrim(@stateIdbyCode ))and 
rtrim(ltrim([PostCode] ))=rtrim(ltrim( @PostalAddresspostalCode)) and [UserID]=@PostalAddressuserId
end
else

begin

	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe] )
	 VALUES
      ( @PostalAddressuserId,@PostalAddressfirstName ,@PostalAddresslastName ,@PostalAddresscompanyName,@PostalAddressemail,@PostalAddressaddress1,@PostalAddressaddress2,@PostalAddresssuburb,@stateIdbyCode,
      @PostalAddresspostalCode,@PostalAddressphone,@PostalAddressmobile,@PostalAddressisRegisterAddress,GETDATE(), @PostalAddresscreatedBy ,@PostalAddressisBusiness,@PostalAddressIsSubscribe)
        SET @PostalAddressID = SCOPE_IDENTITY()
end
			--select @PostalAddressID
			-------------------------------------------------
			---------------------
			
				DECLARE @CompanyIDret int
			INSERT INTO  [dbo].[tblCompany]
           ([AccountNumber]
           ,[CompanyName]
           ,[ParentAccountNo]
           ,[ABN]
           ,[AddressID1]
           ,[PostalAddressID]
           ,[ParentCompanyName]
           ,[BilingAccountNo]
           ,[IsProntoExtracted]
           ,[RateCardCategory])
     VALUES
           (
           @AccountNumber  ,
           @CompanyName  ,
           @ParentAccountNo  ,
           @ABN  ,
           @AddressID1  ,
           @PostalAddressID  ,
           @ParentCompanyName  ,
           @BilingAccountNo  ,
           @IsProntoExtracted  ,
           @RateCardCategory  
           )
            SET @CompanyIDret = SCOPE_IDENTITY()
			select @CompanyIDret
			
			
			--------------------------------
					DECLARE @CompanyUsersIDret int
			INSERT INTO [dbo].[tblCompanyUsers]
           ([UserID]
           ,[CompanyID] )
     VALUES
           (
           @UserID  ,
           @CompanyIDret   )
            SET @CompanyUsersIDret = SCOPE_IDENTITY()
			select @CompanyUsersIDret
			 if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateCompanyTran]'
    , 2)
  end
  else
  commit tran          
            

END

GO
