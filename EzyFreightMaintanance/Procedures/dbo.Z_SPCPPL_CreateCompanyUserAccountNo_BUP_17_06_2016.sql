SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateCompanyUserAccountNo_BUP_17_06_2016]
        
@UserID int = null
           
      
AS
BEGIN
begin tran
			
DECLARE @CompanyIDret int
declare @CompanyID int 
declare @AccountNo varchar(50)

select @CompanyID =   CompanyID    from tblCompanyUsers where UserID =  @UserID
select @AccountNo =   AccountNumber    from tblCompany where CompanyID =  @CompanyID
 
 
update tblCompanyUsers 
set IsUserDisabled = 0
where UserID =  @UserID

 print @companyid


 if (@AccountNo is null)
begin
declare @AccountNumber9Digit nvarchar(10)

select @AccountNumber9Digit = @CompanyID
SELECT @AccountNumber9Digit =  RIGHT('00000000' + replace(@AccountNumber9Digit,'-',''), 8)
if(@AccountNumber9Digit is null)
set @AccountNumber9Digit = '00000000'
set @AccountNumber9Digit = 'WD'+@AccountNumber9Digit
 
 
SELECT @AccountNumber9Digit
            	update   [dbo].[tblCompany] set
           [AccountNumber]= @AccountNumber9Digit where CompanyID  = @CompanyID
			
end
else
begin 
SELECT @AccountNo
end
 
		 
			 if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateCompanyUserAccountN]'
    , 2)
  end
  else
  commit tran          
            

END
GO
