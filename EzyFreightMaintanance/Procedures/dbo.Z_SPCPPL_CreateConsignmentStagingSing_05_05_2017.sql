SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentStagingSing_05_05_2017]

@UserID int= null,
@IsRegUserConsignment bit= null,
@TotalWeight nvarchar(max)= null,
@TotalVolume nvarchar(max)= null,
@NoOfItems int= null,
@SpecialInstruction varchar(500)= null,
@CustomerRefNo varchar(40)= null,
@ConsignmentPreferPickupDate date= null,
@ConsignmentPreferPickupTime varchar(20)= null,
@ClosingTime varchar(10)= null,
@DangerousGoods bit= null,
@Terms bit= null,
@RateCardID nvarchar(50)= null,
@CreatedBy int= null,
@ServiceID int= null,
---------------------------------------------Pickup Variables
@PickupAddressId int=null,
@PickupuserId int=null,
@PickupfirstName nvarchar(50)=null,
@PickuplastName nvarchar(50)=null,
@PickupcompanyName nvarchar(100)=null,
@Pickupemail nvarchar(250)=null,
@Pickupaddress1 nvarchar(200)=null,
@Pickupaddress2 nvarchar(200)=null,
@Pickupsuburb nvarchar(100)=null,
@PickupstateId nvarchar(max)=null,
@PickuppostalCode nvarchar(20)=null,
@Pickupphone nvarchar(20)=null,
@Pickupmobile nvarchar(20)=null,
@PickupcreatedBy int=null,
@PickupisBusiness bit=false,
@PickupisRegisterAddress bit=false,
@PickupIsSubscribe bit=false,
@PickupCountry nvarchar(max)=null,
@PickupCountryCode nvarchar(max)=null,
---------------------------------------------Destination Variables
@DestinationAddressId int=null,
@DestinationuserId int=null,
@DestinationfirstName nvarchar(50)=null,
@DestinationlastName nvarchar(50)=null,
@DestinationcompanyName nvarchar(100)=null,
@Destinationemail nvarchar(250)=null,
@Destinationaddress1 nvarchar(200)=null,
@Destinationaddress2 nvarchar(200)=null,
@Destinationsuburb nvarchar(100)=null,
@DestinationstateId nvarchar(max)=null,
@DestinationpostalCode nvarchar(20)=null,
@Destinationphone nvarchar(20)=null,
@Destinationmobile nvarchar(20)=null,
@DestinationcreatedBy int=null,
@DestinationisBusiness bit=false,
@DestinationisRegisterAddress bit=false,
@DestinationIsSubscribe bit=false,
@DestinationCountry nvarchar(max)=null,
@DestinationCountryCode nvarchar(max)=null,

---------------------------------------------Contact Variables
@ContactAddressId int=null,
@ContactuserId int=null,
@ContactfirstName nvarchar(50)=null,
@ContactlastName nvarchar(50)=null,
@ContactcompanyName nvarchar(100)=null,
@Contactemail nvarchar(250)=null,
@Contactaddress1 nvarchar(200)=null,
@Contactaddress2 nvarchar(200)=null,
@Contactsuburb nvarchar(100)=null,
@ContactstateId nvarchar(max)=null,
@ContactpostalCode nvarchar(20)=null,
@Contactphone nvarchar(20)=null,
@Contactmobile nvarchar(20)=null,
@ContactcreatedBy int=null,
@ContactisBusiness bit=false,
@ContactisRegisterAddress bit=false,
@ContactIsSubscribe bit=false,
@ContactCountry nvarchar(max)=null,
@ContactCountryCode nvarchar(max)=null,
--------------------------------------------------------------
@IsSameAsDestination bit = 0,
@IsSameAsPickup bit = 0,
@IsSignatureReq bit = 0,
@IsDocument nvarchar(max) = null,
@IfUndelivered nvarchar(max)=null,
@ReasonForExport nvarchar(max)=null,
@TypeOfExport nvarchar(max)=null,
@Currency nvarchar(max)=null,
@IsInsurance bit=null,
@IdentityNo   nvarchar(max)=null,
@IdentityType nvarchar(max)=null,
@IsIdentity   bit=null,
@IsATl  bit=null,
@IsReturnToSender   bit=null,
@HasReadInsuranceTc   bit=null,
@NatureOfGoods nvarchar(max) = null,
@SortCode nvarchar(max) = null,
@ETA nvarchar(max) = null,
@InsuranceAmount decimal(10,2) = null,
@GrossTotal decimal(18,2) = null,
@GST  decimal(18,2)  = null

AS

BEGIN
Begin Try
begin tran




--------------------------------------------Pickup----------------------------------------------------

DECLARE @PickupAddID int
if (isnull(@PickupAddressId,0)<>0)
begin
set @PickupAddID = @PickupAddressId 
end
else
begin

---------------------------Pickup------------------------------------------
DECLARE @stateIdbyCode_Pickup int
        SELECT  @stateIdbyCode_Pickup=[StateID]
        FROM [dbo].[tblState]where [StateCode] = @PickupstateId
---------------------------------------------------------------------------
INSERT INTO [dbo].[tblAddress]
	( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,[StateID],
	[PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[Country],[CountryCode]  )
VALUES
      ( @UserID,@PickupfirstName ,@PickuplastName ,@PickupcompanyName,@Pickupemail,@Pickupaddress1,@Pickupaddress2,@Pickupsuburb,@PickupstateId,@stateIdbyCode_Pickup,
      @PickuppostalCode,@Pickupphone,@Pickupmobile,@PickupisRegisterAddress,GETDATE(), @PickupcreatedBy ,@PickupisBusiness,@PickupIsSubscribe
      ,@PickupCountry,@PickupCountryCode)
SET @PickupAddID = SCOPE_IDENTITY()
end


---------------------------Destination-------------------------------------
DECLARE @stateIdbyCode_Destination int 
        SELECT  @stateIdbyCode_Destination=[StateID]
        FROM [dbo].[tblState]where [StateCode] = @DestinationstateId
---------------------------------------------------------------------------
DECLARE @DestinationAddID int
if (isnull(@DestinationAddressId,'')<>'')
begin
Set @DestinationAddID = @DestinationAddressId 
end
else
begin
INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[Country],[CountryCode] )
VALUES
      ( @UserID,@DestinationfirstName ,@DestinationlastName ,@DestinationcompanyName,@Destinationemail,@Destinationaddress1,@Destinationaddress2,@Destinationsuburb,@DestinationstateId,@stateIdbyCode_Destination,
      @DestinationpostalCode,@Destinationphone,@Destinationmobile,@DestinationisRegisterAddress,GETDATE(), @DestinationcreatedBy ,@DestinationisBusiness,@DestinationIsSubscribe
      ,@DestinationCountry,@DestinationCountryCode)
SET @DestinationAddID = SCOPE_IDENTITY()

end


-------------------------------------------Contact-------------------------------------------------------------
DECLARE @ContactAddID int
IF (@IsSameAsDestination=1)
   set @ContactAddID = @DestinationAddID
ELSE 
   BEGIN
      IF (@IsSameAsPickup=1)
set @ContactAddID = @PickupAddID
   ELSE
       begin

if (isnull(@ContactAddressId,'')<>'')
begin
set @ContactAddID = @ContactAddressId 
end
else


begin

---------------------------Contact------------------------------------------
DECLARE @stateIdbyCode_Contact int 
        SELECT  @stateIdbyCode_Contact=[StateID]
        FROM [dbo].[tblState]where [StateCode] = @ContactstateId

----------------------------------------------------------------------------

INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName, StateID,
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe] ,[Country],[CountryCode]  )
VALUES
      ( @UserID,@ContactfirstName ,@ContactlastName ,@ContactcompanyName,@Contactemail,@Contactaddress1,@Contactaddress2,@Contactsuburb,@ContactstateId,@stateIdbyCode_Contact,
      @ContactpostalCode,@Contactphone,@Contactmobile,@ContactisRegisterAddress,GETDATE(), @ContactcreatedBy ,@ContactisBusiness,@ContactIsSubscribe
      ,@ContactCountry,@ContactCountryCode)
SET @ContactAddID = SCOPE_IDENTITY()
end
end
END ;
--------------------------------------------------------------------------------------------------------
DECLARE @ConsignmentIDret int
IF ( Isnull(@SortCode, '') = '' ) 
	BEGIN	
		SELECT TOP 1 @SortCode = sortcode FROM locationmaster 
		WHERE postcode = @DestinationpostalCode 
	END
			INSERT INTO [dbo].tblConsignmentStaging
           (
            [UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[PickupDate]
           ,[PreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[ServiceID]
           ,[IsProcessed]
           ,[IsDocument]
           ,IsSignatureReq
           ,IfUndelivered
           ,ReasonForExport
           ,TypeOfExport
           ,Currency
           ,IsInsurance
           ,IdentityNo  
	       ,IdentityType
		   ,IsIdentity  
		   ,IsATl  
		   ,IsReturnToSender   
		   ,HasReadInsuranceTc  
		   ,NatureOfGoods
		   ,SortCode
		   ,ETA
		   ,InsuranceAmount
		   ,CalculatedTotal
		   ,CalculatedGST )

     VALUES
           (
           @UserID,
           @IsRegUserConsignment,
           @PickupAddID,
           @DestinationAddID,
           @ContactAddID,
           @TotalWeight,
           @TotalVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           GETDATE(),
           @CreatedBy,
           @ServiceID,
           0,
           @IsDocument  ,
         @IsSignatureReq  ,
         @IfUndelivered
           ,@ReasonForExport
           ,@TypeOfExport
           ,@Currency 
           ,@IsInsurance
           ,@IdentityNo  
			,@IdentityType
			,@IsIdentity  
			,@IsATl  
			,@IsReturnToSender    
			,@HasReadInsuranceTc  
			,@NatureOfGoods
			,@SortCode
        ,@ETA
        ,@InsuranceAmount
        ,@GrossTotal
        ,@GST 
           ) SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret, @PickupAddressId
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

COMMIT TRAN 


		END TRY
		BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_CreateConsignmentStagingSing]', 2)
		end
		END CATCH     
END
GO
