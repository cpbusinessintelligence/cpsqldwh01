SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentStagingSing_BUP_20_08_2016]

@UserID int= null,

@IsRegUserConsignment bit= null,

--@PickupID int= null,

--@DestinationID int= null,

--@ContactID int= null,

@TotalWeight nvarchar(max)= null,

@TotalVolume nvarchar(max)= null,

@NoOfItems int= null,

@SpecialInstruction varchar(500)= null,

@CustomerRefNo varchar(40)= null,

@ConsignmentPreferPickupDate date= null,

@ConsignmentPreferPickupTime varchar(20)= null,

@ClosingTime varchar(10)= null,

@DangerousGoods bit= null,

@Terms bit= null,

@RateCardID nvarchar(50)= null,

@CreatedBy int= null,

@ServiceID int= null,

@PickupuserId int=null,

@PickupfirstName nvarchar(50)=null,

@PickuplastName nvarchar(50)=null,

@PickupcompanyName nvarchar(100)=null,

@Pickupemail nvarchar(250)=null,

@Pickupaddress1 nvarchar(200)=null,

@Pickupaddress2 nvarchar(200)=null,

@Pickupsuburb nvarchar(100)=null,

@PickupstateId nvarchar(max)=null,

@PickuppostalCode nvarchar(20)=null,

@Pickupphone nvarchar(20)=null,

@Pickupmobile nvarchar(20)=null,

@PickupcreatedBy int=null,

@PickupisBusiness bit=false,

@PickupisRegisterAddress bit=false,

@PickupIsSubscribe bit=false,

@PickupCountry nvarchar(max)=null,

@PickupCountryCode nvarchar(max)=null,

@DestinationuserId int=null,

@DestinationfirstName nvarchar(50)=null,

@DestinationlastName nvarchar(50)=null,

@DestinationcompanyName nvarchar(100)=null,

@Destinationemail nvarchar(250)=null,

@Destinationaddress1 nvarchar(200)=null,

@Destinationaddress2 nvarchar(200)=null,

@Destinationsuburb nvarchar(100)=null,

@DestinationstateId nvarchar(max)=null,

@DestinationpostalCode nvarchar(20)=null,

@Destinationphone nvarchar(20)=null,

@Destinationmobile nvarchar(20)=null,

@DestinationcreatedBy int=null,

@DestinationisBusiness bit=false,

@DestinationisRegisterAddress bit=false,

@DestinationIsSubscribe bit=false,

@DestinationCountry nvarchar(max)=null,

@DestinationCountryCode nvarchar(max)=null,

@ContactuserId int=null,

@ContactfirstName nvarchar(50)=null,

@ContactlastName nvarchar(50)=null,

@ContactcompanyName nvarchar(100)=null,

@Contactemail nvarchar(250)=null,

@Contactaddress1 nvarchar(200)=null,

@Contactaddress2 nvarchar(200)=null,

@Contactsuburb nvarchar(100)=null,

@ContactstateId nvarchar(max)=null,

@ContactpostalCode nvarchar(20)=null,

@Contactphone nvarchar(20)=null,

@Contactmobile nvarchar(20)=null,

@ContactcreatedBy int=null,

@ContactisBusiness bit=false,

@ContactisRegisterAddress bit=false,

@ContactIsSubscribe bit=false,

@ContactCountry nvarchar(max)=null,

@ContactCountryCode nvarchar(max)=null,

@IsSameAsDestination bit = 0,

@IsSameAsPickup bit = 0,

@IsSignatureReq bit = 0,

@IsDocument nvarchar(max) = null,

@IfUndelivered nvarchar(max)=null,

@ReasonForExport nvarchar(max)=null,

@TypeOfExport nvarchar(max)=null,

@Currency nvarchar(max)=null,

@IsInsurance bit=null,

@IdentityNo   nvarchar(max)=null,

@IdentityType nvarchar(max)=null,

@IsIdentity   bit=null,

@IsATl  bit=null,

@IsReturnToSender   bit=null,

@HasReadInsuranceTc   bit=null,

@NatureOfGoods nvarchar(max) = null,

@SortCode nvarchar(max) = null,

@ETA nvarchar(max) = null,

@InsuranceAmount decimal(10,2) = null,

@GrossTotal decimal(18,2) = null,

@GST  decimal(18,2)  = null





AS

BEGIN

begin tran

-------------------------------------------------
DECLARE @stateIdbyCode int
DECLARE @DestinationAddID int


if exists(select * from [tblAddress]  where 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) = rtrim(ltrim(ISNULL( @DestinationcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [FirstName],'1'))) = rtrim(ltrim(ISNULL(  @DestinationfirstName,'1'))) and 

rtrim(ltrim(ISNULL( [LastName],'1'))) =  rtrim(ltrim(ISNULL( @DestinationlastName,'1'))) and 

rtrim(ltrim(ISNULL( [Address1],'1'))) =  rtrim(ltrim(ISNULL( @Destinationaddress1,'1'))) and 

rtrim(ltrim(ISNULL( [Address2],'1'))) =  rtrim(ltrim(ISNULL( @Destinationaddress2 ,'1'))) and 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) =  rtrim(ltrim(ISNULL( @DestinationcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [Suburb],'1'))) =  rtrim(ltrim(ISNULL( @Destinationsuburb ,'1'))) and 

rtrim(ltrim(ISNULL( [StateID],'1'))) =  rtrim(ltrim(ISNULL( @DestinationstateId,'1'))) and 

rtrim(ltrim(ISNULL( [Phone],'1'))) =  rtrim(ltrim(ISNULL( @Destinationphone,'1'))) and 

rtrim(ltrim(ISNULL( [PostCode],'1'))) =  rtrim(ltrim(ISNULL( @DestinationpostalCode,'1'))) and ISNULL([UserID],'1')=ISNULL(@DestinationuserId,'1'))

begin

select @DestinationAddID = AddressID from [tblAddress]  where 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) = rtrim(ltrim(ISNULL( @DestinationcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [FirstName],'1'))) =rtrim(ltrim(ISNULL(  @DestinationfirstName ,'1'))) and 

rtrim(ltrim(ISNULL( [LastName],'1') ))= rtrim(ltrim(ISNULL( @DestinationlastName ,'1'))) and 

rtrim(ltrim(ISNULL( [Address1],'1'))) =rtrim(ltrim(ISNULL(  @Destinationaddress1,'1'))) and 

rtrim(ltrim(ISNULL( [Address2],'1'))) =rtrim(ltrim(ISNULL(  @Destinationaddress2 ,'1'))) and 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) =rtrim(ltrim(ISNULL(  @DestinationcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [Suburb],'1') ))= rtrim(ltrim(ISNULL( @Destinationsuburb ,'1'))) and 

rtrim(ltrim(ISNULL( [StateID],'1'))) = rtrim(ltrim(ISNULL( @DestinationstateId ,'1'))) and 

rtrim(ltrim(ISNULL( [Phone],'1'))) =  rtrim(ltrim(ISNULL( @Destinationphone,'1'))) and 

rtrim(ltrim(ISNULL( [PostCode],'1') ))=rtrim(ltrim(ISNULL(  @DestinationpostalCode,'1'))) and ISNULL([UserID],'1')=ISNULL(@DestinationuserId,'1')

end

else



begin
---------------------------Destination-------------------------------------
        SELECT  @stateIdbyCode=[StateID]
        FROM [dbo].[tblState]where [StateCode] = @DestinationstateId
---------------------------------------------------------------------------


	INSERT INTO [dbo].[tblAddress]

	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,[StateID],

	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe]

	 ,[Country],[CountryCode] )

	 VALUES

      ( @DestinationuserId,@DestinationfirstName ,@DestinationlastName ,@DestinationcompanyName,@Destinationemail,@Destinationaddress1,@Destinationaddress2,@Destinationsuburb,@DestinationstateId,@stateIdbyCode,

      @DestinationpostalCode,@Destinationphone,@Destinationmobile,@DestinationisRegisterAddress,GETDATE(), @DestinationcreatedBy ,@DestinationisBusiness,@DestinationIsSubscribe

      ,@DestinationCountry,@DestinationCountryCode)

        SET @DestinationAddID = SCOPE_IDENTITY()

end

------------------------------------------------------------------------------------------------

DECLARE @PickupAddID int


if exists(select * from [tblAddress]  where 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) = rtrim(ltrim(ISNULL( @PickupcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [FirstName],'1'))) = rtrim(ltrim(ISNULL(  @PickupfirstName,'1'))) and 

rtrim(ltrim(ISNULL( [LastName],'1'))) =  rtrim(ltrim(ISNULL( @PickuplastName,'1'))) and 

rtrim(ltrim(ISNULL( [Address1],'1'))) =  rtrim(ltrim(ISNULL( @Pickupaddress1,'1'))) and 

rtrim(ltrim(ISNULL( [Address2],'1'))) =  rtrim(ltrim(ISNULL( @Pickupaddress2 ,'1'))) and 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) =  rtrim(ltrim(ISNULL( @PickupcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [Suburb],'1'))) =  rtrim(ltrim(ISNULL( @Pickupsuburb ,'1'))) and 

rtrim(ltrim(ISNULL( [StateID],'1'))) =  rtrim(ltrim(ISNULL( @PickupstateId,'1'))) and 

rtrim(ltrim(ISNULL( [Phone],'1'))) =  rtrim(ltrim(ISNULL( @Pickupphone,'1'))) and 

rtrim(ltrim(ISNULL( [PostCode],'1'))) =  rtrim(ltrim(ISNULL( @PickuppostalCode,'1'))) and ISNULL([UserID],'1')=ISNULL(@PickupuserId,'1'))

begin

select @PickupAddID = AddressID from [tblAddress]  where 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) = rtrim(ltrim(ISNULL( @PickupcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [FirstName],'1'))) =rtrim(ltrim(ISNULL(  @PickupfirstName ,'1'))) and 

rtrim(ltrim(ISNULL( [LastName],'1') ))= rtrim(ltrim(ISNULL( @PickuplastName ,'1'))) and 

rtrim(ltrim(ISNULL( [Address1],'1'))) =rtrim(ltrim(ISNULL(  @Pickupaddress1,'1'))) and 

rtrim(ltrim(ISNULL( [Address2],'1'))) =rtrim(ltrim(ISNULL(  @Pickupaddress2 ,'1'))) and 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) =rtrim(ltrim(ISNULL(  @PickupcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [Suburb],'1') ))= rtrim(ltrim(ISNULL( @Pickupsuburb ,'1'))) and 

rtrim(ltrim(ISNULL( [StateID],'1'))) = rtrim(ltrim(ISNULL( @PickupstateId ,'1'))) and 

rtrim(ltrim(ISNULL( [Phone],'1'))) =  rtrim(ltrim(ISNULL( @Pickupphone,'1'))) and 

rtrim(ltrim(ISNULL( [PostCode],'1') ))=rtrim(ltrim(ISNULL(  @PickuppostalCode,'1'))) and ISNULL([UserID],'1')=ISNULL(@PickupuserId,'1')

end

else



begin

---------------------------Pickuo------------------------------------------
        SELECT  @stateIdbyCode=[StateID]
        FROM [dbo].[tblState]where [StateCode] = @PickupstateId
---------------------------------------------------------------------------



	INSERT INTO [dbo].[tblAddress]

	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,[StateID],

	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe]

	 ,[Country],[CountryCode]  )

	 VALUES

      ( @PickupuserId,@PickupfirstName ,@PickuplastName ,@PickupcompanyName,@Pickupemail,@Pickupaddress1,@Pickupaddress2,@Pickupsuburb,@PickupstateId,@stateIdbyCode,

      @PickuppostalCode,@Pickupphone,@Pickupmobile,@PickupisRegisterAddress,GETDATE(), @PickupcreatedBy ,@PickupisBusiness,@PickupIsSubscribe

      ,@PickupCountry,@PickupCountryCode)

        SET @PickupAddID = SCOPE_IDENTITY()

end

			--select @PickupAddID

			-------------------------------------------------



			----------------------



DECLARE @ContactAddID int



IF (@IsSameAsDestination=1)

   set @ContactAddID = @DestinationAddID

ELSE 

   BEGIN

      IF (@IsSameAsPickup=1)

   set @ContactAddID = @PickupAddID

   ELSE

        begin

     







if exists(select * from [tblAddress]  where 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) = rtrim(ltrim(ISNULL( @ContactcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [FirstName],'1'))) = rtrim(ltrim(ISNULL(  @ContactfirstName,'1'))) and 

rtrim(ltrim(ISNULL( [LastName],'1'))) =  rtrim(ltrim(ISNULL( @ContactlastName,'1'))) and 

rtrim(ltrim(ISNULL( [Address1],'1'))) =  rtrim(ltrim(ISNULL( @Contactaddress1,'1'))) and 

rtrim(ltrim(ISNULL( [Address2],'1'))) =  rtrim(ltrim(ISNULL( @Contactaddress2 ,'1'))) and 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) =  rtrim(ltrim(ISNULL( @ContactcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [Suburb],'1'))) =  rtrim(ltrim(ISNULL( @Contactsuburb ,'1'))) and 

rtrim(ltrim(ISNULL( [StateID],'1'))) =  rtrim(ltrim(ISNULL( @ContactstateId,'1'))) and 

rtrim(ltrim(ISNULL( [Phone],'1'))) =  rtrim(ltrim(ISNULL( @Contactphone,'1'))) and 

rtrim(ltrim(ISNULL( [PostCode],'1'))) =  rtrim(ltrim(ISNULL( @ContactpostalCode,'1'))) and ISNULL([UserID],'1')=ISNULL(@ContactuserId,'1'))

begin

select @ContactAddID = AddressID from [tblAddress]  where 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) = rtrim(ltrim(ISNULL( @ContactcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [FirstName],'1'))) =rtrim(ltrim(ISNULL(  @ContactfirstName ,'1'))) and 

rtrim(ltrim(ISNULL( [LastName],'1') ))= rtrim(ltrim(ISNULL( @ContactlastName ,'1'))) and 

rtrim(ltrim(ISNULL( [Address1],'1'))) =rtrim(ltrim(ISNULL(  @Contactaddress1,'1'))) and 

rtrim(ltrim(ISNULL( [Address2],'1'))) =rtrim(ltrim(ISNULL(  @Contactaddress2 ,'1'))) and 

rtrim(ltrim(ISNULL( [CompanyNAme],'1'))) =rtrim(ltrim(ISNULL(  @ContactcompanyName,'1'))) and 

rtrim(ltrim(ISNULL( [Suburb],'1') ))= rtrim(ltrim(ISNULL( @Contactsuburb ,'1'))) and 

rtrim(ltrim(ISNULL( [StateID],'1'))) = rtrim(ltrim(ISNULL( @ContactstateId ,'1'))) and 

rtrim(ltrim(ISNULL( [Phone],'1'))) =  rtrim(ltrim(ISNULL( @Contactphone,'1'))) and 

rtrim(ltrim(ISNULL( [PostCode],'1') ))=rtrim(ltrim(ISNULL(  @ContactpostalCode,'1'))) and ISNULL([UserID],'1')=ISNULL(@ContactuserId,'1')

end

else



begin

	INSERT INTO [dbo].[tblAddress]

	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,

	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe] 

	 	 ,[Country],[CountryCode]  )

	 VALUES

      ( @ContactuserId,@ContactfirstName ,@ContactlastName ,@ContactcompanyName,@Contactemail,@Contactaddress1,@Contactaddress2,@Contactsuburb,@ContactstateId,

      @ContactpostalCode,@Contactphone,@Contactmobile,@ContactisRegisterAddress,GETDATE(), @ContactcreatedBy ,@ContactisBusiness,@ContactIsSubscribe

      ,@ContactCountry,@ContactCountryCode)

        SET @ContactAddID = SCOPE_IDENTITY()

end

        end

   END ;





	

        ----------------------------------------------------

        

        

				DECLARE @ConsignmentIDret int
				IF ( Isnull(@SortCode, '') = '' ) 
				BEGIN	
					SELECT TOP 1 @SortCode = sortcode FROM locationmaster 
					WHERE postcode = @DestinationpostalCode 
				END

			INSERT INTO [dbo].tblConsignmentStaging

           (

            [UserID]

           ,[IsRegUserConsignment]

           ,[PickupID]

           ,[DestinationID]

           ,[ContactID]

           ,[TotalWeight]

           ,[TotalVolume]

           ,[NoOfItems]

           ,[SpecialInstruction]

           ,[CustomerRefNo]

           ,[PickupDate]

           ,[PreferPickupTime]

           ,[ClosingTime]

           ,[DangerousGoods]

           ,[Terms]

           ,[RateCardID]

           ,[CreatedDateTime]

           ,[CreatedBy]

           ,[ServiceID]

           ,[IsProcessed]

           ,[IsDocument]

           ,IsSignatureReq

           ,IfUndelivered

           ,ReasonForExport

           ,TypeOfExport

           ,Currency

           ,IsInsurance

           ,IdentityNo  

			,IdentityType

			,IsIdentity  

,IsATl  

,IsReturnToSender   

,HasReadInsuranceTc  

,NatureOfGoods

,SortCode

           ,ETA

           ,InsuranceAmount

            ,CalculatedTotal

           ,CalculatedGST 

           )

     VALUES

           (

           @UserID,

           @IsRegUserConsignment,

              @PickupAddID,

           @DestinationAddID,

           @ContactAddID,

           @TotalWeight,

           @TotalVolume,

           @NoOfItems,

           @SpecialInstruction,

           @CustomerRefNo,

           @ConsignmentPreferPickupDate,

           @ConsignmentPreferPickupTime,

           @ClosingTime,

           @DangerousGoods,

           @Terms,

           @RateCardID,

           GETDATE(),

           @CreatedBy,

           @ServiceID,

           0,

           @IsDocument  ,

         @IsSignatureReq  ,

         @IfUndelivered

           ,@ReasonForExport

           ,@TypeOfExport

           ,@Currency 

           ,@IsInsurance

           ,@IdentityNo  

			,@IdentityType

			,@IsIdentity  

			,@IsATl  

			,@IsReturnToSender    

			,@HasReadInsuranceTc  

			,@NatureOfGoods

			,@SortCode

        ,@ETA

        ,@InsuranceAmount

        ,@GrossTotal

        ,@GST 

           ) SET @ConsignmentIDret = SCOPE_IDENTITY()

			select @ConsignmentIDret



	--------------------------------

			

			 if(@@ERROR<>0)

  begin

  rollback tran

  

  INSERT INTO [dbo].[tblErrorLog]

           ([Error]

           ,[FunctionInfo]

           ,[ClientId])

     VALUES

           (ERROR_LINE() +' : '+ERROR_MESSAGE()

           ,'[SPCPPL_CreateConsignmentStagingSing]'

           , 2)

  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2

  end

  else

  commit tran          

            



END
GO
