SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE  PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentStagingTran_BUP_28_09_2015]
           @UserID int= null,
           @IsRegUserConsignment bit= null,
           --@PickupID int= null,
           --@DestinationID int= null,
           --@ContactID int= null,
           @TotalWeight varchar(500)= null,
           @TotalVolume varchar(500)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @CreatedBy int= null,
           @ServiceID int= null,
           
           
@PickupuserId int=null,
@PickupfirstName nvarchar(50)=null,
@PickuplastName nvarchar(50)=null,
@PickupcompanyName nvarchar(100)=null,
@Pickupemail nvarchar(250)=null,
@Pickupaddress1 nvarchar(200)=null,
@Pickupaddress2 nvarchar(200)=null,
@Pickupsuburb nvarchar(100)=null,
@PickupstateId nvarchar(max)=null,
@PickuppostalCode nvarchar(20)=null,
@Pickupphone nvarchar(20)=null,
@Pickupmobile nvarchar(20)=null,
@PickupcreatedBy int=null,
@PickupisBusiness bit=0,
@PickupisRegisterAddress bit=0,
@PickupIsSubscribe bit=0,

@DestinationuserId int=null,
@DestinationfirstName nvarchar(50)=null,
@DestinationlastName nvarchar(50)=null,
@DestinationcompanyName nvarchar(100)=null,
@Destinationemail nvarchar(250)=null,
@Destinationaddress1 nvarchar(200)=null,
@Destinationaddress2 nvarchar(200)=null,
@Destinationsuburb nvarchar(100)=null,
@DestinationstateId nvarchar(max)=null,
@DestinationpostalCode nvarchar(20)=null,
@Destinationphone nvarchar(20)=null,
@Destinationmobile nvarchar(20)=null,
@DestinationcreatedBy int=null,
@DestinationisBusiness bit=0,
@DestinationisRegisterAddress bit=0,
@DestinationIsSubscribe bit=0,

@ContactuserId int=null,
@ContactfirstName nvarchar(50)=null,
@ContactlastName nvarchar(50)=null,
@ContactcompanyName nvarchar(100)=null,
@Contactemail nvarchar(250)=null,
@Contactaddress1 nvarchar(200)=null,
@Contactaddress2 nvarchar(200)=null,
@Contactsuburb nvarchar(100)=null,
@ContactstateId nvarchar(max)=null,
@ContactpostalCode nvarchar(20)=null,
@Contactphone nvarchar(20)=null,
@Contactmobile nvarchar(20)=null,
@ContactcreatedBy int=null,
@ContactisBusiness bit=0,
@ContactisRegisterAddress bit=0,
@ContactIsSubscribe bit=0,


@IsSameAsDestination bit = 0,
@IsSameAsPickup bit = 0,

@IsSignatureReq bit = 0,
@IsDocument nvarchar(max) = null,
@NatureOfGoods nvarchar(max) = null,
@SortCode nvarchar(max) = null,
@ETA nvarchar(max) = null,
@IsATL               bit = null,
@GrossTotal decimal(18,2) = null,
@GST  decimal(18,2)  = null
           
      
AS
BEGIN
begin tran

DECLARE @stateIdbyCode int

-------------------------------------------------
			SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @DestinationstateId
DECLARE @DestinationAddID int

if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@DestinationcompanyName)) and 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @DestinationfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@DestinationlastName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Destinationaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Destinationaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =  rtrim(ltrim(@DestinationcompanyName)) and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Destinationsuburb ))and 
rtrim(ltrim([StateID])) =  rtrim(ltrim(@stateIdbyCode)) and 
rtrim(ltrim([PostCode])) =  rtrim(ltrim(@DestinationpostalCode)) and [UserID]=@DestinationuserId)
begin
select @DestinationAddID = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@DestinationcompanyName)) and 
rtrim(ltrim([FirstName])) =rtrim(ltrim( @DestinationfirstName ))and 
rtrim(ltrim([LastName] ))= rtrim(ltrim(@DestinationlastName ))and 
rtrim(ltrim([Address1])) =rtrim(ltrim( @Destinationaddress1)) and 
rtrim(ltrim([Address2])) =rtrim(ltrim( @Destinationaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =rtrim(ltrim( @DestinationcompanyName)) and 
rtrim(ltrim([Suburb] ))= rtrim(ltrim(@Destinationsuburb ))and 
rtrim(ltrim([StateID])) = rtrim(ltrim(@stateIdbyCode ))and 
rtrim(ltrim([PostCode] ))=rtrim(ltrim( @DestinationpostalCode)) and [UserID]=@DestinationuserId
end
else

begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[stateName]  )
	 VALUES
      ( @DestinationuserId,@DestinationfirstName ,@DestinationlastName ,@DestinationcompanyName,@Destinationemail,@Destinationaddress1,@Destinationaddress2,@Destinationsuburb, @stateIdbyCode ,
      @DestinationpostalCode,@Destinationphone,@Destinationmobile,@DestinationisRegisterAddress,GETDATE(), @DestinationcreatedBy ,@DestinationisBusiness,@DestinationIsSubscribe,@DestinationstateId)
        SET @DestinationAddID = SCOPE_IDENTITY()
end
			--select @DestinationAddID
			-------------------------------------------------
				SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @PickupstateId
DECLARE @PickupAddID int


if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@PickupcompanyName)) and 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @PickupfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@PickuplastName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Pickupaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Pickupaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =  rtrim(ltrim(@PickupcompanyName)) and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Pickupsuburb ))and 
rtrim(ltrim([StateID])) =  rtrim(ltrim(@stateIdbyCode)) and 
rtrim(ltrim([PostCode])) =  rtrim(ltrim(@PickuppostalCode)) and [UserID]=@PickupuserId)
begin
select @PickupAddID = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@PickupcompanyName)) and 
rtrim(ltrim([FirstName])) =rtrim(ltrim( @PickupfirstName ))and 
rtrim(ltrim([LastName] ))= rtrim(ltrim(@PickuplastName ))and 
rtrim(ltrim([Address1])) =rtrim(ltrim( @Pickupaddress1)) and 
rtrim(ltrim([Address2])) =rtrim(ltrim( @Pickupaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =rtrim(ltrim( @PickupcompanyName)) and 
rtrim(ltrim([Suburb] ))= rtrim(ltrim(@Pickupsuburb ))and 
rtrim(ltrim([StateID])) = rtrim(ltrim(@stateIdbyCode ))and 
rtrim(ltrim([PostCode] ))=rtrim(ltrim( @PickuppostalCode)) and [UserID]=@PickupuserId
end
else

begin

	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[stateName] )
	 VALUES
      ( @PickupuserId,@PickupfirstName ,@PickuplastName ,@PickupcompanyName,@Pickupemail,@Pickupaddress1,@Pickupaddress2,@Pickupsuburb, @stateIdbyCode ,
      @PickuppostalCode,@Pickupphone,@Pickupmobile,@PickupisRegisterAddress,GETDATE(), @PickupcreatedBy ,@PickupisBusiness,@PickupIsSubscribe,@PickupstateId)
        SET @PickupAddID = SCOPE_IDENTITY()
end
			--select @PickupAddID
			-------------------------------------------------
			---------------------

DECLARE @ContactAddID int


IF (@IsSameAsDestination=1)
   set @ContactAddID = @DestinationAddID
ELSE 
   BEGIN
      IF (@IsSameAsPickup=1)
   set @ContactAddID = @PickupAddID
   ELSE
        begin
        SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @ContactstateId


if exists(select * from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@ContactcompanyName)) and 
rtrim(ltrim([FirstName])) = rtrim(ltrim( @ContactfirstName)) and 
rtrim(ltrim([LastName])) =  rtrim(ltrim(@ContactlastName)) and 
rtrim(ltrim([Address1])) =  rtrim(ltrim(@Contactaddress1)) and 
rtrim(ltrim([Address2])) =  rtrim(ltrim(@Contactaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =  rtrim(ltrim(@ContactcompanyName)) and 
rtrim(ltrim([Suburb])) =  rtrim(ltrim(@Contactsuburb ))and 
rtrim(ltrim([StateID])) =  rtrim(ltrim(@stateIdbyCode)) and 
rtrim(ltrim([PostCode])) =  rtrim(ltrim(@ContactpostalCode)) and [UserID]=@ContactuserId)
begin
select @ContactAddID = AddressID from [tblAddress] where 
rtrim(ltrim([CompanyNAme])) = rtrim(ltrim(@ContactcompanyName)) and 
rtrim(ltrim([FirstName])) =rtrim(ltrim( @ContactfirstName ))and 
rtrim(ltrim([LastName] ))= rtrim(ltrim(@ContactlastName ))and 
rtrim(ltrim([Address1])) =rtrim(ltrim( @Contactaddress1)) and 
rtrim(ltrim([Address2])) =rtrim(ltrim( @Contactaddress2 ))and 
rtrim(ltrim([CompanyNAme])) =rtrim(ltrim( @ContactcompanyName)) and 
rtrim(ltrim([Suburb] ))= rtrim(ltrim(@Contactsuburb ))and 
rtrim(ltrim([StateID])) = rtrim(ltrim(@stateIdbyCode ))and 
rtrim(ltrim([PostCode] ))=rtrim(ltrim( @ContactpostalCode)) and [UserID]=@ContactuserId
end
else

begin
	INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe],[stateName]  )
	 VALUES
      ( @ContactuserId,@ContactfirstName ,@ContactlastName ,@ContactcompanyName,@Contactemail,@Contactaddress1,@Contactaddress2,@Contactsuburb,@stateIdbyCode ,
      @ContactpostalCode,@Contactphone,@Contactmobile,@ContactisRegisterAddress,GETDATE(), @ContactcreatedBy ,@ContactisBusiness,@ContactIsSubscribe,@ContactstateId)
        SET @ContactAddID = SCOPE_IDENTITY()
end
        end
   END ;


			--select @ContactAddID
			
			-------------------------------------------------
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].tblConsignmentStaging
           (
            [UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[PickupDate]
           ,[PreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[ServiceID]
           ,[IsProcessed]
            ,[IsDocument]
           ,IsSignatureReq
           ,NatureOfGoods
           ,SortCode
           ,ETA
           ,IsATL
           ,CalculatedTotal
           ,CalculatedGST 
           )
     VALUES
           (
           @UserID,
           @IsRegUserConsignment,
             @PickupAddID,
           @DestinationAddID,
           @ContactAddID,
           @TotalWeight,
           @TotalVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           GETDATE(),
           @CreatedBy,
           @ServiceID,
           0,
          @IsDocument   ,
        @IsSignatureReq    
        ,@NatureOfGoods
        ,@SortCode
        ,@ETA
        ,@IsATL
        ,@GrossTotal
        ,@GST 
           ) SET @ConsignmentIDret = SCOPE_IDENTITY()
			select @ConsignmentIDret
			
			
			--------------------------------
			
			 if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateConsignmentStagingTran]'
           , 2)
  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
  end
  else
  commit tran          
            

END


GO
