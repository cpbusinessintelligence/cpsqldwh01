SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentUsingBulkDHLWithoutPay_BUP_27_10_2016]
      
   --             @TotalMeasureWeight decimal(10,2)= null,
   --@TotalMeasureVolume decimal(10,2)= null,
   @ProntoDataExtracted bit= null,
   @LastActivity varchar(200)= null,
     @LastActiivityDateTime datetime= null,
  @EDIDataProcessed bit= null,
   --@ConsignmentStatus int= null,
    @ConsignmentCode varchar(40)= null,
  @ConsignmentStagingID int= null,
        
@GrossTotal decimal(19,4) ,
 @GST decimal(19,4) ,
           @NetTotal decimal(19,4) ,
           
           @dtItemLabel dtItemLabelDHL READONLY ,
          
           @dtCustomDeclaration dtCustomDeclaration READONLY,
           
                      @TotalFreightExGST decimal(19,4) = null,
         
           @TotalFreightInclGST decimal(19,4) = null,
           @InvoiceStatus int = null,
           @SendToPronto bit = null,
           @PaymentRefNo nvarchar(100)=null,
           @merchantReferenceCode nvarchar(max)=null,
           @SubscriptionID nvarchar(max)=null,
           @AuthorizationCode nvarchar(100)=null,
           
           @InvoiceImage varchar(max)=null,
           @LabelImage varchar(max)=null,
           @ItemCodeSing varchar(max)=null,
           
           @AWBBarCode varchar(max)=null,
           @OriginDestnBarcode varchar(max)=null,
           @ClientIDBarCode varchar(max)=null,
           @DHLRoutingBarCode varchar(max)=null,
           
           @AWBCode varchar(max)=null,
           @OriginDestncode varchar(max)=null,
           @ClientIDCode varchar(max)=null,
           @DHLRoutingCode varchar(max)=null,
           
           @CountryServiceAreaFacilityCode varchar(max)=null,
           @OriginServiceAreaCode varchar(max)=null,
           @ProductShortName varchar(max)=null,
           
           @InternalServiceCode varchar(max)=null,
           @NetSubTotal decimal(10,2)=null,
           @NatureOfGoods nvarchar(max)=null
AS
BEGIN
begin tran

declare    @UserID int= null,
           @IsRegUserConsignment bit= null,
           @PickupID int= null,
           @DestinationID int= null,
           @ContactID int= null,
           @TotalWeight decimal(10,2)= null,
           @TotalVolume decimal(10,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @CreatedBy int= null,

@IsSignatureReq bit = 0,
@IsDocument nvarchar(max) = null,


@IfUndelivered nvarchar(max)=null,
@ReasonForExport nvarchar(max)=null,
@TypeOfExport nvarchar(max)=null,
@Currency nvarchar(max)=null,
@IsInsurance bit=null,

@IdentityNo   nvarchar(max)=null,
@IdentityType nvarchar(max)=null,
@IsIdentity   bit=null,
@IsATl  bit=null,
@IsReturnToSender   bit=null,
@HasReadInsuranceTc   bit=null,
@SortCode nvarchar(max) = null,
@ETA nvarchar(max) = null,
@InsuranceAmount decimal(10,2) = null



SELECT 
      @UserID =[UserID]
      ,@IsRegUserConsignment=[IsRegUserConsignment]
      ,@PickupID=[PickupID]
      ,@DestinationID=[DestinationID]
      ,@ContactID=[ContactID]
      ,@TotalWeight=[TotalWeight]
      ,@TotalVolume=[TotalVolume]
      ,@NoOfItems=[NoOfItems]
      ,@SpecialInstruction=[SpecialInstruction]
      ,@CustomerRefNo=[CustomerRefNo]
      ,@ConsignmentPreferPickupDate=[PickupDate]
      ,@ConsignmentPreferPickupTime=[PreferPickupTime]
      ,@ClosingTime=[ClosingTime]
      ,@DangerousGoods=[DangerousGoods]
      ,@Terms=[Terms]
      ,@RateCardID=[RateCardID]
      ,@CreatedBy = [CreatedBy]
       ,@IsSignatureReq =[IsSignatureReq]
      ,@IsDocument =[IsDocument],
      @IfUndelivered   =[IfUndelivered],
@ReasonForExport =[ReasonForExport],
@TypeOfExport =[TypeOfExport],
@Currency =[Currency],
@IsInsurance = [IsInsurance],

@IdentityNo = [IdentityNo],
@IdentityType= [IdentityType],
@IsIdentity  = [IsIdentity],
@IsATl=[IsATl]  ,
@IsReturnToSender=   [IsReturnToSender],
@HasReadInsuranceTc =  [HasReadInsuranceTc]  ,
@SortCode = [SortCode],
@NatureOfGoods=NatureOfGoods,
@ETA = [ETA],
@InsuranceAmount=InsuranceAmount
  FROM [dbo].[tblConsignmentStaging] where ConsignmentStagingID = @ConsignmentStagingID




--declare @ConsignmentCode9Digit nvarchar(9)
-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,6) = @ConsignmentCode
--SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
----SELECT @ConsignmentCode9Digit
--if(@ConsignmentCode9Digit is null)
--set @ConsignmentCode9Digit = '000000000'



declare @ConsignmentCode9Digit nvarchar(9)
  if (LEN(@ConsignmentCode)=7)
   begin
				select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,7) = @ConsignmentCode
				SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
				--SELECT @ConsignmentCode9Digit
				if(@ConsignmentCode9Digit is null)
				set @ConsignmentCode9Digit = '000000000'
				 --SELECT @ConsignmentCode9Digit
	 end	
  else
	 begin
				select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,6) = @ConsignmentCode
				SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
				--SELECT @ConsignmentCode9Digit
				if(@ConsignmentCode9Digit is null)
				set @ConsignmentCode9Digit = '000000000'
				 --SELECT @ConsignmentCode9Digit
	 end	












--SELECT @ConsignmentCode9Digit

--declare @ConsignmentCode9Digit nvarchar(9)
-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where ConsignmentCode =-- @ConsignmentCode
--SELECT @ConsignmentCode9Digit =  RIGHT('00000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode8Digit
--declare @UserId6Digit nvarchar(6)
--if(@UserID is null or @UserID ='')
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(ABS(Checksum(NewID()) ),'-',''), 6)
--end
--else
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(@UserID,'-',''), 6)
--end


			
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].[tblConsignment]
           ([ConsignmentCode]
           ,[UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           --,[TotalMeasureWeight]
           ,[TotalVolume]
           --,[TotalMeasureVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[ConsignmentPreferPickupDate]
           ,[ConsignmentPreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[LastActivity]
           ,[LastActiivityDateTime]
           ,[ConsignmentStatus]
           ,[EDIDataProcessed]
           ,[ProntoDataExtracted]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[IsInternational]
           ,[IsDocument]
           ,IsSignatureReq,
            [IfUndelivered],
			 [ReasonForExport],
			 [TypeOfExport],
			 [Currency],
			 [IsInsurance],
			 [IdentityNo],
			 [IdentityType],
			 [IsIdentity],
			 [Country-ServiceArea-FacilityCode],
			 [InternalServiceCode],
			 [NetSubTotal],
		 [IsATl]  ,
  [IsReturnToSender],
  [HasReadInsuranceTc]   ,
  [NatureOfGoods],
  [OriginServiceAreaCode],
  [ProductShortName],
  [SortCode]
           ,ETA,
           IsAccountCustomer
           ,InsuranceAmount
           ,CalculatedTotal
           ,CalculatedGST
		   ,[IsProcessed]
           )
     VALUES
           (@ConsignmentCode+@ConsignmentCode9Digit,
           @UserID,
           @IsRegUserConsignment,
           @PickupID,
           @DestinationID,
           @ContactID,
           @TotalWeight,
           --@TotalMeasureWeight,
           @TotalVolume,
           --@TotalMeasureVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           @LastActivity,
           @LastActiivityDateTime,
           1,
             0,
           0,
           GETDATE(),
           @CreatedBy,
           1,
             @IsDocument  ,
         @IsSignatureReq
         ,   @IfUndelivered    ,
			@ReasonForExport  ,
			@TypeOfExport  ,
			@Currency   ,
			@IsInsurance,
			@IdentityNo  ,
			@IdentityType ,
			@IsIdentity  ,
			 @CountryServiceAreaFacilityCode,
			 @InternalServiceCode,
			 @NetSubTotal,
			 @IsATl   ,
@IsReturnToSender ,
@HasReadInsuranceTc   ,
@NatureOfGoods,
@OriginServiceAreaCode,
@ProductShortName ,
@SortCode
        ,@ETA,1
           ,@InsuranceAmount
            ,@GrossTotal
        ,@GST, 1
) SET @ConsignmentIDret = SCOPE_IDENTITY()
 
			
---------ItemLabel ------------------------
      
      INSERT INTO [dbo].[tblItemLabel]
           ([ConsignmentID]
           ,[LabelNumber]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[CubicWeight]
           ,[PhysicalWeight]
           ,[MeasureWeight]
           ,[DeclareVolume]
           ,[LastActivity]
           ,[LastActivityDateTime]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[CountryOfOrigin]
           ,[Description]
           ,[HSTariffNumber]
           ,[Quantity]
           ,[UnitValue]
           ,[DHLBarCode]
           )
           SELECT
            @ConsignmentIDret, 
            --@ItemCodeSing,
        cast( strLabelNumber as nvarchar) ,
         --case when strLength='' then null else  cast(strLength as decimal(10,2))end,
        case when  ISNUMERIC(strLength)=1 then CAST(strLength AS decimal(10,2))else null end ,
        --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end,
        case when  ISNUMERIC(strWidth)=1 then CAST(strWidth AS decimal(10,2))else null end ,
         --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,
        case when  ISNUMERIC(strHeight)=1 then CAST(strHeight AS decimal(10,2))else null end ,
         --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end,
        case when  ISNUMERIC(strCubicWeight)=1 then CAST(strCubicWeight AS decimal(10,2))else null end ,
          --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,
        case when  ISNUMERIC(strPhysicalWeight)=1 then CAST(strPhysicalWeight AS decimal(10,2))else null end ,
        --case when strMeasureWeight='' then null else    cast(  strMeasureWeight as decimal(10,2))end,
        case when  ISNUMERIC(strMeasureWeight)=1 then CAST(strMeasureWeight AS decimal(10,2))else null end ,
          --case when strDeclareVolume='' then null else  cast(  strDeclareVolume as decimal(10,2))end,
        case when  ISNUMERIC(strDeclareVolume)=1 then CAST(strDeclareVolume AS decimal(10,2))else null end ,
             strLastActivity,
             strLastActivityDateTime,
             GETDATE(),
             strCreatedBy,
             [CountryOfOrigin]
           ,[Description]
           ,[HSTariffNumber]
           ,[Quantity]
           ,[UnitValue]
           ,[strDHLBarCode]
             FROM @dtItemLabel;
             --------------------------------Item Image ----------------------
     declare    @ItemLabelID int= null        
             select @ItemLabelID = ItemLabelID from [tblItemLabel] where [ConsignmentID] = @ConsignmentIDret
             
             INSERT INTO [dbo].[tblItemLabelImage]
           ([ItemLabelID]
           ,[LableImage]
           ,[CreatedBy]
           )
     VALUES
           (@ItemLabelID
           ,@LabelImage
           ,@UserID
          )

             
           
                     ---------CustomDeclaration ------------------------
             
               INSERT INTO [dbo].tblCustomDeclaration
           ([ConsignmentID]
           ,ItemDescription
           ,ItemInBox
           ,UnitPrice
           ,SubTotal
           ,HSCode
           ,CountryofOrigin
           ,Currency
           ,CreatedBy
           )
           select @ConsignmentIDret,
           ItemDescription,
           ItemInBox,
           UnitPrice,
           SubTotal,
           HSCode,
           CountryofOrigin,
           Currency,
           @UserID
            from @dtCustomDeclaration
            
            
            
           -----------------------insert [tblDHLBarCodeImage] -------
        if(@AWBBarCode is not null)
        begin
           INSERT INTO [dbo].[tblDHLBarCodeImage]
           ([ConsignmentID]
           ,[AWBCode]
           ,[AWBBarCode]
           ,[OriginDestncode]
           ,[OriginDestnBarcode]
           ,[ClientIDCode]
           ,[ClientIDBarCode]
           ,[DHLRoutingCode]
           ,[DHLRoutingBarCode]
          
           ,[CreatedBy])
     VALUES
           (@ConsignmentIDret
           ,@AWBCode 
           ,@AWBBarCode 
           ,@OriginDestncode 
           ,@OriginDestnBarcode 
           ,@ClientIDCode 
           ,@ClientIDBarCode 
           ,@DHLRoutingCode 
           ,@DHLRoutingBarCode 
          
           ,@UserID)
         end
           
           -------------------------------------------------------------
          
          
          ---------------------------------------------------------------------------------------------
 declare @ConsignmentId int = @ConsignmentIDret
 --------------------------------------------Commented By Shubham 16/05/2016--------------------------
			--UPDATE dbo.tblConsignment
   --SET [IsProcessed] =1,
   --   [UpdatedDateTTime] = getDate(),
   --   [UpdatedBy] = @UserID
    --WHERE ConsignmentID = @ConsignmentId
 
 -----------------------------------------------------------------------------------------------------------

 	UPDATE dbo.[tblInvoice]
   SET [PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTime] =GETDATE()
      ,[UpdatedBy] = @UserID
      
 WHERE invoicenumber = (select top 1 InvoiceNo from dbo.tblSalesOrder where ReferenceNo=@ConsignmentId)

 --------------------------------------------------Commented By Shubham 16/05/2016------------------------------------------
 --UPDATE dbo.tblConsignmentStaging
 --  SET [IsProcessed] =1,
 --     [UpdatedDateTTime] = getDate(),
 --     [UpdatedBy] = @CreatedBy      ,
 --     [ConsignmentId]=@ConsignmentIDret,
 --     IsAccountCustomer=1
 --WHERE ConsignmentStagingID = @ConsignmentStagingID

 ------------------------------------------Update Consignment Staging --------------------------------------------------------

 	UPDATE dbo.tblConsignmentstaging
 	set [IsProcessed] =1,[ConsignmentId]=@ConsignmentIDret,IsAccountCustomer=1,
[PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTTime] =GETDATE()
      ,[UpdatedBy] = @UserID
      where 
      ConsignmentStagingID = @ConsignmentStagingID
     
---------------------------------------------------------------------------------------------------------------- 
      
declare  
@PicupID int 
   
--SELECT     dbo.tblInvoice.*
--FROM         dbo.tblConsignment INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo INNER JOIN
--                      dbo.tblInvoice ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
                      
--                       where dbo.tblConsignment.ConsignmentID = @ConsignmentID
           
           SELECT     dbo.tblConsignment.*, dbo.tblConsignment.[Country-ServiceArea-FacilityCode] as Country_ServiceArea_FacilityCode
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID


--SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*
--FROM         dbo.tblSalesOrderDetail INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
--WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID)

				


			   SELECT   @PicupID = dbo.tblConsignment.PickupID,
			   @DestinationID=dbo.tblConsignment.DestinationID,
			   @ContactID=dbo.tblConsignment.ContactID
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID




            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe,case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID
 
 


  SELECT     Cu.UserID, c.IsRegularShipper
FROM         tblCompany c INNER JOIN
                      tblCompanyUsers Cu ON c.CompanyID = Cu.CompanyID
WHERE     (c.IsRegularShipper = 1)
and  Cu.UserID = @UserID
          
          
          
          
            
  if(@@ERROR<>0)
   begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'SPCPPL_CreateConsignmentUsingBulkSing'
           , 2)
  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
  end
  else
  commit tran          
            


            
            
END
GO
