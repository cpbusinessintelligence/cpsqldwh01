SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create  PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentUsingBulkNewProcessBulk_BUP_27_10_2016]
      
   --             @TotalMeasureWeight decimal(10,2)= null,
   --@TotalMeasureVolume decimal(10,2)= null,
   @ProntoDataExtracted bit= null,
   @LastActivity varchar(200)= null,
     @LastActiivityDateTime datetime= null,
  @EDIDataProcessed bit= null,
   --@ConsignmentStatus int= null,
    @ConsignmentCode varchar(40)= null,
  @ConsignmentStagingID int= null,
        
@GrossTotal decimal(19,4) ,
 @GST decimal(19,4) ,
           @NetTotal decimal(19,4) ,
           
           @dtItemLabel dtItemLabel READONLY ,
           @dtSalesOrderDetail dtSalesOrderDetail READONLY,
           
                      @TotalFreightExGST decimal(19,4) = null,
         
           @TotalFreightInclGST decimal(19,4) = null,
           @InvoiceStatus int = null,
           @SendToPronto bit = null,
           @PaymentRefNo nvarchar(100)=null,
           @merchantReferenceCode nvarchar(max)=null,
           @SubscriptionID nvarchar(max)=null,
           @AuthorizationCode nvarchar(100)=null,
            @InvoiceID int = null
AS
BEGIN
begin tran

declare    @UserID int= null,
           @IsRegUserConsignment bit= null,
           @PickupID int= null,
           @DestinationID int= null,
           @ContactID int= null,
           @TotalWeight decimal(10,2)= null,
           @TotalVolume decimal(10,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @CreatedBy int= null,

@IsSignatureReq bit = 0,
@IsATl  bit=null,
@IsDocument bit = 0,
@SortCode nvarchar(max) = null,
@NatureOfGoods nvarchar(max) = null,
@ETA nvarchar(max) = null




SELECT 
      @UserID =[UserID]
      ,@IsRegUserConsignment=[IsRegUserConsignment]
      ,@PickupID=[PickupID]
      ,@DestinationID=[DestinationID]
      ,@ContactID=[ContactID]
      ,@TotalWeight=[TotalWeight]
      ,@TotalVolume=[TotalVolume]
      ,@NoOfItems=[NoOfItems]
      ,@SpecialInstruction=[SpecialInstruction]
      ,@CustomerRefNo=[CustomerRefNo]
      ,@ConsignmentPreferPickupDate=[PickupDate]
      ,@ConsignmentPreferPickupTime=[PreferPickupTime]
      ,@ClosingTime=[ClosingTime]
      ,@DangerousGoods=[DangerousGoods]
      ,@Terms=[Terms]
      ,@RateCardID=[RateCardID]
       ,@IsATl=[IsATl]  
      ,@CreatedBy = [CreatedBy]
      ,@IsSignatureReq =[IsSignatureReq]
      ,@IsDocument =[IsDocument],
@SortCode = [SortCode],
@NatureOfGoods=NatureOfGoods,
@ETA = [ETA]
  FROM [dbo].[tblConsignmentStaging] where ConsignmentStagingID = @ConsignmentStagingID




declare @ConsignmentCode9Digit nvarchar(9)
 select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,6) = @ConsignmentCode
SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode9Digit
if(@ConsignmentCode9Digit is null)
set @ConsignmentCode9Digit = '000000000'
--SELECT @ConsignmentCode9Digit

--declare @ConsignmentCode9Digit nvarchar(9)
-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where ConsignmentCode =-- @ConsignmentCode
--SELECT @ConsignmentCode9Digit =  RIGHT('00000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode8Digit
--declare @UserId6Digit nvarchar(6)
--if(@UserID is null or @UserID ='')
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(ABS(Checksum(NewID()) ),'-',''), 6)
--end
--else
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(@UserID,'-',''), 6)
--end


			
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].[tblConsignment]
           ([ConsignmentCode]
           ,[UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           --,[TotalMeasureWeight]
           ,[TotalVolume]
           --,[TotalMeasureVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[ConsignmentPreferPickupDate]
           ,[ConsignmentPreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[LastActivity]
           ,[LastActiivityDateTime]
           ,[ConsignmentStatus]
           ,[EDIDataProcessed]
           ,[ProntoDataExtracted]
           ,[CreatedDateTime]
           ,[CreatedBy]
            ,[IsDocument]
           ,IsSignatureReq
           ,SortCode
           ,NatureOfGoods
           ,ETA
		  ,[IsATl] 
           )
     VALUES
           (@ConsignmentCode+@ConsignmentCode9Digit,
           @UserID,
           @IsRegUserConsignment,
           @PickupID,
           @DestinationID,
           @ContactID,
           @TotalWeight,
           --@TotalMeasureWeight,
           @TotalVolume,
           --@TotalMeasureVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           @LastActivity,
           @LastActiivityDateTime,
           1,
           0,
           0,
           GETDATE(),
           @CreatedBy,
            @IsDocument  ,
         @IsSignatureReq  ,
         @SortCode
         ,@NatureOfGoods
        ,@ETA
			 , @IsATl  
          ) SET @ConsignmentIDret = SCOPE_IDENTITY()


			UPDATE dbo.tblConsignmentStaging
   SET [IsProcessed] =1,
      [UpdatedDateTTime] = getDate(),
      [UpdatedBy] = @CreatedBy,
      [ConsignmentId]=@ConsignmentIDret
 WHERE ConsignmentStagingID = @ConsignmentStagingID
 
 
 
 DECLARE @SalesOrderIDret int
 INSERT INTO [dbo].[tblSalesOrder]
           ([ReferenceNo]
           ,[UserID]
           ,[NoofItems]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[RateCardID]
           ,[GrossTotal]
           ,[GST]
           ,[NetTotal]
           ,[SalesOrderStatus]
           ,[InvoiceNo]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
           VALUES
           ( 
           @ConsignmentIDret ,
           @UserID  ,
           @NoofItems  ,
           @TotalWeight  ,
           @TotalVolume  ,
           @RateCardID  ,
           @GrossTotal ,
           @GST  ,
           @NetTotal  ,
           7  ,
           null ,
           GETDATE() ,
          @UserID 
           )SET @SalesOrderIDret = SCOPE_IDENTITY()
		
 
 

  
  
 
 

			
			

      

            ---------ItemLabel ------------------------
      
      INSERT INTO [dbo].[tblItemLabel]
           ([ConsignmentID]
           ,[LabelNumber]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[CubicWeight]
           ,[PhysicalWeight]
           ,[MeasureWeight]
           ,[DeclareVolume]
           ,[LastActivity]
           ,[LastActivityDateTime]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
           SELECT
            @ConsignmentIDret, 
        @ConsignmentCode+@ConsignmentCode9Digit+  cast( strLabelNumber as nvarchar) ,
         --case when strLength='' then null else  cast(strLength as decimal(10,2))end,
        case when  ISNUMERIC(strLength)=1 then CAST(strLength AS decimal(10,2))else null end ,
        --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end,
        case when  ISNUMERIC(strWidth)=1 then CAST(strWidth AS decimal(10,2))else null end ,
         --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,
        case when  ISNUMERIC(strHeight)=1 then CAST(strHeight AS decimal(10,2))else null end ,
         --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end,
        case when  ISNUMERIC(strCubicWeight)=1 then CAST(strCubicWeight AS decimal(10,2))else null end ,
          --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,
        case when  ISNUMERIC(strPhysicalWeight)=1 then CAST(strPhysicalWeight AS decimal(10,2))else null end ,
        --case when strMeasureWeight='' then null else    cast(  strMeasureWeight as decimal(10,2))end,
        case when  ISNUMERIC(strMeasureWeight)=1 then CAST(strMeasureWeight AS decimal(10,2))else null end ,
          --case when strDeclareVolume='' then null else  cast(  strDeclareVolume as decimal(10,2))end,
        case when  ISNUMERIC(strDeclareVolume)=1 then CAST(strDeclareVolume AS decimal(10,2))else null end ,
             strLastActivity,
             strLastActivityDateTime,
             GETDATE(),
             strCreatedBy
             FROM @dtItemLabel;
             
            ---------SalesOrderDetail ------------------------
             
               INSERT INTO [dbo].[tblSalesOrderDetail]
           ([SalesOrderID]
           ,[Description]
           ,[LineNo]
           ,[Weight]
           ,[Volume]
           ,[FreightCharge]
           ,[FuelCharge]
           ,[InsuranceCharge]
           ,[ServiceCharge]
           ,[Total]
           ,[CreatedDateTime]
           ,[CreatedBy])
           select @SalesOrderIDret,
           strDescription,
           [strLineNo],
           --case when strPhysicalWeight='' then null else    cast( strPhysicalWeight as decimal(10,2))end,
        case when  ISNUMERIC(strPhysicalWeight)=1 then CAST(strPhysicalWeight AS decimal(10,2))else null end ,
          --case when strDeclareVolume='' then null else    cast(  strDeclareVolume as decimal(10,2))end,
        case when  ISNUMERIC(strDeclareVolume)=1 then CAST(strDeclareVolume AS decimal(10,2))else null end ,
           --case when strFreightCharge='' then null else    cast(  strFreightCharge as decimal(10,2))end,
        case when  ISNUMERIC(strFreightCharge)=1 then CAST(strFreightCharge AS decimal(10,2))else null end ,
           --case when strFuelCharge='' then null else    cast(  strFuelCharge as decimal(19,4))end,
        case when  ISNUMERIC(strFuelCharge)=1 then CAST(strFuelCharge AS decimal(19,4))else null end ,
          --case when strInsuranceCharge='' then null else    cast(   strInsuranceCharge as decimal(19,4))end,
        case when  ISNUMERIC(strInsuranceCharge)=1 then CAST(strInsuranceCharge AS decimal(19,4))else null end ,
         --case when strServiceCharge='' then null else    cast(    strServiceCharge as decimal(19,4))end,
        case when  ISNUMERIC(strServiceCharge)=1 then CAST(strServiceCharge AS decimal(19,4))else null end ,
           --cast (strFreightCharge as decimal(19,4)) +
        case when  ISNUMERIC(strFreightCharge)=1 then CAST(strFreightCharge AS decimal(19,4))else 0.0 end +
           --cast (strFuelCharge as decimal(19,4))+
        case when  ISNUMERIC(strFuelCharge)=1 then CAST(strFuelCharge AS decimal(19,4))else 0.0 end +
           --cast (strInsuranceCharge as decimal(19,4))+
        case when  ISNUMERIC(strInsuranceCharge)=1 then CAST(strInsuranceCharge AS decimal(19,4))else 0.0 end +
           --cast (strServiceCharge as decimal(19,4)),
        case when  ISNUMERIC(strServiceCharge)=1 then CAST(strServiceCharge AS decimal(19,4))else 0.0 end,
           getdate(),
           strCreatedBy
           
            from @dtSalesOrderDetail
            
            
            
            ---------Invoice ------------------------
            
          declare  @InvoiceNumber nvarchar(5)
          set @InvoiceNumber=  SUBSTRING(@ConsignmentCode, 1, 3)+'IN'
            declare @InvoiceCode7Digit nvarchar(7)
 select @InvoiceCode7Digit = max (RIGHT(InvoiceNumber, 7))+1  from tblInvoice where left(InvoiceNumber,5) = @InvoiceNumber
SELECT @InvoiceCode7Digit =  RIGHT('0000000' + replace(@InvoiceCode7Digit,'-',''),7)
--SELECT @ConsignmentCode9Digit
if(@InvoiceCode7Digit is null)
set @InvoiceCode7Digit = '0000000'

 declare  @InvoiceNumbertosave nvarchar(max)= @InvoiceNumber+@InvoiceCode7Digit
--select @UserId5Digit+@ConsignmentCode8Digit

				--DECLARE @InvoiceID int
			if(@InvoiceID is null)
				begin
                  INSERT INTO [dbo].[tblInvoice]
           ([InvoiceNumber]
           ,[UserID]
           ,[PickupAddressID]
           ,[DestinationAddressID]
           ,[ContactAddressID]
           ,[TotalFreightExGST]
           ,[GST]
           ,[TotalFreightInclGST]
           ,[InvoiceStatus]
           ,[SendToPronto]
           ,[CreatedDateTime]
           ,[CreatedBY]
           ,[PaymentRefNo]
           ,[AuthorizationCode]
           ,[merchantReferenceCode]
           ,[SubscriptionID]
         )
     VALUES
           (      @InvoiceNumbertosave , 
           @UserID , 
           @PickupID,
		  @DestinationID,
		  @ContactID ,
           --@TotalFreightExGST , 
           @GrossTotal,
           @GST , 
           --@TotalFreightInclGST , 
           @NetTotal,
           11 , 
           @SendToPronto , 
           GETDATE() , 
           @CreatedBY ,
           @PaymentRefNo,
           @AuthorizationCode,
           @merchantReferenceCode,
           @SubscriptionID
           )SET @InvoiceID = SCOPE_IDENTITY()
                end
           else
				begin
				update  [dbo].[tblInvoice] set 
						[TotalFreightExGST] = [TotalFreightExGST]+   @GrossTotal,
						[GST]                =  [GST]+  @GST , 
						[TotalFreightInclGST]  = [TotalFreightInclGST]+   @NetTotal
						where 
						InvoiceID =@InvoiceID 
						
						select @InvoiceNumbertosave = [InvoiceNumber]  from [dbo].[tblInvoice]
						where 
						InvoiceID =@InvoiceID 
                end
           
           --------Update Salse order -------
           UPDATE dbo.tblConsignmentStaging
   SET [PaymentRefNo] =@PaymentRefNo 
 WHERE ConsignmentStagingID = @ConsignmentStagingID
           --------Update Salse order -------
           
           update tblSalesOrder set InvoiceNo =  @InvoiceNumbertosave ,
           SalesOrderStatus = 9
           where SalesOrderID=  @SalesOrderIDret
           
        
           SELECT     @ConsignmentIDret
           SELECT     @InvoiceID







            
  if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'SPCPPL_CreateConsignmentUsingBulk'
           , 2)
  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
  end
  else
  commit tran          
            
           

            
            
END
GO
