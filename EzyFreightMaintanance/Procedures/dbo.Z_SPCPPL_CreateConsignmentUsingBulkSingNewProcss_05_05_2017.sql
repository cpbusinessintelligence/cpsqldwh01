SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentUsingBulkSingNewProcss_05_05_2017] 

--@TotalMeasureWeight decimal(10,2)= null,
--@TotalMeasureVolume decimal(10,2)= null,
@ProntoDataExtracted bit= NULL,
@LastActivity varchar(200)= NULL,
@LastActiivityDateTime datetime= NULL,
@EDIDataProcessed bit= NULL,
--@ConsignmentStatus int= null,
@ConsignmentCode varchar(40)= NULL,
@ConsignmentStagingID int= NULL,
@GrossTotal decimal(19,4),
@GST decimal(19,4),
@NetTotal decimal(19,4),
@dtItemLabel dtItemLabelSing READONLY,
@dtSalesOrderDetail dtSalesOrderDetail READONLY,
@dtCustomDeclaration dtCustomDeclaration READONLY,
@TotalFreightExGST decimal(19,4) = NULL,
@TotalFreightInclGST decimal(19,4) = NULL,
@InvoiceStatus int = NULL,
@SendToPronto bit = NULL,
@PaymentRefNo nvarchar(100)=NULL,
@merchantReferenceCode nvarchar(MAX)=NULL,
@SubscriptionID nvarchar(MAX)=NULL,
@AuthorizationCode nvarchar(100)=NULL,
@InvoiceImage varchar(MAX)=NULL,
@LabelImage varchar(MAX)=NULL,
@ItemCodeSing varchar(MAX)=NULL,
@AWBBarCode varchar(MAX)=NULL,
@OriginDestnBarcode varchar(MAX)=NULL,
@ClientIDBarCode varchar(MAX)=NULL,
@DHLRoutingBarCode varchar(MAX)=NULL,
@NetSubTotal decimal(10,2)=NULL,
@SenderReference nvarchar(MAX) = NULL,
@NatureOfGoods nvarchar(MAX)=NULL 
AS 

BEGIN 
BEGIN TRY

BEGIN tran 

DECLARE @UserID int= NULL,
@IsRegUserConsignment bit= NULL,
@PickupID int= NULL,
@DestinationID int= NULL,
@ContactID int= NULL,
@TotalWeight decimal(10,2)= NULL,
@TotalVolume decimal(10,2)= NULL,
@NoOfItems int= NULL,
@SpecialInstruction varchar(500)= NULL,
@CustomerRefNo varchar(40)= NULL,
@ConsignmentPreferPickupDate date= NULL,
@ConsignmentPreferPickupTime varchar(20)= NULL,
@ClosingTime varchar(10)= NULL,
@DangerousGoods bit= NULL,
@Terms bit= NULL,
@RateCardID nvarchar(50)= NULL,
@CreatedBy int= NULL,
@IsSignatureReq bit = 0,
@IsDocument nvarchar(MAX) = NULL,
@IfUndelivered nvarchar(MAX)=NULL,
@ReasonForExport nvarchar(MAX)=NULL,
@TypeOfExport nvarchar(MAX)=NULL,
@Currency nvarchar(MAX)=NULL,
@IsInsurance bit=NULL,
@IdentityNo nvarchar(MAX)=NULL,
@IdentityType nvarchar(MAX)=NULL,
@IsIdentity bit=NULL,
@IsATl bit=NULL,
@IsReturnToSender bit=NULL,
@HasReadInsuranceTc bit=NULL,
@SortCode nvarchar(MAX) = NULL,
@ETA nvarchar(MAX) = NULL,
@CountryCode nvarchar(MAX) = NULL


SELECT @UserID =[UserID],
@IsRegUserConsignment=[IsRegUserConsignment],
@PickupID=[PickupID],
@DestinationID=[DestinationID],
@ContactID=[ContactID],
@TotalWeight=[TotalWeight],
@TotalVolume=[TotalVolume],
@NoOfItems=[NoOfItems],
@SpecialInstruction=[SpecialInstruction],
@CustomerRefNo=[CustomerRefNo],
@ConsignmentPreferPickupDate=[PickupDate],
@ConsignmentPreferPickupTime=[PreferPickupTime],
@ClosingTime=[ClosingTime],
@DangerousGoods=[DangerousGoods],
@Terms=[Terms],
@RateCardID=[RateCardID],
@CreatedBy = [CreatedBy],
@IsSignatureReq =[IsSignatureReq],
@IsDocument =[IsDocument],
@IfUndelivered =[IfUndelivered],
@ReasonForExport =[ReasonForExport],
@TypeOfExport =[TypeOfExport],
@Currency =[Currency],
@IsInsurance = [IsInsurance],
@IdentityNo = [IdentityNo],
@IdentityType= [IdentityType],
@IsIdentity = [IsIdentity],
@IsATl=[IsATl],
@IsReturnToSender= [IsReturnToSender],
@HasReadInsuranceTc = [HasReadInsuranceTc],
@SortCode = [SortCode],
@NatureOfGoods=NatureOfGoods,
@ETA = [ETA]
FROM [dbo].[tblConsignmentStaging]
WHERE ConsignmentStagingID = @ConsignmentStagingID 


DECLARE @GeneratedConsignmentCode nvarchar(30)
ConsignmentCodeGeneration:
Set @GeneratedConsignmentCode = @ConsignmentCode + [dbo].GenerateConsignmentCode(@ConsignmentCode)

if exists(select * from tblconsignment where [ConsignmentCode] = @GeneratedConsignmentCode )
begin
Goto ConsignmentCodeGeneration;
end

-----------------------Commented By Shubham 14/02/2017------------------------------------------------------------------------------------

--DECLARE @ConsignmentCode9Digit nvarchar(9)
--SELECT @ConsignmentCode9Digit = MAX (RIGHT(ConsignmentCode, 9))+1
--FROM tblConsignment WHERE left(ConsignmentCode,LEN(@ConsignmentCode)) = @ConsignmentCode
--SELECT @ConsignmentCode9Digit = RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9) --SELECT @ConsignmentCode9Digit
--if(@ConsignmentCode9Digit IS NULL)
--SET @ConsignmentCode9Digit = '000000000' 
-----------------------------------------------------------------------------------------------------------
--SELECT @ConsignmentCode9Digit
--declare @ConsignmentCode9Digit nvarchar(9)
-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where ConsignmentCode =-- @ConsignmentCode
--SELECT @ConsignmentCode9Digit =  RIGHT('00000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode8Digit
--declare @UserId6Digit nvarchar(6)
--if(@UserID is null or @UserID ='')
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(ABS(Checksum(NewID()) ),'-',''), 6)
--end
--else
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(@UserID,'-',''), 6)
--end
----------------------------------------------------------------------------------------------------------

DECLARE @ConsignmentIDret int
INSERT INTO [dbo].[tblConsignment] ([ConsignmentCode],[UserID],[IsRegUserConsignment],[PickupID],[DestinationID],[ContactID],[TotalWeight] --,[TotalMeasureWeight]
,[TotalVolume] --,[TotalMeasureVolume]
,[NoOfItems],[SpecialInstruction],[CustomerRefNo],[ConsignmentPreferPickupDate],[ConsignmentPreferPickupTime],[ClosingTime],[DangerousGoods],[Terms],[RateCardID],[LastActivity],[LastActiivityDateTime],[ConsignmentStatus],[EDIDataProcessed], [ProntoDataExtracted],[CreatedDateTime],[CreatedBy],[IsInternational],[IsDocument],IsSignatureReq, [IfUndelivered], [ReasonForExport], [TypeOfExport], [Currency], [IsInsurance], [IdentityNo], [IdentityType], [IsIdentity], [NetSubTotal], [IsATl], [IsReturnToSender], [HasReadInsuranceTc], [NatureOfGoods], [SortCode]
,ETA ,[IsProcessed])
VALUES (@GeneratedConsignmentCode, --@ConsignmentCode+@ConsignmentCode9Digit,
@UserID,
@IsRegUserConsignment,
@PickupID,
@DestinationID,
@ContactID,
@TotalWeight,
--@TotalMeasureWeight,
@TotalVolume,
--@TotalMeasureVolume,
@NoOfItems,
@SpecialInstruction,
@CustomerRefNo,
@ConsignmentPreferPickupDate,
@ConsignmentPreferPickupTime,
@ClosingTime,
@DangerousGoods,
@Terms,
@RateCardID,
@LastActivity,
@LastActiivityDateTime,
1,
0,
0,
GETDATE(),
@CreatedBy,
1,
@IsDocument,
@IsSignatureReq,
@IfUndelivered,
@ReasonForExport,
@TypeOfExport,
@Currency,
@IsInsurance,
@IdentityNo,
@IdentityType,
@IsIdentity,
@NetSubTotal,
@IsATl,
@IsReturnToSender,
@HasReadInsuranceTc,
@NatureOfGoods,
@SortCode,
@ETA,1)
SET @ConsignmentIDret = SCOPE_IDENTITY()
SELECT @ConsignmentIDret DECLARE @SalesOrderIDret int

------------------Insert In Sales Order------------------

INSERT INTO [dbo].[tblSalesOrder] ([ReferenceNo],[UserID],[NoofItems],[TotalWeight],[TotalVolume],[RateCardID],[GrossTotal],[GST],[NetTotal],[SalesOrderStatus],[InvoiceNo],[CreatedDateTime],[CreatedBy])
VALUES (@ConsignmentIDret,
@UserID,
@NoofItems,
@TotalWeight,
@TotalVolume,
@RateCardID,
@GrossTotal,
@GST,
@NetTotal,
7,
NULL,
GETDATE(),
@UserID)
SET @SalesOrderIDret = SCOPE_IDENTITY() 

---------ItemLabel ------------------------

INSERT INTO [dbo].[tblItemLabel] ([ConsignmentID],[LabelNumber],[Length],[Width],[Height],[CubicWeight],[PhysicalWeight],[MeasureWeight],[DeclareVolume],[LastActivity],[LastActivityDateTime],[CreatedDateTime],[CreatedBy],[CountryOfOrigin],[Description],[HSTariffNumber],[Quantity],[UnitValue])
SELECT @ConsignmentIDret,
@ItemCodeSing,
--@ConsignmentCode+@ConsignmentCode9Digit+  cast( strLabelNumber as nvarchar) ,
--case when strLength='' then null else  cast(strLength as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strLength)=1 THEN CAST(strLength AS decimal(10,2))
ELSE NULL
END,
--case when strWidth='' then null else     cast( strWidth as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strWidth)=1 THEN CAST(strWidth AS decimal(10,2))
ELSE NULL
END,
--case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strHeight)=1 THEN CAST(strHeight AS decimal(10,2))
ELSE NULL
END,
--case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strCubicWeight)=1 THEN CAST(strCubicWeight AS decimal(10,2))
ELSE NULL
END,
--case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strPhysicalWeight)=1 THEN CAST(strPhysicalWeight AS decimal(10,2))
ELSE NULL
END,
--case when strMeasureWeight='' then null else    cast(  strMeasureWeight as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strMeasureWeight)=1 THEN CAST(strMeasureWeight AS decimal(10,2))
ELSE NULL
END,
--case when strDeclareVolume='' then null else  cast(  strDeclareVolume as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strDeclareVolume)=1 THEN CAST(strDeclareVolume AS decimal(10,2))
ELSE NULL
END,
strLastActivity,
strLastActivityDateTime,
GETDATE(),
strCreatedBy,
[CountryOfOrigin],
[Description],
[HSTariffNumber],
[Quantity],
[UnitValue]
FROM @dtItemLabel;

--------------------------------Item Image ----------------------
DECLARE @ItemLabelID int= NULL
SELECT @ItemLabelID = ItemLabelID
FROM [tblItemLabel]
WHERE [ConsignmentID] = @ConsignmentIDret
INSERT INTO [dbo].[tblItemLabelImage] ([ItemLabelID],[LableImage],[CreatedBy])
VALUES (@ItemLabelID,
@LabelImage,
@UserID )---------SalesOrderDetail ------------------------

INSERT INTO [dbo].[tblSalesOrderDetail] ([SalesOrderID],[Description],[LineNo],[Weight],[Volume],[FreightCharge],[FuelCharge],[InsuranceCharge],[ServiceCharge],[Total],[CreatedDateTime],[CreatedBy])
SELECT @SalesOrderIDret,
strDescription,
[strLineNo],
--case when strPhysicalWeight='' then null else    cast( strPhysicalWeight as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strPhysicalWeight)=1 THEN CAST(strPhysicalWeight AS decimal(10,2))
ELSE NULL
END,
--case when strDeclareVolume='' then null else    cast(  strDeclareVolume as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strDeclareVolume)=1 THEN CAST(strDeclareVolume AS decimal(10,2))
ELSE NULL
END,
--case when strFreightCharge='' then null else    cast(  strFreightCharge as decimal(10,2))end,
CASE
WHEN ISNUMERIC(strFreightCharge)=1 THEN CAST(strFreightCharge AS decimal(10,2))
ELSE NULL
END,
--case when strFuelCharge='' then null else    cast(  strFuelCharge as decimal(19,4))end,
CASE
WHEN ISNUMERIC(strFuelCharge)=1 THEN CAST(strFuelCharge AS decimal(19,4))
ELSE NULL
END,
--case when strInsuranceCharge='' then null else    cast(   strInsuranceCharge as decimal(19,4))end,
CASE
WHEN ISNUMERIC(strInsuranceCharge)=1 THEN CAST(strInsuranceCharge AS decimal(19,4))
ELSE NULL
END,
--case when strServiceCharge='' then null else    cast(    strServiceCharge as decimal(19,4))end,
CASE
WHEN ISNUMERIC(strServiceCharge)=1 THEN CAST(strServiceCharge AS decimal(19,4))
ELSE NULL
END,
--cast (strFreightCharge as decimal(19,4)) +
CASE
WHEN ISNUMERIC(strFreightCharge)=1 THEN CAST(strFreightCharge AS decimal(19,4))
ELSE 0.0
END + --cast (strFuelCharge as decimal(19,4))+
CASE
WHEN ISNUMERIC(strFuelCharge)=1 THEN CAST(strFuelCharge AS decimal(19,4))
ELSE 0.0
END + --cast (strInsuranceCharge as decimal(19,4))+
CASE
WHEN ISNUMERIC(strInsuranceCharge)=1 THEN CAST(strInsuranceCharge AS decimal(19,4))
ELSE 0.0
END + --cast (strServiceCharge as decimal(19,4)),
CASE
WHEN ISNUMERIC(strServiceCharge)=1 THEN CAST(strServiceCharge AS decimal(19,4))
ELSE 0.0
END,
getdate(),
strCreatedBy
FROM @dtSalesOrderDetail 

---------CustomDeclaration ------------------------

INSERT INTO [dbo].tblCustomDeclaration ([ConsignmentID],ItemDescription,ItemInBox,UnitPrice,SubTotal,HSCode,CountryofOrigin,Currency,CreatedBy)
SELECT @ConsignmentIDret,
ItemDescription,
ItemInBox,
UnitPrice,
SubTotal,
HSCode,
CountryofOrigin,
Currency,
@UserID
FROM @dtCustomDeclaration 


---------Invoice ------------------------

DECLARE @InvoiceNumber NVARCHAR(5) , @GeneratedInvoiceNo nvarchar(30) = null
SET @InvoiceNumber= Substring(@ConsignmentCode, 1, 3) + 'IN' 

InvoiceNoGeneration: 
Set @GeneratedInvoiceNo = @InvoiceNumber + [dbo].[GenerateInvoiceNumber](@InvoiceNumber)

if exists(select * from tblinvoice where [InvoiceNumber] = @GeneratedInvoiceNo )
begin
Goto InvoiceNoGeneration;
end
---------Commented By Shubham 14/02/2017------------------------------------------------------------------------------------------

--DECLARE @InvoiceNumber nvarchar(5)
--SET @InvoiceNumber= SUBSTRING(@ConsignmentCode, 1, 3)+'IN' DECLARE @InvoiceCode7Digit nvarchar(7)
--SELECT @InvoiceCode7Digit = MAX (RIGHT(InvoiceNumber, 7))+1
--FROM tblInvoice WHERE left(InvoiceNumber,5) = @InvoiceNumber
--SELECT @InvoiceCode7Digit = RIGHT('0000000' + replace(@InvoiceCode7Digit,'-',''),7) --SELECT @ConsignmentCode9Digit
--if(@InvoiceCode7Digit IS NULL)
--SET @InvoiceCode7Digit = '0000000' --select @UserId5Digit+@ConsignmentCode8Digit
-------------------------------------------------------------------------------------------------------------------------------------


DECLARE @InvoiceID int
INSERT INTO [dbo].[tblInvoice] ([InvoiceNumber],[UserID],[PickupAddressID],[DestinationAddressID],[ContactAddressID],[TotalFreightExGST],[GST],[TotalFreightInclGST],[InvoiceStatus],[SendToPronto],[CreatedDateTime],[CreatedBY],[PaymentRefNo],[AuthorizationCode],[merchantReferenceCode],[SubscriptionID])
VALUES (@GeneratedInvoiceNo, --@InvoiceNumber+@InvoiceCode7Digit,
@UserID,
@PickupID,
@DestinationID,
@ContactID,
--@TotalFreightExGST ,
@GrossTotal,
@GST,
--@TotalFreightInclGST ,
@NetTotal,
11,
@SendToPronto,
GETDATE(),
@CreatedBY,
@PaymentRefNo,
@AuthorizationCode,
@merchantReferenceCode,
@SubscriptionID)
SET @InvoiceID = SCOPE_IDENTITY() 

--------Commented By Shubham 16/05/2016-------

--UPDATE dbo.tblConsignmentStaging
--SET [PaymentRefNo] =@PaymentRefNo WHERE ConsignmentStagingID = @ConsignmentStagingID 

-----------------------Invoice image --------------

INSERT INTO [dbo].[tblInvoiceImage] ([InvoiceID],[InvoiceImage],[CreatedBy])
VALUES (@InvoiceID,
@InvoiceImage,
@UserID )

-----------------------insert [tblDHLBarCodeImage] -------
if(@AWBBarCode IS NOT NULL) BEGIN
INSERT INTO [dbo].[tblDHLBarCodeImage] ([ConsignmentID],[AWBBarCode],[OriginDestnBarcode],[ClientIDBarCode],[DHLRoutingBarCode],[CreatedBy])
VALUES (@ConsignmentIDret,
@AWBBarCode,
@OriginDestnBarcode,
@ClientIDBarCode,
@DHLRoutingBarCode,
@UserID) END 

--------Update Sales order -------

UPDATE tblSalesOrder
SET InvoiceNo = @GeneratedInvoiceNo, --@InvoiceNumber +@InvoiceCode7Digit,
SalesOrderStatus = 9 WHERE SalesOrderID= @SalesOrderIDret 

-------------------------------------------------------------
-----------------Final Processed-----------------

SELECT @CountryCode=CountryCode FROM dbo.tblAddress
LEFT JOIN dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID WHERE AddressID =@DestinationID

----------------------------------------------Added By Shubham 16/05/2016--------------------------------
UPDATE dbo.tblConsignmentStaging
SET USPSRefNo = case when @CountryCode = 'US' then @SenderReference else null end WHERE ConsignmentStagingID = @ConsignmentStagingID 

----------------------------------------------Commented By Shubham 16/05/2016--------------------------------
--UPDATE dbo.tblConsignmentStaging
--SET [IsProcessed] =1,
--[UpdatedDateTTime] = getDate(),
--[UpdatedBy] = @CreatedBy,
--[ConsignmentId]=@ConsignmentIDret,
--USPSRefNo = case when @CountryCode = 'US' then @SenderReference else null end WHERE ConsignmentStagingID = @ConsignmentStagingID 

---------------------------------------------------------------------------------------------------------------------


--if(@@ERROR<>0) BEGIN
--ROLLBACK tran
--INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
--VALUES (ERROR_LINE() +' : '+ERROR_MESSAGE(),
--'SPCPPL_CreateConsignmentUsingBulkSing',
--2)--exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
--END ELSE

COMMIT tran 

END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
           ,'SPCPPL_CreateConsignmentUsingBulkSingNewProcss'
           , 2)
end
END CATCH

END
GO
