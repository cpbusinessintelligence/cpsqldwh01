SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentUsingBulkSingWithoutPay_International_BUP_17_06_2016]

@ProntoDataExtracted bit= null,
@LastActivity varchar(200)= null,
@LastActiivityDateTime datetime= null,
@EDIDataProcessed bit= null,
@ConsignmentCode varchar(40)= null,
@ConsignmentStagingID int= null,
@GrossTotal decimal(19,4) = null ,
@GST decimal(19,4) = null,
@NetTotal decimal(19,4) = null,
@dtItemLabel dtItemLabelSing_International READONLY ,
@dtCustomDeclaration dtCustomDeclaration_International READONLY,
@TotalFreightExGST decimal(19,4) = null,
@TotalFreightInclGST decimal(19,4) = null,
@InvoiceStatus int = null,
@SendToPronto bit = null,
@PaymentRefNo nvarchar(100)=null,
@merchantReferenceCode nvarchar(max)=null,
@SubscriptionID nvarchar(max)=null,
@AuthorizationCode nvarchar(100)=null,
@InvoiceImage varchar(max)=null,
@LabelImage varchar(max)=null,
@ItemCodeSing varchar(max)=null,
@AWBBarCode varchar(max)=null,
@OriginDestnBarcode varchar(max)=null,
@ClientIDBarCode varchar(max)=null,
@DHLRoutingBarCode varchar(max)=null,
@NetSubTotal decimal(10,2)=null,
@NatureOfGoods nvarchar(max)=null,
@ClientCode varchar(50) = null 

           

AS

BEGIN

begin tran



declare    @UserID int= null,

           @IsRegUserConsignment bit= 0,

           @PickupID int= null,

           @DestinationID int= null,

           @ContactID int= null,

           @TotalWeight decimal(10,2)= null,

           @TotalVolume decimal(10,2)= null,

           @NoOfItems int= null,

           @SpecialInstruction varchar(500)= null,

           @CustomerRefNo varchar(40)= null,

           @ConsignmentPreferPickupDate date= null,

           @ConsignmentPreferPickupTime varchar(20)= null,

           @ClosingTime varchar(10)= null,

           @DangerousGoods bit= null,

           @Terms bit= null,

           @RateCardID nvarchar(50)= null,

           @CreatedBy int= null,



@IsSignatureReq bit = 0,

@IsDocument nvarchar(max) = null,





@IfUndelivered nvarchar(max)=null,

@ReasonForExport nvarchar(max)=null,

@TypeOfExport nvarchar(max)=null,

@Currency nvarchar(max)=null,

@IsInsurance bit=null,



@IdentityNo   nvarchar(max)=null,

@IdentityType nvarchar(max)=null,

@IsIdentity   bit=null,

@IsATl  bit=null,

@IsReturnToSender   bit=null,

@HasReadInsuranceTc   bit=null,

@SortCode nvarchar(max) = null,

@ETA nvarchar(max) = null,

@InsuranceAmount decimal(10,2) = null









SELECT 

      @UserID =[UserID]

      ,@IsRegUserConsignment=[IsRegUserConsignment]

      ,@PickupID=[PickupID]

      ,@DestinationID=[DestinationID]

      ,@ContactID=[ContactID]

      ,@TotalWeight=[TotalWeight]

      ,@TotalVolume=[TotalVolume]

      ,@NoOfItems=[NoOfItems]

      ,@SpecialInstruction=[SpecialInstruction]

      ,@CustomerRefNo=[CustomerRefNo]

      ,@ConsignmentPreferPickupDate=[PickupDate]

      ,@ConsignmentPreferPickupTime=[PreferPickupTime]

      ,@ClosingTime=[ClosingTime]

      ,@DangerousGoods=[DangerousGoods]

      ,@Terms=[Terms]

      ,@RateCardID=[RateCardID]

      ,@CreatedBy = [CreatedBy]

       ,@IsSignatureReq =[IsSignatureReq]

      ,@IsDocument =[IsDocument],

      @IfUndelivered   =[IfUndelivered],

@ReasonForExport =[ReasonForExport],

@TypeOfExport =[TypeOfExport],

@Currency =[Currency],

@IsInsurance = [IsInsurance],



@IdentityNo = [IdentityNo],

@IdentityType= [IdentityType],

@IsIdentity  = [IsIdentity],

@IsATl=[IsATl]  ,

@IsReturnToSender=   [IsReturnToSender],

@HasReadInsuranceTc =  [HasReadInsuranceTc] ,

@SortCode = [SortCode],

@NatureOfGoods=NatureOfGoods,

@ETA = [ETA],

@InsuranceAmount=InsuranceAmount

  FROM [dbo].[tblConsignmentStaging] where ConsignmentStagingID = @ConsignmentStagingID











declare @ConsignmentCode9Digit nvarchar(9)

  if (LEN(@ConsignmentCode)=7)

   begin

				select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,7) = @ConsignmentCode

				SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)

				--SELECT @ConsignmentCode9Digit

				if(@ConsignmentCode9Digit is null)

				set @ConsignmentCode9Digit = '000000000'

				 --SELECT @ConsignmentCode9Digit

	 end	

  else

	 begin

				select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,6) = @ConsignmentCode

				SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)

				--SELECT @ConsignmentCode9Digit

				if(@ConsignmentCode9Digit is null)

				set @ConsignmentCode9Digit = '000000000'

				 --SELECT @ConsignmentCode9Digit

	 end	



















--declare @ConsignmentCode9Digit nvarchar(9)

-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,6) = @ConsignmentCode

--SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)

----SELECT @ConsignmentCode9Digit

--if(@ConsignmentCode9Digit is null)

--set @ConsignmentCode9Digit = '000000000'

--SELECT @ConsignmentCode9Digit



--declare @ConsignmentCode9Digit nvarchar(9)

-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where ConsignmentCode =-- @ConsignmentCode

--SELECT @ConsignmentCode9Digit =  RIGHT('00000000' + replace(@ConsignmentCode9Digit,'-',''), 9)

--SELECT @ConsignmentCode8Digit

--declare @UserId6Digit nvarchar(6)

--if(@UserID is null or @UserID ='')

--begin

--SELECT @UserId6Digit =  RIGHT('00000000' + replace(ABS(Checksum(NewID()) ),'-',''), 6)

--end

--else

--begin

--SELECT @UserId6Digit =  RIGHT('00000000' + replace(@UserID,'-',''), 6)

--end





			

				DECLARE @ConsignmentIDret int

			INSERT INTO [dbo].[tblConsignment]

           ([ConsignmentCode]

           ,[UserID]

           ,[IsRegUserConsignment]

           ,[PickupID]

           ,[DestinationID]

           ,[ContactID]

           ,[TotalWeight]

           --,[TotalMeasureWeight]

           ,[TotalVolume]

           --,[TotalMeasureVolume]

           ,[NoOfItems]

           ,[SpecialInstruction]

           ,[CustomerRefNo]

           ,[ConsignmentPreferPickupDate]

           ,[ConsignmentPreferPickupTime]

           ,[ClosingTime]

           ,[DangerousGoods]

           ,[Terms]

           ,[RateCardID]

           ,[LastActivity]

           ,[LastActiivityDateTime]

           ,[ConsignmentStatus]

           ,[EDIDataProcessed]

           ,[ProntoDataExtracted]

           ,[CreatedDateTime]

           ,[CreatedBy]

           ,[IsInternational]

           ,[IsDocument]

           ,IsSignatureReq,

            [IfUndelivered],

			 [ReasonForExport],

			 [TypeOfExport],

			 [Currency],

			 [IsInsurance],

			 [IdentityNo],

			 [IdentityType],

			 [IsIdentity],

			 [NetSubTotal],

		 [IsATl]  ,

  [IsReturnToSender],

  [HasReadInsuranceTc]  ,

  [NatureOfGoods],

  [SortCode]

           ,ETA,

           IsAccountCustomer

           ,InsuranceAmount

              ,CalculatedTotal

           ,CalculatedGST 
		   ,ClientCode
           )

     VALUES

           (@ConsignmentCode+@ConsignmentCode9Digit,

           @UserID,

           @IsRegUserConsignment,

           @PickupID,

           @DestinationID,

           @ContactID,

           @TotalWeight,

           --@TotalMeasureWeight,

           @TotalVolume,

           --@TotalMeasureVolume,

           @NoOfItems,

           @SpecialInstruction,

           @CustomerRefNo,

           @ConsignmentPreferPickupDate,

           @ConsignmentPreferPickupTime,

           @ClosingTime,

           @DangerousGoods,

           @Terms,

           @RateCardID,

           @LastActivity,

           @LastActiivityDateTime,

           1,

            0,

           0,

           GETDATE(),

           @CreatedBy,

           1,

             @IsDocument  ,

         @IsSignatureReq

         ,   @IfUndelivered    ,

			@ReasonForExport  ,

			@TypeOfExport  ,

			@Currency   ,

			@IsInsurance,

			@IdentityNo  ,

			@IdentityType ,

			@IsIdentity  ,

			@NetSubTotal,

			 @IsATl   ,

@IsReturnToSender ,

@HasReadInsuranceTc  ,

@NatureOfGoods,

@SortCode

        ,@ETA,

        1

           ,@InsuranceAmount

              ,@GrossTotal

        ,@GST
		,@ClientCode
) SET @ConsignmentIDret = SCOPE_IDENTITY()

 

			

			UPDATE dbo.tblConsignmentStaging

   SET [IsProcessed] =1,

      [UpdatedDateTTime] = getDate(),

      [UpdatedBy] = @CreatedBy      ,

      [ConsignmentId]=@ConsignmentIDret,

      IsAccountCustomer=1

 WHERE ConsignmentStagingID = @ConsignmentStagingID

 

 

 

 

 

 

 

 

 

 

 

 

            ---------ItemLabel ------------------------

      

      INSERT INTO [dbo].[tblItemLabel]

           ([ConsignmentID]

           ,[LabelNumber]

           ,[Length]

           ,[Width]

           ,[Height]

           ,[CubicWeight]

           ,[PhysicalWeight]

		   ,[CreatedDateTime]

           ,[CreatedBy]

           ,[CountryOfOrigin]

          ,[Quantity]

          
           )

           SELECT

            @ConsignmentIDret, 

			@ItemCodeSing,
          --@ConsignmentCode+@ConsignmentCode9Digit+  cast( strLabelNumber as nvarchar) ,

        --@ConsignmentCode+@ConsignmentCode9Digit+  cast( strLabelNumber as nvarchar) ,

         --case when strLength='' then null else  cast(strLength as decimal(10,2))end,

        case when  ISNUMERIC(length)=1 then CAST(length AS decimal(10,2))else null end ,

        --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end,

        case when  ISNUMERIC(width)=1 then CAST(width AS decimal(10,2))else null end ,

         --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,

        case when  ISNUMERIC(height)=1 then CAST(height AS decimal(10,2))else null end ,

         --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end,

        case when  ISNUMERIC(CubicWeight)=1 then CAST(CubicWeight AS decimal(10,2))else null end ,

          --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,

        case when  ISNUMERIC(physicalWeight)=1 then CAST(physicalWeight AS decimal(10,2))else null end ,

        

          

             GETDATE(),

             strCreatedBy,

             [CountryOfOrigin]

           
           ,[quantity]

           
             FROM @dtItemLabel;

             --------------------------------Item Image ----------------------

     declare    @ItemLabelID int= null        

             select @ItemLabelID = ItemLabelID from [tblItemLabel] where [ConsignmentID] = @ConsignmentIDret

             

             INSERT INTO [dbo].[tblItemLabelImage]

           ([ItemLabelID]

           ,[LableImage]

           ,[CreatedBy]

           )

     VALUES

           (@ItemLabelID

           ,@LabelImage

,@UserID

          )



             

            

             

    

            

                   ---------CustomDeclaration ------------------------

             

               INSERT INTO [dbo].tblCustomDeclaration

           ([ConsignmentID]

           ,ItemDescription

           ,ItemInBox

           ,UnitPrice

           ,SubTotal

           ,HSCode

           ,CountryofOrigin

           ,Currency

           ,CreatedBy

           )

           select @ConsignmentIDret,

           ItemDescription,

           ItemInBox,

           UnitPrice,

           SubTotal,

           HSCode,

           CountryofOrigin,

           Currency,

           @UserID

            from @dtCustomDeclaration

            

            

           -----------------------insert [tblDHLBarCodeImage] -------

        if(@AWBBarCode is not null)

        begin

           INSERT INTO [dbo].[tblDHLBarCodeImage]

           ([ConsignmentID]

           ,[AWBBarCode]

           ,[OriginDestnBarcode]

           ,[ClientIDBarCode]

           ,[DHLRoutingBarCode]

          

           ,[CreatedBy])

     VALUES

           (@ConsignmentIDret

           ,@AWBBarCode 

           ,@OriginDestnBarcode 

           ,@ClientIDBarCode 

           ,@DHLRoutingBarCode 

          

           ,@UserID)

         end

          

           -------------------------------------------------------------

 ---------------------------------------------------------------------------------------------

 declare @ConsignmentId int = @ConsignmentIDret

 

			UPDATE dbo.tblConsignment

   SET [IsProcessed] =1,

      [UpdatedDateTTime] = getDate(),

      [UpdatedBy] = @UserID

     

 WHERE ConsignmentID = @ConsignmentId

 

 	UPDATE dbo.[tblInvoice]

   SET [PaymentRefNo] = @PaymentRefNo

           ,[AuthorizationCode]=@AuthorizationCode

           ,[merchantReferenceCode]=@merchantReferenceCode

           ,[SubscriptionID]=@SubscriptionID

           ,[UpdatedDateTime] =GETDATE()

      ,[UpdatedBy] = @UserID

      

 WHERE invoicenumber = (select top 1 InvoiceNo from dbo.tblSalesOrder where ReferenceNo=@ConsignmentId)



 

 	UPDATE dbo.tblConsignmentstaging

 	set 

[PaymentRefNo] = @PaymentRefNo

           ,[AuthorizationCode]=@AuthorizationCode

           ,[merchantReferenceCode]=@merchantReferenceCode

           ,[SubscriptionID]=@SubscriptionID

           ,[UpdatedDateTTime] =GETDATE()

      ,[UpdatedBy] = @UserID

      where 

      ConsignmentId = @ConsignmentId

      

      

declare  

@PicupID int 

   

--SELECT     dbo.tblInvoice.*

--FROM         dbo.tblConsignment INNER JOIN

--                      dbo.tblSalesOrder ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo INNER JOIN

--                      dbo.tblInvoice ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber

                      

--                       where dbo.tblConsignment.ConsignmentID = @ConsignmentID

           

           SELECT     dbo.tblConsignment.*, dbo.tblConsignment.[Country-ServiceArea-FacilityCode] as Country_ServiceArea_FacilityCode

FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID





--SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*

--FROM         dbo.tblSalesOrderDetail INNER JOIN

--                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID

--WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID)



				





			   SELECT   @PicupID = dbo.tblConsignment.PickupID,

			   @DestinationID=dbo.tblConsignment.DestinationID,

			   @ContactID=dbo.tblConsignment.ContactID

FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID









            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 

                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 

                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 

                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 

FROM         dbo.tblAddress left JOIN

                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID

            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 

                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 

                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 

                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe,case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 

FROM         dbo.tblAddress left JOIN

                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID

 

 

  SELECT     Cu.UserID, c.IsRegularShipper

FROM         tblCompany c INNER JOIN

                      tblCompanyUsers Cu ON c.CompanyID = Cu.CompanyID

WHERE     (c.IsRegularShipper = 1)

and  Cu.UserID = @UserID





-----------------------------------------------------------------------------------------------

 

            

  if(@@ERROR<>0)

   begin

  rollback tran

  

  INSERT INTO [dbo].[tblErrorLog]

           ([Error]

           ,[FunctionInfo]

           ,[ClientId])

     VALUES

           (ERROR_LINE() +' : '+ERROR_MESSAGE()

           ,'SPCPPL_CreateConsignmentUsingBulkSing'

           , 2)

  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2

  end

  else

  commit tran          

            





            

            

END

GO
