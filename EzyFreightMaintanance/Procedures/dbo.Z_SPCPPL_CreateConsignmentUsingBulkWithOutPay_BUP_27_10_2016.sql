SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateConsignmentUsingBulkWithOutPay_BUP_27_10_2016]
      
   --             @TotalMeasureWeight decimal(10,2)= null,
   --@TotalMeasureVolume decimal(10,2)= null,
   @ProntoDataExtracted bit= null,
   @LastActivity varchar(200)= null,
     @LastActiivityDateTime datetime= null,
  @EDIDataProcessed bit= null,
   --@ConsignmentStatus int= null,
    @ConsignmentCode varchar(40)= null,
  @ConsignmentStagingID int= null,
        
@GrossTotal decimal(19,4) ,
 @GST decimal(19,4) ,
           @NetTotal decimal(19,4) ,
           
           @dtItemLabel dtItemLabel READONLY ,
  
           
                      @TotalFreightExGST decimal(19,4) = null,
         
           @TotalFreightInclGST decimal(19,4) = null,
           @InvoiceStatus int = null,
           @SendToPronto bit = null 
           
AS
BEGIN
begin tran

declare    @UserID int= null,
           @IsRegUserConsignment bit= null,
           @PickupID int= null,
           @DestinationID int= null,
           @ContactID int= null,
           @TotalWeight decimal(10,2)= null,
           @TotalVolume decimal(10,2)= null,
           @NoOfItems int= null,
           @SpecialInstruction varchar(500)= null,
           @CustomerRefNo varchar(40)= null,
           @ConsignmentPreferPickupDate date= null,
           @ConsignmentPreferPickupTime varchar(20)= null,
           @ClosingTime varchar(10)= null,
           @DangerousGoods bit= null,
           @Terms bit= null,
           @RateCardID nvarchar(50)= null,
           @CreatedBy int= null,

@IsSignatureReq bit = 0,
@IsATl  bit=null,
@IsDocument bit = 0,
@SortCode nvarchar(max) = null,
@NatureOfGoods nvarchar(max) = null,
@ETA nvarchar(max) = null




SELECT 
      @UserID =[UserID]
      ,@IsRegUserConsignment=[IsRegUserConsignment]
      ,@PickupID=[PickupID]
      ,@DestinationID=[DestinationID]
      ,@ContactID=[ContactID]
      ,@TotalWeight=[TotalWeight]
      ,@TotalVolume=[TotalVolume]
      ,@NoOfItems=[NoOfItems]
      ,@SpecialInstruction=[SpecialInstruction]
      ,@CustomerRefNo=[CustomerRefNo]
      ,@ConsignmentPreferPickupDate=[PickupDate]
      ,@ConsignmentPreferPickupTime=[PreferPickupTime]
      ,@ClosingTime=[ClosingTime]
      ,@DangerousGoods=[DangerousGoods]
      ,@Terms=[Terms]
      ,@RateCardID=[RateCardID]
       ,@IsATl=[IsATl]  
      ,@CreatedBy = [CreatedBy]
      ,@IsSignatureReq =[IsSignatureReq]
      ,@IsDocument =[IsDocument],
@SortCode = [SortCode],
@NatureOfGoods=NatureOfGoods,
@ETA = [ETA]
  FROM [dbo].[tblConsignmentStaging] where ConsignmentStagingID = @ConsignmentStagingID




declare @ConsignmentCode9Digit nvarchar(9)
 select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where left(ConsignmentCode,6) = @ConsignmentCode
SELECT @ConsignmentCode9Digit =  RIGHT('000000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode9Digit
if(@ConsignmentCode9Digit is null)
set @ConsignmentCode9Digit = '000000000'
--SELECT @ConsignmentCode9Digit

--declare @ConsignmentCode9Digit nvarchar(9)
-- select @ConsignmentCode9Digit = max (RIGHT(ConsignmentCode, 9))+1  from tblConsignment where ConsignmentCode =-- @ConsignmentCode
--SELECT @ConsignmentCode9Digit =  RIGHT('00000000' + replace(@ConsignmentCode9Digit,'-',''), 9)
--SELECT @ConsignmentCode8Digit
--declare @UserId6Digit nvarchar(6)
--if(@UserID is null or @UserID ='')
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(ABS(Checksum(NewID()) ),'-',''), 6)
--end
--else
--begin
--SELECT @UserId6Digit =  RIGHT('00000000' + replace(@UserID,'-',''), 6)
--end


			
				DECLARE @ConsignmentIDret int
			INSERT INTO [dbo].[tblConsignment]
           ([ConsignmentCode]
           ,[UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           --,[TotalMeasureWeight]
           ,[TotalVolume]
           --,[TotalMeasureVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[ConsignmentPreferPickupDate]
           ,[ConsignmentPreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[RateCardID]
           ,[LastActivity]
           ,[LastActiivityDateTime]
           ,[ConsignmentStatus]
           ,[EDIDataProcessed]
           ,[ProntoDataExtracted]
           ,[CreatedDateTime]
           ,[CreatedBy]
            ,[IsDocument]
           ,IsSignatureReq
           ,SortCode
           ,NatureOfGoods
           ,ETA
		  ,[IsATl],
		  [IsProcessed],
		  [IsAccountCustomer]
		,CalculatedTotal
           ,CalculatedGST 
           )
     VALUES
           (@ConsignmentCode+@ConsignmentCode9Digit,
           @UserID,
           @IsRegUserConsignment,
           @PickupID,
           @DestinationID,
           @ContactID,
           @TotalWeight,
           --@TotalMeasureWeight,
           @TotalVolume,
           --@TotalMeasureVolume,
           @NoOfItems,
           @SpecialInstruction,
           @CustomerRefNo,
           @ConsignmentPreferPickupDate,
           @ConsignmentPreferPickupTime,
           @ClosingTime,
           @DangerousGoods,
           @Terms,
           @RateCardID,
           @LastActivity,
           @LastActiivityDateTime,
           1,
           0,
           0,
           GETDATE(),
           @CreatedBy,
            @IsDocument  ,
         @IsSignatureReq  ,
         @SortCode
         ,@NatureOfGoods
        ,@ETA
			 , @IsATl  ,
			 1,
			 1
			 ,@GrossTotal
        ,@GST
          ) SET @ConsignmentIDret = SCOPE_IDENTITY()
			
			
			UPDATE dbo.tblConsignmentStaging
   SET [IsProcessed] =1,
      [UpdatedDateTTime] = getDate(),
      [UpdatedBy] = @CreatedBy,
      [ConsignmentId]=@ConsignmentIDret,
      IsAccountCustomer=1
 WHERE ConsignmentStagingID = @ConsignmentStagingID
 
 
 
 
 
 
 
  
 
 
 
 
			
			
 
      
      
 
 
            ---------ItemLabel ------------------------
      
      INSERT INTO [dbo].[tblItemLabel]
           ([ConsignmentID]
           ,[LabelNumber]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[CubicWeight]
           ,[PhysicalWeight]
           ,[MeasureWeight]
           ,[DeclareVolume]
           ,[LastActivity]
           ,[LastActivityDateTime]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
           SELECT
            @ConsignmentIDret, 
        @ConsignmentCode+@ConsignmentCode9Digit+  cast( strLabelNumber as nvarchar) ,
         --case when strLength='' then null else  cast(strLength as decimal(10,2))end,
        case when  ISNUMERIC(strLength)=1 then CAST(strLength AS decimal(10,2))else null end ,
        --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end,
        case when  ISNUMERIC(strWidth)=1 then CAST(strWidth AS decimal(10,2))else null end ,
         --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,
        case when  ISNUMERIC(strHeight)=1 then CAST(strHeight AS decimal(10,2))else null end ,
         --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end,
        case when  ISNUMERIC(strCubicWeight)=1 then CAST(strCubicWeight AS decimal(10,2))else null end ,
          --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,
        case when  ISNUMERIC(strPhysicalWeight)=1 then CAST(strPhysicalWeight AS decimal(10,2))else null end ,
        --case when strMeasureWeight='' then null else    cast(  strMeasureWeight as decimal(10,2))end,
        case when  ISNUMERIC(strMeasureWeight)=1 then CAST(strMeasureWeight AS decimal(10,2))else null end ,
          --case when strDeclareVolume='' then null else  cast(  strDeclareVolume as decimal(10,2))end,
        case when  ISNUMERIC(strDeclareVolume)=1 then CAST(strDeclareVolume AS decimal(10,2))else null end ,
             strLastActivity,
             strLastActivityDateTime,
             GETDATE(),
             strCreatedBy
             FROM @dtItemLabel;
             
 
            
  
 
    
           
 
           
           SELECT     dbo.tblConsignment.*
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentIDret


 

				DECLARE @PicupID int
				--DECLARE @DestinationID int
				
				   SELECT   @PicupID = dbo.tblConsignment.PickupID,@DestinationID=dbo.tblConsignment.DestinationID
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentIDret

 




            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID


  
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateName AS StateID
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID
                       where AddressID =@ContactID


SELECT *
  FROM [dbo].[tblItemLabel] where ConsignmentID = @ConsignmentIDret
  
  SELECT     Cu.UserID, c.IsRegularShipper
FROM         tblCompany c INNER JOIN
                      tblCompanyUsers Cu ON c.CompanyID = Cu.CompanyID
WHERE     (c.IsRegularShipper = 1)
and  Cu.UserID = @UserID
            
  if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'[SPCPPL_CreateConsignmentUsingBulkWithOutPay]'
           , 2)
  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
  end
  else
  commit tran          
            
           

            
            
END






GO
