SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[Z_SPCPPL_CreateContactUs_BUP_23_10_2015]
           @FirstName nvarchar(50) =null ,
           @LastName nvarchar(50) =null ,
           @Email nvarchar(250) =null ,
           @Phone nvarchar(250) =null ,
           @Suburb nvarchar(50) =null ,
           @PostCode nvarchar(50) =null ,
           @StateCode nvarchar(50) =null ,
           @Subject nvarchar(250) =null ,
           @HowCanIHelpYou nvarchar(max) =null ,
           @IsSubscribe bit =null ,
           @TrackingNo nvarchar(max)= null

           
AS
BEGIN



   

DECLARE @stateIdbyCode int
SELECT  @stateIdbyCode=[StateID]
      
  FROM [dbo].[tblState]where [StateCode] = @StateCode

DECLARE @PickupAddID int

					INSERT INTO [dbo].[tblContactUs]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Phone]
           ,[Suburb]
           ,[PostCode]
           ,[StateID]
           ,[Subject]
           ,[HowCanIHelpYou]
           ,[IsSubscribe]
           ,[TrackingNo]
          )
     VALUES
           (@FirstName ,
           @LastName ,
           @Email ,
           @Phone ,
           @Suburb ,
           @PostCode ,
           @stateIdbyCode ,
           @Subject ,
           @HowCanIHelpYou ,
           @IsSubscribe ,
           @TrackingNo
           )
      SET @PickupAddID = SCOPE_IDENTITY()
			select @PickupAddID
				
End
GO
