SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[Z_SPCPPL_CreateDomesticConsignment_WithOutPay_05_05_2017] 
  @ProntoDataExtracted   BIT= NULL, 
  @LastActivity          VARCHAR(200)= NULL, 
  @LastActiivityDateTime DATETIME= NULL, 
  @EDIDataProcessed      BIT= NULL, 
  @ConsignmentCode       VARCHAR(40)= NULL, 
  @ConsignmentStagingID  INT= NULL, 
  @GrossTotal            DECIMAL(19, 4), 
  @GST                   DECIMAL(19, 4), 
  @NetTotal              DECIMAL(19, 4), 
  @dtItemLabel           [dtDomesticItemLabelWithoutPay] readonly, 
  @TotalFreightExGST     DECIMAL(19, 4) = NULL, 
  @TotalFreightInclGST   DECIMAL(19, 4) = NULL, 
  @InvoiceStatus         INT = NULL, 
  @SendToPronto          BIT = NULL 
AS 
  BEGIN 
  BEGIN TRY
      BEGIN TRAN 

      DECLARE @UserID                      INT= NULL, 
              @IsRegUserConsignment        BIT= NULL, 
              @PickupID                    INT= NULL, 
              @DestinationID               INT= NULL, 
              @ContactID                   INT= NULL, 
              @TotalWeight                 DECIMAL(10, 2)= NULL, 
              @TotalVolume                 DECIMAL(10, 2)= NULL, 
              @NoOfItems                   INT= NULL, 
              @SpecialInstruction          VARCHAR(500)= NULL, 
              @CustomerRefNo               VARCHAR(40)= NULL, 
              @ConsignmentPreferPickupDate DATE= NULL, 
              @ConsignmentPreferPickupTime VARCHAR(20)= NULL, 
              @ClosingTime                 VARCHAR(10)= NULL, 
              @DangerousGoods              BIT= NULL, 
              @Terms                       BIT= NULL, 
              @RateCardID                  NVARCHAR(50)= NULL, 
              @CreatedBy                   INT= NULL, 
              @IsSignatureReq              BIT = 0, 
              @IsATl                       BIT=NULL, 
              @IsDocument                  BIT = 0, 
              @SortCode                    NVARCHAR(max) = NULL, 
              @NatureOfGoods               NVARCHAR(max) = NULL, 
              @ETA                         NVARCHAR(max) = NULL 

      SELECT @UserID = [userid], 
             @IsRegUserConsignment = [isreguserconsignment], 
             @PickupID = [pickupid], 
             @DestinationID = [destinationid], 
             @ContactID = [contactid], 
             @TotalWeight = [totalweight], 
             @TotalVolume = [totalvolume], 
             @NoOfItems = [noofitems], 
             @SpecialInstruction = [specialinstruction], 
             @CustomerRefNo = [customerrefno], 
             @ConsignmentPreferPickupDate = [pickupdate], 
             @ConsignmentPreferPickupTime = [preferpickuptime], 
             @ClosingTime = [closingtime], 
             @DangerousGoods = [dangerousgoods], 
             @Terms = [terms], 
             @RateCardID = [ratecardid], 
             @IsATl = [isatl], 
             @CreatedBy = [createdby], 
             @IsSignatureReq = [issignaturereq], 
             @IsDocument = [isdocument], 
             @SortCode = [sortcode], 
             @NatureOfGoods = natureofgoods, 
             @ETA = [eta] 
      FROM   [dbo].[tblconsignmentstaging] 
      WHERE  consignmentstagingid = @ConsignmentStagingID 

      
DECLARE @GeneratedConsignmentCode nvarchar(30) = null
ConsignmentCodeGeneration:
Set @GeneratedConsignmentCode = @ConsignmentCode + [dbo].GenerateConsignmentCode(@ConsignmentCode)

------------Commented By Shubham 06/02/2017  Created New Function to get the consignment code
--SELECT @ConsignmentCode9Digit = Max (RIGHT(consignmentcode, 9)) + 1 
--   FROM   tblconsignment WHERE  LEFT(consignmentcode, Len(@ConsignmentCode)) = @ConsignmentCode 
--   SELECT @ConsignmentCode9Digit = RIGHT('000000000' + Replace(@ConsignmentCode9Digit, '-', '') , 9) 
--IF( @ConsignmentCode9Digit IS NULL ) 
--     SET @ConsignmentCode9Digit = '000000000' 

if exists(select * from tblconsignment where [ConsignmentCode] = @GeneratedConsignmentCode )
begin
Goto ConsignmentCodeGeneration;
end

		     
DECLARE @ConsignmentIDret INT 

      INSERT INTO [dbo].[tblconsignment] 
                  ([consignmentcode], [userid], [isreguserconsignment], [pickupid], [destinationid], [contactid], [totalweight] , [totalvolume] , [noofitems], 
                   [specialinstruction], [customerrefno], [consignmentpreferpickupdate], [consignmentpreferpickuptime], [closingtime], [dangerousgoods], 
                   [terms], [ratecardid], [lastactivity], [lastactiivitydatetime], [consignmentstatus], [edidataprocessed], [prontodataextracted], 
                   [createddatetime], [createdby], [isdocument], issignaturereq, sortcode, natureofgoods, eta, [isatl], [isprocessed], [isaccountcustomer], 
                   calculatedtotal, calculatedgst) 
      VALUES      (@GeneratedConsignmentCode, --@ConsignmentCode + @ConsignmentCode9Digit, 
                   @UserID, @IsRegUserConsignment, @PickupID, @DestinationID, @ContactID, @TotalWeight, @TotalVolume, @NoOfItems, @SpecialInstruction, 
				   @CustomerRefNo, @ConsignmentPreferPickupDate, @ConsignmentPreferPickupTime, @ClosingTime, @DangerousGoods, @Terms, @RateCardID, 
                   @LastActivity, @LastActiivityDateTime, 1, 0, 0, Getdate(), @CreatedBy, @IsDocument, @IsSignatureReq, @SortCode, @NatureOfGoods, 
                   @ETA, @IsATl, 1, 1, @GrossTotal, @GST ) 

      SET @ConsignmentIDret = Scope_identity() 

---------ItemLabel ------------------------ 
INSERT INTO [dbo].[tblitemlabel] 
                  ([consignmentid], [labelnumber], [length], [width], [height], [cubicweight], [physicalweight], [measureweight], [declarevolume], 
                   [lastactivity], [lastactivitydatetime], [createddatetime], [createdby]) 
      SELECT @ConsignmentIDret, 
             @GeneratedConsignmentCode 
             + Cast( strlabelnumber AS NVARCHAR), --@ConsignmentCode + @ConsignmentCode9Digit 
             --case when strLength='' then null else  cast(strLength as decimal(10,2))end, 
             CASE WHEN Isnumeric(strlength) = 1 THEN Cast(strlength AS DECIMAL(10, 2)) ELSE NULL END, 
             --case when strWidth='' then null else     cast( strWidth as decimal(10,2))end, 
             CASE WHEN Isnumeric(strwidth) = 1 THEN Cast(strwidth AS DECIMAL(10, 2)) ELSE NULL END, 
             --case when strHeight='' then null else   cast(  strHeight as decimal(10,2))end,
             CASE WHEN Isnumeric(strheight) = 1 THEN Cast(strheight AS DECIMAL(10, 2)) ELSE NULL END, 
             --case when strCubicWeight='' then null else    cast( strCubicWeight as decimal(10,2))end, 
             CASE WHEN Isnumeric(strcubicweight) = 1 THEN Cast( strcubicweight AS DECIMAL(10, 2)) ELSE NULL END, 
             --case when strPhysicalWeight='' then null else   cast( strPhysicalWeight as decimal(10,2))end,
             CASE WHEN Isnumeric(strphysicalweight) = 1 THEN Cast( strphysicalweight AS DECIMAL(10, 2)) ELSE NULL END, 
             --case when strMeasureWeight='' then null else    cast(  strMeasureWeight as decimal(10,2))end,
             CASE WHEN Isnumeric(strmeasureweight) = 1 THEN Cast( strmeasureweight AS DECIMAL(10, 2)) ELSE NULL END, 
             --case when strDeclareVolume='' then null else  cast(  strDeclareVolume as decimal(10,2))end, 
             CASE WHEN Isnumeric(strdeclarevolume) = 1 THEN Cast( strdeclarevolume AS DECIMAL(10, 2)) ELSE NULL END, 
             strlastactivity, 
             strlastactivitydatetime, 
             Getdate(), 
             @CreatedBy 
      FROM   @dtItemLabel;

--------------------Update Consignment Staging -------------------------------------------
UPDATE dbo.tblconsignmentstaging 
      SET    [isprocessed] = 1, 
             [updateddatettime] = Getdate(), 
             [updatedby] = @CreatedBy, 
             [consignmentid] = @ConsignmentIDret, 
             isaccountcustomer = 1 
WHERE  consignmentstagingid = @ConsignmentStagingID 
	  
------------------------------------------------------------------------------------------- 

      SELECT dbo.tblconsignment.* 
      FROM   dbo.tblconsignment 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentIDret 

      DECLARE @PicupID INT 


      SELECT @PicupID = dbo.tblconsignment.pickupid, 
             @DestinationID = dbo.tblconsignment.destinationid 
      FROM   dbo.tblconsignment 
      WHERE  dbo.tblconsignment.consignmentid = @ConsignmentIDret 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             dbo.tblstate.statename AS StateID 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @PicupID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             dbo.tblstate.statename AS StateID 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @DestinationID 

      SELECT dbo.tbladdress.addressid, 
             dbo.tbladdress.userid, 
             dbo.tbladdress.firstname, 
             dbo.tbladdress.lastname, 
             dbo.tbladdress.companyname, 
             dbo.tbladdress.email, 
             dbo.tbladdress.address1, 
             dbo.tbladdress.address2, 
             dbo.tbladdress.suburb, 
             dbo.tbladdress.postcode, 
             dbo.tbladdress.phone, 
             dbo.tbladdress.mobile, 
             dbo.tbladdress.createddatetime, 
             dbo.tbladdress.createdby, 
             dbo.tbladdress.updateddatetime, 
             dbo.tbladdress.updatedby, 
             dbo.tbladdress.isregisteraddress, 
             dbo.tbladdress.isdeleted, 
             dbo.tbladdress.isbusiness, 
             dbo.tbladdress.issubscribe, 
             dbo.tblstate.statename AS StateID 
      FROM   dbo.tbladdress 
             LEFT JOIN dbo.tblstate 
                    ON dbo.tbladdress.stateid = dbo.tblstate.stateid 
      WHERE  addressid = @ContactID 

      SELECT * 
      FROM   [dbo].[tblitemlabel] 
      WHERE  consignmentid = @ConsignmentIDret 

      SELECT Cu.userid, 
             c.isregularshipper 
      FROM   tblcompany c 
             INNER JOIN tblcompanyusers Cu 
                     ON c.companyid = Cu.companyid 
      WHERE  ( c.isregularshipper = 1 ) 
             AND Cu.userid = @UserID 
--------------------------------------------------------------------------------------------------------------------------
     
COMMIT TRAN 

END TRY
BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLog]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_CreateDomesticConsignment_WithOutPay]'
				   , 2)
		end
END CATCH

  END 

GO
