SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateEzyNetCustomer_BUP_17_02_2016]
            @PhoneNo int = null ,
            @Branch nvarchar(250) = null ,
            @FirstName nvarchar(50) = null ,
            @Lastname nvarchar(50) = null ,
            @Email nvarchar(250) = null ,
            @CreatedBy int = null ,
           
            @UpdatedBy int = null 
        

AS
BEGIN
--- IF Begin for Check user exist or not
IF EXISTS (SELECT * FROM  [dbo].[tblEzyNetCustomer] WHERE [PhoneNo] = @PhoneNo and [Branch]=@Branch )
Begin
 UPDATE [dbo].[tblEzyNetCustomer] SET
  [PhoneNo]=@PhoneNo,
 [Branch]=@Branch,
 [FirstName]=@FirstName,
 [Lastname]=@Lastname,
 [Email]=@Email,
 [UpdatedBy]=@CreatedBy,
 [UpdatedDateTime]=GETDATE()
Where [PhoneNo] = @PhoneNo and [Branch]=@Branch 
End
----==== Update if user exist
Else
Begin
	INSERT INTO [dbo].[tblEzyNetCustomer]
	 ([PhoneNo]
           ,[Branch]
           ,[FirstName]
           ,[Lastname]
           ,[Email]
           ,[CreatedBy]
          )
     VALUES
           ( @PhoneNo  ,
            @Branch  ,
            @FirstName  ,
            @Lastname  ,
            @Email  ,
            @CreatedBy   )
End
END
GO
