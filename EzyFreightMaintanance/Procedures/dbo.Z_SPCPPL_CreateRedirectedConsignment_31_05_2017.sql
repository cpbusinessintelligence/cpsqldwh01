SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_CreateRedirectedConsignment_31_05_2017]

@UniqueID nvarchar(50)= null ,
@ConsignmentCode varchar(50)= null ,
@SelectedDeliveryOption varchar(100)= null ,
@TotalWeight  decimal(18,2)= null ,
@TotalVolume decimal(18,2)= null ,
@ServiceType varchar(50)= null ,
@RateCardID varchar(50)= null ,
@CurrentETA varchar(50)= null ,
@NewETA varchar(50)= null ,
@NoOfItems int= null ,
@ConsignmentStatus varchar(50)= null ,
@SpecialInstruction varchar(500)= null ,
@Terms bit= 0 ,
@ATL bit= 0 ,
@ConfirmATLInsuranceVoid bit= null ,
@ConfirmDeliveryAddress bit= null ,
@SortCode varchar(50)= null ,
@CalculatedTotal decimal(18,2)= null ,
@CreditCardSurcharge decimal(18,2)= null ,
@CalculatedFerightCharge decimal(18,2)= null,
@NetTotal decimal(18,2)= null ,
@CreatedBy varchar(50)= null,
@dtItemLabel [dtRedirectedItemLabel] READONLY,
@dtSalesOrderDetail [dtSalesOrderDetail_Redirected] READONLY,

@dtDestinationDetail [dtAddressDetail_Redirected] READONLY,
@dtCurrentDelAddressDetail [dtAddressDetail_Redirected] READONLY,
@dtPickupAddressDetail [dtAddressDetail_Redirected] READONLY,

@GST decimal(18,2) = 0,
@SendToPronto bit = 0,
@ConsignmentIDRet int = null,
@UserID int = null ,
@FlagPayment Bit = 0,
@RedirectionRequest nvarchar(max) = null

AS
BEGIN
begin tran
BEGIN TRY
DECLARE @ReConsignmentIDret int,
		@stateIdbyCode_Destination int,
		@stateIdbyCode_CurrentDelivery int,
		@stateIdbyCode_Pickup int,
		@NewDeliveryAddressID int,
		@CurrentDeliveryAddressID int,
		@PickupID int,
		@ContactID int

If (Not Exists(select UniqueID from tblredirectedconsignment WITH (NOLOCK) where UniqueID = @UniqueID and ConsignmentCode = @ConsignmentCode and Isprocessed = 1))
Begin
--------------------------------Pickup StateId------------------------------------------------------------------------------------------------------

        SELECT  @stateIdbyCode_Pickup=[StateID]
        FROM [dbo].[tblState]where [StateCode] in (select [StrStateId] from @dtPickupAddressDetail)

--------------------------------Pickup Address Entry-------------------------------------------------------------------------------------

INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe]
	 ,[Country],[CountryCode] )
	 select
      @UserID,[StrfirstName] ,[StrlastName] ,[StrCompanyName],[StrEmail],[StrAddress1],[StrAddress2],[StrSuburb],[StrStateId],@stateIdbyCode_Pickup,
      [StrPostalCode],[StrPhone],[StrMobile],[StrIsRegisterAddress],GETDATE(), @CreatedBy ,[StrIsBusiness],[StrIsSubscribe]
	  ,[Country],[CountryCode]
	  from @dtPickupAddressDetail;
	  
SET @PickupID = SCOPE_IDENTITY()


--------------------------------CurrentDeliveryAddress StateId------------------------------------------------------------------------------------------------------

        SELECT  @stateIdbyCode_CurrentDelivery=[StateID]
        FROM [dbo].[tblState]where [StateCode] in (select [StrStateId] from @dtCurrentDelAddressDetail)

--------------------------------Current Delivery Address Entry-------------------------------------------------------------------------------------

INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe]
	 ,[Country],[CountryCode] )
	 select
      @UserID,[StrfirstName] ,[StrlastName] ,[StrCompanyName],[StrEmail],[StrAddress1],[StrAddress2],[StrSuburb],[StrStateId],@stateIdbyCode_CurrentDelivery,
      [StrPostalCode],[StrPhone],[StrMobile],[StrIsRegisterAddress],GETDATE(), @CreatedBy ,[StrIsBusiness],[StrIsSubscribe]
	  ,[Country],[CountryCode]
	  from @dtCurrentDelAddressDetail;
	  
SET @CurrentDeliveryAddressID = SCOPE_IDENTITY()
SET @ContactID = @CurrentDeliveryAddressID

if(UPPER(@SelectedDeliveryOption) <> 'AUTHORITY TO LEAVE')
begin
--------------------------------Destination StateID------------------------------------------------------------------------------------------------------

        SELECT  @stateIdbyCode_Destination=[StateID]
        FROM [dbo].[tblState]where [StateCode] in (select [StrStateId] from @dtDestinationDetail)

--------------------------------New Destination Address Entry-------------------------------------------------------------------------------------

		INSERT INTO [dbo].[tblAddress]
	 ( [UserID], [FirstName] ,[LastName],[CompanyNAme],[Email],[Address1],[Address2],[Suburb],StateName,[StateID],
	 [PostCode],[Phone],[Mobile],[IsRegisterAddress],[CreatedDateTime],[CreatedBy],[isBusiness],[IsSubscribe]
	 ,[Country],[CountryCode] )
	 select
      @UserID,[StrfirstName] ,[StrlastName] ,[StrCompanyName],[StrEmail],[StrAddress1],[StrAddress2],[StrSuburb],[StrStateId],@stateIdbyCode_Destination,
      [StrPostalCode],[StrPhone],[StrMobile],[StrIsRegisterAddress],GETDATE(), @CreatedBy ,[StrIsBusiness],[StrIsSubscribe]
	  ,[Country],[CountryCode]
	  from @dtDestinationDetail;
	  
		SET @NewDeliveryAddressID = SCOPE_IDENTITY()

end

ELSE
begin
		SET @NewDeliveryAddressID = @CurrentDeliveryAddressID
end


-------------------------------Redirected Consignment----------------------------------------------------------------------------------------------
--If (Exists(select UniqueID from tblredirectedconsignment NoLock where UniqueID = @UniqueID and ConsignmentCode = @ConsignmentCode and Isprocessed = 1))
--Begin
--Rollback tran
--GoTo ProcedureEnd;
--End

INSERT INTO [dbo].[tblRedirectedConsignment]
           ([UniqueID]
           ,[ConsignmentCode]
           ,[SelectedDeliveryOption]
		   ,[PickupAddressID]
           ,[CurrentDeliveryAddressID]
           ,[NewDeliveryAddressID]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[ServiceType]
           ,[RateCardID]
           ,[CurrentETA]
           ,[NewETA]
           ,[NoOfItems]
           ,[ConsignmentStatus]
           ,[SpecialInstruction]
           ,[Terms]
		   ,[ATL]
           ,[ConfirmATLInsuranceVoid]
           ,[ConfirmDeliveryAddress]
           ,[IsProcessed]
           ,[SortCode]
           ,[CalculatedTotal]
           ,[CreditCardSurcharge]
           ,[NetTotal]
           ,[CreatedDateTime]
           ,[CreatedBy]
		   ,[RedirectionRequest])
     VALUES
           (@UniqueID, 
           @ConsignmentCode, 
           @SelectedDeliveryOption, 
		   @PickupID,
           @CurrentDeliveryAddressID, 
           @NewDeliveryAddressID,					----New Destination Address Id
           @TotalWeight, 
           @TotalVolume, 
           @ServiceType, 
           @RateCardID, 
           @CurrentETA, 
           @NewETA, 
           @NoOfItems, 
           @ConsignmentStatus, 
           @SpecialInstruction, 
           @Terms,
		   @ATL, 
           @ConfirmATLInsuranceVoid, 
           @ConfirmDeliveryAddress, 
           0, 
           @SortCode, 
           @CalculatedTotal, 
           @CreditCardSurcharge, 
           @NetTotal, 
           Getdate(), 
           @CreatedBy, 
		   @RedirectionRequest)

SET @ReConsignmentIDret = SCOPE_IDENTITY()

---------------------ItemLabel ------------------------

INSERT INTO [dbo].[tblRedirectedItemLabel]
           ([ReConsignmentID]
           ,[LabelNumber]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[CubicWeight]
           ,[PhysicalWeight]
           ,[LastActivity]
           ,[LastActivityDateTime]
           ,[CreatedDateTime]
           ,[CreatedBy]
           )
		   SELECT
		   @ReConsignmentIDret, 
		   RTRIM(LabelNumber),
		   case when  ISNUMERIC([Length])=1 then CAST([Length] AS decimal(10,2))else null end ,
		   case when  ISNUMERIC([Width])=1 then CAST([Width] AS decimal(10,2))else null end ,
           case when  ISNUMERIC([Height])=1 then CAST([Height] AS decimal(10,2))else null end ,
		   case when  ISNUMERIC([CubicWeight])=1 then CAST([CubicWeight] AS decimal(10,2))else null end ,
           case when  ISNUMERIC([PhysicalWeight])=1 then CAST([PhysicalWeight] AS decimal(10,2))else null end ,
		   [LastActivity],
		   [LastActivityDateTime],
           GETDATE(),
		   @CreatedBy
		   FROM @dtItemLabel;

-------------------------Sales Order--------------------------
If @FlagPayment = 1
Begin

 DECLARE @SalesOrderIDret int

 INSERT INTO [dbo].[tblSalesOrder]
            ([ReferenceNo]
           ,[UserID]
           ,[NoofItems]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[RateCardID]
           ,[GrossTotal]
           ,[GST]
           ,[NetTotal]
           ,[SalesOrderStatus]
           ,[InvoiceNo]
           ,[CreatedDateTime]
           ,[CreatedBy]
		   ,[ConsignmentCode]
           )
           VALUES
           (@ConsignmentIDRet ,
		    @UserID  ,
            @NoofItems  ,
            @TotalWeight  ,
            @TotalVolume  ,
            @RateCardID  ,
            @CalculatedFerightCharge ,				--@GrossTotal ,
            @GST  ,
            @NetTotal  ,
            7  ,
            null ,
            GETDATE() ,
            @CreatedBy,
			@ConsignmentCode
           )SET @SalesOrderIDret = SCOPE_IDENTITY()

------------------------Sales Order Detail--------------------------------

INSERT INTO [dbo].[tblSalesOrderDetail]
           ([SalesOrderID]
           ,[Description]
           ,[LineNo]
           ,[Weight]
           ,[Volume]
           ,[FreightCharge]
           ,[FuelCharge]
           ,[InsuranceCharge]
           ,[ServiceCharge]
           ,[Total]
           ,[CreatedDateTime]
           ,[CreatedBy])
           select @SalesOrderIDret,
           strDescription,
           [strLineNo],
	        case when  ISNUMERIC(strPhysicalWeight)=1 then CAST(strPhysicalWeight AS decimal(10,2))else null end ,
		    case when  ISNUMERIC(strDeclareVolume)=1 then CAST(strDeclareVolume AS decimal(10,2))else null end ,
			case when  ISNUMERIC(strFreightCharge)=1 then CAST(strFreightCharge AS decimal(10,2))else null end ,
			case when  ISNUMERIC(strFuelCharge)=1 then CAST(strFuelCharge AS decimal(19,4))else null end ,
			case when  ISNUMERIC(strInsuranceCharge)=1 then CAST(strInsuranceCharge AS decimal(19,4))else null end ,
			case when  ISNUMERIC(strServiceCharge)=1 then CAST(strServiceCharge AS decimal(19,4))else null end ,
			case when  ISNUMERIC(strFreightCharge)=1 then CAST(strFreightCharge AS decimal(19,4))else 0.0 end +
			case when  ISNUMERIC(strFuelCharge)=1 then CAST(strFuelCharge AS decimal(19,4))else 0.0 end +
			case when  ISNUMERIC(strInsuranceCharge)=1 then CAST(strInsuranceCharge AS decimal(19,4))else 0.0 end +
			case when  ISNUMERIC(strServiceCharge)=1 then CAST(strServiceCharge AS decimal(19,4))else 0.0 end,
			getdate(),
			@CreatedBy
from @dtSalesOrderDetail

---------------------------Invoice--------------------------
           

declare  @InvoiceNumber nvarchar(5)
set @InvoiceNumber=  SUBSTRING(@ConsignmentCode, 1, 3)+'IN'
declare @InvoiceCode7Digit nvarchar(7)
select @InvoiceCode7Digit = max (RIGHT(InvoiceNumber, 7))+1  from tblInvoice where left(InvoiceNumber,5) = @InvoiceNumber
SELECT @InvoiceCode7Digit =  RIGHT('0000000' + replace(@InvoiceCode7Digit,'-',''),7)
if(@InvoiceCode7Digit is null)
set @InvoiceCode7Digit = '0000000'

DECLARE @InvoiceID int

INSERT INTO [dbo].[tblInvoice]
           ([InvoiceNumber]
           ,[UserID]
           ,[PickupAddressID]
           ,[DestinationAddressID]
           ,[ContactAddressID]
           ,[TotalFreightExGST]
           ,[GST]
           ,[TotalFreightInclGST]
           ,[InvoiceStatus]
           ,[SendToPronto]
           ,[CreatedDateTime]
           ,[CreatedBY]
         )
     VALUES
           (@InvoiceNumber+@InvoiceCode7Digit , 
           @UserID , 
           @PickupID,
		   @NewDeliveryAddressID,
		   @ContactID ,
           @CalculatedFerightCharge,  --@GrossTotal,
           @GST , 
           @NetTotal,
           11 , 
           @SendToPronto , 
           GETDATE() , 
           @CreatedBY 
           )SET @InvoiceID = SCOPE_IDENTITY()

 --------Update Sales order -------
 
update tblSalesOrder set InvoiceNo =  @InvoiceNumber+@InvoiceCode7Digit ,
SalesOrderStatus = 9 where SalesOrderID=  @SalesOrderIDret

END
------------------Update Isprocessed If Payment Is not-------------------------------------------------------------------
If @FlagPayment <> 1
Begin
	update [tblRedirectedConsignment]  set IsProcessed = 1  ,[UpdatedDateTTime] =GETDATE()
		   ,[UpdatedBy] = @CreatedBY where ReConsignmentId=  @ReConsignmentIDret
END


--------------------------------------------------------------------------------------------------------------------------
select @ReConsignmentIDret as ReConsignmentId , @SalesOrderIDret as SalesOrderId, @NewDeliveryAddressID as NewDeliveryAddressID 
	   ,@CurrentDeliveryAddressID as CurrentDeliveryAddressID,@PickupID as PickupID ,	@ContactID as contactID
--------------------------------------------------------------------------------------------------------------------------


End
Else
Begin
--Select 'Record Already In Table' as Msg
ProcedureEnd:  
select ReConsignmentId , s.SalesOrderID as SalesOrderId, NewDeliveryAddressID ,CurrentDeliveryAddressID,PickupAddressID as PickupID  ,CurrentDeliveryAddressID as contactID
	  from tblredirectedconsignment rc  WITH (nolock)
	  Left Join tblsalesorder s  WITH (nolock) on s.ConsignmentCode = rc.ConsignmentCode
	  where UniqueID = @UniqueID and rc.ConsignmentCode = @ConsignmentCode and Isprocessed = 1

End

commit tran
END TRY
BEGIN CATCH
begin
rollback tran
INSERT INTO [dbo].[tblErrorLog]
		   ([Error]
			,[FunctionInfo]
			,[ClientId])
VALUES
            (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
		    ,'SPCPPL_CreateRedirectedConsignment', 2)

end
END CATCH  
End

GO
