SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[Z_SPCPPL_DeleteDeliveryChoices_BUP_23_06_2017]
@DtIds [DtDeliveryChoiceId] readonly 

As Begin 
Begin Try

Delete from [dbo].[vw_DeliveryChoices] where [DeliveryChoiceID]  in (select [strId] COLLATE Latin1_General_CI_AS from @DtIds )
			
select [strId] from @DtIds

END TRY
		BEGIN CATCH
		begin
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'DeleteDeliveryChoices', 2)
		end
		END CATCH

END 






GO
