SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_DomesticBulkCreateShipmentStaging_FORAPI_05_05_2017] 
@UserID INT= NULL,
@AccountNo nvarchar(100) = null,
@customerRefNo nvarchar(40) = null,
@RateCardID nvarchar(50) = null,
@specialInstruction nvarchar(150) = null,
@DangerousGoods  BIT= NULL, 
@IsATL  BIT = NULL, 
---------------Pickup--------------------------------------------
@PickupAddressId int = null,
@DtPickupDetail DtAddressDetail readonly,
---------------Destination--------------------------------------------
@DestinationAddressId int = null,
@DtDestinationDetail DtAddressDetail readonly,
-----------------------------------------------------------
@ContactAddressId int = null,
@DtContactDetail DtAddressDetail readonly,
---------------Contact--------------------------------------------
@IsSameAsDestination          BIT = 0, 
@IsSameAsPickup               BIT = 0, 
@No_Of_Items int = null,
@TotalWeight  DECIMAL(18, 2) = NULL, 
@TotalVolume   DECIMAL(18, 2) = NULL, 
@IsSignatureRequired  BIT = NULL, 
@ConsignmentPreferPickupDate  DATE= NULL, 
@ConsignmentPreferPickupTime VARCHAR(20)= NULL,
@ClosingTime VARCHAR(10)= NULL, 
@ServiceID INT= NULL, 
@CalculatedTotal DECIMAL(18, 2) = NULL, 
@ClientCode VARCHAR(50) = NULL ,
@CreatedBy INT= NULL 

As Begin

Begin Try
BEGIN TRAN 

------------------Pickup Address Detail-------------------------------------
DECLARE @stateIdbyCode INT 
if isnull(@PickupAddressId,0) = 0 
begin
SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  lower([StateCode]) in (select lower([State]) from @DtPickupDetail) or lower([StateName]) in (select lower([State]) from @DtPickupDetail) 
INSERT INTO [dbo].[tblAddress]
           ([UserID],[FirstName],[LastName],[CompanyName],[Email],[Address1],[Address2],[Suburb],[StateName],[StateID],[PostCode],[Phone],
		   [CreatedDateTime],[CreatedBy],[IsRegisterAddress],[IsBusiness])
	(SELECT @UserID,[FirstName],[LastName],[CompanyName],[Email],[Address1],[Address2],[Suburb],[State], @stateIdbyCode,[PostCode],[Phone],
			GETDATE(), @CreatedBy, 0, [IsBusiness]
	 FROM @DtPickupDetail)
SET @PickupAddressID = Scope_identity() 
end
------------------Destination Address Detail-------------------------------------
if isnull(@DestinationAddressId,0) = 0 
begin

SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  lower([StateCode]) in (select lower([State]) from @DtDestinationDetail) or lower([StateName]) in (select lower([State]) from @DtDestinationDetail) 
INSERT INTO [dbo].[tblAddress]
           ([UserID],[FirstName],[LastName],[CompanyName],[Email],[Address1],[Address2],[Suburb],[StateName],[StateID],[PostCode],[Phone],
		   [CreatedDateTime],[CreatedBy],[IsRegisterAddress],[IsBusiness])
	(SELECT @UserID,[FirstName],[LastName],[CompanyName],[Email],[Address1],[Address2],[Suburb],[State], @stateIdbyCode,[PostCode],[Phone],
			GETDATE(), @CreatedBy, 0, [IsBusiness]
	 FROM @DtDestinationDetail)
SET @DestinationAddressID = Scope_identity() 
end
------------------Contact Address Detail-------------------------------------
if isnull(@ContactAddressID,0) = 0 
begin
IF ( @IsSameAsDestination = 1 ) 
	SET @ContactAddressID = @DestinationAddressID 
ELSE IF ( @IsSameAsPickup = 1 ) 
	SET @ContactAddressID = @PickupAddressID 
ELSE 

BEGIN 
SELECT @stateIdbyCode = [stateid] FROM   [dbo].[tblstate] WHERE  lower([StateCode]) in (select lower([State]) from @DtContactDetail) or lower([StateName]) in (select lower([State]) from @DtContactDetail) 
INSERT INTO [dbo].[tblAddress]
           ([UserID],[FirstName],[LastName],[CompanyName],[Email],[Address1],[Address2],[Suburb],[StateName],[StateID],[PostCode],[Phone],
		   [CreatedDateTime],[CreatedBy],[IsRegisterAddress],[IsBusiness])
	(SELECT @UserID,[FirstName],[LastName],[CompanyName],[Email],[Address1],[Address2],[Suburb],[State], @stateIdbyCode,[PostCode],[Phone],
			GETDATE(), @CreatedBy, 0, [IsBusiness]
	 FROM @DtContactDetail)
SET @ContactAddressID = Scope_identity() 

End
end

------------------Consignment Staging Detail-------------------------------------
DECLARE @SortCode NVARCHAR(20) = NULL , @ConsignmentIDret INT
SELECT TOP 1 @SortCode = sortcode FROM   locationmaster WHERE  postcode in (select [PostCode] from @DtDestinationDetail) 

INSERT INTO [dbo].[tblConsignmentStaging]
           ([UserID],[IsRegUserConsignment],[PickupID],[DestinationID],[ContactID],[TotalWeight],[TotalVolume],[NoOfItems],[SpecialInstruction],
		   [CustomerRefNo],[PickupDate],[PreferPickupTime],[ClosingTime],[DangerousGoods],[Terms],[ServiceID],[RateCardID],[IsProcessed],
		   [CreatedDateTime],[CreatedBy],[IsSignatureReq],[IsATl],[SortCode],[ETA],[IsAccountCustomer],[CalculatedTotal],[ClientCode])
     VALUES
		   (@UserID,1,@PickupAddressID,@DestinationAddressID,@ContactAddressID,@TotalWeight,@TotalVolume,@No_Of_Items,@specialInstruction,
		   @customerRefNo,@ConsignmentPreferPickupDate,@ConsignmentPreferPickupTime,@ClosingTime,@DangerousGoods,1,@ServiceID,@RateCardID,0,
		   Getdate(),@CreatedBy, @IsSignatureRequired,@IsATL,@SortCode,null,1,@CalculatedTotal,@ClientCode)

SET @ConsignmentIDret = Scope_identity()
-----------------------------------------------------------------------------------

SELECT @ConsignmentIDret as ConsignmentStagingID

COMMIT TRAN 

END TRY
		BEGIN CATCH
		begin
			rollback tran
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'[SPCPPL_ DomesticBulkCreateShipmentStaging_FORAPI]', 2)
		end
		END CATCH

END 

GO
