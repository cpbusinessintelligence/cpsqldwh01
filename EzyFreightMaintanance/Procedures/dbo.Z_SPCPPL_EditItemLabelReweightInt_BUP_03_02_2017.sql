SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Z_SPCPPL_EditItemLabelReweightInt_BUP_03_02_2017]
     @ItemLabelID int=null,
      @Quantity int=null,
           @MeasureLength decimal(10,2)=null,
           @MeasureWidth decimal(10,2)=null,
           @MeasureHeight decimal(10,2)=null,
           @MeasureWeight decimal(10,2)=null,
           @UpdatedBy int=null
           
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
begin tran
UPDATE [dbo].[tblItemLabel] 
   SET 
      [Quantity] = case when  @Quantity is null then [Quantity] else @Quantity end
      ,[MeasureLength] =case when   @MeasureLength is null then [MeasureLength] else @MeasureLength end
      ,[MeasureWidth] =case when   @MeasureWidth is null then [MeasureWidth] else @MeasureWidth end
      ,[MeasureHeight] =case when   @MeasureHeight is null then [MeasureHeight] else @MeasureHeight end
      ,[MeasureWeight] =case when   @MeasureWeight is null then [MeasureWeight] else @MeasureWeight end
      ,[MeasureCubicWeight] = (@MeasureLength/100)*(@MeasureWidth/100)*(@MeasureHeight/100)*200
      ,[UpdatedDateTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
 WHERE ItemLabelID = @ItemLabelID
            
         declare    @ConsignmentID int
         select @ConsignmentID =ConsignmentID from tblItemLabel with (nolock) where ItemLabelID = @ItemLabelID
            
         declare    @SumMeasureCubicWeight decimal(10,2),@SumMeasureWeight decimal(10,2)
            SELECT   @SumMeasureCubicWeight= SUM( MeasureCubicWeight),@SumMeasureWeight=SUM(MeasureWeight)
FROM         dbo.tblItemLabel  with (nolock) 
WHERE     (ConsignmentID = @ConsignmentID)


update dbo.tblConsignment
set 
TotalMeasureWeight = @SumMeasureWeight,
TotalMeasureVolume = @SumMeasureCubicWeight
WHERE     (ConsignmentID = @ConsignmentID)

--IF EXISTS (SELECT * FROM dbo.tblReWeight WHERE (ConsignmentID = @ConsignmentID and IsProcessed=0 ) )
--begin
--  update tblReWeight
--  set 
--  ConsignmentID = @ConsignmentID
--  WHERE (ConsignmentID = @ConsignmentID and IsProcessed=0)
--END 
-- ELSE
--begin
--   INSERT INTO tblReWeight
--   ([ConsignmentID]
--           ,[Context1]
--           ,[Context2]
--           ,[Context3]
--           ,[Context4]
--           ,[NumberOfItem]
--           ,[ServiceType]
--           ,[Category]
           
        
--           ,[CreatedBy]
--           )
--     VALUES
--           (@ConsignmentID
--           ,''
--           ,''
--           ,''
--           ,''
--           ,1
--           ,'EXP'
--           ,'ReWeight'
        
       
--           ,@UpdatedBy
--          )
--END 


SELECT     dbo.tblConsignment.TotalMeasureWeight, dbo.tblConsignment.TotalMeasureVolume, dbo.tblAddress.PostCode AS FromPostCode, 
                      dbo.tblAddress.Suburb AS FromSuburb, tblAddress_1.Suburb AS ToSuburb, tblAddress_1.PostCode AS ToPostCode, dbo.tblConsignment.NoOfItems, 
                   dbo.tblInvoice.MerchantReferenceCode,       dbo.tblConsignment.ConsignmentID,   dbo.tblConsignment.RateCardID, dbo.tblSalesOrder.GrossTotal,@MeasureWeight as MeasureWeight,
                      (@MeasureLength/100)*(@MeasureWidth/100)*(@MeasureHeight/100)*250 as MeasureCubicWeight
                      , dbo.tblAddress.Country AS FromCountry, tblAddress_1.Country AS ToCountry
                 , dbo.tblConsignment.IsSignatureReq, dbo.tblConsignment.IsDocument, dbo.tblInvoice.PaymentRefNo

FROM         dbo.tblConsignment  with (nolock) INNER JOIN
                      dbo.tblAddress  with (nolock) ON dbo.tblConsignment.PickupID = dbo.tblAddress.AddressID INNER JOIN
                      dbo.tblAddress AS tblAddress_1  with (nolock) ON dbo.tblConsignment.DestinationID = tblAddress_1.AddressID INNER JOIN
                      dbo.tblSalesOrder  with (nolock) ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo  left JOIN
                      dbo.tblInvoice  with (nolock) ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
                      where (dbo.tblConsignment.ConsignmentID = @ConsignmentID)
  
   if(@@ERROR<>0)
   begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'SPCPPL_EditItemLabelReweightInt'
           , 2)
  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
  end
  else
  commit tran          
            
  
END 


GO
