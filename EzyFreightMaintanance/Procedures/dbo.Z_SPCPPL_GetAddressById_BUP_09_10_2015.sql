SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Z_SPCPPL_GetAddressById_BUP_09_10_2015]
@AddressID int = null
AS
BEGIN
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.StateID, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, 
                      dbo.tblAddress.Mobile, dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, 
                      dbo.tblAddress.IsRegisterAddress, dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, dbo.tblState.StateCode
FROM         dbo.tblAddress LEFT OUTER JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where [AddressID]=@AddressID
END

---[SPCPPL_GetAdminZoneWiseUsers] 41,1,10,'FirstName','ASC',0
--- select * from tblAddress
GO
