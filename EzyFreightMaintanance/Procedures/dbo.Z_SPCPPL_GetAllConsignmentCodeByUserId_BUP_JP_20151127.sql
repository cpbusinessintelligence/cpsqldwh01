SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[Z_SPCPPL_GetAllConsignmentCodeByUserId_BUP_JP_20151127]
@Username nvarchar(max)
 
AS
BEGIN
 

						SELECT     consign.ConsignmentCode 
						,dbo.UNIX_TIMESTAMP(consign.CreatedDateTime) DateSubmitted
						,consign.CreatedDateTime DateSubmittednew
FROM         dbo.tblConsignment AS consign inner join
[CPPLWeb].[dbo].[Users] as Ur on
consign.UserID = Ur.[UserID]
WHERE     (Ur. Username  =@Username) and consign.IsInternational =1 and consign.IsProcessed =1
			

END

	
GO
