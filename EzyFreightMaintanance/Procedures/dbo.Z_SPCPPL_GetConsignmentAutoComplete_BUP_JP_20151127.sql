SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_GetConsignmentAutoComplete_BUP_JP_20151127]
@UserId int,
@Name nvarchar (max),
@strConsignmentNo nvarchar (max),

@PageIndex int=1,
 @SortColumn varchar(50) = null,
  @SortDir varchar(50)=null,
@PageSize int=10

AS
BEGIN

declare @stateId int 




SELECT top (1)    @stateId=AddIn.StateID
FROM         dbo.tblAddress AS AddIn
WHERE     (AddIn.UserID = @UserId)





select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY tblInner.UserID DESC)  RowNum, * 
					FROM 
					(




	SELECT     consign.ConsignmentID, consign.ConsignmentCode, consign.UserID, consign.PickupID, consign.DestinationID, consign.ContactID, 
                      addPikup.FirstName AS PickFirstName, addPikup.LastName AS PickLastName, addPikup.CompanyNAme AS PickCompanyNAme, 
                      addDest.FirstName AS DestFirstName, addDest.LastName AS DestLastName, addDest.CompanyNAme AS DestCompanyNAme, addDest.Suburb AS DestSuburab, 
                      addDest.PostCode AS DestPostCode, addContct.FirstName AS ContFirstName, addContct.LastName AS ContLastName, 
                      addContct.CompanyNAme AS ContCompanyNAme, dbo.tblStatus.StatusContext, dbo.tblStatus.StatusDescription , GETDATE() as ActivityDate
FROM         dbo.tblConsignment AS consign INNER JOIN
                      dbo.tblAddress AS addPikup ON consign.PickupID = addPikup.AddressID INNER JOIN
                      dbo.tblAddress AS addDest ON consign.DestinationID = addDest.AddressID INNER JOIN
                      dbo.tblAddress AS addContct ON consign.ContactID = addContct.AddressID LEFT OUTER JOIN
                      dbo.tblStatus ON consign.ConsignmentStatus = dbo.tblStatus.StatusID
WHERE     (addDest.StateID = @stateId) and (addDest.CompanyName like '%'+@Name+'%' or
addDest.LastName like '%'+@Name+'%' or
addDest.FirstName like '%'+@Name+'%' 
) and (consign.ConsignmentCode like '%'+@strConsignmentNo+'%'  or consign.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL where IL.LabelNumber  like '%'+@strConsignmentNo+'%') )


) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN PickFirstName END DESC,
 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC'  THEN PickFirstName END;
  
  CASE WHEN  @SortDir = 'DESC' THEN @SortColumn END DESC,
  CASE WHEN @SortDir = 'ASC'  THEN @SortColumn END;
  
  
  SELECT    count( co.ConsignmentID)
FROM         dbo.tblConsignment AS co INNER JOIN
                      dbo.tblAddress AS addDest ON co.DestinationID = addDest.AddressID
WHERE     (addDest.StateID = @stateId) and (addDest.CompanyName like '%'+@Name+'%' or
addDest.LastName like '%'+@Name+'%' or
addDest.FirstName like '%'+@Name+'%' 
) and (co.ConsignmentCode like '%'+@strConsignmentNo+'%'  or co.ConsignmentID in (select IL.ConsignmentID from tblItemLabel IL where IL.LabelNumber  like '%'+@strConsignmentNo+'%') )
  
END

---[SPCPPL_GetAdminZoneWiseUsers] 41,1,10,'FirstName','ASC',0
--- select * from tblAddress





-------------------------------------------------------------------------------------------------------------------

GO
