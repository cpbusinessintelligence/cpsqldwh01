SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Z_SPCPPL_GetConsignmentById_BUP_JP_20151127]
@ConsignmentId int
AS
BEGIN


SELECT     dbo.tblConsignment.ConsignmentID, dbo.tblConsignment.ConsignmentCode, dbo.tblConsignment.UserID, dbo.tblConsignment.IsRegUserConsignment, 
                      dbo.tblConsignment.PickupID, dbo.tblConsignment.DestinationID, dbo.tblConsignment.ContactID, dbo.tblConsignment.TotalWeight, 
                      dbo.tblConsignment.TotalMeasureWeight, dbo.tblConsignment.TotalVolume, dbo.tblConsignment.TotalMeasureVolume, dbo.tblConsignment.NoOfItems, 
                      dbo.tblConsignment.SpecialInstruction, dbo.tblConsignment.CustomerRefNo, dbo.tblConsignment.ConsignmentPreferPickupTime, 
                      dbo.tblConsignment.ConsignmentPreferPickupDate, dbo.tblConsignment.ClosingTime, dbo.tblConsignment.DangerousGoods, dbo.tblConsignment.Terms, 
                      dbo.tblConsignment.RateCardID, dbo.tblConsignment.LastActivity, dbo.tblConsignment.LastActiivityDateTime, dbo.tblConsignment.ConsignmentStatus, 
                      dbo.tblConsignment.EDIDataProcessed, dbo.tblConsignment.ProntoDataExtracted, dbo.tblConsignment.CreatedDateTime, dbo.tblConsignment.CreatedBy, 
                      dbo.tblConsignment.UpdatedDateTTime, dbo.tblConsignment.UpdatedBy, dbo.Users.Username, Pickup.FirstName AS PickupFirstName, 
                      Pickup.LastName AS PickupLastName, Pickup.CompanyName AS PickupCompanyName, Pickup.Email AS PickupEmail, Pickup.Address1 AS PickupAddress1, 
                      Pickup.Address2 AS PickupAddress2, Pickup.Suburb AS PickupSuburb, Pickup.PostCode AS PickupPostCode, Pickup.Phone AS PickupPhone, 
                      Pickup.Mobile AS PickupMobile, Pickup.IsBusiness AS PickupIsBusiness, Destination.FirstName AS DestinationFirstName, 
                      Destination.LastName AS DestinationLastName, Destination.CompanyName AS DestinationCompanyName, Destination.Email AS DestinationEmail, 
                      Destination.Address1 AS DestinationAddress1, Destination.Address2 AS DestinationAddress2, Destination.Suburb AS DestinationSuburb, 
                      Destination.PostCode AS DestinationPostCode, Destination.Phone AS DestinationPhone, Destination.Mobile AS DestinationMobile, 
                      Destination.IsBusiness AS DestinationIsBusiness
FROM         dbo.tblConsignment LEFT OUTER JOIN
                      dbo.tblAddress AS Destination ON dbo.tblConsignment.ContactID = Destination.AddressID LEFT OUTER JOIN
                      dbo.tblAddress AS Pickup ON dbo.tblConsignment.PickupID = Pickup.AddressID LEFT OUTER JOIN
                      dbo.Users ON dbo.tblConsignment.UserID = dbo.Users.UserID
WHERE     (tblConsignment.ConsignmentID = @ConsignmentId)
END

---[SPCPPL_GetAdminZoneWiseUsers] 41,1,10,'FirstName','ASC',0
--- select * from tblAddress
GO
