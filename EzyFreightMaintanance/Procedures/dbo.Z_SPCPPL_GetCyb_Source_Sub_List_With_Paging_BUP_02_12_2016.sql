SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[Z_SPCPPL_GetCyb_Source_Sub_List_With_Paging_BUP_02_12_2016]
@SubscriptionId varchar(max)=null,
@MerchantRefCode varchar(max)=null,
@PaymentRefNo varchar(max)=null,
@AuthorizationCode varchar(max)=null,
@FromDate date=null,
@ToDate date=null,
@PageIndex int = 0,
@PageSize int = 10,
@SortColumn varchar(50) = null,
@SortDir varchar(50)=null

AS

BEGIN
  

select tblOuter.* from(
SELECT ROW_NUMBER() OVER(ORDER BY createddatetime Asc, 
CASE WHEN @SortColumn = 'SubscriptionID' AND @SortDir = 'DESC' THEN   SubscriptionID END DESC,
CASE WHEN @SortColumn = 'MerchantReferenceCode' AND @SortDir = 'DESC' THEN    MerchantReferenceCode END DESC,
CASE WHEN @SortColumn = 'PaymentRefNo' AND @SortDir = 'DESC' THEN  PaymentRefNo END DESC,
CASE WHEN @SortColumn = 'AuthorizationCode' AND @SortDir = 'DESC' THEN   AuthorizationCode END DESC,
CASE WHEN @SortColumn = 'createddatetime' AND @SortDir = 'DESC' THEN createddatetime END DESC,

CASE WHEN @SortColumn = 'SubscriptionID' AND @SortDir = 'asc' THEN   SubscriptionID END asc,
CASE WHEN @SortColumn = 'MerchantReferenceCode' AND @SortDir = 'asc' THEN    MerchantReferenceCode END asc,
CASE WHEN @SortColumn = 'PaymentRefNo' AND @SortDir = 'asc' THEN  PaymentRefNo END asc,
CASE WHEN @SortColumn = 'AuthorizationCode' AND @SortDir = 'asc' THEN   AuthorizationCode END asc,
CASE WHEN @SortColumn = 'createddatetime' AND @SortDir = 'asc' THEN createddatetime END asc

) As RowNum
, SubscriptionID, MerchantReferenceCode, PaymentRefNo, AuthorizationCode,FORMAT(createddatetime,'dd/MM/yyyy HH:mm')  as CreatedDate from [tblInvoice]  
where isnull(SubscriptionID,'') <>'' and SubscriptionID not in (SELECT SubscriptionID from [tblDeletedSubscription])
and CASE WHEN isnull(@SubscriptionId,'') = '' THEN isnull(@SubscriptionId,'') else SubscriptionID  END like '%'+isnull(@SubscriptionId,'')+'%' 
and CASE WHEN isnull(@PaymentRefNo,'') = '' THEN isnull(@PaymentRefNo,'') else PaymentRefNo  END like '%'+isnull(@PaymentRefNo,'')+'%' 
and CASE WHEN isnull(@AuthorizationCode,'') = '' THEN isnull(@AuthorizationCode,'') else AuthorizationCode  END like '%'+isnull(@AuthorizationCode,'')+'%' 
and CASE WHEN isnull(@FromDate,'')='' THEN CAST( createddatetime as DATE) else @FromDate END <= CAST( createddatetime as DATE) 
and CASE WHEN isnull(@ToDate,'')='' THEN CAST( createddatetime as DATE) else @ToDate END >= CAST( createddatetime as DATE)

)as tblOuter
where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
or (@PageIndex=2 and @Pagesize=0)
order by 
CASE WHEN @SortColumn = 'SubscriptionID' AND @SortDir = 'DESC' THEN   tblOuter.SubscriptionID END DESC,
CASE WHEN @SortColumn = 'MerchantReferenceCode' AND @SortDir = 'DESC' THEN    tblOuter.MerchantReferenceCode END DESC,
CASE WHEN @SortColumn = 'PaymentRefNo' AND @SortDir = 'DESC' THEN  tblOuter.PaymentRefNo END DESC,
CASE WHEN @SortColumn = 'AuthorizationCode' AND @SortDir = 'DESC' THEN   tblOuter.AuthorizationCode END DESC,
CASE WHEN @SortColumn = 'createddatetime' AND @SortDir = 'DESC' THEN tblOuter.CreatedDate END DESC,

CASE WHEN @SortColumn = 'SubscriptionID' AND @SortDir = 'asc' THEN   tblOuter.SubscriptionID END asc,
CASE WHEN @SortColumn = 'MerchantReferenceCode' AND @SortDir = 'asc' THEN    tblOuter.MerchantReferenceCode END asc,
CASE WHEN @SortColumn = 'PaymentRefNo' AND @SortDir = 'asc' THEN  tblOuter.PaymentRefNo END asc,
CASE WHEN @SortColumn = 'AuthorizationCode' AND @SortDir = 'asc' THEN   tblOuter.AuthorizationCode END asc,
CASE WHEN @SortColumn = 'createddatetime' AND @SortDir = 'asc' THEN tblOuter.CreatedDate END asc





-------------Count Section-----------------------

SELECT Count(*) As Count from [tblInvoice]  
where isnull(SubscriptionID,'') <>'' and SubscriptionID not in (SELECT SubscriptionID from [tblDeletedSubscription])
and CASE WHEN isnull(@SubscriptionId,'') = '' THEN isnull(@SubscriptionId,'') else SubscriptionID  END like '%'+isnull(@SubscriptionId,'')+'%'
and CASE WHEN isnull(@MerchantRefCode,'') = '' THEN isnull(@MerchantRefCode,'') else MerchantReferenceCode  END like '%'+isnull(@MerchantRefCode,'')+'%' 
and CASE WHEN isnull(@MerchantRefCode,'') = '' THEN isnull(@MerchantRefCode,'') else MerchantReferenceCode  END like '%'+isnull(@MerchantRefCode,'')+'%' 
and CASE WHEN isnull(@PaymentRefNo,'') = '' THEN isnull(@PaymentRefNo,'') else PaymentRefNo  END like '%'+isnull(@PaymentRefNo,'')+'%' 
and CASE WHEN isnull(@AuthorizationCode,'') = '' THEN isnull(@AuthorizationCode,'') else AuthorizationCode  END like '%'+isnull(@AuthorizationCode,'')+'%' 
and CASE WHEN isnull(@FromDate,'')='' THEN CAST( createddatetime as DATE) else @FromDate END <= CAST( createddatetime as DATE) 
and CASE WHEN isnull(@ToDate,'')='' THEN CAST( createddatetime as DATE) else @ToDate END >= CAST( createddatetime as DATE)

END

GO
