SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_GetDeliveryChoices_BUP_23_06_2017]
@strId int = null, 
@strCategory nvarchar(12) = null,
@strSuburb nvarchar(100) = null
AS
BEGIN

SELECT *, [Operation Hours] as OperationHours FROM vw_DeliveryChoices 
where Case When isnull(@strId,'') <> '' then Id else '' end = isnull(@strId,'') and
(Case When isnull(@strCategory,'') <> '' then Category else '' end like '%'+ isnull(@strCategory,'') + '%' and
Case When isnull(@strSuburb,'') <> '' then Suburb +' ' + [State] + ' ' + PostCode  else '' end like '%'+ isnull(@strSuburb,'') + '%' )
order by CreatedDateTime desc, ID desc

END
GO
