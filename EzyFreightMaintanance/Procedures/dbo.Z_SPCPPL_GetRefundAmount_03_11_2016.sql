SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_GetRefundAmount_03_11_2016]
           @ConsignmentID int= null
AS
BEGIN
SELECT     SUM(INV.TotalFreightInclGST) AS TotalFreightInclGST, SUM(RF.Amount)/so.nobe AS RefundAmount,so.nobe
FROM         (SELECT     SUM(Amount) AS Amount,  ConsignmentID
                       FROM          dbo.tblRefund AS RFD
                       WHERE      (ConsignmentID IS NOT NULL) AND (ConsignmentID = @ConsignmentID)
                       GROUP BY ConsignmentID) AS RF RIGHT OUTER JOIN
                      (SELECT   *,
                      (SELECT     COUNT(*) AS nobe
FROM         dbo.tblSalesOrder where ReferenceNo=@ConsignmentID
GROUP BY  ReferenceNo)as nobe
FROM         dbo.tblSalesOrder
) AS SO ON RF.ConsignmentID = SO.ReferenceNo RIGHT OUTER JOIN
                      dbo.tblInvoice AS INV ON SO.InvoiceNo = INV.InvoiceNumber
WHERE     (SO.ReferenceNo = @ConsignmentID)
GROUP BY SO.ReferenceNo,so.nobe
                     
           
END

---[SPCPPL_GetAdminZoneWiseUsers] 41,1,10,'FirstName','ASC',0
--- select * from tblAddress
GO
