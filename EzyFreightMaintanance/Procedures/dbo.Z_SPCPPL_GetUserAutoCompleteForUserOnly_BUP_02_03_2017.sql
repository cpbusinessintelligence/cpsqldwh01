SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_GetUserAutoCompleteForUserOnly_BUP_02_03_2017]
@UserId int,
@Text nvarchar (max)= null,
@email nvarchar (max)= null,
@address nvarchar (max)= null,

@PageIndex int=1,
 @SortColumn varchar(50) = null,
  @SortDir varchar(50)=null,
@PageSize int=10

AS
BEGIN

declare @stateId int 
declare @StateCode varchar(10) 

SELECT top (1)    @stateId=AddIn.StateID
FROM         dbo.tblAddress AS AddIn
WHERE     (AddIn.UserID = @UserId)


            select  top (1)     @StateCode=StateCode
           from  dbo.tblState where StateID = @stateId



select * from 
				(SELECT ROW_NUMBER() OVER(ORDER BY 
  CASE WHEN @SortColumn = 'CompanyName' AND @SortDir = 'DESC' THEN    CompanyName END DESC,
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'DESC' THEN   FirstName END DESC,
  CASE WHEN @SortColumn = 'Address1' AND @SortDir = 'DESC' THEN Address1 END DESC,
  CASE WHEN @SortColumn = 'Email' AND @SortDir = 'DESC' THEN Email END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'CompanyName' AND @SortDir = 'ASC' THEN CompanyName END  ,
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'ASC' THEN FirstName END  ,
  CASE WHEN @SortColumn = 'Address1' AND @SortDir = 'ASC' THEN Address1 END  ,
  CASE WHEN @SortColumn = 'Email' AND @SortDir = 'ASC' THEN Email END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN CreatedDateTime END  )  RowNum, * 
					FROM 
					(



SELECT top 100 percent [Add].*,@StateCode as StateCode
FROM         dbo.tblAddress AS [Add] 

where 
--([Add] .FirstName like '%'+@Text+'%' or [Add] .LastName like '%'+@Text+'%' or [Add] .CompanyName like '%'+@Text+'%' 
--or [Add] .Email like '%'+@email+'%' or [Add] .Address1 like '%'+@address+'%'or [Add] .Address2 like '%'+@address+'%') 

--and 

(
 CASE WHEN @Text IS null THEN isnull(@Text,'') else [Add] .FirstName  END LIKE '%'+isnull(@Text,'')+'%' or
 CASE WHEN @Text IS null THEN isnull(@Text,'') else [Add].LastName  END LIKE '%'+isnull(@Text,'')+'%' or
 CASE WHEN @Text IS null THEN isnull(@Text,'') else [Add].CompanyName  END LIKE '%'+isnull(@Text,'')+'%' 

) and

 (
 CASE WHEN @email IS null THEN isnull(@email,'') else [Add] .Email  END LIKE '%'+isnull(@email,'')+'%'  

) 
and

 (
 CASE WHEN @address IS null THEN isnull(@address,'') else [Add] .Address1  END LIKE '%'+isnull(@address,'')+'%'   or
 CASE WHEN @address IS null THEN isnull(@address,'') else [Add] .Address2  END LIKE '%'+isnull(@address,'')+'%'  

) 


 and [Add].UserID =@UserId and IsRegisterAddress=0 and IsDeleted=0

ORDER BY 
  CASE WHEN @SortColumn = 'CompanyName' AND @SortDir = 'DESC' THEN [Add].CompanyName END DESC,
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'DESC' THEN [Add].FirstName END DESC,
  CASE WHEN @SortColumn = 'Address1' AND @SortDir = 'DESC' THEN [Add].Address1 END DESC,
  CASE WHEN @SortColumn = 'Email' AND @SortDir = 'DESC' THEN [Add].Email END DESC,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'DESC' THEN [Add].CreatedDateTime END DESC,
  
    CASE WHEN @SortColumn = 'CompanyName' AND @SortDir = 'ASC' THEN [Add].CompanyName END  ,
  CASE WHEN @SortColumn = 'FirstName' AND @SortDir = 'ASC' THEN [Add].FirstName END  ,
  CASE WHEN @SortColumn = 'Address1' AND @SortDir = 'ASC' THEN [Add].Address1 END  ,
  CASE WHEN @SortColumn = 'Email' AND @SortDir = 'ASC' THEN [Add].Email END  ,
  CASE WHEN @SortColumn = 'CreatedDateTime' AND @SortDir = 'ASC' THEN [Add].CreatedDateTime END   

) as tblInner
				  )	as tblOuter where (tblOuter.RowNum	between (((@PageIndex -1) *@PageSize)+1) and @PageIndex *    @PageSize) 
				or (@PageIndex=0 and @Pagesize=0)
				ORDER BY 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'DESC' THEN PickFirstName END DESC,
 
  --CASE WHEN @SortColumn = 'PickFirstName' AND @SortDir = 'ASC'  THEN PickFirstName END;
  
  CASE WHEN  @SortDir = 'DESC' THEN @SortColumn END DESC,
  CASE WHEN @SortDir = 'ASC'  THEN @SortColumn END;
  
  
  SELECT COUNT( *)
FROM         dbo.tblAddress AS [Add] 

where 

(
 CASE WHEN @Text IS null THEN isnull(@Text,'') else [Add] .FirstName  END LIKE '%'+isnull(@Text,'')+'%' or
 CASE WHEN @Text IS null THEN isnull(@Text,'') else [Add].LastName  END LIKE '%'+isnull(@Text,'')+'%' or
 CASE WHEN @Text IS null THEN isnull(@Text,'') else [Add].CompanyName  END LIKE '%'+isnull(@Text,'')+'%' 

) and

 (
 CASE WHEN @email IS null THEN isnull(@email,'') else [Add] .Email  END LIKE '%'+isnull(@email,'')+'%'  

) 
and

 (
 CASE WHEN @address IS null THEN isnull(@address,'') else [Add] .Address1  END LIKE '%'+isnull(@address,'')+'%'   or
 CASE WHEN @address IS null THEN isnull(@address,'') else [Add] .Address2  END LIKE '%'+isnull(@address,'')+'%'  

) 


 and [Add].UserID =@UserId and IsRegisterAddress=0 and IsDeleted=0
  
END





GO
