SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Z_SPCPPL_GetUserIdsByCompanyId_BUP_17_06_2016]
      
@CompanyID int = null
           
      
AS
BEGIN
 
 
 
SELECT     tblCompanyUsers.UserID, tblCompanyUsers.IsUserDisabled, tblAdd.Username, 
case when tblCompany.IsRegularShipper IS  null then 0 else tblCompany.IsRegularShipper end IsRegularShipper,
 tblCompany.ShipperCreatedBy, 
                      FORMAT(tblCompany.ShipperCreatedDateTime,'dd/MM/yyyy hh:mm:s tt')  as ShipperCreatedDateTime, tblCompany.ShipperUpdatedBy, 
                     FORMAT(tblCompany.ShipperUpdatedDateTime,'dd/MM/yyyy hh:mm:s tt')  as ShipperUpdatedDateTime, tblAdd1.Username AS ShipperCreatedByName, 
                      tblAdd2.Username AS ShipperUpdatedByName
FROM         tblCompanyUsers LEFT OUTER JOIN
                      tblCompany ON tblCompanyUsers.CompanyID = tblCompany.CompanyID LEFT OUTER JOIN
                          (SELECT     UserID, Username
                            FROM          CPPLWeb_8_3.dbo.Users) tblAdd1 ON tblCompany.ShipperCreatedBy = tblAdd1.UserID LEFT OUTER JOIN
                          (SELECT     UserID, Username
                            FROM          CPPLWeb_8_3.dbo.Users Users_2) tblAdd2 ON tblCompany.ShipperUpdatedBy = tblAdd2.UserID LEFT OUTER JOIN
                          (SELECT     UserID, Username
                            FROM          CPPLWeb_8_3.dbo.Users Users_1) tblAdd ON tblCompanyUsers.UserID = tblAdd.UserID
WHERE       tblCompanyUsers.CompanyID=@CompanyID
  
 
 
END


 

GO
