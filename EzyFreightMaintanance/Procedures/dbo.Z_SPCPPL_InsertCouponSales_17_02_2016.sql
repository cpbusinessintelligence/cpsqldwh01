SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[Z_SPCPPL_InsertCouponSales_17_02_2016]

@SalesType varchar(50) = null,
@Branch varchar(50) = null,
@RunNumber varchar(50) = null,
@ContractorNumber varchar(50) = null,
@SalesDateTime datetime= null,
@InvoiceNumber varchar(50) = null,
@CustomerPhoneNumber varchar(50) = null,
@CustomerDLB varchar(50) = null,
@StartCouponNumber varchar(50) = null,
@InsuranceType varchar(50) = null,
@CouponAmount varchar(50) = null,
@InsuranceAmount varchar(50) = null,
@GST varchar(50) = null,
@TotalAmount varchar(50) = null,
@IsProcessed  bit= 0,
@CreatedBy varchar(50) = null,
@CreatedDateTime datetime= null,
@UpdatedBy varchar(50) = null,
@UpdatedDateTime  datetime= null

AS

BEGIN

INSERT INTO [dbo].[tblCouponSales]
           ([SalesType]
           ,[Branch]
           ,[RunNumber]
           ,[ContractorNumber]
           ,[SalesDateTime]
           ,[InvoiceNumber]
           ,[CustomerPhoneNumber]
           ,[CustomerDLB]
           ,[StartCouponNumber]
           ,[InsuranceType]
           ,[CouponAmount]
           ,[InsuranceAmount]
           ,[GST]
           ,[TotalAmount]
           ,[IsProcessed]
           ,[CreatedBy]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedDateTime])
     VALUES
           (@SalesType,
			@Branch,
			@RunNumber,
			@ContractorNumber,
			@SalesDateTime,
			@InvoiceNumber,
			@CustomerPhoneNumber,
			@CustomerDLB,
			@StartCouponNumber,
			@InsuranceType,
			@CouponAmount,
			@InsuranceAmount,
			@GST,
			@TotalAmount,
			@IsProcessed,
			@CreatedBy,
			@CreatedDateTime,
			@UpdatedBy,
			@UpdatedDateTime
			)
    		

End


GO
