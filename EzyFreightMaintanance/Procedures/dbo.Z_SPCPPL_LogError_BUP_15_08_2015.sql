SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


create  PROCEDURE [dbo].[Z_SPCPPL_LogError_BUP_15_08_2015]
@Error nvarchar(max) = null,
@FunctionInfo  nvarchar(max) = null,
 @ClientId  nvarchar(max) = null
AS
BEGIN
INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (@Error
           ,@FunctionInfo
           , @ClientId)

END



-- [SPCPPL_GetAdminConsignment1] 15,1,10
GO
