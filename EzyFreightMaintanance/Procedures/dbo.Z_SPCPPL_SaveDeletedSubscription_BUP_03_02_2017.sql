SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Z_SPCPPL_SaveDeletedSubscription_BUP_03_02_2017]

@PaymentRefNo  varchar(100) = null,
@AuthorizationCode  varchar(100) = null,
@MerchantReferenceCode  varchar(150) = null,
@SubscriptionID  varchar(150) = null,
@NewRequestID  varchar(150) = null,
@Message  varchar(max) = null,
@CreatedDateTime  datetime = null,
@CreatedBY  varchar(50) = null,
@UpdatedDateTime  datetime = null,
@UpdatedBy  varchar(50) = null

As 
Begin

INSERT INTO [dbo].[tblDeletedSubscription]
           ([PaymentRefNo]
           ,[AuthorizationCode]
           ,[MerchantReferenceCode]
           ,[SubscriptionID]
           ,[NewRequestID]
           ,[Message]
           ,[CreatedBY])
     VALUES
           (
		   @PaymentRefNo ,
           @AuthorizationCode ,
           @MerchantReferenceCode ,
           @SubscriptionID ,
           @NewRequestID ,
           @Message ,
           @CreatedBY)
End
GO
