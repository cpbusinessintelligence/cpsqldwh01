SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Z_SPCPPL_UpdateCompanyUserByCompanyId_BUP_17_06_2016]
@CompanyId          int = null ,
 
@IsUserDisabled   bit = null ,
@ReasonSubject    nvarchar(max) = null ,
@ReasonDesciption  nvarchar(max) = null, 
@UserId int = null
           
AS
BEGIN
UPDATE [dbo].[tblCompanyUsers]
   SET          
      
       [IsUserDisabled] =  @IsUserDisabled  
      ,[ReasonSubject] =   @ReasonSubject   
      ,[ReasonDesciption]= @ReasonDesciption
      ,[UpdatedBy] = @UserId
      ,[UpdatedDateTime]= GETDATE()
 WHERE [CompanyId] =          @CompanyId
 
              

 
INSERT INTO  [dbo].[tblCompanyStatusLog]
           ([CompanyID]
           ,[CompanyStatus]
           ,[StatusReason]
           ,[MoreDetails]
           ,[CreatedDateTime]
           ,[CreatedBy]
            )
     VALUES
           ( @CompanyID 
           ,~@IsUserDisabled
           ,@ReasonSubject
           ,@ReasonDesciption
           ,getdate()
           ,@UserID
           )
END

GO
