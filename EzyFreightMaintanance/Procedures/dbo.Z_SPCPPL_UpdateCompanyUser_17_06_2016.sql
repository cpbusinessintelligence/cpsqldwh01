SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_UpdateCompanyUser_17_06_2016]
@UserID           int = null ,
 
@IsUserDisabled   bit = null ,
@ReasonSubject    nvarchar(max) = null ,
@ReasonDesciption  nvarchar(max) = null 

           
AS
BEGIN
UPDATE [dbo].[tblCompanyUsers]
   SET          
      
       [IsUserDisabled] =  @IsUserDisabled  
      ,[ReasonSubject] = case when  @ReasonSubject   IS null then [ReasonSubject] else @ReasonSubject end
      ,[ReasonDesciption]=case when  @ReasonDesciption IS null then [ReasonDesciption] else @ReasonDesciption end
      ,[UpdatedDateTime]=GETDATE()
 WHERE [UserID] =          @UserID 
 
-- declare @CompanyID int
--  select top 1 @CompanyID = CompanyID from [dbo].[tblCompanyUsers] where [UserID] = @UserID
 
--INSERT INTO  [dbo].[tblCompanyStatusLog]
--           ([CompanyID]
--           ,[CompanyStatus]
--           ,[StatusReason]
--           ,[MoreDetails]
--           ,[CreatedDateTime]
--           ,[CreatedBy]
--           )
--     VALUES
--           ( @CompanyID 
--           ,@IsUserDisabled
--           ,@ReasonSubject
--           ,@ReasonDesciption
--           ,getdate()
--           ,@UserID
--           )
            
END
GO
