SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_UpdateCompanyUser_BUP_23_10_2015]
@UserID           int = null ,
 
@IsUserDisabled   bit = null ,
@ReasonSubject    nvarchar(max) = null ,
@ReasonDesciption  nvarchar(max) = null 

           
AS
BEGIN
UPDATE [dbo].[tblCompanyUsers]
   SET          
      
       [IsUserDisabled] =  @IsUserDisabled  
      ,[ReasonSubject] =   @ReasonSubject   
      ,[ReasonDesciption]= @ReasonDesciption
      ,[UpdatedDateTime]=GETDATE()
 WHERE [UserID] =          @UserID 
 
            
END
GO
