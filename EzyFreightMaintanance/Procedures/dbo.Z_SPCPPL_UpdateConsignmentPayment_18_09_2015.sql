SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Z_SPCPPL_UpdateConsignmentPayment_18_09_2015]
      
  
           @ConsignmentId int=null,
           @UpdatedBy int=null,
           
           @PaymentRefNo nvarchar(100)=null,
           @merchantReferenceCode nvarchar(max)=null,
           @SubscriptionID nvarchar(max)=null,
           @AuthorizationCode nvarchar(100)=null
           
AS
BEGIN
begin tran

 

 

			UPDATE dbo.tblConsignment
   SET [IsProcessed] =1,
      [UpdatedDateTTime] = getDate(),
      [UpdatedBy] = @UpdatedBy
     
 WHERE ConsignmentID = @ConsignmentId
 
 	UPDATE dbo.[tblInvoice]
   SET [PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
      
 WHERE invoicenumber = (select top 1 InvoiceNo from dbo.tblSalesOrder where ReferenceNo=@ConsignmentId)

 
 	UPDATE dbo.tblConsignmentstaging
 	set 
[PaymentRefNo] = @PaymentRefNo
           ,[AuthorizationCode]=@AuthorizationCode
           ,[merchantReferenceCode]=@merchantReferenceCode
           ,[SubscriptionID]=@SubscriptionID
           ,[UpdatedDateTTime] =GETDATE()
      ,[UpdatedBy] = @UpdatedBy
      where 
      ConsignmentId = @ConsignmentId
      
      
declare @DestinationID int ,
@ContactID int ,
@PicupID int 
   
--SELECT     dbo.tblInvoice.*
--FROM         dbo.tblConsignment INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblConsignment.ConsignmentID = dbo.tblSalesOrder.ReferenceNo INNER JOIN
--                      dbo.tblInvoice ON dbo.tblSalesOrder.InvoiceNo = dbo.tblInvoice.InvoiceNumber
                      
--                       where dbo.tblConsignment.ConsignmentID = @ConsignmentID
           
           SELECT     dbo.tblConsignment.*, dbo.tblConsignment.[Country-ServiceArea-FacilityCode] as Country_ServiceArea_FacilityCode
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID


--SELECT     dbo.tblSalesOrderDetail.*, dbo.tblSalesOrder.*
--FROM         dbo.tblSalesOrderDetail INNER JOIN
--                      dbo.tblSalesOrder ON dbo.tblSalesOrderDetail.SalesOrderID = dbo.tblSalesOrder.SalesOrderID
--WHERE     (dbo.tblSalesOrder.ReferenceNo = @ConsignmentID)

				


			   SELECT   @PicupID = dbo.tblConsignment.PickupID,
			   @DestinationID=dbo.tblConsignment.DestinationID,
			   @ContactID=dbo.tblConsignment.ContactID
FROM         dbo.tblConsignment where dbo.tblConsignment.ConsignmentID = @ConsignmentID




            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe, case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@PicupID
            SELECT     dbo.tblAddress.AddressID, dbo.tblAddress.UserID, dbo.tblAddress.FirstName, dbo.tblAddress.LastName, dbo.tblAddress.CompanyName, dbo.tblAddress.Email, 
                      dbo.tblAddress.Address1, dbo.tblAddress.Address2, dbo.tblAddress.Suburb, dbo.tblAddress.PostCode, dbo.tblAddress.Phone, dbo.tblAddress.Mobile, 
                      dbo.tblAddress.CreatedDateTime, dbo.tblAddress.CreatedBy, dbo.tblAddress.UpdatedDateTime, dbo.tblAddress.UpdatedBy, dbo.tblAddress.IsRegisterAddress, 
                      dbo.tblAddress.IsDeleted, dbo.tblAddress.IsBusiness, dbo.tblAddress.IsSubscribe,case when dbo.tblState.StateCode IS null then dbo.tblAddress.StateName else dbo.tblState.StateCode end AS StateID, Country, CountryCode 
FROM         dbo.tblAddress left JOIN
                      dbo.tblState ON dbo.tblAddress.StateID = dbo.tblState.StateID where AddressID =@DestinationID
 
 



      



            
  if(@@ERROR<>0)
  begin
  rollback tran
  
  INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[FunctionInfo]
           ,[ClientId])
     VALUES
           (ERROR_LINE() +' : '+ERROR_MESSAGE()
           ,'SPCPPL_UpdateConsignmentPayment'
           , 2)
  --exec [SPCPPL_LogError]  ERROR_LINE() +' : '+ERROR_MESSAGE(),'SPCPPL_CreateConsignmentUsingBulk',2
  end
  else
  commit tran          
            
           

            
            
END

GO
