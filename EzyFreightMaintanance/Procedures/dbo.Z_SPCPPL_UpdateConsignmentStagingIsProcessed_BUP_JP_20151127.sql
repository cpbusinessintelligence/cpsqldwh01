SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[Z_SPCPPL_UpdateConsignmentStagingIsProcessed_BUP_JP_20151127]
           @ConsignmentStagingID int = null 
AS
BEGIN

UPDATE dbo.tblConsignmentStaging
   SET [IsProcessed] =1,
      [UpdatedDateTTime] = getDate(),
      [UpdatedBy] = CreatedBy
 WHERE ConsignmentStagingID = @ConsignmentStagingID
				
End
GO
