SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Z_SPCPPL_UpdateDeliveryChoices_23_06_2017]
@ID int,
@Category varchar(20)= null ,
@DeliveryChoiceID varchar(50)= null ,
@Sortingcode varchar(50)= null ,
@DeliveryChoiceName varchar(100)= null ,
@DeliveryChoiceDescription varchar(max)= null ,
@OperationHours nvarchar(max)= null ,
@Address1 varchar(max)= null ,
@Address2 varchar(max)= null ,
@Address3 varchar(max)= null ,
@Suburb varchar(50)= null ,
@PostCode varchar(50)= null ,
@State varchar(55)= null ,
@Country varchar(55)= null ,
@UpdatedBy int= null ,
@Latitude nvarchar(max)= null ,
@Longtitude nvarchar(max)= null ,
@IsRedeliveryAvailable bit= 0

As Begin 
Begin Try

update [dbo].[vw_DeliveryChoices] set 
		   [Category] = @Category
           ,[DeliveryChoiceID] = @DeliveryChoiceID
           ,[Sortingcode] = @Sortingcode
           ,[DeliveryChoiceName] = @DeliveryChoiceName
           ,[DeliveryChoiceDescription] = @DeliveryChoiceDescription
           ,[Operation Hours] = @OperationHours
           ,[Address1] = @Address1
           ,[Address2] = @Address2
           ,[Address3] = @Address3
           ,[Suburb] = @Suburb
           ,[PostCode] = @PostCode
           ,[State] = @State
           ,[Country] = @Country
           ,[ModifiedDateTime] = Getdate()
           ,[ModifiedBy] = @UpdatedBy
           ,[Latitude] = @Latitude
           ,[Longtitude] = @Longtitude
		   ,[IsRedeliveryAvailable] = @IsRedeliveryAvailable
     where 
           [ID] = @ID
			
select @ID as Id

END TRY
		BEGIN CATCH
		begin
			INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'UpdateDeliveryChoices', 2)
		end
		END CATCH

END 


GO
