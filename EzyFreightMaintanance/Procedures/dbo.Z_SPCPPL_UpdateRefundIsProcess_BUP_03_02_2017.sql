SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Z_SPCPPL_UpdateRefundIsProcess_BUP_03_02_2017]
           @ConsignmentID int= null,
           
           @IsProcess bit= null,
           @PaymentRefNo nvarchar(max)= null,
           @AuthorizationCode nvarchar(max)= null,
           @UpdatedBy int= null
AS
BEGIN
update [dbo].[tblRefund]
set 
IsProcess = @IsProcess,
UpdatedBy = @UpdatedBy,
PaymentRefNo = @PaymentRefNo,
AuthorizationCode = @AuthorizationCode,
UpdatedDateTTime=getDate()
where 

           [ConsignmentID]=@ConsignmentID
           
           
           
           
           update [dbo].tblConsignment
set 
ConsignmentStatus = 15,
UpdatedBy = @UpdatedBy,
UpdatedDateTTime=getDate()
where 

           [ConsignmentID]=@ConsignmentID
           
END

---[SPCPPL_GetAdminZoneWiseUsers] 41,1,10,'FirstName','ASC',0
--- select * from tblAddress
GO
