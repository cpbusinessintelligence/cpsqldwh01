SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Z_SPCPPL_updateConsignmentStagingPAymentPart_BUP_JP_20151127]
@ConsignmentStagingID int,
@PaymentRefNo nvarchar(max) = null,
@merchantReferenceCode nvarchar(max)= null,
@SubscriptionID nvarchar(max)= null,
@AuthorizationCode nvarchar(max)= null
AS
BEGIN

update tblConsignmentStaging set 
PaymentRefNo=@PaymentRefNo,
SubscriptionID=@SubscriptionID,
AuthorizationCode=@AuthorizationCode,
merchantReferenceCode = @merchantReferenceCode

 where 
ConsignmentStagingID = @ConsignmentStagingID

END
GO
