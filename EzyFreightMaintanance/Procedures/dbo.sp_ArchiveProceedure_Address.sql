SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_ArchiveProceedure_Address] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveTrackingEventDate = @ArchiveDate
	

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [tblBookingArchive] ON

	BEGIN TRANSACTION trnAddress
	
INSERT INTO [dbo].[tblAddress_Archive]
           ([AddressID]
           ,[UserID]
           ,[FirstName]
           ,[LastName]
           ,[CompanyName]
           ,[Email]
           ,[Address1]
           ,[Address2]
           ,[Suburb]
           ,[StateName]
           ,[StateID]
           ,[PostCode]
           ,[Phone]
           ,[Mobile]
           ,[Country]
           ,[CountryCode]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[UpdatedDateTime]
           ,[UpdatedBy]
           ,[IsRegisterAddress]
           ,[IsDeleted]
           ,[IsBusiness]
           ,[IsSubscribe]
           ,[IsEDIUser])
	SELECT [AddressID]
      ,[UserID]
      ,[FirstName]
      ,[LastName]
      ,[CompanyName]
      ,[Email]
      ,[Address1]
      ,[Address2]
      ,[Suburb]
      ,[StateName]
      ,[StateID]
      ,[PostCode]
      ,[Phone]
      ,[Mobile]
      ,[Country]
      ,[CountryCode]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[IsRegisterAddress]
      ,[IsDeleted]
      ,[IsBusiness]
      ,[IsSubscribe]
      ,[IsEDIUser]
  FROM [dbo].[tblAddress]
 
	           WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate and
			   IsRegisterAddress = 0 
		AND (UserID = -1 OR UserID IS NULL) 
		AND (IsEDIUser = 0 OR IsEDIUser IS NULL)

	
	DELETE FROM [tblAddress] WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate and IsRegisterAddress = 0 
		AND (UserID = -1 OR UserID IS NULL) 
		AND (IsEDIUser = 0 OR IsEDIUser IS NULL)

	COMMIT TRANSACTION trnAddress

	--SET IDENTITY_INSERT [tblBookingArchive] OFF

-----------------------------------------------------------------

GO
