SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ArchiveProceedure_Booking] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveTrackingEventDate = @ArchiveDate
	

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [tblBookingArchive] ON

	BEGIN TRANSACTION trnBooking
	

INSERT INTO [dbo].[tblBookingArchive]
           ([BookingID]
           ,[PhoneNumber]
           ,[ContactName]
           ,[ContactEmail]
           ,[PickupDate]
           ,[PickupTime]
           ,[OrderCoupons]
           ,[PickupFromCustomer]
           ,[PickupName]
           ,[PickupAddress1]
           ,[PickupAddress2]
           ,[PickupSuburb]
           ,[PickupPhoneNo]
           ,[ContactDetails]
           ,[PickupFrom]
           ,[IsProcessed]
           ,[BookingRefNo]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[UpdatedDateTime]
           ,[UpdatedBy]
           ,[Branch]
           ,[PickupEmail]
           ,[PickupPostCode]
           ,[Costom]
           ,[CostomId]
           ,[ConsignmentID])
	SELECT [BookingID]
      ,[PhoneNumber]
      ,[ContactName]
      ,[ContactEmail]
      ,[PickupDate]
      ,[PickupTime]
      ,[OrderCoupons]
      ,[PickupFromCustomer]
      ,[PickupName]
      ,[PickupAddress1]
      ,[PickupAddress2]
      ,[PickupSuburb]
      ,[PickupPhoneNo]
      ,[ContactDetails]
      ,[PickupFrom]
      ,[IsProcessed]
      ,[BookingRefNo]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[Branch]
      ,[PickupEmail]
      ,[PickupPostCode]
      ,[Costom]
      ,[CostomId]
      ,[ConsignmentID]
  FROM [dbo].[tblBooking]

 
	           WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate
	
	DELETE FROM tblBooking WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate

	COMMIT TRANSACTION trnBooking

	--SET IDENTITY_INSERT [tblBookingArchive] OFF

-----------------------------------------------------------------
GO
