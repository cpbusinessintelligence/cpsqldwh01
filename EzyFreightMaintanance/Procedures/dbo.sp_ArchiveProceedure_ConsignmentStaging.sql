SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ArchiveProceedure_ConsignmentStaging] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveTrackingEventDate = @ArchiveDate
	

-----------------------------------------------------------------
	SET IDENTITY_INSERT [tblConsignmentStaging_Archive] ON

	BEGIN TRANSACTION trnTrackingEvent_2014_FH
	

INSERT INTO [dbo].[tblConsignmentStaging_Archive]
           ([ConsignmentStaging_ArchiveID]
		    ,[UserID]
           ,[IsRegUserConsignment]
           ,[PickupID]
           ,[DestinationID]
           ,[ContactID]
           ,[TotalWeight]
           ,[TotalVolume]
           ,[NoOfItems]
           ,[SpecialInstruction]
           ,[CustomerRefNo]
           ,[PickupDate]
           ,[PreferPickupTime]
           ,[ClosingTime]
           ,[DangerousGoods]
           ,[Terms]
           ,[ServiceID]
           ,[RateCardID]
           ,[IsProcessed]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[UpdatedDateTTime]
           ,[UpdatedBy]
           ,[ConsignmentId]
           ,[IsDocument]
           ,[IsSignatureReq]
           ,[IfUndelivered]
           ,[ReasonForExport]
           ,[TypeOfExport]
           ,[Currency]
           ,[IsInsurance]
           ,[IsIdentity]
           ,[IdentityType]
           ,[IdentityNo]
           ,[XMLRequest]
           ,[XMLResponce]
           ,[IsATl]
           ,[IsReturnToSender]
           ,[HasReadInsuranceTc]
           ,[NatureOfGoods]
           ,[SortCode]
           ,[XMLRequestPU]
           ,[XMLResponcePU]
           ,[ETA]
           ,[PaymentRefNo]
           ,[XMLRequestPU_DHL]
           ,[XMLResponcePU_DHL]
           ,[XMLRequestPU_NZPost]
           ,[XMLResponcePU_NZPost]
           ,[MerchantReferenceCode]
           ,[SubscriptionID]
           ,[AuthorizationCode]
           ,[IsAccountCustomer]
           ,[InsuranceAmount]
           ,[CourierPickupDate]
           ,[CalculatedTotal]
           ,[CalculatedGST]
           ,[ClientCode]
           ,[USPSRefNo])
	SELECT [ConsignmentStagingID]
      ,[UserID]
      ,[IsRegUserConsignment]
      ,[PickupID]
      ,[DestinationID]
      ,[ContactID]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[CustomerRefNo]
      ,[PickupDate]
      ,[PreferPickupTime]
      ,[ClosingTime]
      ,[DangerousGoods]
      ,[Terms]
      ,[ServiceID]
      ,[RateCardID]
      ,[IsProcessed]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[ConsignmentId]
      ,[IsDocument]
      ,[IsSignatureReq]
      ,[IfUndelivered]
      ,[ReasonForExport]
      ,[TypeOfExport]
      ,[Currency]
      ,[IsInsurance]
      ,[IsIdentity]
      ,[IdentityType]
      ,[IdentityNo]
      ,[XMLRequest]
      ,[XMLResponce]
      ,[IsATl]
      ,[IsReturnToSender]
      ,[HasReadInsuranceTc]
      ,[NatureOfGoods]
      ,[SortCode]
      ,[XMLRequestPU]
      ,[XMLResponcePU]
      ,[ETA]
      ,[PaymentRefNo]
      ,[XMLRequestPU_DHL]
      ,[XMLResponcePU_DHL]
      ,[XMLRequestPU_NZPost]
      ,[XMLResponcePU_NZPost]
      ,[MerchantReferenceCode]
      ,[SubscriptionID]
      ,[AuthorizationCode]
      ,[IsAccountCustomer]
      ,[InsuranceAmount]
      ,[CourierPickupDate]
      ,[CalculatedTotal]
      ,[CalculatedGST]
      ,[ClientCode]
      ,[USPSRefNo]
  FROM [dbo].[tblConsignmentStaging]
 
	           WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate
	
	DELETE FROM [tblConsignmentStaging] WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate

	COMMIT TRANSACTION trnTrackingEvent_2014_FH

	SET IDENTITY_INSERT [tblConsignmentStaging_Archive] OFF

-----------------------------------------------------------------
GO
