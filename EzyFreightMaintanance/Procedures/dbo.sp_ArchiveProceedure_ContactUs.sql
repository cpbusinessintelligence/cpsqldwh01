SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_ArchiveProceedure_ContactUs] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveTrackingEventDate = @ArchiveDate
	

-----------------------------------------------------------------

	BEGIN TRANSACTION trnContactUs
	

INSERT INTO [dbo].[tblContactUs_Archive]
           ([Id]
           ,[FirstName]
           ,[LastName]
           ,[Email]
           ,[Phone]
           ,[Suburb]
           ,[PostCode]
           ,[StateID]
           ,[Subject]
           ,[HowCanIHelpYou]
           ,[IsSubscribe]
           ,[CreatedDate]
           ,[TrackingNo]
           ,[AddressLine1]
           ,[AddressLine2])
	SELECT [Id]
      ,[FirstName]
      ,[LastName]
      ,[Email]
      ,[Phone]
      ,[Suburb]
      ,[PostCode]
      ,[StateID]
      ,[Subject]
      ,[HowCanIHelpYou]
      ,[IsSubscribe]
      ,[CreatedDate]
      ,[TrackingNo]
      ,[AddressLine1]
      ,[AddressLine2]
  FROM [dbo].[tblContactUs]

	           WHERE [CreatedDate]<=@ArchiveTrackingEventDate
	
	DELETE FROM [tblContactUs] WHERE [CreatedDate]<=@ArchiveTrackingEventDate

	COMMIT TRANSACTION trnContactUS



-----------------------------------------------------------------

GO
