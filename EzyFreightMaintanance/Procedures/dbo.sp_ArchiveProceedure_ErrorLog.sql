SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_ArchiveProceedure_ErrorLog] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveTrackingEventDate = @ArchiveDate
	

-----------------------------------------------------------------

	BEGIN TRANSACTION trnErrorLog
	

INSERT INTO [dbo].[tblErrorLog_Archive]
           ([Id]
           ,[Error]
           ,[FunctionInfo]
           ,[ClientId]
           ,[CreateDate]
           ,[Request]
           ,[URL])
SELECT [Id]
      ,[Error]
      ,[FunctionInfo]
      ,[ClientId]
      ,[CreateDate]
      ,[Request]
      ,[URL]
  FROM [dbo].[tblErrorLog]

	           WHERE [CreateDate]<=@ArchiveTrackingEventDate
	
	DELETE FROM [tblErrorLog] WHERE [CreateDate]<=@ArchiveTrackingEventDate

	COMMIT TRANSACTION trnErrorLog



-----------------------------------------------------------------

GO
