SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_ArchiveProceedure_Redelivery] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveTrackingEventDate = @ArchiveDate
	

-----------------------------------------------------------------

	BEGIN TRANSACTION trnRedelivery
	

INSERT INTO [dbo].[tblRedelivery_Archive]
           ([RedeliveryID]
           ,[RedeliveryType]
           ,[CardReferenceNumber]
           ,[LableNumber]
           ,[ConsignmentCode]
           ,[Branch]
           ,[State]
           ,[AttemptedRedeliveryTime]
           ,[SenderName]
           ,[NumberOfItem]
           ,[DestinationName]
           ,[DestinationAddress]
           ,[DestinationSuburb]
           ,[DestinationPostCode]
           ,[SelectedETADate]
           ,[PreferDeliverTimeSlot]
           ,[PreferDeliverTime]
           ,[CreatedDateTime]
           ,[CreatedBy]
           ,[UpdatedDateTime]
           ,[UpdatedBy]
           ,[IsProcessed]
           ,[Firstname]
           ,[Lastname]
           ,[Email]
           ,[PhoneNumber]
           ,[SPInstruction])
	SELECT [RedeliveryID]
      ,[RedeliveryType]
      ,[CardReferenceNumber]
      ,[LableNumber]
      ,[ConsignmentCode]
      ,[Branch]
      ,[State]
      ,[AttemptedRedeliveryTime]
      ,[SenderName]
      ,[NumberOfItem]
      ,[DestinationName]
      ,[DestinationAddress]
      ,[DestinationSuburb]
      ,[DestinationPostCode]
      ,[SelectedETADate]
      ,[PreferDeliverTimeSlot]
      ,[PreferDeliverTime]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
      ,[IsProcessed]
      ,[Firstname]
      ,[Lastname]
      ,[Email]
      ,[PhoneNumber]
      ,[SPInstruction]
  FROM [dbo].[tblRedelivery]

	           WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate
	
	DELETE FROM [tblRedelivery] WHERE [CreatedDateTime]<=@ArchiveTrackingEventDate

	COMMIT TRANSACTION trnRedelivery



-----------------------------------------------------------------


GO
