SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create procedure sp_CreateEzy2ShipShipmentsthroughAPI as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_CreateEzy2ShipShipmentsthroughAPI]
    --' ---------------------------
    --' Purpose: sp_CreateEzy2ShipShipmentsthroughAPI-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 04 Nov 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 04/11/2016    AK      1.00    Created the procedure                            

    --'=====================================================================


Select d.SubTotal as DeclaredValue,
       [ItemDescription],
	   d.CountryofOrigin,
	   HSCode,
	   c.[TotalWeight],
	   c.NoOfItems,
	   l.[Length],
	   l.[Width] ,
	   l.[Height], 
	   [CubicWeight],
	   c.[ReasonForExport] as CatgeoryofShipment,
	   [RateCardID] as ServiceCode,

	   a.FirstName+' '+a.LastNAme as ContactContactNAme,
	   a.Phone as ContactPhone,
	   a.Email as ContactEmail,
	   a.countrycode as ContactCountrycode,
	   a.Address1 as ContactAddress1,
	   a.Address2 as ContactAddress2,
	   a.CompanyName as ContactCompanyName,
	   case when isnull(a.IsBusiness,0)=1 then 'True' else 'false' end as ContactIsBusiness,
	   a.Postcode as ContactPostcode,
	   a.StateName as ContactState,
	   a.Suburb as ContactSuburb,
	   a.FirstName as ContactFirstName,
	   a.LastName as ContactLastName,


	   a2.FirstName+' '+a2.LastNAme as DestinationContactNAme,
	   a2.Phone as DestinationPhone,
	   a2.Email as DestinationEmail,
	   a2.countrycode as DestinationCountrycode,
	   a2.Address1 as DestinationAddress1,
	   a2.Address2 as DestinationAddress2,
	   a2.CompanyName as DestinationCompanyName,
	   case when isnull(a2.IsBusiness,0)=1 then 'True' else 'false' end as DestinationIsBusiness,
	   a2.Postcode as DestinationPostcode,
	   a2.StateName as DestinationState,
	   a2.Suburb as DestinationSuburb,
	   a2.FirstName as DestinationFirstName,
	   a2.LastName as DestinationLastName,


	   a1.FirstName+' '+a1.LastNAme as PickupContactNAme,
	   a1.Phone as PickupPhone,
	   a1.Email as PickupEmail,
	   a1.countrycode as PickupCountrycode,
	   a1.Address1 as PickupAddress1,
	   a1.Address2 as PickupAddress2,
	   a1.CompanyName as PickupCompanyName,
	   case when isnull(a1.IsBusiness,0)=1 then 'True' else 'false' end as PickupIsBusiness,
	   a1.Postcode as PickupPostcode,
	   a1.StateName as PickupState,
	   a1.Suburb as PickupSuburb,
	   a1.FirstName as PickupFirstName,
	   a1.LastName as PickupLastName


from tblconsignment c join tblcustomdeclaration d on c.consignmentid=d.consignmentid
                      join tblitemlabel l on l.consignmentid=c.consignmentid
					  join tbladdress a on a.addressid=contactid
					  join tbladdress a1 on a1.addressid=pickupid
					  join tbladdress a2 on a2.addressid=destinationid
where c.ratecardid like '%SAV%' and a2.CountryCode<>'NZ' and convert(Date,c.createddatetime)=convert(date,getdate())

end
GO
