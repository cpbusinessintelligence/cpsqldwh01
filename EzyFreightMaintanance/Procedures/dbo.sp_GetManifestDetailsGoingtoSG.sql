SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetManifestDetailsGoingtoSG] as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_Getezy2shipManifestDetailsGoingtoSG
    --' ---------------------------
    --' Purpose: To get all consignment details for  consignments Going To SG-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

Declare @Temp table(consignmentid int,[Line #] int identity(1,1),[House Bill] varchar(35),Consignor varchar(300),[Consignee First Name] varchar(35),[Consignee Last Name] varchar(35),[Consignee Addr. 1] varchar(35),[Consignee Addr. 2] varchar(35),[Consignee City] varchar(35),[Consignee State] varchar(35),[Consignee Postcode] varchar(10),
                    [Consignee Country] varchar(35),[Goods Description] varchar(50),[Origin] varchar(10),[Dest] varchar(10),[Country Origin] varchar(10),value decimal(4,2),Currency varchar(20),Packs int,UnitPacksMeasure varchar(10),Weight decimal(4,2),Unitweightmeasure varchar(10),[ConsignmentNumber] varchar(35))


Insert into @Temp(consignmentid,
                  [House Bill],
				  Consignor,
				  [Consignee First Name],
				  [Consignee Last Name],
				  [Consignee Addr. 1],
				  [Consignee Addr. 2],
				  [Consignee City],
				  [Consignee State],
				  [Consignee Postcode],
                  [Consignee Country],
				  [Goods Description],
				  [Origin],
				  [Dest],
				  [Country Origin],
				  value,
				  Currency,
				  Packs,
				  UnitPacksMeasure,
				  Weight,
				  Unitweightmeasure,
				 consignmentNumber)
SELECT c.consignmentid
     ,[ConsignmentCode]
	 ,a.FirstName+' '+a.LastNAme+' '+a.CompanyName+' '+a.Address1+' '+a.Address2+' '+a.Suburb+' '+a.StateName+' '+a.CountryCode
	 ,a1.FirstName
	 ,a1.LastNAme
	 ,a1.Address1 
	 ,a1.Address2
	 ,a1.suburb
	 ,a1.StateName
	 ,a1.PostCode
	 ,a1.CountryCode
	 ,isnull(i.description,'') as GoodsDescription
	 ,a.Countrycode
	 ,a1.countrycode
	 ,isnull(i.CountryOfOrigin,'') as CountryofOrigin
	 ,isnull(i.UnitValue,0) as UnitValue
	 ,'AUD'
	 ,NoOfItems
	 ,'PCS'
	 ,isnull(i.physicalweight,0) as Physicalweight
	 ,'KG'
	 ,[ConsignmentCode]
  FROM [EzyFreight].[dbo].[tblConsignment] c left join [EzyFreight].[dbo].[tblAddress] a on  a.addressid=pickupid
                                             left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											 left join [EzyFreight].[dbo].[tblItemLabel] i on i.consignmentid=c.consignmentid
											 where isinternational=1 and  a1.countrycode='SG' 
											 and ratecardid='SAV'
											 --and convert(date,c.CreatedDateTime)=convert(date,dateadd("dd",-1,getdate()))

--select * from @temp
    select * from @temp
end
GO
