SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure sp_Getezy2shipManifestDetailsGoingToSingapore as
begin

  --'=====================================================================
    --' CP -Stored Procedure -sp_Getezy2shipManifestDetailsGoingToSingapore
    --' ---------------------------
    --' Purpose: To get all consignment details for  consignments Going To Singapore-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/07/2015    AB      1.00    Created the procedure                             --AB20150708

    --'=====================================================================

Declare @Temp table(consignmentid int,[Cost Centre Code] varchar(40),[Send from Business Name] varchar(35),[Send from Contact person] varchar(35),[Send from address line 1] varchar(35),[Send from address line 2] varchar(35),[Send from address line 3] varchar(35),
                    [Send from Town] varchar(35),[Send from country] varchar(35),[Send from Postcode] varchar(10),[Send to Business Name] varchar(35),[Send to Contact person] varchar(35),[Send to address line 1] varchar(35),[Send to address line 2] varchar(35),[Send to address line 3] varchar(35),
					[Send to Town] varchar(35),[Send to country] varchar(35),[Send to Postcode] varchar(10),[Send to Phone] varchar(20),[Send to Email Address] varchar(35),[Sender Reference] varchar(20),ItemQuantity int,[Item No. 1 Physical weight(kg)] decimal(4,2),[Item No. 1 Length (cm)] decimal(4,2),
					[Item No. 1 Width (cm)] decimal(4,2),[Item No. 1 Height (cm)] decimal(4,2),[Item No. 1 Item content] varchar(50),[Item No. 1 Declared Value (SGD)] decimal(4,2),[Item No. 2 Physical weight(kg)] decimal(4,2),[Item No. 2 Length (cm)] decimal(4,2),
					[Item No. 2 Width (cm)] decimal(4,2),[Item No. 2 Height (cm)] decimal(4,2),[Item No. 2 Item content] varchar(50),[Item No. 2 Declared Value (SGD)] decimal(4,2),[Item No. 3 Physical weight(kg)] decimal(4,2),[Item No. 3 Length (cm)] decimal(4,2),
					[Item No. 3 Width (cm)] decimal(4,2),[Item No. 3 Height (cm)] decimal(4,2),[Item No. 3 Item content] varchar(50),[Item No. 3 Declared Value (SGD)] decimal(4,2),[Service code] varchar(20),[Enhanced Liability Amount Item No. 1] decimal(4,2),[Enhanced Liability Amount Item No. 2] decimal(4,2)
	                ,[Enhanced Liability Amount Item No. 3] decimal(4,2))

Insert into @Temp
SELECT consignmentid
       ,convert(varchar(40),'') as [Cost Centre Code] 
      ,a.companyname as [Send from Business Name]
	  ,a.FirstName+' '+a.LastNAme as [Send from Contact person]
	  ,'21 North Perimeter Road' as [Send from address line 1]
	  ,'#03-01 Airmail Transit Centre' as [Send from address line 2]
	  ,convert(varchar(35),'') as [Send from address line 3]
	  ,'Singapore' as [Send from Town]
	  ,'SG' as [Send from country]
	  ,'918112' as [Send from Postcode]
	  ,a1.companyname as [Send to Business Name]
	  ,a1.FirstName+' '+a1.LastNAme as [Send to Contact person]
	  ,a1.Address1 as [Send to address line 1]
	  ,a1.Address2 as [Send to address line 2]
	  ,convert(varchar(35),'') as [Send to address line 3]
	  ,a1.suburb as [Send to Town]
	  ,a1.country as [Send to country]
	  ,a1.postcode as [Send to Postcode]
	  ,a1.phone as [Send to Phone]
	  ,a1.email as [Send to Email Address]
	  ,[ConsignmentCode] as [Sender Reference]
	  ,NoOfItems as ItemQuantity
	  ,convert(decimal(4,2),0) as [Item No. 1 Physical weight(kg)]
	  ,convert(decimal(4,2),0) as [Item No. 1 Length (cm)]
	  ,convert(decimal(4,2),0) as [Item No. 1 Width (cm)]
	  ,convert(decimal(4,2),0) as [Item No. 1 Height (cm)]
	  ,convert(varchar(50),'') as [Item No. 1 Item content]
	  ,convert(decimal(4,2),0) as [Item No. 1 Declared Value (SGD)]
	  ,convert(decimal(4,2),0) as [Item No. 2 Physical weight(kg)]
	  ,convert(decimal(4,2),0) as [Item No. 2 Length (cm)]
	  ,convert(decimal(4,2),0) as [Item No. 2 Width (cm)]
	  ,convert(decimal(4,2),0) as [Item No. 2 Height (cm)]
	  ,convert(varchar(50),'') as [Item No. 2 Item content]
	  ,convert(decimal(4,2),0) as [Item No. 2 Declared Value (SGD)]
	  ,convert(decimal(4,2),0) as [Item No. 3 Physical weight(kg)]
	  ,convert(decimal(4,2),0) as [Item No. 3 Length (cm)]
	  ,convert(decimal(4,2),0) as [Item No. 3 Width (cm)]
	  ,convert(decimal(4,2),0) as [Item No. 3 Height (cm)]
	  ,convert(varchar(50),'') as [Item No. 3 Item content]
	  ,convert(decimal(4,2),0) as [Item No. 3 Declared Value (SGD)]
	  ,'TRSTIC' as [Service code]
	  ,convert(decimal(4,2),0) as [Enhanced Liability Amount Item No. 1]
	  ,convert(decimal(4,2),0) as [Enhanced Liability Amount Item No. 2]
	  ,convert(decimal(4,2),0) as [Enhanced Liability Amount Item No. 3]
  FROM [EzyFreight].[dbo].[tblConsignment] c left join [EzyFreight].[dbo].[tblAddress] a on  a.addressid=pickupid
                                             left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											 where isinternational=1 and  a1.countrycode='SG' and ratecardid='SAV'
											 --and convert(date,c.CreatedDateTime)=convert(date,dateadd("dd",-1,getdate()))

--select * from @temp

 Update @temp set [Item No. 1 Physical weight(kg)]=(Select top 1 physicalweight from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID asc)
  Update @temp set [Item No. 1 Length (cm)]=(Select top 1 [length] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID asc)
  Update @temp set [Item No. 1 Width (cm)]=(Select top 1 [width] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID asc)
  Update @temp set [Item No. 1 Height (cm)]=(Select top 1 [height] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID asc)
  Update @temp set [Item No. 1 Item content]=isnull((Select top 1 [description] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID asc),'')
  Update @temp set [Item No. 1 Declared Value (SGD)]=isnull((Select top 1 [UnitValue] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID asc),0)
 
 
 Update @temp set [Item No. 2 Physical weight(kg)]=(Select top 1 physicalweight from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID desc)
  where Itemquantity=2

  Update @temp set [Item No. 2 Length (cm)]=(Select top 1 [length] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID desc)
   where Itemquantity=2

  Update @temp set [Item No. 2 Width (cm)]=(Select top 1 [width] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID desc)
   where Itemquantity=2

  Update @temp set [Item No. 2 Height (cm)]=(Select top 1 [height] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID desc)
   where Itemquantity=2

  Update @temp set [Item No. 1 Item content]=isnull((Select top 1 [description] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID desc),'')
  where Itemquantity=2

  Update @temp set [Item No. 1 Declared Value (SGD)]=isnull((Select top 1 [UnitValue] from @temp t join [EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=t.consignmentid where t.consignmentid=l.consignmentid order by ItemLabelID desc),0)
  where Itemquantity=2

    select * from @temp
end

GO
