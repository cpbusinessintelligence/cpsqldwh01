SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventLog] (
		[LogGUID]                    [varchar](36) COLLATE Latin1_General_CI_AS NOT NULL,
		[LogTypeKey]                 [nvarchar](35) COLLATE Latin1_General_CI_AS NOT NULL,
		[LogConfigID]                [int] NULL,
		[LogUserID]                  [int] NULL,
		[LogUserName]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LogPortalID]                [int] NULL,
		[LogPortalName]              [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LogCreateDate]              [datetime] NOT NULL,
		[LogServerName]              [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LogProperties]              [ntext] COLLATE Latin1_General_CI_AS NOT NULL,
		[LogNotificationPending]     [bit] NULL,
		[LogEventID]                 [bigint] IDENTITY(1, 1) NOT NULL,
		CONSTRAINT [PK_EventLogMaster]
		PRIMARY KEY
		CLUSTERED
		([LogEventID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[EventLog]
	WITH CHECK
	ADD CONSTRAINT [FK_EventLog_EventLogConfig]
	FOREIGN KEY ([LogConfigID]) REFERENCES [dbo].[EventLogConfig] ([ID])
ALTER TABLE [dbo].[EventLog]
	CHECK CONSTRAINT [FK_EventLog_EventLogConfig]

GO
ALTER TABLE [dbo].[EventLog]
	WITH CHECK
	ADD CONSTRAINT [FK_EventLog_EventLogTypes]
	FOREIGN KEY ([LogTypeKey]) REFERENCES [dbo].[EventLogTypes] ([LogTypeKey])
ALTER TABLE [dbo].[EventLog]
	CHECK CONSTRAINT [FK_EventLog_EventLogTypes]

GO
CREATE NONCLUSTERED INDEX [IX_EventLog_LogConfigID]
	ON [dbo].[EventLog] ([LogConfigID], [LogNotificationPending], [LogCreateDate])
	INCLUDE ([LogEventID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_EventLog_LogCreateDate]
	ON [dbo].[EventLog] ([LogCreateDate])
	INCLUDE ([LogConfigID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_EventLog_LogGUID]
	ON [dbo].[EventLog] ([LogGUID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_EventLog_LogType]
	ON [dbo].[EventLog] ([LogTypeKey], [LogPortalID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventLog] SET (LOCK_ESCALATION = TABLE)
GO
