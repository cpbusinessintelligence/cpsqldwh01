SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LocationMaster] (
		[PostCodeId]     [int] NOT NULL,
		[PostCode]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Suburb]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SortCode]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[LocationMaster] SET (LOCK_ESCALATION = TABLE)
GO
