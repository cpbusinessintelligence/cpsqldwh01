SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parameters] (
		[ParametersID]     [int] IDENTITY(1, 1) NOT NULL,
		[Code]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Description]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Value]            [varchar](800) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddWho]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]      [datetime] NOT NULL,
		[EditWho]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[EditDateTime]     [datetime] NOT NULL,
		CONSTRAINT [PK_Parameters]
		PRIMARY KEY
		CLUSTERED
		([ParametersID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Parameters] SET (LOCK_ESCALATION = TABLE)
GO
