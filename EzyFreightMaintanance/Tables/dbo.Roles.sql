SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles] (
		[RoleID]                   [int] IDENTITY(0, 1) NOT NULL,
		[RoleName]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]              [nvarchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[ServiceFee]               [money] NULL,
		[BillingFrequency]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[TrialPeriod]              [int] NULL,
		[TrialFrequency]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[BillingPeriod]            [int] NULL,
		[TrialFee]                 [money] NULL,
		[IsPublic]                 [bit] NOT NULL,
		[AutoAssignment]           [bit] NOT NULL,
		[RoleGroupID]              [int] NULL,
		[RSVPCode]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IconFile]                 [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedByUserID]          [int] NULL,
		[CreatedOnDate]            [datetime] NULL,
		[LastModifiedByUserID]     [int] NULL,
		[LastModifiedOnDate]       [datetime] NULL,
		[Status]                   [int] NOT NULL,
		[SecurityMode]             [int] NOT NULL,
		[IsSystemRole]             [bit] NOT NULL,
		CONSTRAINT [IX_RoleName]
		UNIQUE
		NONCLUSTERED
		([RoleName])
		ON [PRIMARY],
		CONSTRAINT [PK_Roles]
		PRIMARY KEY
		CLUSTERED
		([RoleID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Roles]
	ADD
	CONSTRAINT [DF_Roles_ServiceFee]
	DEFAULT ((0)) FOR [ServiceFee]
GO
ALTER TABLE [dbo].[Roles]
	ADD
	CONSTRAINT [DF_Roles_IsPublic]
	DEFAULT ((0)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[Roles]
	ADD
	CONSTRAINT [DF_Roles_AutoAssignment]
	DEFAULT ((0)) FOR [AutoAssignment]
GO
ALTER TABLE [dbo].[Roles]
	ADD
	CONSTRAINT [DF_Roles_Status]
	DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Roles]
	ADD
	CONSTRAINT [DF_Roles_SecurityMode]
	DEFAULT ((0)) FOR [SecurityMode]
GO
ALTER TABLE [dbo].[Roles]
	ADD
	CONSTRAINT [DF_Roles_IsSystemRole]
	DEFAULT ((0)) FOR [IsSystemRole]
GO
ALTER TABLE [dbo].[Roles]
	WITH CHECK
	ADD CONSTRAINT [FK_Roles_RoleGroups]
	FOREIGN KEY ([RoleGroupID]) REFERENCES [dbo].[RoleGroups] ([RoleGroupID])
ALTER TABLE [dbo].[Roles]
	CHECK CONSTRAINT [FK_Roles_RoleGroups]

GO
CREATE NONCLUSTERED INDEX [IX_Roles]
	ON [dbo].[Roles] ([BillingFrequency])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Roles_RoleGroup]
	ON [dbo].[Roles] ([RoleGroupID], [RoleName])
	INCLUDE ([RoleID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Roles] SET (LOCK_ESCALATION = TABLE)
GO
