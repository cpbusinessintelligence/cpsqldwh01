SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles_Test] (
		[RoleID]                   [int] IDENTITY(0, 1) NOT NULL,
		[RoleName]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Description]              [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceFee]               [money] NULL,
		[BillingFrequency]         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TrialPeriod]              [int] NULL,
		[TrialFrequency]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BillingPeriod]            [int] NULL,
		[TrialFee]                 [money] NULL,
		[IsPublic]                 [bit] NOT NULL,
		[AutoAssignment]           [bit] NOT NULL,
		[RoleGroupID]              [int] NULL,
		[RSVPCode]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IconFile]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedByUserID]          [int] NULL,
		[CreatedOnDate]            [datetime] NULL,
		[LastModifiedByUserID]     [int] NULL,
		[LastModifiedOnDate]       [datetime] NULL,
		[Status]                   [int] NOT NULL,
		[SecurityMode]             [int] NOT NULL,
		[IsSystemRole]             [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[Roles_Test] SET (LOCK_ESCALATION = TABLE)
GO
