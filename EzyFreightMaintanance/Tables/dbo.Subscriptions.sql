SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscriptions] (
		[Category]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Reportname]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[To]                   [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CC]                   [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BCC]                  [varchar](800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Parameter1]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Parameter2]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Parameter3]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Parameter4]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Parameter5]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IncludeReport]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IncludeLink]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RenderFormat]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[shouldsendreport]     [int] NULL,
		[isactive]             [int] NULL,
		[sendorder]            [int] NULL,
		[serialnumber]         [int] NOT NULL,
		[Subject]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isconsolidate]        [bit] NULL
)
GO
ALTER TABLE [dbo].[Subscriptions] SET (LOCK_ESCALATION = TABLE)
GO
