SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_Hubacccustomers] (
		[Email]                      [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[First Name]                 [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Last Name]                  [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]                      [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ABN]                        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Account Name]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Building]                   [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Storey]                     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Unit No]                    [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Billing Address Line 1]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Billing Address Line 2]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Billing Suburb]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Billing Postal Code]        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Billing State]              [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Temp_Hubacccustomers] SET (LOCK_ESCALATION = TABLE)
GO
