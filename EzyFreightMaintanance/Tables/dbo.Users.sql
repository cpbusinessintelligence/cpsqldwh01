SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users] (
		[UserID]                      [int] NOT NULL,
		[Username]                    [nvarchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[FirstName]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LastName]                    [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsSuperUser]                 [bit] NOT NULL,
		[AffiliateId]                 [int] NULL,
		[Email]                       [nvarchar](256) COLLATE Latin1_General_CI_AS NULL,
		[DisplayName]                 [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[UpdatePassword]              [bit] NOT NULL,
		[LastIPAddress]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]                   [bit] NOT NULL,
		[CreatedByUserID]             [int] NULL,
		[CreatedOnDate]               [datetime] NULL,
		[LastModifiedByUserID]        [int] NULL,
		[LastModifiedOnDate]          [datetime] NULL,
		[PasswordResetToken]          [uniqueidentifier] NULL,
		[PasswordResetExpiration]     [datetime] NULL,
		[LowerEmail]                  AS (lower([Email]) collate Latin1_General_CI_AS),
		CONSTRAINT [IX_Users]
		UNIQUE
		NONCLUSTERED
		([Username])
		ON [PRIMARY],
		CONSTRAINT [PK_Users]
		PRIMARY KEY
		CLUSTERED
		([UserID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_IsSuperUser]
	DEFAULT ((0)) FOR [IsSuperUser]
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_UpdatePassword]
	DEFAULT ((0)) FOR [UpdatePassword]
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
CREATE NONCLUSTERED INDEX [IX_Users_Email]
	ON [dbo].[Users] ([Email])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Users_IsDeleted_DisplayName]
	ON [dbo].[Users] ([IsDeleted], [DisplayName])
	INCLUDE ([UserID], [IsSuperUser], [Email])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Users_LastModifiedOnDate]
	ON [dbo].[Users] ([LastModifiedOnDate] DESC)
	INCLUDE ([UserID], [IsSuperUser])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Users_LowerEmail]
	ON [dbo].[Users] ([Email])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Users] SET (LOCK_ESCALATION = TABLE)
GO
