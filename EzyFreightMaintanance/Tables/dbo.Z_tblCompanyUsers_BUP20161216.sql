SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblCompanyUsers_BUP20161216] (
		[CompanyUsersID]       [int] IDENTITY(1, 1) NOT NULL,
		[UserID]               [int] NOT NULL,
		[CompanyID]            [int] NOT NULL,
		[IsUserDisabled]       [bit] NULL,
		[ReasonSubject]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReasonDesciption]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		[PromotionalCode]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_tblCompanyUsers_BUP20161216] SET (LOCK_ESCALATION = TABLE)
GO
