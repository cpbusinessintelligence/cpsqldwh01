SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblCompany_BUP20151224] (
		[CompanyID]                 [int] IDENTITY(1, 1) NOT NULL,
		[AccountNumber]             [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyName]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ParentAccountNo]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ABN]                       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AddressID1]                [int] NULL,
		[PostalAddressID]           [int] NULL,
		[ParentCompanyName]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BilingAccountNo]           [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProntoExtracted]         [bit] NULL,
		[RateCardCategory]          [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IntRateCardCategory]       [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SameBillingAddress]        [bit] NULL,
		[BillingFname]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BillingLname]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BllingEmail]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BillingPhone]              [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BillingAddress]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreditLimit]               [decimal](15, 2) NULL,
		[CreatedDateTime]           [datetime] NULL,
		[CreatedBy]                 [int] NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [int] NULL,
		[IsExistingCustomer]        [bit] NULL,
		[Branch]                    [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExistingAccountNumber]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CouponNo]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_tblCompany_BUP20151224] SET (LOCK_ESCALATION = TABLE)
GO
