SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblConsignmentStaging_BUP] (
		[ConsignmentStagingID]     [int] IDENTITY(1, 1) NOT NULL,
		[UserID]                   [int] NULL,
		[IsRegUserConsignment]     [bit] NOT NULL,
		[PickupID]                 [int] NULL,
		[DestinationID]            [int] NULL,
		[ContactID]                [int] NULL,
		[TotalWeight]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalVolume]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NoOfItems]                [int] NOT NULL,
		[SpecialInstruction]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerRefNo]            [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupDate]               [date] NOT NULL,
		[PreferPickupTime]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ClosingTime]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DangerousGoods]           [bit] NOT NULL,
		[Terms]                    [bit] NOT NULL,
		[ServiceID]                [int] NOT NULL,
		[RateCardID]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsProcessed]              [bit] NULL,
		[CreatedDateTime]          [datetime] NOT NULL,
		[CreatedBy]                [int] NULL,
		[UpdatedDateTTime]         [datetime] NULL,
		[UpdatedBy]                [int] NULL,
		[ConsignmentId]            [int] NULL,
		[IsDocument]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSignatureReq]           [bit] NULL,
		[IfUndelivered]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReasonForExport]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TypeOfExport]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsInsurance]              [bit] NULL,
		[IsIdentity]               [bit] NULL,
		[IdentityType]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IdentityNo]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequest]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponce]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsATl]                    [bit] NULL,
		[IsReturnToSender]         [bit] NULL,
		[HasReadInsuranceTc]       [bit] NULL,
		[NatureOfGoods]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequestPU]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponcePU]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETA]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PaymentRefNo]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequestPU_DHL]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponcePU_DHL]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequestPU_NZPost]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponcePU_NZPost]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblConsignmentStaging_BUP]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentStagingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Z_tblConsignmentStaging_BUP]
	ADD
	CONSTRAINT [DF_tblConsignmentStaging_BUP_IsProcessed_0012]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[Z_tblConsignmentStaging_BUP] SET (LOCK_ESCALATION = TABLE)
GO
