SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblConsignment_BUP] (
		[ConsignmentID]                        [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentCode]                      [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UserID]                               [int] NULL,
		[IsRegUserConsignment]                 [bit] NOT NULL,
		[PickupID]                             [int] NULL,
		[DestinationID]                        [int] NULL,
		[ContactID]                            [int] NULL,
		[TotalWeight]                          [decimal](10, 2) NULL,
		[TotalMeasureWeight]                   [decimal](10, 2) NULL,
		[TotalVolume]                          [decimal](10, 2) NULL,
		[TotalMeasureVolume]                   [decimal](10, 2) NULL,
		[NoOfItems]                            [int] NOT NULL,
		[SpecialInstruction]                   [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerRefNo]                        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentPreferPickupDate]          [date] NOT NULL,
		[ConsignmentPreferPickupTime]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ClosingTime]                          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DangerousGoods]                       [bit] NOT NULL,
		[Terms]                                [bit] NOT NULL,
		[RateCardID]                           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LastActivity]                         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActiivityDateTime]                [datetime] NULL,
		[ConsignmentStatus]                    [int] NULL,
		[EDIDataProcessed]                     [bit] NULL,
		[ProntoDataExtracted]                  [bit] NULL,
		[IsBilling]                            [bit] NULL,
		[IsManifested]                         [bit] NULL,
		[CreatedDateTime]                      [datetime] NOT NULL,
		[CreatedBy]                            [int] NULL,
		[UpdatedDateTTime]                     [datetime] NULL,
		[UpdatedBy]                            [int] NULL,
		[IsInternational]                      [bit] NULL,
		[IsDocument]                           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSignatureReq]                       [bit] NULL,
		[IfUndelivered]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReasonForExport]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TypeOfExport]                         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]                             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsInsurance]                          [bit] NULL,
		[IsIdentity]                           [bit] NULL,
		[IdentityType]                         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IdentityNo]                           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country-ServiceArea-FacilityCode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InternalServiceCode]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NetSubTotal]                          [decimal](10, 2) NULL,
		[IsATl]                                [bit] NULL,
		[IsReturnToSender]                     [bit] NULL,
		[HasReadInsuranceTc]                   [bit] NULL,
		[NatureOfGoods]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginServiceAreaCode]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProductShortName]                     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]                             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETA]                                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CTIManifestExtracted]                 [bit] NULL,
		CONSTRAINT [TUC_tblConsignment_BUP_1]
		UNIQUE
		NONCLUSTERED
		([ConsignmentCode])
		ON [PRIMARY],
		CONSTRAINT [PK_tblConsignment_BUP]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	ADD
	CONSTRAINT [DF_tblConsignment_BUP_IsBilling]
	DEFAULT ((0)) FOR [IsBilling]
GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	ADD
	CONSTRAINT [DF_tblConsignment_BUP_IsManifested]
	DEFAULT ((0)) FOR [IsManifested]
GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	ADD
	CONSTRAINT [DF_tblConsignment_BUP_IsInternational]
	DEFAULT ((0)) FOR [IsInternational]
GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	ADD
	CONSTRAINT [DF__tblConsig__CTIMa__4C8B54C9]
	DEFAULT ((0)) FOR [CTIManifestExtracted]
GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment_BUP]
	FOREIGN KEY ([PickupID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	CHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment_BUP]

GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment_BUP_1]
	FOREIGN KEY ([DestinationID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	CHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment_BUP_1]

GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment_BUP_2]
	FOREIGN KEY ([ContactID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	CHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment_BUP_2]

GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblStatus_tblConsignment_BUP]
	FOREIGN KEY ([ConsignmentStatus]) REFERENCES [dbo].[tblStatus] ([StatusID])
ALTER TABLE [dbo].[Z_tblConsignment_BUP]
	CHECK CONSTRAINT [FK_FK_tblStatus_tblConsignment_BUP]

GO
ALTER TABLE [dbo].[Z_tblConsignment_BUP] SET (LOCK_ESCALATION = TABLE)
GO
