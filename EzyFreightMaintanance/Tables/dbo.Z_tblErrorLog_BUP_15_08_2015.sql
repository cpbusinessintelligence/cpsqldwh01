SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblErrorLog_BUP_15_08_2015] (
		[Id]               [int] IDENTITY(1, 1) NOT NULL,
		[Error]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FunctionInfo]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientId]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDate]       [datetime] NULL,
		CONSTRAINT [PK_tblErrorLog_15_08_2015]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Z_tblErrorLog_BUP_15_08_2015]
	ADD
	CONSTRAINT [DF_tblErrorLog_15_08_2015_CreateDate]
	DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[Z_tblErrorLog_BUP_15_08_2015] SET (LOCK_ESCALATION = TABLE)
GO
