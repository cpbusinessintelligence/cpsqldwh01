SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblRedirectedConsignment_BUP20161124] (
		[ReConsignmentID]              [int] IDENTITY(1, 1) NOT NULL,
		[UniqueID]                     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentCode]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SelectedDeliveryOption]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddressID]              [int] NULL,
		[CurrentDeliveryAddressID]     [int] NOT NULL,
		[NewDeliveryAddressID]         [int] NULL,
		[TotalWeight]                  [decimal](10, 2) NULL,
		[TotalVolume]                  [decimal](10, 2) NULL,
		[ServiceType]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RateCardID]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CurrentETA]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewETA]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NoOfItems]                    [int] NULL,
		[ConsignmentStatus]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SpecialInstruction]           [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Terms]                        [bit] NULL,
		[ATL]                          [bit] NULL,
		[ConfirmATLInsuranceVoid]      [bit] NULL,
		[ConfirmDeliveryAddress]       [bit] NULL,
		[IsProcessed]                  [bit] NOT NULL,
		[SortCode]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PaymentRefNo]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MerchantReferenceCode]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubscriptionID]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CalculatedTotal]              [decimal](18, 2) NULL,
		[CreditCardSurcharge]          [decimal](18, 2) NULL,
		[NetTotal]                     [decimal](18, 2) NULL,
		[CreatedDateTime]              [datetime] NOT NULL,
		[CreatedBy]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTTime]             [datetime] NULL,
		[UpdatedBy]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_tblRedirectedConsignment_BUP20161124] SET (LOCK_ESCALATION = TABLE)
GO
