SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblRedirectedItemLabel_BUP20161124] (
		[ReItemLabelID]            [int] IDENTITY(1, 1) NOT NULL,
		[ReConsignmentID]          [int] NOT NULL,
		[LabelNumber]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Length]                   [decimal](10, 2) NULL,
		[Width]                    [decimal](10, 2) NULL,
		[Height]                   [decimal](10, 2) NULL,
		[CubicWeight]              [decimal](10, 2) NULL,
		[PhysicalWeight]           [decimal](10, 2) NULL,
		[LastActivity]             [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActivityDateTime]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]          [datetime] NOT NULL,
		[CreatedBy]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]          [datetime] NULL,
		[UpdatedBy]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_tblRedirectedItemLabel_BUP20161124] SET (LOCK_ESCALATION = TABLE)
GO
