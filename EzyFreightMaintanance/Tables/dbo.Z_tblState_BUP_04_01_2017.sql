SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tblState_BUP_04_01_2017] (
		[StateID]             [int] IDENTITY(1, 1) NOT NULL,
		[StateCode]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[StateName]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [Z_tblState_BUP_01_01_2017]
		PRIMARY KEY
		CLUSTERED
		([StateID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Z_tblState_BUP_04_01_2017]
	ADD
	CONSTRAINT [DF__Z_tblStat__Creat__6304A5CD]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[Z_tblState_BUP_04_01_2017] SET (LOCK_ESCALATION = TABLE)
GO
