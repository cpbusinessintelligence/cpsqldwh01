SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fr_20150521319] (
		[id]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[city]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[location]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[income]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[price]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[inc_van]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[van_desc]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[run_desc]     [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[modified]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[fr_20150521319] SET (LOCK_ESCALATION = TABLE)
GO
