SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAPIEnquiry] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[CompanyName]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StateID]               [int] NULL,
		[PostCode]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Website]               [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountNumber]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RequesterFname]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RequesterLname]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TechContactName]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TechContactEmail]      [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TechContactNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ShoppingCartUsed]      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Comments]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[RequesterEmail]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ERPInUse]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblAPIEnquiry]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblAPIEnquiry]
	ADD
	CONSTRAINT [DF_tblAPIEnquiry_Country]
	DEFAULT ('Australia') FOR [Country]
GO
ALTER TABLE [dbo].[tblAPIEnquiry]
	ADD
	CONSTRAINT [DF__tblAPIEnq__Creat__1F4E99FE]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblAPIEnquiry] SET (LOCK_ESCALATION = TABLE)
GO
