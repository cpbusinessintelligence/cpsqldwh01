SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblActivityLogging_Archive] (
		[Id]                [int] NOT NULL,
		[CPPLServiceID]     [int] NOT NULL,
		[ClientID]          [int] NOT NULL,
		[CreateDate]        [datetime] NOT NULL,
		[Comment]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblActivityLogging_Archive] SET (LOCK_ESCALATION = TABLE)
GO
