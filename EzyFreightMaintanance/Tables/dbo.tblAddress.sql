SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddress] (
		[AddressID]             [int] IDENTITY(1, 1) NOT NULL,
		[UserID]                [int] NULL,
		[FirstName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastName]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyName]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                 [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address1]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address2]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StateName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StateID]               [int] NULL,
		[PostCode]              [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]                 [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Mobile]                [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryCode]           [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		[IsRegisterAddress]     [bit] NULL,
		[IsDeleted]             [bit] NULL,
		[IsBusiness]            [bit] NULL,
		[IsSubscribe]           [bit] NULL,
		[IsEDIUser]             [bit] NULL,
		CONSTRAINT [PK_tblAddress]
		PRIMARY KEY
		CLUSTERED
		([AddressID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblAddress]
	ADD
	CONSTRAINT [DF__tblAddres__Creat__49C3F6B7]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblAddress]
	ADD
	CONSTRAINT [DF_tblAddress_IsRegisterAddress]
	DEFAULT ((0)) FOR [IsRegisterAddress]
GO
ALTER TABLE [dbo].[tblAddress]
	ADD
	CONSTRAINT [DF_tblAddress_IdDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tblAddress]
	ADD
	CONSTRAINT [DF__tblAddres__IsEDI__69279377]
	DEFAULT ((0)) FOR [IsEDIUser]
GO
CREATE NONCLUSTERED INDEX [IX_tblAddress_Suburb]
	ON [dbo].[tblAddress] ([Suburb])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAddress_Name]
	ON [dbo].[tblAddress] ([FirstName], [LastName])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAddress_UserID]
	ON [dbo].[tblAddress] ([UserID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblAddress_6_1243151474__K2_K21_K22_K1_K11_K17_3_4_5_6_7_8_9_10_12_13_14_15_16_18_19_20_23_24]
	ON [dbo].[tblAddress] ([UserID], [IsRegisterAddress], [IsDeleted], [AddressID], [StateID], [CreatedDateTime])
	INCLUDE ([FirstName], [LastName], [CompanyName], [Email], [Address1], [Address2], [Suburb], [StateName], [PostCode], [Phone], [Mobile], [Country], [CountryCode], [CreatedBy], [UpdatedDateTime], [UpdatedBy], [IsBusiness], [IsSubscribe])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblAddress_6_1243151474__K16_K1_3_4_7_8_9_10_12]
	ON [dbo].[tblAddress] ([CountryCode], [AddressID])
	INCLUDE ([FirstName], [LastName], [Address1], [Address2], [Suburb], [StateName], [PostCode])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblAddress_6_1243151474__K17_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_18_19_20_21_22_23_24_25]
	ON [dbo].[tblAddress] ([CreatedDateTime])
	INCLUDE ([AddressID], [UserID], [FirstName], [LastName], [CompanyName], [Email], [Address1], [Address2], [Suburb], [StateName], [StateID], [PostCode], [Phone], [Mobile], [Country], [CountryCode], [CreatedBy], [UpdatedDateTime], [UpdatedBy], [IsRegisterAddress], [IsDeleted], [IsBusiness], [IsSubscribe], [IsEDIUser])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblAddress_6_1243151474__K1_K11_K15_3_4_7_8_9_10_12_13]
	ON [dbo].[tblAddress] ([AddressID], [StateID], [Country])
	INCLUDE ([FirstName], [LastName], [Address1], [Address2], [Suburb], [StateName], [PostCode], [Phone])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblAddress_6_1243151474__K1_3_4_7_8_9_10_12_13_15]
	ON [dbo].[tblAddress] ([AddressID])
	INCLUDE ([FirstName], [LastName], [Address1], [Address2], [Suburb], [StateName], [PostCode], [Phone], [Country])
	ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1243151474_22_1]
	ON [dbo].[tblAddress] ([IsDeleted], [AddressID])
GO
CREATE STATISTICS [_dta_stat_1243151474_2_22]
	ON [dbo].[tblAddress] ([UserID], [IsDeleted])
GO
CREATE STATISTICS [_dta_stat_1243151474_11_22_21]
	ON [dbo].[tblAddress] ([StateID], [IsDeleted], [IsRegisterAddress])
GO
CREATE STATISTICS [_dta_stat_1243151474_2_1_22]
	ON [dbo].[tblAddress] ([UserID], [AddressID], [IsDeleted])
GO
CREATE STATISTICS [_dta_stat_1243151474_17_22_21]
	ON [dbo].[tblAddress] ([CreatedDateTime], [IsDeleted], [IsRegisterAddress])
GO
CREATE STATISTICS [_dta_stat_1243151474_1_11_21]
	ON [dbo].[tblAddress] ([AddressID], [StateID], [IsRegisterAddress])
GO
CREATE STATISTICS [_dta_stat_1243151474_1_11_2_21]
	ON [dbo].[tblAddress] ([AddressID], [StateID], [UserID], [IsRegisterAddress])
GO
CREATE STATISTICS [_dta_stat_1243151474_22_21_1_11]
	ON [dbo].[tblAddress] ([IsDeleted], [IsRegisterAddress], [AddressID], [StateID])
GO
CREATE STATISTICS [_dta_stat_1243151474_17_2_22_21]
	ON [dbo].[tblAddress] ([CreatedDateTime], [UserID], [IsDeleted], [IsRegisterAddress])
GO
CREATE STATISTICS [_dta_stat_1243151474_21_22_1_11_17]
	ON [dbo].[tblAddress] ([IsRegisterAddress], [IsDeleted], [AddressID], [StateID], [CreatedDateTime])
GO
CREATE STATISTICS [_dta_stat_1243151474_21_2_1_22_11_17]
	ON [dbo].[tblAddress] ([IsRegisterAddress], [UserID], [AddressID], [IsDeleted], [StateID], [CreatedDateTime])
GO
CREATE STATISTICS [_dta_stat_1243151474_15_1]
	ON [dbo].[tblAddress] ([Country], [AddressID])
GO
CREATE STATISTICS [_dta_stat_1243151474_11_1_15]
	ON [dbo].[tblAddress] ([StateID], [AddressID], [Country])
GO
ALTER TABLE [dbo].[tblAddress] SET (LOCK_ESCALATION = TABLE)
GO
