SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddress_Archive] (
		[AddressID]             [int] NULL,
		[UserID]                [int] NULL,
		[FirstName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastName]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyName]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                 [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address1]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address2]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StateName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StateID]               [int] NULL,
		[PostCode]              [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]                 [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Mobile]                [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryCode]           [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [int] NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		[IsRegisterAddress]     [bit] NULL,
		[IsDeleted]             [bit] NULL,
		[IsBusiness]            [bit] NULL,
		[IsSubscribe]           [bit] NULL,
		[IsEDIUser]             [bit] NULL
)
GO
ALTER TABLE [dbo].[tblAddress_Archive] SET (LOCK_ESCALATION = TABLE)
GO
