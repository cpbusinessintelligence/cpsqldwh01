SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBooking] (
		[BookingID]              [int] IDENTITY(1, 1) NOT NULL,
		[PhoneNumber]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ContactName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactEmail]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupDate]             [date] NOT NULL,
		[PickupTime]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OrderCoupons]           [bit] NULL,
		[PickupFromCustomer]     [bit] NULL,
		[PickupName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress1]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress2]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupSuburb]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPhoneNo]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactDetails]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupFrom]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]            [bit] NULL,
		[BookingRefNo]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [int] NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		[Branch]                 [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupEmail]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPostCode]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Costom]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CostomId]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentID]          [int] NULL,
		CONSTRAINT [PK_tblBooking]
		PRIMARY KEY
		CLUSTERED
		([BookingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblBooking]
	ADD
	CONSTRAINT [DEF_tblBooking_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblBooking_6_846626059__K18D_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_19_20_21_22_23_24_25_26]
	ON [dbo].[tblBooking] ([CreatedDateTime] DESC)
	INCLUDE ([BookingID], [PhoneNumber], [ContactName], [ContactEmail], [PickupDate], [PickupTime], [OrderCoupons], [PickupFromCustomer], [PickupName], [PickupAddress1], [PickupAddress2], [PickupSuburb], [PickupPhoneNo], [ContactDetails], [PickupFrom], [IsProcessed], [BookingRefNo], [CreatedBy], [UpdatedDateTime], [UpdatedBy], [Branch], [PickupEmail], [PickupPostCode], [Costom], [CostomId])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBooking] SET (LOCK_ESCALATION = TABLE)
GO
