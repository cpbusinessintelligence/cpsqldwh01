SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBookingArchive] (
		[BookingID]              [int] NULL,
		[PhoneNumber]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactEmail]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupDate]             [date] NULL,
		[PickupTime]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OrderCoupons]           [bit] NULL,
		[PickupFromCustomer]     [bit] NULL,
		[PickupName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress1]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress2]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupSuburb]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPhoneNo]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactDetails]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupFrom]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]            [bit] NULL,
		[BookingRefNo]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]        [datetime] NULL,
		[CreatedBy]              [int] NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		[Branch]                 [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupEmail]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPostCode]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Costom]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CostomId]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentID]          [int] NULL
)
GO
ALTER TABLE [dbo].[tblBookingArchive] SET (LOCK_ESCALATION = TABLE)
GO
