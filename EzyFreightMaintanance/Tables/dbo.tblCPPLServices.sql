SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCPPLServices] (
		[CPPLServiceID]       [int] IDENTITY(1, 1) NOT NULL,
		[ServiceName]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ServiceCode]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblCPPLServices]
		PRIMARY KEY
		CLUSTERED
		([CPPLServiceID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCPPLServices] SET (LOCK_ESCALATION = TABLE)
GO
