SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblClientCPPLServiceAccess] (
		[AccessID]            [int] IDENTITY(1, 1) NOT NULL,
		[ClientID]            [int] NOT NULL,
		[CPPLServiceID]       [int] NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblClientCPPLServiceAccess]
		PRIMARY KEY
		CLUSTERED
		([AccessID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblClientCPPLServiceAccess]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblClient_tblClientCPPLServiceAccess]
	FOREIGN KEY ([ClientID]) REFERENCES [dbo].[tblClient] ([ClientID])
ALTER TABLE [dbo].[tblClientCPPLServiceAccess]
	CHECK CONSTRAINT [FK_FK_tblClient_tblClientCPPLServiceAccess]

GO
ALTER TABLE [dbo].[tblClientCPPLServiceAccess]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblCPPLServices_tblClientCPPLServiceAccess]
	FOREIGN KEY ([CPPLServiceID]) REFERENCES [dbo].[tblCPPLServices] ([CPPLServiceID])
ALTER TABLE [dbo].[tblClientCPPLServiceAccess]
	CHECK CONSTRAINT [FK_FK_tblCPPLServices_tblClientCPPLServiceAccess]

GO
ALTER TABLE [dbo].[tblClientCPPLServiceAccess] SET (LOCK_ESCALATION = TABLE)
GO
