SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCompanyStatusLog] (
		[CompanyStatusLogID]     [int] IDENTITY(1, 1) NOT NULL,
		[CompanyID]              [int] NULL,
		[CompanyStatus]          [bit] NULL,
		[StatusReason]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MoreDetails]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [int] NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		CONSTRAINT [PK_tblCompanyStatusLog]
		PRIMARY KEY
		CLUSTERED
		([CompanyStatusLogID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCompanyStatusLog] SET (LOCK_ESCALATION = TABLE)
GO
