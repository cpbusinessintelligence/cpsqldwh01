SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCompanyTestUser] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[Accountcode]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountName]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[createddatetime]     [datetime] NULL,
		[editeddatetime]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblCompanyTestUser]
	ADD
	CONSTRAINT [DF__tblCompan__creat__5B988E2F]
	DEFAULT (getdate()) FOR [createddatetime]
GO
ALTER TABLE [dbo].[tblCompanyTestUser]
	ADD
	CONSTRAINT [DF__tblCompan__edite__5C8CB268]
	DEFAULT (getdate()) FOR [editeddatetime]
GO
ALTER TABLE [dbo].[tblCompanyTestUser] SET (LOCK_ESCALATION = TABLE)
GO
