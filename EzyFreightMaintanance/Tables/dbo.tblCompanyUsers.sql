SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCompanyUsers] (
		[CompanyUsersID]       [int] IDENTITY(1, 1) NOT NULL,
		[UserID]               [int] NOT NULL,
		[CompanyID]            [int] NOT NULL,
		[IsUserDisabled]       [bit] NULL,
		[ReasonSubject]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReasonDesciption]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		[PromotionalCode]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblCompanyUsers]
		PRIMARY KEY
		CLUSTERED
		([CompanyUsersID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCompanyUsers]
	ADD
	CONSTRAINT [DF__tblCompan__IsUse__5DB5E0CB]
	DEFAULT ((0)) FOR [IsUserDisabled]
GO
ALTER TABLE [dbo].[tblCompanyUsers]
	ADD
	CONSTRAINT [DF_tblCompanyUsers_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblCompanyUsers] SET (LOCK_ESCALATION = TABLE)
GO
