SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignment] (
		[ConsignmentID]                        [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentCode]                      [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UserID]                               [int] NULL,
		[IsRegUserConsignment]                 [bit] NOT NULL,
		[PickupID]                             [int] NULL,
		[DestinationID]                        [int] NULL,
		[ContactID]                            [int] NULL,
		[TotalWeight]                          [decimal](10, 4) NULL,
		[TotalMeasureWeight]                   [decimal](10, 4) NULL,
		[TotalVolume]                          [decimal](10, 4) NULL,
		[TotalMeasureVolume]                   [decimal](10, 4) NULL,
		[NoOfItems]                            [int] NOT NULL,
		[SpecialInstruction]                   [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerRefNo]                        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentPreferPickupDate]          [date] NOT NULL,
		[ConsignmentPreferPickupTime]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ClosingTime]                          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DangerousGoods]                       [bit] NOT NULL,
		[Terms]                                [bit] NOT NULL,
		[RateCardID]                           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LastActivity]                         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActiivityDateTime]                [datetime] NULL,
		[ConsignmentStatus]                    [int] NULL,
		[EDIDataProcessed]                     [bit] NULL,
		[ProntoDataExtracted]                  [bit] NULL,
		[IsBilling]                            [bit] NULL,
		[IsManifested]                         [bit] NULL,
		[CreatedDateTime]                      [datetime] NOT NULL,
		[CreatedBy]                            [int] NULL,
		[UpdatedDateTTime]                     [datetime] NULL,
		[UpdatedBy]                            [int] NULL,
		[IsInternational]                      [bit] NULL,
		[IsDocument]                           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSignatureReq]                       [bit] NULL,
		[IfUndelivered]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReasonForExport]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TypeOfExport]                         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]                             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsInsurance]                          [bit] NULL,
		[IsIdentity]                           [bit] NULL,
		[IdentityType]                         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IdentityNo]                           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country-ServiceArea-FacilityCode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InternalServiceCode]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NetSubTotal]                          [decimal](10, 2) NULL,
		[IsATl]                                [bit] NULL,
		[IsReturnToSender]                     [bit] NULL,
		[HasReadInsuranceTc]                   [bit] NULL,
		[NatureOfGoods]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginServiceAreaCode]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProductShortName]                     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]                             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETA]                                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CTIManifestExtracted]                 [bit] NULL,
		[IsProcessed]                          [bit] NULL,
		[IsAccountCustomer]                    [bit] NULL,
		[InsuranceAmount]                      [decimal](10, 2) NULL,
		[CourierPickupDate]                    [datetime] NULL,
		[CalculatedTotal]                      [decimal](18, 2) NULL,
		[CalculatedGST]                        [decimal](18, 2) NULL,
		[ClientCode]                           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PromotionCode]                        [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [TUC_tblConsignment_1]
		UNIQUE
		NONCLUSTERED
		([ConsignmentCode])
		ON [PRIMARY],
		CONSTRAINT [PK_tblConsignment]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF_tblConsignment_IsBilling]
	DEFAULT ((0)) FOR [IsBilling]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF_tblConsignment_IsManifested]
	DEFAULT ((0)) FOR [IsManifested]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF_tblConsignment_IsInternational]
	DEFAULT ((0)) FOR [IsInternational]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__CTIMa__0CA5D9DE]
	DEFAULT ((0)) FOR [CTIManifestExtracted]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__IsPro__515009E6]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblConsignment]
	ADD
	CONSTRAINT [DF__tblConsig__IsAcc__5BCD9859]
	DEFAULT ((0)) FOR [IsAccountCustomer]
GO
ALTER TABLE [dbo].[tblConsignment]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment]
	FOREIGN KEY ([PickupID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[tblConsignment]
	CHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment]

GO
ALTER TABLE [dbo].[tblConsignment]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment_1]
	FOREIGN KEY ([DestinationID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[tblConsignment]
	CHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment_1]

GO
ALTER TABLE [dbo].[tblConsignment]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblAddress_tblConsignment_2]
	FOREIGN KEY ([ContactID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[tblConsignment]
	CHECK CONSTRAINT [FK_FK_tblAddress_tblConsignment_2]

GO
ALTER TABLE [dbo].[tblConsignment]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblStatus_tblConsignment]
	FOREIGN KEY ([ConsignmentStatus]) REFERENCES [dbo].[tblStatus] ([StatusID])
ALTER TABLE [dbo].[tblConsignment]
	CHECK CONSTRAINT [FK_FK_tblStatus_tblConsignment]

GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblConsignment_ConsignmentCode]
	ON [dbo].[tblConsignment] ([ConsignmentCode])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignment_6_87671360__K28D_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_29_30_31_32_]
	ON [dbo].[tblConsignment] ([CreatedDateTime] DESC)
	INCLUDE ([ConsignmentID], [ConsignmentCode], [UserID], [IsRegUserConsignment], [PickupID], [NatureOfGoods], [OriginServiceAreaCode], [ProductShortName], [SortCode], [ETA], [Country-ServiceArea-FacilityCode], [InternalServiceCode], [NetSubTotal], [IsATl], [IsReturnToSender], [HasReadInsuranceTc], [TypeOfExport], [Currency], [IsInsurance], [IsIdentity], [IdentityType], [IdentityNo], [UpdatedBy], [IsInternational], [IsDocument], [IsSignatureReq], [IfUndelivered], [ReasonForExport], [EDIDataProcessed], [ProntoDataExtracted], [IsBilling], [IsManifested], [CreatedBy], [UpdatedDateTTime], [DangerousGoods], [Terms], [RateCardID], [LastActivity], [LastActiivityDateTime], [ConsignmentStatus], [NoOfItems], [SpecialInstruction], [CustomerRefNo], [ConsignmentPreferPickupDate], [ConsignmentPreferPickupTime], [ClosingTime], [DestinationID], [ContactID], [TotalWeight], [TotalMeasureWeight], [TotalVolume], [TotalMeasureVolume])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignment_6_87671360__K27_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_28_29_30_31_32_]
	ON [dbo].[tblConsignment] ([IsManifested])
	INCLUDE ([ConsignmentID], [ConsignmentCode], [UserID], [IsRegUserConsignment], [PickupID], [NatureOfGoods], [OriginServiceAreaCode], [ProductShortName], [SortCode], [ETA], [Country-ServiceArea-FacilityCode], [InternalServiceCode], [NetSubTotal], [IsATl], [IsReturnToSender], [HasReadInsuranceTc], [TypeOfExport], [Currency], [IsInsurance], [IsIdentity], [IdentityType], [IdentityNo], [UpdatedBy], [IsInternational], [IsDocument], [IsSignatureReq], [IfUndelivered], [ReasonForExport], [EDIDataProcessed], [ProntoDataExtracted], [IsBilling], [CreatedDateTime], [CreatedBy], [UpdatedDateTTime], [DangerousGoods], [Terms], [RateCardID], [LastActivity], [LastActiivityDateTime], [ConsignmentStatus], [NoOfItems], [SpecialInstruction], [CustomerRefNo], [ConsignmentPreferPickupDate], [ConsignmentPreferPickupTime], [ClosingTime], [DestinationID], [ContactID], [TotalWeight], [TotalMeasureWeight], [TotalVolume], [TotalMeasureVolume])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignment_6_87671360__K32_K27_K20_1_2_6_8_12_36_49]
	ON [dbo].[tblConsignment] ([IsInternational], [IsManifested], [RateCardID])
	INCLUDE ([ConsignmentID], [ConsignmentCode], [DestinationID], [TotalWeight], [NoOfItems], [ReasonForExport], [NatureOfGoods])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignment_6_87671360__K32_K27_K20_1_2_5_6_12]
	ON [dbo].[tblConsignment] ([IsInternational], [IsManifested], [RateCardID])
	INCLUDE ([ConsignmentID], [ConsignmentCode], [PickupID], [DestinationID], [NoOfItems])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignment_6_87671360__K1_K28_2_20_27_32]
	ON [dbo].[tblConsignment] ([ConsignmentID], [CreatedDateTime])
	INCLUDE ([ConsignmentCode], [RateCardID], [IsManifested], [IsInternational])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignment_6_87671360__K32_K55_K24_K2_K1_K5_K6_8_10_12_13_18_20_28_45]
	ON [dbo].[tblConsignment] ([IsInternational], [IsProcessed], [EDIDataProcessed], [ConsignmentCode], [ConsignmentID], [PickupID], [DestinationID])
	INCLUDE ([TotalWeight], [TotalVolume], [NoOfItems], [SpecialInstruction], [DangerousGoods], [RateCardID], [CreatedDateTime], [NetSubTotal])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignment_6_87671360__K55_K32_K24_K6_K5_K1_2_8_10_12_13_18_20_28_45]
	ON [dbo].[tblConsignment] ([IsProcessed], [IsInternational], [EDIDataProcessed], [DestinationID], [PickupID], [ConsignmentID])
	INCLUDE ([RateCardID], [CreatedDateTime], [NetSubTotal], [ConsignmentCode], [TotalWeight], [TotalVolume], [NoOfItems], [SpecialInstruction], [DangerousGoods])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblConsignment] SET (LOCK_ESCALATION = TABLE)
GO
