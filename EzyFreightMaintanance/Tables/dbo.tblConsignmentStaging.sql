SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignmentStaging] (
		[ConsignmentStagingID]      [int] IDENTITY(1, 1) NOT NULL,
		[UserID]                    [int] NULL,
		[IsRegUserConsignment]      [bit] NOT NULL,
		[PickupID]                  [int] NULL,
		[DestinationID]             [int] NULL,
		[ContactID]                 [int] NULL,
		[TotalWeight]               [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalVolume]               [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NoOfItems]                 [int] NOT NULL,
		[SpecialInstruction]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerRefNo]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupDate]                [date] NOT NULL,
		[PreferPickupTime]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ClosingTime]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DangerousGoods]            [bit] NOT NULL,
		[Terms]                     [bit] NOT NULL,
		[ServiceID]                 [int] NOT NULL,
		[RateCardID]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsProcessed]               [bit] NULL,
		[CreatedDateTime]           [datetime] NOT NULL,
		[CreatedBy]                 [int] NULL,
		[UpdatedDateTTime]          [datetime] NULL,
		[UpdatedBy]                 [int] NULL,
		[ConsignmentId]             [int] NULL,
		[IsDocument]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSignatureReq]            [bit] NULL,
		[IfUndelivered]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReasonForExport]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TypeOfExport]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsInsurance]               [bit] NULL,
		[IsIdentity]                [bit] NULL,
		[IdentityType]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IdentityNo]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequest]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponce]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsATl]                     [bit] NULL,
		[IsReturnToSender]          [bit] NULL,
		[HasReadInsuranceTc]        [bit] NULL,
		[NatureOfGoods]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequestPU]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponcePU]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETA]                       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PaymentRefNo]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequestPU_DHL]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponcePU_DHL]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLRequestPU_NZPost]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[XMLResponcePU_NZPost]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MerchantReferenceCode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubscriptionID]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsAccountCustomer]         [bit] NULL,
		[InsuranceAmount]           [decimal](10, 2) NULL,
		[CourierPickupDate]         [datetime] NULL,
		[CalculatedTotal]           [decimal](18, 2) NULL,
		[CalculatedGST]             [decimal](18, 2) NULL,
		[ClientCode]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[USPSRefNo]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PromotionCode]             [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblConsignmentStaging]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentStagingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblConsignmentStaging]
	ADD
	CONSTRAINT [DF_tblConsignmentStaging_IsProcessed_0012]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblConsignmentStaging]
	ADD
	CONSTRAINT [DF__tblConsig__IsAcc__5CC1BC92]
	DEFAULT ((0)) FOR [IsAccountCustomer]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignmentStaging_6_39671189__K22_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_23_24_25_26_27_28_29_30_]
	ON [dbo].[tblConsignmentStaging] ([UpdatedDateTTime])
	INCLUDE ([ConsignmentStagingID], [UserID], [IsRegUserConsignment], [PickupID], [DestinationID], [ContactID], [TotalWeight], [TotalVolume], [NoOfItems], [SpecialInstruction], [CustomerRefNo], [PickupDate], [PreferPickupTime], [ClosingTime], [DangerousGoods], [Terms], [ServiceID], [RateCardID], [IsProcessed], [CreatedDateTime], [CreatedBy], [UpdatedBy], [ConsignmentId], [IsDocument], [IsSignatureReq], [IfUndelivered], [ReasonForExport], [TypeOfExport], [Currency], [IsInsurance], [IsIdentity], [IdentityType], [IdentityNo], [XMLRequest], [XMLResponce], [IsATl], [IsReturnToSender], [HasReadInsuranceTc], [NatureOfGoods], [SortCode], [XMLRequestPU], [XMLResponcePU], [ETA], [PaymentRefNo], [XMLRequestPU_DHL], [XMLResponcePU_DHL], [XMLRequestPU_NZPost], [XMLResponcePU_NZPost])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignmentStaging_6_39671189__K1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_]
	ON [dbo].[tblConsignmentStaging] ([ConsignmentStagingID])
	INCLUDE ([UserID], [IsRegUserConsignment], [PickupID], [DestinationID], [ContactID], [TotalWeight], [TotalVolume], [NoOfItems], [SpecialInstruction], [CustomerRefNo], [PickupDate], [PreferPickupTime], [ClosingTime], [DangerousGoods], [Terms], [ServiceID], [RateCardID], [IsProcessed], [CreatedDateTime], [CreatedBy], [UpdatedDateTTime], [UpdatedBy], [ConsignmentId], [IsDocument], [IsSignatureReq], [IfUndelivered], [ReasonForExport], [TypeOfExport], [Currency], [IsInsurance], [IsIdentity], [IdentityType], [IdentityNo], [XMLRequest], [XMLResponce], [IsATl], [IsReturnToSender], [HasReadInsuranceTc], [NatureOfGoods], [SortCode], [XMLRequestPU], [XMLResponcePU], [ETA], [PaymentRefNo], [XMLRequestPU_DHL], [XMLResponcePU_DHL], [XMLRequestPU_NZPost], [XMLResponcePU_NZPost])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblConsignmentStaging_6_39671189__K20D_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_21_22_23_24_25_26_27_28_29_]
	ON [dbo].[tblConsignmentStaging] ([CreatedDateTime] DESC)
	INCLUDE ([ConsignmentStagingID], [UserID], [IsRegUserConsignment], [PickupID], [DestinationID], [ContactID], [TotalWeight], [TotalVolume], [NoOfItems], [SpecialInstruction], [CustomerRefNo], [PickupDate], [PreferPickupTime], [ClosingTime], [DangerousGoods], [Terms], [ServiceID], [RateCardID], [IsProcessed], [CreatedBy], [UpdatedDateTTime], [UpdatedBy], [ConsignmentId], [IsDocument], [IsSignatureReq], [IfUndelivered], [ReasonForExport], [TypeOfExport], [Currency], [IsInsurance], [IsIdentity], [IdentityType], [IdentityNo], [XMLRequest], [XMLResponce], [IsATl], [IsReturnToSender], [HasReadInsuranceTc], [NatureOfGoods], [SortCode], [XMLRequestPU], [XMLResponcePU], [ETA], [PaymentRefNo], [XMLRequestPU_DHL], [XMLResponcePU_DHL], [XMLRequestPU_NZPost], [XMLResponcePU_NZPost])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblConsignmentStaging] SET (LOCK_ESCALATION = TABLE)
GO
