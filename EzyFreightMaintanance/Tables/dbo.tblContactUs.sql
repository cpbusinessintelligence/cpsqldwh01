SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContactUs] (
		[Id]                 [int] IDENTITY(1, 1) NOT NULL,
		[FirstName]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastName]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]              [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]              [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StateID]            [int] NULL,
		[Subject]            [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HowCanIHelpYou]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSubscribe]        [bit] NULL,
		[CreatedDate]        [datetime] NULL,
		[TrackingNo]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AddressLine1]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AddressLine2]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_TBLContactUs]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblContactUs]
	ADD
	CONSTRAINT [DF_TBLContactUs_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblContactUs_6_919674324__K12_1_2_3_4_5_6_7_8_9_10_11_13_14_15]
	ON [dbo].[tblContactUs] ([CreatedDate])
	INCLUDE ([Id], [FirstName], [LastName], [Email], [Phone], [TrackingNo], [AddressLine1], [AddressLine2], [Suburb], [PostCode], [StateID], [Subject], [HowCanIHelpYou], [IsSubscribe])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblContactUs] SET (LOCK_ESCALATION = TABLE)
GO
