SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContactUs_Archive] (
		[Id]                 [int] NULL,
		[FirstName]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastName]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]              [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]              [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StateID]            [int] NULL,
		[Subject]            [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HowCanIHelpYou]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSubscribe]        [bit] NULL,
		[CreatedDate]        [datetime] NULL,
		[TrackingNo]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AddressLine1]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AddressLine2]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblContactUs_Archive] SET (LOCK_ESCALATION = TABLE)
GO
