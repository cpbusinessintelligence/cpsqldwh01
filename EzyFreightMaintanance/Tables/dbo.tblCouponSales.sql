SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCouponSales] (
		[SalesID]                 [int] IDENTITY(1, 1) NOT NULL,
		[SalesType]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RunNumber]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContractorNumber]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SalesDateTime]           [datetime] NULL,
		[InvoiceNumber]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerPhoneNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerDLB]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StartCouponNumber]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InsuranceType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CouponAmount]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InsuranceAmount]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GST]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalAmount]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]             [bit] NULL,
		[CreatedBy]               [int] NULL,
		[CreatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [int] NULL,
		[UpdatedDateTime]         [datetime] NULL,
		CONSTRAINT [PK_tblCouponSales]
		PRIMARY KEY
		CLUSTERED
		([SalesID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCouponSales] SET (LOCK_ESCALATION = TABLE)
GO
