SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomDeclaration] (
		[CustomDeclarationId]     [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]           [int] NULL,
		[ItemDescription]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemInBox]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnitPrice]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubTotal]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HSCode]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryofOrigin]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		[CreatedBy]               [int] NULL,
		[UpdatedDateTTime]        [datetime] NULL,
		[UpdatedBy]               [int] NULL,
		CONSTRAINT [PK_tblCustomDeclaration]
		PRIMARY KEY
		CLUSTERED
		([CustomDeclarationId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCustomDeclaration]
	ADD
	CONSTRAINT [DF_tblCustomDeclaration_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblCustomDeclaration]
	WITH CHECK
	ADD CONSTRAINT [FK_tblCustomDeclaration_tblConsignment]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
ALTER TABLE [dbo].[tblCustomDeclaration]
	CHECK CONSTRAINT [FK_tblCustomDeclaration_tblConsignment]

GO
ALTER TABLE [dbo].[tblCustomDeclaration] SET (LOCK_ESCALATION = TABLE)
GO
