SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomDeclaration_Archive] (
		[CustomDeclarationId]     [int] NULL,
		[ConsignmentID]           [int] NULL,
		[ItemDescription]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemInBox]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnitPrice]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubTotal]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HSCode]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CountryofOrigin]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]         [datetime] NULL,
		[CreatedBy]               [int] NULL,
		[UpdatedDateTTime]        [datetime] NULL,
		[UpdatedBy]               [int] NULL
)
GO
ALTER TABLE [dbo].[tblCustomDeclaration_Archive] SET (LOCK_ESCALATION = TABLE)
GO
