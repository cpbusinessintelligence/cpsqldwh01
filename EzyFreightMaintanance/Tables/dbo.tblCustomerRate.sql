SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomerRate] (
		[CustomerRateID]     [int] IDENTITY(1, 1) NOT NULL,
		[UserID]             [int] NOT NULL,
		[RateCardID]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]           [bit] NOT NULL,
		[CreatedDate]        [datetime] NOT NULL,
		[CreatedBy]          [int] NOT NULL,
		[UpdatedDate]        [datetime] NULL,
		[UpdatedBy]          [int] NULL,
		CONSTRAINT [PK_tblCustomerRate]
		PRIMARY KEY
		CLUSTERED
		([CustomerRateID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblCustomerRate]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblRateCard_tblCustomerRate]
	FOREIGN KEY ([RateCardID]) REFERENCES [dbo].[tblRateCard] ([RateCardID])
ALTER TABLE [dbo].[tblCustomerRate]
	CHECK CONSTRAINT [FK_FK_tblRateCard_tblCustomerRate]

GO
ALTER TABLE [dbo].[tblCustomerRate]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_Users_tblCustomerRate]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[tblCustomerRate]
	CHECK CONSTRAINT [FK_FK_Users_tblCustomerRate]

GO
ALTER TABLE [dbo].[tblCustomerRate] SET (LOCK_ESCALATION = TABLE)
GO
