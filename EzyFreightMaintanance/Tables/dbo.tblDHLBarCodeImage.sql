SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDHLBarCodeImage] (
		[Id]                     [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]          [int] NULL,
		[AWBCode]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AWBBarCode]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginDestncode]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginDestnBarcode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientIDCode]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientIDBarCode]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DHLRoutingCode]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DHLRoutingBarCode]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]        [datetime] NULL,
		[CreatedBy]              [int] NULL,
		CONSTRAINT [PK_tblDHLBarCodeImage]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	ADD
	CONSTRAINT [DF_tblDHLBarCodeImage_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	ADD
	CONSTRAINT [DF_tblDHLBarCodeImage_CreatedBy]
	DEFAULT ((-1)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	WITH CHECK
	ADD CONSTRAINT [FK_tblDHLBarCodeImage_tblConsignment]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
ALTER TABLE [dbo].[tblDHLBarCodeImage]
	CHECK CONSTRAINT [FK_tblDHLBarCodeImage_tblConsignment]

GO
CREATE NONCLUSTERED INDEX [_dta_index_tblDHLBarCodeImage_6_1191675293__K2_9987]
	ON [dbo].[tblDHLBarCodeImage] ([ConsignmentID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblDHLBarCodeImage_6_1191675293__K2_3]
	ON [dbo].[tblDHLBarCodeImage] ([ConsignmentID])
	INCLUDE ([AWBCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage] SET (LOCK_ESCALATION = TABLE)
GO
