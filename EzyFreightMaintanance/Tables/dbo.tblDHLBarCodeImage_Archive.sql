SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDHLBarCodeImage_Archive] (
		[Id]                     [int] NULL,
		[ConsignmentID]          [int] NULL,
		[AWBCode]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AWBBarCode]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginDestncode]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginDestnBarcode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientIDCode]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientIDBarCode]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DHLRoutingCode]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DHLRoutingBarCode]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]        [datetime] NULL,
		[CreatedBy]              [int] NULL
)
GO
ALTER TABLE [dbo].[tblDHLBarCodeImage_Archive] SET (LOCK_ESCALATION = TABLE)
GO
