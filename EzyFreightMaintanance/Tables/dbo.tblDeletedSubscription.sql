SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDeletedSubscription] (
		[DeletedSubID]              [int] IDENTITY(1, 1) NOT NULL,
		[PaymentRefNo]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MerchantReferenceCode]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubscriptionID]            [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewRequestID]              [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Message]                   [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NOT NULL,
		[CreatedBY]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblDeletedSubscription]
		PRIMARY KEY
		CLUSTERED
		([DeletedSubID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblDeletedSubscription]
	ADD
	CONSTRAINT [DF_tblDeletedSubscription_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblDeletedSubscription] SET (LOCK_ESCALATION = TABLE)
GO
