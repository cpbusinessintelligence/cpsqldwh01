SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrorLog] (
		[Id]               [int] IDENTITY(1, 1) NOT NULL,
		[Error]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FunctionInfo]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientId]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDate]       [datetime] NULL,
		[Request]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblErrorLog]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblErrorLog]
	ADD
	CONSTRAINT [DF_tblErrorLog_CreateDate]
	DEFAULT (getdate()) FOR [CreateDate]
GO
CREATE STATISTICS [_dta_stat_66099276_5]
	ON [dbo].[tblErrorLog] ([CreateDate])
GO
ALTER TABLE [dbo].[tblErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
