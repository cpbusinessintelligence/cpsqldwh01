SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrorLog_Archive] (
		[Id]               [int] NULL,
		[Error]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FunctionInfo]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientId]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDate]       [datetime] NULL,
		[Request]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblErrorLog_Archive] SET (LOCK_ESCALATION = TABLE)
GO
