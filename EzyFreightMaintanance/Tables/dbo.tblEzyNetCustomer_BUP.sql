SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEzyNetCustomer_BUP] (
		[CustomerID]          [int] IDENTITY(1, 1) NOT NULL,
		[PhoneNo]             [int] NULL,
		[Branch]              [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FirstName]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Lastname]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]               [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedBy]           [int] NULL,
		[CreatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		CONSTRAINT [IX_tblEzyNetCustomer_BUP]
		UNIQUE
		NONCLUSTERED
		([PhoneNo], [Branch])
		ON [PRIMARY],
		CONSTRAINT [PK_tblEzyNetCustomer_BUP]
		PRIMARY KEY
		CLUSTERED
		([CustomerID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblEzyNetCustomer_BUP]
	ADD
	CONSTRAINT [DF_tblEzyNetCustomer_BUP_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblEzyNetCustomer_BUP] SET (LOCK_ESCALATION = TABLE)
GO
