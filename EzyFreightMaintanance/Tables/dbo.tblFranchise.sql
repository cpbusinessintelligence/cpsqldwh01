SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFranchise] (
		[FranchiseRequestID]     [int] IDENTITY(1, 1) NOT NULL,
		[FirstName]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LastName]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CompanyName]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ABN]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                  [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address1]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Address2]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State]                  [int] NULL,
		[PostCode]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]                  [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Mobile]                 [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DateOfBirth]            [date] NULL,
		[Reason]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ValidLicense]           [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [int] NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		CONSTRAINT [PK_tblFranchise]
		PRIMARY KEY
		CLUSTERED
		([FranchiseRequestID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblFranchise]
	ADD
	CONSTRAINT [DF_tblFranchise_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblFranchise] SET (LOCK_ESCALATION = TABLE)
GO
