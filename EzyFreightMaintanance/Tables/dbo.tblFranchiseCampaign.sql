SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFranchiseCampaign] (
		[FranchiseCampaignID]      [int] IDENTITY(1, 1) NOT NULL,
		[FirstName]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastName]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Postcode]                 [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PreferredArea]            [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Location]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                    [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]                    [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FranchiseRequirement]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Comment]                  [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]          [datetime] NULL,
		[CreatedBy]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDatetime]          [datetime] NULL,
		[UpdateBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblFranchiseCampaign]
		PRIMARY KEY
		CLUSTERED
		([FranchiseCampaignID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblFranchiseCampaign] SET (LOCK_ESCALATION = TABLE)
GO
