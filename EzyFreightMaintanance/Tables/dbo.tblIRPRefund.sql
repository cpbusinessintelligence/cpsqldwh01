SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblIRPRefund] (
		[IRPID]                 [int] IDENTITY(1, 1) NOT NULL,
		[Branch]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountNumber]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FranchiseNumber]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BusinessName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FirstName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastName]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Phone]                 [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StreetAddress1]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StreetAddress2]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Postcode]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BSBNumber]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BankAccountNumber]     [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountName]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RemittanceEmail]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BookNo]                [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDatetime]       [datetime] NULL,
		[UpdateBy]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblIRPRefund]
		PRIMARY KEY
		CLUSTERED
		([IRPID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblIRPRefund]
	ADD
	CONSTRAINT [DF_tblIRPRefund_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblIRPRefund] SET (LOCK_ESCALATION = TABLE)
GO
