SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInsurance] (
		[InsuranceID]             [int] IDENTITY(1, 1) NOT NULL,
		[InsuranceCategoryID]     [int] NOT NULL,
		[InsuranceDetail]         [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[InsuranceCharge]         [decimal](19, 4) NOT NULL,
		[IsArchive]               [bit] NOT NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		[CreatedBy]               [int] NOT NULL,
		[UpdatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [int] NULL,
		CONSTRAINT [PK_tblInsurance]
		PRIMARY KEY
		CLUSTERED
		([InsuranceID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblInsurance]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblnsuranceCategory_tblInsurance]
	FOREIGN KEY ([InsuranceCategoryID]) REFERENCES [dbo].[tblnsuranceCategory] ([InsuranceCategoryID])
ALTER TABLE [dbo].[tblInsurance]
	CHECK CONSTRAINT [FK_FK_tblnsuranceCategory_tblInsurance]

GO
ALTER TABLE [dbo].[tblInsurance] SET (LOCK_ESCALATION = TABLE)
GO
