SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInvoice] (
		[InvoiceID]                 [int] IDENTITY(1, 1) NOT NULL,
		[InvoiceNumber]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UserID]                    [int] NULL,
		[PickupAddressID]           [int] NULL,
		[DestinationAddressID]      [int] NULL,
		[ContactAddressID]          [int] NULL,
		[TotalFreightExGST]         [decimal](19, 4) NOT NULL,
		[GST]                       [decimal](19, 4) NOT NULL,
		[TotalFreightInclGST]       [decimal](19, 4) NOT NULL,
		[InvoiceStatus]             [int] NOT NULL,
		[SendToPronto]              [bit] NOT NULL,
		[PaymentRefNo]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NOT NULL,
		[CreatedBY]                 [int] NOT NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [int] NULL,
		[MerchantReferenceCode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubscriptionID]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PromotionDiscount]         [decimal](19, 4) NULL,
		[DiscountedTotal]           [decimal](19, 4) NULL,
		CONSTRAINT [IX_tblInvoice]
		UNIQUE
		NONCLUSTERED
		([InvoiceNumber])
		ON [PRIMARY],
		CONSTRAINT [PK_tblInvoice]
		PRIMARY KEY
		CLUSTERED
		([InvoiceID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblInvoice] SET (LOCK_ESCALATION = TABLE)
GO
