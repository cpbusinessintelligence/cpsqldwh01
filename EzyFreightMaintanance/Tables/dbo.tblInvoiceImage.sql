SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInvoiceImage] (
		[Id]                      [int] IDENTITY(1, 1) NOT NULL,
		[InvoiceID]               [int] NOT NULL,
		[InvoiceImage]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ArchiveInvoiceImage]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		[CreatedBy]               [int] NOT NULL,
		[UpdatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [int] NULL,
		CONSTRAINT [PK_tblInvoiceImage]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblInvoiceImage]
	ADD
	CONSTRAINT [DF_tblInvoiceImage_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblInvoiceImage] SET (LOCK_ESCALATION = TABLE)
GO
