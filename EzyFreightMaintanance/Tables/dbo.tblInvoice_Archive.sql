SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInvoice_Archive] (
		[InvoiceID]                 [int] NULL,
		[InvoiceNumber]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UserID]                    [int] NULL,
		[PickupAddressID]           [int] NULL,
		[DestinationAddressID]      [int] NULL,
		[ContactAddressID]          [int] NULL,
		[TotalFreightExGST]         [decimal](19, 4) NULL,
		[GST]                       [decimal](19, 4) NULL,
		[TotalFreightInclGST]       [decimal](19, 4) NULL,
		[InvoiceStatus]             [int] NULL,
		[SendToPronto]              [bit] NULL,
		[PaymentRefNo]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NULL,
		[CreatedBY]                 [int] NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [int] NULL,
		[MerchantReferenceCode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubscriptionID]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PromotionDiscount]         [decimal](19, 4) NULL,
		[DiscountedTotal]           [decimal](19, 4) NULL
)
GO
ALTER TABLE [dbo].[tblInvoice_Archive] SET (LOCK_ESCALATION = TABLE)
GO
