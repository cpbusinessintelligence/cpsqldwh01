SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblItemLabel] (
		[ItemLabelID]              [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]            [int] NOT NULL,
		[LabelNumber]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Length]                   [decimal](10, 2) NULL,
		[Width]                    [decimal](10, 2) NULL,
		[Height]                   [decimal](10, 2) NULL,
		[CubicWeight]              [decimal](10, 4) NULL,
		[MeasureLength]            [decimal](10, 2) NULL,
		[MeasureWidth]             [decimal](10, 2) NULL,
		[MeasureHeight]            [decimal](10, 2) NULL,
		[MeasureCubicWeight]       [decimal](10, 4) NULL,
		[PhysicalWeight]           [decimal](10, 4) NULL,
		[MeasureWeight]            [decimal](10, 4) NULL,
		[DeclareVolume]            [decimal](10, 4) NULL,
		[LastActivity]             [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActivityDateTime]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnitValue]                [decimal](10, 2) NULL,
		[Quantity]                 [int] NULL,
		[CountryOfOrigin]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HSTariffNumber]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]          [datetime] NOT NULL,
		[CreatedBy]                [int] NOT NULL,
		[UpdatedDateTime]          [datetime] NULL,
		[UpdatedBy]                [int] NULL,
		[DHLBarCode]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblItemLabel]
		PRIMARY KEY
		CLUSTERED
		([ItemLabelID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblItemLabel]
	ADD
	CONSTRAINT [DEF_tblItemLabel_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblItemLabel]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblConsignment_tblItemLabel]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
ALTER TABLE [dbo].[tblItemLabel]
	CHECK CONSTRAINT [FK_FK_tblConsignment_tblItemLabel]

GO
CREATE NONCLUSTERED INDEX [_dta_index_tblItemLabel_6_295672101__K25]
	ON [dbo].[tblItemLabel] ([UpdatedBy])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblItemLabel_6_295672101__K2_9987]
	ON [dbo].[tblItemLabel] ([ConsignmentID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblItemLabel] SET (LOCK_ESCALATION = TABLE)
GO
