SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblItemLabelImage] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[ItemLabelID]           [int] NOT NULL,
		[LableImage]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ArchiveLableImage]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		CONSTRAINT [PK_tblItemLabelImage]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblItemLabelImage]
	ADD
	CONSTRAINT [DF_tblItemLabelImage_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblItemLabelImage]
	WITH CHECK
	ADD CONSTRAINT [FK_tblItemLabelImage_tblItemLabel]
	FOREIGN KEY ([ItemLabelID]) REFERENCES [dbo].[tblItemLabel] ([ItemLabelID])
ALTER TABLE [dbo].[tblItemLabelImage]
	CHECK CONSTRAINT [FK_tblItemLabelImage_tblItemLabel]

GO
CREATE NONCLUSTERED INDEX [IX_tblItemLabelImage]
	ON [dbo].[tblItemLabelImage] ([Id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblItemLabelImage] SET (LOCK_ESCALATION = TABLE)
GO
