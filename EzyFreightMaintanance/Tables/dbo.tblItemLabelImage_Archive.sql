SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblItemLabelImage_Archive] (
		[Id]                    [int] NULL,
		[ItemLabelID]           [int] NOT NULL,
		[LableImage]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ArchiveLableImage]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [int] NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL
)
GO
ALTER TABLE [dbo].[tblItemLabelImage_Archive] SET (LOCK_ESCALATION = TABLE)
GO
