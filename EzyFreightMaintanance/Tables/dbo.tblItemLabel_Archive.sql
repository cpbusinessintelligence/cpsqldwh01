SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblItemLabel_Archive] (
		[ItemLabelID]              [int] NULL,
		[ConsignmentID]            [int] NULL,
		[LabelNumber]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Length]                   [decimal](10, 2) NULL,
		[Width]                    [decimal](10, 2) NULL,
		[Height]                   [decimal](10, 2) NULL,
		[CubicWeight]              [decimal](10, 4) NULL,
		[MeasureLength]            [decimal](10, 2) NULL,
		[MeasureWidth]             [decimal](10, 2) NULL,
		[MeasureHeight]            [decimal](10, 2) NULL,
		[MeasureCubicWeight]       [decimal](10, 4) NULL,
		[PhysicalWeight]           [decimal](10, 4) NULL,
		[MeasureWeight]            [decimal](10, 4) NULL,
		[DeclareVolume]            [decimal](10, 4) NULL,
		[LastActivity]             [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActivityDateTime]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnitValue]                [decimal](10, 2) NULL,
		[Quantity]                 [int] NULL,
		[CountryOfOrigin]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HSTariffNumber]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]          [datetime] NULL,
		[CreatedBy]                [int] NULL,
		[UpdatedDateTime]          [datetime] NULL,
		[UpdatedBy]                [int] NULL,
		[DHLBarCode]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblItemLabel_Archive] SET (LOCK_ESCALATION = TABLE)
GO
