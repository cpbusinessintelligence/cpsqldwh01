SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOpenAccount] (
		[Id]                                   [int] IDENTITY(1, 1) NOT NULL,
		[AddressId]                            [int] NULL,
		[Position]                             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsAccountPaymentContact]              [bit] NULL,
		[AccountAddressId]                     [int] NULL,
		[BusinessAddressId]                    [int] NULL,
		[TradingName]                          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ACN]                                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsRegisteredForGST]                   [bit] NULL,
		[CompanyWebsite]                       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostalAddressId]                      [int] NULL,
		[MonthlyDomesticVolume]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyUsedDomesticShipping]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MonthlyInternationalVolume]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyUsedInternationalShipping]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreditRequested]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PaymentTermsRequested]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsAgree]                              [bit] NULL,
		[IsAuthorize]                          [bit] NULL,
		[CreatedDateTime]                      [datetime] NOT NULL,
		[CreatedBy]                            [int] NULL,
		[UpdatedDateTTime]                     [datetime] NULL,
		[UpdatedBy]                            [int] NULL,
		CONSTRAINT [PK_tblOpenAccount]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblOpenAccount]
	ADD
	CONSTRAINT [DF_tblOpenAccount_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblOpenAccount] SET (LOCK_ESCALATION = TABLE)
GO
