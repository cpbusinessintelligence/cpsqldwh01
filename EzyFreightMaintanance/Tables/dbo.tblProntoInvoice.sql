SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProntoInvoice] (
		[InvoiceID]                 [int] IDENTITY(1, 1) NOT NULL,
		[InvoiceNo]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[InvoiceDate]               [datetime] NOT NULL,
		[ProntoAccountNo]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[WebsiteAccountNo]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyName]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactFname]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactLname]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactAddress]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContactNo]                 [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GrossTotal]                [decimal](19, 4) NOT NULL,
		[GST]                       [decimal](19, 4) NULL,
		[FuelSurCharge]             [decimal](19, 4) NULL,
		[Insurance]                 [decimal](19, 4) NULL,
		[OtherCharge]               [decimal](19, 4) NULL,
		[NetTotal]                  [decimal](19, 4) NOT NULL,
		[TotalOwingAmount]          [decimal](19, 4) NULL,
		[PaidAmount]                [decimal](19, 4) NOT NULL,
		[ReceiptNo]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsPaymentProcessed]        [bit] NULL,
		[IsReceiptProcessed]        [bit] NULL,
		[Comment]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PaymentRefNo]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubscriptionID]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MerchantReferenceCode]     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MessageFromPronto]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NOT NULL,
		[CreatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblProntoInvoice]
		PRIMARY KEY
		CLUSTERED
		([InvoiceID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblProntoInvoice] SET (LOCK_ESCALATION = TABLE)
GO
