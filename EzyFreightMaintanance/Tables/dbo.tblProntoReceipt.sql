SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblProntoReceipt] (
		[ReceiptID]               [int] IDENTITY(1, 1) NOT NULL,
		[ReceiptNo]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AccountNo]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[WebsiteAccountNo]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[GrossTotal]              [decimal](19, 4) NOT NULL,
		[GST]                     [decimal](19, 4) NULL,
		[FuelSurCharge]           [decimal](19, 4) NULL,
		[Insurance]               [decimal](19, 4) NULL,
		[OtherCharge]             [decimal](19, 4) NULL,
		[CreditCardSurCharge]     [decimal](19, 4) NULL,
		[NetTotal]                [decimal](19, 4) NULL,
		[TotalOwingAmount]        [decimal](19, 4) NULL,
		[PaidAmount]              [decimal](19, 4) NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		[CreatedBy]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblProntoReceipt]
		PRIMARY KEY
		CLUSTERED
		([ReceiptID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblProntoReceipt] SET (LOCK_ESCALATION = TABLE)
GO
