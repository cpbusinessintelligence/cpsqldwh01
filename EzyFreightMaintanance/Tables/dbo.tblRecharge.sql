SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRecharge] (
		[Id]                           [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]                [int] NULL,
		[AdminFee]                     [decimal](18, 2) NULL,
		[Difference]                   [decimal](18, 2) NULL,
		[ETA]                          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExtensionData]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GrossInvoiceAmount]           [decimal](18, 2) NULL,
		[GSTonGrossInvoiceAmount]      [decimal](18, 2) NULL,
		[NetTotal]                     [decimal](18, 2) NULL,
		[NewCalculatedTotal]           [decimal](18, 2) NULL,
		[NewWeightforCalculation]      [decimal](18, 2) NULL,
		[ReweighStatus]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReweighStatusDescription]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceDescription]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SignatureOption]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSignatureReq]               [bit] NULL,
		[OrijnalCalculatedTotal]       [decimal](18, 2) NULL,
		[PaymentRefNo]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MerchantReferenceCode]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]                  [bit] NULL,
		[CreatedDateTime]              [datetime] NULL,
		[CreatedBy]                    [int] NULL,
		[UpdatedDateTime]              [datetime] NULL,
		[UpdatedBy]                    [int] NULL,
		CONSTRAINT [PK_tblRecharge]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRecharge]
	ADD
	CONSTRAINT [DF_Table_1_IsProcess]
	DEFAULT ((0)) FOR [GrossInvoiceAmount]
GO
ALTER TABLE [dbo].[tblRecharge]
	ADD
	CONSTRAINT [DF_tblRecharge_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRecharge]
	WITH CHECK
	ADD CONSTRAINT [FK_tblRecharge_tblConsignment]
	FOREIGN KEY ([ConsignmentID]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
ALTER TABLE [dbo].[tblRecharge]
	CHECK CONSTRAINT [FK_tblRecharge_tblConsignment]

GO
ALTER TABLE [dbo].[tblRecharge] SET (LOCK_ESCALATION = TABLE)
GO
