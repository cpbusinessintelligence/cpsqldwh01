SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedelivery] (
		[RedeliveryID]                [int] IDENTITY(1, 1) NOT NULL,
		[RedeliveryType]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CardReferenceNumber]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LableNumber]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ConsignmentCode]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State]                       [int] NOT NULL,
		[AttemptedRedeliveryTime]     [datetime] NOT NULL,
		[SenderName]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NumberOfItem]                [int] NULL,
		[DestinationName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DestinationAddress]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DestinationSuburb]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DestinationPostCode]         [int] NOT NULL,
		[SelectedETADate]             [date] NOT NULL,
		[PreferDeliverTimeSlot]       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PreferDeliverTime]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]             [datetime] NOT NULL,
		[CreatedBy]                   [int] NOT NULL,
		[UpdatedDateTime]             [datetime] NULL,
		[UpdatedBy]                   [int] NULL,
		[IsProcessed]                 [bit] NULL,
		[Firstname]                   [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Lastname]                    [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                       [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PhoneNumber]                 [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SPInstruction]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblRedelivery]
		PRIMARY KEY
		CLUSTERED
		([RedeliveryID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRedelivery]
	ADD
	CONSTRAINT [DF_tblRedelivery_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRedelivery]
	ADD
	CONSTRAINT [DF_tblRedelivery_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
CREATE NONCLUSTERED INDEX [TUC_%ConsignmentCode%_1]
	ON [dbo].[tblRedelivery] ([ConsignmentCode])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblRedelivery_6_478624748__K17_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_18_19_20_21_22_23_24_25_26]
	ON [dbo].[tblRedelivery] ([CreatedDateTime])
	INCLUDE ([RedeliveryID], [CardReferenceNumber], [LableNumber], [ConsignmentCode], [Branch], [PhoneNumber], [SPInstruction], [UpdatedDateTime], [UpdatedBy], [IsProcessed], [Firstname], [Lastname], [Email], [DestinationSuburb], [DestinationPostCode], [SelectedETADate], [PreferDeliverTimeSlot], [PreferDeliverTime], [CreatedBy], [State], [AttemptedRedeliveryTime], [SenderName], [NumberOfItem], [DestinationName], [DestinationAddress])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRedelivery] SET (LOCK_ESCALATION = TABLE)
GO
