SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedelivery_Archive] (
		[RedeliveryID]                [int] NULL,
		[RedeliveryType]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CardReferenceNumber]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LableNumber]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentCode]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                       [int] NULL,
		[AttemptedRedeliveryTime]     [datetime] NULL,
		[SenderName]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NumberOfItem]                [int] NULL,
		[DestinationName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationAddress]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationSuburb]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPostCode]         [int] NULL,
		[SelectedETADate]             [date] NULL,
		[PreferDeliverTimeSlot]       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PreferDeliverTime]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]             [datetime] NULL,
		[CreatedBy]                   [int] NULL,
		[UpdatedDateTime]             [datetime] NULL,
		[UpdatedBy]                   [int] NULL,
		[IsProcessed]                 [bit] NULL,
		[Firstname]                   [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Lastname]                    [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                       [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PhoneNumber]                 [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SPInstruction]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblRedelivery_Archive] SET (LOCK_ESCALATION = TABLE)
GO
