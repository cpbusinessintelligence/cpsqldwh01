SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedirectedRefund] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[ReConsignmentID]       [int] NULL,
		[Reason]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Amount]                [decimal](18, 2) NULL,
		[PaymentRefNo]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcess]             [bit] NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [int] NULL,
		[UpdatedDateTTime]      [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		CONSTRAINT [PK_tblRedirectedRefund]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRedirectedRefund]
	ADD
	CONSTRAINT [DF_tblRedirectedRefund_IsProcess]
	DEFAULT ((0)) FOR [IsProcess]
GO
ALTER TABLE [dbo].[tblRedirectedRefund]
	ADD
	CONSTRAINT [DF_tblRedirectedRefund_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRedirectedRefund]
	WITH CHECK
	ADD CONSTRAINT [FK_tblRedirectedRefund_tblRedirectedRefund]
	FOREIGN KEY ([ReConsignmentID]) REFERENCES [dbo].[tblRedirectedConsignment] ([ReConsignmentID])
ALTER TABLE [dbo].[tblRedirectedRefund]
	CHECK CONSTRAINT [FK_tblRedirectedRefund_tblRedirectedRefund]

GO
ALTER TABLE [dbo].[tblRedirectedRefund] SET (LOCK_ESCALATION = TABLE)
GO
