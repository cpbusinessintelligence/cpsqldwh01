SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedirectedRefund_Archive] (
		[Id]                    [int] NULL,
		[ReConsignmentID]       [int] NULL,
		[Reason]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Amount]                [decimal](18, 2) NULL,
		[PaymentRefNo]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuthorizationCode]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcess]             [bit] NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [int] NULL,
		[UpdatedDateTTime]      [datetime] NULL,
		[UpdatedBy]             [int] NULL
)
GO
ALTER TABLE [dbo].[tblRedirectedRefund_Archive] SET (LOCK_ESCALATION = TABLE)
GO
