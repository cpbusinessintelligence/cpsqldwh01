SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSalesOrder] (
		[SalesOrderID]          [int] IDENTITY(1, 1) NOT NULL,
		[ReferenceNo]           [int] NOT NULL,
		[UserID]                [int] NULL,
		[NoofItems]             [int] NOT NULL,
		[TotalWeight]           [decimal](10, 4) NOT NULL,
		[TotalVolume]           [decimal](10, 4) NOT NULL,
		[RateCardID]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[GrossTotal]            [decimal](19, 4) NOT NULL,
		[GST]                   [decimal](19, 4) NOT NULL,
		[NetTotal]              [decimal](19, 4) NOT NULL,
		[SalesOrderStatus]      [int] NULL,
		[InvoiceNo]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		[ConsignmentCode]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PromotionDiscount]     [decimal](19, 4) NULL,
		[DiscountedTotal]       [decimal](19, 4) NULL,
		[SalesOrderType]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblSalesOrder]
		PRIMARY KEY
		CLUSTERED
		([SalesOrderID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblSalesOrder]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblStatus_tblSalesOrder]
	FOREIGN KEY ([SalesOrderStatus]) REFERENCES [dbo].[tblStatus] ([StatusID])
ALTER TABLE [dbo].[tblSalesOrder]
	CHECK CONSTRAINT [FK_FK_tblStatus_tblSalesOrder]

GO
ALTER TABLE [dbo].[tblSalesOrder]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblConsignment_tblSalesOrder]
	FOREIGN KEY ([ReferenceNo]) REFERENCES [dbo].[tblConsignment] ([ConsignmentID])
ALTER TABLE [dbo].[tblSalesOrder]
	CHECK CONSTRAINT [FK_FK_tblConsignment_tblSalesOrder]

GO
CREATE NONCLUSTERED INDEX [IX_tblSalesOrder_referenceNo]
	ON [dbo].[tblSalesOrder] ([ReferenceNo])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblSalesOrder_6_375672386__K13_1_2_3_4_5_6_7_8_9_10_11_12_14_15_16]
	ON [dbo].[tblSalesOrder] ([CreatedDateTime])
	INCLUDE ([SalesOrderID], [ReferenceNo], [UserID], [NoofItems], [TotalWeight], [InvoiceNo], [CreatedBy], [UpdatedDateTime], [UpdatedBy], [TotalVolume], [RateCardID], [GrossTotal], [GST], [NetTotal], [SalesOrderStatus])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSalesOrder] SET (LOCK_ESCALATION = TABLE)
GO
