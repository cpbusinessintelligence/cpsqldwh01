SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSalesOrderDetail] (
		[SalesOrderDetailID]     [int] IDENTITY(1, 1) NOT NULL,
		[SalesOrderID]           [int] NOT NULL,
		[Description]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LineNo]                 [int] NOT NULL,
		[Weight]                 [decimal](10, 4) NULL,
		[Volume]                 [decimal](10, 4) NULL,
		[FreightCharge]          [decimal](19, 4) NOT NULL,
		[FuelCharge]             [decimal](19, 4) NULL,
		[InsuranceCharge]        [decimal](19, 4) NULL,
		[ServiceCharge]          [decimal](19, 4) NULL,
		[Total]                  [decimal](19, 4) NOT NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [int] NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		CONSTRAINT [PK_tblSalesOrderDetail]
		PRIMARY KEY
		CLUSTERED
		([SalesOrderDetailID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblSalesOrderDetail]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblSalesOrder_tblSalesOrderDetail]
	FOREIGN KEY ([SalesOrderID]) REFERENCES [dbo].[tblSalesOrder] ([SalesOrderID])
ALTER TABLE [dbo].[tblSalesOrderDetail]
	CHECK CONSTRAINT [FK_FK_tblSalesOrder_tblSalesOrderDetail]

GO
CREATE NONCLUSTERED INDEX [_dta_index_tblSalesOrderDetail_6_455672671__K2_K12_1_3_4_5_6_7_8_9_10_11_13_14_15]
	ON [dbo].[tblSalesOrderDetail] ([SalesOrderID], [CreatedDateTime])
	INCLUDE ([SalesOrderDetailID], [Description], [LineNo], [Weight], [InsuranceCharge], [ServiceCharge], [Total], [CreatedBy], [UpdatedDateTime], [UpdatedBy], [Volume], [FreightCharge], [FuelCharge])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_tblSalesOrderDetail_6_455672671__K12_1_2_3_4_5_6_7_8_9_10_11_13_14_15]
	ON [dbo].[tblSalesOrderDetail] ([CreatedDateTime])
	INCLUDE ([SalesOrderDetailID], [SalesOrderID], [Description], [LineNo], [Weight], [InsuranceCharge], [ServiceCharge], [Total], [CreatedBy], [UpdatedDateTime], [UpdatedBy], [Volume], [FreightCharge], [FuelCharge])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSalesOrderDetail] SET (LOCK_ESCALATION = TABLE)
GO
