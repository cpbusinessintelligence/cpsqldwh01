SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSalesOrder_Archive] (
		[SalesOrderID]          [int] NULL,
		[ReferenceNo]           [int] NULL,
		[UserID]                [int] NULL,
		[NoofItems]             [int] NULL,
		[TotalWeight]           [decimal](10, 4) NULL,
		[TotalVolume]           [decimal](10, 4) NULL,
		[RateCardID]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GrossTotal]            [decimal](19, 4) NULL,
		[GST]                   [decimal](19, 4) NULL,
		[NetTotal]              [decimal](19, 4) NULL,
		[SalesOrderStatus]      [int] NULL,
		[InvoiceNo]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [int] NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		[ConsignmentCode]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PromotionDiscount]     [decimal](19, 4) NULL,
		[DiscountedTotal]       [decimal](19, 4) NULL,
		[SalesOrderType]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblSalesOrder_Archive] SET (LOCK_ESCALATION = TABLE)
GO
