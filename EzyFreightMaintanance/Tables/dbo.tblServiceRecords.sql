SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblServiceRecords] (
		[Id]                        [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentCode]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VendorName]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPPLServiceName]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPPLServiceEndPoint]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPPLServiceRequest]        [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPPLServiceResponse]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VendorServiceName]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VendorServiceEndPoint]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VendorServiceRequest]      [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VendorServiceResponse]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NULL,
		[CreatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblServiceRecords]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblServiceRecords] SET (LOCK_ESCALATION = TABLE)
GO
