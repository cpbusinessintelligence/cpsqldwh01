SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblState] (
		[StateID]             [int] IDENTITY(1, 1) NOT NULL,
		[StateCode]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[StateName]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblState]
		PRIMARY KEY
		CLUSTERED
		([StateID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblState]
	ADD
	CONSTRAINT [DF__tblState__Create__7F60ED59]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblState] SET (LOCK_ESCALATION = TABLE)
GO
