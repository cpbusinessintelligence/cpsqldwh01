SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTrackingStaging] (
		[TrackingD]            [int] IDENTITY(1, 1) NOT NULL,
		[LabelNumber]          [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TrackingActivity]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TrackingDatetime]     [datetime] NULL,
		[IsProcessed]          [bit] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NULL,
		[UpdatedDateTTime]     [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblTrackingStaging]
		PRIMARY KEY
		CLUSTERED
		([TrackingD])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblTrackingStaging] SET (LOCK_ESCALATION = TABLE)
GO
