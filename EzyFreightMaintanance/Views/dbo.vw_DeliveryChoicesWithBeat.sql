SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_DeliveryChoicesWithBeat] as
--SELECT       *
--FROM      cpsqldev01.SampleTestdb.dbo.DeliveryChoices

SELECT        DeliveryChoices.*, DeliveryChoicesBeat.BeatID, DeliveryChoicesBeat.IsProcess As DeliveryChoicesBeat_IsProcess, DeliveryChoicesBeat.CreatedDateTime AS DeliveryChoicesBeat_CreatedDateTime, 
                         DeliveryChoicesBeat.CreatedBy AS DeliveryChoicesBeat_CreatedBy, DeliveryChoicesBeat.UpdatedDateTime As DeliveryChoicesBeat_UpdatedDateTime, DeliveryChoicesBeat.UpdatedBy AS DeliveryChoicesBeat_UpdatedBy
FROM            cpsqlops01.CouponCalculator.dbo.DeliveryChoices  LEFT JOIN
                         cpsqlops01.CouponCalculator.dbo.DeliveryChoicesBeat ON DeliveryChoices.DeliveryChoiceID = DeliveryChoicesBeat.DeliveryChoiceID

GO
