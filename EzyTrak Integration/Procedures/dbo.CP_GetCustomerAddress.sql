SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CP_GetCustomerAddress]
@CustomerCode Varchar(50)
AS
SELECT
	Code As CustomerCode
	,[Name] As CustomerName
	,DeliveryAddress1 As AddressLine1
	,DeliveryAddress2 As AddressLine2
	,DeliveryAddress3 As AddressLine3
	,CAST(deliverypostcode AS VARCHAR(5)) + deliverysuburbname As Suburb
	,'2770' As Postcode
FROM
[dbo].[Customer]
WHERE
	Code = @CustomerCode
	AND IsActive =1
	AND IsDeleted = 0
GO
GRANT EXECUTE
	ON [dbo].[CP_GetCustomerAddress]
	TO [SSISUser]
GO
