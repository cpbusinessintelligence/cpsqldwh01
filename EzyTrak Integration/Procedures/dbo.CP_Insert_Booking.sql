SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CP_Insert_Booking]
(	@Branch VARCHAR(20),
	@PickupType VARCHAR(100),
	@Customer VARCHAR(100),
	@Contact VARCHAR(100),
	@PickupLocation Varchar(100),
	@PickupTime Datetime,
	@PickupCustomer VARCHAR(100) = NULL,
	@PickupSuburb VARCHAR(100) = NULL,
	@PickupPostcode VARCHAR(50) = NULL,
	@PickupAddress1 VARCHAR(100) = NULL,
	@PickupAddress2 VARCHAR(100) = NULL,
	@PickupAddress3 VARCHAR(100) = NULL,
	@PickupAddress4 VARCHAR(100) = NULL,
	@DeliveryCustomer VARCHAR(100) = NULL,
	@DeliverySuburb VARCHAR(100) = NULL,
	@DeliveryPostcode VARCHAR(50) = NULL,
	@DeliveryAddress1 VARCHAR(100) = NULL,
	@DeliveryAddress2 VARCHAR(100) = NULL,
	@DeliveryAddress3 VARCHAR(100) = NULL,
	@DeliveryAddress4 VARCHAR(100) = NULL
)
AS

DECLARE @BookingNo VARCHAR(100), @BookingNumber VARCHAR(100)
EXEC @BookingNo =  [ezy_GetNextBookingNumber] @BookingNumber OUTPUT

INSERT INTO [dbo].[Booking]
      ([BookingNumber]
      ,[Branch]
      ,[BookingDate]
      ,[BookingType]
      ,[Customercode]
	  ,[CustomercontactName]
	  ,[BookingLocation]
      ,[PickupCustomer]
      ,[DeliveryCustomer]
      ,[Pickupsuburb]
      ,[Deliverysuburb]
      ,[PickupPostcode]
      ,[PickupAddress0]
      ,[PickupAddress1]
      ,[PickupAddress2]
      ,[PickupAddress3]
      ,[DeliveryPostcode]
      ,[DeliveryAddress0]
      ,[DeliveryAddress1]
      ,[DeliveryAddress2]
      ,[DeliveryAddress3]
      ,[PickupTime]
      ,[isprocessed]
      ,[CreatedDatetime]
      ,[CreatedBy]
      ,[EditedDatetime]
      ,[EditedBy])
     VALUES
           (
		    @BookingNumber
		   ,@Branch
		   ,Getdate()
		   ,@PickupType
		   ,@Customer
		   ,@Contact
		   ,@PickupLocation
		   ,@PickupCustomer
		   ,@DeliveryCustomer
		   ,@PickupSuburb
		   ,@DeliverySuburb
		   ,@PickupPostcode
		   ,@PickupAddress1
		   ,@PickupAddress2
		   ,@PickupAddress3
		   ,@PickupAddress4
		   ,@DeliveryPostcode
		   ,@DeliveryAddress1
		   ,@DeliveryAddress2
		   ,@DeliveryAddress3
		   ,@DeliveryAddress4
		   ,@PickupTime
		   ,0
		   ,Getdate()
		   ,'Admin'
		   ,Getdate()
		   ,'Admin'
		   )


		   

		   
GO
GRANT EXECUTE
	ON [dbo].[CP_Insert_Booking]
	TO [SSISUser]
GO
