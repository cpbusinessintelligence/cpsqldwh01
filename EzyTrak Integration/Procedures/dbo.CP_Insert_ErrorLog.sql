SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE CP_Insert_ErrorLog
(
@PickupType Varchar(10),
@PickupStatus Varchar(10),
@ErrorMsg Varchar(Max)
)
As
INSERT INTO [dbo].[ExceptionErrorLog]
           ([type]
           ,[status]
           ,[errormsg])
     VALUES
           (@PickupType
           ,@PickupStatus
           ,@ErrorMsg)
GO
GRANT EXECUTE
	ON [dbo].[CP_Insert_ErrorLog]
	TO [SSISUser]
GO
