SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CP_PostCodeExists]
@CPPostCode VARCHAR(50)
AS
SELECT
	Suburb
	,PostCode
FROM
	[dbo].[SuburbException]
WHERE
	PostCode = @CPPostCode
GO
GRANT EXECUTE
	ON [dbo].[CP_PostCodeExists]
	TO [SSISUser]
GO
