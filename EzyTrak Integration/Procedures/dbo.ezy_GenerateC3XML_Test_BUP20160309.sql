SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[ezy_GenerateC3XML_Test_BUP20160309]
AS
DECLARE @c3XML xml
BEGIN TRAN

Declare @Temp table (ItemNumber varchar(50))

--IF OBJECT_ID ('temp..#C3XMLLabels') IS NOT NULL
--	DROP TABLE #C3XMLLables
--	CREATE TABLE #C3XMLLables
--		(
--			[ItemNumber] VARCHAR(50)
--		)

--	INSERT INTO #C3XMLLables([ItemNumber]) 

	Insert into @Temp(ItemNumber)
	SELECT 
		[ItemNumber] 
	FROM 
		[dbo].[DeliveryItemLabel] (NOLOCK)
	WHERE 
		Isprocessed = 0 and istest=1

--Generate XML Starts
	SET @c3XML = (SELECT 
	(SELECT 
	'REQ' + CONVERT(VARCHAR,GETDATE(),112) +  CONVERT(VARCHAR,DATEPART(hour,GETDATE())) +  CONVERT(VARCHAR,DATEPART(minute,GETDATE())) +  CONVERT(VARCHAR,DATEPART(SECOND,GETDATE())) +  REPLACE(STR(CAST(ROW_NUMBER() OVER (ORDER BY [SNo]) as varchar) , 5), SPACE(1), '0')  AS RequestID,
	SourceSystem,
	ProcessingCode,
	DI.ItemNumber,
	CNACardNumber,
	SLCNACardNumber,
	RTCNACardNumber,
	[Weight],
	ServiceType,
	TrafficIndicator,
	ActualItemNumber,
	BillToCustomerID,
	BillToCustomerAccountNumber,
	BillToName,
	DeliveryToCompanyName,
	DeliveryToName,
	DeliveryToRecipientID,
	DeliveryToPhone,
	DeliveryToMail,
	DeliveryToAddress1,
	DeliveryToAddress2,
	DeliveryToAddress3,
	DeliveryToCity,
	DeliveryToAddressZip,
	DeliveryToState,
	DeliveryToCountryCode,
	DeliveryToCountryName,
	DeliveryToPreferredFromTime,
	DeliveryToPreferredToTime,
	Instructions,
	IsATL,
	IsRedelivery,
	RedeliveryDate,
	Courierid As AgentOrVendorId
	FROM
	[dbo].[DeliveryItemLabel] DI(NOLOCK)
			INNER JOIN @Temp t
	ON DI.[ItemNumber] = t.[ItemNumber]
	FOR XML PATH('Delivery'), TYPE)
	FOR XML PATH(''))
--Generate XML Ends

	UPDATE	DIL
	SET DIL.IsProcessed = 1,
	DIL.EditedBy = 'C3Service',
	DIL.EditedDatetime = getdate()
	FROM 
	[dbo].[DeliveryItemLabel] DIL(NOLOCK)
		INNER JOIN 
	@Temp t ON
		DIL.[ItemNumber] = t.[ItemNumber]

IF @@ERROR <> 0
	BEGIN
		ROLLBACK
		RETURN
	END
COMMIT

--IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Temp') DROP TABLE Temp;

SELECT @c3XML as c3

GO
