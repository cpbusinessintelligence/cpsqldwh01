SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_LoadDeliveryItemLabelDev02Test] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadDeliveryItemLabel
    --' ---------------------------
    --' Purpose: sp_LoadDeliveryItemLabel-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 21 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Begin transaction cpweb



Insert into DeliveryItemLabel(
SourceSystem,
ProcessingCode,
ItemNumber,
--SiblingItemNumber,
CNACardNumber,
SLCNACardNumber,
RTCNACardNumber,
Weight,
ServiceType,
--AmountToCollect,
TrafficIndicator,
ActualItemNumber,
BillToCustomerID,
BillToCustomerAccountNumber,
--BillToCustomerCostCentreCode,
BillToName,
DeliveryToCompanyName,
DeliveryToName,
DeliveryToRecipientID,
DeliveryToPhone,
DeliveryToMail,
DeliveryToAddress1,
DeliveryToAddress2,
DeliveryToAddress3,
DeliveryToCity,
DeliveryToAddressZip,
DeliveryToState,
DeliveryToCountryCode,
DeliveryToCountryName,
DeliveryToPreferredFromTime,
DeliveryToPreferredToTime,
Instructions,
IsATL,
IsRedelivery,
DLB,
IsProcessed)

Select 
       'CP_WEB',
       'N' as ProcessingCode,
	   ltrim(rtrim(Labelnumber)),
--	   '',
       '',
	   '',
	   '',
	   l.physicalweight,
	   c.ratecardid,
	   'LCL',
	   '',
	   '112995527',
	   '112995527',
	   'Retail' as CustomerName,
	  a1.firstname+''+a1.lastname,
       a1.firstname+''+a1.lastname,
	   '',
	    a1.phone, 
	    a1.email,
	   a1.address1,
	    a1.address2,
	   '',
	   a1.suburb,
	   a1.postcode,
	   isnull(s1.StateCode,isnull(a1.statename,'Unknown')),
       a1.countrycode,
	   a1.country,
	  convert(varchar(20),ConsignmentPreferPickupDate)+'T'+convert(varchar(12),cast('1:15 pm' as Time)),
	  convert(varchar(20),dateadd("day",4,ConsignmentPreferPickupDate))+'T'+convert(varchar(12),cast('1:15 pm' as Time)),
	   replace(isnull(specialinstruction,''),',',''),
	   IsATL,
	   0,
	   '',
	   0
from  [cpsqldev02].[EzyFreight].[dbo].[tblConsignment] c left join [cpsqldev02].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                            left join [cpsqldev02].[EzyFreight].[dbo].[tblItemLabel] l on l.consignmentid=c.consignmentid
                                            left join [cpsqldev02].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											left JOIN [cpsqldev02].[EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											left JOIN [cpsqldev02].[EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
											left Join [cpsqldwh01].[DWH].[dbo].[Postcodes] p on p.postcode COLLATE SQL_Latin1_General_CP1_CI_AS=a.postcode   COLLATE SQL_Latin1_General_CP1_CI_AS  and p.suburb COLLATE SQL_Latin1_General_CP1_CI_AS=a.suburb COLLATE SQL_Latin1_General_CP1_CI_AS
where  

--c.userid<>2368 and c.userid<>1920 and c.userid<>4071 and  c.userid<> 2603 and c.userid<>4273 and c.userid<>4275 and c.userid<>6177 and
--convert(date,c.createddatetime)= convert(date,getdate()-1)
c.createddatetime>=
dateadd(hour,-1,getdate())  and
--dateadd(minute,-120,getdate()) 
 c.isinternational=0 and (ltrim(rtrim(Labelnumber)) like 'CPWL10%' or ltrim(rtrim(Labelnumber)) like 'CPWY39%')
and not exists(select ItemNumber from DeliveryItemLabel where Itemnumber COLLATE SQL_Latin1_General_CP1_CI_AS =l.labelnumber COLLATE SQL_Latin1_General_CP1_CI_AS)  and labelnumber not like '%CNA'


Commit transaction cpweb

Update DeliveryItemLabel set DeliveryToAddressZip=(Select Sortingcode from [dbo].[DeliveryChoices] where substring(DeliveryToAddress1,5,3)=[DeliveryChoiceID] and [Category]='POPStation')
where SourceSystem ='CP_WEB' and isnull(DeliveryToAddress1,'') like 'C/- %POPStation%' and  istest=0  and (ItemNumber like 'CPWL10%' or ItemNumber like 'CPWY39%')
--and itemnumber='CPWPDC00000207101'
and isprocessed=0


--select * from DeliveryItemLabel where istest=1 order by createddatetime desc
end



GO
