SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadEztdriver] as
begin

Insert into [dbo].[eztDriver](CourierId)

select distinct [dbo].[fn_CreateUniqueDriverID](branch,drivernumber,prontoid) as CourierId
from [cpsqldwh01].[cosmos].[dbo].[driver] d  where d.isactive=1
and not exists(Select courierid from [eztDriver] d1  where [dbo].[fn_CreateUniqueDriverID](branch,drivernumber,prontoid)=d1.courierid )
---where d.courierid=[dbo].[fn_CreateUniqueDriverID](b.name,d.code,d.prontodrivercode))

end
GO
GRANT EXECUTE
	ON [dbo].[sp_LoadEztdriver]
	TO [SSISUser]
GO
