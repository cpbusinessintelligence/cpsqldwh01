SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_LoadEztdriver_BUP20160310] as
begin

Insert into [dbo].[eztDriver](CourierId)

select [dbo].[fn_CreateUniqueDriverID](b.name,d.code,d.prontodrivercode) as CourierId
from [cpsqldwh01].[scannergateway].[dbo].[driver] d join [cpsqldwh01].[scannergateway].[dbo].[Branch] b on b.id=d.branchid where d.isactive=1
and not exists(Select courierid from [eztDriver] d1 where [dbo].[fn_CreateUniqueDriverID](b.name,d.code,d.prontodrivercode)=d1.courierid  )
---where d.courierid=[dbo].[fn_CreateUniqueDriverID](b.name,d.code,d.prontodrivercode))

end
GO
