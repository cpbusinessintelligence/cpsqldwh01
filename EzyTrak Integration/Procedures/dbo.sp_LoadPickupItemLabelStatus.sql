SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadPickupItemLabelStatus](@FileName varchar(100)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadPickupItemLabelStatus
    --' ---------------------------
    --' Purpose: sp_LoadPickupItemLabelStatus-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 21 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

truncate table PickupItemLabelStatusStaging

declare @FilePath varchar(200)
Set @FilePath='E:\ETL Processing\CPEzyTrakIntegraion\Import PickupItemLabelStatus from EzyTrak\Input files\Process\' + @FileName
print @FilePath




DECLARE @x XML, @sql NVARCHAR(MAX);

SELECT @sql = N'SELECT @X = CONVERT(xml,BulkColumn) 
 FROM OPENROWSET(BULK ''' + @FilePath + ''' ,SINGLE_BLOB) as p1';

EXEC sp_executesql @sql, N'@x XML OUTPUT', @x OUTPUT;

INSERT INTO PickupItemLabelStatusStaging(XMLData, LoadedDateTime)
select @x ,getdate()

--INSERT INTO PickupItemLabelStatusStaging(XMLData, LoadedDateTime)
--SELECT CONVERT(XML, BulkColumn) AS BulkColumn, GETDATE() 
--FROM OPENROWSET(BULK @FilePath, SINGLE_BLOB) AS x;


DECLARE @x1 xml
select @x1 = XMLData from PickupItemLabelStatusStaging

  
 Insert into  PickupItemLabelStatus([RequestID]
      ,[OrderNumber]
      ,[StatusCode]
      ,[StatusDescription]
      ,[ReasonDescription]
      ,[StatusBy]
      ,[ServiceType]
      ,[StatusDate]
      ,[TrackingNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[ItemType]
      ,[ItemStatus]
      ,[TotalWeight]
      ,[TotalVolWeight]
      ,[TotalQty]
      ,[CourierID]
      ,[Remarks]
      ,[SplInstructions])
  
  
  select

   T.c.value('RequestID[1]','VARCHAR(100)'),
   T.c.value('OrderNumber[1]','VARCHAR(100)'),
   T.c.value('StatusCode[1]','VARCHAR(100)'),
   T.c.value('StatusDescription[1]','VARCHAR(100)'),
   T.c.value('ReasonDescription[1]','VARCHAR(100)'),
   T.c.value('StatusBy[1]','VARCHAR(100)'),
   T.c.value('ServiceType[1]','VARCHAR(100)'),
   T.c.value('StatusDate[1]','VARCHAR(100)'),
   u.c.value('TrackingNumber[1]','VARCHAR(100)'),
   u.c.value('Length[1]','VARCHAR(100)'),
   u.c.value('Width[1]','VARCHAR(100)'),
   u.c.value('Height[1]','VARCHAR(100)'),
   u.c.value('ItemType[1]','VARCHAR(100)'),
   u.c.value('ItemStatus[1]','VARCHAR(100)'),
   T.c.value('TotalWeight[1]','VARCHAR(100)'),
   T.c.value('TotalVolWeight[1]','VARCHAR(100)'),
   T.c.value('TotalQty[1]','VARCHAR(100)'),
   T.c.value('CourierID[1]','VARCHAR(100)'),
   T.c.value('Remarks[1]','VARCHAR(100)'),
   T.c.value('SplInstrucations[1]','VARCHAR(100)')
from
    @x1.nodes('/ROOT/PickupOrderStatus') T(c)
CROSS APPLY T.c.nodes('PickupItems/PickupItem') u(c)

Update [dbo].[PickupItemLabelStatusStaging] set isprocessed=1 

end
GO
GRANT ALTER
	ON [dbo].[sp_LoadPickupItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT CONTROL
	ON [dbo].[sp_LoadPickupItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_LoadPickupItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[sp_LoadPickupItemLabelStatus]
	TO [z_SSISUser]
GO
GRANT ALTER
	ON [dbo].[sp_LoadPickupItemLabelStatus]
	TO [SSISUser]
GO
GRANT CONTROL
	ON [dbo].[sp_LoadPickupItemLabelStatus]
	TO [SSISUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_LoadPickupItemLabelStatus]
	TO [SSISUser]
GO
