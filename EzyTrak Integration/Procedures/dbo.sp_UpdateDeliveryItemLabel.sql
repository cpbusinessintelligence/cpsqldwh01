SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_UpdateDeliveryItemLabel] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadDeliveryItemLabel
    --' ---------------------------
    --' Purpose: sp_LoadDeliveryItemLabel-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 21 Sep 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/09/2015    AK      1.00    Created the procedure                            

    --'=====================================================================


--------Get Redelivery labels and update------

Select l.labelnumber,convert(varchar(100),'') as Sourcereference,convert(varchar(100),'') as Driver
into #temp
from [cpsqldwh01].[ScannerGateway].[dbo].[Label] l (NOLOCK)
where labelnumber like '%CNA' and l.createddate>= dateadd(day,-5,getdate())
and (not exists (select CNACardNumber from DeliveryItemLabel where labelnumber=CNACardNumber) or not exists (select SLCNACardNumber from DeliveryItemLabel where labelnumber=SLCNACardNumber) or not exists (select RTCNACardNumber from DeliveryItemLabel where labelnumber=RTCNACardNumber))

--select * from #temp where labelnumber='0298765SLCNA'
--drop table #temp1

Update #temp set Sourcereference=te.sourcereference,Driver=[dbo].[fn_CreateUniqueDriverID](b.name,d.code,prontodrivercode)  from [cpsqldwh01].[ScannerGateway].[dbo].[TrackingEvent] te (NOLOCK) join [cpsqldwh01].[ScannerGateway].[dbo].[Driver] d (NOLOCK) on d.id=te.driverid  join [cpsqldwh01].[ScannerGateway].[dbo].[Branch] b (NOLOCK) on b.id=d.branchid where ltrim(rtrim(replace(te.additionaltext1,'Link Coupon ','')))=labelnumber
and te.eventdatetime>= dateadd(day,-5,getdate())

--Select labelnumber,te.sourcereference into #temp1 from #temp join [cpsqldwh01].[ScannerGateway].[dbo].[TrackingEvent] te on ltrim(rtrim(replace(te.additionaltext1,'Link Coupon ','')))=labelnumber
--where te.eventdatetime>= dateadd(hour,-1,getdate())


--select * from #temp where  labelnumber='0018130SLCNA' DeliveryItemLabel

Update DeliveryItemLabel set CNACardNumber=labelnumber,isprocessed=0,ProcessingCode='U',courierid=driver from #temp where #temp.sourcereference=ItemNumber and len(labelnumber)=10 and labelnumber like '%CNA' and processingcode<>'U' and istest=0

Update DeliveryItemLabel set SLCNACardNumber=labelnumber,isprocessed=0,ProcessingCode='U',courierid=driver from #temp where #temp.sourcereference=ItemNumber and len(labelnumber)=12 and labelnumber like '%SLCNA' and processingcode<>'U' and istest=0

Update DeliveryItemLabel set RTCNACardNumber=labelnumber,isprocessed=0,ProcessingCode='U',courierid=driver from #temp where #temp.sourcereference=ItemNumber and len(labelnumber)=12 and labelnumber like '%RTCNA' and processingcode<>'U' and istest=0

--Update DeliveryItemLabel set courierid=[dbo].[fn_CreateUniqueDriverID](b.name,d.code,prontodrivercode)  from #temp join [cpsqldwh01].[ScannerGateway].[dbo].[Driver] d on d.id=Driver  join [cpsqldwh01].[ScannerGateway].[dbo].[Branch] b on b.id=d.branchid
--where driver is not null

end
GO
GRANT EXECUTE
	ON [dbo].[sp_UpdateDeliveryItemLabel]
	TO [SSISUser]
GO
