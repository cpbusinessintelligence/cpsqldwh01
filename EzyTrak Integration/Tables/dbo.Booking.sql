SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Booking] (
		[BookingId]               [int] IDENTITY(1, 1) NOT NULL,
		[BookingNumber]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[BookingType]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Customercode]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CustomercontactName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[BookingLocation]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[BookingDate]             [date] NOT NULL,
		[PickupCustomer]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Pickupsuburb]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostcode]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress0]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress1]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress2]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress3]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryCustomer]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Deliverysuburb]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostcode]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress0]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress1]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress2]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress3]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CouponsReq]              [bit] NULL,
		[BookingStatus]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]             [bit] NOT NULL,
		[CreatedDatetime]         [datetime] NULL,
		[CreatedBy]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]          [datetime] NULL,
		[EditedBy]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupTime]              [datetime] NULL
)
GO
ALTER TABLE [dbo].[Booking]
	ADD
	CONSTRAINT [DF__Booking__isproce__7A672E12]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[Booking]
	ADD
	CONSTRAINT [DF__Booking__Created__7B5B524B]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[Booking]
	ADD
	CONSTRAINT [DF__Booking__Created__7C4F7684]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Booking]
	ADD
	CONSTRAINT [DF__Booking__EditedD__7D439ABD]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[Booking]
	ADD
	CONSTRAINT [DF__Booking__EditedB__7E37BEF6]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[Booking] SET (LOCK_ESCALATION = TABLE)
GO
