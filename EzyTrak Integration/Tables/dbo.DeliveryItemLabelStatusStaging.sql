SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[DeliveryItemLabelStatusStaging] (
		[Id]                 [int] IDENTITY(1, 1) NOT NULL,
		[XMLData]            [xml] NULL,
		[LoadedDateTime]     [datetime] NULL,
		[isprocessed]        [bit] NULL,
		CONSTRAINT [PK__Delivery__3214EC078177F47A]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatusStaging]
	ADD
	CONSTRAINT [DF__DeliveryI__ispro__0B91BA14]
	DEFAULT ((0)) FOR [isprocessed]
GO
GRANT ALTER
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [z_SSISUser]
GO
GRANT CONTROL
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [z_SSISUser]
GO
GRANT DELETE
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [z_SSISUser]
GO
GRANT INSERT
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [z_SSISUser]
GO
GRANT SELECT
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [z_SSISUser]
GO
GRANT TAKE OWNERSHIP
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [z_SSISUser]
GO
GRANT UPDATE
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [z_SSISUser]
GO
GRANT ALTER
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [SSISUser]
GO
GRANT CONTROL
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [SSISUser]
GO
GRANT DELETE
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [SSISUser]
GO
GRANT SELECT
	ON [dbo].[DeliveryItemLabelStatusStaging]
	TO [SSISUser]
GO
ALTER TABLE [dbo].[DeliveryItemLabelStatusStaging] SET (LOCK_ESCALATION = TABLE)
GO
