SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PopStationDetails] (
		[Kiosk Id]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sorting code]           [float] NULL,
		[Kiosk Name]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Kiosk Description]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Location]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Operation Hours]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Full address]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Post code]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Storey]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Unit Number]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[House Block Number]     [float] NULL,
		[Building]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Street Name]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[State]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[go live date]           [datetime] NULL,
		[Lat]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Lng]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Photo done]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PopStationDetails] SET (LOCK_ESCALATION = TABLE)
GO
