SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_TEv2] (
		[requestid]                        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ItemNumber]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CNACardNumber]                    [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[SLCNACardNumber]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[RTCNACardNumber]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[ServiceType]                      [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[TrafficIndicator]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ItemWeight]                       [decimal](18, 5) NULL,
		[StatusCode]                       [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[StatusDescription]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReasonCode]                       [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[ReasonDescription]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CPItemStatusCode]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CPItemReasonCode]                 [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[StatusDate]                       [datetime] NULL,
		[CourierID]                        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NumberOfAttempts]                 [int] NULL,
		[DeliveryToCompanyName]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToName]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToPhone]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress1]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress2]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress3]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddress4]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressPOBox]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressCity]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressState]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressZip]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressCountryCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryToAddressCountryName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryInstructions]             [nvarchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[DLB]                              [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Temp_TEv2] SET (LOCK_ESCALATION = TABLE)
GO
