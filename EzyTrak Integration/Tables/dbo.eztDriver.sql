SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[eztDriver] (
		[CourierId]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[eztDriver]
	ADD
	CONSTRAINT [DF__eztDriver__Creat__17F790F9]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[eztDriver]
	ADD
	CONSTRAINT [DF__eztDriver__Creat__18EBB532]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[eztDriver]
	ADD
	CONSTRAINT [DF__eztDriver__Edite__19DFD96B]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[eztDriver]
	ADD
	CONSTRAINT [DF__eztDriver__Edite__1AD3FDA4]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[eztDriver] SET (LOCK_ESCALATION = TABLE)
GO
