SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create view DeliveryChoices as

SELECT [ID]
      ,[Category]
      ,[DeliveryChoiceID]
      ,[Sortingcode]
      ,[DeliveryChoiceName]
      ,[DeliveryChoiceDescription]
      ,[Operation Hours]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Suburb]
      ,[PostCode]
      ,[State]
      ,[Country]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[ModifiedDateTime]
      ,[ModifiedBy]
      ,[Latitude]
      ,[Longtitude]
  FROM [CPSQLOPS01].[CouponCalculator].[dbo].[DeliveryChoices]
GO
