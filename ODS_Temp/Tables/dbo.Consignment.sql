SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Consignment] (
		[Id]                               [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[ConsignmentNumber]                [varchar](60) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerId]                       [uniqueidentifier] NOT NULL,
		[BillingCustomerId]                [uniqueidentifier] NULL,
		[SenderCustomerId]                 [uniqueidentifier] NULL,
		[ConsignmentDate]                  [datetime] NOT NULL,
		[JobDate]                          [datetime] NOT NULL,
		[CustomerETADate]                  [datetime] NULL,
		[Reference]                        [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[ShipmentTypeId]                   [uniqueidentifier] NOT NULL,
		[TrackingStatus]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProductId]                        [uniqueidentifier] NULL,
		[ContactName]                      [varchar](120) COLLATE Latin1_General_CI_AS NULL,
		[ContactPhone]                     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[ContactEmail]                     [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[ContactPointTypeId]               [uniqueidentifier] NULL,
		[ClosingTime]                      [datetime] NULL,
		[ReadyTime]                        [datetime] NULL,
		[OriginCustomerAddressId]          [uniqueidentifier] NULL,
		[DestinationCustomerAddressId]     [uniqueidentifier] NULL,
		[SpecialInstructions]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[ArrangeDelivery]                  [bit] NOT NULL,
		[ArrangeDeliveryName]              [varchar](120) COLLATE Latin1_General_CI_AS NULL,
		[ArrangeDeliveryPhone]             [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[NotBeforeTime]                    [datetime] NULL,
		[NotAfterTime]                     [datetime] NULL,
		[ArrangeDeliveryDate]              [datetime] NULL,
		[IsArranged]                       [bit] NOT NULL,
		[DangerousGoods]                   [bit] NOT NULL,
		[StatusTypeId]                     [uniqueidentifier] NOT NULL,
		[IsCancelled]                      [bit] NULL,
		[CancelledByUserId]                [uniqueidentifier] NULL,
		[CancelledReasonTypeId]            [uniqueidentifier] NULL,
		[CancelledReasonComment]           [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[CancelledDate]                    [datetime] NULL,
		[CustomerManifestId]               [uniqueidentifier] NULL,
		[OriginAgentManifestId]            [uniqueidentifier] NULL,
		[OriginAgentId]                    [uniqueidentifier] NULL,
		[DestinationAgentManifestId]       [uniqueidentifier] NULL,
		[DestinationAgentId]               [uniqueidentifier] NULL,
		[OriginDepotId]                    [uniqueidentifier] NULL,
		[DestinationDepotId]               [uniqueidentifier] NULL,
		[SourceId]                         [uniqueidentifier] NULL,
		[SourceReference]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[IsBillable]                       [bit] NOT NULL,
		[IsPayable]                        [bit] NOT NULL,
		[BilledStatus]                     [smallint] NOT NULL,
		[PaidStatus]                       [smallint] NOT NULL,
		[IsLocked]                         [bit] NOT NULL,
		[LockedDate]                       [datetime] NULL,
		[LockedByUserId]                   [uniqueidentifier] NULL,
		[IsDeleted]                        [bit] NOT NULL,
		[CreatedDate]                      [datetime] NOT NULL,
		[CreatedByUserId]                  [uniqueidentifier] NOT NULL,
		[LastModifiedDate]                 [datetime] NOT NULL,
		[LastModifiedByUserId]             [uniqueidentifier] NOT NULL,
		[ActualOriginAgentId]              [uniqueidentifier] NULL,
		[ActualOriginDepotId]              [uniqueidentifier] NULL,
		[ActualDestinationAgentId]         [uniqueidentifier] NULL,
		[ActualDestinationDepotId]         [uniqueidentifier] NULL,
		[DeliveryDate]                     [datetime] NULL,
		[PODDate]                          [datetime] NULL,
		CONSTRAINT [PK_Consignment]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_ConsignmentDate]
	DEFAULT (getdate()) FOR [ConsignmentDate]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_JobDate]
	DEFAULT (getdate()) FOR [JobDate]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_ArrangeDelivery]
	DEFAULT ((0)) FOR [ArrangeDelivery]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_IsArranged]
	DEFAULT ((0)) FOR [IsArranged]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_DangerousGoods]
	DEFAULT ((0)) FOR [DangerousGoods]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_IsCancelled]
	DEFAULT ((0)) FOR [IsCancelled]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_IsBillable]
	DEFAULT ((0)) FOR [IsBillable]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_IsPayable]
	DEFAULT ((0)) FOR [IsPayable]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_BilledStatus]
	DEFAULT ((0)) FOR [BilledStatus]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_PaidStatus]
	DEFAULT ((0)) FOR [PaidStatus]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_IsLocked]
	DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Consignment]
	ADD
	CONSTRAINT [DF_Consignment_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Consignment] SET (LOCK_ESCALATION = TABLE)
GO
