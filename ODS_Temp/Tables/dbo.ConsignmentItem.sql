SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConsignmentItem] (
		[Id]                       [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[ConsignmentId]            [uniqueidentifier] NOT NULL,
		[UnitTypeId]               [uniqueidentifier] NOT NULL,
		[ProductId]                [uniqueidentifier] NULL,
		[Quantity]                 [int] NOT NULL,
		[Weight]                   [decimal](18, 4) NOT NULL,
		[Width]                    [decimal](18, 4) NOT NULL,
		[Length]                   [decimal](18, 4) NOT NULL,
		[Height]                   [decimal](18, 4) NOT NULL,
		[Volume]                   [decimal](18, 4) NOT NULL,
		[Reference]                [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]                [bit] NOT NULL,
		[CreatedDate]              [datetime] NOT NULL,
		[CreatedByUserId]          [uniqueidentifier] NOT NULL,
		[LastModifiedDate]         [datetime] NOT NULL,
		[LastModifiedByUserId]     [uniqueidentifier] NOT NULL,
		CONSTRAINT [PK_ConsignmentItem]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ConsignmentItem] SET (LOCK_ESCALATION = TABLE)
GO
