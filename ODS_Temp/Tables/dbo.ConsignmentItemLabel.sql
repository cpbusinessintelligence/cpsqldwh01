SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ConsignmentItemLabel] (
		[Id]                    [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[ConsignmentItemId]     [uniqueidentifier] NOT NULL,
		[LabelId]               [uniqueidentifier] NOT NULL,
		[StatusTypeId]          [uniqueidentifier] NOT NULL,
		[IsDeleted]             [bit] NOT NULL,
		[CreatedDate]           [datetime] NOT NULL,
		[LastModifiedDate]      [datetime] NOT NULL,
		CONSTRAINT [PK_ConsignmentItemLabel]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ConsignmentItemLabel] SET (LOCK_ESCALATION = TABLE)
GO
