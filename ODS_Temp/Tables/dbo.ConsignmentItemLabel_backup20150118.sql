SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ConsignmentItemLabel_backup20150118] (
		[Id]                    [uniqueidentifier] NOT NULL,
		[ConsignmentItemId]     [uniqueidentifier] NOT NULL,
		[LabelId]               [uniqueidentifier] NOT NULL,
		[StatusTypeId]          [uniqueidentifier] NOT NULL,
		[IsDeleted]             [bit] NOT NULL,
		[CreatedDate]           [datetime] NOT NULL,
		[LastModifiedDate]      [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ConsignmentItemLabel_backup20150118] SET (LOCK_ESCALATION = TABLE)
GO
