SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Delivery Choices Report] (
		[ID]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[(NQ1)]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CP1]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CP2]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CP3]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP4]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP5]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP6]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP7]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP8]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP9]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CP10]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CZP11]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[(No column A]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[(No colun name)]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ModifiedDateTime]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[(No column name)]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Latitude]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Longtitude]           [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Delivery Choices Report] SET (LOCK_ESCALATION = TABLE)
GO
