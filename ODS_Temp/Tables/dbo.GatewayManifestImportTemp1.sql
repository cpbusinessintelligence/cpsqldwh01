SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GatewayManifestImportTemp1] (
		[m_id]           [int] NOT NULL,
		[cd_id]          [int] NOT NULL,
		[cd_connote]     [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_GatewayManifestImportTemp_1]
		PRIMARY KEY
		CLUSTERED
		([m_id], [cd_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[GatewayManifestImportTemp1] SET (LOCK_ESCALATION = TABLE)
GO
