SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[Z_fn_CalculateWeekendsandPublicHolidays](@Date date,@Zone varchar(20),@BusinessDays Integer) returns date as
begin

declare @outputdate date =@Date

WHILE (SELECT count(*) FROM [dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))<>0
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	END


WHILE @BusinessDays > 0
BEGIN


Select  @outputdate =dateadd(day,1,@outputdate)

WHILE (SELECT count(*) FROM [dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))<>0
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	END
 Select  @BusinessDays = @BusinessDays -1
END


return @outputdate
end

GO
