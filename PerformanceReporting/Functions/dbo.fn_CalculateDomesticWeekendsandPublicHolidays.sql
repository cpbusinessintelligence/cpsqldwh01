SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


Create function [dbo].[fn_CalculateDomesticWeekendsandPublicHolidays](@Date date,@FromZone varchar(20),@ToZone varchar(20),@BusinessDays Integer) returns date as
begin

declare @outputdate date =@Date

WHILE (SELECT count(*) FROM [dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@FromZone, 'ALL'))<>0
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	END


WHILE @BusinessDays > 0
BEGIN


Select  @outputdate =dateadd(day,1,@outputdate)

WHILE (SELECT count(*) FROM [dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@ToZone, 'ALL'))<>0
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	END
 Select  @BusinessDays = @BusinessDays -1
END


return @outputdate
end

GO
