SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CalculateInternationalETA] (@PickupDate datetime,@Category varchar(50),@ProductType varchar(50),@FromCountry varchar(50),@ToCountry varchar(50),@FromZone varchar(50),@ToZone varchar(50))
returns dateTime
as
begin

Declare @TempCountry varchar(50)

set @TempCountry=case when @Category='OUTBOUND' then @ToCountry when @Category='INBOUND' then @FromCountry else 'Unknown' end

Declare @OutputDateTime datetime

select @OutputDateTime=@PickupDate

Declare @BusinessDays varchar(10)

--Get Business days based on type of Product---

select @BusinessDays=isnull(DHLImpMaxETA,'XXX')
from cpsqlops01.couponcalculator.dbo.intcountry where @Category ='INBOUND' and countryname=@TempCountry

select @BusinessDays=isnull(SaverExpMaxETA,'XXX')
from cpsqlops01.couponcalculator.dbo.intcountry where @Category ='OUTBOUND' and countryname=@TempCountry and @ProductType='Saver'

select @BusinessDays=isnull(DHLExpMaxETA,'XXX')
from cpsqlops01.couponcalculator.dbo.intcountry where @Category ='OUTBOUND' and countryname=@TempCountry and @ProductType='Express'

select @BusinessDays='XXX' where @Category='Unknown'

--select * from cpsqlops01.couponcalculator.dbo.intcountry where  countryname='India' and @ProductType='Express'

--Calculate ETA date based on Business days excluding Public Holidays---
  IF @BusinessDays <> 'XXX'
       
          Select @OutputDateTime = CAST(CAST([dbo].[fn_CalculateInternationalWeekendsandPublicHolidays](convert(date,@PickupDate),@Category,@FromZone,@ToZone,@BusinessDays)AS DATE) AS DATETIME) +  CAST(CAST('23:59:59' AS TIME) AS DATETIME)

   Return @OutputDateTime
end
GO
