SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_CalculateInternationalWeekendsandPublicHolidays](@Date date,@Category varchar(50),@FromZone varchar(20),@ToZone varchar(20),@BusinessDays varchar(10)) returns date as
begin

declare @outputdate date =@Date

--Check if Pickup is done on Sat/Sun incase of OUTBound ---

If @Category='OUTBOUND'

begin

WHILE (SELECT count(*) FROM [dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@FromZone, 'ALL'))<>0
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	END

	WHILE @BusinessDays > 0
BEGIN

Select  @outputdate =dateadd(day,1,@outputdate)
 Select  @BusinessDays = @BusinessDays -1
END


end

--Incase of inbound Pickupdate is date and we do not calculate public holidays--
else
begin

Select  @outputdate = @Date

--Based on  Pickup date calculate the ETA date based on Public holidays in between for delivery to Australia---

WHILE @BusinessDays > 0
BEGIN


Select  @outputdate =dateadd(day,1,@outputdate)

WHILE (SELECT count(*) FROM [dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@ToZone, 'ALL'))<>0
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	END
 Select  @BusinessDays = @BusinessDays -1
END


end






return @outputdate
end
GO
