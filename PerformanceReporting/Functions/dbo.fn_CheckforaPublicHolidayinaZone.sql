SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_CheckforaPublicHolidayinaZone](@Date date,@Zone varchar(20)) returns datetime as
begin

declare @outputdate date=@Date

WHILE (SELECT count(*) FROM [dbo].[PublicHolidays] WHERE [Date] = @outputdate AND Zone IN (@Zone, 'ALL'))<>0
   BEGIN
     Select  @outputdate = dateadd(day,1,@outputdate)
	END



return CAST(@outputdate AS DATETIME) +  CAST(CAST('23:59:59' AS TIME) AS DATETIME)
end
GO
