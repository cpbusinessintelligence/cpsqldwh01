SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_GetETADateFromZoneMinus1Day] (@ProductCategory varchar(50), @RevenueType varchar(20),@AccountCode varchar(20),@Prefix varchar(20), @sFromZone varchar(20), @sToZone varchar(20),@Servicecode varchar(50), @Pickup DateTime,@PickupDriver varchar(20) )
returns dateTime
as
begin
   Declare @OutputDateTime as DateTime
   Declare @PickUpDate as Date = Convert(Date,@Pickup)
   Declare @PickupTime As Time = Convert(Time,@Pickup)
   Declare @BusinessDays  as Varchar(10) 
   Declare @DeliveryTime As Varchar(20)
   IF( Select Count(*) From [dbo].[PerformanceReportingExceptions] WHere isnull(LHType,'')=@ProductCategory and isnull(ProductType,'') = @RevenueType  
                                                                           and isnull([Prefix/AccountCode],'') in ('ALL', CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END) and isnull(Service,'') in ('ALL',@Servicecode) and isnull(PickupBranch,'') in ('ALL', (SELECT [Branch] FROM [dbo].[DimContractor] where DriverID = @PickupDriver)) and isnull([FromZone],'') in ('ALL',@sFromZone)  and isnull(tozone,'') in ('ALL',@sToZone) ) >0
	  BEGIN
         Select @BusinessDays = ISNULL((Select CASE [1stPickupCutOff] WHEN 'XXX' THEN [2ndAllowedDays] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOff]) THEN [2ndAllowedDays] ELSE [1stAllowedDays] END) END  
                                                          From  [dbo].[PerformanceReportingExceptions]
                                                          Where isnull(LHType,'')=@ProductCategory and isnull(ProductType,'') = @RevenueType  
                                                                and isnull([Prefix/AccountCode],'') in ('ALL', CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END) and isnull(Service,'') in ('ALL',@Servicecode) and isnull(PickupBranch,'') in ('ALL', (SELECT [Branch] FROM [dbo].[DimContractor] where DriverID = @PickupDriver)) and isnull([FromZone],'') in ('ALL',@sFromZone)  and isnull(tozone,'') in ('ALL',@sToZone)
	  															and [PickupBranch] in ('ALL', (SELECT [Branch] FROM [dbo].[DimContractor] where DriverID = @PickupDriver))),'XXX')
         Select @DeliveryTime = ISNULL((Select CASE  [1stPickupCutOff] WHEN  'XXX'  THEN [2ndDeliveryOff] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOff]) THEN [2ndDeliveryOff] ELSE [1stDeliveryCutOff] END) END  
                                                          From  [dbo].[PerformanceReportingExceptions]
                                                          Where isnull(LHType,'')=@ProductCategory and isnull(ProductType,'') = @RevenueType  
                                                                and isnull([Prefix/AccountCode],'') in ('ALL', CASE @RevenueType WHEN 'Prepaid' THEN @Prefix ELSE @AccountCode END) and isnull(Service,'') in ('ALL',@Servicecode) and isnull(PickupBranch,'') in ('ALL', (SELECT [Branch] FROM [dbo].[DimContractor] where DriverID = @PickupDriver)) and isnull([FromZone],'') in ('ALL',@sFromZone)  and isnull(tozone,'') in ('ALL',@sToZone)
	  															and [PickupBranch] in ('ALL', (SELECT [Branch] FROM [dbo].[DimContractor] where DriverID = @PickupDriver))),'XXX') 
	  END
	  ELSE 
	  BEGIN
         Select @BusinessDays   =  ISNULL((Select CASE [1stPickupCutOffTime] WHEN 'XXX' THEN [2ndAllowedDays] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOffTime]) THEN [2ndAllowedDays] ELSE [1stAllowedDays] END) END  
                                                          From  [dbo].[ETACalculatorMinus1Day] 
                                                          Where [FromZone] = @sFromZone and [ToZone] = @sToZone and Category=@ProductCategory),'XXX')
         Select @DeliveryTime   =  ISNULL((Select CASE [1stPickupCutOffTime]  WHEN  'XXX'   THEN [2ndDeliveryCutOffTime] ELSE  (CASE WHEN  @PickupTime>COnvert(Time,[1stPickupCutOffTime]) THEN [2ndDeliveryCutOffTime] ELSE [1stDeliveryCutOffTime] END) END  
                                                          From  [dbo].[ETACalculatorMinus1Day] 
                                                          Where [FromZone] = @sFromZone and [ToZone] = @sToZone and Category=@ProductCategory),'XXX') 
	  END
   IF @BusinessDays <> 'XXX'
        IF ISnull(@DeliveryTime,'XXX') <> 'XXX'
          Select @OutputDateTime = CAST(CAST([dbo].[fn_CalculateDomesticWeekendsandPublicHolidays](@Pickup,@sFromZone,@sToZone,@BusinessDays)AS DATE) AS DATETIME) +  CAST(CAST(@DeliveryTime AS TIME) AS DATETIME)
		ELSE 
		  Select @OutputDateTime = CAST(CAST([dbo].[fn_CalculateDomesticWeekendsandPublicHolidays](@Pickup,@sFromZone,@sToZone,@BusinessDays)AS DATE) AS DATETIME) +  CAST(CAST('23:59:59' AS TIME) AS DATETIME)
   Return @OutputDateTime
end


GO
