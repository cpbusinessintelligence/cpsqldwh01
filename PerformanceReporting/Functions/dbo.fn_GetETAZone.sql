SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetETAZone](@PostCode varchar(20) ,@Suburb varchar(20)) returns varchar(800) as
begin

		Return ISNULL((Select [ETAZone]  FROM dbo.Postcodes 
                            Where Postcode =  CASE LEn(Rtrim(Ltrim(@PostCode))) WHEN 3 THEN '0'+Rtrim(Ltrim(@PostCode)) ELSE  Rtrim(Ltrim(@PostCode)) END 
								  and Suburb = Rtrim(Ltrim(@Suburb))),'')

end


GO
