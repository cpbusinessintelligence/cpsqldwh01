SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen
-- Create date: 2021-03-08
-- Description:	Get ETAZone From PricingZone
-- =============================================
CREATE FUNCTION [dbo].[fn_GetETAZoneFromPricingzone] (@PricingZone VARCHAR(10))
RETURNS VARCHAR(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @OutputETAZone AS VARCHAR(10)
	
	SELECT @OutputETAZone = 
		CASE @PricingZone 
			WHEN 'ABX' THEN 'ABXR' 
			WHEN 'ADL' THEN 'ADLM'
			WHEN 'BAL' THEN 'BALR'
			WHEN 'BEN' THEN 'BENR'
			WHEN 'BNE' THEN 'BNEM'
			WHEN 'CBR' THEN 'CBRR'	
			WHEN 'CFS' THEN 'CFSR'
			WHEN 'CNS' THEN 'CNSR'
			WHEN 'GOS' THEN 'GOSR'
			WHEN 'LSY' THEN 'LSYR'
			WHEN 'MEL' THEN 'MELM'
			WHEN 'NTL' THEN 'NTLR'
			WHEN 'OOL' THEN 'OOLM'
			WHEN 'PER' THEN 'PERM'
			WHEN 'SYD' THEN 'SYDM'
			WHEN 'WOL' THEN 'WOLR'
			ELSE ''		
		END
	FROM PerformanceReporting.[dbo].[DimContractor]
		
	RETURN @OutputETAZone

END
GO
