SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_GetNameFormat](@Name varchar(50)) returns varchar(800) as
begin

		Return ISNULL( (SUBSTRING (@Name ,1 , 3 ) + '_' +SUBSTRING (@Name ,4 , 3 )),'')

end



GO
