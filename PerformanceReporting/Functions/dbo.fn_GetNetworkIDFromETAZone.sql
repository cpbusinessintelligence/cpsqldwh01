SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_GetNetworkIDFromETAZone] (@sFromZone varchar(20), @sToZone varchar(20),@ProductCategory varchar(20))
returns integer
as
begin
   Declare @OutputID as integer = 0
   IF @sFromZone <> '' and @sToZone <> ''
        Select @OutputID =  ISNULL((Select n.NetworkCategoryId From [dbo].[ETACalculator] e join [dbo].[NetworkCategory] n on n.[NetworkCategory]=e.PrimaryNetworkCategory and n.[SecondaryNetworkCategory]=e.SecondaryNetworkCategory
                                                          Where [FromZone] = @sFromZone and [ToZone] = @sToZone and Category=@ProductCategory),0)

 
    Return @OutputID
end
GO
