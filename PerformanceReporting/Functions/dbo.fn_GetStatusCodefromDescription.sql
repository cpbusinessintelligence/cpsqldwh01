SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create function [dbo].[fn_GetStatusCodefromDescription] (@sDescription varchar(200))
returns varchar(50)
as
begin
	
	Return Isnull((SELECT Id
        FROM [dbo].[DimStatus] where [StatusDescription] = @sDescription),'')
end

GO
