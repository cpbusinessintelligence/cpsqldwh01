SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyDepotAgentPerformance] (@Date Date, @Depo varchar(20))	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
		
SELECT 
   isnull(state,'Unknown') as State,		
	DeliveryContractorName	,
	OnTimeStatusId,
    count(PrimaryLabelsID) as PerformanceCount
	,Convert(decimal(12,2),0) as TargetKPI 
	into #temp1
    FROM [PerformanceReporting].[dbo].[PrimaryLabels] (NOLOCK) l 
    left join [PerformanceReporting].[dbo].[DimContractor] (NOLOCK) c on l.[DeliveryScannedBy] = c.DriverID
    where PerformanceProcessed=1
    and DeliveryContractorType = 'Agent'
    and PerformanceReportingDate = CONVERT(VARCHAR(10),@Date,111)  
    and (DeliveryDateTime is not  NULL or AttemptedDeliveryDateTime is not null)
    and OnTimeStatusId in ( 6, 7)
	and  c.DepotName= @Depo
    group by  State,l.DeliveryContractorName,DeliveryContractorName,OnTimeStatusId	

	Select State, DeliveryContractorName, OnTimeStatusID, convert(varchar(50),'') as Descr,SUM(PerformanceCount) as PerformanceCount,Convert(int,0) as Total,
	Convert(decimal(12,2),0)  as PerformanceKPI,  
	Convert(decimal(12,2),0) as TargetKPI   into #Temp  from #temp1
	   Group by State,  DeliveryContractorName, OnTimeStatusID

	update #temp set DeliveryContractorName=Substring(DeliveryContractorName,  CHARINDEX( ' - ', DeliveryContractorName, 1)+3,len(DeliveryContractorName))

	update #temp set Descr=s.Description  from dbo.DimStatus s join #temp t on s.id= t.ontimestatusId

    Update #Temp Set Total = (Select SUm(PerformanceCount) from #Temp T Where T.DeliveryContractorName = #Temp.DeliveryContractorName)

    Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),Convert(decimal(12,2),PerformanceCount)/Convert(Decimal(12,2),Total)*100) WHere Total >0

	 Update #Temp SET TargetKPI =K.Target *100 From #Temp T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
     Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
		
    select * from #temp where DeliveryContractorName is not null order by DeliveryContractorName,State
	
END




GO
