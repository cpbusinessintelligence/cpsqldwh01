SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<State No TIme Overview>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyDepotByStateNoTimeOverview] (@Depo varchar(20),@Date Date )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	Set @Depo = Case When @depo = 'TULLUMARINE' Then 'TULLAMARINE' Else @Depo End
   SELECT Isnull(D.DepotName,'Unknown') as DepotName
          , SUM([NoTimeCount]) as NoTimeCount		  
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)   and D.Depotname = @Depo
  GROUP By  Isnull(D.DepotName,'Unknown')

  Select * from #Temp1

END

GO
