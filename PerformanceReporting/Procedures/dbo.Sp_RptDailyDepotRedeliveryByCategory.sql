SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyDepotRedeliveryByCategory]
	(@Date Date, @Depo varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select  isnull(state,'Unknown') as State,
	    case when ( isnull(FailedDeliveryCardNumber,'') like 'NHCLC%' or isnull(FailedDeliveryCardNumber,'') like '%RTCNA' or isnull(FailedDeliveryCardNumber,'') like 'NHCLC%') then 'Hubbed'
	                 when isnull(FailedDeliveryCardNumber,'') like '%SLCNA' then 'POPStation'
		             when  ( isnull(FailedDeliveryCardNumber,'') like '191%' or isnull(FailedDeliveryCardNumber,'') like '%DPCNA')  then 'Depot' else '' end as Category,
  count(*) as  Count
 
  into #temp
 from DeliveryOptions d left join DimContractor c on d.DeliveryContractor=c.DriverID
  where convert(date,scanDatetime)=@Date and c.DepotName= @Depo
  and Category like '%Redelivery%'
  group by isnull(state,'Unknown'),
  case when ( isnull(FailedDeliveryCardNumber,'') like 'NHCLC%' or isnull(FailedDeliveryCardNumber,'') like '%RTCNA' or isnull(FailedDeliveryCardNumber,'') like 'NHCLC%') then 'Hubbed'
	                         when isnull(FailedDeliveryCardNumber,'') like '%SLCNA' then 'POPStation'
			                 when  ( isnull(FailedDeliveryCardNumber,'') like '191%' or isnull(FailedDeliveryCardNumber,'') like '%DPCNA')  then 'Depot' else '' end
    
  Create table #temp1(CardCategory varchar(100),PerformanceCount int default 0)
  Insert into #temp1 values('Hubbed',0)
    Insert into #temp1 values('POPStation',0)
	  Insert into #temp1 values('Depot',0)

update #temp1 set PerformanceCount=Count from #temp where #temp.Category=#temp1.CardCategory

  select * from #temp1 order by PerformanceCount desc

  

END


GO
