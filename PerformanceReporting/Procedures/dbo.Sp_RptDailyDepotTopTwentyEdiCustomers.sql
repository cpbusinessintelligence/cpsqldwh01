SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyDepotTopTwentyEdiCustomers] 
	(@Date Date, @Depo Varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	
 
 select  
    [RevenueType],Convert(varchar(20),'') as AccountCode , Shortname,    
	OnTimeStatus,   
	convert(varchar(50),'') as Descr,
	count(p.PerformanceCount) as PerformanceCount,
	Convert(int,0) as Total,
	Convert(decimal(12,2),0)  as PerformanceKPI,
	Convert(decimal(12,2),0) as TargetKPI  
	into #temp1
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  (NOLOCK)   
	JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id  
	left join   pronto.[dbo].[ProntoDebtor] d on p.accountCode = d.accountCode
		 
	where C.Date = CONVERT(VARCHAR(10),@Date,111)
	and RevenueType= 'EDI'
	and OnTimeStatus in (1,2)		
	group by [RevenueType],OnTimeStatus,Shortname
	
	update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.OnTimeStatus

	Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 t Where t.Shortname = #Temp1.Shortname)

	Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

	Update #Temp1 SET TargetKPI = 100

	Select top 20 * INto #TempFinal from #Temp1 WHere OnTimeStatus = 1 and PerformanceCOunt>20 and ShortName is not null Order by PerformanceKPI asc
  
	Select * from #TempFinal 
	Union all
	Select * from #Temp1 Where OnTimeStatus = 2 and Shortname in (Select distinct Shortname From #TempFinal)
	order by AccountCode asc

END


select * from DailyPerformanceReporting
GO
