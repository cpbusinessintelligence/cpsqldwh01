SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Sp_RptDailyNationalAgentPerformance 
	(@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

   SELECT   Isnull(state,'Unknown')  as state,
         state + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4) as DeliveryDriver
       -- Isnull(DeliveryDriver,'Unknown') as DeliveryDriver
       ,[OnTimeStatus]
	    ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = @Date  and [OnTimeStatus] in (1,2)
  and ContractorType in ('C','V','F','S')
  Group by state + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4),[OnTimeStatus], Isnull(state,'Unknown') 

  Update #Temp1 SET [Descr] =S.Description From #Temp1 T join [PerformanceReporting].dbo.DimStatus S on T.OnTimeStatus=S.id

  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DeliveryDriver = #Temp1.DeliveryDriver)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'

  Select top 20 * INto #TempFinal from #Temp1 WHere OnTimeStatus = 1 and PerformanceCOunt>25  Order by PerformanceKPI desc,PerformanceCount desc
 
  Select DeliveryDriver,[OnTimeStatus],[Descr],[PerformanceCount],Total,PerformanceKPI,TargetKPI
  from
  (
    Select * from #TempFinal 
    Union all
    Select * from #Temp1 Where OnTimeStatus = 2 and DeliveryDriver in (Select distinct DeliveryDriver From #TempFinal) 
  ) results
  order by OnTimeStatus asc, performanceKPI desc ,[PerformanceCount] desc
END
GO
