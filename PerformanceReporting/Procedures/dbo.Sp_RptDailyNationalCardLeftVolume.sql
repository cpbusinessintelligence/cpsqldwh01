SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyNationalCardLeftVolume] (@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

  select 
  case CardCategory when 'Redelivery to Hubbed' then 'Hubbed' 
                    when 'Redelivery to POPStation' then 'POPStation' 
	                when 'Standard Redelivery' then 'Depot' end as CardCategory,
  sum(case when  CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Standard Redelivery') or CardCategory is null  then 1 else 0 end) as  TotalRedeliveries  
  ,1 as Series 
  from [PerformanceReporting].[dbo].[PrimaryLabels]  (NOLOCK) l 
  where AttemptedDeliveryScannedBy is not null 
  and PerformanceProcessed =1
  and PerformanceReportingDate =CONVERT(VARCHAR(10),@Date,111) 
  and CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Standard Redelivery')
  and OnTimeStatusId in (1,2)
  group by CardCategory

END
GO
