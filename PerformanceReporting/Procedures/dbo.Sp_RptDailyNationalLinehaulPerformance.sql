SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <15/02/2017>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyNationalLinehaulPerformance] (@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

 select distinct isnull(SUBSTRING(PickupETAZone,1,3)+'_'+substring(DeliveryETAZone,1,3),'Unknown') as Line
 into #temp1 from [PerformanceReporting].[dbo].LinehaulData
    
SELECT ProductType
        ,isnull(SUBSTRING(PickupETAZone,1,3)+'_'+substring(DeliveryETAZone,1,3),'Unknown') as Line
       -- ,OnTimeStatusId	 
		,CASE OnTimeStatusId WHEN 10 THEN 'On Time' WHEN 11 THEN 'Not On Time'  END as OnTimeStatus 
        ,count(ConsolidatedBarcode) as [PerformanceCount]
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
		,Convert(int,0) as Total	
  into #temp
  FROM [PerformanceReporting].[dbo].LinehaulData P (NOLOCK) 
  left join [PerformanceReporting].[dbo].DimContractor c (NOLOCK) on p.ConsolidateScannedBy = c.DriverID
  where OnTimeStatusId in (10,11) 
  and convert(date,DeConsolidateDateTime,101) = convert(Date,@Date,101)
  group by ProductType ,OnTimeStatusId,isnull(SUBSTRING(PickupETAZone,1,3)+'_'+substring(DeliveryETAZone,1,3),'Unknown')
  
  Update #Temp Set Total = (Select SUm([PerformanceCount]) from #Temp T2 Where T2.Line = #Temp.Line and T2.productType = #temp.productType)
  Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  
  Update #Temp SET TargetKPI =100;

  select ProductType,Line,OnTimeStatus, [PerformanceCount],PerformanceKPI,TargetKPI from #temp  
  order by producttype asc,line asc
 
END



GO
