SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Sp_RptDailyNationalRedeliveryChoicesDetails (@Date Date)
AS
BEGIN
	SET NOCOUNT ON;
	SET FMTONLY OFF;

  select State,     
  case CardCategory when 'Redelivery to Hubbed' then 'Hubbed' 
                    when 'Redelivery to POPStation' then 'POPStation' 
	                when 'Redelivery On a Date' then 'DifferentDate' end as CardCategory,
 
  sum(case when  CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Redelivery On a Date')   then 1 else 0 end) as  TotalRedeliveries
  into #temp
  from [PerformanceReporting].[dbo].[PrimaryLabels]  (NOLOCK) l left join [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on l.[AttemptedDeliveryScannedBy] = c.DriverID
  where AttemptedDeliveryScannedBy is not null 
  and PerformanceProcessed =1
  and PerformanceReportingDate =@Date  
  and CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Redelivery On a Date')
  and OnTimeStatusId in (1,2)
  group by State,CardCategory
  
  select * from #temp where state is not null
END
GO
