SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyNationalVolByCardCategory] (@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

  select 
  case CardCategory when 'Redelivery to Hubbed' then 'Hubbed' 
                    when 'Redelivery to POPStation' then 'POPStation' 
	                when 'Redelivery On a Date' then 'Depot' 
					when 'Standard Redelivery' then 'Depot' 
					end as CardCategory,
  count(*) 
 -- sum(case when  CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Redelivery On a Date','Standard Redelivery')  then 1 else 0 end) as  TotalRedeliveries  
  ,1 as Series 
  from [PerformanceReporting].[dbo].[PrimaryLabels]  (NOLOCK) l 
  where
   PerformanceProcessed =1
  and PerformanceReportingDate =CONVERT(VARCHAR(10),@Date,111) 
  and CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Redelivery On a Date','Standard Redelivery')
  --and OnTimeStatusId in (1,2)
  group by  case CardCategory when 'Redelivery to Hubbed' then 'Hubbed' 
                    when 'Redelivery to POPStation' then 'POPStation' 
	                when 'Redelivery On a Date' then 'Depot' 
					when 'Standard Redelivery' then 'Depot' 
					end

END
GO
