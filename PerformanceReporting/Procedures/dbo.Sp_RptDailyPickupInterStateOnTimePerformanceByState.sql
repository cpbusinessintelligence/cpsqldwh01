SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyPickupInterStateOnTimePerformanceByState] (@State Varchar(20),@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	SELECT isnull(N.[NetworkCategory],'Unknown') as [NetworkCategory]
        , State
		,isnull(d.DepotName,'Unknown') as DepoName
		,Convert(int,0) as SortOrder
	 --   ,CASE [OnTimeStatus] WHEN 'CY' THEN 'Y' WHEN 'CN' THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]		--  Include Y and N
	    ,[OnTimeStatus]	
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK)   
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[NetworkCategory] N (NOLOCK) on P.[NetworkCategoryID] = N.NetworkCategoryID
  left join [PerformanceReporting].[dbo].DimContractor (NOLOCK) d   on p.PickupDriver= d.DriverID                                              
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111) 
  and [OnTimeStatus] in (1,2) 
  --[OnTimeStatus] Not in ('A','AE','NE','X','E','CY','CN')      
  and N.NetworkCategory = 'Interstate'
  and d.state= @state
  Group by isnull(N.[NetworkCategory],'Unknown'),State,[OnTimeStatus],isnull(d.DepotName,'Unknown')
  
  update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.[OnTimeStatus]

  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DepoName = #Temp1.DepoName)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date)  = K.[MonthKey] and K.type ='ON-TIME'
     
  Update #Temp1 SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 

  Select * from #Temp1 order by [OnTimeStatus] asc, DepoName asc
   
END




GO
