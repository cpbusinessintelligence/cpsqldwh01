SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyRedeliveryOnADateByState]
	(@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;


Select C.State,
		Convert(int,0) as SortOrder,
       Case when isnull(convert(date,AttemptedDeliverydatetime),convert(date,Deliverydatetime))=@Date then 'OnTime' else 'Not OnTime' end as StatusDescr,
	   count(*) as Count

into #temp
from Primarylabels l left join  [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on isnull(l.[AttemptedDeliveryScannedBy],l.deliveryscannedby) = c.DriverID  
 where DelayofDeliverydate is not null 
and convert(date,DelayofDeliverydate)=@Date
group by  C.State,
       Case when isnull(convert(date,AttemptedDeliverydatetime),convert(date,Deliverydatetime))=@Date then 'OnTime' else 'Not OnTime' end
	   	 
 --select 
 -- Category,  ----  include all status
 -- OnTimeStatusId,
 -- Convert(Varchar(50),'') as [Descr],
 -- count(d.Labelnumber) as  PerformanceCount

 -- into #temp
 -- from [PerformanceReporting].[dbo].DeliveryOptions (NOLOCK) d left join [PerformanceReporting].[dbo].[PrimaryLabels] (NOLOCK) l on d.Labelnumber = l.LabelNumber
 
 -- where AttemptedDeliveryDateTime is not null  
 -- and PerformanceProcessed =1
 -- and convert(date,d.ETADate,101) = CONVERT(VARCHAR(10),@Date,111)
 -- and DeliveryDateTime is not null  
 -- and Category='Redelivery On a Date'
 -- group by Category,OnTimeStatusId
  
 -- update #temp set Descr=s.Description  from dbo.DimStatus s join #temp t on s.id= t.ontimestatusId

 IF 1 > (SELECT count(*) as total FROM  #Temp where State ='NSW' ) 
    insert into #temp (state,StatusDescr,Count) values('NSW','OnTime',0)

IF 1 > (SELECT count(*) as total FROM  #Temp where State ='QLD' ) 
    insert into #temp (state,StatusDescr,Count) values('QLD','OnTime',0)
IF 1 > (SELECT count(*) as total FROM  #Temp where State ='VIC' ) 
    insert into #temp (state,StatusDescr,Count) values('VIC','OnTime',0)
IF 1 > (SELECT count(*) as total FROM  #Temp where State ='WA' ) 
    insert into #temp (state,StatusDescr,Count) values('WA','OnTime',0)
IF 1 > (SELECT count(*) as total FROM  #Temp where State ='SA' ) 
    insert into #temp (state,StatusDescr,Count) values('SA','OnTime',0)

    Update #Temp SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 
  select * from #temp where state is not null order by StatusDescr desc,SortOrder asc
  

END
GO
