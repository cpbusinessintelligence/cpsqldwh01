SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyRedeliveryOnDeliveryChoicesByState]
	(@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

  select case when ( isnull(FailedDeliveryCardNumber,'') like 'NHCLC%' or isnull(FailedDeliveryCardNumber,'') like '%RTCNA' or isnull(FailedDeliveryCardNumber,'') like 'NHCLC%') then 'Hubbed'
	                         when isnull(FailedDeliveryCardNumber,'') like '%SLCNA' then 'POPStation'
			                 when  ( isnull(FailedDeliveryCardNumber,'') like '191%' or isnull(FailedDeliveryCardNumber,'') like '%DPCNA')  then 'Depot' else '' end as Category,
  count(*) as  Count
 
  into #temp
 from DeliveryOptions 
  where convert(date,scanDatetime)=@Date
  and Category like '%Redelivery%'
  group by 
  case when ( isnull(FailedDeliveryCardNumber,'') like 'NHCLC%' or isnull(FailedDeliveryCardNumber,'') like '%RTCNA' or isnull(FailedDeliveryCardNumber,'') like 'NHCLC%') then 'Hubbed'
	                         when isnull(FailedDeliveryCardNumber,'') like '%SLCNA' then 'POPStation'
			                 when  ( isnull(FailedDeliveryCardNumber,'') like '191%' or isnull(FailedDeliveryCardNumber,'') like '%DPCNA')  then 'Depot' else '' end
  
  
  --[PerformanceReporting].[dbo].[PrimaryLabels]  (NOLOCK) l 
  --where AttemptedDeliveryScannedBy is not null 
  --and PerformanceProcessed =1
  --and PerformanceReportingDate =CONVERT(VARCHAR(10),@Date,111)  
  --and CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Redelivery On a Date')
  --and OnTimeStatusId in (1,2)
  --group by CardCategory,OnTimeStatusId

  --update #temp set [Desc]=s.Description  from dbo.DimStatus s join #temp t on s.id= t.OnTimeStatusId
  
  Create table #temp1(CardCategory varchar(100),PerformanceCount int default 0)
  Insert into #temp1 values('Hubbed',0)
    Insert into #temp1 values('POPStation',0)
	  Insert into #temp1 values('Depot',0)

update #temp1 set PerformanceCount=Count from #temp where #temp.Category=#temp1.CardCategory

  select * from #temp1 order by PerformanceCount desc

END
GO
