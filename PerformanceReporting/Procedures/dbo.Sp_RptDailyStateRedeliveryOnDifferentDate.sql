SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyStateRedeliveryOnDifferentDate]
	(@Date Date, @State varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	
Select C.State,
        Convert(int,0) as SortOrder,
       Case when isnull(convert(date,AttemptedDeliverydatetime),convert(date,Deliverydatetime))=@Date then 'OnTime' else 'Not OnTime' end as StatusDescr,
	   count(*) as Count

into #temp
from Primarylabels l left join  [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on isnull(l.[AttemptedDeliveryScannedBy],l.deliveryscannedby) = c.DriverID  
 where DelayofDeliverydate is not null 
and convert(date,DelayofDeliverydate)=CONVERT(VARCHAR(10),@Date,111)
and state=@State
group by  C.State, Case when isnull(convert(date,AttemptedDeliverydatetime),convert(date,Deliverydatetime))=@Date then 'OnTime' else 'Not OnTime' end  

IF 1 > (SELECT count(*) as total FROM  #Temp where State ='NSW' ) 
    insert into #temp (state,StatusDescr,Count) values('NSW','OnTime',0)

IF 1 > (SELECT count(*) as total FROM  #Temp where State ='QLD' ) 
    insert into #temp (state,StatusDescr,Count) values('QLD','OnTime',0)
IF 1 > (SELECT count(*) as total FROM  #Temp where State ='VIC' ) 
    insert into #temp (state,StatusDescr,Count) values('VIC','OnTime',0)
IF 1 > (SELECT count(*) as total FROM  #Temp where State ='WA' ) 
    insert into #temp (state,StatusDescr,Count) values('WA','OnTime',0)
IF 1 > (SELECT count(*) as total FROM  #Temp where State ='SA' ) 
    insert into #temp (state,StatusDescr,Count) values('SA','OnTime',0)

 Update #Temp SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 

select * from #temp order by SortOrder asc
  

END

GO
