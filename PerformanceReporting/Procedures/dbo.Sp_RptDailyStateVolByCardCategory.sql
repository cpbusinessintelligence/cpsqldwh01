SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyStateVolByCardCategory] (@Date Date, @State varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

 select 
  case CardCategory when 'Redelivery to Hubbed' then 'Hubbed' 
                    when 'Redelivery to POPStation' then 'POPStation' 
	               -- when 'Redelivery On a Date' then 'Depot' 
					when 'Standard Redelivery' then 'Depot' 
					end as CardCategory,
  count(*)  as PerformanceCount
  ,1 as Series 
  from [PerformanceReporting].[dbo].[PrimaryLabels]  (NOLOCK) l  Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on l.DeliveryScannedBy=D.DriverID    
  where
   PerformanceProcessed =1
  and PerformanceReportingDate =CONVERT(VARCHAR(10),@Date,111) 
  and CardCategory in ('Redelivery to Hubbed','Redelivery to POPStation','Standard Redelivery')  
  and state= @state
  group by  case CardCategory when 'Redelivery to Hubbed' then 'Hubbed' 
                    when 'Redelivery to POPStation' then 'POPStation' 
	               -- when 'Redelivery On a Date' then 'Depot' 
					when 'Standard Redelivery' then 'Depot' 
					end



END
GO
