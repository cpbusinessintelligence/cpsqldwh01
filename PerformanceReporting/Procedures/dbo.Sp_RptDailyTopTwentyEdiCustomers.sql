SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptDailyTopTwentyEdiCustomers] 
	(@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	--declare @Date Date
	--set @Date='2017-02-09'

	Select accountcode,sum(Performancecount) as Count 
	into #temp
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id  
	where isnull(accountcode,'')<>''
	and C.Date = CONVERT(VARCHAR(10),@Date,111)
	and RevenueType= 'EDI'
	and OnTimeStatus in (1,2)
	group by accountcode


	--select * from #temp order by count desc
	
 select   Isnull(r.State,'Unknown') as State,[RevenueType],
       -- p.Accountcode,
       --Convert(varchar(20),'') as AccountCode , 
	   shortname as Shortname,    
	OnTimeStatus,   ---  Include Y and N
	convert(varchar(50),'') as Descr,
	sum(p.PerformanceCount) as PerformanceCount,
	Convert(int,0) as Total,
	Convert(decimal(12,2),0)  as PerformanceKPI,
	Convert(decimal(12,2),0) as TargetKPI  
	into #temp1
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  (NOLOCK) 
	JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id  
	left join   pronto.[dbo].[ProntoDebtor] d on p.accountCode = d.accountCode
	 LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] r (NOLOCK) on P.[DeliveryDriver] = r.DriverID  
	where p.accountcode in (select top 20 accountcode from #temp order by count desc)
 and C.Date = CONVERT(VARCHAR(10),@Date,111)
	and RevenueType= 'EDI'
	and OnTimeStatus in (1,2)
	group by [RevenueType],Isnull(r.State,'Unknown'),
	--p.Accountcode,
	Shortname, OnTimeStatus

		--update #temp set Shortname=s.shortname  from  pronto.[dbo].[ProntoDebtor] s join #temp t on s.billto=t.billto
	
	update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.OnTimeStatus

	Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 t Where t.Shortname = #Temp1.Shortname and t.State=#temp1.State)

	Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

	Update #Temp1 SET TargetKPI = 100

	--Select top 20 * INto #TempFinal from #Temp WHere OnTimeStatus = 1 and PerformanceCOunt>20 and ShortName is not null Order by PerformanceKPI asc
  
	Select * from #Temp1   order by Shortname,Performancecount desc
	--Union all
	--Select * from #Temp Where OnTimeStatus = 2 and Shortname in (Select distinct Shortname From #TempFinal)
	--order by billto asc
END

GO
