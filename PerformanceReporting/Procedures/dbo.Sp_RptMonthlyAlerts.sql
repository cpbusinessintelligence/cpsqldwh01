SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <11th Jan, 2017>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Sp_RptMonthlyAlerts] (@Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select Date  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

    select Description, Impact from DimAlerts where convert(date,Date) in (select * from #temp)
END
GO
