SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<State No TIme Overview>
-- =============================================
create PROCEDURE [dbo].[Sp_RptMonthlyDepotByStateNoTimeOverview] (@Depo varchar(20), @Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

   SELECT Isnull(D.DepotName,'Unknown') as DepotName
          , SUM([NoTimeCount]) as NoTimeCount		  
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere NoTimeReportingDayID in (select * from #temp)   
  and D.Depotname = @Depo
  GROUP By  Isnull(D.DepotName,'Unknown')

  Select * from #Temp1

END

GO
