SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Sp_RptMonthlyOnTimeDeliveryChoiceReportFirstTime] 
	(@Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

  select Date  into #temp1 from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

  select  State
  ,Convert(int,0) as SortOrder,
  case CardCategory when '1st Time Delivery to POPStation' then 'POPStation' when '1st Time Delivery to HUBBED' then 'HUBBED' end as CardCategory,    
  OnTimeStatusId,
   Convert(Varchar(50),'') as DisplayDescr,
  sum(case when  CardCategory in ('1st Time Delivery to POPStation','1st Time Delivery to HUBBED')   then 1 else 0 end) as  PerformanceCount ,
  Convert(int,0) as Total,
  Convert(decimal(12,2),0) as TargetKPI 
  ,Convert(decimal(12,2),0)  as PerformanceKPI   	
  into #temp
  from [PerformanceReporting].[dbo].[PrimaryLabels] l left join [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on isnull(l.AttemptedDeliveryScannedBy,l.[DeliveryScannedBy]) = c.DriverID
  where  PerformanceProcessed =1 and OnTimeStatusId in (1,2)
  and PerformanceReportingDate in (select * from #temp1)
  and CardCategory in ('1st Time Delivery to POPStation','1st Time Delivery to HUBBED')
  group by State,case CardCategory when '1st Time Delivery to POPStation' then 'POPStation' when '1st Time Delivery to HUBBED' then 'HUBBED' end,OnTimeStatusId,case when OnTimeStatusId=1 then 'OnTime' else 'Not OnTime' end

  IF 1 > (SELECT count(*) as total FROM  #temp where state ='NSW' and  OnTimeStatusId=1) 
    insert into #temp (State,OnTimeStatusId,SortOrder) select * from statesortorder where state='NSW' and OnTimeStatusId=1

  IF 1 > (SELECT count(*) as total FROM  #temp where state ='QLD' and  OnTimeStatusId=1) 
    insert into #temp (State,OnTimeStatusId,SortOrder) select * from statesortorder where state='QLD' and OnTimeStatusId=1

  IF 1 > (SELECT count(*) as total FROM  #temp where state ='VIC' and  OnTimeStatusId=1) 
    insert into #temp (State,OnTimeStatusId,SortOrder) select * from statesortorder where state='VIC' and OnTimeStatusId=1

  IF 1 > (SELECT count(*) as total FROM  #temp where state ='WA' and  OnTimeStatusId=1) 
    insert into #temp (State,OnTimeStatusId,SortOrder) select * from statesortorder where state='WA' and OnTimeStatusId=1

 IF 1 > (SELECT count(*) as total FROM  #temp where state ='SA' and  OnTimeStatusId=1) 
    insert into #temp (State,OnTimeStatusId,SortOrder) select * from statesortorder where state='SA' and OnTimeStatusId=1

  Update #Temp SET DisplayDescr =S.Description From #Temp T join [PerformanceReporting].dbo.DimStatus S on T.OnTimeStatusId=S.id

  Update #Temp Set Total = (Select SUm([PerformanceCount]) from #Temp T2 Where T2.State = #Temp.State )

  Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0

  Update #Temp SET TargetKPI =K.Target *100 From #Temp T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where CAST(@Year AS varchar(4))+CAST(@month AS varchar(2)) = K.[MonthKey] and K.type ='ON-TIME'   
  
  update #temp set SortOrder = s.sortOrder    from StateSortOrder s where s.state= #temp.State  

 select * from #temp where OnTimeStatusId=1 order by SortOrder asc
	
END
GO
