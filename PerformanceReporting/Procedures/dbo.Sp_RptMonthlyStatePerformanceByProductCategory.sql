SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Sp_RptMonthlyStatePerformanceByProductCategory] (@State varchar(20),@Year int, @month int)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
		
    select id  into #temp1 from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

    select  Isnull(D.State,'Unknown') as State, p.ProductSubCategory,   
    OnTimeStatus,  ---  Include Y and N
	convert(varchar(50),'') as Descr,
    count(PerformanceCount) as PerformanceCount,
    Convert(int,0) as Total,
    Convert(decimal(12,2),0)  as PerformanceKPI,
    Convert(decimal(12,0),0) as TargetKPI  
    into #temp
	
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  (NOLOCK) 
	Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID   
	  	 
	where PerformanceDayID in (select * from #temp1)
 	and OnTimeStatus in (1,2)
	and state=@state
	group by p.ProductSubCategory,OnTimeStatus, Isnull(D.State,'Unknown') 
			
	update #temp set Descr=s.Description  from dbo.DimStatus s join #temp t on s.id= t.OnTimeStatus

    Update #Temp Set Total = (Select SUm([PerformanceCount]) from #Temp t Where t.ProductSubCategory = #Temp.ProductSubCategory)

    Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

	update #Temp set TargetKPI=100

	select * from #temp order by OnTimeStatus asc

END
GO
