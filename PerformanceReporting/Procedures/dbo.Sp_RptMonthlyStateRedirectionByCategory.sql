SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptMonthlyStateRedirectionByCategory] ( @State varchar(20),@Year int, @month int)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

select Date  into #temp1 from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month
	 
  select DeliveryMethod ,
	labelNumber,
	count(*) as PerformanceCount
	into #temp2
	from [PerformanceReporting].[dbo].DeliveryOptions (NOLOCK) d 
	where Category='Redirection'
	group by DeliveryMethod,labelNumber
   
select   d.DeliveryMethod as CardCategory,
         CASE  WHEN OnTimeStatusId=1 THEN 'OnTime' else 'Not OnTime' END as [Descr],   --- include all status
         count(*) as PerformanceCount 
		 into #temp
  from #temp2 d 
  left join [PerformanceReporting].[dbo].[primaryLabels] (NOLOCK) l on d.labelNumber = l.LabelNumber
  left join  [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on isnull(l.[AttemptedDeliveryScannedBy],l.deliveryscannedby) = c.DriverID 
  where PerformanceProcessed =1
  and PerformanceReportingDate in (select * from #temp1)  
  and c.State= @State 
  group by d.DeliveryMethod ,
            CASE  WHEN OnTimeStatusId=1 THEN 'OnTime' else 'Not OnTime' END
			
  select * from #temp order by [Descr] desc

   
END






GO
