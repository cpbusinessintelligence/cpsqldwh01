SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  PROCEDURE [dbo].[Sp_RptMonthlyStateTopTwentyEdiCustomers] 
	( @State Varchar(20),@Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id  into #temp2 from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month
 
 select  case Territory WHEN  'CAD' THEN 'SA' 
                   when 'COO' then 'QLD'
	               when 'CBN'  THEN 'QLD'
				   when 'CCB' THEN 'NSW'
				   when 'CCC' THEN 'NSW'	
				   when 'CSY' THEN 'NSW'
				   when 'CSC' THEN 'NSW'						   		   
				   when 'CME' THEN 'VIC'
				   WHEN  'CPE' THEN 'WA'
				   else 'UNKNOWN' end as state, 
				  
    [RevenueType],Convert(varchar(20),'') as AccountCode , Shortname,    
	OnTimeStatus,   
	convert(varchar(50),'') as Descr,
	count(p.PerformanceCount) as PerformanceCount,
	Convert(int,0) as Total,
	Convert(decimal(12,2),0)  as PerformanceKPI,
	Convert(decimal(12,2),0) as TargetKPI  
	into #temp
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  (NOLOCK) 
	JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id  
	left join   pronto.[dbo].[ProntoDebtor] d on p.accountCode = d.accountCode
		 
	where PerformanceDayID in (select * from #temp2)
	and RevenueType= 'EDI'
	and OnTimeStatus in (1,2)	
	group by [RevenueType],Shortname, OnTimeStatus,case Territory WHEN  'CAD' THEN 'SA' 
                   when 'COO' then 'QLD'
	               when 'CBN'  THEN 'QLD'
				   when 'CCB' THEN 'NSW'
				   when 'CCC' THEN 'NSW'	
				   when 'CSY' THEN 'NSW'
				   when 'CSC' THEN 'NSW'						   		   
				   when 'CME' THEN 'VIC'
				   WHEN  'CPE' THEN 'WA'
				   else 'UNKNOWN' end
    select * into #temp1 from #temp where state= @state
	
	update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.OnTimeStatus

	Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 t Where t.Shortname = #Temp1.Shortname)

	Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

	Update #Temp1 SET TargetKPI = 100

	Select top 20 * INto #TempFinal from #Temp1 WHere OnTimeStatus = 1 and PerformanceCOunt>20 and ShortName is not null Order by PerformanceKPI asc
  
	Select * from #TempFinal 
	Union all
	Select * from #Temp1 Where OnTimeStatus = 2 and Shortname in (Select distinct Shortname From #TempFinal)
	order by AccountCode asc

END
GO
