SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptMonthlyTopTwentyEdiCustomers] 
	(@Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id into #temp2 from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month
		
	Select accountcode,sum(Performancecount) as Count 
	into #temp
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  
	where isnull(accountcode,'')<>''
	and PerformanceDayID in (select * from #temp2) 
	and RevenueType= 'EDI'
	and OnTimeStatus in (1,2)
	group by accountcode

 select  Isnull(c.State,'Unknown') as State,[RevenueType],     
	   shortname as Shortname,    
	OnTimeStatus,   ---  Include Y and N
	convert(varchar(50),'') as Descr,
	sum(p.PerformanceCount) as PerformanceCount,
	Convert(int,0) as Total,
	Convert(decimal(12,2),0)  as PerformanceKPI,
	Convert(decimal(12,2),0) as TargetKPI  
	into #temp1
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  (NOLOCK) 
	left join   pronto.[dbo].[ProntoDebtor] d on p.accountCode = d.accountCode	
    LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] c (NOLOCK) on P.[DeliveryDriver] = c.DriverID  
	where p.accountcode in (select top 20 accountcode from #temp order by count desc)   
	and PerformanceDayID in (select * from #temp2)
	and RevenueType= 'EDI'
	and OnTimeStatus in (1,2)
	group by [RevenueType],	Isnull(c.State,'Unknown'),
	Shortname, OnTimeStatus

	update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.OnTimeStatus

	Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 t Where t.Shortname = #Temp1.Shortname and t.State = #temp1.State)

	Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

	Update #Temp1 SET TargetKPI = 100
 
	Select * from #Temp1   order by Shortname,Performancecount desc
	
END

GO
