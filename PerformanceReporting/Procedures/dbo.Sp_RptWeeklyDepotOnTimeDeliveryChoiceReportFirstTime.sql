SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Sp_RptWeeklyDepotOnTimeDeliveryChoiceReportFirstTime] 
	(@Date Date, @Depo varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select Date into #temp1  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
			
 select  State,
  case CardCategory when '1st Time Delivery to POPStation' then 'POPStation' when '1st Time Delivery to HUBBED' then 'HUBBED' end as CardCategory,    
  OnTimeStatusId,
  case when OnTimeStatusId=1 then 'OnTime' else 'Not OnTime' end as DisplayDescr,
  sum(case when  CardCategory in ('1st Time Delivery to POPStation','1st Time Delivery to HUBBED')   then 1 else 0 end) as  PerformanceCount ,
  Convert(int,0) as Total,
  Convert(decimal(12,2),0) as TargetKPI 
  ,Convert(decimal(12,2),0)  as PerformanceKPI   	
  into #temp
  from [PerformanceReporting].[dbo].[PrimaryLabels] l left join [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on isnull(l.AttemptedDeliveryScannedBy,l.[DeliveryScannedBy]) = c.DriverID
  where  PerformanceProcessed =1 and OnTimeStatusId in (1,2)
  and PerformanceReportingDate  in (select * from #temp1)
  and CardCategory in ('1st Time Delivery to POPStation','1st Time Delivery to HUBBED')
  and c.DepotName=@Depo
  group by State,case CardCategory when '1st Time Delivery to POPStation' then 'POPStation' when '1st Time Delivery to HUBBED' then 'HUBBED' end,OnTimeStatusId,case when OnTimeStatusId=1 then 'OnTime' else 'Not OnTime' end

  Update #Temp Set Total = (Select SUm([PerformanceCount]) from #Temp T2 Where T2.State = #Temp.State )

  Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0

  Update #Temp SET TargetKPI =K.Target *100 From #Temp T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'

   IF 1 > (SELECT count(*) as total FROM  #Temp where CardCategory ='POPStation' and  OnTimeStatusId=1) 
    insert into #temp (CardCategory,OnTimeStatusId,DisplayDescr,Total) values('POPStation',1,'OnTime',0)

  IF 1 > (SELECT count(*) as total FROM  #Temp where CardCategory ='HUBBED' and  OnTimeStatusId=1) 
    insert into #temp (CardCategory,OnTimeStatusId,DisplayDescr,Total) values('HUBBED',1,'OnTime',0)

  select * from #temp where OnTimeStatusId=1
    
END

GO
