SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Sp_RptWeeklyPerformanceByProductCategory] (@Date Date)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id into #temp1  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
	
    select ProductSubCategory,
    OnTimeStatus,  ---  Include Y and N
	convert(varchar(50),'') as Descr,
    sum(PerformanceCount) as PerformanceCount,
    Convert(int,0) as Total,
    Convert(decimal(12,2),0)  as PerformanceKPI,
    Convert(decimal(12,0),0) as TargetKPI  
    into #temp
	
	from [PerformanceReporting].[dbo].DailyPerformanceReporting p  (NOLOCK)  
	where PerformanceDayID in (select * from #temp1) 
 	and OnTimeStatus in (1,2)
	group by ProductSubCategory,OnTimeStatus
			
	update #temp set Descr=s.Description  from dbo.DimStatus s join #temp t on s.id= t.OnTimeStatus

    Update #Temp Set Total = (Select SUm([PerformanceCount]) from #Temp t Where t.ProductSubCategory = #Temp.ProductSubCategory)

    Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

	update #Temp set TargetKPI=100

	select * from #temp order by OnTimeStatus asc, PerformanceCount desc

END

GO
