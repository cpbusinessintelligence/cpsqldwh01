SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptWeeklyRedirectionDetails] 
	(@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY  OFF;
   
   select Date into #temp1  from [dbo].[DimCalendarDate]
   where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
   
 select DeliveryMethod ,
	labelNumber,
	count(*) as PerformanceCount
	into #temp2
	from [PerformanceReporting].[dbo].DeliveryOptions (NOLOCK) d 
	where Category='Redirection'
	group by DeliveryMethod,labelNumber
   
select  	C.State,	
           Convert(int,0) as SortOrder,
            d.DeliveryMethod as CardCategory,
            OnTimeStatusId,
         CASE  WHEN OnTimeStatusId=1 THEN 'OnTime' else 'Not OnTime' END as [Descr],   --- include all status
         count(*) as PerformanceCount, 
  convert(decimal(12,2),0) as PerformanceKPI,
  Convert(int,0) as Total,
  Convert(decimal(12,2),0) as TargetKPI

  into #temp
  from #temp2 d 
  left join [PerformanceReporting].[dbo].[primaryLabels] (NOLOCK) l on d.labelNumber = l.LabelNumber
  left join  [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on isnull(l.[AttemptedDeliveryScannedBy],l.deliveryscannedby) = c.DriverID 
  where PerformanceProcessed =1
  and PerformanceReportingDate in (select * from #temp1) 
  and OnTimeStatusId in (1,2)
  group by C.State,	
            d.DeliveryMethod ,
            OnTimeStatusId,
            CASE  WHEN OnTimeStatusId=1 THEN 'OnTime' else 'Not OnTime' END
			  
  Update #Temp Set Total = (Select SUm([PerformanceCount]) from #Temp T2 Where T2.State = #Temp.State and T2.CardCategory= #temp.CardCategory)
  Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*PerformanceCount)/Convert(Decimal(12,2),Total) WHere Total >0
  
  Update #Temp SET TargetKPI =K.Target *100 From #Temp T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
    
  Update #Temp SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 

  select * from #temp where OnTimeStatusId = 1 order by sortorder asc

END
GO
