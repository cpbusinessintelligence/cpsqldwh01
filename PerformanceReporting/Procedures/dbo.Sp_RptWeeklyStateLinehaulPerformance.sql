SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <15/02/2017>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptWeeklyStateLinehaulPerformance] (@Date Date, @State varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id into #temp4  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

  SELECT ProductType
        ,isnull(SUBSTRING(PickupETAZone,1,3)+'_'+substring(DeliveryETAZone,1,3),'Unknown') as Line
        ,OnTimeStatusId	  
        ,count(ConsolidatedBarcode) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI		
  into #temp
  FROM [PerformanceReporting].[dbo].LinehaulData P (NOLOCK) 
  left join [PerformanceReporting].[dbo].DimContractor c (NOLOCK) on p.ConsolidateScannedBy = c.DriverID
  where OnTimeStatusId in (10,11) 
  group by ProductType ,OnTimeStatusId,isnull(SUBSTRING(PickupETAZone,1,3)+'_'+substring(DeliveryETAZone,1,3),'Unknown')

  Update #Temp Set Total = (Select SUm([PerformanceCount]) from #Temp T2 Where T2.Line = #Temp.Line and T2.productType = #temp.productType)

  Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  
      
  select distinct isnull(SUBSTRING(PickupETAZone,1,3)+'_'+substring(DeliveryETAZone,1,3),'Unknown') as Line
  into #temp1 from [PerformanceReporting].[dbo].LinehaulData
  where ProductType='Standard'

  select distinct ProductType 
  into #temp2
  from [PerformanceReporting].[dbo].LinehaulData
		
  select *,Convert(decimal(12,2),0)  as PerformanceKPI,Convert(decimal(12,2),0)  as [PerformanceCount]	 into #temp3 
  from #temp1 , #temp2
    
  update #temp3 set performanceKPI= t.PerformanceKPI from #temp t join #temp3 t1 on t.line=t1.line and t.ProductType=t1.ProductType and OnTimeStatusId=10

   update #temp3 set PerformanceCount= t.PerformanceCount from #temp t join #temp3 t1 on t.line=t1.line and t.ProductType=t1.ProductType 

  select ProductType,Line, PerformanceKPI,PerformanceCount from #temp3 order by producttype asc,line asc
 
END



GO
