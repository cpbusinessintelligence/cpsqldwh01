SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptWeeklyStateRedirectionByCategory] (@Date Date, @State varchar(20))

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select Date into #temp  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

	select DeliveryMethod ,
	labelNumber,
	count(*) as PerformanceCount
	into #temp2
	from [PerformanceReporting].[dbo].DeliveryOptions (NOLOCK) d 
	where Category='Redirection'
	group by DeliveryMethod,labelNumber
   
select   d.DeliveryMethod as CardCategory,
         CASE  WHEN OnTimeStatusId=1 THEN 'OnTime' else 'Not OnTime' END as [Descr],   --- include all status
         count(*) as PerformanceCount 
		 into #temp1
  from #temp2 d 
  left join [PerformanceReporting].[dbo].[primaryLabels] (NOLOCK) l on d.labelNumber = l.LabelNumber
  left join  [PerformanceReporting].[dbo].[DimContractor]  c (NOLOCK) on isnull(l.[AttemptedDeliveryScannedBy],l.deliveryscannedby) = c.DriverID 
  where PerformanceProcessed =1
  and PerformanceReportingDate in (select * from #temp)  
  and c.State= @State 
  group by d.DeliveryMethod ,
            CASE  WHEN OnTimeStatusId=1 THEN 'OnTime' else 'Not OnTime' END

  select * from #temp1 order by [Descr] desc
  
   
END




GO
