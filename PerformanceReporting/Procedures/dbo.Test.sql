SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Test]
	(@Date Date)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;


	select  substring(lastscannedBy,1,3) as State,
  CardCategory,		
  CASE  WHEN OnTimeStatusId=1 THEN 'Y' else 'N' END as [OnTimeStatus],
  sum(case when  (AttemptedDeliveryScannedBy is not null or  DeliveryScannedBy is not null)   then 1 else 0 end) as  PerformanceCount , 
  convert(decimal(12,2),0) as PerformanceKPI,
  Convert(int,0) as Total,
  Convert(decimal(12,2),0) as TargetKPI

  into #temp
  from [PerformanceReporting].[dbo].[primaryLabels] 
  where PerformanceProcessed =1
  and PerformanceReportingDate = @Date
  and (DeliveryScannedBy is not null or AttemptedDeliveryScannedBy is not null)
  and CardCategory in ('Redirected to Alternate Address','Redirected to Authority To Leave','Redirected to POPPoint')
  group by substring(lastscannedBy,1,3),CardCategory,CASE  WHEN OnTimeStatusId=1 THEN 'Y' else 'N' END

  Update #Temp Set Total = (Select SUm(PerformanceCount) from #Temp T2 Where T2.CardCategory = #Temp.CardCategory)
  Update #Temp Set PerformanceKPI  =Convert(decimal(12,2),100*PerformanceCount)/Convert(Decimal(12,2),Total) WHere Total >0

  select * from #temp order by State asc

END
GO
