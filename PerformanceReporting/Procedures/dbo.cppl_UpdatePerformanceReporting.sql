SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[cppl_UpdatePerformanceReporting](@Date date)
as
begin

--Added by PV on 2020-11-09: To avoid duplicate insertion if job fails and retry
exec [sp_DeletePerformancereportingData] @Date
Print '0 Deleted'
---Added by PV on 2020-11-09 Ends here:
exec [sp_LoadConsignmentsandLabels] @Date
print '1 done'

exec [sp_LoadTrackingEvents] @Date,@Date
print '2 done'

exec [sp_CalculateETAandPerformance] @Date,@Date
print '3 done'

exec  [sp_LoadLineHauldata] @Date
print '4 done'

-------------------------------------------------------------------------------------------
-- Step 4.1 will be copying deplicates duplicates table
-- then delete teh duplicate records from primary table before laoding daily summary tables
---Added by RM
-------------------------------------------------------------------------------------------
exec [sp_DeleteDuplicatesfromPrimaryLabel]

Print '4.1 done  Primary Labels Duplicate data deleted'

exec [sp_LoadDailyPerformanceReporting] @Date
print '5 done'
EXEC [sp_LoadDailyNoTimeReporting] @Date
print '6 done'
exec [sp_LoadDailyNoTimeReportingDetail] @Date
print '7 done'

exec [dbo].[sp_LoadDailyAgentReporting] @Date
print '8 done'

exec [dbo].[sp_CalculateETAandPerformanceMinus1Day] @Date,@Date

print 'step 9 Minus 1 day ETA done'

Exec [dbo].[sp_LoadPrimaryLabel_DataWithAddDatetime] @Date

print 'Step 10 Jobin and Anand Requested to load the data based on adddatetime'

Exec [dbo].[sp_LoadPrimaryLabel_DataWithPerformanceReportingDate] @Date

print 'Step 11 Jobin and Anand Requested to load the data based on performance Reporting Date'

---Added by PV on 2020-10-15
--exec [dbo].[sp_LoadDailyFutileMissedPartialPickup_Summary] @Date
--print '9 done'
--exec [dbo].[sp_LoadDailyFutileMissedPartialPickup_Detail] @Date
--print '10 done'
----Ends here -- Addition on 2020-10-15 by PV--
end
GO
