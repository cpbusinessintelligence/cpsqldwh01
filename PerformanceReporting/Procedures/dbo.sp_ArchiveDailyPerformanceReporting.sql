SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveDailyPerformanceReporting]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(AddDateTime) from [DailyPerformanceReporting]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [DailyPerformanceReporting_Archive_17-18]
	SELECT [PerformanceReportingID]
      ,[PerformanceDayID]
      ,[RevenueType]
      ,[ProductType]
      ,[AccountCode]
      ,[DeliveryDriver]
      ,[PickupDriver]
      ,[OnTimeStatus]
      ,[PickupETAZone]
      ,[DeliveryETAZone]
      ,[NetworkCategoryID]
      ,[BUCode]
      ,[PerformanceCount]
      ,[OnBoardComplainceCount]
      ,[DeliveryComplainceCount]
      ,[PODComplainceCount]
      ,[AddWho]
      ,[AddDateTime]
      ,[ProductSubCategory]
	   from [DailyPerformanceReporting] (nolock) where AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [DailyPerformanceReporting] where  AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
