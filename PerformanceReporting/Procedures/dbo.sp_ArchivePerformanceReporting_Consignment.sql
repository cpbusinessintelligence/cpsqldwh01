SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchivePerformanceReporting_Consignment]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(AddDateTime) from PerformanceReporting..Consignment
PRINT @MinDAte
PRINT DATEADD(day, 1,@MinDate)
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO PerformanceReporting..[Consignment_Archive_17-18]
	SELECT [ConsignmentID]
      ,[Sourcereference]
      ,[GWConsignmentID]
      ,[RevenueType]
      ,[ProductType]
      ,[ServiceType]
      ,[ConNote]
      ,[CustomerConNoteDate]
      ,[ConNoteCreatedDate]
      ,[CustomerETADate]
      ,[AccountCode]
      ,[PickupAddress]
      ,[ItemCount]
      ,[CouponCount]
      ,[OriginBranch]
      ,[OriginDepot]
      ,[DestinationBranch]
      ,[DestinationDepot]
      ,[NetworkCategory]
      ,[BUCode]
      ,[SalesOrderNumber]
      ,[SalesOrderLine]
      ,[SalesOrderDate]
      ,[RevenueAmount]
      ,[InsuranceAmount]
      ,[InsuranceCategory]
      ,[RevenueRecognisedDate]
      ,[RevenueProcessedDateTime]
      ,[IsRevenueProcessed]
      ,[AddWho]
      ,[AddDateTime]
  FROM [PerformanceReporting].[dbo].[Consignment] (nolock) where AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)


	DELETE FROM PerformanceReporting..[Consignment] where  AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END
GO
