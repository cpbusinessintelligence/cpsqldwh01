SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchivePerformanceReporting_InternationalConsignments]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(AddDateTime) from [dbo].[InternationalConsignments]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[InternationalConsignments_Archive_17-18]
	SELECT [ConsignmentID]
      ,[GWConsignmentID]
      ,[ProductType]
      ,[Category]
      ,[Sourcereference]
      ,[ConNote]
      ,[ConNoteCreatedDate]
      ,[CustomerETADate]
      ,[AccountCode]
      ,[PickupAddress]
      ,[ItemCount]
      ,[CouponCount]
      ,[OriginBranch]
      ,[OriginDepot]
      ,[DestinationBranch]
      ,[DestinationDepot]
      ,[NetworkCategory]
      ,[BUCode]
      ,[SalesOrderNumber]
      ,[SalesOrderLine]
      ,[SalesOrderDate]
      ,[RevenueAmount]
      ,[InsuranceAmount]
      ,[InsuranceCategory]
      ,[RevenueRecognisedDate]
      ,[RevenueProcessedDateTime]
      ,[IsRevenueProcessed]
      ,[AddWho]
      ,[AddDateTime]
  FROM [PerformanceReporting].[dbo].[InternationalConsignments] (nolock) where AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [PerformanceReporting].[dbo].[InternationalConsignments] where  AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
