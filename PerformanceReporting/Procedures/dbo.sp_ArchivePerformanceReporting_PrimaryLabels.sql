SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchivePerformanceReporting_PrimaryLabels]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(AddDateTime) from [dbo].[PrimaryLabels]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[PrimaryLabels_Archive_17-18]
	SELECT [PrimaryLabelsID]
      ,[ConsignmentID]
      ,[SourceReference]
      ,[RevenueType]
      ,[AccountCode]
      ,[LabelNumber]
      ,[ProductType]
      ,[ServiceCode]
      ,[FirstScannedBy]
      ,[FirstDateTime]
      ,[FirstScanType]
      ,[PickupScannedBy]
      ,[PickupDateTime]
      ,[InDepotScannedBy]
      ,[InDepotDateTime]
      ,[HandoverScannedBy]
      ,[HandoverDateTime]
      ,[TransferScannedBy]
      ,[TransferDateTime]
      ,[ConsolidateScannedBy]
      ,[ConsolidateDateTime]
      ,[ConsolidatedBarcode]
      ,[DeConsolidateScannedBy]
      ,[DeConsolidateDateTime]
      ,[OutforDeliveryScannedBy]
      ,[OutForDeliverDateTime]
      ,[DelayofDeliveryScannedBy]
      ,[DelayofDeliveryScannedDateTime]
      ,[DelayofDeliveryDate]
      ,[QuerycageScannedBy]
      ,[QuerycageDateTime]
      ,[QueryCageNumber]
      ,[AttemptedDeliveryScannedBy]
      ,[AttemptedDeliveryDateTime]
      ,[AttemptedDeliveryReceivedDatetime]
      ,[AttemptedDeliveryCardNumber]
      ,[CardCategory]
      ,[AcceptedbyPOPPointStatus]
      ,[AcceptedbyPOPPointScannedBy]
      ,[AcceptedbyPOPPointScannedDateTime]
      ,[DeliveryOptionId]
      ,[DeliveryScannedBy]
      ,[DeliveryDateTime]
      ,[DeliveryReceivedDateTime]
      ,[IsPODPresent]
      ,[PODDateTime]
      ,[DeliveryContractorType]
      ,[DeliveryContractorName]
      ,[LastScannedBy]
      ,[LastDateTime]
      ,[LastScanType]
      ,[PickupETAZone]
      ,[DeliveryETAZone]
      ,[BUCode]
      ,[NetworkCategoryID]
      ,[ETADate]
      ,[OnTimeStatusId]
      ,[ETARuletoprocess]
      ,[Reasoncode]
      ,[PerformanceReportingDate]
      ,[PerformanceProcessed]
      ,[isExceptionapplied]
      ,[AddWho]
      ,[LabelCreatedDateTime]
      ,[AddDateTime]
      ,[PerformanceProcessedDate]
      ,[ProductSubCategory]
      ,[AttemptedDeliveryReason]
  FROM [PerformanceReporting].[dbo].[PrimaryLabels] (nolock) where AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [PerformanceReporting].[dbo].[PrimaryLabels] where  AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
