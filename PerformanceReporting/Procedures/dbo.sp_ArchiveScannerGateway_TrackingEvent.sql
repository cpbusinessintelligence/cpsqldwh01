SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveScannerGateway_TrackingEvent]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(AddDateTime) from [dbo].[SecondaryLabels]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[SecondaryLabels_Archive_17-18]
	SELECT [SecondaryLabelsID]
      ,[RevenueType]
      ,[SecondaryLabelNumber]
      ,[SecondaryLabelPrefix]
      ,[PrimaryLabelNumber]
      ,[ScanDateTime]
      ,[ScannedBy]
      ,[LabelCreatedDateTime]
      ,[AddWho]
      ,[AddDateTime]
  FROM [PerformanceReporting].[dbo].[SecondaryLabels] (nolock) where AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [PerformanceReporting].[dbo].[SecondaryLabels] where  AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
