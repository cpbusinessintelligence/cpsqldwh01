SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE proc [dbo].[sp_CalculateETAandPerformance_OctToDec]
(@StartDate Date,@EndDate date )
As
Begin

    --'=====================================================================
    --' CP -Stored Procedure -[sp_CalculateETAandPerformance]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 06 Dec 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 06/12/2016    AK      1.00    Created the procedure                            
	--' 25/03/2020    PV      1.10    Modified for DIFOT report changes: Added ACY and ACN                            
    --'=====================================================================

	Declare @PrepaidExceptiondays int
	Declare @EDIExceptiondays int

	Set @PrepaidExceptiondays=(Select value from [CentralAdmin].[dbo].[Parameters] where code='PrepaidExceptiondays')
	Set @EDIExceptiondays=(Select value from [CentralAdmin].[dbo].[Parameters] where code='EDIExceptiondays')
	
	--SET IDENTITY_INSERT [PrimaryLabels_On20200320_byProcessDate] OFF;
-------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------

	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PerformanceProcessed = 1 , 
			PerformanceReportingDate = Convert(date,Isnull(AttemptedDeliveryDateTime,DeliveryDateTime)),
			PerformanceProcessedDate=Convert(date,Isnull(AttemptedDeliveryReceivedDateTime,DeliveryReceivedDateTime))
	WHERE (AttemptedDeliveryReceivedDateTime is not null or DeliveryReceivedDateTime is not null) 
		and isnull(PerformanceProcessed,0)=0 
		and Convert(date,Isnull(AttemptedDeliveryReceivedDateTime,DeliveryReceivedDateTime))  between @StartDate and @EndDate

   	SELECT RevenueType
	  ,Accountcode
      ,[LabelNumber]
	  ,ProductType
	  ,ServiceCode
      ,[PickupScannedBy]
      ,[PickupDateTime]
	  ,PickupETAZone as PickUpZone
	  ,AttemptedDeliveryDateTime
	  ,AttemptedDeliveryScannedBy
      ,DeliveryDateTime
	  ,DeliveryScannedBy
	  ,DeliveryETAZone as DeliveryZone
      ,[ETADate]
	  ,DeliveryContractorType
	  ,DelayofDeliveryDate
	  ,NetworkCategoryID 
	  ,[DeliveryOptionId]
      ,[OnTimeStatusId]
      ,[PerformanceReportingDate]
	  ,PerformanceProcessed
	  ,[LabelCreatedDateTime]
	  ,CASE WHEN FirstScanType IN ('Pickup','Transfer','In Depot','Consolidate','Deconsolidate','Handover','In Transit','Left in Depot','Link Scan','Misdirected'
			,'Out For Delivery','Tranship'
			,'accepted in Depot' --Added by PV to consider Sortation scans
			)
			THEN FirstDateTime ELSE NULL 
		END [FirstDateTime]
	Into #TempLabels
	FROM [dbo].[PrimaryLabels_AllStatus89Recs] (NOLOCK)
	Where PerformanceProcessed = 1 
			and PerformanceProcessedDate    between @StartDate and @EndDate

	Update #TempLabels SET PickUpZone =C.ETAZone  
    From #TempLabels L Join [dbo].[DimContractor]  C on L.PickupScannedBy = C.DriverID 
	Where Isnull(PickUpZone,'')= ''

	Update #TempLabels SET DeliveryZone =C.ETAZone
			--,DeliveryContractorType = Case  C.ContractorCategory WHEN 'Agent' THEN 'Agent' ELSE '' END  
	From #TempLabels L Join [dbo].[DimContractor]  C on Isnull(L.AttemptedDeliveryScannedBy,L.DeliveryScannedBy) = C.DriverID   and ISnull(DeliveryZone,'') =''

	Update #TempLabels SET PickUpZone=d.ETAZone
	From #TempLabels t 
		join Pronto.dbo.Prontocoupondetails p on t.labelnumber=p.serialnumber
		join Pronto.dbo.Prontosalesorder s on s.ordernumber=p.LastSoldReference and s.CompanyCode='CL1'
		join dbo.[CustomertoZonemap] d on d.Accountcode=s.CustomerCode
	Where Isnull(PickUpZone,'')= '' and RevenueType='Prepaid' 
  
	PRINT 'Calculated [PickUpZone] for Prepaid from Sales Orders '  

	Update #TempLabels SET DeliveryContractorType = 'Agent'
                       --  [OnTimeStatus] = [dbo].[fn_GetStatusCodefromDescription] ('Agent Manifested')
    From #TempLabels P 
		Join CpplEdi.dbo.consignment C on P.LabelNumber = C.cd_connote 
		Left Join CpplEDI.dbo.agents A on C.cd_agent_id = A.A_id 
	Where P.RevenueType = 'Prepaid' and DeliveryContractorType is null and A.a_cppl = 'N'

	--Update [DeliveryOptions] set DeliveryContractor =isnull(l.AttemptedDeliveryScannedBy,DeliveryScannedBy)
	--From Deliveryoptions d join #TempLabels l on l.labelnumber=d.labelnumber

	--Update [DeliveryOptions] set DeliveryContractor =isnull(l.AttemptedDeliveryScannedBy,DeliveryScannedBy)
	--From Deliveryoptions d join Primarylabels l on l.labelnumber=d.labelnumber
	--Where d.Deliverycontractor is null
	------------------------------------------------
	---Calculate Network Category
	---------------------------------------
	Update #TempLabels SET [NetworkCategoryID] = [dbo].[fn_GetNetworkIDFromETAZone](PickUpZone,DeliveryZone,ProductType) 
	From #TempLabels
	Where [NetworkCategoryID] is null
	-----------------------------------------------------
	--select * from #templabels where labelnumber='30204280180'

	Update #TempLabels SET DeliveryContractorType = 'Agent' 
    From #TempLabels P 
		Join [dbo].[NetworkCategory] n on n.networkcategoryid=p.networkcategoryid
	Where n.[SecondaryNetworkCategory]='Agent' and DeliveryContractorType is null 	   
	--Update [LinehaulData] SET ETADate = [dbo].[fn_GetETADateFromZone](ProductType,Null,Null,Null,PickUpetaZone,DeliveryetaZone,Null,Convert(datetime,ConsolidatedDatetime),ConsolidateScannedBy) 
	--Where ConsolidatedDatetime is not null and ETADate is null
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--Updating ETAs
	Update #TempLabels SET ETADate =[dbo].[fn_CheckforaPublicHolidayinaZone]([DelayofDeliveryDate],DeliveryZone) 
	Where DelayofDeliveryDate is not null and ETADate is null
    
	--Update #TempLabels SET ETADate=[dbo].[fn_CheckforaPublicHolidayinaZone](d.ETADate,t.DeliveryZone)
	--from #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	-- where  t.ETADate is null and d.ETADate is not null and d.Category='Standard Redelivery'
	
	--Update it based on where ETAdate is null and Redelivery
	Update #TempLabels SET ETADate =
	[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
	From #TempLabels t 
		Join Deliveryoptions d on d.labelnumber=t.labelnumber
	Where  t.ETADate is null  and d.Category in ('Redelivery to HUBBED','Redelivery to POPStation')
		
	--Update DeliveryOptions SET ETADate=
	--[dbo].[fn_CheckforaPublicHolidayinaZone](CAST(CAST(dateadd(day,1,AttemptedDeliveryDateTime) AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME),t.DeliveryZone)
	--From  Deliveryoptions d  join #Templabels t on d.labelnumber=t.labelnumber
	--Where d.ETADate is  null and d.Category in ('Redelivery to HUBBED','Redelivery to POPStation')

	--Update it based on where Pickupdate is not null and ETAdate is null and Redirection
    Update #TempLabels SET ETADate = 
	[dbo].[fn_CheckforaPublicHolidayinaZone](dateadd(day,1,[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),d.PickUpetaZone,d.NewDeliveryETAZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)),d.NewDeliveryETAZone)
    From #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.ETADate is null and d.[Category]='Redirection' and d.DeliveryMethod<>'Authority To Leave'

    --Update it based on where Pickupdate is not null and ETAdate is null and Redirection and 'Authority to leave'	
	Update #TempLabels SET ETADate =
	[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),d.PickUpetaZone,d.NewDeliveryETAZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)
	From #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.ETADate is null and d.[Category]='Redirection' and d.DeliveryMethod='Authority To Leave'
	
	--Update it when pickupdate is not null and eta is null
	Update #TempLabels SET ETADate =
	[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,Accountcode,LEft(LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
    Where [PickupDateTime] is not null and ETADate is null
	
	--Update it when pickupdate and eta is null and Revenutype is EDI
	Update #TempLabels SET ETADate =
	[dbo].[fn_GetETADateFromZone](ProductType,RevenueType,Accountcode,LEft(LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,Convert(datetime,[LabelCreatedDateTime]),'') 
	Where [PickupDateTime] is  null and RevenueType = 'EDI' and ETADate is null
	-------------------------------------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------------------------------------
	--Updating Status IDs
	
	--Updating 'CY' when Attmpted Del/Delivery is before ETA Date, and 'CN' if its after ETA; and Delivery by 'CP'
	Update #TempLabels SET [OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
			THEN  [dbo].[fn_GetStatusCodefromDescription] ('On Time by First Scan Date CP Deliveries Only')  
			ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not On Time by Creation Date CP Deliveries Only') END
    Where Isnull([OnTimeStatusId],'') ='' And Etadate is not null And [PickupDateTime] is null and RevenueType = 'EDI' And isnull(DeliveryContractortype,'')<>'Agent'

	--Updating 'Y' when Attmpted Del/Delivery is before ETA Date, and 'N' if its after ETA; and Delivery by 'CP'
	Update #TempLabels SET [OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
	        THEN [dbo].[fn_GetStatusCodefromDescription] ('On Time CP Deliveries Only') 
			ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not On Time CP Deliveries Only')  END
    Where Isnull([OnTimeStatusId],'') ='' And ETADate is not null And PickupDateTime is not null And isnull(DeliveryContractortype,'')<>'Agent'
		  
	--Updating 'AY' when Attmpted Del/Delivery is before ETA Date, and 'AN' if its after ETA; and Delivery by 'Agent'	  
	Update #TempLabels SET [OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
	        THEN [dbo].[fn_GetStatusCodefromDescription] ('On Time Agent Non CP Deliveries Only') 
			ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not On Time Agent Non CP Deliveries Only')  END
    Where Isnull([OnTimeStatusId],'') ='' And ETADate is not null And [PickupDateTime] is not null And isnull(DeliveryContractortype,'')='Agent'

	---Added by PV: For ACY and ACN
	Update #TempLabels SET [OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
			THEN  [dbo].[fn_GetStatusCodefromDescription] ('On Time by First Scan Date Agent Delivery Only')  
			ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not On Time by First Scan Date Agent Delivery Only') END
    Where Isnull([OnTimeStatusId],'') ='' And Etadate is not null And [PickupDateTime] is null And isnull(DeliveryContractortype,'')= 'Agent'
	---Addition for ACY and ACN - Ends here---

	--Updating 'CE' for CP delivery Excpetion and 'AE' for Agent delivery exception; i.e. when ETA is null and Attempted/Delivery is not null.
	Update #TempLabels SET [OnTimeStatusId] = 
		CASE WHEN isnull(DeliveryContractorType,'')<>'Agent' 
			THEN  [dbo].[fn_GetStatusCodefromDescription] ('CP Delivery Exception')  
			ELSE  [dbo].[fn_GetStatusCodefromDescription] ('Agent Delivery Exception') END
    Where Isnull([OnTimeStatusId],'') ='' And ETADate is null 
	And Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) is not null
	------------------

	--Update [LinehaulData] SET [OnTimeStatusId] =
	--	CASE WHEN  convert(time,[DeConsolidateDateTime])<CAST('12:00:00' AS TIME)  and Convert(DateTime, [DeConsolidateDateTime] ) <= ETADate  
	--		THEN [dbo].[fn_GetStatusCodefromDescription] ('LH TransitId OnTime') 
	--		ELSE [dbo].[fn_GetStatusCodefromDescription] ('LH TransitId NotOnTime')  END
	--Where Isnull([OnTimeStatusId],'') ='' and  Etadate is not null 

--Update  #TempLabels SET [OnTimeStatus] =[dbo].[fn_GetStatusCodefromDescription] ('Unknown') 
--Where ETADate is null and  Isnull([OnTimeStatus],'') =''

	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PickupETAZone =T.PickUpZone,
									DeliveryETAZone = T.DeliveryZone,
									ETADate=T.ETADate,
									DeliveryContractorType = T.DeliveryContractorType,
									[OnTimeStatusId] = T.OnTimeStatusId,
									NetworkCategoryId=T.NetworkCategoryId
    From #TempLabels T 
		Join [dbo].[PrimaryLabels_AllStatus89Recs] P on T.LabelNumber = P.LabelNumber
-------------------------------------
-------------21 Day Exception for EDI--------
-------------------------------------

	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription] ('No Activity') 
    Where  isnull(PerformanceProcessed,0)=0 
		and  isnull(Etadate,convert(date,LabelCreateddatetime)) <= convert(date,Dateadd(Day,-@EDIExceptiondays,@StartDate)) and revenuetype='EDI'
		and ISnull([OnTimeStatusId],'') = '' 
		and PickupDateTime is null 
		and DeliveryDateTime is null 
		and AttemptedDeliveryDateTime is null           
		and TransferDateTime is null  
		and HandoverDateTime is null 
		and OutForDeliverDateTime is null 
		and InDepotDateTime is null
				  

	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription] ('CP Delivery Exception') 
	Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@EDIExceptiondays,@StartDate) and revenuetype='EDI'
		and ISnull([OnTimeStatusId],'') = ''  and isnull(DeliveryContractorType,'')<>'Agent' 

	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription] ('Agent Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@EDIExceptiondays,@StartDate) and revenuetype='EDI'
		and ISnull([OnTimeStatusId],'') = ''  and isnull(DeliveryContractorType,'')='Agent' 		

-------------------------------------
-------------40 Day Exception for Prepaid--------
-------------------------------------

	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription] ('No Activity') 
    Where isnull(PerformanceProcessed,0)=0 
		and  [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId],'') = '' 
		and PickupDateTime is null 
		and DeliveryDateTime is null 
		and AttemptedDeliveryDateTime is null           
		and TransferDateTime is null  
		and HandoverDateTime is null 
		and OutForDeliverDateTime is null 
		and InDepotDateTime is null

	--Any Prepaid got any activity and no status for 40 days will be Exception--
	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription] ('CP Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and  [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId],'') = '' and isnull(DeliveryContractorType,'')<>'Agent' 


	Update [dbo].[PrimaryLabels_AllStatus89Recs] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription] ('Agent Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId],'') = '' and isnull(DeliveryContractorType,'')='Agent' 

	--Exec sp_CalculateInternationalETAandPerformance @StartDate ,@EndDate
	------------------------------------------------
	-----Calculate ETA fro non processed EDI Labels
	-----------------------------------------
	--  Update [dbo].[PrimaryLabels_AllStatus89Recs] SET ETADate = [dbo].[fn_GetETADateFromZone](RevenueType,Accountcode,LEft(LabelNumber,3),Convert(datetime,[PickupDateTime]),PickupScannedBy,PickupETAZone,DeliveryETAZone) 
	--  Where [PickupDateTime] is not null and ETADate is not null and RevenueType = 'EDI'

End

GO
