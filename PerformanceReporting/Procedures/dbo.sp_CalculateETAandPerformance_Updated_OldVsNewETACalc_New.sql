SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



 --[sp_CalculateETAandPerformance_Updated_OldVsNewETACalc_New] '2020-09-04', '2020-09-04'

CREATE proc [dbo].[sp_CalculateETAandPerformance_Updated_OldVsNewETACalc_New]
(@StartDate Date,@EndDate date )
As
Begin

    --'=====================================================================
    --' CP -Stored Procedure -[sp_CalculateETAandPerformance]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 06 Dec 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 06/12/2016    AK      1.00    Created the procedure                            
	--' 25/03/2020    PV      1.10    Modified for DIFOT report changes: Added ACY and ACN                            
    --'=====================================================================

	Declare @PrepaidExceptiondays int
	Declare @EDIExceptiondays int

	Set @PrepaidExceptiondays=(Select value from [CentralAdmin].[dbo].[Parameters] where code='PrepaidExceptiondays')
	Set @EDIExceptiondays=(Select value from [CentralAdmin].[dbo].[Parameters] where code='EDIExceptiondays')
	
	SET IDENTITY_INSERT [PrimaryLabels_On20200320_byProcessDate] OFF;
-------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------

	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET PerformanceProcessed = 1 , 
			PerformanceReportingDate = Convert(date,Isnull(AttemptedDeliveryDateTime,DeliveryDateTime)),
			PerformanceProcessedDate=Convert(date,Isnull(AttemptedDeliveryReceivedDateTime,DeliveryReceivedDateTime))
	WHERE (AttemptedDeliveryReceivedDateTime is not null or DeliveryReceivedDateTime is not null) 
		and isnull(PerformanceProcessed,0)=0 
		and Convert(date,Isnull(AttemptedDeliveryReceivedDateTime,DeliveryReceivedDateTime))  between @StartDate and @EndDate

   	SELECT RevenueType
	  ,Accountcode
      ,[LabelNumber]
	  ,ProductType
	  ,ServiceCode
      ,[PickupScannedBy]
      ,[PickupDateTime]
	  ,PickupETAZone as PickUpZone
	  ,AttemptedDeliveryDateTime
	  ,AttemptedDeliveryScannedBy
      ,DeliveryDateTime
	  ,DeliveryScannedBy
	  ,DeliveryETAZone as DeliveryZone
      ,[ETADate]
	  ,DeliveryContractorType
	  ,DelayofDeliveryDate
	  ,NetworkCategoryID 
	  ,[DeliveryOptionId]
      ,[OnTimeStatusId]
      ,[PerformanceReportingDate]
	  ,PerformanceProcessed
	  ,[LabelCreatedDateTime]
	  ,CASE WHEN FirstScanType IN ('Pickup','Transfer','In Depot','Consolidate','Deconsolidate','Handover','In Transit','Left in Depot','Link Scan','Misdirected'
			,'Out For Delivery','Tranship'
			,'accepted in Depot' --Added by PV to consider Sortation scans
			)
			THEN FirstDateTime ELSE NULL 
		END [FirstDateTime]
	  ,[ETADate_1Day]
	  ,[OnTimeStatusId_1Day]
	Into #TempLabels
	FROM [dbo].[PrimaryLabels_OldVsNewETACalc] (NOLOCK)
	Where PerformanceProcessed = 1 
			and PerformanceProcessedDate BETWEEN @StartDate AND @EndDate

	-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--Updating ETAs
	Update #TempLabels SET ETADate =[dbo].[fn_CheckforaPublicHolidayinaZone_OldVsNewETA]([DelayofDeliveryDate],DeliveryZone)
	Where DelayofDeliveryDate is not null and ETADate is null
    
	Update #TempLabels SET ETADate_1Day =[dbo].[fn_CheckforaPublicHolidayinaZone_OldVsNewETA]([DelayofDeliveryDate],DeliveryZone)
	Where DelayofDeliveryDate is not null and ETADate_1Day is null
    
	--Update #TempLabels SET ETADate=[dbo].[fn_CheckforaPublicHolidayinaZone_OldVsNewETA](d.ETADate,t.DeliveryZone)
	--from #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	-- where  t.ETADate is null and d.ETADate is not null and d.Category='Standard Redelivery'
	
	--Update it based on where ETAdate is null and Redelivery
	Update #TempLabels SET 
	ETADate = [dbo].[fn_GetETADateFromZone_OldVsNewETA](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
	From #TempLabels t 
	Join Deliveryoptions d on d.labelnumber=t.labelnumber
	Where  t.ETADate is null  and d.Category in ('Redelivery to HUBBED','Redelivery to POPStation')
	
	Update #TempLabels SET 
	ETADate_1Day = [dbo].[fn_GetETADateFromZone_1DayETA](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
	From #TempLabels t 
	Join Deliveryoptions d on d.labelnumber=t.labelnumber
	Where t.ETADate_1Day is null  and d.Category in ('Redelivery to HUBBED','Redelivery to POPStation')
		
	----Update DeliveryOptions SET ETADate=
	----[dbo].[fn_CheckforaPublicHolidayinaZone_OldVsNewETA](CAST(CAST(dateadd(day,1,AttemptedDeliveryDateTime) AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME),t.DeliveryZone)
	----From  Deliveryoptions d  join #Templabels t on d.labelnumber=t.labelnumber
	----Where d.ETADate is  null and d.Category in ('Redelivery to HUBBED','Redelivery to POPStation')

	--Update it based on where Pickupdate is not null and ETAdate is null and Redirection
    Update #TempLabels SET 
	ETADate = [dbo].[fn_CheckforaPublicHolidayinaZone_OldVsNewETA](dateadd(day,1,[dbo].[fn_GetETADateFromZone_OldVsNewETA](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),d.PickUpetaZone,d.NewDeliveryETAZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)),d.NewDeliveryETAZone)
    From #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.ETADate is null and d.[Category]='Redirection' and d.DeliveryMethod<>'Authority To Leave'

	Update #TempLabels SET 
	ETADate_1Day = [dbo].[fn_CheckforaPublicHolidayinaZone_OldVsNewETA](dateadd(day,1,[dbo].[fn_GetETADateFromZone_1DayETA](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),d.PickUpetaZone,d.NewDeliveryETAZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)),d.NewDeliveryETAZone)
    From #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.ETADate_1Day is null and d.[Category]='Redirection' and d.DeliveryMethod<>'Authority To Leave'

    --Update it based on where Pickupdate is not null and ETAdate is null and Redirection and 'Authority to leave'	
	Update #TempLabels SET 
	ETADate = [dbo].[fn_GetETADateFromZone_OldVsNewETA](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),d.PickUpetaZone,d.NewDeliveryETAZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)
	From #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.ETADate is null and d.[Category]='Redirection' and d.DeliveryMethod='Authority To Leave'
	
	Update #TempLabels SET 
	ETADate_1Day = [dbo].[fn_GetETADateFromZone_1DayETA](ProductType,RevenueType,Accountcode,LEft(t.LabelNumber,3),d.PickUpetaZone,d.NewDeliveryETAZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy)
	From #TempLabels t join Deliveryoptions d on d.DeliveryOptionId=t.DeliveryOptionId
	Where [PickupDateTime] is not null and t.ETADate_1Day is null and d.[Category]='Redirection' and d.DeliveryMethod='Authority To Leave'
	
	--Update it when pickupdate is not null and eta is null
	Update #TempLabels SET 
	ETADate = [dbo].[fn_GetETADateFromZone_OldVsNewETA](ProductType,RevenueType,Accountcode,LEft(LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
    Where [PickupDateTime] is not null and ETADate is null
	
	Update #TempLabels SET 
	ETADate_1Day = [dbo].[fn_GetETADateFromZone_1DayETA](ProductType,RevenueType,Accountcode,LEft(LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,ISNULL(Convert(datetime,[PickupDateTime]),Convert(datetime,[FirstDateTime])),PickupScannedBy) 
    Where [PickupDateTime] is not null and ETADate_1Day is null
	
	--Update it when pickupdate and eta is null and Revenutype is EDI
	Update #TempLabels SET 
	ETADate = [dbo].[fn_GetETADateFromZone_OldVsNewETA](ProductType,RevenueType,Accountcode,LEft(LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,Convert(datetime,[LabelCreatedDateTime]),'') 
	Where [PickupDateTime] is  null and RevenueType = 'EDI' and ETADate is null
	
	Update #TempLabels SET 
	ETADate_1Day = [dbo].[fn_GetETADateFromZone_1DayETA](ProductType,RevenueType,Accountcode,LEft(LabelNumber,3),PickUpZone,DeliveryZone,ServiceCode,Convert(datetime,[LabelCreatedDateTime]),'') 
	Where [PickupDateTime] is  null and RevenueType = 'EDI' and ETADate_1Day is null
	-------------------------------------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------------------------------------
	--Updating Status IDs
	
	--Updating 'CY' when Attmpted Del/Delivery is before ETA Date, and 'CN' if its after ETA; and Delivery by 'CP'
	Update #TempLabels SET 
		[OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
			THEN  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time by First Scan Date CP Deliveries Only')  
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time by Creation Date CP Deliveries Only') END		
    Where Isnull([OnTimeStatusId],'') ='' And Etadate is not null And [PickupDateTime] is null and RevenueType = 'EDI' And isnull(DeliveryContractortype,'')<>'Agent'

	Update #TempLabels SET 
		[OnTimeStatusId_1Day] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate_1Day  
			THEN  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time by First Scan Date CP Deliveries Only')  
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time by Creation Date CP Deliveries Only') END
    Where Isnull([OnTimeStatusId_1Day],'') ='' And ETADate_1Day is not null And [PickupDateTime] is null and RevenueType = 'EDI' And isnull(DeliveryContractortype,'')<>'Agent'
	
	--Updating 'Y' when Attmpted Del/Delivery is before ETA Date, and 'N' if its after ETA; and Delivery by 'CP'
	Update #TempLabels SET 
		[OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate_1Day  
	        THEN [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time CP Deliveries Only') 
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time CP Deliveries Only')  END
	Where Isnull([OnTimeStatusId],'') ='' And ETADate is not null And PickupDateTime is not null And isnull(DeliveryContractortype,'')<>'Agent'
	Update #TempLabels SET 
		[OnTimeStatusId_1Day] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate_1Day
	        THEN [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time CP Deliveries Only') 
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time CP Deliveries Only')  END
    Where Isnull([OnTimeStatusId_1Day],'') ='' And ETADate_1Day is not null And PickupDateTime is not null And isnull(DeliveryContractortype,'')<>'Agent'
		  
	--Updating 'AY' when Attmpted Del/Delivery is before ETA Date, and 'AN' if its after ETA; and Delivery by 'Agent'	  
	Update #TempLabels SET 
		[OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
	        THEN [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time Agent Non CP Deliveries Only') 
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time Agent Non CP Deliveries Only')  END		
    Where Isnull([OnTimeStatusId],'') ='' And ETADate is not null And [PickupDateTime] is not null And isnull(DeliveryContractortype,'')='Agent'
	Update #TempLabels SET 
		[OnTimeStatusId_1Day] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate_1Day
	        THEN [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time Agent Non CP Deliveries Only') 
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time Agent Non CP Deliveries Only')  END
    Where Isnull([OnTimeStatusId_1Day],'') ='' And ETADate_1Day is not null And [PickupDateTime] is not null And isnull(DeliveryContractortype,'')='Agent'

	---Added by PV: For ACY and ACN
	Update #TempLabels SET 
		[OnTimeStatusId] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
			THEN  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time by First Scan Date Agent Delivery Only')  
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time by First Scan Date Agent Delivery Only') END		
    Where Isnull([OnTimeStatusId],'') ='' And Etadate is not null And [PickupDateTime] is null And isnull(DeliveryContractortype,'')= 'Agent'
	Update #TempLabels SET 
		[OnTimeStatusId_1Day] = 
		CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate_1Day 
			THEN  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('On Time by First Scan Date Agent Delivery Only')  
			ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Not On Time by First Scan Date Agent Delivery Only') END
    Where Isnull([OnTimeStatusId_1Day],'') ='' And ETADate_1Day is not null And [PickupDateTime] is null And isnull(DeliveryContractortype,'')= 'Agent'
	---Addition for ACY and ACN - Ends here---

	--Updating 'CE' for CP delivery Excpetion and 'AE' for Agent delivery exception; i.e. when ETA is null and Attempted/Delivery is not null.
	Update #TempLabels SET 
		[OnTimeStatusId] = 
		CASE WHEN isnull(DeliveryContractorType,'')<>'Agent' 
			THEN  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('CP Delivery Exception')  
			ELSE  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Agent Delivery Exception') END
	Where Isnull([OnTimeStatusId],'') ='' And ETADate is null 
	And Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) is not null	
	Update #TempLabels SET 
		[OnTimeStatusId_1Day] = 
		CASE WHEN isnull(DeliveryContractorType,'')<>'Agent' 
			THEN  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('CP Delivery Exception')  
			ELSE  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Agent Delivery Exception') END
    Where Isnull([OnTimeStatusId_1Day],'') ='' And ETADate_1Day is null 
	And Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) is not null
	
	------------------

	----Update [LinehaulData] SET [OnTimeStatusId] =
	----	CASE WHEN  convert(time,[DeConsolidateDateTime])<CAST('12:00:00' AS TIME)  and Convert(DateTime, [DeConsolidateDateTime] ) <= ETADate  
	----		THEN [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('LH TransitId OnTime') 
	----		ELSE [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('LH TransitId NotOnTime')  END
	----Where Isnull([OnTimeStatusId],'') ='' and  Etadate is not null 

--Update  #TempLabels SET [OnTimeStatus] =[dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Unknown') 
--Where ETADate is null and  Isnull([OnTimeStatus],'') =''

	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET 
									ETADate=T.ETADate,
									[OnTimeStatusId] = T.OnTimeStatusId,
									ETADate_1Day = T.ETADate_1Day,
									OnTimeStatusId_1Day = T.OnTimeStatusId_1Day
    From #TempLabels T 
		Join [dbo].[PrimaryLabels_OldVsNewETACalc] P on T.LabelNumber = P.LabelNumber
-------------------------------------
-------------21 Day Exception for EDI--------
-------------------------------------
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET
									[OnTimeStatusId_1Day] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('No Activity') 
    Where  isnull(PerformanceProcessed,0)=0 
		and  isnull(ETADate_1Day,convert(date,LabelCreateddatetime)) <= convert(date,Dateadd(Day,-@EDIExceptiondays,@StartDate)) and revenuetype='EDI'
		and ISnull([OnTimeStatusId_1Day],'') = '' 
		and PickupDateTime is null 
		and DeliveryDateTime is null 
		and AttemptedDeliveryDateTime is null           
		and TransferDateTime is null  
		and HandoverDateTime is null 
		and OutForDeliverDateTime is null 
		and InDepotDateTime is null

	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('No Activity') 
    Where  isnull(PerformanceProcessed,0)=0 
		and  isnull(Etadate,convert(date,LabelCreateddatetime)) <= convert(date,Dateadd(Day,-@EDIExceptiondays,@StartDate)) and revenuetype='EDI'
		and ISnull([OnTimeStatusId],'') = '' 
		and PickupDateTime is null 
		and DeliveryDateTime is null 
		and AttemptedDeliveryDateTime is null           
		and TransferDateTime is null  
		and HandoverDateTime is null 
		and OutForDeliverDateTime is null 
		and InDepotDateTime is null
	----			  
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET 
									[OnTimeStatusId_1Day] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('CP Delivery Exception') 
	Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@EDIExceptiondays,@StartDate) and revenuetype='EDI'
		and ISnull([OnTimeStatusId_1Day],'') = ''  and isnull(DeliveryContractorType,'')<>'Agent' 
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('CP Delivery Exception') 
	Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@EDIExceptiondays,@StartDate) and revenuetype='EDI'
		and ISnull([OnTimeStatusId],'') = ''  and isnull(DeliveryContractorType,'')<>'Agent' 
	---
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET 
									[OnTimeStatusId_1Day] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Agent Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@EDIExceptiondays,@StartDate) and revenuetype='EDI'
		and ISnull([OnTimeStatusId_1Day],'') = ''  and isnull(DeliveryContractorType,'')='Agent' 
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Agent Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@EDIExceptiondays,@StartDate) and revenuetype='EDI'
		and ISnull([OnTimeStatusId],'') = ''  and isnull(DeliveryContractorType,'')='Agent' 
-------------------------------------
-------------40 Day Exception for Prepaid--------
-------------------------------------
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET 
									[OnTimeStatusId_1Day] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('No Activity') 
    Where isnull(PerformanceProcessed,0)=0 
		and  [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId_1Day],'') = '' 
		and PickupDateTime is null 
		and DeliveryDateTime is null 
		and AttemptedDeliveryDateTime is null           
		and TransferDateTime is null  
		and HandoverDateTime is null 
		and OutForDeliverDateTime is null 
		and InDepotDateTime is null
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('No Activity') 
    Where isnull(PerformanceProcessed,0)=0 
		and  [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId],'') = '' 
		and PickupDateTime is null 
		and DeliveryDateTime is null 
		and AttemptedDeliveryDateTime is null           
		and TransferDateTime is null  
		and HandoverDateTime is null 
		and OutForDeliverDateTime is null 
		and InDepotDateTime is null
	--Any Prepaid got any activity and no status for 40 days will be Exception--
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET 
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('CP Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and  [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId_1Day],'') = '' and isnull(DeliveryContractorType,'')<>'Agent' 
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('CP Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and  [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId],'') = '' and isnull(DeliveryContractorType,'')<>'Agent' 
	----
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET 
									[OnTimeStatusId_1Day] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Agent Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId_1Day],'') = '' and isnull(DeliveryContractorType,'')='Agent'
	Update [dbo].[PrimaryLabels_OldVsNewETACalc] SET PerformanceProcessed = 1 , 
									PerformanceReportingDate = Convert(date,@StartDate) ,
									[OnTimeStatusID] =  [dbo].[fn_GetStatusCodefromDescription_OldVsNewETA] ('Agent Delivery Exception') 
    Where isnull(PerformanceProcessed,0)=0 
		and [LabelCreatedDateTime] <= Dateadd(Day,-@PrepaidExceptiondays,@StartDate) and revenuetype<>'EDI'
		and ISnull([OnTimeStatusId],'') = '' and isnull(DeliveryContractorType,'')='Agent' 			
	--Exec sp_CalculateInternationalETAandPerformance @StartDate ,@EndDate
	------------------------------------------------
	-----Calculate ETA fro non processed EDI Labels
	-----------------------------------------
	--  Update [dbo].[PrimaryLabels] SET ETADate = [dbo].[fn_GetETADateFromZone_OldVsNewETA](RevenueType,Accountcode,LEft(LabelNumber,3),Convert(datetime,[PickupDateTime]),PickupScannedBy,PickupETAZone,DeliveryETAZone) 
	--  Where [PickupDateTime] is not null and ETADate is not null and RevenueType = 'EDI'

End
GO
