SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_CalculateInternationalETAandPerformance](@StartDate date,@EndDate date) as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_CalculateInternationalETAandPerformance]
    --' ---------------------------
    --' Purpose: sp_CalculateInternationalETAandPerformance-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 06 Dec 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 06/12/2016    AK      1.00    Created the procedure                            

    --'=====================================================================


Update [dbo].[InternationalLabels] SET PerformanceProcessed = 1 , 
                                        PerformanceReportingDate = Convert(date,Isnull(AttemptedDeliveryDateTime,DeliveryDateTime))
		 WHere (AttemptedDeliveryDateTime is not null or DeliveryDateTime is not null) 
		     and  isnull(PerformanceProcessed,0)=0 
			 and  Convert(date,Isnull(AttemptedDeliveryDateTime,DeliveryDateTime))  between @StartDate and @EndDate

	SELECT 
	   Accountcode
      ,[LabelNumber]
	  ,Category
	  ,ProductType
      ,FromCountry
	  ,ToCountry
	  ,PickupScannedBy
	  ,PickupDateTime
	  ,[PickupETAZone] as PickUpZone
	  ,AttemptedDeliveryDateTime
	  ,AttemptedDeliveryScannedBy
      ,DeliveryDateTime
	  ,DeliveryScannedBy
	  ,[DeliveryETAZone] as DeliveryZone
      ,[ETADate]
      ,[OnTimeStatusId]
      ,[PerformanceReportingDate]
	  ,PerformanceProcessed
	  ,[LabelCreatedDateTime]
  Into #TempLabelsI
  FROM [dbo].InternationalLabels (NOLOCK)
  Where  PerformanceProcessed = 1 
      and PerformanceReportingDate  between @StartDate and @EndDate


 Update #TempLabelsI SET PickUpZone =C.ETAZone  
             From #TempLabelsI L Join [dbo].[DimContractor]  C on L.PickupScannedBy = C.DriverID 
			 Where Isnull(PickUpZone,'')= ''

 Update #TempLabelsI SET DeliveryZone =C.ETAZone
  --,DeliveryContractorType = Case  C.ContractorCategory WHEN 'Agent' THEN 'Agent' ELSE '' END  
		     From #TempLabelsI L Join [dbo].[DimContractor]  C on Isnull(L.AttemptedDeliveryScannedBy,L.DeliveryScannedBy) = C.DriverID   and ISnull(DeliveryZone,'') =''


 update #TempLabelsI set [ETADate]=[dbo].[fn_CalculateInternationalETA] (isnull(PickupDateTime,LabelCreatedDatetime),Category,ProductType,FromCountry,ToCountry,PickupZone ,DeliveryZone)
 where ETADate is null and PickupDateTime is not null


 update #TempLabelsI set [OnTimeStatusId]=CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
                                            THEN  [dbo].[fn_GetStatusCodefromDescription] ('On Time by Creation Date CP Deliveries Only')  ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not On Time by Creation Date CP Deliveries Only')  END
             Where  Isnull([OnTimeStatusId],'') ='' and  Etadate is not null and  [PickupDateTime] is  null 


  Update #TempLabelsI SET [OnTimeStatusId] = CASE WHEN Convert(DateTime,CASE When AttemptedDeliveryDateTime is Null THEN DeliveryDateTime ELSE AttemptedDeliveryDateTime END ) <= ETADate  
	            THEN [dbo].[fn_GetStatusCodefromDescription] ('On Time CP Deliveries Only') ELSE [dbo].[fn_GetStatusCodefromDescription] ('Not On Time CP Deliveries Only')  END
              Where  Isnull([OnTimeStatusId],'') ='' and  Etadate is not null 



  Update [dbo].InternationalLabels SET  PickupETAZone =T.PickUpZone,
                                          DeliveryETAZone = T.DeliveryZone,
										  ETADate=T.ETADate,
										  [OnTimeStatusId] = T.OnTimeStatusId
              From #TempLabelsI T Join [dbo].InternationalLabels P on T.LabelNumber = P.LabelNumber



end
GO
