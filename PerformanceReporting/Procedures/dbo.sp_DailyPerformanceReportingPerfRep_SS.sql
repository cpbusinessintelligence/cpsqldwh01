SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_DailyPerformanceReportingPerfRep_SS (@AddDatetime DateTime)
as


Begin transaction PerfRep2

Insert into [dbo].[DailyPerformanceReporting_Archive_17-18]
select * from [dbo].[DailyPerformanceReporting]
where [AddDateTime] < = @AddDatetime

Delete from [dbo].[DailyPerformanceReporting]
where [AddDateTime] < = @AddDatetime


Commit transaction PerfRep2

GO
