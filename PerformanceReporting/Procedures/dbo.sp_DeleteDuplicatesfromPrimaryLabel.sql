SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_DeleteDuplicatesfromPrimaryLabel]
AS
BEGIN

	--'=====================================================================
	--' CP -Stored Procedure -[sp_DeleteDuplicatefromPrimaryLabel]
	--' ---------------------------
	--' Purpose: Delete duplicates from primary labels-----
	--' Developer: Rajender Reddy (Couriers Please Pty Ltd)
	--- Its tempory fix to avoid duplicates
	--' Date: 11 Dec 2020 
	--' Copyright: 2020 Couriers Please Pty Ltd
	--' Change Log: 
	--' Date          Who     Ver     Reason                                            
	--' ----          ---     ---     -----                                            
	--' 11/12/2020    RM      1.00    Created the procedure                            
    --'=====================================================================

	--WITH Cte AS 
 --    (
 --     Select *, ROW_NUMBER() OVER(PARTITION BY  LabelNumber ORDER BY [PrimaryLabelsID] ) AS RowNum from [dbo].[PrimaryLabels]  
 --    )
 
 --    --Select * from Cte where RowNum>1
 --     -------------------------------------------------------------------------------------------------------------------------  Insert duplicate rows into [PrimaryLabelsDuplicates] table for future 	  ----------------------------------------------------------------------------------------------------------------------
 --     INSERT INTO [dbo].[PrimaryLabelsDuplicates](
	--   [PrimaryLabelsID]
 --     ,[ConsignmentID]
 --     ,[SourceReference]
 --     ,[RevenueType]
 --     ,[AccountCode]
 --     ,[LabelNumber]
 --     ,[ProductType]
 --     ,[ServiceCode]
 --     ,[FirstScannedBy]
 --     ,[FirstDateTime]
 --     ,[FirstScanType]
 --     ,[PickupScannedBy]
 --     ,[PickupDateTime]
 --     ,[InDepotScannedBy]
 --     ,[InDepotDateTime]
 --     ,[HandoverScannedBy]
 --     ,[HandoverDateTime]
 --     ,[TransferScannedBy]
 --     ,[TransferDateTime]
 --     ,[ConsolidateScannedBy]
 --     ,[ConsolidateDateTime]
 --     ,[ConsolidatedBarcode]
 --     ,[DeConsolidateScannedBy]
 --     ,[DeConsolidateDateTime]
 --     ,[OutforDeliveryScannedBy]
 --     ,[OutForDeliverDateTime]
 --     ,[DelayofDeliveryScannedBy]
 --     ,[DelayofDeliveryScannedDateTime]
 --     ,[DelayofDeliveryDate]
 --     ,[QuerycageScannedBy]
 --     ,[QuerycageDateTime]
 --     ,[QueryCageNumber]
 --     ,[AttemptedDeliveryScannedBy]
 --     ,[AttemptedDeliveryDateTime]
 --     ,[AttemptedDeliveryReceivedDatetime]
 --     ,[AttemptedDeliveryCardNumber]
 --     ,[CardCategory]
 --     ,[AcceptedbyPOPPointStatus]
 --     ,[AcceptedbyPOPPointScannedBy]
 --     ,[AcceptedbyPOPPointScannedDateTime]
 --     ,[DeliveryOptionId]
 --     ,[DeliveryScannedBy]
 --     ,[DeliveryDateTime]
 --     ,[DeliveryReceivedDateTime]
 --     ,[IsPODPresent]
 --     ,[PODDateTime]
 --     ,[DeliveryContractorType]
 --     ,[DeliveryContractorName]
 --     ,[LastScannedBy]
 --     ,[LastDateTime]
 --     ,[LastScanType]
 --     ,[PickupETAZone]
 --     ,[DeliveryETAZone]
 --     ,[BUCode]
 --     ,[NetworkCategoryID]
 --     ,[ETADate]
 --     ,[OnTimeStatusId]
 --     ,[ETARuletoprocess]
 --     ,[Reasoncode]
 --     ,[PerformanceReportingDate]
 --     ,[PerformanceProcessed]
 --     ,[isExceptionapplied]
 --     ,[AddWho]
 --     ,[LabelCreatedDateTime]
 --     ,[AddDateTime]
 --     ,[PerformanceProcessedDate]
 --     ,[ProductSubCategory]
 --     ,[AttemptedDeliveryReason]) 
	--  SELECT  [PrimaryLabelsID]
 --     ,[ConsignmentID]
 --     ,[SourceReference]
 --     ,[RevenueType]
 --     ,[AccountCode]
 --     ,[LabelNumber]
 --     ,[ProductType]
 --     ,[ServiceCode]
 --     ,[FirstScannedBy]
 --     ,[FirstDateTime]
 --     ,[FirstScanType]
 --     ,[PickupScannedBy]
 --     ,[PickupDateTime]
 --     ,[InDepotScannedBy]
 --     ,[InDepotDateTime]
 --     ,[HandoverScannedBy]
 --     ,[HandoverDateTime]
 --     ,[TransferScannedBy]
 --     ,[TransferDateTime]
 --     ,[ConsolidateScannedBy]
 --     ,[ConsolidateDateTime]
 --     ,[ConsolidatedBarcode]
 --     ,[DeConsolidateScannedBy]
 --     ,[DeConsolidateDateTime]
 --     ,[OutforDeliveryScannedBy]
 --     ,[OutForDeliverDateTime]
 --     ,[DelayofDeliveryScannedBy]
 --     ,[DelayofDeliveryScannedDateTime]
 --     ,[DelayofDeliveryDate]
 --     ,[QuerycageScannedBy]
 --     ,[QuerycageDateTime]
 --     ,[QueryCageNumber]
 --     ,[AttemptedDeliveryScannedBy]
 --     ,[AttemptedDeliveryDateTime]
 --     ,[AttemptedDeliveryReceivedDatetime]
 --     ,[AttemptedDeliveryCardNumber]
 --     ,[CardCategory]
 --     ,[AcceptedbyPOPPointStatus]
 --     ,[AcceptedbyPOPPointScannedBy]
 --     ,[AcceptedbyPOPPointScannedDateTime]
 --     ,[DeliveryOptionId]
 --     ,[DeliveryScannedBy]
 --     ,[DeliveryDateTime]
 --     ,[DeliveryReceivedDateTime]
 --     ,[IsPODPresent]
 --     ,[PODDateTime]
 --     ,[DeliveryContractorType]
 --     ,[DeliveryContractorName]
 --     ,[LastScannedBy]
 --     ,[LastDateTime]
 --     ,[LastScanType]
 --     ,[PickupETAZone]
 --     ,[DeliveryETAZone]
 --     ,[BUCode]
 --     ,[NetworkCategoryID]
 --     ,[ETADate]
 --     ,[OnTimeStatusId]
 --     ,[ETARuletoprocess]
 --     ,[Reasoncode]
 --     ,[PerformanceReportingDate]
 --     ,[PerformanceProcessed]
 --     ,[isExceptionapplied]
 --     ,[AddWho]
 --     ,[LabelCreatedDateTime]
 --     ,[AddDateTime]
 --     ,[PerformanceProcessedDate]
 --     ,[ProductSubCategory]
 --     ,[AttemptedDeliveryReason]
 -- FROM Cte
 -- Where RowNum>1;

  --------------------------------
  --Delete Duplicate Records
  --------------------------------
 DELETE FROM [dbo].[PrimaryLabels]  where [PrimaryLabelsID] IN (
 SELECT a.[PrimaryLabelsID] FROM 
  (
  Select [PrimaryLabelsID], ConsignmentId,LabelNumber, ROW_NUMBER() OVER(PARTITION BY  LabelNumber ORDER BY [PrimaryLabelsID] ) AS RowNumber from [dbo].[PrimaryLabels]  
 )a
 Where a.RowNumber>1
 )


End
GO
