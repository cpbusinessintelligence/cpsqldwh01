SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[sp_DeletePerformancereportingData](@Date date) as
begin

--declare @Date date
--set @Date='2017-02-02'


   Declare @StartDate as DateTime
   Declare @EndDate as DateTime


 Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)


   Delete
   FROM [dbo].[PrimaryLabels] WHere LabelCreatedDateTime >= @StartDate and LabelCreatedDateTime <= @EndDate

   Delete
   FROM [dbo].[PrimaryLabelsMinus1day] WHere LabelCreatedDateTime >= @StartDate and LabelCreatedDateTime <= @EndDate

Update [dbo].[PrimaryLabels] SET [OnTimeStatusId] = Null ,
                                          PerformanceProcessed = 0 , 
										  PerformanceReportingDate = Null ,
										  PerformanceProcessedDate=Null,
										  ETADate = Null,
										  Reasoncode=Null,
										  ETARuletoprocess=Null
						Where PerformanceprocessedDate = @Date


    Delete 
  FROM [dbo].[LinehaulData]  where  convert(date,DeConsolidateDateTime)= @Date


Delete [dbo].[DailyPerformanceReporting]
  FROM [dbo].[DailyPerformanceReporting] P Join dbo.DimCalendarDate C on  P.PerformanceDayID = C.Id 
  Where C.Date = @Date



  Delete [dbo].[DailyNoTimeReporting]
  FROM [dbo].[DailyNoTimeReporting] P Join dbo.DimCalendarDate C on  P.NoTimeReportingDayID = C.Id 
  Where C.Date = @Date


    Delete [dbo].[DailyAgentReporting]
  FROM [dbo].[DailyAgentReporting] P Join dbo.DimCalendarDate C on  P.AgentDayID = C.Id 
  Where C.Date = @Date


      Delete 
  FROM [dbo].[DailyNoTimeReportingDetail]  where PerformanceReportingDate = @Date

delete from [dbo].[InternationalConsignments] where convert(Date,ConNoteCreatedDate)=@Date

delete from [dbo].[InternationalLabels] where convert(Date,LabelCreatedDateTime)=@Date

  delete from [dbo].[DeliveryOptions] where convert(Date,scandatetime)=@Date


	---Added By PV on 2020-10-15
	--Delete [dbo].[DailyMissedPickupReporting]
	--FROM [dbo].[DailyMissedPickupReporting] P 
	--	Join dbo.DimCalendarDate C on  P.MissedPickupReportingDayID = C.Id 
	--Where C.[Date] = @Date

	--Delete From [dbo].[DailyMissedPickupReportingDetail]  Where PerformanceReportingDate = @Date
  -----Ends here -- Addition on 2020-10-15
  end
GO
