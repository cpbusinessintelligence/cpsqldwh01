SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc sp_DeliveryItemLabelArchive_SS(@CreatedDatetime Datetime)
AS

Begin Transaction Delivery

Insert into [dbo].[DeliveryItemLabel_Archive]
Select * from [dbo].[DeliveryItemLabel]
where CreatedDatetime <= @CreatedDatetime

Delete from [dbo].[DeliveryItemLabel]
where CreatedDatetime <= @CreatedDatetime

Commit Transaction Delivery

GO
