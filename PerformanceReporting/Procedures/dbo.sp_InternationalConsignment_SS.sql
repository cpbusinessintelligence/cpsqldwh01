SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc sp_InternationalConsignment_SS (@AddDatetime DateTime)
as


Begin transaction PerfRep3

Insert into [dbo].[InternationalConsignments_Archive_17-18]
select * from [dbo].[InternationalConsignments]
where [AddDateTime] < = @AddDatetime

Delete from [dbo].[InternationalConsignments]
where [AddDateTime] < = @AddDatetime


Commit transaction PerfRep3

GO
