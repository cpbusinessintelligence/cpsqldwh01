SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE proc [dbo].[sp_LoadConsignmentsandLabels_Bkup_AH_04122020]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadConsignmentsandLabels]
    --' ---------------------------
    --' Purpose: sp_LoadConsignmentsandLabels-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/11/2016    AK      1.00    Created the procedure                            
	-- 12/09/2018     SS      1.01    Label exception capture
    --'=====================================================================

   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)



   --------------------------
   ----- PREPAID-------------
   --------------------------
   --Getting all Valid Prepaid Coupon Type into memory
   
   SELECT [Code]
          ,[Category]
         ,[RevenueCategory]
	INTO #TempPrepaidProducts
    FROM [dbo].[DimProduct] where RevenueType = 'Prepaid'
 PRINT 'Loaded all Products int tempTable'

 --Getting all  Prepaid Coupons which had activity

   SELECT  LTRIM(RTRIM(A.SerialNumber)) as SerialNumber,
           LEFT(LTRIM(RTRIM(A.SerialNumber)), 3) as Prefix,
		   MIN(ActivityDateTime) as ActivityDateTime
   INTO #TempActivityImport
   FROM Cosmos.dbo.ActivityImport  A(NOLOCK)
   WHERE  CONVERT(date, A.ActivityDateTime) >= @StartDate
		 AND  CONVERT(date, A.ActivityDateTime) <= @EndDate 
		 AND  ISNUMERIC(A.SerialNumber) = 1
		 AND  LEN(LTRIM(RTRIM(A.SerialNumber))) = 11	                 
   Group By LTRIM(RTRIM(A.SerialNumber))
 PRINT 'Loaded all Coupons which had Activity into temp table'

   
   INSERT INTO [dbo].[Consignment]
                ([RevenueType],[Sourcereference],[ProductType],[ServiceType],[ConNote],[ConNoteCreatedDate],[ItemCount],[CouponCount],[AddWho],[AddDateTime])
	      Select 'Prepaid','Activity',T.Prefix,'', SerialNumber,ActivityDateTime,1,1,'Sys',Getdate()
	      from #TempActivityImport T Join #TempPrepaidProducts P on T.Prefix=P.[Code]
	       where P.RevenueCategory = 'PRIMARY'
		      AND NOT EXISTS (SELECT 1  FROM [dbo].[Consignment] C  WHERE C.ConNote = LTRIM(RTRIM(T.SerialNumber)))
   PRINT 'Inserted all Prepaid Coupons into Consigment Table'  


	INSERT INTO [dbo].[PrimaryLabels]
           ([RevenueType],[SourceReference],[LabelNumber],[AddWho],[LabelCreatedDateTime],[AddDateTime])
	   Select 'Prepaid','Activity', SerialNumber,'Sys',ActivityDateTime,Getdate()
	   from #TempActivityImport T Join #TempPrepaidProducts P on T.Prefix=P.[Code]
	   where P.RevenueCategory = 'PRIMARY'
		      AND NOT EXISTS (SELECT 1  FROM [dbo].[PrimaryLabels] C  WHERE C.LabelNumber = LTRIM(RTRIM(T.SerialNumber)))
   PRINT 'Inserted all Prepaid Coupons into PrimaryLabels Table'  
			  
	INSERT INTO [dbo].[SecondaryLabels]
           ([SecondaryLabelNumber],[SecondaryLabelPrefix],[LabelCreatedDateTime] ,[AddWho],[AddDateTime])
        Select SerialNumber,Left(SerialNumber,3),ActivityDateTime,'Sys',Getdate()
	    from #TempActivityImport T Join #TempPrepaidProducts P on T.Prefix=P.[Code]
	     where P.RevenueCategory <> 'PRIMARY'
		      AND NOT EXISTS (SELECT 1  FROM [dbo].[SecondaryLabels] S  WHERE S.SecondaryLabelNumber = LTRIM(RTRIM(T.SerialNumber)))
   PRINT 'Inserted all Prepaid Coupons into SecondaryLabels Table'  

--------------------
------------EDI-----
-------------------

-- Insert Consignment Exceptions

   INSERT INTO [dbo].[Consignment_Exceptions]([GWConsignmentID],[RevenueType],[SourceReference],[ProductType],[ServiceType],[ConNote],[CustomerConNoteDate]
							      ,[ConNoteCreatedDate],[CustomerETADate],[AccountCode],[PickupAddress] 
								  ,[ItemCount],[CouponCount],[AddWho],[AddDateTime])
                           SELECT  cd_id,'EDI','EDIGW',[cd_pricecode],'',Rtrim(Ltrim([cd_connote])),CONVERT(Date,[cd_consignment_date]) 
						          ,CONVERT(Date,[cd_date]),CONVERT(Date,cd_customer_eta),[cd_account],[cd_pickup_addr0]
                                  ,[cd_items],[cd_coupons],'Sys',Getdate()
                           FROM [CpplEDI].[dbo].[consignment] P
						   WHERE CONVERT(Date,[cd_date]) >= @StartDate 
						     and CONVERT(Date,[cd_date]) <=@EndDate
							 and cd_account not in ('112995527','112995519') and len(Rtrim(Ltrim([cd_connote])))>50
							 AND NOT EXISTS (SELECT 1  FROM [dbo].[Consignment] C  WHERE C.ConNote = LTRIM(RTRIM(P.cd_connote)))  

--Consignment insertions
   
   INSERT INTO [dbo].[Consignment]([GWConsignmentID],[RevenueType],[SourceReference],[ProductType],[ServiceType],[ConNote],[CustomerConNoteDate]
							      ,[ConNoteCreatedDate],[CustomerETADate],[AccountCode],[PickupAddress] 
								  ,[ItemCount],[CouponCount],[AddWho],[AddDateTime])
                           SELECT  cd_id,'EDI','EDIGW',[cd_pricecode],'',Rtrim(Ltrim([cd_connote])),CONVERT(Date,[cd_consignment_date]) 
						          ,CONVERT(Date,[cd_date]),CONVERT(Date,cd_customer_eta),[cd_account],[cd_pickup_addr0]
                                  ,[cd_items],[cd_coupons],'Sys',Getdate()
                           FROM [CpplEDI].[dbo].[consignment] P
						   WHERE CONVERT(Date,[cd_date]) >= @StartDate 
						     and CONVERT(Date,[cd_date]) <=@EndDate
							 and cd_account not in ('112995527','112995519') and len(Rtrim(Ltrim([cd_connote])))<50
							 AND NOT EXISTS (SELECT 1  FROM [dbo].[Consignment] C  WHERE C.ConNote = LTRIM(RTRIM(P.cd_connote)))  

			--				-- and P.cd_id not in (57637920,57637952,57615754,57750901,57751005,57751891)
	   PRINT 'Inserted all EDI Cons into Consigment Table'  	

	   -- Label Exceptions -- Sinshith

	      INSERT INTO [dbo].[PrimaryLabels_Exceptions]([ConsignmentID],[RevenueType],[SourceReference],CardCategory,[AccountCode],[LabelNumber],Servicecode,[DeliveryContractorType],[DeliveryContractorName],[PickupETAZone],[DeliveryETAZone],[AddWho],[LabelCreatedDateTime],[AddDateTime])
						  SELECT C.cd_id,'EDI','EDIGW',case when cd_delivery_addr1 like 'C/- %POPStation%' then '1st Time Delivery to POPStation' when  cd_delivery_addr1 like '%NewsAgency%' then '1st Time Delivery to HUBBED' else '' end ,C.cd_account,Rtrim(Ltrim(P.cc_coupon)),cd_pricecode,CASE A.a_cppl WHEN 'N' THEN 'Agent' ELSE '' END,a.a_name,[dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_pickup_postcode])),Rtrim(Ltrim(C.[cd_pickup_suburb]))) 
						  , Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_delivery_postcode])),Rtrim(Ltrim(C.[cd_delivery_suburb]))),'')
						   ,'Sys',C.[cd_date],getdate()
                           FROM [CpplEDI].[dbo].[consignment] C Join CpplEDI.dbo.cdcoupon P On C.cd_id = P.cc_consignment
						                                        Left Join CpplEDI.dbo.agents A on C.cd_agent_id = A.A_id
						   WHERE CONVERT(Date,[cd_date]) >= @StartDate 
						     and CONVERT(Date,[cd_date]) <=@EndDate
							 and Not (ISnumeric([cd_connote])=1 and len([cd_connote]) =11)
							 and cd_account not in ('112995527','112995519') and len(Rtrim(Ltrim(P.cc_coupon)))>30
                             AND NOT EXISTS (SELECT 1  FROM [dbo].[PrimaryLabels] C  WHERE C.LabelNumber = LTRIM(RTRIM(P.cc_coupon)))
    PRINT 'Inserted all Labels Exceptions into PrimaryLabels Table' 
	   
			      

   INSERT INTO [dbo].[PrimaryLabels]([ConsignmentID],[RevenueType],[SourceReference],CardCategory,[AccountCode],[LabelNumber],Servicecode,[DeliveryContractorType],[DeliveryContractorName],[PickupETAZone],[DeliveryETAZone],[AddWho],[LabelCreatedDateTime],[AddDateTime])
						  SELECT C.cd_id,'EDI','EDIGW',case when cd_delivery_addr1 like 'C/- %POPStation%' then '1st Time Delivery to POPStation' when  cd_delivery_addr1 like '%NewsAgency%' then '1st Time Delivery to HUBBED' else '' end ,C.cd_account,Rtrim(Ltrim(P.cc_coupon)),cd_pricecode,CASE A.a_cppl WHEN 'N' THEN 'Agent' ELSE '' END,a.a_name,[dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_pickup_postcode])),Rtrim(Ltrim(C.[cd_pickup_suburb]))) 
						  , Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_delivery_postcode])),Rtrim(Ltrim(C.[cd_delivery_suburb]))),'')
						   ,'Sys',C.[cd_date],getdate()
                           FROM [CpplEDI].[dbo].[consignment] C Join CpplEDI.dbo.cdcoupon P On C.cd_id = P.cc_consignment
						                                        Left Join CpplEDI.dbo.agents A on C.cd_agent_id = A.A_id
						   WHERE CONVERT(Date,[cd_date]) >= @StartDate 
						     and CONVERT(Date,[cd_date]) <=@EndDate
							 and Not (ISnumeric([cd_connote])=1 and len([cd_connote]) =11)
							 and cd_account not in ('112995527','112995519') and len(Rtrim(Ltrim(P.cc_coupon)))<31
                             AND NOT EXISTS (SELECT 1  FROM [dbo].[PrimaryLabels] C  WHERE C.LabelNumber = LTRIM(RTRIM(P.cc_coupon)))
    PRINT 'Inserted all EDI Labels into PrimaryLabels Table' 


-------------------	
---WEBSITE Domestic--
-------------------

   INSERT INTO [dbo].[Consignment]([GWConsignmentID],[RevenueType],[SourceReference],[ProductType],[ServiceType],[ConNote],[CustomerConNoteDate]
							      ,[ConNoteCreatedDate],[CustomerETADate],[AccountCode],[PickupAddress] 
								  ,[ItemCount],[CouponCount],[AddWho],[AddDateTime])
                           
						   SELECT  c.ConsignmentID,'EDI',case when isnull(clientcode,'')='CPAPI' then 'WEBAPIDomestic' else 'WEBDomestic' end,c.RateCardID,'',Rtrim(Ltrim(c.consignmentcode)),convert(date,c.createddatetime),
						            convert(date,c.createddatetime),'',case when isnull(c1.AccountNumber,'')='' then '112995527' else c1.AccountNumber end,
									a.[CompanyName],c.NoOfItems,c.NoOfItems,'Sys',Getdate()

						   from EzyFreight.dbo.tblconsignment c with (Nolock)
						                                        left join EzyFreight.dbo.tblcompanyusers u on u.userid=c.userid
																left join EzyFreight.dbo.tblcompany c1 on c1.companyid=u.companyid
																left join EzyFreight.dbo.tbladdress a on a.addressid=c.pickupid
						   
						   	   WHERE convert(date,c.createddatetime) >= @StartDate 
						     and convert(date,c.createddatetime) <=@EndDate
							 and c.RateCardID not like 'SAV%' and c.RateCardID not like 'EXP%'
							 AND NOT EXISTS (SELECT 1  FROM [dbo].[Consignment] m  WHERE m.ConNote = LTRIM(RTRIM(c.consignmentcode)))
						   
	   PRINT 'Inserted all WEBSITE Domestic Cons into Consigment Table'  					      

   INSERT INTO [dbo].[PrimaryLabels]([ConsignmentID],[RevenueType],[SourceReference],[CardCategory],[AccountCode],[LabelNumber],Servicecode,[DeliveryContractorType],[DeliveryContractorName],[PickupETAZone],[DeliveryETAZone],[AddWho],[LabelCreatedDateTime],[AddDateTime])
						  SELECT  c.ConsignmentID,'EDI',case when isnull(clientcode,'')='CPAPI' then 'WEBAPIDomestic' else 'WEBDomestic' end,case when a.address1 like 'C/- %POPStation%' then '1st Time Delivery to POPStation' when  a.address1 like '%NewsAgency%' then '1st Time Delivery to HUBBED' else '' end ,case when isnull(c1.AccountNumber,'')='' then '112995527' else c1.AccountNumber end,Rtrim(Ltrim(l.labelnumber)),c.ratecardid,CASE A2.a_cppl WHEN 'N' THEN 'Agent' ELSE '' END,A2.[a_name],[dbo].[fn_GetETAZone](Rtrim(Ltrim(a.[postcode])),Rtrim(Ltrim(a.[suburb]))) 
						  , Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(a1.postcode)),Rtrim(Ltrim(a1.suburb))),'')
						   ,'Sys',convert(date,c.createddatetime),getdate()
                            from EzyFreight.dbo.tblconsignment c with (Nolock) join EzyFreight.dbo.tblitemlabel l on c.consignmentid=l.consignmentid
						                                        left join EzyFreight.dbo.tblcompanyusers u on u.userid=c.userid
																left join EzyFreight.dbo.tblcompany c1 on c1.companyid=u.companyid
																left join EzyFreight.dbo.tbladdress a on a.addressid=c.pickupid
																left join EzyFreight.dbo.tbladdress a1 on a1.addressid=c.destinationid
																Left Join CpplEDI.dbo.consignment c2 on c2.cd_connote=c.ConsignmentCode
						                                        Left Join CpplEDI.dbo.agents A2 on C2.cd_agent_id = A2.A_id
						   	   WHERE convert(date,c.createddatetime) >= @StartDate 
						     and convert(date,c.createddatetime) <=@EndDate
							 and c.RateCardID not like 'SAV%' and c.RateCardID not like 'EXP%'
                             AND NOT EXISTS (SELECT 1  FROM [dbo].[PrimaryLabels] m  WHERE m.LabelNumber = Rtrim(Ltrim(l.labelnumber)))
    PRINT 'Inserted all WEBSITE Domestic Labels into PrimaryLabels Table'  
	
	
update [PrimaryLabels] set ProductType='Standard'

 update [PrimaryLabels] set ProductType=p.category  from [PrimaryLabels] l join [dbo].[DimProduct] p on l.servicecode=p.Code
 where p.Category in ('ACE','Domestic Priority','Domestic Off Peak')

 update Primarylabels set ProductSubCategory=pc_category
 from Primarylabels join [dbo].[Cost_Model_PriceCode_Classifictions] on pc_code=servicecode and ltrim(rtrim(pricecategory))=ltrim(rtrim(revenuetype))
where servicecode is not null  and ProductSubCategory is null


 update Primarylabels set ProductSubCategory=pc_category
 from [dbo].[Cost_Model_PriceCode_Classifictions] where pc_code=left(ltrim(rtrim(labelnumber)),3) and ltrim(rtrim(pricecategory))=ltrim(rtrim(revenuetype))
 and   ProductSubCategory is null
 --and Category='Prepaid' and revenuetype='Prepaid'
--and servicecode is not null

update [PrimaryLabels] set [DeliveryContractorName]=a_name from Primarylabels l join cpplEDI.dbo.cdcoupon on cc_coupon=labelnumber 
                                                                                join cpplEDI.dbo.consignment on cc_consignment=cd_id 
	                                                                            left join cpplEDI.dbo.agents a on cd_agent_id=a_id
where [DeliveryContractorName] is null

--select * from cpsqlrct01.[OperationalReporting].[dbo].[Cost_Model_PriceCode_Classifictions]
	
-------------------	
---WEBSITE International--
-------------------	
	


   INSERT INTO [dbo].[InternationalLabels]([ConsignmentID],Sourcereference,[AccountCode],[LabelNumber],[Producttype],[FromCountry],[FromState],[ToCountry],[ToState],[AddWho],[LabelCreatedDateTime],[AddDateTime])
						  SELECT  c.ConsignmentID,case when isnull(clientcode,'')='CPAPI' then 'WEBAPIInternational' else 'WEBInternational' end,case when isnull(c1.AccountNumber,'')='' then '112995519' else c1.AccountNumber end,Rtrim(Ltrim(l.labelnumber)),
						          case when c.ratecardid like 'EXP%' then 'Express' when c.ratecardid like 'SAV%' then 'Saver'  else 'Unknown' end,
						          case when isnull(a.country,a.countrycode) is null then 'Australia' else a.country end,isnull(a.statename,s.statecode),
						          case when isnull(a1.country,a1.countrycode) is null then 'Australia' else a1.country end,isnull(a1.statename,s1.statecode),'Sys',convert(date,c.createddatetime),getdate()
						 
                            from EzyFreight.dbo.tblconsignment c with (Nolock) join EzyFreight.dbo.tblitemlabel l on c.consignmentid=l.consignmentid
						                                        left join EzyFreight.dbo.tblcompanyusers u on u.userid=c.userid
																left join EzyFreight.dbo.tblcompany c1 on c1.companyid=u.companyid
																left join EzyFreight.dbo.tbladdress a on a.addressid=c.pickupid
																left join EzyFreight.dbo.tblstate s on s.stateid=a.stateid
																left join EzyFreight.dbo.tbladdress a1 on a1.addressid=c.destinationid
																left join EzyFreight.dbo.tblstate s1 on s1.stateid=a1.stateid
																Left Join CpplEDI.dbo.consignment c2 on c2.cd_connote=c.ConsignmentCode
						                                        Left Join CpplEDI.dbo.agents A2 on C2.cd_agent_id = A2.A_id
						   	     WHERE convert(date,c.createddatetime) >= @StartDate 
						     and convert(date,c.createddatetime) <=@EndDate
							 and (c.RateCardID  like 'SAV%' or c.RateCardID  like 'EXP%')
                             AND NOT EXISTS (SELECT 1  FROM [dbo].[InternationalLabels] m  WHERE m.LabelNumber = Rtrim(Ltrim(l.labelnumber)))
    PRINT 'Inserted all WEBSITE International Labels into InternationalLabels Table'  

	Update [InternationalLabels] set Category=case when [FromCountry]='Australia' then 'OUTBOUND' when ToCountry='Australia' then 'INBOUND' else 'Unknown' end


	Insert into InternationalConsignments([GWConsignmentID],[ProductType],[Category] ,Sourcereference,[ConNote],[ConNoteCreatedDate],[AccountCode],[PickupAddress],[ItemCount],[CouponCount],[AddWho],[AddDateTime])
	SELECT                 c.ConsignmentID
	                       , case when c.ratecardid like 'EXP%' then 'Express' when c.ratecardid like 'SAV%' then 'Saver'  else 'Unknown' end
	                       , case when (case when isnull(a.country,a.countrycode) is null then 'Australia' else a.country end) ='Australia' then 'OUTBOUND'
						          when (case when isnull(a1.country,a1.countrycode) is null then 'Australia' else a1.country end) ='Australia' then 'INBOUND'
								   else 'Unknown' end

	                      ,case when isnull(clientcode,'')='CPAPI' then 'WEBAPIInternational' else 'WEBInternational' end
						  ,Rtrim(Ltrim(c.consignmentcode))
						  ,convert(date,c.createddatetime)
						  ,case when isnull(c1.AccountNumber,'')='' then '112995527' else c1.AccountNumber end
						  ,a.[CompanyName]
						  ,c.NoOfItems
						  ,c.NoOfItems
						  ,'Sys'
						  ,Getdate()
                           from EzyFreight.dbo.tblconsignment c with (Nolock) join EzyFreight.dbo.tblitemlabel l  on c.consignmentid=l.consignmentid
						                                        left join EzyFreight.dbo.tblcompanyusers u on u.userid=c.userid
																left join EzyFreight.dbo.tblcompany c1 on c1.companyid=u.companyid
																left join EzyFreight.dbo.tbladdress a on a.addressid=c.pickupid
																left join EzyFreight.dbo.tbladdress a1 on a1.addressid=c.destinationid
																Left Join CpplEDI.dbo.consignment c2 on c2.cd_connote=c.ConsignmentCode
						                                        Left Join CpplEDI.dbo.agents A2 on C2.cd_agent_id = A2.A_id
						   	   WHERE convert(date,c.createddatetime) >= @StartDate 
						     and convert(date,c.createddatetime) <=@EndDate
							  and c.RateCardID  like 'SAV%' or c.RateCardID  like 'EXP%'



	------------------------------------------------------
	 

 Update [dbo].[PrimaryLabels] SET [DeliveryETAZone] = Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_delivery_postcode])),Rtrim(Ltrim(C.[cd_delivery_addr2]))),'')  
                FROM [dbo].[PrimaryLabels] P  Join [CpplEDI].[dbo].[consignment] C on P.[ConsignmentID] = C.Cd_id 
                Where Isnull(P.[DeliveryETAZone],'') = '' and P.Sourcereference = 'EDIGW' and P.[LabelCreatedDateTime] >= @StartDate and P.[LabelCreatedDateTime] <=@EndDate
  PRINT 'Calculated [DeliveryETAZone] for EDI from DeliveryAddress2 '  
 
 Update [dbo].[PrimaryLabels] SET [DeliveryETAZone] = Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_delivery_postcode])),Rtrim(Ltrim(C.[cd_delivery_addr3]))),'')  
                FROM [dbo].[PrimaryLabels] P  Join [CpplEDI].[dbo].[consignment] C on P.[ConsignmentID] = C.Cd_id 
                Where ISnull(P.[DeliveryETAZone],'') = '' and P.Sourcereference = 'EDIGW' and P.[LabelCreatedDateTime] >= @StartDate and P.[LabelCreatedDateTime] <=@EndDate
  PRINT 'Calculated [DeliveryETAZone] for EDI from DeliveryAddress3 '  

 Update [dbo].[PrimaryLabels] SET PickupETAZone = Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(C.[cd_pickup_postcode])),Rtrim(Ltrim(C.[cd_pickup_addr2]))),'')  
                FROM [dbo].[PrimaryLabels] P  Join [CpplEDI].[dbo].[consignment] C on P.[ConsignmentID] = C.Cd_id 
                Where Isnull(P.PickupETAZone,'') = '' and P.Sourcereference = 'EDIGW' and  P.[LabelCreatedDateTime] >= @StartDate and P.[LabelCreatedDateTime] <=@EndDate
  PRINT 'Calculated [PickupETAZone] for EDI from [cd_pickup_addr2] '  
end


GO
