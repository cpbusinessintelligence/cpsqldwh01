SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE proc [dbo].[sp_LoadDailyAgentReporting]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadDailyPerformanceReporting]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 06 Dec 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 06/12/2016    AK      1.00    Created the procedure                            

    --'=====================================================================
   Declare @CalendarDayId as Integer = ISnull((SELECT [Id] FROM [dbo].[DimCalendarDate] where Date = @Date),0)
   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)
   
   INSERT INTO [dbo].[DailyAgentReporting]
                 (AgentDayID,[RevenueType],[ProductType],ProductSubCategory,[AccountCode],AgentName,[DeliveryDriver],[PickUpDriver],[OnTimeStatus]
				 ,[PickupETAZone],[DeliveryETAZone],[NetworkCategoryID],[BUCode]
				 ,[PerformanceCount],[AddWho],[AddDateTime])
   Select @CalendarDayId
        , RevenueType
		,[ProductType]
		,ProductSubCategory
		,[AccountCode]
		,isnull(DeliveryContractorName,'') as DeliveryContractorName
		, Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'')
		 ,ISNull(PickupSCannedBy,'')
		 ,[OnTimeStatusId]
		 ,[PickupETAZone]
		 ,[DeliveryETAZone]
		 ,[NetworkCategoryID]
		 ,[BUCode]
		 ,COUNT(*)
		 ,'Sys'
		 ,Getdate()
   From [dbo].[PrimaryLabels] 
   Where [PerformanceprocessedDate] = @Date
     and [PerformanceProcessed] =1 and (isnull([CardCategory] ,'')='' or isnull([CardCategory] ,'') like '%1st%')
	 and OnTimeStatusId in (6,7)
   Group by  RevenueType
		,[ProductType]
		,ProductSubCategory
		,[AccountCode]
		,isnull(DeliveryContractorName,'') 
		, Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'')
		 ,ISNull(PickupSCannedBy,'')
		 ,[OnTimeStatusId]
		 ,[PickupETAZone]
		 ,[DeliveryETAZone]
		 ,[NetworkCategoryID]
		 ,[BUCode]
end




GO
