SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-09-17
-- Description:	
-- =============================================
--exec [dbo].[sp_LoadDailyFutileMissedPartialPickup_Detail] '2020-09-05'
CREATE PROCEDURE [dbo].[sp_LoadDailyFutileMissedPartialPickup_Detail] (@Date Date) 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Drop table #tempreport
	--Drop table #tempPartialCG
	--Declare @consignmentstatus Varchar(50) = 'Futile,Missed Pickup,Picked up'
	--	,@Customeraccount varchar(50) = 'ALL'
	--Declare @StartDate Date = '2020-09-04'
	--	,@EndDate Date = '2020-09-04'
	--	,@state varchar(50)	= 'NSW,SA,QLD,WA,VIC'
	
	Declare @StartDate as DateTime
	Declare @EndDate as DateTime

	--Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
	--Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)
	Select @StartDate = @Date
	Select @EndDate = @Date

	/************************/
	Select --cd_pickup_stamp,cd_connote,cp.cc_coupon, TE.EventTypeId
		Distinct
		c.cd_id,
		c.cd_account,
		c.cd_connote,
		c.cd_pricecode,
		cd_pickup_addr0,
		cd_pickup_addr1,
		cd_pickup_addr2,
		cd_pickup_addr3,
		cd_pickup_suburb,
		cd_pickup_postcode,
		cd_last_status,
		cd_date,
		cd_pickup_branch
	Into #tempPartialCG
	From CpplEDI..consignment c (nolock) 
	Join cppledi..cdcoupon cp (nolock) On c.cd_id = cp.cc_consignment
	Left Join ScannerGateway..TrackingEvent TE (nolock) On cp.cc_coupon =  TE.SourceReference
	where 
		c.cd_date >=@StartDate and c.cd_date <= @EndDate
		and cd_pickup_stamp is not null 
		and TE.EventTypeId Is Null
	/***********************/

    -- Insert statements for procedure here
	Select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		cd.cb_cosmos_job [Booking Number],
		cd.cb_cosmos_date [Booking Date],
		cd_pickup_addr0 as "Pickup Customer Name",
		cd_pickup_addr1 as "Pickup Address1",
		cd_pickup_addr2 as "Pickup Address2",
		cd_pickup_addr3 as "Pickup Address3",
		cd_pickup_suburb as "Pickup Suburb",
		cd_pickup_postcode as "Pickup Postcode",
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Customer PU' as [Customer PU/Hubbed PU],
		'' as [Hubbed location],
		Case cd_last_status When '' Then 'Missed Pickup' When 'Futile' Then 'Futile' Else 'Picked Up' End As [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
			when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
			when  b.b_name='Melbourne' then 'VIC'
			when b.b_name='Sydney' then 'NSW'
			when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		(CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
		cd_date [Consignment Date],
		'1' [Flow]		
	Into #tempreport
	From CpplEDI.dbo.consignment(nolock) c 
		inner join CpplEDI.dbo.cdcosmosbooking (nolock) cd on cd.cb_consignment=c.cd_id
		inner join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		left join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator (nolock) o on o.Id=bo.allocatedby and o.branch=bo.branch
		left join pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where --cd_last_status in ('Futile','') and 
		bo.IsActive = 1  
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate
	
	Union All
 
	Select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number],
		convert (date,scanTime) [Booking Date],		
		cd_pickup_addr0 as [Pickup Customer Name],
		cd_pickup_addr1 as [Pickup Address1],
		cd_pickup_addr2 as [Pickup Address2],
		cd_pickup_addr3 as [Pickup Address3],
		cd_pickup_suburb as [Pickup Suburb],
		cd_pickup_postcode as [Pickup Postcode],
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Hubbed PU' as [Customer PU/Hubbed PU],
		hb.agentname [Hubbed location],
		Case cd_last_status When '' Then 'Missed Pickup' When 'Futile' Then 'Futile' Else 'Picked Up' End As [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
		when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
		when  b.b_name='Melbourne' then 'VIC'
		when b.b_name='Sydney' then 'NSW'
		when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		bo.bookingnumber  as [Pickup booking],
		cd_date [Consignment Date],
		'2' [Flow]
	From cpplEDI.dbo.consignment(nolock) c 
		inner join CpplEDI.dbo.cdcoupon (nolock)  cc on c.cd_id= cc.cc_consignment
		inner join [ScannerGateway].[dbo].[HubbedStaging] (nolock) hb on  hb.TrackingNumber=cc.cc_coupon
		inner join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		left join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator (nolock) o on o.[Id]=bo.allocatedby and o.Branch = bo.Branch
		left join pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where --cd_last_status in ('Futile','')  and 
		bo.IsActive = 1  
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate

	Union All

	Select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		cd.cb_cosmos_job [Booking Number],
		cd.cb_cosmos_date [Booking Date],
		cd_pickup_addr0 as "Pickup Customer Name",
		cd_pickup_addr1 as "Pickup Address1",
		cd_pickup_addr2 as "Pickup Address2",
		cd_pickup_addr3 as "Pickup Address3",
		cd_pickup_suburb as "Pickup Suburb",
		cd_pickup_postcode as "Pickup Postcode",
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Customer PU' as [Customer PU/Hubbed PU],
		'' as [Hubbed location],
		'Partial Pickup' as [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
			when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
			when  b.b_name='Melbourne' then 'VIC'
			when b.b_name='Sydney' then 'NSW'
			when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		(CASE cd.cb_branch WHEN 6 THEN 'PER' WHEN 5 THEN 'SYD' WHEN 2 THEN 'BNE' WHEN 4 THEN 'MEL' WHEN 3 THEN 'OOL' WHEN 1 THEN 'ADL' ELSE 'XXX' END) + [PerformanceReporting].dbo.[fn_PrefillZeros] (cb_cosmos_job,6)+Convert(varchar(20),Day(cb_cosmos_date))+[PerformanceReporting].dbo.[fn_PrefillZeros] (Convert(varchar(20),Month(cb_cosmos_date)),2)+Convert(varchar(20),Year(cb_cosmos_date)) as [Pickup booking],
		cd_date [Consignment Date]
		,'3' [Flow]
	From #tempPartialCG c 
		inner join CpplEDI.dbo.cdcosmosbooking (nolock) cd on cd.cb_consignment=c.cd_id		
		inner join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		left join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator (nolock) o on o.Id=bo.allocatedby and o.branch=bo.branch
		left join pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where bo.IsActive = 1 
		and cd_last_status not in ('Futile','')
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate	
	Union ALL
	Select	
		Distinct c.cd_account [Customer Account Number],
		c.cd_connote [Consignment Number],
		c.cd_pricecode [Service code],
		d.Shortname [Customer Name],
		rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number],
		convert (date,scanTime) [Booking Date],		
		cd_pickup_addr0 as [Pickup Customer Name],
		cd_pickup_addr1 as [Pickup Address1],
		cd_pickup_addr2 as [Pickup Address2],
		cd_pickup_addr3 as [Pickup Address3],
		cd_pickup_suburb as [Pickup Suburb],
		cd_pickup_postcode as [Pickup Postcode],
		bo.timesent [Job sent time],
		bo.timeaccepted [Job accepted time],
		case when o.name='default' then 'automated' else 'manual' end [Dispatch automated],
		case when b_futile_reason='Failed Pickup - Wrong job' then 'Yes' else 'No' end [wrong Jobbed],
		'Hubbed PU' as [Customer PU/Hubbed PU],
		hb.agentname [Hubbed location],
		case cd_last_status when '' then 'Missed Pickup' else 'Futile' end as [ConsignmentStatus],
		bo.b_futile_stamp [Futile Time Stamp],
		REPLACE(REPLACE(bo.b_futile_reason, CHAR(13), ''), CHAR(10), '') as  [Futile reason],
		bo.DriverId [Driver Number],
		DC.pricingzone as 'ByZone',
		b.b_name [Branch],
		case  when b.b_name='Adelaide' then 'SA'
		when b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
		when  b.b_name='Melbourne' then 'VIC'
		when b.b_name='Sydney' then 'NSW'
		when b.b_name='Perth' then 'WA'
		else 'Unknown' end as [State],
		bo.bookingnumber  as [Pickup booking],
		cd_date [Consignment Date]		
		,'4' [Flow]
	From #tempPartialCG c
		inner join CpplEDI.dbo.cdcoupon (nolock) cc on c.cd_id= cc.cc_consignment
		inner join [ScannerGateway].[dbo].[HubbedStaging] (nolock) hb on  hb.TrackingNumber=cc.cc_coupon
		inner join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		inner join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		left join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		left join [cosmos].[dbo].Operator (nolock) o on o.[Id]=bo.allocatedby and o.Branch = bo.Branch
		left join pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where bo.IsActive = 1  
		and cd_last_status not in ('Futile','')
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate
--------------------------------------------
Select Rank() over (partition by [Consignment Number] order by [Consignment Number],consignmentstatus) [DeleteRowNum], * 
Into #tempDelete
From #tempreport where [Consignment Number] in
(
Select [Consignment Number] From #tempreport
Where ConsignmentStatus in ('Picked Up','Partial Pickup')
group by [Consignment Number]
having count(*) > 1
)
----------------------------------------------------
Delete tr From #tempreport tr
Join #tempDelete td On tr.[Consignment Number] = td.[Consignment Number] and tr.ConsignmentStatus = 'Picked Up'
Where DeleteRowNum = 2
----------------------------------------------------
	Insert Into PerformanceReporting..[DailyMissedPickupReportingDetail]
	(
		[ConsignmentNumber]
		,[RevenueType]
		,[AccountCode]
		,[ServiceCode]
		,[CustomerName]
		,[BookingNumber]
		,[BookingDate]
		,[PickupCustomerName]
		,[PickupAddress1]
		,[PickupAddress2]
		,[PickupAddress3]
		,[PickupSuburb]	
		,[PickupPostcode]
		,[JobSentTime]
		,[JobAcceptedTime]
		,[DispatchAutomated]
		,[WrongJobbed]
		,[CustomerPU/HubbedPU]
		,[HubbedLocation]
		,[consignmentstatus]
		,[FutileTimeStamp]
		,[FutileReason]
		,[DriverNumber]
		,[ByZone]
		,[Branch]
		,[State]
		,[PickupBooking]
		,[ConsignmentDate]
		,[PerformanceReportingDate]
	)
	Select  
		[Consignment Number]
		,'EDI'
		,[Customer Account Number]
		,[Service code]
		,[Customer Name]
		,[Booking Number]
		,[Booking Date]
		,[Pickup Customer Name]
		,[Pickup Address1]
		,[Pickup Address2]
		,[Pickup Address3]
		,[Pickup Suburb]
		,[Pickup Postcode]
		,[Job sent time]
		,[Job accepted time]
		,[Dispatch automated]
		,[wrong Jobbed]
		,[Customer PU/Hubbed PU]
		,[Hubbed location]
		,ConsignmentStatus
		,[Futile Time Stamp]
		,[Futile reason]
		,[Driver Number]
		,[ByZone]
		,[Branch]
		,[State]
		,[Pickup booking]
		,Convert(date,[Consignment Date])
		,@Date
	From #tempreport 		
END
GO
