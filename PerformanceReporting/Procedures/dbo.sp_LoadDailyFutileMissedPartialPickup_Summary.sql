SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-09-11
-- Description:	
-- =============================================
-- [sp_LoadDailyFutileMissedPartialPickup_Summary] '2020-10-15'
CREATE PROCEDURE [dbo].[sp_LoadDailyFutileMissedPartialPickup_Summary] (@Date Date) 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Drop table #tmpReport
	--Drop table #tempPartialCG
	--Drop table #tempAllCust
	--Drop table #tempAllCust2
	--Declare @StartDate Date = '2020-09-04'
	--	,@EndDate Date = '2020-09-04'
	
	Declare @StartDate as DateTime
	Declare @EndDate as DateTime

	Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
	Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)
	--------
    Select 
		Distinct
		c.cd_id,
		c.cd_account,
		c.cd_connote,
		c.cd_pricecode,
		cd_pickup_addr0,
		cd_pickup_addr1,
		cd_pickup_addr2,
		cd_pickup_addr3,
		cd_pickup_suburb,
		cd_pickup_postcode,
		cd_last_status,
		cd_date,
		cd_pickup_branch
	Into #tempPartialCG
	From CpplEDI..consignment c (nolock) 
		Join cppledi..cdcoupon cp (nolock) On c.cd_id = cp.cc_consignment
		Left Join ScannerGateway..TrackingEvent TE (nolock) On cp.cc_coupon =  TE.SourceReference
	where 
		c.cd_date >=@StartDate and c.cd_date <= @EndDate
		and cd_pickup_stamp Is Not Null 
		and TE.EventTypeId Is Null	
	--------
	Select
		Distinct 
			c.cd_account [Customer Account Number]
			, c.cd_connote [Consignment Number]
			, b.b_name [Branch]
			, Case cd_last_status When '' Then 'Missed Pickup' When 'Futile' Then 'Futile' Else 'Picked Up' End As [ConsignmentStatus]
			, Case  When b.b_name='Adelaide' Then 'SA'
					When b.b_name='Brisbane' or b.b_name='Gold Coast' Then 'QLD'
					When  b.b_name='Melbourne' Then 'VIC'
					When b.b_name='Sydney' Then 'NSW'
					When b.b_name='Perth' Then 'WA'
				Else 'Unknown' End As [State]
			, cd_date [Consignment Date]
			, bo.DriverId [Driver Number]
			, cd.cb_cosmos_job [Booking Number]
	Into #tmpReport
	From cpplEDI.dbo.consignment(nolock) c 
		Inner Join CpplEDI.dbo.cdcosmosbooking (nolock) cd on cd.cb_consignment=c.cd_id
		Inner Join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		Inner Join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		Left Join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		Left Join [cosmos].[dbo].Operator (nolock) o on o.Id=bo.allocatedby and o.branch=bo.branch
		Left Join pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where --cd_last_status In ('Futile','')  And	
		bo.IsActive = 1  
		And c.cd_date >=@StartDate And c.cd_date <= @EndDate
			
	Union All
 
	Select	
		Distinct 
			c.cd_account [Customer Account Number]
			, c.cd_connote [Consignment Number]
			, b.b_name [Branch]		
			, Case cd_last_status When '' Then 'Missed Pickup' When 'Futile' Then 'Futile' Else 'Picked Up' End As [ConsignmentStatus]
			, Case  When b.b_name='Adelaide' Then 'SA'
					When b.b_name='Brisbane' or b.b_name='Gold Coast' Then 'QLD'
					When  b.b_name='Melbourne' Then 'VIC'
					When b.b_name='Sydney' Then 'NSW'
					When b.b_name='Perth' Then 'WA'
				Else 'Unknown' End As [State]
			, cd_date [Consignment Date]
			, bo.DriverId [Driver Number]
			, rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number]
	From cpplEDI.dbo.consignment(nolock) c 
		Inner Join CpplEDI.dbo.cdcoupon (nolock) cc on c.cd_id= cc.cc_consignment
		Inner Join [ScannerGateway].[dbo].[HubbedStaging] (nolock) hb on  hb.TrackingNumber=cc.cc_coupon
		Inner Join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		Inner Join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		Left Join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		Left Join [cosmos].[dbo].Operator (nolock) o on o.[Id]=bo.allocatedby
		Left Join pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where --cd_last_status In ('Futile','')  And 
		bo.IsActive = 1  
		And c.cd_date >=@StartDate And c.cd_date <= @EndDate

	Union All
	
	Select	
		Distinct 
			c.cd_account [Customer Account Number]
			, c.cd_connote [Consignment Number]
			, b.b_name [Branch]
			, 'Partial Pickup' [ConsignmentStatus]
			, Case	When b.b_name='Adelaide' Then 'SA'
					When b.b_name='Brisbane' or b.b_name='Gold Coast' then 'QLD'
					When b.b_name='Melbourne' then 'VIC'
					When b.b_name='Sydney' then 'NSW'
					When b.b_name='Perth' then 'WA'
				Else 'Unknown' end as [State]
			, cd_date [Consignment Date]
			, bo.DriverId [Driver Number]
			, cd.cb_cosmos_job [Booking Number]
	From #tempPartialCG c 
		Inner Join CpplEDI.dbo.cdcosmosbooking (nolock) cd on cd.cb_consignment=c.cd_id		
		Inner Join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		Inner Join cosmos.dbo.booking (nolock) bo on bo.BookingId=cd.cb_cosmos_job and bo.BookingDate=cd.cb_cosmos_date and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		Left Join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		Left Join [cosmos].[dbo].Operator (nolock) o on o.Id=bo.allocatedby and o.branch=bo.branch
		Left Join Pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where --cd_last_status not in ('Futile','') and 
		bo.IsActive = 1
		and c.cd_date >=@StartDate and c.cd_date <= @EndDate
			
	Union ALL

	Select	
		Distinct 
			c.cd_account [Customer Account Number]
			, c.cd_connote [Consignment Number]
			, b.b_name [Branch]
			, 'Partial Pickup' [ConsignmentStatus]			
			, Case	When b.b_name='Adelaide' Then 'SA'
					When b.b_name='Brisbane' or b.b_name='Gold Coast' Then 'QLD'
					When  b.b_name='Melbourne' Then 'VIC'
					When b.b_name='Sydney' Then 'NSW'
					When b.b_name='Perth' Then 'WA'
				Else 'Unknown' End As [State]
			, cd_date [Consignment Date]
			, bo.DriverId [Driver Number]
			, rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) [Booking Number]
	From #tempPartialCG c
		Inner Join CpplEDI.dbo.cdcoupon (nolock) cc on c.cd_id= cc.cc_consignment
		Inner Join [ScannerGateway].[dbo].[HubbedStaging] (nolock) hb on  hb.TrackingNumber=cc.cc_coupon
		Inner Join [CpplEDI].dbo.[branchs] (nolock) b on c.cd_pickup_branch=b_id
		Inner Join cosmos.dbo.booking (nolock) bo on rtrim(cast(BookingId as varchar))=rtrim(REPLACE(replace(Jobnumber,char(13),''),char(10),'')) and bo.BookingDate=convert (date,scanTime) and  replace(b.b_name,'Gold Coast','GoldCoast')=bo.Branch
		Left Join [cosmos].[dbo].[Driver] (nolock) DC On(bo.DriverId = Dc.DriverNumber and bo.Branch = DC.Branch and DC.IsActive=1)
		Left Join [cosmos].[dbo].Operator (nolock) o on o.[Id]=bo.allocatedby and o.Branch = bo.Branch
		Left Join pronto.dbo.ProntoDebtor (nolock) d on c.cd_account=d.Accountcode
	Where --cd_last_status not in ('Futile','') and 
		bo.IsActive = 1
		and c.cd_date >=@StartDate 
		and c.cd_date <= @EndDate
----------------------------------------------------
	Select * Into #tempAllCust From #tmpReport 
----------------------------------------------------
Select Rank() over (partition by [Consignment Number] order by [Consignment Number],consignmentstatus) [DeleteRowNum], * 
Into #tempDelete
From #tempAllCust where [Consignment Number] in
(
Select [Consignment Number] from #tempAllCust
Where consignmentstatus in ('Picked Up','Partial Pickup')
group by [Consignment Number]
having count(*) > 1
)
----------------------------------------------------
Delete ta From #tempAllCust ta
Join #tempDelete td On ta.[Consignment Number] = td.[Consignment Number] and ta.ConsignmentStatus = 'Picked Up'
Where DeleteRowNum = 2  
----------------------------------------------------
	Select Branch
			,[Driver Number] As RunNumber
			,Convert(varchar(20),'') As ProntoID
			,Convert(varchar(20),'') As DriverNumber 
			, Convert(int, 0) [Futile]
			, Convert(int, 0) [Missed] 
			, Convert(int, 0) [Partial]
			, Convert(int, 0) [PickedUp]
			, Convert(Int, 0 ) [Total]			
	Into #tempAllCust2
	From #tempAllCust
	Group by 
		Branch
		,[Driver Number]
----------------------------------------------------------------------------------------------------------------
	Update T2 Set ProntoID = d.ProntoId
	From #tempAllCust2 T2 
	Join cosmos..driver d On Replace(T2.Branch,'Gold Coast','GoldCoast') = d.Branch And T2.RunNumber = d.DriverNumber and d.IsActive = 1

	Update T2 Set DriverNumber = PerformanceReporting.dbo.fn_CreateUniqueDriverID(Branch,RunNumber,ProntoID)
	From #tempAllCust2 T2
-----------------------------------------------------------------------------------------------------------------
	Update T1 Set Futile = T2.Total
	From #tempAllCust2 T1
	Join (
			Select Branch,[Driver Number], ConsignmentStatus, Count(*) [Total]
			From #tempAllCust
			Group by Branch,[Driver Number],ConsignmentStatus
		) T2 ON T1.Branch = T2.Branch and T1.RunNumber = T2.[Driver Number]
	Where T2.ConsignmentStatus = 'Futile'

	Update T1 Set Missed = T2.Total
	From #tempAllCust2 T1
	Join (
			Select Branch, [Driver Number], ConsignmentStatus, count(*) [Total]
			From #tempAllCust
			Group by Branch, [Driver Number], ConsignmentStatus
		) T2 ON T1.Branch = T2.Branch and T1.RunNumber = T2.[Driver Number]
	Where T2.ConsignmentStatus = 'Missed Pickup'

	Update T1 Set [Partial] = T2.Total
	From #tempAllCust2 T1
	Join (
			Select Branch, [Driver Number], ConsignmentStatus, count(*) [Total]
			From #tempAllCust
			Group by Branch, [Driver Number], ConsignmentStatus
		) T2 ON T1.Branch = T2.Branch and T1.RunNumber = T2.[Driver Number]
	Where T2.ConsignmentStatus = 'Partial Pickup'

	Update T1 Set PickedUp = T2.Total
	From #tempAllCust2 T1
	Join (
			Select Branch, [Driver Number], ConsignmentStatus, count(*) [Total]
			From #tempAllCust
			Group by Branch, [Driver Number], ConsignmentStatus
		) T2 ON T1.Branch = T2.Branch and T1.RunNumber = T2.[Driver Number]
	Where T2.ConsignmentStatus = 'Picked Up'

	Update T1 Set T1.Total = T2.Total
	From #tempAllCust2 T1
	Join (
			Select Branch,[Driver Number], count(*) [Total]
			From #tempAllCust
			Group by Branch,[Driver Number]
		) T2 ON T1.Branch = T2.Branch and T1.RunNumber = T2.[Driver Number]
--------------
	Declare @CalendarDayId as Integer = ISnull((SELECT [Id] FROM PerformanceReporting.[dbo].[DimCalendarDate] where Date = @Date),0)	
	Insert Into PerformanceReporting..[DailyMissedPickupReporting]
	(
		MissedPickupReportingDayID,
		RevenueType,
		OnBoardDriver,
		--PickupETAZone,
		--DeliveryETAZone,
		--BUCode,
		Branch,
		FutileCount,
		MissedCount,
		PartialCount,
		PickedUpCount,
		Total,
		AddWho,
		AddDateTime
	)
	Select 
		@CalendarDayId 
		,'EDI'
		,DriverNumber
		--,''
		--,''
		--,''
		,Branch
		,Futile
		,Missed
		,[Partial]
		,PickedUp
		,Total
		,'Sys'
		,Getdate()
	From #tempAllCust2
-----------------------	
END
GO
