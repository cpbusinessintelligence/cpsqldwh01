SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[sp_LoadDailyNoTimeReporting]
(@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadDailyNoTimeReporting]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
   Declare @CalendarDayId as Integer = ISnull((SELECT [Id] FROM [dbo].[DimCalendarDate] where Date = @Date),0)
   Declare @StartDate as DateTime
   Declare @EndDate as DateTime

   Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
   Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)
   
   INSERT INTO [dbo].[DailyNoTimeReporting]
           ([NoTimeReportingDayID],[RevenueType],[OnBoardDriver]
           ,[PickupETAZone],[DeliveryETAZone],[BUCode]
           ,[NoTimeCount],[AddWho],[AddDateTime])
   Select @CalendarDayId
        , RevenueType
		, Isnull(OutforDeliveryScannedBy,'')
		 ,[PickupETAZone]
		 ,[DeliveryETAZone]
		 ,[BUCode]
		 ,COUNT(*)
		 ,'Sys'
		 ,Getdate()
   From [dbo].[PrimaryLabels] P Join dbo.DimContractor C on P.OutforDeliveryScannedBy = C.DriverID
   Where OutForDeliverDateTime <= CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('17:00:00' AS TIME) AS DATETIME)   and C.ContractorType not in ('D','B') and ContractorCategory <> 'Agent'
           and ((AttemptedDeliveryDateTime is null 
		   and DeliveryDateTime is null)   OR COnvert(date,[DeliveryDateTime]) >@EndDate OR Convert(Date,AttemptedDeliveryDateTime) > @EndDate)
		      and  Convert(date,OutForDeliverDateTime) = @Date 
   Group by RevenueType,
           Isnull(OutforDeliveryScannedBy,''),
		   [PickupETAZone],
		   [DeliveryETAZone],
		    [BUCode]
end
GO
