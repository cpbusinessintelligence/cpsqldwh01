SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_LoadDailyNoTimeReportingDetail](@Date date) as
begin

   --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadDailyNoTimeReportingDetail]
    --' ---------------------------
    --' Purpose: Load Daily NoTimeReporting Detail from Primary labels-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 05 Mar 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 05/03/2015    AK      1.00    Created the procedure                            

    --'=====================================================================
	declare @Mindate date
	select @Mindate=min(Performancereportingdate) from DailyNoTimeReportingDetail
	If datediff("dd",@Mindate,dateadd("dd",-1,getdate()))>30
	delete from DailyNoTimeReportingDetail where Performancereportingdate between @Mindate and dateadd("dd",-1,getdate())

Insert into DailyNoTimeReportingDetail

SELECT distinct [LabelNumber]
      --,[GWConsignmentID]
      ,[RevenueType]
	  ,AccountCode
	  ,isnull([PickupDateTime],'') as PickupScan
      ,isnull([PickupScannedBy],'') as PickupDrivernumber
      ,case left(ltrim(rtrim(PickupScannedBy)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as PickupBranch
	  ,isnull(C.DepotName,'') as PickupDepot
      ,isnull([AttemptedDeliveryDateTime],'') as AttemptScan
	  ,P.OutforDeliveryScannedBy as OnBoardDriver
	  ,isnull(C1.DepotName,'') as OnBoardDepot
	  ,isnull([OutForDeliverDateTime],'') as OnBoardScan
      ,isnull([DeliveryDateTime],'') as DeliveryScan
	  ,Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'') as DeliveryDriver
	  ,isnull(C2.DepotName,'') as DeliveryDepot
	  ,convert(decimal(12,2),datediff("mi",[PickupDateTime],isnull([AttemptedDeliveryDateTime],[DeliveryDateTime]))/convert(decimal(12,2),60)) as DeliveryHours
	  --datediff("mi",[PickupDateTime],isnull([AttemptedDeliveryDateTime],[DeliveryDateTime]))/convert(decimal(4,2),60) as DeliveryHours
      ,isnull([PickupETAZone],'') as PickupETAZone
      ,isnull([DeliveryETAZone],'') as DeliveryETAZone
      ,isnull(replace([1stDeliveryCutOffTime],'XXX',''),'') as [1stDeliveryCutOff]
      ,isnull(replace([1stAllowedDays],'XXX',''),'') as [1stAllowedDays]
      ,isnull(replace([2ndDeliverycutOffTime],'XXX',''),'') as [2ndDeliveryCutOff]
      ,isnull(replace([2ndAllowedDays],'XXX',''),'') as [2ndAllowedDays]
	  ,@Date
  From [dbo].[PrimaryLabels] P 
  Join dbo.DimContractor C1 on P.OutforDeliveryScannedBy = C1.DriverID
  left Join dbo.DimContractor C on P.PickupScannedBy = C.DriverID
  left Join dbo.DimContractor C2 on Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'') = C2.DriverID
	 left join  [dbo].[ETACalculator] e on e.FromZone=[PickupETAZone] and e.[ToZone]=[DeliveryETAZone] and p.producttype=category
   Where  OutForDeliverDateTime <= CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('17:00:00' AS TIME) AS DATETIME) 
   and  Convert(date,OutForDeliverDateTime) = @Date 
   --and  Isnull(OutforDeliveryScannedBy,'') like 'ADL%' 
   and C1.ContractorType not in ('D','B') and C1.ContractorCategory <> 'Agent'
           and ((AttemptedDeliveryDateTime is null 
		   and DeliveryDateTime is null)   OR COnvert(date,[DeliveryDateTime]) >@Date OR Convert(Date,AttemptedDeliveryDateTime) > @Date)
		   --and p.labelnumber not in (Select labelnumber from 

	end	   
GO
