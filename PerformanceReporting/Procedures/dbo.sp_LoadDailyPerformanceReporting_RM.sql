SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[sp_LoadDailyPerformanceReporting_RM]
(@Date Date )
AS
BEGIN

	--'=====================================================================
	--' CP -Stored Procedure -[sp_LoadDailyPerformanceReporting_RM]
	--' ---------------------------
	--' Purpose: Full load of small tables-----
	--' Developer: Abhigna Kona (Couriers Please Pty Ltd)
	--' Date: 06 Dec 2016
	--' Copyright: 2014 Couriers Please Pty Ltd
	--' Change Log: 
	--' Date          Who     Ver     Reason                                            
	--' ----          ---     ---     -----                                            
	--' 11/03/2021    RM      1.00    Created the procedure                            
    --'=====================================================================
	Declare @CalendarDayId as Integer = ISnull((SELECT [Id] FROM [dbo].[DimCalendarDate] where Date = @Date),0)
	Declare @StartDate as DateTime
	Declare @EndDate as DateTime

	Select @StartDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)
	Select @EndDate = CAST(CAST(@Date AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)
   
	INSERT INTO [dbo].[DailyPerformanceReporting_RM]
		([PerformanceDayID],[RevenueType],[ProductType],ProductSubCategory,[AccountCode],[DeliveryDriver],[PickUpDriver],[OnTimeStatus]
		,[PickupETAZone],[DeliveryETAZone],[NetworkCategoryID],[BUCode]
		,[PerformanceCount],[OnBoardComplainceCount],[DeliveryComplainceCount],
		[PODComplainceCount],[AddWho],[AddDateTime])
	Select 
		@CalendarDayId
		, RevenueType
		,[ProductType]
		,ProductSubCategory
		,[AccountCode]
		, Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'')
		,ISNull(PickupSCannedBy,'')
		,[OnTimeStatusId]
		,[PickupETAZone]
		,[DeliveryETAZone]
		,[NetworkCategoryID]
		,[BUCode]
		,COUNT(*)
		,SUM(CASE WHEN [OutForDeliverDateTime] is not null THEN 1 ELSE 0 END )
		,SUM(CASE WHEN  [DeliveryDateTime] is not Null or AttemptedDeliveryDateTime is not null THEN 1 ELSE 0 END)
		,SUM(CASE WHEN AttemptedDeliveryDateTime is not null OR ISNULL(IsPODPresent,'N') = 'Y' THEN 1 ELSE 0 END)
		,'Sys'
		,Getdate()
	From [dbo].[PrimaryLabels] 
	Where [PerformanceReportingDate] = @Date
		and [PerformanceProcessed] =1
		and OnTimeStatusId Is Not Null -- Added by PV on 2020-08-28
		-- and (isnull([CardCategory] ,'')='' or isnull([CardCategory] ,'') like '%1st%')
	Group by 
		RevenueType,
		[ProductType],
		ProductSubCategory,
		[AccountCode],
		Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),''),
		ISNull(PickupSCannedBy,''),
		[OnTimeStatusId],
		[PickupETAZone],
		[DeliveryETAZone],
		[NetworkCategoryID],
		[BUCode]
--------------------
/*Added by PV to insert Unprocessed data to Unprocessed table*/
--------------------
	----INSERT INTO [dbo].[DailyPerformanceReporting_Unprocessed]
	----	([PerformanceDayID],[RevenueType],[ProductType],ProductSubCategory,[AccountCode],[DeliveryDriver],[PickUpDriver],[OnTimeStatus]
	----	,[PickupETAZone],[DeliveryETAZone],[NetworkCategoryID],[BUCode]
	----	,[PerformanceCount],[OnBoardComplainceCount],[DeliveryComplainceCount],
	----	[PODComplainceCount],[AddWho],[AddDateTime])
	----Select 
	----	@CalendarDayId
	----	, RevenueType
	----	,[ProductType]
	----	,ProductSubCategory
	----	,[AccountCode]
	----	, Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'')
	----	,ISNull(PickupSCannedBy,'')
	----	,[OnTimeStatusId]
	----	,[PickupETAZone]
	----	,[DeliveryETAZone]
	----	,[NetworkCategoryID]
	----	,[BUCode]
	----	,COUNT(*)
	----	,SUM(CASE WHEN [OutForDeliverDateTime] is not null THEN 1 ELSE 0 END )
	----	,SUM(CASE WHEN  [DeliveryDateTime] is not Null or AttemptedDeliveryDateTime is not null THEN 1 ELSE 0 END)
	----	,SUM(CASE WHEN AttemptedDeliveryDateTime is not null OR ISNULL(IsPODPresent,'N') = 'Y' THEN 1 ELSE 0 END)
	----	,'Sys'
	----	,Getdate()
	----From [dbo].[PrimaryLabels] 
	----Where [PerformanceReportingDate] = @Date
	----	and [PerformanceProcessed] = 1
	----	and OnTimeStatusId Is Null
	----Group by RevenueType,
	----	[ProductType],
	----	ProductSubCategory,
	----	[AccountCode],
	----	Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),''),
	----	ISNull(PickupSCannedBy,''),
	----	[OnTimeStatusId],
	----	[PickupETAZone],
	----	[DeliveryETAZone],
	----	[NetworkCategoryID],
	----	[BUCode]
End
GO
