SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_LoadDimContractor] as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[cpplEDI_FullTablesLoad]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	SELECT [dbo].[fn_CreateUniqueDriverID]([Branch],[DriverNumber],[ProntoID]) as DriverID
      ,IsNull([DriverNumber],'XXXX') as [DriverNumber]
      ,IsNull([ProntoID],'XXXX') as ProntoID
      ,Convert(Varchar(20),'') [DepotCode]
      ,Convert(Varchar(50),'') [DepotName]
      ,[Branch]
      ,Case [Branch] WHEN 'Adelaide' THEN 'SA' WHEN 'Sydney' Then 'NSW' WHEN 'Melbourne' THEN 'VIC' WHEN 'Brisbane' THEN 'QLD' WHEN 'Perth' THEN 'WA' WHen 'NKope' Then 'SA' WHEN 'Goldcoast' THEN 'QLD' WHEN 'Gold Coast' THEN 'QLD' ELSE '' END  as [State]
      ,Isnull([ContractorType],'X') as [ContractorType]
	  ,DepotNo
      ,Isnull([DriverName],'') as [DriverName]
      ,IsNull([ETAZone],'') as [ETAZone]
      ,IsNull([PricingZone],'') as [PricingZone]
      ,Isnull([Address1],'') as [Address1]
      ,Isnull([Address2],'') as [Address2]
      ,Isnull([Address3],'') as [Address3]
      ,Isnull(Case Rtrim(Ltrim([PostCode])) WHEN '0' THEN '' ELSE [PostCode] END,'') as [PostCode]
      ,Isnull([PhoneNumber],'') as [PhoneNumber]
      ,Isnull([MobileNumber],'') as [MobileNumber]
      ,Isnull([FaxNumber],'') as [FaxNumber]
      ,Isnull([CreditLimit],0) as [CreditLimit]
      ,Convert(Date,[StartDate]) as StartDate
      ,Convert(Date,[EndDate]) as EndDate
      ,Convert(Date,[ArchiveDate]) as [ArchiveDate]
  INTO #Temp1 
  FROM [Cosmos].[dbo].[Driver] 
  Where IsActive = 1 

  Update #Temp1 SET [DepotCode] =D.DepotCode ,DepotName =D.DepotName
        From #Temp1 T join Cosmos.dbo.Depot D on  T.DepotNo = D.DepotNumber and T.Branch = D.Branch Where D.IsActive =1
  

  MERGE dbo.DimContractor AS DimC
			USING (SELECT DriverID,DriverNumber,ProntoID,[DepotCode],[DepotName],[Branch],[State]
			              ,[ContractorType],DepotNo,[DriverName],[ETAZone],[PricingZone],[Address1]
						  ,[Address2],[Address3],[PostCode],[PhoneNumber],[MobileNumber],[FaxNumber]
						  ,[CreditLimit],StartDate,EndDate,[ArchiveDate]
			       From #Temp1)
		    AS SD	ON DimC.DriverID = SD.DriverID
            WHEN MATCHED THEN UPDATE SET   [DriverNumber]= SD.DriverNumber
			                              ,[ProntoID] = SD.ProntoID 
										  ,DepotCode =SD.DepotCode
										  ,DepotName =SD.DepotName
										  ,[Branch]=SD.Branch
										  ,[State]=SD.[State]
										  ,[ContractorType] = SD.[ContractorType]
										  ,[DriverName]=SD.[DriverName]
										  ,[ETAZone]=SD.[ETAZone]
										  ,[PricingZone]=SD.[PricingZone]
										  ,[Address1]=SD.[Address1]
										  ,[Address2]=SD.[Address2]
										  ,[Address3]=SD.[Address3]
										  ,[PostCode]=SD.[PostCode]
										  ,[PhoneNumber]=SD.[PhoneNumber]
										  ,[MobileNumber]=SD.[MobileNumber]
										  ,[FaxNumber]=SD.[FaxNumber]
										  ,[CreditLimit]=SD.[CreditLimit]
										  ,StartDate=SD.StartDate
										  ,EndDate=SD.EndDate
										  ,[ArchiveDate]=SD.[ArchiveDate]

            WHEN NOT MATCHED THEN INSERT  ([DriverID],[DriverNumber],[ProntoID],[DepotCode],[DepotName],[Branch]
										   ,[State],[ContractorType],[DriverName],[ETAZone],[PricingZone],[Address1]
										   ,[Address2],[Address3],[PostCode],[PhoneNumber],[MobileNumber],[FaxNumber]
										   ,[CreditLimit],StartDate,EndDate,[ArchiveDate])
							       VALUES (SD.DriverID,SD.[DriverNumber],SD.[ProntoID],SD.[DepotCode],SD.[DepotName],SD.[Branch]
								           ,SD.[State],SD.[ContractorType],SD.[DriverName],SD.[ETAZone],SD.[PricingZone],SD.[Address1]
										   ,SD.[Address2],SD.[Address3],SD.[PostCode],SD.[PhoneNumber],SD.MobileNumber,SD.[FaxNumber]
										   ,SD.[CreditLimit],SD.StartDate,SD.EndDate,SD.[ArchiveDate]);
   

   Update dbo.DimContractor SET [ContractorCategory] =  T.[Category] ,[Region] = T.[Region]
        From dbo.DimContractor C Join [dbo].[ContractorCategory] T on C.ETAZone = T.ETAZOne
end

GO
