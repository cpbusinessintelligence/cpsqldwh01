SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_LoadDimProduct] as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadDimCustomer]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	
------------------------------------------
-------    PREPAID------------------------
------------------------------------------
  MERGE dbo.DimProduct AS DimP
			USING (SELECT 'PREPAID' as RevenueType
						  ,[StockCode] as Code
						  ,[StkDescription] as Description
						  ,[StkStockStatus] as StockStatus
						  ,ISNULL([StkSalesTypeCode],'') as StockSalesType
						  ,Convert(int,[StkConversionFactor]) as [StockConversionFactor]
						  ,Convert(int,[StkPricePer]) as [StockPricePer]
						  ,[StkReplacementCost] as [StockReplacementCost]
						  ,[StkDateLastChange] as [StkLastChange]
                    FROM [Pronto].[dbo].[ProntoStockMaster])
		    AS SD	ON DimP.Code = SD.[Code] and DimP.RevenueType = 'PREPAID'
            WHEN MATCHED THEN UPDATE SET   [Description] = SD.[Description]
			                              ,StockStatus = SD.StockStatus
										  ,StockSalesType =SD.StockSalesType
							     		  ,[StockConversionFactor]=SD.[StockConversionFactor]
										  ,[StockPricePer]=SD.[StockPricePer]
										  ,[StockReplacementCost] = SD.[StockReplacementCost]
										  ,[StkLastChange]=SD.[StkLastChange]
										 
            WHEN NOT MATCHED THEN INSERT  ([RevenueType],[Code],[Category],RevenueCategory,[Description],[BusinessUnit]
										  ,[StockStatus],[StockSalesType],[StockConversionFactor],[StockPricePer]
								          ,[StockReplacementCost],[StkLastChange])
							       VALUES ('PREPAID',SD.[Code],'UNKNOWN','UNKNOWN',SD.[Description],''
										  ,SD.StockStatus,SD.StockSalesType,SD.[StockConversionFactor]
										  ,SD.[StockPricePer],SD.[StockReplacementCost],SD.[StkLastChange]);




------------------------------------------
-------    EDI------------------------
------------------------------------------

  MERGE dbo.DimProduct AS DimP
			USING (SELECT 'EDI' as RevenueType
						  ,[pc_code] as PriceCode
                          ,[pc_name] as PriceCodeDescription
  FROM [CpplEDI].[dbo].[pricecodes])
		    AS SD	ON DimP.Code = SD.[PriceCode] and DimP.RevenueType = 'EDI'
            WHEN MATCHED THEN UPDATE SET   [Description] = SD.[PriceCodeDescription]
			                              										 
            WHEN NOT MATCHED THEN INSERT  ([RevenueType],[Code],[Category],RevenueCategory,[Description],[BusinessUnit])
			    			       VALUES ('EDI',SD.[PriceCode],'UNKNOWN','UNKNOWN',SD.[PriceCodeDescription],'');



end

GO
