SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadETACalculator] as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadETACalculator]
    --' ---------------------------
    --' Purpose: Load ETA Calculator----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 25 Nov 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 25/11/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

	--Truncate table ETACalculator

--Insert into ETACalculator(Category,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stPickupCutOffTime]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndPickupCutOffTime]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'Standard'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
      ,[ETA]
      ,[FromETA]
      ,[ToETA]
      ,[1stPickupCutOffTime]
      ,[1stDeliveryCutOffTime]
      ,[1stAllowedDays]
      ,[2ndPickupCutOffTime]
      ,[2ndDeliveryCutOffTime]
      ,[2ndAllowedDays]
from CouponCalculator.dbo.ETACalculator


--Insert into ETACalculator(Category,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'Domestic Priority'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
      ,[AIR_ETA]
      ,[AIR_FromETA]
      ,[AIR_ToETA]
      ,[AIR_1stDeliveryCutOffTime]
      ,[AIR_1stAllowedDays]
      ,[AIR_2ndDeliveryCutOffTime]
      ,[AIR_2ndAllowedDays]
from CouponCalculator.dbo.ETACalculator



--Insert into ETACalculator(Category,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'SameDay'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
      ,[SameDay_ETA]
      ,[SameDay_FromETA]
      ,[SameDay_ToETA]
      ,[SameDay_1stDeliveryCutOffTime]
      ,[SameDay_1stAllowedDays]
      ,[SameDay_2ndDeliveryCutOffTime]
      ,[SameDay_2ndAllowedDays]
from CouponCalculator.dbo.ETACalculator



--Insert into ETACalculator(Category,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'Domestic Off Peak'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
      ,[Domestic_OffPeak_ETA]
      ,[Domestic_OffPeak_FromETA]
      ,[Domestic_OffPeak_ToETA]
      ,[Domestic_OffPeak_1stDeliveryCutOff]
      ,[Domestic_OffPeak_1stAllowedDays]
      ,[Domestic_OffPeak_2ndDeliveryCutOff]
      ,[Domestic_OffPeak_2ndAllowedDays]
from CouponCalculator.dbo.ETACalculator

end
GO
