SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-09-01
-- Description:	Load ETA Calculator from staging for all 4 categories
-- =============================================
CREATE PROCEDURE [dbo].[sp_LoadETACalculator_FromStaging]
AS
BEGIN
	SET NOCOUNT ON;

	
	--Backup Table--
	Declare @date varchar(200),@sql varchar(1000)
	SET @date = replace(convert(varchar(8), getdate(), 112)+convert(varchar(8), getdate(), 114), ':','')

	SET @sql='SELECT *
	INTO [dbo].[ETACalculator_' + CONVERT(varchar(30),@date,113) +']
	FROM [PerformanceReporting].[dbo].[ETACalculator] '	
	EXEC(@Sql)

	--Truncate
	Truncate table ETACalculator

	----
	SET IDENTITY_INSERT ETACalculator ON
	INSERT INTO ETACalculator
	(
		ETAID
		,Category
		,FromZone
		,ToZone
		,PrimaryNetworkCategory
		,SecondaryNetworkCategory
		,ETA
		,FromETA
		,ToETA
		,[1stPickupCutOffTime]
		,[1stDeliveryCutOffTime]
		,[1stAllowedDays]
		,[2ndPickupCutOffTime]
		,[2ndDeliveryCutOffTime]	
		,[2ndAllowedDays]
		,AddWho	
		,AddDateTime	
		,EditWho	
		,EditDateTime
	)
	Select 
		ETAID
		,'Standard'
		,FromZone
		,ToZone
		,PrimaryNetworkCategory
		,SecondaryNetworkCategory
		,ETA
		,FromETA
		,ToETA
		,[1stPickupCutOffTime]
		,[1stDeliveryCutOffTime]
		,[1stAllowedDays]
		,[2ndPickupCutOffTime]
		,[2ndDeliveryCutOffTime]
		,[2ndAllowedDays]
		,AddWho
		,AddWhen
		,EditWho
		,EditWhen
	FROM Load_ETA_CALCULATOR

	-----
	INSERT INTO ETACalculator
	(
		ETAID
		,Category
		,FromZone
		,ToZone
		,PrimaryNetworkCategory
		,SecondaryNetworkCategory
		,ETA
		,FromETA
		,ToETA
		,[1stPickupCutOffTime]
		,[1stDeliveryCutOffTime]
		,[1stAllowedDays]
		,[2ndPickupCutOffTime]
		,[2ndDeliveryCutOffTime]	
		,[2ndAllowedDays]
		,AddWho	
		,AddDateTime	
		,EditWho	
		,EditDateTime
	)
	Select 
		ETAID
		,'SameDay' 
		,FromZone	
		,ToZone	
		,PrimaryNetworkCategory	
		,SecondaryNetworkCategory	
		,SameDay_ETA	
		,SameDay_FromETA
		,SameDay_ToETA
		,NULL
		,SameDay_1stDeliveryCutOffTime
		,SameDay_1stAllowedDays	
		,NULL
		,SameDay_2ndDeliveryCutOffTime
		,SameDay_2ndAllowedDays	
		,AddWho
		,AddWhen
		,EditWho
		,EditWhen
	From Load_ETA_CALCULATOR
	-----
	INSERT INTO ETACalculator
	(
		ETAID
		,Category
		,FromZone
		,ToZone
		,PrimaryNetworkCategory
		,SecondaryNetworkCategory
		,ETA
		,FromETA
		,ToETA
		,[1stPickupCutOffTime]
		,[1stDeliveryCutOffTime]
		,[1stAllowedDays]
		,[2ndPickupCutOffTime]
		,[2ndDeliveryCutOffTime]	
		,[2ndAllowedDays]
		,AddWho	
		,AddDateTime	
		,EditWho	
		,EditDateTime
	)
	Select 
		ETAID
		,'Domestic Priority'
		,FromZone
		,ToZone
		,PrimaryNetworkCategory
		,SecondaryNetworkCategory
		,Domestic_Priority_ETA
		,Domestic_Priority_FromETA
		,Domestic_Priority_ToETA
		,Domestic_Priority_1stPickupCutOff
		,Domestic_Priority_1stDeliveryCutOff
		,Domestic_Priority_1stAllowedDays
		,Domestic_Priority_2ndPickupCutOff
		,Domestic_Priority_2ndDeliveryCutOff
		,Domestic_Priority_2ndAllowedDays
		,AddWho	
		,AddWhen	
		,EditWho	
		,EditWhen
	From Load_ETA_Calculator
	---
	INSERT INTO ETACalculator
	(
		ETAID
		,Category
		,FromZone
		,ToZone
		,PrimaryNetworkCategory
		,SecondaryNetworkCategory
		,ETA
		,FromETA
		,ToETA
		,[1stPickupCutOffTime]
		,[1stDeliveryCutOffTime]
		,[1stAllowedDays]
		,[2ndPickupCutOffTime]
		,[2ndDeliveryCutOffTime]	
		,[2ndAllowedDays]
		,AddWho	
		,AddDateTime	
		,EditWho	
		,EditDateTime
	)
	Select 
		ETAID	
		,'Domestic Off Peak'
		,FromZone
		,ToZone
		,PrimaryNetworkCategory
		,SecondaryNetworkCategory
		,Domestic_OffPeak_ETA
		,Domestic_OffPeak_FromETA
		,Domestic_OffPeak_ToETA
		,NULL
		,Domestic_OffPeak_1stDeliveryCutOff
		,Domestic_OffPeak_1stAllowedDays
		,NULL
		,Domestic_OffPeak_2ndDeliveryCutOff
		,Domestic_OffPeak_2ndAllowedDays
		,AddWho
		,AddWhen
		,EditWho
		,EditWhen
	From Load_ETA_Calculator

END
GO
