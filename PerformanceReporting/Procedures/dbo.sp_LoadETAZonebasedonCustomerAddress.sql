SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[sp_LoadETAZonebasedonCustomerAddress] 
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_LoadETAZonebasedonCustomerAddress]]
    --' ---------------------------
    --' Purpose: [sp_LoadETAZonebasedonCustomerAddress]-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 21 Dec 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 21/12/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

	Insert into [PerformanceReporting].[dbo].[CustomertoZonemap]([Accountcode]
      ,[BillTo]
      ,[Shortname]
      ,[Territory]
      ,[Postcode]
      ,[Locality]
      ,[RepCode]
      ,[OriginalRepCode]
      ,[RepName]
      ,[OriginalRepName]
	  ,ETAZone
      ,[CreatedDatetime]
      ,[CreatedBy]
      ,[EditedDatetime]
      ,[EditedBy])


SELECT  
       [Accountcode]
      ,[BillTo]
      ,[Shortname]
      ,[Territory]
      ,[Postcode]
      ,[Locality]
      ,[RepCode]
      ,[OriginalRepCode]
      ,[RepName]
      ,[OriginalRepName]
	  ,Isnull([dbo].[fn_GetETAZone](Rtrim(Ltrim(d.postcode)),Rtrim(Ltrim(d.locality))),'')
      ,getdate()
      ,'Sys'
      ,getdate()
      ,'Sys'
  FROM [Pronto].[dbo].[ProntoDebtor] d (NOLOCK) where territory like 'X%'


end

GO
