SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 CREATE procedure [dbo].[sp_LoadLineHauldata](@Date date) as
  begin

  
     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadLineHauldata]
    --' ---------------------------
    --' Purpose: sp_LoadLineHauldata-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 01 Mar 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 01/03/2017   JP      1.00    Created the procedure                            

    --'=====================================================================
 
  Select [ConsolidatedBarcode],
         min([ConsolidateDateTime]) as [ConsolidateDateTime],
         convert(varchar(100),'') as [ConsolidateScannedBy],
         min([DeConsolidateDateTime]) as [DeConsolidateDateTime],
         convert(varchar(100),'') as [DeConsolidateScannedBy],
		 Producttype,
		 convert(varchar(100),'') as PickUpZone,
		 convert(varchar(100),'') as DeliveryZone,
		 convert(datetime,Null) as ETADate,
		 Convert(int,0) as OntimeStatusId
into #tempLineHaul
 from Primarylabels where convert(Date,Deconsolidatedatetime)=@Date and [ConsolidatedBarcode] is not null
 group by [ConsolidatedBarcode],Producttype

 Update #tempLineHaul set [ConsolidateScannedBy]=te.ConsolidateScannedBy
 from #tempLineHaul t join Primarylabels te on te.[ConsolidatedBarcode]=t.[ConsolidatedBarcode] and t.[ConsolidateDateTime]=te.[ConsolidateDateTime]


 
 Update #tempLineHaul set [DeConsolidateScannedBy]=te.[DeConsolidateScannedBy]
 from #tempLineHaul t join Primarylabels te on te.[ConsolidatedBarcode]=t.[ConsolidatedBarcode] and t.[DeConsolidateDateTime]=te.[DeConsolidateDateTime]


 Update #tempLineHaul set PickUpZone=C.ETAZone  
             From #tempLineHaul L Join [dbo].[DimContractor]  C on L.[ConsolidateScannedBy] = C.DriverID 
			 Where Isnull(PickUpZone,'')= ''


			 
 Update #tempLineHaul set DeliveryZone=C.ETAZone  
             From #tempLineHaul L Join [dbo].[DimContractor]  C on L.[DeConsolidateScannedBy] = C.DriverID 
			 Where Isnull(DeliveryZone,'')= ''

 Update #tempLineHaul set ETADate=[dbo].[fn_GetETADateFromZone](ProductType,Null,Null,Null,PickUpZone,DeliveryZone,Null,Convert(datetime,ConsolidateDatetime),ConsolidateScannedBy) 
             where ConsolidateDatetime is not null and ETADate is null



  Update #tempLineHaul SET [OnTimeStatusId] = CASE WHEN Convert(DateTime,  [DeConsolidateDateTime]) <= ETADate  
	            THEN [dbo].[fn_GetStatusCodefromDescription] ('LH TransitId OnTime') ELSE [dbo].[fn_GetStatusCodefromDescription] ('LH TransitId NotOnTime')  END
              Where  Isnull([OnTimeStatusId],'') ='' and  Etadate is not null 



 Insert into [dbo].[LinehaulData]([ConsolidatedBarcode]
      ,[ConsolidatedDateTime]
      ,[ConsolidateScannedBy]
      ,[PickupETAZone]
      ,[DeConsolidateDateTime]
      ,[DeConsolidateScannedBy]
      ,[DeliveryETAZone]
      ,[ProductType]
      ,[ETADate]
      ,[OnTimeStatusId])

select   [ConsolidatedBarcode],
         [ConsolidateDateTime],
         [ConsolidateScannedBy],
		 PickUpZone,
         [DeConsolidateDateTime],
         [DeConsolidateScannedBy],
		 DeliveryZone,
		 Producttype,
		 ETADate,
		 OntimeStatusId from #tempLineHaul where not exists(Select 1 from [LinehaulData] where #tempLineHaul.[ConsolidatedBarcode]=[LinehaulData].[ConsolidatedBarcode])


end
GO
