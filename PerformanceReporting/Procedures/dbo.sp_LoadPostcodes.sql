SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_LoadPostcodes] as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadPostcodes]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================

   Truncate table dbo.Postcodes
   INSERT INTO [dbo].[PostCodes]
           ([PostCodeID]
      ,[PostCode]
      ,[Suburb]
      ,[State]
      ,[PrizeZone]
      ,[ETAZone]
      ,[RedemptionZone]
      ,[DepotCode]
      ,[SortCode]
      ,[DeliveryAgent]
      ,[DeliveryAgentRunCode]
      ,[AlternateDeliveryAgent]
      ,[AlternateDeliveryAgentRunCode]
      ,[RedeliveryFlag]
      ,[SatchelLinks]
      ,[RoadExpressPickupFlag]
      ,[RoadExpressDeliveryFlag]
      ,[RoadExpressCutOffTime]
      ,[CosmosCutLetter]
      ,[ACE_SameDayPickupFlag]
      ,[ACE_SameDayDeliveryFlag]
      ,[ACE_SameDayCutOffTime]
      ,[ACE_InterstatePickupFlag]
      ,[ACE_InterstateDeliveryFlag]
      ,[ACE_InterstateCutOffTime]
      ,[DomesticPriorityPickupFlag]
      ,[DomesticPriorityDeliveryFlag]
      ,[DomesticPriorityCutOffTime]
      ,[DomesticMidTierPickupFlag]
      ,[DomesticMidTierDeliveryFlag]
      ,[DomesticMidTierCutOffTime]
      ,[Internationl_PriorityPickUpFlag]
      ,[Internationl_PriorityDeliveryFlag]
      ,[Internationl_PriorityCutOffTime]
      ,[Internationl_SaverPickUpFlag]
      ,[Internationl_SaverDeliveryFlag]
      ,[Internationl_SaverCutOffTime]
      ,[Internationl_MidTierPickUpFlag]
      ,[Internationl_MidTierDeliveryFlag]
      ,[Internationl_MidTierCutOffTime]
      ,[POPStationPickupFlag]
      ,[POPStationDeliveryFlag]
      ,[HubbedPickupFlag]
      ,[HubbedDeliveryFlag]
      ,[Status]
      ,[SL_UFI]
      ,[MICODE]
      ,[IsModified]
      ,[IsProcessed]
      ,[AddWho]
      ,[AddDateTime]
      ,[EditWho]
      ,[EditDateTime])
		   /****** Script for SelectTopNRows command from SSMS  ******/
SELECT [PostCodeID]
      ,[PostCode]
      ,[Suburb]
      ,[State]
      ,[PrizeZone]
      ,[ETAZone]
      ,[RedemptionZone]
      ,[DepotCode]
      ,[SortCode]
      ,[DeliveryAgent]
      ,[DeliveryAgentRunCode]
      ,[AlternateDeliveryAgent]
      ,[AlternateDeliveryAgentRunCode]
      ,[RedeliveryFlag]
      ,[SatchelLinks]
      ,[RoadExpressPickupFlag]
      ,[RoadExpressDeliveryFlag]
      ,[RoadExpressCutOffTime]
      ,[CosmosCutLetter]
      ,[ACE_SameDayPickupFlag]
      ,[ACE_SameDayDeliveryFlag]
      ,[ACE_SameDayCutOffTime]
      ,[ACE_InterstatePickupFlag]
      ,[ACE_InterstateDeliveryFlag]
      ,[ACE_InterstateCutOffTime]
      ,[DomesticPriorityPickupFlag]
      ,[DomesticPriorityDeliveryFlag]
      ,[DomesticPriorityCutOffTime]
      ,[DomesticMidTierPickupFlag]
      ,[DomesticMidTierDeliveryFlag]
      ,[DomesticMidTierCutOffTime]
      ,[Internationl_PriorityPickUpFlag]
      ,[Internationl_PriorityDeliveryFlag]
      ,[Internationl_PriorityCutOffTime]
      ,[Internationl_SaverPickUpFlag]
      ,[Internationl_SaverDeliveryFlag]
      ,[Internationl_SaverCutOffTime]
      ,[Internationl_MidTierPickUpFlag]
      ,[Internationl_MidTierDeliveryFlag]
      ,[Internationl_MidTierCutOffTime]
      ,[POPStationPickupFlag]
      ,[POPStationDeliveryFlag]
      ,[HubbedPickupFlag]
      ,[HubbedDeliveryFlag]
      ,[Status]
      ,[SL_UFI]
      ,[MICODE]
      ,[IsModified]
      ,[IsProcessed]
      ,[AddWho]
      ,[AddDateTime]
      ,[EditWho]
      ,[EditDateTime]
  FROM [CouponCalculator].[dbo].[PostCodes]

end


GO
