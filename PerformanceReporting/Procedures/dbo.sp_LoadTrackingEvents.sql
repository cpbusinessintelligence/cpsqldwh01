SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO












CREATE proc [dbo].[sp_LoadTrackingEvents] --'2014-10-01'

(@FromDate date,@ToDate date )

 as

begin



     --'=====================================================================

    --' CP -Stored Procedure -[sp_LoadTrackingEvents]

    --' ---------------------------

    --' Purpose: Full load of small tables-----

    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)

    --' Date: 01 Dec 2016

    --' Copyright: 2014 Couriers Please Pty Ltd

    --' Change Log: 

    --' Date          Who     Ver     Reason                                            

    --' ----          ---     ---     -----                                            

    --' 01/12/2016    AK      1.00    Created the procedure                            



    --'=====================================================================



--declare @FromDate date

--declare @ToDate date



--set @FromDate='2016-12-20'

--set @ToDate='2016-12-20'





   Declare @StartDate as DateTime

   Declare @EndDate as DateTime





   Select @StartDate = CAST(CAST(@FromDate AS DATE) AS DATETIME) + CAST(CAST('00:00:00' AS TIME) AS DATETIME)

   Select @EndDate = CAST(CAST(@ToDate AS DATE) AS DATETIME) + CAST(CAST('23:59:59' AS TIME) AS DATETIME)



   declare @Trackingeventdays int

   set @Trackingeventdays= (select  value FROM [CentralAdmin].[dbo].[Parameters] where code='Trackingeventdays')



   SELECT Rtrim(Ltrim(L.LabelNumber)) as LabelNumber

         ,min([EventDateTime]) as [EventDateTime]

		 ,min(T.Createddate) as [CreatedDateTime]

		 ,eventtypeid

		 ,E.Description

		 ,convert(varchar(100),'') as ProntoDriverCode

		 ,convert(varchar(100),'') as CosmosBranch

		 ,convert(varchar(100),'') as Code

		 ,convert(varchar(100),'') as ExceptionReason

		 ,convert(varchar(100),'') as AdditionalText  

		 ,convert(varchar(100),'') as AdditionalText2

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]

  INTO #TempTrackingEvents

FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id

                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

  WHERE T.CreatedDate >=dateadd(day,-@Trackingeventdays,@StartDate) and  T.CreatedDate <=@EndDate and L.LabelNumber not like 'JD%' and L.Labelnumber not like 'CPWSAV%'

  and Labelnumber not like '%SLCNA' and Labelnumber not like 'NHCLC%' and Labelnumber not like '%RTCNA' and  Labelnumber not like '%DPCNA' and  Labelnumber not like '191%'

  group by Rtrim(Ltrim(L.LabelNumber)) 

         ,eventtypeid

		 ,E.Description

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]
		  --Added by Sinshith
		 ,EventDateTime


update #TempTrackingEvents set ProntoDriverCode=D.ProntoDrivercode,CosmosBranch=B.CosmosBranch,Code=D.Code,[ExceptionReason]=ltrim(rtrim(isnull(T.ExceptionReason,''))),AdditionalText=LTRIM(RTRIM(REPLACE(T.AdditionalText1, 'Link Coupon ', ''))),AdditionalText2=T.AdditionalText2

from #TempTrackingEvents  te join [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) on T.Sourcereference=te.labelnumber and t.EventDateTime=te.eventdatetime  and te.eventtypeid=T.eventtypeid

                             --join [ScannerGateway].[dbo].EventType E (NOLOCK)on  E.Description=te.description

							Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

							Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1





  CREATE CLUSTERED INDEX #TempTrackingEventsidx ON #TempTrackingEvents(Description,LabelNumber)

  PRINT 'Loaded all Non International Scanning Eventts into TempTable'



		 

   SELECT Rtrim(Ltrim(L.LabelNumber)) as LabelNumber

         ,max([EventDateTime]) as [EventDateTime]

		 ,max(T.CreatedDate) as CreatedDatetime

		 ,eventtypeid

		 ,E.Description

		 ,convert(varchar(100),'') as ProntoDriverCode

		 ,convert(varchar(100),'') as CosmosBranch

		 ,convert(varchar(100),'') as Code

		 ,convert(varchar(100),'') as ExceptionReason

		 ,convert(varchar(100),'') as AdditionalText  

		 ,convert(varchar(100),'') as AdditionalText2

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]

  INTO #TempQueryCage

  FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id

                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

  WHERE T.CreatedDate >=dateadd(day,-@Trackingeventdays,@StartDate) and  T.CreatedDate <=@EndDate and L.LabelNumber not like 'JD%' and L.Labelnumber not like 'CPWSAV%'

  and isnull(additionaltext1,'') in ('6501','6502','6503','6504','6505','6506','6507','6508','6509','6510','6511','6512','6513')

  group by Rtrim(Ltrim(L.LabelNumber)) 

         ,eventtypeid

		 ,E.Description

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]
		 ,EventDateTime





update #TempQueryCage set ProntoDriverCode=D.ProntoDrivercode,CosmosBranch=B.CosmosBranch,Code=D.Code,[ExceptionReason]=ltrim(rtrim(isnull(T.ExceptionReason,''))),AdditionalText=LTRIM(RTRIM(REPLACE(T.AdditionalText1, 'Link Coupon ', ''))),AdditionalText2=
T.AdditionalText2

from #TempQueryCage  te join [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) on T.Sourcereference=te.labelnumber and t.EventDateTime=te.eventdatetime and T.eventtypeid=te.EventTypeId

                            -- join [ScannerGateway].[dbo].EventType E (NOLOCK)on E.Description=te.description 

							Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

							Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1





 CREATE CLUSTERED INDEX #TempQueryCageidx ON #TempQueryCage(Description,LabelNumber)

  PRINT 'Loaded all Non International QueryCagescans  into TempTable'

  

  





 





  --drop table #TempTrackingEvents

  -- SELECT Rtrim(Ltrim(L.LabelNumber)) as LabelNumber

  --       ,[EventDateTime]

		-- ,E.Description

		-- ,D.ProntoDriverCode

		-- ,B.CosmosBranch

		-- ,D.Code

		-- ,[ExceptionReason]

		-- ,LTRIM(RTRIM(REPLACE([AdditionalText1], 'Link Coupon ', ''))) AS AdditionalText  

		-- ,AdditionalText2

		-- ,[NumberOfItems]

		-- ,[SourceReference]

		-- ,[CosmosSignatureId]

  --INTO #TempTrackingEvents

  --FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id

  --                                              join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

		--										Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

		--									    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

  --WHERE convert(date,[EventDateTime]) >='2016-11-09' and  convert(date,[EventDateTime]) <='2016-11-09' 

  --CREATE CLUSTERED INDEX #TempTrackingEventsidx ON #TempTrackingEvents(Description,LabelNumber)

  --PRINT 'Loaded all Scanning Eventts into TempTable'



  Insert into [dbo].[DeliveryOptions]( [Category]

                                      ,[Labelnumber]

									  ,consignmentcode

                                      ,[FailedDeliveryCardNumber]

                                      ,[Deliverymethod]

                                      ,[ScanDatetime]

                                      ,[PickupETAZone]

                                      ,[NewDeliveryETAZone]

                                      ,[AddWho]

                                      ,[AddDateTime])



  Select 'Redirection'

         ,l.labelnumber

		 ,c.consignmentcode

         ,''

		 ,[SelectedDeliveryOption]

		 ,c.[CreatedDateTime]

		 ,p.etazone

		 ,p1.etazone

		 ,'Sys'

		 ,getdate()

		 from EzyFreight.dbo.tblredirecteditemlabel l join EzyFreight.dbo.tblredirectedconsignment c on l.reconsignmentid=c.reconsignmentid

		                                          join EzyFreight.dbo.tbladdress a on a.addressid=[PickupAddressID]

												  join EzyFreight.dbo.tbladdress a1 on a1.addressid=[NewDeliveryAddressID]

												  join Postcodes p on p.postcode=a.postcode and p.suburb=a.suburb

												  join Postcodes p1 on p1.postcode=a1.postcode and p1.suburb=a1.suburb



         where c.[CreatedDateTime] between @StartDate and @EndDate







  Insert into [dbo].[DeliveryOptions]( [Category]

                                      ,[Labelnumber]

									  ,consignmentcode

                                      ,[FailedDeliveryCardNumber]

                                      ,[ScanDatetime]

									  ,ETADate

                                      ,[AddWho]

                                      ,[AddDateTime])



  Select 'Redelivery on a Date'

         ,l.lablenumber

         , l.ConsignmentCode

		 ,l.[CardReferenceNumber]

		 ,l.[CreatedDateTime]

		 ,CAST(l.[SelectedETADate] AS DATETIME) +  CAST(CAST('23:59:59' AS TIME) AS DATETIME)

		 ,'Sys'

		 ,getdate()

	 from [EzyFreight].[dbo].[tblRedelivery]  l where l.[CreatedDateTime] between @StartDate and @EndDate

		and isprocessed=1 

	



  --select * from #TempTrackingEvents where  description='redelivery'

  

  --AdditionalText<>''





  select LabelNumber,

         min(eventdatetime) as Firstscandatetime,

		 convert(varchar(50),'') as FirstscanEventtype,

		 convert(varchar(50),'') as FirstscanProntoDrivercode,

		 convert(varchar(50),'') as FirstscanDrivercode,

		 convert(varchar(50),'') as FirstscanBranch

  into #tempFirstscan

  from #TempTrackingEvents

  group by LabelNumber



  update #tempFirstscan set FirstscanEventtype=Te.Description,FirstscanProntoDrivercode=te.ProntoDriverCode,FirstscanDrivercode=te.Code,FirstscanBranch=te.CosmosBranch

  from #tempFirstscan t join #TempTrackingEvents Te on Te.Labelnumber=t.Labelnumber and Te.EventDateTime=t.Firstscandatetime





  select LabelNumber,

         max(eventdatetime) as Lastscandatetime,

		 convert(varchar(50),'') as LastscanEventtype,

		 convert(varchar(50),'') as LastscanProntoDrivercode,

		 convert(varchar(50),'') as LastscanDrivercode,

		 convert(varchar(50),'') as LastscanBranch

  into #tempLastscan

  from #TempTrackingEvents

  group by LabelNumber



  update #tempLastscan set LastscanEventtype=Te.Description,LastscanProntoDrivercode=te.ProntoDriverCode,LastscanDrivercode=te.Code,LastscanBranch=te.CosmosBranch

  from #tempLastscan t join #TempTrackingEvents Te on Te.Labelnumber=t.Labelnumber and Te.EventDateTime=t.Lastscandatetime









 ------------------------

 -------Firstscan for NonInternational-----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET [FirstScannedBy]=dbo.fn_CreateUniqueDriverID(FirstscanBranch,FirstscanDrivercode,FirstscanProntoDrivercode)

                                       ,[FirstDateTime]=t.Firstscandatetime

                                       ,[FirstScanType]=FirstscanEventtype

          From  #tempFirstscan T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere   [FirstDateTime] is  null 

PRINT 'Updated DIrect Firt Scans'





 ------------------------

 -------Pickup for NonInternational-----------

 ------------------------

  Update [dbo].[PrimaryLabels] SET PickupDateTime = T.EventDateTime,

                                       PickupScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Pickup' and PickupDateTime is  null

PRINT 'Updated DIrect Pickup Scans'

 ------------------------

 -------Handover for NonInternational----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET [HandoverDateTime] = T.EventDateTime,

                                        [HandoverScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Handover'  and [HandoverDateTime] is  null

PRINT 'Updated DIrect Handover Scans'

 ------------------------

 -------InDepot for NonInternational----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET InDepotDateTime = T.EventDateTime,

                                       InDepotScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'In Depot'  and InDepotDateTime is  null

PRINT 'Updated DIrect InDepot Scans'



 ------------------------

 -------Transfer for NonInternational----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET TransferDateTime = T.EventDateTime,

                                        TransferScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Transfer'  and TransferDateTime is  null

PRINT 'Updated DIrect Transfer Scans'







   Update [dbo].[PrimaryLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,

                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

										[AttemptedDeliveryReceivedDatetime]=T.CreatedDateTime

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Transfer'  and AttemptedDeliveryDateTime is  null and t.AdditionalText='6500'

PRINT 'Updated DIrect AttemptedDelivery Scans'



 ------------------------

 -------Consolidate for NonInternational----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET Consolidatedatetime = T.EventDateTime,

                                        [ConsolidateScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

                                        [ConsolidatedBarcode]=Additionaltext

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Consolidate'  and Consolidatedatetime is  null

PRINT 'Updated DIrect Consolidatedatetime Scans'



 ------------------------

 -------DeConsolidate for NonInternational----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET DeConsolidatedatetime = T.EventDateTime,

                                        [DeConsolidateScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Deconsolidate'  and DeConsolidatedatetime is  null

PRINT 'Updated DIrect Consolidatedatetime Scans'





 ------------------------

 -------OutForDelivery for NonInternational---

 ------------------------
 Update #TempTrackingEvents Set [ExceptionReason] = Replace([ExceptionReason],'31 Sep','1 Oct')
   Update [dbo].[PrimaryLabels] SET OutForDeliverDateTime = T.EventDateTime,OutforDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Out For Delivery'  and OutForDeliverDateTime is  null and Exceptionreason<>'In Depot'

PRINT 'Updated DIrect Out For Delivery Scans'



 ------------------------

 -------DelayofDelivery for NonInternational---

 ------------------------
 Update #TempTrackingEvents Set [ExceptionReason] = Replace ([ExceptionReason],'29 Feb 2021','1 Mar 2021') Where [Description] = 'Delay of Delivery' 

   Update [dbo].[PrimaryLabels] SET DelayofDeliveryScannedDateTime = T.EventDateTime,DelayofDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

                                    ,DelayofDeliveryDate=cast(substring(replace(T.[ExceptionReason],'Delay: ',''),1,(charindex(':',replace(T.[ExceptionReason],'Delay: ',''),1)-1)) as date)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Delay of Delivery'  and DelayofDeliveryDate is  null

PRINT 'Updated DIrect Out For Delivery Scans'







 ------------------------

 -------QueryCage for NonInternational---

 ------------------------

   Update [dbo].[PrimaryLabels] SET [QuerycageDateTime] = T.EventDateTime,[QuerycageScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

                                    ,[QueryCageNumber]=Additionaltext

          From  #TempQueryCage T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

PRINT 'Updated DIrect  For QueryCage Scans'





 --------------------------------

 -------AcceptedbyPOPPoint for NonInternational---

 ----------------------------------



    Update [dbo].[PrimaryLabels] SET [AcceptedbyPOPPointStatus]=T.Description ,[AcceptedbyPOPPointScannedDateTime] = T.EventDateTime,[AcceptedbyPOPPointScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description  in ('Drop off in POPStation','Accepted by NewsAgent')  and [AcceptedbyPOPPointScannedDateTime] is  null



PRINT 'Updated Direct AcceptedbyPOPPoint Scans'



 ----------------------------

 -------Attempted Delivery for NonInternational---

 ----------------------------

   Update [dbo].[PrimaryLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,

                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

										[AttemptedDeliveryReceivedDatetime]=T.CreatedDateTime,

										[AttemptedDeliveryReason] = T.ExceptionReason

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Attempted Delivery' and AttemptedDeliveryDateTime is  null

PRINT 'Updated DIrect Attempted Delivery Scans'





   Update [dbo].[PrimaryLabels] SET [AttemptedDeliveryCardNumber]=Additionaltext

      ,[CardCategory]=  case when ( isnull(additionaltext,'') like 'NHCLC%' or isnull(additionaltext,'') like '%RTCNA' or isnull(additionaltext,'') like 'NHCLC%') then 'Redelivery to Hubbed'

	                         when isnull(additionaltext,'') like '%SLCNA' then 'Redelivery to POPStation'

			                 when  ( isnull(additionaltext,'') like '191%' or isnull(additionaltext,'') like '%DPCNA')  then 'Standard Redelivery' else '' end

      ,[DeliveryOptionId]=d.[DeliveryOptionid]

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		                              left join [dbo].[DeliveryOptions] d on d.[FailedDeliveryCardNumber]=Additionaltext



		  WHere  T.Description = 'Link Scan' and  ( isnull(additionaltext,'') like 'NHCLC%' or isnull(additionaltext,'') like '191%' or isnull(additionaltext,'') like '%CNA' or isnull(additionaltext,'') like 'NHCLC%')  and isnull([CardCategory],'')=''

 



  --Update [dbo].[PrimaryLabels] SET [CardCategory]='Redelivery On a Date',[ETADate]=d.ETADate,[Reasoncode]='ETA override as Redelivery requested on a different date' from #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber


		--                                                                                            left join [dbo].[DeliveryOptions] d on  d.labelnumber=P.labelnumber

		--																							where [CardCategory]='Standard Redelivery'  and d.Category='Redelivery On a Date'



 --select * from  [dbo].[PrimaryLabels] where AttemptedDeliveryDateTime is not null

 ------------------------

 -------Delivered for NonInternational----------

 ------------------------

  Update [dbo].[PrimaryLabels] SET [DeliveryDateTime] = T.EventDateTime,

                            DeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

									   [IsPODPresent] =  CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0 OR ExceptionReason = 'atl') THEN 'Y' ELSE 'N' END,

									   [PODDateTime] = CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0 OR ExceptionReason = 'atl') THEN T.EventDateTime ELSE Null END,

									   [DeliveryReceivedDateTime]=T.CreatedDateTime

          From  #TempTrackingEvents T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Delivered'  and [DeliveryDateTime] is  null

PRINT 'Updated DIrect Delivered Scans'		





 ------------------------

 -------Lastscan for NonInternational-----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET     [LastScannedBy]=dbo.fn_CreateUniqueDriverID(LastscanBranch,LastscanDrivercode,LastscanProntoDrivercode)

                                       ,[LastDateTime]=t.Lastscandatetime

                                       ,[LastScanType]=LastscanEventtype

          From  #tempLastscan T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  ( [LastDateTime] is  null or t.Lastscandatetime>[LastDateTime])

PRINT 'Updated DIrect Lastscans'

	





---------------International------------------



    SELECT Rtrim(Ltrim(L.LabelNumber)) as LabelNumber

         ,min([EventDateTime]) as [EventDateTime]

		-- ,min(T.CreatedDate) as CreatedDatetime

		 ,eventtypeid

		 ,E.Description

		 ,convert(varchar(100),'') as ProntoDriverCode

		 ,convert(varchar(100),'') as CosmosBranch

		 ,convert(varchar(100),'') as Code

		 ,convert(varchar(100),'') as ExceptionReason

		 ,convert(varchar(100),'') as AdditionalText  

		 ,convert(varchar(100),'') as AdditionalText2

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]

  INTO #TempTrackingEvents1

  FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id

                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

  WHERE T.CreatedDate >=dateadd(day,-@Trackingeventdays,@StartDate) and  T.CreatedDate <=@EndDate and (L.LabelNumber  like 'JD%' or L.Labelnumber  like 'CPW%')

    group by Rtrim(Ltrim(L.LabelNumber)) 

	      ,eventtypeid

		 ,E.Description

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]
		 --Added by Sinshith
		 ,EventDateTime







update #TempTrackingEvents1 set ProntoDriverCode=D.ProntoDrivercode,CosmosBranch=B.CosmosBranch,Code=D.Code,[ExceptionReason]=ltrim(rtrim(isnull(T.ExceptionReason,''))),AdditionalText=LTRIM(RTRIM(REPLACE(T.AdditionalText1, 'Link Coupon ', ''))),AdditionalText2=T.AdditionalText2

from #TempTrackingEvents1  te join [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) on T.Sourcereference=te.labelnumber and t.EventDateTime=te.eventdatetime and t.eventtypeid=te.eventtypeid

                            -- join [ScannerGateway].[dbo].EventType E (NOLOCK)on E.Description=te.description

							Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

							Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1





  CREATE CLUSTERED INDEX #TempTrackingEventsidx1 ON #TempTrackingEvents1(Description,LabelNumber)

  PRINT 'Loaded all International Eventts into TempTable'





  select LabelNumber,

         min(eventdatetime) as Firstscandatetime,

		 convert(varchar(50),'') as FirstscanEventtype,

		 convert(varchar(50),'') as FirstscanProntoDrivercode,

		 convert(varchar(50),'') as FirstscanDrivercode,

		 convert(varchar(50),'') as FirstscanBranch

  into #tempFirstscan1

  from #TempTrackingEvents1

  group by LabelNumber



  update #tempFirstscan1 set FirstscanEventtype=Te.Description,FirstscanProntoDrivercode=te.ProntoDriverCode,FirstscanDrivercode=te.Code,FirstscanBranch=te.CosmosBranch

  from #tempFirstscan1 t join #TempTrackingEvents1 Te on Te.Labelnumber=t.Labelnumber and Te.EventDateTime=t.Firstscandatetime





  select LabelNumber,

         max(eventdatetime) as Lastscandatetime,

		 convert(varchar(50),'') as LastscanEventtype,

		 convert(varchar(50),'') as LastscanProntoDrivercode,

		 convert(varchar(50),'') as LastscanDrivercode,

		 convert(varchar(50),'') as LastscanBranch

  into #tempLastscan1

  from #TempTrackingEvents1

  group by LabelNumber



  update #tempLastscan1 set LastscanEventtype=Te.Description,LastscanProntoDrivercode=te.ProntoDriverCode,LastscanDrivercode=te.Code,LastscanBranch=te.CosmosBranch

  from #tempLastscan1 t join #TempTrackingEvents1 Te on Te.Labelnumber=t.Labelnumber and Te.EventDateTime=t.Lastscandatetime







 ------------------------

 -------Firstscan for International-----------

 ------------------------

   Update [dbo].[InternationalLabels] SET [FirstScannedBy]=dbo.fn_CreateUniqueDriverID(FirstscanBranch,FirstscanDrivercode,FirstscanProntoDrivercode)

                                       ,[FirstscanDateTime]=t.Firstscandatetime

                                       ,[FirstScanType]=FirstscanEventtype

          From  #tempFirstscan1 T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere   p.[FirstscanDateTime] is  null

PRINT 'Updated DIrect Firt Scans for International'





 ------------------------

 -------Pickup for International-----------

 ------------------------

  Update [dbo].[InternationalLabels] SET PickupDateTime = T.EventDateTime,

                                       PickupScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents1 T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Pickup' and PickupDateTime is  null

PRINT 'Updated DIrect Pickup Scans for International'









 ------------------------

 -------Consolidate for International----------

 ------------------------

   Update [dbo].[InternationalLabels] SET Consolidatedatetime = T.EventDateTime,

                                        [ConsolidateScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

                                        [ConsolidatedBarcode]=Additionaltext

          From  #TempTrackingEvents1 T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Consolidate'  and Consolidatedatetime is  null

PRINT 'Updated DIrect Consolidatedatetime Scans for International'

 ------------------------

 -------Customs Clearance for International---

 ------------------------

   Update [dbo].[InternationalLabels] SET [CustomsClearanceDateTime] = T.EventDateTime, [CustomsClearanceBy]= dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents1 T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Customs Clearance Processing Complete'  and [CustomsClearanceDateTime] is  null

PRINT 'Updated Customs Clearance Processing Complete scans for International'





 ----------------------------

 -------Attempted Delivery for International---

 ----------------------------

   Update [dbo].[InternationalLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,

                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents1 T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Attempted Delivery' and AttemptedDeliveryDateTime is  null

PRINT 'Updated DIrect Attempted Delivery Scans for International'





 --select * from  [dbo].[PrimaryLabels] where AttemptedDeliveryDateTime is not null

 ------------------------

 -------Delivered for International----------

 ------------------------

  Update [dbo].[InternationalLabels] SET [DeliveryDateTime] = T.EventDateTime,

                                       DeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

									   [IsPODPresent] =  CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0  OR ExceptionReason = 'atl') THEN 'Y' ELSE 'N' END,

									   [PODDateTime] = CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0  OR ExceptionReason = 'atl') THEN T.EventDateTime ELSE Null END

          From  #TempTrackingEvents1 T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Delivered'  and [DeliveryDateTime] is  null

PRINT 'Updated DIrect Delivered Scans for International'		





 ------------------------

 -------Lastscan for International-----------

 ------------------------

   Update [dbo].[InternationalLabels] SET     [LastScannedBy]=dbo.fn_CreateUniqueDriverID(LastscanBranch,LastscanDrivercode,LastscanProntoDrivercode)

                                       ,[LastDateTime]=t.Lastscandatetime

                                       ,[LastScanType]=LastscanEventtype

          From  #tempLastscan1 T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  ( [LastDateTime] is  null or t.Lastscandatetime>[LastDateTime])

PRINT 'Updated DIrect Last Scans for International'





--AWB Scanned for International-----------





Select    Rtrim(Ltrim(L.LabelNumber)) as LabelNumber

         ,[EventDateTime] as [EventDateTime]

		 ,E.Description

		 ,D.ProntoDriverCode

		 ,B.CosmosBranch

		 ,D.Code

		 ,ltrim(rtrim([ExceptionReason])) as [ExceptionReason]

		 ,LTRIM(RTRIM(REPLACE([AdditionalText1], 'Link Coupon ', ''))) AS AdditionalText  

		 ,AdditionalText2

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]

		 into #TempTEInternationalAWB

from ScannerGateway.dbo.Trackingevent t (NOLOCK) join EzyFreight.dbo.tblDHLBarCodeImage b1 on sourcereference=AWBcode  

                                               join  EzyFreight.dbo.tblitemlabel l on l.consignmentid=b1.consignmentid

											     join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

                                               

where T.CreatedDate >=dateadd(day,-@Trackingeventdays,@StartDate) and  T.CreatedDate <=@EndDate 

--group by  Rtrim(Ltrim(L.LabelNumber)) 

--		 ,E.Description

--		 ,D.ProntoDriverCode

--		 ,B.CosmosBranch

--		 ,D.Code

--		 ,ltrim(rtrim([ExceptionReason])) 

--		 ,LTRIM(RTRIM(REPLACE([AdditionalText1], 'Link Coupon ', ''))) 

--		 ,AdditionalText2

--		 ,[NumberOfItems]

--		 ,[SourceReference]

--		 ,[CosmosSignatureId]





 CREATE CLUSTERED INDEX #TempTEInternationalAWBidx ON #TempTEInternationalAWB(Description,LabelNumber)







 

 ------------------------------------------------------------

 -------Pickup for International based on AWB scan-----------

 -----------------------------------------------------------

  Update [dbo].[InternationalLabels] SET PickupDateTime = T.EventDateTime,

                                       PickupScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTEInternationalAWB T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Pickup' and PickupDateTime is  null

PRINT 'Updated DIrect Pickup Scans for International based on AWB scan'









 -----------------------------------------------------------

 -------Consolidate for International based on AWB scan----------

 -----------------------------------------------------------

   Update [dbo].[InternationalLabels] SET Consolidatedatetime = T.EventDateTime,

                                        [ConsolidateScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

                                        [ConsolidatedBarcode]=Additionaltext

          From  #TempTEInternationalAWB T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Consolidate'  and Consolidatedatetime is  null

PRINT 'Updated DIrect Consolidatedatetime Scans for International based on AWB scan'



 -----------------------------------------------------------

 -------Customs Clearance for International---

 -----------------------------------------------------------

   Update [dbo].[InternationalLabels] SET [CustomsClearanceDateTime] = T.EventDateTime, [CustomsClearanceBy]= dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTEInternationalAWB T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Customs Clearance Processing Complete'  and [CustomsClearanceDateTime] is  null

PRINT 'Updated Customs Clearance Processing Complete scans for International based on AWB scan'





 -----------------------------------------------------------

 -------Attempted Delivery for International based on AWB scan---

 -----------------------------------------------------------

   Update [dbo].[InternationalLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,

                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTEInternationalAWB T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Attempted Delivery' and AttemptedDeliveryDateTime is  null

PRINT 'Updated DIrect Attempted Delivery Scans for International based on AWB scan'





 --select * from  [dbo].[PrimaryLabels] where AttemptedDeliveryDateTime is not null

 -----------------------------------------------------------

 -------Delivered for International based on AWB scan----------

 --------------------------------------------------------------

  Update [dbo].[InternationalLabels] SET [DeliveryDateTime] = T.EventDateTime,

                                       DeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

									   [IsPODPresent] =  CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0  OR ExceptionReason = 'atl') THEN 'Y' ELSE 'N' END,

									   [PODDateTime] = CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0  OR ExceptionReason = 'atl') THEN T.EventDateTime ELSE Null END

          From  #TempTEInternationalAWB T join [dbo].[InternationalLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Delivered'  and [DeliveryDateTime] is  null

PRINT 'Updated DIrect Delivered Scans for International based on AWB scan'		







---------------------------

----Secondary Labels for NonInternational-------

----------------------------





 SELECT Rtrim(Ltrim(L.LabelNumber)) as LabelNumber

         ,[EventDateTime]

		 ,E.Description

		 ,T.CreatedDate

		 ,D.ProntoDriverCode

		 ,B.CosmosBranch

		 ,D.Code

		 ,[ExceptionReason]

		 ,LTRIM(RTRIM(REPLACE([AdditionalText1], 'Link Coupon ', ''))) AS AdditionalText  

		 ,AdditionalText2

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]

  INTO #TempTrackingEventforSecondarylabels

FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id

                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

  WHERE T.CreatedDate >=dateadd(day,-@Trackingeventdays,@StartDate) and  T.CreatedDate <=@EndDate and L.LabelNumber not like 'JD%' and L.Labelnumber not like 'CPWSAV%'

  and Labelnumber not like '%SLCNA' and Labelnumber not like 'NHCLC%' and Labelnumber not like '%RTCNA' and  Labelnumber not like '%DPCNA' and  Labelnumber not like '191%'





  Select T.* ,

         P.Category  as SecondaryCouponCategory ,

		 Convert(Varchar(20),'') as PrimaryCouponCategory

     Into #TempLinkCoupons

     from #TempTrackingEventforSecondarylabels T Join dbo.SecondaryLabels S on T.LabelNumber =S.SecondaryLabelNumber

                                Join dbo.DimProduct P on Left(LabelNumber,3) = P.Code and P.RevenueType = 'Prepaid'

     where t.Description = 'Link Scan' 



   Update #TempLinkCoupons SET PrimaryCouponCategory =P.RevenueCategory  

                           From #TempLinkCoupons T Join   dbo.DimProduct P on Left(AdditionalText,3) = P.Code and P.RevenueType = 'Prepaid'





   Update dbo.SecondaryLabels SET PrimaryLabelNumber =T.AdditionalText,

                                        ScanDateTime = T.EventDateTime,

										[ScannedBy] = dbo.fn_CreateUniqueDriverID(T.CosmosBranch,T.Code,T.ProntoDriverCode)

                                 From #TempLinkCoupons T Join dbo.SecondaryLabels S on T.LabelNumber = S.SecondaryLabelNumber

                                 Where T.PrimaryCouponCategory ='PRIMARY'  

   

     Update dbo.SecondaryLabels SET PrimaryLabelNumber =T.AdditionalText,

	                                      ScanDateTime = T.EventDateTime,

										  [ScannedBy] = dbo.fn_CreateUniqueDriverID(T.CosmosBranch,T.Code,T.ProntoDriverCode)

                                 From #TempLinkCoupons T Join dbo.SecondaryLabels S on T.LabelNumber = S.SecondaryLabelNumber

                                 Where T.PrimaryCouponCategory ='' and Isnull(S.PrimaryLabelNumber,'') = '' 

PRINT 'Updated all Secondary Scans'	



Update Primarylabels set [ProductSubCategory]='RP' from   #TempLinkCoupons T Join dbo.SecondaryLabels S on T.LabelNumber = S.SecondaryLabelNumber

                                                                             join dbo.Primarylabels l on l.labelnumber=S.Primarylabelnumber

							where [SecondaryLabelPrefix] in ('214','314','414','514','614','714','814','914') and isnull([ProductSubCategory],'')<>'RP'



 Drop Table #TempTrackingEvents

-------------------------------------

-- Fill Tracking Events based on Secondary Labels

------------------------------------------

SELECT [SecondaryLabelNumber]

      ,[PrimaryLabelNumber]

  into #TempSecondaryLabels

  FROM [dbo].[SecondaryLabels]

  Where PrimaryLabelNumber <>'' and [LabelCreatedDateTime]>= Dateadd(Day,-7,@StartDate) and LabelCreatedDateTime <= @EndDate



 PRINT 'Loaded  all Secondary Scans which is linked to Primary fro the last 7 days'	



      SELECT Rtrim(Ltrim(L.LabelNumber)) as SecondaryLabelNumber

	     ,SL.PrimaryLabelNumber as LabelNumber

         ,min([EventDateTime]) as [EventDateTime]

		 ,min(T.CreatedDate) as CreatedDatetime

		 ,eventtypeid

		 ,E.Description

	     ,convert(varchar(100),'') as ProntoDriverCode

		 ,convert(varchar(100),'') as CosmosBranch

		 ,convert(varchar(100),'') as Code

		 ,convert(varchar(100),'') as ExceptionReason

		 ,convert(varchar(100),'') as AdditionalText  

		 ,convert(varchar(100),'') as AdditionalText2

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]

  INTO #TempTrackingEvents2

  FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id

                                                Join #TempSecondaryLabels SL on L.LabelNumber =SL.SecondaryLabelNumber

                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

										Where E.Description <> 'Link Scan'

group by Rtrim(Ltrim(L.LabelNumber))

	     ,SL.PrimaryLabelNumber

		 ,eventtypeid

		 ,E.Description

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]
		  --Added by Sinshith
		 ,EventDateTime












update #TempTrackingEvents2 set ProntoDriverCode=D.ProntoDrivercode,CosmosBranch=B.CosmosBranch,Code=D.Code,[ExceptionReason]=ltrim(rtrim(isnull(T.ExceptionReason,''))),AdditionalText=LTRIM(RTRIM(REPLACE(T.AdditionalText1, 'Link Coupon ', ''))),AdditionalText2=T.AdditionalText2

from #TempTrackingEvents2  te join [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) on T.Sourcereference=te.SecondaryLabelNumber and t.EventDateTime=te.eventdatetime and te.eventtypeid=t.eventtypeid

                             join [ScannerGateway].[dbo].EventType E (NOLOCK)on E.Description=te.description

							Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

							Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1





  CREATE CLUSTERED INDEX #TempTrackingEvents2idx ON #TempTrackingEvents2(Description,LabelNumber)



   PRINT 'Loaded  all Scanning Events fro the secondary labels'	





      SELECT Rtrim(Ltrim(L.LabelNumber)) as SecondaryLabelNumber

	     ,SL.PrimaryLabelNumber as LabelNumber

         ,max([EventDateTime]) as [EventDateTime]

		 ,max(T.CreatedDate) as CreatedDatetime

		 ,eventtypeid

		 ,E.Description

		 ,convert(varchar(100),'') as ProntoDriverCode

		 ,convert(varchar(100),'') as CosmosBranch

		 ,convert(varchar(100),'') as Code

		 ,convert(varchar(100),'') as ExceptionReason

		 ,convert(varchar(100),'') as AdditionalText  

		 ,convert(varchar(100),'') as AdditionalText2

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]

  INTO #TempQueryCage2

  FROM [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) Join [ScannerGateway].[dbo].Label L (NOLOCK) on T.LabelId = L.id

                                                Join #TempSecondaryLabels SL on L.LabelNumber =SL.SecondaryLabelNumber

                                                join [ScannerGateway].[dbo].EventType E (NOLOCK)on T.EventTypeId = E.Id

												Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

											    Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1

										Where E.Description <> 'Link Scan' and  isnull(additionaltext1,'') in ('6501','6502','6503','6504','6505','6506','6507','6508','6509','6510','6511','6512','6513')

group by Rtrim(Ltrim(L.LabelNumber))

	     ,SL.PrimaryLabelNumber

		 ,eventtypeid

		 ,E.Description

		 ,[NumberOfItems]

		 ,[SourceReference]

		 ,[CosmosSignatureId]
		  --Added by Sinshith
		 ,EventDateTime


update #TempQueryCage2 set ProntoDriverCode=D.ProntoDrivercode,CosmosBranch=B.CosmosBranch,Code=D.Code,[ExceptionReason]=ltrim(rtrim(isnull(T.ExceptionReason,''))),AdditionalText=LTRIM(RTRIM(REPLACE(T.AdditionalText1, 'Link Coupon ', ''))),AdditionalText2
=T.AdditionalText2

from #TempQueryCage2  te join [ScannerGateway].[dbo].[TrackingEvent] T (NOLOCK) on T.Sourcereference=te.SecondaryLabelNumber and t.EventDateTime=te.eventdatetime and te.eventtypeid=t.eventtypeid

                             --join [ScannerGateway].[dbo].EventType E (NOLOCK)on E.Description=te.description

							Left Join [ScannerGateway].[dbo].Driver D  (NOLOCK) on T.DriverId = D.Id 

							Left Join [ScannerGateway].[dbo].Branch B on D.BranchId = B.Id and B.isactive= 1







     PRINT 'Loaded  all  Query cage Scanning Events fro the secondary labels'	 



 ------------------------

 -------Pickup-----------

 ------------------------

  Update [dbo].[PrimaryLabels] SET PickupDateTime = T.EventDateTime,

                                       PickupScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Pickup' and PickupDateTime is null

PRINT 'Updated Secondary Pickup Scans'

 ------------------------

 -------Handover----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET [HandoverDateTime] = T.EventDateTime,

                                        [HandoverScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Handover' and HandoverDateTime is null

PRINT 'Updated Secondary Handover Scans'

 ------------------------

 -------InDepot----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET InDepotDateTime = T.EventDateTime,

                                       InDepotScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'In Depot'  and InDepotDateTime is null

PRINT 'Updated Secondary InDepot Scans'



 ------------------------

 -------Transfer----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET TransferDateTime = T.EventDateTime,

                                        TransferScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Transfer' and  TransferDateTime is null

PRINT 'Updated Secondary Transfer Scans'





   Update [dbo].[PrimaryLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,

                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

										[AttemptedDeliveryReceivedDatetime]=T.CreatedDateTime

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Transfer'  and AttemptedDeliveryDateTime is  null and t.AdditionalText='6500'

PRINT 'Updated Secondary AttemptedDelivery Scans'





 ------------------------

 -------Consolidate for NonInternational----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET Consolidatedatetime = T.EventDateTime,

                                        [ConsolidateScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

                                        [ConsolidatedBarcode]=Additionaltext

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Consolidate'  and Consolidatedatetime is  null

PRINT 'Updated Secondary Consolidatedatetime Scans'





 ------------------------

 -------DeConsolidate for NonInternational----------

 ------------------------

   Update [dbo].[PrimaryLabels] SET DeConsolidatedatetime = T.EventDateTime,

                                        [DeConsolidateScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Deconsolidate'  and DeConsolidatedatetime is  null

PRINT 'Updated Secondary DeConsolidatedatetime Scans'





 ------------------------

 -------OutForDelivery---

 ------------------------

   Update [dbo].[PrimaryLabels] SET OutForDeliverDateTime = T.EventDateTime,OutforDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Out For Delivery' and Exceptionreason<>'In Depot' and  OutForDeliverDateTime is null

PRINT 'Updated Secondary OutForDelivery Scans'







 ------------------------

 -------DelayofDelivery for NonInternational---

 ------------------------
  Update #TempTrackingEvents2 Set [ExceptionReason] = Replace ([ExceptionReason],'29 Feb 2021','1 Mar 2021') WHere  [Description] = 'Delay of Delivery'
   Update [dbo].[PrimaryLabels] SET DelayofDeliveryScannedDateTime = T.EventDateTime,DelayofDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

                                    ,DelayofDeliveryDate=cast(substring(replace(T.[ExceptionReason],'Delay: ',''),1,(charindex(':',replace(T.[ExceptionReason],'Delay: ',''),1)-1)) as date)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description = 'Delay of Delivery'  and DelayofDeliveryDate is  null

PRINT 'Updated Secondary Out For Delivery Scans'





 --------------------------------

 -------QueryCage for NonInternational---

 ----------------------------------



    Update [dbo].[PrimaryLabels] SET [QuerycageDateTime] = T.EventDateTime,[QuerycageScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

                                    ,[QueryCageNumber]=Additionaltext

          From  #TempQueryCage2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  and T.Eventdatetime>[QuerycageDateTime]



PRINT 'Updated Secondary QueryCage Scans'





 --------------------------------

 -------AcceptedbyPOPPoint for NonInternational---

 ----------------------------------



    Update [dbo].[PrimaryLabels] SET [AcceptedbyPOPPointStatus]=T.Description ,[AcceptedbyPOPPointScannedDateTime] = T.EventDateTime,[AcceptedbyPOPPointScannedBy] = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode)

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber 

		  WHere  T.Description  in ('Drop off in POPStation','Accepted by NewsAgent')  and [AcceptedbyPOPPointScannedDateTime] is  null



PRINT 'Updated Secondary AcceptedbyPOPPoint Scans'







 ----------------------------

 -------Attempted Delivery---

 ----------------------------



    Update [dbo].[PrimaryLabels] SET AttemptedDeliveryDateTime = T.EventDateTime,

                                        AttemptedDeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

										AttemptedDeliveryReceivedDatetime=T.CreatedDatetime

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Attempted Delivery' and AttemptedDeliveryDateTime is  null





  

   Update [dbo].[PrimaryLabels] SET [AttemptedDeliveryCardNumber]=Additionaltext

      ,[CardCategory]=  case when ( isnull(additionaltext,'') like 'NHCLC%' or isnull(additionaltext,'') like '%RTCNA' or isnull(additionaltext,'') like 'NHCLC%') then 'Redelivery to Hubbed'

	                         when isnull(additionaltext,'') like '%SLCNA' then 'Redelivery to POPStation'

			                 when  ( isnull(additionaltext,'') like '191%' or isnull(additionaltext,'') like '%DPCNA')  then 'Standard Redelivery' else '' end

      ,[DeliveryOptionId]=d.[DeliveryOptionid]

          From  #TempTrackingEvents2 T     join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		                              left join [dbo].[DeliveryOptions] d on d.[FailedDeliveryCardNumber]=Additionaltext



		  WHere  T.Description = 'Link Scan' and  ( isnull(additionaltext,'') like 'NHCLC%' or isnull(additionaltext,'') like '191%' or isnull(additionaltext,'') like '%CNA' or isnull(additionaltext,'') like 'NHCLC%') and isnull([CardCategory],'')=''





    --Update [dbo].[PrimaryLabels] SET [CardCategory]='Redelivery On a Date' ,[ETADate]=d.ETADate,[Reasoncode]='ETA override as Redelivery requested on a different date' from #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  --                                                                                          left join [dbo].[DeliveryOptions] d on d.labelnumber=P.labelnumber

				--																					where [CardCategory]='Standard Redelivery'  and d.Category='Redelivery On a Date'



 

PRINT 'Updated Secondary Attempted Delivery Scans'

 ------------------------

 -------Delivered----------

 ------------------------

  Update [dbo].[PrimaryLabels] SET [DeliveryDateTime] = T.EventDateTime,

                                       DeliveryScannedBy = dbo.fn_CreateUniqueDriverID(CosmosBranch,Code,ProntoDriverCode),

									   [IsPODPresent] =  CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0  OR ExceptionReason = 'atl') THEN 'Y' ELSE 'N' END,

									   [PODDateTime] = CASE WHEN (T.AdditionalText2 like 'DLB%' OR T.[CosmosSignatureId] >0  OR ExceptionReason = 'atl') THEN T.EventDateTime ELSE Null END,

									   DeliveryReceivedDatetime=T.CreatedDatetime

          From  #TempTrackingEvents2 T join [dbo].[PrimaryLabels]  P on  T.LabelNumber=P.LabelNumber

		  WHere  T.Description = 'Delivered'  and [DeliveryDateTime] is null



PRINT 'Updated Secondary Delivered Scans'



-----------------

  update [PerformanceReporting].[dbo].[LinehaulData] set [ProductType]=p.producttype

  from [PerformanceReporting].[dbo].[LinehaulData] l join [PerformanceReporting].[dbo].Primarylabels p on p.ConsolidatedBarcode=l.ConsolidatedBarcode

  where p.producttype='Domestic Priority'





    update [PerformanceReporting].[dbo].[LinehaulData] set [ProductType]=p.producttype

  from [PerformanceReporting].[dbo].[LinehaulData] l join [PerformanceReporting].[dbo].Primarylabels p on p.ConsolidatedBarcode=l.ConsolidatedBarcode

  where p.producttype='Standard' and isnull(l.producttype,'')=''



  



    update [PerformanceReporting].[dbo].[LinehaulData] set [ProductType]=p.producttype

  from [PerformanceReporting].[dbo].[LinehaulData] l join [PerformanceReporting].[dbo].Primarylabels p on p.ConsolidatedBarcode=l.ConsolidatedBarcode

  where p.producttype='Domestic Off Peak' and isnull(l.producttype,'')=''









  Insert into [dbo].[DeliveryOptions]( [Category]

                                      ,[Labelnumber]

									  ,consignmentcode

                                      ,[FailedDeliveryCardNumber]

                                      ,[ScanDatetime]

									 -- ,ETADate

                                      ,[AddWho]

                                      ,[AddDateTime])



  Select  case when [CardCategory]='Redelivery to HUBBED' then 'Redelivery to HUBBED'

               when [CardCategory]='Redelivery to POPStation' then 'Redelivery to POPStation'

			   else '' end

         ,l.labelnumber

         ,[ConNote]

		 ,l.[AttemptedDeliveryCardNumber]

		 ,l.AttemptedDeliveryDateTime

		-- ,CAST(dateadd(day,1,AttemptedDeliveryDateTime) AS DATETIME) +  CAST(CAST('23:59:59' AS TIME) AS DATETIME)

		 ,'Sys'

		 ,getdate()

	 from [dbo].[PrimaryLabels]  l join consignment c on gwconsignmentid=l.[ConsignmentID] and c.[Sourcereference]=l.Sourcereference where  isnull(CardCategory,'') in ('Redelivery to Hubbed','Redelivery to POPStation') 

	and AttemptedDeliveryReceivedDatetime between @StartDate and @EndDate

		 and not exists (select 1 from DeliveryOptions d where l.labelnumber=d.labelnumber and l.CardCategory=d.category)

	

update Primarylabels set [CardCategory]= 'Redirected to '+d.Deliverymethod from Primarylabels l join Deliveryoptions d on l.labelnumber=d.Labelnumber

where D.Category='Redirection' and d.[ScanDatetime] between @StartDate and @EndDate and isnull(CardCategory,'')=''



	

end


GO
