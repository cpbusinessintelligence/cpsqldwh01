SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Load_ETACalculator_T] as
begin

--Truncate table ETACalculator
--Insert into ETACalculator(Category
--      ,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stPickupCutOffTime]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndPickupCutOffTime]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'Standard'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
      ,[STD_ETA]
      ,[STD_FromETA]
      ,[STD_ToETA]
      ,[STD_1stPickupCutOffTime]
      ,[STD_1stDeliveryCutOff]
      ,[STD_1stAllowedDays]
      ,[STD_2ndPickupCutOff]
      ,[STD_2ndDeliveryCutOffTime]
      ,[STD_2ndAllowedDays]
FROM [dbo].[ETA - Calculator - 2017-07-28]

--Insert into ETACalculator(Category
--      ,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stPickupCutOffTime]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndPickupCutOffTime]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


--Select 'InterState'
--      ,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ACE_Interstate_ETA]
--      ,[ACE_Interstate_FromETA]
--      ,[ACE_Interstate_ToETA]
--      ,[ACEInterstate_1stPickupCutOff]
--      ,[ACE_Interstate_1stDeliveryCutOff]
--      ,[ACE_Interstate_1stAllowedDays]
--      ,[ACE_Interstate_2ndPickupCutOff]
--      ,[ACE_Interstate_2ndDeliveryCutOff]
--      ,[ACE_Interstate_2ndAllowedDays]
--FROM [dbo].[ETA - Calculator - 2017-07-28]

--Insert into ETACalculator(Category
--      ,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stPickupCutOffTime]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndPickupCutOffTime]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'SameDay'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
            ,[ACE_SameDay_ETA]
      ,[ACE_SameDay_FromETA]
      ,[ACE_SameDay_ToETA]
      ,[ACE_SameDay_1stPickupCutOff]
      ,[ACE_SameDay_1stDeliveryCutOff]
      ,[ACE_SameDay_1stAllowedDays]
      ,[ACE_SameDay_2ndPickupCutOff]
      ,[ACE_SameDay_2ndDeliveryCutOff]
      ,[ACE_SameDay_2ndAllowedDays]
FROM [dbo].[ETA - Calculator - 2017-07-28]

--Insert into ETACalculator(Category
--      ,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stPickupCutOffTime]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndPickupCutOffTime]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'Domestic Priority'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
      ,[Domestic_Priority_ETA]
      ,[Domestic_Priority_FromETA]
      ,[Domestic_Priority_ToETA]
      ,[Domestic_Priority_1stPickupCutOff]
      ,[Domestic_Priority_1stDeliveryCutOff]
      ,[Domestic_Priority_1stAllowedDays]
      ,[Domestic_Priority_2ndPickupCutOff]
      ,[Domestic_Priority_2ndDeliveryCutOff]
      ,[Domestic_Priority_2ndAllowedDays]
FROM [dbo].[ETA - Calculator - 2017-07-28]


--Insert into ETACalculator(Category
--      ,[FromZone]
--      ,[ToZone]
--      ,[PrimaryNetworkCategory]
--      ,[SecondaryNetworkCategory]
--      ,[ETA]
--      ,[FromETA]
--      ,[ToETA]
--      ,[1stPickupCutOffTime]
--      ,[1stDeliveryCutOffTime]
--      ,[1stAllowedDays]
--      ,[2ndPickupCutOffTime]
--      ,[2ndDeliveryCutOffTime]
--      ,[2ndAllowedDays])


Select 'Domestic Off Peak'
      ,[FromZone]
      ,[ToZone]
      ,[PrimaryNetworkCategory]
      ,[SecondaryNetworkCategory]
      ,[Domestic_OffPeak_ETA]
      ,[Domestic_OffPeak_FromETA]
      ,[Domestic_OffPeak_ToETA]
      ,[Domestic_OffPeak_1stPickupCutOff]
      ,[Domestic_OffPeak_1stDeliveryCutOff]
      ,[Domestic_OffPeak_1stAllowedDays]
      ,[Domestic_OffPeak_2ndPickupCutOff]
      ,[Domestic_OffPeak_2ndDeliveryCutOff]
      ,[Domestic_OffPeak_2ndAllowedDays]
FROM [dbo].[ETA - Calculator - 2017-07-28]

END
GO
