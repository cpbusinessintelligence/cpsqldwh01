SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Proc sp_PrimaryLabelsPerfRep_SS (@AddDatetime DateTime)
as


Begin transaction PerfRep1

Insert into [dbo].[PrimaryLabels_Archive_17-18]
select * from [dbo].[PrimaryLabels]
where [AddDateTime] < = @AddDatetime

Delete from [dbo].[PrimaryLabels]
where [AddDateTime] < = @AddDatetime


Commit transaction PerfRep1

GO
