SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_PrimarylabelsArchiveProceedure] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in PerformaceReporting!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	09/11/2016	AK		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    



	 
	 If(@ArchiveDate='')
select @ArchiveEventDate=dateadd(month,-3,getdate())
else
Select @ArchiveEventDate = @ArchiveDate

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION trnLabelEvent


INSERT INTO [dbo].[PrimaryLabels_Archive17-18]
              ([PrimaryLabelsID]
      ,[GWConsignmentID]
      ,[RevenueType]
      ,[AccountCode]
      ,[LabelNumber]
      ,[PickupScannedBy]
      ,[PickupDateTime]
      ,[InDepotScannedBy]
      ,[InDepotDateTime]
      ,[HandoverScannedBy]
      ,[HandoverDateTime]
      ,[TransferScannedBy]
      ,[TransferDateTime]
      ,[OutforDeliveryScannedBy]
      ,[OutForDeliverDateTime]
      ,[AttemptedDeliveryScannedBy]
      ,[AttemptedDeliveryDateTime]
      ,[DeliveryScannedBy]
      ,[DeliveryDateTime]
      ,[IsPODPresent]
      ,[PODDateTime]
      ,[DeliveryContractorType]
      ,[PickupETAZone]
      ,[DeliveryETAZone]
      ,[BUCode]
      ,[NetworkCategoryID]
      ,[ETADate]
      ,[OnTimeStatus]
      ,[PerformanceReportingDate]
      ,[PerformanceProcessed]
      ,[AddWho]
      ,[LabelCreatedDateTime]
      ,[AddDateTime])


     			SELECT [PrimaryLabelsID]
      ,[ConsignmentID]
      ,[RevenueType]
      ,[AccountCode]
      ,[LabelNumber]
      ,[PickupScannedBy]
      ,[PickupDateTime]
      ,[InDepotScannedBy]
      ,[InDepotDateTime]
      ,[HandoverScannedBy]
      ,[HandoverDateTime]
      ,[TransferScannedBy]
      ,[TransferDateTime]
      ,[OutforDeliveryScannedBy]
      ,[OutForDeliverDateTime]
      ,[AttemptedDeliveryScannedBy]
      ,[AttemptedDeliveryDateTime]
      ,[DeliveryScannedBy]
      ,[DeliveryDateTime]
      ,[IsPODPresent]
      ,[PODDateTime]
      ,[DeliveryContractorType]
      ,[PickupETAZone]
      ,[DeliveryETAZone]
      ,[BUCode]
      ,[NetworkCategoryID]
      ,[ETADate]
      ,[OnTimeStatusId]
      ,[PerformanceReportingDate]
      ,[PerformanceProcessed]
      ,[AddWho]
      ,[LabelCreatedDateTime]
      ,[AddDateTime]
  FROM [dbo].Primarylabels(NOLOCK)
  WHERE [PerformanceReportingDate]<=@ArchiveEventDate 


	DELETE FROM Primarylabels WHERE [PerformanceReportingDate]<=@ArchiveEventDate

	COMMIT TRANSACTION trnLabelEvent

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------



GO
