SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_ReloadPerformancereportingData](@StartDate date,@EndDate date) as
begin

If (@EndDate='')
set @EndDate =convert(date,dateadd(day,-1,getdate()))



exec [sp_LoadConsignmentsandLabels] @StartDate
print '1 done'
exec [sp_LoadTrackingEvents] @StartDate,@EndDate
print '2 done'

exec [sp_CalculateETAandPerformance] @StartDate,@EndDate
print '3 done'

exec  [sp_LoadLineHauldata] @StartDate
print '4 done'

exec [sp_LoadDailyPerformanceReporting] @StartDate
print '5 done'
EXEC [sp_LoadDailyNoTimeReporting] @StartDate
print '6 done'
exec [sp_LoadDailyNoTimeReportingDetail] @StartDate
print '7 done'

exec [dbo].[sp_LoadDailyAgentReporting] @StartDate
print '8 done'

---Added by PV on 2020-10-15
--exec [dbo].[sp_LoadDailyFutileMissedPartialPickup_Summary] @StartDate
--print '9 done'
--exec [dbo].[sp_LoadDailyFutileMissedPartialPickup_Detail] @StartDate
--print '10 done'
----Ends here -- Addition on 2020-10-15 by PV--
end
GO
