SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<State Best 20 Drivers>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptDailyDepotBest20DriversOverview] (@Depo Varchar(20),@Date Date )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	
	Set @Depo = Case When @depo = 'TULLUMARINE' Then 'TULLAMARINE' Else @Depo End
   SELECT isnull(State,'Unknown') as State 
       ,d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4) as DeliveryDriver
       ,[OnTimeStatus]       
	    ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus] in (1,2) and D.Depotname = @Depo   
  and ContractorType in ('C','V','F','S') 
  Group by d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4),[OnTimeStatus] ,isnull(State,'Unknown')

  Update #Temp1 SET [Descr] =S.Description From #Temp1 T join [PerformanceReporting].dbo.DimStatus S on T.OnTimeStatus=S.id

  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DeliveryDriver = #Temp1.DeliveryDriver)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

   Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'

  Select top 20 * into #TempFinal from  #Temp1  where ontimestatus=1  Order by PerformanceKPI desc,PerformanceCount desc

   Select DeliveryDriver,[OnTimeStatus],[Descr],[PerformanceCount],Total,PerformanceKPI,TargetKPI
  from
  (
    Select * from #TempFinal 
    Union all
    Select * from #Temp1 Where OnTimeStatus = 2 and DeliveryDriver in (Select distinct DeliveryDriver From #TempFinal) 
  ) results
   order by OnTimeStatus asc , performanceKPI desc, PerformanceCount desc
END

GO
