SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_RptDailyDepotOverview]
(@Date Date , @Depo varchar(20))
 as
begin

 --'=====================================================================   
    --' Purpose: National Scanning Compliance
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 18 Jan 2017
    --'=====================================================================

  SET FMTONLY OFF;
  
   SET FMTONLY OFF;

	 SELECT [OnTimeStatus]
            ,SUM([PerformanceCount]) as [PerformanceCount]
      INTO #Temp1 
	  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id     
	                                                          JOIN  PerformanceReporting.[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                         
	  WHere C.Date = CONVERT(VARCHAR(10),@Date,111) and D.DepotName= @Depo
	  Group by [OnTimeStatus]

      SELECT id
            ,[Description]
			,Convert(Int,0) as Total
	        ,Convert(decimal(12,2),0) as Percentage
      into #TempStatusCodes    
    FROM PerformanceReporting.[dbo].DimStatus
 
      Update #TempStatusCodes SET Total  = T.PerformanceCount , 
                                  Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1 WHere [OnTimeStatus] in (1,2,4,5)),0)) 
      From  #TempStatusCodes S Join #Temp1 T on S.Id = T.[OnTimeStatus] 

	  Update #TempStatusCodes SET Percentage = 0 where id not in (1,2,4,5)

      Select * from #TempStatusCodes 

end



GO
