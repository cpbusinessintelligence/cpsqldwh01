SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDailyDepotPerformanceOverviewByState]
(@Date Date, @State varchar(20))
 as
begin

     SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyDepotPerformanceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
   SELECT Isnull(d.State,'Unknown') as State,Isnull(d.Branch,'Unknown') as Branch,    
	   (case m.State when @state then m.DepotName 
              else m.MappedTo end) as DepotName
       ,[OnTimeStatus]
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1         ---- hange deops from harly table
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID  
  left join [PerformanceReporting].[dbo].[Network_Depot_Mapping]  m on d.DepotName= m.Depotname and m.state= d.state
                                                      
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus] in(1,2) --and d.state= @State
  Group by Isnull(d.State,'Unknown')
          ,Isnull(d.Branch,'Unknown')
		  , (case m.State when @state then m.DepotName 
              else m.MappedTo end)
		  ,[OnTimeStatus]		  
  
  update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.[OnTimeStatus]
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DepotName = #Temp1.DepotName)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T 
  join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 Where DepotName not in ('Unknown')
  order by PerformanceKPI desc

end


GO
