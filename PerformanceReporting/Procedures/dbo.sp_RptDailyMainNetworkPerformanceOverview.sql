SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[sp_RptDailyMainNetworkPerformanceOverview]
(@Date Date )
 as
begin
       SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyMainNetworkPerformanceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
   
  SELECT isnull(N.[NetworkCategory],'Unknown') as [NetworkCategory]
	    ,CASE [OnTimeStatus] WHEN 1 THEN 'Y' WHEN 2 THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
		,Convert(int,0) as SortOrder
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK)
  JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  PerformanceReporting.[dbo].[NetworkCategory] N (NOLOCK) on P.[NetworkCategoryID] = N.NetworkCategoryID
                                                      
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus] in (1,2)
  --[OnTimeStatus] Not in ('A','AE','NE','X','E','CY','CN')  --  include only Y and N
  Group by isnull(N.[NetworkCategory],'Unknown'),CASE [OnTimeStatus] WHEN 1 THEN 'Y' WHEN 2 THEN 'N' ELSE [OnTimeStatus] END

  Update #Temp1 SET SortOrder = CASE [NetworkCategory] WHEN 'METRO' THEN 1 WHEN 'INTERSTATE' THEN 3 WHEN 'INTRASTATE' THEN 4 WHEN 'REGIONAL' THEN 2 ELSE 5 END 
  Update #Temp1 SET [Descr] =S.Description From #Temp1 T join [PerformanceReporting].dbo.DimStatus S on T.OnTimeStatus=S.Statuscode
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.NetworkCategory = #Temp1.NetworkCategory)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
   
  Select * from #Temp1 order by SortOrder asc, [OnTimeStatus] desc

end

GO
