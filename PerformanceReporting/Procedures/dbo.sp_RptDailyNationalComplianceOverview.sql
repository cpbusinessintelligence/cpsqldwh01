SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptDailyNationalComplianceOverview]
(@Date Date )
 as
begin

 --'=====================================================================
    --' CP -Stored Procedure -[dbo].[sp_RptDailyNationalComplianceOverview]
    --' ---------------------------
    --' Purpose: National Scanning Compliance
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 18 Jan 2017
    --'=====================================================================

  SET FMTONLY OFF;
  
  SELECT  'Achieved' as ComplianceStatus
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,1 as Series
		
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK)
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C     (NOLOCK) on  P.[PerformanceDayID] = C.Id
  Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111) and  d.ContractorType in ('c','v','o','s','f')
 
  Union all   
                                                                          
  SELECT   'Not Achieved' as ComplianceStatus
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,1 as Series
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK)
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111) and  d.ContractorType in ('c','v','o','s','f')

  Update #Temp1 SET TotalPercentage = Convert(decimal(12,2),([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount])*100)/Convert(decimal(12,2),[PerformanceCount]*3)

  Select * from #Temp1

end


GO
