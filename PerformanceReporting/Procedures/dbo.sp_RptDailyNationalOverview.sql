SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptDailyNationalOverview]
(@Date Date )
 as
begin
    SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
     
 SELECT [OnTimeStatus]
            ,SUM([PerformanceCount]) as [PerformanceCount]
      INTO #Temp1 
	  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	  JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id                                                    
	  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)
	  Group by [OnTimeStatus]

      SELECT Statuscode
	         ,id
            ,Description
			,StatusDescription
        --    ,[SortOrder]
			,Convert(Int,0) as Total
	        ,Convert(decimal(12,2),0) as Percentage
			,1 as Series
      into #TempStatusCodes    
      FROM PerformanceReporting.[dbo].DimStatus
	   
      Update #TempStatusCodes SET Total  = T.PerformanceCount , 
      Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1),0)) 
      From  #TempStatusCodes S Join #Temp1 T on S.Id = T.[OnTimeStatus] 
	  
      Select * 	  from #TempStatusCodes  
end

GO
