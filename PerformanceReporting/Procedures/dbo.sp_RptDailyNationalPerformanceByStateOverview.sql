SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDailyNationalPerformanceByStateOverview]
(@Date Date )
 as
begin

   SET FMTONLY OFF;
    --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalPerformanceByStateOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
  
   SELECT Isnull(State,'Unknown') as State
       ,CASE [OnTimeStatus] WHEN 1 THEN 'Y' WHEN 2 THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
	    ,Convert(Varchar(50),'') as [Descr]
		,Convert(int,0) as SortOrder
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus]  in (1,2)  -- include Y and N
  Group by Isnull(State,'Unknown'),CASE [OnTimeStatus] WHEN 1 THEN 'Y' WHEN 2 THEN 'N' ELSE [OnTimeStatus] END

  Update #Temp1 SET [Descr] =S.Description From #Temp1 T join [PerformanceReporting].dbo.DimStatus S on T.OnTimeStatus=S.statuscode
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.State = #Temp1.State)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  Update #Temp1 SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1   
  order by  [OnTimeStatus] desc,SortOrder asc
end



GO
