SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptDailyNationalPerformanceOverview]
(@Date Date )
 as
begin
     SET FMTONLY OFF;
     --'=====================================================================
    --' [sp_RptDailyNationalPerformanceandComplianceOverview] 
    --' Purpose: National Combined On Time Performance
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
  SELECT CASE [OnTimeStatus] WHEN 4 THEN 1 WHEN 5 THEN 2 ELSE [OnTimeStatus] END as [OnTimeStatus]
        ,SUM([PerformanceCount]) as [PerformanceCount]  -- include CY, CN, Y, N, AY, AN  --1,2,4,5,6,7
		,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		,SUM([PODComplainceCount]) as [PODComplainceCount]
		
  INTO #Temp1 
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id                                                   
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111) 
	 and [OnTimeStatus]  in (1,2,3,4,5,6,7)
  Group by CASE [OnTimeStatus] WHEN 4 THEN 1 WHEN 5 THEN 2 ELSE [OnTimeStatus] END 

  SELECT Convert(Varchar(20),'PERFORMANCE') as [Category],Id
      ,Statuscode
      ,Description   
	,Convert(Int,0) as Total
	,Convert(decimal(12,2),0) as Percentage
	,1 as Series
  into #TempStatusCodes   
  FROM PerformanceReporting.[dbo].DimStatus Where Id in (1,2,6,7) 
   
  Update #TempStatusCodes SET  Total  = T.PerformanceCount , 
  Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1),0)) 
  From  #TempStatusCodes S Join #Temp1 T on S.Id = T.[OnTimeStatus] 
  Where S.Category = 'PERFORMANCE'
    
  Select * from #TempStatusCodes 

end

GO
