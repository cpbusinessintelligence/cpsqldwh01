SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptDailyNationalPerformanceWithConNoteOverview]
(@Date Date )
 as
begin
    SET FMTONLY OFF;
     --'=====================================================================
    --'[sp_RptDailyNationalPerformanceWithConNoteOverview]    
    --' Purpose: National On Time Performance Based on Compleated Scans
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
 SELECT [OnTimeStatus],	 
		 case [OnTimeStatus]  when 4 then 'On Time' when 5 then 'Not On Time' end as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI   		
		,1 as Series
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date =  CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus] in (4,5)
  Group by  case [OnTimeStatus]  when 4 then 'On Time' when 5 then 'Not On Time' end,[OnTimeStatus]
  
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 )

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
    
  Select * from #Temp1  order by PerformanceKPI desc
end


GO
