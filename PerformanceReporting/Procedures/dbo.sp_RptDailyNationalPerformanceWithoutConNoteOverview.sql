SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptDailyNationalPerformanceWithoutConNoteOverview]
(@Date Date )
 as
begin

   --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalPerformanceWithoutConNoteOverview]
    --' ---------------------------   
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 21 Dec 2016
    --' Copyright: 2016 Couriers Please Pty Ltd    
    --'=====================================================================

    SET FMTONLY OFF;
	 
  SELECT [OnTimeStatus]  
	    ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
		,1 as Series
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus] in (1,2) ---include Y and N
  Group by  [OnTimeStatus]   

  update #Temp1 set Descr= s.Description  from dbo.DimStatus s  join #Temp1 t on s.id=t.OnTimeStatus
 
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 )

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
   
  Select * from #Temp1 order by PerformanceKPI desc

end


GO
