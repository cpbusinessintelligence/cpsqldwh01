SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptDailyNationalWorst20NoTimeDrivers]
(@Date Date )
 as
begin

     SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNoTimeOverview]   
    --' Purpose: National Worst 20 No Time Drivers
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
  SELECT   --Isnull(D.DriverID,'Unknown') as Driver
          d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4) as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  
  GROUP By  d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4) 
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc
  
end

GO
