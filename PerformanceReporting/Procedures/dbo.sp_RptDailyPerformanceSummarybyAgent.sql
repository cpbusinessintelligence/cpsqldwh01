SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valppil
-- Create date: 2020-10-07
-- Description:	
-- =============================================
-- [sp_RptDailyPerformanceSummarybyAgent] '2020-10-05','2020-10-06','Sydney', ''
CREATE PROCEDURE [dbo].[sp_RptDailyPerformanceSummarybyAgent] 
	-- Add the parameters for the stored procedure here
	@FromDate Date = NULL,
	@ToDate Date = NULL,
	@Branch varchar(50) ='All',
	@Agent varchar(100) ='All'
AS
BEGIN

	SELECT 
		P.AgentName
		,C.Date As [Date]
		,D.QueryBranch As Branch 
		--,case left(ltrim(rtrim(PickupDriver)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as Branch
		--,D.DepotCode
		--,D.DepotName
		--,substring(ltrim(rtrim(PickupDriver)),4,4) as Runno
		--,right(ltrim(rtrim(PickupDriver)),4) as ProntoID
		,[PerformanceCount]
		,Case When [OnTimeStatus] IN ('6') Then PerformanceCount Else 0 End As [OnTimeCount]
		,Case when [OnTimeStatus] IN ('7') Then PerformanceCount Else 0 End As [NotOnTimeCount]
		,Convert(decimal(12,2),0) as CompliancePercentage
	INTO #temp
	FROM [PerformanceReporting].[dbo].[DailyAgentReporting] P (NOLOCK) 
		JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[AgentDayID] = C.Id
		LEFT JOIN [PerformanceReporting].[dbo].[DimAgent] D  (NOLOCK) on P.[AgentName]=D.AgentName
	WHERE 
		Convert(date,C.[Date]) >= @FromDate 
		And Convert(date,C.[Date]) <= @ToDate
		--And D.QueryBranch=@Branch
		--and OnTimeStatus not in ('A','AE','NE','X','E','CY','CN')

	SELECT 
		AgentName
		,Date
		,Branch
		--,DepotName
		--,Runno
		--,ProntoID
		,sum([PerformanceCount]) as PerformanceCount
		,sum(OnTimeCount) as OnTimeCount
		,sum(NotOnTimeCount) as NotOnTimeCount
		,CompliancePercentage
	INTO #temp2 
	FROM #temp
	GROUP BY AgentName
			,Date
			,Branch
			--,DepotName
			--,Runno
			--,ProntoID
			,CompliancePercentage
						  
	--update #temp2 set CompliancePercentage=Convert(decimal(12,2),100*([OnBoardComplianceCount]+[DeliveryComplianceCount]+[PODComplianceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 
	UPDATE #temp2 Set CompliancePercentage = Convert(decimal(12,2),100*([OnTimeCount]))/Convert(decimal(12,2),[PerformanceCount]) 

	If @Branch <> 'ALL' and @Agent <>'ALL'
	Begin
		SELECT * FROM #temp2 Where Branch = @Branch and AgentName = @Agent
	End
	Else If @Branch <> 'ALL' and @Agent ='ALL'
	Begin
		SELECT * FROM #temp2 Where Branch = @Branch
	End
	Else If @Branch = 'ALL' and @Agent <> 'ALL'
	Begin
		SELECT * FROM #temp2 Where AgentName = @Agent
	End
	Else
	Begin
		SELECT * FROM #temp2
	End
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDailyPerformanceSummarybyAgent]
	TO [ReportUser]
GO
