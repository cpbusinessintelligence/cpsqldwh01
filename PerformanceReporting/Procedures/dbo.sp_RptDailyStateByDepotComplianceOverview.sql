SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_RptDailyStateByDepotComplianceOverview] 
(@State Varchar(20),@Date Date )
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyBranchComplianceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	  set FMTONLY off;
SELECT  'Achieved' as ComplianceStatus
	      ,Isnull(D.State,'Unknown' ) as State
		  ,Isnull(D.DepotName,'Unknown' ) as DepotName
	      ,C.Date
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN PerformanceReporting.[dbo].[DimCalendarDate] C     (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                          Left Join PerformanceReporting.[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = @Date and [OnTimeStatus] in (1,2) and D.State = @State
  Group by Isnull(D.State,'Unknown' ) ,Isnull(D.DepotName,'Unknown' ) 
       	  ,C.Date
  Union all
  SELECT   'Not Achieved' as ComplianceStatus
          ,Isnull(D.State,'Unknown' ) as State
	      ,Isnull(D.DepotName,'Unknown' ) as DepotName
		  ,C.Date
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join PerformanceReporting.[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere C.Date = @Date and [OnTimeStatus] in (1,2) and D.State = @State
  Group by  Isnull(D.State,'Unknown' ) ,Isnull(D.DepotName,'Unknown' ) ,C.Date

  Update #Temp1 SET OnBoardPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]))/Convert(decimal(12,2),[PerformanceCount])
                Where [PerformanceCount]>0
	
  Update #Temp1 SET DeliveryPercentage = Convert(decimal(12,2),100*([DeliveryComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0
  
  Update #Temp1 SET PODPercentage = Convert(decimal(12,2),100*([PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0


  
  
  Update #Temp1 SET TotalPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 
                Where [PerformanceCount]>0
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join PerformanceReporting.[dbo].[TargetKPI] K on T.[State] = K.[State] 
                Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] 
				and K.type ='SCAN-COMP'

  Select * from #Temp1 

end
GO
