SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

--[sp_RptDailyStatePerformanceWithConNoteOverview] 'QLD','08-08-2017'

-- Author:		<Satya Gandu>



-- Create date: <19 Jan 2017>



-- Description:	<Daily State On Time Performance Based on Compleated Scans>



-- =============================================



CREATE PROCEDURE [dbo].[sp_RptDailyStatePerformanceWithConNoteOverview] (@State Varchar(20),@Date Date )



AS



BEGIN



	-- SET NOCOUNT ON added to prevent extra result sets from



	-- interfering with SELECT statements.



	SET NOCOUNT ON;



	SET FMTONLY OFF;







	SELECT Isnull(State,'Unknown') as State

	,[OnTimeStatus]

	    , case [OnTimeStatus]  when 1 then 'On Time' when 2 then 'Not On Time' end as [Descr] 	    



        ,SUM([PerformanceCount]) as [PerformanceCount]



		,Convert(int,0) as Total



		,Convert(decimal(12,2),0)  as PerformanceKPI   		



		,1 as Series



  INTO #Temp1 



  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id



                                                          LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID



                                                      



  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus] in (4,5) and D.State =@State 



  Group by Isnull(State,'Unknown'),  case [OnTimeStatus]  when 1 then 'On Time' when 1 then 'Not On Time' end ,[OnTimeStatus]



   



  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 )







  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0



    



  Select * from #Temp1  order by PerformanceKPI desc



END
GO
