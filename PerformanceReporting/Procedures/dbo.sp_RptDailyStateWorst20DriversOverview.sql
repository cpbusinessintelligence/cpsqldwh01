SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<Daily State Worst 20 Drivers>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptDailyStateWorst20DriversOverview] (@State Varchar(20),@Date Date )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

    SELECT Isnull(state,'Unknown') as state, 
	     d.[state] + '_' +RIGHT(CONCAT('0000', (d.DriverNumber)), 4) as DeliveryDriver
       ,CASE [OnTimeStatus] WHEN 1 THEN 'Y' WHEN 2 THEN 'N' ELSE [OnTimeStatus] END as [OnTimeStatus]
	    ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and [OnTimeStatus] in (1,2) and D.State =@State  
  and ContractorType in ('C','V','F','S')
  Group by Isnull(state,'Unknown'),  d.[state] + '_' +RIGHT(CONCAT('0000', (d.DriverNumber)), 4)
          ,CASE [OnTimeStatus] WHEN 1 THEN 'Y' WHEN 2 THEN 'N' ELSE [OnTimeStatus] END
  Update #Temp1 SET [Descr] =S.Description From #Temp1 T join [PerformanceReporting].dbo.DimStatus S on T.OnTimeStatus=S.Statuscode
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DeliveryDriver = #Temp1.DeliveryDriver)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'

  Select top 20 * INto #TempFinal from #Temp1 WHere OnTimeStatus = 'Y' and PerformanceCOunt>20  Order by PerformanceKPI asc
 
  Select DeliveryDriver,[OnTimeStatus],[Descr],[PerformanceCount],Total,PerformanceKPI,TargetKPI
  from
  (
    Select * from #TempFinal 
    Union all
    Select * from #Temp1 Where OnTimeStatus = 'N' and DeliveryDriver in (Select distinct DeliveryDriver From #TempFinal) 
  ) results
  order by [OnTimeStatus] desc, PerformanceKPI asc

END
GO
