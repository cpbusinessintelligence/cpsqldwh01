SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<Daily State Worst 20 No TIme Drivers>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptDailyStateWorst20NoTimeDrivers] (@State Varchar(20),@Date Date )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

    SELECT d.[state] + '_' +RIGHT(CONCAT('0000', (d.DriverNumber)), 4) as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere C.Date = CONVERT(VARCHAR(10),@Date,111)  and D.State =  @State
  GROUP By  d.[state] + '_' +RIGHT(CONCAT('0000', (d.DriverNumber)), 4)
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc
END
GO
