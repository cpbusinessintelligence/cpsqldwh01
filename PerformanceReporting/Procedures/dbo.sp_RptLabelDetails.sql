SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure sp_RptLabelDetails
(
@AccountCode Varchar (100),
@StartDate Date,
@EndDate Date
)
AS
BEGIN
     --'=====================================================================
    --' CP -Stored Procedure  sp_RptLabelDetails
	--sp_RptLabelDetails '112877139','2017-07-01','2017-07-30'
    --' ---------------------------
    --' Purpose: Data Extract for Labels-----
    --' Developer: Sinshith (Couriers Please Pty Ltd)
    --' Date: 28 July 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
                          

    --'=====================================================================
SELECT 
DISTINCT 
	LabelNumber	,
	ServiceCode	,
	FirstScannedBy	,
	FirstDateTime	,
	FirstScanType	,
	ConsolidatedBarcode	,
	DeConsolidateDateTime	,
	OutForDeliverDateTime	,
	QuerycageScannedBy	,
	QuerycageDateTime	,
	DriverName,
	AttemptedDeliveryDateTime	,
	CardCategory	,
	AcceptedbyPOPPointStatus	,
	DeliveryScannedBy	,
	DeliveryDateTime	,
	PODDateTime	,
	DeliveryContractorType	,
	DeliveryContractorName	,
	LastDateTime	,
	LastScanType	,
	PickupETAZone	,
	DeliveryETAZone	,
	ETADate	,
	OnTimeStatusId	,
	LabelCreatedDateTime	,
	ProductSubCategory	,
	AttemptedDeliveryReason	
FROM [dbo].[PrimaryLabels] A
LEFT JOIN DimContractor B
ON(A.QueryCageNumber = B.DriverNumber)
WHERE AccountCode = @AccountCode
AND LabelCreatedDateTime BETWEEN @StartDate AND  @EndDate

End
GO
GRANT CONTROL
	ON [dbo].[sp_RptLabelDetails]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptLabelDetails]
	TO [ReportUser]
GO
GRANT VIEW DEFINITION
	ON [dbo].[sp_RptLabelDetails]
	TO [ReportUser]
GO
