SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_RptMonthlyDepotByPickUpPerformanceByState]
(@State varchar(20),@Year int, @month int)
 as
begin

      SET FMTONLY OFF;

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyDepotPerformanceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
	 select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

     SELECT Isnull(d.State,'Unknown') as State,
        Isnull(d.Branch,'Unknown') as Branch,
		  (case m.State when @state then  Isnull(m.DepotName,'Unknown')
              else  Isnull(m.mappedTo,'Unknown')  end) as DepotName	       	
       ,[OnTimeStatus]
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI  
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.PickupDriver = D.DriverID
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] pic (NOLOCK) on P.PickupDriver = pic.DriverID
  left join  [PerformanceReporting].[dbo].[Network_Depot_Mapping]  m on d.DepotName= m.Depotname and m.state=pic.state
                                                      
  WHere PerformanceDayID in (select * from #temp)
  and [OnTimeStatus] in (1,2) --and d.state=@State and pic.state=@State
  Group by Isnull(d.State,'Unknown')
          ,Isnull(d.Branch,'Unknown')
		  ,(case m.State when @state then  Isnull(m.DepotName,'Unknown')
              else  Isnull(m.mappedTo,'Unknown')  end)
		  ,[OnTimeStatus]
		  
  update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.[OnTimeStatus]
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DepotName = #Temp1.DepotName)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T 
  join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where CAST(@Year AS varchar(4))+RIGHT('0' + CAST(@month AS varchar(2)), 2) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 t Where t.DepotName not in ('Unknown') 
  order by PerformanceKPI desc

end


GO
