SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[sp_RptMonthlyDepotPerformanceOverviewByDepot]
(@Depo varchar(20),@Year int, @month int)
 as
begin
     
	 SET NOCOUNT ON;
     SET FMTONLY OFF;
	 	 
    select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month
     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyDepotPerformanceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
	Declare @state varchar(20)
	set @state= (select distinct dc.state from [PerformanceReporting].[dbo].[DimContractor] dc  where dc.DepotName=@Depo)

    SELECT Isnull(D.State,'Unknown') as State,Isnull(Branch,'Unknown') as Branch,Isnull(m.mappedTo,'Unknown') as DepotName
       ,[OnTimeStatus]
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1         ---- hange deops from harly table
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID  
  left join [PerformanceReporting].[dbo].[Network_Depot_Mapping]  m on d.DepotName= m.Depotname and m.state= d.state
  WHere PerformanceDayID in (select * from #temp)  
  and [OnTimeStatus] in(1,2) --and d.state= @state
  Group by Isnull(D.State,'Unknown')
          ,Isnull(Branch,'Unknown')
		  ,Isnull(m.mappedTo,'Unknown')
		  ,[OnTimeStatus]  
  
  update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.[OnTimeStatus]
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DepotName = #Temp1.DepotName)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T 
  join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where CAST(@Year AS varchar(4))+RIGHT('0' + CAST(@month AS varchar(2)), 2) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 Where DepotName not in ('Unknown')
  order by PerformanceKPI desc

end


GO
