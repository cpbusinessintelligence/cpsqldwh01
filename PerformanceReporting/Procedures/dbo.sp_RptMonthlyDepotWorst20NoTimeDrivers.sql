SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<Daily State Worst 20 No TIme Drivers>
-- =============================================
create PROCEDURE [dbo].[sp_RptMonthlyDepotWorst20NoTimeDrivers] (@Depo Varchar(20),@Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

    SELECT d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4) as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere NoTimeReportingDayID  in (select * from #temp)
  and D.Depotname =  @Depo
  GROUP By  d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4)
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc
END

GO
