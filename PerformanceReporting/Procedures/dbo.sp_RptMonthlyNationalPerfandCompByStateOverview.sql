SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptMonthlyNationalPerfandCompByStateOverview]
(@Year int, @month int)
 as
begin
   
    set FMTONLY off;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalPerfandCompByStateOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 29 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/09/2014    JP      1.00    Created the procedure                            

    --'=====================================================================
	select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

   SELECT Isnull(State,'Unknown') as State       
       ,[OnTimeStatus]
	    ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK)
   JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id   
   LEFT JOIN  PerformanceReporting.[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere PerformanceDayID in (select * from #temp)
  and [OnTimeStatus] in (1,2)
  Group by Isnull(State,'Unknown'),[OnTimeStatus]

  Update #Temp1 SET [Descr] =S.StatusDescription From #Temp1 T join PerformanceReporting.[dbo].DimStatus S on T.OnTimeStatus=S.id
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.State = #Temp1.State)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join PerformanceReporting.[dbo].[TargetKPI] K on T.[State] = K.[State] 
       Where CAST(@Year AS varchar(4))+RIGHT('0' + CAST(@month AS varchar(2)), 2) = K.[MonthKey] and K.type ='ON-TIME'
 

   SELECT  'Achieved' as ComplianceStatus
          ,Isnull(D.State,'Unknown') as State
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		
  INTO #Temp2 
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  Left Join PerformanceReporting.[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID     
  													                                             
  WHere PerformanceDayID in (select * from #temp)
  and [OnTimeStatus] in (1,2)
  Group by Isnull(D.State,'Unknown') 	
   Union all
  SELECT   'Not Achieved' as ComplianceStatus
		  ,Isnull(D.State,'Unknown')  as State
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
	  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK)
	  JOIN PerformanceReporting.[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
      Left Join PerformanceReporting.[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere PerformanceDayID in (select * from #temp)
  and [OnTimeStatus] in (1,2)
  Group by Isnull(D.State,'Unknown') 	

  Update #Temp2 SET TotalPercentage = Convert(decimal(12,2),([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount])*100)/Convert(decimal(12,2),[PerformanceCount]*3)


CREATE TABLE #TempFinal
(
    [State] varchar(100),
	SortOrder int,
    TotalCount int,
	OnTimeCount int,
	NotOnTimeCount int,
	OnTimePercentage decimal(12,2),
	TargetOnTimePercentage decimal(12,2),
	TotalCompliancePercentage decimal(12,2),
)

Insert into  #TempFinal Select 'NSW',0,0,0,0,0,0,0
Insert into  #TempFinal Select 'QLD',0,0,0,0,0,0,0
Insert into  #TempFinal Select 'VIC',0,0,0,0,0,0,0
Insert into  #TempFinal Select 'SA',0,0,0,0,0,0,0
Insert into  #TempFinal Select 'WA',0,0,0,0,0,0,0
--Insert into  #TempFinal Select 'NT',0,0,0,0,0,0

Update #TempFinal SET  OnTimeCount =T.PerformanceCount,TOtalCOunt= T.Total,OnTimePercentage= T.PerformanceKPI,TargetOnTimePercentage = T.TargetKPI From #TempFinal F Join  #Temp1 T on F.State = T.State and T.OnTimeStatus = 1
Update #TempFinal SET  notOnTimeCount =T.PerformanceCount From #TempFinal F Join  #Temp1 T on F.State = T.State and T.OnTimeStatus = 2
Update #TempFinal SET  TotalCompliancePercentage =T.TotalPercentage From #TempFinal F Join  #Temp2 T on F.State = T.State and T.ComplianceStatus = 'Achieved'
Update #TempFinal SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 

Select * from #TempFinal order by  SortOrder asc



end
GO
