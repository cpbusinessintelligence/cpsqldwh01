SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[sp_RptMonthlyNationalWorst20NoTimeDrivers]
(@Year int, @month int)
 as
begin

     SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNoTimeOverview]   
    --' Purpose: National Worst 20 No Time Drivers
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
  select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

  SELECT   --Isnull(D.DriverID,'Unknown') as Driver
          d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4) as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere NoTimeReportingDayID in (select * from #temp)  
  GROUP By  d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4) 
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc
  
end

GO
