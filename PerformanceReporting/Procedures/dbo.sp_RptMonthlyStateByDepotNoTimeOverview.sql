SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<State No TIme Overview>
-- =============================================
create PROCEDURE [dbo].[sp_RptMonthlyStateByDepotNoTimeOverview] (@State varchar(20),@Year int, @month int )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

   select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

   SELECT  Isnull(D.State,'Unknown') as State,
          Isnull(D.DepotName,'Unknown') as DepotName
          , SUM([NoTimeCount]) as NoTimeCount	
		  ,Convert(decimal(12,2),0) as TargetKPI	  
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere NoTimeReportingDayID in (select * from #temp)
  and D.State = @State
  GROUP By  Isnull(D.DepotName,'Unknown'), Isnull(D.State,'Unknown') 

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where CAST(@Year AS varchar(4))+RIGHT('0' + CAST(@month AS varchar(2)), 2) = K.[MonthKey] and K.type ='ON-TIME'

  Select * from #Temp1

END
GO
