SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<National Scanning Compliance>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptMonthlyStateComplianceOverview] (@State Varchar(20),@Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET FMTONLY OFF;

  select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month
  
  SELECT  'Achieved' as ComplianceStatus
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,1 as Series
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK)
  Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
   WHere PerformanceDayID in (select * from #temp)
   and  d.ContractorType in ('c','v','o','s','f') and D.State = @State
  
  Union all    
                                                                         
  SELECT   'Not Achieved' as ComplianceStatus
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,1 as Series
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK)
  Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere  PerformanceDayID in (select * from #temp)
  and  d.ContractorType in ('c','v','o','s','f') and D.State = @State

  Update #Temp1 SET TotalPercentage = Convert(decimal(12,2),([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount])*100)/Convert(decimal(12,2),[PerformanceCount]*3)

  Select * from #Temp1

END
GO
