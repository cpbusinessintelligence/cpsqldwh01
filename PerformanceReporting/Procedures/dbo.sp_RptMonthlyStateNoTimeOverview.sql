SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



create proc [dbo].[sp_RptMonthlyStateNoTimeOverview]
(@Year int, @month int )
 as
begin

      SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNoTimeOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
  select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

  SELECT Isnull(D.State,'Unknown') as State
		  ,Convert(int,0) as SortOrder
          , SUM([NoTimeCount]) as NoTimeCount
		  ,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere NoTimeReportingDayID in (select * from #temp)  
  GROUP By  Isnull(D.State,'Unknown')

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where  (@Year+@month)= K.[MonthKey] and K.type ='ON-TIME'

  Update #Temp1 SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 

  Select * from #Temp1 order by SortOrder asc
end
GO
