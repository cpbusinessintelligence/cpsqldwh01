SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <18 Jan 2017>
-- Description:	<State On Time Performance Based on Compleated Scans>
-- =============================================
create PROCEDURE [dbo].[sp_RptMonthlyStatePerformanceWithoutConNoteOverview] (@State Varchar(20),@Year int, @month int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	 
  select id  into #temp from [dbo].[DimCalendarDate] where CalendarYear=@year and CalendarMonthNumber= @month

  SELECT Isnull(State,'Unknown') as State 
        , [OnTimeStatus]  
	    ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
		,1 as Series
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere PerformanceDayID in (select * from #temp)
  and [OnTimeStatus] in (1,2)  and D.State =@State ---include Y and N
  Group by   Isnull(State,'Unknown') ,[OnTimeStatus] 
  
  update #Temp1 set Descr= s.Description  from dbo.DimStatus s  join #Temp1 t on s.id=t.OnTimeStatus

  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.State = #Temp1.State)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
   
  Select * from #Temp1 order by PerformanceKPI desc,[OnTimeStatus] asc

   
END
GO
