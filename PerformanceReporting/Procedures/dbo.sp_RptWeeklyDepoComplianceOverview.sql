SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[sp_RptWeeklyDepoComplianceOverview]
(@Date Date , @Depo varchar(20))
 as
begin

 --'=====================================================================   
    --' Purpose: National Scanning Compliance
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 18 Jan 2017
    --'=====================================================================

  SET FMTONLY OFF;
    
  select id into #temp  from [dbo].[DimCalendarDate]
  where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
  
  SELECT  'Achieved' as ComplianceStatus
          ,DepotName
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,1 as Series
		
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK)
  Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere PerformanceDayID in (select * from #temp)  
  and  d.ContractorType in ('c','v','o','s') and DepotName=@Depo
  group by DepotName
 
  Union all   
                                                                          
  SELECT   'Not Achieved' as ComplianceStatus
          ,DepotName
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,1 as Series
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK)
  Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere PerformanceDayID in (select * from #temp)   
  and  d.ContractorType in ('c','v','o','s') and DepotName=@Depo
  group by DepotName

  Update #Temp1 SET TotalPercentage = Convert(decimal(12,2),([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount])*100)/Convert(decimal(12,2),[PerformanceCount]*3)

  Select * from #Temp1

end


GO
