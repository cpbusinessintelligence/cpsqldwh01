SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptWeeklyDepotByPickUpPerformance]
(@Date Date )
 as
begin

      SET FMTONLY OFF;

     --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyDepotPerformanceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
   select id into #temp  from [dbo].[DimCalendarDate]
   where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

   SELECT Isnull(d.State,'Unknown') as State,
        Isnull(d.Branch,'Unknown') as Branch,
        Isnull(m.mappedTo,'Unknown') as DepotName		
       ,[OnTimeStatus]
        ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI 
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.PickupDriver = D.DriverID
  left join  [PerformanceReporting].[dbo].[Network_Depot_Mapping]  m on d.DepotName= m.Depotname and m.State=d.State
                                                      
  WHere PerformanceDayID in (select * from #temp) 
  and [OnTimeStatus] in (1,2)
  Group by Isnull(d.State,'Unknown')
          ,Isnull(d.Branch,'Unknown')
		  , Isnull(m.mappedTo,'Unknown') 
		  ,[OnTimeStatus]
		  
  update #temp1 set Descr=s.Description  from dbo.DimStatus s join #temp1 t on s.id= t.[OnTimeStatus]
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DepotName = #Temp1.DepotName)
  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T 
  join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 t Where t.DepotName not in ('Unknown') 
  order by PerformanceKPI desc

end

GO
