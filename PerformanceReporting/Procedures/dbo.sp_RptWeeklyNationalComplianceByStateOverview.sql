SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_RptWeeklyNationalComplianceByStateOverview] 
(@Date Date )
 as
begin

    --'=====================================================================
    --' CP -Stored Procedure -[[sp_RptDailyBranchComplianceOverview]]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
	
	SET FMTONLY OFF;

	select id into #temp  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
	
    SELECT  'Achieved' as ComplianceStatus
		  ,Isnull(D.State,'Unknown') as State
		  ,Convert(int,0) as SortOrder
          ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  ,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere PerformanceDayID in (select * from #temp) 
  and d.ContractorType in ('c','v', 'o', 's','f')  
  Group by Isnull(D.State,'Unknown')              
                                    
  Union all

  SELECT   'Not Achieved' as ComplianceStatus       
		   ,Isnull(D.State,'Unknown') as State
		    ,Convert(int,0) as SortOrder
		  ,SUM([PerformanceCount])  as [PerformanceCount]
		  ,SUM([PerformanceCount])-SUM([OnBoardComplainceCount]) as [OnBoardComplainceCount]
		  ,SUM([PerformanceCount])-SUM([DeliveryComplainceCount]) as [DeliveryComplainceCount]
		  ,SUM([PerformanceCount])-SUM([PODComplainceCount]) as [PODComplainceCount]
		  ,Convert(decimal(12,2),0) as OnBoardPercentage
		  ,Convert(decimal(12,2),0) as DeliveryPercentage
		  ,Convert(decimal(12,2),0) as PODPercentage
		  ,Convert(decimal(12,2),0) as TotalPercentage
		  		  ,Convert(decimal(12,2),0) as TargetKPI
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) Left Join [PerformanceReporting].[dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID                                                
  WHere PerformanceDayID in (select * from #temp) and d.ContractorType in ('c','v', 'o', 's','f')
  Group by  Isnull(D.State,'Unknown')

  Update #Temp1 SET OnBoardPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]))/Convert(decimal(12,2),[PerformanceCount])
                Where [PerformanceCount]>0
	
  Update #Temp1 SET DeliveryPercentage = Convert(decimal(12,2),100*([DeliveryComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0
  
  Update #Temp1 SET PODPercentage = Convert(decimal(12,2),100*([PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]) 
                Where [PerformanceCount]>0

  Update #Temp1 SET TotalPercentage = Convert(decimal(12,2),100*([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 
                Where [PerformanceCount]>0

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'

   Update #Temp1 SET SortOrder = CASE State WHEN 'QLD' THEN 1 WHEN 'NSW' THEN 2 WHEN 'VIC' THEN 3 WHEN 'SA' THEN 4  WHEN 'WA' THEN 5 ELSE 6 END 
				
  Select * from #Temp1  order by SortOrder asc

end


GO
