SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[sp_RptWeeklyNationalOverview]
(@Date Date )
 as
begin
    SET FMTONLY OFF;
     --'=====================================================================
    --' CP -Stored Procedure -[sp_RptDailyNationalOverview]
    --' ---------------------------
    --' Purpose: Full load of small tables-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
     select id into #temp  from [dbo].[DimCalendarDate]
     where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
	 
      SELECT [OnTimeStatus]
      ,SUM([PerformanceCount]) as [PerformanceCount]
      INTO #Temp1 
	  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK)                                                 
	  WHere PerformanceDayID in (select * from #temp) 
	  Group by [OnTimeStatus]

      SELECT Statuscode
	         ,id
            ,Description
			,StatusDescription
        --    ,[SortOrder]
			,Convert(Int,0) as Total
	        ,Convert(decimal(12,2),0) as Percentage
			,1 as Series
      into #TempStatusCodes    
      FROM PerformanceReporting.[dbo].DimStatus
	   
      Update #TempStatusCodes SET Total  = T.PerformanceCount , 
      Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1),0)) 
      From  #TempStatusCodes S Join #Temp1 T on S.Id = T.[OnTimeStatus] 
	  
      Select * 	  from #TempStatusCodes  
end

GO
