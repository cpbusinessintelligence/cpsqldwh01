SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[sp_RptWeeklyNationalWorst20DriversOverview]
(@Date Date )
 as
begin
      SET FMTONLY OFF;
     --'=====================================================================
    --' [sp_RptDailyNoTimeOverview]  
    --' Purpose: National Worst 20 Drivers
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --'=====================================================================
	select id into #temp  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
  
    SELECT Isnull(State,'Unknown') as [State],
	     d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4)  as DeliveryDriver
	 --   ,Isnull(DeliveryDriver,'Unknown') as DeliveryDriver
        ,[OnTimeStatus]
	    ,Convert(Varchar(50),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyPerformanceReporting] P (NOLOCK) 
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID
                                                      
  WHere PerformanceDayID in (select * from #temp) 
  and [OnTimeStatus] in (1,2)   
  and ContractorType in ('C','V','F','S')
  Group by  d.[state] + '_' +RIGHT(CONCAT('0000', (DriverNumber)), 4)
          ,[OnTimeStatus], Isnull(State,'Unknown')

  Update #Temp1 SET [Descr] =S.Description From #Temp1 T join [PerformanceReporting].dbo.DimStatus S on T.OnTimeStatus=S.Id
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.DeliveryDriver = #Temp1.DeliveryDriver)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0  

  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join [PerformanceReporting].[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'

  Select top 20 * INto #TempFinal from #Temp1 WHere OnTimeStatus = 1 and PerformanceCOunt>20  Order by PerformanceKPI asc

  Select DeliveryDriver,[OnTimeStatus],[Descr],[PerformanceCount],Total,PerformanceKPI,TargetKPI
  from
  (
    Select * from #TempFinal 
    Union all
    Select * from #Temp1 Where OnTimeStatus = 2 and DeliveryDriver in (Select distinct DeliveryDriver From #TempFinal) 
  ) results
  order by PerformanceKPI 

end

GO
