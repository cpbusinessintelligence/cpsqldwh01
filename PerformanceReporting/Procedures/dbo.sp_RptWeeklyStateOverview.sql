SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_RptWeeklyStateOverview]
(@State varchar(20),@Date Date )
 as
begin

     --'=====================================================================    
    --' Developer: Satya Gandu
    --' Date: 03/02/2017                       

    --'=====================================================================
     SET FMTONLY OFF;
	 	 
    select id into #temp  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

	 SELECT [OnTimeStatus]
            ,SUM([PerformanceCount]) as [PerformanceCount]
      INTO #Temp1 
	  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK) 
	  JOIN  PerformanceReporting.[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                         
	  WHere PerformanceDayID in (select * from #temp) 
	  and D.State= @State
	  Group by [OnTimeStatus]

      SELECT id
            ,[Description]
			,Convert(Int,0) as Total
	        ,Convert(decimal(12,2),0) as Percentage
      into #TempStatusCodes    
    FROM PerformanceReporting.[dbo].DimStatus
 
      Update #TempStatusCodes SET Total  = T.PerformanceCount , 
                                  Percentage = Convert(decimal(12,2),T.PerformanceCount)*100 / Convert(decimal(12,2),Isnull((Select SUM([PerformanceCount]) from #Temp1 WHere [OnTimeStatus] in (1,2,4,5)),0)) 
      From  #TempStatusCodes S Join #Temp1 T on S.Id = T.[OnTimeStatus] 

	  Update #TempStatusCodes SET Percentage = 0 where id not in (1,2,4,5)

      Select * from #TempStatusCodes 

end

GO
