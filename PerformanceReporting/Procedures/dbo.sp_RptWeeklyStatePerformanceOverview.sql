SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<State Combined On Time Performance>
-- =============================================
create PROCEDURE [dbo].[sp_RptWeeklyStatePerformanceOverview] (@State Varchar(20),@Date Date )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id into #temp  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)

    SELECT Isnull(State,'Unknown') as State
       ,CASE [OnTimeStatus] WHEN 4 THEN 1 WHEN 5 THEN 2 ELSE [OnTimeStatus] END as [OnTimeStatus]
	    ,Convert(Varchar(20),'') as [Descr]
        ,SUM([PerformanceCount]) as [PerformanceCount]
		,Convert(int,0) as Total
		,Convert(decimal(12,2),0)  as PerformanceKPI
		,Convert(decimal(12,2),0) as TargetKPI
  INTO #Temp1 
  FROM PerformanceReporting.[dbo].[DailyPerformanceReporting] P (NOLOCK)
  LEFT JOIN  PerformanceReporting.[dbo].[DimContractor] D (NOLOCK) on P.[DeliveryDriver] = D.DriverID                                                      
  WHere PerformanceDayID in (select * from #temp)    
  and [OnTimeStatus]  in (1,2,3,4,5,6,7) and D.State =@State
  Group by Isnull(State,'Unknown'),CASE [OnTimeStatus] WHEN 4 THEN 1 WHEN 5 THEN 2 ELSE [OnTimeStatus] END

  Update #Temp1 SET [Descr] =S.Description From #Temp1 T join [PerformanceReporting].dbo.Dimstatus S on T.OnTimeStatus=S.id
  Update #Temp1 Set Total = (Select SUm([PerformanceCount]) from #Temp1 T2 Where T2.State = #Temp1.State)

  Update #Temp1 Set PerformanceKPI  =Convert(decimal(12,2),100*[PerformanceCount])/Convert(Decimal(12,2),Total) WHere Total >0
  
  Update #Temp1 SET TargetKPI =K.Target *100 From #Temp1 T join PerformanceReporting.[dbo].[TargetKPI] K on T.[State] = K.[State] 
  Where (Datepart(year,@Date)*100)+Datepart(MONTH,@Date) = K.[MonthKey] and K.type ='ON-TIME'
 
  Select * from #Temp1 order by PerformanceKPI desc

END
GO
