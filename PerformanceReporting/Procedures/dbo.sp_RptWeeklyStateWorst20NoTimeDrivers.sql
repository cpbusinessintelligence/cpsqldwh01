SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya Gandu>
-- Create date: <19 Jan 2017>
-- Description:	<Daily State Worst 20 No TIme Drivers>
-- =============================================
create PROCEDURE [dbo].[sp_RptWeeklyStateWorst20NoTimeDrivers] (@State Varchar(20),@Date Date )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;

	select id into #temp  from [dbo].[DimCalendarDate]
    where WeekEndingdate = (Select WeekEndingdate from [PerformanceReporting].[dbo].[DimCalendarDate] C (NOLOCK) Where C.Date = @Date)
	
    SELECT d.[state] + '_' +RIGHT(CONCAT('0000', (d.DriverNumber)), 4) as Driver
          , SUM([NoTimeCount]) as NoTimeCount
  INTO #Temp1 
  FROM [PerformanceReporting].[dbo].[DailyNoTimeReporting] P (NOLOCK)
  LEFT JOIN  [PerformanceReporting].[dbo].[DimContractor] D (NOLOCK) on P.[OnBoardDriver] = D.DriverID
  WHere NoTimeReportingDayID in (select * from #temp)  
  and D.State =  @State
  GROUP By  d.[state] + '_' +RIGHT(CONCAT('0000', (d.DriverNumber)), 4)
  
  Select Top 20 * from #Temp1 Order by NoTimeCount desc
END
GO
