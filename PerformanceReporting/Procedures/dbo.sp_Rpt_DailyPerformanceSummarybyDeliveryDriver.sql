SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_Rpt_DailyPerformanceSummarybyDeliveryDriver](@Depot varchar(100),@StartDate date,@EndDate date) as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_OnTimePerformanceSummary]
    --' ---------------------------
    --' Purpose: OnTime Performance summary-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 29 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/12/2014    AK      1.00    Created the procedure                            

    --'=====================================================================

SELECT DeliveryDriver
      ,C.Date as Date
	  ,case left(ltrim(rtrim(DeliveryDriver)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as Branch
	  ,D.DepotCode
	  ,D.DepotName
	  ,substring(ltrim(rtrim(DeliveryDriver)),4,4) as Runno
	  ,right(ltrim(rtrim(DeliveryDriver)),4) as ProntoID
	  ,convert(int,0) as TotalPickups
	  ,convert(int,0) as NonMeasuredDeliveries
	  ,convert(int,0) as MeasuredDeliveries
	  ,[PerformanceCount]
      ,case when [OnTimeStatus] in (1) then PerformanceCount else 0 end as OnTimeCount
	  ,case when [OnTimeStatus] in (2) then PerformanceCount else 0 end as NotOnTimeCount
	  ,[OnBoardComplainceCount]
      ,[DeliveryComplainceCount]
	  ,PODComplainceCount
	  ,convert(decimal(12,2),0) as OnBoardCompliancePercentage
	  ,convert(decimal(12,2),0) as DeliveredCompliancePercentage
	  ,convert(decimal(12,2),0) as PODCompliancePercentage
	  ,convert(decimal(12,2),0) as CompliancePercentage
	  ,convert(int,0) as NoTimeCount
	  ,convert(int,0) as MeasuredPickups
	  ,convert(int,0) as PickupOnTime
	  ,convert(int,0) as PickupNotOnTime
into #temp
FROM [dbo].[DailyPerformanceReporting] P (NOLOCK) JOIN [dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id
                                                       Left Join [dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID  
													   where c.date between @StartDate and @EndDate and 	D.DepotName=@Depot	and OnTimeStatus  in (1,2)	
													   
													   
													   
													   
select c.date as Date,
       [DeliveryDriver],
	   D.DepotName,
	   sum([PerformanceCount]) as NonMeasuredDeliveries
into #tempD 
from [dbo].[DailyPerformanceReporting] p JOIN [dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id  Left Join [dbo].[DimContractor] D  (NOLOCK) on P.[DeliveryDriver]=D.DriverID  
where c.date between @StartDate and @EndDate and 	D.DepotName=@Depot		and   OnTimeStatus not in (1,2,6,7)	
group by c.date,[DeliveryDriver], D.DepotName
												   		   


select --c.date as Date,
       PickupDriver,
	   D.DepotName,
      sum(case when [OnTimeStatus] in (1) then PerformanceCount else 0 end) as OnTimeCount,
	  sum(case when [OnTimeStatus] in (2) then PerformanceCount else 0 end) as NotOnTimeCount,
	  sum(([PerformanceCount])) as MeasuredPickups 
into #tempP 
from [dbo].[DailyPerformanceReporting] p JOIN [dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id  Left Join [dbo].[DimContractor] D  (NOLOCK) on P.PickupDriver=D.DriverID  
where c.date between @StartDate and @EndDate and 	D.DepotName=@Depot		and   OnTimeStatus  in (1,2)	
group by --c.date ,
       PickupDriver,
	   D.DepotName

Select --Date,
       PickupDriver,
	   DepotName,
	  sum(MeasuredPickups) as TotalMeasuredPickups 
into #tempPSummary
from #tempP
group by --date,
       PickupDriver,
	   DepotName

select c.date as Date,
       PickupDriver,
	   DepotName,
	   sum([PerformanceCount]) as TotalPickups 
into #tempP1 
from [dbo].[DailyPerformanceReporting] p JOIN [dbo].[DimCalendarDate] C (NOLOCK) on  P.[PerformanceDayID] = C.Id  Left Join [dbo].[DimContractor] D  (NOLOCK) on P.PickupDriver=D.DriverID  
where c.date between @StartDate and @EndDate and 	D.DepotName=@Depot		
group by c.date,PickupDriver, D.DepotName






select c.date as Date,
       [OnBoardDriver],
	   D.DepotName,
	   sum(NotimeCount) as NoTimeCount 
into #temp1 
from [dbo].[DailyNoTimeReporting] p JOIN [dbo].[DimCalendarDate] C (NOLOCK) on  P.[NoTimeReportingDayID] = C.Id  Left Join [dbo].[DimContractor] D  (NOLOCK) on P.[OnBoardDriver]=D.DriverID  
where c.date between @StartDate and @EndDate and 	D.DepotName=@Depot		 
group by c.date,[OnBoardDriver], D.DepotName


													   
select DeliveryDriver as Driver
    --  ,Date
	  ,Branch
	  ,DepotName
	  ,Runno
	  ,ProntoID
	  ,sum([PerformanceCount]) as PerformanceCount
      ,sum(OnTimeCount) as OnTimeCount
	  ,sum(NotOnTimeCount) as NotOnTimeCount
	  ,sum([OnBoardComplainceCount]) as OnBoardComplianceCount
      ,sum([DeliveryComplainceCount]) as DeliveryComplianceCount
	  ,sum(PODComplainceCount) as PODComplianceCount
	  ,CompliancePercentage
	  ,NoTimeCount
	  ,TotalPickups
	  ,NonMeasuredDeliveries
	  ,OnBoardCompliancePercentage
	  ,DeliveredCompliancePercentage
	  ,PODCompliancePercentage
	  ,MeasuredPickups
	  ,PickupOnTime
	  ,PickupNotOnTime
	  into #temp2 
	  from #temp
	  group by DeliveryDriver
         --     ,Date
	          ,Branch
			  ,DepotName
	          ,Runno
	          ,ProntoID
			  ,NoTimeCount
			  ,CompliancePercentage
			  ,TotalPickups
	  ,NonMeasuredDeliveries
	  ,OnBoardCompliancePercentage
	  ,DeliveredCompliancePercentage
	  ,PODCompliancePercentage
	  ,MeasuredPickups
	  ,PickupOnTime
	  ,PickupNotOnTime
			  

update #temp2 set NoTimeCount=t1.NotimeCount from #temp2 t join #temp1 t1 on t1.[OnBoardDriver]=t.Driver

update #temp2 set TotalPickups=t1.TotalPickups from #temp2 t join #tempP1 t1 on t1.PickupDriver=t.Driver


update #temp2 set NonMeasuredDeliveries=t1.NonMeasuredDeliveries from #temp2 t join #tempD t1 on t1.DeliveryDriver=t.Driver


update #temp2 set OnBoardCompliancePercentage=Convert(decimal(12,2),100*(OnBoardComplianceCount))/Convert(decimal(12,2),[PerformanceCount]) 
update #temp2 set DeliveredCompliancePercentage=Convert(decimal(12,2),100*(DeliveryComplianceCount))/Convert(decimal(12,2),[PerformanceCount]) 
update #temp2 set PODCompliancePercentage=Convert(decimal(12,2),100*(PODComplianceCount))/Convert(decimal(12,2),[PerformanceCount]) 


update #temp2 set MeasuredPickups=t1.TotalMeasuredPickups from #temp2 t join #tempPSummary t1 on t1.PickupDriver=t.Driver

update #temp2 set PickupOnTime=t1.OnTimeCount from #temp2 t join #tempP t1 on t1.PickupDriver=t.Driver

update #temp2 set PickupNotOnTime=t1.NotOnTimeCount from #temp2 t join #tempP t1 on t1.PickupDriver=t.Driver

update #temp2 set CompliancePercentage=Convert(decimal(12,2),100*([OnBoardComplianceCount]+[DeliveryComplianceCount]+[PODComplianceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 


--select Driver
--	  ,Branch
--	  ,DepotName
--	  ,Runno
--	  ,ProntoID
--	  ,sum([PerformanceCount]) as PerformanceCount
--      ,sum(OnTimeCount) as OnTimeCount
--	  ,sum(NotOnTimeCount) as NotOnTimeCount
--	  ,sum([OnBoardComplianceCount]) as OnBoardComplianceCount
--      ,sum([DeliveryComplianceCount]) as DeliveryComplianceCount
--	  ,sum(PODComplianceCount) as PODComplianceCount
--	  ,CompliancePercentage
--	  ,sum(NoTimeCount) as NoTimeCount
--	   ,sum(TotalPickups) as TotalPickups
--	  ,sum(NonMeasuredDeliveries) as NonMeasuredDeliveries
--	  ,OnBoardCompliancePercentage
--	  ,DeliveredCompliancePercentage
--	  ,PODCompliancePercentage
--	  ,sum(MeasuredPickups) as MeasuredPickups
--	  ,sum(PickupOnTime) as PickupOnTime
--	  ,sum(PickupNotOnTime)  as PickupNotOnTime
--	  into #temp3 from #temp2
--	  group by Driver
--            --  ,Date
--	          ,Branch
--			  ,DepotName
--	          ,Runno
--	          ,ProntoID
--			  ,NoTimeCount
--			  ,CompliancePercentage 
--	          ,OnBoardCompliancePercentage
--	  ,DeliveredCompliancePercentage
--	  ,PODCompliancePercentage


--update #temp3 set CompliancePercentage=Convert(decimal(12,2),100*([OnBoardComplianceCount]+[DeliveryComplianceCount]+[PODComplianceCount]))/Convert(decimal(12,2),[PerformanceCount]*3) 

Select  * from #temp2

end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DailyPerformanceSummarybyDeliveryDriver]
	TO [ReportUser]
GO
