SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

  CREATE Procedure [dbo].[sp_Rpt_DrilldownScanningComplianceDetailByDrivernumber](@Driver varchar(200),@StartDate date,@EndDate date,@Depot varchar(100)) as
  begin
       --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_DrilldownScanningComplainceDetailByDrivernumber]
    --' ---------------------------
    --' Purpose: Scanning complaince Detail Report-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 29 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 29/12/2014    AK      1.00    Created the procedure                            

    --'=====================================================================

  select [LabelNumber]
      --,[GWConsignmentID]
      ,[RevenueType]
	  ,isnull([PickupDateTime],'') as PickupScan
      ,isnull([PickupScannedBy],'') as PickupDrivernumber
      ,case left(ltrim(rtrim(PickupScannedBy)),3)  WHEN   'ADL' THEN 'Adelaide' WHEN  'BNE' THEN 'Brisbane' WHEN  'GLC' THEN 'Gold Coast'  WHEN 'SYD' THEN 'Sydney'   WHEN  'MEL' THEN  'Melbourne'  WHEN 'PER' THEN 'Perth'   WHEN 'NKP' THEN 'Nkope'   ELSE 'Unknown' END as PickupBranch
	  ,isnull(D.DepotName,'') as PickupDepot
	  ,OutForDeliverDateTime
      ,isnull([AttemptedDeliveryDateTime],'') as AttemptScan
      ,isnull([DeliveryDateTime],'') as DeliveryScan
	  ,isnull(D1.DepotName,'') as DeliveryDepot
      ,datediff("mi",[PickupDateTime],isnull([AttemptedDeliveryDateTime],[DeliveryDateTime]))/convert(decimal(4,2),60) as DeliveryHours
      ,isnull([PickupETAZone],'') as PickupETAZone
      ,isnull([DeliveryETAZone],'') as DeliveryETAZone
	  ,(CASE WHEN [OutForDeliverDateTime] is not null THEN 1 ELSE 0 END)  as [OnBoardComplainceCount]
	  ,(CASE WHEN  [DeliveryDateTime] is not Null or AttemptedDeliveryDateTime is not null THEN 1 ELSE 0 END) as [DeliveryComplainceCount]
	  ,(CASE WHEN AttemptedDeliveryDateTime is not null OR ISNULL(IsPODPresent,'N') = 'Y' THEN 1 ELSE 0 END)	as [PODComplainceCount]
	  into #temp
from Primarylabels P Left Join [dbo].[DimContractor] D  (NOLOCK) on P.[PickupScannedBy]=D.DriverID  
  Left Join [dbo].[DimContractor] D1  (NOLOCK) on Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'')=D1.DriverID   
  where Isnull(Isnull([AttemptedDeliveryScannedBy],[DeliveryScannedBy]),'')=@Driver and D1.Depotname=@Depot and PerformanceReportingDate between @StartDate and @EndDate and OnTimeStatusId in (1,2)	
   --and (isnull([CardCategory] ,'')='' or isnull([CardCategory] ,'') like '%1st%')


  select [LabelNumber]
       ,[RevenueType]
	   ,PickupScan
       ,PickupDrivernumber
       ,PickupBranch
	   ,PickupDepot
	   ,OutForDeliverDateTime
       ,AttemptScan
       ,DeliveryScan
	   ,DeliveryDepot
       ,DeliveryHours
       ,PickupETAZone
       ,DeliveryETAZone
	   ,count(*) as PerformanceCount
	   ,sum(OnBoardComplainceCount)  as [OnBoardComplainceCount]
	   ,sum(DeliveryComplainceCount) as [DeliveryComplainceCount]
	   ,sum(PODComplainceCount) as [PODComplainceCount]
from #temp
group by [LabelNumber]
       ,[RevenueType]
	   ,PickupScan
       ,PickupDrivernumber
       ,PickupBranch
	   ,PickupDepot
	   ,OutForDeliverDateTime
       ,AttemptScan
       ,DeliveryScan
	   ,DeliveryDepot
       ,DeliveryHours
       ,PickupETAZone
       ,DeliveryETAZone

	   end

GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DrilldownScanningComplianceDetailByDrivernumber]
	TO [ReportUser]
GO
