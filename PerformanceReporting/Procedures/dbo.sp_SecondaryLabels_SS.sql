SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_SecondaryLabels_SS (@AddDatetime DateTime)
as


Begin transaction PerfRep4

Insert into [dbo].[SecondaryLabels_Archive_17-18]
select * from [dbo].[SecondaryLabels]
where [AddDateTime] < = @AddDatetime

Delete from [dbo].[SecondaryLabels]
where [AddDateTime] < = @AddDatetime


Commit transaction PerfRep4

GO
