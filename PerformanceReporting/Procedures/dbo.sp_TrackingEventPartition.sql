SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_TrackingEventPartition]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(eventDatetime) from trackingevent
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[TrackingEvent_Partition]
              ([Id],[EventTypeId],[EventDateTime]
				,[Description],[LabelId],[AgentId]
                ,[DriverId],[RunId],[EventLocation]
			    ,[ExceptionReason],[AdditionalText1],[AdditionalText2]
           ,[NumberOfItems]
           ,[SourceId]
           ,[SourceReference]
           ,[CosmosSignatureId]
           ,[CosmosBranchId]
           ,[IsDeleted]
           ,[CreatedDate]
           ,[LastModifiedDate])
     			SELECT [Id]
      ,[EventTypeId]
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]
      ,[AgentId]
      ,[DriverId]
      ,[RunId]
      ,[EventLocation]
      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
      ,[NumberOfItems]
      ,[SourceId]
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
      ,[IsDeleted]
      ,[CreatedDate]
      ,[LastModifiedDate]
  FROM [dbo].[TrackingEvent] (nolock) where eventDatetime >= @MinDate and  eventDatetime <= DATEADD(day, 1,@MinDate)


	--DELETE FROM [DailyPerformanceReporting] where  AddDateTime >= @MinDate and  AddDAtetime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END


GO
