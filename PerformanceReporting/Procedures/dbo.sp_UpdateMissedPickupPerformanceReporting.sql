SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_UpdateMissedPickupPerformanceReporting]
AS
BEGIN
	
	Declare @Date date = Getdate()-1
	---Added By PV on 2020-10-15
	Delete [dbo].[DailyMissedPickupReporting]
	From [dbo].[DailyMissedPickupReporting] P 
		Join dbo.DimCalendarDate C on  P.MissedPickupReportingDayID = C.Id 
	Where C.[Date] = @Date

	Delete From [dbo].[DailyMissedPickupReportingDetail]  Where PerformanceReportingDate = @Date
	-----Ends here -- Addition on 2020-10-15

	---Added by PV on 2020-10-15	
	EXEC [dbo].[sp_LoadDailyFutileMissedPartialPickup_Detail] @Date
	print '2 done'
	EXEC [dbo].[sp_LoadDailyFutileMissedPartialPickup_Summary] @Date
	print '1 done'
	----Ends here -- Addition on 2020-10-15 by PV--

END
GO
