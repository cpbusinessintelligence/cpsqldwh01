SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Harley Boyd-Skinner>
-- Create date: <28/02/2017>
Create PROCEDURE [dbo].[usp_Rpt_Fleet_FranchiseeReviewData]
	-- Add the parameters for the stored procedure here

@StartDate AS Date 
,@EndDate AS Date 
,@DriverNumber AS INT 
,@ProntoID AS VARCHAR (20)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

-------------------------------------------------------------------------------------
--Extract Earnings data and make columns for OTP & Scanning compliance information

SELECT [ContractorId]
      ,[RunNumber]
      ,[RunName]
      ,[Branch]
      ,[RctiWeekEndingDate]
      ,[TotalDevelopmentIncentive]
      ,[TotalPrepaidRctiAmount]
      ,[TotalEdiRctiAmount]
      ,[TotalRctiAllowances]
      ,[TotalRctiDeductions]
      ,[TotalRctiRedemptions]
	  ,CONVERT (FLOAT,'') AS Pickup_Scans
	  ,CONVERT (FLOAT,'') AS Delivery_Scans
	  ,CONVERT (FLOAT,'') AS On_Time
	  ,CONVERT (FLOAT,'') AS Not_OnTime
	  ,CONVERT (FLOAT,'') AS Not_Measured
	  ,CONVERT (FLOAT,'') AS OTP
	  ,CONVERT (FLOAT,'') AS OnBoard_Compliance
	  ,CONVERT (FLOAT,'') AS Delivery_Compliance
	  ,CONVERT (FLOAT,'') AS POD_Compliance
	  ,CONVERT (FLOAT,'') AS Scanning_Compliance
 
INTO #Temp1

FROM Pronto.[dbo].[FranchiseePaymentSummary]

WHERE [RunNumber] = @DriverNumber AND [ContractorId] = @ProntoID AND [RctiWeekEndingDate] >= @StartDate AND [RctiWeekEndingDate] < @EndDate

-------------------------------------------------------------------------------------
--Extract Performance Information for pickup and delivery

SELECT [PerformanceReportingID]
      ,[PerformanceDayID]
      ,[RevenueType]
      ,[DeliveryDriver]
      ,[PickupDriver]
      ,[OnTimeStatus]
      ,[PickupETAZone]
      ,[DeliveryETAZone]
      ,[NetworkCategoryID]
      ,[BUCode]
      ,[PerformanceCount]
      ,[OnBoardComplainceCount]
      ,[DeliveryComplainceCount]
      ,[PODComplainceCount]
      ,[AddWho]
      ,[AddDateTime]

INTO #Temp2

FROM [dbo].[DailyPerformanceReporting] AS a

LEFT JOIN [dbo].[DimCalendarDate_Harley] AS b
	ON a.PerformanceDayID = b.Id


WHERE a.[DeliveryDriver] = CASE 
											 WHEN LEFT (@ProntoID,1) = 'A' THEN 'ADL' 
											 WHEN LEFT (@ProntoID,1) = 'B' THEN 'BNE' 
											 WHEN LEFT (@ProntoID,1) = 'G' THEN 'GLC' 
											 WHEN LEFT (@ProntoID,1) = 'S' THEN 'SYD' 
											 WHEN LEFT (@ProntoID,1) = 'M' THEN 'MEL' 
											 WHEN LEFT (@ProntoID,1) = 'W' THEN 'PER' 
											 WHEN LEFT (@ProntoID,1) = 'C' THEN 'SYD' 
											 ELSE 'XXX' 
											 END + REPLICATE(0,4-LEN(@DriverNumber)) + CAST (@DriverNumber AS VARCHAR) + @ProntoID 
	OR a.[PickupDriver] = CASE 
											 WHEN LEFT (@ProntoID,1) = 'A' THEN 'ADL' 
											 WHEN LEFT (@ProntoID,1) = 'B' THEN 'BNE' 
											 WHEN LEFT (@ProntoID,1) = 'G' THEN 'GLC' 
											 WHEN LEFT (@ProntoID,1) = 'S' THEN 'SYD' 
											 WHEN LEFT (@ProntoID,1) = 'M' THEN 'MEL' 
											 WHEN LEFT (@ProntoID,1) = 'W' THEN 'PER' 
											 WHEN LEFT (@ProntoID,1) = 'C' THEN 'SYD' 
											 ELSE 'XXX' 
											 END + REPLICATE(0,4-LEN(@DriverNumber)) + CAST (@DriverNumber AS VARCHAR) + @ProntoID  
											 
	AND b.RCTIWeekingEnding >= @StartDate AND b.RCTIWeekingEnding < @EndDate

-------------------------------------------------------------------------------------
--Calc pickup volumes


SELECT b.RCTIWeekingEnding
	   ,a.[PickupDriver]
	   ,SUM (a.[PerformanceCount]) AS Pickup_Count

INTO #Temp3

FROM #Temp2 AS a

LEFT JOIN [dbo].[DimCalendarDate_Harley] AS b
	ON a.PerformanceDayID = b.Id

WHERE a.[PickupDriver] =  CASE 
											 WHEN LEFT (@ProntoID,1) = 'A' THEN 'ADL' 
											 WHEN LEFT (@ProntoID,1) = 'B' THEN 'BNE' 
											 WHEN LEFT (@ProntoID,1) = 'G' THEN 'GLC' 
											 WHEN LEFT (@ProntoID,1) = 'S' THEN 'SYD' 
											 WHEN LEFT (@ProntoID,1) = 'M' THEN 'MEL' 
											 WHEN LEFT (@ProntoID,1) = 'W' THEN 'PER' 
											 WHEN LEFT (@ProntoID,1) = 'C' THEN 'SYD' 
											 ELSE 'XXX' 
											 END + REPLICATE(0,4-LEN(@DriverNumber)) + CAST (@DriverNumber AS VARCHAR) + @ProntoID  

GROUP BY b.RCTIWeekingEnding
	   ,a.[PickupDriver]

-------------------------------------------------------------------------------------
--Calc On time items

SELECT b.RCTIWeekingEnding
	   ,a.DeliveryDriver
	   ,SUM (a.[PerformanceCount]) AS On_Time_Count

INTO #Temp4

FROM #Temp2 AS a

LEFT JOIN [dbo].[DimCalendarDate_Harley] AS b
	ON a.PerformanceDayID = b.Id

WHERE a.DeliveryDriver =  CASE 
											 WHEN LEFT (@ProntoID,1) = 'A' THEN 'ADL' 
											 WHEN LEFT (@ProntoID,1) = 'B' THEN 'BNE' 
											 WHEN LEFT (@ProntoID,1) = 'G' THEN 'GLC' 
											 WHEN LEFT (@ProntoID,1) = 'S' THEN 'SYD' 
											 WHEN LEFT (@ProntoID,1) = 'M' THEN 'MEL' 
											 WHEN LEFT (@ProntoID,1) = 'W' THEN 'PER' 
											 WHEN LEFT (@ProntoID,1) = 'C' THEN 'SYD' 
											 ELSE 'XXX' 
											 END + REPLICATE(0,4-LEN(@DriverNumber)) + CAST (@DriverNumber AS VARCHAR) + @ProntoID   AND [OnTimeStatus] IN ('Y','1')

GROUP BY b.RCTIWeekingEnding
	   ,a.DeliveryDriver

-------------------------------------------------------------------------------------
--Calc Not On Time Items

SELECT b.RCTIWeekingEnding
	   ,a.DeliveryDriver
	   ,SUM (a.[PerformanceCount]) AS NotOn_Time_Count

INTO #Temp5

FROM #Temp2 AS a

LEFT JOIN [dbo].[DimCalendarDate_Harley] AS b
	ON a.PerformanceDayID = b.Id

WHERE a.DeliveryDriver =  CASE 
											 WHEN LEFT (@ProntoID,1) = 'A' THEN 'ADL' 
											 WHEN LEFT (@ProntoID,1) = 'B' THEN 'BNE' 
											 WHEN LEFT (@ProntoID,1) = 'G' THEN 'GLC' 
											 WHEN LEFT (@ProntoID,1) = 'S' THEN 'SYD' 
											 WHEN LEFT (@ProntoID,1) = 'M' THEN 'MEL' 
											 WHEN LEFT (@ProntoID,1) = 'W' THEN 'PER' 
											 WHEN LEFT (@ProntoID,1) = 'C' THEN 'SYD' 
											 ELSE 'XXX' 
											 END + REPLICATE(0,4-LEN(@DriverNumber)) + CAST (@DriverNumber AS VARCHAR) + @ProntoID   AND [OnTimeStatus] IN ('N','2')

GROUP BY b.RCTIWeekingEnding
	   ,a.DeliveryDriver
-------------------------------------------------------------------------------------
--Calc Not Measured Items

SELECT b.RCTIWeekingEnding
	   ,a.DeliveryDriver
	   ,SUM (a.[PerformanceCount]) AS NotMeasured_Count

INTO #Temp6

FROM #Temp2 AS a

LEFT JOIN [dbo].[DimCalendarDate_Harley] AS b
	ON a.PerformanceDayID = b.Id

WHERE a.DeliveryDriver =  CASE 
											 WHEN LEFT (@ProntoID,1) = 'A' THEN 'ADL' 
											 WHEN LEFT (@ProntoID,1) = 'B' THEN 'BNE' 
											 WHEN LEFT (@ProntoID,1) = 'G' THEN 'GLC' 
											 WHEN LEFT (@ProntoID,1) = 'S' THEN 'SYD' 
											 WHEN LEFT (@ProntoID,1) = 'M' THEN 'MEL' 
											 WHEN LEFT (@ProntoID,1) = 'W' THEN 'PER' 
											 WHEN LEFT (@ProntoID,1) = 'C' THEN 'SYD' 
											 ELSE 'XXX' 
											 END + REPLICATE(0,4-LEN(@DriverNumber)) + CAST (@DriverNumber AS VARCHAR) + @ProntoID   AND [OnTimeStatus] NOT IN ('Y','1','N','2')

GROUP BY b.RCTIWeekingEnding
	   ,a.DeliveryDriver
-------------------------------------------------------------------------------------
--Calc Scanning Compliance

SELECT b.RCTIWeekingEnding
	,a.DeliveryDriver
	,SUM([PerformanceCount])  AS [PerformanceCount]
    ,SUM([OnBoardComplainceCount]) AS [OnBoardComplainceCount]
    ,SUM([DeliveryComplainceCount]) AS [DeliveryComplainceCount]
    ,SUM([PODComplainceCount]) AS [PODComplainceCount]
    ,CONVERT(DECIMAL(12,4),0) AS OnBoardPercentage
    ,CONVERT(DECIMAL(12,4),0) AS DeliveryPercentage
    ,CONVERT(DECIMAL(12,4),0) AS PODPercentage
    ,CONVERT(DECIMAL(12,4),0) AS TotalPercentage


INTO #Temp7

FROM [dbo].[DailyPerformanceReporting] AS a

LEFT JOIN [dbo].[DimCalendarDate_Harley] AS b ON a.[PerformanceDayID] = b.Id 

WHERE a.DeliveryDriver =  CASE 
											 WHEN LEFT (@ProntoID,1) = 'A' THEN 'ADL' 
											 WHEN LEFT (@ProntoID,1) = 'B' THEN 'BNE' 
											 WHEN LEFT (@ProntoID,1) = 'G' THEN 'GLC' 
											 WHEN LEFT (@ProntoID,1) = 'S' THEN 'SYD' 
											 WHEN LEFT (@ProntoID,1) = 'M' THEN 'MEL' 
											 WHEN LEFT (@ProntoID,1) = 'W' THEN 'PER' 
											 WHEN LEFT (@ProntoID,1) = 'C' THEN 'SYD' 
											 ELSE 'XXX' 
											 END + REPLICATE(0,4-LEN(@DriverNumber)) + CAST (@DriverNumber AS VARCHAR) + @ProntoID    AND b.RCTIWeekingEnding >= @StartDate AND b.RCTIWeekingEnding < @EndDate

GROUP BY b.RCTIWeekingEnding
	   ,a.DeliveryDriver

UPDATE #Temp7 SET OnBoardPercentage = CONVERT(DECIMAL(12,2),100*([OnBoardComplainceCount]))/CONVERT(DECIMAL(12,2),[PerformanceCount]) WHERE [PerformanceCount]>0
       
UPDATE #Temp7 SET DeliveryPercentage = CONVERT(DECIMAL(12,2),100*([DeliveryComplainceCount]))/CONVERT(DECIMAL(12,2),[PerformanceCount]) WHERE [PerformanceCount]>0
  
UPDATE #Temp7 SET PODPercentage = CONVERT(DECIMAL(12,2),100*([PODComplainceCount]))/CONVERT(DECIMAL(12,2),[PerformanceCount]) WHERE [PerformanceCount]>0

UPDATE #Temp7 SET TotalPercentage = CONVERT(DECIMAL(12,2),100*([OnBoardComplainceCount]+[DeliveryComplainceCount]+[PODComplainceCount]))/CONVERT(DECIMAL(12,2),[PerformanceCount]*3) WHERE [PerformanceCount]>0

-------------------------------------------------------------------------------------
--Update Result table with values

UPDATE #Temp1 SET Pickup_Scans = b.Pickup_Count  FROM #Temp1 AS a LEFT JOIN #Temp3 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

UPDATE #Temp1 SET On_Time = b.On_Time_Count FROM #Temp1 AS a LEFT JOIN #Temp4 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

UPDATE #Temp1 SET Not_OnTime = b.NotOn_Time_Count FROM #Temp1 AS a LEFT JOIN #Temp5 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

UPDATE #Temp1 SET Not_Measured = b.NotMeasured_Count FROM #Temp1 AS a LEFT JOIN #Temp6 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

UPDATE #Temp1 SET Delivery_Scans = On_Time + Not_OnTime + Not_Measured

UPDATE #Temp1 SET OTP = On_Time / (On_Time + Not_OnTime) WHERE On_Time + Not_OnTime <> 0

UPDATE #Temp1 SET OnBoard_Compliance = b.OnBoardPercentage / 100 FROM #Temp1 AS a LEFT JOIN #Temp7 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

UPDATE #Temp1 SET Delivery_Compliance = b.DeliveryPercentage / 100 FROM #Temp1 AS a LEFT JOIN #Temp7 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

UPDATE #Temp1 SET POD_Compliance = b.PODPercentage / 100 FROM #Temp1 AS a LEFT JOIN #Temp7 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

UPDATE #Temp1 SET Scanning_Compliance = b.TotalPercentage / 100 FROM #Temp1 AS a LEFT JOIN #Temp7 AS b ON a.RctiWeekEndingDate = b.RCTIWeekingEnding 

SELECT * FROM #Temp1 ORDER BY RCTIWeekEndingdate

END


GO
GRANT EXECUTE
	ON [dbo].[usp_Rpt_Fleet_FranchiseeReviewData]
	TO [ReportUser]
GO
