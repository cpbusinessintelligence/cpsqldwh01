SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Consignment_Archive_17-18] (
		[ConsignmentID]                [int] NOT NULL,
		[Sourcereference]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GWConsignmentID]              [int] NULL,
		[RevenueType]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProductType]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceType]                  [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConNote]                      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerConNoteDate]          [date] NULL,
		[ConNoteCreatedDate]           [date] NULL,
		[CustomerETADate]              [date] NULL,
		[AccountCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress]                [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[ItemCount]                    [int] NULL,
		[CouponCount]                  [int] NULL,
		[OriginBranch]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginDepot]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DestinationBranch]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DestinationDepot]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategory]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SalesOrderNumber]             [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[SalesOrderLine]               [int] NULL,
		[SalesOrderDate]               [date] NULL,
		[RevenueAmount]                [decimal](13, 2) NULL,
		[InsuranceAmount]              [decimal](13, 2) NULL,
		[InsuranceCategory]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueRecognisedDate]        [date] NULL,
		[RevenueProcessedDateTime]     [datetime] NULL,
		[IsRevenueProcessed]           [bit] NOT NULL,
		[AddWho]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                  [datetime] NULL
)
GO
ALTER TABLE [dbo].[Consignment_Archive_17-18] SET (LOCK_ESCALATION = TABLE)
GO
