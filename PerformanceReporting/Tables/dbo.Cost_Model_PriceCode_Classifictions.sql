SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cost_Model_PriceCode_Classifictions] (
		[pc_code]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[pc_name]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[pc_category]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Std_Custom]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Category]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Grouping]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PriceCategory]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Packaging_Costs]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelfaultWeight]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddWho]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddWhen]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Cost_Model_PriceCode_Classifictions] SET (LOCK_ESCALATION = TABLE)
GO
