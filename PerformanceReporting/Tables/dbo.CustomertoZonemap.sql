SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomertoZonemap] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[Accountcode]         [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[BillTo]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Locality]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepName]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ETAZone]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CustomertoZonemap]
	ADD
	CONSTRAINT [DF__Customert__Creat__3E1D39E1]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[CustomertoZonemap]
	ADD
	CONSTRAINT [DF__Customert__Creat__3F115E1A]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[CustomertoZonemap]
	ADD
	CONSTRAINT [DF__Customert__Edite__40058253]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[CustomertoZonemap]
	ADD
	CONSTRAINT [DF__Customert__Edite__40F9A68C]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[CustomertoZonemap] SET (LOCK_ESCALATION = TABLE)
GO
