SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyAgentReporting_bckup_for_Addedby20200324] (
		[AgentReportingID]       [int] IDENTITY(1, 1) NOT NULL,
		[AgentDayID]             [int] NOT NULL,
		[RevenueType]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProductType]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AgentName]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriver]         [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupDriver]           [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[OnTimeStatus]           [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupETAZone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]      [int] NULL,
		[BUCode]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceCount]       [int] NOT NULL,
		[AddWho]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]            [datetime] NULL,
		[ProductSubCategory]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DailyAgentReporting_bckup_for_Addedby20200324] SET (LOCK_ESCALATION = TABLE)
GO
