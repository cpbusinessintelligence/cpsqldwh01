SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyMissedPickupReporting] (
		[MissedPickupReportingID]        [int] IDENTITY(1, 1) NOT NULL,
		[MissedPickupReportingDayID]     [int] NOT NULL,
		[RevenueType]                    [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[OnBoardDriver]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FutileCount]                    [int] NULL,
		[MissedCount]                    [int] NULL,
		[PartialCount]                   [int] NULL,
		[PickedUpCount]                  [int] NULL,
		[Total]                          [int] NULL,
		[AddWho]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                    [datetime] NULL,
		CONSTRAINT [PK_DailyMissedPickupReporting]
		PRIMARY KEY
		CLUSTERED
		([MissedPickupReportingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DailyMissedPickupReporting] SET (LOCK_ESCALATION = TABLE)
GO
