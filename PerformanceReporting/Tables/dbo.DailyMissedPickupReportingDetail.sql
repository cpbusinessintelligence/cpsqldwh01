SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyMissedPickupReportingDetail] (
		[ConsignmentNumber]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[RevenueType]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CustomerName]                 [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[BookingNumber]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookingDate]                  [datetime] NULL,
		[PickupCustomerName]           [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress1]               [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress2]               [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress3]               [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PickupSuburb]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostcode]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[JobSentTime]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[JobAcceptedTime]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DispatchAutomated]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WrongJobbed]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CustomerPU/HubbedPU]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[HubbedLocation]               [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentStatus]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FutileTimeStamp]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FutileReason]                 [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ByZone]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupBooking]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentDate]              [datetime] NULL,
		[PerformanceReportingDate]     [date] NULL
)
GO
ALTER TABLE [dbo].[DailyMissedPickupReportingDetail] SET (LOCK_ESCALATION = TABLE)
GO
