SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyNoTimeReporting] (
		[NoTimeReportingID]        [int] IDENTITY(1, 1) NOT NULL,
		[NoTimeReportingDayID]     [int] NOT NULL,
		[RevenueType]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[OnBoardDriver]            [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupETAZone]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NoTimeCount]              [int] NOT NULL,
		[AddWho]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]              [datetime] NULL,
		CONSTRAINT [PK_DailyNoTimeReporting]
		PRIMARY KEY
		CLUSTERED
		([NoTimeReportingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DailyNoTimeReporting] SET (LOCK_ESCALATION = TABLE)
GO
