SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyNoTimeReportingDetail] (
		[LabelNumber]                  [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[RevenueType]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupScan]                   [datetime] NULL,
		[PickupDriver]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupBranch]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[PickupDepot]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AttemptScan]                  [datetime] NULL,
		[OnBoardDriver]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[OnBoardDepot]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[OnBoardScan]                  [datetime] NULL,
		[DeliveryScan]                 [datetime] NULL,
		[DeliveryDriver]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDepot]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryHours]                [decimal](12, 2) NULL,
		[PickupETAZone]                [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]              [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[1stDeliveryCutOff]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stAllowedDays]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndDeliveryCutOff]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndAllowedDays]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceReportingDate]     [date] NULL
)
GO
ALTER TABLE [dbo].[DailyNoTimeReportingDetail] SET (LOCK_ESCALATION = TABLE)
GO
