SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyNoTimeReporting_bckup_for_Addedby20200324] (
		[NoTimeReportingID]        [int] IDENTITY(1, 1) NOT NULL,
		[NoTimeReportingDayID]     [int] NOT NULL,
		[RevenueType]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[OnBoardDriver]            [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupETAZone]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NoTimeCount]              [int] NOT NULL,
		[AddWho]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]              [datetime] NULL
)
GO
ALTER TABLE [dbo].[DailyNoTimeReporting_bckup_for_Addedby20200324] SET (LOCK_ESCALATION = TABLE)
GO
