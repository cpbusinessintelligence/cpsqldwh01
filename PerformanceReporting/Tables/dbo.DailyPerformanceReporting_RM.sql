SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyPerformanceReporting_RM] (
		[PerformanceReportingID]      [int] IDENTITY(1, 1) NOT NULL,
		[PerformanceDayID]            [int] NOT NULL,
		[RevenueType]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProductType]                 [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountCode]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriver]              [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupDriver]                [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[OnTimeStatus]                [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupETAZone]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]           [int] NULL,
		[BUCode]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceCount]            [int] NOT NULL,
		[OnBoardComplainceCount]      [int] NOT NULL,
		[DeliveryComplainceCount]     [int] NOT NULL,
		[PODComplainceCount]          [int] NOT NULL,
		[AddWho]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                 [datetime] NULL,
		[ProductSubCategory]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_DailyPerformanceReporting_RM]
		PRIMARY KEY
		CLUSTERED
		([PerformanceReportingID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DailyPerformanceReporting_RM] SET (LOCK_ESCALATION = TABLE)
GO
