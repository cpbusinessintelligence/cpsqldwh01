SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyPerformanceReporting_Unprocessed] (
		[PerformanceReportingID]      [int] IDENTITY(1, 1) NOT NULL,
		[PerformanceDayID]            [int] NOT NULL,
		[RevenueType]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ProductType]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriver]              [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[PickupDriver]                [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[OnTimeStatus]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupETAZone]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]           [int] NULL,
		[BUCode]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceCount]            [int] NULL,
		[OnBoardComplainceCount]      [int] NULL,
		[DeliveryComplainceCount]     [int] NULL,
		[PODComplainceCount]          [int] NULL,
		[AddWho]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                 [datetime] NULL,
		[ProductSubCategory]          [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DailyPerformanceReporting_Unprocessed] SET (LOCK_ESCALATION = TABLE)
GO
