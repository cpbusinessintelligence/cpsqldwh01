SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveryOptions] (
		[DeliveryOptionid]             [int] IDENTITY(1, 1) NOT NULL,
		[Category]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Labelnumber]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Consignmentcode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FailedDeliveryCardNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Deliverymethod]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ScanDatetime]                 [datetime] NULL,
		[PickupETAZone]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NewDeliveryETAZone]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                      [datetime] NULL,
		[AddWho]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                  [datetime] NULL,
		[DeliveryContractor]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDatetime]             [datetime] NULL,
		[OnTimeStatus]                 [int] NULL,
		CONSTRAINT [PK__Delivery__A6456072ED73C690]
		PRIMARY KEY
		CLUSTERED
		([DeliveryOptionid])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DeliveryOptions] SET (LOCK_ESCALATION = TABLE)
GO
