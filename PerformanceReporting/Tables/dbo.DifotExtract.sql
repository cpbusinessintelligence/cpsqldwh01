SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[DifotExtract] (
		[Consignment1]        [int] NOT NULL,
		[GWConsignmentID]     [int] NULL,
		[ConsignmentID]       [int] NULL
)
GO
ALTER TABLE [dbo].[DifotExtract] SET (LOCK_ESCALATION = TABLE)
GO
