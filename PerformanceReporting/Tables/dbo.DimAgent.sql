SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimAgent] (
		[Id]                   [int] IDENTITY(1, 1) NOT NULL,
		[AgentID]              [int] NOT NULL,
		[AgentCode]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AgentName]            [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[SupplierCode]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[QueryBranch]          [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsCPPLContractor]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsPODRequired]        [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[DriverNumber]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [UQ__DimAgent__9AC3BFD01F025B7B]
		UNIQUE
		NONCLUSTERED
		([AgentID])
		ON [PRIMARY],
		CONSTRAINT [PK__DimAgent__3214EC076892398D]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimAgent] SET (LOCK_ESCALATION = TABLE)
GO
