SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimAlerts] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[Date]                [datetime] NULL,
		[Description]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[Impact]              [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__DimAlert__CA1FE4642848F00F]
		PRIMARY KEY
		CLUSTERED
		([Sno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimAlerts]
	ADD
	CONSTRAINT [DF__DimAlerts__Creat__1DE57479]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[DimAlerts]
	ADD
	CONSTRAINT [DF__DimAlerts__Creat__1ED998B2]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DimAlerts]
	ADD
	CONSTRAINT [DF__DimAlerts__Edite__1FCDBCEB]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[DimAlerts]
	ADD
	CONSTRAINT [DF__DimAlerts__Edite__20C1E124]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[DimAlerts] SET (LOCK_ESCALATION = TABLE)
GO
