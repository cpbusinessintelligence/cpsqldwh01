SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimCalendarDate] (
		[Id]                      [int] IDENTITY(1, 1) NOT NULL,
		[Date]                    [date] NOT NULL,
		[Day]                     [int] NOT NULL,
		[WeekNumber]              [int] NOT NULL,
		[CalendarMonthNumber]     [int] NOT NULL,
		[CalendarQuarter]         [int] NULL,
		[CalendarYear]            [int] NULL,
		[WeekDay]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[WeekEndingdate]          [date] NULL,
		[CalendarMonthName]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FiscalMonthNumber]       [int] NULL,
		[FiscalQuarter]           [int] NULL,
		[FiscalYear]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SortOrder]               [int] NULL
)
GO
ALTER TABLE [dbo].[DimCalendarDate] SET (LOCK_ESCALATION = TABLE)
GO
