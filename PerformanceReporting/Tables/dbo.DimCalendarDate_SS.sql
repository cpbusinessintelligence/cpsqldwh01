SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimCalendarDate_SS] (
		[Id]                      [int] NULL,
		[Date]                    [date] NULL,
		[Day]                     [int] NULL,
		[WeekNumber]              [int] NULL,
		[CalendarMonthNumber]     [int] NULL,
		[CalendarQuarter]         [int] NULL,
		[CalendarYear]            [int] NULL,
		[WeekDay]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[WeekEndingdate]          [date] NULL,
		[CalendarMonthName]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FiscalMonthNumber]       [int] NULL,
		[FiscalQuarter]           [int] NULL,
		[FiscalYear]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SortOrder]               [int] NULL,
		[RCTIWeekingEnding]       [date] NULL
)
GO
ALTER TABLE [dbo].[DimCalendarDate_SS] SET (LOCK_ESCALATION = TABLE)
GO
