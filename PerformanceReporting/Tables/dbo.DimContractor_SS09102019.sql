SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimContractor_SS09102019] (
		[Id]                     [int] IDENTITY(1, 1) NOT NULL,
		[DriverID]               [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DriverNumber]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntoID]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DepotCode]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DepotName]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Branch]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[State]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ContractorType]         [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[DriverName]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[ETAZone]                [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[PricingZone]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ContractorCategory]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Region]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Address1]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address2]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address3]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PhoneNumber]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[MobileNumber]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FaxNumber]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CreditLimit]            [decimal](12, 2) NULL,
		[StartDate]              [date] NULL,
		[EndDate]                [date] NULL,
		[ArchiveDate]            [date] NULL,
		[isactive]               [bit] NULL
)
GO
ALTER TABLE [dbo].[DimContractor_SS09102019] SET (LOCK_ESCALATION = TABLE)
GO
