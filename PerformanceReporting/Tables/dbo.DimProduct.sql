SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimProduct] (
		[RevenueType]                 [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Code]                        [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Category]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RevenueCategory]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Description]                 [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[BusinessUnit]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[StockStatus]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[StockSalesType]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[StockConversionFactor]       [int] NULL,
		[StockPricePer]               [int] NULL,
		[StockReplacementCost]        [decimal](12, 2) NULL,
		[StkLastChange]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[StockDefaultWeight]          [decimal](12, 4) NULL,
		[StockDefaultCube]            [decimal](12, 4) NULL,
		[StockDefaultInstruction]     [decimal](12, 4) NULL,
		CONSTRAINT [PK_DimProduct]
		PRIMARY KEY
		CLUSTERED
		([RevenueType], [Code])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimProduct] SET (LOCK_ESCALATION = TABLE)
GO
