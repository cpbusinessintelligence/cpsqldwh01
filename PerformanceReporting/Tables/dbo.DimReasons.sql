SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimReasons] (
		[Sno]                   [int] IDENTITY(1, 1) NOT NULL,
		[Reasoncode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReasonDescription]     [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]        [datetime] NULL,
		[EditedBy]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__DimReaso__CA1FE46418185CDF]
		PRIMARY KEY
		CLUSTERED
		([Sno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimReasons]
	ADD
	CONSTRAINT [DF__DimReason__Creat__182C9B23]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[DimReasons]
	ADD
	CONSTRAINT [DF__DimReason__Creat__1920BF5C]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DimReasons]
	ADD
	CONSTRAINT [DF__DimReason__Edite__1A14E395]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[DimReasons]
	ADD
	CONSTRAINT [DF__DimReason__Edite__1B0907CE]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[DimReasons] SET (LOCK_ESCALATION = TABLE)
GO
