SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimStatus] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[Statuscode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDescription]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]        [datetime] NULL,
		[EditedBy]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]           [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__DimStatu__CA1FE46433CB477A]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[DimStatus]
	ADD
	CONSTRAINT [DF__DimStatus__Creat__239E4DCF]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[DimStatus]
	ADD
	CONSTRAINT [DF__DimStatus__Creat__24927208]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DimStatus]
	ADD
	CONSTRAINT [DF__DimStatus__Edite__25869641]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[DimStatus]
	ADD
	CONSTRAINT [DF__DimStatus__Edite__267ABA7A]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[DimStatus] SET (LOCK_ESCALATION = TABLE)
GO
