SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ETACalculatorMinus1Day] (
		[ETAID]                        [int] IDENTITY(1, 1) NOT NULL,
		[Category]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                     [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToZone]                       [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[PrimaryNetworkCategory]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryNetworkCategory]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ETA]                          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[FromETA]                      [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ToETA]                        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[1stPickupCutOffTime]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stDeliveryCutOffTime]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stAllowedDays]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[2ndPickupCutOffTime]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndDeliveryCutOffTime]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndAllowedDays]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AddWho]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                  [datetime] NOT NULL,
		[EditWho]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]                 [datetime] NULL
)
GO
ALTER TABLE [dbo].[ETACalculatorMinus1Day]
	ADD
	CONSTRAINT [DF1_ETACalculatorMinus1Day_AddDateTime]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[ETACalculatorMinus1Day]
	ADD
	CONSTRAINT [DF1_ETACalculatorMinus1Day_EditDateTime]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_ETACalculatorMinus1Day]
	ON [dbo].[ETACalculatorMinus1Day] ([Category], [FromZone], [ToZone])
	ON [PerformanceReporting_Archives]
GO
ALTER TABLE [dbo].[ETACalculatorMinus1Day] SET (LOCK_ESCALATION = TABLE)
GO
