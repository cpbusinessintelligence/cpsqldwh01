SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Excep] (
		[ProductType]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Prefix/AccountCode]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[LHType]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Service]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Tozone]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Customer/PrefixName]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[PickupBranch]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ReportTypeFlag]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[AddtoETADays]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[AllowedDays]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[AllowFromFlag]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[1stPickupCutOff]         [datetime] NULL,
		[1stDeliveryCutOff]       [datetime] NULL,
		[1stAllowedDays]          [float] NULL,
		[2ndPickupCutOff]         [datetime] NULL,
		[2ndDeliveryOff]          [datetime] NULL,
		[2ndAllowedDays]          [float] NULL
)
GO
ALTER TABLE [dbo].[Excep] SET (LOCK_ESCALATION = TABLE)
GO
