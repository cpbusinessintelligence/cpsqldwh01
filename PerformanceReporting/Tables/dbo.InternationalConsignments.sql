SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InternationalConsignments] (
		[ConsignmentID]                [int] IDENTITY(1, 1) NOT NULL,
		[GWConsignmentID]              [int] NULL,
		[ProductType]                  [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Category]                     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Sourcereference]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConNote]                      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConNoteCreatedDate]           [date] NULL,
		[CustomerETADate]              [date] NULL,
		[AccountCode]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress]                [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[ItemCount]                    [int] NULL,
		[CouponCount]                  [int] NULL,
		[OriginBranch]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[OriginDepot]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DestinationBranch]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DestinationDepot]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategory]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SalesOrderNumber]             [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[SalesOrderLine]               [int] NULL,
		[SalesOrderDate]               [date] NULL,
		[RevenueAmount]                [decimal](13, 2) NULL,
		[InsuranceAmount]              [decimal](13, 2) NULL,
		[InsuranceCategory]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RevenueRecognisedDate]        [date] NULL,
		[RevenueProcessedDateTime]     [datetime] NULL,
		[IsRevenueProcessed]           [bit] NOT NULL,
		[AddWho]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]                  [datetime] NULL,
		CONSTRAINT [PK_InternationalConsignments]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[InternationalConsignments]
	ADD
	CONSTRAINT [DF_InternationalConsignments_IsRevenueProcessed]
	DEFAULT ((0)) FOR [IsRevenueProcessed]
GO
ALTER TABLE [dbo].[InternationalConsignments] SET (LOCK_ESCALATION = TABLE)
GO
