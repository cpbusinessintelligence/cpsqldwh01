SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InternationalLabels] (
		[LabelID]                        [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]                  [int] NULL,
		[Sourcereference]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                    [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[Category]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProductType]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromCountry]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromState]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromPostcode]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FromSuburb]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToCountry]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToState]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToPostcode]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ToSuburb]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FirstScannedBy]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FirstscanDateTime]              [datetime] NULL,
		[FirstScanType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupScannedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupDateTime]                 [datetime] NULL,
		[PickupETAZone]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsolidateScannedBy]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ConsolidateDateTime]            [datetime] NULL,
		[ConsolidatedBarcode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustomsClearanceBy]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CustomsClearanceDateTime]       [datetime] NULL,
		[AttemptedDeliveryScannedBy]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryDateTime]      [datetime] NULL,
		[DeliveryScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDateTime]               [datetime] NULL,
		[DeliveryETAZone]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsPODPresent]                   [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[PODDateTime]                    [datetime] NULL,
		[LastScannedBy]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LastDateTime]                   [datetime] NULL,
		[LastScanType]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                        [datetime] NULL,
		[OnTimeStatusId]                 [int] NULL,
		[ETARuletoprocess]               [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[Reasoncode]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceReportingDate]       [date] NULL,
		[PerformanceProcessed]           [int] NULL,
		[isExceptionapplied]             [bit] NULL,
		[AddWho]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelCreatedDateTime]           [datetime] NULL,
		[AddDateTime]                    [datetime] NULL,
		CONSTRAINT [PK_InternationalLabels]
		PRIMARY KEY
		CLUSTERED
		([LabelID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[InternationalLabels]
	ADD
	CONSTRAINT [DF__Internati__isExc__30C33EC3]
	DEFAULT ((0)) FOR [isExceptionapplied]
GO
ALTER TABLE [dbo].[InternationalLabels] SET (LOCK_ESCALATION = TABLE)
GO
