SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LinehaulData] (
		[ConsolidatedBarcode]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ConsolidatedDateTime]       [datetime] NULL,
		[ConsolidateScannedBy]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[PickupETAZone]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeConsolidateDateTime]      [datetime] NULL,
		[DeConsolidateScannedBy]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ProductType]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                    [datetime] NULL,
		[CreatedDatetime]            [datetime] NULL,
		[EditedDatetime]             [datetime] NULL,
		[OnTimeStatusId]             [int] NULL
)
GO
ALTER TABLE [dbo].[LinehaulData]
	ADD
	CONSTRAINT [DF__LinehaulD__Creat__66EA454A]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[LinehaulData]
	ADD
	CONSTRAINT [DF__LinehaulD__Edite__67DE6983]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[LinehaulData] SET (LOCK_ESCALATION = TABLE)
GO
