SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Network_Depot_Mapping] (
		[State]         [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[DepotName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MappedTo]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Network_Depot_Mapping] SET (LOCK_ESCALATION = TABLE)
GO
