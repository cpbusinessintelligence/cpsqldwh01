SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PerformanceReportingExceptions] (
		[Sno]                     [int] IDENTITY(1, 1) NOT NULL,
		[ProductType]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Prefix/AccountCode]      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LHType]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Service]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Tozone]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Customer/PrefixName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupBranch]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReportTypeFlag]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddtoETADays]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AllowedDays]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AllowFromFlag]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stPickupCutOff]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stDeliveryCutOff]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[1stAllowedDays]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndPickupCutOff]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndDeliveryOff]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[2ndAllowedDays]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[isactive]                [bit] NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_PerformanceReportingExceptions]
	ON [dbo].[PerformanceReportingExceptions] ([LHType], [FromZone], [Tozone])
	ON [PerformanceReporting_Archives]
GO
ALTER TABLE [dbo].[PerformanceReportingExceptions] SET (LOCK_ESCALATION = TABLE)
GO
