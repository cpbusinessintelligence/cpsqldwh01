SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PostCodes_Trevor] (
		[Lookup]                             [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Pcode]                              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Locality]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Comments]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[STATUS]                             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DepotCode]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SortCode]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CPLTerritory]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrimaryPricingZone]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryPricingZone]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PricingZone]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ETAZone]                            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedemptionZone]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrimaryDeliveryAgent]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrimaryAgentRunCode]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AlternateDeliveryAgent]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AlternateAgentRunCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedeliveryFlag]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SatchelLinks]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RoadExpressPickup]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RoadExpressDelivery]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RoadExpressCutTime]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CosmosCutLetter]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DomesticPriorityPickup]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DomesticPriorityDelivery]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DomesticPriorityCutTime]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DomesticMidTierPickup]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DomesticMidTierDelivery]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DomesticMidTierCutTime]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[International_PriorityPickup]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[International_PriorityDelivery]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[International_PriorityCutTime]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[International_SaverPickup]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[International_SaverDelivery]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[International_SaverCutTime]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DefaultCutTime]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CosmosDepotName]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 38]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 39]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 40]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PostCodes_Trevor] SET (LOCK_ESCALATION = TABLE)
GO
