SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrimaryLabels_Archive_16-17] (
		[PrimaryLabelsID]                       [int] NOT NULL,
		[ConsignmentID]                         [int] NULL,
		[SourceReference]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueType]                           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                           [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProductType]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FirstScannedBy]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FirstDateTime]                         [datetime] NULL,
		[FirstScanType]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupScannedBy]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupDateTime]                        [datetime] NULL,
		[InDepotScannedBy]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[InDepotDateTime]                       [datetime] NULL,
		[HandoverScannedBy]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[HandoverDateTime]                      [datetime] NULL,
		[TransferScannedBy]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[TransferDateTime]                      [datetime] NULL,
		[ConsolidateScannedBy]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ConsolidateDateTime]                   [datetime] NULL,
		[ConsolidatedBarcode]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeConsolidateScannedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeConsolidateDateTime]                 [datetime] NULL,
		[OutforDeliveryScannedBy]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[OutForDeliverDateTime]                 [datetime] NULL,
		[DelayofDeliveryScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DelayofDeliveryScannedDateTime]        [datetime] NULL,
		[DelayofDeliveryDate]                   [datetime] NULL,
		[QuerycageScannedBy]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[QuerycageDateTime]                     [datetime] NULL,
		[QueryCageNumber]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryScannedBy]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryDateTime]             [datetime] NULL,
		[AttemptedDeliveryReceivedDatetime]     [datetime] NULL,
		[AttemptedDeliveryCardNumber]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CardCategory]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AcceptedbyPOPPointStatus]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AcceptedbyPOPPointScannedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AcceptedbyPOPPointScannedDateTime]     [datetime] NULL,
		[DeliveryOptionId]                      [int] NULL,
		[DeliveryScannedBy]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDateTime]                      [datetime] NULL,
		[DeliveryReceivedDateTime]              [datetime] NULL,
		[IsPODPresent]                          [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[PODDateTime]                           [datetime] NULL,
		[DeliveryContractorType]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryContractorName]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LastScannedBy]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LastDateTime]                          [datetime] NULL,
		[LastScanType]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupETAZone]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryETAZone]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]                     [int] NULL,
		[ETADate]                               [datetime] NULL,
		[OnTimeStatusId]                        [int] NULL,
		[ETARuletoprocess]                      [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[Reasoncode]                            [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[PerformanceReportingDate]              [date] NULL,
		[PerformanceProcessed]                  [int] NULL,
		[isExceptionapplied]                    [bit] NULL,
		[AddWho]                                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelCreatedDateTime]                  [datetime] NULL,
		[AddDateTime]                           [datetime] NULL,
		[PerformanceProcessedDate]              [date] NULL,
		[ProductSubCategory]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryReason]               [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PrimaryLabels_Archive_16-17] SET (LOCK_ESCALATION = TABLE)
GO
