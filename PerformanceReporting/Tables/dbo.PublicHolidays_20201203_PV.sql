SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PublicHolidays_20201203_PV] (
		[Date]            [date] NOT NULL,
		[Description]     [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
		[Zone]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[PublicHolidays_20201203_PV] SET (LOCK_ESCALATION = TABLE)
GO
