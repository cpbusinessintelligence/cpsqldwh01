SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PublicHolidays_tmp] (
		[Date]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Zone]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PublicHolidays_tmp] SET (LOCK_ESCALATION = TABLE)
GO
