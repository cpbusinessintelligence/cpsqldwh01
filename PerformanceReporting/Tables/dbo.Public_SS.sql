SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Public_SS] (
		[Date]            [datetime] NULL,
		[Description]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Zone]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Public_SS] SET (LOCK_ESCALATION = TABLE)
GO
