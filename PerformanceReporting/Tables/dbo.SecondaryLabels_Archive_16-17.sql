SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SecondaryLabels_Archive_16-17] (
		[SecondaryLabelsID]        [int] NOT NULL,
		[RevenueType]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryLabelNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SecondaryLabelPrefix]     [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[PrimaryLabelNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ScanDateTime]             [datetime] NULL,
		[ScannedBy]                [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[LabelCreatedDateTime]     [datetime] NULL,
		[AddWho]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]              [datetime] NULL
)
GO
ALTER TABLE [dbo].[SecondaryLabels_Archive_16-17] SET (LOCK_ESCALATION = TABLE)
GO
