SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SortCode] (
		[Pcode]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Locality]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrimaryDeliveryAgent]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sort Zone]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EDI sort code]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SortCode] SET (LOCK_ESCALATION = TABLE)
GO
