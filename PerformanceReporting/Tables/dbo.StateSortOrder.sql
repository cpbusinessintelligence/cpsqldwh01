SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StateSortOrder] (
		[state]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[OnTimeStatusId]     [int] NULL,
		[SortOrder]          [int] NULL
)
GO
ALTER TABLE [dbo].[StateSortOrder] SET (LOCK_ESCALATION = TABLE)
GO
