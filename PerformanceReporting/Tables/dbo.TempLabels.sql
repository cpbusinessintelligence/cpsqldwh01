SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempLabels] (
		[RevenueType]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                    [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProductType]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupScannedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupDateTime]                 [datetime] NULL,
		[PickUpZone]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryDateTime]      [datetime] NULL,
		[AttemptedDeliveryScannedBy]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDateTime]               [datetime] NULL,
		[DeliveryScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                        [datetime] NULL,
		[DeliveryContractorType]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DelayofDeliveryDate]            [datetime] NULL,
		[NetworkCategoryID]              [int] NULL,
		[DeliveryOptionId]               [int] NULL,
		[OnTimeStatusId]                 [int] NULL,
		[PerformanceReportingDate]       [date] NULL,
		[PerformanceProcessed]           [int] NULL,
		[LabelCreatedDateTime]           [datetime] NULL
)
GO
ALTER TABLE [dbo].[TempLabels] SET (LOCK_ESCALATION = TABLE)
GO
