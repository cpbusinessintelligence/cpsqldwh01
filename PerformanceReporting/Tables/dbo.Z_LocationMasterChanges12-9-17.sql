SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_LocationMasterChanges12-9-17] (
		[PostCode]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrizeZone]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ETAZone]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedemptionZone]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DepotCode]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SortCode]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgent]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentRunCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AlternateDeliveryAgent]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AlternateDeliveryAgentRunCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedeliveryFlag]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SatchelLinks]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RoadExpressPickupFlag]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RoadExpressDeliveryFlag]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RoadExpressCutOffTime]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CosmosCutLetter]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Z_LocationMasterChanges12-9-17] SET (LOCK_ESCALATION = TABLE)
GO
