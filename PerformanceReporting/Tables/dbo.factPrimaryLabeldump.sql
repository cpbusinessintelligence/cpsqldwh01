SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[factPrimaryLabeldump] (
		[cd_date]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_connote]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[factPrimaryLabeldump] SET (LOCK_ESCALATION = TABLE)
GO
