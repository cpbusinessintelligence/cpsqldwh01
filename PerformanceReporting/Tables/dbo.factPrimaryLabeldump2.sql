SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[factPrimaryLabeldump2] (
		[cd_date]         [datetime] NULL,
		[Labelnumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[factPrimaryLabeldump2] SET (LOCK_ESCALATION = TABLE)
GO
