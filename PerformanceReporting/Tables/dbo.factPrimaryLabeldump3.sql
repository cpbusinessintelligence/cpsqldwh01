SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[factPrimaryLabeldump3] (
		[cd_date]         [datetime2](7) NOT NULL,
		[LabelNumber]     [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[factPrimaryLabeldump3] SET (LOCK_ESCALATION = TABLE)
GO
