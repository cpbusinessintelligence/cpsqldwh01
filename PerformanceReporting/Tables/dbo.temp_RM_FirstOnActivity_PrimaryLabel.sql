SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_RM_FirstOnActivity_PrimaryLabel] (
		[Consignment]            [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountNumber]          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[Items]                  [int] NULL,
		[cd_date]                [smalldatetime] NULL,
		[cd_pickup_suburb]       [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_postcode]     [int] NULL
)
GO
ALTER TABLE [dbo].[temp_RM_FirstOnActivity_PrimaryLabel] SET (LOCK_ESCALATION = TABLE)
GO
