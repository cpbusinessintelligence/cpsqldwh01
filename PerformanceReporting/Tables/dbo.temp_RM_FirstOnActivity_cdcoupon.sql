SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_RM_FirstOnActivity_cdcoupon] (
		[Consignment]                    [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountNumber]                  [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[Items]                          [int] NULL,
		[cd_date]                        [smalldatetime] NULL,
		[cd_pickup_suburb]               [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_pickup_postcode]             [int] NULL,
		[Category]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SubCategory]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Exceptions]                     [varchar](120) COLLATE Latin1_General_CI_AS NULL,
		[PickupZone]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr0]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr1]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_addr2]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_suburb]             [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[cd_delivery_postcode]           [int] NULL,
		[DeliveryState]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategoryID]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[NetworkCategory]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FromETA]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ToETA]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETA]                            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETADate]                        [datetime] NULL,
		[StatusID]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[StatusDescription]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cc_coupon]                      [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                     [datetime] NULL,
		[OutForDeliveryDate]             [datetime] NULL,
		[AttemptedDeliveryDate]          [datetime] NULL,
		[AttemptedDeliveryCard]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]                   [datetime] NULL,
		[Total]                          [int] NULL,
		[PickupScannedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AttemptedDeliveryScannedBy]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryScannedBy]              [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[temp_RM_FirstOnActivity_cdcoupon] SET (LOCK_ESCALATION = TABLE)
GO
