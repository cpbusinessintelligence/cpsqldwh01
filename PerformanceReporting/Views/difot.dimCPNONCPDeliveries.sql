SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [difot].[dimCPNONCPDeliveries] AS
SELECT CONCAT (PostCode,	Suburb) As PostCodeKey
      ,[PostCodeID]
      ,[PostCode]
      ,[Suburb]
      ,[State]
      ,[ETAZone]
      ,[DepotCode]
      ,[SortCode]
      ,[DeliveryAgent]
      ,[AlternateDeliveryAgentRunCode]
      ,[RedeliveryFlag]
      ,[RoadExpressPickupFlag]
	  ,[RoadExpressDeliveryFlag]	  
	  ,DeliveryType = CASE WHEN [RoadExpressDeliveryFlag] ='Y' THEN 'CP Delivered'
	                       ELSE 'Non CP Delivered'
						   END                  
FROM [CouponCalculator].[dbo].[PostCodes] With(Nolock)
GO
