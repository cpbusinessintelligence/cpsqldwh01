SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [difot].[dimConsignments] AS
SELECT cc.[cd_connote] 
	  ,cc.[cd_id]
      ,cc.[cd_date]
      ,cc.[cd_consignment_date]
      ,cc.[cd_eta_date]
      ,cc.[cd_eta_earliest]
      ,cc.[cd_customer_eta]
	  ,CONCAT(cc.[cd_pickup_postcode],cc.[cd_pickup_suburb]) AS PickupPostCodeKey
      ,cc.[cd_pickup_addr0]
      ,cc.[cd_pickup_addr1]
      ,cc.[cd_pickup_addr2]
      ,cc.[cd_pickup_addr3]
      ,cc.[cd_pickup_suburb]
      ,cc.[cd_pickup_postcode]
      ,cc.[cd_pickup_record_no]
	  ,CONCAT(cc.[cd_delivery_postcode],cc.[cd_delivery_suburb]) As DeliveryPostCodeKey
      ,cc.[cd_delivery_addr0]
      ,cc.[cd_delivery_addr1]
      ,cc.[cd_delivery_addr2]
      ,cc.[cd_delivery_addr3]
      ,cc.[cd_delivery_email]
      ,cc.[cd_delivery_suburb]
      ,cc.[cd_delivery_postcode]
      ,cc.[cd_delivery_record_no]
      ,cc.[cd_pickup_branch]
      ,cc.[cd_pickup_pay_branch]
      ,cc.[cd_deliver_branch]
      ,cc.[cd_deliver_pay_branch]
      ,cc.[cd_special_driver]
      ,cc.[cd_items]
      ,cc.[cd_deadweight]
      ,cc.[cd_volume]
      ,cc.[cd_activity_stamp]
      ,cc.[cd_activity_driver]
      ,cc.[cd_pickup_stamp]
      ,cc.[cd_pickup_driver]
      ,cc.[cd_pickup_pay_driver]
      ,cc.[cd_accept_stamp]
	   FROM [CpplEDI].[dbo].[consignment] cc  with (nolock) Where cd_date >='2021-01-01'
	    
GO
