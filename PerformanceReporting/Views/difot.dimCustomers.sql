SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [difot].[dimCustomers] AS
SELECT
RevenueType,	
Accountcode,	
ParentCompany,
CompanyName,
Warehouse,
TermDiscount,
Territory,
Postcode,
Locality,
LastSale
FROM 
dbo.dimcustomer WITH (NOLOCK) where RevenueType='EDI' and Accountcode NOT IN ('','0000') and Accountcode in (Select c.AccountCode from Consignment c WITH (NOLOCK)
	join PrimaryLabels a on a.ConsignmentID = c.GWConsignmentID							
	Where c.AddDateTime > '2018-01-01' 
	Group by c.AccountCode
	having Count(*) >1)
;
GO
