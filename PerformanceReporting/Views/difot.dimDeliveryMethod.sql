SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create View difot.dimDeliveryMethod As 
select 	distinct labelNumber,
        Consignmentcode,
        Category,   
        ISNULL(DeliveryMethod,'Un Known') AS DeliveryMethod	
from [PerformanceReporting].[dbo].DeliveryOptions (NOLOCK) ;
GO
