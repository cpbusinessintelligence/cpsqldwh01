SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [difot].[dimNetworkCategory] AS
SELECT
NetworkCategoryID,
[NetworkCategory],
[SecondaryNetworkCategory]
FROM dbo.NetworkCategory;
GO
