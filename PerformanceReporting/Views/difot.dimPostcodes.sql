SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW difot.dimPostcodes AS
SELECT 
PostCodeID,	CONCAT (PostCode,	Suburb) As PostCodeKey, PostCode, Suburb,	State,	PrizeZone,	ETAZone,	DepotCode,	SortCode,	DeliveryAgent,	DeliveryAgentRunCode,	AlternateDeliveryAgent,	AlternateDeliveryAgentRunCode,	RedeliveryFlag,	SatchelLinks,	RoadExpressPickupFlag,	RoadExpressDeliveryFlag,	RoadExpressCutOffTime,	CosmosCutLetter,	DomesticPriorityPickupFlag,	DomesticPriorityDeliveryFlag,	DomesticPriorityCutOffTime,	DomesticMidTierPickupFlag,	DomesticMidTierDeliveryFlag,	DomesticMidTierCutOffTime,	Internationl_PriorityPickUpFlag,	Internationl_PriorityDeliveryFlag,	Internationl_PriorityCutOffTime	,Internationl_SaverPickUpFlag,	Internationl_SaverDeliveryFlag,	Internationl_SaverCutOffTime,	Internationl_MidTierPickUpFlag	Internationl_MidTierDeliveryFlag
FROM PostCodes With (NOLOCK)
GO
