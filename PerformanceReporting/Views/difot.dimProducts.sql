SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW difot.dimProducts AS
SELECT 
CONCAT(RevenueType,	Code) As ProductKey,
RevenueType,
Code,
Category,	
RevenueCategory,
Description,
BusinessUnit,
StockStatus,
StockSalesType,
StockConversionFactor,
StockPricePer,
StockReplacementCost,
StkLastChange
FROM dbo.DimProduct;
GO
