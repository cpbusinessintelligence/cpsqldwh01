SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [difot].[factLabels] AS
SELECT
      cc.GWConsignmentID,
	  cc.ConNoteCreatedDate,
	  cc.CustomerConNoteDate,
	  cc.ItemCount,
      pl.PrimaryLabelsID,	
	  pl.ConsignmentID,
      CONCAT(pl.RevenueType,	pl.ServiceCode) As ProductKey,
      pl.SourceReference,	
	  pl.RevenueType,	
	  pl.AccountCode,	
	  pl.LabelNumber,	
	  pl.ProductType,	
	  pl.ServiceCode,	
	  pl.FirstScannedBy,	
	  pl.FirstDateTime,	
	  pl.FirstScanType,	
	  pl.PickupScannedBy,	
	  pl.PickupDateTime,	
	  pl.InDepotScannedBy,	
	  pl.InDepotDateTime,	
	  pl.HandoverScannedBy,	
	  pl.HandoverDateTime,	
	  pl.TransferScannedBy,	
	  pl.TransferDateTime,	
	  pl.ConsolidateScannedBy,	
	  pl.ConsolidateDateTime,	
	  pl.ConsolidatedBarcode,	
	  pl.DeConsolidateScannedBy,	
	  pl.DeConsolidateDateTime,	
	  pl.OutforDeliveryScannedBy,	
	  pl.OutForDeliverDateTime,	
	  pl.DelayofDeliveryScannedBy,	
	  pl.DelayofDeliveryScannedDateTime,	
	  pl.DelayofDeliveryDate,	
	  pl.QuerycageScannedBy,	
	  pl.QuerycageDateTime,	
	  pl.QueryCageNumber,	
	  pl.AttemptedDeliveryScannedBy,	
	  pl.AttemptedDeliveryDateTime,	
	  pl.AttemptedDeliveryReceivedDatetime,	
	  pl.AttemptedDeliveryCardNumber,	
	  pl.CardCategory,	
	  pl.AcceptedbyPOPPointStatus,	
	  pl.AcceptedbyPOPPointScannedBy,	
	  pl.AcceptedbyPOPPointScannedDateTime,	
	  pl.DeliveryOptionId,	DeliveryScannedBy,	
	  pl.DeliveryDateTime,	
	  pl.DeliveryReceivedDateTime,	
	  pl.IsPODPresent,	
	  pl.PODDateTime,	
	  pl.DeliveryContractorType,	
	  pl.DeliveryContractorName,	
	  pl.LastScannedBy,	
	  pl.LastDateTime,	
	  pl.LastScanType,	
	  pl.PickupETAZone,	
	  pl.DeliveryETAZone,	
	  pl.BUCode,	
	  pl.NetworkCategoryID,	
	  pl.ETADate,	
	  pl.OnTimeStatusId,	
	  pl.ETARuletoprocess,	
	  pl.Reasoncode,	
	  pl.PerformanceReportingDate,	
	  pl.PerformanceProcessed,	
	  pl.isExceptionapplied,	
	  pl.AddWho,	
	  pl.LabelCreatedDateTime,
	  pl.ProductSubCategory,	
	  pl.AttemptedDeliveryReason,
	  ---Below SQL is for Calculated Columns---
		Datediff(dd,Convert(date,cc.ConNoteCreatedDate),convert(date,pl.PickupDateTime)) AS 'Days Between Pick Up', 
		Datediff(dd,Convert(date,pl.PickupDateTime),convert(date,pl.DeliveryDateTime)) AS  'Days Between Pick UP and Delivery', 
		Datediff(dd,Convert(date,pl.OutForDeliverDateTime),convert(date,pl.DeliveryDateTime)) AS'Days from Out for Del to Delivery',
		Datediff(dd,Convert(date,cc.ConNoteCreatedDate),convert(date,pl.DeliveryDateTime)) AS 'Days Late' ,
		'DEL Scan'          = CASE WHEN pl.DeliveryDateTime IS NULL THEN 'True'    ELSE 'FALSE'  END,
		'DEL Status'        = CASE WHEN pl.DeliveryDateTime IS NULL THEN 'No Delivery Scan'  ELSE 'Delivery Scan'  END,
		'DEL Value'         = CASE WHEN pl.DeliveryDateTime IS NULL THEN 0 ELSE 1 END,	
		 Datediff(dd,Convert(date,pl.ETADate),convert(date,pl.DeliveryDateTime)) AS 'ETA to POD',
		 Datediff(dd,Convert(date,pl.ETADate),convert(date,pl.DeliveryDateTime)) AS 'ETA to Delivery Days',		 
		'OFD Scan'          = CASE WHEN pl.OutForDeliverDateTime IS NULL THEN 'TRUE'    ELSE 'FALSE'  END,
		'OFD Status'        = CASE WHEN pl.OutForDeliverDateTime IS NULL THEN 'No Onboard Scan'  ELSE 'Onboard Scan'  END,
		'OFD Value'         = CASE WHEN pl.OutForDeliverDateTime IS NULL THEN 0 ELSE 1 END,
		'Status Value'      = CASE  WHEN pl.OnTimeStatusId IN (1,4,6,13) THEN 'On Time'
		                            WHEN pl.OnTimeStatusId IN (2,5,7,14) THEN 'Not On Time'
								   ELSE 'NA'
								 END,		
		'On Time Value'     = CASE WHEN pl.OnTimeStatusId IN (1,4,6,13) THEN 'On Time' ELSE 'NA' END,
	    'Ontime Count'      = CASE WHEN pl.OnTimeStatusId IN (1,4,6,13) THEN 1 ELSE 0 END,
		'Not On Time Value' = CASE WHEN pl.OnTimeStatusId IN (2,5,7,14) THEN 'Not On Time' ELSE 'NA' END,
	    'Not On Time Count' = CASE WHEN pl.OnTimeStatusId IN (2,5,7,14) THEN 1 ELSE 0 END,
		'Pickup Scan'       = CASE WHEN pl.PickupDateTime IS NULL THEN 'True'    ELSE 'FALSE'  END,
		'Pickup Status'     = CASE WHEN pl.PickupDateTime IS NULL THEN 'No Pickup Scan'  ELSE 'Pickup Scan'  END,
		'Pickup Value'      = CASE WHEN pl.PickupDateTime IS NULL THEN 0 ELSE 1 END,
		CAST(pl.PickupDateTime as Time) 'Pick Up Time',
		CAST(pl.OutForDeliverDateTime AS Time) As 'OB Time'	,
        DATEDIFF(SECOND, 0, DATEADD(Day, 0 - DATEDIFF(Day, 0, CAST(pl.PickupDateTime as datetime)), CAST(pl.PickupDateTime as datetime))) as pickup_time_in_sec,
        DATEDIFF(SECOND, 0, DATEADD(Day, 0 - DATEDIFF(Day, 0, CAST(pl.OutForDeliverDateTime as datetime)), CAST(pl.OutForDeliverDateTime as datetime))) as ob_time_in_sec

	FROM [PerformanceReporting].[dbo].[consignment] cc WITH (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[PrimaryLabels] pl WITH (NOLOCK)
  ON cc.GWConsignmentID = pl.ConsignmentID
  Where ConNoteCreatedDate >= '2021-01-01'

  --'2020-08-01' 
  --and pl.accountcode in (Select TOP 25 c.AccountCode from Consignment c WITH (NOLOCK)
		--												join PrimaryLabels a on a.ConsignmentID = c.GWConsignmentID
		--												Where c.AddDateTime > '2019-01-01' 
		--												Group by c.AccountCode
		--												Order by Count(*) Desc)
GO
