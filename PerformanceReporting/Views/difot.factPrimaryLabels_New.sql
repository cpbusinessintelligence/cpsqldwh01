SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO









CREATE VIEW [difot].[factPrimaryLabels_New] AS
SELECT cc.[cd_id]
      ,cc.[cd_connote] 
      ,cc.[cd_date]
      ,cc.[cd_consignment_date]
      ,cc.[cd_eta_date]
      ,cc.[cd_eta_earliest]
      ,cc.[cd_customer_eta]
	  ,CONCAT(cc.[cd_pickup_postcode],cc.[cd_pickup_suburb]) AS PickupPostCodeKey
      ,cc.[cd_pickup_addr0]
      ,cc.[cd_pickup_addr1]
      ,cc.[cd_pickup_addr2]
      ,cc.[cd_pickup_addr3]
      ,cc.[cd_pickup_suburb]
      ,cc.[cd_pickup_postcode]
      ,cc.[cd_pickup_record_no]
	  ,CONCAT(cc.[cd_delivery_postcode],cc.[cd_delivery_suburb]) As DeliveryPostCodeKey
      ,cc.[cd_delivery_addr0]
      ,cc.[cd_delivery_addr1]
      ,cc.[cd_delivery_addr2]
      ,cc.[cd_delivery_addr3]
      ,cc.[cd_delivery_email]
      ,cc.[cd_delivery_suburb]
      ,cc.[cd_delivery_postcode]
      ,cc.[cd_delivery_record_no]
      ,cc.[cd_pickup_branch]
      ,cc.[cd_pickup_pay_branch]
      ,cc.[cd_deliver_branch]
      ,cc.[cd_deliver_pay_branch]
      ,cc.[cd_special_driver]
      ,cc.[cd_items]
      ,cc.[cd_deadweight]
      ,cc.[cd_volume]
      ,cc.[cd_activity_stamp]
      ,cc.[cd_activity_driver]
      ,cc.[cd_pickup_stamp]
      ,cc.[cd_pickup_driver]
      ,cc.[cd_pickup_pay_driver]
      ,cc.[cd_accept_stamp],
      pl.PrimaryLabelsID,	
	  pl.ConsignmentID,
      CONCAT(pl.RevenueType,	pl.ServiceCode) As ProductKey,
      pl.SourceReference,	
	  pl.RevenueType,	
	  pl.AccountCode,	
	  pl.LabelNumber,	
	  pl.ProductType,	
	  pl.ServiceCode,	
	  pl.FirstScannedBy,	
	  pl.FirstDateTime,	
	  pl.FirstScanType,	
	  pl.PickupScannedBy,	
	  pl.PickupDateTime,	
	  pl.InDepotScannedBy,	
	  pl.InDepotDateTime,	
	  pl.HandoverScannedBy,	
	  pl.HandoverDateTime,	
	  pl.TransferScannedBy,	
	  pl.TransferDateTime,	
	  pl.ConsolidateScannedBy,	
	  pl.ConsolidateDateTime,	
	  pl.ConsolidatedBarcode,	
	  pl.DeConsolidateScannedBy,	
	  pl.DeConsolidateDateTime,	
	  pl.OutforDeliveryScannedBy,	
	  pl.OutForDeliverDateTime,	
	  pl.DelayofDeliveryScannedBy,	
	  pl.DelayofDeliveryScannedDateTime,	
	  pl.DelayofDeliveryDate,	
	  pl.QuerycageScannedBy,	
	  pl.QuerycageDateTime,	
	  pl.QueryCageNumber,	
	  pl.AttemptedDeliveryScannedBy,	
	  pl.AttemptedDeliveryDateTime,	
	  pl.AttemptedDeliveryReceivedDatetime,	
	  pl.AttemptedDeliveryCardNumber,	
	  pl.CardCategory,	
	  pl.AcceptedbyPOPPointStatus,	
	  pl.AcceptedbyPOPPointScannedBy,	
	  pl.AcceptedbyPOPPointScannedDateTime,	
	  pl.DeliveryOptionId,	DeliveryScannedBy,	
	  pl.DeliveryDateTime,	
	  pl.DeliveryReceivedDateTime,	
	  pl.IsPODPresent,	
	  pl.PODDateTime,	
	  pl.DeliveryContractorType,	
	  pl.DeliveryContractorName,	
	  pl.LastScannedBy,	
	  pl.LastDateTime,	
	  pl.LastScanType,	
	  pl.PickupETAZone,	
	  pl.DeliveryETAZone,	
	  pl.BUCode,	
	  pl.NetworkCategoryID,	
	  pl.ETADate,	
	  pl.OnTimeStatusId,	
	  pl.ETARuletoprocess,	
	  pl.Reasoncode,	
	  pl.PerformanceReportingDate,	
	  pl.PerformanceProcessed,	
	  pl.isExceptionapplied,	
	  pl.AddWho,	
	  pl.LabelCreatedDateTime,
	  pl.ProductSubCategory,	
	  pl.AttemptedDeliveryReason,
	  ---Below SQL is for Calculated Columns---
		Datediff(dd,Convert(date,cc.cd_date),convert(date,pl.PickupDateTime)) AS 'Days Between Pick Up', 
		Datediff(dd,Convert(date,pl.PickupDateTime),convert(date,pl.DeliveryDateTime)) AS  'Days Between Pick UP and Delivery', 
		Datediff(dd,Convert(date,pl.OutForDeliverDateTime),convert(date,pl.DeliveryDateTime)) AS'Days from Out for Del to Delivery',
		Datediff(dd,Convert(date,cc.cd_date),convert(date,pl.DeliveryDateTime)) AS 'Days Late' ,
		'DEL Scan'          = CASE WHEN pl.DeliveryDateTime IS NULL THEN 'True'    ELSE 'FALSE'  END,
		'DEL Status'        = CASE WHEN pl.DeliveryDateTime IS NULL THEN 'No Delivery Scan'  ELSE 'Delivery Scan'  END,
		'DEL Value'         = CASE WHEN pl.DeliveryDateTime IS NULL THEN 0 ELSE 1 END,	
		 Datediff(dd,Convert(date,pl.ETADate),convert(date,pl.DeliveryDateTime)) AS 'ETA to POD',
		 Datediff(dd,Convert(date,pl.ETADate),convert(date,pl.DeliveryDateTime)) AS 'ETA to Delivery Days',
		'OFD Scan'          = CASE WHEN pl.OutForDeliverDateTime IS NULL THEN 'TRUE'    ELSE 'FALSE'  END,
		'OFD Status'        = CASE WHEN pl.OutForDeliverDateTime IS NULL THEN 'No Onboard Scan'  ELSE 'Onboard Scan'  END,
		'OFD Value'         = CASE WHEN pl.OutForDeliverDateTime IS NULL THEN 0 ELSE 1 END,
		'On Time Value'     = CASE WHEN pl.OnTimeStatusId  IN (1,4,6,10,13) THEN 'On Time' 
		                           WHEN pl.ETADate IS NULL THEN 'Cant Calculate'	
								   WHEN pl.PickupDateTime is null and pl.OutForDeliverDateTime is null and pl.AttemptedDeliveryDateTime is null and pl.DeliveryDateTime is null  THEN 'No Activity' 
								   ELSE 'Not On Time' 
							  END,
	    'Not On Time Count' = CASE WHEN pl.OnTimeStatusId IN (1,4,6,10,13) THEN 0 ELSE 1 END,
		'Ontime Count'      = CASE WHEN pl.OnTimeStatusId IN (1,4,6,10,13) Then 1 ELSE 0 End,
		'Pickup Scan'       = CASE WHEN pl.PickupDateTime IS NULL THEN 'True'    ELSE 'FALSE'  END,
		'Pickup Status'     = CASE WHEN pl.PickupDateTime IS NULL THEN 'No Pickup Scan'  ELSE 'Pickup Scan'  END,
		'Pickup Value'      = CASE WHEN pl.PickupDateTime IS NULL THEN 0 ELSE 1 END,
		CAST(pl.PickupDateTime as Time) 'Pick Up Time',
		CAST(pl.OutForDeliverDateTime AS Time) As 'OB Time'	,
        DATEDIFF(SECOND, 0, DATEADD(Day, 0 - DATEDIFF(Day, 0, CAST(pl.PickupDateTime as datetime)), CAST(pl.PickupDateTime as datetime))) as pickup_time_in_sec,
        DATEDIFF(SECOND, 0, DATEADD(Day, 0 - DATEDIFF(Day, 0, CAST(pl.OutForDeliverDateTime as datetime)), CAST(pl.OutForDeliverDateTime as datetime))) as ob_time_in_sec

		--'Status' = CASE WHEN CAST(pl.PickupDateTime as datetime) IS NULL AND CAST(pl.OutForDeliverDateTime as datetime) IS NULL AND CAST(pl.AttemptedDeliveryDateTime as datetime) IS NULL AND convert(date,pl.DeliveryDateTime) IS NULL THEN 'No Activity'
		--                WHEN pl.ETADate IS NULL THEN 'Cant Calculate'
		--				WHEN Datediff(day,pl.ETADate,Isnull(pl.AttemptedDeliveryDateTime,pl.DeliveryDateTime))<=0 and ETADate is not null THEN 'On Time'
		--				WHEN Datediff(day,pl.ETADate,Isnull(pl.AttemptedDeliveryDateTime,pl.DeliveryDateTime))>0 and ETADate is not null THEN 'Not On Time'
		--				WHEN Datediff(day,ETADate,GETDATE()) <=0 THEN 'On TIme' ELSE 'Not On Time' 
		--				END
	FROM [CpplEDI].[dbo].[consignment] cc WITH (NOLOCK) 
  JOIN [PerformanceReporting].[dbo].[PrimaryLabels] pl WITH (NOLOCK)
  ON cc.cd_id = pl.ConsignmentID
  Where [cd_date] >= '2020-07-01' 
  --and pl.accountcode in (Select TOP 25 c.AccountCode from Consignment c WITH (NOLOCK)
		--												join PrimaryLabels a on a.ConsignmentID = c.GWConsignmentID
		--												Where c.AddDateTime > '2019-01-01' 
		--												Group by c.AccountCode
		--												Order by Count(*) Desc)
GO
