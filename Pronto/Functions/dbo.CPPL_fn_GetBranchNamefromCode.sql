SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION [dbo].[CPPL_fn_GetBranchNamefromCode]
(
	@Code		char(5)
)
RETURNS varchar(15)
AS
BEGIN

	DECLARE @ret varchar(15);
	SELECT @ret = Null;

	SELECT @ret = CASE
	    WHEN @Code = 'CAD' THEN 'ADELAIDE'
	    WHEN @Code = 'CBN' THEN 'BRISBANE'
	    WHEN @Code = 'CCB' THEN 'CANBERRA'
	    WHEN @Code = 'CCC' THEN 'CENTRAL COAST'
	    WHEN @Code = 'CME' THEN 'MELBOURNE'
	    WHEN @Code = 'COO' THEN 'GOLD COAST'
	    WHEN @Code = 'CSY' THEN 'SYDNEY'
	    WHEN @Code = 'CPE' THEN 'PERTH'
	    WHEN @Code = 'CSC' THEN 'SUNSHINE COAST'
		ELSE @Code
	END

	RETURN @ret;
	
END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetBranchNamefromCode]
	TO [ReportUser]
GO
