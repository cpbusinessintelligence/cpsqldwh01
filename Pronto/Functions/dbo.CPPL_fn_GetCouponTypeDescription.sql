SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[CPPL_fn_GetCouponTypeDescription]
(
	@LabelPrefix					varchar(20)

)
RETURNS Varchar(100)
AS
BEGIN

	
	RETURN ISnull((SELECT  Top 1 StkDescription  FROM pRONTO.dbo.ProntoStockMaster Where StockCode = @LabelPrefix),'')

END

GO
