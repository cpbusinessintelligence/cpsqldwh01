SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


create FUNCTION [dbo].[CPPL_fn_GetStateNamefromCode]
(
	@Code		char(5)
)
RETURNS varchar(15)
AS
BEGIN

	DECLARE @ret varchar(15);
	SELECT @ret = Null;

	SELECT @ret = CASE
	    WHEN @Code = 'CAD' THEN 'SA'
	    WHEN @Code = 'CBN' THEN 'QLD'
	    WHEN @Code = 'CCB' THEN 'ACT'
	    WHEN @Code = 'CCC' THEN 'NSW'
	    WHEN @Code = 'CME' THEN 'VIC'
	    WHEN @Code = 'COO' THEN 'QLD'
	    WHEN @Code = 'CSY' THEN 'NSW'
	    WHEN @Code = 'CPE' THEN 'WA'
	    WHEN @Code = 'CSC' THEN 'QLD'
		ELSE @Code
	END

	RETURN @ret;
	
END
GO
