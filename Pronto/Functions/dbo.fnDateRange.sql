SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fnDateRange]
	(
		@StartDate datetime
		, @EndDate datetime
		, @MinuteIncrement int
		, @HourIncrement int
		, @DayIncrement int
		, @WeekIncrement int
		, @MonthIncrement int
	)

RETURNS @Range TABLE (

   DayID int IDENTITY(1, 1)
 , DateValue DATE

) As

BEGIN

     --'=====================================================================
    --' CP -Function -[dbo].[dbo].[fnDateRange]
    --' ---------------------------
    --' Purpose: Gives a particular date range of values-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/09/2014    AB      1.00                                                     --AB20140909

    --'=====================================================================

      SET @StartDate = DATEADD(hh, CASE WHEN @HourIncrement Is Not Null OR @MinuteIncrement Is Not Null THEN DATEPART(hh, @StartDate) ELSE 0 END

		, DATEADD(mi, CASE WHEN @MinuteIncrement Is Not Null THEN DATEPART(mi, @StartDate) ELSE 0 END
		, CONVERT(varchar(30), @StartDate, 101)))

      WHILE (@StartDate < @EndDate)

      BEGIN

            INSERT INTO @Range (DateValue) VALUES(@StartDate)

            SELECT
             @StartDate =   CASE WHEN @WeekIncrement   Is Not Null THEN DATEADD(wk, @WeekIncrement,   @StartDate) ELSE @StartDate END
             , @StartDate = CASE WHEN @MonthIncrement  Is Not Null THEN DATEADD(m,  @MonthIncrement,  @StartDate) ELSE @StartDate END
             , @StartDate = CASE WHEN @DayIncrement    Is Not Null THEN DATEADD(d,  @DayIncrement,    @StartDate) ELSE @StartDate END
             , @StartDate = CASE WHEN @HourIncrement   Is Not Null THEN DATEADD(hh, @HourIncrement,   @StartDate) ELSE @StartDate END
             , @StartDate = CASE WHEN @MinuteIncrement Is Not Null THEN DATEADD(mi, @MinuteIncrement, @StartDate) ELSE @StartDate END
					
     END

      RETURN
      
      /*
		-------------------------------------------------------
		DAYS --> MTD SAMPLE
		SELECT DayID, CAST(DateValue AS DATE) 
		FROM dbo.fnDateRange(CAST(YEAR(GETDATE()) AS VARCHAR(4)) + '-' + CAST(MONTH(GETDATE()) AS VARCHAR(4)) + '-01', GETDATE(), NULL, NULL, 1, NULL, NULL)
		-------------------------------------------------------
		
		-------------------------------------------------------
		DAYS --> LAST MONTH SAMPLE
		SELECT DayID, CAST(DateValue AS DATE) 
		FROM dbo.fnDateRange(
		CAST(CAST(YEAR(DATEADD(M, -1, GETDATE())) AS VARCHAR(4)) + '-' + CAST(MONTH(DATEADD(M, -1, GETDATE())) AS VARCHAR(4)) + '-01' AS DATETIME)
		, DATEADD(D, -1, CAST(CAST(YEAR(GETDATE()) AS VARCHAR(4)) + '-'	+ CAST(MONTH(GETDATE()) AS VARCHAR(4)) + '-01' AS DATETIME))
		, NULL, NULL, 1, NULL, NULL)
		-------------------------------------------------------

		-------------------------------------------------------
		1 minute between two days from now and now
		SELECT * FROM dbo.fnDateRange(DATEADD(d, -2, GETDATE()), GETDATE(), 1, NULL, NULL, NULL, NULL)
		-------------------------------------------------------

		
		-------------------------------------------------------
		SELECT DayID
			, CAST(DateValue AS DATE) AS [Date]
			, DATENAME(dw, DateValue) AS [Day]
			, DATENAME(m, DateValue) AS [Month]
			, RIGHT(CAST(YEAR(DateValue)AS VARCHAR(4)),2) + '-0' + CAST(DATENAME(qq, DateValue) AS VARCHAR(1)) AS [Quater]
		FROM dbo.fnDateRange(
			CAST(CAST(YEAR(GETDATE()) AS VARCHAR(4)) + '-01-01' AS DATETIME)
			, GETDATE()
			, NULL, NULL, 1, NULL, NULL)
		-------------------------------------------------------

      
      */

END


GO
