SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_GetMultipleValues](@str varchar(200)) returns @valuesinrows table(string varchar(200))
as
   BEGIN
     WHILE charindex(',',@str)<>0
     BEGIN
       insert into @valuesinrows select substring(@str,1,charindex(',',@str)-1)
       set @str=substring(@str,charindex(',',@str)+1,len(@str)-charindex(',',@str))
     END
     if charindex(',',@str)=0
     BEGIN
       insert into @valuesinrows values(@str)
     END
     RETURN
   END

GO
GRANT ALTER
	ON [dbo].[fn_GetMultipleValues]
	TO [ReportUser]
GO
GRANT CONTROL
	ON [dbo].[fn_GetMultipleValues]
	TO [ReportUser]
GO
GRANT SELECT
	ON [dbo].[fn_GetMultipleValues]
	TO [ReportUser]
GO
