SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PrefillSpace]
 (@Input varchar(20), @length int)
   returns char(9)
as
begin
       Declare @Strlen  Int
       Declare @Output varchar(25) = ''
       Select @Strlen = len(@Input)
       
       if @Strlen = @length
              Select @Output = @Input
          
       if  @Strlen > @length
              Select @Output =  Left(@Input,@length)  
       if @Strlen < @length
         Begin
           while Len(@Input) < @Length
           BEGIN 
             Select @input =' '+Ltrim( @Input)
             Select @Output = @input
           END
         End
		Return @Output
end
GO
