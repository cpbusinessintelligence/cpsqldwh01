SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_Split] 
( 
  @ItemList NVARCHAR(4000), 
  @delimiter CHAR(1) 
) 
  RETURNS @itemtable TABLE (Item NVARCHAR(50) ) 
 
AS 
 
BEGIN 
IF @delimiter IS NULL 
BEGIN  
set @delimiter = ',' 
END 
 
DECLARE @tempItemList NVARCHAR(4000) 
SET @tempItemList = @ItemList 
 
DECLARE @i INT 
DECLARE @Item NVARCHAR(4000) 
 
SET @tempItemList = REPLACE (@tempItemList, @delimiter + ' ', @delimiter) 
SET @i = CHARINDEX(@delimiter, @tempItemList) 
 
WHILE (LEN(@tempItemList) > 0) 
BEGIN 
IF @i = 0 
SET @Item = @tempItemList 
ELSE 
SET @Item = LEFT(@tempItemList, @i - 1) 
 
INSERT INTO @itemtable(Item) VALUES(@Item) 
 
IF @i = 0 
SET @tempItemList = '' 
ELSE 
SET @tempItemList = RIGHT(@tempItemList, LEN(@tempItemList) - @i) 
 
SET @i = CHARINDEX(@delimiter, @tempItemList) 
END 
RETURN 
END 
GO
