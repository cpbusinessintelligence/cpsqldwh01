SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create  PROC [dbo].[CPPL_BuildContractorRedemptionsReportData2_3102018_HB_Backup] 
(
	@StartDateOverride	date,
	@Debug				bit
)
AS
BEGIN

	SET NOCOUNT ON;

	SET DATEFIRST 1;

	DECLARE @StartDate date;
	DECLARE @EndDate date;
		
	IF @StartDateOverride Is Not Null
	BEGIN
		SELECT @StartDate = @StartDateOverride;
	END
	ELSE
	BEGIN
		--if no start date is provided, the default is to find the most recent Monday,
		--then go back two weeks from there
		--
	
		SELECT @StartDate = CAST((CONVERT(datetime, GETDATE()) - (DATEPART(DW, CONVERT(datetime, GETDATE())) - 1)) AS DATE);
		SELECT @StartDate = DATEADD(day, -14, @StartDate);
	END
	
	SELECT @EndDate = DATEADD(day, 6, @StartDate);

	IF @Debug = 1
	BEGIN
		PRINT 'Stored procedure starting: ' + CONVERT(varchar(20), GETDATE());
		PRINT 'Start date: ' + CONVERT(varchar(100), @StartDate);
		PRINT 'End date: ' + CONVERT(varchar(100), @EndDate);
	END

	-- if data already exists for the week that we're processing, just exit out
	IF ((EXISTS (SELECT 1 FROM CPPL_ContractorRctiPaymentsByCouponPrefix 
					WHERE RctiWeekEndingDate BETWEEN @StartDate AND @EndDate))
			OR
		 (EXISTS (SELECT 1 FROM FranchiseePaymentSummary 
					WHERE RctiWeekEndingDate BETWEEN @StartDate AND @EndDate)))
	BEGIN
		PRINT 'Redemption data already exists for week ending date: ' + CONVERT(varchar(30), @EndDate);
		RETURN 0;
	END

	--if the stock charges override table is empty, we'll throw an error
	--
	--(if it is empty, the table is likely in the middle of a reload from Pronto)
	--
	IF NOT EXISTS (SELECT TOP 1 'X' FROM ProntoStockChargesOverride)
	BEGIN
		RAISERROR ('ProntoStockChargesOverride table is empty.', 16, 1);
		RETURN 1;
	END

	--if the coupon details table is empty, we'll throw an error
	--
	--(if it is empty, the table is likely in the middle of a reload from Pronto)
	--
	IF NOT EXISTS (SELECT TOP 1 'X' FROM ProntoCouponDetails)
	BEGIN
		RAISERROR ('ProntoCouponDetails table is empty.', 16, 1);
		RETURN 1;
	END

	IF @Debug = 1
	BEGIN
		PRINT 'Insert allowance/deduction records: ' + CONVERT(varchar(20), GETDATE());
	END

	BEGIN TRY

		BEGIN TRAN;

		-- start summary rows with Development Incentive amounts and allowance/deduction totals
		INSERT INTO FranchiseePaymentSummary 
		(
			ContractorId,
			RunNumber,
			RunName,
			Branch,
			RctiWeekEndingDate,
			TotalDevelopmentIncentive,
			TotalPrepaidRctiAmount,
			TotalEdiRctiAmount,
			TotalRctiAllowances,
			TotalRctiDeductions,
			TotalRctiRedemptions
		)
		SELECT
			ISNULL(Contractor, 'UNKNOWN'),
			0,
			'',
			'',
			@EndDate,
			ISNULL(
				SUM(ISNULL(
					(CASE WHEN LTRIM(RTRIM(ISNULL(DeductionOrAllowance, ''))) IN ('A', 'D')
							--AND RIGHT(LTRIM(RTRIM(ISNULL(Code, ''))), 4) = '_INC'
							AND LTRIM(RTRIM(ISNULL(Code, ''))) LIKE '%_INC%'
						THEN ISNULL(Amount, 0.00)
						ELSE 0.00
					END), 0.00))
			, 0.00), --Development Incentive amount
			0.00, --Prepaid Rcti amount
			0.00, --Edi Rcti amount
			ISNULL(
				SUM(ISNULL(
					(CASE WHEN LTRIM(RTRIM(ISNULL(DeductionOrAllowance, ''))) = 'A'
						THEN ISNULL(Amount, 0.00)
						ELSE 0.00
					END), 0.00))
			, 0.00), --Total Rcti allowances
			ISNULL(
				SUM(ISNULL(
					(CASE WHEN LTRIM(RTRIM(ISNULL(DeductionOrAllowance, ''))) = 'D'
						THEN ISNULL(Amount, 0.00)
						ELSE 0.00
					END), 0.00))
			, 0.00), --Total Rcti deductions
			0.00 --Total Rcti Redemptions
		FROM ProntoContractorAllowDeduct ad (NOLOCK)
		WHERE AccountingDate BETWEEN @StartDate AND @EndDate 
		GROUP BY Contractor;
		--GROUP BY Contractor, LTRIM(RTRIM(ISNULL(DeductionOrAllowance, '')));

		IF @Debug = 1
		BEGIN
			PRINT 'Insert delivery redemption detail rows: ' + CONVERT(varchar(20), GETDATE());
		END;
		
		-- populate detail rows with delivery redemptions
		WITH DeliveryPayments AS
		(
			SELECT
				ISNULL(cd.DeliveryContractor, 'UNKNOWN') AS [ContractorId],
				0 AS [RunNumber],
				'' AS [RunName],
				'' AS [Branch],
				@EndDate AS [WeekEndingDate],
				convert(bit, case when ltrim(rtrim(ISNULL(ReturnSerialNumber, ''))) = ''
					then 0
					else 1
				end) AS [ReceiverPaysFlag],
				LEFT(LTRIM(rtrim(isnull(SerialNumber, ''))), 3) AS [CouponPrefix],
				--ISNULL(SUM(ISNULL(DeliveryRctiAmount, 0.00)), 0.00),
				ISNULL(DeliveryRctiAmount, 0.00) AS [DeliveryRctiAmount],
				ISNULL((
					SELECT TOP 1 ISNULL(sco.RedemptionAmount, 0.00)
					FROM ProntoStockChargesOverride sco (NOLOCK)
					WHERE sco.BranchCode = 
							CASE LEFT(LTRIM(RTRIM(ISNULL(cd.DeliveryContractor, ''))), 1)
								when 'M' then 'CME'
								when 'S' then 'CSY'
								when 'C' then 'CCB'
								when 'A' then 'CAD'
								when 'B' then 'CBN'
								when 'G' then 'COO'
								when 'P' then 'CPE'
								when 'W' then 'CPE'
							END
					AND
					(
						(LTRIM(RTRIM(ISNULL(sco.ContractorCode, ''))) = '')
					)
					AND 
					(
						(LTRIM(RTRIM(ISNULL(sco.CouponPrefix, ''))) = '')
						OR
						(LTRIM(RTRIM(ISNULL(sco.CouponPrefix, ''))) = LEFT(LTRIM(rtrim(isnull(cd.SerialNumber, ''))), 3))
					)
					AND
					(
						sco.EffectiveDate <= (DATEADD(day, 3, cd.DeliveryRctiDate))
					)
					AND LTRIM(RTRIM(ISNULL(sco.RedemptionType, ''))) = 'D'
					ORDER BY CouponPrefix DESC, EffectiveDate DESC
				), 0.00) AS [DeliveryStdAmount]
			from ProntoCouponDetails cd (nolock)
			where cd.DeliveryRctiDate between @StartDate and @EndDate 
			AND cd.DeliveryRctiDate Is Not Null
			AND ISNUMERIC(cd.SerialNumber) = 1
			AND LEN(LTRIM(rtrim(isnull(SerialNumber, '')))) = 11
			AND ISNULL(cd.DeliveryRctiAmount, 0.00) <> 0.00
		)
		INSERT INTO CPPL_ContractorRctiPaymentsByCouponPrefix 
		(
			ContractorId,
			RunNumber,
			RunName,
			Branch,
			RctiWeekEndingDate,
			IsReceiverPays,
			CouponPrefix,
			DeliveryRctiAmount,
			DeliveryStdAmount,
			DeliveryDifference,
			PickupRctiAmount,
			PickupStdAmount,
			PickupDifference,
			TotalRctiAmount,
			TotalStdAmount,
			TotalDifference 
		)
		SELECT
		ISNULL(ContractorId, 'UNKNOWN'), RunNumber, ISNULL(RunName, 'UNKNOWN'),
		ISNULL(Branch, 'UNKNOWN'), WeekEndingDate, ReceiverPaysFlag,
		CouponPrefix, SUM(isnull(DeliveryRctiAmount, 0.00)) AS [DeliveryRctiAmount],
		SUM(isnull(DeliveryStdAmount, 0.00)) AS [DeliveryStdAmount],
				0.00 AS [DeliveryDifference],
				0.00 AS [PickupRctiAmount],
				0.00 AS [PickupStdAmount],
				0.00 AS [PickupDifference],
				0.00 AS [TotalRctiAmount],
				0.00 AS [TotalStdAmount],
				0.00 AS [TotalDifference]
		FROM DeliveryPayments dp 
		GROUP BY ContractorId, WeekEndingDate, ReceiverPaysFlag, CouponPrefix,
			RunNumber, RunName, Branch;

		IF @Debug = 1
		BEGIN
			PRINT 'Update/add pickup redemption detail rows: ' + CONVERT(varchar(20), GETDATE());
		END;
		
		-- update/add detail rows with pickup redemptions
		WITH PickupPayments AS
		(
			SELECT
				ISNULL(cd.PickupContractor, 'UNKNOWN') AS [ContractorId],
				0 AS [RunNumber],
				'' AS [RunName],
				'' AS [Branch],
				@EndDate AS [WeekEndingDate],
				convert(bit, case when ltrim(rtrim(ISNULL(ReturnSerialNumber, ''))) = ''
					then 0
					else 1
				end) AS [ReceiverPaysFlag],
				LEFT(LTRIM(rtrim(isnull(SerialNumber, ''))), 3) AS [CouponPrefix],
				--ISNULL(SUM(ISNULL(DeliveryRctiAmount, 0.00)), 0.00),
				ISNULL(PickupRctiAmount, 0.00) AS [PickupRctiAmount],
				ISNULL((
					SELECT TOP 1 ISNULL(sco.RedemptionAmount, 0.00)
					FROM ProntoStockChargesOverride sco (NOLOCK)
					WHERE sco.BranchCode = 
							CASE LEFT(LTRIM(RTRIM(ISNULL(cd.PickupContractor, ''))), 1)
								when 'M' then 'CME'
								when 'S' then 'CSY'
								when 'C' then 'CCB'
								when 'A' then 'CAD'
								when 'B' then 'CBN'
								when 'G' then 'COO'
								when 'P' then 'CPE'
								when 'W' then 'CPE'
							END
					AND
					(
						(LTRIM(RTRIM(ISNULL(sco.ContractorCode, ''))) = '')
					)
					AND 
					(
						(LTRIM(RTRIM(ISNULL(sco.CouponPrefix, ''))) = '')
						OR
						(LTRIM(RTRIM(ISNULL(sco.CouponPrefix, ''))) = LEFT(LTRIM(rtrim(isnull(cd.SerialNumber, ''))), 3))
					)
					AND
					(
						sco.EffectiveDate <= (DATEADD(day, 3, cd.PickupRctiDate))
					)
					AND LTRIM(RTRIM(ISNULL(sco.RedemptionType, ''))) = 'P'
					ORDER BY CouponPrefix DESC, EffectiveDate DESC
				), 0.00) AS [PickupStdAmount]
			from ProntoCouponDetails cd (nolock)
			where cd.PickupRctiDate between @StartDate and @EndDate 
			AND cd.PickupRctiDate Is Not Null
			AND ISNUMERIC(cd.SerialNumber) = 1
			AND LEN(LTRIM(rtrim(isnull(SerialNumber, '')))) = 11
			AND ISNULL(cd.PickupRctiAmount, 0.00) <> 0.00
		)
		MERGE CPPL_ContractorRctiPaymentsByCouponPrefix AS Target
		USING 
		(
			SELECT
			ISNULL(ContractorId, 'UNKNOWN') AS [ContractorId], 
			RunNumber, ISNULL(RunName, 'UNKNOWN') AS [RunName],
			Branch, WeekEndingDate, ReceiverPaysFlag,
			CouponPrefix,
					0.00 AS [DeliveryRctiAmount],
					0.00 AS [DeliveryStdAmount],
					0.00 AS [DeliveryDifference],
			 SUM(isnull(PickupRctiAmount, 0.00)) AS [PickupRctiAmount],
			SUM(isnull(PickupStdAmount, 0.00)) AS [PickupStdAmount],
					0.00 AS [PickupDifference],
					0.00 AS [TotalRctiAmount],
					0.00 AS [TotalStdAmount],
					0.00 AS [TotalDifference]
			FROM PickupPayments pp 
			GROUP BY ContractorId, WeekEndingDate, ReceiverPaysFlag, CouponPrefix,
				RunNumber, RunName, Branch
		)
		AS Source
		ON Target.ContractorId = Source.ContractorId
		AND Target.RctiWeekEndingDate = Source.WeekEndingDate
		AND Target.IsReceiverPays = Source.ReceiverPaysFlag
		AND Target.CouponPrefix = Source.CouponPrefix
		WHEN
			NOT MATCHED BY TARGET THEN
				INSERT
				(
					ContractorId,
					RunNumber,
					RunName,
					Branch,
					RctiWeekEndingDate,
					IsReceiverPays,
					CouponPrefix,
					DeliveryRctiAmount,
					DeliveryStdAmount,
					DeliveryDifference,
					PickupRctiAmount,
					PickupStdAmount,
					PickupDifference,
					TotalRctiAmount,
					TotalStdAmount,
					TotalDifference
				)
				VALUES
				(
					Source.ContractorId,
					0,
					'',
					'',
					Source.WeekEndingDate,
					Source.ReceiverPaysFlag,
					Source.CouponPrefix,
					0.00,
					0.00,
					0.00,
					Source.PickupRctiAmount,
					Source.PickupStdAmount,
					(Source.PickupRctiAmount - Source.PickupStdAmount),
					0.00,
					0.00,
					0.00
				)
		WHEN
			MATCHED THEN
				UPDATE
					SET Target.PickupRctiAmount = Source.PickupRctiAmount,
						Target.PickupStdAmount = Source.PickupStdAmount,
						Target.PickupDifference = (Source.PickupRctiAmount - Source.PickupStdAmount);

		IF @Debug = 1
		BEGIN
			PRINT 'Update delivery Rcti differences: ' + CONVERT(varchar(20), GETDATE());
		END
						
		-- update the differences in Delivery Rcti vs Delivery Std
		UPDATE CPPL_ContractorRctiPaymentsByCouponPrefix 
		SET DeliveryDifference = (DeliveryRctiAmount - DeliveryStdAmount)
		WHERE RctiWeekEndingDate = @EndDate
		AND (DeliveryRctiAmount - DeliveryStdAmount) <> 0.00;

		IF @Debug = 1
		BEGIN
			PRINT 'Update detail row totals: ' + CONVERT(varchar(20), GETDATE());
		END
		
		-- update the totals for the detail rows
		UPDATE CPPL_ContractorRctiPaymentsByCouponPrefix 
		SET TotalRctiAmount = (ISNULL(DeliveryRctiAmount, 0.00) + ISNULL(PickupRctiAmount, 0.00)),
			TotalStdAmount = (ISNULL(DeliveryStdAmount, 0.00) + ISNULL(PickupStdAmount, 0.00)),
			TotalDifference = (ISNULL(DeliveryDifference, 0.00) + ISNULL(PickupDifference, 0.00))
		WHERE RctiWeekEndingDate = @EndDate;

		IF @Debug = 1
		BEGIN
			PRINT 'Update EDI Rcti amount (totals): ' + CONVERT(varchar(20), GETDATE());
		END;

		-- update EDI Rcti amounts
		WITH EdiRcti
		AS
		(
			SELECT 
			DISTINCT ISNULL(cd.DeliveryContractor, 'UNKNOWN') AS [ContractorId],
			ISNULL(SUM(ISNULL(cd.DeliveryRctiAmount, 0.00)), 0.00) AS [RctiAmount]
			FROM ProntoCouponDetails cd (Nolock)
			WHERE cd.DeliveryRctiDate Is Not Null
			AND cd.DeliveryRctiDate BETWEEN @StartDate AND @EndDate 
			AND (
					(LEN(LTRIM(RTRIM(ISNULL(cd.SerialNumber, '')))) = 11
						AND ISNUMERIC(ISNULL(cd.SerialNumber, '')) = 0
					)
					OR
					(LEN(LTRIM(RTRIM(ISNULL(cd.SerialNumber, '')))) != 11)
			)
			GROUP BY cd.DeliveryContractor
			UNION ALL
			SELECT 
			DISTINCT ISNULL(cd.PickupContractor, 'UNKNOWN') AS [ContractorId],
			ISNULL(SUM(ISNULL(cd.PickupRctiAmount, 0.00)), 0.00) AS [RctiAmount]
			FROM ProntoCouponDetails cd (Nolock)
			WHERE cd.PickupRctiDate Is Not Null
			AND cd.PickupRctiDate BETWEEN @StartDate AND @EndDate 
			AND (
					(LEN(LTRIM(RTRIM(ISNULL(cd.SerialNumber, '')))) = 11
						AND ISNUMERIC(ISNULL(cd.SerialNumber, '')) = 0
					)
					OR
					(LEN(LTRIM(RTRIM(ISNULL(cd.SerialNumber, '')))) != 11)
			)
			GROUP BY cd.PickupContractor
		)
		MERGE FranchiseePaymentSummary AS Target
		USING
		(
			SELECT
				DISTINCT ISNULL(ContractorId, 'UNKNOWN') AS [ContractorId],
				ISNULL(SUM(ISNULL(RctiAmount, 0.00)), 0.00) AS EdiRctiAmount
			FROM EdiRcti 
			GROUP BY ContractorId
		)
		AS Source
		ON Target.ContractorId = Source.ContractorId
		AND Target.RctiWeekEndingDate = @EndDate
		WHEN
			NOT MATCHED BY TARGET THEN
				INSERT
				(
					ContractorId,
					RunNumber,
					RunName,
					Branch,
					RctiWeekEndingDate,
					TotalDevelopmentIncentive,
					TotalPrepaidRctiAmount,
					TotalEdiRctiAmount,
					TotalRctiAllowances,
					TotalRctiDeductions,
					TotalRctiRedemptions
				)
				VALUES
				(
					Source.ContractorId,
					0,
					'',
					'',
					@EndDate,
					0.00,
					0.00,
					Source.EdiRctiAmount,
					0.00,
					0.00,
					0.00
				)
		WHEN
			MATCHED THEN
				UPDATE
					SET Target.TotalEdiRctiAmount = Source.EdiRctiAmount;

		IF @Debug = 1
		BEGIN
			PRINT 'Update pre-paid Rcti amount (totals): ' + CONVERT(varchar(20), GETDATE());
		END;

		MERGE FranchiseePaymentSummary AS Target
		USING
		(
			SELECT
				DISTINCT
				ISNULL(ContractorId, 'UNKNOWN') AS [ContractorId],
				ISNULL(SUM(ISNULL(TotalRctiAmount, 0.00)), 0.00) AS PrepaidRctiAmount
			FROM CPPL_ContractorRctiPaymentsByCouponPrefix cp (NOLOCK)
			WHERE RctiWeekEndingDate = @EndDate
			GROUP BY ContractorId
		)
		AS Source
		ON Target.ContractorId = Source.ContractorId
		AND Target.RctiWeekEndingDate = @EndDate
		WHEN
			NOT MATCHED BY TARGET THEN
				INSERT
				(
					ContractorId,
					RunNumber,
					RunName,
					Branch,
					RctiWeekEndingDate,
					TotalDevelopmentIncentive,
					TotalPrepaidRctiAmount,
					TotalEdiRctiAmount,
					TotalRctiAllowances,
					TotalRctiDeductions,
					TotalRctiRedemptions
				)
				VALUES
				(
					Source.ContractorId,
					0,
					'',
					'',
					@EndDate,
					0.00,
					Source.PrepaidRctiAmount,
					0.00,
					0.00,
					0.00,
					0.00
				)
		WHEN
			MATCHED THEN
				UPDATE
					SET Target.TotalPrepaidRctiAmount = Source.PrepaidRctiAmount;

		IF @Debug = 1
		BEGIN
			PRINT 'Update overall totals: ' + CONVERT(varchar(20), GETDATE());
		END;
		
		UPDATE FranchiseePaymentSummary 
		SET TotalRctiRedemptions = (ISNULL(TotalPrepaidRctiAmount, 0.00) + ISNULL(TotalEdiRctiAmount, 0.00))
		WHERE RctiWeekEndingDate = @EndDate;

		IF @Debug = 1
		BEGIN
			PRINT 'Update run numbers and names (totals): ' + CONVERT(varchar(20), GETDATE());
		END;
		
		-- update driver names and numbers
		WITH ContractorDriver AS
		(
			select 
			pt.ContractorId,
			(
				select TOP 1 d.Id
				FROM Cosmos.dbo.Driver d (NOLOCK)
				WHERE
					d.ProntoId = pt.ContractorId 
					AND d.EffectiveDate < pt.RctiWeekEndingDate 
					AND ISNULL(d.DriverNumber, 9999) < 8000
				ORDER BY d.EffectiveDate DESC
			) AS [DriverId]
			 from FranchiseePaymentSummary pt (nolock)
			 WHERE ISNULL(pt.RunNumber, 0) = 0
		)
		UPDATE FranchiseePaymentSummary 
		SET 
			Branch = UPPER(d2.Branch),
			RunNumber = d2.DriverNumber,
			RunName = UPPER(d2.DriverName)
			--SELECT
			--	pt2.ContractorId,
			--	d2.Branch,
			--	d2.DriverNumber,
			--	d2.DriverName 
		FROM 
		FranchiseePaymentSummary pt2
		JOIN ContractorDriver cd
		ON pt2.ContractorId = cd.ContractorId 
		JOIN Cosmos.dbo.Driver d2 (NOLOCK)
		ON d2.Id = cd.DriverId 
		WHERE ISNULL(pt2.RunNumber, 0) = 0;
		--AND pt2.RctiWeekEndingDate = @EndDate;

		IF @Debug = 1
		BEGIN
			PRINT 'Update run numbers and names (detail): ' + CONVERT(varchar(20), GETDATE());
		END;

		WITH ContractorDriver AS
		(
			select 
			cp.ContractorId,
			cp.CouponPrefix,
			cp.IsReceiverPays,
			cp.RctiWeekEndingDate,
			(
				select TOP 1 d.Id
				FROM Cosmos.dbo.Driver d (NOLOCK)
				WHERE
					d.ProntoId = cp.ContractorId 
					AND d.EffectiveDate < cp.RctiWeekEndingDate 
					AND
					((ISNULL(d.DriverNumber, 9999) < 8000)
						OR (ISNULL(d.DriverNumber, 9999) > 8999))
				ORDER BY d.EffectiveDate DESC
			) AS [DriverId]
			 from CPPL_ContractorRctiPaymentsByCouponPrefix cp (nolock)
			 WHERE ISNULL(cp.RunNumber, 0) = 0
		)
		UPDATE CPPL_ContractorRctiPaymentsByCouponPrefix  
		SET 
			Branch = UPPER(d2.Branch),
			RunNumber = d2.DriverNumber,
			RunName = UPPER(d2.DriverName)
			--SELECT
			--	cp2.ContractorId,
			--	d2.Branch,
			--	d2.DriverNumber,
			--	d2.DriverName 
		FROM 
		CPPL_ContractorRctiPaymentsByCouponPrefix cp2
		JOIN ContractorDriver cd
		ON cp2.ContractorId = cd.ContractorId 
		AND cp2.CouponPrefix = cd.CouponPrefix 
		AND cp2.IsReceiverPays = cd.IsReceiverPays 
		AND cp2.RctiWeekEndingDate = cd.RctiWeekEndingDate 
		JOIN Cosmos.dbo.Driver d2 (NOLOCK)
		ON d2.Id = cd.DriverId 
		WHERE ISNULL(cp2.RunNumber, 0) = 0;
		--AND pt2.RctiWeekEndingDate = @EndDate;

		--here we update any stray records where a matching driver record wasn't found,
		-- or where Brisbane/Gold Coast drivers got an incorrect branch from Cosmos
		UPDATE CPPL_ContractorRctiPaymentsByCouponPrefix 
		SET Branch = CASE LEFT(ISNULL(ContractorId, ' '), 1)
			WHEN 'B' THEN 'BRISBANE'
			WHEN 'C' THEN 'SYDNEY'
			WHEN 'A' THEN 'ADELAIDE'
			WHEN 'M' THEN 'MELBOURNE'
			WHEN 'G' THEN 'GOLDCOAST'
			WHEN 'O' THEN 'GOLDCOAST'
			WHEN 'S' THEN 'SYDNEY'
			when 'W' then 'PERTH'
			ELSE ''
		END
		WHERE LTRIM(RTRIM(ISNULL(Branch, ''))) = ''
		OR (LTRIM(RTRIM(ISNULL(Branch, ''))) = 'brisbane' AND LEFT(ISNULL(ContractorId, ' '), 1) = 'G')
		OR (LTRIM(RTRIM(ISNULL(Branch, ''))) = 'goldcoast' AND LEFT(ISNULL(ContractorId, ' '), 1) = 'B');

		UPDATE FranchiseePaymentSummary 
		SET Branch = CASE LEFT(ISNULL(ContractorId, ' '), 1)
			WHEN 'B' THEN 'BRISBANE'
			WHEN 'C' THEN 'SYDNEY'
			WHEN 'A' THEN 'ADELAIDE'
			WHEN 'M' THEN 'MELBOURNE'
			WHEN 'G' THEN 'GOLDCOAST'
			WHEN 'O' THEN 'GOLDCOAST'
			WHEN 'S' THEN 'SYDNEY'
			when 'W' then 'PERTH'
			ELSE ''
		END
		WHERE LTRIM(RTRIM(ISNULL(Branch, ''))) = ''
		OR (LTRIM(RTRIM(ISNULL(Branch, ''))) = 'brisbane' AND LEFT(ISNULL(ContractorId, ' '), 1) = 'G')
		OR (LTRIM(RTRIM(ISNULL(Branch, ''))) = 'goldcoast' AND LEFT(ISNULL(ContractorId, ' '), 1) = 'B');

		WHILE @@TRANCOUNT > 0
		BEGIN
			COMMIT WORK;
		END

	END TRY
	BEGIN CATCH
	
		WHILE @@TRANCOUNT > 0
		BEGIN
			ROLLBACK WORK;
		END
		
		EXEC dbo.PDG_LogRethrowError @RethrowError = 1, @LogError = 1;
	
	END CATCH
	
	IF @Debug = 1
	BEGIN
		PRINT 'Stored procedure finished: ' + CONVERT(varchar(20), GETDATE());
	END;
	
	SET NOCOUNT OFF;

END

GO
