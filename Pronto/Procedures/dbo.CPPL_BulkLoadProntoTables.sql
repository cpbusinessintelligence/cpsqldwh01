SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CPPL_BulkLoadProntoTables]
AS
 
       --'=====================================================================
    --' CP -Stored Procedure -[CPPL_BulkLoadProntoTables]
    --' ---------------------------
    --' Purpose: Load the bulk tables-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/09/2014    AB      1.00    Created the procedure                             --AB20140905

    --'=====================================================================

 BEGIN

 


	-----------------------
   --Load Serial Link Table
   -----------------------

    Truncate Table [Load_ProntoStockSerialLink];
   
    BULK INSERT [Load_ProntoStockSerialLink] FROM 'E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Process\cl1_stock_serial_link_txt.dif' 
    WITH (FIELDTERMINATOR = '|', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\PRONTO_SerialLink_insert_errors.txt');
	EXEC Pronto.dbo.cppl_LoadProntoStockSerialLink;
    Truncate Table [Load_ProntoStockSerialLink];


   --  -----------------------
   --  --Load SalesOrder Table
   --  -----------------------
    Truncate Table  [Load_ProntoSalesOrder];
    
    BULK INSERT [Load_ProntoSalesOrder] FROM 'E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Process\dw_sales.dif' 
    WITH (FIELDTERMINATOR = '|', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\PRONTO_SalesOrder_insert_errors.txt'); 
     
    EXEC Pronto.dbo.cppl_LoadProntoSalesOrder
    Truncate Table  [Load_ProntoSalesOrder];  
      
      -----------------------
     --Load SalesOrder Lines Table
     -----------------------
    Truncate Table  [Load_ProntoSalesOrderLines];  
    
    BULK INSERT [Load_ProntoSalesOrderLines] FROM 'E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Process\dw_sales_order_lines_txt.dif' 
    WITH (FIELDTERMINATOR = '|', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\PRONTO_SalesOrderLines_insert_errors.txt');     
   
    EXEC Pronto.dbo.cppl_LoadProntoSalesOrderLines
    Truncate Table  [Load_ProntoSalesOrderLines]; 
     
     -----------------------
     --Load Stock Master Table
     -----------------------
    Truncate Table  [Load_ProntoStockMaster];  
    
    BULK INSERT [Load_ProntoStockMaster] FROM 'E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Process\cl1_stock_master_txt.dif' 
    WITH (FIELDTERMINATOR = '|', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\PRONTO_StockMaster_insert_errors.txt');   
    
   EXEC Pronto.dbo.cppl_LoadProntoStockMaster
   Truncate Table  [Load_ProntoStockMaster];  


        -----------------------
     --Load Debtor Ageing Table
     -----------------------
    Truncate Table  [ProntoDebtorAgeing];  
    
    BULK INSERT [ProntoDebtorAgeing] FROM 'E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Process\bi_deb_fact.dif' 
    WITH (FIELDTERMINATOR = '|', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\bi_deb_fact.txt');   


END

GO
