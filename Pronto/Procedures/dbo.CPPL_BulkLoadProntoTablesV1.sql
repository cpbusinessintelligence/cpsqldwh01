SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CPPL_BulkLoadProntoTablesV1]
AS
 
       --'=====================================================================
    --' CP -Stored Procedure -[CPPL_BulkLoadProntoTables]
    --' ---------------------------
    --' Purpose: Load the bulk tables-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 16 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 16/01/2017    AB      1.00    Created the procedure                             --AB20170116

    --'=====================================================================

 BEGIN

 


	-----------------------
   --Load Pronto Creditor Table
   -----------------------

   
  Truncate Table [Load_ProntoCreditorV1];
   
    BULK INSERT [Load_ProntoCreditorV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\bi_cre_dim.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999,ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\bi_cre_dim.txt');
	
	EXEC Pronto.dbo.cppl_LoadProntoCreditorV1;
    Truncate Table [Load_ProntoCreditorV1];


	-----------------------
   --Load Pronto Debtor Table
   -----------------------

   
  Truncate Table [Load_ProntoDebtorV1];
   
    BULK INSERT [Load_ProntoDebtorV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\bi_deb_dim.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999,ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\bi_deb_dim.txt');
	
	EXEC Pronto.dbo.cppl_LoadProntoDebtorV1;
    Truncate Table [Load_ProntoDebtorV1];


	 --Load ProntoContractorAllowDeduct Table
     -----------------------
    Truncate Table  [Load_ProntoContractorAllowDeductV1];  
    
    BULK INSERT [Load_ProntoContractorAllowDeductV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\dw_contractor_allow_deduct.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Daily Pronto BI\Daily Pronto BI Extracts\Error\dw_contractor_allow_deduct.txt');     
   
    EXEC Pronto.dbo.[cppl_LoadProntoContractorAllowDeductV1]
    Truncate Table  [Load_ProntoContractorAllowDeductV1]; 


	
-----------------------
     --Load SalesOrder  Table
     -----------------------
    Truncate Table  [Load_ProntoSalesOrderV1];  
    
    BULK INSERT [Load_ProntoSalesOrderV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\dw_sales.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Download Pronto Data\Error\dw_sales.txt');     
   
    EXEC Pronto.[dbo].[cppl_LoadProntoSalesOrderV1]
    Truncate Table  [Load_ProntoSalesOrderV1]; 


      -----------------------
     --Load SalesOrder Lines Table
     -----------------------
    Truncate Table  [Load_ProntoSalesOrderLinesV1];  
    
    BULK INSERT [Load_ProntoSalesOrderLinesV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\dw_sales_order_lines.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Download Pronto Data\Error\dw_sales_order_lines.txt');     
   
    EXEC Pronto.dbo.cppl_LoadProntoSalesOrderLinesV1
    Truncate Table  [Load_ProntoSalesOrderLinesV1]; 


	      -----------------------
     --Load Stock Override charges Table
     -----------------------
    Truncate Table  [dbo].[Load_ProntoStockChargesOverrideV1]
    
    BULK INSERT [Load_ProntoStockChargesOverrideV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\zstock_contractor_charges.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Download Pronto Data\Error\zstock_contractor_charges.txt');     
   
    EXEC Pronto.[dbo].[cppl_LoadProntoStockChargesOverrideV1]
    Truncate Table  [Load_ProntoStockChargesOverrideV1]; 
     

     -----------------------
     --Load Stock Master Table
     -----------------------
    Truncate Table  [Load_ProntoStockMasterV1];  
    
    BULK INSERT [Load_ProntoStockMasterV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\stock_master.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Download Pronto Data\Error\stock_master.txt');   
    
   EXEC Pronto.dbo.[cppl_LoadProntoStockMasterV1]
   Truncate Table  [Load_ProntoStockMasterV1];  


   	-----------------------
   --Load Serial Link Table
   -----------------------

    Truncate Table [Load_ProntoStockSerialLinkV1];
   
    BULK INSERT [Load_ProntoStockSerialLinkV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\stock_serial_link.dif'
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Download Pronto Data\Error\stock_serial_link.txt');
	EXEC Pronto.dbo.cppl_LoadProntoStockSerialLinkV1;
    Truncate Table [Load_ProntoStockSerialLinkV1];


	 	-----------------------
   --Load Serial Number Table
   -----------------------

    Truncate Table [ProntoStockSerialNumberV1];
   
    BULK INSERT [ProntoStockSerialNumberV1] FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\stock_serial_number.dif'
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Download Pronto Data\Error\stock_serial_number.txt');


        -----------------------
     --Load Debtor Ageing Table
     -----------------------
    Truncate Table  [dbo].[Load_ProntoDebtorAgeingV1];  
    
    BULK INSERT [dbo].[Load_ProntoDebtorAgeingV1]  FROM 'E:\ETL Processing\Download Pronto Data\New Extracts_Nov16\bi_deb_fact.dif' 
    WITH (FIELDTERMINATOR = '', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=9999, ERRORFILE='E:\ETL Processing\Download Pronto Data\Error\bi_deb_fact.txt');
		EXEC Pronto.[dbo].[sp_LoadProntoDebtorAgeingV1]
    Truncate Table [Load_ProntoDebtorAgeingV1];

END


GO
