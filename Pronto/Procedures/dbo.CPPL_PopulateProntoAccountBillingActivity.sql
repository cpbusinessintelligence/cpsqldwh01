SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_PopulateProntoAccountBillingActivity]
/*(
	@Debug	bit
)*/
AS
BEGIN

	SET NOCOUNT ON;
	
	-- populate new account codes that haven't had billing activity before
	INSERT INTO ProntoAccountBillingActivity 
	(AccountCode, FirstBillingWeek)
	SELECT DISTINCT pb.AccountCode, MIN(BillingDate)
	FROM ProntoBilling pb (NOLOCK)
	WHERE CONVERT(int, pb.AccountingWeek) > 201139
	AND NOT EXISTS 
	(
		SELECT 1 
		FROM ProntoAccountBillingActivity ba
		WHERE ba.AccountCode = pb.AccountCode
	)
	GROUP BY pb.AccountCode;

	-- this variable is used throughout the proc
	DECLARE @acc varchar(30);
	SELECT @acc = Null;
	
	-- update first billing week for customers that haven't had the value set before
	--
	DECLARE @wk date;

	DECLARE curBA1 CURSOR READ_ONLY STATIC FOR
	SELECT DISTINCT AccountCode
	FROM ProntoAccountBillingActivity
	WHERE FirstBillingWeek Is Null;

	OPEN curBA1;

	FETCH NEXT FROM curBA1 INTO @acc;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @wk = Null;
		
		SELECT @wk = MIN(BillingDate)
		FROM ProntoBilling (NOLOCK)
		WHERE AccountCode = @acc
		AND CONVERT(int, AccountingWeek) > 201139;
		
		IF @wk Is Not Null
		BEGIN
			UPDATE ProntoAccountBillingActivity 
			SET FirstBillingWeek = @wk
			WHERE AccountCode = @acc;
		END

		FETCH NEXT FROM curBA1 INTO @acc;
	END

	CLOSE curBA1;
	DEALLOCATE curBA1;
	-- end of first billing week update

	SELECT @acc = Null;
	
	DECLARE @bill varchar(30);
	DECLARE @par varchar(30);

	-- only update for accounts that have had some billing within the last 3 weeks
	-- or where the bill-to code hasn't previously been updated
	--
	DECLARE curBA2 CURSOR READ_ONLY STATIC FOR
	SELECT DISTINCT AccountCode,
		BillToAccountCode
	FROM ProntoAccountBillingActivity
	WHERE LastBillingWeek >= CONVERT(date, DATEADD(week, -3, GETDATE()))
	OR BillToAccountCode Is Null;

	OPEN curBA2;

	FETCH NEXT FROM curBA2 INTO @acc, @bill;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @par = Null;
		
		SELECT TOP 1 @par = AccountBillToCode 
		FROM ProntoBilling (NOLOCK)
		WHERE AccountCode = @acc
		AND CONVERT(int, AccountingWeek) > 201139
		ORDER BY AccountingWeek DESC;
		
		IF ((@par Is Not Null) AND (ISNULL(@par, '') != ISNULL(@bill, '')))
		BEGIN
			UPDATE ProntoAccountBillingActivity 
			SET BillToAccountCode = @par 
			WHERE AccountCode = @acc;
		END

		FETCH NEXT FROM curBA2 INTO @acc, @bill;
	END

	CLOSE curBA2;
	DEALLOCATE curBA2;
	-- end of bill-to code update

	SELECT @acc = Null;

	-- update the last billing week as required
	--
	DECLARE @lastwk date;
	DECLARE @nextwk date;

	DECLARE curBA3 CURSOR READ_ONLY STATIC FOR
	SELECT DISTINCT ba.AccountCode, LastBillingWeek
	FROM ProntoAccountBillingActivity ba
	WHERE ba.LastBillingWeek Is Null
	OR EXISTS 
	(
		SELECT TOP 1 'X' FROM
		ProntoBilling pb (NOLOCK)
		WHERE pb.AccountCode = ba.AccountCode 
		AND pb.BillingDate > ISNULL(ba.LastBillingWeek, '1jan1900')
	);

	OPEN curBA3;

	FETCH NEXT FROM curBA3 INTO @acc, @lastwk;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @wk = Null;
		
		SELECT @nextwk = MAX(BillingDate)
		FROM ProntoBilling (NOLOCK)
		WHERE AccountCode = @acc
		AND CONVERT(int, AccountingWeek) > 201139;
		
		IF ((@nextwk Is Not Null) AND (ISNULL(@nextwk, '1jan1900') > ISNULL(@lastwk, '1jan1900')))
		BEGIN
			UPDATE ProntoAccountBillingActivity 
			SET LastBillingWeek = @nextwk
			WHERE AccountCode = @acc;
		END

		FETCH NEXT FROM curBA3 INTO @acc, @lastwk;
	END

	CLOSE curBA3;
	DEALLOCATE curBA3;
	-- end of last billing week update

	SELECT @acc = Null;
	
	-- update last pickup details (branch, driver) for accounts that have had
	-- some billing within the past 2 weeks
	--
	DECLARE @vBranch varchar(30), @vContractor varchar(30), @vDriver varchar(20);
	DECLARE @vId uniqueidentifier;
	DECLARE @counter int;
	DECLARE @cmd varchar(2000);

	SELECT @counter = 0;

	DECLARE curAcc CURSOR READ_ONLY STATIC FOR
	SELECT
		DISTINCT AccountCode
	FROM ProntoAccountBillingActivity
	WHERE LastBillingWeek >= CONVERT(date, DATEADD(week, -2, GETDATE()));

	OPEN curAcc;

	FETCH NEXT FROM curAcc INTO @acc;

	WHILE @@FETCH_STATUS = 0 AND @counter < 1000
	BEGIN
		SELECT @counter = @counter + 1;
		SELECT @vId = NEWID();
		
		-- we get 500 of the most recent consignments to check for pickup events
		--
		/* old statement was using linked server
		--
		SELECT @cmd = 'INSERT INTO ODS.dbo.[ConsignmentsTemp] WITH (TABLOCK)
							--(Id, ConsignmentNumber)
							SELECT
								TOP 500
									''' + CONVERT(varchar(50), @vId) + ''', ConsignmentReference
							FROM [RDS].[RDS].[dbo].[ProntoBilling] WITH (NOLOCK)
							WHERE AccountCode = ''' + @acc + '''
							AND AccountingDate >= ''' + CONVERT(varchar(30), CONVERT(date, DATEADD(week, -2, GETDATE())), 106) + ''' 
							ORDER BY CONVERT(int, AccountingWeek) DESC;';
		
		EXEC (@cmd) AT [ODS];
		
		SELECT @vBranch = Null;
		SELECT @vContractor = Null;
		SELECT @vDriver = Null;

		EXEC [ODS].[ODS].[dbo].[CPPL_GetLatestDriverDetailsForConsignmentsTemp]
										@Id = @vId, @Branch = @vBranch OUTPUT, @Contractor = @vContractor OUTPUT, @Driver = @vDriver OUTPUT;
		*/

		SELECT @cmd = 'INSERT INTO ODS.dbo.[ConsignmentsTemp] WITH (TABLOCK)
							--(Id, ConsignmentNumber)
							SELECT
								TOP 500
									''' + CONVERT(varchar(50), @vId) + ''', ConsignmentReference
							FROM [RDS].[dbo].[ProntoBilling] WITH (NOLOCK)
							WHERE AccountCode = ''' + @acc + '''
							AND AccountingDate >= ''' + CONVERT(varchar(30), CONVERT(date, DATEADD(week, -2, GETDATE())), 106) + ''' 
							ORDER BY CONVERT(int, AccountingWeek) DESC;';
		
		EXEC (@cmd);
		
		SELECT @vBranch = Null;
		SELECT @vContractor = Null;
		SELECT @vDriver = Null;

		EXEC [ODS].[dbo].[CPPL_GetLatestDriverDetailsForConsignmentsTemp]
										@Id = @vId, @Branch = @vBranch OUTPUT, @Contractor = @vContractor OUTPUT, @Driver = @vDriver OUTPUT;
										
		IF ((@vBranch Is Not Null) AND (@vContractor Is Not Null) AND (@vDriver Is Not Null))
		BEGIN
			UPDATE ProntoAccountBillingActivity 
			SET LastConsignmentPickupBranch = @vBranch,
				LastConsignmentPickupContractor = @vContractor,
				LastConsignmentPickupDriver = @vDriver
			WHERE AccountCode = @acc;
		END

		--SELECT @cmd = 'DELETE FROM [ODS].[dbo].[ConsignmentsTemp] WHERE Id = ''' + CONVERT(varchar(50), @vId) + ''';';
		SELECT @cmd = 'TRUNCATE TABLE [ODS].[dbo].[ConsignmentsTemp];';
		EXEC (@cmd);

		FETCH NEXT FROM curAcc INTO @acc;
	END

	CLOSE curAcc;
	DEALLOCATE curAcc;
	-- end of pickup details update

	SET NOCOUNT OFF;
	RETURN 0;

END
GO
