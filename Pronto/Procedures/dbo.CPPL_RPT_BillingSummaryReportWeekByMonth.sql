SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_BillingSummaryReportWeekByMonth]
(
	@AccountCodeFilter		varchar(20),
	@AccountNameFilter		varchar(40),
	@CompanyCode			varchar(10),
	@StartDate				date,
	@EndDate				date,
	@RevenueBusinessUnit	varchar(1000),
	@IncludeChildren		bit,
	@FirstDayOfWeek			char(3),
	@GroupByState			bit = 0
)
AS
BEGIN

	SET NOCOUNT ON;
	
	/*CREATE TABLE #Output
	(
		Account			varchar(200),
		BilledAccount	varchar(200),
		ReportDate		varchar(15),
		BilledAmount	decimal(18,2),
		BilledFreight	decimal(18,2),
		BilledInsurance	decimal(18,2),
		BilledOther		decimal(18,2)
	)*/

	DECLARE @FilterBU TABLE (BU varchar(20) PRIMARY KEY);
	DECLARE @HasFilterBU bit;
	SELECT @HasFilterBU = 0;
	
	IF LTRIM(RTRIM(ISNULL(@RevenueBusinessUnit, ''))) != ''
	BEGIN
		INSERT INTO @FilterBU 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(s.Data)) FROM
		dbo.Split(@RevenueBusinessUnit, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @FilterBU)
		BEGIN
			SELECT @HasFilterBU = 1;
		END
		ELSE
		BEGIN
			SELECT @HasFilterBU = 0;
		END
	END;

	SELECT @StartDate = ISNULL(@StartDate, CAST(GETDATE() AS DATE));
	SELECT @EndDate = ISNULL(@EndDate, CAST(GETDATE() AS DATE));
	SELECT @CompanyCode = ISNULL(@CompanyCode, '');
	
	-- first day of the week is Sunday
	IF @FirstDayOfWeek = 'MON'
	BEGIN
		SET DATEFIRST 1;
	END
	ELSE IF @FirstDayOfWeek = 'TUE'
	BEGIN
		SET DATEFIRST 2;
	END
	ELSE IF @FirstDayOfWeek = 'WED'
	BEGIN
		SET DATEFIRST 3;
	END
	ELSE IF @FirstDayOfWeek = 'THU'
	BEGIN
		SET DATEFIRST 4;
	END
	ELSE IF @FirstDayOfWeek = 'FRI'
	BEGIN
		SET DATEFIRST 5;
	END
	ELSE IF @FirstDayOfWeek = 'SAT'
	BEGIN
		SET DATEFIRST 6;
	END
	ELSE IF @FirstDayOfWeek = 'SUN'
	BEGIN
		SET DATEFIRST 7;
	END
	ELSE
	BEGIN
		-- default to Monday
		SET DATEFIRST 1;
	END
	
	--IF @ReportType = 'W' -- weekly
	--BEGIN
		SELECT  @StartDate = CAST((CONVERT(datetime, @StartDate) - (DATEPART(DW, CONVERT(datetime, @StartDate)) - 1)) AS DATE);
		SELECT @EndDate = CAST((CONVERT(datetime, @EndDate) + (7 - (DATEPART(DW, CONVERT(datetime, @EndDate))))) AS DATE);
	--END
	--ELSE
	--BEGIN
	--	SELECT @StartDate = CONVERT(date, (
	--							CONVERT(char(4), DATEPART(year, @StartDate)) + '-' + 
	--							CONVERT(char(2), REPLACE(STR(DATEPART(month, @StartDate), 2), ' ', '0')) + '-01'));
	--
	--	SELECT @EndDate = CONVERT(date, (
	--							CONVERT(char(4), DATEPART(year, @EndDate)) + '-' + 
	--							CONVERT(char(2), REPLACE(STR(DATEPART(month, @EndDate), 2), ' ', '0')) + '-01'));
	--	
	--	SELECT @EndDate = DATEADD(day, -1, (DATEADD(MONTH, 1, @EndDate)));
	--END
	
	PRINT @StartDate;
	PRINT @EndDate;
	
	WITH report AS
	(
		SELECT
		CASE @IncludeChildren 
			WHEN 0 THEN '-'
			WHEN 1 THEN d.AccountCode + ' - ' + d.Shortname
		END AS [ConsignmentAccount],
		bd.Accountcode + ' - ' + bd.Shortname AS [ConsignmentBillingAccount],
		ap.PeriodName AS [ReportMonth],
		ap.WeekId AS [ReportWeek],
		(LEFT(ap.WeekId, 4) + '-' + RIGHT(ap.WeekId, 2)) AS [ReportWeekSort],
		CONVERT(char(4), (CONVERT(date, ('1-' + ap.PeriodName)))) + '-' + REPLACE(STR(MONTH((CONVERT(date, ('1-' + ap.PeriodName)))), 2), ' ', '0')
			AS [ReportMonthSort],
		CASE so.CompanyCode 
			WHEN 'CL1' THEN 'CouriersPlease'
			WHEN 'PL1' THEN 'ParcelDirect'
			WHEN 'WL1' THEN 'WesternAustralia'
			ELSE 'Unknown'
		END AS [CompanyCode],
		CASE LTRIM(RTRIM(ISNULL(so.TerritoryCode, '')))
			WHEN 'CAD' THEN CASE @GroupByState WHEN 1 THEN 'SA' ELSE 'Adelaide' END
			WHEN 'CBN' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Brisbane' END
			WHEN 'CCB' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Canberra' END
			WHEN 'CCC' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Central Coast' END
			WHEN 'CHQ' THEN CASE @GroupByState WHEN 1 THEN 'HQ' ELSE 'Head Office' END
			WHEN 'CNL' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Newcastle' END
			WHEN 'CME' THEN CASE @GroupByState WHEN 1 THEN 'VIC' ELSE 'Melbourne' END
			WHEN 'COO' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Gold Coast' END
			WHEN 'CPE' THEN CASE @GroupByState WHEN 1 THEN 'WA' ELSE 'Perth' END
			WHEN 'CSC' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Sunshine Coast' END
			WHEN 'CSY' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Sydney' END
			WHEN 'PHQ' THEN CASE @GroupByState WHEN 1 THEN 'HQ' ELSE 'Head Office' END
			WHEN 'PNT' THEN CASE @GroupByState WHEN 1 THEN 'NT' ELSE 'NT' END
			
			WHEN 'CCF' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Sydney' END
			WHEN 'CDA' THEN CASE @GroupByState WHEN 1 THEN 'SA' ELSE 'Adelaide' END
			WHEN 'CCA' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Brisbane' END
			WHEN 'CAB' THEN CASE @GroupByState WHEN 1 THEN 'VIC' ELSE 'Melbourne' END

			WHEN 'PNW' THEN 'NSW'
			WHEN 'PQL' THEN 'QLD'
			WHEN 'PSA' THEN 'SA'
			WHEN 'PTA' THEN CASE @GroupByState WHEN 1 THEN 'VIC' ELSE 'TAS' END
			WHEN 'PVI' THEN 'VIC'
			WHEN 'PWA' THEN 'WA'
			WHEN 'WCR' THEN 'WA'
			ELSE ISNULL(so.TerritoryCode, 'UNKNOWN')
		END AS [RevenueBranch],
		ISNULL(SUM(ISNULL(so.GrossAmount, 0.00)), 0.00) AS [BilledAmount],
		ISNULL(
			CASE so.SalesType
				WHEN 'E' 
					THEN ISNULL(SUM(ISNULL(so.GrossAmount, 0.00)),0.00) - ISNULL(SUM(ISNULL(pb.BilledFuelSurcharge, 0.00)), 0.00) - 
						ISNULL(SUM(ISNULL(pb.BilledInsurance, 0.00)), 0.00) - ISNULL(SUM(ISNULL(pb.BilledOtherCharge, 0.00)), 0.00)
				ELSE 0.00 
			END
		, 0.00) AS [BilledFreight],
		ISNULL(
			CASE so.SalesType
				WHEN 'E'
					THEN ISNULL(SUM(ISNULL(pb.BilledFuelSurcharge, 0.00)), 0.00) 
				ELSE 0.00
			END
		, 0.00) AS [BilledFuel],
		ISNULL(
			CASE so.SalesType
				WHEN 'E'
					THEN ISNULL(SUM(ISNULL(pb.BilledInsurance, 0.00)), 0.00) 
				ELSE 0.00
			END
		, 0.00) AS [BilledInsurance],
		ISNULL(
			CASE so.SalesType
				WHEN 'E'
					THEN ISNULL(SUM(ISNULL(pb.BilledOtherCharge, 0.00)), 0.00) 
				ELSE ISNULL(SUM(ISNULL(so.GrossAmount, 0.00)),0.00)
			END
		, 0.00) AS [BilledOther]
		FROM
		ProntoSalesOrder so (NOLOCK)
		LEFT OUTER JOIN ProntoAccountingPeriod ap (NOLOCK)
		ON so.AccountingDate = ap.DayId 
		LEFT OUTER JOIN ProntoBilling pb (NOLOCK)
		ON so.OrderNumber = pb.OrderNumber 
		AND so.CompanyCode = pb.CompanyCode 
		LEFT OUTER JOIN ProntoDebtor d (NOLOCK)
		ON so.CustomerCode = d.Accountcode 
		LEFT OUTER JOIN ProntoDebtor bd (NOLOCK)
		ON d.BillTo = bd.Accountcode 
		WHERE so.CompanyCode = @CompanyCode
		AND CAST(so.AccountingDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
		AND
		(
			(so.TerritoryCode IN
				(SELECT BU
				 FROM @FilterBU)
			)
			OR
			(
				@HasFilterBU = 0
			)
			OR
			(
				LTRIM(RTRIM(ISNULL(@RevenueBusinessUnit, ''))) LIKE '%<ALL%'
			)
		)
		AND
		(
			(
				(((so.CustomerCode LIKE ((@AccountCodeFilter + '%'))) AND (@IncludeChildren = 1)))
				OR d.BillTo LIKE ((@AccountCodeFilter + '%'))
			)
			OR
			(LTRIM(RTRIM(ISNULL(@AccountCodeFilter, ''))) = '')
		)
		AND
		(
			(
				(((d.Shortname LIKE ((@AccountNameFilter + '%'))) AND (@IncludeChildren = 1)))
				OR bd.Shortname LIKE ((@AccountNameFilter + '%'))
			)
			OR
			(LTRIM(RTRIM(ISNULL(@AccountNameFilter, ''))) = '')
		)
		AND ISNULL(so.TerritoryCode, '') < 'xaa'
		AND so.SalesType IN ('M', 'E')
		GROUP BY 
		CASE @IncludeChildren 
			WHEN 0 THEN '-'
			WHEN 1 THEN d.Accountcode + ' - ' + d.Shortname
		END,
		bd.Accountcode,
		bd.Shortname,
		so.CompanyCode,
		so.TerritoryCode,
		so.SalesType,
		ap.PeriodName,
		ap.WeekId
	)
	SELECT
		[ConsignmentAccount],
		[ConsignmentBillingAccount],
		[ReportMonth],
		[ReportWeek],
		[ReportMonthSort],
		[ReportWeekSort],
		[CompanyCode],
		[RevenueBranch],
		ISNULL(SUM(ISNULL([BilledAmount], 0.00)), 0.00) AS [BilledAmount],
		ISNULL(SUM(ISNULL([BilledFreight], 0.00)), 0.00) AS [BilledFreight],
		ISNULL(SUM(ISNULL([BilledFuel], 0.00)), 0.00) AS [BilledFuel],
		ISNULL(SUM(ISNULL([BilledInsurance], 0.00)), 0.00) AS [BilledInsurance],
		ISNULL(SUM(ISNULL([BilledOther], 0.00)), 0.00) AS [BilledOther] 
	FROM report 
	GROUP BY
		[ConsignmentAccount],
		[ConsignmentBillingAccount],
		[ReportMonth],
		[ReportWeek],
		[ReportMonthSort],
		[ReportWeekSort],
		[CompanyCode],
		[RevenueBranch]
	ORDER BY [ConsignmentBillingAccount] ASC, [ConsignmentAccount] ASC, ReportMonthSort ASC, [ReportWeekSort] ASC;
	
	SET NOCOUNT OFF;

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_BillingSummaryReportWeekByMonth]
	TO [ReportUser]
GO
