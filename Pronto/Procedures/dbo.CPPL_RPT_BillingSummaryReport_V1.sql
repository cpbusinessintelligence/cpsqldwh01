SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CPPL_RPT_BillingSummaryReport_V1]( @AccountCodeFilter varchar(20),
@AccountNameFilter	varchar(40),
@CompanyCode		varchar(10),
@StartDate			date,
@EndDate			date,
@ReportType			varchar(1),
@RevenueBusinessUnit varchar(1000),
@IncludeChildren	bit,
@FirstDayOfWeek		char(3),
@GroupByState		bit = 0) as 
begin



DECLARE @FilterBU TABLE (BU varchar(20) PRIMARY KEY);
	DECLARE @HasFilterBU bit;
	SELECT @HasFilterBU = 0;


	
	IF LTRIM(RTRIM(ISNULL(@RevenueBusinessUnit, ''))) != ''
	BEGIN
		INSERT INTO @FilterBU 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(s.Data)) FROM
		dbo.Split(@RevenueBusinessUnit, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @FilterBU)
		BEGIN
			SELECT @HasFilterBU = 1;
		END
		ELSE
		BEGIN
			SELECT @HasFilterBU = 0;
		END
	END;

	SELECT @StartDate = ISNULL(@StartDate, CAST(GETDATE() AS DATE));
	SELECT @EndDate = ISNULL(@EndDate, CAST(GETDATE() AS DATE));
	SELECT @CompanyCode = ISNULL(@CompanyCode, '');
	SELECT @ReportType = ISNULL(@ReportType, 'M');
	
	-- first day of the week is Sunday
	IF @FirstDayOfWeek = 'MON'
	BEGIN
		SET DATEFIRST 1;
	END
	ELSE IF @FirstDayOfWeek = 'TUE'
	BEGIN
		SET DATEFIRST 2;
	END
	ELSE IF @FirstDayOfWeek = 'WED'
	BEGIN
		SET DATEFIRST 3;
	END
	ELSE IF @FirstDayOfWeek = 'THU'
	BEGIN
		SET DATEFIRST 4;
	END
	ELSE IF @FirstDayOfWeek = 'FRI'
	BEGIN
		SET DATEFIRST 5;
	END
	ELSE IF @FirstDayOfWeek = 'SAT'
	BEGIN
		SET DATEFIRST 6;
	END
	ELSE IF @FirstDayOfWeek = 'SUN'
	BEGIN
		SET DATEFIRST 7;
	END
	ELSE
	BEGIN
		-- default to Monday
		SET DATEFIRST 1;
	END
	
	IF @ReportType = 'W' -- weekly
	BEGIN
		SELECT  @StartDate = CAST((CONVERT(datetime, @StartDate) - (DATEPART(DW, CONVERT(datetime, @StartDate)) - 1)) AS DATE);
		SELECT @EndDate = CAST((CONVERT(datetime, @EndDate) + (7 - (DATEPART(DW, CONVERT(datetime, @EndDate))))) AS DATE);
	END
	ELSE
	BEGIN
		SELECT @StartDate = CONVERT(date, (
								CONVERT(char(4), DATEPART(year, @StartDate)) + '-' + 
								CONVERT(char(2), REPLACE(STR(DATEPART(month, @StartDate), 2), ' ', '0')) + '-01'));

		SELECT @EndDate = CONVERT(date, (
								CONVERT(char(4), DATEPART(year, @EndDate)) + '-' + 
								CONVERT(char(2), REPLACE(STR(DATEPART(month, @EndDate), 2), ' ', '0')) + '-01'));
		
		SELECT @EndDate = DATEADD(day, -1, (DATEADD(MONTH, 1, @EndDate)));
	END
	
	PRINT @StartDate;
	PRINT @EndDate;
	
	WITH report AS
	(
		SELECT
		CASE @IncludeChildren 
			WHEN 0 THEN '-'
			WHEN 1 THEN d.AccountCode + ' - ' + d.Shortname
		END AS [ConsignmentAccount],
		bd.Accountcode + ' - ' + bd.Shortname AS [ConsignmentBillingAccount],
		CASE @ReportType 
			WHEN 'M' THEN
				ap.PeriodName 
				/*CASE MONTH(so.AccountingDate)
					WHEN 1 THEN 'Jan'
					WHEN 2 THEN 'Feb'
					WHEN 3 THEN 'Mar'
					WHEN 4 THEN 'Apr'
					WHEN 5 THEN 'May'
					WHEN 6 THEN 'Jun'
					WHEN 7 THEN 'Jul'
					WHEN 8 THEN 'Aug'
					WHEN 9 THEN 'Sep'
					WHEN 10 THEN 'Oct'
					WHEN 11 THEN 'Nov'
					WHEN 12 THEN 'Dec'
				END + '-' + CONVERT(char(4), YEAR(so.AccountingDate))*/
			WHEN 'W' THEN
				ap.WeekId 
				/*REPLACE(STR(DATEPART(week, so.AccountingDate), 2), ' ', '0') + '-' + (CONVERT(char(4), DATEPART(year, so.AccountingDate)))*/
			ELSE 
				'Unknown'
		END AS [ReportDate],
		CASE @ReportType 
			WHEN 'M' THEN
				/*CONVERT(char(4), YEAR(so.AccountingDate)) + '-' + REPLACE(STR(MONTH(so.AccountingDate), 2), ' ', '0')*/
				CONVERT(char(4), (CONVERT(date, ('1-' + ap.PeriodName)))) + '-' + REPLACE(STR(MONTH((CONVERT(date, ('1-' + ap.PeriodName)))), 2), ' ', '0')
			WHEN 'W' THEN
				(LEFT(ap.WeekId, 4) + '-' + RIGHT(ap.WeekId, 2))
				/*(CONVERT(char(4), DATEPART(year, so.AccountingDate))) + '-' + REPLACE(STR(DATEPART(week, so.AccountingDate), 2), ' ', '0')*/
			ELSE 
				'Unknown'
		END AS [ReportDateSort],
		CASE pb.CompanyCode 
			WHEN 'CL1' THEN 'CouriersPlease'
			WHEN 'PL1' THEN 'ParcelDirect'
			WHEN 'WL1' THEN 'WesternAustralia'
			ELSE 'Unknown'
		END AS [CompanyCode],
		CASE LTRIM(RTRIM(ISNULL(pb.revenuebusinessunit, '')))
			WHEN 'CAD' THEN CASE @GroupByState WHEN 1 THEN 'SA' ELSE 'Adelaide' END
			WHEN 'CBN' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Brisbane' END
			WHEN 'CCB' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Canberra' END
			WHEN 'CCC' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Central Coast' END
			WHEN 'CHQ' THEN CASE @GroupByState WHEN 1 THEN 'HQ' ELSE 'Head Office' END
			WHEN 'CNL' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Newcastle' END
			WHEN 'CME' THEN CASE @GroupByState WHEN 1 THEN 'VIC' ELSE 'Melbourne' END
			WHEN 'COO' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Gold Coast' END
			WHEN 'CPE' THEN CASE @GroupByState WHEN 1 THEN 'WA' ELSE 'Perth' END
			WHEN 'CSC' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Sunshine Coast' END
			WHEN 'CSY' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Sydney' END
			WHEN 'PHQ' THEN CASE @GroupByState WHEN 1 THEN 'HQ' ELSE 'Head Office' END
			WHEN 'PNT' THEN CASE @GroupByState WHEN 1 THEN 'NT' ELSE 'NT' END

			
			WHEN 'CCF' THEN CASE @GroupByState WHEN 1 THEN 'NSW' ELSE 'Sydney' END
			WHEN 'CDA' THEN CASE @GroupByState WHEN 1 THEN 'SA' ELSE 'Adelaide' END
			WHEN 'CCA' THEN CASE @GroupByState WHEN 1 THEN 'QLD' ELSE 'Brisbane' END
			WHEN 'CAB' THEN CASE @GroupByState WHEN 1 THEN 'VIC' ELSE 'Melbourne' END

			WHEN 'PNW' THEN 'NSW'
			WHEN 'PQL' THEN 'QLD'
			WHEN 'PSA' THEN 'SA'
			WHEN 'PTA' THEN CASE @GroupByState WHEN 1 THEN 'VIC' ELSE 'TAS' END
			WHEN 'PVI' THEN 'VIC'
			WHEN 'PWA' THEN 'WA'
			WHEN 'WCR' THEN 'WA'
			ELSE ISNULL(pb.revenuebusinessunit, 'UNKNOWN')
		END AS [RevenueBranch],
		ISNULL(SUM(ISNULL(pb.billedtotal, 0.00)), 0.00) AS [BilledAmount],
		ISNULL(SUM(ISNULL(pb.billedfreightcharge, 0.00)), 0.00) AS [BilledFreight],
		ISNULL(SUM(ISNULL(pb.BilledFuelSurcharge, 0.00)), 0.00) AS [BilledFuel],
		ISNULL(SUM(ISNULL(pb.BilledInsurance, 0.00)), 0.00) AS [BilledInsurance],
		ISNULL(SUM(ISNULL(pb.BilledOtherCharge, 0.00)), 0.00) AS [BilledOther],
		pb.AccountingDate
		,pb.billingdate
		FROM
		ProntoBilling pb

		LEFT OUTER JOIN ProntoAccountingPeriod ap (NOLOCK)
		ON pb.AccountingDate = ap.DayId 
		LEFT OUTER JOIN ProntoDebtor d (NOLOCK)
		ON pb.AccountBillToCode = d.Accountcode 
		LEFT OUTER JOIN ProntoDebtor bd (NOLOCK)
		ON d.BillTo = bd.Accountcode 
		WHERE
		pb.CompanyCode = @CompanyCode and
	   CAST(pb.AccountingDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
		AND
		(
			(pb.revenuebusinessunit IN
				(SELECT BU
				 FROM @FilterBU)
			)
			OR
			(
				@HasFilterBU = 0
			)
			OR
			(
				LTRIM(RTRIM(ISNULL(@RevenueBusinessUnit, ''))) LIKE '%<ALL%'
			)
		)
		AND
		(
			(
				((( pb.AccountBillToCode LIKE ((@AccountCodeFilter + '%'))) AND (@IncludeChildren = 1)))
				OR d.BillTo LIKE ((@AccountCodeFilter + '%'))
			)
			OR
			(LTRIM(RTRIM(ISNULL(@AccountCodeFilter, ''))) = '')
		)
		AND
		(
			(
				(((d.Shortname LIKE ((@AccountNameFilter + '%'))) AND (@IncludeChildren = 1)))
				OR bd.Shortname LIKE ((@AccountNameFilter + '%'))
			)
			OR
			(LTRIM(RTRIM(ISNULL(@AccountNameFilter, ''))) = '')
		)
		AND ISNULL(pb.revenuebusinessunit, '') < 'xaa'
		----AND so.SalesType IN ('M', 'E')
		GROUP BY 
		CASE @IncludeChildren 
			WHEN 0 THEN '-'
			WHEN 1 THEN d.Accountcode + ' - ' + d.Shortname
		END,
		bd.Accountcode,
		bd.Shortname,
		pb.AccountBillToCode,
		pb.RevenueBusinessUnit,
		pb.CompanyCode,
		--so.SalesType,
			pb.AccountingDate,
		CASE @ReportType 
			WHEN 'M' THEN
				ap.PeriodName 
				/*CASE MONTH(so.AccountingDate)
					WHEN 1 THEN 'Jan'
					WHEN 2 THEN 'Feb'
					WHEN 3 THEN 'Mar'
					WHEN 4 THEN 'Apr'
					WHEN 5 THEN 'May'
					WHEN 6 THEN 'Jun'
					WHEN 7 THEN 'Jul'
					WHEN 8 THEN 'Aug'
					WHEN 9 THEN 'Sep'
					WHEN 10 THEN 'Oct'
					WHEN 11 THEN 'Nov'
					WHEN 12 THEN 'Dec'
				END + '-' + CONVERT(char(4), YEAR(so.AccountingDate))*/
			WHEN 'W' THEN
				ap.WeekId 
				/*REPLACE(STR(DATEPART(week, so.AccountingDate), 2), ' ', '0') + '-' + (CONVERT(char(4), DATEPART(year, so.AccountingDate)))*/
			ELSE 
				'Unknown'
		END,
		CASE @ReportType 
			WHEN 'M' THEN
				/*CONVERT(char(4), YEAR(so.AccountingDate)) + '-' + REPLACE(STR(MONTH(so.AccountingDate), 2), ' ', '0')*/
				CONVERT(char(4), (CONVERT(date, ('1-' + ap.PeriodName)))) + '-' + REPLACE(STR(MONTH((CONVERT(date, ('1-' + ap.PeriodName)))), 2), ' ', '0')
			WHEN 'W' THEN
				(LEFT(ap.WeekId, 4) + '-' + RIGHT(ap.WeekId, 2))
				/*(CONVERT(char(4), DATEPART(year, so.AccountingDate))) + '-' + REPLACE(STR(DATEPART(week, so.AccountingDate), 2), ' ', '0')*/
			ELSE 
				'Unknown'
		END,
		pb.billedtotal,
		billedfreightcharge,
		pb.BilledFuelSurcharge,
		pb.BilledInsurance,
	    pb.BilledOtherCharge
		,pb.BillingDate
	)
	SELECT
		[ConsignmentAccount],
		[ConsignmentBillingAccount],
		[ReportDate],
		[ReportDateSort],
		[CompanyCode],
		[RevenueBranch],
		ISNULL(SUM(ISNULL([BilledAmount], 0.00)), 0.00) AS [BilledAmount],
		ISNULL(SUM(ISNULL([BilledFreight], 0.00)), 0.00) AS [BilledFreight],
		ISNULL(SUM(ISNULL([BilledFuel], 0.00)), 0.00) AS [BilledFuel],
		ISNULL(SUM(ISNULL([BilledInsurance], 0.00)), 0.00) AS [BilledInsurance],
		ISNULL(SUM(ISNULL([BilledOther], 0.00)), 0.00) AS [BilledOther] ,
	AccountingDate
	,Billingdate
	FROM report 
	GROUP BY
		[ConsignmentAccount],
		[ConsignmentBillingAccount],
		[ReportDate],
		[ReportDateSort],
		[CompanyCode],
		[RevenueBranch],
		AccountingDate,
		Billingdate
	ORDER BY [ConsignmentBillingAccount] ASC, [ConsignmentAccount] ASC, [ReportDate] ASC;
	end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_BillingSummaryReport_V1]
	TO [ReportUser]
GO
