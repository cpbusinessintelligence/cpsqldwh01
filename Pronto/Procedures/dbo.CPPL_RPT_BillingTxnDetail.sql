SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_BillingTxnDetail]
(
	@AccountCodes	varchar(2000),
	@AccountNames	varchar(2000),
	@StartDate		date,
	@EndDate		date
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FilterAccountNames TABLE (AcctName varchar(100) PRIMARY KEY);
	DECLARE @FilterAccountCodes TABLE (AcctCode varchar(30) PRIMARY KEY);

	IF LTRIM(RTRIM(ISNULL(@AccountCodes, ''))) != ''
	BEGIN
		INSERT INTO @FilterAccountCodes  
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(ISNULL(s.Data, ''))) FROM
		dbo.Split(@AccountCodes, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
	END

	IF LTRIM(RTRIM(ISNULL(@AccountNames, ''))) != ''
	BEGIN
		INSERT INTO @FilterAccountNames 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(ISNULL(s.Data, ''))) FROM
		dbo.Split(@AccountNames, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
	END

	SELECT
		DISTINCT
		pb.AccountBillToCode,
		pb.AccountBillToName,
		--ISNULL(d.Shortname, '') AS [ProntoBillToAccountName],
		pb.AccountCode as [ConsignmentAccount],
		pb.AccountName as [ConsignmentAccountName],
		--ISNULL(bd.Shortname, '') AS [ProntoAccountName],
		pb.ConsignmentReference,
		pb.ConsignmentDate,
		pb.ServiceCode,
		pb.AccountingDate,
		pb.AccountingPeriod,
		pb.AccountingWeek,
		pb.BillingDate,
		pb.BilledTotal,
		pb.BilledFreightCharge,
		pb.BilledFuelSurcharge,
		pb.BilledInsurance,
		pb.BilledOtherCharge,
		pb.CardRateDiscountOff,
		ISNULL(pb.DeadWeight, 0.00) AS [DeadWeight],
		ISNULL(pb.Volume, 0.00) AS [Volume],
		ISNULL(pb.ChargeableWeight, 0.00) AS [ChargeableWeight],
		pb.CustomerReference,
		pb.ItemQuantity,
		pb.ManifestDate,
		pb.ManifestReference,
		pb.OrderNumber AS [ProntoSalesOrderNumber],
		pb.InvoiceNumber AS [ProntoInvoiceNumber],
		pb.InsuranceCategory,
		pb.InsuranceDeclaredValue,
		pb.OriginLocality,
		pb.OriginPostcode,
		pb.RevenueOriginZone AS [OriginZone],
		pb.PickupSupplier,
		pb.PickupCost,
		pb.ReceiverName,
		pb.DestinationLocality,
		pb.DestinationPostcode,
		pb.RevenueDestinationZone AS [DestinationZone],
		pb.DeliverySupplier,
		pb.DeliveryCost,
		pb.TariffId AS [ProntoTariffId],
		pb.CalculatedFreightCharge,
		pb.CalculatedFuelSurcharge,
		pb.CardRateFreightCharge,
		pb.CardRateFuelSurcharge,
		pb.CompanyCode 
	FROM ProntoBilling pb (NOLOCK)
	--LEFT OUTER JOIN ProntoDebtor d (NOLOCK)
	--ON pb.AccountCode = d.AccountCode
	--LEFT OUTER JOIN ProntoDebtor bd (NOLOCK)
	--ON pb.AccountBillToCode = d.Accountcode 
	WHERE
	((
		/*(
			(LTRIM(RTRIM(ISNULL(@AccountCodes, ''))) = '')
			AND
			(LTRIM(RTRIM(ISNULL(@AccountNames, ''))) != '')
		)
		OR*/
		(
			pb.AccountBillToCode IN (SELECT AcctCode FROM @FilterAccountCodes)
			OR
			pb.AccountCode IN (SELECT AcctCode FROM @FilterAccountCodes)
		)
	)
	OR
	(
		/*(
			(LTRIM(RTRIM(ISNULL(@AccountNames, ''))) = '')
			AND
			(LTRIM(RTRIM(ISNULL(@AccountCodes, ''))) != '')
		)
		OR*/
		(
			pb.AccountBillToName LIKE (SELECT ('%' + AcctName + '%') FROM @FilterAccountNames)
			OR
			pb.AccountName LIKE (SELECT ('%' + AcctName + '%') FROM @FilterAccountNames)
		)
	))
	AND pb.BillingDate BETWEEN @StartDate AND @EndDate
	ORDER BY pb.AccountBillToCode ASC, pb.AccountCode ASC, pb.ConsignmentReference ASC;

	SET NOCOUNT OFF;

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_BillingTxnDetail]
	TO [ReportUser]
GO
