SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC
[dbo].[CPPL_RPT_ContractorRedemptionDifferenceIncentiveSummary_SS08042019] 
--'2019-01-01','2019-04-01','sydney'
(
	@StartDate		date,
	@EndDate		date,
	@Branches		varchar(1000)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT @StartDate = ISNULL(@StartDate, '1jan1900');
	SELECT @EndDate = ISNULL(@EndDate, CONVERT(date, GETDATE()));
	
	DECLARE @FilterBranches TABLE (BranchName varchar(25) PRIMARY KEY);
	DECLARE @HasFilterBranches bit;
	
	SELECT @HasFilterBranches = 0;

	IF LTRIM(RTRIM(ISNULL(@Branches, ''))) != ''
	BEGIN
	
		INSERT INTO @FilterBranches 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(s.Data)) FROM
		dbo.Split(@Branches, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @FilterBranches)
		BEGIN
			SELECT @HasFilterBranches = 1;
		END
	
	END;

	WITH results AS
	(
		SELECT
		DISTINCT Branch, RctiWeekEndingDate, 
		0.00 AS [SumTotalIncentive], 0.00 AS [SumTotalRedemptions],
		SUM(ISNULL(
			CASE WHEN ISNULL(IsReceiverPays, 0) = 1
				THEN 0.00
				ELSE ISNULL(TotalDifference, 0.00)
			END
		, 0.00)) AS [SumTotalDifference],
		0 AS [ContractorCount]
		FROM CPPL_ContractorRctiPaymentsByCouponPrefix (NOLOCK)
		WHERE RctiWeekEndingDate BETWEEN @StartDate AND @EndDate 
		AND ContractorId NOT IN ('S049','S050')
		AND ContractorId NOT LIKE '[ABCGMS]099'
		AND
		(
			(Branch IN
			(
				SELECT BranchName
				FROM @FilterBranches
			))
			OR
			(@HasFilterBranches = 0)
		)
		GROUP BY Branch, RctiWeekEndingDate 
		--ORDER BY Branch, RctiWeekEndingDate 
		UNION ALL
		SELECT 
		DISTINCT Branch, RctiWeekEndingDate, SUM(ISNULL(TotalDevelopmentIncentive, 0.00)) AS [SumTotalIncentive],
		SUM(ISNULL(TotalRctiRedemptions, 0.00)) AS [SumTotalRedemptions],
		0.00 AS [SumTotalDifference],
		COUNT(DISTINCT ContractorId) AS [ContractorCount]
		FROM CPPL_ContractorRctiPaymentsTotal (NOLOCK)
		WHERE RctiWeekEndingDate BETWEEN @StartDate AND @EndDate 
		AND ContractorId NOT IN ('S049','S050')
		AND ContractorId NOT LIKE '[ABCGMS]099'
		AND
		(
			(Branch IN
			(
				SELECT BranchName
				FROM @FilterBranches
			))
			OR
			(@HasFilterBranches = 0)
		)
		GROUP BY Branch, RctiWeekEndingDate 
	)
	SELECT
	DISTINCT
	Branch, RctiWeekEndingDate, SUM(SumTotalIncentive) AS [SumTotalIncentive],
	SUM(SumTotalRedemptions) AS [SumTotalRedemptions],
	SUM(SumTotalDifference) AS [SumTotalDifference],
	SUM(ContractorCount) AS [ContractorCount],
	ISNULL(SUM(SumTotalRedemptions) / 
		CASE WHEN SUM(ContractorCount) < 1
			THEN 1
			ELSE SUM(ContractorCount)
	END, 0.00) AS [ContractorAverage]
	FROM results
	GROUP BY results.Branch, results.RctiWeekEndingDate;
	--ORDER BY Branch, RctiWeekEndingDate;

	SET NOCOUNT OFF;
	
END


GO
