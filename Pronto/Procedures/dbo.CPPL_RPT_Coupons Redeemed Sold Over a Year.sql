SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[CPPL_RPT_Coupons Redeemed Sold Over a Year](@Startdate datetime) as
begin
SELECT [SerialNumber]
       ,MIn(ActivityDateTime) AS [Date]
  into #temp1
  FROM [Cosmos].[dbo].[ActivityImport]
  wHERE convert(date,ActivityDateTime) >= @Startdate
  AND  convert(date,ActivityDateTime) <  dateadd("mm",1,@startdate)
  
  --@Year+'-'+right('00'+convert(varchar(10),convert(int,right('00'+@month,2))+1),2)+'-'+'01'
  GROUP BY [SerialNumber]
  
 -- select * from #temp1
  SELECT [SerialNumber]
       ,MIn(ActivityDateTime) AS [Date]
  into #temp2
  FROM [Cosmos].[dbo].[ActivityImport]
  wHERE convert(date,ActivityDateTime) >= convert(varchar(4),Year(dateadd("mm",-1,@startdate)))+'-'+right('00'+convert(varchar(2),month(dateadd("mm",-1,@startdate))),2)+'-'+'15'
  AND  convert(date,ActivityDateTime) <@Startdate
  GROUP BY [SerialNumber]
  
  
  sELECT #temp1.SerialNumber  INTO #TEMP3 FROM  #temp1 JOIN #temp2 ON #temp1.SerialNumber = #temp2.SerialNumber
  
  DELETE FROM #temp1 WHERE #temp1.SerialNumber IN (SELECT SerialNumber FROM #Temp3 )
  
  ----update #temp1 set lastsolddate='' where lastsolddate='1900-01-01'
  
  Select T.*,(case when D.LastSoldDate='1900-01-01' or  D.LastSoldDate is null then '' else D.LastSoldDate end) as LastSoldDate,D.LastSoldReference,D.RevenueAmount  ,DATEDIFF(DAY,D.LastSoldDate,T.Date) as duration Into #TempFinal  from #temp1 T Join Pronto.dbo.ProntoCouponDetails D on T.SerialNumber = D.SerialNumber 
      Where  lastsolddate<>'1900-01-01' and (LastSoldReference = 'CONVERSI'  OR DATEDIFF(DAY,D.LastSoldDate,T.Date) >365)




  update #tempfinal set lastsolddate='' where lastsolddate='1900-01-01'

  select  t.SerialNumber,[Date],CASE WHEN S.SerialWhseCode  like 'S%' OR S.SerialWhseCode like 'CSY' OR S.SerialWhseCode like 'C[0-9]%'  THEN 'SYDNEY'
                                    WHEN S.SerialWhseCode like 'M%' or  S.SerialWhseCode like 'CME' THEN  'MELBOURNE' 
                                    WHEN S.SerialWhseCode like 'A%' or  S.SerialWhseCode like 'CAD' THEN  'ADELAIDE'
                                    WHEN S.SerialWhseCode like 'P%' or  S.SerialWhseCode like 'CPE' THEN  'PERTH'
                                    WHEN S.SerialWhseCode like 'B%' or  S.SerialWhseCode like 'CBN' THEN  'BRISBANE'
                                    WHEN S.SerialWhseCode like 'G%' or  S.SerialWhseCode like 'COO' THEN  'GOLD COAST'
                                    ELSE  'Unknown' END   as BusinessUnit,t.LastSoldDate,t.LastSoldReference,t.RevenueAmount ,Duration
  from #tempfinal t left join Pronto.dbo.Prontostockserialnumber s on t.serialnumber=s.serialnumber
  order by duration desc


--Select * from  #TempFinal order by duration desc
end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_Coupons Redeemed Sold Over a Year]
	TO [ReportUser]
GO
