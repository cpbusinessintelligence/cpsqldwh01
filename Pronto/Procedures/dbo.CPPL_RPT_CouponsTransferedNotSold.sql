SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_RPT_CouponsTransferedNotSold]
  (@Branch Varchar(20))
AS
BEGIN
     SELECT  C.StartSerialNumber , 
             MAX(C.TransferDate) as TransferDate,
             COUNT(*) as CouponCount
     Into #Temp1
     FROM [dbo].[ProntoCouponDetails] C --join Pronto.dbo.ProntoStockSerialNumber S on C.SerialNumber =S.SerialNumber
     where C.TransferDate is not null 
       and C.LastSoldDate is null 
       and C.LastSoldReference is null
       and Pronto.dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(C.StartSerialNumber,3)) not in ('RETURN TRK','IRP TRK','ATL')
     group by C.StartSerialNumber
     
     
      
     Select T.StartSerialNumber , 
         --Revenue.dbo.CPPL_fn_GetCouponBranchFromPrefix(LEFT(T.StartSerialNumber,3)) as Branch,
         Pronto.dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(Ltrim(T.StartSerialNumber),3)) as CouponType ,
         T.TransferDate ,
         T.CouponCount 
         ,DATEDIFF(day,TransferDate,GETDATE()) as DurationDays,
         S.SerialWhseCode ,
         S.SerialStatus ,
         CASE WHEN S.SerialWhseCode  like 'S%' OR S.SerialWhseCode like 'C%'  THEN 'SYDNEY'
                                    WHEN S.SerialWhseCode like 'M%'THEN  'MELBOURNE' 
                                    WHEN S.SerialWhseCode like 'A%'THEN  'ADELAIDE'
                                    WHEN S.SerialWhseCode like 'P%'THEN  'PERTH'
                                    WHEN S.SerialWhseCode like 'B%'THEN  'BRISBANE'
                                    WHEN S.SerialWhseCode like 'G%'THEN  'GOLD COAST'
                                    ELSE  '' END   as Branch 
      Into #Temp2
      from #Temp1 T Join Pronto.dbo.ProntoStockSerialNumber S on T.StartSerialNumber =S.SerialNumber
      where  s.SerialStatus <> '90'  and SerialWhseCode not in ('CBN','CAD','CSY','CME','CCB','COO','CPE')
      IF @Branch = 'ALL'
          Select T.*,D.ShortName from #Temp2 T Left Join Pronto.dbo.ProntoDebtor D on T.SerialWhseCode = D.Accountcode
      ELSE 
          Select T.*,D.ShortName from #Temp2 T Left Join Pronto.dbo.ProntoDebtor D on T.SerialWhseCode = D.Accountcode WHERE T.Branch in(select * from fn_GetMultipleValues(@Branch))
  
  
  SET NOCOUNT OFF;

END


GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_CouponsTransferedNotSold]
	TO [ReportUser]
GO
