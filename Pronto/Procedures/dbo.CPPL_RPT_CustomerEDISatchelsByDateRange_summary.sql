SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_CustomerEDISatchelsByDateRange_summary]
  (@CompanyAccounts Varchar(20),@StartDate Date,@EndDate Date)
AS
BEGIN
   SELECT pc_code, pc_name  into #Temp1 from cpplEDI.dbo.pricecodes Where pc_name like '%Satchel%'
    
    SELECT ConsignmentReference
      ,  [AccountCode]
      ,[AccountName]
      ,[AccountBillToCode]
      ,[AccountBillToName]
      ,[ServiceCode]
      ,T.pc_name as ServiceCodeDescr
      ,[ItemQuantity]
      ,1 as ConsignmentCount
  into #Temp2
  FROM [Pronto].[dbo].[ProntoBilling] B join  #Temp1 T  on B.ServiceCode = T.pc_code                             
  Where BillingDate >= @StartDate and BillingDate <= @EndDate and [AccountCode] = @CompanyAccounts
  
  Select T.ConsignmentReference,
         T.AccountCode,
         T.AccountName,
         C.cd_pickup_addr0 as Sender ,
		 C.cd_pickup_branch as Branchid,
         T.[AccountBillToCode],
         T.[AccountBillToName],
         T.[ServiceCode],
         T.ServiceCodeDescr,
         T.[ItemQuantity],
         T.ConsignmentCount
    into #Temp3
  from #Temp2 T Left Join cppledi.dbo.consignment C on T.ConsignmentReference = C.cd_connote 


  select T.AccountName,
         T.Sender ,
		 B.b_name as branchname,
         T.ServiceCodeDescr,
         sum(T.[ItemQuantity]) as ItemQuantity,
         sum(T.ConsignmentCount) as ConsignmentCount from #Temp3 T Left Join [cpplEDI].[dbo].[branchs] B on T.Branchid=B.b_id
		 group by B.b_name ,T.AccountName,T.Sender,T.ServiceCodeDescr
  
  SET NOCOUNT OFF;

END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_CustomerEDISatchelsByDateRange_summary]
	TO [ReportUser]
GO
