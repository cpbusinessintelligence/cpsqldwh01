SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_RPT_EDIRedemptionCheck]
  (@StartDate Date,@EndDate Date)
AS
BEGIN

	SET NOCOUNT ON;
   Select C.cd_connote as Consignment 
         ,a_name as AgentName
         ,C.cd_delivery_suburb, 
         C.cd_delivery_postcode , 
         C.cd_deadweight ,  
         C.cd_items 
    into #Temp1
   from [cpplEDI].[dbo].[consignment] C (Nolock) Left Join [cpplEDI].[dbo].[Agents] A (Nolock) on C.[cd_agent_id] = A.a_id
    Where cd_date >= @StartDate and cd_date < = @EndDate and (a_name not like '%Couriers Please%' and a_name not like '%CPPL%')
   
 --SELECT SerialNumber 
 --Into #Temp1
 --FROM [RDS].[dbo].[ProntoCouponDetails]
 --Where  LEN(SerialNumber) <> 11
 --       and ISNUMERIC(SerialNumber) = 0 and DeliveryDate  >= '2014-03-10' and DeliveryDate <= '2014-03-16'
   
   Select T.Consignment as SerialNumber ,
		  C.DeliveryContractor , 
		  T.AgentName,
		  C.DeliveryDate ,
		  C.DeliveryRCTIdate,
		  C.BillingAccountCode,
          C.BillingService,
         COnvert(varchar(20),CASE LEFT([DeliveryContractor],1) WHEN 'C' THEN 'Canberra' WHEN 'G' THEN 'GoldCoast' WHEN 'S' THEN 'Sydney' WHEN 'M' THEN 'Melbourne' WHEN 'A' THEN 'Adelaide'WHEN 'B' THEN 'Brisbane' ELSE '' END) as Branch ,
         C.DeliveryRctiAmount ,
         T.cd_delivery_suburb as Suburb,
         T.cd_delivery_postcode as PostCode,
         T.cd_deadweight as WtPerConsignemt,
         T.cd_items as ItemsPerConsignemts

   From #Temp1 T LEft Join dbo.ProntoCouponDetails C on T.Consignment = C.SerialNumber 
   Where  C.DeliveryContractor =  C.PickupContractor 


	SET NOCOUNT OFF;

END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_EDIRedemptionCheck]
	TO [ReportUser]
GO
