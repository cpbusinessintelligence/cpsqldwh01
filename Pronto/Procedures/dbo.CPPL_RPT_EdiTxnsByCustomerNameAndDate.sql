SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_RPT_EdiTxnsByCustomerNameAndDate]
(
	@AccountName	varchar(250),
	@StartDate		date,
	@EndDate		date
)
AS
BEGIN
    Select * Into #Temp1 from [dbo].[ufn_String_To_Table](@AccountName,'|',1)
    
	SELECT so.CustomerCode AS [Account Code],
	pb.AccountName AS [Account Name],
	pb.AccountBillToCode AS [Bill To Account Code],
	pb.AccountBillToName AS [Bill To Account Name],
	pb.InvoiceNumber AS [Invoice Number],
	pb.BillingDate AS [Date Billed],
	pb.ConsignmentReference AS [Consignment Number],
	pb.ConsignmentDate AS [Consignment Date],
	pb.ServiceCode AS [Service Code],
	pb.OriginLocality AS [Origin Suburb],
	pb.OriginPostcode AS [Origin Postcode],
	pb.ReceiverName AS [Receiver Name],
	pb.DestinationLocality AS [Destination Suburb],
	pb.DestinationPostcode AS [Destination Postcode],
	pb.ItemQuantity AS [Item Qty],
	pb.DeadWeight AS [Deadweight],
	pb.Volume AS [Cubic Volume],
	pb.ChargeableWeight AS [Chargeable Weight],
	pb.BilledFreightCharge AS [Billed Freight Charge],
	pb.BilledFuelSurcharge AS [Billed Fuel Surcharge],
	pb.BilledInsurance AS [Billed Insurance],
	pb.BilledOtherCharge AS [Billed Other Charge],
	--pb.CardRateDiscountOff AS [Discount],
	so.GrossAmount AS [Invoice Amount Ex.GST],
	so.GSTAmount AS [GST Amount],
	pb.RevenueOriginZone AS [OriginZone],
	pb.RevenueDestinationZone AS [DestinationZone]
	 FROM ProntoSalesOrder so (NOLOCK)	join ProntoSalesOrderLines sl (NOLOCK)	ON so.OrderNumber = sl.OrderNumber 	and so.CompanyCode = sl.CompanyCode 
	                                    left outer join ProntoBilling pb (NOLOCK)	ON pb.OrderNumber = so.OrderNumber 	and pb.CompanyCode = so.CompanyCode 
	                                    Join #Temp1 T  On pb.AccountName Like '%'+ Rtrim(Ltrim(T.Val)) + '%'
	WHERE --so.CustomerCode in (Select Distinct Rtrim(Ltrim(Val)) From #Temp1)
	--and
	 so.AccountingDate between @StartDate and @EndDate
	--and pb.AccountCode is null
	ORDER BY so.AccountingDate, pb.ConsignmentReference;

END



GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_EdiTxnsByCustomerNameAndDate]
	TO [ReportUser]
GO
