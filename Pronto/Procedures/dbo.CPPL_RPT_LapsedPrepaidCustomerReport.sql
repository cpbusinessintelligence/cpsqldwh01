SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_RPT_LapsedPrepaidCustomerReport]
(
	@StartDate			date,
	@EndDate			date,
	@LapsedPeriod		smallint,
	@Branches		varchar(1000)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FilterBranches TABLE (BranchName varchar(25) PRIMARY KEY);
	DECLARE @HasFilterBranches bit;

	SELECT @HasFilterBranches = 0;

	IF LTRIM(RTRIM(ISNULL(@Branches, ''))) != ''
	BEGIN
	
		INSERT INTO @FilterBranches 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(s.Data)) FROM
		dbo.Split(@Branches, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @FilterBranches)
		BEGIN
			SELECT @HasFilterBranches = 1;
		END
	
	END;
	
	SELECT
	so.CustomerCode,
	d.Shortname,
	CASE WHEN ISNULL(aca.CosmosBranch, '') = ''
		THEN 'UNKNOWN'
		ELSE aca.CosmosBranch 
	END AS [CosmosBranch],
	ISNULL(aca.CosmosCode, '') AS [CosmosCode],
	ISNULL(aca.LastScanActivity, '1jan1900') AS [LastScanActivity],
	ISNULL(aca.LastStockCode, '') AS [LastStockCode],
	ISNULL(aca.LastStockDescription, 'UNKNOWN') AS [LastStockDescription],
	--ISNULL(so.WarehouseCode, '') AS [ContractorId],
	ISNULL(d.Warehouse, '') AS [ContractorId],
	ISNULL((
		SELECT TOP 1 
			CONVERT(varchar(10), ISNULL(cd.DriverNumber, 0))
		FROM Cosmos.dbo.Driver cd (NOLOCK)
		WHERE cd.Branch = ISNULL(aca.CosmosBranch, '')
		AND ISNULL(cd.ProntoId, '') = ISNULL(d.Warehouse, '')
		AND IsActive = 1
	), '0') AS [RunNumber],
	ol.StockCode,
	ISNULL(pc.Shortname, 'UNKNOWN') AS [ContractorName],
	ISNULL(sm.StkDescription, 'UNKNOWN') AS [StockDescription],
	(CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '-' + 
		RIGHT((REPLACE(SPACE(2), ' ', '0') + CONVERT(varchar(2), DATEPART(month, so.AccountingDate))), 2) 
		+ '-01') AS [SalesMonth],
	CASE DATEPART(MONTH, so.AccountingDate)
		WHEN 1 THEN 'Jan ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 2 THEN 'Feb ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 3 THEN 'Mar ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 4 THEN 'Apr ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 5 THEN 'May ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 6 THEN 'Jun ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 7 THEN 'Jul ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 8 THEN 'Aug ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 9 THEN 'Sep ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 10 THEN 'Oct ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 11 THEN 'Nov ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 12 THEN 'Dec ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		ELSE 'UNKNOWN'
	END AS [SalesMonthName],
	CASE DATEPART(month, so.AccountingDate)
		WHEN 1 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '01'
		WHEN 2 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '01'
		WHEN 3 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '01'
		WHEN 4 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '02'
		WHEN 5 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '02'
		WHEN 6 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '02'
		WHEN 7 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '03'
		WHEN 8 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '03'
		WHEN 9 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '03'
		WHEN 10 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '04'
		WHEN 11 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '04'
		WHEN 12 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '04'
		ELSE 'UNKNOWN'
	END AS [SalesQuarter],
	CASE DATEPART(month, so.AccountingDate)
		WHEN 1 THEN 'Qtr 1 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 2 THEN 'Qtr 1 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 3 THEN 'Qtr 1 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 4 THEN 'Qtr 2 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 5 THEN 'Qtr 2 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 6 THEN 'Qtr 2 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 7 THEN 'Qtr 3 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 8 THEN 'Qtr 3 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 9 THEN 'Qtr 3 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 10 THEN 'Qtr 4 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 11 THEN 'Qtr 4 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 12 THEN 'Qtr 4 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		ELSE 'UNKNOWN'
	END AS [SalesQuarterName],
	--ISNULL(SUM(ISNULL(so.GrossAmount, 0.00)), 0.00) AS [SalesTotal]
	ISNULL(SUM(ISNULL(ol.GrossAmount, 0.00)), 0.00) AS [SalesTotal]
	FROM ProntoSalesOrder so (NOLOCK)
	JOIN ProntoSalesOrderLines ol (NOLOCK)
	ON so.OrderNumber = ol.OrderNumber 
	AND so.CompanyCode = ol.CompanyCode 
	JOIN ProntoAccountCosmosActivity aca (NOLOCK)
	ON so.CustomerCode = aca.AccountCode 
	LEFT OUTER JOIN ProntoDebtor d (NOLOCK)
	ON so.CustomerCode = d.Accountcode 
	LEFT OUTER JOIN ProntoStockMaster sm (NOLOCK)
	ON ol.StockCode = sm.StockCode 
	LEFT OUTER JOIN ProntoCreditor pc (NOLOCK)
	ON ISNULL(pc.AccountCode, '') = ISNULL(d.Warehouse, '')
	WHERE ISNULL(so.SalesType, '') = 'C'
	AND so.AccountingDate BETWEEN @StartDate AND @EndDate
	AND ISNUMERIC(so.CustomerCode) = 1
	--AND ISNUMERIC(ol.StockCode) = 1
		AND
		(
			(ISNULL(aca.CosmosBranch, '') IN
			(
				SELECT BranchName
				FROM @FilterBranches
			))
			OR
			(@HasFilterBranches = 0)
			OR
			((LTRIM(RTRIM(ISNULL(aca.CosmosBranch, ''))) = '')
				AND EXISTS (SELECT 1 FROM @FilterBranches WHERE BranchName = 'UNKNOWN'))
		)
	AND ISNULL(aca.LastScanActivity, '1jan1900') <= 
	  DATEADD(month, (1 - @LapsedPeriod),
		DATEADD(day, -1, CONVERT(date,
			(CONVERT(varchar(4), DATEPART(year, CONVERT(date, GETDATE()))) + '-' 
			+ RIGHT((REPLACE(SPACE(2), ' ', '0') + CONVERT(varchar(2), DATEPART(month, CONVERT(date, GETDATE())))), 2)
			+ '-01')
		))
	  )
	GROUP BY so.CustomerCode, d.Shortname, aca.CosmosBranch, aca.CosmosCode,
	aca.LastScanActivity, aca.LastStockCode, aca.LastStockDescription, ISNULL(d.Warehouse, ''), ol.StockCode,
	sm.StkDescription, (CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '-' + 
		RIGHT((REPLACE(SPACE(2), ' ', '0') + CONVERT(varchar(2), DATEPART(month, so.AccountingDate))), 2) 
		+ '-01'),
	CASE DATEPART(month, so.AccountingDate)
		WHEN 1 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '01'
		WHEN 2 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '01'
		WHEN 3 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '01'
		WHEN 4 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '02'
		WHEN 5 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '02'
		WHEN 6 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '02'
		WHEN 7 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '03'
		WHEN 8 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '03'
		WHEN 9 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '03'
		WHEN 10 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '04'
		WHEN 11 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '04'
		WHEN 12 THEN CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '04'
		ELSE 'UNKNOWN'
	END,
	CASE DATEPART(month, so.AccountingDate)
		WHEN 1 THEN 'Qtr 1 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 2 THEN 'Qtr 1 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 3 THEN 'Qtr 1 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 4 THEN 'Qtr 2 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 5 THEN 'Qtr 2 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 6 THEN 'Qtr 2 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 7 THEN 'Qtr 3 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 8 THEN 'Qtr 3 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 9 THEN 'Qtr 3 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 10 THEN 'Qtr 4 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 11 THEN 'Qtr 4 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 12 THEN 'Qtr 4 ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		ELSE 'UNKNOWN'
	END,
	CASE DATEPART(MONTH, so.AccountingDate)
		WHEN 1 THEN 'Jan ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 2 THEN 'Feb ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 3 THEN 'Mar ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 4 THEN 'Apr ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 5 THEN 'May ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 6 THEN 'Jun ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 7 THEN 'Jul ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 8 THEN 'Aug ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 9 THEN 'Sep ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 10 THEN 'Oct ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 11 THEN 'Nov ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		WHEN 12 THEN 'Dec ' + CONVERT(varchar(4), DATEPART(year, so.AccountingDate))
		ELSE 'UNKNOWN'
	END,
	ISNULL(pc.Shortname, 'UNKNOWN');
	/*ORDER BY so.CustomerCode, (CONVERT(varchar(4), DATEPART(year, so.AccountingDate)) + '-' + 
		RIGHT((REPLACE(SPACE(2), ' ', '0') + CONVERT(varchar(2), DATEPART(month, so.AccountingDate))), 2) 
		+ '-01'), ol.StockCode;*/

	SET NOCOUNT OFF;
	
END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_LapsedPrepaidCustomerReport]
	TO [ReportUser]
GO
