SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_ProntoBillingReport]
(
	@FromDate			date,
	@ToDate				date,
	@InvoiceAccount		varchar(30),
	@ConsignmentAccount	varchar(30),
	@State				varchar(250)
)
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[dbo].[CPPL_RPT_ProntoBillingReport]
    --' ---------------------------
    --' Purpose: Split the data-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/09/2014    AB      1.00                                                     --AB20140909

    --'=====================================================================

	SET NOCOUNT ON;

	DECLARE @FilterState TABLE ([State] varchar(20) PRIMARY KEY);

	IF LTRIM(RTRIM(ISNULL(@State, ''))) != ''
	BEGIN
		INSERT INTO @FilterState 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(s.Data)) FROM
		dbo.Split(@State, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
	END;

	--SELECT * FROM @FilterState;
	
	SELECT
	DISTINCT
	pb.*,
	CASE
		WHEN pb.ServiceCode IN ('48', '49', '18')
			THEN dest.[State]  
		ELSE
			orig.[State]  
	END AS [State]
	FROM ProntoBilling pb (NOLOCK)
	LEFT OUTER JOIN ProntoGeoDim dest (NOLOCK)
	ON pb.DestinationLocality = dest.Locality 
	AND pb.DestinationPostcode = dest.Postcode 
	AND dest.CompanyCode = 'CL1'
	LEFT OUTER JOIN ProntoGeoDim orig (NOLOCK)
	ON pb.OriginLocality = orig.Locality 
	AND pb.OriginPostcode = orig.Postcode 
	AND orig.CompanyCode = 'CL1'
	WHERE 
	(
		--filter by invoice customer, if applicable
		(AccountBillToCode = @InvoiceAccount)
		OR
		(ISNULL(@InvoiceAccount, '') = '')
	)
	AND
	(
		--filter by consignment customer, if applicable
		(AccountCode = @ConsignmentAccount)
		OR
		(ISNULL(@ConsignmentAccount, '') = '')
	)
	AND
	--filter by selected date range
	--(AccountingDate >= @FromDate AND AccountingDate <= @ToDate)	
	(pb.ConsignmentDate BETWEEN @FromDate AND @ToDate)
	AND
	(
		(
			(
				--filter by origin state for regular despatch jobs
				(
					EXISTS (SELECT 1 FROM @FilterState f
								WHERE f.[State] = ISNULL(orig.[State], ''))
					AND
					(LTRIM(RTRIM(ISNULL(pb.ServiceCode, ''))) NOT IN ('48', '49', '18'))
				)
			)
			OR
			(
				--filter by destination state for returns
				(
					EXISTS (SELECT 1 FROM @FilterState f
								WHERE f.[State] = ISNULL(dest.[State], ''))
					AND
					(LTRIM(RTRIM(ISNULL(pb.ServiceCode, ''))) IN ('48', '49', '18'))
				)
			)
		)
		OR
		(ISNULL(@State, '') = '')
	)
	AND
	--no test consignments
	(ISNULL(TestFlag, 0) = 0)
	AND
	(ISNULL(pb.CompanyCode, 'XX') = 'CL1');
	
	SET NOCOUNT OFF;
	
END
GO
