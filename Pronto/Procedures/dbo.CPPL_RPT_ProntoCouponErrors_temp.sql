SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CPPL_RPT_ProntoCouponErrors_temp]
  (@Branch Varchar(20))
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[dbo].[CPPL_RPT_ProntoCouponErrors_temp]
    --' ---------------------------
    --' Purpose: Get PRonto Coupon Errors-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/09/2014    AB      1.00                                                     --AB20140909

    --'=====================================================================

   SELECT  CASE WHEN [Contractor] like 'A%' THEN 'ADELAIDE' WHEN [Contractor] like 'B%' THEN 'BRISBANE' WHEN [Contractor] like 'S%' OR [Contractor] like 'C%'  THEN 'SYDNEY' WHEN [Contractor] like 'M%' THEN 'MELBOURNE' WHEN [Contractor] like 'G%' THEN  'GOLD COAST' WHEN [Contractor] like 'P%' THEN 'PERTH' ELSE 'UNKNOWN' END as Branch
          ,[Contractor]
          ,[ActivityDateTime]
          ,DATEDIFF(day,[ActivityDateTime],GETDATE()) as duration
          ,[SerialNumber]
          ,CONVERT(Varchar(50),'') as PrimaryCPNReason
          ,CONVERT(Varchar(50),'') as PrimaryCPNBookStartNumber
          ,[LinkCouponNumber]
          ,CONVERT(Varchar(50),'') as LinkCPNReason
          ,CONVERT(Varchar(50),'') as LinkCPNBookStartNumber
          ,[ReturnTracker]
          ,CONVERT(Varchar(50),'') as ReturnCPNReason
          ,[AccountNumber]  
          ,CONVERT(Varchar(50),'') as AccountName     
          ,[ActivityType]
          ,CASE [ConsignmentType] WHEN 'RP' THEN 'Return' WHEN 'CP' THEN 'Prepaid' WHEN 'CL1' THEN 'Consignment' ELse [ConsignmentType] End as [Type]
          ,[FileName]
  INTO #Temp1
  FROM [Pronto].[dbo].[ProntoCPNImportErrors]
  
 Update #Temp1 SET AccountName=D.ShortName from #Temp1 T Left Join Pronto.dbo.ProntoDebtor D on T.[AccountNumber] = D.Accountcode Where AccountNumber <>''

   Select distinct * from #Temp1 WHERE Branch = @Branch  

   end

GO
