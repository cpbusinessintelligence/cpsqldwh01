SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_RPT_ZeroEDIRedemptionByContractor]
  (@ContractorNumber Varchar(20), @StartDate Date,@EndDate Date)
AS
BEGIN

	SET NOCOUNT ON;
	SELECT COnvert(varchar(20),CASE LEFT([PickupContractor],1) WHEN 'C' THEN 'Canberra' WHEN 'G' THEN 'GoldCoast' WHEN 'S' THEN 'Sydney' WHEN 'M' THEN 'Melbourne' WHEN 'A' THEN 'Adelaide'WHEN 'B' THEN 'Brisbane' ELSE 'Unknown' END) as Branch 
	  ,[SerialNumber]
      ,COnvert(varchar(20),'PICKUP') as Activity
      ,[PickupContractor] as Contractor
      ,[PickupDate] as ActivityDate
      ,[PickupRctiDate] as RCTIDate
      ,[BillingAccountCode]
      ,[BillingService]
      ,CONVERT(varchar(100),'') as Suburb
      ,CONVERT(varchar(100),'') as PostCode
      ,CONVERT(varchar(100),'') as WeightPerCOnsignment
      ,CONVERT(varchar(20),'') as ItemsPerConsignment
  into #Temp1
  FROM [Pronto].[dbo].[ProntoCouponDetails] where BillingAccountCode is not null and 
   PickupDate >= @StartDate and PickupDate <= @EndDate
   and PickupRctiAmount =0
   and [PickupContractor] = @ContractorNumber
   Union all
   
   SELECT COnvert(varchar(20),CASE LEFT([DeliveryContractor],1) WHEN 'C' THEN 'Canberra' WHEN 'G' THEN 'GoldCoast' WHEN 'S' THEN 'Sydney' WHEN 'M' THEN 'Melbourne' WHEN 'A' THEN 'Adelaide'WHEN 'B' THEN 'Brisbane' ELSE 'Unknown' END) as Branch 
      ,[SerialNumber]
      ,COnvert(varchar(20),'DELIVERY') as Activity
      ,[DeliveryContractor] as Contractor
      ,[DeliveryDate] as ActivityDate
      ,[DeliveryRctiDate] as RCTIDate
      ,[BillingAccountCode]
      ,[BillingService]
      ,CONVERT(varchar(100),'') as Suburb
      ,CONVERT(varchar(100),'') as PostCode
      ,CONVERT(varchar(100),'') as WeightPerCOnsignment
      ,CONVERT(varchar(20),'') as ItemsPerConsignment
  FROM [Pronto].[dbo].[ProntoCouponDetails] where BillingAccountCode is not null and 
   DeliveryDate  >= @StartDate and DeliveryDate <= @EndDate
   and DeliveryRctiAmount = 0
   and DeliveryContractor = @ContractorNumber
  Update #Temp1 SET  Suburb = C.cd_delivery_suburb,PostCode = C.cd_delivery_postcode ,WeightPerCOnsignment = C.cd_deadweight , ItemsPerConsignment = C.cd_items 
     From #Temp1 T Join cppledi.dbo.consignment C  ON T.SerialNumber = C.cd_connote   Where T.Activity = 'DELIVERY'
   Update #Temp1 SET  Suburb = C.cd_pickup_suburb,PostCode = C.cd_pickup_postcode ,WeightPerCOnsignment = C.cd_deadweight , ItemsPerConsignment = C.cd_items 
     From #Temp1 T Join cppledi.dbo.consignment C  ON T.SerialNumber = C.cd_connote   Where T.Activity = 'PICKUP'
   Select * from #Temp1
	SET NOCOUNT OFF;

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_ZeroEDIRedemptionByContractor]
	TO [ReportUser]
GO
