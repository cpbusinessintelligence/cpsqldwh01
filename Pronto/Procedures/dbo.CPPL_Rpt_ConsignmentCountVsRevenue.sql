SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[CPPL_Rpt_ConsignmentCountVsRevenue](@Satchel varchar(100),@StartDate date,@Enddate date)
as 
begin
    
SELECT pc_code, pc_name,convert(varchar(20),'') as SatchelType into #temp1  from cpplEDI.dbo.pricecodes Where pc_name like '%Satchel%'

update #temp1 set SatchelType=case when pc_name like '%500 Gram%' then '500 Grams'
                              when pc_name like '%1Kg%' or pc_name like '%1KG%' or pc_name like '%1 Kg%'  then '1 Kg'
                              when pc_name like '%3Kg%' or pc_name like '%3KG%' or pc_name like '%3 Kg%' then '3 Kg'
                              when pc_name like '%5Kg%' or pc_name like '%5KG%' or pc_name like '%5 Kg%'  then '5 Kg'
	                          else 'unknown' end
    
SELECT ConsignmentReference
      ,[AccountCode]
      ,[AccountName]
	  ,Billingdate
      ,T.pc_name as SatchelDetail
      ,1 as ConsignmentCount
	  ,BilledTotal
	  ,T.SatchelType
	  ,og.state as OriginState
	  ,dg.state as DestinationState
  into #Temp2
  FROM [Pronto].[dbo].[ProntoBilling] B join  #Temp1 T  on B.ServiceCode = T.pc_code    
  LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim og 
  ON b.OriginLocality = og.Locality AND b.OriginPostcode = og.Postcode 
  LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim dg ON b.DestinationLocality = dg.Locality AND b.DestinationPostcode = dg.Postcode                          
  Where BillingDate >= @StartDate and BillingDate <=@EndDate 

  Update #temp2 set OriginState='Unknown' where OriginState is NULL
  Update #temp2 set DestinationState='Unknown' where DestinationState is NULL

  If @Satchel='ALL' 
  begin
  select Billingdate,
         SatchelType,
		 SatchelDetail,
		 [AccountCode] as CustomerAccount,
		 [AccountName] as CustomerName,
		 OriginState,
		 DestinationState,
		 count(*) as ConsignmentCount,
		 sum(BilledTotal) as RevenueAmount
    from #temp2 
    group by Billingdate,SatchelType,SatchelDetail,[AccountCode],[AccountName], OriginState,DestinationState
	end
else  
begin
select Billingdate,
         SatchelType,
		 SatchelDetail,
		 [AccountCode] as CustomerAccount,
		 [AccountName] as CustomerName,
		 OriginState,
		 DestinationState,
		 count(*) as ConsignmentCount,
		 sum(BilledTotal) as RevenueAmount
    from #temp2 where SatchelType=@Satchel 
    group by Billingdate,SatchelType,SatchelDetail,[AccountCode],[AccountName], OriginState,DestinationState
	end
						

  end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_Rpt_ConsignmentCountVsRevenue]
	TO [ReportUser]
GO
