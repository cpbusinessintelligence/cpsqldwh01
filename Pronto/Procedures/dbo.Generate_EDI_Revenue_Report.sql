SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Generate_EDI_Revenue_Report]

as

BEGIN

--DROP TABLE #temp1
-- truncate table [dbo].[EDI_Consignment_Billing_Data]
SELECT * INTO #temp1 FROM [dbo].[ProntoBilling] WITH(NOLOCK) 
WHERE [ConsignmentReference] 
IN (SELECT consignmentnumber FROM  [dbo].[EDI_Consignment_Billing_Data] WITH(NOLOCK))

CREATE NONCLUSTERED INDEX idx_temp1_Consignmentreference ON #temp1([Consignmentreference])

-- select [ConsignmentReference],count(*) from #temp1 group by [ConsignmentReference] having count(*) > 1
-- select consignmentnumber,count(*) from [dbo].[EDI_Consignment_Billing_Data] group by consignmentnumber having count(*) > 1
-- select * from  [dbo].[EDI_Consignment_Billing_Data] where consignmentnumber = 'CPAFXLC7329678'
-- select count(*) from  [dbo].[EDI_Consignment_Billing_Data]
--DROP TABLE #temp2

SELECT 

	Pronto.ConsignmentDate									as Pronto_ConsignmentDate
	,Pronto.BillingDate										AS Pronto_BillingDate
	,EDI.ConsignmentDate									as EDI_ConsignmentDate
	,CAST([Consignmentreference] as nvarchar(50))			as [Consignmentreference]

	,EDI.CustomerAccountCode								AS EDI_AccountCode
	,c.c_name												AS EDI_CompanyName
	,Pronto.AccountBillToCode								AS Pronto_AccountCode
	,Pronto.AccountBillToName								AS Pronto_BillToName	
	--,Pronto.[Manifestreference]
	,EDI.ServiceCode										AS EDI_OriginalServiceCode
	,Pronto.ServiceCode										AS Pronto_ServiceCode
	,(Case WHEN EDI.ServiceCode <> Pronto.ServiceCode Then 'Service Code Change From '+EDI.ServiceCode+' To '+Pronto.ServiceCode ELSE '' END) AS ServiceCodeUpdate

	,EDI.FuelSurcharge					AS EDI_OriginalFuelSurcharge
	,Pronto.BilledFuelSurcharge			AS Pronto_BilledFuelSurcharge
	,(CASE WHEN Pronto.BilledFuelSurcharge-EDI.FuelSurcharge > 0 THEN Pronto.BilledFuelSurcharge-EDI.FuelSurcharge ELSE 0 END) AS Fuel_Surcharge_Recovered
	,EDI.FinalCharge					AS EDI_FinalCharge
	,Pronto.BilledTotal					AS Pronto_BilledTotal
	
	,Pronto.DeclaredWeight				AS Pronto_DeclaredWeight
	,Pronto.ChargeableWeight			AS Pronto_ChargeableWeight
	,EDI.DeclaredWeight					AS EDI_DeclaredWeight

	,Pronto.ChargeableWeight-EDI.DeclaredWeight AS WeightVariation

	,Pronto.DeadWeight					AS Pronto_DeadWeight
	,Pronto.DeclaredVolume				AS Pronto_DeclaredVolume
	,Pronto.CardRateFreightCharge		AS Pronto_CardRateFreightCharge
	,EDI.TotalChargeExGST				AS EDI_TotalChargeExGST_OriginalFreight
	,Pronto.BilledFreightCharge			AS Pronto_BilledFreightCharge
	,Pronto.BilledFreightCharge - EDI.TotalChargeExGST		AS RevenueRecovered
	,(Case WHEN Pronto.BilledFreightCharge - EDI.TotalChargeExGST	 > 0 THEN 'YES' ELSE 'NO' END) AS RevenueProtected


	,EDI.DeclaredVolume					AS EDI_DeclaredVolume	
	,EDI.DelcaredCubicWeight			AS EDI_DelcaredCubicWeight
	,EDI.OriginalBilledWeight			AS EDI_OriginalBilledWeight

	,Pronto.RevenueBusinessUnit
	,Pronto.RevenueOriginZone
	,Pronto.RevenueDestinationZone
	,Pronto.AccountingWeek
	,EDI.SYDSortation

INTO #temp2

FROM #temp1 Pronto join [dbo].[EDI_Consignment_Billing_Data] EDI on Pronto.[Consignmentreference] = EDI.consignmentnumber
outer apply (select distinct [c_id],[c_name] from CpplEDI.[dbo].[companies] where c_id = EDI.CompanyID) c 
WHERE Pronto.[Consignmentreference] not in ('CPAHVNZ0113398','CPAP97Z1285826','CPAP97Z1486662') -- and edi.TotalChargeExGST is not null



DROP TABLE dbo.EDI_RevenueReport_Data
SELECT * INTO dbo.EDI_RevenueReport_Data from #temp2
/*
TRUNCATE TABLE dbo.EDI_RevenueReport_Data

INSERT INTO dbo.EDI_RevenueReport_Data
(
	ConsignmentDate
	,Consignmentreference
	,EDI_AccountCode
	,EDI_CompanyName
	,Pronto_AccountCode
	,AccountBillToName
	,Manifestreference
	,PE_FuelSurcharge
	,PE_TotalChargeExGST
	,PE_FinalCharge
	,Pronto_BilledTotal
	,Pronto_FuelSurcharge
	,Pronto_FreightCharge
	,Pronto_ServiceCode
	,EDI_ServiceCode
	,EDI_OriginalServiceCode
	,Pronto_ChargeableWeight
	,Pronto_DeclaredWeight
	,EDI_DeclaredWeight
	,EDI_BilledWeight
	,Pronto_DeadWeight
	,EDI_MeasuredWeight
	,EDI_DerivedMeasuredWeight
	,EDI_MeasuredCubicWeight
	,RevenueBusinessUnit
	,RevenueOriginZone
	,RevenueDestinationZone
	,AccountingWeek
	,ServiceCodeUpdateComments
)
SELECT 
	ConsignmentDate
	,Consignmentreference
	,EDI_AccountCode
	,EDI_CompanyName
	,Pronto_AccountCode
	,AccountBillToName
	,Manifestreference
	,PE_FuelSurcharge
	,PE_TotalChargeExGST
	,PE_FinalCharge
	,Pronto_BilledTotal
	,Pronto_FuelSurcharge
	,Pronto_FreightCharge
	,Pronto_ServiceCode
	,EDI_ServiceCode
	,EDI_OriginalServiceCode
	,Pronto_ChargeableWeight
	,Pronto_DeclaredWeight
	,EDI_DeclaredWeight
	,EDI_BilledWeight
	,Pronto_DeadWeight
	,EDI_MeasuredWeight
	,EDI_DerivedMeasuredWeight
	,EDI_MeasuredCubicWeight
	,RevenueBusinessUnit
	,RevenueOriginZone
	,RevenueDestinationZone
	,AccountingWeek
	,ServiceCodeUpdateComments
FROM #temp2
*/

-- select * from dbo.EDI_RevenueReport_Data order by ConsignmentDate,consignmentreference
-- select * from [dbo].[PricingDataForCWC_Measured]

/*
select Case when i.SortationCount > 0 then 'Y' else '' end as SYDSortation,sum(RevenueRecovered) from dbo.EDI_RevenueReport_Data r
outer apply (select count(*) as SortationCount from  [dbo].[Incoming_CWCDetails] where cd_connote = r.Consignmentreference) i
where EDI_TotalChargeExGST_OriginalFreight > 0
group by Case when i.SortationCount > 0 then 'Y' else '' end 

select Case when i.SortationCount > 0 then 'Y' else '' end as SYDSortation,* from dbo.EDI_RevenueReport_Data r
outer apply (select count(*) as SortationCount from  [dbo].[Incoming_CWCDetails] where cd_connote = r.Consignmentreference) i
where EDI_TotalChargeExGST_OriginalFreight > 0 and Pronto_ConsignmentDate between '2019-11-01' and '2019-11-11'

select Case when i.SortationCount > 0 then 'Y' else '' end as SYDSortation,* from dbo.EDI_RevenueReport_Data r
outer apply (select count(*) as SortationCount from  [dbo].[Incoming_CWCDetails] where cd_connote = r.Consignmentreference) i
where EDI_TotalChargeExGST_OriginalFreight = 0 and Pronto_ConsignmentDate between '2019-11-01' and '2019-11-11'


select * from dbo.EDI_RevenueReport_Data where pronto_accountcode = '112925235'

*/


end
GO
