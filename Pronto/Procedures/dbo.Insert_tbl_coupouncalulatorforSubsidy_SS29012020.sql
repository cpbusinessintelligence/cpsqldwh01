SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[Insert_tbl_coupouncalulatorforSubsidy_SS29012020]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN


truncate table ProntoCouponDetails_forsubsidy

Insert into ProntoCouponDetails_forsubsidy select  * from Pronto.dbo.ProntoCouponDetails (nolock) where year(deliverydate)=2020

Delete from ProntoCouponDetails_forsubsidy where len(serialnumber)=11 and ISNUMERIC(serialnumber)=1 

and Pronto.dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(SerialNumber,3))  in ('RETURN TRK','IRP TRK','ATL','LINK','PROMO','REDELIVERY CPN','REDELIVERY CARD','PROFORMA','LINK - REGIONAL')



END
GO
