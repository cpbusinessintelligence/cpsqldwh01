SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[MaintenancePlan_Pronto]
AS
Begin
	---ReOrganize
	
	ALTER INDEX [PK_CPPL_ContractorRctiPaymentsTotal] ON [dbo].[CPPL_ContractorRctiPaymentsTotal] REORGANIZE  
		
	ALTER INDEX [PK_cppl_ErrorLog] ON [dbo].[ErrorLog] REORGANIZE  
		
	ALTER INDEX [PK_Incoming_CWCDetails_New] ON [dbo].[Incoming_CWCDetails] REORGANIZE  
		
	ALTER INDEX [PK_InterfaceConfig] ON [dbo].[InterfaceConfig] REORGANIZE  
	
	ALTER INDEX [PK_Load_ProntoBilling] ON [dbo].[Load_ProntoBilling] REORGANIZE  
	
	ALTER INDEX [PK_Load_ProntoCreditor] ON [dbo].[Load_ProntoCreditor] REORGANIZE  
	
	ALTER INDEX [PK_Load_ProntoMail] ON [dbo].[Load_ProntoMail] REORGANIZE  
	
	ALTER INDEX [PK_Load_ProntoWarehouse] ON [dbo].[Load_ProntoWarehouse] REORGANIZE  
	
	ALTER INDEX [PK_PDG_ErrorLog] ON [dbo].[PDG_ErrorLog] REORGANIZE  
	
	ALTER INDEX [PK_ProntoAccountBillingActivity_1] ON [dbo].[ProntoAccountBillingActivity] REORGANIZE  
	
	ALTER INDEX [PK_ProntoAccountCosmosActivity] ON [dbo].[ProntoAccountCosmosActivity] REORGANIZE  
	
	ALTER INDEX [PK_ProntoAccountingPeriod] ON [dbo].[ProntoAccountingPeriod] REORGANIZE  
	
	ALTER INDEX [_dta_index_ProntoBilling_6_1490104349__K9_K10_K14_K1_44_46_47] ON [dbo].[ProntoBilling] REORGANIZE  
	
	ALTER INDEX [idx_ProntoBilling_AccountCodeAcctWeek1] ON [dbo].[ProntoBilling] REORGANIZE  
	
	ALTER INDEX [PK_ProntoBilling1] ON [dbo].[ProntoBilling] REORGANIZE  
	
	ALTER INDEX [PK_ProntoContractorAllowDeduct] ON [dbo].[ProntoContractorAllowDeduct] REORGANIZE  
	
	ALTER INDEX [PK_ProntoContractorAllowDeduct1] ON [dbo].[ProntoContractorAllowDeductV1] REORGANIZE  
	
	ALTER INDEX [IX_ProntoContractorWeeklyDelivery] ON [dbo].[ProntoContractorWeeklyDelivery] REORGANIZE  
	
	ALTER INDEX [IX_ProntoContractorWeeklyReceipt] ON [dbo].[ProntoContractorWeeklyReceipt] REORGANIZE  

	
	ALTER INDEX [PK_ProntoContractorWeeklySales] ON [dbo].[ProntoContractorWeeklySales] REORGANIZE  
	
	
	
	ALTER INDEX [NonClusteredIndex-20210222-154755] ON [dbo].[ProntoCouponDetails] REORGANIZE  
	
	
	
	ALTER INDEX [NonClusteredIndex-20210222-161023] ON [dbo].[ProntoCouponDetails] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoCouponDetails_1] ON [dbo].[ProntoCouponDetails] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoCouponDetails_2] ON [dbo].[ProntoCouponDetails_forsubsidy] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoCreditor] ON [dbo].[ProntoCreditor] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoCreditor2] ON [dbo].[ProntoCreditorV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoDebtor] ON [dbo].[ProntoDebtor] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoDebtorCustNotes] ON [dbo].[ProntoDebtorCustNotes] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoDebtorForSalesForce] ON [dbo].[ProntoDebtorForSalesForce] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoDebtorMaster] ON [dbo].[ProntoDebtorMaster] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoDebtorMaster1] ON [dbo].[ProntoDebtorMaster1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoDebtor1] ON [dbo].[ProntoDebtorV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoGeneralLedger] ON [dbo].[ProntoGeneralLedger] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoGeneralLedger1] ON [dbo].[ProntoGeneralLedgerV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoGlBalance] ON [dbo].[ProntoGlBalance] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoGLBudgets] ON [dbo].[ProntoGLBudgets] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoGlChartOfAccounts_1] ON [dbo].[ProntoGlChartOfAccounts] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoGlChartOfAccounts_2] ON [dbo].[ProntoGlChartOfAccountsV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoGlFact] ON [dbo].[ProntoGlFact] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoMail] ON [dbo].[ProntoProspect] REORGANIZE  
	
	
	
	ALTER INDEX [idx_ProntoPurgeData_Temp_SerialNumber] ON [dbo].[ProntoPurgeData_Temp] REORGANIZE  
	
	
	
	ALTER INDEX [_dta_index_ProntoSalesOrder_7_53575229__K7_K6_K4_K8_K1_K3_12] ON [dbo].[ProntoSalesOrder] REORGANIZE  
	
	
	
	ALTER INDEX [idx_ProntoSalesOrder_CustomerOrderCompanyDate] ON [dbo].[ProntoSalesOrder] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrder1112] ON [dbo].[ProntoSalesOrder] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrderArchive] ON [dbo].[ProntoSalesOrderArchive] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrderLines_1] ON [dbo].[ProntoSalesOrderLines] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrderLines_2] ON [dbo].[ProntoSalesOrderLinesV1] REORGANIZE  
	
	
	
	ALTER INDEX [_dta_index_ProntoSalesOrderV1_7_53575229__K7_K6_K4_K8_K1_K3_12] ON [dbo].[ProntoSalesOrderV1] REORGANIZE  
	
	
	
	ALTER INDEX [idx_ProntoSalesOrderV1_CustomerOrderCompanyDate] ON [dbo].[ProntoSalesOrderV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrder1] ON [dbo].[ProntoSalesOrderV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoService] ON [dbo].[ProntoService] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoService1] ON [dbo].[ProntoServiceV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoStockChargesOverride] ON [dbo].[ProntoStockChargesOverride] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoStockChargesOverride1] ON [dbo].[ProntoStockChargesOverrideV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoStockMaster_1] ON [dbo].[ProntoStockMaster] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoStockMaster_2] ON [dbo].[ProntoStockMasterV1] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoStockSerialLink] ON [dbo].[ProntoStockSerialLink] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoStockSerialLink1] ON [dbo].[ProntoStockSerialLinkV1] REORGANIZE  
	
	
	
	ALTER INDEX [IX_ProntoUnredeemedCoupons] ON [dbo].[ProntoUnredeemedCoupons] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoUnredeemedCoupons1] ON [dbo].[ProntoUnredeemedCoupons] REORGANIZE  
	
	
	
	ALTER INDEX [PK_ProntoWarehouse] ON [dbo].[ProntoWarehouse] REORGANIZE  
	
	
	
	ALTER INDEX [idxProntoID] ON [dbo].[tblContractor] REORGANIZE  
	
	
	
	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REORGANIZE  
	
	
	
	ALTER INDEX [PK_JP_tblRCTISummary1] ON [dbo].[tblRCTISummary] REORGANIZE  
	

	-----Rebuild


	
	
	ALTER INDEX [PK_CPPL_ContractorRctiPaymentsTotal] ON [dbo].[CPPL_ContractorRctiPaymentsTotal] REBUILD  
	
	
	
	ALTER INDEX [PK_cppl_ErrorLog] ON [dbo].[ErrorLog] REBUILD  
	
	
	
	ALTER INDEX [PK_Incoming_CWCDetails_New] ON [dbo].[Incoming_CWCDetails] REBUILD  
	
	
	
	ALTER INDEX [PK_InterfaceConfig] ON [dbo].[InterfaceConfig] REBUILD  
	
	
	
	ALTER INDEX [PK_Load_ProntoBilling] ON [dbo].[Load_ProntoBilling] REBUILD  
	
	
	
	ALTER INDEX [PK_Load_ProntoCreditor] ON [dbo].[Load_ProntoCreditor] REBUILD  
	
	
	
	ALTER INDEX [PK_Load_ProntoMail] ON [dbo].[Load_ProntoMail] REBUILD  
	
	
	
	ALTER INDEX [PK_Load_ProntoWarehouse] ON [dbo].[Load_ProntoWarehouse] REBUILD  
	
	
	
	ALTER INDEX [PK_PDG_ErrorLog] ON [dbo].[PDG_ErrorLog] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoAccountBillingActivity_1] ON [dbo].[ProntoAccountBillingActivity] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoAccountCosmosActivity] ON [dbo].[ProntoAccountCosmosActivity] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoAccountingPeriod] ON [dbo].[ProntoAccountingPeriod] REBUILD  
	
	
	
	ALTER INDEX [_dta_index_ProntoBilling_6_1490104349__K9_K10_K14_K1_44_46_47] ON [dbo].[ProntoBilling] REBUILD  
	
	
	
	ALTER INDEX [idx_ProntoBilling_AccountCodeAcctWeek1] ON [dbo].[ProntoBilling] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoBilling1] ON [dbo].[ProntoBilling] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoContractorAllowDeduct] ON [dbo].[ProntoContractorAllowDeduct] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoContractorAllowDeduct1] ON [dbo].[ProntoContractorAllowDeductV1] REBUILD  
	
	
	
	ALTER INDEX [IX_ProntoContractorWeeklyDelivery] ON [dbo].[ProntoContractorWeeklyDelivery] REBUILD  
	
	
	
	ALTER INDEX [IX_ProntoContractorWeeklyReceipt] ON [dbo].[ProntoContractorWeeklyReceipt] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoContractorWeeklySales] ON [dbo].[ProntoContractorWeeklySales] REBUILD  
	
	
	
	ALTER INDEX [NonClusteredIndex-20210222-154755] ON [dbo].[ProntoCouponDetails] REBUILD  
	
	
	
	ALTER INDEX [NonClusteredIndex-20210222-161023] ON [dbo].[ProntoCouponDetails] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoCouponDetails_1] ON [dbo].[ProntoCouponDetails] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoCouponDetails_2] ON [dbo].[ProntoCouponDetails_forsubsidy] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoCreditor] ON [dbo].[ProntoCreditor] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoCreditor2] ON [dbo].[ProntoCreditorV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoDebtor] ON [dbo].[ProntoDebtor] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoDebtorCustNotes] ON [dbo].[ProntoDebtorCustNotes] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoDebtorForSalesForce] ON [dbo].[ProntoDebtorForSalesForce] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoDebtorMaster] ON [dbo].[ProntoDebtorMaster] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoDebtorMaster1] ON [dbo].[ProntoDebtorMaster1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoDebtor1] ON [dbo].[ProntoDebtorV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoGeneralLedger] ON [dbo].[ProntoGeneralLedger] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoGeneralLedger1] ON [dbo].[ProntoGeneralLedgerV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoGlBalance] ON [dbo].[ProntoGlBalance] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoGLBudgets] ON [dbo].[ProntoGLBudgets] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoGlChartOfAccounts_1] ON [dbo].[ProntoGlChartOfAccounts] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoGlChartOfAccounts_2] ON [dbo].[ProntoGlChartOfAccountsV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoGlFact] ON [dbo].[ProntoGlFact] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoMail] ON [dbo].[ProntoProspect] REBUILD  
	
	
	
	ALTER INDEX [idx_ProntoPurgeData_Temp_SerialNumber] ON [dbo].[ProntoPurgeData_Temp] REBUILD  
	
	
	
	ALTER INDEX [_dta_index_ProntoSalesOrder_7_53575229__K7_K6_K4_K8_K1_K3_12] ON [dbo].[ProntoSalesOrder] REBUILD  
	
	
	
	ALTER INDEX [idx_ProntoSalesOrder_CustomerOrderCompanyDate] ON [dbo].[ProntoSalesOrder] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrder1112] ON [dbo].[ProntoSalesOrder] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrderArchive] ON [dbo].[ProntoSalesOrderArchive] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrderLines_1] ON [dbo].[ProntoSalesOrderLines] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrderLines_2] ON [dbo].[ProntoSalesOrderLinesV1] REBUILD  
	
	
	
	ALTER INDEX [_dta_index_ProntoSalesOrderV1_7_53575229__K7_K6_K4_K8_K1_K3_12] ON [dbo].[ProntoSalesOrderV1] REBUILD  
	
	
	
	ALTER INDEX [idx_ProntoSalesOrderV1_CustomerOrderCompanyDate] ON [dbo].[ProntoSalesOrderV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoSalesOrder1] ON [dbo].[ProntoSalesOrderV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoService] ON [dbo].[ProntoService] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoService1] ON [dbo].[ProntoServiceV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoStockChargesOverride] ON [dbo].[ProntoStockChargesOverride] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoStockChargesOverride1] ON [dbo].[ProntoStockChargesOverrideV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoStockMaster_1] ON [dbo].[ProntoStockMaster] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoStockMaster_2] ON [dbo].[ProntoStockMasterV1] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoStockSerialLink] ON [dbo].[ProntoStockSerialLink] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoStockSerialLink1] ON [dbo].[ProntoStockSerialLinkV1] REBUILD  
	
	
	
	ALTER INDEX [IX_ProntoUnredeemedCoupons] ON [dbo].[ProntoUnredeemedCoupons] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoUnredeemedCoupons1] ON [dbo].[ProntoUnredeemedCoupons] REBUILD  
	
	
	
	ALTER INDEX [PK_ProntoWarehouse] ON [dbo].[ProntoWarehouse] REBUILD  
	
	
	
	ALTER INDEX [idxProntoID] ON [dbo].[tblContractor] REBUILD  
	
	
	
	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REBUILD  
	
	
	
	ALTER INDEX [PK_JP_tblRCTISummary1] ON [dbo].[tblRCTISummary] REBUILD  
	

	----Update Statistics

	
	
	UPDATE STATISTICS [dbo].[Acct with email addresses] 
	
	
	
	
	UPDATE STATISTICS [dbo].[AdjustedVsOriginalRCTISummary] 
	
	
	
	
	UPDATE STATISTICS [dbo].[AdjustmentsforNoSubsidy] 
	
	
	
	
	UPDATE STATISTICS [dbo].[BranchesOrigPayments] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CL1-name-and-address-master] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CL1-stock-master] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Cons_test] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CouponDatatobepurgedPhase1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CouponDatatobepurgedPhase2] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Couponsalesbycustomer] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CPPL_ContractorRctiPaymentsByCouponPrefix] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CPPL_ContractorRctiPaymentsByCouponPrefix_Archive_15-17] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CPPL_ContractorRctiPaymentsByServiceCode] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CPPL_ContractorRctiPaymentsByServiceCode_Archive_15-17] 
	
	
	
	
	UPDATE STATISTICS [dbo].[CPPL_ContractorRctiPaymentsTotal] 
	
	
	
	
	UPDATE STATISTICS [dbo].[cremaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Customised RC Tab only] 
	
	
	
	
	UPDATE STATISTICS [dbo].[deb_master] 
	
	
	
	
	UPDATE STATISTICS [dbo].[debmaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[DriverDetails] 
	
	
	
	
	UPDATE STATISTICS [dbo].[dumplinks] 
	
	
	
	
	UPDATE STATISTICS [dbo].[EDI_Consignment_Billing_Data] 
	
	
	
	
	UPDATE STATISTICS [dbo].[EDI_RevenueReport_Data] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ErrorLog] 
	
	
	
	
	UPDATE STATISTICS [dbo].[FranchiseePaymentSummary] 
	
	
	
	
	UPDATE STATISTICS [dbo].[FreightAnalysisDetails] 
	
	
	
	
	UPDATE STATISTICS [dbo].[FuelLevy] 
	
	
	
	
	UPDATE STATISTICS [dbo].[fuellivefinal] 
	
	
	
	
	UPDATE STATISTICS [dbo].[FuelToBeBilledDetail] 
	
	
	
	
	UPDATE STATISTICS [dbo].[FuelToBeBilledDetailv1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[FuelToBeBilledSummary] 
	
	
	
	
	UPDATE STATISTICS [dbo].[FuelToBeBilledSummaryv1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[geo_dim] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Incoming_CWCDetails] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Incoming_CWCDetails_Load] 
	
	
	
	
	UPDATE STATISTICS [dbo].[InterfaceConfig] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_Postcodes] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoActivityError] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoBankRecCashTransactions] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoBilling] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoContractorAllowDeduct] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoContractorAllowDeductV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoContractorWeeklyDelivery] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoContractorWeeklyReceipt] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoContractorWeeklySales] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoCouponDetails] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoCreditor] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoCreditorTransArchive] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoCreditorV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoCustomerRates] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoCustomerUprateActions] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoCustomerUprateData] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoDebtor] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoDebtorAgeingV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoDebtorMaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoDebtorTrans] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoDebtorTransArchive] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoDebtorv1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGeneralLedger] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGeneralLedgerV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGeoDim] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGlBalance] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGlBalanceV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGLBudgets] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGlChartOfAccounts] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGlChartOfAccountsV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGlDim] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGlFact] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoGlHierarchy] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoMail] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoNameAddressMaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoSalesOrder] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoSalesOrderArchive] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoSalesOrderArchive_17-18] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoSalesOrderLines] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoSalesOrderLinesV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoSalesOrderV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoService] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockChargesOverride] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockChargesOverrideV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockMaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockMasterV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockSerialLink] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockSerialLinkV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockSerialNumber] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoStockSerialNumberV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoSystemTable] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Load_ProntoWarehouse] 
	
	
	
	
	UPDATE STATISTICS [dbo].[LoadSalesOrder] 
	
	
	
	
	UPDATE STATISTICS [dbo].[mailer_master] 
	
	
	
	
	UPDATE STATISTICS [dbo].[name_address] 
	
	
	
	
	UPDATE STATISTICS [dbo].[name_and_address_master] 
	
	
	
	
	UPDATE STATISTICS [dbo].[name_and_address_masterV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[name_data] 
	
	
	
	
	UPDATE STATISTICS [dbo].[nameandaddmaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[PDG_ErrorLog] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Postcodes] 
	
	
	
	
	UPDATE STATISTICS [dbo].[PrepaidCouponReport] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Pronto Customised Inactive] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Pronto Zones - Exclusion] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Pronto_SalesOrder_Couponstobepurged] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoAccountBillingActivity] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoAccountCosmosActivity] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoAccountingPeriod] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoActivityError] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoActivityErrorHistory] 
	
	
	
	
	UPDATE STATISTICS [dbo].[prontoAuditTable] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoBankRecCashTransactions] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoBilling] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoBilling_Archive] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoBillingPaymentSurcharge] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoContractorAllowDeduct] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoContractorAllowDeductV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoContractorWeeklyDelivery] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoContractorWeeklyReceipt] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoContractorWeeklySales] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCouponDetails] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCouponDetails_forsubsidy] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCPDetailstobePurged] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCPNImportErrors] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCPNImportErrors_Temp] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCreditor] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCreditorV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoCustomerRates] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtor] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorAgeing] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorAgeingV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorCustNotes] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorForSalesForce] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorMaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorMaster1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorTrans] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDebtorV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDriverExclusionList] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoDriverExclusionList_Backup] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoFlagIndicator] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGeneralLedger] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGeneralLedgerV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGeoDim] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGlBalance] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGLBudgets] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGlChartOfAccounts] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGlChartOfAccountsV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGlDim] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGlFact] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGlHierarchy] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoGlOther] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoNameAddressMaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoProspect] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoRatesServiceLevelValues] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrder] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrder_Archive_07] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrder_Archive17-18] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrderArchive] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrderArchive_17_18] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrderLines] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrderLinesV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSalesOrderV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSerialIntegrity] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoService] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoServiceV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockChargesOverride] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockChargesOverrideV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockMaster] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockMasterV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockSerialLink] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockSerialLinkV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockSerialNumber] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoStockSerialNumberV1] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoSystemTable] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoUnredeemedCoupons] 
	
	
	
	
	UPDATE STATISTICS [dbo].[ProntoWarehouse] 
	
	
	
	
	UPDATE STATISTICS [dbo].[rates] 
	
	
	
	
	UPDATE STATISTICS [dbo].[rates_CardDetail] 
	
	
	
	
	UPDATE STATISTICS [dbo].[rates_CardHeader] 
	
	
	
	
	UPDATE STATISTICS [dbo].[redemptiontable] 
	
	
	
	
	UPDATE STATISTICS [dbo].[rep_master] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Rpt_Branch] 
	
	
	
	
	UPDATE STATISTICS [dbo].[sales_order] 
	
	
	
	
	UPDATE STATISTICS [dbo].[sales_order_archive] 
	
	
	
	
	UPDATE STATISTICS [dbo].[SalesLoad_Dummy] 
	
	
	
	
	UPDATE STATISTICS [dbo].[SalesOrd] 
	
	
	
	
	UPDATE STATISTICS [dbo].[SalesOrderSingleTemp] 
	
	
	
	
	UPDATE STATISTICS [dbo].[service_dim] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tbl_ProntoEDIExclusionSubCodes] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tbl_ProntoMissScanExclusionDriver] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tbl_tempCouponLoad] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tbl_tempcouponload_FirstLoad] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tblContractor] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tblErrorLog] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tblRCTICouponCalculationStaging] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tblRCTISummary] 
	
	
	
	
	UPDATE STATISTICS [dbo].[tblwork] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Total] 	
	
	
	
	UPDATE STATISTICS [dbo].[websiterates] 
	
	
	
	
	UPDATE STATISTICS [dbo].[Zones_To_Zone_FuelExemption] 
	

	--Shrink DB

	DBCC SHRINKDATABASE(N'Pronto')

END
GO
