SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Luke Grenfell
-- Create date: 12/8/2009
-- Description:	Cleanup Pronto Tables
-- =============================================
CREATE PROCEDURE [dbo].[PDG_CleanupProntoData] 
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRAN
		
		UPDATE ProntoContractorWeeklyDelivery
		SET Customer = REPLACE(Customer, '~~~~~~~~~~', NULL)
		WHERE Customer = '~~~~~~~~~~'
		
		UPDATE ProntoContractorWeeklyDelivery
		SET Customer = REPLACE(Customer, '', NULL)
		WHERE Customer = ''
		
		COMMIT TRAN;
	END TRY
	
	BEGIN CATCH
		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC PDG_LogRethrowError;
		
		ROLLBACK TRAN;
		
	END CATCH;
	
END
GO
