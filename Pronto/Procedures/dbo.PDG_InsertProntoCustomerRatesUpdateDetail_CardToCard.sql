SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC  [PDG_InsertProntoCustomerRatesUpdateDetail_CardToCard]
@CompanyCode VARCHAR(5) = 'CL1'

AS


INSERT INTO [ProntoCustomerRatesUpdateDetail]
           ([Id]
           ,[ZoneFrom]
           ,[ZoneTo]
           ,[MinimumCharge]
           ,[BasicCharge]
		   ,[FuelOverrideCharge]
		   ,[FuelPercent]
           ,[QtyOrWeightRounding1]
           ,[QtyOrWeightRounding2]
           ,[QtyOrWeightRounding3]
           ,[QtyOrWeightRounding4]
           ,[QtyOrWeightRounding5]
           ,[QtyOrWeightRounding6]
           ,[QtyOrWeightRounding7]
           ,[QtyOrWeightRounding8]
           ,[QtyOrWeightRounding9]
           ,[QtyOrWeightRounding10]
           ,[QtyOrWeightCharges1]
           ,[QtyOrWeightCharges2]
           ,[QtyOrWeightCharges3]
           ,[QtyOrWeightCharges4]
           ,[QtyOrWeightCharges5]
           ,[QtyOrWeightCharges6]
           ,[QtyOrWeightCharges7]
           ,[QtyOrWeightCharges8]
           ,[QtyOrWeightCharges9]
           ,[QtyOrWeightCharges10]
           ,[QtyOrWeightBreaks1]
           ,[QtyOrWeightBreaks2]
           ,[QtyOrWeightBreaks3]
           ,[QtyOrWeightBreaks4]
           ,[QtyOrWeightBreaks5]
           ,[QtyOrWeightBreaks6]
           ,[QtyOrWeightBreaks7]
           ,[QtyOrWeightBreaks8]
           ,[QtyOrWeightBreaks9]
           ,[QtyOrWeightBreaks10]
           ,[CompanyCode])
SELECT DISTINCT 
		   ProntoCustomerRatesUpdateHeader.Id
           ,ProntoCustomerRates.[ZoneFrom]
           ,ProntoCustomerRates.[ZoneTo]
		   ,CAST(ISNULL(ProntoCustomerRates.Charges4, 0) + (ISNULL(ProntoCustomerRates.Charges4, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) AS MONEY) MinimumCharge
		   ,CAST(ISNULL(ProntoCustomerRates.Charges5, 0) + (ISNULL(ProntoCustomerRates.Charges5, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) AS MONEY) BasicCharge
		   ,CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge
		   ,ProntoCustomerRates.Charges1 FuelPercent
           ,[QtyOrWeightRounding1]
           ,[QtyOrWeightRounding2]
           ,[QtyOrWeightRounding3]
           ,[QtyOrWeightRounding4]
           ,[QtyOrWeightRounding5]
           ,[QtyOrWeightRounding6]
           ,[QtyOrWeightRounding7]
           ,[QtyOrWeightRounding8]
           ,[QtyOrWeightRounding9]
           ,[QtyOrWeightRounding10]
		   ,CAST(ISNULL(ProntoCustomerRates.QtyOrWeightCharges1, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges1, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) AS DECIMAL(18,5)) AS QtyOrWeightCharges1
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges2, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges2, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges2, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges2, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges2
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges3, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges3, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges3, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges3, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges3
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges4, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges4, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges4, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges4, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges4
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges5, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges5, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges5, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges5, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges5
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges6, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges6, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges6, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges6, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges6
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges7, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges7, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges7, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges7, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges7
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges8, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges8, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges8, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges8, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges8
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges9, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges9, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges9, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges9, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges9
		   ,CAST(CASE WHEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges10, 0) < 9999.99 THEN ISNULL(ProntoCustomerRates.QtyOrWeightCharges10, 0) + (ISNULL(ProntoCustomerRates.QtyOrWeightCharges10, 0) * ( ISNULL(ProntoCustomerUprateActions.Percentage_Change, 0))) ELSE ISNULL(ProntoCustomerRates.QtyOrWeightCharges10, 0) END AS DECIMAL(18,5)) AS QtyOrWeightCharges10
           ,[QtyOrWeightBreaks1]
           ,[QtyOrWeightBreaks2]
           ,[QtyOrWeightBreaks3]
           ,[QtyOrWeightBreaks4]
           ,[QtyOrWeightBreaks5]
           ,[QtyOrWeightBreaks6]
           ,[QtyOrWeightBreaks7]
           ,[QtyOrWeightBreaks8]
           ,[QtyOrWeightBreaks9]
           ,[QtyOrWeightBreaks10]
		   ,ProntoCustomerRatesUpdateHeader.CompanyCode
		   
FROM ProntoCustomerUprateActions
JOIN ProntoCustomerRates ON ISNULL(ProntoCustomerRates.AccountCode, 'Card') = ISNULL(ProntoCustomerUprateActions.Pronto_Billing_Account, 'Card')
	AND ProntoCustomerRates.Service = ProntoCustomerUprateActions.Existing_Service
	AND ProntoCustomerRates.IsActive = 1  
	AND ProntoCustomerRates.IsCumulative = 0
	AND ProntoCustomerRates.CustomerOrSupplier = 'C'
	AND ProntoCustomerRates.AccountCode IS NULL
JOIN ProntoCustomerRatesUpdateHeader ON ProntoCustomerRatesUpdateHeader.Id = 
	CASE  
		WHEN ProntoCustomerRates.AccountCode IS NULL THEN 
			CASE 
				WHEN New_Service = 'Delete' THEN 'CARD_'+Existing_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
			    ELSE 'CARD_'+New_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
			END
		WHEN New_Service = 'Delete' THEN LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUprateActions.Pronto_Billing_Account, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + Existing_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
		ELSE LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUprateActions.Pronto_Billing_Account, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + New_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
	END 
	AND ProntoCustomerRates.CompanyCode = ProntoCustomerRatesUpdateHeader.CompanyCode

WHERE Pronto_Data_Change_Required = 1
AND Existing_Rating_Type = 'Custom'
AND New_Rating_Type IN ('Card', 'Adhoc')
AND	Execution_Timing = 'CPPL'
AND New_Service != 'Delete'
GO
