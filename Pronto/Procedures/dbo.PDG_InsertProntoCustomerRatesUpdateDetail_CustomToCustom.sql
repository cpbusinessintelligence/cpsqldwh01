SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [PDG_InsertProntoCustomerRatesUpdateDetail_CustomToCustom]
@CompanyCode VARCHAR(5) = 'CL1'

AS


INSERT INTO [ProntoCustomerRatesUpdateDetail]
           ([Id]
           ,[ZoneFrom]
           ,[ZoneTo]
           ,[MinimumCharge]
           ,[BasicCharge]
		   ,[FuelOverrideCharge]
		   ,[FuelPercent]
           ,[QtyOrWeightRounding1]
           ,[QtyOrWeightRounding2]
           ,[QtyOrWeightRounding3]
           ,[QtyOrWeightRounding4]
           ,[QtyOrWeightRounding5]
           ,[QtyOrWeightRounding6]
           ,[QtyOrWeightRounding7]
           ,[QtyOrWeightRounding8]
           ,[QtyOrWeightRounding9]
           ,[QtyOrWeightRounding10]
           ,[QtyOrWeightCharges1]
           ,[QtyOrWeightCharges2]
           ,[QtyOrWeightCharges3]
           ,[QtyOrWeightCharges4]
           ,[QtyOrWeightCharges5]
           ,[QtyOrWeightCharges6]
           ,[QtyOrWeightCharges7]
           ,[QtyOrWeightCharges8]
           ,[QtyOrWeightCharges9]
           ,[QtyOrWeightCharges10]
           ,[QtyOrWeightBreaks1]
           ,[QtyOrWeightBreaks2]
           ,[QtyOrWeightBreaks3]
           ,[QtyOrWeightBreaks4]
           ,[QtyOrWeightBreaks5]
           ,[QtyOrWeightBreaks6]
           ,[QtyOrWeightBreaks7]
           ,[QtyOrWeightBreaks8]
           ,[QtyOrWeightBreaks9]
           ,[QtyOrWeightBreaks10]
           ,[CompanyCode])
SELECT DISTINCT
		   ProntoCustomerRatesUpdateHeader.Id
           ,ProntoCustomerRates.[ZoneFrom]
           ,ProntoCustomerRates.[ZoneTo]
		   ,CAST(ProntoCustomerRates.Charges4 + (ProntoCustomerRates.Charges4 * ( ProntoCustomerUprateActions.Percentage_Change)) AS MONEY) MinimumCharge
		   ,CAST(ProntoCustomerRates.Charges5 + (ProntoCustomerRates.Charges5 * ( ProntoCustomerUprateActions.Percentage_Change)) AS MONEY) BasicCharge
		   ,CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge
		   ,ProntoCustomerRates.Charges1 FuelPercent
           ,[QtyOrWeightRounding1]
           ,[QtyOrWeightRounding2]
           ,[QtyOrWeightRounding3]
           ,[QtyOrWeightRounding4]
           ,[QtyOrWeightRounding5]
           ,[QtyOrWeightRounding6]
           ,[QtyOrWeightRounding7]
           ,[QtyOrWeightRounding8]
           ,[QtyOrWeightRounding9]
           ,[QtyOrWeightRounding10]
		   ,CAST(ProntoCustomerRates.QtyOrWeightCharges1 + (ProntoCustomerRates.QtyOrWeightCharges1 * ( ProntoCustomerUprateActions.Percentage_Change)) AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges2 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges2 + (ProntoCustomerRates.QtyOrWeightCharges2 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges2 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges3 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges3 + (ProntoCustomerRates.QtyOrWeightCharges3 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges3 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges4 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges4 + (ProntoCustomerRates.QtyOrWeightCharges4 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges4 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges5 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges5 + (ProntoCustomerRates.QtyOrWeightCharges5 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges5 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges6 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges6 + (ProntoCustomerRates.QtyOrWeightCharges6 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges6 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges7 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges7 + (ProntoCustomerRates.QtyOrWeightCharges7 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges7 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges8 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges8 + (ProntoCustomerRates.QtyOrWeightCharges8 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges8 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges9 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges9 + (ProntoCustomerRates.QtyOrWeightCharges9 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges9 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges10 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges10 + (ProntoCustomerRates.QtyOrWeightCharges10 * ( ProntoCustomerUprateActions.Percentage_Change)) ELSE ProntoCustomerRates.QtyOrWeightCharges10 END AS DECIMAL(18,5))
           ,[QtyOrWeightBreaks1]
           ,[QtyOrWeightBreaks2]
           ,[QtyOrWeightBreaks3]
           ,[QtyOrWeightBreaks4]
           ,[QtyOrWeightBreaks5]
           ,[QtyOrWeightBreaks6]
           ,[QtyOrWeightBreaks7]
           ,[QtyOrWeightBreaks8]
           ,[QtyOrWeightBreaks9]
           ,[QtyOrWeightBreaks10]
           ,ProntoCustomerRatesUpdateHeader.CompanyCode
    
FROM ProntoCustomerUprateActions
JOIN ProntoCustomerRates ON ISNULL(ProntoCustomerRates.AccountCode, 'Card') = ISNULL(ProntoCustomerUprateActions.Pronto_Billing_Account, 'Card')
	AND ProntoCustomerRates.Service = ProntoCustomerUprateActions.New_Service
	AND ProntoCustomerRates.IsActive = 1  
	AND ProntoCustomerRates.IsCumulative = 0
	AND ProntoCustomerRates.CustomerOrSupplier = 'C'
	AND ProntoCustomerRates.AccountCode IS NOT NULL
JOIN ProntoCustomerRatesUpdateHeader ON ProntoCustomerRatesUpdateHeader.Id = 
	CASE  
		WHEN Pronto_Billing_Account IS NULL THEN 'CARD_'+Existing_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
		ELSE
		 LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUprateActions.Pronto_Billing_Account, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + Existing_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
	END	AND ProntoCustomerRates.CompanyCode = ProntoCustomerRatesUpdateHeader.CompanyCode

WHERE 
Pronto_Data_Change_Required = 1
AND Existing_Rating_Type = 'Custom'
AND New_Rating_Type IN ('Custom', 'Adhoc')
AND	Execution_Timing = 'CPPL'


GO
