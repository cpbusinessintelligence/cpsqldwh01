SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [PDG_InsertProntoCustomerRatesUpdateDetail_CustomToCustom_PD]
@EffectiveDate DATE = '2010-10-01'

AS


INSERT INTO [ProntoCustomerRatesUpdateDetail]
           ([Id]
           ,[ZoneFrom]
           ,[ZoneTo]
           ,[MinimumCharge]
           ,[BasicCharge]
		   ,[FuelOverrideCharge]
		   ,[FuelPercent]
           ,[QtyOrWeightRounding1]
           ,[QtyOrWeightRounding2]
           ,[QtyOrWeightRounding3]
           ,[QtyOrWeightRounding4]
           ,[QtyOrWeightRounding5]
           ,[QtyOrWeightRounding6]
           ,[QtyOrWeightRounding7]
           ,[QtyOrWeightRounding8]
           ,[QtyOrWeightRounding9]
           ,[QtyOrWeightRounding10]
           ,[QtyOrWeightCharges1]
           ,[QtyOrWeightCharges2]
           ,[QtyOrWeightCharges3]
           ,[QtyOrWeightCharges4]
           ,[QtyOrWeightCharges5]
           ,[QtyOrWeightCharges6]
           ,[QtyOrWeightCharges7]
           ,[QtyOrWeightCharges8]
           ,[QtyOrWeightCharges9]
           ,[QtyOrWeightCharges10]
           ,[QtyOrWeightBreaks1]
           ,[QtyOrWeightBreaks2]
           ,[QtyOrWeightBreaks3]
           ,[QtyOrWeightBreaks4]
           ,[QtyOrWeightBreaks5]
           ,[QtyOrWeightBreaks6]
           ,[QtyOrWeightBreaks7]
           ,[QtyOrWeightBreaks8]
           ,[QtyOrWeightBreaks9]
           ,[QtyOrWeightBreaks10]
           ,[CompanyCode])
SELECT DISTINCT
		   ProntoCustomerRatesUpdateHeader.Id
           ,ProntoCustomerRates.[ZoneFrom]
           ,ProntoCustomerRates.[ZoneTo]
		   ,CAST(ProntoCustomerRates.Charges4 + (ProntoCustomerRates.Charges4 * ( ProntoCustomerUpratePercentPD.UpratePercent)) AS MONEY) MinimumCharge
		   ,CAST(ProntoCustomerRates.Charges5 + (ProntoCustomerRates.Charges5 * ( ProntoCustomerUpratePercentPD.UpratePercent)) AS MONEY) BasicCharge
		   ,CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge
		   ,ProntoCustomerRates.Charges1 FuelPercent
           ,[QtyOrWeightRounding1]
           ,[QtyOrWeightRounding2]
           ,[QtyOrWeightRounding3]
           ,[QtyOrWeightRounding4]
           ,[QtyOrWeightRounding5]
           ,[QtyOrWeightRounding6]
           ,[QtyOrWeightRounding7]
           ,[QtyOrWeightRounding8]
           ,[QtyOrWeightRounding9]
           ,[QtyOrWeightRounding10]
		   ,CAST(ProntoCustomerRates.QtyOrWeightCharges1 + (ProntoCustomerRates.QtyOrWeightCharges1 * ( ProntoCustomerUpratePercentPD.UpratePercent)) AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges2 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges2 + (ProntoCustomerRates.QtyOrWeightCharges2 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges2 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges3 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges3 + (ProntoCustomerRates.QtyOrWeightCharges3 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges3 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges4 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges4 + (ProntoCustomerRates.QtyOrWeightCharges4 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges4 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges5 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges5 + (ProntoCustomerRates.QtyOrWeightCharges5 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges5 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges6 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges6 + (ProntoCustomerRates.QtyOrWeightCharges6 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges6 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges7 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges7 + (ProntoCustomerRates.QtyOrWeightCharges7 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges7 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges8 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges8 + (ProntoCustomerRates.QtyOrWeightCharges8 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges8 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges9 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges9 + (ProntoCustomerRates.QtyOrWeightCharges9 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges9 END AS DECIMAL(18,5))
		   ,CAST(CASE WHEN ProntoCustomerRates.QtyOrWeightCharges10 < 9999.99 THEN ProntoCustomerRates.QtyOrWeightCharges10 + (ProntoCustomerRates.QtyOrWeightCharges10 * ( ProntoCustomerUpratePercentPD.UpratePercent)) ELSE ProntoCustomerRates.QtyOrWeightCharges10 END AS DECIMAL(18,5))
           ,[QtyOrWeightBreaks1]
           ,[QtyOrWeightBreaks2]
           ,[QtyOrWeightBreaks3]
           ,[QtyOrWeightBreaks4]
           ,[QtyOrWeightBreaks5]
           ,[QtyOrWeightBreaks6]
           ,[QtyOrWeightBreaks7]
           ,[QtyOrWeightBreaks8]
           ,[QtyOrWeightBreaks9]
           ,[QtyOrWeightBreaks10]
           ,ProntoCustomerRatesUpdateHeader.CompanyCode
    
FROM ProntoCustomerUpratePercentPD
JOIN ProntoCustomerRates ON ProntoCustomerRates.AccountCode = ProntoCustomerUpratePercentPD.AccountCode
JOIN ProntoCustomerRatesUpdateHeader ON ProntoCustomerRatesUpdateHeader.Id = 
	LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUpratePercentPD.AccountCode, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + ProntoCustomerRates.Service+'_'+REPLACE(CAST(CAST(@EffectiveDate AS DATE)AS VARCHAR(20)), '-', '')
		AND ProntoCustomerRates.CompanyCode = ProntoCustomerRatesUpdateHeader.CompanyCode

WHERE 
	ProntoCustomerRates.IsActive = 1  
	AND ProntoCustomerRates.IsCumulative = 0
	AND ProntoCustomerRates.CustomerOrSupplier = 'C'
	AND ProntoCustomerRates.AccountCode IS NOT NULL
GO
