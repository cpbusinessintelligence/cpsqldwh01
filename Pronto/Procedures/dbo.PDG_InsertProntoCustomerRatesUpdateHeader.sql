SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [PDG_InsertProntoCustomerRatesUpdateHeader] 
@CompanyCode VARCHAR(5) = 'CL1'

AS

SET NOCOUNT ON;

TRUNCATE TABLE [ProntoCustomerRatesUpdateHeader];

INSERT INTO [RDS].[dbo].[ProntoCustomerRatesUpdateHeader]
           ([AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id])
SELECT DISTINCT
ProntoCustomerRates.AccountCode AccountCode,
ProntoCustomerRates.[Service] [Service],
ProntoCustomerRates.FromDate,
CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge,
ProntoCustomerRates.Charges1 FuelPercent,
CASE ProntoCustomerRates.OverrideCharge2 WHEN 1 THEN 'Y' ELSE 'N' END OtherOverrideCharge,
ProntoCustomerRates.Charges2 OtherPercent,
ProntoCustomerRates.QtyOrWeightMethod,
CASE ProntoCustomerRates.QtyOrWeightMethod WHEN 'W' THEN ProntoCustomerRates.VolumeToWeightFactor ELSE 0 END VolumeToWeightFactor,
CASE WHEN PATINDEX('%_'+ProntoCustomerRates.[Service]+'_%', [Id]) > 0 THEN 
	SUBSTRING(Id, 1, PATINDEX('%_'+ProntoCustomerRates.[Service]+'_%', [Id])-1) + '_'+ProntoCustomerRates.[Service]+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
ELSE
CASE AccountCode WHEN NULL THEN 'CARD_'+ProntoCustomerRates.[Service]+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
ELSE AccountCode + ProntoCustomerRates.[Service]+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
END
END AS Id

FROM ProntoCustomerRates
JOIN ProntoCustomerUprateActions ON 
	(ISNULL(ProntoCustomerRates.AccountCode, 'CARD') = ISNULL(ProntoCustomerUprateActions.ProntoAccountNumber, 'CARD') ) AND
	ProntoCustomerRates.CustomerOrSupplier = ProntoCustomerUprateActions.C_S_D AND
	ProntoCustomerRates.Service = ProntoCustomerUprateActions.ExistingService
	AND ProntoCustomerUprateActions.ProntoDataChangeRequired = 1

LEFT JOIN LocationMaster ON ProntoCustomerRates.ZoneFrom = PrimaryZoneCode
WHERE ProntoCustomerRates.IsActive = 1  
AND ProntoCustomerRates.IsCumulative = 1
AND ProntoCustomerRates.CustomerOrSupplier = 'C'
AND ProntoCustomerRates.CompanyCode = @CompanyCode
GO
