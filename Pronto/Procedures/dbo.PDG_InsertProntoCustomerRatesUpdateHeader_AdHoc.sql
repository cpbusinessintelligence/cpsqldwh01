SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [PDG_InsertProntoCustomerRatesUpdateHeader_AdHoc] 

AS

WITH A
           ([AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id]
           ,CompanyCode) AS
(SELECT DISTINCT
Pronto_Billing_Account AccountCode ,
ProntoCustomerRates.[Service] [Service],
CAST('2010-09-06' AS DATE) AS FromDate,
CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge,
ProntoCustomerRates.Charges1 FuelPercent,
CASE ProntoCustomerRates.OverrideCharge2 WHEN 1 THEN 'Y' ELSE 'N' END OtherOverrideCharge,
ProntoCustomerRates.Charges2 OtherPercent,
ProntoCustomerRates.QtyOrWeightMethod,
CASE ProntoCustomerRates.QtyOrWeightMethod WHEN 'W' THEN ProntoCustomerRates.VolumeToWeightFactor ELSE 0 END VolumeToWeightFactor,
LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUprateActions.Pronto_Billing_Account, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + ProntoCustomerRates.[Service]+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '') AS Id
, CompanyCode
FROM ProntoCustomerUprateActions
JOIN ProntoCustomerRates ON ISNULL(ProntoCustomerRates.AccountCode, 'Card') = 
	CASE Existing_Rating_Type WHEN 'CARD' THEN 'Card' ELSE ISNULL(ProntoCustomerUprateActions.Pronto_Billing_Account, 'Card') END
	AND ProntoCustomerRates.Service = ProntoCustomerUprateActions.Existing_Service
	AND ProntoCustomerRates.IsActive = 1  
	AND ProntoCustomerRates.IsCumulative = 1
	AND ProntoCustomerRates.CustomerOrSupplier = 'C'

WHERE 
Pronto_Data_Change_Required = 1
AND New_Rating_Type = 'Adhoc'
AND	Execution_Timing = 'CPPL'
)

INSERT INTO [RDS].[dbo].[ProntoCustomerRatesUpdateHeader]
           ([AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id]
           ,CompanyCode)
SELECT * FROM A 
WHERE NOT EXISTS (SELECT 
           [AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id]
           ,CompanyCode FROM [ProntoCustomerRatesUpdateHeader] WHERE [ProntoCustomerRatesUpdateHeader].Id =  A.Id )
GO
