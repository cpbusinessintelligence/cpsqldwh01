SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [PDG_InsertProntoCustomerRatesUpdateHeader_CardToCard_PD] 
@EffectiveDate DATE = '2010-10-01'
AS

INSERT INTO [RDS].[dbo].[ProntoCustomerRatesUpdateHeader]
           ([AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id]
           ,CompanyCode)

SELECT DISTINCT
AccountCode,
[Service],
CAST(@EffectiveDate AS DATE) AS FromDate,
CASE OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge,
Charges1 FuelPercent,
CASE OverrideCharge2 WHEN 1 THEN 'Y' ELSE 'N' END OtherOverrideCharge,
Charges2 OtherPercent,
QtyOrWeightMethod,
CASE QtyOrWeightMethod WHEN 'W' THEN VolumeToWeightFactor ELSE 0 END VolumeToWeightFactor,
'CARD_'+ Service + '_'+REPLACE(CAST(CAST(@EffectiveDate AS DATE)AS VARCHAR(20)), '-', '') AS Id
,CompanyCode
FROM ProntoCustomerRates 
WHERE IsActive = 1  
	AND IsCumulative = 1
	AND CustomerOrSupplier = 'C'
	AND AccountCode IS NULL
AND CompanyCode IN ('PL1', 'WL1')
AND FromDate < '2010-09-01'
GO
