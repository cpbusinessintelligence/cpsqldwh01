SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC  [PDG_InsertProntoCustomerRatesUpdateHeader_CardToCustom] 

AS

INSERT INTO [RDS].[dbo].[ProntoCustomerRatesUpdateHeader]
           ([AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id]
           ,CompanyCode)

SELECT DISTINCT
ProntoCustomerUprateActions.Pronto_Billing_Account AccountCode,
New_Service [Service],
CAST('2010-09-06' AS DATE) AS FromDate,
'N', --CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge,
0, --ProntoCustomerRates.Charges1 FuelPercent,
CASE ProntoCustomerRates.OverrideCharge2 WHEN 1 THEN 'Y' ELSE 'N' END OtherOverrideCharge,
ProntoCustomerRates.Charges2 OtherPercent,
ProntoCustomerRates.QtyOrWeightMethod,
CASE ProntoCustomerRates.QtyOrWeightMethod WHEN 'W' THEN ProntoCustomerRates.VolumeToWeightFactor ELSE 0 END VolumeToWeightFactor,
CASE  
WHEN Pronto_Billing_Account IS NULL THEN 'CARD_'+New_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
ELSE
 LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUprateActions.Pronto_Billing_Account, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + New_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
END AS Id
, CompanyCode


FROM ProntoCustomerUprateActions
JOIN ProntoCustomerRates ON ProntoCustomerRates.Service = ProntoCustomerUprateActions.Existing_Service

WHERE 
ProntoCustomerRates.IsActive = 1  
AND ProntoCustomerRates.IsCumulative = 1
AND ProntoCustomerRates.CustomerOrSupplier = 'C'
AND ProntoCustomerRates.AccountCode IS NULL

AND Pronto_Data_Change_Required = 1
AND Existing_Rating_Type = 'Card'
AND New_Rating_Type = 'Custom'
AND	Execution_Timing = 'CPPL'

GO
