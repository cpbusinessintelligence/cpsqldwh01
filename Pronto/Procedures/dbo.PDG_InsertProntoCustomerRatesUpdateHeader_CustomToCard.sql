SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [PDG_InsertProntoCustomerRatesUpdateHeader_CustomToCard] 

AS

INSERT INTO [RDS].[dbo].[ProntoCustomerRatesUpdateHeader]
           ([AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id]
           ,CompanyCode)

SELECT DISTINCT
ProntoCustomerUprateActions.Pronto_Billing_Account AccountCode,
ProntoCustomerRates.[Service] [Service],
CAST('2010-09-06' AS DATE) AS FromDate,
CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge,
ProntoCustomerRates.Charges1 FuelPercent,
CASE ProntoCustomerRates.OverrideCharge2 WHEN 1 THEN 'Y' ELSE 'N' END OtherOverrideCharge,
ProntoCustomerRates.Charges2 OtherPercent,
ProntoCustomerRates.QtyOrWeightMethod,
CASE ProntoCustomerRates.QtyOrWeightMethod WHEN 'W' THEN ProntoCustomerRates.VolumeToWeightFactor ELSE 0 END VolumeToWeightFactor,
CASE  
WHEN AccountCode IS NULL THEN 'CARD_'+ProntoCustomerUprateActions.Existing_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
ELSE
 LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUprateActions.Pronto_Billing_Account, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + ProntoCustomerUprateActions.Existing_Service+'_'+REPLACE(CAST(CAST('2010-09-06' AS DATE)AS VARCHAR(20)), '-', '')
END AS Id
, CompanyCode
FROM ProntoCustomerUprateActions
JOIN ProntoCustomerRates ON ISNULL(ProntoCustomerRates.AccountCode, 'Card') = ISNULL(ProntoCustomerUprateActions.Pronto_Billing_Account, 'Card')
	AND ProntoCustomerRates.Service = ProntoCustomerUprateActions.Existing_Service
	AND ProntoCustomerRates.IsActive = 1  
	AND ProntoCustomerRates.IsCumulative = 1
	AND ProntoCustomerRates.CustomerOrSupplier = 'C'
	AND ProntoCustomerRates.AccountCode IS NOT NULL

WHERE Pronto_Data_Change_Required = 1
AND Existing_Rating_Type = 'Custom'
AND New_Rating_Type = 'Card'
AND	Execution_Timing = 'CPPL'
ORDER BY Id 
GO
