SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC  [PDG_InsertProntoCustomerRatesUpdateHeader_CustomToCustom_PD] 
@EffectiveDate DATE = '2010-10-01'

AS

INSERT INTO [RDS].[dbo].[ProntoCustomerRatesUpdateHeader]
           ([AccountCode]
           ,[Service]
           ,[FromDate]
           ,[FuelOverrideCharge]
           ,[FuelPercent]
           ,[OtherOverrideCharge]
           ,[OtherPercent]
           ,[QtyOrWeightMethod]
           ,[VolumeToWeightFactor]
           ,[Id]
           ,CompanyCode)

SELECT DISTINCT
ProntoCustomerUpratePercentPD.AccountCode,
ProntoCustomerRates.[Service] [Service],
CAST(@EffectiveDate AS DATE) AS FromDate,
CASE ProntoCustomerRates.OverrideCharge1 WHEN 1 THEN 'Y' ELSE 'N' END FuelOverrideCharge,
ProntoCustomerRates.Charges1 FuelPercent,
CASE ProntoCustomerRates.OverrideCharge2 WHEN 1 THEN 'Y' ELSE 'N' END OtherOverrideCharge,
ProntoCustomerRates.Charges2 OtherPercent,
ProntoCustomerRates.QtyOrWeightMethod,
CASE ProntoCustomerRates.QtyOrWeightMethod WHEN 'W' THEN ProntoCustomerRates.VolumeToWeightFactor ELSE 0 END VolumeToWeightFactor,
LEFT(UPPER(REPLACE(REPLACE(REPLACE(REPLACE(ProntoCustomerUpratePercentPD.AccountCode, ' ', ''), ')', ''), '(', ''), '-', '')), 10) + '_' + ProntoCustomerRates.[Service]+'_'+REPLACE(CAST(CAST(@EffectiveDate AS DATE)AS VARCHAR(20)), '-', '')AS Id
, CompanyCode
FROM ProntoCustomerUpratePercentPD
JOIN ProntoCustomerRates ON ProntoCustomerRates.AccountCode = ProntoCustomerUpratePercentPD.AccountCode

WHERE 
ProntoCustomerRates.IsActive = 1  
AND ProntoCustomerRates.IsCumulative = 1
AND ProntoCustomerRates.CustomerOrSupplier = 'C'
AND ProntoCustomerRates.AccountCode IS NOT NULL


GO
