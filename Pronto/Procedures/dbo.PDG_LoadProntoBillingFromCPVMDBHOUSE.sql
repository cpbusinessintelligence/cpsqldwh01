SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[PDG_LoadProntoBillingFromCPVMDBHOUSE]    Script Date: 08/21/2009 14:07:09 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
CREATE PROC [dbo].[PDG_LoadProntoBillingFromCPVMDBHOUSE] as
BEGIN

	DBCC SHRINKFILE(RDS_log, 1);

SET NOCOUNT ON;
--TRUNCATE TABLE [RDS].[dbo].[ProntoBilling];

INSERT INTO [RDS].[dbo].[ProntoBilling]WITH(TABLOCK)
(
	[OrderNumber]
	,[OrderSuffix]
	,[OrderId]
	,[ConsignmentReference]
	,[AccountCode]
	,[AccountName]
	,[AccountBillToCode]
	,[AccountBillToName]
	,[ServiceCode]
	,[CompanyCode]
	,[AccountingPeriod]
	,[AccountingWeek]
	,[BillingDate]
	,[AccountingDate]
	,[InvoiceNumber]
	,[OriginLocality]
	,[OriginPostcode]
	,[ReceiverName]
	,[DestinationLocality]
	,[DestinationPostcode]
	,[ConsignmentDate]
	,[ManifestReference]
	,[ManifestDate]
	,[CustomerReference]
	,[LogisticsUnitsQuantity]
	,[ItemQuantity]
	,[DeadWeight]
	,[Volume]
	,[ChargeableWeight]
	,[LinehaulWeight]
	,[InsuranceCategory]
	,[InsuranceDeclaredValue]
	,[InsurancePriceOverride]
	,[TestFlag]
	,[RevenueBusinessUnit]
	,[TariffId]
	,[RevenueOriginZone]
	,[RevenueDestinationZone]
	,[CalculatedFreightCharge]
	,[CalculatedFuelSurcharge]
	,[CalculatedTransportCharge]
	,[PriceOverride]
	,[BilledFreightCharge]
	,[BilledFuelSurcharge]
	,[BilledTransportCharge]
	,[BilledInsurance]
	,[BilledOtherCharge]
	,[BilledTotal]
	,[CardRateFreightCharge]
	,[CardRateFuelSurcharge]
	,[CardRateTransportCharge]
	,[CardRateDiscountOff]
	,[PickupCost]
	,[PickupSupplier]
	,[PickupZone]
	,[LinehaulCost]
	,[LinehaulRoute]
	,[DeliveryCost]
	,[DeliverySupplier]
	,[DeliveryZone]
	,[TotalPUDCost]
	,[MarginTransportcharge]
)

SELECT DISTINCT CAST([Order-no] AS bigint)
      ,CAST([Order-suffix] as varchar(5)) COLLATE Latin1_General_CI_AS 
      ,CAST([Order-ID] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Consignment-ID] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Account-code] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([customer-name] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Account-billto-code] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([BillTo-name] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Service-code] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([Database-code] as varchar(5)) COLLATE Latin1_General_CI_AS
      ,CAST([Accounting-period] as varchar(6)) COLLATE Latin1_General_CI_AS
      ,CAST([Accounting-week] as varchar(6)) COLLATE Latin1_General_CI_AS
      ,CAST([Billing-date] as date)
      ,CAST([Accounting-date] as date)
      ,CAST([Invoice-number] as varchar(8)) COLLATE Latin1_General_CI_AS
      ,CAST([Origin-locality] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Origin-postcode] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([Receiver-name] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Destination-locality] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Destination-postcode] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([Consignment-date] as date)
      ,CAST([Manifest-ID] as varchar(25)) COLLATE Latin1_General_CI_AS
      ,CAST([Manifest-date] as date)
      ,CAST([Customer-reference] as varchar(5)) COLLATE Latin1_General_CI_AS
      ,CAST([Logisticsunits-quantity] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Item-quantity] as float)
      ,CAST([Dead-weight] as float)
      ,CAST([Volume] as float)
      ,CAST([Chargeable-weight] as float)
      ,CAST([Linehaul-weight] as float)
      ,CAST([Insurance-category] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([Insurance-declared-value] as money)
      ,CAST([Insurance-price-override] as money)
      ,CAST([Test-flag] as bit)
      ,CAST([Revenue-BU] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Tariff-ID] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Revenue-origin-zone] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Revenue-destination-zone] as varchar(50)) COLLATE Latin1_General_CI_AS
      ,CAST([Calculated-freightcharge] as money)
      ,CAST([Calculated-fuelsurcharge] as money)
      ,CAST([Calculated-transportcharge] as money)
      ,CAST([Price-override] as money)
      ,CAST([Billed-freightcharge] as money)
      ,CAST([Billed-fuelsurcharge] as money)
      ,CAST([Billed-transportcharge] as money)
      ,CAST([Billed-insurance] as money)
      ,CAST([Billed-othercharge] as money)
      ,CAST([Billed-total] as money)
      ,CAST([Card-rate-freightcharge] as money)
      ,CAST([Card-rate-fuelsurcharge] as money)
      ,CAST([Card-rate-transport] as money)
      ,CAST([Card-rate-discountoff] as money)
      ,CAST([Pickup-cost] as money)
      ,CAST([Pickup-supplier] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([Pickup-zone] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([Linehaul-cost] as money)
      ,CAST([Linehaul-route] as money)
      ,CAST([Delivery-cost] as money)
      ,CAST([Delivery-supplier] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([Delivery-zone] as varchar(10)) COLLATE Latin1_General_CI_AS
      ,CAST([PUD-cost] as money)
      ,CAST([Margin-transportcharge] as money)
	FROM [ECA_DW_v01_TEMP].dbo.[fact-zbilling]
EXCEPT
SELECT	[OrderNumber] 
	,[OrderSuffix]
	,[OrderId] 
	,[ConsignmentReference] 
	,[AccountCode] 
	,[AccountName] 
	,[AccountBillToCode] 
	,[AccountBillToName] 
	,[ServiceCode] 
	,[CompanyCode] 
	,[AccountingPeriod] 
	,[AccountingWeek] 
	,[BillingDate] 
	,[AccountingDate] 
	,[InvoiceNumber] 
	,[OriginLocality] 
	,[OriginPostcode] 
	,[ReceiverName] 
	,[DestinationLocality] 
	,[DestinationPostcode] 
	,[ConsignmentDate] 
	,[ManifestReference] 
	,[ManifestDate] 
	,[CustomerReference] 
	,[LogisticsUnitsQuantity] 
	,[ItemQuantity]  
	,[DeadWeight]   
	,[Volume]  
	,[ChargeableWeight] 
	,[LinehaulWeight]  
	,[InsuranceCategory] 
	,[InsuranceDeclaredValue] 
	,[InsurancePriceOverride]  
	,[TestFlag] 
	,[RevenueBusinessUnit] 
	,[TariffId] 
	,[RevenueOriginZone] 
	,[RevenueDestinationZone] 
	,[CalculatedFreightCharge] 
	,[CalculatedFuelSurCharge] 
	,[CalculatedTransportCharge] 
	,[PriceOverride]  
	,[BilledFreightCharge] 
	,[BilledFuelSurCharge] 
	,[BilledTransportCharge] 
	,[BilledInsurance]  
	,[BilledOtherCharge] 
	,[BilledTotal]
	,[CardRateFreightCharge] 
	,[CardRateFuelSurCharge] 
	,[CardRateTransportCharge] 
	,[CardRateDiscountOff]
	,[PickupCost] 
	,[PickupSupplier] 
	,[PickupZone] 
	,[LinehaulCost] 
	,[LinehaulRoute] 
	,[DeliveryCost] 
	,[DeliverySupplier] 
	,[DeliveryZone] 
	,[TotalPUDCost]
	,[MarginTransportcharge] 
FROM [RDS].[dbo].[ProntoBilling];

SET NOCOUNT OFF;

	DBCC SHRINKFILE(RDS_log, 1);

END
GO
