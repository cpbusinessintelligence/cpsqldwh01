SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PDG_LoadProntoCouponDetails_VERSION2]
AS
BEGIN

--EXEC [dbo].[PDG_LoadProntoCouponDetails_VERSION2]

	SET NOCOUNT ON;

	DBCC SHRINKFILE(RDS_log, 1);
	
    BEGIN TRY
    
		/****** Object:  Index [PK_ProntoCouponDetails_1]    Script Date: 05/25/2011 20:00:06 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoCouponDetails]') AND name = N'PK_ProntoCouponDetails_1')
		ALTER TABLE [dbo].[ProntoCouponDetails] DROP CONSTRAINT [PK_ProntoCouponDetails_1];

		/****** Object:  Index [idx_ProntoCouponDetails_LastRef]    Script Date: 07/11/2012 09:57:22 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoCouponDetails]') AND name = N'idx_ProntoCouponDetails_LastRef')
		DROP INDEX [idx_ProntoCouponDetails_LastRef] ON [dbo].[ProntoCouponDetails] WITH ( ONLINE = OFF )

		/****** Object:  Index [IX_ProntoCouponDetails_Pickup_RCTI]    Script Date: 05/25/2011 19:59:47 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoCouponDetails]') AND name = N'IX_ProntoCouponDetails_Pickup_RCTI')
		DROP INDEX [IX_ProntoCouponDetails_Pickup_RCTI] ON [dbo].[ProntoCouponDetails] WITH ( ONLINE = OFF );

		/****** Object:  Index [IX_ProntoCouponDetails_Delivery_RCTI]    Script Date: 05/25/2011 19:59:42 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoCouponDetails]') AND name = N'IX_ProntoCouponDetails_Delivery_RCTI')
		DROP INDEX [IX_ProntoCouponDetails_Delivery_RCTI] ON [dbo].[ProntoCouponDetails] WITH ( ONLINE = OFF );

		DBCC SHRINKFILE(RDS_log, 1);

		/* BEGIN TRAN InsertDetails */
			TRUNCATE TABLE [RDS].[dbo].[ProntoCouponDetails];
			
			--temp table
			SELECT
					LEFT([SerialNumber], 20) AS [SerialNumber]
					,LEFT([StartSerialNumber], 20) AS [StartSerialNumber]
					,LEFT([ReturnSerialNumber], 20) AS [ReturnSerialNumber]
					,LTRIM(RTRIM([TransferReference])) AS col1
					,CONVERT(DATE, [TransferDate], 113) AS [TransferDate]
					,LTRIM(RTRIM([LastSoldReference])) AS col2
					,CONVERT(DATE, [LastSoldDate], 113) AS [LastSoldDate]
					,LEFT([InsuranceCategory], 10) AS [InsuranceCategory]
					,CAST([InsuranceAmount] AS [money]) AS [InsuranceAmount]
					,LEFT([PickupContractor], 10) AS [PickupContractor]
					,CONVERT(DATE, [PickupDate], 113) AS [PickupDate]
					,CONVERT(DATE, [PickupRctiDate], 113) AS [PickupRctiDate]
					,[PickupRctiReference]
					,CAST([PickupRctiAmount] AS [money]) AS [PickupRctiAmount]
					,LEFT([DeliveryContractor], 10) AS [DeliveryContractor]
					,CONVERT(DATE, [DeliveryDate] , 113) AS [DeliveryDate]
					,CONVERT(DATE, [DeliveryRctiDate], 113) AS [DeliveryRctiDate]
					,[DeliveryRctiReference]
					,CAST([DeliveryRctiAmount] AS [money]) AS [DeliveryRctiAmount]
					,LEFT([BillingAccountCode], 11) AS [BillingAccountCode]
					,[BillingService]
					,[RevenueBatchReference]
					,[RevenueTransactionNumber]
					,CONVERT(DATE, [RevenueDate], 113) AS [RevenueDate]
					,CAST(REPLACE(REPLACE([RevenueAmount], 0x0D, ''), 0x0A, '') AS [money])[RevenueAmount]
					, CONVERT(datetime, [PickupProcessedDate]) AS [PickupProcessedDate]
					, CONVERT(datetime, REPLACE(REPLACE([DeliveryProcessedDate], 0x0D, ''), 0x0A, '')) AS [DeliveryProcessedDate]
				--SELECT COUNT(*)
			INTO #insert	
			FROM [RDS].[dbo].[Load_ProntoCouponDetails] (NOLOCK);
			--
			
			
			 --select count(*) from #insert	
			
			INSERT INTO [RDS].[dbo].[ProntoCouponDetails] WITH(TABLOCK)
				(
				[SerialNumber]
				,[StartSerialNumber]
				,[ReturnSerialNumber]
				,[TransferReference]
				,[TransferDate]
				,[LastSoldReference]
				,[LastSoldDate]
				,[InsuranceCategory]
				,[InsuranceAmount]
				,[PickupContractor]
				,[PickupDate]
				,[PickupRctiDate]
				,[PickupRctiReference]
				,[PickupRctiAmount]
				,[DeliveryContractor]
				,[DeliveryDate]
				,[DeliveryRctiDate]
				,[DeliveryRctiReference]
				,[DeliveryRctiAmount]
				,[BillingAccountCode]
				,[BillingService]
				,[RevenueBatchReference]
				,[RevenueTransactionNumber]
				,[RevenueDate]
				,[RevenueAmount]
				,[PickupProcessedDate] 
				,[DeliveryProcessedDate] 
				)
				SELECT * FROM #insert
/*
				SELECT
					LEFT([SerialNumber], 20) AS [SerialNumber]
					,LEFT([StartSerialNumber], 20) AS [StartSerialNumber]
					,LEFT([ReturnSerialNumber], 20) AS [ReturnSerialNumber]
					,LTRIM(RTRIM([TransferReference]))
					,CONVERT(DATE, [TransferDate], 113) AS [TransferDate]
					,LTRIM(RTRIM([LastSoldReference]))
					,CONVERT(DATE, [LastSoldDate], 113) AS [LastSoldDate]
					,LEFT([InsuranceCategory], 10) AS [InsuranceCategory]
					,CAST([InsuranceAmount] AS [money]) AS [InsuranceAmount]
					,LEFT([PickupContractor], 10) AS [PickupContractor]
					,CONVERT(DATE, [PickupDate], 113) AS [PickupDate]
					,CONVERT(DATE, [PickupRctiDate], 113) AS [PickupRctiDate]
					,[PickupRctiReference]
					,CAST([PickupRctiAmount] AS [money]) AS [PickupRctiAmount]
					,LEFT([DeliveryContractor], 10) AS [DeliveryContractor]
					,CONVERT(DATE, [DeliveryDate] , 113) AS [DeliveryDate]
					,CONVERT(DATE, [DeliveryRctiDate], 113) AS [DeliveryRctiDate]
					,[DeliveryRctiReference]
					,CAST([DeliveryRctiAmount] AS [money]) AS [DeliveryRctiAmount]
					,LEFT([BillingAccountCode], 11) AS [BillingAccountCode]
					,[BillingService]
					,[RevenueBatchReference]
					,[RevenueTransactionNumber]
					,CONVERT(DATE, [RevenueDate], 113) AS [RevenueDate]
					,CAST(REPLACE(REPLACE([RevenueAmount], 0x0D, ''), 0x0A, '') AS [money])[RevenueAmount]
					, CONVERT(datetime, [PickupProcessedDate]) AS [PickupProcessedDate]
					, CONVERT(datetime, REPLACE(REPLACE([DeliveryProcessedDate], 0x0D, ''), 0x0A, '')) AS [DeliveryProcessedDate]
				--SELECT COUNT(*)
				FROM [RDS].[dbo].[Load_ProntoCouponDetails] (NOLOCK);
*/				
				TRUNCATE TABLE [RDS].[dbo].[Load_ProntoCouponDetails];
			/* COMMIT TRAN InsertDetails */

			DBCC SHRINKFILE(RDS_log, 1);

			/****** Object:  Index [PK_ProntoCouponDetails_1]    Script Date: 05/25/2011 20:00:06 ******/
			ALTER TABLE [dbo].[ProntoCouponDetails] ADD  CONSTRAINT [PK_ProntoCouponDetails_1] PRIMARY KEY NONCLUSTERED 
			(
				[SerialNumber] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

			DBCC SHRINKFILE(RDS_log, 1);

			/****** Object:  Index [IX_ProntoCouponDetails_Pickup_RCTI]    Script Date: 05/25/2011 19:59:47 ******/
			CREATE NONCLUSTERED INDEX [IX_ProntoCouponDetails_Pickup_RCTI] ON [dbo].[ProntoCouponDetails] 
			(
				[PickupContractor] ASC,
				[PickupRctiDate] ASC,
				[PickupRctiReference] ASC
			)
			INCLUDE ( [SerialNumber],
			[PickupRctiAmount]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY];

			DBCC SHRINKFILE(RDS_log, 1);

			/****** Object:  Index [IX_ProntoCouponDetails_Delivery_RCTI]    Script Date: 05/25/2011 19:59:42 ******/
			CREATE NONCLUSTERED INDEX [IX_ProntoCouponDetails_Delivery_RCTI] ON [dbo].[ProntoCouponDetails] 
			(
				[DeliveryContractor] ASC,
				[DeliveryRctiDate] ASC,
				[DeliveryRctiReference] ASC
			)
			INCLUDE ( [SerialNumber],
			[DeliveryRctiAmount]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY];

			DBCC SHRINKFILE(RDS_log, 1);

			/****** Object:  Index [idx_ProntoCouponDetails_LastRef]    Script Date: 07/11/2012 09:57:22 ******/
			CREATE NONCLUSTERED INDEX [idx_ProntoCouponDetails_LastRef] ON [dbo].[ProntoCouponDetails] 
			(
				[LastSoldReference] ASC
			)
			INCLUDE ( [PickupDate],
			[DeliveryDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

			DBCC SHRINKFILE(RDS_log, 1);

			/*
			 *
			 * 03/01/2012 - Craig Parris
			 *
			 * Disable this report processing for now
			--
			-- now populate the RCTI report & consignment-related data
			--
			-- we go back 30 days by default, just to catch any past records
			-- that might not have been updated yet
			--
			EXEC dbo.PDG_PopulateContractorRCTIReportInitialData 30;

			DBCC SHRINKFILE(RDS_log, 1);
			--
			-- 31 days for the consignment data
			--
			EXEC dbo.PDG_PopulateContractorRCTIReportConsignmentData 31;
			 *
			 */

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC PDG_LogRethrowError;
		
		--WHILE @@TRANCOUNT > 0
		--BEGIN
		--	ROLLBACK WORK;
		--END
		
	END CATCH
	
	DBCC SHRINKFILE(RDS_log, 1);

END;
GO
