SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC PDG_LoadProntoCustomerRates_Initial
as
DECLARE @bulk_cmd varchar(1000)

TRUNCATE TABLE ProntoCustomerRates;

TRUNCATE TABLE Load_ProntoCustomerRates;
SET @bulk_cmd = 'BULK INSERT Load_ProntoCustomerRates 
FROM ''D:\Temp\cl1_zaccount_service_zone_rates.txt'' 
WITH (FIELDTERMINATOR = ''|'', ROWTERMINATOR = '''+CHAR(10)+''')'
EXEC(@bulk_cmd);
EXEC PDG_LoadProntoCustomerRates_cl1;

TRUNCATE TABLE Load_ProntoCustomerRates;
SET @bulk_cmd = 'BULK INSERT Load_ProntoCustomerRates 
FROM ''D:\Temp\pl1_zaccount_service_zone_rates.txt'' 
WITH (FIELDTERMINATOR = ''|'', ROWTERMINATOR = '''+CHAR(10)+''')'
EXEC(@bulk_cmd);
EXEC PDG_LoadProntoCustomerRates_pl1;

TRUNCATE TABLE Load_ProntoCustomerRates;
SET @bulk_cmd = 'BULK INSERT Load_ProntoCustomerRates 
FROM ''D:\Temp\wl1_zaccount_service_zone_rates.txt'' 
WITH (FIELDTERMINATOR = ''|'', ROWTERMINATOR = '''+CHAR(10)+''')'
EXEC(@bulk_cmd);
EXEC PDG_LoadProntoCustomerRates_wl1;
GO
