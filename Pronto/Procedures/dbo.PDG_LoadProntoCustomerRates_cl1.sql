SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[PDG_LoadProntoCustomerRates_cl1]
AS
BEGIN
	SET NOCOUNT ON;

    BEGIN TRY

		DELETE FROM [RDS].[dbo].[ProntoCustomerRates] WHERE CompanyCode = 'CL1';

		INSERT INTO [RDS].[dbo].[ProntoCustomerRates] 
		SELECT DISTINCT
		     'CL1'
		    ,[CustomerOrSupplier]
			,[AccountCode]
			,[Service]
			,[ZoneFrom]
			,[ZoneTo]
			,CAST([FromDate] AS DATE)
			,[QtyOrWeightMethod]
			,CAST([QtyOrWeightRounding1] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding2] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding3] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding4] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding5] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding6] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding7] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding8] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding9] AS DECIMAL(10,2))
			,CAST([QtyOrWeightRounding10] AS DECIMAL(10,2))
			,CAST([QtyOrWeightCharges1] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges2] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges3] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges4] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges5] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges6] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges7] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges8] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges9] AS DECIMAL(18,5))
			,CAST([QtyOrWeightCharges10] AS DECIMAL(18,5))
			,CAST([QtyOrWeightBreaks1] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks2] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks3] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks4] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks5] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks6] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks7] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks8] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks9] AS DECIMAL(18,2))
			,CAST([QtyOrWeightBreaks10] AS DECIMAL(18,2))
			,CAST([Charges1] AS DECIMAL(18,3))
			,CAST([Charges2] AS DECIMAL(18,3))
			,CAST([Charges3] AS DECIMAL(18,3))
			,CAST([Charges4] AS DECIMAL(18,3))
			,CAST([Charges5] AS DECIMAL(18,3))
			,CAST([Charges6] AS DECIMAL(18,3))
			,CAST([Charges7] AS DECIMAL(18,3))
			,CAST([Charges8] AS DECIMAL(18,3))
			,CAST([Charges9] AS DECIMAL(18,3))
			,CAST([Charges10] AS DECIMAL(18,3))
			,CASE [IsCumulative] WHEN 'Y' THEN 1 ELSE 0 END
			,CAST([VolumeToWeightFactor] AS DECIMAL(10,2))
			,CAST([LogDate] AS DATETIME)
			,[LogWho]
			,[Id]
			,CASE [OverrideCharge1] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge2] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge3] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge4] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge5] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge6] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge7] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge8] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE [OverrideCharge9] WHEN 'Y' THEN 1 ELSE 0 END
			,CASE REPLACE(REPLACE([OverrideCharge10], 0x0D, ''), 0x0A, '') WHEN 'Y' THEN 1 ELSE 0 END
			,0
		FROM [RDS].[dbo].[Load_ProntoCustomerRates];

WITH B (CompanyCode
	  ,CustomerOrSupplier
	  ,[AccountCode]
      ,[Service]
      ,[ZoneFrom]
      ,[ZoneTo]
      , MaxDate)
AS
(
SELECT CompanyCode
	  ,ISNULL([CustomerOrSupplier], '')
	  ,ISNULL([AccountCode], '')
      ,ISNULL([Service], '')
      ,ISNULL([ZoneFrom], '')
      ,ISNULL([ZoneTo], '')
      ,MAX([FromDate]) MaxDate
  FROM [RDS].[dbo].[ProntoCustomerRates]
  GROUP BY CompanyCode
	  ,[CustomerOrSupplier]
	  ,[AccountCode]
      ,[Service]
      ,[ZoneFrom]
      ,[ZoneTo]
)
UPDATE [ProntoCustomerRates] SET IsActive = 1
From [ProntoCustomerRates], B
WHERE B.CompanyCode = ProntoCustomerRates.CompanyCode  
AND B.CustomerOrSupplier = ISNULL([ProntoCustomerRates].CustomerOrSupplier, '')
AND B.AccountCode =	ISNULL([ProntoCustomerRates].AccountCode, '')
AND B.Service = ISNULL([ProntoCustomerRates].Service, '')
AND B.ZoneFrom = ISNULL([ProntoCustomerRates].ZoneFrom, '')
AND B.ZoneTo = ISNULL([ProntoCustomerRates].ZoneTo, '')
AND B.MaxDate = [ProntoCustomerRates].FromDate 

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC PDG_LogRethrowError;
		
	END CATCH
END;

GO
