SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Tejes Singam
-- Create date: 15/12/2017
-- Description:	Create the Freight anlysis report For 13 months 
-- =============================================
Create PROCEDURE [dbo].[Sp_RptFreightAnalysisFor13Month_BasedOnMonth_BKP_HB_20180608] (@AccountCode varchar(500),@Date Date)

WITH RECOMPILE
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
SET NOCOUNT ON;
SET FMTONLY OFF;

--DECLARE @AccountCode varchar(30)
--DECLARE @Date Date

DECLARE @StartDate Date
DECLARE @EndDate Date

--SET @AccountCode='113058192'
--SET @Date='2017-11-15'

SET @StartDate = convert(date,dateadd(month, datediff(month, 0,@Date) - 13, 0))
SET @EndDate = EOMONTH (@Date,-1) 


SELECT distinct [AccountCode]
	  ,[AccountingDate]
	  ,convert(varchar(10),'') as SalesYear
	  ,convert(varchar(10),'') as SalesMonth
	  --,convert(varchar(10),'') as [MonthName]
      ,[ItemQuantity] 
	  ,[ConsignmentReference] 
      ,[ChargeableWeight] 
      ,[BilledTotal] 
  into #Temp
  FROM [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
  Where [AccountCode] IN (select *from dbo.fnSplitString(@AccountCode,','))  and [AccountingDate] >= @StartDate and [AccountingDate] <= @EndDate  and ReceiverName <> 'Iconic C/O Seko'
 

  --UPDATE T SET AccountName= PD.ShortName from [Pronto].[dbo].[ProntoDebtor] PD Join #Temp T on PD.AccountCode=T.AccountCode

  --UPDATE T SET SalesYear = CASE WHEN MONTH([AccountingDate])>6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
	 --       WHEN MONTH([AccountingDate])<=6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
		--	END from #Temp T

UPDATE T SET SalesYear = CASE WHEN Month([AccountingDate]) BETWEEN 4 AND 12
                THEN CONVERT(VARCHAR(4),YEAR([AccountingDate])) + '/' + RIGHT(CONVERT(VARCHAR(4),YEAR([AccountingDate]) + 1),2)
            WHEN Month([AccountingDate]) BETWEEN 1 AND 3
                THEN CONVERT(VARCHAR(4),YEAR([AccountingDate]) - 1) + '/' + RIGHT(CONVERT(VARCHAR(4),YEAR([AccountingDate])),2)
            End from #Temp T

  UPDATE T SET SalesMonth = MONTH([AccountingDate]) from #Temp T

  --UPDATE T SET [MonthName]= DateName(mm,[AccountingDate]) from #Temp T


  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT [SalesYear]
      ,[SalesMonth]
	  --,convert(varchar(10),'') as [MonthName]
      ,SUM([ItemQuantity]) as TotalItems
      ,COUNT([ConsignmentReference]) as TotalConsignments
      ,SUM([ChargeableWeight]) as TotalChargeableWeight
      ,SUM([BilledTotal]) as TotalBilled
	  --,convert(decimal(18,2),AVG([ChargeableWeight])) as AverageChargeableWeight
	  --,convert(decimal(18,2),AVG([BilledTotal])) as AverageBilledTotal
	  ,convert(decimal(18,2),SUM([BilledTotal])/NULLIF(COUNT([ConsignmentReference]),0)) as AvgDollarsPerConsignment
	  ,convert(decimal(18,2),SUM([BilledTotal])/NULLIF(SUM([ItemQuantity]),0)) as AvgDollarsPerItem
	  ,convert(decimal(18,2),SUM ([ItemQuantity])/NULLIF(count([ConsignmentReference]),0)) as AvgItemsPerConsignment
	  ,convert(decimal(18,2),SUM ([ChargeableWeight])/NULLIF(count([ConsignmentReference]),0)) as AvgWeightPerConnote
	  ,convert(decimal(18,2),SUM ([ChargeableWeight])/NULLIF(SUM ([ItemQuantity]),0)) as AvgWeightPerItem
  into #Temp1
  FROM #Temp
  GROUP BY [SalesYear]
      ,[SalesMonth]
  ORDER BY [SalesYear] asc
      ,[SalesMonth] asc


--UPDATE T SET [MonthName]= DateName( month,DateAdd(month,[SalesMonth], 0 ) - 1 ) from #Temp1 T


Select * from #Temp1 

--ORDER BY [SalesYear] asc,len([SalesMonth]),[SalesMonth] asc


END


GO
