SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptFreightAnalysisFor13Months_BasedOnState] (@AccountCode varchar(500),@Date Date)

WITH RECOMPILE
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;


DECLARE @StartDate Date
DECLARE @EndDate Date


SET @StartDate = convert(date,dateadd(month, datediff(month, 0,@Date) - 13, 0))
SET @EndDate = EOMONTH (@Date,-1) 


SELECT distinct [AccountCode]
      ,p.[DestinationPostcode]
	  ,[AccountingDate]
	  ,convert(varchar(10),'') as [State]
	  ,convert(varchar(10),'') as [SalesYear]
	  ,convert(varchar(10),'') as [SalesMonth]
      ,[ItemQuantity] 
	  ,[ConsignmentReference] 
      ,[ChargeableWeight] 
      ,[BilledTotal] 
  into #Temp
  FROM [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
  Where [AccountCode] IN (select *from dbo.fnSplitString(@AccountCode,','))  and [AccountingDate] >= @StartDate and [AccountingDate] <= @EndDate  and ReceiverName <> 'Iconic C/O Seko'

  UPDATE T  SET State= P1.State from PerformanceReporting.[dbo].[PostCodes] P1 (NOLOCK) join #Temp T on T.[DestinationPostcode] = P1.PostCode 


  UPDATE T SET SalesYear = CASE WHEN Month([AccountingDate]) BETWEEN 4 AND 12
                THEN CONVERT(VARCHAR(4),YEAR([AccountingDate])) + '/' + RIGHT(CONVERT(VARCHAR(4),YEAR([AccountingDate]) + 1),2)
            WHEN Month([AccountingDate]) BETWEEN 1 AND 3
                THEN CONVERT(VARCHAR(4),YEAR([AccountingDate]) - 1) + '/' + RIGHT(CONVERT(VARCHAR(4),YEAR([AccountingDate])),2)
            End from #Temp T

  UPDATE T SET SalesMonth = MONTH([AccountingDate]) from #Temp T

  --UPDATE T SET [MonthName]= DateName(mm,[AccountingDate]) from #Temp T


  select [AccountCode]
        ,[State]
        ,SalesYear
		,SalesMonth
		,SUM([ItemQuantity]) as ItemsCount
		,count([ConsignmentReference]) as ConsignmentCount
		,SUM([ChargeableWeight]) as TotalWeight
		,SUM([BilledTotal]) as FreightTotalCost
	    --,convert(decimal(18,2),SUM([BilledTotal])/NULLIF(count([ConsignmentReference]),0)) as AvgDollarsPerConnote
	    --,convert(decimal(18,2),SUM([BilledTotal])/NULLIF(SUM([ItemQuantity]),0)) as AvgDollarsPerItem
	    --,convert(decimal(18,2),SUM ([ItemQuantity])/NULLIF(count([ConsignmentReference]),0)) as AvgItemsPerConsignment
	    --,convert(decimal(18,2),SUM ([ChargeableWeight])/NULLIF(count([ConsignmentReference]),0)) as AvgWeightPerConnote
	    --,convert(decimal(18,2),SUM ([ChargeableWeight])/NULLIF(SUM ([ItemQuantity]),0)) as AvgWeightPerItem
  from #Temp
  Group By [AccountCode]
        ,[State]
        ,[SalesYear]
		,[SalesMonth]
  ORDER BY SalesYear
		  ,SalesMonth desc

END
GO
GRANT ALTER
	ON [dbo].[Sp_RptFreightAnalysisFor13Months_BasedOnState]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptFreightAnalysisFor13Months_BasedOnState]
	TO [ReportUser]
GO
