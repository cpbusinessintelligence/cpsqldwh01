SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Sp_RptRevenueDifferencesInProntoAndPivot  
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Select LabelNumber ,[RevenueRecognisedDate],CouponType,BU, RevenueAmount ,Convert(Date,CreatedDate) as CreatedDate
  into #temp
  FROM [Revenue].[dbo].[PrepaidRevenueReporting] 
  Where [IsFirstScanProcessed] = 1 and [RevenueRecognisedDate] >= '2016-11-01' and RevenueAmount >0
  union all
    Select LabelNumber, [RevenueRecognisedDate],CouponType,BU, RevenueAmount ,Convert(Date,CreatedDate) as CreatedDate
       FROM [Revenue].[dbo].[PrepaidRevenueReportingExceptions]
  Where [IsFirstScanProcessed] = 1 and [RevenueRecognisedDate] >= '2016-11-01'and RevenueAmount >0
 
  --drop table #Temp
 
 
  Delete from  #temp where  RevenueAmount  = 0
 
 
 
  Select T.LabelNumber ,T.[RevenueRecognisedDate],T.CouponType,BU, T.RevenueAmount,CreatedDate ,P.RevenueDate , P.RevenueAmount as ProntoRevenue
   into #temp2
   from #Temp T left join [Pronto].[dbo].[ProntoCouponDetails] P on  T.LabelNumber = P.[SerialNumber]
 
   
 
 
   Select RevenueRecognisedDate , (Datepart(year,RevenueRecognisedDate)*100) + Datepart(Month,RevenueRecognisedDate) as MonthKey , SUM(RevenueAmount) as PrepaidRevenue, sum(ProntoRevenue) as ProntoRevenue ,
   SUm(RevenueAmount - ProntoRevenue) as Difference  from #Temp2
   group by RevenueRecognisedDate
   order by 1 desc

END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptRevenueDifferencesInProntoAndPivot]
	TO [ReportUser]
GO
