SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--EXEC [dbo].[Sp_RptDIFOT_InternalStaff_AllAccounts_Updated] 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Z_Sp_RptFreightAnalysisFor13Months_BKP_TS_20171129] (@AccountCode varchar(30),@Date Date)

WITH RECOMPILE
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;


DECLARE @StartDate Date
DECLARE @EndDate Date


SET @StartDate = convert(date,dateadd(month, datediff(month, 0,@Date) - 13, 0))
SET @EndDate = EOMONTH (@Date,-1) 

--CREATE TABLE #TEMP (ACCCode VARCHAR(MAX))

--  /*INSERTING EACH COMMA SEPERATED VALUE INTO TEMP TABLE*/
--  WHILE CHARINDEX(',',@AccountCode)<>0
--  BEGIN
--    INSERT INTO #TEMP VALUES((SELECT LEFT(@AccountCode, CHARINDEX(',',@AccountCode)-1)))
--    SET @AccountCode=(SELECT RIGHT(@AccountCode,LEN(@AccountCode)-CHARINDEX(',',@AccountCode)))
--  END


/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct [AccountCode]
      ,[AccountName]
   --   ,CASE WHEN MONTH([AccountingDate])>6 THEN convert(varchar(4),YEAR([AccountingDate]))+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])+1),2)
	  --      WHEN MONTH([AccountingDate])<=6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
			--END AS SalesYear
	  ,CASE WHEN MONTH([AccountingDate])>6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
		WHEN MONTH([AccountingDate])<=6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
		END AS SalesYear
	  ,MONTH([AccountingDate]) as MonthNum
	  ,DateName(mm,[AccountingDate]) as [MonthName]
	  ,PC.State as [State]    
      ,SUM([ItemQuantity]) as NumberOfItems
	  ,count([ConsignmentReference]) as NumberOfConsignments
      ,SUM([ChargeableWeight]) as TotalChargeableWeight
      ,SUM([BilledTotal]) as TotalFreightCost
	  ,convert(decimal(18,2),SUM([BilledTotal])/NULLIF(count([ConsignmentReference]),0)) as AvgDollarsPerConnote
	  ,convert(decimal(18,2),SUM([BilledTotal])/NULLIF(SUM([ItemQuantity]),0)) as AvgDollarsPerItem
	  ,convert(decimal(18,2),SUM ([ItemQuantity])/NULLIF(count([ConsignmentReference]),0)) as AvgItemsPerConsignment
	  ,convert(decimal(18,2),SUM ([ChargeableWeight])/NULLIF(count([ConsignmentReference]),0)) as AvgWeightPerConnote
	  ,convert(decimal(18,2),SUM ([ChargeableWeight])/NULLIF(SUM ([ItemQuantity]),0)) as AvgWeightPerItem
  FROM [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
  join PerformanceReporting.[dbo].[PostCodes] PC on p.[OriginPostcode] = PC.PostCode
  Where [AccountCode] IN (select *from dbo.fnSplitString(@AccountCode,','))  and [AccountingDate] >= @StartDate and [AccountingDate] <= @EndDate

  GROUP BY [AccountCode]
      ,[AccountName]
	  ,CASE WHEN MONTH([AccountingDate])>6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
	        WHEN MONTH([AccountingDate])<=6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
			END
	  ,MONTH([AccountingDate])
	  ,DateName(mm,[AccountingDate])
	  ,PC.State 
ORDER BY CASE WHEN MONTH([AccountingDate])>6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
	        WHEN MONTH([AccountingDate])<=6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
			END
	 ,MONTH([AccountingDate])


--  GROUP BY [AccountCode]
--      ,[AccountName]
--	  ,[AccountingDate]
--	  ,PC.State 
--ORDER BY CASE WHEN MONTH([AccountingDate])>6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
--		WHEN MONTH([AccountingDate])<=6 THEN convert(varchar(4),YEAR([AccountingDate])-1)+'/'+RIGHT(convert(varchar(4),YEAR([AccountingDate])),2)
--		END
--	  ,MONTH([AccountingDate])
END
GO
GRANT ALTER
	ON [dbo].[Z_Sp_RptFreightAnalysisFor13Months_BKP_TS_20171129]
	TO [ReportUser]
GO
GRANT CONTROL
	ON [dbo].[Z_Sp_RptFreightAnalysisFor13Months_BKP_TS_20171129]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[Z_Sp_RptFreightAnalysisFor13Months_BKP_TS_20171129]
	TO [ReportUser]
GO
