SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Z_sp_RptSubsidy_BKP_TS_20180510](@StartDate date,@EndDate date) as
begin

    --'=====================================================================
    --' CP -Stored Procedure -[sp_RptSubsidy]
    --' ---------------------------
    --' Purpose: Create Subsidy Report-----
    --' Developer: Tejes (Couriers Please Pty Ltd)
    --' Date: 11 April 2018
    --' Copyright: 2018 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 11/04/2018    TS     1.00    Created the procedure                             --TS20180411

    --'=====================================================================
select distinct [ProntoID] into #temp from [dbo].[tblContractor]

select distinct [RctiWeekEndingDate] into #Temp0 from [Pronto].[dbo].[FranchiseePaymentSummary] (NOLOCK) where [RctiWeekEndingDate] between @StartDate and @EndDate

Select    C.*
         ,[ContractorId]
		 ,[RunNumber]
		 ,[RunName]
		 ,[Branch]
		 ,P.[RctiWeekEndingDate]
		 ,convert(Datetime,Null) as ProcessingWeekEnding
         ,[TotalDevelopmentIncentive] as SubSidyPaid
         ,[TotalRctiRedemptions] as RCTIRedemptionPayment
         ,Convert(decimal(12,2),0) as TotalDelivery
         ,Convert(decimal(12,2),0) as DeliveryAllowanceValue
         ,Convert(decimal(12,2),0) as DifferenceSubsidyandAllowance
         ,Convert(decimal(12,2),0) as PaymnetAmount
         ,Convert(decimal(12,2),0) as AdditionalAmttobepaid
Into #Temp1  from Pronto.dbo.tblContractor C (NOLOCK)
Join  [Pronto].[dbo].[FranchiseePaymentSummary] P (NOLOCK) on C.ProntoId = P.ContractorID 
join #Temp0 T0 on P.RctiWeekEndingDate=T0.RctiWeekEndingDate
where P.[RctiWeekEndingDate] = T0.RctiWeekEndingDate

Select D.DeliveryContractor , Count(*) as Ct,T0.RctiWeekEndingDate  into #Temp2 from Pronto.dbo.ProntoCouponDetails D  (NOLOCK)
Join #Temp T On D.DeliveryContractor=T.ProntoID
join #Temp0 T0 On D.DeliveryRCTIDate=T0.RctiWeekEndingDate
Where D.DeliveryRCTIDate=T0.RctiWeekEndingDate 
--and Pronto.dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(D.StartSerialNumber,3)) not in ('RETURN TRK','IRP TRK','ATL','LINK')
group by   D.DeliveryContractor,T0.RctiWeekEndingDate 

update #Temp1 SET ProcessingWeekEnding = dateadd(ww,+2,[RctiWeekEndingDate])

Update #Temp1 SET TotalDelivery  =  t2.Ct  From #Temp1 T Join #Temp2 T2 on T.ProntoId = T2.DeliveryContractor where T.RctiWeekEndingDate=T2.RctiWeekEndingDate

Update #Temp1 SET DeliveryAllowanceValue  = TotalDelivery * 0.90 
Update #Temp1 SET DifferenceSubsidyandAllowance  =DeliveryAllowanceValue- SubSidyPaid

Update #Temp1 SET PaymnetAmount = CASE  WHEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment <= BaseAmount THen BaseAmount  
                                               WHEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment Between BaseAmount and MaxAmount THEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment 
                                                                           WHEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment > MaxAmount THEN MaxAmount 
                                                                           ELSE 0 END
Update #temp1 set AdditionalAmttobepaid = PaymnetAmount - (SubSidyPaid+RCTIRedemptionPayment) where PaymnetAmount - (SubSidyPaid+RCTIRedemptionPayment) >0

       --Update #Temp1 SET  EDIDelivery =  (Select sum(P.[DeliveryCount])  From #Temp1 T join [Pronto].[dbo].[CPPL_ContractorRctiPaymentsByServiceCode] P on T.ProntoId = P.ContractorId where  P.[RctiWeekEndingDate] = '2018-01-07')
       --Update tblContractor SET MaxAmount = 2000,baseamount = 1600


Select * from #Temp1 ORDER BY [RctiWeekEndingDate]

end
GO
GRANT EXECUTE
	ON [dbo].[Z_sp_RptSubsidy_BKP_TS_20180510]
	TO [ReportUser]
GO
