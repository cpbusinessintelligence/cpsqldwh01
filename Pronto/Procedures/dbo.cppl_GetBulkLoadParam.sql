SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_GetBulkLoadParam] 
(
	@FileName varchar(254),
	@LoadTableName varchar(100) OUTPUT,
	@LoadStoredProcedure varchar(100) OUTPUT,
	@LegacyDbStoredProcedure varchar(100) OUTPUT,
	@RowDelimiter varchar(50) OUTPUT,
	@ColumnDelimiter varchar(50) OUTPUT
)
AS
BEGIN

       --'=====================================================================
    --' CP -Stored Procedure -[cppl_GetBulkLoadParam] 
    --' ---------------------------
    --' Purpose: Get Parameters for Bulk Load-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/09/2014    AB      1.00    Created the procedure                             --AB20140905

    --'=====================================================================


	/* 
	 *
	 * since this proc is regularly called, we'll do a shrink of the
	 * transaction log here
	 *
	 */
	--DBCC SHRINKFILE (Pronto_log, 1);

	SELECT @FileName = LTRIM(RTRIM(ISNULL(@FileName, '')));

	IF (LOWER(RIGHT(@FileName, 4)) = '.txt')
	BEGIN
		SELECT @FileName = LEFT(@FileName, 
							(LEN(@FileName) - 4));
	END

	SELECT TOP 1
	@LoadTableName = LoadTableName, 
	@LoadStoredProcedure = LoadStoredProcedure,
	@LegacyDbStoredProcedure = LegacyDbStoredProcedure,
	@RowDelimiter = RowDelimiter,
	@ColumnDelimiter = ColumnDelimiter
	FROM [CentralAdmin].[dbo].SSIS_FileToTableMap (NOLOCK)
	WHERE ([FileName] = @FileName)
	AND IsActive = 1;

	IF (@LoadTableName IS NULL)
		SELECT @LoadTableName = '';
	IF (@LoadStoredProcedure IS NULL)
		SELECT @LoadStoredProcedure = '';
	IF (@LegacyDbStoredProcedure IS NULL)
		SELECT @LegacyDbStoredProcedure = '';
	IF (@RowDelimiter IS NULL)
		SELECT @RowDelimiter = '';
	IF (@ColumnDelimiter IS NULL)
		SELECT @ColumnDelimiter = '';
		
END;
GO
