SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[cppl_LoadProntoAccountingPeriod]
AS
BEGIN


     --'=====================================================================
    --' CP -Stored Procedure -[dbo].[cppl_LoadProntoAccountingPeriod]
    --' ---------------------------
    --' Purpose: Load ProntoAccounting Period-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/09/2014    AB      1.00                                                     --AB20140909

    --'=====================================================================


	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

	MERGE [Pronto].[dbo].[ProntoAccountingPeriod] AS Target
	USING (SELECT 
			CAST(DateValue AS DATE) AS DayId
			,LEFT(CAST(DateValue AS varchar(50)), 4) + DATENAME(wk,DateValue) AS WeekId
			,CAST(CASE 
				WHEN MONTH(DATEADD(m, -6 , DateValue)) < 7 THEN YEAR(DateValue) ELSE YEAR(DateValue) - 1
				END AS VARCHAR(4)) 
				+ REPLICATE('0', 2 - DATALENGTH(CAST(MONTH(DATEADD(m, -6 , DateValue)) AS VARCHAR(2)))) 
				+ CAST(MONTH(DATEADD(m, -6 , DateValue)) AS VARCHAR(2)) AS [PeriodId]
			,LEFT(DATENAME(m, DateValue), 3) + '-' + RIGHT(DATENAME(yy, DateValue), 2) PeriodName
			,DATEPART(month, DateValue) AS CalendarMonth
			,DATEPART(year, DateValue) AS CalendarYear
			,'FY' + RIGHT(CAST(CASE 
				WHEN MONTH(DATEADD(m, -6 , DateValue)) < 7 THEN YEAR(DateValue) ELSE YEAR(DateValue) - 1
				END AS VARCHAR(4)), 2)
				+ '-' + RIGHT(CAST(CASE 
				WHEN MONTH(DATEADD(m, -6 , DateValue)) < 7 THEN YEAR(DateValue) + 1 ELSE YEAR(DateValue) 
			END AS VARCHAR(4)), 2) AS FinancialYear

FROM dbo.fnDateRange(
CAST(CAST(YEAR(DATEADD(YEAR, -6, GETDATE())) AS VARCHAR(4)) + '-' + CAST(MONTH(DATEADD(YEAR, -6, GETDATE())) AS VARCHAR(4)) + '-01' AS DATETIME)
, DATEADD(D, -1, CAST(CAST(YEAR(GETDATE()) + 1 AS VARCHAR(4)) + '-'	+ '12-31' AS DATETIME))
, NULL, NULL, 1, NULL, NULL)
) AS Source
	ON Target.[DayId] = Source.[DayId]
	WHEN MATCHED THEN
		UPDATE SET
			Target.[WeekId] = Source.[WeekId]
			,Target.[PeriodId] = Source.[PeriodId]
			,Target.[PeriodName] = Source.[PeriodName]
			,Target.[calendarMonth] = Source.[calendarMonth]
			,Target.[calendarYear] = Source.[calendarYear]
			,Target.[FinancialYear] = Source.[FinancialYear]
	WHEN NOT MATCHED THEN
		INSERT
			([DayId]
			,[WeekId]
			,[PeriodId]
			,[PeriodName]
			,[calendarMonth]
			,[calendarYear]
			,[FinancialYear]
			)
			VALUES
			(
			Source.[DayId]
			,Source.[WeekId]
			,Source.[PeriodId]
			,Source.[PeriodName]
			,Source.[calendarMonth]
			,Source.[calendarYear]
			,Source.[FinancialYear]
			);

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END
GO
