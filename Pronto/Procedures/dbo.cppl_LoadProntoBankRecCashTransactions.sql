SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[cppl_LoadProntoBankRecCashTransactions] AS


     --'=====================================================================
    --' CP -Stored Procedure -[dbo].[cppl_Load[cppl_LoadProntoBankRecCashTransactions]]
    --' ---------------------------
    --' Purpose: Load ProntoBank RecCash Transactions Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 09 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 09/09/2014    AB      1.00                                                     --AB20140909

    --'=====================================================================


	DBCC SHRINKFILE(Pronto_log, 1);

INSERT INTO [Pronto].[dbo].[ProntoBankRecCashTransactions]
           ([BankGlAccount]
           ,[TransactionType]
           ,[DocumentNumber]
           ,[BankCode]
           ,[AccountCode]
           ,[ShortName]
           ,[TransactionDate]
           ,[TransactionReference]
           ,[Details]
           ,[BatchReference]
           ,[TransactionNumber]
           ,[LocalAmount]
           ,[ForeignAmount]
           ,[PresentedFlag]
           ,[PresentedDate]
           ,[WithholdTax]
           ,[ForeignWithholdTax]
           ,[FecCode]
           ,[FecRate]
           ,[DateStamp]
           ,[BsbNumber]
           ,[CompanyCode]
           ,[BankAccountCode]
           ,[TransactionSource]
           ,[PayeeDetails]
           ,[ReceiptLocation]
           ,[BankSlipNumber]
           ,[OnStatement]
           ,[CardAuthorisation]
           ,[OnBankSlip])
SELECT [BankGlAccount]
      ,[TransactionType]
      ,[DocumentNumber]
      ,[BankCode]
      ,[AccountCode]
      ,[ShortName]
      ,[TransactionDate]
      ,[TransactionReference]
      ,[Details]
      ,[BatchReference]
      ,[TransactionNumber]
      ,[LocalAmount]
      ,[ForeignAmount]
      ,[PresentedFlag]
      ,[PresentedDate]
      ,[WithholdTax]
      ,[ForeignWithholdTax]
      ,[FecCode]
      ,[FecRate]
      ,[DateStamp]
      ,[BsbNumber]
      ,[CompanyCode]
      ,[BankAccountCode]
      ,[TransactionSource]
      ,[PayeeDetails]
      ,[ReceiptLocation]
      ,[BankSlipNumber]
      ,[OnStatement]
      ,[CardAuthorisation]
      ,[OnBankSlip]
  FROM [Pronto].[dbo].[Load_ProntoBankRecCashTransactions];

	DBCC SHRINKFILE(Pronto_log, 1);
GO
