SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[cppl_LoadProntoBilling] AS

BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoBilling]
    --' ---------------------------
    --' Purpose: Load ProntoBilling Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================
	
	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
	
		IF EXISTS (SELECT 1 FROM InterfaceConfig 
					WHERE Name = 'ProntoBillingImportRunning')
		BEGIN
			UPDATE InterfaceConfig 
			SET Value = 'Y'
			WHERE Name = 'ProntoBillingImportRunning';
		END
		ELSE
		BEGIN
			INSERT INTO InterfaceConfig 
			(Name, Value)
			VALUES
			('ProntoBillingImportRunning', 'Y');
		END
	
	TRUNCATE TABLE  [ProntoBilling];

		/****** Object:  Index [_dta_index_ProntoBilling_7_1490104349__K9_K10_K14_K1_44_46_47]    Script Date: 02/06/2013 13:25:05 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoBilling]') AND name = N'_dta_index_ProntoBilling_6_1490104349__K9_K10_K14_K1_44_46_47')
		DROP INDEX [_dta_index_ProntoBilling_6_1490104349__K9_K10_K14_K1_44_46_47] ON [dbo].[ProntoBilling] WITH ( ONLINE = OFF );

		/****** Object:  Index [idx_ProntoBilling_AccountCodeAcctWeek]    Script Date: 02/06/2013 13:25:53 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoBilling]') AND name = N'idx_ProntoBilling_AccountCodeAcctWeek1')
		DROP INDEX [idx_ProntoBilling_AccountCodeAcctWeek1] ON [dbo].[ProntoBilling] WITH ( ONLINE = OFF );

		/****** Object:  Index [PK_ProntoBilling]    Script Date: 02/06/2013 13:26:43 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoBilling]') AND name = N'PK_ProntoBilling1')
		ALTER TABLE [dbo].[ProntoBilling] DROP CONSTRAINT [PK_ProntoBilling1];
	
	INSERT INTO [ProntoBilling] WITH (TABLOCK)
			(
			[OrderNumber]
           ,[OrderSuffix]
           ,[OrderId]
           ,[ConsignmentReference]
           ,[AccountCode]
           ,[AccountName]
           ,[AccountBillToCode]
           ,[AccountBillToName]
           ,[ServiceCode]
           ,[CompanyCode]
           ,[AccountingPeriod]
           ,[AccountingWeek]
           ,[BillingDate]
           ,[AccountingDate]
           ,[InvoiceNumber]
           ,[OriginLocality]
           ,[OriginPostcode]
           ,[ReceiverName]
           ,[DestinationLocality]
           ,[DestinationPostcode]
           ,[ConsignmentDate]
           ,[ManifestReference]
           ,[ManifestDate]
           ,[CustomerReference]
           ,[LogisticsUnitsQuantity]
           ,[ItemQuantity]
           ,[DeclaredWeight]
           ,[DeadWeight]
           ,[DeclaredVolume]
           ,[Volume]
           ,[ChargeableWeight]
           ,[LinehaulWeight]
           ,[InsuranceCategory]
           ,[InsuranceDeclaredValue]
           ,[InsurancePriceOverride]
           ,[TestFlag]
           ,[RevenueBusinessUnit]
           ,[TariffId]
           ,[RevenueOriginZone]
           ,[RevenueDestinationZone]
           ,[CalculatedFreightCharge]
           ,[CalculatedFuelSurcharge]
           ,[CalculatedTransportCharge]
           ,[PriceOverride]
           ,[BilledFreightCharge]
           ,[BilledFuelSurcharge]
           ,[BilledTransportCharge]
           ,[BilledInsurance]
           ,[BilledOtherCharge]
           ,[BilledTotal]
           ,[CardRateFreightCharge]
           ,[CardRateFuelSurcharge]
           ,[CardRateTransportCharge]
           ,[CardRateDiscountOff]
           ,[PickupCost]
           ,[PickupSupplier]
           ,[PickupZone]
           ,[LinehaulCost]
           ,[LinehaulRoute]
           ,[DeliveryCost]
           ,[DeliverySupplier]
           ,[DeliveryZone]
           ,[TotalPUDCost]
           ,[MarginTransportcharge]
			)

			SELECT
			CAST([Load_ProntoBilling].[OrderNumber] AS BIGINT) AS [OrderNumber]
			, CAST([Load_ProntoBilling].[Suffix] AS VARCHAR(5)) AS [OrderSuffix]
			, CAST([Load_ProntoBilling].[Order] AS VARCHAR(50)) AS [OrderId]
			, CAST([Load_ProntoBilling].[ConsignmentReference] AS VARCHAR(50)) AS [ConsignmentReference]
			, CAST([Load_ProntoBilling].[AccountCode] AS VARCHAR(10)) AS [AccountCode]
			, CAST(ISNULL(cu.Shortname, '') AS VARCHAR(50)) AS [AccountName]
			, CAST(ISNULL(cu2.AccountCode, '') AS VARCHAR(10)) AS [AccountBillToCode]
			, CAST(ISNULL(cu2.Shortname, '') AS VARCHAR(50)) AS [AccountBillToName]
			, CAST([Load_ProntoBilling].[Service] AS VARCHAR(10)) AS [ServiceCode]
			, CAST([Load_ProntoBilling].CompanyCode AS VARCHAR(5)) AS [CompanyCode]
			, CAST([Load_ProntoBilling].PostingYearAndPeriod AS VARCHAR(6)) AS [AccountingPeriod]
			, CAST(ProntoAccountingPeriod.WeekId AS VARCHAR(6)) AS [AccountingWeek]
			, CAST([Load_ProntoBilling].[BillingDate] AS DATE) AS [BillingDate]
			, CAST([Load_ProntoBilling].[InvoiceDate] AS DATE) AS [AccountingDate]
			, CAST([Load_ProntoBilling].[InvoiceNo] AS VARCHAR(8)) AS [InvoiceNumber]
			, CAST([Load_ProntoBilling].[OriginLocality] AS VARCHAR(50)) AS [OriginLocality]
			, CAST([Load_ProntoBilling].[OriginPostcode] AS VARCHAR(10)) AS [OriginPostcode]
			, CAST([Load_ProntoBilling].[ReceiverName] AS VARCHAR(5000)) AS [ReceiverName]
			, CAST([Load_ProntoBilling].[DestinationLocality] AS VARCHAR(50)) AS [DestinationLocality]
			, CAST([Load_ProntoBilling].[DestinationPostcode] AS VARCHAR(10)) AS [DestinationPostcode]
			, CAST([Load_ProntoBilling].[ConsignmentDate] AS DATE) AS [ConsignmentDate]
			, CAST([Load_ProntoBilling].[ManifestReference] AS VARCHAR(25)) AS [ManifestReference]
			, CAST([Load_ProntoBilling].[ManifestDate] AS DATE) AS [ManifestDate]
			, CAST([Load_ProntoBilling].[CustomerReference] AS VARCHAR(50)) AS [CustomerReference]
			, CAST([Load_ProntoBilling].[LogisticsUnits] AS FLOAT) AS [LogisticsUnitsQuantity]
			, CAST([Load_ProntoBilling].[ItemQty] AS FLOAT) AS [ItemQuantity]
			, CAST([Load_ProntoBilling].[DeclaredWeight] AS FLOAT) AS [DeadWeight]
			, CAST([Load_ProntoBilling].[Weight] AS FLOAT) AS [DeadWeight]
			, CAST([Load_ProntoBilling].[DeclaredVolume] AS FLOAT) AS [Volume]
			, CAST([Load_ProntoBilling].[Volume] AS FLOAT) AS [Volume]
			, CAST(CASE [Load_ProntoBilling].[Service]
				WHEN '55' THEN
					CONVERT(DECIMAL (18,2), ISNULL([Load_ProntoBilling].ItemQty, 0)) * 500
				ELSE
					CONVERT(DECIMAL (18,2), [Load_ProntoBilling].ChargeableWeight)
				END AS float) AS ChargeableWeight
			--, CAST(CONVERT(DECIMAL (18,5), CASE 
			--	WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].ItemQty, 0)) >= 0 THEN
			--		CASE 
			--			WHEN CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Volume] * 250.00) > CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight]) THEN
			--				CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Volume] * 250.00)
			--			ELSE CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight]) 
			--		END
			--		ELSE CASE 
			--			WHEN CONVERT(DECIMAL (18,5), [Load_ProntoBilling].Volume * 250.00) < CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight]) THEN
			--				CONVERT(DECIMAL (18,5), [Load_ProntoBilling].volume * 250.00)
			--			ELSE CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight])
			--		END
			--	END) AS float) AS LineHaulWeight
			, CONVERT(DECIMAL (18,5), ISNULL(CASE 
				WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].ItemQty, 0)) >= 0 THEN
					CASE 
						WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Volume], 0)) * 250.00 > CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0)) THEN
							CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Volume], 0)) * 250.00
						ELSE CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0)) 
					END
					ELSE CASE 
						WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].Volume, 0)) * 250.00 < CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0)) THEN
							CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].volume, 0)) * 250.00
						ELSE CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0))
					END
				END, 0))  AS LineHaulWeight
			, CAST([Load_ProntoBilling].InsuranceCategory AS VARCHAR(10)) AS [InsuranceCategory]
			, CAST([Load_ProntoBilling].DeclaredValue AS MONEY) AS [InsuranceDeclaredValue]
			, CAST([Load_ProntoBilling].InsurancePriceOverride AS MONEY) AS [InsurancePriceOverride]
			, CAST(CASE [TestFlag] WHEN 'Y' THEN 1 ELSE 0 END AS bit) AS [TestFlag]
			--,[TestFlag]
			, CAST([Load_ProntoBilling].Territory AS VARCHAR(50)) AS [RevenueBusinessUnit]
			, CAST(LEFT([Load_ProntoBilling].TariffId, 50) AS VARCHAR(50)) AS [TariffId]
			, CAST([Load_ProntoBilling].OriginZone AS VARCHAR(50)) AS [RevenueOriginZone]
			, CAST([Load_ProntoBilling].DestinationZone AS VARCHAR(50)) AS [RevenueDestinationZone]
			, CAST([Load_ProntoBilling].FreightCalc AS MONEY) AS [CalculatedFreightCharge]
			, CAST([Load_ProntoBilling].FuelCalc AS MONEY) AS [CalculatedFuelSurcharge]
			, CAST(REPLACE(REPLACE([Load_ProntoBilling].TransCalc, 0x0D, ''), 0x0A, '') AS MONEY) AS [CalculatedTransportCharge]
			, CAST([Load_ProntoBilling].PriceOverride AS MONEY) AS [PriceOverride]
			, CAST([Load_ProntoBilling].FreightCust AS MONEY) AS [BilledFreightCharge]
			, CAST([Load_ProntoBilling].FuelCust AS MONEY) AS [BilledFuelSurcharge]
			, CAST([Load_ProntoBilling].TransCust AS MONEY) AS [BilledTransportCharge]
			, CAST([Load_ProntoBilling].InsurCharge AS MONEY) AS [BilledInsurance]
			, CAST([Load_ProntoBilling].OtherCharge AS MONEY) AS [BilledOtherCharge]
			, CAST([Load_ProntoBilling].TotalCust AS MONEY) AS [BilledTotal]
			, CAST([Load_ProntoBilling].FreightCard AS MONEY) AS [CardRateFreightCharge]
			, CAST([Load_ProntoBilling].FuelCard AS MONEY) AS [CardRateFuelSurcharge]
			, CAST([Load_ProntoBilling].TransCard AS MONEY) AS [CardRateTransportCharge]
			, CAST([Load_ProntoBilling].DiscOffCard AS MONEY) AS [CardRateDiscountOff]
			, CAST([Load_ProntoBilling].PickupCost AS MONEY) AS [PickupCost]
			, CAST([Load_ProntoBilling].PickupSupplier AS VARCHAR(50)) AS [PickupSupplier]
			, CAST([Load_ProntoBilling].PickupZone AS VARCHAR(50)) AS [PickupZone]
			, NULL AS [LinehaulCost]
			, NULL AS [LinehaulRoute]
			, CAST([Load_ProntoBilling].DeliveryCost AS MONEY) AS [DeliveryCost]
			, CAST([Load_ProntoBilling].Supplier AS VARCHAR(10)) AS [DeliverySupplier]
			, CAST([Load_ProntoBilling].DeliveryZone AS VARCHAR(10)) AS [DeliveryZone]
			, CAST([Load_ProntoBilling].Totalpudcost AS MONEY) AS [TotalPUDCost]
			, CONVERT(MONEY, ISNULL([Load_ProntoBilling].TransCust, 0)) - CONVERT(MONEY, ISNULL([Load_ProntoBilling].Totalpudcost, 0)) AS [MarginTransportCharge]
			FROM 
				(
					(
						(
							(
								[Load_ProntoBilling] 
								LEFT JOIN ProntoAccountingPeriod ON [Load_ProntoBilling].[invoicedate] = ProntoAccountingPeriod.DayId
							)
						)
						LEFT JOIN ProntoDebtor cu ON [Load_ProntoBilling].Accountcode = cu.Accountcode
					)
					LEFT JOIN ProntoDebtor cu2 ON cu.BillTo = cu2.AccountCode
				) where BillingDate is not null and  CAST([Load_ProntoBilling].[InvoiceDate] AS DATE) >='2020-01-01'
			;

--Commented by PV on 2020-05-06: To avoid error while loading records to not existing table. This sp backup has created with the prefix 'PV_On20200506'
/*
TRUNCATE TABLE  [ProntoBilling_Archives07-13];

INSERT INTO [ProntoBilling_Archives07-13] WITH (TABLOCK)
			(
			[OrderNumber]
           ,[OrderSuffix]
           ,[OrderId]
           ,[ConsignmentReference]
           ,[AccountCode]
           ,[AccountName]
           ,[AccountBillToCode]
           ,[AccountBillToName]
           ,[ServiceCode]
           ,[CompanyCode]
           ,[AccountingPeriod]
           ,[AccountingWeek]
           ,[BillingDate]
           ,[AccountingDate]
           ,[InvoiceNumber]
           ,[OriginLocality]
           ,[OriginPostcode]
           ,[ReceiverName]
           ,[DestinationLocality]
           ,[DestinationPostcode]
           ,[ConsignmentDate]
           ,[ManifestReference]
           ,[ManifestDate]
           ,[CustomerReference]
           ,[LogisticsUnitsQuantity]
           ,[ItemQuantity]
           ,[DeclaredWeight]
           ,[DeadWeight]
           ,[DeclaredVolume]
           ,[Volume]
           ,[ChargeableWeight]
           ,[LinehaulWeight]
           ,[InsuranceCategory]
           ,[InsuranceDeclaredValue]
           ,[InsurancePriceOverride]
           ,[TestFlag]
           ,[RevenueBusinessUnit]
           ,[TariffId]
           ,[RevenueOriginZone]
           ,[RevenueDestinationZone]
           ,[CalculatedFreightCharge]
           ,[CalculatedFuelSurcharge]
           ,[CalculatedTransportCharge]
           ,[PriceOverride]
           ,[BilledFreightCharge]
           ,[BilledFuelSurcharge]
           ,[BilledTransportCharge]
           ,[BilledInsurance]
           ,[BilledOtherCharge]
           ,[BilledTotal]
           ,[CardRateFreightCharge]
           ,[CardRateFuelSurcharge]
           ,[CardRateTransportCharge]
           ,[CardRateDiscountOff]
           ,[PickupCost]
           ,[PickupSupplier]
           ,[PickupZone]
           ,[LinehaulCost]
           ,[LinehaulRoute]
           ,[DeliveryCost]
           ,[DeliverySupplier]
           ,[DeliveryZone]
           ,[TotalPUDCost]
           ,[MarginTransportcharge]
			)

			SELECT
			CAST([Load_ProntoBilling].[OrderNumber] AS BIGINT) AS [OrderNumber]
			, CAST([Load_ProntoBilling].[Suffix] AS VARCHAR(5)) AS [OrderSuffix]
			, CAST([Load_ProntoBilling].[Order] AS VARCHAR(50)) AS [OrderId]
			, CAST([Load_ProntoBilling].[ConsignmentReference] AS VARCHAR(50)) AS [ConsignmentReference]
			, CAST([Load_ProntoBilling].[AccountCode] AS VARCHAR(10)) AS [AccountCode]
			, CAST(ISNULL(cu.Shortname, '') AS VARCHAR(50)) AS [AccountName]
			, CAST(ISNULL(cu2.AccountCode, '') AS VARCHAR(10)) AS [AccountBillToCode]
			, CAST(ISNULL(cu2.Shortname, '') AS VARCHAR(50)) AS [AccountBillToName]
			, CAST([Load_ProntoBilling].[Service] AS VARCHAR(10)) AS [ServiceCode]
			, CAST([Load_ProntoBilling].CompanyCode AS VARCHAR(5)) AS [CompanyCode]
			, CAST([Load_ProntoBilling].PostingYearAndPeriod AS VARCHAR(6)) AS [AccountingPeriod]
			, CAST(ProntoAccountingPeriod.WeekId AS VARCHAR(6)) AS [AccountingWeek]
			, CAST([Load_ProntoBilling].[BillingDate] AS DATE) AS [BillingDate]
			, CAST([Load_ProntoBilling].[InvoiceDate] AS DATE) AS [AccountingDate]
			, CAST([Load_ProntoBilling].[InvoiceNo] AS VARCHAR(8)) AS [InvoiceNumber]
			, CAST([Load_ProntoBilling].[OriginLocality] AS VARCHAR(50)) AS [OriginLocality]
			, CAST([Load_ProntoBilling].[OriginPostcode] AS VARCHAR(10)) AS [OriginPostcode]
			, CAST([Load_ProntoBilling].[ReceiverName] AS VARCHAR(50)) AS [ReceiverName]
			, CAST([Load_ProntoBilling].[DestinationLocality] AS VARCHAR(50)) AS [DestinationLocality]
			, CAST([Load_ProntoBilling].[DestinationPostcode] AS VARCHAR(10)) AS [DestinationPostcode]
			, CAST([Load_ProntoBilling].[ConsignmentDate] AS DATE) AS [ConsignmentDate]
			, CAST([Load_ProntoBilling].[ManifestReference] AS VARCHAR(25)) AS [ManifestReference]
			, CAST([Load_ProntoBilling].[ManifestDate] AS DATE) AS [ManifestDate]
			, CAST([Load_ProntoBilling].[CustomerReference] AS VARCHAR(50)) AS [CustomerReference]
			, CAST([Load_ProntoBilling].[LogisticsUnits] AS FLOAT) AS [LogisticsUnitsQuantity]
			, CAST([Load_ProntoBilling].[ItemQty] AS FLOAT) AS [ItemQuantity]
			, CAST([Load_ProntoBilling].[DeclaredWeight] AS FLOAT) AS [DeadWeight]
			, CAST([Load_ProntoBilling].[Weight] AS FLOAT) AS [DeadWeight]
			, CAST([Load_ProntoBilling].[DeclaredVolume] AS FLOAT) AS [Volume]
			, CAST([Load_ProntoBilling].[Volume] AS FLOAT) AS [Volume]
			, CAST(CASE [Load_ProntoBilling].[Service]
				WHEN '55' THEN
					CONVERT(DECIMAL (18,2), ISNULL([Load_ProntoBilling].ItemQty, 0)) * 500
				ELSE
					CONVERT(DECIMAL (18,2), [Load_ProntoBilling].ChargeableWeight)
				END AS float) AS ChargeableWeight
			--, CAST(CONVERT(DECIMAL (18,5), CASE 
			--	WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].ItemQty, 0)) >= 0 THEN
			--		CASE 
			--			WHEN CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Volume] * 250.00) > CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight]) THEN
			--				CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Volume] * 250.00)
			--			ELSE CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight]) 
			--		END
			--		ELSE CASE 
			--			WHEN CONVERT(DECIMAL (18,5), [Load_ProntoBilling].Volume * 250.00) < CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight]) THEN
			--				CONVERT(DECIMAL (18,5), [Load_ProntoBilling].volume * 250.00)
			--			ELSE CONVERT(DECIMAL (18,5), [Load_ProntoBilling].[Weight])
			--		END
			--	END) AS float) AS LineHaulWeight
			, CONVERT(DECIMAL (18,5), ISNULL(CASE 
				WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].ItemQty, 0)) >= 0 THEN
					CASE 
						WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Volume], 0)) * 250.00 > CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0)) THEN
							CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Volume], 0)) * 250.00
						ELSE CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0)) 
					END
					ELSE CASE 
						WHEN CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].Volume, 0)) * 250.00 < CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0)) THEN
							CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].volume, 0)) * 250.00
						ELSE CONVERT(DECIMAL (18,5), ISNULL([Load_ProntoBilling].[Weight], 0))
					END
				END, 0))  AS LineHaulWeight
			, CAST([Load_ProntoBilling].InsuranceCategory AS VARCHAR(10)) AS [InsuranceCategory]
			, CAST([Load_ProntoBilling].DeclaredValue AS MONEY) AS [InsuranceDeclaredValue]
			, CAST([Load_ProntoBilling].InsurancePriceOverride AS MONEY) AS [InsurancePriceOverride]
			, CAST(CASE [TestFlag] WHEN 'Y' THEN 1 ELSE 0 END AS bit) AS [TestFlag]
			--,[TestFlag]
			, CAST([Load_ProntoBilling].Territory AS VARCHAR(50)) AS [RevenueBusinessUnit]
			, CAST(LEFT([Load_ProntoBilling].TariffId, 50) AS VARCHAR(50)) AS [TariffId]
			, CAST([Load_ProntoBilling].OriginZone AS VARCHAR(50)) AS [RevenueOriginZone]
			, CAST([Load_ProntoBilling].DestinationZone AS VARCHAR(50)) AS [RevenueDestinationZone]
			, CAST([Load_ProntoBilling].FreightCalc AS MONEY) AS [CalculatedFreightCharge]
			, CAST([Load_ProntoBilling].FuelCalc AS MONEY) AS [CalculatedFuelSurcharge]
			, CAST(REPLACE(REPLACE([Load_ProntoBilling].TransCalc, 0x0D, ''), 0x0A, '') AS MONEY) AS [CalculatedTransportCharge]
			, CAST([Load_ProntoBilling].PriceOverride AS MONEY) AS [PriceOverride]
			, CAST([Load_ProntoBilling].FreightCust AS MONEY) AS [BilledFreightCharge]
			, CAST([Load_ProntoBilling].FuelCust AS MONEY) AS [BilledFuelSurcharge]
			, CAST([Load_ProntoBilling].TransCust AS MONEY) AS [BilledTransportCharge]
			, CAST([Load_ProntoBilling].InsurCharge AS MONEY) AS [BilledInsurance]
			, CAST([Load_ProntoBilling].OtherCharge AS MONEY) AS [BilledOtherCharge]
			, CAST([Load_ProntoBilling].TotalCust AS MONEY) AS [BilledTotal]
			, CAST([Load_ProntoBilling].FreightCard AS MONEY) AS [CardRateFreightCharge]
			, CAST([Load_ProntoBilling].FuelCard AS MONEY) AS [CardRateFuelSurcharge]
			, CAST([Load_ProntoBilling].TransCard AS MONEY) AS [CardRateTransportCharge]
			, CAST([Load_ProntoBilling].DiscOffCard AS MONEY) AS [CardRateDiscountOff]
			, CAST([Load_ProntoBilling].PickupCost AS MONEY) AS [PickupCost]
			, CAST([Load_ProntoBilling].PickupSupplier AS VARCHAR(50)) AS [PickupSupplier]
			, CAST([Load_ProntoBilling].PickupZone AS VARCHAR(50)) AS [PickupZone]
			, NULL AS [LinehaulCost]
			, NULL AS [LinehaulRoute]
			, CAST([Load_ProntoBilling].DeliveryCost AS MONEY) AS [DeliveryCost]
			, CAST([Load_ProntoBilling].Supplier AS VARCHAR(10)) AS [DeliverySupplier]
			, CAST([Load_ProntoBilling].DeliveryZone AS VARCHAR(10)) AS [DeliveryZone]
			, CAST([Load_ProntoBilling].Totalpudcost AS MONEY) AS [TotalPUDCost]
			, CONVERT(MONEY, ISNULL([Load_ProntoBilling].TransCust, 0)) - CONVERT(MONEY, ISNULL([Load_ProntoBilling].Totalpudcost, 0)) AS [MarginTransportCharge]
			FROM 
				(
					(
						(
							(
								[Load_ProntoBilling] 
								LEFT JOIN ProntoAccountingPeriod ON [Load_ProntoBilling].[invoicedate] = ProntoAccountingPeriod.DayId
							)
						)
						LEFT JOIN ProntoDebtor cu ON [Load_ProntoBilling].Accountcode = cu.Accountcode
					)
					LEFT JOIN ProntoDebtor cu2 ON cu.BillTo = cu2.AccountCode
				) where BillingDate is not null and  CAST([Load_ProntoBilling].[InvoiceDate] AS DATE) <'2016-01-01'
			;
			
*/
--Commented by PV on 2020-05-06: Ends here

--truncate table ProntoBillingPaymentSurcharge;


--Insert into ProntoBillingPaymentSurcharge 
--Select * from  [Load_ProntoBilling] where BillingDate is  null



			
--Update Prontobilling set RevenueBusinessUnit='CCF' where accountcode in ('113003248','113003719','113004691','113005292','113007397','113013619',
--'113015994','113016885') and accountingdate >='2015-12-06'


--Update Prontobilling set RevenueBusinessUnit='CAB' where  accountcode in ('112988225','113007892','113016331','113016349','113016372','112988225',
--'113007892','113016331','113016349','113016372') and  accountingdate >='2015-12-06'

--Update Prontobilling set RevenueBusinessUnit='CCA' where accountcode in ('113013460','113015622') and accountingdate >='2015-12-06'


	--TRUNCATE TABLE  [Load_ProntoBilling];

		/****** Object:  Index [_dta_index_ProntoBilling_7_1490104349__K9_K10_K14_K1_44_46_47]    Script Date: 02/06/2013 13:25:05 ******/
		CREATE NONCLUSTERED INDEX [_dta_index_ProntoBilling_6_1490104349__K9_K10_K14_K1_44_46_47] ON [dbo].[ProntoBilling] 
		(
			[ServiceCode] ASC,
			[CompanyCode] ASC,
			[AccountingDate] ASC,
			[OrderNumber] ASC
		)
		INCLUDE ( [BilledFuelSurcharge],
		[BilledInsurance],
		[BilledOtherCharge]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];

		CREATE NONCLUSTERED INDEX [idx_ProntoBilling_AccountCodeAcctWeek1] ON [dbo].[ProntoBilling] 
		(
			[AccountCode] ASC,
			[AccountingWeek] ASC
		)
		INCLUDE ( [ConsignmentReference],
		[OrderNumber],
		[AccountBillToCode],
		[ServiceCode],
		[CompanyCode],
		[AccountingPeriod],
		[BillingDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

		/****** Object:  Index [PK_ProntoBilling]    Script Date: 02/06/2013 13:26:44 ******/
		ALTER TABLE [dbo].[ProntoBilling] ADD  CONSTRAINT [PK_ProntoBilling1] PRIMARY KEY CLUSTERED 
		(
			[OrderNumber] ASC,
			[OrderId] ASC,
			[ConsignmentReference] ASC,
			[ServiceCode] ASC,
			[BillingDate] ASC,
			[AccountCode] ASC,
			[AccountBillToCode] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;

	END CATCH

		IF EXISTS (SELECT 1 FROM InterfaceConfig 
					WHERE Name = 'ProntoBillingImportRunning')
		BEGIN
			UPDATE InterfaceConfig 
			SET Value = 'N'
			WHERE Name = 'ProntoBillingImportRunning';
		END
		ELSE
		BEGIN
			INSERT INTO InterfaceConfig 
			(Name, Value)
			VALUES
			('ProntoBillingImportRunning', 'N');
		END

	DBCC SHRINKFILE(Pronto_log, 1);

END
GO
