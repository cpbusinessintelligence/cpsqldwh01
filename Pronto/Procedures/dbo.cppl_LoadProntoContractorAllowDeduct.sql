SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoContractorAllowDeduct]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoContractorAllowDeduct]
    --' ---------------------------
    --' Purpose: Load LoadProntoContractorAllowDeduct Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================
	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
    
	TRUNCATE TABLE [Pronto].[dbo].[ProntoContractorAllowDeduct]
	
	INSERT INTO [Pronto].[dbo].[ProntoContractorAllowDeduct] WITH(TABLOCK)
			(
			[AccountingDate]
			, [Contractor]
			, [DeductionOrAllowance]
			, [SeqNumber]
			, [Code]
			, [CalculationRule]
			, [Amount] 
			, [GST]
			)
	SELECT
		CAST([AccountingDate] AS [date]) AS [AccountingDate]
		, LEFT([Contractor], 10) AS [Contractor]
		, [DeductionOrAllowance]
		, [SeqNumber]
		, [Code]
		, [CalculationRule]
		, CAST([Amount] AS [money])[Amount]
		, CAST(REPLACE(REPLACE([GST], 0x0D, ''), 0x0A, '') AS [money])[GST]
	FROM [Pronto].[dbo].[Load_ProntoContractorAllowDeduct];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
