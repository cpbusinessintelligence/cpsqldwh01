SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoContractorWeeklyDelivery]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoContractorWeeklyDelivery]
    --' ---------------------------
    --' Purpose: Load ProntoContractorWeeklyDelivery Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================


	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

	TRUNCATE TABLE [Pronto].[dbo].[ProntoContractorWeeklyDelivery];
	
	INSERT INTO [Pronto].[dbo].[ProntoContractorWeeklyDelivery] WITH(TABLOCK)
			([TransactionType]
			,[Contractor]
			,[ItemCode]
			,[CustomerAccountCode]
			,[InsuranceCategory]
			,[AccountingDate]
			,[Amount]
			,[InsuranceAmount]
			,[Count])
	SELECT
		[TransactionType]
		,LEFT([Contractor], 10) AS [Contractor]
		,LEFT([ItemCode], 15) AS [ItemCode]
		,LEFT([CustomerAccountCode], 10) AS [CustomerAccountCode]
		,LEFT([InsuranceCategory], 10) AS [InsuranceCategory]
		, CAST(ISNULL([AccountingDate], [InsuranceCategory]) AS [date]) AS [AccountingDate]
		, CAST([Amount] AS [money]) AS [Amount]
		, CAST([InsuranceAmount] AS [money]) AS [InsuranceAmount]
		, CAST(REPLACE(REPLACE([Count], 0x0D, ''), 0x0A, '') AS [int])[Count]
	FROM [Pronto].[dbo].[Load_ProntoContractorWeeklyDelivery]
    WHERE [CustomerAccountCode] NOT LIKE '~%';
    
    TRUNCATE TABLE [Pronto].[dbo].[Load_ProntoContractorWeeklyDelivery];
    
	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
