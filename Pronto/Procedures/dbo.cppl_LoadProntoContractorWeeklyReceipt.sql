SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoContractorWeeklyReceipt]
AS
BEGIN


     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoContractorWeeklyReceipt]
    --' ---------------------------
    --' Purpose: Load ProntoContractorWeeklyReceipt Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

	SET NOCOUNT ON;

	DBCC SHRINKFILE(cppl_log, 1);

    BEGIN TRY
	
	TRUNCATE TABLE [cppl].[dbo].[ProntoContractorWeeklyReceipt];
	
	INSERT INTO [cppl].[dbo].[ProntoContractorWeeklyReceipt]
		(
			[AccountingDate]
			,[Contractor]
			,[CashType]
			,[CustomerAccountCode]
			,[Amount]
		)
	SELECT DISTINCT
				CAST([AccountingDate] AS [date]) AS [AccountingDate]
				, LEFT(ISNULL([Contractor], ''), 10) AS [Contractor]
				, [CashType]
				, LEFT([CustomerAccountCode], 10) AS [CustomerAccountCode]
				, CAST(REPLACE(REPLACE([Amount], 0x0D, 0), 0x0A, 0) AS [money])[Amount]
			FROM [cppl].[dbo].[Load_ProntoContractorWeeklyReceipt];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(cppl_log, 1);

END;
GO
