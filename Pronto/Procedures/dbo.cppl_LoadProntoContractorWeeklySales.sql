SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoContractorWeeklySales]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoContractorWeeklySales]
    --' ---------------------------
    --' Purpose: Load ProntoContractorWeeklySales Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
    
    TRUNCATE TABLE [Pronto].[dbo].[ProntoContractorWeeklySales]

	INSERT INTO [Pronto].[dbo].[ProntoContractorWeeklySales] WITH(TABLOCK)
			(
			[AccountingDate]
			,[CustomerAccountCode]
			,[Contractor]
			,[ItemCode]
			,[InsuranceCategory]
			,[CouponAmount]
			,[InsuranceAmount]
			,[BookQty]
			)	
			SELECT DISTINCT
					CAST([AccountingDate] AS [date]) AS [AccountingDate]
					, LEFT(REPLACE([CustomerAccountCode], '~~~~~~~~~~', 'UNKNOWN'), 10) AS [CustomerAccountCode]
					, LEFT([Contractor], 10) AS [Contractor]
					, [ItemCode]
					, LEFT(ISNULL([InsuranceCategory], ''), 10) AS [InsuranceCategory]
					, CAST([CouponAmount] AS [money]) AS [CouponAmount]
					, CAST([InsuranceAmount] AS [money]) AS [InsuranceAmount]
					, CAST(REPLACE(REPLACE([BookQty], 0x0D, ''), 0x0A, '') AS [int]) AS [BookQty]
			FROM [dbo].[Load_ProntoContractorWeeklySales];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	TRUNCATE TABLE [Pronto].[dbo].[Load_ProntoContractorWeeklySales];

	DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
