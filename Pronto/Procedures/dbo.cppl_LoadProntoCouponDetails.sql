SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

		CREATE PROC [dbo].[cppl_LoadProntoCouponDetails]
		As
		Begin

			DBCC SHRINKFILE(Pronto_log, 1);
		TRUNCATE TABLE [Pronto].[dbo].[ProntoCouponDetails];
			
			INSERT INTO [Pronto].[dbo].[ProntoCouponDetails] WITH(TABLOCK)
				(
				[SerialNumber]
				,[StartSerialNumber]
				,[ReturnSerialNumber]
				,[TransferReference]
				,[TransferDate]
				,[LastSoldReference]
				,[LastSoldDate]
				,[InsuranceCategory]
				,[InsuranceAmount]
				,[PickupContractor]
				,[PickupDate]
				,[PickupRctiDate]
				,[PickupRctiReference]
				,[PickupRctiAmount]
				,[DeliveryContractor]
				,[DeliveryDate]
				,[DeliveryRctiDate]
				,[DeliveryRctiReference]
				,[DeliveryRctiAmount]
				,[BillingAccountCode]
				,[BillingService]
				,[RevenueBatchReference]
				,[RevenueTransactionNumber]
				,[RevenueDate]
				,[RevenueAmount]
				,[PickupProcessedDate] 
				,[DeliveryProcessedDate]
				,[ActivityFlag]
				)

				SELECT 
					LEFT([SerialNumber], 20) AS [SerialNumber]
					,CASE WHEN [StartSerialNumber] = '' THEN NULL ELSE LEFT([StartSerialNumber], 20) END AS [StartSerialNumber]
					,CASE WHEN [ReturnSerialNumber] = '' THEN NULL ELSE LEFT([ReturnSerialNumber], 20) END AS [ReturnSerialNumber]
					,CASE WHEN [TransferReference] = '' THEN NULL ELSE LTRIM(RTRIM([TransferReference])) END AS [TransferReference]
					,CASE WHEN [TransferDate] = '' THEN NULL ELSE CONVERT(DATE, [TransferDate], 113) END AS [TransferDate]
					,CASE WHEN [LastSoldReference] = '' THEN NULL ELSE LTRIM(RTRIM([LastSoldReference])) END AS [LastSoldReference]
					,CASE WHEN [LastSoldDate] = '' THEN NULL ELSE CONVERT(DATE, [LastSoldDate], 113) END AS [LastSoldDate]
					,CASE WHEN [InsuranceCategory] = '' THEN NULL ELSE LEFT([InsuranceCategory], 10) END AS [InsuranceCategory]
					,CAST([InsuranceAmount] AS [money]) AS [InsuranceAmount]
					,CASE WHEN [PickupContractor] = '' THEN NULL ELSE LEFT([PickupContractor], 10) END AS [PickupContractor]
					,CASE WHEN [PickupDate] = '' THEN NULL ELSE CONVERT(DATE, [PickupDate], 113) END AS [PickupDate]
					,CASE WHEN [PickupRctiDate] = '' THEN NULL ELSE CONVERT(DATE, [PickupRctiDate], 113) END AS [PickupRctiDate]
					,[PickupRctiReference]
					,CAST([PickupRctiAmount] AS [money]) AS [PickupRctiAmount]
					,CASE WHEN [DeliveryContractor] = '' THEN NULL ELSE LEFT([DeliveryContractor], 10) END AS [DeliveryContractor]
					,CASE WHEN [DeliveryDate] = '' THEN NULL ELSE CONVERT(DATE, [DeliveryDate] , 113) END AS [DeliveryDate]
					,CASE WHEN [DeliveryRctiDate] = '' THEN NULL ELSE CONVERT(DATE, [DeliveryRctiDate], 113) END AS [DeliveryRctiDate]
					,CASE WHEN [DeliveryRctiReference] = '' THEN NULL ELSE [DeliveryRctiReference] END AS [DeliveryRctiReference]
					,CAST([DeliveryRctiAmount] AS [money]) AS [DeliveryRctiAmount]
					,CASE WHEN [BillingAccountCode] = '' THEN NULL ELSE LEFT([BillingAccountCode], 11) END AS [BillingAccountCode]
					,CASE WHEN [BillingService] = '' THEN NULL ELSE [BillingService] END AS [BillingService]
					,CASE WHEN [RevenueBatchReference] = '' THEN NULL ELSE [RevenueBatchReference] END AS [RevenueBatchReference]
					,CASE WHEN [RevenueTransactionNumber] = '' THEN NULL ELSE [RevenueTransactionNumber] END AS [RevenueTransactionNumber]
					,CASE WHEN [RevenueDate] = '' THEN NULL ELSE  CONVERT(DATE, [RevenueDate], 113) END AS [RevenueDate]
					,CAST(REPLACE(REPLACE([RevenueAmount], 0x0D, ''), 0x0A, '') AS [money])[RevenueAmount]
					,CASE WHEN [PickupProcessedDate] = '' THEN NULL ELSE CONVERT(datetime, [PickupProcessedDate]) END AS [PickupProcessedDate]
					,CASE WHEN [DeliveryProcessedDate] = '' THEN NULL ELSE CONVERT(datetime, REPLACE(REPLACE([DeliveryProcessedDate], 0x0D, ''), 0x0A, '')) END AS [DeliveryProcessedDate]
					,CASE WHEN [ActivityFlag] = '' THEN NULL ELSE [ActivityFlag] END AS [ActivityFlag]
				FROM [Pronto].[dbo].[Load_ProntoCouponDetails] (NOLOCK) 
				End

					DBCC SHRINKFILE(Pronto_log, 1);
GO
