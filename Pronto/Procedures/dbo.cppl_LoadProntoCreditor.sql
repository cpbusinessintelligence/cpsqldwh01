SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoCreditor]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoCreditor]
    --' ---------------------------
    --' Purpose: Load ProntoCreditor Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

		TRUNCATE TABLE [Pronto].[dbo].[ProntoCreditor];

		INSERT INTO [Pronto].[dbo].[ProntoCreditor] WITH(TABLOCK)
		SELECT 
			LEFT([AccountCode], 10)[AccountCode]
			,LEFT([PayToCode], 10)[PayToCode]
			,[Shortname]
			,LEFT([Type], 10)[Type]
			,[AccountStatus]
			,[SettlementDiscCode]
			,[VolumeDiscCode]
			,CAST([DiscRate] AS [money])[DiscRate]
			,[CurrCode]
			,CAST([UserOnlyDate] AS [date])[UserOnlyDate]
			,[UserOnlyAlpha20]
			,[UserOnlyAlpha]
			,CAST([UserOnlyNum1] AS [float])[UserOnlyNum1]
			,CAST([UserOnlyNum2] AS [float])[UserOnlyNum2]
			,[ABN]
			,[Address1]
			,[Address2]
			,[Address3]
			,[Address4]
			,[Address5]
			,[Address6]
			,LEFT([Postcode], 10)
			,[Phone]
			,[Fax]
			,[Mobile]
			,CAST([CreditLimit] AS [money])[CreditLimit]
			,REPLACE(REPLACE([Email], 0x0D, ''), 0x0A, '')
		FROM [Pronto].[dbo].[Load_ProntoCreditor];
	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
