SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoDebtor]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoDebtor]
    --' ---------------------------
    --' Purpose: Load ProntoDebtor Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

		TRUNCATE TABLE [Pronto].[dbo].[ProntoDebtor];

		INSERT INTO [Pronto].[dbo].[ProntoDebtor] WITH(TABLOCK)
			SELECT [Accountcode]
			,[BillTo]
			,[Shortname]
			,[Warehouse]
			,[TermDisc]
			,[Territory]
			,[RepCode]
			,[IndustryCode]
			,[CustType]
			,[MarketingFlag]
			,[AverageDaysToPay]
			,[IndustrySubGroup]
			,[Postcode]
			,[Locality]
			,[OriginalRepCode]
			,[LastSale]
			,[ABN]
			,[RepName]
			,[CrmAccount]
			,[CrmParent]
			,[CrmType]
			,[CrmRegion]
			,REPLACE(REPLACE([OriginalRepName], 0x0D, ''), 0x0A, '')
		  FROM [Pronto].[dbo].[Load_ProntoDebtor] where accountcode is not null
	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
