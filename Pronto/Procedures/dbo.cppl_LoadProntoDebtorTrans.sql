SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoDebtorTrans] AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoDebtorTrans]
    --' ---------------------------
    --' Purpose: Load ProntoDebtorTrans Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================



	DBCC SHRINKFILE(Pronto_log, 1);

TRUNCATE TABLE [Pronto].[dbo].[ProntoDebtorTrans];

/****** Object:  Index [idx_ProntoDebtorTrans_AcctTypeDate]    Script Date: 08/22/2012 13:01:56 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoDebtorTrans]') AND name = N'idx_ProntoDebtorTrans_AcctTypeDate')
DROP INDEX [idx_ProntoDebtorTrans_AcctTypeDate] ON [dbo].[ProntoDebtorTrans] WITH ( ONLINE = OFF );

/*INSERT INTO [Pronto].[dbo].[ProntoDebtorTrans]
           ([AccountCode]
           ,[BrAccCode]
           ,[TransType]
           ,[TransDate]
           ,[TransRef]
           ,[TransDetails]
           ,[BatchRef]
           ,[TransAmount]
           ,[TermsDiscAmount]
           ,[WhseCode]
           ,[OrderNo]
           ,[BoSuffix]
           ,[JobCode]
           ,[RepCode1]
           ,[Territory]
           ,[Cost]
           ,[SundryCharges]
           ,[ProvincialTaxAmount]
           ,[TaxEamt]
           ,[NonTamt]
           ,[TaxAmt1]
           ,[TaxAmt2]
           ,[TaxAmt3]
           ,[TaxAmt4]
           ,[SalesTaxClaim]
           ,[TermsDisc]
           ,[OsAmount]
           ,[CurrCode]
           ,[TransDateStamp]
           ,[TransTimeStamp]
           ,[Uid]
           ,[InvoiceStatus]
           ,[InvoiceDate]
           ,[InvoiceSource]
           ,[DueDate]
           ,[TransNo]
           ,[ReportedDate]
           ,[Spare1]
           ,[UserOnlyDate1]
           ,[UserOnlyAlpha4_1]
           ,[UserOnlyAlpha4_2]
           ,[UserOnlyNum1]
           ,[UserOnlyNum2]
           ,[AgeingDate]
           ,[TaxCode]
           ,[AnalysisCode])

SELECT [AccountCode]
      ,[BrAccCode]
      ,[TransType]
      ,[TransDate]
      ,[TransRef]
      ,[TransDetails]
      ,[BatchRef]
      ,[TransAmount]
      ,[TermsDiscAmount]
      ,[WhseCode]
      ,[OrderNo]
      ,[BoSuffix]
      ,[JobCode]
      ,[RepCode1]
      ,[Territory]
      ,[Cost]
      ,[SundryCharges]
      ,[ProvincialTaxAmount]
      ,[TaxEamt]
      ,[NonTamt]
      ,[TaxAmt1]
      ,[TaxAmt2]
      ,[TaxAmt3]
      ,[TaxAmt4]
      ,[SalesTaxClaim]
      ,[TermsDisc]
      ,[OsAmount]
      ,[CurrCode]
      ,[TransDateStamp]
      ,[TransTimeStamp]
      ,[Uid]
      ,[InvoiceStatus]
      ,[InvoiceDate]
      ,[InvoiceSource]
      ,[DueDate]
      ,[TransNo]
      ,[ReportedDate]
      ,[Spare1]
      ,[UserOnlyDate1]
      ,[UserOnlyAlpha4_1]
      ,[UserOnlyAlpha4_2]
      ,[UserOnlyNum1]
      ,[UserOnlyNum2]
      ,[AgeingDate]
      ,[TaxCode]
      ,[AnalysisCode]
FROM [Pronto].[dbo].[Load_ProntoDebtorTrans];*/

INSERT INTO [Pronto].[dbo].[ProntoDebtorTrans]
           ([AccountCode]
           ,[BrAccCode]
           ,[TransType]
           ,[TransDate]
           ,[TransRef]
           ,[TransDetails]
           ,[BatchRef]
           ,[TransAmount]
           ,[TermsDiscAmount]
           ,[WhseCode]
           ,[OrderNo]
           ,[BoSuffix]
           ,[JobCode]
           ,[RepCode1]
           ,[Territory]
           ,[Cost]
           ,[SundryCharges]
           ,[ProvincialTaxAmount]
           ,[TaxEamt]
           ,[NonTamt]
           ,[TaxAmt1]
           ,[TaxAmt2]
           ,[TaxAmt3]
           ,[TaxAmt4]
           ,[SalesTaxClaim]
           ,[TermsDisc]
           ,[OsAmount]
           ,[CurrCode]
           ,[TransDateStamp]
           ,[TransTimeStamp]
           ,[Uid]
           ,[InvoiceStatus]
           ,[InvoiceDate]
           ,[InvoiceSource]
           ,[DueDate]
           ,[TransNo]
           ,[ReportedDate]
           ,[Spare1]
           ,[UserOnlyDate1]
           ,[UserOnlyAlpha4_1]
           ,[UserOnlyAlpha4_2]
           ,[UserOnlyNum1]
           ,[UserOnlyNum2]
           ,[AgeingDate]
           ,[TaxCode]
           ,[AnalysisCode])
SELECT [AccountCode]
      ,[BrAccCode]
      ,[TransType]
      ,CASE ISDATE([TransDate]) WHEN 1 THEN CONVERT(date, [TransDate]) ELSE Null END
      ,[TransRef]
      ,[TransDetails]
      ,[BatchRef]
      ,CASE ISNUMERIC(([TransAmount] + 'e0')) WHEN 1 THEN CONVERT(money, [TransAmount]) ELSE 0.00 END
      ,[TermsDiscAmount]
      ,[WhseCode]
      ,[OrderNo]
      ,[BoSuffix]
      ,[JobCode]
      ,[RepCode1]
      ,[Territory]
      ,CASE ISNUMERIC(([Cost] + 'e0')) WHEN 1 THEN CONVERT(money, [Cost]) ELSE 0.00 END
      ,CASE ISNUMERIC(([SundryCharges] + 'e0')) WHEN 1 THEN CONVERT(money, [SundryCharges]) ELSE 0.00 END
      ,CASE ISNUMERIC(([ProvincialTaxAmount] + 'e0')) WHEN 1 THEN CONVERT(money, [ProvincialTaxAmount]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxEamt] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxEamt]) ELSE 0.00 END
      ,CASE ISNUMERIC(([NonTamt] + 'e0')) WHEN 1 THEN CONVERT(money, [NonTamt]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt1] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt1]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt2] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt2]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt3] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt3]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt4] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt4]) ELSE 0.00 END
      ,[SalesTaxClaim]
      ,[TermsDisc]
      ,CASE ISNUMERIC(([OsAmount] + 'e0')) WHEN 1 THEN CONVERT(money, [OsAmount]) ELSE 0.00 END
      ,[CurrCode]
      ,CASE ISDATE([TransDateStamp]) WHEN 1 THEN CONVERT(date, [TransDateStamp]) ELSE Null END
      ,CASE ISDATE([TransTimeStamp]) WHEN 1 THEN CONVERT(time, [TransTimeStamp]) ELSE Null END
      ,[Uid]
      ,[InvoiceStatus]
      ,CASE ISDATE([InvoiceDate]) WHEN 1 THEN CONVERT(date, [InvoiceDate]) ELSE Null END
      ,[InvoiceSource]
      ,CASE ISDATE([DueDate]) WHEN 1 THEN CONVERT(date, [DueDAte]) ELSE Null END
      ,[TransNo]
      ,CASE ISDATE([ReportedDate]) WHEN 1 THEN CONVERT(date, [ReportedDate]) ELSE Null END
      ,[Spare1]
      ,[UserOnlyDate1]
      ,[UserOnlyAlpha4_1]
      ,[UserOnlyAlpha4_2]
      ,[UserOnlyNum1]
      ,[UserOnlyNum2]
      ,CASE ISDATE([AgeingDate]) WHEN 1 THEN CONVERT(date, [AgeingDate]) ELSE Null END
      ,[TaxCode]
      ,[AnalysisCode]
  FROM [Pronto].[dbo].[Load_ProntoDebtorTrans];

	DBCC SHRINKFILE(Pronto_log, 1);
  
  INSERT INTO [Pronto].[dbo].[ProntoDebtorTrans]
           ([AccountCode]
           ,[BrAccCode]
           ,[TransType]
           ,[TransDate]
           ,[TransRef]
           ,[TransDetails]
           ,[BatchRef]
           ,[TransAmount]
           ,[TermsDiscAmount]
           ,[WhseCode]
           ,[OrderNo]
           ,[BoSuffix]
           ,[JobCode]
           ,[RepCode1]
           ,[Territory]
           ,[Cost]
           ,[SundryCharges]
           ,[ProvincialTaxAmount]
           ,[TaxEamt]
           ,[NonTamt]
           ,[TaxAmt1]
           ,[TaxAmt2]
           ,[TaxAmt3]
           ,[TaxAmt4]
           ,[SalesTaxClaim]
           ,[TermsDisc]
           ,[OsAmount]
           ,[CurrCode]
           ,[TransDateStamp]
           ,[TransTimeStamp]
           ,[Uid]
           ,[InvoiceStatus]
           ,[InvoiceDate]
           ,[InvoiceSource]
           ,[DueDate]
           ,[TransNo]
           ,[ReportedDate]
           ,[Spare1]
           ,[UserOnlyDate1]
           ,[UserOnlyAlpha4_1]
           ,[UserOnlyAlpha4_2]
           ,[UserOnlyNum1]
           ,[UserOnlyNum2]
           ,[AgeingDate]
           ,[TaxCode]
           ,[AnalysisCode])
SELECT [AccountCode]
      ,[BrAccCode]
      ,[TransType]
      ,CASE ISDATE([TransDate]) WHEN 1 THEN CONVERT(date, [TransDate]) ELSE Null END
      ,[TransRef]
      ,[TransDetails]
      ,[BatchRef]
      ,CASE ISNUMERIC(([TransAmount] + 'e0')) WHEN 1 THEN CONVERT(money, [TransAmount]) ELSE 0.00 END
      ,[TermsDiscAmount]
      ,[WhseCode]
      ,[OrderNo]
      ,[BoSuffix]
      ,[JobCode]
      ,[RepCode1]
      ,[Territory]
      ,CASE ISNUMERIC(([Cost] + 'e0')) WHEN 1 THEN CONVERT(money, [Cost]) ELSE 0.00 END
      ,CASE ISNUMERIC(([SundryCharges] + 'e0')) WHEN 1 THEN CONVERT(money, [SundryCharges]) ELSE 0.00 END
      ,CASE ISNUMERIC(([ProvincialTaxAmount] + 'e0')) WHEN 1 THEN CONVERT(money, [ProvincialTaxAmount]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxEamt] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxEamt]) ELSE 0.00 END
      ,CASE ISNUMERIC(([NonTamt] + 'e0')) WHEN 1 THEN CONVERT(money, [NonTamt]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt1] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt1]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt2] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt2]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt3] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt3]) ELSE 0.00 END
      ,CASE ISNUMERIC(([TaxAmt4] + 'e0')) WHEN 1 THEN CONVERT(money, [TaxAmt4]) ELSE 0.00 END
      ,[SalesTaxClaim]
      ,[TermsDisc]
      ,CASE ISNUMERIC(([OsAmount] + 'e0')) WHEN 1 THEN CONVERT(money, [OsAmount]) ELSE 0.00 END
      ,[CurrCode]
      ,CASE ISDATE([TransDateStamp]) WHEN 1 THEN CONVERT(date, [TransDateStamp]) ELSE Null END
      ,CASE ISDATE([TransTimeStamp]) WHEN 1 THEN CONVERT(time, [TransTimeStamp]) ELSE Null END
      ,[Uid]
      ,[InvoiceStatus]
      ,CASE ISDATE([InvoiceDate]) WHEN 1 THEN CONVERT(date, [InvoiceDate]) ELSE Null END
      ,[InvoiceSource]
      ,CASE ISDATE([DueDate]) WHEN 1 THEN CONVERT(date, [DueDAte]) ELSE Null END
      ,[TransNo]
      ,CASE ISDATE([ReportedDate]) WHEN 1 THEN CONVERT(date, [ReportedDate]) ELSE Null END
      ,[Spare1]
      ,[UserOnlyDate1]
      ,[UserOnlyAlpha4_1]
      ,[UserOnlyAlpha4_2]
      ,[UserOnlyNum1]
      ,[UserOnlyNum2]
      ,CASE ISDATE([AgeingDate]) WHEN 1 THEN CONVERT(date, [AgeingDate]) ELSE Null END
      ,[TaxCode]
      ,[AnalysisCode]
  FROM [Pronto].[dbo].[Load_ProntoDebtorTransArchive];
  
	DBCC SHRINKFILE(Pronto_log, 1);

CREATE NONCLUSTERED INDEX [idx_ProntoDebtorTrans_AcctTypeDate] ON [dbo].[ProntoDebtorTrans] 
(
	[AccountCode] ASC,
	[TransType] ASC,
	[TransDate] ASC,
	[TransRef] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];

	DBCC SHRINKFILE(Pronto_log, 1);

END

GO
