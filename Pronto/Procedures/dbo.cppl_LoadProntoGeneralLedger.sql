SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_LoadProntoGeneralLedger]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoGeneralLedger]
    --' ---------------------------
    --' Purpose: Load ProntoGeneralLedger Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

	SET NOCOUNT ON;

	--DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

		TRUNCATE TABLE [Pronto].[dbo].[ProntoGeneralLedger];

		INSERT INTO [Pronto].[dbo].[ProntoGeneralLedger]
		SELECT DISTINCT [AccountCode]
			  ,[AltKey]
			  ,[Description]
			  ,[BalanceSheetOrProfitLoss]
			  ,[AccountType]
			  ,[Company]
			  ,[BusinessUnit]
			  ,[Division]
			  ,[Account]
			  ,[CostCentre]
			  ,[SpecialReportFlag]
			  ,[UserOnlyAlpha20_1]
			  ,[UserOnlyAlpha20_2]
			  ,[MlcCode]
			  ,[CompanyId]
			  ,[TotalLevel]
			  ,[UserOnlyAlpha4_1]
			  ,[UserOnlyAlpha4_2]
			  ,[UserOnlyAlpha4_3]
			  ,REPLACE(REPLACE([UserOnlyAlpha4_4 ], 0x0D, ''), 0x0A, '')
		  FROM [Pronto].[dbo].[Load_ProntoGeneralLedger]


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	--DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
