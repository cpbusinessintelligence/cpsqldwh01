SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_LoadProntoGeneralLedgerV1]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoGeneralLedger]
    --' ---------------------------
    --' Purpose: Load ProntoGeneralLedger Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

		TRUNCATE TABLE [Pronto].[dbo].[ProntoGeneralLedgerV1];

		INSERT INTO [Pronto].[dbo].[ProntoGeneralLedgerV1]([SysActionCode]
      ,[SysConsCode]
      ,[SysCompcode]
      ,[AccountCode]
      ,[AltKey]
      ,[Description]
      ,[BalanceSheetOrProfitLoss]
      ,[AccountType]
      ,[Company]
      ,[BusinessUnit]
      ,[Division]
      ,[Account]
      ,[CostCentre]
      ,[SpecialReportFlag]
      ,[UserOnlyAlpha20_1]
      ,[UserOnlyAlpha20_2]
      ,[MlcCode]
      ,[CompanyId]
      ,[TotalLevel]
      ,[UserOnlyAlpha4_1]
      ,[UserOnlyAlpha4_2]
      ,[UserOnlyAlpha4_3]
      ,[UserOnlyAlpha4_4])


		SELECT DISTINCT [bi_sys_action_code]
      ,[bi_sys_comp_cons_code]
      ,[bi_sys_comp_code]
      ,[bi_gd_gl_accountcode]
      ,[bi_gd_gl_alt_key]
      ,[bi_gd_gl_desc]
      ,[bi_gd_gl_bal_sheet_profit_loss]
      ,[bi_gd_gl_account_type]
      ,[bi_gd_company]
      ,[bi_gd_business_unit]
      ,[bi_gd_department]
	  ,[bi_gd_account]
      ,[bi_gd_cost_centre]
      ,[bi_gd_gl_special_report_flag]
      ,[bi_gd_gl_user_only_alpha20_1]
      ,[bi_gd_gl_user_only_alpha20_2]
      ,[bi_gd_gl_mlc_code]
	  ,[bi_sys_comp_code]
      ,[bi_gd_total_level]
      ,[bi_gd_user_only_alpha4_1]
      ,[bi_gd_user_only_alpha4_2]
      ,[bi_gd_user_only_alpha4_3]
      ,REPLACE(REPLACE([bi_gd_user_only_alpha4_4], 0x0D, ''), 0x0A, '')
  FROM [Pronto].[dbo].[Load_ProntoGeneralLedgerV1]

 
	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;

GO
