SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_LoadProntoGeoDim]
AS
BEGIN


     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoGeoDim]
    --' ---------------------------
    --' Purpose: Load ProntoGeoDim Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================


	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		TRUNCATE TABLE [Pronto].[dbo].[ProntoGeoDim];

		INSERT INTO [Pronto].[dbo].[ProntoGeoDim] WITH(TABLOCK)
		SELECT DISTINCT
				[CompanyCode]
				,[Postcode]
				,[State]
				,[PrimaryBillZone]
				,[SecondaryBillZone]
				,[Warehouse]
				,[PrimaryCostZone]
				,[SecondryCostZone]
				,[Territory]
				,REPLACE(REPLACE([Locality], 0x0D, ''), 0x0A, '')
		FROM [Pronto].[dbo].[Load_ProntoGeoDim];
	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;

GO
