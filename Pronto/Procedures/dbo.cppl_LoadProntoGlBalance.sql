SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_LoadProntoGlBalance]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoGlBalance]
    --' ---------------------------
    --' Purpose: Load ProntoGlBalance Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================



	SET NOCOUNT ON;

	--DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		TRUNCATE TABLE [Pronto].[dbo].[ProntoGlBalance];

		INSERT INTO [Pronto].[dbo].[ProntoGlBalance] 
		SELECT DISTINCT
				LEFT([AccountNumber], 10)
				,CAST([AccountingYear] AS [smallint])
				,CAST([AccountingPeriod] AS [smallint])
				,CAST([GLBalance] AS [money])
				,CAST([GLBudget] AS [money])
				,CAST([TransactionDate] AS [date])
				,REPLACE(REPLACE(LEFT([DatabaseCode], 3), 0x0D, ''), 0x0A, '')
		FROM [Pronto].[dbo].[Load_ProntoGlBalance] 


		
  
  update ProntoGlBalance set transactiondate=case when Accountingperiod in (10,11,12) then convert(varchar(4),(Accountingyear+1)) else convert(varchar(4),Accountingyear) end+'-'+
	  case when Accountingperiod='1' then '04' 
	   when Accountingperiod='2' then '05'
	   when Accountingperiod='3' then '06'
	   when Accountingperiod='4' then '07'
	   when Accountingperiod='5' then '08'
	   when Accountingperiod='6' then '09'
	   when Accountingperiod='7' then '10'
	   when Accountingperiod='8' then '11'
	   when Accountingperiod='9' then '12'
	   when Accountingperiod='10' then '01'
	   when Accountingperiod='11' then '02'
	   when Accountingperiod='12' then '03'
	  else '00' end+'-'+convert(varchar(2),day(Transactiondate))

  from ProntoGlBalance 



	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH
/*
		DELETE FROM [CPVMDBHOUSE].ECA_DW_v01.dbo.[fact-GL-balance];
		INSERT INTO [CPVMDBHOUSE].ECA_DW_v01.dbo.[fact-GL-balance]
				   ([Company-code]
				   ,[bu-code]
				   ,[department-code]
				   ,[Account-code]
				   ,[Period-ID]
				   ,[GL-balance]
				   ,[GL-budget]
				   ,[Database-code])
		SELECT CompanyCode
			  ,BusinessUnitCode
			  ,DepartmentCode
			  ,AccountCode
			  ,AccountingPeriod
			  ,GLBalance
			  ,GLBudget
			  ,DatabaseCode
		FROM [Pronto].[dbo].[cppl_vw_TM1GlBalance]
		WHERE NOT(GLBalance = 0 AND GLBudget = 0)
		AND NOT(CompanyCode = 'H')
*/

	--BEGIN TRAN
		--TRUNCATE TABLE [CPVMDBHOUSE].ECA_DW_v01.dbo.[fact-GL-balance];
		--INSERT INTO [CPVMDBHOUSE].ECA_DW_v01.dbo.[fact-GL-balance] WITH(TABLOCK)
		--		   ([Company-code]
		--		   ,[bu-code]
		--		   ,[department-code]
		--		   ,[Account-code]
		--		   ,[Period-ID]
		--		   ,[GL-balance]
		--		   ,[GL-budget]
		--		   ,[Database-code])
		--SELECT CompanyCode
		--	  ,BusinessUnitCode
		--	  ,DepartmentCode
		--	  ,AccountCode
		--	  ,AccountingPeriod
		--	  ,GLBalance
		--	  ,GLBudget
		--	  ,DatabaseCode
		--FROM [cppl_vw_TM1GlBalance]
		--WHERE NOT(GLBalance = 0 AND GLBudget = 0)
		--AND NOT(CompanyCode = 'H')
		
--		INSERT INTO [CPVMDBHOUSE].ECA_DW_v01.dbo.[fact-GL-balance] 
--		( 
--		[Company-code]
--		, [Bu-code]
--		, [Department-code]
--		, [Account-code]
--		, [Period-ID]
--		, [GL-balance]
--		, [GL-budget]
--		, [Database-code]
--		)
--		SELECT 
--		[Company-code]
--		, [Bu-code]
--		, [Department-code]
--		, [Account-code]
--		, [Period-ID]
--		, [GL-balance]
--		, [GL-budget]
--		, [Database-code]
--		FROM [CPVMDBHOUSE].ECA_DW_v01.dbo.[load-GL-other]

--COMMIT TRAN;

	--DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
