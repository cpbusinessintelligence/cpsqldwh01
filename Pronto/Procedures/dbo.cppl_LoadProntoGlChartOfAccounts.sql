SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoGlChartOfAccounts]
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoGlChartOfAccounts]
    --' ---------------------------
    --' Purpose: Load ProntoGlChartOfAccounts Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

		TRUNCATE TABLE [Pronto].[dbo].[ProntoGlChartOfAccounts];

		INSERT INTO [Pronto].[dbo].[ProntoGlChartOfAccounts] 
		SELECT DISTINCT
				LEFT(ComponentType,5)
				,LEFT(AccountType,5)
				,LEFT(AccountCode,5)
				,AccountName
				,REPLACE(REPLACE(LEFT([AccountType1],1), 0x0D, ''), 0x0A, '')
		  FROM [Pronto].[dbo].[Load_ProntoGlChartOfAccounts] 



	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;
GO
