SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[cppl_LoadProntoGlFact] 
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoGlFact]
    --' ---------------------------
    --' Purpose: Load ProntoGlFact Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================



	SET NOCOUNT ON;

	--DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [dbo].[ProntoGlFact]
	
		INSERT INTO [dbo].[ProntoGlFact] WITH(TABLOCK)
			(
			  [AccountCode]
			  ,[Year]
			  ,[Period]
			  ,[Actual]
			  ,[Budget]
			  ,[ReportDate]
			  ,[Company]
			)
		SELECT DISTINCT
				[AccountCode]
				,CAST([Year] AS [int])
				,CAST([Period] AS [int])
				,CAST([Actual] AS [money])
				,CAST([Budget] AS [money])
				,CAST([ReportDate] AS [date])
				,LEFT(REPLACE(REPLACE([Company], 0x0d, 0), 0X0a, 0), 5) AS [SsnUserOnlyNum4]
			FROM [dbo].[Load_ProntoGlFact];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	--DBCC SHRINKFILE(Pronto_log, 1);

END

GO
