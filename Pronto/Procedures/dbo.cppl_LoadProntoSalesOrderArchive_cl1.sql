SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_LoadProntoSalesOrderArchive_cl1]
AS
BEGIN


     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoSalesOrderArchive_cl1]
    --' ---------------------------
    --' Purpose: LoadProntoSalesOrderArchive for cl1 Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================

SET DATEFORMAT dmy;

	DBCC SHRINKFILE(Pronto_log, 1);

DELETE FROM [ProntoSalesOrderArchive] WHERE CompanyCode = 'CL1';

	DBCC SHRINKFILE(Pronto_log, 1);

INSERT INTO [Pronto].[dbo].[ProntoSalesOrderArchive]
           ([CompanyCode]
           ,[OrderNumber]
           ,[BoSuffix]
           ,[CustomerCode]
           ,[OrderDate]
           ,[DeliveryDate]
           ,[DeliveryTime]
           ,[EarliestDeliveryDateTime]
           ,[ActualDeliveryDate]
           ,[ActualDeliveryTime]
           ,[LatestActualDeliveryDateTime]
           ,[FollowUpDate]
           ,[OrderPackages]
           ,[OrderWeight]
           ,[ExtraReference]
           ,[OrderStatus]
           ,[OrderReasonCode]
           ,[OrderTypeCode]
           ,[WarehouseCode]
           ,[WarehouseTo]
           ,[TerritoryCode]
           ,[CustomerType]
           ,[RepCode]
           ,[SalesTaxExemptionNumber]
           ,[TermsDiscount]
           ,[PriceCode]
           ,[CustomerReference]
           ,[InvoiceNumber]
           ,[CreditNoteNumber]
           ,[InvoiceDate]
           ,[ProcessingDate]
           ,[PostingPeriod]
           ,[AddToMailerFlag]
           ,[AutoTransferFlag]
           ,[CarrierCode]
           ,[CarrierDropSeq]
           ,[ConsignmentNote]
           ,[OrderPriority]
           ,[PartShipmentAllowed]
           ,[OrderTotalAmount]
           ,[OrderedTotalCost]
           ,[OrderTotalTax]
           ,[OrderTotalTaxAdj]
           ,[OrderTotalShippedAmount]
           ,[OrderTotalShippedTax]
           ,[OrderTotalShippedTaxAdj]
           ,[OrderTotalShippedCost]
           ,[OrderTotalCharges]
           ,[DateStamp]
           ,[TimeStamp]
           ,[CurrencyCode]
           ,[UserIdCode]
           ,[CurrencyFinalExchangeRate]
           ,[OrderSource]
           ,[ContactNameCode]
           ,[MiscCode]
           ,[BatchReference]
           )

SELECT 'CL1'
	  ,[OrderNumber]
      ,ISNULL(NULLIF([BoSuffix], ''), '')
      ,[CustomerCode] 
      ,CAST(NULLIF([OrderDate] , '') AS DATE)[OrderDate]
      ,CAST(NULLIF([DeliveryDate] , '') AS DATE)[DeliveryDate]
      ,CAST(NULLIF([DeliveryTime] , '') AS TIME)[DeliveryTime]
      ,CAST(NULLIF([EarliestDeliveryDateTime] , '') AS DATETIME)[EarliestDeliveryDateTime]
      ,CAST(NULLIF([ActualDeliveryDate] , '') AS DATE)[ActualDeliveryDate]
      ,CAST(NULLIF([ActualDeliveryTime] , '') AS TIME)[ActualDeliveryTime]
      ,CAST(NULLIF([LatestActualDeliveryDateTime] , '') AS DATETIME)[LatestActualDeliveryDateTime]
      ,CAST(NULLIF([FollowUpDate] , '') AS DATE)[FollowUpDate]
      ,[OrderPackages]
      ,ISNULL(NULLIF([OrderWeight], ''), 0.00) AS [OrderWeight]
      ,[ExtraReference]
      ,[OrderStatus]
      ,[OrderReasonCode]
      ,[OrderTypeCode]
      ,[WarehouseCode]
      ,[WarehouseTo]
      ,[TerritoryCode]
      ,[CustomerType]
      ,[RepCode]
      ,[SalesTaxExemptionNumber]
      ,[TermsDiscount]
      ,[PriceCode]
      ,[CustomerReference]
      ,[InvoiceNumber]
      ,[CreditNoteNumber]
      ,CAST(NULLIF(NULLIF([InvoiceDate] , ''), '0') AS DATE) [InvoiceDate]
      ,CAST(NULLIF([ProcessingDate] , '') AS DATE) [ProcessingDate]
      ,[PostingPeriod]
      ,[AddToMailerFlag]
      ,[AutoTransferFlag]
      ,[CarrierCode]
      ,[CarrierDropSeq]
      ,[ConsignmentNote]
      ,[OrderPriority]
      ,[PartShipmentAllowed]
      ,NULLIF(NULLIF([OrderTotalAmount] , ''), 'Y')[OrderTotalAmount]
      ,[OrderedTotalCost]
      ,[OrderTotalTax]
      ,[OrderTotalTaxAdj]
      ,[OrderTotalShippedAmount]
      ,[OrderTotalShippedTax]
      ,[OrderTotalShippedTaxAdj]
      ,[OrderTotalShippedCost]
      ,[OrderTotalCharges]
      ,CAST(NULLIF(NULLIF([DateStamp] , ''), '0.00') AS DATE)[DateStamp]
      ,CAST(NULLIF([TimeStamp] , '') AS TIME)[TimeStamp]
      ,[CurrencyCode]
      ,[UserIdCode]
      ,ISNULL(NULLIF([CurrencyFinalExchangeRate], ''), 0.00) AS [CurrencyFinalExchangeRate]
      ,[OrderSource]
      ,[ContactNameCode]
      ,[MiscCode]
      ,[BatchReference]
  FROM [Pronto].[dbo].[Load_ProntoSalesOrderArchive]
  WHERE OrderNumber NOT IN ('1994770','970945','1269708');

  --TRUNCATE TABLE [Pronto].[dbo].[Load_ProntoSalesOrderArchive];
 
	DBCC SHRINKFILE(Pronto_log, 1);

END  




GO
