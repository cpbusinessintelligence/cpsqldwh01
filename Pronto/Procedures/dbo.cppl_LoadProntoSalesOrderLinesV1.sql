SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[cppl_LoadProntoSalesOrderLinesV1]
AS
BEGIN
      --'=====================================================================
    --' CP -Stored Procedure -[PDG_LoadProntoSalesOrderLines]
    --' ---------------------------
    --' Purpose:Load ProntoSalesOrderLines Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/09/2014    AB      1.00    Created the procedure                             --AB20140905

    --'=====================================================================


	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

		TRUNCATE TABLE [Pronto].[dbo].[ProntoSalesOrderLinesV1];

		/****** Object:  Index [PK_ProntoSalesOrderLines_1]    Script Date: 01/30/2013 17:40:13 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoSalesOrderLinesV1]') AND name = N'PK_ProntoSalesOrderLines_2')
		ALTER TABLE [dbo].[ProntoSalesOrderLinesV1] DROP CONSTRAINT [PK_ProntoSalesOrderLines_2];

		INSERT INTO [Pronto].[dbo].[ProntoSalesOrderLinesV1] 
		SELECT DISTINCT
				[OrderNumber]
				,ISNULL( [Suffix] ,'') 
				,CAST([LineSequence] AS [float])
				,[CompanyCode]
				,[StockCode]
				,[StockGroup]
				,CAST([NetAmount] AS [money])
				,CAST([GSTAmount] AS [money])
				,CAST([GrossAmount] AS [money])
				,CAST([CostAmount] AS [money])
				,CAST(REPLACE(REPLACE([Quantity], 0x0D, ''), 0x0A, '') AS [float])
		  FROM [Pronto].[dbo].[Load_ProntoSalesOrderLinesV1];
		  
	   TRUNCATE TABLE dbo.[Load_ProntoSalesOrderLinesV1];

		/****** Object:  Index [PK_ProntoSalesOrderLines_1]    Script Date: 01/30/2013 17:40:14 ******/
		ALTER TABLE [dbo].[ProntoSalesOrderLinesV1] ADD  CONSTRAINT [PK_ProntoSalesOrderLines_2] PRIMARY KEY CLUSTERED 
		(
			[OrderNumber] ASC,
			[Suffix] ASC,
			[LineSequence] ASC,
			[CompanyCode] ASC,
			[StockCode] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;



GO
