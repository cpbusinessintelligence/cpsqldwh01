SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[cppl_LoadProntoSalesOrderV1]
AS
BEGIN

      --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoSalesOrder]
    --' ---------------------------
    --' Purpose:Load ProntoSalesOrder Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/09/2014    AB      1.00    Created the procedure                             --AB20140905

    --'=====================================================================


	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
    
		TRUNCATE TABLE [Pronto].[dbo].[ProntoSalesOrderV1];

		/****** Object:  Index [_dta_index_ProntoSalesOrder_7_53575229__K7_K6_K4_K8_K1_K3_12]    Script Date: 01/30/2013 17:34:21 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoSalesOrderV1]') AND name = N'_dta_index_ProntoSalesOrderV1_7_53575229__K7_K6_K4_K8_K1_K3_12')
		DROP INDEX [_dta_index_ProntoSalesOrderV1_7_53575229__K7_K6_K4_K8_K1_K3_12] ON [dbo].[ProntoSalesOrderV1] WITH ( ONLINE = OFF );

		/****** Object:  Index [idx_ProntoSalesOrder_CustomerOrderCompanyDate]    Script Date: 01/30/2013 17:35:05 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoSalesOrderV1]') AND name = N'idx_ProntoSalesOrderV1_CustomerOrderCompanyDate')
		DROP INDEX [idx_ProntoSalesOrderV1_CustomerOrderCompanyDate] ON [dbo].[ProntoSalesOrderV1] WITH ( ONLINE = OFF );

		/****** Object:  Index [PK_ProntoSalesOrder]    Script Date: 01/30/2013 17:38:46 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoSalesOrderV1]') AND name = N'PK_ProntoSalesOrder1')
		ALTER TABLE [dbo].[ProntoSalesOrderV1] DROP CONSTRAINT [PK_ProntoSalesOrder1];

		INSERT INTO [dbo].[ProntoSalesOrderV1] 
		SELECT DISTINCT 
				LTRIM(RTRIM([OrderNumber]))
				,Isnull([Suffix],'')
				,LTRIM(RTRIM([CompanyCode]))
				,[CustomerCode]
				,[WarehouseCode]
				,[TerritoryCode]
				,LTRIM(RTRIM([SalesType]))
				,CAST([AccountingDate] AS [date])
				,CAST([AccountingPeriod] AS [int])
				,CAST([NetAmount] AS [money])
				,CAST([GSTAmount] AS [money])
				,CAST([GrossAmount] AS [money])
				,CAST(REPLACE(REPLACE([CostAmount], 0x0D, ''), 0x0A, '') AS [money])
		  FROM [dbo].[Load_ProntoSalesOrderV1];
		  
		  --TRUNCATE TABLE [Load_ProntoSalesOrder];

		/****** Object:  Index [_dta_index_ProntoSalesOrder_7_53575229__K7_K6_K4_K8_K1_K3_12]    Script Date: 01/30/2013 17:34:21 ******/
		CREATE NONCLUSTERED INDEX [_dta_index_ProntoSalesOrderV1_7_53575229__K7_K6_K4_K8_K1_K3_12] ON [dbo].[ProntoSalesOrderV1] 
		(
			[SalesType] ASC,
			[TerritoryCode] ASC,
			[CustomerCode] ASC,
			[AccountingDate] ASC,
			[OrderNumber] ASC,
			[CompanyCode] ASC
		)
		INCLUDE ( [GrossAmount]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];
		
		/****** Object:  Index [idx_ProntoSalesOrder_CustomerOrderCompanyDate]    Script Date: 01/30/2013 17:35:05 ******/
		CREATE NONCLUSTERED INDEX [idx_ProntoSalesOrderV1_CustomerOrderCompanyDate] ON [dbo].[ProntoSalesOrderV1] 
		(
			[CustomerCode] ASC
		)
		INCLUDE ( [OrderNumber],
		[CompanyCode],
		[AccountingDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

		/****** Object:  Index [PK_ProntoSalesOrder]    Script Date: 01/30/2013 17:38:47 ******/
		ALTER TABLE [dbo].[ProntoSalesOrderV1] ADD  CONSTRAINT [PK_ProntoSalesOrder1] PRIMARY KEY CLUSTERED 
		(
			[OrderNumber] ASC,
			[Suffix] AsC,
			[CompanyCode] ASC,
			[CustomerCode] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;


GO
