SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[cppl_LoadProntoStockChargesOverride]
AS
BEGIN
     --'=====================================================================
    --' CP -Stored Procedure -[cppl_ProntoStockChargesOverride]
    --' ---------------------------
    --' Purpose: Load ProntoStockChargesOverride Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 08/09/2014    AB      1.00                                                     --AB20140908

    --'=====================================================================
	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY

		--we do a merge now, to retain prior effective date rates that may have been updated
		--
		MERGE [Pronto].[dbo].[ProntoStockChargesOverride] AS Target
		USING
		(
			SELECT DISTINCT
					ISNULL(CouponPrefix, '') AS [CouponPrefix],
					ISNULL(BranchCode, '') AS [BranchCode],
					ISNULL(ContractorCode, '') AS [ContractorCode],
					ISNULL(RedemptionType, '') AS [RedemptionType],
					CASE ISNUMERIC(ISNULL(RedemptionAmount, '0.00'))
						WHEN 1 THEN CONVERT(decimal(9,2), ISNULL(RedemptionAmount, '0.00'))
						ELSE 0.00
					END AS [RedemptionAmount],
					CONVERT(date,
					  CASE ISDATE(ISNULL(EffectiveDate, '1jan1900'))
						WHEN 1 THEN CONVERT(datetime, ISNULL(EffectiveDate, '1jan1900'))
						ELSE CONVERT(datetime, '1jan1900')
					  END
					) AS [EffectiveDate]
			FROM [Pronto].[dbo].[Load_ProntoStockChargesOverride]
		) AS Source
		ON Target.CouponPrefix = Source.CouponPrefix
		AND Target.BranchCode = Source.BranchCode
		AND Target.ContractorCode = Source.ContractorCode
		AND Target.RedemptionType = Source.RedemptionType
		AND Target.EffectiveDate = Source.EffectiveDate
		WHEN
			NOT MATCHED BY TARGET THEN
				INSERT
				(
					Id,
					CouponPrefix,
					BranchCode,
					ContractorCode,
					RedemptionType,
					RedemptionAmount,
					EffectiveDate,
					DateCreated,
					DateUpdated
				)
				VALUES
				(
					NEWID(),
					Source.CouponPrefix,
					Source.BranchCode,
					Source.ContractorCode,
					Source.RedemptionType,
					Source.RedemptionAmount,
					Source.EffectiveDate,
					GETDATE(),
					GETDATE()
				)
		WHEN
			MATCHED THEN
				UPDATE SET
					Target.RedemptionAmount = Source.RedemptionAmount,
					Target.DateUpdated = GETDATE();
					
					
		/* TRUNCATE TABLE [Pronto].[dbo].[ProntoStockChargesOverride];

		INSERT INTO [Pronto].[dbo].[ProntoStockChargesOverride] WITH(TABLOCK) 
		SELECT DISTINCT
				ISNULL(CouponPrefix, ''),
				ISNULL(BranchCode, ''),
				ISNULL(ContractorCode, ''),
				ISNULL(RedemptionType, ''),
				CASE ISNUMERIC(ISNULL(RedemptionAmount, '0.00'))
					WHEN 1 THEN CONVERT(decimal(9,2), ISNULL(RedemptionAmount, '0.00'))
					ELSE 0.00
				END,
				CASE ISDATE(ISNULL(EffectiveDate, '1jan1900'))
					WHEN 1 THEN CONVERT(datetime, ISNULL(EffectiveDate, '1jan1900'))
					ELSE CONVERT(datetime, '1jan1900')
				END
		FROM [Pronto].[dbo].[Load_ProntoStockChargesOverride]; */

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);
	
	SET NOCOUNT OFF;

END;
GO
