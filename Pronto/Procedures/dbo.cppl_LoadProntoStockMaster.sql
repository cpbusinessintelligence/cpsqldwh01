SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_LoadProntoStockMaster]
AS
BEGIN

      --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoStockMaster]
    --' ---------------------------
    --' Purpose:Load ProntoProntoStockMaster Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/09/2014    AB      1.00    Created the procedure                             --AB20140905

    --'=====================================================================


	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
    
		SET NOCOUNT ON;
		
		TRUNCATE TABLE [Pronto].[dbo].[ProntoStockMaster];

		INSERT INTO [Pronto].[dbo].[ProntoStockMaster] WITH(TABLOCK)
		   ([StockCode]
		   ,[StockGroup]
		   ,[StkAbcClass]
		   ,[StkStockStatus]
		   ,[StkIssueControlCode]
		   ,[StkConditionCode]
		   ,[StkIndicator]
		   ,[StkCyclicCode]
		   ,[StkUserGroup1]
		   ,[StkUserGroup2]
		   ,[StkDescription]
		   ,[StkDescLine2]
		   ,[StkDescLine3]
		   ,[StkUnitDesc]
		   ,[StkPackWeight]
		   ,[StkPackDesc]
		   ,[StkPackQty]
		   ,[StkPackCubicSize]
		   ,[StkConversionFactor]
		   ,[StkAltUnitDesc]
		   ,[StkApnNumber]
		   ,[StkImportTariffCode]
		   ,[StkSerializedFlag]
		   ,[StkStorageTypeFlag]
		   ,[StkStdCost]
		   ,[StkReplacementCost]
		   ,[StkSalesCost]
		   ,[StkDutyPaidCost]
		   ,[StkInfoCost]
		   ,[StkShelfLifeDays]
		   ,[StkWarrantyTypeFlag]
		   ,[StkDateLastChange]
		   ,[StkPricePer]
		   ,[StkReorderPolicy]
		   ,[StkReorderReview]
		   ,[StkReorderBuyer]
		   ,[StkCreationDate]
		   ,[StkMovementCode]
		   ,[StkSalesTypeCode]
		   ,[StkSalesTaxPaidRate]
		   ,[StkSortAnalysisCode]
		   ,[StkExiseQty]
		   ,[StkUnSpscCode]
		   ,[StkAnalysisCode1]
		   ,[StkAnalysisCode2]
		   ,[StkAnalysisCode3]
		   ,[StkSpareAnalysisCode]
		   ,[StkTallyCode]
		   ,[StkSpare1]
		   ,[StkSpare2]
		   ,[StkUserOnlyDate1]
		   ,[StkUserOnlyDate2]
		   ,[StkUserOnlyAlpha1]
		   ,[StkUserOnlyAlpha2]
		   ,[StkUserOnlyAlpha3]
		   ,[StkUserOnlyAlpha4]
		   ,[StkUserOnlyAlpha5]
		   ,[StkUserOnlyAlpha6]
		   ,[StkUserOnlyNum1]
		   ,[StkUserOnlyNum2]
		   ,[StkUserOnlyNum3]
		   ,[StkUserOnlyNum4])
		SELECT 
			LEFT([StockCode], 20) 
			, LEFT([StockGroup], 5)
			, LEFT([StkAbcClass], 50)
			, LEFT([StkStockStatus], 1)
			, LEFT([StkIssueControlCode], 50)
			, LEFT([StkConditionCode], 1)
			, LEFT([StkIndicator], 50)
			, LEFT([StkCyclicCode], 50)
			, LEFT([StkUserGroup1], 50)
			, LEFT([StkUserGroup2], 50)
			, LEFT([StkDescription], 50)
			, LEFT([StkDescLine2], 50)
			, LEFT([StkDescLine3], 50)
			, LEFT([StkUnitDesc], 50)
			, CAST([StkPackWeight] AS [numeric](18, 4))
			, LEFT([StkPackDesc], 50)
			, CAST([StkPackQty] AS [numeric](18, 4))
			, CAST([StkPackCubicSize] AS [numeric](18, 4))
			, CAST([StkConversionFactor] AS [numeric](18, 4))
			, LEFT([StkAltUnitDesc], 50)
			, LEFT([StkApnNumber], 50)
			, LEFT([StkImportTariffCode], 50)
			, CAST((CASE [StkSerializedFlag] WHEN 'Y' THEN 1 ELSE 0 END) AS [bit])
			, CAST((CASE [StkStorageTypeFlag] WHEN 'Y' THEN 1 ELSE 0 END) AS [bit])
			, CAST([StkStdCost] AS [money])
			, CAST([StkReplacementCost] AS [money])
			, CAST([StkSalesCost] AS [money])
			, CAST([StkDutyPaidCost] AS [money])
			, CAST([StkInfoCost] AS [money])
			, CAST([StkShelfLifeDays] AS [int])
			, CAST([StkWarrantyTypeFlag] AS [bit])
			, CAST([StkDateLastChange] AS [date])
			, CAST([StkPricePer] AS [money])
			, LEFT([StkReorderPolicy], 50)
			, LEFT([StkReorderReview], 50)
			, LEFT([StkReorderBuyer], 50)
			, CAST([StkCreationDate] AS [date])
			, LEFT([StkMovementCode], 50)
			, LEFT([StkSalesTypeCode], 50)
			, CAST([StkSalesTaxPaidRate] AS [money])
			, LEFT([StkSortAnalysisCode], 50)
			, CAST([StkExiseQty] AS [numeric](18, 4))
			, LEFT([StkUnSpscCode], 50)
			, LEFT([StkAnalysisCode1], 50)
			, LEFT([StkAnalysisCode2], 50)
			, LEFT([StkAnalysisCode3], 50)
			, LEFT([StkSpareAnalysisCode], 50)
			, LEFT([StkTallyCode], 50)
			, LEFT([StkSpare1], 50) 
			, LEFT([StkSpare2], 50)
			, CAST([StkUserOnlyDate1] AS [date])
			, CAST([StkUserOnlyDate2] AS [date])
			, LEFT([StkUserOnlyAlpha1], 50)
			, LEFT([StkUserOnlyAlpha2], 50)
			, LEFT([StkUserOnlyAlpha3], 50)
			, LEFT([StkUserOnlyAlpha4], 50)
			, LEFT([StkUserOnlyAlpha5], 50)
			, LEFT([StkUserOnlyAlpha6], 50)
			, CAST([StkUserOnlyNum1] AS [numeric](18, 4))
			, CAST([StkUserOnlyNum2] AS [numeric](18, 4))
			, CAST([StkUserOnlyNum3] AS [numeric](18, 4))
			,CAST(REPLACE(REPLACE([StkUserOnlyNum4], 0x0D, ''), 0x0A, '') AS [numeric](18,4))
		FROM [Pronto].[dbo].[Load_ProntoStockMaster]

		TRUNCATE TABLE [Pronto].[dbo].[Load_ProntoStockMaster];


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);
	
 END;
 


GO
