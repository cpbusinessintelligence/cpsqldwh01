SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_LoadProntoStockSerialLink]
AS
BEGIN

      --'=====================================================================
    --' CP -Stored Procedure -[cppl_LoadProntoStockSerialLink]
    --' ---------------------------
    --' Purpose:Load ProntoStockSerialLink Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 05 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 05/09/2014    AB      1.00    Created the procedure                             --AB20140905

    --'=====================================================================

	--DBCC SHRINKFILE(Pronto_log, 1);

	SET NOCOUNT ON;

    BEGIN TRY
    
		TRUNCATE TABLE [Pronto].[dbo].[ProntoStockSerialLink];

		/****** Object:  Index [PK_ProntoStockSerialLink]    Script Date: 01/21/2013 12:21:33 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProntoStockSerialLink]') AND name = N'PK_ProntoStockSerialLink')
		ALTER TABLE [dbo].[ProntoStockSerialLink] DROP CONSTRAINT [PK_ProntoStockSerialLink];

		INSERT INTO [Pronto].[dbo].[ProntoStockSerialLink] WITH(TABLOCK)
				(
					[SerialNumber]
				   ,[SerialLinkType]
				   ,[SerialLinkCode]
				   ,[SerialLinkSuffix]
				   ,[SerialLinkSeqNo]
				)
		SELECT DISTINCT
				LEFT([SerialNumber], 20)[SerialNumber]
				,LEFT([SerialLinkType], 5)[SerialLinkType]
				,LEFT(RTRIM(LTRIM([SerialLinkCode])), 10)[SerialLinkCode]
				,LEFT([SerialLinkSuffix], 10)[SerialLinkSuffix]
				,CAST(CAST(REPLACE(REPLACE([SerialLinkSeqNo], 0x0D, ''), 0x0A, '') AS FLOAT) AS INT)[SerialLinkSeqNo]
				 FROM [Pronto].[dbo].[Load_ProntoStockSerialLink]
				 WHERE [SerialLinkType] ='S' AND [SerialLinkCode] IS NOT NULL;

		/****** Object:  Index [PK_ProntoStockSerialLink]    Script Date: 01/21/2013 12:21:34 ******/
		ALTER TABLE [dbo].[ProntoStockSerialLink] ADD  CONSTRAINT [PK_ProntoStockSerialLink] PRIMARY KEY CLUSTERED 
		(
			[SerialNumber] ASC,
			[SerialLinkType] ASC,
			[SerialLinkCode] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];

		TRUNCATE TABLE [Pronto].[dbo].[Load_ProntoStockSerialLink];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

END;
 

GO
