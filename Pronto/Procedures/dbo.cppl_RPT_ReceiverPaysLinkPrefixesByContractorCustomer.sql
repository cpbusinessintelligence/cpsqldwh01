SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--DROP TABLE #Serials

CREATE PROC [dbo].[cppl_RPT_ReceiverPaysLinkPrefixesByContractorCustomer]
(
	@StartDate	date,
	@EndDate	date,
	@ContractorFilter	varchar(2000),
	@CustomerCodeFilter	varchar(20),
	@CustomerNameFilter	varchar(100),
	@BranchFilter		varchar(20)
)
AS
BEGIN

	SET NOCOUNT ON;

	CREATE TABLE #Serials (PrimaryPrefix char(3) PRIMARY KEY);
	CREATE TABLE #Output (CouponPrefix char(3),
							Contractor varchar(10),
							ContractorBranch varchar(20),
							CustomerCode varchar(20),
							CustomerName varchar(100),
							SerialCount int,
							CustomerTotalCount int);

	CREATE INDEX idx_TempOutput ON #Output (CustomerCode, Contractor,
												ContractorBranch, CustomerName)
												INCLUDE (SerialCount, CustomerTotalCount);
	
	DECLARE @ContractorFilterTable TABLE (Code varchar(15) PRIMARY KEY);
	DECLARE @HasContractorFilter bit;
	
	SELECT @HasContractorFilter = 0;

	IF LTRIM(RTRIM(ISNULL(@ContractorFilter, ''))) != ''
	BEGIN
		INSERT INTO @ContractorFilterTable 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(ISNULL(s.Data, ''))) FROM
		dbo.Split(@ContractorFilter, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @ContractorFilterTable)
		BEGIN
			SELECT @HasContractorFilter = 1;
		END
		ELSE
		BEGIN
			SELECT @HasContractorFilter = 0;
		END
	END

	INSERT INTO #Serials 
	SELECT * FROM(
	SELECT '201' AS PrimaryPrefix UNION ALL
	SELECT '202' AS PrimaryPrefix UNION ALL
	SELECT '203' AS PrimaryPrefix UNION ALL
	SELECT '204' AS PrimaryPrefix UNION ALL
	SELECT '205' AS PrimaryPrefix UNION ALL
	SELECT '206' AS PrimaryPrefix UNION ALL
	SELECT '207' AS PrimaryPrefix UNION ALL
	SELECT '208' AS PrimaryPrefix UNION ALL
	SELECT '211' AS PrimaryPrefix UNION ALL
	SELECT '215' AS PrimaryPrefix UNION ALL
	SELECT '216' AS PrimaryPrefix UNION ALL
	SELECT '218' AS PrimaryPrefix UNION ALL
	SELECT '301' AS PrimaryPrefix UNION ALL
	SELECT '302' AS PrimaryPrefix UNION ALL
	SELECT '303' AS PrimaryPrefix UNION ALL
	SELECT '304' AS PrimaryPrefix UNION ALL
	SELECT '305' AS PrimaryPrefix UNION ALL
	SELECT '306' AS PrimaryPrefix UNION ALL
	SELECT '307' AS PrimaryPrefix UNION ALL
	SELECT '308' AS PrimaryPrefix UNION ALL
	SELECT '311' AS PrimaryPrefix UNION ALL
	SELECT '315' AS PrimaryPrefix UNION ALL
	SELECT '316' AS PrimaryPrefix UNION ALL
	SELECT '318' AS PrimaryPrefix UNION ALL
	SELECT '340' AS PrimaryPrefix UNION ALL
	SELECT '341' AS PrimaryPrefix UNION ALL
	SELECT '342' AS PrimaryPrefix UNION ALL
	SELECT '401' AS PrimaryPrefix UNION ALL
	SELECT '402' AS PrimaryPrefix UNION ALL
	SELECT '403' AS PrimaryPrefix UNION ALL
	SELECT '404' AS PrimaryPrefix UNION ALL
	SELECT '405' AS PrimaryPrefix UNION ALL
	SELECT '406' AS PrimaryPrefix UNION ALL
	SELECT '407' AS PrimaryPrefix UNION ALL
	SELECT '408' AS PrimaryPrefix UNION ALL
	SELECT '411' AS PrimaryPrefix UNION ALL
	SELECT '415' AS PrimaryPrefix UNION ALL
	SELECT '416' AS PrimaryPrefix UNION ALL
	SELECT '440' AS PrimaryPrefix UNION ALL
	SELECT '501' AS PrimaryPrefix UNION ALL
	SELECT '502' AS PrimaryPrefix UNION ALL
	SELECT '503' AS PrimaryPrefix UNION ALL
	SELECT '504' AS PrimaryPrefix UNION ALL
	SELECT '505' AS PrimaryPrefix UNION ALL
	SELECT '506' AS PrimaryPrefix UNION ALL
	SELECT '507' AS PrimaryPrefix UNION ALL
	SELECT '508' AS PrimaryPrefix UNION ALL
	SELECT '511' AS PrimaryPrefix UNION ALL
	SELECT '515' AS PrimaryPrefix UNION ALL
	SELECT '516' AS PrimaryPrefix UNION ALL
	SELECT '518' AS PrimaryPrefix UNION ALL
	SELECT '540' AS PrimaryPrefix UNION ALL
	SELECT '601' AS PrimaryPrefix UNION ALL
	SELECT '602' AS PrimaryPrefix UNION ALL
	SELECT '603' AS PrimaryPrefix UNION ALL
	SELECT '604' AS PrimaryPrefix UNION ALL
	SELECT '605' AS PrimaryPrefix UNION ALL
	SELECT '606' AS PrimaryPrefix UNION ALL
	SELECT '607' AS PrimaryPrefix UNION ALL
	SELECT '608' AS PrimaryPrefix UNION ALL
	SELECT '611' AS PrimaryPrefix UNION ALL
	SELECT '615' AS PrimaryPrefix UNION ALL
	SELECT '616' AS PrimaryPrefix UNION ALL
	SELECT '618' AS PrimaryPrefix UNION ALL
	SELECT '639' AS PrimaryPrefix UNION ALL
	SELECT '640' AS PrimaryPrefix UNION ALL
	SELECT '692' AS PrimaryPrefix UNION ALL
	SELECT '701' AS PrimaryPrefix UNION ALL
	SELECT '703' AS PrimaryPrefix UNION ALL
	SELECT '705' AS PrimaryPrefix UNION ALL
	SELECT '706' AS PrimaryPrefix UNION ALL
	SELECT '707' AS PrimaryPrefix UNION ALL
	SELECT '708' AS PrimaryPrefix UNION ALL
	SELECT '711' AS PrimaryPrefix UNION ALL
	SELECT '715' AS PrimaryPrefix UNION ALL
	SELECT '903' AS PrimaryPrefix UNION ALL
	SELECT '904' AS PrimaryPrefix UNION ALL
	SELECT '906' AS PrimaryPrefix UNION ALL
	SELECT '907' AS PrimaryPrefix UNION ALL
	SELECT '908' AS PrimaryPrefix UNION ALL
	SELECT '911' AS PrimaryPrefix UNION ALL
	SELECT '293' AS PrimaryPrefix UNION ALL
	SELECT '294' AS PrimaryPrefix UNION ALL
	SELECT '393' AS PrimaryPrefix UNION ALL
	SELECT '394' AS PrimaryPrefix UNION ALL
	SELECT '493' AS PrimaryPrefix UNION ALL
	SELECT '494' AS PrimaryPrefix UNION ALL
	SELECT '593' AS PrimaryPrefix UNION ALL
	SELECT '594' AS PrimaryPrefix UNION ALL
	SELECT '693' AS PrimaryPrefix UNION ALL
	SELECT '694' AS PrimaryPrefix UNION ALL
	SELECT '793' AS PrimaryPrefix UNION ALL
	SELECT '794' AS PrimaryPrefix UNION ALL
	SELECT '993' AS PrimaryPrefix UNION ALL
	SELECT '994' AS PrimaryPrefix UNION ALL
	SELECT '915' AS PrimaryPrefix) L;

	--select * from #Serials;

	INSERT INTO #Output 
	(
		Contractor, ContractorBranch, CouponPrefix, CustomerCode, CustomerName, SerialCount
	)
	SELECT DISTINCT
	--ProntoCouponDetails.DeliveryContractor
	--LTRIM(RTRIM(ISNULL(Warehouse, 'UNKNOWN'))) AS Contractor
	LTRIM(RTRIM(ISNULL(DeliveryContractor, 'UNKNOWN'))) AS [Contractor]
	, CASE LEFT(LTRIM(RTRIM(ISNULL(DeliveryContractor, ''))), 1)
		WHEN 'A' THEN 'Adelaide'
		WHEN 'C' THEN 'Canberra'
		WHEN 'G' THEN 'Gold Coast'
		WHEN 'B' THEN 'Brisbane'
		WHEN 'S' THEN 'Sydney'
		WHEN 'M' THEN 'Melbourne'
		WHEN 'P' THEN 'Perth'
		ELSE 'UNKNOWN'
	END AS [ContractorBranch]
	, LEFT(LTRIM(RTRIM(ISNULL(SerialNumber, ''))), 3) AS Prefix
	, ISNULL(ProntoSalesOrder.CustomerCode, 'UNKNOWN') AS [CustomerCode]
	, ISNULL(ProntoDebtor.Shortname, 'UNKNOWN') AS CustomerName
	--, Cosmos.dbo.Driver.DriverNumber AS CosmosRunId
	,COUNT(SerialNumber) AS SerialCount
	--, ProntoCouponDetails.*
	FROM ProntoCouponDetails (NOLOCK)
	JOIN #Serials s ON LEFT(LTRIM(RTRIM(ISNULL(SerialNumber, ''))), 3) = s.PrimaryPrefix
	LEFT OUTER JOIN ProntoSalesOrder (NOLOCK)
		ON LTRIM(RTRIM(ISNULL(LastSoldReference, ''))) = LTRIM(RTRIM(ISNULL(OrderNumber, '')))
		AND ProntoSalesOrder.CompanyCode = 'CL1'
	LEFT OUTER JOIN ProntoDebtor (NOLOCK)
		ON LTRIM(RTRIM(ISNULL(ProntoSalesOrder.CustomerCode, ''))) = LTRIM(RTRIM(ISNULL(ProntoDebtor.Accountcode, '')))
	--LEFT JOIN Cosmos.dbo.Driver ON ProntoId = ProntoCouponDetails.PickupContractor AND Driver.IsActive = 1
	WHERE 
	--PickupDate Is Not Null
	--AND PickupDate BETWEEN ISNULL(@StartDate, GETDATE()) AND ISNULL(@EndDate, GETDATE())
		DeliveryRctiDate Is Not Null
		AND DeliveryRctiDate BETWEEN ISNULL(@StartDate, GETDATE()) AND ISNULL(@EndDate, GETDATE())
	AND ReturnSerialNumber IS NOT NULL
	AND ((LEFT(LTRIM(RTRIM(ISNULL(DeliveryContractor, ''))), 1) = 
		CASE WHEN ISNULL(@BranchFilter, '') = 'Adelaide' THEN 'A'
		WHEN ISNULL(@BranchFilter, '') = 'Canberra' THEN 'C'
		WHEN ISNULL(@BranchFilter, '') = 'Gold Coast' THEN 'G'
		WHEN ISNULL(@BranchFilter, '') = 'Brisbane' THEN 'B'
		WHEN ISNULL(@BranchFilter, '') = 'Melbourne' THEN 'M'
		WHEN ISNULL(@BranchFilter, '') = 'Sydney' THEN 'S'
		WHEN ISNULL(@BranchFilter, '') = 'Perth' THEN 'P'
		END)
		OR
		(LTRIM(RTRIM(ISNULL(@BranchFilter, ''))) = '<ALL>'))
	AND
	(
		(ProntoDebtor.Accountcode LIKE CONVERT(varchar(50), (@CustomerCodeFilter + '%')))
		OR
		(ISNULL(@CustomerCodeFilter, '') = '')
	)
	AND
	(
		(ProntoDebtor.Shortname LIKE CONVERT(varchar(100), ('%' + @CustomerNameFilter + '%')))
		OR
		(ISNULL(@CustomerNameFilter, '') = '')
	)
	AND
	(
		(ISNULL(DeliveryContractor, '') IN (SELECT Code FROM @ContractorFilterTable))
		OR
		(@HasContractorFilter = 0)
	)
	--and ProntoDebtor.Shortname LIKE '%BURSON%'
	GROUP BY 
	--Warehouse
	DeliveryContractor
	, ProntoSalesOrder.CustomerCode
	, ProntoDebtor.Shortname
	, LEFT(LTRIM(RTRIM(ISNULL(SerialNumber, ''))), 3);

	UPDATE #Output 
	SET CustomerTotalCount = 
	(
		SELECT SUM(SerialCount) 
		FROM #Output WHERE 
		CustomerCode = o.CustomerCode 
	)
	FROM #Output o;

	SELECT * FROM #Output;
	--order by Contractor asc, PickupPrefix asc, CustomerCode asc;
	--ORDER BY CustomerTotalCount desc;

	DROP TABLE #Serials;
	DROP TABLE #Output;

	SET NOCOUNT OFF;

END
GO
GRANT EXECUTE
	ON [dbo].[cppl_RPT_ReceiverPaysLinkPrefixesByContractorCustomer]
	TO [ReportUser]
GO
