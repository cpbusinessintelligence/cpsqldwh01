SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_RptProntoBillingDetailsReport_Sharon]
(
	@AccountCode			varchar(50),
	@StartDate				date,
	@EndDate				date
)
AS
BEGIN

SET NOCOUNT ON;
	
	
SELECT  DATENAME(month, [BillingDate]) as [BillingMonth]
      ,DATEPART(YEAR,[BillingDate]) as [BillingYear]
      ,[BillingDate]
	  ,[AccountingDate]
	  ,[AccountCode]
      ,[AccountName]
      ,[AccountBillToCode]
      ,[AccountBillToName]
      ,[ConsignmentReference]
	  ,[ConsignmentDate]
      ,[ServiceCode]
	  ,[TariffId]
	  ,C.cd_pickup_addr0 as Sender
	  ,[OriginLocality]
      ,[OriginPostcode]
	  ,[RevenueOriginZone]
	  ,[DestinationLocality]
      ,[DestinationPostcode]
	  ,[RevenueDestinationZone]
	  ,[DeclaredVolume]
	  ,[DeclaredWeight]
	  ,[ItemQuantity]
	  ,[ChargeableWeight]
	  ,[BilledFreightCharge]
      ,[BilledFuelSurcharge]
	  ,[BilledInsurance]
      ,[BilledTotal]
	  into #Temp1
  FROM [dbo].[ProntoBilling] T (NOLOCK)
 Left Join cppledi.dbo.consignment C on T.ConsignmentReference = C.cd_connote
 Where [AccountBillToCode]= @AccountCode and [BillingDate] >=@StartDate and [BillingDate] <=@EndDate
 ORDER BY  [BillingDate]

 UPDATE #Temp1
 set Sender= C.cd_pickup_addr0 FROM #Temp1 T JOIN [CpplEDI].[dbo].[consignment_Archive_16-17] C on T.ConsignmentReference = C.cd_connote
 where Sender IS NULL

 UPDATE #Temp1
 set Sender= C.cd_pickup_addr0 FROM #Temp1 T JOIN [CpplEDI].[dbo].[consignment_Archive_15-16] C on T.ConsignmentReference = C.cd_connote
 where Sender IS NULL

select distinct * from #Temp1 where [BillingDate] >=@StartDate and [BillingDate] <=@EndDate ORDER BY [BillingDate]
	
SET NOCOUNT OFF;

END
GO
