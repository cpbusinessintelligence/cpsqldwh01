SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveProntoBilling]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(BillingDate) from [ProntoBilling]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [ProntoBilling_Archive]
	SELECT [OrderNumber]
      ,[OrderSuffix]
      ,[OrderId]
      ,[ConsignmentReference]
      ,[AccountCode]
      ,[AccountName]
      ,[AccountBillToCode]
      ,[AccountBillToName]
      ,[ServiceCode]
      ,[CompanyCode]
      ,[AccountingPeriod]
      ,[AccountingWeek]
      ,[BillingDate]
      ,[AccountingDate]
      ,[InvoiceNumber]
      ,[OriginLocality]
      ,[OriginPostcode]
      ,[ReceiverName]
      ,[DestinationLocality]
      ,[DestinationPostcode]
      ,[ConsignmentDate]
      ,[ManifestReference]
      ,[ManifestDate]
      ,[CustomerReference]
      ,[LogisticsUnitsQuantity]
      ,[ItemQuantity]
      ,[DeclaredWeight]
      ,[DeadWeight]
      ,[DeclaredVolume]
      ,[Volume]
      ,[ChargeableWeight]
      ,[LinehaulWeight]
      ,[InsuranceCategory]
      ,[InsuranceDeclaredValue]
      ,[InsurancePriceOverride]
      ,[TestFlag]
      ,[RevenueBusinessUnit]
      ,[TariffId]
      ,[RevenueOriginZone]
      ,[RevenueDestinationZone]
      ,[CalculatedFreightCharge]
      ,[CalculatedFuelSurcharge]
      ,[CalculatedTransportCharge]
      ,[PriceOverride]
      ,[BilledFreightCharge]
      ,[BilledFuelSurcharge]
      ,[BilledTransportCharge]
      ,[BilledInsurance]
      ,[BilledOtherCharge]
      ,[BilledTotal]
      ,[CardRateFreightCharge]
      ,[CardRateFuelSurcharge]
      ,[CardRateTransportCharge]
      ,[CardRateDiscountOff]
      ,[PickupCost]
      ,[PickupSupplier]
      ,[PickupZone]
      ,[LinehaulCost]
      ,[LinehaulRoute]
      ,[DeliveryCost]
      ,[DeliverySupplier]
      ,[DeliveryZone]
      ,[TotalPUDCost]
      ,[MarginTransportcharge]
  FROM [Pronto].[dbo].[ProntoBilling] (nolock) where BillingDate >= @MinDate and  BillingDate <= DATEADD(day, 1,@MinDate)


	DELETE FROM [Pronto].[dbo].[ProntoBilling] where  BillingDate >= @MinDate and  BillingDate <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
