SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveProntoSalesOrder]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(AccountingDate) from [ProntoSalesOrder]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [ProntoSalesOrder_Archive17-18]
	SELECT [OrderNumber]
      ,[Suffix]
      ,[CompanyCode]
      ,[CustomerCode]
      ,[WarehouseCode]
      ,[TerritoryCode]
      ,[SalesType]
      ,[AccountingDate]
      ,[AccountingPeriod]
      ,[NetAmount]
      ,[GSTAmount]
      ,[GrossAmount]
      ,[CostAmount]
  FROM [Pronto].[dbo].[ProntoSalesOrder] (nolock) where AccountingDate >= @MinDate and  AccountingDate <= DATEADD(day, 1,@MinDate)


	DELETE FROM [Pronto].[dbo].[ProntoSalesOrder] where  AccountingDate >= @MinDate and  AccountingDate <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
