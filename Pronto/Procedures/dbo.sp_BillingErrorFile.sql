SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc dbo.sp_BillingErrorFile
as
--'=====================================================================
    --' CP -Stored Procedure - 
    --' ---------------------------
    --' Purpose: sp_BillingErrorFile-----
    --' Developer: SS (Couriers Please Pty Ltd)
    --' Date: 19 Nov 2018
    --' Copyright: 2018 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
                        

    --'=====================================================================

Begin

Declare @date varchar(200),@sql varchar(1000)

SET @date = replace(convert(varchar(8), getdate(), 112)+convert(varchar(8), getdate(), 114), ':','')

SET @sql='SELECT *
INTO cpsqlops01.ProntoBilling.dbo.ProntoBilling_' + CONVERT(varchar(30),@date,113) +'
FROM cpsqlops01.ProntoBilling.dbo.ProntoBilling Where IsProcessed = 0 and IsDeleted = 0 and HasError =1'
Exec (@Sql)

End

Begin

Update cpsqlops01.ProntoBilling.dbo.ProntoBilling 
SET HasError = 0, ErrorCode = '',EditWho = 'JP', EditDateTime = GETDATE()  
Where IsProcessed = 0 and IsDeleted = 0 and HasError =1

End

GO
