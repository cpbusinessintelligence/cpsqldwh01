SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Proc sp_ConsignmentPerfRep_SS (@AddDatetime DateTime)
as


Begin transaction PerfRep

Insert into [dbo].[Consignment_Archive_17-18]
select * from [dbo].[Consignment]
where [AddDateTime] < = @AddDatetime

Delete from [dbo].[Consignment]
where [AddDateTime] < = @AddDatetime


Commit transaction PerfRep

GO
