SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_LoadContractorRCTIPaymentsbyServicecode_backup20151210]
as
begin

 --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadEDIRCTIPaymentsbyServicecode]
    --' ---------------------------
    --' Purpose: LoadEDIRCTIPaymentsbyServicecode-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 15 Oct 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 15/10/2015    AB      1.00    Created the procedure                             --AB20151015

    --'=====================================================================


	SET DATEFIRST 1;

	DECLARE @StartDate date;
	DECLARE @EndDate date;

	SELECT @StartDate = CAST((CONVERT(datetime, GETDATE()) - (DATEPART(DW, CONVERT(datetime, GETDATE())) - 1)) AS DATE);
		SELECT @StartDate = DATEADD(day, -14, @StartDate);
	
	SELECT @EndDate = DATEADD(day, 6, @StartDate);

	print @StartDate
	print @EndDate



If not exists (Select RCTIWeekEndingDate from [dbo].[CPPL_ContractorRctiPaymentsByServiceCode] where RCTIWeekEndingDate=@EndDate)
begin

INSERT INTO CPPL_ContractorRctiPaymentsByServiceCode
		(
			ContractorId,
			RunNumber,
			RunName,
			Branch,
			RctiWeekEndingDate,
			IsReceiverPays,
			servicecode,
			DeliveryRctiAmount,
			DeliveryStdAmount,
			DeliveryDifference,
			DeliveryCount,
			PickupRctiAmount,
			PickupStdAmount,
			PickupDifference,
			Pickupcount,
			TotalRctiAmount,
			TotalStdAmount,
			TotalDifference 
		)


		SELECT
				ISNULL(cd.DeliveryContractor, 'UNKNOWN') AS [ContractorId],
				0 AS [RunNumber],
				'' AS [RunName],
				'' AS [Branch],
				@EndDate AS [WeekEndingDate],
				convert(bit, case when ltrim(rtrim(ISNULL(ReturnSerialNumber, ''))) = ''
					then 0
					else 1
				end) AS [ReceiverPaysFlag],
				billingservice AS Servicecode,
				--ISNULL(SUM(ISNULL(DeliveryRctiAmount, 0.00)), 0.00),
				sum(ISNULL(DeliveryRctiAmount, 0.00)) AS [DeliveryRctiAmount],
				0.00,
				0.00,
				count(*),
				0.00,
				
				0.00,
				0.00,
				0,
				0.00,
				0.00,
				0.00

				FROM Prontocoupondetails cd
				where cd.DeliveryRctiDate between @StartDate and @EndDate 
			AND cd.DeliveryRctiDate Is Not Null
			AND not(ISNUMERIC(cd.SerialNumber) = 1 AND LEN(LTRIM(rtrim(isnull(SerialNumber, '')))) = 11)
			AND ISNULL(cd.DeliveryRctiAmount, 0.00) <> 0.00
			group by ISNULL(cd.DeliveryContractor, 'UNKNOWN') ,
				convert(bit, case when ltrim(rtrim(ISNULL(ReturnSerialNumber, ''))) = ''
					then 0
					else 1
				end) ,
				billingservice ;

WITH PickupPayments AS
		(
			SELECT
				ISNULL(cd.PickupContractor, 'UNKNOWN') AS [ContractorId],
				0 AS [RunNumber],
				'' AS [RunName],
				'' AS [Branch],
				@EndDate AS [WeekEndingDate],
				convert(bit, case when ltrim(rtrim(ISNULL(ReturnSerialNumber, ''))) = ''
					then 0
					else 1
				end) AS [ReceiverPaysFlag],
				Billingservice as Servicecode,
				sum(ISNULL(PickupRctiAmount, 0.00)) AS [PickupRctiAmount],
				count(*) as Pickupcount
			from ProntoCouponDetails cd (nolock)
			where cd.PickupRctiDate between @StartDate and @EndDate 
			AND cd.PickupRctiDate Is Not Null
			AND not(ISNUMERIC(cd.SerialNumber) = 1 AND LEN(LTRIM(rtrim(isnull(SerialNumber, '')))) = 11)
			AND ISNULL(cd.PickupRctiAmount, 0.00) <> 0.00
			group by ISNULL(cd.PickupContractor, 'UNKNOWN') ,
				convert(bit, case when ltrim(rtrim(ISNULL(ReturnSerialNumber, ''))) = ''
					then 0
					else 1
				end) ,
				billingservice 
		)
		MERGE CPPL_ContractorRctiPaymentsByServiceCode AS Target
		USING 
		(
			SELECT
			ISNULL(ContractorId, 'UNKNOWN') AS [ContractorId], 
			RunNumber, ISNULL(RunName, 'UNKNOWN') AS [RunName],
			Branch, WeekEndingDate, ReceiverPaysFlag,
			Servicecode,
					0.00 AS [DeliveryRctiAmount],
				0.00 AS [DeliveryStdAmount],
				0.00 AS [DeliveryDifference],
			 SUM(isnull(PickupRctiAmount, 0.00)) AS [PickupRctiAmount],
			       0.00 AS [PickupStdAmount],
			 		0.00 AS [PickupDifference],
					0.00 AS [TotalRctiAmount],
					0.00 AS [TotalStdAmount],
				0.00 AS [TotalDifference],
				Pickupcount
			FROM PickupPayments pp 
			GROUP BY ContractorId, WeekEndingDate, ReceiverPaysFlag, Servicecode,
				RunNumber, RunName, Branch,Pickupcount
		)
		AS Source
		ON Target.ContractorId = Source.ContractorId
		AND Target.RctiWeekEndingDate = Source.WeekEndingDate
		AND Target.IsReceiverPays = Source.ReceiverPaysFlag
		AND Target.Servicecode = Source.Servicecode
		
		WHEN
			NOT MATCHED BY TARGET THEN
				INSERT
				(
					ContractorId,
					RunNumber,
					RunName,
					Branch,
					RctiWeekEndingDate,
					IsReceiverPays,
					Servicecode,
					DeliveryRctiAmount,
					DeliveryStdAmount,
					DeliveryDifference,
					DeliveryCount,
					PickupRctiAmount,
					PickupStdAmount,
					PickupDifference,
					PickupCount,
					TotalRctiAmount,
					TotalStdAmount,
					TotalDifference
				)
				VALUES
				(
					Source.ContractorId,
					0,
					'',
					'',
					Source.WeekEndingDate,
					Source.ReceiverPaysFlag,
					Source.Servicecode,
					0.00,
					0.00,
					0.00,
					0,
					Source.PickupRctiAmount,
					0.00,
					0.00,
					Source.PickupCount,
					0.00,
					0.00,
					0.00
		--			Source.PickupStdAmount,
		--			(Source.PickupRctiAmount - Source.PickupStdAmount),
		--			0.00,
		--			0.00,
		--			0.00
				)
		WHEN
			MATCHED THEN
				UPDATE
					SET Target.PickupRctiAmount = Source.PickupRctiAmount,
					 Target.Pickupcount = Source.Pickupcount;


UPDATE CPPL_ContractorRctiPaymentsByServiceCode
		SET TotalRctiAmount=PickupRctiAmount+DeliveryRctiAmount;


	WITH ContractorDriver AS
		(
			select 
			cp.ContractorId,
			cp.servicecode,
			cp.IsReceiverPays,
			cp.RctiWeekEndingDate,
			(
				select TOP 1 d.Id
				FROM Cosmos.dbo.Driver d (NOLOCK)
				WHERE
					d.ProntoId = cp.ContractorId 
					AND d.EffectiveDate < cp.RctiWeekEndingDate 
					AND
					((ISNULL(d.DriverNumber, 9999) < 8000)
						OR (ISNULL(d.DriverNumber, 9999) > 8999))
				ORDER BY d.EffectiveDate DESC
			) AS [DriverId]
			 from CPPL_ContractorRctiPaymentsByServiceCode cp (nolock)
			 WHERE ISNULL(cp.RunNumber, 0) = 0
		)
		UPDATE CPPL_ContractorRctiPaymentsByServiceCode 
		SET 
			Branch = UPPER(d2.Branch),
			RunNumber = d2.DriverNumber,
			RunName = UPPER(d2.DriverName)
			--SELECT
			--	cp2.ContractorId,
			--	d2.Branch,
			--	d2.DriverNumber,
			--	d2.DriverName 
		FROM 
		CPPL_ContractorRctiPaymentsByServiceCode cp2
		JOIN ContractorDriver cd
		ON cp2.ContractorId = cd.ContractorId 
		AND cp2.servicecode = cd.servicecode 
		AND cp2.IsReceiverPays = cd.IsReceiverPays 
		AND cp2.RctiWeekEndingDate = cd.RctiWeekEndingDate 
		JOIN Cosmos.dbo.Driver d2 (NOLOCK)
		ON d2.Id = cd.DriverId 
		WHERE ISNULL(cp2.RunNumber, 0) = 0;
		--AND pt2.RctiWeekEndingDate = @EndDate;


		UPDATE CPPL_ContractorRctiPaymentsByServiceCode
		SET Branch = CASE LEFT(ISNULL(ContractorId, ' '), 1)
			WHEN 'B' THEN 'BRISBANE'
			WHEN 'C' THEN 'SYDNEY'
			WHEN 'A' THEN 'ADELAIDE'
			WHEN 'M' THEN 'MELBOURNE'
			WHEN 'G' THEN 'GOLDCOAST'
			WHEN 'O' THEN 'GOLDCOAST'
			WHEN 'S' THEN 'SYDNEY'
			ELSE ''
		END
		WHERE LTRIM(RTRIM(ISNULL(Branch, ''))) = ''
		OR (LTRIM(RTRIM(ISNULL(Branch, ''))) = 'brisbane' AND LEFT(ISNULL(ContractorId, ' '), 1) = 'G')
		OR (LTRIM(RTRIM(ISNULL(Branch, ''))) = 'goldcoast' AND LEFT(ISNULL(ContractorId, ' '), 1) = 'B');

		
end

end

GO
