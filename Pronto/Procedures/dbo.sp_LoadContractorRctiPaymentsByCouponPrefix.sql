SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_LoadContractorRctiPaymentsByCouponPrefix]
as 
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadContractorRctiPaymentsByCouponPrefix]
    --' ---------------------------
    --' Purpose: LoadContractorRctiPaymentsByCouponPrefix-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 22 Oct 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 22/10/2015    AB      1.00    Created the procedure                             --AB20151022

    --'=====================================================================

SET DATEFIRST 1;

	DECLARE @StartDate date;
	DECLARE @EndDate date;

		--select @StartDate='2016-06-06'

	SELECT @StartDate = CAST((CONVERT(datetime, GETDATE()) - (DATEPART(DW, CONVERT(datetime, GETDATE())) - 1)) AS DATE);
		SELECT @StartDate = DATEADD(day, -14, @StartDate);
	
	SELECT @EndDate = DATEADD(day, 6, @StartDate);

	print @StartDate
	print @EndDate




If not exists (Select RCTIWeekEndingDate from [dbo].[CPPL_ContractorRctiPaymentsByCouponPrefix] where RCTIWeekEndingDate=@EndDate)


Insert into [CPPL_ContractorRctiPaymentsByCouponPrefix]([ContractorId]
      ,[RunNumber]
      ,[RunName]
      ,[Branch]
      ,[RctiWeekEndingDate]
      ,[IsReceiverPays]
      ,[CouponPrefix]
      ,[DeliveryRctiAmount]
      ,[DeliveryStdAmount]
      ,[DeliveryDifference]
      ,[PickupRctiAmount]
      ,[PickupStdAmount]
      ,[PickupDifference]
      ,[TotalRctiAmount]
      ,[TotalStdAmount]
      ,[TotalDifference]
      ,[Depot])

Select [ContractorId]
      ,[RunNumber]
      ,[RunName]
      ,[Branch]
      ,[RctiWeekEndingDate]
      ,[IsReceiverPays]
      ,[CouponPrefix]
      ,[DeliveryRctiAmount]
      ,[DeliveryStdAmount]
      ,[DeliveryDifference]
      ,[PickupRctiAmount]
      ,[PickupStdAmount]
      ,[PickupDifference]
      ,[TotalRctiAmount]
      ,[TotalStdAmount]
      ,[TotalDifference]
      ,[Depot] from  [cp-sql01].RDS.[dbo].[CPPL_ContractorRctiPaymentsByCouponPrefix] where RCTIWeekEndingdate=@EndDate


end


GO
