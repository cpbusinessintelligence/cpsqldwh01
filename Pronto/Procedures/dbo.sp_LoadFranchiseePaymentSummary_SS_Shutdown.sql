SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_LoadFranchiseePaymentSummary_SS_Shutdown]
as 
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadFranchiseePaymentSummary]
    --' ---------------------------
    --' Purpose: LoadFranchiseePaymentSummary-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 22 Oct 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 22/10/2015    AB      1.00    Created the procedure                             --AB20151022

    --'=====================================================================

SET DATEFIRST 1;

	DECLARE @StartDate date;
	DECLARE @EndDate date;


	

	SELECT @StartDate = CAST((CONVERT(datetime, GETDATE()) - (DATEPART(DW, CONVERT(datetime, GETDATE())) - 1)) AS DATE);
		SELECT @StartDate = DATEADD(day, -14, @StartDate);
	
	SELECT @EndDate = DATEADD(day, 6, @StartDate);

	print @StartDate
	print @EndDate




If not exists (Select RCTIWeekEndingDate from [dbo].[FranchiseePaymentSummary] where RCTIWeekEndingDate=@EndDate)


Insert into [FranchiseePaymentSummary]([ContractorId]
      ,[RunNumber]
      ,[RunName]
      ,[Branch]
      ,[RctiWeekEndingDate]
      ,[TotalDevelopmentIncentive]
      ,[TotalPrepaidRctiAmount]
      ,[TotalEdiRctiAmount]
      ,[TotalRctiAllowances]
      ,[TotalRctiDeductions]
      ,[TotalRctiRedemptions]
      ,[Depot])

Select [ContractorId]
      ,[RunNumber]
      ,[RunName]
      ,[Branch]
      ,[RctiWeekEndingDate]
      ,[TotalDevelopmentIncentive]
      ,[TotalPrepaidRctiAmount]
      ,[TotalEdiRctiAmount]
      ,[TotalRctiAllowances]
      ,[TotalRctiDeductions]
      ,[TotalRctiRedemptions]
      ,[Depot] from  [cp-sql01].RDS.[dbo].[CPPL_ContractorRctiPaymentsTotal] where RCTIWeekEndingdate=@EndDate


end



GO
