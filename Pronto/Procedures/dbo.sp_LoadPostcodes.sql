SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[sp_LoadPostcodes] as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[[sp_LoadPostcodes]]
    --' ---------------------------
    --' Purpose: Load [sp_LoadPostcodes]-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE  [Pronto].[dbo].[Postcodes]
	
		INSERT INTO  [Pronto].[dbo].[Postcodes]
			([SysActionCode]
      ,[SysCompSource]
      ,[SysCompCode]
      ,[postcode]
      ,[postcode_desc]
      ,[postcode_state]
      ,[postcode_country]
      ,[postcode_carrier_code]
      ,[postcode_carrier_rate]
      ,[postcode_carrier_charge]
			)


SELECT  [bi_sys_action_code]
      ,[bi_sys_cons_code]
      ,[bi_sys_comp_code]
      ,[postcode]
      ,[postcode_desc]
      ,[postcode_state]
      ,[postcode_country]
      ,[postcode_carrier_code]
      ,[postcode_carrier_rate]
      ,[postcode_carrier_charge]
  FROM [Pronto].[dbo].[Load_Postcodes]


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
