SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadProntoCreditorV1] as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[[[sp_LoadProntoCreditorV1]]]
    --' ---------------------------
    --' Purpose: Load ProntoCreditorV1-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [dbo].[ProntoCreditorV1]
	
		INSERT INTO [dbo].[ProntoCreditorV1](
       [SysCompcode]
	  ,[AccountCode]
      ,[PayToCode]
      ,[Shortname]
      ,[Type]
      ,[AccountStatus]
      ,[SettlementDiscCode]
      ,[VolumeDiscCode]
      ,[DiscRate]
      ,[CurrCode]
      ,[UserOnlyAlpha]
      ,[UserOnlyNum1]
      ,[UserOnlyNum2]
      ,[ABN]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Address4]
      ,[Address5]
      ,[Address6]
      ,[Postcode]
      ,[Phone]
      ,[Fax]
      ,[Mobile]
      ,[CreditLimit]
      ,[Email])


		SELECT    [bi_sys_comp_code]
		         ,[bi_cre_accountcode]
                 ,[bi_cr_pay_to_code]
                 ,[bi_cr_shortname]
                 ,[bi_cr_type]
                 ,[bi_cr_account_status]
                 ,[bi_cr_settlement_disc_code]
                 ,[bi_cr_volume_disc_code]
                 ,[bi_cr_disc_rate]
                 ,[bi_cr_curr_code]
                 ,upper([bi_cr_user_only_alpha4_1])
				 ,0
				 ,0
				 ,[NameAddressCompanyId]
				 ,[NameAddressCompany]
				 ,[NameAddressStreet]
				 ,[NameAddressSuburb]
				 ,[NameAddressCountry]
				 ,[NameAddressAddress_6]
				 ,[NameAddressAddress_7]
				 ,[NameAddressPostCode]
				 ,[NameAddressPhone]
				 ,[NameAddressFaxNo]
				 ,[NameAddressMobilePhone]
				 ,d.Creditlimitamount
				 ,''
FROM [Pronto].[dbo].[Load_ProntoCreditorV1] l join [Pronto].[dbo].[ProntoNameAddressMaster] p on accountcode=[bi_cre_accountcode] and [NameAddressType]='C'
                                            join Pronto.dbo.ProntoDebtorV1 d on l.[bi_cre_accountcode]=d.accountcode

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
