SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_LoadProntoDebtorAgeingV1] as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadProntoGlChartOfAccountsV1]
    --' ---------------------------
    --' Purpose: Load ProntoGLChartofAccounts-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [dbo].[ProntoDebtorAgeingV1]
	
		INSERT INTO [dbo].[ProntoDebtorAgeingV1]
			([SysChangeType]
      ,[SystemCompSource]
      ,[SystemCompCode]
      ,[DfAcountCode]
      ,[DfYear]
      ,[DfPeriod]
      ,[DfReportDate]
      ,[DfAgedBalance1]
      ,[DfAgedBalance2]
      ,[DfAgedBalance3]
      ,[DfAgedBalance4]
      ,[DfAgedBalance5]
      ,[DfAgedBalance6]
      ,[DfAgedBalance7]
      ,[DfAgedBalance8]
      ,[DfAgedBalance9]
      ,[DfAgedBalance10]
      ,[DfAgedBalance11]
      ,[DfAgedBalance12]
      ,[DfAgedBalance13]
      ,[DfFutureBalance]
      ,[DfDrAgedFutureBalance]
			)


		SELECT   [SysChangeType]
      ,[SystemCompSource]
      ,[SystemCompCode]
      ,[DfAcountCode]
      ,[DfYear]
      ,[DfPeriod]
      ,[DfReportDate]
      ,[DfAgedBalance1]
      ,[DfAgedBalance2]
      ,[DfAgedBalance3]
      ,[DfAgedBalance4]
      ,[DfAgedBalance5]
      ,[DfAgedBalance6]
      ,[DfAgedBalance7]
      ,[DfAgedBalance8]
      ,[DfAgedBalance9]
      ,[DfAgedBalance10]
      ,[DfAgedBalance11]
      ,[DfAgedBalance12]
      ,[DfAgedBalance13]
      ,[DfFutureBalance]
      ,[DfDrAgedFutureBalance]
  FROM [Pronto].[dbo].[Load_ProntoDebtorAgeingV1]


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
