SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_LoadProntoDebtorV1] as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[[[[sp_LoadProntoDebtorV1]]]]
    --' ---------------------------
    --' Purpose: Load ProntoDebtorV1-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [dbo].[ProntoDebtorV1]
	
		--INSERT INTO [dbo].[ProntoDebtorV1]([SysCompCode]
  --    ,[Accountcode]
  --    ,[BillTo]
  --    ,[Shortname]
  --    ,[Warehouse]
  --    ,[TermDisc]
  --    ,[Territory]
  --    ,[RepCode]
  --    ,[CreditLimit]
  --    ,[CreditLimitAmount]
  --    ,[IndustryCode]
  --    ,[CustType]
  --    ,[MarketingFlag]
  --    ,[AverageDaysToPay]
  --    ,[IndustrySubGroup]
  --    ,[Postcode]
  --    ,[Locality]
  --    ,[OriginalRepCode]
  --    ,[LastSale]
  --    ,[ABN]
  --    ,[RepName]
  --    ,[CrmAccount]
  --    ,[CrmParent]
  --    ,[CrmType]
  --    ,[CrmRegion]
  --    ,[OriginalRepName])


--		SELECT    [bi_sys_comp_code]
--		         ,[bi_dd_accountcode]
--                 ,[bi_dd_bill_to]
--                 ,[bi_dd_shortname]
--                 ,[bi_dd_warehouse]
--				 ,[bi_dd_terms_disc]
--				 ,[bi_dd_territory]
--				 ,[bi_dd_rep_code]
--		--		 ,
--			--	 ,
--				 ,[bi_dd_dr_industry_code]
--				 ,[bi_dd_dr_cust_type]
--				 ,[bi_dd_dr_marketing_flag]
--				 ,[bi_dd_dr_average_days_to_pay]
--				 ,[bi_dd_dr_industry_sub_group]
--				 ,[NameAddressPostCode]
--				 ,[NameAddressSuburb]
--			--	 ,
--				 ,[bi_dd_last_sale]
				

--                 ,[bi_cr_account_status]
--                 ,[bi_cr_settlement_disc_code]
--                 ,[bi_cr_volume_disc_code]
--                 ,[bi_cr_disc_rate]
--                 ,[bi_cr_curr_code]
--                 ,upper([bi_cr_user_only_alpha4_1])
--				 ,0
--				 ,0
--				 ,[NameAddressCompanyId]
--				 ,[NameAddressCompany]
--				 ,[NameAddressStreet]
--				 ,[NameAddressSuburb]
--				 ,[NameAddressCountry]
--				 ,[NameAddressAddress_6]
--				 ,[NameAddressAddress_7]
--				 ,[NameAddressPostCode]
--				 ,[NameAddressPhone]
--				 ,[NameAddressFaxNo]
--				 ,[NameAddressMobilePhone]
--				 ,d.Creditlimitamount
--				 ,''
--FROM [Pronto].[dbo].[Load_ProntoDebtorv1] l join [Pronto].[dbo].[ProntoNameAddressMaster] p on accountcode=[bi_dd_accountcode] and [NameAddressType] in ('DA','C')
--                                            join Pronto.dbo.ProntoDebtorV1 d on l.[bi_cre_accountcode]=d.accountcode

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
