SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_LoadProntoDebtorbyDate] AS

BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadProntoDebtorbyDate]
    --' ---------------------------
    --' Purpose: sp_LoadProntoDebtorbyDate-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 08 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 21/12/2015    AB      1.00                                                     --AB20151221

    --'=====================================================================
	
	Insert into [ProntoDebtorbyDate]
	Select [Accountcode]
      ,[BillTo]
      ,[Shortname]
      ,[Warehouse]
      ,[TermDisc]
      ,[Territory]
      ,[RepCode]
      ,[IndustryCode]
      ,[CustType]
      ,[MarketingFlag]
      ,[AverageDaysToPay]
      ,[IndustrySubGroup]
      ,[Postcode]
      ,[Locality]
      ,[OriginalRepCode]
      ,[LastSale]
      ,[ABN]
      ,[RepName]
      ,[CrmAccount]
      ,[CrmParent]
      ,[CrmType]
      ,[CrmRegion]
      ,[OriginalRepName]
	  ,getdate()
	  from ProntoDebtor(NOLOCK)

END



GO
