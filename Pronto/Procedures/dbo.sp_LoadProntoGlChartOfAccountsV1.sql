SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure sp_LoadProntoGlChartOfAccountsV1 as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadProntoGlChartOfAccountsV1]
    --' ---------------------------
    --' Purpose: Load ProntoGLChartofAccounts-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [dbo].[ProntoGlChartOfAccountsV1]
	
		INSERT INTO [dbo].[ProntoGlChartOfAccountsV1]
			([SysChangeType]
      ,[SysCompSource]
      ,[SysCompCode]
      ,[ComponentType]
      ,[AccountType]
      ,[AccountCode]
      ,[AccountName]
      ,[AccountType1]
      ,[TaxEntityCode]
			)


		SELECT [bi_sys_action_code]
      ,[bi_sys_comp_cons_code]
      ,[bi_sys_comp_code]
      ,[bi_glc_gl_chart_function_seq]
      ,[bi_glc_gl_chart_function]
      ,[bi_glc_gl_accountcode]
      ,[bi_glc_gl_account_desc]
      ,[bi_glc_gl_account_type]
      ,[bi_glc_glcoa_tax_entity_code]
  FROM [Pronto].[dbo].[Load_ProntoGlChartOfAccountsV1]


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
