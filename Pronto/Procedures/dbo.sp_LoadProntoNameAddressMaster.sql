SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure sp_LoadProntoNameAddressMaster as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadProntoNameAddressMaster]
    --' ---------------------------
    --' Purpose: Load ProntoNameAddressMaster Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [dbo].[ProntoNameAddressMaster]
	
		INSERT INTO [dbo].[ProntoNameAddressMaster] 
			([SysChangeType]
      ,[SysCompSource]
      ,[SysCompCode]
      ,[AccountCode]
      ,[NameAddressType]
      ,[NameAddressName]
      ,[NameAddressCompany]
      ,[NameAddressStreet]
      ,[NameAddressSuburb]
      ,[NameAddressCountry]
      ,[NameAddressAddress_6]
      ,[NameAddressAddress_7]
      ,[NameAddressPostCode]
      ,[NameAddressCountryCode]
      ,[NameAddressPhone]
      ,[NameAddressMobilePhone]
      ,[NameAddressFaxNo]
      ,[NameAddressCompanyId]
      ,[NameAddressRouteCode]
      ,[NameAddressAddressId]
      ,[NameAddressAusbarCode]
      ,[NameAddressUserOnlyDate1]
      ,[NameAddressUserOnlyDate2]
      ,[NameAddressUserOnlyAlpha30_1]
      ,[NameAddressUserOnlyAlpha30_2]
      ,[NameAddressUserOnlyAlpha4_1]
      ,[NameAddressUserOnlyAlpha4_2]
      ,[NameAddressUserOnlyAum1]
      ,[NameAddressUserOnlyNum2]
      ,[NameAddressTaxProvinceCode]
      ,[NameAddressProvExemption]
      ,[NameAddressFedExemption]
      ,[NameAddressSpareAlpha20_1]
      ,[NameAddressSpareAlpha20_2]
      ,[NameAddressSpareDateTime]
      ,[NameAddressSpareAlpha4_1]
      ,[NameAddressSpareAlpha4_2]
      ,[NameAddressMapId]
			)


		SELECT [bi_sys_change_type]
      ,[bi_sys_comp_source]
      ,[bi_sys_comp_code]
      ,replace([accountcode],'"','') 
      ,[na_type]
      ,[na_name]
      ,[na_company]
      ,[na_street]
      ,[na_suburb]
      ,[na_country]
      ,[na_address_6]
      ,[na_address_7]
      ,[postcode]
      ,[na_country_code]
      ,[na_phone]
      ,[na_phone_2]
      ,[na_fax_no]
      ,[na_company_id]
      ,[na_route_code]
      ,[na_address_id]
      ,[na_ausbar_code]
      ,convert(datetime,[user_only_date1])
      ,convert(datetime,[user_only_date2])
      ,[only_alpha30_1]
      ,[only_alpha30_2]
      ,[only_alpha4_1]
      ,[only_alpha4_2]
      ,[user_only_num1]
      ,[user_only_num2]
      ,[province_code]
      ,[prov_exemption]
      ,[na_fed_exemption]
      ,[spare_alpha20_1]
      ,[spare_alpha20_2]
      ,convert(datetime,[spare_date_time])
      ,[spare_alpha4_1]
      ,[spare_alpha4_2]
      ,[na_map_id]
			FROM [dbo].[Load_ProntoNameAddressMaster];

	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
