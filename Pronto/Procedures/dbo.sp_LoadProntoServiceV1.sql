SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_LoadProntoServiceV1] as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[[sp_LoadProntoServiceV1]]
    --' ---------------------------
    --' Purpose: Load ProntoServiceV1-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [dbo].[ProntoServiceV1]
	
		INSERT INTO [dbo].[ProntoServiceV1]
			([CompanyCode]
      ,[ServiceCode]
      ,[ServiceName]
      ,[ServiceGroup]
			)


		SELECT   [SysCompCode],
		         [SystblCode],
                 [SysDescription],
                 [SystblAlpha2]
FROM [Pronto].[dbo].[ProntoSystemTable] where [SystblType]='@S'


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
