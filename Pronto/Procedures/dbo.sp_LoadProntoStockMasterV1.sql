SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_LoadProntoStockMasterV1] as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadProntoStockMasterV1]
    --' ---------------------------
    --' Purpose: Load ProntoProntoStockMasterV1-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE [Pronto].[dbo].[ProntoStockMasterV1]
	
		INSERT INTO [Pronto].[dbo].[ProntoStockMasterV1]
			( [SysActionCode]
      ,[SysCompSource]
      ,[SysCompCode]
      ,[StockCode]
      ,[StockGroup]
      ,[StkAbcClass]
      ,[StkStockStatus]
      ,[StkIssueControlCode]
      ,[StkConditionCode]
      ,[StkIndicator]
      ,[StkCyclicCode]
      ,[StkUserGroup1]
      ,[StkUserGroup2]
      ,[StkDescription]
      ,[StkDescLine2]
      ,[StkDescLine3]
      ,[StkUnitDesc]
      ,[StkPackWeight]
      ,[StkPackDesc]
      ,[StkPackQty]
      ,[StkPackCubicSize]
      ,[StkConversionFactor]
      ,[StkAltUnitDesc]
      ,[StkApnNumber]
      ,[StkImportTariffCode]
      ,[StkSerializedFlag]
      ,[StkStorageTypeFlag]
      ,[StkStdCost]
      ,[StkReplacementCost]
      ,[StkSalesCost]
      ,[StkDutyPaidCost]
      ,[StkInfoCost]
      ,[StkShelfLifeDays]
      ,[StkWarrantyTypeFlag]
      ,[StkDateLastChange]
      ,[StkPricePer]
      ,[StkReorderPolicy]
      ,[StkReorderReview]
      ,[StkReorderBuyer]
      ,[StkCreationDate]
      ,[StkMovementCode]
      ,[StkSalesTypeCode]
      ,[StkSalesTaxPaidRate]
      ,[StkSortAnalysisCode]
      ,[StkExiseQty]
      ,[StkUnSpscCode]
      ,[StkAnalysisCode1]
      ,[StkAnalysisCode2]
      ,[StkAnalysisCode3]
      ,[StkSpareAnalysisCode]
      ,[StkTallyCode]
      ,[StkSpare1]
      ,[StkSpare2]
      ,[StkUserOnlyDate1]
      ,[StkUserOnlyDate2]
      ,[StkUserOnlyAlpha1]
      ,[StkUserOnlyAlpha2]
      ,[StkUserOnlyAlpha3]
      ,[StkUserOnlyAlpha4]
      ,[StkUserOnlyAlpha5]
      ,[StkUserOnlyAlpha6]
      ,[StkUserOnlyNum1]
      ,[StkUserOnlyNum2]
      ,[StkUserOnlyNum3]
      ,[StkUserOnlyNum4]
			)

     SELECT  [bi_sys_action_code]
            ,[bi_sys_cons_code]
            ,[bi_sys_comp_code]
			,LEFT([stock_code], 20) 
			, LEFT([stock_group], 5)
			, LEFT([stk_abc_class], 50)
			, LEFT([stk_stock_status], 1)
			, LEFT([control_code], 50)
			, LEFT([condition_code], 1)
			, LEFT([stk_security], 50)
			, LEFT([stk_cyclic_code], 50)
			, LEFT([stk_user_group_1], 50)
			, LEFT([stk_user_group_2], 50)
			, LEFT([stk_description], 50)
			, LEFT([stk_desc_line_2], 50)
			, LEFT([stk_desc_line_3], 50)
			, LEFT([stk_unit_desc], 50)
			, CAST([stk_pack_weight] AS [numeric](18, 4))
			, LEFT([stk_pack_desc], 50)
			, CAST([stk_pack_qty] AS [numeric](18, 4))
			, CAST([pack_cubic_size] AS [numeric](18, 4))
			, CAST([factor] AS [numeric](18, 4))
			, LEFT([alt_unit_desc], 50)
			, LEFT([stk_apn_number], 50)
			, LEFT([tariff_code], 50)
			, CAST((CASE [serialized_flag] WHEN 'Y' THEN 1 ELSE 0 END) AS [bit])
			, CAST((CASE [type_flag] WHEN 'Y' THEN 1 ELSE 0 END) AS [bit])
			, CAST([stk_std_cost] AS [money])
			, CAST([replacement_cost] AS [money])
			, CAST([stk_sales_cost] AS [money])
			, CAST([duty_paid_cost] AS [money])
			, CAST([stk_info_cost] AS [money])
			, CAST([shelf_life_days] AS [int])
			, CAST([warranty_type] AS [bit])
			, CAST([date_last_change] AS [date])
			, CAST([stk_price_per] AS [money])
			, LEFT([reorder_policy], 50)
			, LEFT([reorder_review], 50)
			, LEFT([reorder_buyer], 50)
			, CAST([creation_date] AS [date])
			, LEFT([movement_code], 50)
			, LEFT([sales_type_code], 50)
			, CAST([tax_paid_rate] AS [money])
			, LEFT([analysis_code], 50)
			, CAST([stk_exise_qty] AS [numeric](18, 4))
			, LEFT([stk_un_spsc_code], 50)
			, LEFT([analysis_code_1], 50)
			, LEFT([analysis_code_2], 50)
			, LEFT([analysis_code_3], 50)
			, LEFT([stk_analysis_cod], 50)
			, LEFT([stk_tally_code], 50)
			, LEFT([stk_spare_1], 50) 
			, LEFT([stk_spare_2], 50)
			, CAST([user_only_date1] AS [date])
			, CAST([user_only_date2] AS [date])
			, LEFT([only_alpha20_1], 50)
			, LEFT([only_alpha20_2], 50)
			, LEFT([only_alpha4_1], 50)
			, LEFT([only_alpha4_2], 50)
			, LEFT([only_alpha4_3], 50)
			, LEFT([only_alpha4_4], 50)
			, CAST([user_only_num1] AS [numeric](18, 4))
			, CAST([user_only_num2] AS [numeric](18, 4))
			, CAST([user_only_num3] AS [numeric](18, 4))
			,CAST(REPLACE(REPLACE([user_only_num4], 0x0D, ''), 0x0A, '') AS [numeric](18,4))
		FROM [Pronto].[dbo].[Load_ProntoStockMasterV1]

     



	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
