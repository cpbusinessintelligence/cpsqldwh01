SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure sp_LoadProntoSystemTable as
begin
  

	  --'=====================================================================
    --' CP -Stored Procedure -[sp_LoadProntoSystemTable]
    --' ---------------------------
    --' Purpose: Load sp_LoadProntoSystemTable-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 10 Jan 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 10/01/2017    AB      1.00                                                     --AB20170110

    --'=====================================================================



	SET NOCOUNT ON;

	DBCC SHRINKFILE(Pronto_log, 1);

    BEGIN TRY
		
		TRUNCATE TABLE  [Pronto].[dbo].[ProntoSystemTable]
	
		INSERT INTO  [Pronto].[dbo].[ProntoSystemTable]
			([SysActionCode]
      ,[SysCompSource]
      ,[SysCompCode]
      ,[SystblType]
      ,[SystblCode]
      ,[Systblsalesmtd]
      ,[Systblsalesytd]
      ,[Systblsalesly]
      ,[SysDescription]
      ,[SysMoneyValue]
      ,[SysDateLastChange]
      ,[SystblAlpha1]
      ,[SystblAlpha2]
      ,[SystblAlpha3]
      ,[SystblAlpha41]
      ,[SystblAlpha42]
      ,[SystblAlpha43]
      ,[SystblAlpha44]
			)


		SELECT  [bi_sys_action_code]
      ,[bi_sys_comp_cons_code]
      ,[bi_sys_comp_code]
      ,[bi_sys_tbl_type]
      ,[bi_sys_tbl_code]
      ,[bi_sys_tbl_sales_mtd]
      ,[bi_sys_tbl_sales_ytd]
      ,[bi_sys_tbl_sales_ly]
      ,[bi_sys_description]
      ,[bi_sys_money_value]
      ,[bi_sys_date_last_change]
      ,[bi_sys_tbl_alpha_1]
      ,[bi_sys_tbl_alpha_2]
      ,[bi_sys_tbl_alpha_3]
      ,[bi_sys_tbl_alpha_41]
      ,[bi_sys_tbl_alpha_42]
      ,[bi_sys_tbl_alpha_43]
      ,[bi_sys_tbl_alpha_44]
  FROM [Pronto].[dbo].[Load_ProntoSystemTable]


	END TRY
	BEGIN CATCH

		/* 
		==================================================================================
		FAILED! -- LOG ERROR 
		==================================================================================
		*/
		EXEC cppl_LogRethrowError;
		
	END CATCH

	DBCC SHRINKFILE(Pronto_log, 1);

	end
GO
