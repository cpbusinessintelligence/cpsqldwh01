SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_NowGoRCTIRecon_SSIS]
(
@StartDate Datetime,
@EndDate Datetime,
@Branch Varchar(100)
)
as
Begin

SELECT @StartDate = DATEADD(wk, -2, DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 7))

SELECT @EndDate = DATEADD(wk, -2, DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 13))

select @Branch = null

select * into #Temp6 from scannergateway..[tbl_TrackingEventNowGo]
where CreatedDateTime >= @StartDate and CreatedDateTime <=  @EndDate and (@Branch is null or Branch =@Branch)
and LabelNumber not  like '125%'



select [DriverExtRef],Labelnumber,cd_connote As Connote,convert(varchar(100),'') as PickupDriver,convert(money,'')  as PickupRCTI,convert(varchar(100),'') as DeliveryDriver,cd_account as AccountCode ,cd_pricecode as ServiceCd ,convert(Money,'') as DeliveryRCTI,WorkflowType,EventDateTime,
DriverRunNumber,Branch
into #Temp1
from #Temp6 jp
left join cppledi..cdcoupon cd
On(jp.LabelNumber =cd.cc_coupon)
left join cppledi..consignment co
On(cd.cc_consignment =co.cd_id)
where WorkflowType in ('pickup','deliver')


update #temp1
set pickupdriver = pickupContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber) 
where WorkflowType = 'pickup'


update #temp1
set DeliveryDriver = DeliveryContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber) where WorkflowType = 'deliver'



update #temp1
set PickupRCTI = PickupRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber)
where WorkflowType in ('pickup')


update #temp1
set DeliveryRCTI = deliveryRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber)
where WorkflowType in ('deliver')


--Label


update #temp1
set pickupdriver = pickupContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null and  WorkflowType = 'pickup'



update #temp1
set DeliveryDriver = DeliveryContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null and WorkflowType = 'deliver'


update #temp1
set PickupRCTI = PickupRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null
and  WorkflowType in ('pickup')


update #temp1
set DeliveryRCTI = deliveryRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null
and  WorkflowType in ('deliver')

-----

Update #temp1 SET COnnote  =  Labelnumber where isnumeric(Labelnumber) = 1 and len(LabelNumber) =11 and ConNote is null


Select distinct Labelnumber,connote,
case 
 when [workflowtype] = 'pickup' then pickupDriver
when [workflowtype] = 'deliver' then Deliverydriver end Driver,

case 
 when [workflowtype] = 'pickup' then PickupRCTI
when [workflowtype] = 'deliver' then DeliveryRCTI end RCTIAmount,
[WorkflowType],
--EventDateTime,
DriverRunNumber,Branch,AccountCode,ServiceCd into #Temp2
 From #Temp1
where workflowtype in ('pickup' , 'deliver')
--and driver in ()
--order by eventdatetime 

delete from #Temp2 where labelnumber like '_38%'

--select * from #Temp2 where Connote is null
--#1 output



select distinct connote,driver,RCTIAmount,Case when WorkflowType = 'deliver' then 'D' 
when WorkflowType = 'pickup' then 'P' end as WorkflowType,
AccountCode,ServiceCd,max(DriverRunNumber) Runnumber,Branch,left(connote,3) as Prefix,
case when isnumeric(connote) = 1 and len(connote) =11 then 'PPD' else 'EDI' end as 'EDI\PPD'
 into #TempConn 
from #temp2 where connote is not null
group by connote,driver,RCTIAmount,WorkflowType,AccountCode,ServiceCd,Branch,AccountCode
--13967


delete from #TempConn
where WorkflowType = 'd' and
(Connote like '_28%' or Connote like '_29%'or Connote like '_30%'
or Connote like '_31%' or Connote like '_32%'or Connote like '_66%'
or Connote like '_67%' or Connote like '_80%'or Connote like '_81%')


--drop table #TempConn
/*
select connote,workflowtype,count(*)from #TempConn
group by connote,workflowtype
having count(*)>1
--Duplicate check
*/

select distinct * into #14 from #Temp2 where LabelNumber like '_14%' and [WorkflowType] IN ('pickup','deliver')

select a.Labelnumber,a.WorkflowType,b.[NowGoWorkflowCompletionID] 
into #14Res 
from
#14 a
inner join scannergateway..[tbl_TrackingEventNowGo] b
On(a.LabelNumber = b.LabelNumber)
where a.[WorkflowType] IN ('pickup','deliver') 


--get the linked
							select distinct TRK1.Labelnumber lab1 ,TRK2.SourceReference lab2 into #re  from #14Res TRK1
inner join scannergateway..TrackingEvent TRK2
on(Trk1.LabelNumber = trk2.SourceReference)
where AdditionalText1 like '%link%'

delete from #tempconn where Connote in(
select distinct lab1 from #re)


select distinct * from #TempConn
--Final

End

GO
