SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[sp_PromoCode]

--sp_PromoCode '2019-07-01','2019-08-01'
As
Begin
select  ordernumber,StockCode,NetAmount,GSTAmount,GrossAmount 
into #Temp1
from prontosalesorderlines where stockcode  in( '124','693')

select a.StockCode,CustomerCode,pd.shortname as CustomerName,WarehouseCode as DriverNumber,a.GrossAmount as TotalSaleAmount,ps.accountingdate as  SalesDate , 
--Datepart(yyyy,ps.accountingdate) *100 + Datepart(MM,ps.accountingdate) as MonthKey 
  Convert(Varchar(20),Datepart(yyyy,ps.accountingdate))+'-'+Convert(Varchar(20),Datename(MM,ps.accountingdate)) as MonthKey ,
 COnvert (Date,Null) as FirstTransactionDate
into #Temp2
from #Temp1 a
inner join prontosalesorder ps
On(a.ordernumber =ps.ordernumber)
left join ProntoDebtor pd
On(ps.CustomerCode = pd.accountcode)

--Drop Table #Temp2
Delete From  #Temp2   where StockCode = '693' and DriverNumber not like 'A%'


Update #Temp2 SET FirstTransactionDate = (Select   Min(AccountingDate) from  prontosalesorder ps where ps.CustomerCode  = #Temp2.CustomerCode)


select StockCode, CustomerCode,CustomerName,Ps.DriverNumber,Depotcode,DepotName,convert(varchar(20),case left(Ps.DriverNumber,1) when   'B' Then 'Brisbane' 
     when 'A' Then 'Adelaide' 
	 when  'M' Then 'Melbourne' 
     when  'S' Then 'Sydney' 
	 when 'P' Then 'Perth' 
	 when  'T' Then 'Tasmania' 
	 when  'G' Then 'Gold Coast' 
	 when  'C' Then 'Canberra' 
	 when  'W' Then 'WA Country' 
	 else '' end) as Branch, TotalSaleAmount,SalesDate,MonthKey,FirstTransactionDate
from #Temp2 Ps
left join performancereporting..dimcontractor  Dc
On(ps.DriverNumber =Dc.ProntoID)
--where FirstTransactionDate between @StartDate and @EndDate

End

GO
GRANT CONTROL
	ON [dbo].[sp_PromoCode]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_PromoCode]
	TO [ReportUser]
GO
