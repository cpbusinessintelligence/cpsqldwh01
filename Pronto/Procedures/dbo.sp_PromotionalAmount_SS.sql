SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_PromotionalAmount_SS]
(@StartDate datetime,
 @EndDate datetime,
 @BranchCode Varchar(500)
 )
as


If @BranchCode = 'All'

Begin
select ordernumber,warehousecode,customercode,accountingdate  into #temp1 
from prontosalesorder ps
where 
 ps.accountingdate between @StartDate and @EndDate

  Select warehousecode,customercode AccountNo,stockcode,convert(date,null) as Salesstartdate,Sum(pso.GrossAmount) as GrossAmount,count(*) count into #TempFinal2 from prontosalesorderlines pso
inner join #temp1 ps
On(pso.ordernumber =ps.ordernumber)
group by warehousecode,customercode,stockcode
order by grossamount desc


Update #TempFinal2 SET Salesstartdate =  (Select min (accountingdate) From prontosalesorder where prontosalesorder.CustomerCode = #TempFinal2.AccountNo)

Select * from #TempFinal2 order by grossamount desc
end

 else

 Begin

select ordernumber,warehousecode,customercode,accountingdate into #temp2 
from prontosalesorder ps
where 
 ps.accountingdate between @StartDate and @EndDate and 
 @BranchCode = case when left(warehousecode,1) = 'B' Then 'Brisbane' 
     when left(warehousecode,1) = 'A' Then 'Adelaide' 
	 when left(warehousecode,1) = 'M' Then 'Melbourne' 
     when left(warehousecode,1) = 'S' Then 'Sydney' 
	 when left(warehousecode,1) = 'P' Then 'Perth' 
	 when left(warehousecode,1) = 'T' Then 'Tasmania' 
	 when left(warehousecode,1) = 'G' Then 'Gold Coast' 
	 when left(warehousecode,1) = 'C' Then 'Canberra' 
	 when left(warehousecode,1) = 'W' Then 'WA Country' 
	 else warehousecode end

	   Select warehousecode,customercode AccountNo,stockcode,convert(date,null) as Salesstartdate,Sum(pso.GrossAmount) as GrossAmount,count(*) count
	   
	   into #TempFinal from prontosalesorderlines pso
inner join #temp2 ps
On(pso.ordernumber =ps.ordernumber)
group by warehousecode,customercode,stockcode


Update #TempFinal SET Salesstartdate =  (Select min (accountingdate) From prontosalesorder where prontosalesorder.CustomerCode = #TempFinal.AccountNo)

Select * from #TempFinal order by grossamount desc

end
GO
GRANT EXECUTE
	ON [dbo].[sp_PromotionalAmount_SS]
	TO [ReportUser]
GO
