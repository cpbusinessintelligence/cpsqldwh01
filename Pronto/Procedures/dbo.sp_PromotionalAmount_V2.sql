SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE  proc [dbo].[sp_PromotionalAmount_V2]
as
Begin

--,ps.accountingdate SalesDate , Datepart(yyyy,ps.accountingdate) *100 + Datepart(MM,ps.accountingdate) as MonthKey , COnvert (Date,Null) as FirstTransactionDate

select ordernumber,
       warehousecode,
	   customercode,
	   convert(varchar(50), '') as customername,
	    convert(varchar(50), '') as DepotCode,
		 convert(varchar(50), '') as DepotName,
	   accountingdate, 
	   ps.accountingdate SalesDate , [GrossAmount] ,
	   --Datepart(yyyy,ps.accountingdate) *100 + Datepart(MM,ps.accountingdate) as MonthKey ,
	    Convert(Varchar(20),Datepart(yyyy,ps.accountingdate))+'-'+Convert(Varchar(20),Datename(MM,ps.accountingdate)) as MonthKey ,
	   COnvert (varchar(20),'') as ReturningCustomerLastSaleDate
	into #temp1 
	from prontosalesorder ps
	where  ps.accountingdate >=  '2019-07-01'  and warehousecode <>'01' and len(customercode) >5
	
Update #temp1 SET ReturningCustomerLastSaleDate =  (Select max (accountingdate) From prontosalesorder where prontosalesorder.CustomerCode = #temp1.customercode and AccountingDate <= '2019-07-01')
--Removing all sales which are happened in last 6 months
Update #temp1 SET ReturningCustomerLastSaleDate = 'New Customer' where ReturningCustomerLastSaleDate is null
Delete  from #temp1 where ReturningCustomerLastSaleDate >= '2019-01-01' and  ReturningCustomerLastSaleDate <= '2019-07-01'
	 update #temp1
set customername = shortname
from ProntoDebtor a
inner join #temp1 b
On(a.Accountcode = b.customercode)

update #temp1
set Depotcode = dc.DepotCode
From performancereporting..dimcontractor dc
join #temp1 b
On(dc.prontoid =b.warehousecode)

update #temp1
set Depotname = dc.DepotName
From performancereporting..dimcontractor dc
join #temp1 b
On(dc.prontoid =b.warehousecode)

	Select * ,Case left(warehousecode,1) when   'B' Then 'Brisbane' 
     when 'A' Then 'Adelaide' 
	 when  'M' Then 'Melbourne' 
     when  'S' Then 'Sydney' 
	 when 'P' Then 'Perth' 
	 when  'T' Then 'Tasmania' 
	 when  'G' Then 'Gold Coast' 
	 when  'C' Then 'Canberra' 
	 when  'W' Then 'WA Country' 
	 else '' end as Branch from #Temp1





end

GO
GRANT EXECUTE
	ON [dbo].[sp_PromotionalAmount_V2]
	TO [ReportUser]
GO
