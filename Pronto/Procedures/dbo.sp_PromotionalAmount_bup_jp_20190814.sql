SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_PromotionalAmount_bup_jp_20190814]
(@StartDate datetime,
 @EndDate datetime,
 @BranchCode Varchar(500)
 )
as


If @BranchCode = 'All'

Begin
select ordernumber,warehousecode,customercode,accountingdate  into #temp1 
from prontosalesorder ps
where 
 ps.accountingdate between @StartDate and @EndDate

  Select warehousecode,customercode AccountNo,stockcode,Sum(pso.GrossAmount) as GrossAmount,count(*) count from prontosalesorderlines pso
inner join #temp1 ps
On(pso.ordernumber =ps.ordernumber)
group by warehousecode,customercode,stockcode
order by grossamount desc
end

 else

 Begin

select ordernumber,warehousecode,customercode,accountingdate into #temp2 
from prontosalesorder ps
where 
 ps.accountingdate between @StartDate and @EndDate and 
 @BranchCode = case when left(warehousecode,1) = 'B' Then 'Brisbane' 
     when left(warehousecode,1) = 'A' Then 'Adelaide' 
	 when left(warehousecode,1) = 'M' Then 'Melbourne' 
     when left(warehousecode,1) = 'S' Then 'Sydney' 
	 when left(warehousecode,1) = 'P' Then 'Perth' 
	 when left(warehousecode,1) = 'T' Then 'Tasmania' 
	 when left(warehousecode,1) = 'G' Then 'Gold Coast' 
	 when left(warehousecode,1) = 'C' Then 'Canberra' 
	 when left(warehousecode,1) = 'W' Then 'WA Country' 
	 else warehousecode end

	   Select warehousecode,customercode AccountNo,stockcode,Sum(pso.GrossAmount) as GrossAmount,count(*) count from prontosalesorderlines pso
inner join #temp2 ps
On(pso.ordernumber =ps.ordernumber)
group by warehousecode,customercode,stockcode
order by grossamount desc
end
GO
