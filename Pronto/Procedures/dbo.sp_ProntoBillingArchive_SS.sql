SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure sp_ProntoBillingArchive_SS (@BillingDate Date)
as
Begin

Begin transaction ProntoBilling

INSERT INTO DBO.ProntoBilling_Archive
Select * from DBO.ProntoBilling
Where BillingDate <= @BillingDate

Delete from DBO.ProntoBilling
Where BillingDate <= @BillingDate

Commit transaction ProntoBilling

End
GO
