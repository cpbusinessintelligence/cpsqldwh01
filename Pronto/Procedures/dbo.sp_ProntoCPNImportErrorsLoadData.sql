SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_ProntoCPNImportErrorsLoadData] as
begin


     --'=====================================================================
    --' CP -Stored Procedure -[sp_ProntoCPNImportErrorsLoadData]
    --' ---------------------------
    --' Purpose: Load ProntoCPNImport Errors table from load table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 06 Nov 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 06/11/2014    AB      1.00    Created the procedure                             --AB20141106

    --'=====================================================================


truncate table [dbo].[ProntoCPNImportErrors]
INSERT INTO [dbo].[ProntoCPNImportErrors]
           ([RecordType]
           ,[Contractor]
           ,[ActivityDateTime]
           ,[SerialNumber]
           ,[LinkCouponNumber]
           ,[ReturnTracker]
           ,[AccountNumber]
           ,[PhoneNumber]
           ,[Invoice]
           ,[ActivityType]
           ,[ConsignmentType]
           ,[InsuranceCode]
           ,[InsuranceValue]
           ,[FileName])
    select [RecordType]
           ,[Contractor]
           ,[ActivityDateTime]
           ,[SerialNumber]
           ,[LinkCouponNumber]
           ,[ReturnTracker]
           ,[AccountNumber]
           ,[PhoneNumber]
           ,[Invoice]
           ,[ActivityType]
           ,[ConsignmentType]
           ,[InsuranceCode]
           ,[InsuranceValue]
           ,[FileName] from [dbo].[ProntoCPNImportErrors_Temp]
           
end

GO
GRANT EXECUTE
	ON [dbo].[sp_ProntoCPNImportErrorsLoadData]
	TO [SSISUser]
GO
