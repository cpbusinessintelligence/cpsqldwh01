SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-08-10
-- Description:	Pronto Debtor List
-- =============================================
CREATE PROCEDURE sp_ProntoDebtorListAutomation
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	SELECT pd.[Accountcode]
		  ,Pd.[BillTo]
		  ,Pd.[Shortname] as [Shortname]
		  ,Pd.[Warehouse] as [Warehouse]
		  ,Pd.[TermDisc] as [TermDisc]
		  ,Pd.[Territory] as [Territory]
		  ,Pd.[RepCode] as [RepCode]
		  ,Pd.[CustType] as [CustType]
		  ,Pd.[AverageDaysToPay] as [AverageDaysToPay]
		  ,[Postcode] as [Postcode]
		  ,Pd.[LastSale] as [LastSale]
		  ,[ABN] as [ABN]	   
		  ,[RepName] as [RepName],
		  ----------------------------------------------------
		  --,[NameAddressName] as [Address Name]
		  --,[NameAddressCompany] as [Address Company]
	   --   ,[NameAddressStreet] as [Street Name]
		  --,[NameAddressSuburb] as [Suburb]
		  --,[NameAddressCountry] as [Country]
		  --,[NameAddressAddress_6] as [Address1]
		  --,[NameAddressAddress_7] as [Address2]
		  -- ,[NameAddressPhone] as [Phone]
		  --,[NameAddressMobilePhone] as [MobilePhone]
		[na_name] as [Address Name],
		[na_company] as [Address Company],
		[na_street] as [Street Name],
		[na_suburb]  as [Suburb],
		[na_country]  as [Country],
		[na_address_6] as [Address1],
		[na_address_7] as [Address2],
		[na_phone] as [Phone],
		[na_phone_2] as [MobilePhone],
		dm.dr_date_created as [AccountOpenDate],
		Convert(varchar(10),'Active') [Status]
	INTO #tmpList 
	FROM [Pronto].[dbo].[ProntoDebtor] Pd with(Nolock)
		LEFT JOIN (select [accountcode],[na_name],
			 [na_company],
			 [na_street],
			 [na_suburb],
			 [na_country],
			 [na_address_6],
			 [na_address_7],
			 [na_phone],
			 [na_phone_2] from cpsqldev01.[ProntoTest].[dbo].[name_and_address_master]   where na_type = 'C' ) Naddr
			On(Pd.Accountcode = Naddr.Accountcode)
		LEFT JOIN ( select distinct accountcode, bill_to, dr_date_created from cpsqldev01.[ProntoTest].[dbo].[deb_master]) dm
			On (Pd.Accountcode = dm.accountcode)

	--Select count(*) From drop table #tmpList --195018
	---------------------------------------------------------------------------------------
	Update #tmpList Set [Status] = 'InActive' Where DATEDIFF(D,ISNULL([LastSale],GETDATE()),GETDATE()) > 365 ---No Sale for last 12 months
	Update #tmpList Set [Status] = 'Active' Where DATEDIFF(D,ISNULL([AccountOpenDate],GETDATE()),GETDATE()) <= 90 -- Account created within last 3 months
	Update #tmpList Set [Status] = 'Active' Where Accountcode in (Select Distinct BillTo From #tmpList Where [Status] = 'Active') --Parent account set to Active, if child is active.

	Select
		Accountcode
		,BillTo	
		,Shortname	
		,Warehouse	
		,TermDisc	
		,Territory	
		,RepCode	
		,CustType	
		,AverageDaysToPay	
		,Postcode	
		,LastSale	
		,ABN	
		,RepName	
		,[Address Name]	
		,[Address Company]	
		,[Street Name]	
		,Suburb	
		,Country	
		,Address1	
		,Address2	
		,Phone	
		,MobilePhone	
		,Status
	From #tmpList

END
GO
