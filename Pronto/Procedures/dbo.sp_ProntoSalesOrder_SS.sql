SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_ProntoSalesOrder_SS (@AccountingDate DateTime)
as

Begin transaction PerfRep5

Insert into [dbo].[ProntoSalesOrder_Archive17-18]
select * from [dbo].ProntoSalesOrder
where [AccountingDate] < = @AccountingDate

Delete from [dbo].ProntoSalesOrder
where [AccountingDate] < = @AccountingDate


Commit transaction PerfRep5

GO
