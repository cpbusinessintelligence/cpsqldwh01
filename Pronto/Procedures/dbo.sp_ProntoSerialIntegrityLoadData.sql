SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_ProntoSerialIntegrityLoadData] 
	AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[sp_ProntoSerialIntegrityLoadData]
    --' ---------------------------
    --' Purpose: Load ProntoSerial Integrity table from load table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 06 Nov 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 06/11/2014    AB      1.00    Created the procedure                             --AB20141106

    --'=====================================================================


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    Truncate table  [Pronto].[dbo].[ProntoSerialIntegrity]

    INSERT INTO [Pronto].[dbo].[ProntoSerialIntegrity]
          ([SerialNumber]
           ,[Contractor]
           ,[CurrentStatus]
           ,[Error]
           ,[Action]
           ,[NewStatus]) 
        SELECT  [SerialNumber]
           ,[Contractor]
           ,[CurrentStatus]
           ,[Error]
           ,[Action]
           ,[NewStatus]
  FROM [Pronto].[dbo].[ProntoSerialIntegrity_Temp]
  end
GO
GRANT EXECUTE
	ON [dbo].[sp_ProntoSerialIntegrityLoadData]
	TO [SSISUser]
GO
