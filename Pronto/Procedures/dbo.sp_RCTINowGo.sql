SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Proc sp_RCTINowGo (@StartDate Datetime,@EndDate Datetime,@DriverID Varchar(200))
as
Begin
--select distinct driverrunnumber,driverextref  from scannergateway..[tbl_TrackingEventNowGo]
--where driverrunnumber in (32,111,15,27,22)

select * into #Temp6 from scannergateway..[tbl_TrackingEventNowGo]
where CreatedDateTime >= @StartDate and CreatedDateTime <=  @EndDate and ([DriverExtRef] = @DriverID  or @DriverID is null)


--select * from #temp6 where DriverRunNumber in (32,111,193)
--order by branch

select [DriverExtRef],Labelnumber,cd_connote As Connote,convert(varchar(100),'') as PickupDriver,convert(money,'')  as PickupRCTI,convert(varchar(100),'') as DeliveryDriver,convert(Money,'') as DeliveryRCTI,WorkflowType,EventDateTime 
into #Temp1
from #Temp6 jp
left join cppledi..cdcoupon cd
On(jp.LabelNumber =cd.cc_coupon)
left join cppledi..consignment co
On(cd.cc_consignment =co.cd_id)
where WorkflowType in ('pickup','deliver')


update #temp1
set pickupdriver = pickupContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber) 
where WorkflowType = 'pickup'


update #temp1
set DeliveryDriver = DeliveryContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber) where WorkflowType = 'deliver'


update #temp1
set PickupRCTI = PickupRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber)
where WorkflowType in ('pickup')


update #temp1
set DeliveryRCTI = deliveryRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.Connote = Pc.SerialNumber)
where WorkflowType in ('deliver')


--Label
update #temp1
set pickupdriver = pickupContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null and  WorkflowType = 'pickup'


update #temp1
set DeliveryDriver = DeliveryContractor
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null and WorkflowType = 'deliver'


update #temp1
set PickupRCTI = PickupRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null
and  WorkflowType in ('pickup')


update #temp1
set DeliveryRCTI = deliveryRctiAmount
From #temp1 te
inner join  [dbo].[ProntoCouponDetails] Pc
On(te.LabelNumber = Pc.SerialNumber)
where connote is null
and  WorkflowType in ('deliver')

-----

Update #temp1 SET COnnote  =  Labelnumber where isnumeric(Labelnumber) = 1 and len(LabelNumber) =11 and ConNote is null

Select distinct Labelnumber,connote,
case 
 when [workflowtype] = 'pickup' then pickupDriver
when [workflowtype] = 'deliver' then Deliverydriver end Driver,

case 
 when [workflowtype] = 'pickup' then PickupRCTI
when [workflowtype] = 'deliver' then DeliveryRCTI end RCTIAmount,
[WorkflowType],EventDateTime 
 From #Temp1
where workflowtype in ('pickup' , 'deliver')
--and driver in ()
order by eventdatetime 
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RCTINowGo]
	TO [ReportUser]
GO
