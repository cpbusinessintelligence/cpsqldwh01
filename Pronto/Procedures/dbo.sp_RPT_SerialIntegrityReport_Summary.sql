SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[sp_RPT_SerialIntegrityReport_Summary]
  (@Branch Varchar(20))
AS
BEGIN

     --'=====================================================================
    --' CP -Stored Procedure -[sp_RPT_SerialIntegrityReport_Summary]
    --' ---------------------------
    --' Purpose: ProntoSerialIntegrity Summary Report -----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 06 Nov 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 06/11/2014    AB      1.00    Created the procedure                             --AB20141106

    --'=====================================================================

 
SELECT  I.[SerialNumber]
       ,I.[Contractor]
       ,I.[CurrentStatus]
       ,I.[Error]
       ,I.[Action]
       ,I.[NewStatus]
       ,S.SerialInvoiceNumber
       ,S.SerialWhseCode
       ,S.SerialStatus 
  INTO #Temp1     
  FROM [Pronto].[dbo].[ProntoSerialIntegrity] I Join Pronto.dbo.ProntoStockSerialNumber S ON I.SerialNumber = S.SerialNumber
  WHERE S.SerialOriginalInvNumber <> 'CONVERSI'
 
 if @Branch = 'ALL' 
   Select 
  (CASE WHEN T.SerialWhseCode ='CSY' OR   T.SerialWhseCode LIKE 'S%' THEN 'SYDNEY' 
              WHEN T.SerialWhseCode ='CME' OR   T.SerialWhseCode LIKE 'M%' THEN 'MELBOURNE' 
              WHEN T.SerialWhseCode ='CAD' OR   T.SerialWhseCode LIKE 'A%' THEN 'ADELAIDE'                             
              WHEN T.SerialWhseCode ='CPE' OR   T.SerialWhseCode LIKE 'P%' THEN 'PERTH'                            
              WHEN T.SerialWhseCode ='CBN' OR   T.SerialWhseCode LIKE 'B%' THEN 'BRISBANE'                            
              WHEN T.SerialWhseCode ='COO' OR   T.SerialWhseCode LIKE 'G%' THEN 'GOLD COAST'                          
              ELSE 'UNKNOWN' END)  as Branch
       ,T.SerialWhseCode
       ,C.StartSerialNumber 
	   ,count(*) as SerialnumberCount
      FROM #Temp1  T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
	  group by 
	    (CASE WHEN T.SerialWhseCode ='CSY' OR   T.SerialWhseCode LIKE 'S%' THEN 'SYDNEY' 
              WHEN T.SerialWhseCode ='CME' OR   T.SerialWhseCode LIKE 'M%' THEN 'MELBOURNE' 
              WHEN T.SerialWhseCode ='CAD' OR   T.SerialWhseCode LIKE 'A%' THEN 'ADELAIDE'                             
              WHEN T.SerialWhseCode ='CPE' OR   T.SerialWhseCode LIKE 'P%' THEN 'PERTH'                            
              WHEN T.SerialWhseCode ='CBN' OR   T.SerialWhseCode LIKE 'B%' THEN 'BRISBANE'                            
              WHEN T.SerialWhseCode ='COO' OR   T.SerialWhseCode LIKE 'G%' THEN 'GOLD COAST'                          
              ELSE 'UNKNOWN' END)
        ,T.SerialWhseCode
       ,C.StartSerialNumber 
  ELSE
     Select @Branch as Branch
       ,T.SerialWhseCode
       ,C.StartSerialNumber 
	   ,count(*) as SerialnumberCount
      FROM #Temp1  T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
      WHERE T.SerialWhseCode = CASE WHEN @Branch = 'SYDNEY' THEN 'CSY'
                                    WHEN @Branch = 'MELBOURNE' THEN   'CME'
                                    WHEN @Branch = 'ADELAIDE' THEN   'CAD'
                                    WHEN @Branch = 'PERTH' THEN      'CPE'
                                    WHEN @Branch = 'BRISBANE' THEN   'CBN'
                                    WHEN @Branch = 'GOLD COAST' THEN   'COO'
                                    ELSE '' END
	  group by T.SerialWhseCode
               ,C.StartSerialNumber
  UNION ALL
      Select @Branch as Branch
       ,T.SerialWhseCode
       ,C.StartSerialNumber 
	   ,count(*) as SerialnumberCount
      FROM #Temp1  T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
      WHERE T.SerialWhseCode Like CASE WHEN @Branch = 'SYDNEY' THEN  'S%' 
                 WHEN @Branch = 'MELBOURNE' THEN  'M%' 
                 WHEN @Branch = 'ADELAIDE' THEN   'A%' 
                 WHEN @Branch = 'PERTH' THEN      'P%' 
                 WHEN @Branch = 'BRISBANE' THEN   'B%' 
                 WHEN @Branch = 'GOLD COAST' THEN 'G%' 
                 ELSE  'Z%' END
      group by T.SerialWhseCode
               ,C.StartSerialNumber
  SET NOCOUNT OFF;

END
GO
GRANT EXECUTE
	ON [dbo].[sp_RPT_SerialIntegrityReport_Summary]
	TO [ReportUser]
GO
