SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-12-10
-- Description:	Get RCTI information of the driver for the given date range
-- =============================================
CREATE PROCEDURE sp_RptContractorRCTISumaryReport
	-- Add the parameters for the stored procedure here
	@Contractor Varchar(20), 
	@FromDate Date,
	@Todate Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
		[ContractorId]
		,[RunNumber]
		,[RunName]
		,[Branch]
		,[RctiWeekEndingDate]
		,[TotalDevelopmentIncentive]
		,[TotalPrepaidRctiAmount]
		,[TotalEdiRctiAmount]
		,[TotalRctiAllowances]
		,[TotalRctiDeductions]
		,[TotalRctiRedemptions]
		,[Depot]
		,[TotalDevelopmentBonus]
	FROM [Pronto].[dbo].[CPPL_ContractorRctiPaymentsTotal] 
	WHERE ContractorId = @Contractor
		AND RctiWeekEndingDate >= @FromDate
		AND RctiWeekEndingDate <= @Todate
    
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptContractorRCTISumaryReport]
	TO [ReportUser]
GO
