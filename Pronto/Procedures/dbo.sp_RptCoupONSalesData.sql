SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_RptCoupONSalesData] 
      (@OrderNumber int)  
AS
BEGIN
  --'=====================================================================
    --' [sp_RptCoupONSalesData] '35517814'
    --' ---------------------------
    --' Purpose: sp_RptCoupONSalesData-----
    --' Developer: Sinshith
    --' Date: 06/07/2017
    --' Copyright:  Couriers PleASe Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     ReASON                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --'=====================================================================

SELECT 
       [OrderNumber]
      ,[CustomerCode]
      ,[WarehouseCode]
      ,[TerritoryCode]
      ,[AccountingDate]
      ,[AccountingPeriod]
      ,[NetAmount]
      ,[GSTAmount]
      ,[GrossAmount]
INTO #temp1
  FROM [PrONto].[dbo].[PrONtoSalesOrder]
  --where [TerritoryCode] like 'X%'
  WHERE AccountingDate > '2016-10-10'
  AND ordernumber = @OrderNumber


  SELECT  t.[OrderNumber]
      , D.SerialInvoicedDate AS [Invoice Date]
	  , D.SerialInvoiceNumber AS [Invoice Number]
	  ,[CustomerCode]
	  ,ShortName AS [CustomerName]
	  ,F.Stockcode AS ItemCode 
	  ,F.StkDescriptiON AS [Item DescriptiON]
	  ,StartSerialNumber AS [Serial No From]
	  ,C.ReturnSerialNumber AS [Serial No To]
	 , F.StkCONversiONFactor AS ItemQuantity 
	 ,[WarehouseCode]
	 ,F.StkReplacementCost
      ,C.RevenueAmount AS [Price Sold Per Unit]
      ,[NetAmount]
      ,[GSTAmount]
      ,[GrossAmount]
         
  from #temp1 t 
  join [PrONto].dbo.[PrONtoCoupONDetails] C ON T.OrderNumber = C.[LAStSoldReference]
  join [PrONto].dbo.prONtostockserialnumber D ON (StartSerialNumber = D.SerialNumber)
  join [PrONto].dbo.prONtodebtor E ON (D.AccountCode =E.AccountCode)
  join [PrONto].dbo.prONtostockMASter F ON (D.stockcode =F.stockcode)
  --join prONtoBilling Pb ON (T.OrderNumber = Pb.OrderNumber)
  GROUP BY T.[OrderNumber]
	  ,D.SerialInvoicedDate
	  ,D.SerialInvoiceNumber
	  ,ReturnSerialNumber
      ,[CustomerCode]
	  ,ShortName
      ,[WarehouseCode]
	  ,F.Stockcode
	  ,F.StkDescriptiON
	  , F.StkPackQty
	  ,F.StkReplacementCost 
      ,[NetAmount]
      ,[GSTAmount]
      ,[GrossAmount],
       StartSerialNumber
	  ,C.RevenueAmount
	  , F.StkCONversiONFactor
         ORDER BY 1 ASC

		 END
GO
