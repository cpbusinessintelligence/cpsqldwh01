SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptFueltobeRebilledetail](@Account varchar(50)) as 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
      [InvoiceNumber],
       [ConsignmentReference] as [Consignemnt Number]
      ,[CustomerReference]
      ,[ConsignmentDate]
	  ,AccountBillToName
	  ,AccountName
         ,[AccountingDate] as [BilledDate]
      ,[AccountCode]
      ,[AccountBillToCode]
      ,[ServiceCode]

      ,[OriginLocality]
      ,[OriginPostcode]
      ,[ReceiverName]
      ,[DestinationLocality]
      ,[DestinationPostcode]
    

      ,[ItemQuantity]
      ,[DeclaredWeight]
      ,[DeclaredVolume]
      ,[ChargeableWeight]

     ,[BilledFreightCharge]
      ,[BilledFuelSurcharge]
      ,[BilledInsurance]
      ,[BilledTotal]

      ,[FuelTobeBilled]
      ,[DifferenceAmount] as [Amount to be Billed]

  FROM [Pronto].[dbo].[FuelToBeBilledDetailv1]
  where [accountBilltocode]=@Account

  
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptFueltobeRebilledetail]
	TO [ReportUser]
GO
