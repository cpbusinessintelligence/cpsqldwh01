SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure sp_RptFueltobebilledetail(@Account varchar(50),@Diff decimal(12,2)) as 
begin

Select * into #temp5 from FuelToBeBilledSummary where [Difference]>@Diff and accountcode=@Account

select * from [FuelToBeBilledDetail] where  accountcode+'_'+servicecode+'_'+convert(varchar(20),accountingdate) in (Select accountcode+'_'+servicecode+'_'+convert(varchar(20),accountingdate) from #temp5)

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptFueltobebilledetail]
	TO [ReportUser]
GO
