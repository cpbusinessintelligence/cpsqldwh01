SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptInvoicereportByInvoiceNumber ](@InvoiceNum varchar(100),@StartDate date,@EndDate date) as
begin

      --'=====================================================================
    --' CP -Stored Procedure -[sp_RptInvoicereportforParentAccount]
    --' ---------------------------
    --' Purpose:sp_RptInvoicereportforParentAccount-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 04 Aug 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 04/08/2016    AB      1.00    Created the procedure                             --AB20160804

    --'=====================================================================
SELECT  [InvoiceNumber] as [Inv.No.],
                [AccountingDate] as [Inv.Date],
				[ConsignmentReference],
				[ConsignmentDate],
				[ManifestReference],
				[AccountCode] as CustomerCode,
				[AccountName] as CustomerName,
                [ServiceCode],
				pc_name as ServiceName,
				[ManifestDate],
				[OriginPostcode],
				[OriginLocality],
				[DestinationPostcode],
				[DestinationLocality],
				[ReceiverName],
				[CustomerReference],
				[ItemQuantity] as TotalItems,
				[DeclaredWeight] as TotalWeight,
				DeclaredVolume as TotalVolume,
				isnull([InsuranceCategory],'') as [InsuranceCategory],
				0 as DeclaredValue,
				[ChargeableWeight],
				[BilledFreightCharge] as ExGST,
                [BilledFuelSurcharge] as FuelSurcharge,
                [BilledInsurance] as Insurance,
                [BilledOtherCharge] as Other,
				0 as GST,
				RevenueOriginZone as OriginZone,
				RevenueDestinationZone as DestinationZone,
				[DeclaredWeight] as DeclaredWeight,
                [DeadWeight] as MeasuredWeight,
                [DeclaredVolume]*250 as DeclaredVolume,
                [Volume]*250 as  MeasuredVolume
--into #temp
from [Pronto].[dbo].[ProntoBilling]  left join cpplEDI.dbo.pricecodes on pc_code=servicecode
where ltrim(rtrim([InvoiceNumber])) =@InvoiceNum and accountingdate between @StartDate and @EndDate
order by [Inv.Date],Accountcode

--union
----Insert into #temp

--Select          [InvoiceNumber] as [Inv.No.],
--                [AccountingDate]  as [Inv.Date],
--				[ConsignmentReference],
--				[ConsignmentDate],
--				isnull([ManifestReference],'') as [ManifestReference],
--				[AccountCode],
--				[AccountName],
--                [ServiceCode],
--				'' as ServiceName,
--				'' as [ManifestDate],
--				isnull([OriginPostcode],'') as [OriginPostcode],
--				isnull([OriginLocality],'') as [OriginLocality],
--				isnull([DestinationPostcode],'') as [DestinationPostcode],
--				isnull([DestinationLocality],'') as [DestinationLocality],
--				isnull([ReceiverName],'') as [ReceiverName],
--				isnull([CustomerReference],'') as [CustomerReference],
--				[ItemQuantity] as TotalItems,
--				[DeclaredWeight] as TotalWeight,
--				DeclaredVolume as TotalVolume,
--				isnull([InsuranceCategory],'') as [InsuranceCategory],
--				0 as DeclaredValue,
--				[ChargeableWeight],
--				[BilledFreightCharge] as ExGST,
--                [BilledFuelSurcharge] as FuelSurcharge,
--                [BilledInsurance] as Insurance,
--                [BilledOtherCharge] as Other,
--				0 as GST,
--				RevenueOriginZone as OriginZone,
--				RevenueDestinationZone as DestinationZone,
--				[DeclaredWeight] as DeclaredWeight,
--                [DeadWeight] as MeasuredWeight,
--                [DeclaredVolume]*250 as DeclaredVolume,
--                [DeadVolume]*250 as  MeasuredVolume
-- FROM [Pronto].[dbo].ProntoBillingPaymentSurcharge 
-- where accountcode in (select accountcode from [ProntoBilling] where AccountBillToCode=@Account) and  [AccountingDate] between @StartDate and @EndDate
-- order by [Inv.Date],Accountcode,ManifestDate desc

 --Select * from #temp 
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptInvoicereportByInvoiceNumber ]
	TO [ReportUser]
GO
