SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-12-10
-- Description:	
-- =============================================
-- sp_RptPrepaidSurchargeReport 'CouponSummary','20200490329'
-- sp_RptPrepaidSurchargeReport 'StockSummary','20200490329'
CREATE PROCEDURE sp_RptPrepaidSurchargeReport
	-- Add the parameters for the stored procedure here
	@ReportType varchar(20),
	@SerialNumber varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @ReportType = 'CouponSummary'
	BEGIN
		SELECT 
			 [SerialNumber]
			,[StartSerialNumber]
			,[ReturnSerialNumber]
			,[TransferDate]
			,[LastSoldReference]
			,[LastSoldDate]
			,[PickupContractor]
			,[PickupDate]
			,[PickupRctiDate]
			,[PickupRctiReference]
			,[PickupRctiAmount]
			,[DeliveryContractor]
			,[DeliveryDate]
			,[DeliveryRctiDate]
			,[DeliveryRctiReference]
			,[DeliveryRctiAmount]
			,[RevenueDate]
			,[RevenueAmount]	
		FROM [ProntoPurge_Bkup_Do_Not Delete].[dbo].[ProntoCouponDetails_bkup_12062020_do_not_delete]
		WHERE SerialNumber = @SerialNumber		
	END
	ELSE
	BEGIN
		SELECT 
			 [SerialNumber]		
			,[ContractNumber]
			,[StockCode]
			,[Accountcode]
			,[SerialStatus]
			,[SerialWhseCode]
			,[SerialInvoiceNumber]
			,[SerialCurrentContractIncome]
			,[SerialOriginalInvNumber]
			,[SerialSequenceNumber]			
		FROM [ProntoPurge_Bkup_Do_Not Delete].[dbo].[ProntoStockSerialNumber] nolock
		WHERE SerialNumber = @SerialNumber
	END


END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptPrepaidSurchargeReport]
	TO [ReportUser]
GO
