SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_RptSubsidy_Test](@StartDate date,@EndDate date) as
begin
--declare @publicholiday as int;
    --'=====================================================================
    --' CP -Stored Procedure -[sp_RptSubsidy]
    --' ---------------------------
    --' Purpose: Create Subsidy Report-----
    --' Developer: Tejes (Couriers Please Pty Ltd)
    --' Date: 11 April 2018
    --' Copyright: 2018 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 11/04/2018    TS     1.00    Created the procedure                             --TS20180411

    --'=====================================================================
	--drop table #temp
	--drop table #temp0
	--drop table #temp1
	--drop table #temp2
	--DECLARE @StartDate date
	--DECLARE @EndDate date
	--Set @StartDate = '2020-08-25' 
	--Set @EndDate = '2020-09-25' 
	--changed by Heena on 11th sept 2020
select distinct [ProntoID] into #temp from [dbo].[tblContractor] where status='Active'

select distinct [RctiWeekEndingDate] into #Temp0 from [Pronto].[dbo].[FranchiseePaymentSummary] (NOLOCK) where [RctiWeekEndingDate] between @StartDate and @EndDate  -- '2019-04-15' and '2019-04-21'  --

--select * from  #Temp0 order by 1 desc

Select    C.[DeliveryDriver]
         ,C.[ProntoID]
		 ,C.DriverNumber
	--	 ,CASE WHEN EffectiveDate is not null THEN C.NewBaseAmount Else C.BaseAmount END as BaseAmount
		,CASE WHEN EffectiveDate<=@StartDate  THEN C.NewBaseAmount Else C.BaseAmount END as BaseAmount
		 ,C.MaxAmount
		 ,C.State
		 ,Convert(decimal(12,2),0) as CalculatedBaseAmount
		 ,convert(varchar(2),'') as BaseAmountFlag
         ,[ContractorId]
		 ,[RunNumber]
		 ,[RunName]
		 ,[Branch]
		 ,P.[RctiWeekEndingDate]
		 ,convert(Datetime,Null) as ProcessingWeekEnding
         ,[TotalDevelopmentIncentive] as SubSidyPaid
         ,[TotalRctiRedemptions] as RCTIRedemptionPayment
         ,Convert(decimal(12,2),0) as TotalDelivery
         ,Convert(decimal(12,2),0) as DeliveryAllowanceValue
         ,Convert(decimal(12,2),0) as DifferenceSubsidyandAllowance
         ,Convert(decimal(12,2),0) as PaymnetAmount
         ,Convert(decimal(12,2),0) as AdditionalAmttobepaid
		 ,convert (int,0) as [Publicholiday]
Into #Temp1  from Pronto.dbo.tblContractor C (NOLOCK)
Join  [Pronto].[dbo].[FranchiseePaymentSummary] P (NOLOCK) on C.ProntoId = P.ContractorID 
join #Temp0 T0 on P.RctiWeekEndingDate=T0.RctiWeekEndingDate
where P.[RctiWeekEndingDate] = T0.RctiWeekEndingDate and C.status='Active'

Select D.DeliveryContractor , Count(*) as Ct,T0.RctiWeekEndingDate  into #Temp2 from ProntoCouponDetails_forsubsidy D (NOLOCK) --Pronto.dbo.ProntoCouponDetails D  (NOLOCK)
Join #Temp T On D.DeliveryContractor=T.ProntoID
join #Temp0 T0 On D.DeliveryRCTIDate=T0.RctiWeekEndingDate
Where D.DeliveryRCTIDate=T0.RctiWeekEndingDate 
--and Pronto.dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(D.StartSerialNumber,3)) not in ('RETURN TRK','IRP TRK','ATL','LINK','PROMO','REDELIVERY CPN','REDELIVERY CARD','PROFORMA','LINK - REGIONAL')
group by   D.DeliveryContractor,T0.RctiWeekEndingDate 
--select * from #temp1

update  t1 set t1.publicholiday=p1.publicholiday  from #temp1 t1 Inner join
(select Prontoid,count(*) publicholiday from  CpplEDI.[dbo].[holidays] h  inner join #Temp1 t on t.State=h.h_state where  convert(date,h_holiday,103) between @StartDate and @EndDate   group by Prontoid )as p1 on t1.Prontoid=p1.prontoid

/*

if (@publicholiday<>0 )
begin
update #Temp1 set BaseAmount =(BaseAmount/5) * (5-@publicholiday);
end
*/

update #temp1 set BaseAmount= case when publicholiday <>0 then (BaseAmount/5) * (5-publicholiday) else BaseAmount end

--update #Temp1 set MaxAmount = case when publicholiday <>0 then (MaxAmount/5) * (5-publicholiday) else MaxAmount end

update #Temp1 SET ProcessingWeekEnding = dateadd(ww,+2,[RctiWeekEndingDate])

--update #Temp1 SET SubSidyPaid = [BaseAmount]-RCTIRedemptionPayment

Update #Temp1 SET TotalDelivery  =  t2.Ct  From #Temp1 T Join #Temp2 T2 on T.ProntoId = T2.DeliveryContractor where T.RctiWeekEndingDate=T2.RctiWeekEndingDate

Update #Temp1 SET DeliveryAllowanceValue  = TotalDelivery * 0.90 
Update #Temp1 SET DifferenceSubsidyandAllowance  =DeliveryAllowanceValue- SubSidyPaid

Update #Temp1 SET PaymnetAmount = CASE  WHEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment <= BaseAmount THen BaseAmount  
                                               WHEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment Between BaseAmount and MaxAmount THEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment 
                                                                           WHEN DifferenceSubsidyandAllowance + SubSidyPaid + RCTIRedemptionPayment > MaxAmount THEN MaxAmount 
                                                                           ELSE 0 END
Update #temp1 set AdditionalAmttobepaid = PaymnetAmount - (SubSidyPaid+RCTIRedemptionPayment) where PaymnetAmount - (SubSidyPaid+RCTIRedemptionPayment) >0

update #Temp1 SET CalculatedBaseAmount = SubSidyPaid+RCTIRedemptionPayment

update #Temp1 SET BaseAmountFlag = CASE WHEN BaseAmount=CalculatedBaseAmount THEN 'Y'
								WHEN CalculatedBaseAmount>BaseAmount and SubSidyPaid=0 THEN 'Y'

							ELSE 'N' END
/*If calculatedamt > baseamount and subsidy=0 then 'Y'

If calculatedamt > baseamount and subsidy>0 then 'N'*/
       --Update #Temp1 SET  EDIDelivery =  (Select sum(P.[DeliveryCount])  From #Temp1 T join [Pronto].[dbo].[CPPL_ContractorRctiPaymentsByServiceCode] P on T.ProntoId = P.ContractorId where  P.[RctiWeekEndingDate] = '2018-01-07')
       --Update tblContractor SET MaxAmount = 2000,baseamount = 1600


Select * from #Temp1 ORDER BY [RctiWeekEndingDate]

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptSubsidy_Test]
	TO [ReportUser]
GO
