SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Rpt_CoupONSalesData] 
   (
   @FromDate Datetime,
   @ToDate Datetime
   )
AS
BEGIN
  --'=====================================================================
    --' [sp_Rpt_CouponSalesData] '2017-06-01','2017-06-30'
    --' ---------------------------
    --' Purpose: sp_RptCoupONSalesData-----
    --' Developer: Sinshith
    --' Date: 06/07/2017
    --' Copyright:  Couriers PleASe Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     ReASON                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --'=====================================================================
--Drop Table #Temp3

Select 
		SerialNumber,
		ContractNumber,
		StockCode,
		AccountCode,
		SerialWhseCode,
		SerialInvoicedDate,
		SerialInvoiceAmount,
		SerialOriginalInvNumber 
into    #Temp1
from    [PrONto].dbo.prONtostockserialnumber 
where 
		SerialStatus=90 and 
		SerialInvoicedDate 
		between @FromDate and @ToDate

Select 
distinct
		LastSoldReference ,
		SerialInvoicedDate ,
		SerialOriginalInvNumber 
		,[AccountCode]
		,StockCode  
		,StartSerialNumber
		,SerialWhseCode 
		,Isnull(InsuranceCategory,'') as InsuranceCategory
		,RevenueAmount 
into #temp2
from 
        #Temp1 T Join [PrONto].dbo.[PrONtoCoupONDetails] C 
        ON T.SerialNumber = C.SerialNumber
 

Select 
		  LastSoldReference AS [Order No],
		  SerialInvoicedDate AS [Invoice Date],
		  SerialOriginalInvNumber AS [Invoice No],
		  D.AccountCode AS [Customer No],
		  E.Shortname AS [Customer Name],
		  D.StockCode AS [Item Code],
		  F.StkDescriptiON AS [Item DescriptiON],
		  StartSerialNumber AS [Serial No from],
		  (StartSerialNumber)+ Convert(bigint,F.StkCONversiONFactor)-1 AS [Serial No To],
		  Convert(bigint,F.StkCONversiONFactor) AS Quantity,
		  SerialWhseCode AS [Warehouse],
		  F.StkReplacementCost AS [Replacement cost],
		  convert(bigint,F.StkCONversiONFactor)* CASE InsuranceCategory WHEN 'INS2' THEN 2.0 WHEN 'INS3' THEN 3 ELSE 0 END as InsuranceAMount,
		  RevenueAmount AS [Price sold per unit],
		  convert(bigint,F.StkCONversiONFactor)*F.StkReplacementCost  as [Coupon Selling Price],
		  convert(decimal(12,2),0) AS [Total Price Excluding GST],
		  convert(decimal(12,2),0) AS [GST],
		  convert(decimal(12,2),0) AS  [Total Price Including GST],
		  convert(decimal(12,2),0) AS  [TotalDiscount]
into #Temp3
	  from #temp2 D
inner join  
      [PrONto].dbo.prONtodebtor E ON (D.AccountCode =E.AccountCode)
join 
      [PrONto].dbo.prONtostockMASter F ON (D.stockcode =F.stockcode)

       update #Temp3 
       set   [Total Price Excluding GST] = ol.GrossAmount,GST= ol.GSTAmount,[Total Price Including GST] =ol.NetAmount 
       from #Temp3 T Join  Pronto.dbo.[ProntoStockSerialLink]  L on T.[Serial No from]=L.SerialNumber
       join Pronto.dbo.ProntoSalesOrderLines ol (NOLOCK)	ON L.SerialLinkCode = ol.OrderNumber and L.SerialLinkSeqNo = ol.LineSequence
	   
	   Update #Temp3 
	   set [TotalDiscount]  = [Coupon Selling Price]- ([Total Price Excluding GST]- InsuranceAMount)

	  Select * from #Temp3

		 END
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_CoupONSalesData]
	TO [ReportUser]
GO
