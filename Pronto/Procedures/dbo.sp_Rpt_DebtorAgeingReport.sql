SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create procedure sp_Rpt_DebtorAgeingReport as
begin

declare @month nvarchar(50)
declare @month1 nvarchar(50)
declare @month2 nvarchar(50)
declare @month3 nvarchar(50)
declare @month4 nvarchar(50)
declare @month5 nvarchar(50)
declare @month6 nvarchar(50)
declare @month7 nvarchar(50)
declare @month8 nvarchar(50)
declare @month9 nvarchar(50)
declare @month10 nvarchar(50)
declare @month11 nvarchar(50)
declare @month12 nvarchar(50)
declare @sql nvarchar(1000)

set @month =datename(Month,getdate())+right(year(getdate()),2)
set @month1 =datename(Month,dateadd(month,-1,getdate()))+right(year(dateadd(month,-1,getdate())),2)
set @month2 =datename(Month,dateadd(month,-2,getdate()))+right(year(dateadd(month,-2,getdate())),2)
set @month3 =datename(Month,dateadd(month,-3,getdate()))+right(year(dateadd(month,-3,getdate())),2)
set @month4 =datename(Month,dateadd(month,-4,getdate()))+right(year(dateadd(month,-4,getdate())),2)
set @month5 =datename(Month,dateadd(month,-5,getdate()))+right(year(dateadd(month,-5,getdate())),2)
set @month6 =datename(Month,dateadd(month,-6,getdate()))+right(year(dateadd(month,-6,getdate())),2)
set @month7 =datename(Month,dateadd(month,-7,getdate()))+right(year(dateadd(month,-7,getdate())),2)
set @month8 =datename(Month,dateadd(month,-8,getdate()))+right(year(dateadd(month,-8,getdate())),2)
set @month9 =datename(Month,dateadd(month,-9,getdate()))+right(year(dateadd(month,-9,getdate())),2)
set @month10 =datename(Month,dateadd(month,-10,getdate()))+right(year(dateadd(month,-10,getdate())),2)
set @month11 =datename(Month,dateadd(month,-11,getdate()))+right(year(dateadd(month,-11,getdate())),2)
set @month12 =datename(Month,dateadd(month,-12,getdate()))+right(year(dateadd(month,-12,getdate())),2)

set @sql=N'select DfAcountCode,
       Territory,
	   RepCode,
	   RepName,
	   ShortNAme as AccountName,
	   DfAgedBalance1+DfAgedBalance2+DfAgedBalance3+DfAgedBalance4+DfAgedBalance5+DfAgedBalance6+DfAgedBalance7+DfAgedBalance8+DfAgedBalance9+DfAgedBalance10+DfAgedBalance11+DfAgedBalance12+DfAgedBalance13 as CurrentBalance,
	   DfAgedBalance1 as '+ @Month+
	   ', DfAgedBalance2 as '+ @Month1+
	   ', DfAgedBalance3 as '+ @Month2+
	   ', DfAgedBalance4 as '+ @Month3+
	   ', DfAgedBalance5 as '+ @Month4+
	   ', DfAgedBalance6 as '+ @Month5+
	   ', DfAgedBalance7 as '+ @Month6+
	   ', DfAgedBalance8 as '+ @Month7+
	   ', DfAgedBalance9 as '+ @Month8+
	   ', DfAgedBalance10 as '+ @Month9+
	   ', DfAgedBalance11 as '+ @Month10+
	   ', DfAgedBalance12 as '+ @Month11+
	   ', DfAgedBalance13 as '+ @Month12+
' from ProntoDebtorAgeing a left join ProntoDebtor d on d.accountcode=a.DfAcountCode
where DfReportDate=''01''+''-''+datename(Month,getdate())+''-''+''2015'''



exec sp_executesql @sql

end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_DebtorAgeingReport]
	TO [ReportUser]
GO
