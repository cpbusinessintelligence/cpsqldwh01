SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure sp_Rpt_EDISalesbyMonthForParentAccounts(@Year varchar(10),@Month varchar(10)) as
begin

---Prepaid----

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_EDISalesbyMonthForParentAccounts]
    --' ---------------------------
    --' Purpose: Get month wise sales for the last 6 months for EDI-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 04 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                    Bookmark
    --' ----          ---     ---     -----                                                                      -------
    --' 04/12/2014    AB      1.00    Created the procedure                                                      --AB20141204
    --'=====================================================================

------EDI----------------

select distinct 
                BillTo,
				Territory,
				Repname,
				OriginalRepname
				into #temp10
				from ProntoDebtor where territory not like 'x%' and accountcode=billto

select datepart(yyyy,consignmentdate)*100+datepart(mm,consignmentdate) as Month,
       p.AccountBillToCode,
       Territory,
	   RevenueBusinessUnit,
       case when RevenueBusinessUnit like 'CSY' or RevenueBusinessUnit like 'CCC' or RevenueBusinessUnit like 'CCB' then 'NSW' 
            when RevenueBusinessUnit like 'COO' or RevenueBusinessUnit like 'CSC' or RevenueBusinessUnit like 'CBN' then 'QLD'
            when RevenueBusinessUnit like 'CME'  then 'VIC'
            when RevenueBusinessUnit like 'CPE'  then 'WA'
            when RevenueBusinessUnit like 'CAD'  then 'SA'
        else 'Unknown' end as State,
		Repname,
        OriginalRepname,
       sum(BilledTotal) as BilledAmount
 into #temp12
from  ProntoBilling p join #temp10 t on p.[AccountBillToCode]=t.BillTo
where datepart(yyyy,consignmentdate)*100+datepart(mm,consignmentdate)>=@Year+right('00'+@Month,2) and datepart(yyyy,consignmentdate)*100+datepart(mm,consignmentdate)<=datepart(yyyy,dateadd(mm,-1,getdate()))*100+datepart(mm,dateadd(mm,-1,getdate()))
group by  datepart(yyyy,consignmentdate)*100+datepart(mm,consignmentdate) ,
 p.AccountBillToCode,
Territory,
RevenueBusinessUnit,
case when RevenueBusinessUnit like 'CSY' or RevenueBusinessUnit like 'CCC' or RevenueBusinessUnit like 'CCB' then 'NSW' 
when RevenueBusinessUnit like 'COO' or RevenueBusinessUnit like 'CSC' or RevenueBusinessUnit like 'CBN' then 'QLD'
when RevenueBusinessUnit like 'CME'  then 'VIC'
when RevenueBusinessUnit like 'CPE'  then 'WA'
when RevenueBusinessUnit like 'CAD'  then 'SA'
else 'Unknown' end,
Repname,
OriginalRepname



select t.AccountBillToCode,
       max(ConsignmentDate) as LastdayTrade,
       min(ConsignmentDate) as DateStartedTrade 
into #temp14 from #temp12 t join ProntoBilling p on t.AccountBillToCode=p.AccountBillToCode
group by t.AccountBillToCode


select t.Month,
       t.AccountBillToCode,
       t.Territory,
       t.RevenueBusinessUnit,
       t.state,
       t.Repname,
       OriginalRepname,
       BilledAmount,
       LastdayTrade,
       DateStartedTrade
into #temp16 from #temp12 t join #temp14 t1 on t.AccountBillToCode=t1.AccountBillToCode


select t.Month,
       t.AccountBillToCode,
	   p.shortname as AccountName,
	   t.Territory,
	   t.RevenueBusinessUnit,
	   t.state,
	   t.Repname,
	   t.OriginalRepname,
	   BilledAmount,
	   LastdayTrade,
	   DateStartedTrade from #temp16 t join ProntoDebtor p on t.AccountBillTocode=p.BillTo 
where p.BillTo=p.Accountcode
order by month,t.AccountBillToCode

end
GO
