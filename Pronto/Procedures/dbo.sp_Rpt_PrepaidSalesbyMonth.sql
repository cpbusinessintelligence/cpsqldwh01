SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure sp_Rpt_PrepaidSalesbyMonth(@Year varchar(10),@Month varchar(10)) as
begin

---Prepaid----

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_PrepaidSalesbyMonth]
    --' ---------------------------
    --' Purpose: Get month wise sales for the last 6 months-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 04 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                    Bookmark
    --' ----          ---     ---     -----                                                                      -------
    --' 04/12/2014    AB      1.00    Created the procedure                                                      --AB20141204
    --'=====================================================================

select distinct Accountcode,
                BillTo,
				Repname,
				OriginalRepname
				into #temp 
				from ProntoDebtor where territory like 'x%'

		
select datepart(yyyy,Accountingdate)*100+datepart(mm,Accountingdate) as Month,
CustomerCode,
Territorycode,
case when Territorycode like 'XSY' or Territorycode like 'XCC' or Territorycode like 'XCB' then 'NSW' 
when Territorycode like 'XOO' or Territorycode like 'XSC' or Territorycode like 'XBN' then 'QLD'
when Territorycode like 'XME'  then 'VIC'
when Territorycode like 'XPE'  then 'WA'
when Territorycode like 'XAD'  then 'SA'
else 'Unknown' end as State,
 Repname,
 OriginalRepname,
 sum(GrossAmount) as GrossAmount ,
 sum(NetAmount) NetAmount,
 sum(GSTAmount) as GSTAmount
 into #temp1 
 from  ProntosalesOrder p join #temp t on p.CustomerCode=t.Accountcode 
 where datepart(yyyy,Accountingdate)*100+datepart(mm,Accountingdate)>=@Year+right('00'+@Month,2) and datepart(yyyy,Accountingdate)*100+datepart(mm,Accountingdate)<=datepart(yyyy,dateadd(mm,-1,getdate()))*100+datepart(mm,dateadd(mm,-1,getdate()))
and territorycode like 'x%' and companycode='CL1' 
group by  datepart(yyyy,Accountingdate)*100+datepart(mm,Accountingdate) ,
          CustomerCode,
          Territorycode,
          case when Territorycode like 'XSY' or Territorycode like 'XCC' or Territorycode like 'XCB' then 'NSW' 
               when Territorycode like 'XOO' or Territorycode like 'XSC' or Territorycode like 'XBN' then 'QLD'
               when Territorycode like 'XME'  then 'VIC'
               when Territorycode like 'XPE'  then 'WA'
               when Territorycode like 'XAD'  then 'SA'
           else 'Unknown' end,
Repname,
OriginalRepname

select t.Customercode,
      max(Accountingdate) as LastdayTrade,
	  min(Accountingdate) as DateStartedTrade 
into #temp2 from #temp1 t join ProntosalesOrder p on t.customercode=p.customercode
group by t.Customercode

select t.Month,
       t.CustomerCode,
	   t.Territorycode,
	   t.state,
	   t.Repname,
	   OriginalRepname,
	   GrossAmount,
	   NetAmount,
	   GSTAmount,
	   LastdayTrade,
	   DateStartedTrade
into #temp3 from #temp1 t join #temp2 t1 on t.customercode=t1.customercode

select t.Month,
       t.CustomerCode,
	   p.Shortname as AccountName,
	   t.Territorycode,
	   t.state,
	   t.Repname,
	   t.OriginalRepname,
	   GrossAmount,
	   NetAmount,
	   GSTAmount,
	   LastdayTrade,
	   DateStartedTrade from #temp3 t join ProntoDebtor p on t.Customercode=p.Accountcode order by month,customercode

	   end
GO
