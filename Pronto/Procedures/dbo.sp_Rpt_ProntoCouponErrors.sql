SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_Rpt_ProntoCouponErrors]
  (@Branch Varchar(20))
AS
BEGIN

       --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_ProntoCouponErrors] 
    --' ---------------------------
    --' Purpose: Pronto Coupon Errors Report-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 06 Nov 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 06/11/2014    AB      1.00    Created the procedure                             --AB20141106

    --'=====================================================================


   SELECT  CASE WHEN [Contractor] like 'A%' THEN 'ADELAIDE' WHEN [Contractor] like 'B%' THEN 'BRISBANE' WHEN [Contractor] like 'S%' OR [Contractor] like 'C%'  THEN 'SYDNEY' WHEN [Contractor] like 'M%' THEN 'MELBOURNE' WHEN [Contractor] like 'G%' THEN  'GOLD COAST' WHEN [Contractor] like 'P%' THEN 'PERTH' ELSE 'UNKNOWN' END as Branch
          ,[Contractor]
          ,[ActivityDateTime]
          ,DATEDIFF(day,[ActivityDateTime],GETDATE()) as duration
          ,[SerialNumber]
          ,CONVERT(Varchar(50),'') as PrimaryCPNReason
          ,CONVERT(Varchar(50),'') as PrimaryCPNBookStartNumber
          ,[LinkCouponNumber]
          ,CONVERT(Varchar(50),'') as LinkCPNReason
          ,CONVERT(Varchar(50),'') as LinkCPNBookStartNumber
          ,[ReturnTracker]
          ,CONVERT(Varchar(50),'') as ReturnCPNReason
          ,[AccountNumber]  
          ,CONVERT(Varchar(50),'') as AccountName     
          ,[ActivityType]
          ,CASE [ConsignmentType] WHEN 'RP' THEN 'Return' WHEN 'CP' THEN 'Prepaid' WHEN 'CL1' THEN 'Consignment' ELse [ConsignmentType] End as [Type]
          ,[FileName]
		  ,1 as count

  INTO #Temp1
  FROM [Pronto].[dbo].[ProntoCPNImportErrors]
  
  Update #Temp1 SET AccountName=D.ShortName from #Temp1 T Left Join Pronto.dbo.ProntoDebtor D on T.[AccountNumber] = D.Accountcode Where AccountNumber <>''
  
  Update #Temp1 SET PrimaryCPNReason = 'Invalid Coupons' 
          WHere (LEN(Rtrim(Ltrim([SerialNumber])))<>11  or ISNUMERIC([SerialNumber]) = 0 )  and [SerialNumber] <>'' and [TYPE] <> 'Consignment'
  Update #Temp1 SET LinkCPNReason = 'Invalid Coupons'
          WHere (LEN(Rtrim(Ltrim([LinkCouponNumber])))<>11  or ISNUMERIC([LinkCouponNumber]) = 0 )  and [LinkCouponNumber] <>'' and [TYPE] <> 'Consignment'
  Update #Temp1 SET ReturnCPNReason = 'Invalid Coupons' 
          WHere (LEN(Rtrim(Ltrim([ReturnTracker])))<>11  or ISNUMERIC([ReturnTracker]) = 0 )  and [ReturnTracker] <>'' and [TYPE] <> 'Consignment'
  
  Update #Temp1 SET PrimaryCPNReason = 'Invalid Coupons'
          WHere Pronto.dbo.CPPL_fn_GetCouponTypeFromPrefix(Left([SerialNumber],3)) is null and [SerialNumber] <>'' and [TYPE] <> 'Consignment' and PrimaryCPNReason = ''
 
 
 
  Update #Temp1 SET  PrimaryCPNReason = 'Conversi Coupons' 
        From #Temp1 T Join Pronto.dbo.ProntoStockSerialNumber C on T.SerialNumber = C.SerialNumber and C.SerialOriginalInvNumber = 'CONVERSI' WHERE T.PrimaryCPNReason = ''and T.[SerialNumber] <>'' and T.[TYPE] <> 'Consignment'

  Update #Temp1 SET  LinkCPNReason = 'Conversi Coupons' 
        From #Temp1 T Join Pronto.dbo.ProntoStockSerialNumber C on T.[LinkCouponNumber] = C.SerialNumber and C.SerialOriginalInvNumber = 'CONVERSI' WHERE T.LinkCPNReason = ''and T.[LinkCouponNumber] <>'' and T.[TYPE] <> 'Consignment'
 
  Update #Temp1 SET  ReturnCPNReason = 'Conversi Coupons' 
        From #Temp1 T Join Pronto.dbo.ProntoStockSerialNumber C on T.[ReturnTracker] = C.SerialNumber and C.SerialOriginalInvNumber = 'CONVERSI' WHERE T.ReturnCPNReason = ''and T.[ReturnTracker] <>'' and T.[TYPE] <> 'Consignment'

 
  Update #Temp1 SET  PrimaryCPNReason = 'Coupons Dont Exist'  
        From #Temp1 T Left Join Pronto.dbo.ProntoStockSerialNumber C on T.SerialNumber = C.SerialNumber WHERE T.PrimaryCPNReason = '' and  C.SerialNumber  is null and T.[SerialNumber] <>'' and [TYPE] <> 'Consignment' 

 Update #Temp1 SET  LinkCPNReason = 'Coupons Dont Exist'  
        From #Temp1 T Left Join Pronto.dbo.ProntoStockSerialNumber C on T.[LinkCouponNumber] = C.SerialNumber WHERE T.LinkCPNReason = '' and  C.SerialNumber  is null and T.[LinkCouponNumber] <>'' and [TYPE] <> 'Consignment' 

 Update #Temp1 SET  ReturnCPNReason = 'Coupons Dont Exist'  
        From #Temp1 T Left Join Pronto.dbo.ProntoStockSerialNumber C on T.[ReturnTracker] = C.SerialNumber WHERE T.[ReturnTracker] = '' and  C.SerialNumber  is null and T.[ReturnTracker] <>'' and [TYPE] <> 'Consignment' 

 
  Update #Temp1 SET  PrimaryCPNReason = 'Coupons Not Sold'   
        From #Temp1 T Left Join Pronto.dbo.ProntoStockSerialLink C on T.SerialNumber = C.SerialNumber and c.SerialLinkType = 'S' WHERE T.PrimaryCPNReason = '' and  C.SerialNumber  is null and T.[SerialNumber] <>'' and [TYPE] <> 'Consignment' 

  Update #Temp1 SET  LinkCPNReason = 'Coupons Not Sold'   
        From #Temp1 T Left Join Pronto.dbo.ProntoStockSerialLink C on T.[LinkCouponNumber] = C.SerialNumber and c.SerialLinkType = 'S' WHERE T.LinkCPNReason = '' and  C.SerialNumber  is null and T.[LinkCouponNumber] <>'' and [TYPE] <> 'Consignment' 
        
  Update #Temp1 SET PrimaryCPNReason = 'Billing Record Not Found'  From #Temp1 T Left join Pronto.dbo.ProntoBilling B  on T.SerialNumber = B.ConsignmentReference  Where B.ConsignmentReference is null  and    T.PrimaryCPNReason = '' and [TYPE] = 'Consignment' 
  Update #Temp1 SET PrimaryCPNReason = 'Invalid Contractor'  From #Temp1 Where    PrimaryCPNReason = '' and ( LEn(RTRIM(Ltrim(contractor))) <>4 OR Left(RTRIM(Ltrim(contractor)),1) not in ('A','B','G','M','S','P','C'))

  Update #Temp1 SET  PrimaryCPNBookStartNumber = C.StartSerialNumber  From #Temp1 T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber  Where T.SerialNumber <>''
  Update #Temp1 SET  LinkCPNBookStartNumber = C.StartSerialNumber  From #Temp1 T Join Pronto.dbo.ProntoCouponDetails C on T.[LinkCouponNumber] = C.SerialNumber  Where T.[LinkCouponNumber] <>''
 
  update #Temp1  SET PrimaryCPNReason ='Coupons Cancelled' from  #Temp1 T Join Pronto.dbo.ProntoStockSerialNumber C on T.SerialNumber = C.SerialNumber and C.SerialStatus = 90 WHERE T.PrimaryCPNReason = ''and T.[SerialNumber] <>'' and T.[TYPE] <> 'Consignment'
  update #Temp1  SET LinkCPNReason ='Coupons Cancelled' from  #Temp1 T Join Pronto.dbo.ProntoStockSerialNumber C on T.[LinkCouponNumber] = C.SerialNumber and C.SerialStatus = 90 WHERE  T.LinkCPNReason = '' and  T.[LinkCouponNumber] <>'' and [TYPE] <> 'Consignment' 
 

--Update #Temp1 SET PrimaryCPNReason = 'Identical Serial Number Found' where PrimaryCPNReason = 'Billing Record Not Found' and #temp1.count< (select count(*) from #temp1 t2 where #Temp1.serialnumber=t2.serialnumber)



 IF @Branch = 'ALL'
      Select * from #Temp1 order by 4 desc
 ELSE 
    Select  * from #Temp1 WHERE Branch = @Branch  order by 4 desc
  
  SET NOCOUNT OFF;

END

GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_ProntoCouponErrors]
	TO [ReportUser]
GO
