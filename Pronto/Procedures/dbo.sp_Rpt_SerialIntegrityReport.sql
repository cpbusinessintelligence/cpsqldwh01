SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[sp_Rpt_SerialIntegrityReport]
  (@Branch Varchar(20))
AS
BEGIN

       --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_SerialIntegrityReport] 
    --' ---------------------------
    --' Purpose: Serial Integrity Report-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 06 Nov 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 06/11/2014    AB      1.00    Created the procedure                             --AB20141106

    --'=====================================================================



 
SELECT  I.[SerialNumber]
       ,I.[Contractor]
       ,I.[CurrentStatus]
       ,I.[Error]
       ,I.[Action]
       ,I.[NewStatus]
       ,S.SerialInvoiceNumber
       ,S.SerialWhseCode
       ,S.SerialStatus 
  INTO #Temp1     
  FROM [Pronto].[dbo].[ProntoSerialIntegrity] I Join Pronto.dbo.ProntoStockSerialNumber S ON I.SerialNumber = S.SerialNumber
  WHERE S.SerialOriginalInvNumber <> 'CONVERSI'
 
 if @Branch = 'ALL' 
   Select  T.[SerialNumber]
       ,C.StartSerialNumber 
       ,T.[Contractor]
       ,T.[CurrentStatus]
       ,T.[Error]
       ,T.[Action]
       ,T.[NewStatus]
       ,T.SerialInvoiceNumber
       ,T.SerialWhseCode
       ,T.SerialStatus 
      FROM #Temp1  T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
  ELSE
     Select  T.[SerialNumber]
       ,C.StartSerialNumber 
       ,T.[Contractor]
       ,T.[CurrentStatus]
       ,T.[Error]
       ,T.[Action]
       ,T.[NewStatus]
       ,T.SerialInvoiceNumber
       ,T.SerialWhseCode
       ,T.SerialStatus 
      FROM #Temp1  T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
      WHERE T.SerialWhseCode = CASE WHEN @Branch = 'SYDNEY' THEN 'CSY'
                                    WHEN @Branch = 'MELBOURNE' THEN   'CME'
                                    WHEN @Branch = 'ADELAIDE' THEN   'CAD'
                                    WHEN @Branch = 'PERTH' THEN      'CPE'
                                    WHEN @Branch = 'BRISBANE' THEN   'CBN'
                                    WHEN @Branch = 'GOLD COAST' THEN   'COO'
                                    ELSE '' END
  UNION ALL
     Select  T.[SerialNumber]
       ,C.StartSerialNumber 
       ,T.[Contractor]
       ,T.[CurrentStatus]
       ,T.[Error]
       ,T.[Action]
       ,T.[NewStatus]
       ,T.SerialInvoiceNumber
       ,T.SerialWhseCode
       ,T.SerialStatus 
      FROM #Temp1  T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
      WHERE T.SerialWhseCode Like CASE WHEN @Branch = 'SYDNEY' THEN  'S%' 
                 WHEN @Branch = 'MELBOURNE' THEN  'M%' 
                 WHEN @Branch = 'ADELAIDE' THEN   'A%' 
                 WHEN @Branch = 'PERTH' THEN      'P%' 
                 WHEN @Branch = 'BRISBANE' THEN   'B%' 
                 WHEN @Branch = 'GOLD COAST' THEN 'G%' 
                 ELSE  'Z%' END
  
  SET NOCOUNT OFF;

END


GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_SerialIntegrityReport]
	TO [ReportUser]
GO
