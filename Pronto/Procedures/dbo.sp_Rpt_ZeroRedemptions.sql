SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_Rpt_ZeroRedemptions](@StartDate Date,@EndDate Date) as
begin

---Prepaid----

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_PrepaidSalesbyMonth]
    --' ---------------------------
    --' Purpose: To get the Zero Redemptions-----
    --' Developer: Tejes
    --' Date: 04 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                   
    --' ----          ---     ---     -----                                                                      
    --' 07/12/2017    TS      1.00    Created the procedure                                                    
    --'=====================================================================

select * into #Temp from (SELECT [SerialNumber] as ConsignmnetNumber
      ,[AccountCode]
	  ,[AccountName]
	  ,[ServiceCode]
	  ,[OriginLocality] as PickupSuburb
	  ,[OriginPostcode] as PickupPostcode
	  ,[DestinationLocality] as DeliverySuburb
	  ,[DestinationPostcode] as DeliveryPostcode
	  ,'P' as [PUP/DEL]
      ,[PickupContractor] as ProntoID
      ,[PickupRctiAmount] as RCTIAmount
	  ,[PickupRctiDate] as RCTIDate
  FROM [Pronto].[dbo].[ProntoCouponDetails] C (NOLOCK)
  JOIN [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
  ON C.SerialNumber=P.[ConsignmentReference]
  Where [PickupRctiAmount]=0 and [PickupContractor] IS NOT NULL and [PickupRctiDate] IS NOT NULL and [PickupRctiDate]>=@StartDate and 
        [PickupRctiDate] <= @EndDate
  UNION ALL
  SELECT [SerialNumber] as ConsignmnetNumber
      ,[AccountCode]
	  ,[AccountName]
	  ,[ServiceCode]
	  ,[OriginLocality] as PickupSuburb
	  ,[OriginPostcode] as PickupPostcode
	  ,[DestinationLocality] as DeliverySuburb
	  ,[DestinationPostcode] as DeliveryPostcode
	  ,'D' as [PUP/DEL]
      ,[DeliveryContractor] as ProntoID
      ,[DeliveryRctiAmount] as RCTIAmount
	  ,[DeliveryRctiDate] as RCTIDate
  FROM [Pronto].[dbo].[ProntoCouponDetails] C (NOLOCK)
  JOIN [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
  ON C.SerialNumber=P.[ConsignmentReference]
  Where [DeliveryRctiAmount]=0 and [DeliveryContractor] IS NOT NULL and [DeliveryRctiDate] IS NOT NULL and [DeliveryRctiDate]>=@StartDate and 
        [DeliveryRctiDate] <= @EndDate)T


Select * from #Temp ORDER BY RCTIDate


end
GO
