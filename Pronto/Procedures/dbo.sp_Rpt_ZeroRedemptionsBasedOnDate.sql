SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_Rpt_ZeroRedemptionsBasedOnDate](@StartDate Date,@EndDate Date) as
begin

---Prepaid----

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_ZeroRedemptionsBasedOnDate]
    --' ---------------------------
    --' Purpose: To get the Zero Redemptions-----
    --' Developer: Tejes
    --' Date: 04 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                   
    --' ----          ---     ---     -----                                                                      
    --' 07/12/2017    TS      1.00    Created the procedure                                                    
    --'=====================================================================

SET NOCOUNT ON;

select * into #Temp from (SELECT [SerialNumber] as ConsignmnetNumber
      ,[AccountCode]
	  ,[AccountName]
	  ,[ServiceCode]
	  ,[OriginLocality] as PickupSuburb
	  ,[OriginPostcode] as PickupPostcode
	  ,[RevenueOriginZone] 
	  ,[DestinationLocality] as DeliverySuburb
	  ,[DestinationPostcode] as DeliveryPostcode
	  ,[RevenueDestinationZone]
	  ,[ChargeableWeight]
	  ,'P' as [PUP/DEL]
	  ,[ItemQuantity]
      ,[PickupContractor] as ProntoID
      ,[PickupRctiAmount] as RCTIAmount
	  ,convert(date,[PickupRctiDate]) as RCTIDate
  FROM [Pronto].[dbo].[ProntoCouponDetails] C (NOLOCK)
  JOIN [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
  ON C.SerialNumber=P.[ConsignmentReference]
  Where [PickupRctiAmount]=0 and [PickupContractor] IS NOT NULL and [PickupRctiDate] IS NOT NULL and [PickupRctiDate]>=@StartDate and 
        [PickupRctiDate] <= @EndDate and [ServiceCode] NOT IN ('55','CTY','DS0','DS1','DS3','DS5','NPU','X53')
  UNION ALL
  SELECT [SerialNumber] as ConsignmnetNumber
      ,[AccountCode]
	  ,[AccountName]
	  ,[ServiceCode]
	  ,[OriginLocality] as PickupSuburb
	  ,[OriginPostcode] as PickupPostcode
	  ,[RevenueOriginZone] 
	  ,[DestinationLocality] as DeliverySuburb
	  ,[DestinationPostcode] as DeliveryPostcode
	  ,[RevenueDestinationZone]
	  ,[ChargeableWeight]
	  ,'D' as [PUP/DEL]
	  ,[ItemQuantity]
      ,[DeliveryContractor] as ProntoID
      ,[DeliveryRctiAmount] as RCTIAmount
	  ,convert(date,[DeliveryRctiDate]) as RCTIDate
  FROM [Pronto].[dbo].[ProntoCouponDetails] C (NOLOCK)
  JOIN [Pronto].[dbo].[ProntoBilling] P (NOLOCK)
  ON C.SerialNumber=P.[ConsignmentReference]
  Where [DeliveryRctiAmount]=0 and [DeliveryContractor] IS NOT NULL and [DeliveryRctiDate] IS NOT NULL and [DeliveryRctiDate]>=@StartDate and 
        [DeliveryRctiDate] <=@EndDate  and ([ServiceCode] not like 'EXP%' and [ServiceCode] not like 'SAV%' and [ServiceCode] not in ('NDL','UPS')))T

   Select * from #Temp

	SET NOCOUNT OFF;
end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_ZeroRedemptionsBasedOnDate]
	TO [ReportUser]
GO
