SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_couponsalesreport_weekly
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	

	Declare @Date DATEtime
	set @Date= getdate()

SELECT  s.[OrderNumber]      
 ,sl.LineSequence [Order linesequence]   
      ,[CustomerCode]
         ,d.Shortname [Customer Name]
      ,[WarehouseCode] 
         , e.Shortname as Franchisename
      ,[TerritoryCode]
         , case [TerritoryCode] WHEN 'XSY' THEN 'Sydney' when 'XME' THEN 'Melbourne' 
								WHEN 'XAD' THEN	'Adelaide'
								WHEN 'XBN' THEN 'Brisbane'
								WHEN 'XCB' THEN	'Canberra'
								WHEN 'XCC' THEN 'Central coast'
								WHEN 'XCF' THEN  'Coffs Harbour'
								WHEN 'XNL' THEN 'Newcastle'
								WHEN 'XOO' THEN 'Goldcoast'
								WHEN 'XPE' THEN 'Perth'
								WHEN 'XSC' THEN 'Sunshine Coast'

		 else [TerritoryCode] End as TerritoryName
      ,[AccountingDate]
      ,[AccountingPeriod]
      ,sl.[NetAmount]
      ,sl.[GSTAmount]
      ,sl.[GrossAmount]
      ,sl.[CostAmount]
      ,sl.quantity*st.StkConversionFactor [couponcount]

  FROM [Pronto].[dbo].[ProntoSalesOrder] s (nolock)
  inner join [Pronto].[dbo].[ProntoSalesOrderLines] sl (nolock) on s.OrderNumber=sl.OrderNumber
  inner join [Pronto].[dbo].[ProntoStockMaster] st on st.StockCode=sl.StockCode
  left join [Pronto].dbo.ProntoDebtor d on d.accountcode=s.CustomerCode
  left join [Pronto].dbo.ProntoDebtor e on s.[WarehouseCode]=e.accountcode
  where AccountingDate between   dateadd(DAY,-8,@Date) and dateadd(DAY,-1,@Date)
  
  order by OrderNumber


END
GO
GRANT EXECUTE
	ON [dbo].[sp_couponsalesreport_weekly]
	TO [ReportUser]
GO
