SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_drpinvcommas] as
	begin
	update [dbo].[ProntoCPNImportErrors_Temp] set [RecordType]= replace([RecordType],'"','')
    update [dbo].[ProntoCPNImportErrors_Temp] set [Contractor]= replace([Contractor],'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [SerialNumber]= replace([SerialNumber],'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [LinkCouponNumber] = replace([LinkCouponNumber] ,'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [ReturnTracker]= replace([ReturnTracker],'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [AccountNumber]= replace([AccountNumber],'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [PhoneNumber]= replace([PhoneNumber],'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set Invoice= replace(Invoice,'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [ActivityType]= replace([ActivityType],'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [ConsignmentType]= replace([ConsignmentType],'"','')

	update [dbo].[ProntoCPNImportErrors_Temp] set [InsuranceCode]= replace([InsuranceCode],'"','')

	end


GO
GRANT EXECUTE
	ON [dbo].[sp_drpinvcommas]
	TO [SSISUser]
GO
