SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_trim] as
begin
update [dbo].[ProntoCPNImportErrors_Temp] set [RecordType]=LTRIM(RTRIM([RecordType]))

update [dbo].[ProntoCPNImportErrors_Temp] set [Contractor]=LTRIM(RTRIM([Contractor]))

update [dbo].[ProntoCPNImportErrors_Temp] set [ActivityDateTime]=LTRIM(RTRIM([ActivityDateTime]))

update [dbo].[ProntoCPNImportErrors_Temp] set [SerialNumber]=LTRIM(RTRIM([SerialNumber]))

update [dbo].[ProntoCPNImportErrors_Temp] set [LinkCouponNumber]=LTRIM(RTRIM([LinkCouponNumber]))

update [dbo].[ProntoCPNImportErrors_Temp] set [ReturnTracker]=LTRIM(RTRIM([ReturnTracker]))

update [dbo].[ProntoCPNImportErrors_Temp] set [AccountNumber]=LTRIM(RTRIM([AccountNumber]))

update [dbo].[ProntoCPNImportErrors_Temp] set   [PhoneNumber]=LTRIM(RTRIM([PhoneNumber]))

update [dbo].[ProntoCPNImportErrors_Temp] set [Invoice]=LTRIM(RTRIM([Invoice]))

update [dbo].[ProntoCPNImportErrors_Temp] set [ActivityType]=LTRIM(RTRIM([ActivityType]))

update [dbo].[ProntoCPNImportErrors_Temp] set [ConsignmentType]=LTRIM(RTRIM([ConsignmentType]))

update [dbo].[ProntoCPNImportErrors_Temp] set [InsuranceCode]=LTRIM(RTRIM([InsuranceCode]))

update [dbo].[ProntoCPNImportErrors_Temp] set [InsuranceValue]=LTRIM(RTRIM([InsuranceValue]))

update [dbo].[ProntoCPNImportErrors_Temp] set [FileName]=LTRIM(RTRIM([FileName]))
end


GO
GRANT EXECUTE
	ON [dbo].[sp_trim]
	TO [SSISUser]
GO
