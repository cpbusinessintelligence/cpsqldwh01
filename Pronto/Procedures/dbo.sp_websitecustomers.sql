SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_websitecustomers
	
AS
BEGIN
	select [Accountcode]
      ,[BillTo] [Parent Account]
      ,[Shortname] [Account Name] 
	  ,[Column 2] [Contact name]
     ,[Column 11] [Contact number]
	,'email@insertfromaddressssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss.com' [emailID]
	,[Column 4]+'  '+[Column 5]+'  '+[Column 6] [Address]
	  into #temp
	  from [Pronto].[dbo].[ProntoDebtor] left join [dbo].name_address on Accountcode= [dbo].name_address.[Column 0]  where [Column 1]='C' 

	
	update #temp set [emailID]= [Column 2]+[Column 3] 
  from [dbo].name_address join #temp on Accountcode= [dbo].name_address.[Column 0]
  where [Column 1]='E' 

  update #temp set emailID='' where emailid='email@insertfromaddressssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss.com' 

   select * from #temp
END
GO
GRANT EXECUTE
	ON [dbo].[sp_websitecustomers]
	TO [ReportUser]
GO
