SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure z_tmp_prontopurge
as
begin

	Select serialnumber into #temp1 from z_tmp_Prontopurgedata_pv where Isnull(Isupdatedflag,'') <> 'Y'

	Update t1 Set isupdatedflag ='Y' 
	From z_tmp_Prontopurgedata_pv t1
	Join #temp1 t2 on t1.serialnumber = t2.serialnumber 
	
	Select * from #temp1
end


GO
