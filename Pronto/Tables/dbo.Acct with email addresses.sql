SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Acct with email addresses] (
		[Account]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Name]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Balance]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 5]     [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Acct with email addresses] SET (LOCK_ESCALATION = TABLE)
GO
