SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdjustedVsOriginalRCTISummary] (
		[Prontoid]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[AdjustedRCTI]       [decimal](12, 2) NULL,
		[Origamountpaid]     [decimal](12, 2) NULL
)
GO
ALTER TABLE [dbo].[AdjustedVsOriginalRCTISummary] SET (LOCK_ESCALATION = TABLE)
GO
