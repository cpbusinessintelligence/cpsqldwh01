SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdjustmentsforNoSubsidy] (
		[Consignmentnumber]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RCTIWeekEndingDate]     [datetime] NULL,
		[Scandate]               [datetime] NULL,
		[Scantype]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Prontoid]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RCTIAmount]             [float] NULL,
		[Servicecode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]            [varchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AdjustmentsforNoSubsidy] SET (LOCK_ESCALATION = TABLE)
GO
