SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BranchesOrigPayments] (
		[Driver]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Name]        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[BSB]         [float] NULL,
		[Account]     [float] NULL,
		[EFT]         [float] NULL,
		[Amount]      [float] NULL
)
GO
ALTER TABLE [dbo].[BranchesOrigPayments] SET (LOCK_ESCALATION = TABLE)
GO
