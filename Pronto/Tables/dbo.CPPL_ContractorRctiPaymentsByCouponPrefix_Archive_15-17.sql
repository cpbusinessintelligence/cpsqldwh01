SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CPPL_ContractorRctiPaymentsByCouponPrefix_Archive_15-17] (
		[ContractorId]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[RunNumber]              [smallint] NULL,
		[RunName]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                 [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[RctiWeekEndingDate]     [date] NOT NULL,
		[IsReceiverPays]         [bit] NOT NULL,
		[CouponPrefix]           [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeliveryRctiAmount]     [decimal](13, 2) NOT NULL,
		[DeliveryStdAmount]      [decimal](13, 2) NOT NULL,
		[DeliveryDifference]     [decimal](13, 2) NOT NULL,
		[PickupRctiAmount]       [decimal](13, 2) NOT NULL,
		[PickupStdAmount]        [decimal](13, 2) NOT NULL,
		[PickupDifference]       [decimal](13, 2) NOT NULL,
		[TotalRctiAmount]        [decimal](13, 2) NOT NULL,
		[TotalStdAmount]         [decimal](13, 2) NOT NULL,
		[TotalDifference]        [decimal](13, 2) NOT NULL,
		[Depot]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[AddedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddedDatetime]          [datetime] NULL,
		[EditedBy]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]         [datetime] NULL
)
GO
ALTER TABLE [dbo].[CPPL_ContractorRctiPaymentsByCouponPrefix_Archive_15-17] SET (LOCK_ESCALATION = TABLE)
GO
