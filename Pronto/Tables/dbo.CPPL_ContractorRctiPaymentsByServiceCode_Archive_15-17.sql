SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CPPL_ContractorRctiPaymentsByServiceCode_Archive_15-17] (
		[ContractorId]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[RunNumber]              [smallint] NULL,
		[RunName]                [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                 [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[RctiWeekEndingDate]     [date] NOT NULL,
		[IsReceiverPays]         [bit] NOT NULL,
		[Servicecode]            [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryRctiAmount]     [decimal](13, 2) NOT NULL,
		[DeliveryStdAmount]      [decimal](18, 2) NULL,
		[DeliveryDifference]     [decimal](18, 2) NULL,
		[PickupRctiAmount]       [decimal](13, 2) NOT NULL,
		[PickupStdAmount]        [decimal](18, 2) NULL,
		[PickupDifference]       [decimal](18, 2) NULL,
		[TotalRctiAmount]        [decimal](13, 2) NOT NULL,
		[TotalStdAmount]         [decimal](18, 2) NULL,
		[TotalDifference]        [decimal](18, 2) NULL,
		[Depot]                  [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[AddedBy]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddedDatetime]          [datetime] NULL,
		[EditedBy]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]         [datetime] NULL,
		[PickupCount]            [int] NULL,
		[DeliveryCount]          [int] NULL,
		[Accountcode]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]            [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CPPL_ContractorRctiPaymentsByServiceCode_Archive_15-17] SET (LOCK_ESCALATION = TABLE)
GO
