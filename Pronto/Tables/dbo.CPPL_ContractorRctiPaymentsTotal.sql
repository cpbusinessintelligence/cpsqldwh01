SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CPPL_ContractorRctiPaymentsTotal] (
		[ContractorId]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[RunNumber]                     [smallint] NULL,
		[RunName]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                        [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[RctiWeekEndingDate]            [date] NOT NULL,
		[TotalDevelopmentIncentive]     [decimal](15, 2) NOT NULL,
		[TotalPrepaidRctiAmount]        [decimal](15, 2) NOT NULL,
		[TotalEdiRctiAmount]            [decimal](15, 2) NOT NULL,
		[TotalRctiAllowances]           [decimal](15, 2) NOT NULL,
		[TotalRctiDeductions]           [decimal](15, 2) NOT NULL,
		[TotalRctiRedemptions]          [decimal](15, 2) NOT NULL,
		[Depot]                         [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[TotalDevelopmentBonus]         [decimal](15, 2) NULL,
		CONSTRAINT [PK_CPPL_ContractorRctiPaymentsTotal]
		PRIMARY KEY
		CLUSTERED
		([ContractorId], [RctiWeekEndingDate])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[CPPL_ContractorRctiPaymentsTotal] SET (LOCK_ESCALATION = TABLE)
GO
