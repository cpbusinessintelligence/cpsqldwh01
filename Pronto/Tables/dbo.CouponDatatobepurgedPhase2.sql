SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponDatatobepurgedPhase2] (
		[SerialNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[StartSerialNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LastSoldDate]           [date] NULL,
		[LastSoldReference]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pickupdate]             [date] NULL,
		[DeliveryDate]           [date] NULL,
		[BillingAccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillingService]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Extractionround]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Round]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Round1]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CouponDatatobepurgedPhase2] SET (LOCK_ESCALATION = TABLE)
GO
