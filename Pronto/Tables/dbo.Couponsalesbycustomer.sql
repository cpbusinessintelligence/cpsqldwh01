SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Couponsalesbycustomer] (
		[AccountingDate]          [date] NULL,
		[Ordernumber]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Customercode]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Shortname]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Stockcode]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[NumberofCouponBooks]     [float] NULL,
		[Gross]                   [money] NULL,
		[GST]                     [money] NULL,
		[Net]                     [money] NULL,
		[Couponcost]              [decimal](12, 2) NULL,
		[Couponsinbook]           [int] NULL
)
GO
ALTER TABLE [dbo].[Couponsalesbycustomer] SET (LOCK_ESCALATION = TABLE)
GO
