SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customised RC Tab only] (
		[Accountcode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Customised RC Tab only] SET (LOCK_ESCALATION = TABLE)
GO
