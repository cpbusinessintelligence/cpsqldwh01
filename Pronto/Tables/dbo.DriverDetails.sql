SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DriverDetails] (
		[AccountCode]        [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[PayToCode]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ABN]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address1]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address2]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address3]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address4]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Phone]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BSBNumber]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber]      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BSBNUMBER2]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber2]     [char](9) COLLATE Latin1_General_CI_AS NULL,
		[ID]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DriverDetails] SET (LOCK_ESCALATION = TABLE)
GO
