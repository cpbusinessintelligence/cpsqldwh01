SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EDI_Consignment_Billing_Data] (
		[ConsignmentID]                [int] NULL,
		[CustomerAccountCode]          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]            [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[CompanyID]                    [int] NULL,
		[ConsignmentDate]              [datetime] NULL,
		[TarrifAccountCode]            [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[OriginPriceZoneCode]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPriceZoneCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ShippingDate]                 [nvarchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredWeight]               [numeric](20, 5) NULL,
		[DeclaredVolume]               [numeric](20, 5) NULL,
		[MeasuredVolume]               [numeric](20, 5) NULL,
		[MeasuredWeight]               [numeric](20, 5) NULL,
		[ChargebleItems]               [int] NULL,
		[Insurance]                    [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[ChargeMethod]                 [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[TotalNetCharge]               [numeric](38, 6) NULL,
		[FuelSurcharge]                [numeric](38, 6) NULL,
		[TotalChargeExGST]             [numeric](38, 6) NULL,
		[GST]                          [numeric](38, 6) NULL,
		[FinalCharge]                  [numeric](20, 2) NULL,
		[DelcaredCubicWeight]          [numeric](25, 6) NULL,
		[OriginalBilledWeight]         [numeric](27, 6) NULL,
		[SYDSortation]                 [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EDI_Consignment_Billing_Data] SET (LOCK_ESCALATION = TABLE)
GO
