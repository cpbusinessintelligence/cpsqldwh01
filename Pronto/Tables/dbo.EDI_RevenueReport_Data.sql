SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EDI_RevenueReport_Data] (
		[Pronto_ConsignmentDate]                   [date] NULL,
		[Pronto_BillingDate]                       [date] NOT NULL,
		[EDI_ConsignmentDate]                      [datetime] NULL,
		[Consignmentreference]                     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EDI_AccountCode]                          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[EDI_CompanyName]                          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_AccountCode]                       [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Pronto_BillToName]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EDI_OriginalServiceCode]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_ServiceCode]                       [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceCodeUpdate]                        [varchar](59) COLLATE Latin1_General_CI_AS NULL,
		[EDI_OriginalFuelSurcharge]                [numeric](38, 6) NULL,
		[Pronto_BilledFuelSurcharge]               [money] NULL,
		[Fuel_Surcharge_Recovered]                 [numeric](38, 6) NULL,
		[EDI_FinalCharge]                          [numeric](20, 2) NULL,
		[Pronto_BilledTotal]                       [money] NULL,
		[Pronto_DeclaredWeight]                    [float] NULL,
		[Pronto_ChargeableWeight]                  [float] NULL,
		[EDI_DeclaredWeight]                       [numeric](20, 5) NULL,
		[WeightVariation]                          [float] NULL,
		[Pronto_DeadWeight]                        [float] NULL,
		[Pronto_DeclaredVolume]                    [float] NULL,
		[Pronto_CardRateFreightCharge]             [money] NULL,
		[EDI_TotalChargeExGST_OriginalFreight]     [numeric](38, 6) NULL,
		[Pronto_BilledFreightCharge]               [money] NULL,
		[RevenueRecovered]                         [numeric](38, 6) NULL,
		[RevenueProtected]                         [varchar](3) COLLATE Latin1_General_CI_AS NOT NULL,
		[EDI_DeclaredVolume]                       [numeric](20, 5) NULL,
		[EDI_DelcaredCubicWeight]                  [numeric](25, 6) NULL,
		[EDI_OriginalBilledWeight]                 [numeric](27, 6) NULL,
		[RevenueBusinessUnit]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueOriginZone]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDestinationZone]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingWeek]                           [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[SYDSortation]                             [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EDI_RevenueReport_Data] SET (LOCK_ESCALATION = TABLE)
GO
