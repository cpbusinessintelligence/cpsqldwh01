SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FranchiseePaymentSummary] (
		[ContractorId]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[RunNumber]                     [smallint] NULL,
		[RunName]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                        [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[RctiWeekEndingDate]            [date] NOT NULL,
		[TotalDevelopmentIncentive]     [decimal](15, 2) NOT NULL,
		[TotalPrepaidRctiAmount]        [decimal](15, 2) NOT NULL,
		[TotalEdiRctiAmount]            [decimal](15, 2) NOT NULL,
		[TotalRctiAllowances]           [decimal](15, 2) NOT NULL,
		[TotalRctiDeductions]           [decimal](15, 2) NOT NULL,
		[TotalRctiRedemptions]          [decimal](15, 2) NOT NULL,
		[Depot]                         [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[AddedBy]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AddedDatetime]                 [datetime] NULL,
		[EditedBy]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]                [datetime] NULL,
		[TotalDevelopmentBonus]         [decimal](15, 2) NULL
)
GO
ALTER TABLE [dbo].[FranchiseePaymentSummary]
	ADD
	CONSTRAINT [DF__Franchise__Added__7AC720C5]
	DEFAULT ('Admin') FOR [AddedBy]
GO
ALTER TABLE [dbo].[FranchiseePaymentSummary]
	ADD
	CONSTRAINT [DF__Franchise__Added__7BBB44FE]
	DEFAULT (getdate()) FOR [AddedDatetime]
GO
ALTER TABLE [dbo].[FranchiseePaymentSummary]
	ADD
	CONSTRAINT [DF__Franchise__Edite__7CAF6937]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[FranchiseePaymentSummary]
	ADD
	CONSTRAINT [DF__Franchise__Edite__7DA38D70]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[FranchiseePaymentSummary] SET (LOCK_ESCALATION = TABLE)
GO
