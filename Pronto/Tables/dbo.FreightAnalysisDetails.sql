SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FreightAnalysisDetails] (
		[AccountCode]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountingDate]             [date] NULL,
		[SalesYear]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SalesMonth]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ItemsCount]                 [bigint] NULL,
		[ConsignmentCount]           [bigint] NULL,
		[TotalWeight]                [decimal](18, 2) NULL,
		[FreightTotalCost]           [decimal](18, 2) NULL,
		[AvgDollarsPerConnote]       [decimal](18, 2) NULL,
		[AvgDollarsPerItem]          [decimal](18, 2) NULL,
		[AvgItemsPerConsignment]     [decimal](18, 2) NULL,
		[AvgWeightPerConnote]        [decimal](18, 2) NULL,
		[AvgWeightPerItem]           [decimal](18, 2) NULL,
		[Total]                      [bigint] NULL
)
GO
ALTER TABLE [dbo].[FreightAnalysisDetails] SET (LOCK_ESCALATION = TABLE)
GO
