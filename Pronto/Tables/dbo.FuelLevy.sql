SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FuelLevy] (
		[Monthkey]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FuelPercent]     [decimal](12, 2) NULL
)
GO
ALTER TABLE [dbo].[FuelLevy] SET (LOCK_ESCALATION = TABLE)
GO
