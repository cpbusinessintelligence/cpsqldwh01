SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FuelToBeBilledSummary] (
		[AccountCode]           [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountBillToCode]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountBillToName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Consignmentcount]      [int] NULL,
		[BilledCharge]          [decimal](12, 4) NULL,
		[BilledFuel]            [decimal](12, 4) NULL,
		[Repcode]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FuelTobeBilled]        [decimal](12, 4) NULL,
		[Difference]            [decimal](12, 4) NULL
)
GO
ALTER TABLE [dbo].[FuelToBeBilledSummary] SET (LOCK_ESCALATION = TABLE)
GO
