SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FuelToBeBilledSummaryv1] (
		[AccountBilltocode]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountBilltoname]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Differenceamount]      [decimal](38, 4) NULL,
		[NetAmount]             [decimal](12, 2) NULL,
		[GST]                   [decimal](12, 2) NULL,
		[GrossAmount]           [decimal](12, 2) NULL,
		[Address1]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address2]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ContactName]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ContactEmail]          [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[DisplayName]           [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceNumber]         [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FuelToBeBilledSummaryv1] SET (LOCK_ESCALATION = TABLE)
GO
