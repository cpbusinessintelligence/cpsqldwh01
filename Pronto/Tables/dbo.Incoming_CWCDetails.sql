SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_CWCDetails] (
		[RawId]              [int] IDENTITY(1, 1) NOT NULL,
		[DwsTimestamp]       [datetime2](7) NULL,
		[CubeLength]         [int] NULL,
		[CubeWidth]          [int] NULL,
		[CubeHeight]         [int] NULL,
		[Volume]             [int] NULL,
		[Weight]             [int] NULL,
		[Barcodes]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_connote]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_id]              [bigint] NULL,
		[AccountCode]        [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[CreateDateTime]     [datetime] NULL,
		[IsSent]             [bit] NULL,
		CONSTRAINT [PK_Incoming_CWCDetails_New]
		PRIMARY KEY
		CLUSTERED
		([RawId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Incoming_CWCDetails] SET (LOCK_ESCALATION = TABLE)
GO
