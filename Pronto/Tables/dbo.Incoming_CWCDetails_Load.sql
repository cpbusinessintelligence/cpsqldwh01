SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_CWCDetails_Load] (
		[RawID]                [bigint] NOT NULL,
		[DWSResultId]          [int] NULL,
		[SortResultJSONID]     [int] NULL,
		[DwsTimestamp]         [datetime2](7) NULL,
		[CubeLength]           [int] NULL,
		[CubeWidth]            [int] NULL,
		[CubeHeight]           [int] NULL,
		[Volume]               [int] NULL,
		[Weight]               [int] NULL,
		[Barcodes]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SorterName]           [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_connote]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_id]                [bigint] NULL,
		[AccountCode]          [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[CreateDateTime]       [datetime] NULL,
		[IsSent]               [bit] NULL,
		[SentDateTime]         [datetime] NULL
)
GO
ALTER TABLE [dbo].[Incoming_CWCDetails_Load] SET (LOCK_ESCALATION = TABLE)
GO
