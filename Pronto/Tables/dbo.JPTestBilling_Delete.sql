SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JPTestBilling_Delete] (
		[Consignment reference]     [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment date]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Service]                   [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Account code]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Item quantity]             [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Declared weight]           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Measured weight]           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Declared volume]           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Measured volume]           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Logistics Units]           [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[JPTestBilling_Delete] SET (LOCK_ESCALATION = TABLE)
GO
