SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JamesPOC] (
		[LabelNumber]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Relation]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NowGoWorkflowCompletionID]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NowGoConsignmentID]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WorkflowType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Outcome]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Attribute]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DLBCode]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedeliveryBarCode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverExtRef]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverRunNumber]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventDateTime]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[JamesPOC] SET (LOCK_ESCALATION = TABLE)
GO
