SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Jp_ReturnsRevenue] (
		[ConsignmentCode]     [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]     [datetime2](3) NOT NULL,
		[RateCardID]          [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerRefNo]       [varchar](40) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Jp_ReturnsRevenue] SET (LOCK_ESCALATION = TABLE)
GO
