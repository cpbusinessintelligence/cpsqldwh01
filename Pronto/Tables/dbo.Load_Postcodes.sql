SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Load_Postcodes] (
		[bi_sys_action_code]          [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_cons_code]            [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]            [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[postcode]                    [char](10) COLLATE Latin1_General_CI_AS NULL,
		[postcode_desc]               [char](30) COLLATE Latin1_General_CI_AS NULL,
		[postcode_state]              [char](3) COLLATE Latin1_General_CI_AS NULL,
		[postcode_country]            [char](3) COLLATE Latin1_General_CI_AS NULL,
		[postcode_carrier_code]       [char](4) COLLATE Latin1_General_CI_AS NULL,
		[postcode_carrier_rate]       [numeric](7, 2) NULL,
		[postcode_carrier_charge]     [numeric](7, 2) NULL
)
GO
ALTER TABLE [dbo].[Load_Postcodes] SET (LOCK_ESCALATION = TABLE)
GO
