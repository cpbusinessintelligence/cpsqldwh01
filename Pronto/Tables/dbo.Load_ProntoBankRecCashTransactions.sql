SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoBankRecCashTransactions] (
		[BankGlAccount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionType]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DocumentNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankCode]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ShortName]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionDate]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionReference]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Details]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BatchReference]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionNumber]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LocalAmount]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ForeignAmount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PresentedFlag]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PresentedDate]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WithholdTax]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ForeignWithholdTax]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FecCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FecRate]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DateStamp]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BsbNumber]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompanyCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankAccountCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionSource]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PayeeDetails]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReceiptLocation]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankSlipNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OnStatement]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyDate1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyNum1]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyNum2]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareNum1]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareDateTime]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareNum2]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareAlpha2]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CardAuthorisation]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareDate]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareDate2]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareDate3]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OnBankSlip]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareAlpha2_1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoBankRecCashTransactions] SET (LOCK_ESCALATION = TABLE)
GO
