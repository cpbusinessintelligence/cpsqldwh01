SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoBilling] (
		[CompanyCode]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OrderNumber]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Suffix]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Order]                      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentReference]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentDate]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ManifestReference]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ManifestDate]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]                    [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Accountcode]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[OriginLocality]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginPostcode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReceiverName]               [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[DestinationLocality]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPostcode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustomerReference]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LogisticsUnits]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ItemQty]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredWeight]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Weight]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredVolume]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volume]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PriceOverride]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceCategory]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeclaredValue]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsurancePriceOverride]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TestFlag]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompanyId]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillingDate]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginZone]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DestinationZone]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ChargeableWeight]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FreightCust]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FuelCust]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransCust]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsurCharge]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherCharge]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TotalCust]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FreightCard]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FuelCard]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransCard]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DiscOffCard]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupCost]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupSupplier]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupZone]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryCost]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Supplier]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TotalPudCost]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceNo]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceDate]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PostingYearAndPeriod]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TariffId]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FreightCalc]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FuelCalc]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransCalc]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_Load_ProntoBilling]
		PRIMARY KEY
		CLUSTERED
		([OrderNumber], [Order], [ConsignmentReference], [Service])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Load_ProntoBilling] SET (LOCK_ESCALATION = TABLE)
GO
