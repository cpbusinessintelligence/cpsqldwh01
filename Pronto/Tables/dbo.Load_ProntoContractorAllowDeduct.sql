SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoContractorAllowDeduct] (
		[AccountingDate]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeductionOrAllowance]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SeqNumber]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Code]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CalculationRule]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GST]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoContractorAllowDeduct] SET (LOCK_ESCALATION = TABLE)
GO
