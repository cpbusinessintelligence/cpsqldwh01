SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoContractorWeeklyDelivery] (
		[TransactionType]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ItemCode]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceCategory]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingDate]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceAmount]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Count]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoContractorWeeklyDelivery] SET (LOCK_ESCALATION = TABLE)
GO
