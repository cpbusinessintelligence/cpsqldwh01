SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoContractorWeeklyReceipt] (
		[AccountingDate]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CashType]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoContractorWeeklyReceipt] SET (LOCK_ESCALATION = TABLE)
GO
