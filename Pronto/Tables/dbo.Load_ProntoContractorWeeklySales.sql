SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoContractorWeeklySales] (
		[AccountingDate]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerAccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Contractor]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ItemCode]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[InsuranceCategory]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CouponAmount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceAmount]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BookQty]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoContractorWeeklySales] SET (LOCK_ESCALATION = TABLE)
GO
