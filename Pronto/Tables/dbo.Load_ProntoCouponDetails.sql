SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoCouponDetails] (
		[SerialNumber]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StartSerialNumber]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReturnSerialNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransferReference]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransferDate]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastSoldReference]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastSoldDate]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceCategory]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceAmount]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupContractor]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupRctiDate]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupRctiReference]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupRctiAmount]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryContractor]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryRctiDate]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryRctiReference]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryRctiAmount]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillingAccountCode]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillingService]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueBatchReference]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueTransactionNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDate]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueAmount]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupProcessedDate]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryProcessedDate]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFlag]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoCouponDetails] SET (LOCK_ESCALATION = TABLE)
GO
