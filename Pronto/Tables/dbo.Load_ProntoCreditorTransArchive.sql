SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoCreditorTransArchive] (
		[AccountCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SortKey]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BranchCode]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherSide]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PayByDate]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransType]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransDate]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransReference]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PoOrderNo]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PoBackorderFlag]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Details]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ApplyDiscFlag]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BatchRef]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceDate]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CurrCode]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CurrRate]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OverseasAmount]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransDateStamp]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransTimeStamp]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransUid]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvApprovalRequired]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RetentionApproved]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[JobCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[JobCostCentre]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TaxAmount]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TaxCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransNo]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TaxClaimedDate]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WithholdingIndicator]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WithholdingRate]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpareFlag]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReportedDate]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TermsDiscCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TermsDiscAmt]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyDate]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyNum1]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyNum2]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransDescCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AnalysisCode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProvincialTaxAmount]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoCreditorTransArchive] SET (LOCK_ESCALATION = TABLE)
GO
