SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoCreditorV1] (
		[bi_sys_action_code]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_cons_code]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]                  [char](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_cre_accountcode]                [char](10) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_pay_to_code]                 [char](10) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_shortname]                   [char](30) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_def_payment_terms]           [char](2) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_payment_selection]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_auto_rq_flag]                [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_type]                        [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_chq_or_bankdraft]            [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_account_status]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_clearing_flag]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_settlement_disc_code]        [char](2) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_volume_disc_code]            [char](2) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_disc_rate]                   [numeric](8, 4) NULL,
		[bi_cr_date_last_payment]           [smalldatetime] NULL,
		[bi_cr_date_last_purchase]          [smalldatetime] NULL,
		[bi_cr_contact_name]                [char](20) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_default_gl_accountcode]      [char](25) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_date_last_changed]           [smalldatetime] NULL,
		[bi_cr_curr_code]                   [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_date_created]                [smalldatetime] NULL,
		[bi_cr_def_purchase_terms_days]     [smallint] NULL,
		[bi_cr_order_terms]                 [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_supplier_grp]                [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_tax_code]                    [char](2) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_user_only_alpha4_3]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_user_only_alpha4_4]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_spare_1]                     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_spare_2]                     [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_user_only_alpha20_1]         [char](20) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_user_only_alpha20_2]         [char](20) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_user_only_alpha4_1]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_user_only_alpha4_2]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_cr_user_only_num_1]             [numeric](14, 2) NULL,
		[bi_cr_user_only_num_2]             [numeric](14, 2) NULL,
		[bi_cr_user_only_num_3]             [numeric](14, 2) NULL,
		[bi_cr_user_only_num_4]             [numeric](14, 2) NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoCreditorV1] SET (LOCK_ESCALATION = TABLE)
GO
