SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoCustomerUprateActions] (
		[Pronto_Data_Change_Required]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service_Change_or_Deactivation]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Existing_Rating_Type]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New_Rating_Type]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_Billing_Account]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Execution_Timing]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_Territory]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[C_S_D]                              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pronto_Account_Number]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Account_Name]                       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[State]                              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Existing_Service]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New_Service]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Uprate_Action]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Percentage_Change]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pending_Action]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoCustomerUprateActions] SET (LOCK_ESCALATION = TABLE)
GO
