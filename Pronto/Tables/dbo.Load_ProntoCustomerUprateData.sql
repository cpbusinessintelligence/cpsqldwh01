SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoCustomerUprateData] (
		[ProntoDataChangeRequired]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceChangeDeactivation]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExistingRatingType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NewRatingType]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NewCardRateExists]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DateSetupInPronto]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[0to12monthEDIConsCount]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[12to24monthEDIConsCount]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AutoDelete]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProntoBillingAccount]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillingAccountKey]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillingAccountServiceKey]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceKeyCount]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GatewayAccountNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProntoTerritory]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CSD]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProntoAccountNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StateBasedChange]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExistingService]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NewService]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PercentageChange]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SourceEmailDate]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SourceOriginator]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SourceFileName]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SourceTabName]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ItemsToBeClarified]            [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoCustomerUprateData] SET (LOCK_ESCALATION = TABLE)
GO
