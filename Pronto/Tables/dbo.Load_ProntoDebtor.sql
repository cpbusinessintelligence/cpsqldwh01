SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoDebtor] (
		[CompanyCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BillTo]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TermDisc]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IndustryCode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustType]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MarketingFlag]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AverageDaysToPay]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IndustrySubGroup]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Locality]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepCode]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastSale]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ABN]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RepName]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmAccount]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmParent]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmType]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmRegion]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoDebtor] SET (LOCK_ESCALATION = TABLE)
GO
