SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGLBudgets] (
		[AccountCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProntoBudgetCode]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Period1Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period2Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period3Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period4Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period5Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period6Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period7Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period8Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period9Value]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period10Value]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period11Value]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Period12Value]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DummyValue1]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DummyValue2]          [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGLBudgets] SET (LOCK_ESCALATION = TABLE)
GO
