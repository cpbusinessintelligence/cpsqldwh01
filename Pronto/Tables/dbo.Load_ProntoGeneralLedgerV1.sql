SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGeneralLedgerV1] (
		[bi_sys_action_code]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_cons_code]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]                   [char](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_accountcode]               [char](25) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_alt_key]                   [char](25) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_desc]                      [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_bal_sheet_profit_loss]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_account_type]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_account]                      [char](9) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_corp_breakdown]               [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_company]                      [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_department]                   [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_state]                        [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_business_unit]                [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_region]                       [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_section]                      [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_territory]                    [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_division]                     [char](10) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_sub_section]                  [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_cost_centre]                  [char](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_special_report_flag]       [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_user_only_alpha20_1]       [char](20) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_user_only_alpha20_2]       [char](20) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_gl_mlc_code]                  [char](10) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_total_level]                  [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_user_only_alpha4_1]           [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_user_only_alpha4_2]           [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_user_only_alpha4_3]           [char](4) COLLATE Latin1_General_CI_AS NULL,
		[bi_gd_user_only_alpha4_4]           [char](4) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGeneralLedgerV1] SET (LOCK_ESCALATION = TABLE)
GO
