SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGeoDim] (
		[CompanyCode]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrimaryBillZone]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SecondaryBillZone]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PrimaryCostZone]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SecondryCostZone]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Locality]              [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGeoDim] SET (LOCK_ESCALATION = TABLE)
GO
