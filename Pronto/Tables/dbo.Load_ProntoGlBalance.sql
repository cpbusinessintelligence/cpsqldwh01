SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGlBalance] (
		[AccountNumber]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingYear]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingPeriod]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GLBalance]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GLBudget]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionDate]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DatabaseCode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGlBalance] SET (LOCK_ESCALATION = TABLE)
GO
