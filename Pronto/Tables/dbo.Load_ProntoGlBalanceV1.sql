SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGlBalanceV1] (
		[bi_sys_action_code]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_cons_code]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]               [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_glf_gl_accountcode]          [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[bi_glf_rec_type]                [varchar](2) COLLATE Latin1_General_CI_AS NULL,
		[bi_glf_year]                    [int] NULL,
		[bi_glf_period]                  [smallint] NULL,
		[bi_glf_amount]                  [numeric](14, 2) NULL,
		[bi_glf_report_date]             [smalldatetime] NULL,
		[bi_glf_total_level]             [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_glf_balsheet_profitloss]     [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_glf_company]                 [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[bi_glf_rpt_flag]                [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGlBalanceV1] SET (LOCK_ESCALATION = TABLE)
GO
