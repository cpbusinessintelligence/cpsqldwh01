SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGlChartOfAccounts] (
		[ComponentType]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountType]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountType1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGlChartOfAccounts] SET (LOCK_ESCALATION = TABLE)
GO
