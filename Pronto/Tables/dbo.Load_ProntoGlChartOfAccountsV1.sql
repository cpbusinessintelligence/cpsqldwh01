SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGlChartOfAccountsV1] (
		[bi_sys_action_code]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_cons_code]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]                 [char](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_glc_gl_chart_function_seq]     [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_glc_gl_chart_function]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_glc_gl_accountcode]            [char](25) COLLATE Latin1_General_CI_AS NULL,
		[bi_glc_gl_account_desc]           [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[bi_glc_gl_account_type]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[bi_glc_glcoa_tax_entity_code]     [char](4) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGlChartOfAccountsV1] SET (LOCK_ESCALATION = TABLE)
GO
