SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoGlFact] (
		[AccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Year]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Period]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Actual]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Budget]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReportDate]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Company]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoGlFact] SET (LOCK_ESCALATION = TABLE)
GO
