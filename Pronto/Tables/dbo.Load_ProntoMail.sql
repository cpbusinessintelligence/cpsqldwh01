SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoMail] (
		[CompanyCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ForecastNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CrmAccount]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmAccountName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepName]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SourceC]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SourceText]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StageC]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StageText]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rating]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ForecastAmount]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExpectedAmount]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Factor]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompetitorCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompetitorName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_Load_ProntoMail]
		PRIMARY KEY
		CLUSTERED
		([ForecastNumber])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Load_ProntoMail] SET (LOCK_ESCALATION = TABLE)
GO
