SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoSalesOrder] (
		[OrderNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Suffix]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompanyCode]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerCode]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[WarehouseCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TerritoryCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SalesType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingDate]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingPeriod]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NetAmount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GSTAmount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GrossAmount]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CostAmount]           [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoSalesOrder] SET (LOCK_ESCALATION = TABLE)
GO
