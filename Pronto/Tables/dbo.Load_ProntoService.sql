SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoService] (
		[CompanyCode]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceName]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceGroup]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoService] SET (LOCK_ESCALATION = TABLE)
GO
