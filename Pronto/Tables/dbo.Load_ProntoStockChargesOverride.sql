SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoStockChargesOverride] (
		[CouponPrefix]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[BranchCode]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ContractorCode]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RedemptionType]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[RedemptionAmount]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[OldRedemptionAmount]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EffectiveDate]           [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[OldEffectiveDate]        [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoStockChargesOverride] SET (LOCK_ESCALATION = TABLE)
GO
