SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoStockSerialLink] (
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SerialLinkType]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialLinkCode]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialLinkSuffix]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialLinkSeqNo]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoStockSerialLink] SET (LOCK_ESCALATION = TABLE)
GO
