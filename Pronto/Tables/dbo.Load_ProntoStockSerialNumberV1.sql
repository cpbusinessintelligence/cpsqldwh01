SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoStockSerialNumberV1] (
		[bi_sys_action_code]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_cons_code]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]                [char](3) COLLATE Latin1_General_CI_AS NULL,
		[SerialNumber]                    [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ContractNumber]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StockCode]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialStatus]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialSiteDesc]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialWarrantyDate]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialContractType]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialActionDate]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialInstallDate]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialStockType]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialWhseCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialContractRate]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialItemCost]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialInvoicedDate]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialInvoiceAmount]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialInvoicedFromDate]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialInvoicedToDate]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialInvoiceNumber]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialCurrentContractIncome]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialOriginalInvNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialSequenceNumber]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialSecondaryKey]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialFastAccess]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialTraceabilityCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyDate1]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyDate2]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha201]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha202]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha41]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha42]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha43]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha44]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyNum1]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyNum2]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyNum3]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyNum4]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoStockSerialNumberV1] SET (LOCK_ESCALATION = TABLE)
GO
