SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoSystemTable] (
		[bi_sys_action_code]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_cons_code]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]            [char](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_type]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_code]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_sales_mtd]        [numeric](12, 2) NULL,
		[bi_sys_tbl_sales_ytd]        [numeric](12, 2) NULL,
		[bi_sys_tbl_sales_ly]         [numeric](12, 2) NULL,
		[bi_sys_description]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_money_value]          [numeric](19, 6) NULL,
		[bi_sys_date_last_change]     [datetime] NULL,
		[bi_sys_tbl_alpha_1]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_alpha_2]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_alpha_3]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_alpha_41]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_alpha_42]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_alpha_43]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_tbl_alpha_44]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ProntoSystemTable] SET (LOCK_ESCALATION = TABLE)
GO
