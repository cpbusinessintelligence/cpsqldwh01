SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ProntoWarehouse] (
		[Warehouse]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Group]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GroupDescription]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Spare1]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Spare2]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Spare3]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Spare4]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_Load_ProntoWarehouse]
		PRIMARY KEY
		CLUSTERED
		([Warehouse])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Load_ProntoWarehouse] SET (LOCK_ESCALATION = TABLE)
GO
