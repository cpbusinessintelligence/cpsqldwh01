SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Postcodes] (
		[SysActionCode]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompSource]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompCode]                 [char](3) COLLATE Latin1_General_CI_AS NULL,
		[postcode]                    [char](10) COLLATE Latin1_General_CI_AS NULL,
		[postcode_desc]               [char](30) COLLATE Latin1_General_CI_AS NULL,
		[postcode_state]              [char](3) COLLATE Latin1_General_CI_AS NULL,
		[postcode_country]            [char](3) COLLATE Latin1_General_CI_AS NULL,
		[postcode_carrier_code]       [char](4) COLLATE Latin1_General_CI_AS NULL,
		[postcode_carrier_rate]       [numeric](7, 2) NULL,
		[postcode_carrier_charge]     [numeric](7, 2) NULL
)
GO
ALTER TABLE [dbo].[Postcodes] SET (LOCK_ESCALATION = TABLE)
GO
