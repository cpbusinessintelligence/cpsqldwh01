SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrepaidCouponReport] (
		[PrepaidCouponBookID]     [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[TransferDate]            [date] NULL,
		[LastSoldDate]            [date] NULL,
		[Aging]                   [int] NULL,
		[PickupDate]              [date] NULL,
		[DeliveryDate]            [date] NULL,
		[CourierID]               [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[BusinessUnit]            [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[PrepaidCouponReport] SET (LOCK_ESCALATION = TABLE)
GO
