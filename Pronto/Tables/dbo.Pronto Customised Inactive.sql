SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pronto Customised Inactive] (
		[Accountcode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Pronto Customised Inactive] SET (LOCK_ESCALATION = TABLE)
GO
