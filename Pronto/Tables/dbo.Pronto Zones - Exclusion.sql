SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pronto Zones - Exclusion] (
		[Zone]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Keep]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Pronto Zones - Exclusion] SET (LOCK_ESCALATION = TABLE)
GO
