SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoAccountBillingActivity] (
		[AccountCode]                         [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[BillToAccountCode]                   [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[FirstBillingWeek]                    [date] NULL,
		[LastBillingWeek]                     [date] NULL,
		[LastConsignmentPickupContractor]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[LastConsignmentPickupBranch]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LastConsignmentPickupDriver]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoAccountBillingActivity_1]
		PRIMARY KEY
		CLUSTERED
		([AccountCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoAccountBillingActivity] SET (LOCK_ESCALATION = TABLE)
GO
