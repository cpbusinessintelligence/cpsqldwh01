SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoAccountCosmosActivity] (
		[AccountCode]                    [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[CosmosBranch]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CosmosCode]                     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[LastScanActivity]               [date] NULL,
		[LastStockCode]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[LastStockDescription]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[FirstProntoOrderDate]           [date] NULL,
		[FirstProntoOrderContractor]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[LastProntoOrderDate]            [date] NULL,
		[LastProntoOrderContractor]      [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoAccountCosmosActivity]
		PRIMARY KEY
		CLUSTERED
		([AccountCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoAccountCosmosActivity] SET (LOCK_ESCALATION = TABLE)
GO
