SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoAccountingPeriod_BUP_2020] (
		[DayId]             [date] NOT NULL,
		[WeekId]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[PeriodId]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[PeriodName]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[calendarMonth]     [int] NULL,
		[calendarYear]      [int] NULL,
		[FinancialYear]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoAccountingPeriod_BUP_2020] SET (LOCK_ESCALATION = TABLE)
GO
