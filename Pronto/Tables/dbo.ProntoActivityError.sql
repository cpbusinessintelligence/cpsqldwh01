SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoActivityError] (
		[ActivityDateTime]     [datetime] NOT NULL,
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ActivityFlag]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFileOn]       [date] NOT NULL
)
GO
ALTER TABLE [dbo].[ProntoActivityError] SET (LOCK_ESCALATION = TABLE)
GO
