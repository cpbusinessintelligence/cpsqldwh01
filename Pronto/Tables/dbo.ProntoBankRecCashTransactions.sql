SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoBankRecCashTransactions] (
		[BankGlAccount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionType]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DocumentNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankCode]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ShortName]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionDate]          [date] NULL,
		[TransactionReference]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Details]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BatchReference]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionNumber]        [int] NULL,
		[LocalAmount]              [money] NULL,
		[ForeignAmount]            [money] NULL,
		[PresentedFlag]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PresentedDate]            [date] NULL,
		[WithholdTax]              [money] NULL,
		[ForeignWithholdTax]       [money] NULL,
		[FecCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FecRate]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DateStamp]                [datetime] NULL,
		[BsbNumber]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompanyCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankAccountCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransactionSource]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PayeeDetails]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReceiptLocation]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BankSlipNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OnStatement]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CardAuthorisation]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OnBankSlip]               [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoBankRecCashTransactions] SET (LOCK_ESCALATION = TABLE)
GO
