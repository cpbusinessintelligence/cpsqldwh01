SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoBilling] (
		[OrderNumber]                   [bigint] NOT NULL,
		[OrderSuffix]                   [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[OrderId]                       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentReference]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountCode]                   [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountBillToCode]             [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountBillToName]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                   [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[CompanyCode]                   [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AccountingPeriod]              [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[AccountingWeek]                [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[BillingDate]                   [date] NOT NULL,
		[AccountingDate]                [date] NULL,
		[InvoiceNumber]                 [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[OriginLocality]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginPostcode]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ReceiverName]                  [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DestinationLocality]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPostcode]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentDate]               [date] NULL,
		[ManifestReference]             [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[ManifestDate]                  [date] NULL,
		[CustomerReference]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LogisticsUnitsQuantity]        [float] NULL,
		[ItemQuantity]                  [float] NULL,
		[DeclaredWeight]                [float] NULL,
		[DeadWeight]                    [float] NULL,
		[DeclaredVolume]                [float] NULL,
		[Volume]                        [float] NULL,
		[ChargeableWeight]              [float] NULL,
		[LinehaulWeight]                [float] NULL,
		[InsuranceCategory]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceDeclaredValue]        [money] NULL,
		[InsurancePriceOverride]        [money] NULL,
		[TestFlag]                      [bit] NULL,
		[RevenueBusinessUnit]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TariffId]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueOriginZone]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDestinationZone]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CalculatedFreightCharge]       [money] NULL,
		[CalculatedFuelSurcharge]       [money] NULL,
		[CalculatedTransportCharge]     [money] NULL,
		[PriceOverride]                 [money] NULL,
		[BilledFreightCharge]           [money] NULL,
		[BilledFuelSurcharge]           [money] NULL,
		[BilledTransportCharge]         [money] NULL,
		[BilledInsurance]               [money] NULL,
		[BilledOtherCharge]             [money] NULL,
		[BilledTotal]                   [money] NULL,
		[CardRateFreightCharge]         [money] NULL,
		[CardRateFuelSurcharge]         [money] NULL,
		[CardRateTransportCharge]       [money] NULL,
		[CardRateDiscountOff]           [money] NULL,
		[PickupCost]                    [money] NULL,
		[PickupSupplier]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupZone]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[LinehaulCost]                  [money] NULL,
		[LinehaulRoute]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryCost]                  [money] NULL,
		[DeliverySupplier]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[TotalPUDCost]                  [money] NULL,
		[MarginTransportcharge]         [money] NULL,
		CONSTRAINT [PK_ProntoBilling1]
		PRIMARY KEY
		CLUSTERED
		([OrderNumber], [OrderId], [ConsignmentReference], [ServiceCode], [BillingDate], [AccountCode], [AccountBillToCode])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [_dta_index_ProntoBilling_6_1490104349__K9_K10_K14_K1_44_46_47]
	ON [dbo].[ProntoBilling] ([ServiceCode], [CompanyCode], [AccountingDate], [OrderNumber])
	INCLUDE ([BilledFuelSurcharge], [BilledInsurance], [BilledOtherCharge])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_ProntoBilling_AccountCodeAcctWeek1]
	ON [dbo].[ProntoBilling] ([AccountCode], [AccountingWeek])
	INCLUDE ([ConsignmentReference], [OrderNumber], [AccountBillToCode], [ServiceCode], [CompanyCode], [AccountingPeriod], [BillingDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProntoBilling] SET (LOCK_ESCALATION = TABLE)
GO
