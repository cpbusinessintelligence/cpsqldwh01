SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoBilling_Archive] (
		[OrderNumber]                   [bigint] NULL,
		[OrderSuffix]                   [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[OrderId]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentReference]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                   [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountBillToCode]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AccountBillToName]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceCode]                   [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[CompanyCode]                   [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AccountingPeriod]              [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[AccountingWeek]                [varchar](6) COLLATE Latin1_General_CI_AS NULL,
		[BillingDate]                   [date] NULL,
		[AccountingDate]                [date] NULL,
		[InvoiceNumber]                 [varchar](8) COLLATE Latin1_General_CI_AS NULL,
		[OriginLocality]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginPostcode]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ReceiverName]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DestinationLocality]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DestinationPostcode]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentDate]               [date] NULL,
		[ManifestReference]             [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[ManifestDate]                  [date] NULL,
		[CustomerReference]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LogisticsUnitsQuantity]        [float] NULL,
		[ItemQuantity]                  [float] NULL,
		[DeclaredWeight]                [float] NULL,
		[DeadWeight]                    [float] NULL,
		[DeclaredVolume]                [float] NULL,
		[Volume]                        [float] NULL,
		[ChargeableWeight]              [float] NULL,
		[LinehaulWeight]                [float] NULL,
		[InsuranceCategory]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceDeclaredValue]        [money] NULL,
		[InsurancePriceOverride]        [money] NULL,
		[TestFlag]                      [bit] NULL,
		[RevenueBusinessUnit]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TariffId]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueOriginZone]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDestinationZone]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CalculatedFreightCharge]       [money] NULL,
		[CalculatedFuelSurcharge]       [money] NULL,
		[CalculatedTransportCharge]     [money] NULL,
		[PriceOverride]                 [money] NULL,
		[BilledFreightCharge]           [money] NULL,
		[BilledFuelSurcharge]           [money] NULL,
		[BilledTransportCharge]         [money] NULL,
		[BilledInsurance]               [money] NULL,
		[BilledOtherCharge]             [money] NULL,
		[BilledTotal]                   [money] NULL,
		[CardRateFreightCharge]         [money] NULL,
		[CardRateFuelSurcharge]         [money] NULL,
		[CardRateTransportCharge]       [money] NULL,
		[CardRateDiscountOff]           [money] NULL,
		[PickupCost]                    [money] NULL,
		[PickupSupplier]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupZone]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[LinehaulCost]                  [money] NULL,
		[LinehaulRoute]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryCost]                  [money] NULL,
		[DeliverySupplier]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryZone]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[TotalPUDCost]                  [money] NULL,
		[MarginTransportcharge]         [money] NULL
)
GO
ALTER TABLE [dbo].[ProntoBilling_Archive] SET (LOCK_ESCALATION = TABLE)
GO
