SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoCPDetailstobePurged] (
		[SerialNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Pickupdate]             [date] NULL,
		[DeliveryDate]           [date] NULL,
		[BillingAccountCode]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[BillingService]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoCPDetailstobePurged] SET (LOCK_ESCALATION = TABLE)
GO
