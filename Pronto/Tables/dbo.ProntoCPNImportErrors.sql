SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoCPNImportErrors] (
		[RecordType]           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActivityDateTime]     [datetime] NULL,
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LinkCouponNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReturnTracker]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PhoneNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Invoice]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActivityType]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentType]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceValue]       [decimal](3, 2) NULL,
		[FileName]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoCPNImportErrors] SET (LOCK_ESCALATION = TABLE)
GO
