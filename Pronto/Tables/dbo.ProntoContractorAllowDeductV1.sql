SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoContractorAllowDeductV1] (
		[AccountingDate]           [date] NOT NULL,
		[Contractor]               [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[DeductionOrAllowance]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SeqNumber]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Code]                     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CalculationRule]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                   [money] NULL,
		[GST]                      [money] NULL,
		CONSTRAINT [PK_ProntoContractorAllowDeduct1]
		PRIMARY KEY
		CLUSTERED
		([AccountingDate], [Contractor], [DeductionOrAllowance], [SeqNumber], [Code])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoContractorAllowDeductV1] SET (LOCK_ESCALATION = TABLE)
GO
