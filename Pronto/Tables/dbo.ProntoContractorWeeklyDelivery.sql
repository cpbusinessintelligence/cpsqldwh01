SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoContractorWeeklyDelivery] (
		[Id]                      [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[TransactionType]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ItemCode]                [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAccountCode]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceCategory]       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AccountingDate]          [date] NULL,
		[Amount]                  [money] NULL,
		[InsuranceAmount]         [money] NULL,
		[Count]                   [int] NULL
)
GO
CREATE CLUSTERED INDEX [IX_ProntoContractorWeeklyDelivery]
	ON [dbo].[ProntoContractorWeeklyDelivery] ([AccountingDate], [Contractor], [CustomerAccountCode], [InsuranceAmount])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProntoContractorWeeklyDelivery] SET (LOCK_ESCALATION = TABLE)
GO
