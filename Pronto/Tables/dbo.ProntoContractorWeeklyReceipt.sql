SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoContractorWeeklyReceipt] (
		[Id]                      [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[AccountingDate]          [date] NOT NULL,
		[Contractor]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[CashType]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerAccountCode]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Amount]                  [money] NOT NULL
)
GO
CREATE CLUSTERED INDEX [IX_ProntoContractorWeeklyReceipt]
	ON [dbo].[ProntoContractorWeeklyReceipt] ([AccountingDate], [Contractor], [CustomerAccountCode], [Amount])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProntoContractorWeeklyReceipt] SET (LOCK_ESCALATION = TABLE)
GO
