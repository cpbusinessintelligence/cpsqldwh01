SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoContractorWeeklySales] (
		[AccountingDate]          [date] NOT NULL,
		[CustomerAccountCode]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[Contractor]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ItemCode]                [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[InsuranceCategory]       [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[CouponAmount]            [money] NOT NULL,
		[InsuranceAmount]         [money] NOT NULL,
		[BookQty]                 [int] NOT NULL,
		CONSTRAINT [PK_ProntoContractorWeeklySales]
		PRIMARY KEY
		CLUSTERED
		([AccountingDate], [CustomerAccountCode], [Contractor], [ItemCode], [InsuranceCategory], [CouponAmount], [InsuranceAmount], [BookQty])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoContractorWeeklySales] SET (LOCK_ESCALATION = TABLE)
GO
