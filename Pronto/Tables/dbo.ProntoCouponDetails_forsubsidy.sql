SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoCouponDetails_forsubsidy] (
		[SerialNumber]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[StartSerialNumber]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ReturnSerialNumber]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[TransferReference]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransferDate]                 [date] NULL,
		[LastSoldReference]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastSoldDate]                 [date] NULL,
		[InsuranceCategory]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceAmount]              [money] NULL,
		[PickupContractor]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                   [date] NULL,
		[PickupRctiDate]               [date] NULL,
		[PickupRctiReference]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupRctiAmount]             [money] NULL,
		[DeliveryContractor]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]                 [date] NULL,
		[DeliveryRctiDate]             [date] NULL,
		[DeliveryRctiReference]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryRctiAmount]           [money] NULL,
		[BillingAccountCode]           [varchar](11) COLLATE Latin1_General_CI_AS NULL,
		[BillingService]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueBatchReference]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueTransactionNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDate]                  [date] NULL,
		[RevenueAmount]                [money] NULL,
		[PickupProcessedDate]          [datetime] NULL,
		[DeliveryProcessedDate]        [datetime] NULL,
		[ActivityFlag]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoCouponDetails_2]
		PRIMARY KEY
		NONCLUSTERED
		([SerialNumber])
	ON [TERTIARY]
)
GO
ALTER TABLE [dbo].[ProntoCouponDetails_forsubsidy] SET (LOCK_ESCALATION = TABLE)
GO
