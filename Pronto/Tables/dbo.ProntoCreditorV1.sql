SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoCreditorV1] (
		[SysActionCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysConsCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompcode]            [char](3) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]            [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[PayToCode]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]                   [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AccountStatus]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SettlementDiscCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[VolumeDiscCode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DiscRate]               [money] NULL,
		[CurrCode]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyDate]           [date] NULL,
		[UserOnlyAlpha20]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyNum1]           [float] NULL,
		[UserOnlyNum2]           [float] NULL,
		[ABN]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address1]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address2]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address3]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address4]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address5]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address6]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Phone]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Fax]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Mobile]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreditLimit]            [money] NULL,
		[Email]                  [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoCreditor2]
		PRIMARY KEY
		CLUSTERED
		([AccountCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoCreditorV1] SET (LOCK_ESCALATION = TABLE)
GO
