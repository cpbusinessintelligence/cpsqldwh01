SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoDebtorAgeing] (
		[SysChangeType]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystemCompSource]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystemCompCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DfAcountCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DfYear]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DfPeriod]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DfReportDate]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DfAgedBalance1]            [decimal](12, 2) NULL,
		[DfAgedBalance2]            [decimal](12, 2) NULL,
		[DfAgedBalance3]            [decimal](12, 2) NULL,
		[DfAgedBalance4]            [decimal](12, 2) NULL,
		[DfAgedBalance5]            [decimal](12, 2) NULL,
		[DfAgedBalance6]            [decimal](12, 2) NULL,
		[DfAgedBalance7]            [decimal](12, 2) NULL,
		[DfAgedBalance8]            [decimal](12, 2) NULL,
		[DfAgedBalance9]            [decimal](12, 2) NULL,
		[DfAgedBalance10]           [decimal](12, 2) NULL,
		[DfAgedBalance11]           [decimal](12, 2) NULL,
		[DfAgedBalance12]           [decimal](12, 2) NULL,
		[DfAgedBalance13]           [decimal](12, 2) NULL,
		[DfFutureBalance]           [decimal](12, 2) NULL,
		[DfDrAgedFutureBalance]     [decimal](12, 2) NULL
)
GO
ALTER TABLE [dbo].[ProntoDebtorAgeing] SET (LOCK_ESCALATION = TABLE)
GO
