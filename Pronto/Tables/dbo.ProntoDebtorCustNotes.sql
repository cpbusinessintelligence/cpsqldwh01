SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoDebtorCustNotes] (
		[DCN_Account]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DCN_Type]              [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[DCN_SequenceNo]        [float] NOT NULL,
		[DCN_Text]              [varchar](150) COLLATE Latin1_General_CI_AS NULL,
		[DCN_EntryDate]         [date] NULL,
		[DCN_FollowUpDate]      [date] NULL,
		[DCN_LoginId]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DCN_DateTimeStamp]     [datetime] NULL,
		CONSTRAINT [PK_ProntoDebtorCustNotes]
		PRIMARY KEY
		CLUSTERED
		([DCN_Account], [DCN_Type], [DCN_SequenceNo])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoDebtorCustNotes] SET (LOCK_ESCALATION = TABLE)
GO
