SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoDebtorForSalesForce] (
		[AccountCode]            [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Type]                   [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[LastExportDateTime]     [datetime] NULL,
		CONSTRAINT [PK_ProntoDebtorForSalesForce]
		PRIMARY KEY
		CLUSTERED
		([AccountCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoDebtorForSalesForce] SET (LOCK_ESCALATION = TABLE)
GO
