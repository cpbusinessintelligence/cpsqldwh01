SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoDebtorV1] (
		[SysActionCode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompSource]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompCode]           [char](3) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]           [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[BillTo]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Shortname]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TermDisc]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Territory]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreditLimit]           [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[CreditLimitAmount]     [numeric](14, 2) NULL,
		[IndustryCode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustType]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MarketingFlag]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AverageDaysToPay]      [smallint] NULL,
		[IndustrySubGroup]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Locality]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepCode]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastSale]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ABN]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RepName]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmAccount]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmParent]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmType]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CrmRegion]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OriginalRepName]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoDebtor1]
		PRIMARY KEY
		CLUSTERED
		([Accountcode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoDebtorV1] SET (LOCK_ESCALATION = TABLE)
GO
