SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoDriverExclusionList_Backup] (
		[Branch]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ContractorType]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[DepotNo]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ProntoId]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[isActive]           [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[EffectiveDate]      [datetime] NULL,
		[Include_Ignore]     [varchar](10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoDriverExclusionList_Backup] SET (LOCK_ESCALATION = TABLE)
GO
