SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGLBudgets] (
		[AccountCode]          [varchar](25) COLLATE Latin1_General_CI_AS NOT NULL,
		[ProntoBudgetCode]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[PeriodName]           [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[PeriodValue]          [decimal](15, 2) NOT NULL,
		CONSTRAINT [PK_ProntoGLBudgets]
		PRIMARY KEY
		CLUSTERED
		([AccountCode], [ProntoBudgetCode], [PeriodName])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoGLBudgets] SET (LOCK_ESCALATION = TABLE)
GO
