SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGlBalance] (
		[AccountNumber]        [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountingYear]       [smallint] NOT NULL,
		[AccountingPeriod]     [smallint] NOT NULL,
		[GLBalance]            [money] NULL,
		[GLBudget]             [money] NULL,
		[TransactionDate]      [date] NULL,
		[DatabaseCode]         [varchar](3) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_ProntoGlBalance]
		PRIMARY KEY
		CLUSTERED
		([AccountNumber], [AccountingYear], [AccountingPeriod], [DatabaseCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoGlBalance] SET (LOCK_ESCALATION = TABLE)
GO
