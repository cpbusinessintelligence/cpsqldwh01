SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGlChartOfAccounts] (
		[ComponentType]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AccountType]       [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]       [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountType1]      [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoGlChartOfAccounts_1]
		PRIMARY KEY
		CLUSTERED
		([AccountCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoGlChartOfAccounts] SET (LOCK_ESCALATION = TABLE)
GO
