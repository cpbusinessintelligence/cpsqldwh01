SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGlChartOfAccountsV1] (
		[SysChangeType]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompSource]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompCode]       [char](3) COLLATE Latin1_General_CI_AS NULL,
		[ComponentType]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AccountType]       [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]       [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountType1]      [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[TaxEntityCode]     [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoGlChartOfAccounts_2]
		PRIMARY KEY
		CLUSTERED
		([AccountCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoGlChartOfAccountsV1] SET (LOCK_ESCALATION = TABLE)
GO
