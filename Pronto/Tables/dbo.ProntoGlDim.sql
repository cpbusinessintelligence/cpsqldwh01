SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGlDim] (
		[AccountCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AltKey]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BalanceSheetOrProfitLoss]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Company]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BusinessUnit]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Division]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Account]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CostCentre]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SpecialReportFlag]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha20_1]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha20_2]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MlcCode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CompanyId]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TotalLevel]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_1]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_2]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_3]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserOnlyAlpha4_4 ]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoGlDim] SET (LOCK_ESCALATION = TABLE)
GO
