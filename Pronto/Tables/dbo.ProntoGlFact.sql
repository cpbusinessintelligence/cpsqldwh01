SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGlFact] (
		[AccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Year]            [int] NOT NULL,
		[Period]          [int] NOT NULL,
		[Actual]          [money] NULL,
		[Budget]          [money] NULL,
		[ReportDate]      [date] NULL,
		[Company]         [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_ProntoGlFact]
		PRIMARY KEY
		CLUSTERED
		([AccountCode], [Year], [Period], [Company])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoGlFact] SET (LOCK_ESCALATION = TABLE)
GO
