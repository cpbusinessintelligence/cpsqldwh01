SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGlHierarchy] (
		[Id]         [uniqueidentifier] NOT NULL,
		[Parent]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Child]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoGlHierarchy] SET (LOCK_ESCALATION = TABLE)
GO
