SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoGlOther] (
		[CompanyCode]      [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL,
		[BusinessUnit]     [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL,
		[Department]       [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]      [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL,
		[PeriodID]         [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL,
		[GLBalance]        [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL,
		[GLBudget]         [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL,
		[DatabaseCode]     [nvarchar](2048) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoGlOther] SET (LOCK_ESCALATION = TABLE)
GO
