SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoNameAddressMaster] (
		[SysChangeType]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompSource]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompCode]                      [char](3) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressName]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressCompany]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressStreet]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressSuburb]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressCountry]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressAddress_6]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressAddress_7]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressPostCode]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressCountryCode]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressPhone]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressMobilePhone]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressFaxNo]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressCompanyId]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressRouteCode]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressAddressId]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressAusbarCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyDate1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyDate2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyAlpha30_1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyAlpha30_2]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyAlpha4_1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyAlpha4_2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyAum1]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressUserOnlyNum2]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressTaxProvinceCode]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressProvExemption]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressFedExemption]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressSpareAlpha20_1]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressSpareAlpha20_2]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressSpareDateTime]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressSpareAlpha4_1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressSpareAlpha4_2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NameAddressMapId]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoNameAddressMaster] SET (LOCK_ESCALATION = TABLE)
GO
