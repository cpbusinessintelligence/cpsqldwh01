SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoPurgeData_Temp] (
		[SerialNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 5]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 6]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 7]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 8]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 9]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 10]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 11]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 12]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 13]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 14]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 15]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 16]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 17]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 18]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 19]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 20]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 21]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 22]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 23]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 24]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 25]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 26]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 27]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 28]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 29]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 30]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 31]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 32]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 33]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 34]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 35]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 36]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [idx_ProntoPurgeData_Temp_SerialNumber]
	ON [dbo].[ProntoPurgeData_Temp] ([SerialNumber])
	ON [TERTIARY]
GO
ALTER TABLE [dbo].[ProntoPurgeData_Temp] SET (LOCK_ESCALATION = TABLE)
GO
