SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoRatesServiceLevelValues] (
		[ServiceCode]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EffectiveDate]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FuelOverride]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FuelPercent]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MultiOverride]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MultiPercent]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[QorW]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volumetric]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoRatesServiceLevelValues] SET (LOCK_ESCALATION = TABLE)
GO
