SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoSalesOrder] (
		[OrderNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Suffix]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CompanyCode]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerCode]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[WarehouseCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TerritoryCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SalesType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingDate]       [date] NULL,
		[AccountingPeriod]     [int] NULL,
		[NetAmount]            [money] NULL,
		[GSTAmount]            [money] NULL,
		[GrossAmount]          [money] NULL,
		[CostAmount]           [money] NULL,
		CONSTRAINT [PK_ProntoSalesOrder1112]
		PRIMARY KEY
		CLUSTERED
		([OrderNumber], [Suffix], [CompanyCode], [CustomerCode])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [_dta_index_ProntoSalesOrder_7_53575229__K7_K6_K4_K8_K1_K3_12]
	ON [dbo].[ProntoSalesOrder] ([SalesType], [TerritoryCode], [CustomerCode], [AccountingDate], [OrderNumber], [CompanyCode])
	INCLUDE ([GrossAmount])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_ProntoSalesOrder_CustomerOrderCompanyDate]
	ON [dbo].[ProntoSalesOrder] ([CustomerCode])
	INCLUDE ([OrderNumber], [CompanyCode], [AccountingDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProntoSalesOrder] SET (LOCK_ESCALATION = TABLE)
GO
