SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoSalesOrderArchive] (
		[CompanyCode]                      [varchar](3) COLLATE Latin1_General_CI_AS NOT NULL,
		[OrderNumber]                      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[BoSuffix]                         [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerCode]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OrderDate]                        [date] NULL,
		[DeliveryDate]                     [date] NULL,
		[DeliveryTime]                     [time](7) NULL,
		[EarliestDeliveryDateTime]         [datetime] NULL,
		[ActualDeliveryDate]               [date] NULL,
		[ActualDeliveryTime]               [time](7) NULL,
		[LatestActualDeliveryDateTime]     [datetime] NULL,
		[FollowUpDate]                     [date] NULL,
		[OrderPackages]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OrderWeight]                      [float] NULL,
		[ExtraReference]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[OrderStatus]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OrderReasonCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OrderTypeCode]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WarehouseCode]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WarehouseTo]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TerritoryCode]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CustomerType]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SalesTaxExemptionNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TermsDiscount]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PriceCode]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CustomerReference]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceNumber]                    [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreditNoteNumber]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceDate]                      [date] NULL,
		[ProcessingDate]                   [date] NULL,
		[PostingPeriod]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddToMailerFlag]                  [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[AutoTransferFlag]                 [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[CarrierCode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CarrierDropSeq]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNote]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OrderPriority]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PartShipmentAllowed]              [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[OrderTotalAmount]                 [money] NULL,
		[OrderedTotalCost]                 [money] NULL,
		[OrderTotalTax]                    [money] NULL,
		[OrderTotalTaxAdj]                 [money] NULL,
		[OrderTotalShippedAmount]          [money] NULL,
		[OrderTotalShippedTax]             [money] NULL,
		[OrderTotalShippedTaxAdj]          [money] NULL,
		[OrderTotalShippedCost]            [money] NULL,
		[OrderTotalCharges]                [money] NULL,
		[DateStamp]                        [date] NULL,
		[TimeStamp]                        [time](7) NULL,
		[CurrencyCode]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UserIdCode]                       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CurrencyFinalExchangeRate]        [float] NULL,
		[OrderSource]                      [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[ContactNameCode]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[MiscCode]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BatchReference]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoSalesOrderArchive]
		PRIMARY KEY
		CLUSTERED
		([CompanyCode], [OrderNumber], [BoSuffix], [InvoiceNumber])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoSalesOrderArchive] SET (LOCK_ESCALATION = TABLE)
GO
