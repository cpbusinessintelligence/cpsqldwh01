SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoSalesOrderLines] (
		[OrderNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Suffix]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LineSequence]     [float] NOT NULL,
		[CompanyCode]      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[StockCode]        [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[StockGroup]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NetAmount]        [money] NULL,
		[GSTAmount]        [money] NULL,
		[GrossAmount]      [money] NULL,
		[CostAmount]       [money] NULL,
		[Quantity]         [float] NULL,
		CONSTRAINT [PK_ProntoSalesOrderLines_1]
		PRIMARY KEY
		CLUSTERED
		([OrderNumber], [Suffix], [LineSequence], [CompanyCode], [StockCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoSalesOrderLines] SET (LOCK_ESCALATION = TABLE)
GO
