SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoSalesOrder_Archive_07] (
		[OrderNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Suffix]               [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CompanyCode]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CustomerCode]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[WarehouseCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TerritoryCode]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SalesType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountingDate]       [date] NULL,
		[AccountingPeriod]     [int] NULL,
		[NetAmount]            [money] NULL,
		[GSTAmount]            [money] NULL,
		[GrossAmount]          [money] NULL,
		[CostAmount]           [money] NULL
)
GO
ALTER TABLE [dbo].[ProntoSalesOrder_Archive_07] SET (LOCK_ESCALATION = TABLE)
GO
