SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoSerialIntegrity_Temp] (
		[SerialNumber]      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]        [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CurrentStatus]     [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Error]             [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Action]            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NewStatus]         [varchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoSerialIntegrity_Temp] SET (LOCK_ESCALATION = TABLE)
GO
