SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoService] (
		[CompanyCode]      [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceCode]      [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceName]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceGroup]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoService]
		PRIMARY KEY
		CLUSTERED
		([CompanyCode], [ServiceCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoService] SET (LOCK_ESCALATION = TABLE)
GO
