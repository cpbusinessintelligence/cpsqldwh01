SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoServiceV1] (
		[CompanyCode]      [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceCode]      [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServiceName]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ServiceGroup]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_ProntoService1]
		PRIMARY KEY
		CLUSTERED
		([CompanyCode], [ServiceCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoServiceV1] SET (LOCK_ESCALATION = TABLE)
GO
