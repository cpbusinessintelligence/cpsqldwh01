SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoStockChargesOverride] (
		[Id]                   [uniqueidentifier] NULL,
		[CouponPrefix]         [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[BranchCode]           [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[ContractorCode]       [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[RedemptionType]       [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[RedemptionAmount]     [decimal](9, 2) NOT NULL,
		[EffectiveDate]        [date] NOT NULL,
		[DateCreated]          [datetime] NULL,
		[DateUpdated]          [datetime] NULL,
		CONSTRAINT [PK_ProntoStockChargesOverride]
		PRIMARY KEY
		CLUSTERED
		([CouponPrefix], [BranchCode], [ContractorCode], [RedemptionType], [EffectiveDate])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ProntoStockChargesOverride] SET (LOCK_ESCALATION = TABLE)
GO
