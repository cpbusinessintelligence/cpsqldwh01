SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoStockSerialLinkV1] (
		[SerialNumber]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[SerialLinkType]          [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[SerialLinkCode]          [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[SerialLinkSuffix]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialLinkSeqNoTemp]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialLinkSeqNo]         [int] NULL,
		CONSTRAINT [PK_ProntoStockSerialLink1]
		PRIMARY KEY
		CLUSTERED
		([SerialNumber], [SerialLinkType], [SerialLinkCode])
	ON [TERTIARY]
)
GO
ALTER TABLE [dbo].[ProntoStockSerialLinkV1] SET (LOCK_ESCALATION = TABLE)
GO
