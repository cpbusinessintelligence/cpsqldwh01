SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoStockSerialNumber] (
		[SerialNumber]                    [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ContractNumber]                  [varchar](11) COLLATE Latin1_General_CI_AS NULL,
		[StockCode]                       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialStatus]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialSiteDesc]                  [varchar](16) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]                     [varchar](11) COLLATE Latin1_General_CI_AS NULL,
		[SerialWarrantyDate]              [date] NULL,
		[SerialContractType]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SerialActionDate]                [date] NULL,
		[SerialInstallDate]               [date] NULL,
		[SerialStockType]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialWhseCode]                  [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[SerialContractRate]              [money] NULL,
		[SerialItemCost]                  [money] NULL,
		[SerialInvoicedDate]              [date] NULL,
		[SerialInvoiceAmount]             [money] NULL,
		[SerialInvoicedFromDate]          [date] NULL,
		[SerialInvoicedToDate]            [date] NULL,
		[SerialInvoiceNumber]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialCurrentContractIncome]     [money] NULL,
		[SerialOriginalInvNumber]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialSequenceNumber]            [decimal](10, 2) NULL,
		[SerialSecondaryKey]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SerialFastAccess]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SerialTraceabilityCode]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyDate1]                [date] NULL,
		[SsnUserOnlyDate2]                [date] NULL,
		[SsnUserOnlyAlpha201]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha202]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha41]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha42]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha43]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyAlpha44]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SsnUserOnlyNum1]                 [float] NULL,
		[SsnUserOnlyNum2]                 [float] NULL,
		[SsnUserOnlyNum3]                 [float] NULL,
		[SsnUserOnlyNum4]                 [float] NULL
)
GO
ALTER TABLE [dbo].[ProntoStockSerialNumber] SET (LOCK_ESCALATION = TABLE)
GO
