SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProntoSystemTable] (
		[SysActionCode]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompSource]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysCompCode]           [char](3) COLLATE Latin1_General_CI_AS NULL,
		[SystblType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystblCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Systblsalesmtd]        [numeric](12, 2) NULL,
		[Systblsalesytd]        [numeric](12, 2) NULL,
		[Systblsalesly]         [numeric](12, 2) NULL,
		[SysDescription]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SysMoneyValue]         [numeric](19, 6) NULL,
		[SysDateLastChange]     [datetime] NULL,
		[SystblAlpha1]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystblAlpha2]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystblAlpha3]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystblAlpha41]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystblAlpha42]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystblAlpha43]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SystblAlpha44]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoSystemTable] SET (LOCK_ESCALATION = TABLE)
GO
