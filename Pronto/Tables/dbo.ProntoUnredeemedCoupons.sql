SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoUnredeemedCoupons] (
		[SerialNumber]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[StartSerialNumber]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ReturnSerialNumber]        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[TransferReference]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TransferDate]              [date] NULL,
		[LastSoldReference]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastSoldDate]              [date] NULL,
		[InsuranceCategory]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[InsuranceAmount]           [money] NULL,
		[PickupContractor]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]                [date] NULL,
		[PickupRctiDate]            [date] NULL,
		[PickupRctiReference]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupRctiAmount]          [money] NULL,
		[DeliveryContractor]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]              [date] NULL,
		[DeliveryRctiDate]          [date] NULL,
		[DeliveryRctiReference]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryRctiAmount]        [money] NULL,
		[IsRedeemed]                [int] NULL,
		[Warehouse]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Status]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[InvoiceAmount]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]               [date] NULL,
		[Prefix]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CouponNumber]              [int] NULL,
		[PerCoupon]                 [decimal](12, 2) NULL,
		[CouponType]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SoldAmount]                [money] NULL,
		[testflag]                  [int] NULL,
		CONSTRAINT [PK_ProntoUnredeemedCoupons1]
		PRIMARY KEY
		NONCLUSTERED
		([SerialNumber])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [IX_ProntoUnredeemedCoupons]
	ON [dbo].[ProntoUnredeemedCoupons] ([testflag])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProntoUnredeemedCoupons] SET (LOCK_ESCALATION = TABLE)
GO
