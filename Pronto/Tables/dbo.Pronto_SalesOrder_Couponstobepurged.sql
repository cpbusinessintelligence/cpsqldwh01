SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pronto_SalesOrder_Couponstobepurged] (
		[SerialNumber]           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[StartSerialNumber]      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LastSoldDate]           [date] NULL,
		[PickupContractor]       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]             [date] NULL,
		[PickupRctiDate]         [date] NULL,
		[DeliveryContractor]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]           [date] NULL,
		[DeliveryRctiDate]       [date] NULL,
		[RevenueDate]            [date] NULL,
		[RevenueAmount]          [money] NULL,
		[Prefix]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Type]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[accountNumber]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[IS URC]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Pronto_SalesOrder_Couponstobepurged] SET (LOCK_ESCALATION = TABLE)
GO
