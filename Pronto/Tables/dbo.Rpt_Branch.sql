SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rpt_Branch] (
		[branchname]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[branchid]       [int] NULL
)
GO
ALTER TABLE [dbo].[Rpt_Branch] SET (LOCK_ESCALATION = TABLE)
GO
