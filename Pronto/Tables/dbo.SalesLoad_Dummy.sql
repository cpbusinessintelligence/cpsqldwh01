SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesLoad_Dummy] (
		[Flat File Source.dw_sales dif]         [varchar](2000) COLLATE Latin1_General_CI_AS NULL,
		[Flat File Source.dw_sales dif (1)]     [varchar](2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SalesLoad_Dummy] SET (LOCK_ESCALATION = TABLE)
GO
