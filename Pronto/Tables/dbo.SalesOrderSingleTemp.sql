SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesOrderSingleTemp] (
		[Column 0]      [varchar](2) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]      [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]      [varchar](9) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]      [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]      [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[Column 5]      [varchar](2) COLLATE Latin1_General_CI_AS NULL,
		[Column 6]      [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[Column 7]      [varchar](7) COLLATE Latin1_General_CI_AS NULL,
		[Column 8]      [varchar](7) COLLATE Latin1_General_CI_AS NULL,
		[Column 9]      [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[Column 10]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[Column 11]     [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SalesOrderSingleTemp] SET (LOCK_ESCALATION = TABLE)
GO
