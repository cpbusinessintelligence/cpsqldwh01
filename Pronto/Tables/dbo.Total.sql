SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Total] (
		[Consignment Ref ]          [varchar](502) COLLATE Latin1_General_CI_AS NULL,
		[Pickup Pronto ID]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pickup Payment Date]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pickup Amount Ex GST]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Pronto ID]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Payment Date]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Payment]          [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Total] SET (LOCK_ESCALATION = TABLE)
GO
