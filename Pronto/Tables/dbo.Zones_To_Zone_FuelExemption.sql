SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zones_To_Zone_FuelExemption] (
		[LookupZone]             [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[FromZone]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[FromCPZone]             [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[FromState]              [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[ToCPZone]               [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[ToState]                [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[Fuel_Exemption]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ProntoFuelOverride]     [varchar](10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Zones_To_Zone_FuelExemption] SET (LOCK_ESCALATION = TABLE)
GO
