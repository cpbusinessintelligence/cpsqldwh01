SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cremaster] (
		[cre-accountcode]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-supp-accountcode]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-normal-lead-time]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-normal-carrier]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-order-types-accepted]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-order-cutoff-time]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-contract-expiry-date]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-co-op-advertising]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-returns-policy]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-warranty-policy]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-order-contact]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-orders-phone-no]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-orders-fax-no]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-last-meeting-date]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-bsb-code]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-bank-account]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-allows-back-orders]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-member-group-code]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-lodgement-reference]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-reg-buyer-phone]          [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-reg-buyer-fax]            [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-minimum-order-amt]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-supplier-spare]           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cre-long-name]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[cremaster] SET (LOCK_ESCALATION = TABLE)
GO
