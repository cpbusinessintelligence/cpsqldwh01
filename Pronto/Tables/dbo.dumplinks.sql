SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dumplinks] (
		[Column 0]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[dumplinks] SET (LOCK_ESCALATION = TABLE)
GO
