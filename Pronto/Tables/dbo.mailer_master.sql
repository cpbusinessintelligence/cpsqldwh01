SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mailer_master] (
		[bi_sys_action_code]     [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_cons_code]       [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]       [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[item_number]            [int] NULL,
		[name_index]             [char](30) COLLATE Latin1_General_CI_AS NULL,
		[street_index]           [char](20) COLLATE Latin1_General_CI_AS NULL,
		[mailer_postcode]        [char](10) COLLATE Latin1_General_CI_AS NULL,
		[postbag]                [char](4) COLLATE Latin1_General_CI_AS NULL,
		[accountcode]            [char](10) COLLATE Latin1_General_CI_AS NULL,
		[account_type]           [char](1) COLLATE Latin1_General_CI_AS NULL,
		[rep_code]               [char](3) COLLATE Latin1_General_CI_AS NULL,
		[country_code]           [char](4) COLLATE Latin1_General_CI_AS NULL,
		[mailer_std_code]        [char](8) COLLATE Latin1_General_CI_AS NULL,
		[mailer_phone_no]        [char](15) COLLATE Latin1_General_CI_AS NULL,
		[fax_country]            [char](4) COLLATE Latin1_General_CI_AS NULL,
		[mailer_fax_area]        [char](4) COLLATE Latin1_General_CI_AS NULL,
		[mailer_fax_no]          [char](15) COLLATE Latin1_General_CI_AS NULL,
		[mailer_field]           [char](20) COLLATE Latin1_General_CI_AS NULL,
		[last_amend_date]        [datetime] NULL,
		[last_amend_time]        [datetime] NULL,
		[last_sale_date]         [datetime] NULL,
		[last_mail_date]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[action_date]            [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[mail_address_1]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mail_address_2]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mail_address_3]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mail_address_4]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mail_address_5]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mail_address_6]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mail_address_7]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mailer_dupes_key]       [char](8) COLLATE Latin1_General_CI_AS NULL,
		[mailer_region]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[mailer_parent]          [int] NULL,
		[address_id]             [char](15) COLLATE Latin1_General_CI_AS NULL,
		[company_id_1]           [char](15) COLLATE Latin1_General_CI_AS NULL,
		[company_id_2]           [char](15) COLLATE Latin1_General_CI_AS NULL,
		[mm_address_code]        [char](2) COLLATE Latin1_General_CI_AS NULL,
		[user_only_date1]        [datetime] NULL,
		[user_only_date2]        [datetime] NULL,
		[only_alpha20_1]         [char](20) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha20_2]         [char](20) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha4_1]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha4_2]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha4_3]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha4_4]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[user_only_num1]         [numeric](14, 4) NULL,
		[user_only_num2]         [numeric](14, 4) NULL,
		[user_only_num3]         [numeric](14, 4) NULL,
		[user_only_num4]         [numeric](14, 4) NULL,
		[mm_ausbar_code]         [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[lead_indicator]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[spare_alpha1_1]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[spare_alpha1_2]         [char](1) COLLATE Latin1_General_CI_AS NULL,
		[spare_alpha4_1]         [char](4) COLLATE Latin1_General_CI_AS NULL,
		[spare_alpha4_2]         [char](4) COLLATE Latin1_General_CI_AS NULL,
		[spare_num1]             [numeric](14, 2) NULL,
		[spare_num2]             [numeric](14, 2) NULL,
		[spare_alpha30]          [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mm_spare_alpha20]       [char](20) COLLATE Latin1_General_CI_AS NULL,
		[date_time_1]            [datetime] NULL,
		[date_time_2]            [datetime] NULL,
		[mm_trading_name]        [char](30) COLLATE Latin1_General_CI_AS NULL,
		[mm_company_email]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[company_website]        [varchar](256) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[mailer_master] SET (LOCK_ESCALATION = TABLE)
GO
