SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[name_and_address_master] (
		[Column 0]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 5]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 6]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 7]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 8]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 9]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 10]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 11]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 12]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 13]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 14]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 15]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 16]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 17]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 18]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 19]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 20]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 21]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 22]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 23]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 24]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 25]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 26]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 27]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 28]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 29]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 30]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 31]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 32]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 33]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 34]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 35]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 36]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Column 37]     [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[name_and_address_master] SET (LOCK_ESCALATION = TABLE)
GO
