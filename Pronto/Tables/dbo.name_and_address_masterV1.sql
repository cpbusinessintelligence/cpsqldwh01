SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[name_and_address_masterV1] (
		[bi_sys_change_type]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_source]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[bi_sys_comp_code]       [char](3) COLLATE Latin1_General_CI_AS NULL,
		[accountcode]            [char](10) COLLATE Latin1_General_CI_AS NULL,
		[na_type]                [char](2) COLLATE Latin1_General_CI_AS NULL,
		[na_name]                [char](30) COLLATE Latin1_General_CI_AS NULL,
		[na_company]             [char](30) COLLATE Latin1_General_CI_AS NULL,
		[na_street]              [char](30) COLLATE Latin1_General_CI_AS NULL,
		[na_suburb]              [char](30) COLLATE Latin1_General_CI_AS NULL,
		[na_country]             [char](30) COLLATE Latin1_General_CI_AS NULL,
		[na_address_6]           [char](30) COLLATE Latin1_General_CI_AS NULL,
		[na_address_7]           [char](30) COLLATE Latin1_General_CI_AS NULL,
		[postcode]               [char](10) COLLATE Latin1_General_CI_AS NULL,
		[na_country_code]        [char](3) COLLATE Latin1_General_CI_AS NULL,
		[na_phone]               [char](15) COLLATE Latin1_General_CI_AS NULL,
		[na_phone_2]             [char](15) COLLATE Latin1_General_CI_AS NULL,
		[na_fax_no]              [char](15) COLLATE Latin1_General_CI_AS NULL,
		[na_company_id]          [char](14) COLLATE Latin1_General_CI_AS NULL,
		[na_route_code]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[na_address_id]          [char](15) COLLATE Latin1_General_CI_AS NULL,
		[na_ausbar_code]         [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[user_only_date1]        [datetime] NULL,
		[user_only_date2]        [datetime] NULL,
		[only_alpha30_1]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha30_2]         [char](30) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha4_1]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[only_alpha4_2]          [char](4) COLLATE Latin1_General_CI_AS NULL,
		[user_only_num1]         [numeric](14, 2) NULL,
		[user_only_num2]         [numeric](14, 2) NULL,
		[province_code]          [char](8) COLLATE Latin1_General_CI_AS NULL,
		[prov_exemption]         [char](15) COLLATE Latin1_General_CI_AS NULL,
		[na_fed_exemption]       [char](15) COLLATE Latin1_General_CI_AS NULL,
		[spare_alpha20_1]        [char](20) COLLATE Latin1_General_CI_AS NULL,
		[spare_alpha20_2]        [char](20) COLLATE Latin1_General_CI_AS NULL,
		[spare_date_time]        [datetime] NULL,
		[spare_alpha4_1]         [char](4) COLLATE Latin1_General_CI_AS NULL,
		[spare_alpha4_2]         [char](4) COLLATE Latin1_General_CI_AS NULL,
		[na_map_id]              [char](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[name_and_address_masterV1] SET (LOCK_ESCALATION = TABLE)
GO
