SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[name_data] (
		[Column 0]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 5]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 6]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 7]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 8]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 9]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 10]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 11]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 12]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 13]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 14]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 15]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 16]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 17]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 18]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 19]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 20]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 21]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 22]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 23]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 24]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 25]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 26]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 27]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 28]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 29]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 30]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 31]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 32]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 33]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Column 34]     [varchar](200) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[name_data] SET (LOCK_ESCALATION = TABLE)
GO
