SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[prontoAuditTable] (
		[Sno]                [int] IDENTITY(1, 1) NOT NULL,
		[TableName]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Count]              [int] NULL,
		[date]               [datetime] NOT NULL,
		[isTableUpdated]     [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[prontoAuditTable]
	ADD
	CONSTRAINT [DF__prontoAudi__date__37A611D3]
	DEFAULT (getdate()) FOR [date]
GO
ALTER TABLE [dbo].[prontoAuditTable]
	ADD
	CONSTRAINT [DF__prontoAud__isTab__389A360C]
	DEFAULT ((0)) FOR [isTableUpdated]
GO
ALTER TABLE [dbo].[prontoAuditTable] SET (LOCK_ESCALATION = TABLE)
GO
