SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redemptiontable] (
		[column1]                   [int] NULL,
		[column2]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CSY]                       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[P]                         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[_0]                        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[_01_JAN_2010_01_00_00]     [datetime2](7) NULL
)
GO
ALTER TABLE [dbo].[redemptiontable] SET (LOCK_ESCALATION = TABLE)
GO
