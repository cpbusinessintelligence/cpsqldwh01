SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContractor] (
		[DeliveryDriver]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ProntoID]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[State]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BaseAmount]         [decimal](12, 2) NULL,
		[MaxAmount]          [decimal](12, 2) NULL,
		[NewBaseAmount]      [decimal](12, 2) NULL,
		[EffectiveDate]      [datetime] NULL,
		[Status]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
CREATE CLUSTERED INDEX [idxProntoID]
	ON [dbo].[tblContractor] ([ProntoID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblContractor] SET (LOCK_ESCALATION = TABLE)
GO
