SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRCTICouponCalculationStaging] (
		[Id]                         [int] IDENTITY(1, 1) NOT NULL,
		[SerialNumber]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CouponPrefix]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Activity]                   [varchar](1) COLLATE Latin1_General_CI_AS NULL,
		[ScannedBy]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanDateTime]               [datetime] NULL,
		[ScanBranch]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RCTIAmount]                 [decimal](9, 2) NULL,
		[IsPaymentProcessed]         [bit] NULL,
		[PaymentProcessDateTime]     [datetime] NULL,
		[CreatedDateTime]            [datetime] NULL,
		[CreatedBy]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]            [datetime] NULL,
		[UpdatedBy]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblRCTICouponCalculationStaging]
	ADD
	CONSTRAINT [DF__tblRCTICo__Creat__71F2AEF9]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRCTICouponCalculationStaging]
	ADD
	CONSTRAINT [DF__tblRCTICo__Updat__72E6D332]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblRCTICouponCalculationStaging] SET (LOCK_ESCALATION = TABLE)
GO
