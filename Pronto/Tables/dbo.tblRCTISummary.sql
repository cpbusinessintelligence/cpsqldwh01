SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblRCTISummary] (
		[RCTIResultID]        [int] IDENTITY(1, 1) NOT NULL,
		[RunNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Category]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PaymentWeek]         [datetime] NULL,
		[RCTITotalAmount]     [decimal](18, 2) NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_JP_tblRCTISummary1]
		PRIMARY KEY
		CLUSTERED
		([RCTIResultID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRCTISummary] SET (LOCK_ESCALATION = TABLE)
GO
