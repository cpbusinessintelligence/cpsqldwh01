SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProntoEDIExclusionSubCodes] (
		[Sno]             [int] IDENTITY(1, 1) NOT NULL,
		[Code]            [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Type]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[WeightItem]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]     [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[PUP]             [decimal](18, 2) NULL,
		[Status]          [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tbl_ProntoEDIExclusionSubCodes] SET (LOCK_ESCALATION = TABLE)
GO
