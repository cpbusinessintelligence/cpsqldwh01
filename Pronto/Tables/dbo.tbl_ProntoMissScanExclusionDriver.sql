SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProntoMissScanExclusionDriver] (
		[nc_branch]          [int] NULL,
		[nc_contractor]      [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[nc_description]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tbl_ProntoMissScanExclusionDriver] SET (LOCK_ESCALATION = TABLE)
GO
