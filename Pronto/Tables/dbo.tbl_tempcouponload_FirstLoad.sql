SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_tempcouponload_FirstLoad] (
		[Col1]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col2]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col3]         [datetime] NULL,
		[CouponID]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col5]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col6]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col7]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col8]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col9]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Col11]        [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Col12]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col13]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Col14]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[id]           [bigint] IDENTITY(1, 1) NOT NULL
)
GO
ALTER TABLE [dbo].[tbl_tempcouponload_FirstLoad] SET (LOCK_ESCALATION = TABLE)
GO
