SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblwork] (
		[ContractorID]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RunNumber]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Base]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Max]              [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblwork] SET (LOCK_ESCALATION = TABLE)
GO
