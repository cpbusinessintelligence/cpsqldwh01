SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[CPPL_vw_TM1GLBalance]
AS
select ISNULL(LEFT(AccountNumber, 1), '') AS [CompanyCode],
ISNULL(SUBSTRING(AccountNumber, 2, 3), '') AS [BusinessUnit],
ISNULL(SUBSTRING(AccountNumber, 5, 2), '') AS [Department],
ISNULL(SUBSTRING(AccountNumber, 7, 4), '') AS [AccountCode],
CASE WHEN ap.PeriodId Is Null
	THEN CONVERT(varchar(6),
			CONVERT(varchar(4), bal.AccountingYear) + 
				RIGHT(('00' + CONVERT(varchar(2), bal.AccountingPeriod)), 2))
	ELSE ap.PeriodId
END AS [PeriodID],
ISNULL(bal.GLBalance, 0.00) AS [GLBalance],
ISNULL(bal.GLBudget, 0.00) AS [GLBudget],
bal.DatabaseCode 
FROM ProntoGlBalance bal (NOLOCK)
LEFT OUTER JOIN ProntoAccountingPeriod ap (NOLOCK)
ON bal.TransactionDate = ap.DayId 
WHERE 
 bal.TransactionDate >= '01 July 2010' AND ISNULL(LEFT(bal.AccountNumber, 1), '') in ('A','B','C','J','X','Z') AND
--LEN(AccountNumber) = 10 AND
NOT (ISNULL(bal.GLBalance, 0.00) = 0.00 AND ISNULL(bal.GLBudget, 0.00) = 0.00)
AND LEFT(ISNULL(AccountNumber, ' '), 1) != 'H'
--UNION ALL
--SELECT
--CompanyCode,
--BusinessUnit,
--Department,
--AccountCode,
--PeriodID,
--GLBalance,
--GLBudget,
--DatabaseCode 
--FROM ProntoGlOther 





GO
