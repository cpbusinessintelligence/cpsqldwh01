SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CleanseEmail](@Email varchar(100)) returns varchar(100) as
begin
Declare @Temp varchar(100)
Declare @Op bit

Select @Temp=ltrim(rtrim(@Email))
Select @Temp=replace(replace(@Temp,';',','),'[','')
Select @Temp=replace(@Temp,'mailto:','')
Return @Temp
end
GO
