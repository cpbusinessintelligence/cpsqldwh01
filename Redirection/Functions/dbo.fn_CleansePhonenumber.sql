SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CleansePhonenumber](@Phone varchar(100)) returns varchar(20) as
begin
Declare @Temp varchar(100)
Declare @Op bit

Select @Temp=ltrim(rtrim(replace(@Phone,' ','')))

Select @Temp=replace(@Temp,'+61','0')
where @Temp like '+61%'

Select @Temp=replace(@Temp,'614','04')
where @Temp like '614%'

Select @Temp=case when len(@Temp)=9 and @Temp like '4%' then '0'+@Temp else @Temp end

Return @Temp
end
GO
