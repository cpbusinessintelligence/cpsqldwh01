SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_GetEmailBody](@Body varchar(5000),@Parameter1 varchar(100),@Parameter2 varchar(800), @Parameter3 varchar(800),@Parameter4 varchar(100),@Parameter5 varchar(100),@Parameter6 varchar(500),@Parameter7 varchar(500),@Parameter8 varchar(500),@Parameter9 varchar(500),@Parameter10 varchar(500))
 returns varchar(5000) as
begin
declare @BodyOp varchar(max)
--select @BodyOp='a'

Select @BodyOp=replace(@Body,'[Parameter1]',@Parameter1)

Select @BodyOp=replace(@BodyOp,'[Parameter2]',@Parameter2)

Select @BodyOp=replace(@BodyOp,'[Parameter3]',@Parameter3)

Select @BodyOp=replace(@BodyOp,'[Parameter4]',@Parameter4)

Select @BodyOp=replace(@BodyOp,'[Parameter5]',@Parameter5)

Select @BodyOp=replace(@BodyOp, '[Parameter6]',@Parameter6)
Select @BodyOp=replace(@BodyOp, '[Parameter7]',@Parameter7)

Select @BodyOp=replace(@BodyOp, '[Parameter8]',@Parameter8)

Select @BodyOp=replace(@BodyOp, '[Parameter9]',@Parameter9)

Select @BodyOp=replace(@BodyOp, '[Parameter10]',@Parameter10)


return @BodyOp

end
GO
