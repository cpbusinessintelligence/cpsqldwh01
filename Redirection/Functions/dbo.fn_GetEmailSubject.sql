SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_GetEmailSubject](@Subject varchar(5000),@Parameter1 varchar(100),@Parameter2 varchar(800), @Parameter3 varchar(800),@Parameter4 varchar(100),@Parameter5 varchar(100),@Parameter6 varchar(500),@Parameter7 varchar(500),@Parameter8 varchar(500),@Parameter9 varchar(500),@Parameter10 varchar(500))
 returns varchar(5000) as
begin
declare @SubjectOp varchar(5000)
--select @SubjectOp='a'

Select @SubjectOp=replace(@Subject,'[Parameter1]',@Parameter1)

Select @SubjectOp=replace(@SubjectOp,'[Parameter2]',@Parameter2)

Select @SubjectOp=replace(@SubjectOp,'[Parameter3]',@Parameter3)

Select @SubjectOp=replace(@SubjectOp,'[Parameter4]',@Parameter4)

Select @SubjectOp=replace(@SubjectOp,'[Parameter5]',@Parameter5)

Select @SubjectOp=replace(@SubjectOp, '[Parameter6]',@Parameter6)
Select @SubjectOp=replace(@SubjectOp, '[Parameter7]',@Parameter7)

Select @SubjectOp=replace(@SubjectOp, '[Parameter8]',@Parameter8)

Select @SubjectOp=replace(@SubjectOp, '[Parameter9]',@Parameter9)

Select @SubjectOp=replace(@SubjectOp, '[Parameter10]',@Parameter10)


return @SubjectOp

end
GO
