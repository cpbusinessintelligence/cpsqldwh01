SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[fn_GetEmailbeforeRedirection](@Body varchar(5000),@Consignmentnumber varchar(100),@DelName varchar(800), @DeliveryAddress varchar(800),@DelContactPhone varchar(100),@DelContactEmail varchar(100),@OriginName varchar(500))
 returns varchar(5000) as
begin
declare @BodyOp varchar(max)
--select @BodyOp='a'

Select @BodyOp=replace(@Body,'[Consignment Number]',@Consignmentnumber)

Select @BodyOp=replace(@BodyOp,'[FirstName LastName/Business Name]',@DelName)

Select @BodyOp=replace(@BodyOp,'[Delivery Address]',@DeliveryAddress)

Select @BodyOp=replace(@BodyOp,'[Delivery Phone]',@DelContactPhone)

Select @BodyOp=replace(@BodyOp,'[Delivery Email]',@DelContactEmail)

Select @BodyOp=replace(@BodyOp, '[Origin FirstName LastName/Business Name]',@OriginName)

return @BodyOp

end
GO
