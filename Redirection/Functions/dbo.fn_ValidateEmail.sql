SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_ValidateEmail](@Email varchar(100)) returns bit as
begin

declare @Op bit

select @Op=case when (ltrim(rtrim(isnull(@Email,'')))<>'' ) and len( ltrim(rtrim(isnull(@Email,''))))>=5 and ltrim(rtrim(isnull(@Email,''))) like '%@%'
and (charindex(',',ltrim(rtrim(isnull(@Email,''))))<>len(ltrim(rtrim(isnull(@Email,'')))))
--and charindex(ltrim(rtrim(@Email)) ,'@')<>0 )
--and charindex(ltrim(rtrim(@Email)) ,',')=0) 
then 1 else 0 end

Return @Op

end
GO
