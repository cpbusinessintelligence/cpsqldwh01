SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_ValidatePhone](@Phone varchar(100)) returns bit as
begin

Declare @Temp varchar(100)
Declare @Op bit

Select @Temp=ltrim(rtrim(replace(@Phone,' ','')))

Select @Temp=replace(@Temp,'+61','0')
where @Temp like '+61%'

Select @Op=case when @Temp like '04%' and len(@Temp)=10 then 1 else 0 end 




Return @Op


end
GO
