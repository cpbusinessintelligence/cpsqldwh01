SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[MaintenancePlan_Redirection]
AS
Begin
	--REORGANIZE

	ALTER INDEX [IX_CustomerExceptions_AccountNumber] ON [dbo].[CustomerExceptions] REORGANIZE  



	ALTER INDEX [NonClusteredIndex-20200811-201718] ON [dbo].[FailedDeliveryCardLeft] REORGANIZE  



	ALTER INDEX [PK__FailedDe__CA1FE464DC7F3DC8] ON [dbo].[FailedDeliveryCardLeft] REORGANIZE  



	ALTER INDEX [PK__MailTemp__CA1FE464BC064C61] ON [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate] REORGANIZE  



	ALTER INDEX [IX_SendMail_TF] ON [dbo].[SendMail] REORGANIZE  



	ALTER INDEX [PK_DGSendMail] ON [dbo].[SendMail] REORGANIZE  



	ALTER INDEX [PK_DGSendMail_1] ON [dbo].[SendMailAPI] REORGANIZE  



	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REORGANIZE  



	ALTER INDEX [IX_tblParcelOnWayConsignment-20160913-094623] ON [dbo].[tblParcelOnWayConsignment] REORGANIZE  



	ALTER INDEX [NonClusteredIndex-20180208-201032] ON [dbo].[tblParcelOnWayConsignment] REORGANIZE  



	ALTER INDEX [PK_tblParcelOnWayConsignment] ON [dbo].[tblParcelOnWayConsignment] REORGANIZE  



	ALTER INDEX [PK__tblParce__3214EC273CD0A534] ON [dbo].[tblParcelOnWayLabel] REORGANIZE  



	ALTER INDEX [IX_tblRedirectConsignment-20160916-111432] ON [dbo].[tblRedirectConsignment] REORGANIZE  



	ALTER INDEX [PK_tblRedirectConsignment] ON [dbo].[tblRedirectConsignment] REORGANIZE  



	ALTER INDEX [UQ__tblRedir__A2A2BAAB9FC6890D] ON [dbo].[tblRedirectConsignment] REORGANIZE  



	ALTER INDEX [PK_tblredirectionaccounts] ON [dbo].[tblRedirectionAccounts] REORGANIZE  



	ALTER INDEX [PK_tblRedirectedItemLabel] ON [dbo].[tblRedirectItem] REORGANIZE  


	--REBUILD



	ALTER INDEX [IX_CustomerExceptions_AccountNumber] ON [dbo].[CustomerExceptions] REBUILD  



	ALTER INDEX [NonClusteredIndex-20200811-201718] ON [dbo].[FailedDeliveryCardLeft] REBUILD  



	ALTER INDEX [PK__FailedDe__CA1FE464DC7F3DC8] ON [dbo].[FailedDeliveryCardLeft] REBUILD  



	ALTER INDEX [PK__MailTemp__CA1FE464BC064C61] ON [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate] REBUILD  



	ALTER INDEX [IX_SendMail_TF] ON [dbo].[SendMail] REBUILD  



	ALTER INDEX [PK_DGSendMail] ON [dbo].[SendMail] REBUILD  



	ALTER INDEX [PK_DGSendMail_1] ON [dbo].[SendMailAPI] REBUILD  



	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REBUILD  



	ALTER INDEX [IX_tblParcelOnWayConsignment-20160913-094623] ON [dbo].[tblParcelOnWayConsignment] REBUILD  



	ALTER INDEX [NonClusteredIndex-20180208-201032] ON [dbo].[tblParcelOnWayConsignment] REBUILD  



	ALTER INDEX [PK_tblParcelOnWayConsignment] ON [dbo].[tblParcelOnWayConsignment] REBUILD  



	ALTER INDEX [PK__tblParce__3214EC273CD0A534] ON [dbo].[tblParcelOnWayLabel] REBUILD  



	ALTER INDEX [IX_tblRedirectConsignment-20160916-111432] ON [dbo].[tblRedirectConsignment] REBUILD  



	ALTER INDEX [PK_tblRedirectConsignment] ON [dbo].[tblRedirectConsignment] REBUILD  



	ALTER INDEX [UQ__tblRedir__A2A2BAAB9FC6890D] ON [dbo].[tblRedirectConsignment] REBUILD  



	ALTER INDEX [PK_tblredirectionaccounts] ON [dbo].[tblRedirectionAccounts] REBUILD  



	ALTER INDEX [PK_tblRedirectedItemLabel] ON [dbo].[tblRedirectItem] REBUILD  

	
	---Update Stat


	UPDATE STATISTICS [dbo].[CustomerExceptions] 
	



	UPDATE STATISTICS [dbo].[FailedDeliveryCardLeft] 
	



	UPDATE STATISTICS [dbo].[FailedDeliveryCardLeft_Archive20200811] 
	



	UPDATE STATISTICS [dbo].[MailTemplates] 
	


	

	UPDATE STATISTICS [dbo].[scannerLogIssue] 
	



	UPDATE STATISTICS [dbo].[SendMail] 
	



	UPDATE STATISTICS [dbo].[SendMail_Archive] 
	



	UPDATE STATISTICS [dbo].[SendMailAPI] 
	



	UPDATE STATISTICS [dbo].[SendMailAPI_Errors] 
	



	UPDATE STATISTICS [dbo].[SendMailAPIResponse] 
	



	UPDATE STATISTICS [dbo].[SendmailAPITesting] 
	



	UPDATE STATISTICS [dbo].[SendMailTest] 
	



	UPDATE STATISTICS [dbo].[tblErrorLog] 
	



	UPDATE STATISTICS [dbo].[tblParcelOnWayConsignment] 
	



	UPDATE STATISTICS [dbo].[tblParcelOnWayConsignment_Archive] 
	



	UPDATE STATISTICS [dbo].[tblParcelOnWayLabel] 
	



	UPDATE STATISTICS [dbo].[tblParcelOnWayLabel_Archive] 
	



	UPDATE STATISTICS [dbo].[tblRedirectConsignment] 
	



	UPDATE STATISTICS [dbo].[tblRedirectConsignment_Archive] 
	



	UPDATE STATISTICS [dbo].[tblRedirectConsignment_Archive2016-2017] 
	



	UPDATE STATISTICS [dbo].[tblRedirectConsignment_Archive20170601] 
	



	UPDATE STATISTICS [dbo].[tblRedirectionAccounts] 
	



	UPDATE STATISTICS [dbo].[tblRedirectionPostcodes] 
	



	UPDATE STATISTICS [dbo].[tblRedirectItem] 
	



	UPDATE STATISTICS [dbo].[tblRedirectItem_Archive] 
	



	UPDATE STATISTICS [dbo].[tblRedirectItem_Archive2016-2017] 
	



	UPDATE STATISTICS [dbo].[tblRedirectItem_Archive20170601] 
	



	UPDATE STATISTICS [dbo].[TempSendMailStatusError] 
	

	--Shrink DB
	DBCC SHRINKDATABASE(N'Redirection')

END
GO
