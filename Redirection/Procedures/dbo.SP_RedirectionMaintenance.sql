SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SP_RedirectionMaintenance]

@StartDate datetime = null --'2015-05-28'

As 
Begin
Begin Try
BEGIN TRAN 

--EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

		PRINT '-------- START DISABLING TABLE CONSTRAINT --------';

		ALTER TABLE tblRedirectConsignment NOCHECK CONSTRAINT ALL
		ALTER TABLE tblRedirectItem NOCHECK CONSTRAINT ALL
		

		PRINT '-------- END DISABLING TABLE CONSTRAINT --------';

				PRINT '-------- START COPYING DATA--------';

				PRINT '-------- COPY REDIRECT CONSIGNMENT TABLE--------';

				--SELECT count(*) FROM tblRedirectConsignment with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				INSERT INTO tblRedirectConsignment_Archive 
				select * from tblRedirectConsignment with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				SELECT count(*) FROM tblRedirectConsignment_Archive with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				PRINT '-------- COPY REDIRECT LABEL --------';

				--SELECT count(*) FROM tblRedirectItem with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				INSERT INTO tblRedirectItem_Archive 
				select * from tblRedirectItem with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				SELECT count(*) FROM tblRedirectItem_Archive with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 



				
				PRINT '-------- END COPYING DATA --------';

		----Delete Record from table---------------------

				PRINT '-------- DELETE DATA START --------';


				Delete from tblRedirectConsignment where convert(Date, CreatedDateTime, 103) <= @StartDate 
		
				Delete from tblRedirectItem where convert(Date,CreatedDateTime, 103) <= @StartDate 

				PRINT '-------- DELETE DATA END --------';

		--EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
		PRINT '-------- START ENABLING TABLE CONSTRAINT --------';

		ALTER TABLE tblRedirectConsignment CHECK CONSTRAINT ALL
		ALTER TABLE tblRedirectItem CHECK CONSTRAINT ALL

		PRINT '-------- END ENABLING TABLE CONSTRAINT --------';
		select 'OK' as Result

		COMMIT TRAN 
		--rollback tran
		END TRY
				BEGIN CATCH
				begin
					rollback tran
					INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
					 VALUES
						   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
						   ,'SP_RedirectionMaintenance', 2)

						   select cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as Result
				end
				END CATCH

END 
GO
