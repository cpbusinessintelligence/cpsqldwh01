SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_ArchiveProceedure] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveEventDate = @ArchiveDate
	

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION trnRedirection
	

INSERT INTO [dbo].tblRedirectConsignment_Archive([ID]
      ,[UniqueID]
      ,[ConsignmentId]
      ,[ConsignmentCode]
      ,[OriginBusinessName]
      ,[OriginFirstName]
      ,[OriginLastName]
      ,[OriginAddressLine1]
      ,[OriginAddressLine2]
      ,[OriginAddressLine3]
      ,[OriginSuburb]
      ,[OriginPostcode]
      ,[OriginState]
      ,[OriginContactPhone]
      ,[OriginContactEmail]
      ,[DelBusinessName]
      ,[DelFirstName]
      ,[DelLastName]
      ,[DelAddressLine1]
      ,[DelAddressLine2]
      ,[DelAddressLine3]
      ,[DelSuburb]
      ,[DelPostcode]
      ,[DelState]
      ,[DelContactPhone]
      ,[DelContactEmail]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[RateCardID]
      ,[CurrentETAFrom]
      ,[CurrentETATo]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[SortCode]
      ,[SourceReference]
      ,[ReadyforRedirection]
      ,[ReadytoSendMail]
      ,[ReadytoSendSMS]
      ,[PickupDateTime]
      ,[OnBoardDateTime]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[OriginZone]
      ,[DestinationZone]
      ,[ETA]
      ,[1stPickupcutoff]
      ,[RoadExpressPickupCutoffflag]
      ,[Account])
            
SELECT  [ID]
      ,[UniqueID]
      ,[ConsignmentId]
      ,[ConsignmentCode]
      ,[OriginBusinessName]
      ,[OriginFirstName]
      ,[OriginLastName]
      ,[OriginAddressLine1]
      ,[OriginAddressLine2]
      ,[OriginAddressLine3]
      ,[OriginSuburb]
      ,[OriginPostcode]
      ,[OriginState]
      ,[OriginContactPhone]
      ,[OriginContactEmail]
      ,[DelBusinessName]
      ,[DelFirstName]
      ,[DelLastName]
      ,[DelAddressLine1]
      ,[DelAddressLine2]
      ,[DelAddressLine3]
      ,[DelSuburb]
      ,[DelPostcode]
      ,[DelState]
      ,[DelContactPhone]
      ,[DelContactEmail]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[RateCardID]
      ,[CurrentETAFrom]
      ,[CurrentETATo]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[SortCode]
      ,[SourceReference]
      ,[ReadyforRedirection]
      ,[ReadytoSendMail]
      ,[ReadytoSendSMS]
      ,[PickupDateTime]
      ,[OnBoardDateTime]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[OriginZone]
      ,[DestinationZone]
      ,[ETA]
      ,[1stPickupcutoff]
      ,[RoadExpressPickupCutoffflag]
      ,[Account] from [dbo].[tblRedirectConsignment] WHERE Createddatetime<=@ArchiveEventDate

	
INSERT INTO [dbo].tblRedirectItem_Archive([ItemID]
      ,[ConsignmentID]
      ,[LabelNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[CubicWeight]
      ,[PhysicalWeight]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy])
            
SELECT [ItemID]
      ,[ConsignmentID]
      ,[LabelNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[CubicWeight]
      ,[PhysicalWeight]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
  FROM [Redirection].[dbo].[tblRedirectItem] where Createddatetime <=@ArchiveEventDate




	DELETE FROM [tblRedirectItem] WHERE Createddatetime<=@ArchiveEventDate

		DELETE FROM [tblRedirectConsignment] WHERE Createddatetime<=@ArchiveEventDate



	COMMIT TRANSACTION trnRedirection

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------



GO
