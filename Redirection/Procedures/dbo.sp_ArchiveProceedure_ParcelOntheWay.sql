SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_ArchiveProceedure_ParcelOntheWay] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveEventDate = @ArchiveDate
	

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION trnRedirection
	
INSERT INTO [dbo].[tblParcelOnWayConsignment_Archive]
           ([ID]
           ,[ConsignmentID]
           ,[ConsignmentNumber]
           ,[AccountNumber]
           ,[DeliveryBusinessName]
           ,[DeliveryAddress1]
           ,[DeliveryAddress2]
           ,[DeliveryAddress3]
           ,[DeliverySuburb]
           ,[DeliveryPostcode]
           ,[DeliveryState]
           ,[DeliveryEmail]
           ,[DeliveryPhone]
           ,[PickupDatetime]
           ,[Sourcereference]
           ,[ReadytoSendMail]
           ,[ReadytoSendSMS]
           ,[CreatedDatetime]
           ,[CreatedBy]
           ,[EditedDatetime]
           ,[EditedBy])
            
SELECT [ID]
      ,[ConsignmentID]
      ,[ConsignmentNumber]
      ,[AccountNumber]
      ,[DeliveryBusinessName]
      ,[DeliveryAddress1]
      ,[DeliveryAddress2]
      ,[DeliveryAddress3]
      ,[DeliverySuburb]
      ,[DeliveryPostcode]
      ,[DeliveryState]
      ,[DeliveryEmail]
      ,[DeliveryPhone]
      ,[PickupDatetime]
      ,[Sourcereference]
      ,[ReadytoSendMail]
      ,[ReadytoSendSMS]
      ,[CreatedDatetime]
      ,[CreatedBy]
      ,[EditedDatetime]
      ,[EditedBy]
  FROM [dbo].[tblParcelOnWayConsignment] WHERE Createddatetime<=@ArchiveEventDate

	
INSERT INTO [dbo].[tblParcelOnWayLabel_Archive]
           ([ID]
           ,[ConID]
           ,[LabelNumber]
           ,[CreatedDatetime]
           ,[CreatedBy]
           ,[EditedDatetime]
           ,[EditedBy])
            
SELECT  [ID]
      ,[ConID]
      ,[LabelNumber]
      ,[CreatedDatetime]
      ,[CreatedBy]
      ,[EditedDatetime]
      ,[EditedBy]
  FROM [Redirection].[dbo].[tblParcelOnWayLabel] where Createddatetime <=@ArchiveEventDate




	DELETE FROM [tblParcelOnWayLabel] WHERE Createddatetime<=@ArchiveEventDate

		DELETE FROM [tblParcelOnWayConsignment] WHERE Createddatetime<=@ArchiveEventDate



	COMMIT TRANSACTION trnRedirection

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------



GO
