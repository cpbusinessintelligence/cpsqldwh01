SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Archive_SendMail]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min([AddDatetime]) from [dbo].[SendMail]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[SendMail_Archive]
	SELECT [SendMailID]
      ,[ContextID]
      ,[FromAddr]
      ,[ToAddr]
      ,[Bcc]
      ,[Subject]
      ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
      ,[Parameter5]
      ,[Parameter6]
      ,[Parameter7]
      ,[Parameter8]
      ,[Parameter9]
      ,[Parameter10]
      ,[TransmitFlag]
      ,[AddWho]
      ,[AddDateTime]
      ,[ModifiedDateTime]
      ,[iserror]
      ,[ReturnAPICode]
  FROM [Redirection].[dbo].[SendMail] (nolock) where AddDateTime >= @MinDate and  AddDateTime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [Redirection].[dbo].[SendMail] where  AddDateTime >= @MinDate and  AddDateTime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
