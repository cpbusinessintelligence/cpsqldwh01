SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Archive_tblRedirectConsignment]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(CreatedDateTime) from [dbo].[tblRedirectConsignment]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 180,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[tblRedirectConsignment_Archive]
	select
	[ID]
      ,[UniqueID]
      ,[ConsignmentId]
      ,[ConsignmentCode]
      ,[OriginBusinessName]
      ,[OriginFirstName]
      ,[OriginLastName]
      ,[OriginAddressLine1]
      ,[OriginAddressLine2]
      ,[OriginAddressLine3]
      ,[OriginSuburb]
      ,[OriginPostcode]
      ,[OriginState]
      ,[OriginContactPhone]
      ,[OriginContactEmail]
      ,[DelBusinessName]
      ,[DelFirstName]
      ,[DelLastName]
      ,[DelAddressLine1]
      ,[DelAddressLine2]
      ,[DelAddressLine3]
      ,[DelSuburb]
      ,[DelPostcode]
      ,[DelState]
      ,[DelContactPhone]
      ,[DelContactEmail]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[RateCardID]
      ,[CurrentETAFrom]
      ,[CurrentETATo]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[SortCode]
      ,[SourceReference]
      ,[ReadyforRedirection]
      ,[ReadytoSendMail]
      ,[ReadytoSendSMS]
      ,[PickupDateTime]
      ,[OnBoardDateTime]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
      ,[OriginZone]
      ,[DestinationZone]
      ,[ETA]
      ,[1stPickupcutoff]
      ,[RoadExpressPickupCutoffflag]
      ,[Account]
  FROM [Redirection].[dbo].[tblRedirectConsignment] (nolock) where CreatedDateTime >= @MinDate and  CreatedDateTime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [Redirection].[dbo].[tblRedirectConsignment] where  CreatedDateTime >= @MinDate and  CreatedDateTime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
