SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Archive_tblRedirectItem]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min(CreatedDateTime) from [dbo].[tblRedirectItem]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 180,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[tblRedirectItem_Archive]
	select
	[ItemID]
      ,[ConsignmentID]
      ,[LabelNumber]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[CubicWeight]
      ,[PhysicalWeight]
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy]
  FROM [Redirection].[dbo].[tblRedirectItem] (nolock) where CreatedDateTime >= @MinDate and  CreatedDateTime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [Redirection].[dbo].[tblRedirectItem] where  CreatedDateTime >= @MinDate and  CreatedDateTime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
