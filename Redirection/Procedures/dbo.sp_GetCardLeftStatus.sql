SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[sp_GetCardLeftStatus] as
begin



  --'=====================================================================
    --' CP -Stored Procedure - [[sp_GetCardLeftCards]]
    --' ---------------------------
    --' Purpose: Update Redelivery Card Tracking-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 02 Aug 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 02/08/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

Insert into FailedDeliveryCardLeft
Select  distinct  Sourcereference as Labelnumber,
       replace(additionaltext1,'Link Coupon ','') as CardLeft , 
	  -- eventdatetime as StatusDateTime,
	   case when ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon %RTCNA' or isnull(additionaltext1,'') like 'NHCLC%') then 'Hubbed'
	         when isnull(additionaltext1,'') like 'Link Coupon %SLCNA' then 'POPStation'
			 when  ( isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon %DPCNA')  then 'Depot' else '' end as Category,
		cd_connote as Connote,
		[cd_delivery_addr0] as DeliveryBusinessName,

		case when isnull(cd_delivery_contact,'')=''  then [cd_delivery_addr0] else  isnull(cd_delivery_contact,'') end as DeliveryContact,
	[cd_delivery_addr1] as DeliveryAddr1,
    [cd_delivery_addr2] as DeliveryAddr2,
    [cd_delivery_addr3] as DeliveryAddr3,
    [cd_delivery_suburb] as DeliverySuburb,
    [cd_delivery_postcode] as DeliveryPostcode,
	case when b1.b_name='Sydney' then 'NSW'
	     when b1.b_name='Brisbane' then 'QLD'
		 when b1.b_name='Gold Coast' then 'QLD'
		 when b1.b_name='Melbourne' then  Case When (cd_delivery_postcode like '7%') Then 'TAS' Else 'VIC' End
		 when b1.b_name='Perth' then 'WA'
		 when b1.b_name='Canberra' then 'NSW'
		 when b1.b_name='Adelaide' then Case When (cd_delivery_postcode like '8%' or cd_delivery_postcode like '08%') Then 'NT' Else 'SA' End
		 else 'Unknown' end as DeliveryState,
		 [dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) as DeliveryContactPhone,
	     [dbo].[fn_CleanseEmail](cd_delivery_email) as  DeliveryEmail,
		 0 as ReadytoSendMail,
		 0 as ReadytoSendSMS,
		 getdate() as Createddatetime,
		 'Admin' as CreatedBy,
		 getdate() as Editeddatetime,
		 'Admin' as EditedBy,
		 cd_account
 from scannergateway.dbo.trackingevent t (NOLOCK)  join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=sourcereference
                                                  join cpplEDI.dbo.consignment(NOLOCK) on cd_id=cc_consignment
												  join cpplEDI.dbo.branchs b1 on b1.b_id=cd_deliver_branch
 where  eventtypeid in ('A341A7FC-3E0E-4124-B16E-6569C5080C6D','47CFA05F-3897-4F1F-BDF4-00C6A69152E3') and ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon%CNA' or isnull(additionaltext1,'') like 'NHCLC%')
and convert(date,eventdatetime)>=convert(date,dateadd(day,-1,getdate())) and ([dbo].[fn_ValidatePhone]([dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1 or [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](cd_delivery_email))=1 )
and Sourcereference not like 'CPW%' and sourcereference not in (Select labelnumber from FailedDeliveryCardLeft)
and t.eventdatetime>='2016-08-11 16:30:00.000' 
--and Sourcereference in ('CPA25VS0000046','CPA25VS0000047','CPA25VS0000049','CPA25VS0000050','CPA25VS0000051','CPA25VS0000052','CPA25VS0000053','CPA25VS0000054')



Insert into FailedDeliveryCardLeft
Select distinct  Sourcereference as Labelnumber,
       replace(additionaltext1,'Link Coupon ','') as CardLeft , 
	 --  eventdatetime as StatusDateTime,
	   case when ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon %RTCNA' or isnull(additionaltext1,'') like 'NHCLC%') then 'Hubbed'
	        when isnull(additionaltext1,'') like 'Link Coupon %SLCNA' then 'POPStation'
			when  ( isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon %DPCNA')  then 'Depot' else '' end as Category,
	   consignmentcode as Connote,
	   a1.[CompanyName] as   DeliveryBusinessName,
	   a1.FirstNAme+ ' '+a1.LastName as DeliveryContact,
	   a1.[Address1] as DeliveryAddress1,
         a1.[Address2] as DeliveryAddress2,
	   '' as DeliveryAddress3,
       a1.Suburb as DeliverySuburb,
       a1.[PostCode] as DeliveryPostcode ,
	   a1.[StateName] as DeliveryState,
       [dbo].[fn_CleansePhonenumber](a1.mobile) as DeliveryContactPhone,
	  [dbo].[fn_CleanseEmail]( a1.Email) as  DeliveryEmail,
	    0 as ReadytoSendMail,
		 0 as ReadytoSendSMS,
		 getdate() as Createddatetime,
		 'Admin' as CreatedBy,
		 getdate() as Editeddatetime,
		 'Admin' as EditedBy,
		 AccountNumber

 from scannergateway.dbo.trackingevent t (NOLOCK)  join ezyfreight.dbo.tblitemlabel l(NOLOCK) on l.labelnumber=sourcereference
                                                  join ezyfreight.dbo.tblconsignment c (NOLOCK)  on c.consignmentid=l.consignmentid
                                                  join ezyfreight.dbo.tbladdress a1 (NOLOCK) on c.destinationid=a1.addressid
												  left join ezyfreight.dbo.tblcompanyusers u on u.userid=c.userid
												  left join ezyfreight.dbo.tblcompany c1 on c1.companyid=u.companyid
 where  eventtypeid in ('A341A7FC-3E0E-4124-B16E-6569C5080C6D','47CFA05F-3897-4F1F-BDF4-00C6A69152E3') and ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon%CNA' or isnull(additionaltext1,'') like 'NHCLC%')
and convert(date,eventdatetime)>=convert(date,dateadd(day,-1,getdate())) and  ([dbo].[fn_ValidatePhone]([dbo].[fn_CleansePhonenumber](a1.mobile))=1  or [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](a1.Email))=1 )
and Sourcereference  like 'CPW%' and ratecardid not like 'EXP%' and ratecardid not like 'SAV%' and sourcereference not in (Select labelnumber from FailedDeliveryCardLeft)
and t.eventdatetime>='2016-08-11 16:30:00.000' 
end

GO
