SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetCardLeftStatus_20200924_PV_UpdateStateNT] as
begin

  --'=====================================================================
    --' CP -Stored Procedure - [[sp_GetCardLeftCards]]
    --' ---------------------------
    --' Purpose: Update Redelivery Card Tracking-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 02 Aug 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 02/08/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

Insert into FailedDeliveryCardLeft
Select  distinct  Sourcereference as Labelnumber,
       replace(additionaltext1,'Link Coupon ','') as CardLeft , 
	  -- eventdatetime as StatusDateTime,
	   case when ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon %RTCNA' or isnull(additionaltext1,'') like 'NHCLC%') then 'Hubbed'
	         when isnull(additionaltext1,'') like 'Link Coupon %SLCNA' then 'POPStation'
			 when  ( isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon %DPCNA')  then 'Depot' else '' end as Category,
		cd_connote as Connote,
		[cd_delivery_addr0] as DeliveryBusinessName,

		case when isnull(cd_delivery_contact,'')=''  then [cd_delivery_addr0] else  isnull(cd_delivery_contact,'') end as DeliveryContact,
	[cd_delivery_addr1] as DeliveryAddr1,
    [cd_delivery_addr2] as DeliveryAddr2,
    [cd_delivery_addr3] as DeliveryAddr3,
    [cd_delivery_suburb] as DeliverySuburb,
    [cd_delivery_postcode] as DeliveryPostcode,
	case when b1.b_name='Sydney' then 'NSW'
	     when b1.b_name='Brisbane' then 'QLD'
		 when b1.b_name='Gold Coast' then 'QLD'
		 when b1.b_name='Melbourne' then 'VIC'
		 when b1.b_name='Perth' then 'WA'
		 when b1.b_name='Canberra' then 'NSW'
		 when b1.b_name='Adelaide' then 'SA'
		 else 'Unknown' end as DeliveryState,
		 [dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) as DeliveryContactPhone,
	     [dbo].[fn_CleanseEmail](cd_delivery_email) as  DeliveryEmail,
		 0 as ReadytoSendMail,
		 0 as ReadytoSendSMS,
		 getdate() as Createddatetime,
		 'Admin' as CreatedBy,
		 getdate() as Editeddatetime,
		 'Admin' as EditedBy,
		 cd_account
 from scannergateway.dbo.trackingevent t (NOLOCK)  join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=sourcereference
                                                  join cpplEDI.dbo.consignment(NOLOCK) on cd_id=cc_consignment
												  join cpplEDI.dbo.branchs b1 on b1.b_id=cd_deliver_branch
 where  eventtypeid in ('A341A7FC-3E0E-4124-B16E-6569C5080C6D','47CFA05F-3897-4F1F-BDF4-00C6A69152E3') and ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon%CNA' or isnull(additionaltext1,'') like 'NHCLC%')
and convert(date,eventdatetime)>=convert(date,dateadd(day,-1,getdate())) and ([dbo].[fn_ValidatePhone]([dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1 or [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](cd_delivery_email))=1 )
and Sourcereference not like 'CPW%' and sourcereference not in (Select labelnumber from FailedDeliveryCardLeft)
and t.eventdatetime>='2016-08-11 16:30:00.000' 
--and Sourcereference in ('CPA25VS0000046','CPA25VS0000047','CPA25VS0000049','CPA25VS0000050','CPA25VS0000051','CPA25VS0000052','CPA25VS0000053','CPA25VS0000054')



Insert into FailedDeliveryCardLeft
Select distinct  Sourcereference as Labelnumber,
       replace(additionaltext1,'Link Coupon ','') as CardLeft , 
	 --  eventdatetime as StatusDateTime,
	   case when ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon %RTCNA' or isnull(additionaltext1,'') like 'NHCLC%') then 'Hubbed'
	        when isnull(additionaltext1,'') like 'Link Coupon %SLCNA' then 'POPStation'
			when  ( isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon %DPCNA')  then 'Depot' else '' end as Category,
	   consignmentcode as Connote,
	   a1.[CompanyName] as   DeliveryBusinessName,
	   a1.FirstNAme+ ' '+a1.LastName as DeliveryContact,
	   a1.[Address1] as DeliveryAddress1,
         a1.[Address2] as DeliveryAddress2,
	   '' as DeliveryAddress3,
       a1.Suburb as DeliverySuburb,
       a1.[PostCode] as DeliveryPostcode ,
	   a1.[StateName] as DeliveryState,
       [dbo].[fn_CleansePhonenumber](a1.mobile) as DeliveryContactPhone,
	  [dbo].[fn_CleanseEmail]( a1.Email) as  DeliveryEmail,
	    0 as ReadytoSendMail,
		 0 as ReadytoSendSMS,
		 getdate() as Createddatetime,
		 'Admin' as CreatedBy,
		 getdate() as Editeddatetime,
		 'Admin' as EditedBy,
		 AccountNumber

 from scannergateway.dbo.trackingevent t (NOLOCK)  join ezyfreight.dbo.tblitemlabel l(NOLOCK) on l.labelnumber=sourcereference
                                                  join ezyfreight.dbo.tblconsignment c (NOLOCK)  on c.consignmentid=l.consignmentid
                                                  join ezyfreight.dbo.tbladdress a1 (NOLOCK) on c.destinationid=a1.addressid
												  left join ezyfreight.dbo.tblcompanyusers u on u.userid=c.userid
												  left join ezyfreight.dbo.tblcompany c1 on c1.companyid=u.companyid
 where  eventtypeid in ('A341A7FC-3E0E-4124-B16E-6569C5080C6D','47CFA05F-3897-4F1F-BDF4-00C6A69152E3') and ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon%CNA' or isnull(additionaltext1,'') like 'NHCLC%')
and convert(date,eventdatetime)>=convert(date,dateadd(day,-1,getdate())) and  ([dbo].[fn_ValidatePhone]([dbo].[fn_CleansePhonenumber](a1.mobile))=1  or [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](a1.Email))=1 )
and Sourcereference  like 'CPW%' and ratecardid not like 'EXP%' and ratecardid not like 'SAV%' and sourcereference not in (Select labelnumber from FailedDeliveryCardLeft)
and t.eventdatetime>='2016-08-11 16:30:00.000' 



BEGIN TRAN INSERTMAIL

Select *
into #temp from FailedDeliveryCardLeft where ReadytoSendMail=0
and  isnull(AccountNumber,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=case when [Category]='POPStation' then 6
                                                                                                                        when [Category]='Hubbed' then 5
																														when [Category]='Depot' then 7 else 0 end  and isactive=1 )
and [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](DeliveryEmail))=1 and convert(date,createddatetime)>=convert(Date,dateadd(day,-1,getdate()))


Insert into SendMail([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
	  ,[parameter2]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])

Select  5,
       'noreply@couriersplease.com.au',
	   [dbo].[fn_CleanseEmail](DeliveryEmail),
	   'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   --'kirsty.tuffley@couriersplease.com.au,hayat.horma@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	  -- 'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   0,
	   Labelnumber,
	   [dbo].[fn_CleanseEmail](DeliveryEmail),
	   'Admin',
	   getdate(),
	   getdate()
from #temp where Category='Hubbed'  

Insert into SendMail([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
	  ,[parameter2]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])

Select  6,
       'noreply@couriersplease.com.au',
	   [dbo].[fn_CleanseEmail](DeliveryEmail),
	    'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	  -- 'kirsty.tuffley@couriersplease.com.au,hayat.horma@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	  --'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   0,
	   Labelnumber,
	   [dbo].[fn_CleanseEmail](DeliveryEmail),
	   'Admin',
	   getdate(),
	   getdate()
from #temp where Category='POPStation'


--Declare @NewChar char(2)=CHAR(13) + CHAR(10)
--print 'a'+@Newchar+'b'

Insert into SendMail([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
	  ,[Parameter2]
	  ,[Parameter3]
	  ,[parameter4]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])

Select  7,
       'noreply@couriersplease.com.au',
	   [dbo].[fn_CleanseEmail](DeliveryEmail),
	    'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   --'kirsty.tuffley@couriersplease.com.au,hayat.horma@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	  -- 'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   0,
	   Labelnumber,
	   CardLeft,
	  -- [DeliveryContact]+'<br/>'+DeliveryAddr1+'<br/>'+DeliveryAddr2+'<br/>'+case when isnull(DeliveryAddr3,'')='' then DeliverySuburb else isnull(DeliveryAddr3,'') end +'<br/>'+case when isnull(DeliveryAddr3,'')='' then DeliveryState else DeliverySuburb end+' '+DeliveryState+' '+DeliveryPostcode
	  isnull(DeliveryBusinessName,'')+'<br/>'+isnull(DeliveryAddr1,'')+'<br/>'+isnull(DeliveryAddr2,'')+'<br/>'+isnull(DeliveryAddr3,'')+'<br/>'+isnull(DeliverySuburb,'')+' '+isnull(DeliveryState,'')+' '+isnull(DeliveryPostcode,''),
	  -- case when isnull(DeliveryBusinessName,'')='' then DeliveryContact else isnull(DeliveryBusinessName,'') end +'<br/>'+DeliveryAddr1+'<br/>'+DeliveryAddr2+'<br/>'+DeliveryAddr3+'<br/>'+DeliverySuburb+' '+DeliveryState+' '+DeliveryPostcode,
	   [dbo].[fn_CleanseEmail](DeliveryEmail),
	   'Admin',
	   getdate(),
	   getdate()
from #temp where Category='Depot'  

--and labelnumber='056446074580'

Update FailedDeliveryCardLeft set ReadytoSendMail=1 from #temp where #temp.sno=FailedDeliveryCardLeft.sno

COMMIT TRAN INSERTMAIL

BEGIN TRAN INSERTSMS

Select  *
into #temp1 from FailedDeliveryCardLeft where ReadytoSendSMS=0 
and  isnull(AccountNumber,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=case when [Category] = 'POPStation' then 16
                                                                                                                                   when [Category] = 'HUBBED' then 17                                   
																														           when [Category]='Depot' then 14 else 0 end  and isactive=1  )
and [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](DeliveryEmail))=0 and [dbo].[fn_ValidatePhone]([dbo].[fn_CleansePhonenumber]([DeliveryContactPhone]))=1 and convert(date,createddatetime)>=convert(Date,dateadd(day,-1,getdate()))

Insert into SendMail([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
	  ,[Parameter2]
    ,[parameter3]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])

Select   14,
       'noreply@couriersplease.com.au',
	    Redirection.dbo.[fn_CleansePhonenumber]([DeliveryContactPhone])+'@preview.pcsms.com.au',
	    'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   0,
	   Labelnumber,
     CardLeft,
     Redirection.dbo.[fn_CleansePhonenumber]([DeliveryContactPhone]),
	   'Admin',
	   getdate(),
	   getdate()
from #temp1 where Category='Depot'



Insert into SendMail([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
	  ,[Parameter2]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])

Select   16,
       'noreply@couriersplease.com.au',
	    Redirection.dbo.[fn_CleansePhonenumber]([DeliveryContactPhone])+'@preview.pcsms.com.au',
	    'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   0,
	   Labelnumber,
	   Redirection.dbo.[fn_CleansePhonenumber]([DeliveryContactPhone]),
	   'Admin',
	   getdate(),
	   getdate()
from #temp1 where Category='POPStation'


Insert into SendMail([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
	  ,[Parameter2]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])

Select   17,
       'noreply@couriersplease.com.au',
	    Redirection.dbo.[fn_CleansePhonenumber]([DeliveryContactPhone])+'@preview.pcsms.com.au',
	    'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   0,
	   Labelnumber,
	   Redirection.dbo.[fn_CleansePhonenumber]([DeliveryContactPhone]),
	   'Admin',
	   getdate(),
	   getdate()
from #temp1 where Category='HUBBED'

Update FailedDeliveryCardLeft set ReadytoSendSMS=1 from #temp1 where #temp1.sno=FailedDeliveryCardLeft.sno

COMMIT TRAN INSERTSMS


end


GO
