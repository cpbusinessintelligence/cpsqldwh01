SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetPickupScansforSendingEmails] as
begin

  --'=====================================================================
    --' CP -Stored Procedure - sp_GetPickupScansforSendingEmails
    --' ---------------------------
    --' Purpose: sp_GetPickupScansforSendingEmails-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 15 Aug 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 15/08/2016    AK      1.00    Created the procedure                            

    --'=====================================================================


Select  cd_id,
       cd_connote,
	   cd_account,
	   isnull([cd_delivery_addr0],'') as cd_delivery_addr0,
	   --case when isnull([cd_delivery_addr0],'')='' then cd_delivery_contact else isnull([cd_delivery_addr0],'') end  as cd_delivery_addr0,
	   [cd_delivery_addr1],
	   [cd_delivery_addr2],
	   [cd_delivery_addr3],
	   [cd_delivery_suburb],
	   [cd_delivery_postcode],
	   case when b1.b_name='Sydney' then 'NSW' 
	        when b1.b_name='Brisbane' then 'QLD'
			when b1.b_name='Gold Coast' then 'QLD'
			when b1.b_name='Melbourne' then  Case When (cd_delivery_postcode like '7%') Then 'TAS' Else 'VIC' End  
			when b1.b_name='Perth' then 'WA'
			when b1.b_name='Canberra' then 'NSW'
			when b1.b_name='Adelaide' then Case When (cd_delivery_postcode like '8%' or cd_delivery_postcode like '08%') Then 'NT' Else 'SA' End
			else 'Unknown' end as DelState,
      ltrim(rtrim([dbo].[fn_CleanseEmail](cd_delivery_email))) as DeliveryEmail,
	  isnull([dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone),'') as DeliveryPhone,
	  convert(datetime,null) as Pickupdatetime
	  into #tempC
from cpplEDI.dbo.consignment (NOLOCK) join cpplEDI.dbo.branchs(NOLOCK) b1 on b1.b_id=cd_deliver_branch
where cd_connote not like 'CPW%' and convert(date,cd_Date)>=convert(date,dateadd(day,-3,getdate()))  and  cd_deliver_stamp is  null
and cd_id not in (Select [ConsignmentId] from tblParcelOnWayConsignment(NOLOCK) where sourcereference='EDIGW')
and cd_id not in (Select [ConsignmentId] from tblredirectconsignment(NOLOCK) where sourcereference='EDIGW')
and  [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](cd_delivery_email))=1 
and  isnull(cd_account,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where  mailtemplateid=15 and isactive=1)
and convert(date,cd_Date)>='2016-08-18'



Insert into tblParcelOnWayConsignment(ConsignmentID
      ,[ConsignmentNumber]
      ,[AccountNumber]
	  ,[DeliveryBusinessName]
      ,[DeliveryAddress1]
	  ,[DeliveryAddress2]
	  ,[DeliveryAddress3]
	  ,[DeliverySuburb]
	  ,[DeliveryPostcode]
	  ,[DeliveryState]
      ,[DeliveryEmail]
      ,[DeliveryPhone]
	  ,Sourcereference)

Select cd_id,
       cd_connote,
	   cd_account,
	   [cd_delivery_addr0],
	    [cd_delivery_addr1],
	   [cd_delivery_addr2],
	   [cd_delivery_addr3],
	   [cd_delivery_suburb],
	   [cd_delivery_postcode],
	   Delstate,
	   DeliveryEmail,
	   DeliveryPhone,
	   'EDIGW'
from #tempC


Insert into tblParcelOnWayLabel([ConID]
      ,[LabelNumber])

Select r.ID,
       cc_coupon
from #tempC t join cpplEDI.dbo.cdcoupon (NOLOCK) on t.cd_id=cc_consignment
	           join [dbo].tblParcelOnWayConsignment	r on r.consignmentid=t.cd_id
where sourcereference='EDIGW'




 Select   consignmentid,
         consignmentcode,
		 isnull(c1.Accountnumber,'') as Accountnumber,
		 isnull(a1.CompanyName,'') as CompanyName,
		-- case when isnull(a1.CompanyName,'')='' then a1.FirstName+' '+a1.LastName else isnull(a1.CompanyName,'') end as CompanyName,
         a1.[Address1] as DeliveryAddress1,
         a1.[Address2] as DeliveryAddress2,
		 '' as DeliveryAddress3,
         a1.Suburb as DeliverySuburb,
         a1.[PostCode] as DeliveryPostcode ,
	     a1.[StateName] as DeliveryState,
	     [dbo].[fn_CleansePhonenumber](a1.phone) as DeliveryContactPhone,
	     [dbo].[fn_CleanseEmail](a1.Email) as  DeliveryEmail
    into #tempw
 from ezyfreight.dbo.tblconsignment c (NOLOCK) join ezyfreight.dbo.tbladdress a (NOLOCK) on c.pickupid=a.addressid
                                      join ezyfreight.dbo.tbladdress a1 (NOLOCK) on c.destinationid=a1.addressid
									 left join ezyfreight.dbo.tblcompanyusers cu (NOLOCK) on cu.userid=c.userid
									 left join ezyfreight.dbo.tblcompany c1 (NOLOCK) on c1.companyid=cu.companyid
  where 
   isnull(isinternational,0)=0 and 
   convert(date,c.createddatetime)>=convert(date,dateadd(day,-1,getdate())) and consignmentid not in (Select [ConsignmentId] from tblParcelOnWayConsignment where sourcereference='CP_WEB')
   and consignmentid not in (Select [ConsignmentId] from tblredirectconsignment where sourcereference='CP_WEB')
	and [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](a1.Email))=1 
	and  isnull(c1.Accountnumber,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=15 and isactive=1)
	--and isnull(c1.Accountnumber,'') not in (Select [Accountnumber] from [dbo].[tblRedirectionAccounts])
	--and convert(date,cd_Date)>='2016-08-18'


Insert into tblParcelOnWayConsignment(ConsignmentID
      ,[ConsignmentNumber]
      ,[AccountNumber]
	  ,[DeliveryBusinessName]
      ,[DeliveryAddress1]
	  ,[DeliveryAddress2]
	  ,[DeliveryAddress3]
	  ,[DeliverySuburb]
	  ,[DeliveryPostcode]
	  ,[DeliveryState]
      ,[DeliveryEmail]
      ,[DeliveryPhone]
	  ,Sourcereference)

Select   consignmentid,
         consignmentcode,
		 Accountnumber,
		 CompanyName,
         DeliveryAddress1,
         DeliveryAddress2,
		 DeliveryAddress3,
         DeliverySuburb,
         DeliveryPostcode ,
	     DeliveryState,
	     [dbo].[fn_CleanseEmail](DeliveryEmail) as  DeliveryEmail,
		 isnull([dbo].[fn_CleansePhonenumber](DeliveryContactPhone),'') as DeliveryContactPhone,
		 'CP_WEB'
from #tempw




Insert into tblParcelOnWayLabel([ConID]
      ,[LabelNumber])

Select r.ID,
       labelnumber
from #tempW t join ezyfreight.dbo.tblitemlabel l (NOLOCK) on l.consignmentid=t.consignmentid
	          join [dbo].tblParcelOnWayConsignment	r on r.[Consignmentnumber]=t.[ConsignmentCode]
where sourcereference='CP_WEB'



Select r.ID,[ConsignmentNumber],labelnumber,convert(datetime,null) as Pickupdatetime
into #temp2 
from tblParcelOnWayConsignment r join [dbo].tblParcelOnWayLabel l on l.[ConID]=r.ID
 where PickupDatetime is null 

 update #temp2 set PickupDatetime=eventdatetime
from scannergateway.dbo.trackingevent w (NOLOCK)
  WHere sourcereference=labelnumber and  eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'


 Update tblParcelOnWayConsignment set PickupDatetime=#temp2.PickupDatetime from #temp2 where #temp2.ID=tblParcelOnWayConsignment.ID
 and tblParcelOnWayConsignment.PickupDatetime is null and #temp2.Pickupdatetime is not null






end
GO
