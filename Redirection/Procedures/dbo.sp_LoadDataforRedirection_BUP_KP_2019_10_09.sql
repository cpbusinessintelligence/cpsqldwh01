SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_LoadDataforRedirection_BUP_KP_2019_10_09]  as
begin

Begin tran CP_EDI


 Select  distinct cd_id,
        cd_connote,
		[cd_pickup_addr0],
	cd_pickup_contact,
    [cd_pickup_addr1],
    [cd_pickup_addr2],
    [cd_pickup_addr3],
    [cd_pickup_suburb],
    [cd_pickup_postcode],
	cd_pickup_branch,
	[dbo].[fn_CleansePhonenumber](cd_pickup_contact_phone) as PickupContactPhone,
	--cd_pickup_email,
	[cd_delivery_addr0],
	cd_delivery_contact,
    [cd_delivery_addr1],
    [cd_delivery_addr2],
    [cd_delivery_addr3],
    [cd_delivery_suburb],
    [cd_delivery_postcode],
	[cd_deliver_branch],
	[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) as DeliveryContactPhone,
	ltrim(rtrim([dbo].[fn_CleanseEmail](cd_delivery_email))) as cd_delivery_email,
	[cd_deadweight],
	[cd_volume]*250 as [cd_volume],
	[cd_pricecode],
	convert(date,[cd_eta_earliest]) as [cd_eta_earliest],
	convert(date,[cd_customer_eta]) as [cd_customer_eta],
	[cd_items],
	[cd_special_instructions],
	cd_account
	--convert(datetime,null) as PickupDatetime,
	--convert(datetime,null) as OnBoardDatetime 
    into #temp
 from cpplEDI.dbo.consignment with (NOLOCK) left join cpplEDI.dbo.cdadditional with (NOLOCK) on cd_id=ca_consignment
                                           join cpplEDI.dbo.cdcoupon with (NOLOCK) on cc_consignment=cd_id
  where 
 -- (cd_account in (select accountnumber from tblRedirectionAccounts) or convert(varchar(100),[cd_delivery_postcode])+'_'+[cd_delivery_suburb] in (Select convert(varchar(100),Postcode)+'_'+Suburb from tblRedirectionPostcodes)) 
   cd_connote not like 'CPW%' and 
	  convert(date,cd_Date)>=convert(date,dateadd(day,-4,getdate())) and cd_id not in (Select [ConsignmentId] from [tblRedirectConsignment] with (NOLOCK) where sourcereference='EDIGW')
	 and ([dbo].[fn_ValidatePhone](dbo.[fn_CleansePhonenumber](cd_delivery_contact_phone))=1 or [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](cd_delivery_email))=1 )
	 and isnull(ca_ATL,'N')='N'
	 -- and cd_deliver_branch=5

--select * from #temp cpsqldwh01.cpplEDI.dbo.branchs

Delete from #Temp Where cd_account = '113058192' and cd_delivery_addr0 = 'Iconic C/O Seko'


 Insert into dbo.[tblRedirectConsignment] ([UniqueID]
      ,[ConsignmentId]
      ,[ConsignmentCode]
	   ,[OriginBusinessName]
      ,[OriginFirstName]
      ,[OriginLastName]
      ,[OriginAddressLine1]
      ,[OriginAddressLine2]
      ,[OriginAddressLine3]
      ,[OriginSuburb]
      ,[OriginPostcode]
      ,[OriginState]
      ,[OriginContactPhone]
      ,[OriginContactEmail]
      ,[DelBusinessName]
      ,[DelFirstName]
      ,[DelLastName]
      ,[DelAddressLine1]
      ,[DelAddressLine2]
      ,[DelAddressLine3]
      ,[DelSuburb]
      ,[DelPostcode]
      ,[DelState]
	  ,[DelContactPhone]
	  ,[DelContactEmail]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[RateCardID]
      ,[CurrentETAFrom]
      ,[CurrentETATo]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[SortCode]
      ,[SourceReference]
      ,[ReadyforRedirection]
      ,[ReadytoSendMail]
      ,[ReadytoSendSMS]
	   ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
	  ,Account)


	SELECT 
	cast((Abs(Checksum(NewId()))%10) as varchar(1)) + 
       char(ascii('b')+(Abs(Checksum(NewId()))%25)) +
       char(ascii('B')+(Abs(Checksum(NewId()))%25)) +
       left(newid(),5)+ right(newid(),5),
	--[dbo].[fn_GetUniqueIDforRedirection](cd_id) ,
	cd_id,
	t.cd_connote,
	[cd_pickup_addr0],
	case when cd_pickup_contact='' then [cd_pickup_addr0] else  cd_pickup_contact end,
	case when cd_pickup_contact='' then [cd_pickup_addr0] else  cd_pickup_contact end,
    [cd_pickup_addr1],
    [cd_pickup_addr2],
    [cd_pickup_addr3],
    [cd_pickup_suburb],
    [cd_pickup_postcode],
	case when b.b_name='Sydney' then 'NSW'
	     when  b.b_name='Brisbane' then 'QLD'
		 when  b.b_name='Gold Coast' then 'QLD'
		 when  b.b_name='Melbourne' then 'VIC'
		 when  b.b_name='Perth' then 'WA'
		 when  b.b_name='Canberra' then 'NSW'
		 when  b.b_name='Adelaide' then 'SA'
		 else 'Unknown' end,
	PickupContactPhone,
	'noreply@couriersplease.com.au',
	[cd_delivery_addr0],
	case when cd_delivery_contact='' then [cd_delivery_addr0] else  cd_delivery_contact end,
	case when cd_delivery_contact='' then [cd_delivery_addr0] else  cd_delivery_contact end,
    [cd_delivery_addr1],
    [cd_delivery_addr2],
    [cd_delivery_addr3],
    [cd_delivery_suburb],
    [cd_delivery_postcode],
	case when b1.b_name='Sydney' then 'NSW'
	     when b1.b_name='Brisbane' then 'QLD'
		 when b1.b_name='Gold Coast' then 'QLD'
		 when b1.b_name='Melbourne' then 'VIC'
		 when b1.b_name='Perth' then 'WA'
		 when b1.b_name='Canberra' then 'NSW'
		 when b1.b_name='Adelaide' then 'SA'
		 else 'Unknown' end,
   -- '0466419484' ,
	--'kirsty.tuffley@couriersplease.com.au',
	[dbo].[fn_CleansePhonenumber](DeliveryContactPhone) as DeliveryContactPhone,
	[dbo].[fn_CleanseEmail](cd_delivery_email) as cd_delivery_email,
	[cd_deadweight],
	[cd_volume],
	[cd_pricecode],
	convert(date,isnull([cd_eta_earliest],getdate())),
	convert(date,isnull([cd_customer_eta],getdate())),
	[cd_items],
	[cd_special_instructions],
	1,
	'EDIGW',
	0,
	0,
	0,
    getdate(),
	'AK',
	getdate(),
	'AK',
	cd_account
	from #temp t left join cpsqldwh01.cpplEDI.dbo.branchs  b with (NOLOCK) on b.b_id=cd_pickup_branch
			     left join cpsqldwh01.cpplEDI.dbo.branchs b1 with (NOLOCK) on b1.b_id=cd_deliver_branch
	--		          join #temp2 t1 on t.cd_connote=t1.cd_connote
	--where [dbo].[fn_GetUniqueIDforRedirection](cd_id) not in (Select uniqueid from [tblRedirectConsignment])
	



-- where  Pickupdatetime is not null and OnBoardDatetime is null

--update dbo.[tblRedirectConsignment] set delcontactemail='abhigna.kona@couriersplease.com.au',delcontactphone='0469024068'


  Insert into  dbo.tblRedirectItem([ConsignmentID]
      ,[LabelNumber]
	  ,CubicWeight
	  ,PhysicalWeight
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy])

	  Select r.id
	  ,cc_coupon
	  ,convert(decimal(12,2),TotalVolume/[NoOfItems])
	  ,convert(decimal(12,2),TotalWeight/[NoOfItems])
	  ,getdate()
	  ,'AK'
	  ,getdate()
	  ,'AK' 
	from #temp t join cpplEDI.dbo.cdcoupon (NOLOCK) on t.cd_id=cc_consignment
	           join [dbo].[tblRedirectConsignment]	r with (NOLOCK) on r.consignmentid=t.cd_id
	where sourcereference='EDIGW'

COMMIT Tran CP_EDI




Begin tran CP_WEB




 Select consignmentid,
         consignmentcode,
		 a.[CompanyName] as OriginCompanyName,
		 a.[FirstName] as OriginFirstName,
		 a.[LastName] as  OriginLastName,
         a.[Address1] as PickupAddress1,
         a.[Address2] as PickupAddress2,
         ''  as PickupAddress3,
         a.Suburb as PickupSuburb,
         a.[PostCode] as PickupPostcode,
	     a.[StateName] as PickupState,
	     [dbo].[fn_CleansePhonenumber](a.phone) as PickupContactPhone,
		 [dbo].[fn_CleanseEmail](a.Email) as PickupEmail,
	     a1.[CompanyName] as DeliveryCompanyName,
		 a1.[FirstName] as DeliveryFirstName,
		 a1.[LastName] as  DeliveryLastName,
         a1.[Address1] as DeliveryAddress1,
         a1.[Address2] as DeliveryAddress2,
         '' as DeliveryAddress3,
         a1.Suburb as DeliverySuburb,
         a1.[PostCode] as DeliveryPostcode ,
	     a1.[StateName] as DeliveryState,
	     [dbo].[fn_CleansePhonenumber](a1.phone) as DeliveryContactPhone,
	     [dbo].[fn_CleanseEmail](a1.Email) as  DeliveryEmail,
	     c.TotalWeight,
	     c.TotalVolume,
	     [RateCardID],
	     convert(date,c.[createddatetime]) as [cd_eta_earliest],
	     convert(date,c.[createddatetime]) as [cd_customer_eta],
	     [NoOfItems],
	     [SpecialInstruction],
		 isnull(c1.accountnumber,'') as Account
	--convert(datetime,null) as PickupDatetime,
	--convert(datetime,null) as OnBoardDatetime 
    into #tempweb
 from ezyfreight.dbo.tblconsignment c (NOLOCK) join ezyfreight.dbo.tbladdress a (NOLOCK) on c.pickupid=a.addressid
                                      join ezyfreight.dbo.tbladdress a1 (NOLOCK) on c.destinationid=a1.addressid
									  left join ezyfreight.dbo.tblcompanyusers cu (NOLOCK) on cu.userid=c.userid
									  left join ezyfreight.dbo.tblcompany c1 (NOLOCK) on c1.companyid=cu.companyid
  where 
 -- (c1.accountnumber in (select accountnumber from tblRedirectionAccounts) or  convert(varchar(100),a1.[PostCode])+'_'+a1.Suburb in (Select convert(varchar(100),Postcode)+'_'+Suburb from tblRedirectionPostcodes)) 
   isnull(isinternational,0)=0 and 
	  convert(date,c.createddatetime)>=convert(date,dateadd(day,-2,getdate())) and consignmentid not in (Select [ConsignmentId] from [tblRedirectConsignment] with (NOLOCK) where sourcereference='CP_WEB')
	 and ([dbo].[fn_ValidatePhone]( [dbo].[fn_CleansePhonenumber](a1.phone))=1  or [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail](a1.Email))=1 )
   and ratecardid not in ('SDC','CE3','CE5','PDC','PE3','PE5')
    




 Insert into dbo.[tblRedirectConsignment] ([UniqueID]
      ,[ConsignmentId]
      ,[ConsignmentCode]
	   ,[OriginBusinessName]
      ,[OriginFirstName]
      ,[OriginLastName]
      ,[OriginAddressLine1]
      ,[OriginAddressLine2]
      ,[OriginAddressLine3]
      ,[OriginSuburb]
      ,[OriginPostcode]
      ,[OriginState]
      ,[OriginContactPhone]
      ,[OriginContactEmail]
      ,[DelBusinessName]
      ,[DelFirstName]
      ,[DelLastName]
      ,[DelAddressLine1]
      ,[DelAddressLine2]
      ,[DelAddressLine3]
      ,[DelSuburb]
      ,[DelPostcode]
      ,[DelState]
	  ,[DelContactPhone]
	  ,[DelContactEmail]
      ,[TotalWeight]
      ,[TotalVolume]
      ,[RateCardID]
      ,[CurrentETAFrom]
      ,[CurrentETATo]
      ,[NoOfItems]
      ,[SpecialInstruction]
      ,[SortCode]
      ,[SourceReference]
      ,[ReadyforRedirection]
      ,[ReadytoSendMail]
      ,[ReadytoSendSMS]
	   ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTTime]
      ,[UpdatedBy]
	  ,Account)


	SELECT cast((Abs(Checksum(NewId()))%10) as varchar(1)) + 
       char(ascii('a')+(Abs(Checksum(NewId()))%25)) +
       char(ascii('A')+(Abs(Checksum(NewId()))%25)) +
       left(newid(),5)+ right(newid(),5),
	   -- [dbo].[fn_GetUniqueIDforRedirection](consignmentid) ,
		 consignmentid,
	     consignmentcode,
		 OriginCompanyName,
		 OriginFirstName,
		 OriginLastName,
         PickupAddress1,
         PickupAddress2,
         PickupAddress3,
         PickupSuburb,
         PickupPostcode,
	     PickupState,
	     PickupContactPhone,
		 PickupEmail,
	     DeliveryCompanyName,
		 DeliveryFirstName,
		 DeliveryLastName,
         DeliveryAddress1,
         DeliveryAddress2,
         DeliveryAddress3,
         DeliverySuburb,
         DeliveryPostcode ,
	     DeliveryState,
	     DeliveryContactPhone ,
	     DeliveryEmail,
	     TotalWeight,
	     TotalVolume,
	     [RateCardID],
	     [cd_eta_earliest],
	     [cd_customer_eta],
	     [NoOfItems],
	     [SpecialInstruction],
		 '',
	     'CP_WEB',
	     0,
	     0,
	     0,
         getdate(),
	     'AK',
	      getdate(),
	      'AK',
		  Account
	from #tempweb t 
	--where [dbo].[fn_GetUniqueIDforRedirection](consignmentid) not in (Select uniqueid from [tblRedirectConsignment])
	



--update dbo.[tblRedirectConsignment] set delcontactemail='abhigna.kona@couriersplease.com.au',delcontactphone='0469024068'


  Insert into  dbo.tblRedirectItem([ConsignmentID]
      ,[LabelNumber]
	  ,CubicWeight
	  ,PhysicalWeight
	  ,length
	  ,width
	  ,height
      ,[CreatedDateTime]
      ,[CreatedBy]
      ,[UpdatedDateTime]
      ,[UpdatedBy])

	  Select r.id
	  ,l.labelnumber
	  ,l.Cubicweight
	  ,l.PhysicalWeight
	  ,l.length
	  ,l.width
	  ,l.height
	  ,getdate()
	  ,'AK'
	  ,getdate()
	  ,'AK' 
	from #tempweb t join ezyfreight.dbo.tblitemlabel l (NOLOCK) on l.consignmentid=t.consignmentid
	                join [dbo].[tblRedirectConsignment]	r with (NOLOCK) on r.[ConsignmentCode]=t.[ConsignmentCode]



COMMIT Tran CP_WEB

--drop table #temp4


Select ID,UniqueId,consignmentcode,l.labelnumber,convert(datetime,null) as Pickupdatetime,convert(datetime,null) as OnBoarddatetime,convert(datetime,null) as AttDelorDelDatetime
into #temp2 
from [tblRedirectConsignment] r with (NOLOCK) join [dbo].[tblRedirectItem] l with (NOLOCK) on l.consignmentid=r.ID
 where PickupDatetime is null or [OnBoardDateTime] is null

 update #temp2 set PickupDatetime=eventdatetime
from scannergateway.dbo.trackingevent w (NOLOCK)
  WHere sourcereference=labelnumber and  eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'
  and #temp2.PickupDatetime is null

update #temp2 set OnBoardDatetime=eventdatetime
from scannergateway.dbo.trackingevent w (NOLOCK)
  WHere sourcereference=labelnumber and  eventtypeid='93B2E381-6A89-4F2E-9131-2DC2FB300941'
  and #temp2.OnBoardDatetime is null

 update #temp2 set AttDelorDelDatetime=eventdatetime
from scannergateway.dbo.trackingevent w (NOLOCK)
  WHere sourcereference=labelnumber and  eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
  and #temp2.OnBoardDatetime is null


 Update [tblRedirectConsignment] set PickupDatetime=#temp2.PickupDatetime,[UpdatedDateTTime]=getdate() from #temp2 where #temp2.ID=[tblRedirectConsignment].ID
 and tblRedirectConsignment.PickupDatetime is null

  Update [tblRedirectConsignment] set OnBoardDateTime=#temp2.OnBoardDateTime,[UpdatedDateTTime]=getdate() from #temp2 where #temp2.ID=[tblRedirectConsignment].ID
 and tblRedirectConsignment.OnBoardDateTime is null

  Update [tblRedirectConsignment] set OnBoardDateTime=#temp2.AttDelorDelDatetime,[UpdatedDateTTime]=getdate() from #temp2 where #temp2.ID=[tblRedirectConsignment].ID 
   and tblRedirectConsignment.OnBoardDateTime is null

  Update [tblRedirectConsignment] set OriginZone=p.etazone,DestinationZone=p1.etazone,[RoadExpressPickupCutoffflag]=p1.RoadExpressPickupFlag from [tblRedirectConsignment] join couponcalculator.dbo.postcodes p on p.postcode=originpostcode and p.suburb=[OriginSuburb]
                                                                                                                    join couponcalculator.dbo.postcodes p1 on p1.postcode=[DelPostcode] and p1.suburb=[DelSuburb]
    where originzone is null or DestinationZone is null

  Update [tblRedirectConsignment] set [ETA] =c.eta ,[1stPickupcutoff]=[1stPickupcutofftime]
  from couponcalculator.dbo.etaCalculator c where c.fromzone=originzone and c.tozone=destinationzone and [tblRedirectConsignment].eta is null
  and c.[1stPickupcutofftime]<>'XXX'

  --select * from tblredirectconsignment

  --Update [tblRedirectConsignment] set [RoadExpressPickupCutoffflag]=p1.RoadExpressPickupFlag from[tblRedirectConsignment] join couponcalculator.dbo.postcodes p1 on p1.postcode=[DelPostcode] and p1.suburb=[DelSuburb]
  --  where [RoadExpressPickupCutoffflag] is null
  --update [tblRedirectConsignment] set [1stPickupcutoff] ='XXX' where uniqueid='1fS5B338'

--delete ri from tblredirectitem ri join [tblRedirectConsignment] r on r.Id=ri.consignmentid where OnBoardDateTime is not null

--delete from [tblRedirectConsignment] where OnBoardDateTime is not null


BEGIN TRAN INSERTMAIL



Select  uniqueid into #temp3 from [tblRedirectConsignment] with (NOLOCK) where readytosendmail=0 and OnBoardDateTime is null and Pickupdatetime is not null and [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail]([DelContactEmail]))=1 and  eta='0 - 1' and convert(time,pickupdatetime)>[1stpickupcutoff] 
and Pickupdatetime>= Getdate()-4  and isnull([RoadExpressPickupCutoffflag],'N')='Y'
and isnull([Account],'') not in (Select [AccountNumber] from [dbo].[CustomerExceptions] where [MailTemplateid]=1 and isactive=1 )
and convert(date,PickupDateTime)>=convert(date,dateadd(day,-1,getdate()))

Insert into #temp3
Select  uniqueid  from [tblRedirectConsignment] with (NOLOCK) where readytosendmail=0 and OnBoardDateTime is null and Pickupdatetime is not null and [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail]([DelContactEmail]))=1 and  eta<>'0 - 1'
and Pickupdatetime>= Getdate()-4 and isnull([RoadExpressPickupCutoffflag],'N')='Y'
and isnull([Account],'') not in (Select [AccountNumber] from [dbo].[CustomerExceptions] where [MailTemplateid]=1 and isactive=1 )
and convert(date,PickupDateTime)>=convert(date,dateadd(day,-1,getdate()))

Select uniqueid into #temp4 from [tblRedirectConsignment] with (NOLOCK) where readytosendsms=0 and OnBoardDateTime is null and Pickupdatetime is not null and [dbo].[fn_ValidatePhone]( [dbo].[fn_CleansePhonenumber]([DelContactPhone]))=1  and  eta='0 - 1' and convert(time,pickupdatetime)>[1stpickupcutoff] 
and Pickupdatetime>= Getdate()-4 and isnull([RoadExpressPickupCutoffflag],'N')='Y'
and isnull([Account],'') not in (Select [AccountNumber] from [dbo].[CustomerExceptions] where [MailTemplateid]=2 and isactive=1 )
and convert(date,PickupDateTime)>=convert(date,dateadd(day,-1,getdate()))

Insert into #temp4
Select uniqueid  #temp4 from [tblRedirectConsignment] with (NOLOCK) where readytosendsms=0 and OnBoardDateTime is null and Pickupdatetime is not null and [dbo].[fn_ValidatePhone]( [dbo].[fn_CleansePhonenumber]([DelContactPhone]))=1  and  eta<>'0 - 1'
and Pickupdatetime>= Getdate()-4 and isnull([RoadExpressPickupCutoffflag],'N')='Y'
and isnull([Account],'') not in (Select [AccountNumber] from [dbo].[CustomerExceptions] where [MailTemplateid]=2 and isactive=1 )
 and convert(date,PickupDateTime)>=convert(date,dateadd(day,-1,getdate()))

Select  uniqueid into #temp5 from [tblRedirectConsignment] with (NOLOCK)
where readytosendmail=0 and OnBoardDateTime is null and Pickupdatetime is not null and [dbo].[fn_ValidateEmail]([dbo].[fn_CleanseEmail]([DelContactEmail]))=1 and  eta='0 - 1' and convert(time,pickupdatetime)<=[1stpickupcutoff] 
and Pickupdatetime>= Getdate()-4
and isnull([Account],'') not in (Select [AccountNumber] from [dbo].[CustomerExceptions] with (NOLOCK) where [MailTemplateid] in (15, 18) and isactive=1 ) 
and convert(varchar(20),consignmentid)+'_'+Sourcereference not in (select convert(varchar(20),consignmentid)+'_'+Sourcereference from  tblParcelOnWayConsignment)
--and isnull([RoadExpressPickupCutoffflag],'N')='Y'
and convert(date,PickupDateTime)>=convert(date,dateadd(day,-1,getdate()))


Select uniqueid into #temp6 from [tblRedirectConsignment] with (NOLOCK)
where readytosendsms=0 
and OnBoardDateTime is null 
and Pickupdatetime is not null 
and [dbo].[fn_ValidatePhone]( [dbo].[fn_CleansePhonenumber]([DelContactPhone]))=1  
and  eta='0 - 1' 
and convert(time,pickupdatetime)<=[1stpickupcutoff] 
and Pickupdatetime>= Getdate()-4 and isnull([RoadExpressPickupCutoffflag],'N')='Y'
and isnull([Account],'') not in (Select [AccountNumber] from [dbo].[CustomerExceptions] where [MailTemplateid]=23 and isactive=1 )
and convert(date,PickupDateTime)>=convert(date,dateadd(day,-1,getdate()))
and  Account in (  '113058192','113071435')


Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
	  ,[Parameter5]
      ,[Parameter6]
	  ,[Parameter7]
	  ,[Parameter8]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])




Select   1,
       'noreply@couriersplease.com.au',
	   --'kirsty.tuffley@couriersplease.com.au,hayat.horma@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   [DelContactEmail],
	   'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au,21b7515f07@invite.trustpilot.com',
	   0,
	   [ConsignmentCode],
	   [DelFirstName] + ' '+case when [DelLastName]=[DelFirstName] then '' else [DelLastName] end+'/'+[DelBusinessName],
	   [OriginFirstName] + ' '+[OriginLastName]+'/'+[OriginBusinessName],
	   [DelAddressLine1]+ '<br/>' +[DelAddressLine2]+ '<br/>'+[DelAddressLine3]+'<br/>'+[DelSuburb]+'<br/>'+[DelPostcode]+'<br/>'[Delstate],
	   [DelContactEmail],
	   [DelContactPhone],
	   t.[UniqueID],
	   [DelContactEmail],
	  -- [DelContactEmail],
	   'AK',
	   getdate(),
	   getdate()

from #temp3 t join [tblRedirectConsignment](NOLOCK)r on r.uniqueid=t.uniqueid  where  readytosendmail=0




Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
	  ,[parameter3]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])


	  --Select distinct 4,
   --    'noreply@couriersplease.com.au',
	  -- 'abhigna.kona@couriersplease.com.au',
	  -- --Deliverytomail,
	  -- 0,
	  -- 'CPAX12356234547',
	  -- 'Bicheno Newsagency',
	  -- ' 41 Foster St,BICHENO,7215,TAS,Australia',
	  -- 'Sun: 08:30-12:30, Mon-Fri: 08:00-17:00, Sat: 08:00-13:00',
	  -- 'AK',
	  -- getdate(),
	  -- getdate()

Select   2,
       'noreply@couriersplease.com.au',
	  -- '0412232545@preview.pcsms.com.au,0469024068@preview.pcsms.com.au,0466419484@preview.pcsms.com.au',
	   [DelContactPhone]+'@preview.pcsms.com.au',
	   'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   0,
	   [ConsignmentCode],
	   t.[UniqueID],
	 -- [DelContactPhone]+'@preview.pcsms.com.au',
	   ltrim(rtrim(DelContactPhone)),
	   'AK',
	   getdate(),
	   getdate()

from #temp4 t join [tblRedirectConsignment](NOLOCK)r on r.uniqueid=t.uniqueid  where  readytosendsms=0





Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
	  ,[Parameter5]
      ,[Parameter6]
	  ,[Parameter7]
	  ,[Parameter8]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])

--
--#Jp 2017-07-05
-- Included Option for ATL and Neighbour Delivery.No longer sending context ID 15 as per Hayats request



Select   18,
       'noreply@couriersplease.com.au',
	   --'kirsty.tuffley@couriersplease.com.au,hayat.horma@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   [DelContactEmail],
	   'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au,kalpesh.pandya@couriersplease.com.au,21b7515f07@invite.trustpilot.com',
	   0,
	   [ConsignmentCode],
	   [DelFirstName] + ' '+case when [DelLastName]=[DelFirstName] then '' else [DelLastName] end+'/'+[DelBusinessName],
	   [OriginFirstName] + ' '+[OriginLastName]+'/'+[OriginBusinessName],
	   [DelAddressLine1]+ '<br/>' +[DelAddressLine2]+ '<br/>'+[DelAddressLine3]+'<br/>'+[DelSuburb]+'<br/>'+[DelPostcode]+'<br/>'[Delstate],
	   [DelContactEmail],
	   [DelContactPhone],
	   t.[UniqueID],
	   [DelContactEmail],
	  -- [DelContactEmail],
	   'AK',
	   getdate(),
	   getdate()

from #temp5 t join [tblRedirectConsignment](NOLOCK)r on r.uniqueid=t.uniqueid



Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
	  ,[parameter3]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])


	  --Select distinct 4,
   --    'noreply@couriersplease.com.au',
	  -- 'abhigna.kona@couriersplease.com.au',
	  -- --Deliverytomail,
	  -- 0,
	  -- 'CPAX12356234547',
	  -- 'Bicheno Newsagency',
	  -- ' 41 Foster St,BICHENO,7215,TAS,Australia',
	  -- 'Sun: 08:30-12:30, Mon-Fri: 08:00-17:00, Sat: 08:00-13:00',
	  -- 'AK',
	  -- getdate(),
	  -- getdate()

Select   23,
       'noreply@couriersplease.com.au',
	  -- '0412232545@preview.pcsms.com.au,0469024068@preview.pcsms.com.au,0466419484@preview.pcsms.com.au',
	   [DelContactPhone]+'@preview.pcsms.com.au',
	   'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
	   0,
	   [ConsignmentCode],
	   t.[UniqueID],
	 -- [DelContactPhone]+'@preview.pcsms.com.au',
	   ltrim(rtrim(DelContactPhone)),
	   'AK',
	   getdate(),
	   getdate()

from #temp6 t join [tblRedirectConsignment](NOLOCK)r on r.uniqueid=t.uniqueid  where  readytosendsms=0



update [tblRedirectConsignment] set ReadytoSendMail=1 where uniqueid in (select uniqueid from #temp3)

update [tblRedirectConsignment] set ReadytoSendSMS=1 where uniqueid in (select uniqueid from #temp4)

update [tblRedirectConsignment] set ReadytoSendMail=1 where uniqueid in (select uniqueid from #temp5)    

update [tblRedirectConsignment] set ReadytoSendSMS=1 where uniqueid in (select uniqueid from #temp6)                                             



COMMIT TRAN INSERTMAIL

-- This section is for sending Parcel on its way emails only for ATL

BEGIN TRAN MAILINSERT2

Select  ID,ConsignmentNumber into #tempAtl from tblParcelOnWayConsignment with (NOLOCK) where readytosendmail=0  and Pickupdatetime is not null 
and pickupdatetime>= Getdate()-1
and  Accountnumber not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] with (NOLOCK) where mailtemplateid=15 and isactive=1)
and  NOT EXISTS (Select 1 from [Redirection].[dbo].tblRedirectConsignment where ConsignmentCode = tblParcelOnWayConsignment.ConsignmentNumber)

Insert into SendMail([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
	  ,[Parameter5]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])




Select  distinct  15,
       'noreply@couriersplease.com.au',
	    [DeliveryEmail],
		'deliverychoices@couriersplease.com.au,cpreporting@couriersplease.com.au,21b7515f07@invite.trustpilot.com',
		--'kirsty.tuffley@couriersplease.com.au',
	   --'abhigna.kona@couriersplease.com.au',
	   --'kirsty.tuffley@couriersplease.com.au,hayat.horma@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   --[DeliveryEmail],
	--   'CPReporting@couriersplease.com.au',
	   0,
	   r.[ConsignmentNumber],
	   [DeliveryBusinessName]+'<br/>'+[DeliveryAddress1]+ ' ' +[DeliveryAddress2]+ ' '+[DeliveryAddress3]+'<br/>'+[DeliverySuburb]+' '+[Deliverystate]+' '+[DeliveryPostcode],
	   [DeliveryEmail],
	   isnull([DeliveryPhone],'') as DeliveryPhone ,
	   [DeliveryEmail],
	  -- [DeliveryEmail],
	   'AK',
	   getdate(),
	   getdate()

from #tempAtl t join tblParcelOnWayConsignment(NOLOCK)r on r.ConsignmentNumber=t.ConsignmentNumber where  readytosendmail=0

update tblParcelOnWayConsignment set ReadytoSendMail=1 where ID in (select ID from #tempAtl)


COMMIT TRAN MAILINSERT2


end
GO
