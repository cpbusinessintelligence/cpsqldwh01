SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptConsolidateEmailSentToCustomers]  (@StartDate Date, @EndDate Date, @Context varchar(200))
as
begin

  --'=====================================================================
    --' CP -Stored Procedure - [[sp_GetCardLeftCards]]
    --' ---------------------------
    --' Purpose: -----
    --' Developer: Satya Gandu (Couriers Please Pty Ltd)
    --' Date: 03 Apr 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
                         
    --'=====================================================================
Declare @contextId int
set @contextId = (select sno from MailTemplates where context= @context)

select  parameter1,ContextID,ToAddr 
into #temp
from SendMail  (Nolock)
where AddDateTime between @StartDate and @EndDate
and ContextID =@contextId

select  c.cd_account,cd_connote,E.ContextID,E.ToAddr 
into #temp1
from #temp E join CpplEDI.dbo.consignment c on E.parameter1=c.cd_connote

select cd_account,shortname,cd_connote,ContextID,ToAddr 
from #temp1 T left join pronto.dbo.ProntoDebtor p  on T.cd_account=p.Accountcode

end



GO
GRANT EXECUTE
	ON [dbo].[sp_RptConsolidateEmailSentToCustomers]
	TO [ReportUser]
GO
GRANT VIEW DEFINITION
	ON [dbo].[sp_RptConsolidateEmailSentToCustomers]
	TO [ReportUser]
GO
