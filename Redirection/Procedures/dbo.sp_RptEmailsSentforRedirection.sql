SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_RptEmailsSentforRedirection(@StartDate date,@EndDate date) as
begin
  --'=====================================================================
    --' CP -Stored Procedure -sp_RptRedirectedItemCount
    --' ---------------------------
    --' Purpose: sp_RptRedirectedItemCount-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 11 Oct 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 11/10/2016    AB      1.00    Created the procedure                             --AB20161011

    --'=====================================================================

Select case when contextid=1 then 'Emails before Redirection' when Contextid=2 then 'SMS before Redirection' else '' end,count(*) as [Count]

from SendMAil where isnull(iserror,0)=0 and transmitflag<>9 and contextid in (1,2) and convert(date,adddatetime)>='2016-10-06' and convert(date,adddatetime) between @StartDate and @EndDate
group by case when contextid=1 then 'Emails before Redirection' when Contextid=2 then 'SMS before Redirection' else '' end 
end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptEmailsSentforRedirection]
	TO [ReportUser]
GO
