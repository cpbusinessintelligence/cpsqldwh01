SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_RptEmailsSentforRedirection_AccNo]
      (@StartDate DATE,@EndDate DATE,
      @AccountNumber VARCHAR(200))  
AS
BEGIN
  --'=====================================================================
    --' sp_RptEmailsSentforRedirection_AccNo '2017-07-01','2017-09-01','112970702'
    --' ---------------------------
    --' Purpose: sp_RptRedirectedItemCount-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 11 Oct 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 11/10/2016    AB      1.00    Created the procedure                             --AB20161011
	--   09/06/2017   SS      1.01    Added Account Number parameter
    --'=====================================================================
SELECT
    cd_connote,
	cc_coupon
INTO #temp 
FROM CpplEDI.dbo.cdcoupon cd 
JOIN CpplEDI.dbo.consignment c on cd.cc_consignment= c.cd_id
WHERE 
    CONVERT(DATE,CD_DATE) 
	BETWEEN @StartDate and @EndDate
	AND CD_ACCOUNT = @AccountNumber

SELECT @AccountNumber as AccountCode,
Context as EmailType,
    (Select count(*) from #Temp) as TotalEmails,
	COUNT(*) as [Count]
	FROM  #temp cons
	INNER JOIN SendMAil Sm
	ON (Parameter1 = Cons.cd_connote)  
	INNER JOIN [MailTemplates] Mt
	ON (Sm.contextid = Mt.Sno)
	WHERE ISNULL(iserror,0)=0 
	AND transmitflag = 1
	--AND contextid in (1,2) 
GROUP BY 
	Context

	UNION ALL

SELECT @AccountNumber as AccountCode,
Context as EmailType,
    (Select count(*) from #Temp) as TotalEmails,
	COUNT(*) as [Count]
	FROM  #temp cons
	INNER JOIN SendMAil Sm
	ON (Parameter1 = Cons.cc_coupon)  
	INNER JOIN [MailTemplates] Mt
	ON (Sm.contextid = Mt.Sno)
	WHERE ISNULL(iserror,0)=0 
	AND transmitflag = 1

GROUP BY 
	Context
END

  

GO
GRANT EXECUTE
	ON [dbo].[sp_RptEmailsSentforRedirection_AccNo]
	TO [ReportUser]
GO
