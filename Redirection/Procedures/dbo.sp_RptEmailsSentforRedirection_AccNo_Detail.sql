SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_RptEmailsSentforRedirection_AccNo_Detail]
      (@StartDate DATE,@EndDate DATE,
      @AccountNumber VARCHAR(200)
	  )  
AS
BEGIN
  --'=====================================================================
    --' [sp_RptEmailsSentforRedirection_AccNo_Detail] '2017-07-01','2017-07-18','112970702','Email before Redirection'
    --' ---------------------------
    --' Purpose: sp_RptRedirectedItemCount-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 11 Oct 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 11/10/2016    AB      1.00    Created the procedure                             --AB20161011
	--   09/06/2017   SS      1.01    Added Account Number parameter
    --'=====================================================================

--drop table #temp
SELECT
    cd_connote,
	cc_coupon
INTO #temp 
FROM CpplEDI.dbo.cdcoupon cd 
JOIN CpplEDI.dbo.consignment c on cd.cc_consignment= c.cd_id
WHERE 
    CONVERT(DATE,CD_DATE) 
	BETWEEN @StartDate and @EndDate
	AND CD_ACCOUNT = @AccountNumber


SELECT 
    Parameter1,
	ToAddr,
	Parameter2 as AccountName,
	Parameter3 as Address,
	Parameter6 as Phone,
	Parameter7,
	TransmitFlag,
	Context,
	Mt."Subject"
FROM  #temp cons
INNER JOIN SendMAil Sm
ON (Parameter1 = Cons.cd_connote)  
INNER JOIN [MailTemplates] Mt
ON (Sm.contextid = Mt.Sno)
WHERE ISNULL(iserror,0)=0 
AND transmitflag = 1

	--AND Context ='Fail Delivery Depot SMS'

	UNION ALL

SELECT 
    Parameter1,
	ToAddr,
	Parameter2 as AccountName,
	Parameter3 as Address,
	Parameter6 as Phone,
	Parameter7,
	TransmitFlag,
	Context,
	Mt."Subject"
FROM  #temp cons
INNER JOIN SendMAil Sm
ON (Parameter1 = Cons.cc_coupon)  
INNER JOIN [MailTemplates] Mt
ON (Sm.contextid = Mt.Sno)
WHERE ISNULL(iserror,0)=0 
AND transmitflag = 1


END


GO
GRANT EXECUTE
	ON [dbo].[sp_RptEmailsSentforRedirection_AccNo_Detail]
	TO [ReportUser]
GO
