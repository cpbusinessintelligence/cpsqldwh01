SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_RptEmailsSentforRedirection_Detail]
      (@StartDate DATE,@EndDate DATE,
      @AccountNumber VARCHAR(200))  
AS
BEGIN
--'=====================================================================
    --' sp_RptEmailsSentforRedirection_Detail '2017-07-01','2017-09-01','112970702'
    --' ---------------------------
    --' Purpose: -----
    --' Developer: Sinshith (Couriers Please Pty Ltd)
    --' Date: 27 Nov 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --'=====================================================================
SELECT
    cd_connote,
	cc_coupon
INTO #temp 
FROM CpplEDI.dbo.cdcoupon cd 
JOIN CpplEDI.dbo.consignment c on cd.cc_consignment= c.cd_id
WHERE 
    CONVERT(DATE,CD_DATE) 
	BETWEEN @StartDate and @EndDate
	AND CD_ACCOUNT = @AccountNumber

SELECT 
Parameter1 as Consignment,
Context as Type,
AddDateTime as SMSorEmailAddedDateandTime,
ModifiedDateTime as ActionedDate
   
	FROM  #temp cons
	INNER JOIN SendMAil Sm
	ON (Parameter1 = Cons.cd_connote)  
	INNER JOIN [MailTemplates] Mt
	ON (Sm.contextid = Mt.Sno)
	WHERE ISNULL(iserror,0)=0 
	AND transmitflag = 1
	--AND contextid in (1,2) 

	UNION ALL

SELECT Parameter1 as Consignment,
Context as Type,
AddDateTime as SMSorEmailAddedDateandTime,
ModifiedDateTime as ActionedDate
	FROM  #temp cons
	INNER JOIN SendMAil Sm
	ON (Parameter1 = Cons.cc_coupon)  
	INNER JOIN [MailTemplates] Mt
	ON (Sm.contextid = Mt.Sno)
	WHERE ISNULL(iserror,0)=0 
	AND transmitflag = 1
END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptEmailsSentforRedirection_Detail]
	TO [ReportUser]
GO
