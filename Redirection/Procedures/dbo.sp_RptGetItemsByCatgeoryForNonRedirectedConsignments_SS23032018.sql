SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptGetItemsByCatgeoryForNonRedirectedConsignments_SS23032018] (@StartDate date,@EndDate date) as

begin

  --'=====================================================================

    --' CP -Stored Procedure -sp_RptGetItemsByCatgeoryForNonRedirectedConsignments

    --' ---------------------------

    --' Purpose: sp_RptGetItemsByCatgeoryForNonRedirectedConsignments-----

    --' Developer: Abhigna (Couriers Please Pty Ltd)

    --' Date: 01 Nov 2016

    --' Copyright: 2014 Couriers Please Pty Ltd

    --' Change Log: 

    --' Date          Who     Ver     Reason                                            Bookmark

    --' ----          ---     ---     -----                                             -------

    --' 01/11/2016    AB      1.00    Created the procedure                             --AB20161101



    --'=====================================================================

  



SELECT distinct [ID]

      ,[UniqueID]

      ,c.[ConsignmentId]

      ,[ConsignmentCode]

	  ,r.Shortname as AccountName

	  ,l.labelnumber

      ,[OriginBusinessName]

      ,[OriginFirstName]

      ,[OriginLastName]

      ,[OriginAddressLine1]

      ,[OriginAddressLine2]

      ,[OriginAddressLine3]

      ,[OriginSuburb]

      ,[OriginPostcode]

      ,[OriginState]

      ,[OriginContactPhone]

      ,[OriginContactEmail]

      ,[DelBusinessName]

      ,[DelFirstName]

      ,[DelLastName]

      ,[DelAddressLine1]

      ,[DelAddressLine2]

      ,[DelAddressLine3]

      ,[DelSuburb]

      ,[DelPostcode]

      ,[DelState]

      ,[DelContactPhone]

      ,[DelContactEmail]

      ,[TotalWeight]

      ,[TotalVolume]

      ,[RateCardID]

      ,[CurrentETAFrom]

      ,[CurrentETATo]

      ,[NoOfItems]

      ,[SpecialInstruction]

      ,[SortCode]

      ,[SourceReference]

      ,[PickupDateTime]

      ,[OnBoardDateTime]

      ,[OriginZone]

      ,[DestinationZone]

      ,[ETA]

      ,[1stPickupcutoff]

      ,[RoadExpressPickupCutoffflag]

	  ,f.cardleft

	  ,f.category

	  ,convert(datetime,null) as DeliveryDatetime

	  ,convert(varchar(100),'') as Status

	  into #temp

FROM [Redirection].[dbo].[tblRedirectConsignment] c with (Nolock) join tblredirectitem l with (Nolock) on l.consignmentid=c.ID 

                                                    left join [dbo].[FailedDeliveryCardLeft] f with (Nolock) on f.labelnumber=l.labelnumber

													left join Pronto.dbo.Prontodebtor  r with (Nolock) on r.Accountcode=c.account

where (c.[ReadytoSendMail] =1 or  c.[ReadytoSendSMS]=1 )

and c.CreatedDateTime between @StartDate and @EndDate

and uniqueid not in (Select uniqueid from ezyfreight.dbo.tblredirectedconsignment with (Nolock))



update #temp set DeliveryDatetime=eventdatetime,status='Delivered' from #temp t join ScannerGateway.dbo.trackingevent te with (Nolock) on te.sourcereference=t.labelnumber

where te.eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3'





select * from #temp 



end

GO
