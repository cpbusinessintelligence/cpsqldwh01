SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerExceptions] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[AccountNumber]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MailTemplateid]      [int] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[isactive]            [bit] NULL
)
GO
ALTER TABLE [dbo].[CustomerExceptions]
	ADD
	CONSTRAINT [DF__CustomerE__Creat__236943A5]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[CustomerExceptions]
	ADD
	CONSTRAINT [DF__CustomerE__Creat__245D67DE]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[CustomerExceptions]
	ADD
	CONSTRAINT [DF__CustomerE__Edite__25518C17]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[CustomerExceptions]
	ADD
	CONSTRAINT [DF__CustomerE__Edite__2645B050]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[CustomerExceptions]
	ADD
	CONSTRAINT [DF__CustomerE__isact__65F62111]
	DEFAULT ((1)) FOR [isactive]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CustomerExceptions_AccountNumber]
	ON [dbo].[CustomerExceptions] ([AccountNumber], [MailTemplateid])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerExceptions] SET (LOCK_ESCALATION = TABLE)
GO
