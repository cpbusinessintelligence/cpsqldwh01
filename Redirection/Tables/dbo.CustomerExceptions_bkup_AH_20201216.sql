SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerExceptions_bkup_AH_20201216] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[AccountNumber]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MailTemplateid]      [int] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[isactive]            [bit] NULL
)
GO
ALTER TABLE [dbo].[CustomerExceptions_bkup_AH_20201216] SET (LOCK_ESCALATION = TABLE)
GO
