SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FailedDeliveryCardLeft] (
		[Sno]                      [int] IDENTITY(1, 1) NOT NULL,
		[Labelnumber]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CardLeft]                 [varchar](8000) COLLATE Latin1_General_CI_AS NULL,
		[Category]                 [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[Connote]                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryBusinessName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryContact]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddr1]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddr2]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddr3]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostcode]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryContactPhone]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryEmail]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ReadytoSendMail]          [bit] NOT NULL,
		[ReadytoSendSMS]           [bit] NOT NULL,
		[Createddatetime]          [datetime] NOT NULL,
		[CreatedBy]                [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[Editeddatetime]           [datetime] NOT NULL,
		[EditedBy]                 [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountNumber]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__FailedDe__CA1FE464DC7F3DC8]
		PRIMARY KEY
		CLUSTERED
		([Sno])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200811-201718]
	ON [dbo].[FailedDeliveryCardLeft] ([CardLeft])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[FailedDeliveryCardLeft] SET (LOCK_ESCALATION = TABLE)
GO
