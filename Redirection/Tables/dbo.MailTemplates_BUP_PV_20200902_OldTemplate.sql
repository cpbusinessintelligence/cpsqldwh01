SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate] (
		[Sno]                [int] IDENTITY(1, 1) NOT NULL,
		[Context]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Subject]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Body]               [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Addedby]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddedDatetime]      [datetime] NULL,
		[Editedby]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]     [datetime] NULL,
		[isactive]           [bit] NULL,
		[APIKey]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__MailTemp__CA1FE464BC064C61]
		PRIMARY KEY
		CLUSTERED
		([Sno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate]
	ADD
	CONSTRAINT [DF__MailTempl__Added__267ABA7A]
	DEFAULT ('Admin') FOR [Addedby]
GO
ALTER TABLE [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate]
	ADD
	CONSTRAINT [DF__MailTempl__Added__276EDEB3]
	DEFAULT (getdate()) FOR [AddedDatetime]
GO
ALTER TABLE [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate]
	ADD
	CONSTRAINT [DF__MailTempl__Edite__286302EC]
	DEFAULT ('Admin') FOR [Editedby]
GO
ALTER TABLE [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate]
	ADD
	CONSTRAINT [DF__MailTempl__Edite__29572725]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate]
	ADD
	CONSTRAINT [DF__MailTempl__isact__70DDC3D8]
	DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[MailTemplates_BUP_PV_20200902_OldTemplate] SET (LOCK_ESCALATION = TABLE)
GO
