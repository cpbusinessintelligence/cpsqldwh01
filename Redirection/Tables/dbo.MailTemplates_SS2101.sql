SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailTemplates_SS2101] (
		[Sno]                [int] IDENTITY(1, 1) NOT NULL,
		[Context]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Subject]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Body]               [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Addedby]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddedDatetime]      [datetime] NULL,
		[Editedby]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]     [datetime] NULL,
		[isactive]           [bit] NULL
)
GO
ALTER TABLE [dbo].[MailTemplates_SS2101] SET (LOCK_ESCALATION = TABLE)
GO
