SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMail] (
		[SendMailID]           [int] IDENTITY(1, 1) NOT NULL,
		[ContextID]            [int] NOT NULL,
		[FromAddr]             [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToAddr]               [varchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Bcc]                  [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Subject]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter6]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter7]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter8]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter9]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter10]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[TransmitFlag]         [int] NOT NULL,
		[AddWho]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]          [datetime] NOT NULL,
		[ModifiedDateTime]     [datetime] NULL,
		[iserror]              [bit] NULL,
		[ReturnAPICode]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_DGSendMail]
		PRIMARY KEY
		CLUSTERED
		([SendMailID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Bcc__01142BA1]
	DEFAULT ('') FOR [Bcc]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__02084FDA]
	DEFAULT ('') FOR [Parameter1]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__02FC7413]
	DEFAULT ('') FOR [Parameter2]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__03F0984C]
	DEFAULT ('') FOR [Parameter3]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__04E4BC85]
	DEFAULT ('') FOR [Parameter4]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__05D8E0BE]
	DEFAULT ('') FOR [Parameter5]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__06CD04F7]
	DEFAULT ('') FOR [Parameter6]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__07C12930]
	DEFAULT ('') FOR [Parameter7]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF_Addwho]
	DEFAULT ('Admin') FOR [AddWho]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__08B54D69]
	DEFAULT ('') FOR [Parameter8]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF_AddDateTime]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__09A971A2]
	DEFAULT ('') FOR [Parameter9]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF_ModifiedDateTime]
	DEFAULT (getdate()) FOR [ModifiedDateTime]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Parame__0A9D95DB]
	DEFAULT ('') FOR [Parameter10]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__Transm__0B91BA14]
	DEFAULT ((0)) FOR [TransmitFlag]
GO
ALTER TABLE [dbo].[SendMail]
	ADD
	CONSTRAINT [DF__SendMail__iserro__0C50D423]
	DEFAULT ((0)) FOR [iserror]
GO
CREATE NONCLUSTERED INDEX [IX_SendMail_TF]
	ON [dbo].[SendMail] ([TransmitFlag])
	INCLUDE ([Parameter1])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[SendMail] SET (LOCK_ESCALATION = TABLE)
GO
