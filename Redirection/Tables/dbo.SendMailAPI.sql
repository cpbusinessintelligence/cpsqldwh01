SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMailAPI] (
		[SendMailID]           [int] IDENTITY(1, 1) NOT NULL,
		[ContextID]            [int] NOT NULL,
		[FromAddr]             [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToAddr]               [varchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Bcc]                  [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Subject]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter6]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter7]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter8]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter9]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter10]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[TransmitFlag]         [int] NOT NULL,
		[AddWho]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]          [datetime] NOT NULL,
		[ModifiedDateTime]     [datetime] NULL,
		[iserror]              [bit] NULL,
		[ReturnAPICode]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_DGSendMail_1]
		PRIMARY KEY
		CLUSTERED
		([SendMailID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailAPI__Bcc__48EFCE0F]
	DEFAULT ('') FOR [Bcc]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__49E3F248]
	DEFAULT ('') FOR [Parameter1]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__4AD81681]
	DEFAULT ('') FOR [Parameter2]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__4BCC3ABA]
	DEFAULT ('') FOR [Parameter3]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__4CC05EF3]
	DEFAULT ('') FOR [Parameter4]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__4DB4832C]
	DEFAULT ('') FOR [Parameter5]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__4EA8A765]
	DEFAULT ('') FOR [Parameter6]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__4F9CCB9E]
	DEFAULT ('') FOR [Parameter7]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__5090EFD7]
	DEFAULT ('') FOR [Parameter8]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__51851410]
	DEFAULT ('') FOR [Parameter9]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Param__52793849]
	DEFAULT ('') FOR [Parameter10]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__Trans__536D5C82]
	DEFAULT ((0)) FOR [TransmitFlag]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF_AddwhoAPI]
	DEFAULT ('Admin') FOR [AddWho]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF_AddDateTimeAPI]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF_ModifiedDateTimeAPI]
	DEFAULT (getdate()) FOR [ModifiedDateTime]
GO
ALTER TABLE [dbo].[SendMailAPI]
	ADD
	CONSTRAINT [DF__SendMailA__iserr__573DED66]
	DEFAULT ((0)) FOR [iserror]
GO
ALTER TABLE [dbo].[SendMailAPI] SET (LOCK_ESCALATION = TABLE)
GO
