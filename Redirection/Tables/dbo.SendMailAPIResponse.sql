SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMailAPIResponse] (
		[Id]                  [int] IDENTITY(1, 1) NOT NULL,
		[ConnoteNo]           [varchar](400) COLLATE Latin1_General_CI_AS NULL,
		[ResponseCode]        [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[SendMailAPIResponse]
	ADD
	CONSTRAINT [DF__SendMailA__Creat__06ED0088]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[SendMailAPIResponse] SET (LOCK_ESCALATION = TABLE)
GO
