SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMailTest] (
		[FromAddr]        [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToAddr]          [varchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Subject]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[Body]            [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[parameter1]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter2]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter3]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter4]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter5]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter6]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter7]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter8]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter9]      [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[parameter10]     [varchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[BCC]             [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SendMailTest] SET (LOCK_ESCALATION = TABLE)
GO
