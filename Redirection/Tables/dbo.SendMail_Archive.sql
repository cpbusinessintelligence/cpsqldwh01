SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMail_Archive] (
		[SendMailID]           [int] NOT NULL,
		[ContextID]            [int] NOT NULL,
		[FromAddr]             [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToAddr]               [varchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Bcc]                  [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Subject]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter6]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter7]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter8]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter9]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter10]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[TransmitFlag]         [int] NOT NULL,
		[AddWho]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]          [datetime] NOT NULL,
		[ModifiedDateTime]     [datetime] NULL,
		[iserror]              [bit] NULL,
		[ReturnAPICode]        [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___iserr__004002F9]
	DEFAULT ((0)) FOR [iserror]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail_Ar__Bcc__73DA2C14]
	DEFAULT ('') FOR [Bcc]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__74CE504D]
	DEFAULT ('') FOR [Parameter1]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__75C27486]
	DEFAULT ('') FOR [Parameter2]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__76B698BF]
	DEFAULT ('') FOR [Parameter3]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__77AABCF8]
	DEFAULT ('') FOR [Parameter4]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__789EE131]
	DEFAULT ('') FOR [Parameter5]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__7993056A]
	DEFAULT ('') FOR [Parameter6]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__7A8729A3]
	DEFAULT ('') FOR [Parameter7]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__7B7B4DDC]
	DEFAULT ('') FOR [Parameter8]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__7C6F7215]
	DEFAULT ('') FOR [Parameter9]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Param__7D63964E]
	DEFAULT ('') FOR [Parameter10]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF__SendMail___Trans__7E57BA87]
	DEFAULT ((0)) FOR [TransmitFlag]
GO
ALTER TABLE [dbo].[SendMail_Archive]
	ADD
	CONSTRAINT [DF_Addwho_archive]
	DEFAULT ('Admin') FOR [AddWho]
GO
ALTER TABLE [dbo].[SendMail_Archive] SET (LOCK_ESCALATION = TABLE)
GO
