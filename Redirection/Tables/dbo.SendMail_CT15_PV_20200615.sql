SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMail_CT15_PV_20200615] (
		[SendMailID]           [int] IDENTITY(1, 1) NOT NULL,
		[ContextID]            [int] NOT NULL,
		[FromAddr]             [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToAddr]               [varchar](1000) COLLATE Latin1_General_CI_AS NOT NULL,
		[Bcc]                  [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Subject]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter6]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter7]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter8]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter9]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter10]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[TransmitFlag]         [int] NOT NULL,
		[AddWho]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]          [datetime] NOT NULL,
		[ModifiedDateTime]     [datetime] NULL,
		[iserror]              [bit] NULL,
		[ReturnAPICode]        [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SendMail_CT15_PV_20200615] SET (LOCK_ESCALATION = TABLE)
GO
