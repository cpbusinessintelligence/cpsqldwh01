SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMail_TEMP_JP_Test] (
		[SendMailID]           [int] NOT NULL,
		[ContextID]            [int] NOT NULL,
		[FromAddr]             [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[ToAddr]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Bcc]                  [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[Subject]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Parameter1]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter2]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter3]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter4]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter5]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter6]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter7]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter8]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter9]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Parameter10]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[TransmitFlag]         [int] NOT NULL,
		[AddWho]               [varchar](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[AddDateTime]          [datetime] NOT NULL,
		[ModifiedDateTime]     [datetime] NOT NULL,
		[iserror]              [int] NULL
)
GO
ALTER TABLE [dbo].[SendMail_TEMP_JP_Test] SET (LOCK_ESCALATION = TABLE)
GO
