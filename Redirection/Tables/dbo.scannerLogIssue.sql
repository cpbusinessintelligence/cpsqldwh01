SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[scannerLogIssue] (
		[session_id]                 [smallint] NOT NULL,
		[status]                     [nvarchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[Blk by]                     [smallint] NULL,
		[wait_type]                  [nvarchar](60) COLLATE Latin1_General_CI_AS NULL,
		[wait_resource]              [nvarchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
		[Wait Sec]                   [numeric](17, 6) NULL,
		[cpu_time]                   [int] NOT NULL,
		[logical_reads]              [bigint] NOT NULL,
		[reads]                      [bigint] NOT NULL,
		[writes]                     [bigint] NOT NULL,
		[Elaps Sec]                  [numeric](17, 6) NULL,
		[statement_text]             [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[command_text]               [nvarchar](776) COLLATE Latin1_General_CI_AS NULL,
		[command]                    [nvarchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[login_name]                 [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
		[host_name]                  [nvarchar](128) COLLATE Latin1_General_CI_AS NULL,
		[program_name]               [nvarchar](128) COLLATE Latin1_General_CI_AS NULL,
		[last_request_end_time]      [datetime] NULL,
		[login_time]                 [datetime] NOT NULL,
		[open_transaction_count]     [int] NOT NULL
)
GO
ALTER TABLE [dbo].[scannerLogIssue] SET (LOCK_ESCALATION = TABLE)
GO
