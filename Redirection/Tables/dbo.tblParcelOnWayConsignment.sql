SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblParcelOnWayConsignment] (
		[ID]                       [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]            [int] NULL,
		[ConsignmentNumber]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryBusinessName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress1]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress2]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress3]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]           [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostcode]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]            [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryEmail]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPhone]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupDatetime]           [datetime] NULL,
		[Sourcereference]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReadytoSendMail]          [bit] NULL,
		[ReadytoSendSMS]           [bit] NULL,
		[CreatedDatetime]          [datetime] NULL,
		[CreatedBy]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]           [datetime] NULL,
		[EditedBy]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tblParcelOnWayConsignment]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment]
	ADD
	CONSTRAINT [DF__tblParcel__Edite__00DF2177]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment]
	ADD
	CONSTRAINT [DF__tblParcel__Edite__01D345B0]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment]
	ADD
	CONSTRAINT [DF__tblParcel__Picku__7C1A6C5A]
	DEFAULT (NULL) FOR [PickupDatetime]
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment]
	ADD
	CONSTRAINT [DF__tblParcel__Ready__7D0E9093]
	DEFAULT ((0)) FOR [ReadytoSendMail]
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment]
	ADD
	CONSTRAINT [DF__tblParcel__Ready__7E02B4CC]
	DEFAULT ((0)) FOR [ReadytoSendSMS]
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment]
	ADD
	CONSTRAINT [DF__tblParcel__Creat__7EF6D905]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment]
	ADD
	CONSTRAINT [DF__tblParcel__Creat__7FEAFD3E]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
CREATE NONCLUSTERED INDEX [IX_tblParcelOnWayConsignment-20160913-094623]
	ON [dbo].[tblParcelOnWayConsignment] ([ConsignmentID], [AccountNumber], [PickupDatetime], [ReadytoSendMail], [Sourcereference])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20180208-201032]
	ON [dbo].[tblParcelOnWayConsignment] ([ConsignmentNumber])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment] SET (LOCK_ESCALATION = TABLE)
GO
