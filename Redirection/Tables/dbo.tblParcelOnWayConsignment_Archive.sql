SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblParcelOnWayConsignment_Archive] (
		[ID]                       [int] NULL,
		[ConsignmentID]            [int] NULL,
		[ConsignmentNumber]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AccountNumber]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryBusinessName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress1]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress2]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress3]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]           [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostcode]         [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryState]            [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryEmail]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPhone]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupDatetime]           [datetime] NULL,
		[Sourcereference]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReadytoSendMail]          [bit] NULL,
		[ReadytoSendSMS]           [bit] NULL,
		[CreatedDatetime]          [datetime] NULL,
		[CreatedBy]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]           [datetime] NULL,
		[EditedBy]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblParcelOnWayConsignment_Archive] SET (LOCK_ESCALATION = TABLE)
GO
