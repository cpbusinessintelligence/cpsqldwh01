SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblParcelOnWayLabel] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[ConID]               [int] NOT NULL,
		[LabelNumber]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__tblParce__3214EC273CD0A534]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblParcelOnWayLabel]
	ADD
	CONSTRAINT [DF__tblParcel__Creat__02C769E9]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[tblParcelOnWayLabel]
	ADD
	CONSTRAINT [DF__tblParcel__Creat__03BB8E22]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblParcelOnWayLabel]
	ADD
	CONSTRAINT [DF__tblParcel__Edite__04AFB25B]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[tblParcelOnWayLabel]
	ADD
	CONSTRAINT [DF__tblParcel__Edite__05A3D694]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[tblParcelOnWayLabel]
	WITH CHECK
	ADD CONSTRAINT [FK__tblParcel__ConID__0697FACD]
	FOREIGN KEY ([ConID]) REFERENCES [dbo].[tblParcelOnWayConsignment] ([ID])
ALTER TABLE [dbo].[tblParcelOnWayLabel]
	CHECK CONSTRAINT [FK__tblParcel__ConID__0697FACD]

GO
ALTER TABLE [dbo].[tblParcelOnWayLabel] SET (LOCK_ESCALATION = TABLE)
GO
