SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblParcelOnWayLabel_Archive] (
		[ID]                  [int] NULL,
		[ConID]               [int] NULL,
		[LabelNumber]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblParcelOnWayLabel_Archive] SET (LOCK_ESCALATION = TABLE)
GO
