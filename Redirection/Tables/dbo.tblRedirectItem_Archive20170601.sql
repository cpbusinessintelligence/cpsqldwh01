SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedirectItem_Archive20170601] (
		[ItemID]              [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]       [int] NOT NULL,
		[LabelNumber]         [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[Length]              [decimal](10, 2) NULL,
		[Width]               [decimal](10, 2) NULL,
		[Height]              [decimal](10, 2) NULL,
		[CubicWeight]         [decimal](10, 2) NULL,
		[PhysicalWeight]      [decimal](10, 2) NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblRedirectItem_Archive20170601] SET (LOCK_ESCALATION = TABLE)
GO
