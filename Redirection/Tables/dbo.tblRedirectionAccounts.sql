SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedirectionAccounts] (
		[Sno]                  [int] IDENTITY(1, 1) NOT NULL,
		[Accountnumber]        [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AddedBy]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]          [datetime] NOT NULL,
		[ModifiedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ModifiedDateTime]     [datetime] NULL,
		CONSTRAINT [PK_tblredirectionaccounts]
		PRIMARY KEY
		CLUSTERED
		([Accountnumber])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblRedirectionAccounts]
	ADD
	CONSTRAINT [DF__tblRedire__Added__2DB1C7EE]
	DEFAULT ('Admin') FOR [AddedBy]
GO
ALTER TABLE [dbo].[tblRedirectionAccounts]
	ADD
	CONSTRAINT [DF__tblRedire__AddDa__2EA5EC27]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[tblRedirectionAccounts]
	ADD
	CONSTRAINT [DF__tblRedire__Modif__2F9A1060]
	DEFAULT ('Admin') FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[tblRedirectionAccounts]
	ADD
	CONSTRAINT [DF__tblRedire__Modif__308E3499]
	DEFAULT (getdate()) FOR [ModifiedDateTime]
GO
ALTER TABLE [dbo].[tblRedirectionAccounts] SET (LOCK_ESCALATION = TABLE)
GO
