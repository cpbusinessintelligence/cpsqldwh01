SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedirectionPostcodes] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[Postcode]            [int] NULL,
		[suburb]              [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[State]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PricingZone]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblRedirectionPostcodes]
	ADD
	CONSTRAINT [DF__tblRedire__Creat__373B3228]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[tblRedirectionPostcodes]
	ADD
	CONSTRAINT [DF__tblRedire__Creat__382F5661]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblRedirectionPostcodes]
	ADD
	CONSTRAINT [DF__tblRedire__Edite__39237A9A]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[tblRedirectionPostcodes]
	ADD
	CONSTRAINT [DF__tblRedire__Edite__3A179ED3]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[tblRedirectionPostcodes] SET (LOCK_ESCALATION = TABLE)
GO
