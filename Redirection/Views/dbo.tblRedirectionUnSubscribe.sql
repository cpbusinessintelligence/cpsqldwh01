SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
Create view tblRedirectionUnSubscribe as

SELECT  [Sno]
      ,[Type]
      ,[Value]
      ,[AddedBy]
      ,[AddDateTime]
      ,[ModifiedBy]
      ,[ModifiedDateTime]
  FROM cpsqlops01.[CouponCalculator].[dbo].[tblRedirectionUnSubscribe]

GO
