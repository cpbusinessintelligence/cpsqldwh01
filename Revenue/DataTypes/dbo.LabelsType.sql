CREATE TYPE [dbo].[LabelsType]
AS TABLE (
		[LabelNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
