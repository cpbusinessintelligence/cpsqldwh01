CREATE TYPE [dbo].[LinkLabelsType]
AS TABLE (
		[LabelNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LinkLabelNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
