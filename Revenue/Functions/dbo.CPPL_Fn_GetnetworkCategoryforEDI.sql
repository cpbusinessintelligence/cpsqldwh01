SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CPPL_Fn_GetnetworkCategoryforEDI](@OriginZone varchar(10),@DestinationZone varchar(10),@ogstate varchar(20),@dgstate varchar(20))
returns varchar(30)
 as
begin
declare @network varchar(30)

	set @network=	CASE WHEN (LTRIM(RTRIM(@OriginZone)) = LTRIM(RTRIM(@DestinationZone))
							AND LTRIM(RTRIM(@OriginZone)) IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
													'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL'))
						THEN 'METRO'
					ELSE
						CASE WHEN (@OriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
														'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND @DestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND ((ISNULL(@ogstate, 'XX') = ISNULL(@dgstate, 'ZZ'))
											OR (ISNULL(@ogstate, 'XX') IN ('ACT','NSW')
												AND ISNULL(@dgstate, 'ZZ') IN ('ACT','NSW'))))
							THEN  'REGIONAL'
						ELSE
							CASE WHEN ((ISNULL(@ogstate, 'XX') = ISNULL(@dgstate, 'ZZ'))
											OR (ISNULL(@ogstate, '') IN ('ACT','NSW')
												AND ISNULL(@ogstate, 'ZZ') IN ('ACT','NSW')))
								THEN 'INTRASTATE'
							ELSE
								'INTERSTATE'
				end
				end
				end
					
	return @network
	end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_Fn_GetnetworkCategoryforEDI]
	TO [SSISUser]
GO
