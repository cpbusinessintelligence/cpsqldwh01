SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue]
(
	@ProntoOrderContractorCode		varchar(20),
	@ReturnsFlag					bit,
	@FirstScanBranch				varchar(20),
	@FirstScanDepot					varchar(40),
	@OtherScanBranch				varchar(20),
	@OtherScanDepot					varchar(40),
	@LabelPrefix					varchar(3),
	@ConsolidateOriginBranch		varchar(20),
	@ConsolidateDestinationBranch	varchar(20),
	@ConsignmentOriginState			varchar(10),
	@ConsignmentDestinationState	varchar(10)
)
RETURNS varchar(8)
AS
BEGIN

	DECLARE @ret varchar(8);
	SELECT @ret = Null;
	
		-- BU gets set first from the original contractor who sold the items, or if that
		-- information is not available, based on other criteria
		--
		IF LTRIM(RTRIM(ISNULL(@ProntoOrderContractorCode, ''))) != ''
		BEGIN
			SELECT @ret = CASE LEFT(LTRIM(RTRIM(@ProntoOrderContractorCode)), 1)
				-- can't automatically allocate Sydney or Brisbane here, as
				-- it might be Sunshine Coast or Central Coast
				WHEN 'G' THEN 'COO'
				WHEN 'P' THEN 'CPE'
				WHEN 'W' THEN 'CPE'
				WHEN 'C' THEN 'CCB'
			END;
		END

		-- determine business unit for non-returns products
		--
		IF (((ISNULL(@ReturnsFlag, 0) = 0)) AND (ISNULL(@ret, '') = ''))
		BEGIN
		
			IF ((ISNULL(@FirstScanBranch, '') = 'SYDNEY') OR (LEFT(ISNULL(@ProntoOrderContractorCode, ''), 1) = 'S'))
			BEGIN
				IF (ISNULL(@FirstScanDepot, '') = 'CANBERRA')						
					SELECT @ret = 'CCB';
				ELSE
				IF (ISNULL(@FirstScanDepot, '') IN ('CARDIFF','CENTRAL COAST'))
					SELECT @ret = 'CCC';
                ELSE
				IF (ISNULL(@FirstScanDepot, '') = 'COFFS HARBOUR')
					SELECT @ret = 'CCF';
				ELSE
					SELECT @ret = 'CSY';	
			END
			ELSE
			IF (ISNULL(@FirstScanBranch, '') = 'MELBOURNE')
					
			BEGIN
			IF (ISNULL(@FirstScanDepot, '') in ('VFPD ALBURY', 'ALBURY'))						
					SELECT @ret = 'CAB';
			ELSE
				SELECT @ret = 'CME';
			END
			ELSE
			IF (ISNULL(@FirstScanBranch, '') = 'ADELAIDE')
			BEGIN
			IF (ISNULL(@FirstScanDepot, '') = 'DARWIN')						
					SELECT @ret = 'CDA';
			ELSE
				SELECT @ret = 'CAD';
			END
			ELSE
			IF ((ISNULL(@FirstScanBranch, '') = 'BRISBANE')	OR (LEFT(ISNULL(@ProntoOrderContractorCode, ''), 1) = 'B'))		
			BEGIN
				IF (ISNULL(@FirstScanDepot, '') = 'SUNSHINE COAST')						
					SELECT @ret = 'CSC';
				ELSE
				IF (ISNULL(@FirstScanDepot, '') in ('CAIRNS','CAIRNS DEPOT'))						
					SELECT @ret = 'CCA';
				ELSE			
					SELECT @ret = 'CBN';			
			END
			ELSE
			IF (ISNULL(@FirstScanBranch, '') = 'GOLDCOAST')
				   SELECT @ret = 'COO';
			ELSE
			IF (ISNULL(@FirstScanBranch, '') = 'PERTH')				
				SELECT @ret = 'CPE';	
			ELSE
			IF LTRIM(RTRIM(ISNULL(@FirstScanBranch, ''))) != ''
			BEGIN
				-- some other unknown branch
				SELECT @ret = 'UNKNOWN';
			END
			
			--
			-- if we still don't have a BU yet, then we'll work off the consolidation
			-- barcode or the consignment information
			--
			IF (ISNULL(@ret, '') = '')
			BEGIN
				IF ISNULL(@ConsolidateOriginBranch, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsolidateOriginBranch
						WHEN 'ADELAIDE' THEN 'CAD'
						WHEN 'PERTH' THEN 'CPE'
						WHEN 'MELBOURNE' THEN 'CME'
						WHEN 'SYDNEY' THEN 'CSY'
						WHEN 'CANBERRA' THEN 'CCB'
						WHEN 'BRISBANE' THEN 'CBN'
						WHEN 'GOLDCOAST' THEN 'COO'
					END
				END
				ELSE
				IF ISNULL(@ConsignmentOriginState, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsignmentOriginState
						WHEN 'SA' THEN 'CAD'
						WHEN 'NT' THEN 'CAD'
						WHEN 'WA' THEN 'CPE'
						WHEN 'QLD' THEN 'CBN'
						WHEN 'VIC' THEN 'CME'
						WHEN 'TAS' THEN 'CME'
						WHEN 'NSW' THEN 'CSY'
						WHEN 'ACT' THEN 'CCB'
					END
				END
			END		-- (ISNULL(@ret, '') = '')
		
		END		-- ((@ServiceCategory != 'RETURNS') AND (ISNULL(@BUCode, '') = ''))
		ELSE
		IF (((ISNULL(@ReturnsFlag, 0) = 1)) AND (ISNULL(@ret, '') = ''))
		BEGIN

			--
			-- for returns, first we'll base it on the prefix, if it's branch specific
			--
			IF ISNULL(@OtherScanBranch, '') = 'SYDNEY'
					
			BEGIN
				IF (ISNULL(@OtherScanDepot, '') = 'CANBERRA')						
					SELECT @ret = 'CCB';				
				ELSE
				IF (ISNULL(@OtherScanDepot, '') IN ('CARDIFF','CENTRAL COAST'))
					SELECT @ret = 'CCC';

				IF (ISNULL(@OtherScanDepot, '') = 'COFFS HARBOUR')
					SELECT @ret = 'CCF';
				ELSE
					SELECT @ret = 'CSY';
			END
			ELSE
			IF ISNULL(@OtherScanBranch, '') = 'MELBOURNE'
			BEGIN
			IF (ISNULL(@OtherScanDepot, '') in ('VFPD ALBURY', 'ALBURY'))						
					SELECT @ret = 'CAB';
			ELSE
				SELECT @ret = 'CME';
			END
			ELSE
			IF ISNULL(@OtherScanBranch, '') = 'ADELAIDE'
			BEGIN
			IF (ISNULL(@OtherScanDepot, '') = 'DARWIN')						
			   SELECT @ret = 'CDA';
             ELSE
			   SELECT @ret = 'CAD';
			END
			ELSE
			IF ISNULL(@OtherScanBranch, '') = 'BRISBANE'
					
			BEGIN
				IF (ISNULL(@OtherScanDepot, '') = 'SUNSHINE COAST')
					SELECT @ret = 'CSC';
				ELSE
				IF (ISNULL(@OtherScanDepot, '') in ('CAIRNS DEPOT', 'CAIRNS'))					
					SELECT @ret = 'CCA';			
						
				ELSE
					SELECT @ret = 'CBN';
			END
			ELSE
			IF (ISNULL(@OtherScanBranch, '') = 'GOLDCOAST')
				SELECT @ret = 'COO';
			ELSE
			IF (ISNULL(@OtherScanBranch, '') = 'PERTH')
				SELECT @ret = 'CPE';
			
			--
			-- if we still don't have a BU yet, then we'll work off the consolidation
			-- barcode or the consignment information
			--
			IF (ISNULL(@ret, '') = '')
			BEGIN
				IF ISNULL(@ConsolidateDestinationBranch, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsolidateOriginBranch
						WHEN 'ADELAIDE' THEN 'CAD'
						WHEN 'PERTH' THEN 'CPE'
						WHEN 'MELBOURNE' THEN 'CME'
						WHEN 'SYDNEY' THEN 'CSY'
						WHEN 'CANBERRA' THEN 'CCB'
						WHEN 'BRISBANE' THEN 'CBN'
						WHEN 'GOLDCOAST' THEN 'COO'
					END
				END
				ELSE
				IF ISNULL(@ConsignmentDestinationState, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsignmentOriginState
						WHEN 'SA' THEN 'CAD'
						WHEN 'NT' THEN 'CAD'
						WHEN 'WA' THEN 'CPE'
						WHEN 'QLD' THEN 'CBN'
						WHEN 'VIC' THEN 'CME'
						WHEN 'TAS' THEN 'CME'
						WHEN 'NSW' THEN 'CSY'
						WHEN 'ACT' THEN 'CCB'
					END
				END
			END		-- (ISNULL(@ret, '') = '')

			--
			-- if there's still no BU, then we'll work off whatever
			-- branch/depot scan information is available
			--
			-- (ISNULL(@ret, '') = '')
			
		END
		
		-- 
		-- last chance check here - if it's a redelivery card or coupon, and there's an "other" scan,
		-- then we'll allocate the revenue to the corresponding business unit.
		--
		-- Essentially, there is $0.00 revenue on these anyway, but it allocates them to the correct branch
		-- we only do this for redelivery cards/coupons as they seem to be orphaned reasonably frequently
		--
		IF (
				((ISNULL(@ret, '') = '') AND ((@FirstScanBranch Is Not Null) OR (@OtherScanBranch Is Not Null)))
				AND
				(ISNULL(@LabelPrefix, '') IN ('190','191')) -- redelivery cards/coupons
		)
		BEGIN
			IF (@OtherScanBranch Is Not Null)
			BEGIN
				SELECT @ret = CASE LTRIM(RTRIM(@OtherScanBranch))
					WHEN 'SYDNEY' THEN
						CASE ISNULL(@OtherScanDepot, '')
							WHEN 'CANBERRA' THEN 'CCB'
							WHEN 'CENTRAL COAST' THEN 'CCC'
							WHEN 'CARDIFF' THEN 'CCC'
							WHEN 'COFFS HARBOUR' THEN 'CCF'
							ELSE 'CSY'
						END
					WHEN 'BRISBANE' THEN
						CASE ISNULL(@OtherScanDepot, '')
							WHEN 'SUNSHINE COAST' THEN 'CSC'
							WHEN 'CAIRNS' THEN 'CCA'
							WHEN 'CAIRNS DEPOT' THEN 'CCA'
							ELSE 'CBN'
						END
					WHEN 'GOLDCOAST' THEN 'COO'
					WHEN 'ADELAIDE' THEN 
					    CASE ISNULL(@OtherScanDepot, '')
						WHEN 'DARWIN' THEN 'CDA'
						ELSE 'CAD'
						END
					WHEN 'MELBOURNE' THEN
					    CASE ISNULL(@OtherScanDepot, '') 
						WHEN 'ALBURY' THEN 'CAB'
						WHEN 'VFPD ALBURY' THEN 'CAB'
						ELSE 'CME'
						END
					WHEN 'PERTH' THEN 'CPE'
				END
			END
			ELSE
			IF (@FirstScanBranch Is Not Null)
			BEGIN
				SELECT @ret = CASE LTRIM(RTRIM(@FirstScanBranch))
					WHEN 'SYDNEY' THEN
						CASE ISNULL(@FirstScanDepot, '')
							WHEN 'CANBERRA' THEN 'CCB'
							WHEN 'CENTRAL COAST' THEN 'CCC'
							WHEN 'CARDIFF' THEN 'CCC'
							WHEN 'COFFS HARBOUR' THEN 'CCF'
							ELSE 'CSY'
						END
					WHEN 'BRISBANE' THEN
						CASE ISNULL(@FirstScanDepot, '')
							WHEN 'SUNSHINE COAST' THEN 'CSC'
							WHEN 'CAIRNS' THEN 'CCA'
							WHEN 'CAIRNS DEPOT' THEN 'CCA'
							ELSE 'CBN'
						END
					WHEN 'GOLDCOAST' THEN 'COO'
					WHEN 'ADELAIDE' THEN 
					    CASE ISNULL(@FirstScanDepot, '')
						WHEN 'DARWIN' THEN 'CDA'
						ELSE 'CAD'
						END
					WHEN 'MELBOURNE' THEN
					    CASE ISNULL(@FirstScanDepot, '') 
						WHEN 'ALBURY' THEN 'CAB'
						WHEN 'VFPD ALBURY' THEN 'CAB'
						ELSE 'CME'
						END
					WHEN 'PERTH' THEN 'CPE'
				END
			END
		END		-- redelivery cpns/card check


	IF (ISNULL(@ret, '') = '' and LTRIM(RTRIM(ISNULL(@ProntoOrderContractorCode, ''))) != '')


	SELECT @ret = CASE LEFT(LTRIM(RTRIM(@ProntoOrderContractorCode)), 1)
				-- can't automatically allocate Sydney or Brisbane here, as
				-- it might be Sunshine Coast or Central Coast
				WHEN 'A' THEN 'CAD'
				WHEN 'M' THEN 'CME'
				WHEN 'G' THEN 'COO'
				WHEN 'P' THEN 'CPE'
				WHEN 'C' THEN 'CCB'
				WHEN 'S' THEN 'CSY'
				WHEN 'B' THEN 'CBN'
				else 'UNKNOWN' end


		
		SELECT @ret = case when LEFT(@LabelPrefix, 1) = '9' then 'CCB'
	                   when LEFT(@LabelPrefix, 1) = '7' then 'CCC'
					   when LEFT(@LabelPrefix, 1) = '6' then 'CSY'
					   when LEFT(@LabelPrefix, 1) = '2' then 'CAD'
					   when LEFT(@LabelPrefix, 1) = '5' then 'CME'
					   when LEFT(@LabelPrefix, 1) = '4' then 'COO'
					   when LEFT(@LabelPrefix, 1) = '8' then 'CPE'
					   when @LabelPrefix IN ('341','342')  then 'CSC'
                       when @LabelPrefix not IN ('341','342') and LEFT(@LabelPrefix, 1) = '3' then 'CBN'
					   else 'UNKNOWN' end
				where ISNULL(@ret, '') = 'UNKNOWN' 


	RETURN @ret;

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue]
	TO [SSISUser]
GO
