SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue_BUP_JP_20151127]
(
	@ProntoOrderContractorCode		varchar(20),
	@ReturnsFlag					bit,
	@FirstScanBranch				varchar(20),
	@FirstScanDepot					varchar(40),
	@OtherScanBranch				varchar(20),
	@OtherScanDepot					varchar(40),
	@LabelPrefix					varchar(3),
	@ConsolidateOriginBranch		varchar(20),
	@ConsolidateDestinationBranch	varchar(20),
	@ConsignmentOriginState			varchar(10),
	@ConsignmentDestinationState	varchar(10)
)
RETURNS varchar(8)
AS
BEGIN

	DECLARE @ret varchar(8);
	SELECT @ret = Null;
	
		-- BU gets set first from the original contractor who sold the items, or if that
		-- information is not available, based on other criteria
		--
		IF LTRIM(RTRIM(ISNULL(@ProntoOrderContractorCode, ''))) != ''
		BEGIN
			SELECT @ret = CASE LEFT(LTRIM(RTRIM(@ProntoOrderContractorCode)), 1)
				-- can't automatically allocate Sydney or Brisbane here, as
				-- it might be Sunshine Coast or Central Coast
				WHEN 'A' THEN 'CAD'
				WHEN 'M' THEN 'CME'
				WHEN 'G' THEN 'COO'
				WHEN 'P' THEN 'CPE'
				WHEN 'C' THEN 'CCB'
			END;
		END

		-- determine business unit for non-returns products
		--
		IF (((ISNULL(@ReturnsFlag, 0) = 0)) AND (ISNULL(@ret, '') = ''))
		BEGIN
		
			IF ((ISNULL(@FirstScanBranch, '') = 'SYDNEY')
					OR (LEFT(ISNULL(@ProntoOrderContractorCode, ''), 1) = 'S')
					OR (LEFT(@LabelPrefix, 1) IN ('6','7','9')))
			BEGIN
				IF ((ISNULL(@FirstScanDepot, '') = 'CANBERRA')
						OR (LEFT(@LabelPrefix, 1) = '9'))
				BEGIN
					SELECT @ret = 'CCB';
				END
				ELSE
				IF ((ISNULL(@FirstScanDepot, '') IN ('CARDIFF','CENTRAL COAST'))
						OR (LEFT(@LabelPrefix, 1) = '7'))
				BEGIN
					SELECT @ret = 'CCC';
				END
				ELSE
				BEGIN
					SELECT @ret = 'CSY';
				END
			END
			ELSE
			IF ((ISNULL(@FirstScanBranch, '') = 'MELBOURNE')
					OR (LEFT(@LabelPrefix, 1) = '5'))
			BEGIN
				SELECT @ret = 'CME';
			END
			ELSE
			IF ((ISNULL(@FirstScanBranch, '') = 'ADELAIDE')
					OR (LEFT(@LabelPrefix, 1) = '2'))
			BEGIN
				SELECT @ret = 'CAD';
			END
			ELSE
			IF ((ISNULL(@FirstScanBranch, '') = 'BRISBANE')
					OR (LEFT(ISNULL(@ProntoOrderContractorCode, ''), 1) = 'B')
					OR (LEFT(@LabelPrefix, 1) IN ('3')))
			BEGIN
				IF ((ISNULL(@FirstScanDepot, '') = 'SUNSHINE COAST')
						OR (@LabelPrefix IN ('341','342')))
				BEGIN
					SELECT @ret = 'CSC';
				END
				ELSE
				BEGIN
					SELECT @ret = 'CBN';
				END
			END
			ELSE
			IF ((ISNULL(@FirstScanBranch, '') = 'GOLDCOAST')
					OR (LEFT(@LabelPrefix, 1) = '4'))
			BEGIN
				SELECT @ret = 'COO';
			END
			ELSE
			IF ((ISNULL(@FirstScanBranch, '') = 'PERTH')
					OR (LEFT(@LabelPrefix, 1) = '8'))
			BEGIN
				SELECT @ret = 'CPE';
			END
			ELSE
			IF LTRIM(RTRIM(ISNULL(@FirstScanBranch, ''))) != ''
			BEGIN
				-- some other unknown branch
				SELECT @ret = 'UNKNOWN';
			END
			
			--
			-- if we still don't have a BU yet, then we'll work off the consolidation
			-- barcode or the consignment information
			--
			IF (ISNULL(@ret, '') = '')
			BEGIN
				IF ISNULL(@ConsolidateOriginBranch, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsolidateOriginBranch
						WHEN 'ADELAIDE' THEN 'CAD'
						WHEN 'PERTH' THEN 'CPE'
						WHEN 'MELBOURNE' THEN 'CME'
						WHEN 'SYDNEY' THEN 'CSY'
						WHEN 'CANBERRA' THEN 'CCB'
						WHEN 'BRISBANE' THEN 'CBN'
						WHEN 'GOLDCOAST' THEN 'COO'
					END
				END
				ELSE
				IF ISNULL(@ConsignmentOriginState, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsignmentOriginState
						WHEN 'SA' THEN 'CAD'
						WHEN 'NT' THEN 'CAD'
						WHEN 'WA' THEN 'CPE'
						WHEN 'QLD' THEN 'CBN'
						WHEN 'VIC' THEN 'CME'
						WHEN 'TAS' THEN 'CME'
						WHEN 'NSW' THEN 'CSY'
						WHEN 'ACT' THEN 'CCB'
					END
				END
			END		-- (ISNULL(@ret, '') = '')
		
		END		-- ((@ServiceCategory != 'RETURNS') AND (ISNULL(@BUCode, '') = ''))
		ELSE
		IF (((ISNULL(@ReturnsFlag, 0) = 1)) AND (ISNULL(@ret, '') = ''))
		BEGIN

			--
			-- for returns, first we'll base it on the prefix, if it's branch specific
			--
			IF ((LEFT(ISNULL(@ProntoOrderContractorCode, ''), 1) = 'S')
					OR (LEFT(@LabelPrefix, 1) IN ('6','7','9')))
			BEGIN
				IF ((ISNULL(@OtherScanDepot, '') = 'CANBERRA')
						OR (LEFT(@LabelPrefix, 1) = '9'))
				BEGIN
					SELECT @ret = 'CCB';
				END
				ELSE
				IF ((ISNULL(@OtherScanDepot, '') IN ('CARDIFF','CENTRAL COAST'))
						OR (LEFT(@LabelPrefix, 1) = '7'))
				BEGIN
					SELECT @ret = 'CCC';
				END
				ELSE
				BEGIN
					SELECT @ret = 'CSY';
				END
			END
			ELSE
			IF ((LEFT(@LabelPrefix, 1) = '5'))
			BEGIN
				SELECT @ret = 'CME';
			END
			ELSE
			IF ((LEFT(@LabelPrefix, 1) = '2'))
			BEGIN
				SELECT @ret = 'CAD';
			END
			ELSE
			IF ((LEFT(ISNULL(@ProntoOrderContractorCode, ''), 1) = 'B')
					OR (LEFT(@LabelPrefix, 1) IN ('3')))
			BEGIN
				IF ((ISNULL(@OtherScanDepot, '') = 'SUNSHINE COAST')
						OR (@LabelPrefix IN ('341','342')))
				BEGIN
					SELECT @ret = 'CSC';
				END
				ELSE
				BEGIN
					SELECT @ret = 'CBN';
				END
			END
			ELSE
			IF ((ISNULL(@OtherScanBranch, '') = 'GOLDCOAST')
					OR (LEFT(@LabelPrefix, 1) = '4'))
			BEGIN
				SELECT @ret = 'COO';
			END
			ELSE
			IF ((ISNULL(@OtherScanBranch, '') = 'PERTH')
					OR (LEFT(@LabelPrefix, 1) = '8'))
			BEGIN
				SELECT @ret = 'CPE';
			END
			
			--
			-- if we still don't have a BU yet, then we'll work off the consolidation
			-- barcode or the consignment information
			--
			IF (ISNULL(@ret, '') = '')
			BEGIN
				IF ISNULL(@ConsolidateDestinationBranch, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsolidateOriginBranch
						WHEN 'ADELAIDE' THEN 'CAD'
						WHEN 'PERTH' THEN 'CPE'
						WHEN 'MELBOURNE' THEN 'CME'
						WHEN 'SYDNEY' THEN 'CSY'
						WHEN 'CANBERRA' THEN 'CCB'
						WHEN 'BRISBANE' THEN 'CBN'
						WHEN 'GOLDCOAST' THEN 'COO'
					END
				END
				ELSE
				IF ISNULL(@ConsignmentDestinationState, '') != ''
				BEGIN
					SELECT @ret = CASE @ConsignmentOriginState
						WHEN 'SA' THEN 'CAD'
						WHEN 'NT' THEN 'CAD'
						WHEN 'WA' THEN 'CPE'
						WHEN 'QLD' THEN 'CBN'
						WHEN 'VIC' THEN 'CME'
						WHEN 'TAS' THEN 'CME'
						WHEN 'NSW' THEN 'CSY'
						WHEN 'ACT' THEN 'CCB'
					END
				END
			END		-- (ISNULL(@ret, '') = '')

			--
			-- if there's still no BU, then we'll work off whatever
			-- branch/depot scan information is available
			--
			IF (ISNULL(@ret, '') = '')
			BEGIN
				IF (ISNULL(@OtherScanBranch, '') != '')
				BEGIN
					SELECT @ret = CASE @OtherScanBranch
						WHEN 'SYDNEY' THEN
							CASE ISNULL(@OtherScanDepot, '')
								WHEN 'CENTRAL COAST' THEN 'CCC'
								WHEN 'CANBERRA' THEN 'CCC'
								WHEN 'CARDIFF' THEN 'CCC'
								ELSE 'CSY'
							END
						WHEN 'ADELAIDE' THEN 'CAD'
						WHEN 'MELBOURNE' THEN 'CME'
						WHEN 'BRISBANE' THEN
							CASE ISNULL(@OtherScanDepot, '')
								WHEN 'SUNSHINE COAST' THEN 'CSC'
								ELSE 'CBN'
							END
						WHEN 'GOLDCOAST' THEN 'COO'
						WHEN 'PERTH' THEN 'CPE'
					END
				END
			END		-- (ISNULL(@ret, '') = '')
			
		END
		
		-- 
		-- last chance check here - if it's a redelivery card or coupon, and there's an "other" scan,
		-- then we'll allocate the revenue to the corresponding business unit.
		--
		-- Essentially, there is $0.00 revenue on these anyway, but it allocates them to the correct branch
		-- we only do this for redelivery cards/coupons as they seem to be orphaned reasonably frequently
		--
		IF (
				((ISNULL(@ret, '') = '') AND ((@FirstScanBranch Is Not Null) OR (@OtherScanBranch Is Not Null)))
				AND
				(ISNULL(@LabelPrefix, '') IN ('190','191')) -- redelivery cards/coupons
		)
		BEGIN
			IF (@OtherScanBranch Is Not Null)
			BEGIN
				SELECT @ret = CASE LTRIM(RTRIM(@OtherScanBranch))
					WHEN 'SYDNEY' THEN
						CASE ISNULL(@OtherScanDepot, '')
							WHEN 'CANBERRA' THEN 'CCB'
							WHEN 'CENTRAL COAST' THEN 'CCC'
							WHEN 'CARDIFF' THEN 'CCC'
							ELSE 'CSY'
						END
					WHEN 'BRISBANE' THEN
						CASE ISNULL(@OtherScanDepot, '')
							WHEN 'SUNSHINE COAST' THEN 'CSC'
							ELSE 'CBN'
						END
					WHEN 'GOLDCOAST' THEN 'COO'
					WHEN 'ADELAIDE' THEN 'CAD'
					WHEN 'MELBOURNE' THEN 'CME'
					WHEN 'PERTH' THEN 'CPE'
				END
			END
			ELSE
			IF (@FirstScanBranch Is Not Null)
			BEGIN
				SELECT @ret = CASE LTRIM(RTRIM(@FirstScanBranch))
					WHEN 'SYDNEY' THEN
						CASE ISNULL(@FirstScanDepot, '')
							WHEN 'CANBERRA' THEN 'CCB'
							WHEN 'CENTRAL COAST' THEN 'CCC'
							WHEN 'CARDIFF' THEN 'CCC'
							ELSE 'CSY'
						END
					WHEN 'BRISBANE' THEN
						CASE ISNULL(@FirstScanDepot, '')
							WHEN 'SUNSHINE COAST' THEN 'CSC'
							ELSE 'CBN'
						END
					WHEN 'GOLDCOAST' THEN 'COO'
					WHEN 'ADELAIDE' THEN 'CAD'
					WHEN 'MELBOURNE' THEN 'CME'
					WHEN 'PERTH' THEN 'CPE'
				END
			END
		END		-- redelivery cpns/card check


	IF (ISNULL(@ret, '') = '')
	BEGIN
		SELECT @ret = 'UNKNOWN';
	END

	RETURN @ret;

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue_BUP_JP_20151127]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue_BUP_JP_20151127]
	TO [SSISUser]
GO
