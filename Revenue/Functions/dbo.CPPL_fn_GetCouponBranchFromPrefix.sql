SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_fn_GetCouponBranchFromPrefix]
(
	@Prefix		char(3)
)
RETURNS varchar(15)
AS
BEGIN

	DECLARE @ret varchar(15);
	SELECT @ret = Null;
    if RTRIM(LTrim(@Prefix)) IN ( '135','136','137','182','183','186','187','188','190','191')
       Select  @ret = 'NATIONWIDE'
    ELSE
       SELECT @ret = CASE LEFT(Rtrim(Ltrim(@Prefix)),1) WHEN '2'  THEN 'ADELAIDE'
                                                        WHEN '3'  THEN 'BRISBANE' 
                                                        WHEN '4'  THEN 'GOLD COAST'
                                                        WHEN '5'  THEN 'MELBOURNE'
                                                        WHEN '6'  THEN 'SYDNEY'
                                                        WHEN '7'  THEN 'NEWCASTLE'
                                                        WHEN '9'  THEN 'CANBERRA'
                                                        ELSE ''  END
       
       

	

	RETURN @ret;
	
END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetCouponBranchFromPrefix]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetCouponBranchFromPrefix]
	TO [SSISUser]
GO
