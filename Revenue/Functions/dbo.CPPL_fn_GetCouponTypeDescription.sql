SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[CPPL_fn_GetCouponTypeDescription]
(
	@LabelPrefix					varchar(20)

)
RETURNS Varchar(100)
AS
BEGIN

	
	RETURN (Select pRONTO.Dbo.[CPPL_fn_GetCouponTypeDescription](@LabelPrefix))
END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetCouponTypeDescription]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetCouponTypeDescription]
	TO [SSISUser]
GO
