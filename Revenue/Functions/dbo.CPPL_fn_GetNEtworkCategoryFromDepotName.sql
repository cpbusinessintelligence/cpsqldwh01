SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_fn_GetNEtworkCategoryFromDepotName]
(
	@DepotName		char(5)
)
RETURNS varchar(25)
AS
BEGIN

	DECLARE @ret varchar(25);
	SELECT @ret = Null;

	RETURN( CASE  Rtrim(Ltrim(@DepotName))  WHEN  'MAIN THEBARTON DEPOT' THEN 'METRO'
	                              WHEN  'MAIN COORPAROO DEPOT' THEN 'METRO'
	                              WHEN  'PINKENBA' THEN 'METRO'
	                              WHEN  'HPD BNE' THEN 'METRO'
	                              WHEN  'BRENDALE SUB-DEPOT' THEN 'METRO'
	                              WHEN  'LOGAN SUB-DEPOT' THEN 'METRO'	  
	                              WHEN  'GOLD COAST' THEN 'METRO'
	                              WHEN  'OAKLEIGH' THEN 'METRO'
	                              WHEN  'CARRUM DOWNS' THEN 'METRO'
	                              WHEN  'HALLAM' THEN 'METRO'
	                              WHEN  'BLACKBURN NTH' THEN 'METRO'
	                              WHEN  'LAVERTON' THEN 'METRO'
	                              WHEN  'PORT MELBOURNE' THEN 'METRO'
	                              WHEN  'LATROBE VALLEY' THEN 'METRO'
	                              WHEN  'NORTH WEST HUB' THEN 'METRO'
	                              WHEN  'SOUTH EAST HUB' THEN 'METRO'	
	                              WHEN  'MORDIALLOC' THEN 'METRO'
	                              WHEN  'SCORESBY' THEN 'METRO'
	                              WHEN  'HEIDELBERG' THEN 'METRO'
	                              WHEN  'TULLUMARINE' THEN 'METRO'		                              
	                              WHEN  'HOMEBUSH' THEN 'METRO'	
	                              WHEN  'SUNSHINE COAST DEPOT' THEN 'REGIONAL'		                              
	                              WHEN  'BFT XPRESS' THEN 'REGIONAL'	                              
	                              WHEN  'IPSWICH SUB-DEPOT' THEN 'REGIONAL'	                              
	                              WHEN  'TOOWOOMBA' THEN 'REGIONAL'	                              
	                              WHEN  'SUNSHINE COAST' THEN 'REGIONAL'		                              
	                              WHEN  'BALLARAT' THEN 'REGIONAL'	                              
	                              WHEN  'BENDIGO' THEN 'REGIONAL'	                              
	                              WHEN  'GEELONG' THEN 'REGIONAL'
	                              WHEN  'WOLLONGONG DEPOT' THEN 'REGIONAL'		                              
	                              WHEN  'CARDIFF' THEN 'REGIONAL'	                              
	                              WHEN  'CENTRAL COAST' THEN 'REGIONAL'	                              
	                              WHEN  'CANBERRA' THEN 'REGIONAL'
	                              WHEN  'DARWIN' THEN 'INTERSTATE'		                              
	                              ELSE 'p' END   
	                              )                                                         	                                                          	                              	                              
	 
	
END

















GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetNEtworkCategoryFromDepotName]
	TO [SSISUser]
GO
