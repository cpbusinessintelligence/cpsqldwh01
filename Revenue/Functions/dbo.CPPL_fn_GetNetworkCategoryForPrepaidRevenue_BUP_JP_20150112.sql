SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_fn_GetNetworkCategoryForPrepaidRevenue_BUP_JP_20150112]
(
	@OriginBranch		varchar(30),
	@OriginDepot		varchar(60),
	@OriginState		varchar(10),
	@DestinationBranch	varchar(30),
	@DestinationDepot	varchar(60),
	@DestinationState	varchar(10),
	@LabelPrefix		char(3),
	@CouponType			varchar(15)
)
RETURNS char(1)
AS
BEGIN

	DECLARE @ret char(1);
	SELECT @ret = '';

	SELECT @OriginBranch = LTRIM(RTRIM(ISNULL(@OriginBranch, '')));
	SELECT @OriginDepot = LTRIM(RTRIM(ISNULL(@OriginDepot, '')));
	SELECT @OriginState = LTRIM(RTRIM(ISNULL(@OriginState, '')));
	SELECT @DestinationBranch = LTRIM(RTRIM(ISNULL(@DestinationBranch, '')));
	SELECT @DestinationDepot = LTRIM(RTRIM(ISNULL(@DestinationDepot, '')));
	SELECT @DestinationState = LTRIM(RTRIM(ISNULL(@DestinationState, '')));
	
	IF ((@OriginBranch != '') AND (@OriginDepot != '')
			AND (@DestinationBranch != '') AND (@DestinationDepot != ''))
	BEGIN
		-- we use the origin and destination depot/branch to work out the network category
		IF ((@OriginBranch = @DestinationBranch)
				OR
				(
					@OriginBranch IN ('BRISBANE','GOLDCOAST')
					AND
					@DestinationBranch IN ('BRISBANE','GOLDCOAST')
				)
		)
		BEGIN
			-- this is within the same state
			IF (@DestinationState != '')
			BEGIN
				-- there's a consignment, so we're assuming this is country
				SELECT @ret = 'N'; -- "intrastate"
				RETURN @ret;
			END
			ELSE
			BEGIN
				IF (@OriginDepot = @DestinationDepot)
				BEGIN
					-- "metro"
					SELECT @ret = 'M';
					RETURN @ret;
				END
				ELSE
				BEGIN
					-- "regional"
					--
					/* here we need to check the depots to determine if it really
					   is regional, or still classified as metro */
					IF (@OriginBranch = 'BRISBANE' AND @DestinationBranch = 'BRISBANE')
					BEGIN
						IF ((@OriginDepot IN ('COORPAROO','BRENDALE','LOGAN','IPSWICH')
								AND @DestinationDepot IN ('COORPAROO','BRENDALE','LOGAN','IPSWICH'))
							OR (@OriginDepot = @DestinationDepot))
						BEGIN
							-- between these depots is "metro"
							SELECT @ret = 'M';
						END
						ELSE
						BEGIN
							SELECT @ret = 'R';
						END
					END
					ELSE IF (@OriginBranch = 'GOLDCOAST' AND @DestinationBranch = 'GOLDCOAST')
					BEGIN
						-- gold coast to gold coast is always "metro"
						SELECT @ret = 'M';
					END
					ELSE IF (@OriginBranch IN ('BRISBANE','GOLDCOAST') AND @DestinationBranch IN ('BRISBANE','GOLDCOAST'))
					BEGIN
						-- between brisbane and gold coast is always "regional"
						SELECT @ret = 'R'
					END
					ELSE IF (@OriginBranch = 'SYDNEY' AND @DestinationBranch = 'SYDNEY')
					BEGIN
						IF (@OriginDepot = @DestinationDepot)
						BEGIN
							SELECT @ret = 'M';
						END
						ELSE
						BEGIN
							-- anywhere to anywhere in Sydney is "regional"
							SELECT @ret = 'R';
						END
					END
					ELSE IF (@OriginBranch = 'ADELAIDE' AND @DestinationBranch = 'ADELAIDE')
					BEGIN
						-- adelaide is always "metro" (only MILE END depot)
						SELECT @ret = 'M';
					END
					ELSE IF (@OriginBranch = 'MELBOURNE' AND @DestinationBranch = 'MELBOURNE')
					BEGIN
						IF ((@OriginDepot IN ('BLACKBURN NTH','CARRUM DOWNS',
												'HALLAM','HEIDELBERG',
												'LAVERTON','MORDIALLOC','NORTH WEST HUB',
												'OAKLEIGH','PORT MELBOURNE','SCORESBY',
												'SOUTH EAST HUB','TULLUMARINE')
								AND @DestinationDepot IN ('BLACKBURN NTH','CARRUM DOWNS',
												'HALLAM','HEIDELBERG',
												'LAVERTON','MORDIALLOC','NORTH WEST HUB',
												'OAKLEIGH','PORT MELBOURNE','SCORESBY',
												'SOUTH EAST HUB','TULLUMARINE'))
								OR (@OriginDepot = @DestinationDepot))
						BEGIN
							SELECT @ret = 'M';
						END
						ELSE
						BEGIN
							SELECT @ret = 'R';
						END
					END
					
					RETURN @ret;
					
					--SELECT @ret = 'R';
					--RETURN @ret;
				END
			END
		END
		ELSE
		BEGIN
			-- branch is different, so it just goes into "interstate"
			SELECT @ret = 'I';
			RETURN @ret;
		END
	END

	-- haven't got a network category yet
	IF (@OriginBranch != '')
	BEGIN
		IF (@DestinationBranch != '')
		BEGIN
			-- check whether it's interstate
			IF
			(
				(@OriginBranch IN ('BRISBANE','GOLDCOAST')
				AND @DestinationBranch NOT IN ('BRISBANE','GOLDCOAST'))
				OR
				((@OriginBranch NOT IN ('BRISBANE','GOLDCOAST')
				AND @DestinationBranch != @OriginBranch))
			)
			BEGIN
				SELECT @ret = 'I';
				RETURN @ret;
			END

			-- we know we don't have either origin or destination depot,
			-- so we can't specifically identify whether it's metro or
			-- regional, so it's just going to be a case of whether it's
			-- country - if not, it will just drop into the "unknown" bucket
			IF @DestinationState = ''
			BEGIN
				SELECT @ret = 'X'; -- "unknown"
				RETURN @ret;
			END
		END
		
		IF (@DestinationState != '')
		BEGIN
			-- no network category yet, so we just check the consignment destination
			SELECT @ret = CASE
				WHEN @OriginBranch = 'SYDNEY' AND @DestinationState IN ('NSW','ACT')
					THEN 'N' --intrastate
				WHEN @OriginBranch IN ('BRISBANE','GOLDCOAST') AND @DestinationState = 'QLD'
					THEN 'N'
				WHEN @OriginBranch = 'ADELAIDE' AND @DestinationState = 'SA'
					THEN 'N'
				WHEN @OriginBranch = 'MELBOURNE' AND @DestinationState = 'VIC'
					THEN 'N'
				WHEN @OriginBranch = 'PERTH' AND @DestinationState = 'WA'
					THEN 'M' --drop in "metro" bucket for Perth
				ELSE 'I' --interstate
			END;
			
			RETURN @ret;
		END
	END		-- (@OriginBranch != '')
	ELSE
	BEGIN
		-- no origin branch, so we need to use the consignment origin
		IF ((@OriginState != '') AND
				(@DestinationBranch != '' OR @DestinationState != ''))
		BEGIN
			SELECT @ret = CASE
				WHEN @OriginState IN ('NSW','ACT')
					AND ((@DestinationBranch = 'SYDNEY' AND @DestinationState IN ('NSW','ACT'))
						OR (@DestinationBranch = 'SYDNEY' AND @DestinationState = '')
						OR (@DestinationBranch = '' AND @DestinationState IN ('NSW','ACT')))
					THEN 'N' --intrastate
				WHEN @OriginState = 'QLD'
					AND ((@DestinationBranch IN ('BRISBANE','GOLDCOAST') AND @DestinationState = 'QLD')
						OR (@DestinationBranch IN ('BRISBANE','GOLDCOAST') AND @DestinationState = '')
						OR (@DestinationBranch = '' AND @DestinationState = 'QLD'))
					THEN 'N'
				WHEN @OriginState = 'VIC'
					AND ((@DestinationBranch = 'MELBOURNE' AND @DestinationState = 'VIC')
						OR (@DestinationBranch = 'MELBOURNE' AND @DestinationState = '')
						OR (@DestinationBranch = '' AND @DestinationState = 'VIC'))
					THEN 'N'
				WHEN @OriginState = 'SA'
					AND ((@DestinationBranch = 'ADELAIDE' AND @DestinationState = 'SA')
						OR (@DestinationBranch = 'ADELAIDE' AND @DestinationState = '')
						OR (@DestinationBranch = '' AND @DestinationState = 'SA'))
					THEN 'N'
				WHEN @OriginState = 'WA'
					AND ((@DestinationBranch = 'PERTH' AND @DestinationState = 'WA')
						OR (@DestinationBranch = 'PERTH' AND @DestinationState = '')
						OR (@DestinationBranch = '' AND @DestinationState = 'WA'))
					THEN 'N'
				ELSE 'I' --interstate
			END
			
			RETURN @ret;
		END
	END		-- (@OriginBranch != '') ELSE

	--
	-- if there's no network category yet, then we'll just just force IRPs into "interstate"
	-- and force links into "intrastate"
	--
	IF (ISNULL(@ret, '') = '')
	BEGIN
		IF (ISNULL(@LabelPrefix, '') IN ('182','187','183'))
		BEGIN
			-- IRPs
			SELECT @ret = 'I';
			RETURN @ret;
		END
		ELSE
		IF (ISNULL(@CouponType, '') LIKE 'link%')
		BEGIN
			-- links
			SELECT @ret = 'N';
			RETURN @ret;
		END
	END

	-- if we've got this far, then the data was incomplete to determine the
	-- network category.  The default is just to drop into the "unknown" bucket, then
	SELECT @ret = 'X';
	RETURN @ret;

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetNetworkCategoryForPrepaidRevenue_BUP_JP_20150112]
	TO [SSISUser]
GO
