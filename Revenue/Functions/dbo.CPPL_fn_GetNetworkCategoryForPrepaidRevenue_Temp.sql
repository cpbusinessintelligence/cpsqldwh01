SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[CPPL_fn_GetNetworkCategoryForPrepaidRevenue_Temp]
(
	@OriginBranch		varchar(30),
	@OriginDepot		varchar(60),
	@DestinationBranch	varchar(30),
	@DestinationDepot	varchar(60)
)
RETURNS char(1)
AS
BEGIN

	DECLARE @ret char(1);
	SELECT @ret = '';

	SELECT @OriginBranch = LTRIM(RTRIM(ISNULL(@OriginBranch, '')));
	SELECT @OriginDepot = LTRIM(RTRIM(ISNULL(@OriginDepot, '')));
	SELECT @DestinationBranch = LTRIM(RTRIM(ISNULL(@DestinationBranch, '')));
	SELECT @DestinationDepot = LTRIM(RTRIM(ISNULL(@DestinationDepot, '')));
IF (@OriginBranch = 'SYDNEY' AND @DestinationBranch = 'SYDNEY')
					BEGIN
												IF ((@OriginDepot IN ('SYDNEY','SYDNEY ZONE1','SYDNEY ZONE2','SYDNEY ZONE3','SYDNEY ZONE4')
								AND @DestinationDepot IN ('SYDNEY','SYDNEY ZONE1','SYDNEY ZONE2','SYDNEY ZONE3','SYDNEY ZONE4'))
							OR (@OriginDepot = @DestinationDepot))
						BEGIN
							SELECT @ret = 'M';
						END
						ELSE
						BEGIN
							-- anywhere to anywhere in Sydney is "regional"
							SELECT @ret = 'R';
						END
					END
	RETURN @ret;

END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetNetworkCategoryForPrepaidRevenue_Temp]
	TO [SSISUser]
GO
