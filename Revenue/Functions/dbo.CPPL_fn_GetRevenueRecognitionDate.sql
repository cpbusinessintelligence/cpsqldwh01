SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CPPL_fn_GetRevenueRecognitionDate]
(
	@FirstScanDate	DateTime,
	@OtherScanDate  DateTime,
	@CreatedDate    DateTime,
	@EndDate   Date

)
RETURNS Date
AS
BEGIN
  Declare @Output as Date = Null
  Select @Output =  CONVERT(date,@FirstScanDate) WHere @FirstScanDate is Not Null  and @OtherScanDate is not null
                                                   
  IF @Output is Null 
      Select @Output = CONVERT(date, @CreatedDate) where @FirstScanDate Is  Null  
                                                      and @CreatedDate is not null
                                                      and (ISNULL(@EndDate, '1jan2099') < CONVERT(date, (DATEADD(day, -4, GETDATE()))))
  IF @Output is Null 
      Select @Output  = CONVERT(date, @OtherScanDate) where @FirstScanDate Is  Null  
                                                      and @CreatedDate is  null
                                                      and @OtherScanDate is not null
                                                      and (ISNULL(@EndDate, '1jan2099') < CONVERT(date, (DATEADD(day, -4, GETDATE()))))
  ---------------------------------
  --If the Report Processing Month is Not same as Report Executing Month (In other words
  -- if we run the report which has got data from previous month, then always set the revenue recognition date as current date
  -- Then always Return the report Execution Date)
  -----------------------
                                          
                                                                           
   IF @Output is Not Null 
       IF (DATEPART(Year,@Output)*100 + DATEPART(Month,@Output)) < (DATEPART(YEar,GETDATE())*100 + DATEPART(Month,GETDATE())) 
          BEGIN
          IF DATEPART(day,@EndDate) = 1
            Select @Output  = CONVERT(date, DateAdd(day,-1,@EndDate))
          ELSE
            Select @Output  = CONVERT(date, GETDATE()) 
          END

     Return @Output	



END


GO
GRANT EXECUTE
	ON [dbo].[CPPL_fn_GetRevenueRecognitionDate]
	TO [SSISUser]
GO
