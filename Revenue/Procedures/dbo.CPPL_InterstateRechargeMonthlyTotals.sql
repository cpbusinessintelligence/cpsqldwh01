SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[CPPL_InterstateRechargeMonthlyTotals](@Year varchar(20),@Month varchar(20),@Type varchar(20)) as
begin


    --'=====================================================================
    --' CP -Stored Procedure -[CPPL_InterstateRechargeMonthlyTotals]
    --' ---------------------------
    --' Purpose: For calculating RechargeCosts and loading into Recharge table
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 26 Jul 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 26/07/2014    AB      1.00    - Created the procedure                           --AB20140605

    --'=====================================================================


If @Type='EDI'
begin
select
[Monthkey]
,Type 
,category
,(case when category='NORMAL' then (case when ra.a_cppl='Y' then 'Redshirt' else 'Agent' end)
else (case when ra.a_cppl='Y' and ra.a_shortname not in ('CPLWOL','CPLNTL') then 'Redshirt' else 'Agent' end) end) as RechargeCategory
,[BusinessUnit]
,ra.Agentstate as [DelivBranch]
--,[DelivScanBranch]
,[DeliveryAgentName]
,[ConCount]
,ra.a_cppl
,ra.a_shortname
,convert(decimal(15,3),0) as AgentCost
,convert(decimal(15,3),0) as Redshirtcost 
,convert(decimal(15,3),0) as Handlingcost 
into #temp
from [dbo].[IntRecharge_EDISummary] left outer join [cpplEDI].[dbo].[agents] a on a.a_name=[DeliveryAgentName] 
left  outer join [dbo].[IntRecharge_Agents] ra on ra.a_name=[DeliveryAgentName]
where [Monthkey]=@Year+right('00'+@Month ,2) 

update #temp set [BusinessUnit]=(case when [BusinessUnit]= 'ADELAIDE' THEN 'SA'
		WHEN [BusinessUnit]='BRISBANE' THEN 'QLD'
		WHEN [BusinessUnit]= 'GOLDCOAST'  or [BusinessUnit]= 'GOLD COAST' THEN 'QLD'
		WHEN [BusinessUnit]='SYDNEY' THEN 'NSW'
		WHEN [BusinessUnit]='MELBOURNE' THEN 'VIC'
		WHEN [BusinessUnit]='PERTH' THEN 'WA'
		ELSE 'Unknown'
		END) 

 select
t.[Monthkey]
,t.Type
,RechargeCategory
,t.[BusinessUnit]
,t.[DelivBranch]
,t.[DeliveryAgentName]
,sum(t.[ConCount]) as ConCount
,(case when Rechargecategory='Agent' then AgentPrice else 0 end ) as AgentCost
,(case when Rechargecategory='RedShirt' then RedshirtPrice else 0 end ) as Redshirtcost 
,handlingPrice as Handlingcost 

into #temp3 from #temp t left outer join [dbo].[IntRecharge_BudgetPricing] p on p.monthkey=t.[monthkey] and p.type=t.[type] and p.BusinessUnit=t.[DelivBranch]


 group by t.[MonthKey]
,t.Type
,t.RechargeCategory
,t.[BusinessUnit]
,t.[DelivBranch]
,t.[DeliveryAgentName]
,(case when Rechargecategory='Agent' then AgentPrice else 0 end ) 
,(case when Rechargecategory='RedShirt' then RedshirtPrice else 0 end )
,handlingPrice 

--IF EXISTS(Select * from [IntRechargeMonthlyTotals] where monthkey= @Year+right('00'+@Month ,2)  )
--	delete from [IntRechargeMonthlyTotals] where monthkey= @Year+right('00'+@Month ,2)   ;

Insert into [IntRechargeMonthlyTotals]
([MonthKey]
,Type
,RechargeCategory
,[BusinessUnit]
,[DelivScanBranch]
,[DeliveryAgentName]
,ItemCount 
,AgentCost
,RedShirtcost 
,Handlingcost
,CreatedDate
,CreatedBy)


select 
[MonthKey]
,Type
,RechargeCategory
,[BusinessUnit]
,[DelivBranch]
,[DeliveryAgentName]
,concount
,(AgentCost*Concount) as AgentCost
,(Redshirtcost *Concount) as Redshirtcost
,(Handlingcost *Concount) as Handlingcost
,getdate()
,'Abhigna'
from #temp3


--select * from #temp3

end

else if @Type='Prepaid'
begin

select 
[MonthKey]
, Type
,category
,convert(varchar(20),'') as RechargeCategory
,[BusinessUnit]
,[DelivScanDriverBranch]
,[DeliveryAgentName]
,[ItemCount]
,convert(decimal(15,3),0) as AgentCost
,convert(decimal(15,3),0) as Redshirtcost 
,convert(decimal(15,3),0) as Handlingcost 
into #temp1 from [dbo].[IntRecharge_PrepaidSummary] 
where [Monthkey]=@Year+right('00'+@Month ,2) and type='Prepaid'



--select
--[Monthkey]
--,Type 
--,category
--,(case when category='NORMAL' then (case when a_cppl='Y' then 'Redshirt' else 'Agent' end)
--else (case when a_cppl='Y' and a_shortname not in ('CPLWOL','CPLNTL') then 'Redshirt' else 'Agent' end) end) as RechargeCategory
--,[BusinessUnit]
--,[DelivScanBranch]
--,[DeliveryAgentName]
--,[ConCount]
--,a_cppl
--,a_shortname
--,convert(decimal(15,3),0) as AgentCost
--,convert(decimal(15,3),0) as Redshirtcost 
--,convert(decimal(15,3),0) as Handlingcost 
--into #temp1
--from [dbo].[IntRecharge_EDISummary_1] left outer join  [dbo].[agents] a on a.a_name=[DeliveryAgentName] 
--where [Monthkey]=@Year+right('00'+@Month ,2)
--AND Type='Prepaid' 

--SELECT * into #temp1 from
--#temp11


update #temp1 set BusinessUnit=(case when [BusinessUnit]= 'ADELAIDE' THEN 'SA'
		WHEN [BusinessUnit]='BRISBANE' THEN 'QLD'
		WHEN [BusinessUnit]= 'GOLDCOAST'  or [BusinessUnit]= 'GOLD COAST' THEN 'QLD'
		WHEN [BusinessUnit]='SYDNEY' THEN 'NSW'
		WHEN [BusinessUnit]='MELBOURNE' THEN 'VIC'
		WHEN [BusinessUnit]='PERTH' THEN 'WA'
				ELSE 'Unknown'
		END) 



		update #temp1 set [DelivScanDriverBranch]=(case when [DelivScanDriverBranch]= 'ADELAIDE' THEN 'SA'
		WHEN [DelivScanDriverBranch]='BRISBANE' THEN 'QLD'
		WHEN [DelivScanDriverBranch]= 'GOLDCOAST'  or [DelivScanDriverBranch]= 'GOLD COAST' THEN 'QLD'
		WHEN [DelivScanDriverBranch]='SYDNEY' THEN 'NSW'
		WHEN [DelivScanDriverBranch]='MELBOURNE' THEN 'VIC'
		WHEN [DelivScanDriverBranch]='PERTH' THEN 'WA'
		        ELSE 'Unknown'
		END) 


update #temp1 set rechargecategory=(CASE WHEN category='NORMAL' THEN (case WHEN [DelivScanDriverBranch]='SA' THEN 'RedShirt'
		WHEN [DelivScanDriverBranch]='QLD' THEN 'RedShirt'
		WHEN [DelivScanDriverBranch]='NSW' THEN 'RedShirt'
		WHEN [DelivScanDriverBranch]='VIC' THEN 'RedShirt'
		WHEN [DelivScanDriverBranch]='WA' THEN 'Agent'
		ELSE 'Unknown'
		END )

		ELSE (case WHEN [DelivScanDriverBranch]='SA' THEN 'RedShirt'
		      WHEN [DelivScanDriverBranch]='QLD' THEN 'RedShirt'
		      WHEN [DelivScanDriverBranch]='NSW' THEN 'Agent'
		      WHEN [DelivScanDriverBranch]='VIC' THEN 'RedShirt'
		      WHEN [DelivScanDriverBranch]='WA' THEN 'Agent'
		      ELSE 'Unknown'
		      END )

		end)


select 
t1.[MonthKey]
,t1.Type
,t1.RechargeCategory
,t1.[BusinessUnit]
,t1.[DelivScanDriverBranch]
,t1.[DeliveryAgentName]
,sum(t1.[ItemCount]) as [ItemCount]
,(case when Rechargecategory='Agent' then AgentPrice else 0 end )  as AgentCost
,(case when Rechargecategory='RedShirt' then RedShirtPrice else 0 end ) as Redshirtcost 
,handlingPrice as Handlingcost 
into #temp2
from  #temp1 t1 left outer join [dbo].[IntRecharge_BudgetPricing] p on p.monthkey=t1.[monthkey] and p.type=t1.[type] and p.BusinessUnit=t1.[DelivScanDriverBranch]

group by t1.[MonthKey]
,t1.Type
,t1.RechargeCategory
,t1.[BusinessUnit]
,t1.[DelivScanDriverBranch]
,t1.[DeliveryAgentName]
,(case when Rechargecategory='Agent' then AgentPrice else 0 end )  
,(case when Rechargecategory='RedShirt' then RedShirtPrice else 0 end ) 
,handlingPrice 



--IF EXISTS(Select * from [IntRechargeMonthlyTotals] where monthkey= @Year+right('00'+@Month ,2) )
--	delete from [IntRechargeMonthlyTotals] where monthkey= @Year+right('00'+@Month ,2)  ;

Insert into [IntRechargeMonthlyTotals]
([MonthKey]
,Type
,RechargeCategory
,[BusinessUnit]
,[DelivScanBranch]
,[DeliveryAgentName]
,ItemCount 
,AgentCost
,RedShirtcost 
,Handlingcost
,CreatedDate
,CreatedBy)


select 
[MonthKey]
,Type
,RechargeCategory
,[BusinessUnit]
,[DelivScanDriverBranch]
,[DeliveryAgentName]
, [ItemCount]
,(AgentCost* [ItemCount]) as AgentCost
,(Redshirtcost * [ItemCount]) as Redshirtcost
,(Handlingcost * [ItemCount]) as Handlingcost
,getdate()
,'Abhigna'
from #temp2

end

else if @Type='IRP'
begin

select 
[MonthKey]
, Type
,category
,convert(varchar(20),'') as RechargeCategory
,[BusinessUnit]
,[DelivScanDriverBranch]
,[DeliveryAgentName]
,[ItemCount]
,convert(decimal(15,3),0) as AgentCost
,convert(decimal(15,3),0) as Redshirtcost 
,convert(decimal(15,3),0) as Handlingcost 
into #temp11 from [dbo].[IntRecharge_PrepaidSummary] 
where [Monthkey]=@Year+right('00'+@Month ,2) and type='IRP'




update #temp11 set BusinessUnit=(case when [BusinessUnit]= 'ADELAIDE' THEN 'SA'
		WHEN [BusinessUnit]='BRISBANE' THEN 'QLD'
		WHEN [BusinessUnit]= 'GOLDCOAST'  or [BusinessUnit]= 'GOLD COAST' THEN 'QLD'
		WHEN [BusinessUnit]='SYDNEY' THEN 'NSW'
		WHEN [BusinessUnit]='MELBOURNE' THEN 'VIC'
		WHEN [BusinessUnit]='PERTH' THEN 'WA'
				ELSE 'Unknown'
		END) 

		update #temp11 set [DelivScanDriverBranch]=(case when [DelivScanDriverBranch]= 'ADELAIDE' THEN 'SA'
		WHEN [DelivScanDriverBranch]='BRISBANE' THEN 'QLD'
		WHEN [DelivScanDriverBranch]= 'GOLDCOAST'  or [DelivScanDriverBranch]= 'GOLD COAST' THEN 'QLD'
		WHEN [DelivScanDriverBranch]='SYDNEY' THEN 'NSW'
		WHEN [DelivScanDriverBranch]='MELBOURNE' THEN 'VIC'
		WHEN [DelivScanDriverBranch]='PERTH' THEN 'WA'
		        ELSE 'Unknown'
		END) 


update #temp11 set rechargecategory='RedShirt'

		


select 
t1.[MonthKey]
,t1.Type
,t1.RechargeCategory
,t1.[BusinessUnit]
,t1.[DelivScanDriverBranch]
,t1.[DeliveryAgentName]
,sum(t1.[ItemCount]) as [ItemCount]
,(case when Rechargecategory='Agent' then AgentPrice else 0 end )  as AgentCost
,(case when Rechargecategory='RedShirt' then RedShirtPrice else 0 end ) as Redshirtcost 
,handlingPrice as Handlingcost 
into #temp12
from  #temp11 t1 left outer join [dbo].[IntRecharge_BudgetPricing] p on p.monthkey=t1.[monthkey] and p.type=t1.[type] and p.BusinessUnit=t1.BusinessUnit

group by t1.[MonthKey]
,t1.Type
,t1.RechargeCategory
,t1.[BusinessUnit]
,t1.[DelivScanDriverBranch]
,t1.[DeliveryAgentName]
,(case when Rechargecategory='Agent' then AgentPrice else 0 end )  
,(case when Rechargecategory='RedShirt' then RedShirtPrice else 0 end ) 
,handlingPrice 



--IF EXISTS(Select * from [IntRechargeMonthlyTotals] where monthkey= @Year+right('00'+@Month ,2) )
--	delete from [IntRechargeMonthlyTotals] where monthkey= @Year+right('00'+@Month ,2)  ;

Insert into [IntRechargeMonthlyTotals]
([MonthKey]
,Type
,RechargeCategory
,[BusinessUnit]
,[DelivScanBranch]
,[DeliveryAgentName]
,ItemCount 
,AgentCost
,RedShirtcost 
,Handlingcost
,CreatedDate
,CreatedBy)


select 
[MonthKey]
,'Prepaid' as Type
,RechargeCategory
,[BusinessUnit]
,[DelivScanDriverBranch]
,[DeliveryAgentName]
, [ItemCount]
,(AgentCost* [ItemCount]) as AgentCost
,(Redshirtcost * [ItemCount]) as Redshirtcost
,(Handlingcost * [ItemCount]) as Handlingcost
,getdate()
,'Abhigna'
from #temp12

end
-----------------------------------------------
end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_InterstateRechargeMonthlyTotals]
	TO [ReportUser]
GO
