SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_InterstateRevenueAllocations]
AS
BEGIN
     SELECT  pb.ConsignmentReference, 
			 ISNULL(pb.BillingDate, pb.AccountingDate) as BillingDate,
			 CASE WHEN ISNULL(pb.ServiceCode, '') IN ('18','48','49','49L','49K','PL6')
				  THEN  CASE WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('OOL','LSY') THEN 'GOLD COAST'
							 WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('MCY')  	THEN 'SUNSHINE COAST'
							 WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CBR')		THEN 'CANBERRA'
						     WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('GOS','NCL') THEN 'CENTRAL COAST'
							 WHEN ISNULL(dg.[State], '') IN ('NSW')	THEN 'SYDNEY'
							 WHEN ISNULL(dg.[State], '') IN ('ACT')	THEN 'CANBERRA'
							 WHEN ISNULL(dg.[State], '') IN ('QLD')	THEN 'BRISBANE'
						     WHEN ISNULL(dg.[State], '') IN ('VIC','TAS') THEN 'MELBOURNE'
							 WHEN ISNULL(dg.[State], '') IN ('SA','NT')   THEN 'ADELAIDE'
							 WHEN ISNULL(dg.[State], '') IN ('WA')        THEN 'PERTH'
							 ELSE 'UNKNOWN'
				         END	
				  ELSE	CASE ISNULL(pb.RevenueBusinessUnit, '')
								WHEN 'CAD' THEN 'ADELAIDE'
								WHEN 'CCB' THEN 'CANBERRA'
								WHEN 'CBN' THEN 'BRISBANE'
								WHEN 'CCC' THEN 'CENTRAL COAST'
								WHEN 'CME' THEN 'MELBOURNE'
								WHEN 'COO' THEN 'GOLD COAST'
								WHEN 'CPE' THEN 'PERTH'
								WHEN 'CSC' THEN 'SUNSHINE COAST'
								WHEN 'CSY' THEN 'SYDNEY'
								ELSE 'UNKNOWN'
							END
					END
					AS [BusinessUnit],

					CASE WHEN (LTRIM(RTRIM(pb.RevenueOriginZone)) = LTRIM(RTRIM(pb.RevenueDestinationZone))
							AND LTRIM(RTRIM(pb.RevenueOriginZone)) IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
													'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL'))
						THEN 'METRO'
					ELSE
						CASE WHEN (pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
														'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], 'XX') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW'))))
							THEN 'REGIONAL'
						ELSE
							CASE WHEN ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], '') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW')))
								THEN 'INTRASTATE'
							ELSE
								'INTERSTATE'
							END
						END
					END
					AS [NetworkCategory],
					ISNULL(SUM((ISNULL(pb.BilledFreightCharge, 0.00) + ISNULL(pb.BilledOtherCharge, 0.00))), 0.00) AS [FreightCharge],
					ISNULL(SUM(ISNULL(pb.BilledFuelSurcharge, 0.00)), 0.00) AS [FuelSurcharge],
					ISNULL(COUNT(*), 0.00) AS [ConsignmentCount],
					ISNULL(SUM(ISNULL(pb.ItemQuantity, 0.00)), 0.00) AS [ItemCount],
					ISNULL(og.[State], 'UNKNOWN') AS [OriginState],
					ISNULL(dg.[State], 'UNKNOWN') AS [DestinationState]
                INTO #TempEDI
				FROM Pronto.dbo.ProntoBilling pb (NOLOCK) 		LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim og (NOLOCK)	ON pb.OriginLocality = og.Locality
																						AND pb.OriginPostcode = og.Postcode AND og.CompanyCode = 'CL1'
															LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim dg (NOLOCK)   ON pb.DestinationLocality = dg.Locality
																						AND pb.DestinationPostcode = dg.Postcode AND dg.CompanyCode = 'CL1'
				WHERE pb.CompanyCode = 'CL1' and pb.BillingDate > '2013-12-01'
				  
				GROUP BY pb.ConsignmentReference ,
					ISNULL(pb.BillingDate, pb.AccountingDate),

					CASE WHEN ISNULL(pb.ServiceCode, '') IN ('18','48','49','49L','49K','PL6')
						THEN 
							CASE WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('OOL','LSY')
								THEN 'GOLD COAST'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('MCY')
								THEN 'SUNSHINE COAST'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CBR')
								THEN 'CANBERRA'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('GOS','NCL')
								THEN 'CENTRAL COAST'
							WHEN ISNULL(dg.[State], '') IN ('NSW')
								THEN 'SYDNEY'
							WHEN ISNULL(dg.[State], '') IN ('ACT')
								THEN 'CANBERRA'
							WHEN ISNULL(dg.[State], '') IN ('QLD')
								THEN 'BRISBANE'
							WHEN ISNULL(dg.[State], '') IN ('VIC','TAS')
								THEN 'MELBOURNE'
							WHEN ISNULL(dg.[State], '') IN ('SA','NT')
								THEN 'ADELAIDE'
							WHEN ISNULL(dg.[State], '') IN ('WA')
								THEN 'PERTH'
							  ELSE 'UNKNOWN'
							END	
						ELSE
							CASE ISNULL(pb.RevenueBusinessUnit, '')
								WHEN 'CAD' THEN 'ADELAIDE'
								WHEN 'CCB' THEN 'CANBERRA'
								WHEN 'CBN' THEN 'BRISBANE'
								WHEN 'CCC' THEN 'CENTRAL COAST'
								WHEN 'CME' THEN 'MELBOURNE'
								WHEN 'COO' THEN 'GOLD COAST'
								WHEN 'CPE' THEN 'PERTH'
								WHEN 'CSC' THEN 'SUNSHINE COAST'
								WHEN 'CSY' THEN 'SYDNEY'
								ELSE 'UNKNOWN'
							END
					END,
					CASE WHEN (LTRIM(RTRIM(pb.RevenueOriginZone)) = LTRIM(RTRIM(pb.RevenueDestinationZone))
							AND LTRIM(RTRIM(pb.RevenueOriginZone)) IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
													'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL'))
						THEN 'METRO'
					ELSE
						CASE WHEN (pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
														'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL')
									AND ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], 'XX') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW'))))
							THEN 'REGIONAL'
						ELSE
							CASE WHEN ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], '') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW')))
								THEN 'INTRASTATE'
							ELSE
								'INTERSTATE'
							END
						END
					END,

					ISNULL(og.[State], 'UNKNOWN'), ISNULL(dg.[State], 'UNKNOWN')
					
				
				Select CONVERT(varchar(20),'EDI') as [Type], 
				      Convert(Varchar(50),'') as AgentID,
				      Convert(Varchar(50),'') as AgentName,
				      Convert(Varchar(50),'') as DriverNumber,
				      ConsignmentReference,
				      BusinessUnit,
				      FreightCharge,
				      FuelSurcharge,
				      ConsignmentCount ,
				      ItemCount,
				      [OriginState],
				      [DestinationState]
				into #TempFinal  
			    from #TempEDI 
			    WHere NetworkCategory = 'INTERSTATE'
			    Union all
			    Select 'PREPAID','','',OtherScanDriverNumber,
			           LabelNumber,
			           BU,
			           RevenueAmount,
			           InsuranceAmount,
			           1,1,FirstScanDriverBranch,
			           OtherScanDriverBranch
				from dbo.vPrepaidRevenueReporting 
				Where IsProcessed = 1 and RevenueRecognisedDate >='2013-12-01' and NetworkCategory = 'I'
		
		Drop Table #TempEDI
		
		Update #TempFinal SET AgentID = C.cd_agent_id    From #TempFinal T Join cppledi.dbo.consignment C on T.ConsignmentReference = C.cd_connote  WHere T.Type = 'EDI'
		Update #TempFinal SET AgentName = A.a_name     From #TempFinal T Join cppledi.dbo.agents A on T.AgentID = A.a_id  WHere T.Type = 'EDI'
	   -- Update #TempFinal SET AgentName = A.a_name     From #TempFinal T Join cppledi.dbo.agents A on T.DriverNumber  = A.a_driver  WHere T.Type = 'PREPAID'
	
		Select * from #TempFInal
					
		--Select * from cppledi.dbo.consignment where cd_connote in (	'61601150268','62901770803')	
					
END
GO
