SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[CPPL_Interstate_EDIRecharge](@monthkey int) as
begin
 

     --'=====================================================================
    --' CP -Stored Procedure -[CPPL_Interstate_EDIRecharge]
    --' ---------------------------
    --' Purpose: For calculating InterstateRechargeCosts getting EDI data and loading into EDI tables
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 26 Jul 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 26/07/2014    AB      1.00    - Created the procedure                           --AB20140605

    --'=====================================================================

	--drop table #temp2

 ---getting IRP's from EDI and excluding from consignment table--------------
 
 --select  * into #temp11 from [ods].[dbo].[Label] where labelnumber like '183%' and datepart(yyyy,createddate)*100+datepart(mm,createddate)=@MonthKey order by createddate

----select serialnumber,ReturnSerialNumber
----into #temp12 from #temp11  join [rds].[dbo].[ProntoCouponDetails] on returnserialnumber=labelnumber


-------------------------------------------------------------------------------------------------
Select distinct datepart(yyyy,cd_date)*100+datepart(mm,cd_date) as [MonthKey] 
	,Convert(varchar(20),'') as [Category]
	,Convert(varchar(20),'') as [Type]
	,cd_id
    ,[cd_connote]
    ,[cd_date]
    ,cd_items
	,Convert(int,0) as [CouponCount]
	,cd_account
    ,cd_pricecode 
	--,cd_deadweight
	--,cd_volume
   ,isnull(convert(decimal(18,3),cd_deadweight ),0) as cd_deadweight
   ,isnull(convert(decimal(18,3),cd_volume),0) as cd_volume
	,cd_pickup_addr0 
    ,[cd_pickup_branch]
    ,[cd_deliver_branch]
    ,cd_pickup_driver
    ,cd_deliver_driver
    ,[cd_delivery_agent]
	,cd_delivery_postcode
	,cd_delivery_suburb
	,convert(varchar(20),'') as DeliveryZone
    ,Convert(varchar(20),'') as [PickupScanDepot]
    ,Convert(varchar(50),'') as [PickupScanBranch]  
    ,Convert(varchar(20),'') as [DelivScanDepot]
    ,Convert(varchar(50),'') as [DelivScanBranch]  
    ,Convert(varchar(70),'') as [DeliveryAgentName]
	--,getdate() as [CreatedDate]
	--,Convert(varchar(100),'') as [CreatedBy]
   into #Temp2
   from cpplEdi.dbo.consignment Nolock
 -- join #temp12 on cd_connote=startserialnumber
WHERE  (datepart(yyyy,cd_date)*100)+ datepart(mm,cd_date) =@monthkey  and cd_test= 'N' 

---and cd_connote not in(select serialnumber from #temp12)
 
 -- and Not (LEN([cd_connote])= 11 and ISNUMERIC ([cd_connote]) =1)
--   where CONVERT(date,[cd_date]) > =cast(left(@monthkey,4) as varchar(10))+'-'+cast(right(@monthkey,2) as varchar(10))+'-'+'01'
 --    and  CONVERT(date,[cd_date]) <  cast(left(@monthkey,4) as varchar(10))+'-'+cast(right(@monthkey,2)+1 as varchar(10))+'-'+'01'-- and cd_test= 'N' and LEN([cd_connote]) <> 11 and ISNUMERIC([cd_connote])<>1


-- select * from cpplEdi.dbo.consignment Nolock
-- -- join #temp12 on cd_connote=startserialnumber
--WHERE  (datepart(yyyy,cd_date)*100)+ datepart(mm,cd_date) = 201608 and cd_test= 'N' and cd_deadweight='3060508074433' 

  ------------select * from #temp2

  Update #Temp2 SET DeliveryZone=p.PrizeZone
 from Couponcalculator.dbo.Postcodes p where p.suburb=cd_delivery_suburb and p.PostCode=cd_delivery_postcode

  delete from #temp2 where   cd_account  IN ('112871512','112906177','112955711','112994363','112994371','112994389','112994397') AND cd_pricecode IN ('X90','I88','I44') AND DeliveryZone = 'SYD' AND LTRIM(RTRIM(cd_delivery_suburb)) <> 'WETHERILL PARK'

  Update #Temp2 SET [PickupScanBranch] = B.b_name   From #temp2 join cpplEDi.dbo.branchs B on  #Temp2.cd_pickup_branch = B.B_id 
   Update #Temp2 SET [DelivScanBranch] = B.b_name From #temp2 join cpplEDi.dbo.branchs B on  #Temp2.cd_deliver_branch = B.B_id
   
        
    Update #temp2 SET cd_pickup_driver  =P.FirstScanDriverNumber, 
					   [PickupScanBranch] = P.FirstScanDriverBranch 
			 From #temp2 Join Revenue.dbo.vPrepaidRevenueReporting P on #temp2.cd_connote = P.LabelNumber		 
	   
   Update #Temp2 SET [PickupScanDepot] = cd.depotcode
                 From #Temp2 Join Cosmos.dbo.Driver D on #Temp2.cd_pickup_driver = D.DriverNumber and #Temp2.[PickupScanBranch] = D.Branch
                             inner join [Cosmos].[dbo].[Depot] cd on cd.depotnumber=d.depotNo and cd.branch=#Temp2.[PickupScanBranch]
                 Where D.IsActive =1 and D.IsDeleted =0 and cd.IsActive=1
			 
   Update #Temp2 SET [DelivScanDepot] = cd1.depotcode 
                 From #Temp2 Join Cosmos.dbo.Driver D on #Temp2.cd_deliver_driver = D.DriverNumber and #Temp2.[DelivScanBranch] = D.Branch
                             inner join [Cosmos].[dbo].[Depot] cd1 on cd1.depotnumber=d.depotNo
                 Where D.IsActive =1 and D.IsDeleted =0 and cd1.IsActive=1
     
     
     
   Update #Temp2 Set [DeliveryAgentName] =A.a_name   From #Temp2 Join cpplEdi.dbo.agents A On #Temp2.[cd_delivery_agent] = A.A_ID
   
    update #temp2 set Type=(case when (len(cd_Connote)=11 and isnumeric(cd_connote)=1) then 'Prepaid' else 'EDI' end)

  
   Update #Temp2 SET COuponCount = (Select COUNT(*) from cpplEDI.dbo.cdcoupon C Where   #Temp2.cd_id = C.cc_consignment and Type='EDI') 

   update #temp2 set COuponCount=1 where Type='Prepaid'

   Update #Temp2 SET [Category] =(case when cd_deliver_driver not  in ('2000','2001','229','7181','2540','300','407','409','411','301') then 'NORMAL' Else 'SPECIAL' end)

    
	
 
  ------Select * from #Temp2 



-----------DETAIL TABLE---------------------------------------------------

	IF EXISTS(Select * from [IntRecharge_EDIDetail] where monthkey=@MonthKey)
	delete from [IntRecharge_EDIDetail] where monthkey=@MonthKey;

     insert into [dbo].[IntRecharge_EDIDetail] 
      ([MonthKey], 
	   Type,
       [Category],
       [ConNote],
       [cd_date],
       [cd_items],
       [CouponCount],
       [cd_pricecode],
       [cd_deadweight],
       [cd_volume],
       [PickupScanDepot],
       [PickupScanBranch],
       [Deliver_driver],
       [DelivScanDepot],
       [DelivScanBranch],
       [DeliveryAgentName],
       [CreatedDate],
       [CreatedBy])

	 Select  [MonthKey] 
	,Type
	,[Category]
    ,[cd_connote]
    ,[cd_date]
    ,cd_items
	,[CouponCount]
    ,cd_pricecode 
    ,cd_deadweight 
    ,cd_volume 
    ,[PickupScanDepot]
    ,[PickupScanBranch] 
	,cd_deliver_driver 
    ,[DelivScanDepot]
    ,[DelivScanBranch] 
    ,[DeliveryAgentName]
	,Getdate()
	, 'Abhigna'
	 from #Temp2 

	---------SUMMARY TABLE--------------------------------------------------
	IF EXISTS(Select * from [IntRecharge_EDISummary] where monthkey=@MonthKey)
    delete from [IntRecharge_EDISummary] where monthkey=@MonthKey;

    insert into [dbo].[IntRecharge_EDISummary]( Monthkey,
	Type,
	Category ,
	[BusinessUnit] ,
	[cd_pricecode] ,
	[PickupScanDepot] ,
	[DelivScanBranch] ,
    [DelivScanDepot] ,
	[DeliveryAgentName] ,
    [ConCount] ,
    [Items] ,
    [Volume] ,
    [DeadWeight] ,
	[CreatedDate],
    [CreatedBy] )

	   Select [MonthKey],Type,[Category],[PickupScanBranch] as BusinessUnit,cd_pricecode,[PickupScanDepot],[DelivScanBranch],[DelivScanDepot],[DeliveryAgentName],
	  sum(COuponCount) as ConCount ,SUM(cd_Items) as Items,SUM(cd_volume) as Volume, SUM(cd_deadweight) as DeadWeight,MAx(Getdate()),MAx('Abhigna')

    From  #Temp2
    Group by [MonthKey],Type,[Category],[PickupScanBranch],cd_pricecode,[PickupScanDepot],[DelivScanBranch],[DelivScanDepot],[DeliveryAgentName]
	
    order by 1,2,3,4,5,6,7,8

	end

GO
GRANT EXECUTE
	ON [dbo].[CPPL_Interstate_EDIRecharge]
	TO [ReportUser]
GO
