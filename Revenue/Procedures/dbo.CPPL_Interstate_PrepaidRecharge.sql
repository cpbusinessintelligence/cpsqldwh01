SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CPPL_Interstate_PrepaidRecharge](@monthkey int) as
begin


     --'=====================================================================
    --' CP -Stored Procedure -[CPPL_Interstate_PrepaidRecharge]
    --' ---------------------------
    --' Purpose: For calculating InterstateRechargeCosts getting Prepaid data and loading into Prepaid tables
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 26 Jul 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 26/07/2014    AB      1.00    - Created the procedure                           --AB20140605

    --'=====================================================================
----------------Getting prepaid coupons processed in consignment table,and  excluding from prepaid tables as we are processing this batch in edi---------------------------

Select [cd_connote]
     into #Temp2
   from cpplEdi.dbo.consignment Nolock
   Where datepart(yyyy,cd_date)*100+datepart(mm,cd_date)=@MonthKey
    and LEN([cd_connote]) = 11 and ISNUMERIC([cd_connote])=1

------------getting irp's here and loading into main prepaid table----------------------
select  * into #temp11 from [scannergateway].[dbo].[Label] where labelnumber like '183%' and datepart(yyyy,createddate)*100+datepart(mm,createddate)=@MonthKey order by createddate


select serialnumber,ReturnSerialNumber
into #temp12 from #temp11  join [Pronto].[dbo].[ProntoCouponDetails] on returnserialnumber=labelnumber


SELECT
 [MonthKey]=@Monthkey
    ,'IRP' as [Type]
	,Convert(varchar(20),'') as [Category]
	,[LabelNumber]
    ,[CouponType]
    ,[RevenueRecognisedDate]
	,Returnserialnumber
    ,[FirstScanDateTime]
    ,[FirstScanDriverBranch]
    ,[FirstScanDriverNumber]
    ,[FirstScanContractorCode]
	--,1 as itemcount
      --,Convert(varchar(20),'') as [PickupScanDepotID]
    ,Convert(varchar(20),'') as [PickupScanDepot]
    ,Convert(varchar(20),'') as [FirstScanNetworkCategory]
    ,[FirstScanType]
    ,[OtherScanDateTime]
    ,[OtherScanDriverBranch]
    ,[OtherScanDriverNumber]
    ,[OtherScanContractorCode]
	,Convert(varchar(20),'') as [RechargeCategory]
    ,Convert(varchar(20),'') as [DelivScanDepot]
    ,Convert(varchar(20),'') as [SecondScanNetworkCategory]
    ,Convert(varchar(20),'') as [DeliveryAgentID]
    ,Convert(varchar(70),'') as [DeliveryAgentName]
    ,[OtherScanType]
    ,[BUCode]
  INTO #Temp9
  FROM  #temp12 join [Revenue].[dbo].[vPrepaidRevenueReporting] on serialnumber=labelnumber
  --Where --RevenueRecognisedDate >= '2013-11-01' 
 -- and RevenueRecognisedDate <= '2013-11-30'
  

  Update #Temp9 SET [FirstScanDriverNumber]  = V.[FirstScanDriverNumber]  From #Temp9  T Join Revenue.dbo.[vPrepaidRevenueReporting]  V on T.ReturnSerialNumber = V.LabelNumber Where T.[FirstScanDriverNumber] is null

  Update #Temp9 SET [FirstScanDriverBranch]  = V.[FirstScanDriverBranch] From #Temp9  T Join Revenue.dbo.[vPrepaidRevenueReporting]  V on T.ReturnSerialNumber = V.LabelNumber Where T.[FirstScanDriverBranch] is null


           
 -----------Getting prepaid coupons not processed in consignment table and loading in detail and summary tables---------------------------------------------------------------          
SELECT
datepart(yyyy,OtherScanDateTime)*100+datepart(mm,OtherScanDateTime) as [MonthKey]
    ,Convert(varchar(30),'') as [Type]
	,Convert(varchar(20),'') as [Category]
	,[LabelNumber]
    ,[CouponType]
    ,[RevenueRecognisedDate]
    ,[FirstScanDateTime]
    ,[FirstScanDriverBranch]
    ,[FirstScanDriverNumber]
    ,[FirstScanContractorCode]
	--,1 as itemcount
      --,Convert(varchar(20),'') as [PickupScanDepotID]
    ,Convert(varchar(20),'') as [PickupScanDepot]
    ,Convert(varchar(20),'') as [FirstScanNetworkCategory]
    ,[FirstScanType]
    ,[OtherScanDateTime]
    ,[OtherScanDriverBranch]
    ,[OtherScanDriverNumber]
    ,[OtherScanContractorCode]
	,Convert(varchar(20),'') as [RechargeCategory]
    ,Convert(varchar(20),'') as [DelivScanDepot]
    ,Convert(varchar(20),'') as [SecondScanNetworkCategory]
    ,Convert(varchar(20),'') as [DeliveryAgentID]
    ,Convert(varchar(70),'') as [DeliveryAgentName]
    ,[OtherScanType]
    ,[BUCode]
  INTO #Temp6 
  FROM [Revenue].[dbo].[vPrepaidRevenueReporting] 
Where --RevenueRecognisedDate >= '2013-11-01' 
 -- and RevenueRecognisedDate <= '2013-11-30'
  datepart(yyyy,OtherScanDateTime)*100+datepart(mm,OtherScanDateTime)=@MonthKey
  and NetworkCategory  = 'I' 
  and IsProcessed = 1
  and CouponType not in ('IRP','IRP TRK','RETURN TRK','ATL','LINK','COD')
  and LabelNumber not in ( select cd_connote from #Temp2) 
  and labelnumber not in (select serialnumber from #temp12)


  SELECT datepart(yyyy,RevenueRecognisedDate)*100+datepart(mm,RevenueRecognisedDate) as [MonthKey]
    ,Convert(varchar(30),'') as [Type]
	,Convert(varchar(20),'') as [Category]
	,[LabelNumber]
    ,[CouponType]
    ,[RevenueRecognisedDate]
    ,[FirstScanDateTime]
    ,[FirstScanDriverBranch]
    ,[FirstScanDriverNumber]
    ,[FirstScanContractorCode]
	--,1 as itemcount
      --,Convert(varchar(20),'') as [PickupScanDepotID]
    ,Convert(varchar(20),'') as [PickupScanDepot]
    ,Convert(varchar(20),'') as [FirstScanNetworkCategory]
    ,[FirstScanType]
    ,[OtherScanDateTime]
    ,[OtherScanDriverBranch]
    ,[OtherScanDriverNumber]
    ,[OtherScanContractorCode]
	,Convert(varchar(20),'') as [RechargeCategory]
    ,Convert(varchar(20),'') as [DelivScanDepot]
    ,Convert(varchar(20),'') as [SecondScanNetworkCategory]
    ,Convert(varchar(20),'') as [DeliveryAgentID]
    ,Convert(varchar(70),'') as [DeliveryAgentName]
    ,[OtherScanType]
    ,[BUCode]
	into #temp7
  FROM [Revenue].[dbo].[PrepaidRevenueReportingExceptions]
  Where   datepart(yyyy,RevenueRecognisedDate)*100+datepart(mm,RevenueRecognisedDate) = @MonthKey and OtherScanDateTime <  convert(varchar(20),left(@monthkey,4))+'-'+right('00'+convert(varchar(20),right(@monthkey,2)),2)+'-'+'01'
  and NetworkCategory = 'I'

  select * into #temp10 from #temp6
  union all 
  select * from #temp7


  select * into #temp1 from #temp10
  union all
  select  [MonthKey]
    ,[Type]
	, [Category]
	,[LabelNumber]
    ,[CouponType]
    ,[RevenueRecognisedDate]
    ,[FirstScanDateTime]
    ,[FirstScanDriverBranch]
    ,[FirstScanDriverNumber]
    ,[FirstScanContractorCode]
    ,[PickupScanDepot]
    ,[FirstScanNetworkCategory]
    ,[FirstScanType]
    ,[OtherScanDateTime]
    ,[OtherScanDriverBranch]
    ,[OtherScanDriverNumber]
    ,[OtherScanContractorCode]
	,[RechargeCategory]
    , [DelivScanDepot]
    , [SecondScanNetworkCategory]
    , [DeliveryAgentID]
    , [DeliveryAgentName]
    ,[OtherScanType]
    ,[BUCode] from #temp9





  
  Update #Temp1 SET [PickupScanDepot] = cd.depotcode
     From #Temp1 Join Cosmos.dbo.Driver D on #Temp1.FirstScanDriverNumber = D.DriverNumber and #Temp1.FirstScanDriverBranch = D.Branch
	   join [Cosmos].[dbo].[Depot] cd on cd.depotnumber=d.depotNo and cd.branch=#Temp1.FirstScanDriverBranch
     Where D.IsActive =1 and D.IsDeleted =0 and cd.IsActive=1 and cd.IsDeleted =0
   
  
  Update #Temp1 SET [DelivScanDepot] = cd1.depotcode
     From #Temp1 Join Cosmos.dbo.Driver D on #Temp1.OtherScanDriverNumber = D.DriverNumber and #Temp1.OtherScanDriverBranch = D.Branch
	  join [Cosmos].[dbo].[Depot] cd1 on cd1.depotnumber=d.depotNo and cd1.branch=#Temp1.OtherScanDriverBranch
     Where D.IsActive =1 and D.IsDeleted =0 and cd1.IsActive=1 and cd1.IsDeleted =0
  
   
   Update #Temp1 Set  [DeliveryAgentID] =  C.[cd_delivery_agent]
   From #temp1 Join CpplEDI.dbo.consignment C on #Temp1.LabelNumber= C.cd_connote 

      
   Update #Temp1 Set [DeliveryAgentName] =A.a_name  
   From #Temp1 Join cpplEdi.dbo.agents A On #Temp1.[DeliveryAgentID] = A.A_ID

  
  
  Update #Temp1 SET [DeliveryAgentName] = 'WA- Australia Post' Where SecondScanNetworkCategory = '' and DeliveryAgentName = '' and OtherScanDriverNumber = '9404'
  Update #Temp1 SET [DeliveryAgentName] = 'WA- CTI' Where SecondScanNetworkCategory = '' and DeliveryAgentName = '' and OtherScanDriverNumber = '9402'
    
  Update #Temp1 Set SecondScanNetworkCategory = 'INTRASTATE' WHere  [DeliveryAgentName] <>''  
  

   Update #Temp1 SET [Category] =(case when OtherScanDriverNumber not  in ('2000','2001','229','7181','2540','300','407','409','411','301') then 'NORMAL' Else 'SPECIAL' end)

   update #Temp1 set [Type]= 'Prepaid' where [type] <>'IRP'



-------------DETAIL TABLE---------------------------------------------------

-------------Delete from detail and summary tables previous records in that month-------------
 IF EXISTS(Select * from [IntRecharge_PrepaidDetails] where monthkey=@MonthKey)
delete from [IntRecharge_PrepaidDetails] where monthkey=@MonthKey;
--------------------


Insert into [dbo].[IntRecharge_PrepaidDetails](
    [MonthKey]
	,[Type]
    ,[Category]
    ,[LabelNumber]
    ,[CouponType] 
	--,[itemcount]
    ,[RevenueRecognisedDate] 
    ,[FirstScanDriverBranch] 
    ,[PickupScanDepot]
    ,[FirstScanType]
    ,[OtherScanDateTime]
    ,[OtherScanDriverBranch]
    ,[OtherScanDriverNumber]
    ,[OtherScanContractorCode]
    ,[DelivScanDepot]
    ,[DeliveryAgentName]
    ,[OtherScanType]
    ,[BUCode]
    ,[CreatedDate]
    ,[CreatedBy] )
    
	Select 
     [MonthKey]
	,[Type]
    ,[Category]
    ,[LabelNumber]
    ,[CouponType]
	--,sum(itemcount) as itemcount
    ,[RevenueRecognisedDate]
    ,[FirstScanDriverBranch]
    ,[PickupScanDepot]
    ,[FirstScanType]
    ,[OtherScanDateTime]
    ,[OtherScanDriverBranch]
    ,[OtherScanDriverNumber]
    ,[OtherScanContractorCode]
    ,[DelivScanDepot]
    ,[DeliveryAgentName]
    ,[OtherScanType]
    ,[BUCode]
    ,Getdate()
	,'Abhigna'
      from #Temp1 
	  
order by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17


-----------SUMMARY TABLE---------------------------------------------------

IF EXISTS(Select * from [IntRecharge_PrepaidSummary] where monthkey=@MonthKey)
delete from [IntRecharge_PrepaidSummary] where monthkey=@MonthKey;
 
Insert into [IntRecharge_PrepaidSummary]
( [MonthKey] , 
  [Type] ,
  [Category] ,
  [BusinessUnit] ,
  [PickupScanDepot] ,
  [DelivScanDriverBranch] ,
  [DelivScanDepot] ,
  [DeliveryAgentName] ,
 --[itemcount],
  [ItemCount] ,
  [CreatedDate] ,
  [CreatedBy] )
	 
Select [MonthKey],[Type],[Category], FirstScanDriverBranch as BusinessUnit,
PickupScanDepot,OtherScanDriverBranch as DelivScanDriverBranch,DelivScanDepot,DeliveryAgentName,COUNT(*) as [ItemCount], MAx(Getdate()),MAx('Abhigna')
From  #Temp1
Group by [MonthKey],[Type],[Category],FirstScanDriverBranch,PickupScanDepot,OtherScanDriverBranch,DelivScanDepot,DeliveryAgentName
order by 1,2,3,4,5,6,7,8
-------------------------------------------------------------------------------------------------------------------	 
end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_Interstate_PrepaidRecharge]
	TO [ReportUser]
GO
