SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_PopulateEdiRevenueOlapTables_SS30042019]
AS
BEGIN



	--===============================================================================
	--
	-- new account / zone dimension inserts for CP-SQL01
	--
	--

    MERGE OlapDimAccount AS ODM
	USING (SELECT Distinct AccountCode,AccountName FROM EdiRevenueReporting (NOLOCK) R ) AS SD
														ON ODM.AccountCode = SD.AccountCode
    WHEN MATCHED AND Ltrim(Rtrim(ODM.AccountName)) <> Ltrim(Rtrim(SD.AccountName)) THEN UPDATE SET ODM.AccountName = SD.AccountName 
    WHEN NOT MATCHED THEN INSERT(AccountCode,AccountName) VALUES (SD.AccountCode,SD.AccountName);

	--insert into OlapDimAccount 
	--(AccountCode, AccountName)
	--select
	--distinct 
	--AccountCode, AccountName 
	--from EdiRevenueReporting r
	--where not exists 
	--(
	--	select 1
	--	from OlapDimAccount 
	--	where AccountCode = r.AccountCode 
	--	AND AccountName = r.AccountName 
	--);



	insert into OlapDimZone 
	(ZoneCode)
	select
	distinct OriginZone AS [ZoneCode]
	from EdiRevenueReporting r
	where not exists
	(
		select 1 from OlapDimZone 
		where ZoneCode = r.OriginZone 
	)
	union
	select
	distinct DestinationZone AS [ZoneCode]
	from EdiRevenueReporting r
	where not exists
	(
		select 1 from OlapDimZone 
		where ZoneCode = r.DestinationZone 
	);



	--==============================================================================
	--
	-- new fact table insert for CP-SQL01
	--
	--



	truncate table OlapFactEdiRevenueReporting;


	--================ NATIONAL ==================
/*
	insert into OlapFactEdiRevenueReporting 
	(
		FinancialYearId, FinancialPeriodId, FinancialWeekId, CalendarDayId, BusinessUnitId, AccountId,
		NetworkCategoryId, ServiceCategoryId, OriginStateId, DestinationStateId, BusinessUnitStateId,
		OriginZoneId, DestinationZoneId, OriginNetworkTypeId, DestinationNetworkTypeId,
		FreightCharge, InsuranceCharge, FuelSurcharge, ConsignmentTotal, ItemTotal,
		DeadWeightTotal, CubicWeightTotal, ProntoChargeableWeightTotal, PickupCost,
		DeliveryCost, BusinessDays, BillableWeightTotal, CouponTypeId, InsuranceCategoryId 
	)
	select
	ISNULL(fy.FinancialYearId, 0),
	ISNULL(fp.FinancialPeriodId, 0),
	ISNULL(fw.FinancialWeekId, 0),
	ISNULL(oday.DayId, 0),
	bu.BusinessUnitId,
	ISNULL(oacct.AccountId, 0),
	nc.NetworkCategoryId,
	sc.ServiceCategoryId,
	ost.StateId AS [OriginStateId],
	dst.StateId AS [DestinationStateId],
	bust.StateId AS [BusinessUnitStateId],
	oz.ZoneId AS [OriginZoneId],
	dz.ZoneId AS [DestinationZoneId],
	ont.NetworkTypeId AS [OriginNetworkTypeId],
	dnt.NetworkTypeId AS [DestinationNetworkTypeId],
	ISNULL(SUM(ISNULL(FreightCharge, 0.00)), 0.00) AS [FreightCharge],
	ISNULL(SUM(ISNULL(InsuranceCharge, 0.00)), 0.00) AS [InsuranceCharge],
	ISNULL(SUM(ISNULL(FuelSurcharge, 0.00)), 0.00) AS [FuelSurcharge],
	ISNULL(SUM(ISNULL(ConsignmentCount, 0)), 0) AS [ConsignmentTotal],
	ISNULL(SUM(ISNULL(ItemCount, 0)), 0) AS [ItemTotal],
	ISNULL(SUM(ISNULL(DeadWeightTotal, 0.00)), 0.00) AS [DeadWeightTotal],
	ISNULL(SUM(ISNULL(CubicWeightTotal, 0.00)), 0.00) AS [CubicWeightTotal],
	ISNULL(SUM(ISNULL(ChargeWeightTotal, 0.00)), 0.00) AS [ProntoChargeableWeightTotal],
	ISNULL(SUM(ISNULL(PickupCost, 0.00)), 0.00) AS [PickupCost],
	ISNULL(SUM(ISNULL(DeliveryCost, 0.00)), 0.00) AS [DeliveryCost],
	ISNULL((
				SELECT MAX(GLBalance)
				FROM RDS.dbo.ProntoGlBalance (NOLOCK)
				WHERE AccountNumber LIKE 
					('C%BUSD')
				AND AccountingYear = CONVERT(smallint, REPLACE(rr.FYear, 'FY', ''))
				AND AccountingPeriod = CONVERT(smallint, RIGHT(rr.AcctMonth, 2))
	), 0),
	ISNULL(SUM(ISNULL(CASE WHEN (ISNULL(DeadWeightTotal, 0.00) > ISNULL(CubicWeightTotal, 0.00))
		THEN ISNULL(DeadWeightTotal, 0.00)
		ELSE ISNULL(CubicWeightTotal, 0.00)
	END, 0.00)), 0.00) AS [BillableWeightTotal],
	ISNULL(ct.CouponTypeId, 99) AS [CouponTypeId], --default to 99, N/A, for EDI
	ISNULL(ins.InsuranceCategoryId, 9) AS [InsuranceCategoryId] --default to 9, N/A, if empty
	from EdiRevenueReporting rr (nolock)
	left outer join OlapDimBusinessUnit bu (nolock)
	--on rr.BUCode = bu.BusinessUnitCode 
	ON bu.BusinessUnitCode = 'NATIONAL'
	left outer join OlapDimFinancialYear fy (nolock)
	on rr.FYearSort = fy.FinancialYear 
	left outer join OlapDimFinancialPeriod fp (nolock)
	on fy.FinancialYearId = fp.FinancialYearId 
	AND fp.FinancialPeriod = CONVERT(smallint, RIGHT(rr.AcctMonth, 2))
	left outer join OlapDimFinancialWeek fw (nolock)
	ON fw.FinancialPeriodId = fp.FinancialPeriodId 
	AND CONVERT(smallint, RIGHT(rr.AcctWeek, 2)) = fw.FinancialWeek 
	--ON fw.WeekName = rr.AcctWeek 
	left outer join OlapDimCalendarDay oday (nolock)
	ON 
		CASE WHEN rr.ConsignmentDate BETWEEN '25jun2011' AND '31mar2014'
			THEN rr.ConsignmentDate
			ELSE '1jul2011'
	END = oday.DayDate 
	left outer join OlapDimAccount oacct (nolock)
	ON rr.AccountCode = oacct.AccountCode
	AND rr.AccountName = oacct.AccountName 
	left outer join OlapDimNetworkCategory nc (Nolock)
	ON rr.NWCategory = nc.NetworkCategory 
	left outer join OlapDimNetworkType ont (nolock)
	ON rr.OriginLaneType = ont.NetworkTypeCode 
	left outer join OlapDimNetworkType dnt (nolock)
	ON rr.DestinationLaneType = dnt.NetworkTypeCode 
	left outer join OlapDimServiceCategory sc (nolock)
	on rr.ServCategory = sc.ServiceCategory 
	left outer join OlapDimState bust (nolock)
	--ON rr.StateCode = bust.StateCode 
	ON bust.StateCode = 'ALL'
	left outer join OlapDimState ost (nolock)
	ON rr.OriginState = ost.StateCode 
	left outer join OlapDimState dst (nolock)
	ON rr.DestinationState = dst.StateCode 
	left outer join OlapDimZone oz (nolock)
	ON rr.OriginZone = oz.ZoneCode 
	left outer join OlapDimZone dz (nolock)
	ON rr.DestinationZone = dz.ZoneCode 
	left outer join OlapDimCouponType ct (nolock)
	ON ct.CouponType = 'N/A' --default to N/A for EDI
	left outer join OlapDimInsuranceCategory ins (nolock)
	ON ins.InsuranceCategory = CASE LTRIM(RTRIM(ISNULL(rr.InsuranceCategory, 'N/A')))
		WHEN '' THEN 'N/A'
		ELSE LTRIM(RTRIM(rr.InsuranceCategory))
	END
	WHERE ((ISNULL(fy.FinancialYearId, 0) IN (1,2, 3,4)) OR (ISNULL(fp.FinancialPeriodId, 0) < 30))
	GROUP BY ISNULL(fy.FinancialYearId, 0), ISNULL(fp.FinancialPeriodId, 0), ISNULL(oday.DayId, 0),
	ISNULL(fw.FinancialWeekId, 0), 
	bu.BusinessUnitId, ISNULL(oacct.AccountId, 0),
	nc.NetworkCategoryId, sc.ServiceCategoryId, bust.StateId, ost.StateId, dst.StateId,
	oz.ZoneId, dz.ZoneId, ont.NetworkTypeId, dnt.NetworkTypeId, ISNULL(ct.CouponTypeId, 99),
	rr.FYear, rr.AcctMonth, ISNULL(ins.InsuranceCategoryId, 9);
	/*CASE LTRIM(RTRIM(ISNULL(rr.InsuranceCategory, 'N/A')))
		WHEN '' THEN 'N/A'
		ELSE LTRIM(RTRIM(rr.InsuranceCategory))
	END ;*/

*/

	-- ================= STATE / BU LEVEL ==================



	insert into OlapFactEdiRevenueReporting 
	(
		FinancialYearId, FinancialPeriodId, FinancialWeekId, CalendarDayId, BusinessUnitId, AccountId,
		NetworkCategoryId, ServiceCategoryId, OriginStateId, DestinationStateId, BusinessUnitStateId,
		OriginZoneId, DestinationZoneId, OriginNetworkTypeId, DestinationNetworkTypeId,
		FreightCharge, InsuranceCharge, FuelSurcharge, ConsignmentTotal, ItemTotal,
		DeadWeightTotal, CubicWeightTotal, ProntoChargeableWeightTotal, PickupCost, 
		DeliveryCost, BusinessDays, BillableWeightTotal, CouponTypeId, InsuranceCategoryId
	)
	select
	ISNULL(fy.FinancialYearId, 0),
	ISNULL(fp.FinancialPeriodId, 0),
	ISNULL(fw.FinancialWeekId, 0),
	ISNULL(oday.DayId, 0),
	bu.BusinessUnitId,
	ISNULL(oacct.AccountId, 0),
	nc.NetworkCategoryId,
	sc.ServiceCategoryId,
	ost.StateId AS [OriginStateId],
	dst.StateId AS [DestinationStateId],
	bust.StateId AS [BusinessUnitStateId],
	oz.ZoneId AS [OriginZoneId],
	dz.ZoneId AS [DestinationZoneId],
	ont.NetworkTypeId AS [OriginNetworkTypeId],
	dnt.NetworkTypeId AS [DestinationNetworkTypeId],
	ISNULL(SUM(ISNULL(FreightCharge, 0.00)), 0.00) AS [FreightCharge],
	ISNULL(SUM(ISNULL(InsuranceCharge, 0.00)), 0.00) AS [InsuranceCharge],
	ISNULL(SUM(ISNULL(FuelSurcharge, 0.00)), 0.00) AS [FuelSurcharge],
	ISNULL(SUM(ISNULL(ConsignmentCount, 0)), 0) AS [ConsignmentTotal],
	ISNULL(SUM(ISNULL(ItemCount, 0)), 0) AS [ItemTotal],
	ISNULL(SUM(ISNULL(DeadWeightTotal, 0.00)), 0.00) AS [DeadWeightTotal],
	ISNULL(SUM(ISNULL(CubicWeightTotal, 0.00)), 0.00) AS [CubicWeightTotal],
	ISNULL(SUM(ISNULL(ChargeWeightTotal, 0.00)), 0.00) AS [ProntoChargeableWeightTotal],
	ISNULL(SUM(ISNULL(PickupCost, 0.00)), 0.00) AS [PickupCost],
	ISNULL(SUM(ISNULL(DeliveryCost, 0.00)), 0.00) AS [DeliveryCost],
	ISNULL((
				SELECT MAX(GLBalance)
				FROM Pronto.dbo.ProntoGlBalance (NOLOCK)
				WHERE AccountNumber LIKE 
					('C' + ISNULL(rr.BUCode, 'ZZZ') + '%BUSD')
				AND AccountingYear = CONVERT(smallint, REPLACE(rr.FYear, 'FY', ''))
				AND AccountingPeriod = CONVERT(smallint, RIGHT(rr.AcctMonth, 2))
	), 0),
	ISNULL(SUM(ISNULL(CASE WHEN (ISNULL(DeadWeightTotal, 0.00) > ISNULL(CubicWeightTotal, 0.00))
		THEN ISNULL(DeadWeightTotal, 0.00)
		ELSE ISNULL(CubicWeightTotal, 0.00)
	END, 0.00)), 0.00) AS [BillableWeightTotal],
	ISNULL(ct.CouponTypeId, 99) AS [CouponTypeId], --default to 99, N/A, for EDI
	ISNULL(ins.InsuranceCategoryId, 9) AS [InsuranceCategoryId] --default to 9, N/A, if empty
	from EdiRevenueReporting rr (nolock)
	left outer join OlapDimBusinessUnit bu (nolock)
	on rr.BUCode = bu.BusinessUnitCode 
	left outer join OlapDimFinancialYear fy (nolock)
	on rr.FYearSort = fy.FinancialYear 
	left outer join OlapDimFinancialPeriod fp (nolock)
	on fy.FinancialYearId = fp.FinancialYearId 
	AND fp.FinancialPeriod = CONVERT(smallint, RIGHT(rr.AcctMonth, 2))
	left outer join OlapDimFinancialWeek fw (nolock)
	ON fw.FinancialPeriodId = fp.FinancialPeriodId 
	AND CONVERT(smallint, RIGHT(rr.AcctWeek, 2)) = fw.FinancialWeek 
	--ON fw.WeekName = rr.AcctWeek 
	left outer join OlapDimCalendarDay oday (nolock)
	ON 
		CASE WHEN rr.ConsignmentDate BETWEEN '25jun2011' AND '31mar2018'
			THEN rr.ConsignmentDate
			ELSE '1jul2011'
	END = oday.DayDate 
	left outer join OlapDimAccount oacct (nolock)
	ON rr.AccountCode = oacct.AccountCode
	AND ltrim(rtrim(rr.AccountName)) = ltrim(rtrim(oacct.AccountName))  --AB20141022,added ltrim and rtrim----
	left outer join OlapDimNetworkCategory nc (Nolock)
	ON rr.NWCategory = nc.NetworkCategory 
	left outer join OlapDimNetworkType ont (nolock)
	ON rr.OriginLaneType = ont.NetworkTypeCode 
	left outer join OlapDimNetworkType dnt (nolock)
	ON rr.DestinationLaneType = dnt.NetworkTypeCode 
	left outer join OlapDimServiceCategory sc (nolock)
	on rr.ServCategory = sc.ServiceCategory 
	left outer join OlapDimState bust (nolock)
	ON rr.StateCode = bust.StateCode 
	left outer join OlapDimState ost (nolock)
	ON rr.OriginState = ost.StateCode 
	left outer join OlapDimState dst (nolock)
	ON rr.DestinationState = dst.StateCode 
	left outer join OlapDimZone oz (nolock)
	ON rr.OriginZone = oz.ZoneCode 
	left outer join OlapDimZone dz (nolock)
	ON rr.DestinationZone = dz.ZoneCode 
	left outer join OlapDimCouponType ct (nolock)
	ON ct.CouponType = 'N/A' --default to N/A for EDI
	left outer join OlapDimInsuranceCategory ins (nolock)
	ON ins.InsuranceCategory = CASE LTRIM(RTRIM(ISNULL(rr.InsuranceCategory, 'N/A')))
		WHEN '' THEN 'N/A'
		ELSE LTRIM(RTRIM(rr.InsuranceCategory))
	END
	WHERE 
	--rr.BUcode <>'' and
	((ISNULL(fy.FinancialYearId, 0) IN (1,2, 3,4,6,7,8,11,12)) OR (ISNULL(fp.FinancialPeriodId, 0) < 30))
	GROUP BY ISNULL(fy.FinancialYearId, 0), ISNULL(fp.FinancialPeriodId, 0), ISNULL(oday.DayId, 0),
	ISNULL(fw.FinancialWeekId, 0), 
	bu.BusinessUnitId, ISNULL(oacct.AccountId, 0),
	nc.NetworkCategoryId, sc.ServiceCategoryId, bust.StateId, ost.StateId, dst.StateId,
	oz.ZoneId, dz.ZoneId, ont.NetworkTypeId, dnt.NetworkTypeId, ISNULL(ct.CouponTypeId, 99),
	rr.BUCode, rr.FYear, rr.AcctMonth, ISNULL(ins.InsuranceCategoryId, 9);
	/*CASE LTRIM(RTRIM(ISNULL(rr.InsuranceCategory, 'N/A')))
		WHEN '' THEN 'N/A'
		ELSE LTRIM(RTRIM(rr.InsuranceCategory))
	END ;*/



UPDATE OlapFactEDIRevenueReporting set Businessdays=isnull((select min(Businessdays) from OlapFactEDIRevenueReporting t where t.financialperiodid=OlapFactEDIRevenueReporting.financialperiodid and businessdays<>0),0)
where businessdays=0 and businessunitid=2

END

GO
