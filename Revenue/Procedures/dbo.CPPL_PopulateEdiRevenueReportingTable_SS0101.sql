SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_PopulateEdiRevenueReportingTable_SS0101]
AS
BEGIN

	-- if there load of data into Pronto' ProntoBilling table is still running, then we wait for it to finish
	--
	IF EXISTS (SELECT 1 FROM Pronto.dbo.InterfaceConfig 
				WHERE Name = 'ProntoBillingImportRunning'
				AND Value = 'Y')
	BEGIN
		-- wait 2 minutes before trying again
		WAITFOR DELAY '0:02:00';
	END


	-- import's not running, so we can run the process
	--
	TRUNCATE TABLE EdiRevenueReporting;


		/****** Object:  Index [PK_EdiRevenueReporting]    Script Date: 01/25/2013 13:43:49 ******/
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EdiRevenueReporting]') AND name = N'PK_EdiRevenueReporting')
		ALTER TABLE [dbo].[EdiRevenueReporting] DROP CONSTRAINT [PK_EdiRevenueReporting];


		-- insert data since October last year
			INSERT INTO EdiRevenueReporting
			(FYear, FYearSort, AcctMonth, AcctMonthSort, AcctWeek, AcctWeekSort, ConsignmentDate, AccountCode,
			AccountName, BUCode, BU, StateCode, NWCategory, ServCategory, FreightCharge, InsuranceCharge, FuelSurcharge,
			ConsignmentCount, DeadWeightTotal, CubicWeightTotal, ChargeWeightTotal, ItemCount,
			OriginState, DestinationState, PickupCost, DeliveryCost, OriginZone, DestinationZone,
			OriginLaneType, DestinationLaneType, InsuranceCategory)
				SELECT
				DISTINCT
					('FY ' + LEFT(pb.AccountingPeriod, 4)) AS [FinancialYear],
					CONVERT(smallint, LEFT(pb.AccountingPeriod, 4)) AS [FinancialYearSort],
					(LEFT(pb.AccountingPeriod, 4) + '-' + RIGHT(pb.AccountingPeriod, 2)) AS [AccountingPeriod],
					CONVERT(int, pb.AccountingPeriod) AS [AccountingMonthSort],
					(LEFT(pb.AccountingWeek, 4) + '-' + RIGHT(pb.AccountingWeek, 2)) AS [AccountingWeek],
					CONVERT(int, pb.AccountingWeek) AS [AccountingWeekSort],
					ISNULL(pb.ConsignmentDate, pb.AccountingDate),
					ISNULL(pb.AccountBillToCode, ISNULL(pb.AccountCode, 'UNKNOWN')),
					ISNULL(pb.AccountBillToName, ISNULL(pb.AccountName, 'UNKNOWN')),
					--
					-- for returns, it's the destination business unit who receives the revenue
					--
					CASE WHEN ISNULL(pb.ServiceCode, '') IN ('18','48','49','49L','49K','PL6')
						THEN 
							CASE WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('OOL','LSY')
								THEN 'COO'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CFS')
							     THEN 'CCF'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('ABX')
							     THEN 'CAB'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CNS')
							     THEN 'CCA'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('DRW')
							     THEN 'CDA'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('MCY')
								THEN 'CSC'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CBR')
								THEN 'CCB'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('GOS','NCL')
								THEN 'CCC'
							WHEN ISNULL(dg.[State], '') IN ('NSW')
								THEN 'CSY'
							WHEN ISNULL(dg.[State], '') IN ('ACT')
								THEN 'CCB'
							WHEN ISNULL(dg.[State], '') IN ('QLD')
								THEN 'CBN'
							WHEN ISNULL(dg.[State], '') IN ('VIC','TAS')
								THEN 'CME'
							WHEN ISNULL(dg.[State], '') IN ('SA')
								THEN 'CAD'
							WHEN ISNULL(dg.[State], '') IN ('NT')
								THEN 'CDA'
							WHEN ISNULL(dg.[State], '') IN ('WA')
								THEN 'CPE'
							ELSE 'UNKNOWN'
							END	
						ELSE
							LTRIM(RTRIM(ISNULL(pb.RevenueBusinessUnit, '')))
					END AS [BusinessUnitCode],
					--
					-- for returns, it's the destination business unit who receives the revenue
					--
					CASE WHEN ISNULL(pb.ServiceCode, '') IN ('18','48','49','49L','49K','PL6')
						THEN 
							CASE WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('OOL','LSY')
								THEN 'GOLD COAST'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('MCY')
								THEN 'SUNSHINE COAST'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CFS')
							     THEN 'COFFS HARBOUR'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('ABX')
							     THEN 'ALBURY'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CNS')
							     THEN 'CAIRNS'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('DRW')
							     THEN 'DARWIN'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CBR')
								THEN 'CANBERRA'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('GOS','NCL')
								THEN 'CENTRAL COAST'
							WHEN ISNULL(dg.[State], '') IN ('NSW')
								THEN 'SYDNEY'
							WHEN ISNULL(dg.[State], '') IN ('ACT')
								THEN 'CANBERRA'
							WHEN ISNULL(dg.[State], '') IN ('QLD')
								THEN 'BRISBANE'
							WHEN ISNULL(dg.[State], '') IN ('VIC','TAS')
								THEN 'MELBOURNE'
							WHEN ISNULL(dg.[State], '') IN ('SA')
								THEN 'ADELAIDE'
							WHEN ISNULL(dg.[State], '') IN ('NT')
								THEN 'DARWIN'
							WHEN ISNULL(dg.[State], '') IN ('WA')
								THEN 'PERTH'
							  ELSE 'UNKNOWN'
							END	
						ELSE
							CASE ISNULL(pb.RevenueBusinessUnit, '')
								WHEN 'CAD' THEN 'ADELAIDE'
								WHEN 'CCB' THEN 'CANBERRA'
								WHEN 'CBN' THEN 'BRISBANE'
								WHEN 'CCC' THEN 'CENTRAL COAST'
								WHEN 'CME' THEN 'MELBOURNE'
								WHEN 'COO' THEN 'GOLD COAST'
								WHEN 'CPE' THEN 'PERTH'
								WHEN 'CSC' THEN 'SUNSHINE COAST'
								WHEN 'CSY' THEN 'SYDNEY'
								WHEN 'CCF' THEN 'COFFS HARBOUR'
								WHEN 'CAB' THEN 'ALBURY'
								WHEN 'CCA' THEN 'CAIRNS'
								WHEN 'CDA' THEN 'DARWIN'
								ELSE 'UNKNOWN'
							END
					END
					AS [BusinessUnit],
					/*CASE pb.RevenueBusinessUnit
						WHEN 'CAD' THEN 'SA'
						--WHEN 'CCB' THEN 'ACT'
						WHEN 'CCB' THEN 'NSW'
						WHEN 'CBN' THEN 'QLD'
						WHEN 'CCC' THEN 'NSW'
						WHEN 'CME' THEN 'VIC'
						WHEN 'COO' THEN 'QLD'
						WHEN 'CPE' THEN 'WA'
						WHEN 'CSC' THEN 'QLD'
						WHEN 'CSY' THEN 'NSW'
						ELSE 'UNKNOWN'
					END*/
					CASE LTRIM(RTRIM(ISNULL(dg.[State], '')))
						WHEN '' THEN 'UNKNOWN'
						ELSE LTRIM(RTRIM(dg.[State]))
					END
					AS [StateCode],
					CASE WHEN (LTRIM(RTRIM(pb.RevenueOriginZone)) = LTRIM(RTRIM(pb.RevenueDestinationZone))
							AND LTRIM(RTRIM(pb.RevenueOriginZone)) IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
													'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX','DRW'))
						THEN 'METRO'
					ELSE
						CASE WHEN (pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
														'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
									AND pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
									AND ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], 'XX') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW'))))
							THEN 'REGIONAL'
						ELSE
							CASE WHEN ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], '') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW')))
								THEN 'INTRASTATE'
							ELSE
								'INTERSTATE'
							END
						END
					END
					AS [NetworkCategory],
					CASE WHEN pb.ServiceCode IN ('18','48','49','49L','49K','PL6')
							THEN 'RETURNS'
						WHEN pb.ServiceCode IN ('AS1','AS3','AS5','X31','X33','X35','Y31','Y33','Y35','ZX1','ZX3','ZX5')
							THEN 'SATCHELS'
						WHEN pb.ServiceCode IN ('12','17','25','55','64','65','850','95','96','Z1')
							THEN 'OTHER'
						ELSE
							'PARCELS'
					END
					AS [ServiceCategory],
					ISNULL(SUM((ISNULL(pb.BilledFreightCharge, 0.00) + ISNULL(pb.BilledOtherCharge, 0.00))), 0.00) AS [FreightCharge],
					ISNULL(SUM(ISNULL(pb.BilledInsurance, 0.00)), 0.00) AS [InsuranceCharge],
					ISNULL(SUM(ISNULL(pb.BilledFuelSurcharge, 0.00)), 0.00) AS [FuelSurcharge],
					ISNULL(COUNT(*), 0.00) AS [ConsignmentCount],
					CASE ISNULL(SUM(ISNULL(pb.DeadWeight, 0.00)), 0.00) WHEN 0.00 THEN ISNULL(SUM(ISNULL(pb.DeclaredWeight, 0.00)), 0.00) ELSE ISNULL(SUM(ISNULL(pb.DeadWeight, 0.00)), 0.00) END   AS [DeadWeightTotal],
					CASE ISNULL(SUM((ISNULL(pb.Volume, 0.00) * 250.00)), 0.00) WHEN 0.00 THEN ISNULL(SUM((ISNULL(pb.DeclaredVolume, 0.00) * 250.00)), 0.00) ELSE ISNULL(SUM((ISNULL(pb.Volume, 0.00) * 250.00)), 0.00) END AS [CubicWeightTotal],
					ISNULL(SUM(ISNULL(pb.ChargeableWeight, 0.00)), 0.00) AS [ChargeWeightTotal],
					ISNULL(SUM(ISNULL(pb.ItemQuantity, 0.00)), 0.00) AS [ItemCount],
					ltrim(rtrim(ISNULL(og.[State], 'UNKNOWN'))) AS [OriginState],
					ltrim(rtrim(ISNULL(dg.[State], 'UNKNOWN'))) AS [DestinationState],
					ISNULL(SUM(ISNULL(pb.PickupCost, 0.00)), 0.00) AS [PickupCost],
					ISNULL(SUM(ISNULL(pb.DeliveryCost, 0.00)), 0.00) AS [DeliveryCost],
					pb.RevenueOriginZone AS [OriginZone],
					pb.RevenueDestinationZone AS [DestinationZone],
					CASE WHEN pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
															'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
						THEN 'R'
						ELSE 'C'
					END AS [OriginType],
					CASE WHEN pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
						THEN 'R'
						ELSE 'C'
					END AS [DestinationType],
					ISNULL(pb.InsuranceCategory, '') AS [InsuranceCategory]
		  	 FROM Pronto.dbo.ProntoBilling pb (NOLOCK) 
				--FROM [ECA-SQL2K8-1.ECAU.INT.AU\REPORTING].Pronto.dbo.CPPL_vw_ProntoBillingTemp pb (NOLOCK)
				LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim og (NOLOCK)
				ON pb.OriginLocality = og.Locality
				AND pb.OriginPostcode = og.Postcode 
				AND og.CompanyCode = 'CL1'
				LEFT OUTER JOIN Pronto.dbo.ProntoGeoDim dg (NOLOCK)
				ON pb.DestinationLocality = dg.Locality
				AND pb.DestinationPostcode = dg.Postcode 
				AND dg.CompanyCode = 'CL1'
				WHERE ( pb.revenuebusinessunit <>'' and pb.billingdate <>'1900-01-01') and  ----AB20141113(To exclude null business unit and zero $ revenue)
				 pb.CompanyCode = 'CL1'
				and accountingweek = 201752
				--AND pb.BillingDate > '1oct2011'
				-- 25/02/2013 - Craig Parris.  Included data from July 2011 now 200527

				and convert(int, pb.AccountingWeek) >= 201527 and  accountcode not in ('112995527','112995519') 
				--and convert(int, pb.AccountingWeek) >= 201127 and  accountcode not in ('112995527','112995519') 
				--and accountcode not like 'WD%' and accountcode not like 'WI%'--201139 (Account customers Revenue is billed by Pronto hence not in exclusion list)--20151028
				GROUP BY ('FY ' + LEFT(pb.AccountingPeriod, 4)),
					CONVERT(smallint, LEFT(pb.AccountingPeriod, 4)),
					(LEFT(pb.AccountingPeriod, 4) + '-' + RIGHT(pb.AccountingPeriod, 2)), CONVERT(int, pb.AccountingPeriod),
					(LEFT(pb.AccountingWeek, 4) + '-' + RIGHT(pb.AccountingWeek, 2)), CONVERT(int, pb.AccountingWeek),
					ISNULL(pb.ConsignmentDate, pb.AccountingDate),
					ISNULL(pb.AccountBillToCode, ISNULL(pb.AccountCode, 'UNKNOWN')),
					ISNULL(pb.AccountBillToName, ISNULL(pb.AccountName, 'UNKNOWN')),
						--
					CASE WHEN ISNULL(pb.ServiceCode, '') IN ('18','48','49','49L','49K','PL6')
						THEN 
							CASE WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('OOL','LSY')
								THEN 'COO'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CFS')
							     THEN 'CCF'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('ABX')
							     THEN 'CAB'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CNS')
							     THEN 'CCA'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('DRW')
							     THEN 'CDA'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('MCY')
								THEN 'CSC'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CBR')
								THEN 'CCB'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('GOS','NCL')
								THEN 'CCC'
							WHEN ISNULL(dg.[State], '') IN ('NSW')
								THEN 'CSY'
							WHEN ISNULL(dg.[State], '') IN ('ACT')
								THEN 'CCB'
							WHEN ISNULL(dg.[State], '') IN ('QLD')
								THEN 'CBN'
							WHEN ISNULL(dg.[State], '') IN ('VIC','TAS')
								THEN 'CME'
							WHEN ISNULL(dg.[State], '') IN ('SA')
								THEN 'CAD'
							WHEN ISNULL(dg.[State], '') IN ('NT')
								THEN 'CDA'
							WHEN ISNULL(dg.[State], '') IN ('WA')
								THEN 'CPE'
							ELSE 'UNKNOWN'
							END	
						ELSE
							LTRIM(RTRIM(ISNULL(pb.RevenueBusinessUnit, '')))
					END,
					CASE WHEN ISNULL(pb.ServiceCode, '') IN ('18','48','49','49L','49K','PL6')
						THEN 
							CASE WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('OOL','LSY')
								THEN 'GOLD COAST'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('MCY')
								THEN 'SUNSHINE COAST'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CFS')
							     THEN 'COFFS HARBOUR'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('ABX')
							     THEN 'ALBURY'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CNS')
							     THEN 'CAIRNS'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('DRW')
							     THEN 'DARWIN'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('CBR')
								THEN 'CANBERRA'
							WHEN ISNULL(pb.RevenueDestinationZone, '') IN ('GOS','NCL')
								THEN 'CENTRAL COAST'
							WHEN ISNULL(dg.[State], '') IN ('NSW')
								THEN 'SYDNEY'
							WHEN ISNULL(dg.[State], '') IN ('ACT')
								THEN 'CANBERRA'
							WHEN ISNULL(dg.[State], '') IN ('QLD')
								THEN 'BRISBANE'
							WHEN ISNULL(dg.[State], '') IN ('VIC','TAS')
								THEN 'MELBOURNE'
							WHEN ISNULL(dg.[State], '') IN ('SA')
								THEN 'ADELAIDE'
							WHEN ISNULL(dg.[State], '') IN ('NT')
								THEN 'DARWIN'
							WHEN ISNULL(dg.[State], '') IN ('WA')
								THEN 'PERTH'
							  ELSE 'UNKNOWN'
							END	
						ELSE
							CASE ISNULL(pb.RevenueBusinessUnit, '')
								WHEN 'CAD' THEN 'ADELAIDE'
								WHEN 'CCB' THEN 'CANBERRA'
								WHEN 'CBN' THEN 'BRISBANE'
								WHEN 'CCC' THEN 'CENTRAL COAST'
								WHEN 'CME' THEN 'MELBOURNE'
								WHEN 'COO' THEN 'GOLD COAST'
								WHEN 'CPE' THEN 'PERTH'
								WHEN 'CSC' THEN 'SUNSHINE COAST'
								WHEN 'CSY' THEN 'SYDNEY'
								WHEN 'CCF' THEN 'COFFS HARBOUR'
								WHEN 'CAB' THEN 'ALBURY'
								WHEN 'CCA' THEN 'CAIRNS'
								WHEN 'CDA' THEN 'DARWIN'
								ELSE 'UNKNOWN'
							END
					END,
					CASE LTRIM(RTRIM(ISNULL(dg.[State], '')))
						WHEN '' THEN 'UNKNOWN'
						ELSE LTRIM(RTRIM(dg.[State]))
					END,
					CASE WHEN (LTRIM(RTRIM(pb.RevenueOriginZone)) = LTRIM(RTRIM(pb.RevenueDestinationZone))
							AND LTRIM(RTRIM(pb.RevenueOriginZone)) IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
													'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX','DRW'))
						THEN 'METRO'
					ELSE
						CASE WHEN (pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
														'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
									AND pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
																'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
									AND ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], 'XX') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW'))))
							THEN 'REGIONAL'
						ELSE
							CASE WHEN ((ISNULL(og.[State], 'XX') = ISNULL(dg.[State], 'ZZ'))
											OR (ISNULL(og.[State], '') IN ('ACT','NSW')
												AND ISNULL(dg.[State], 'ZZ') IN ('ACT','NSW')))
								THEN 'INTRASTATE'
							ELSE
								'INTERSTATE'
							END
						END
					END,
					CASE WHEN pb.ServiceCode IN ('18','48','49','49L','49K','PL6')
							THEN 'RETURNS'
						WHEN pb.ServiceCode IN ('AS1','AS3','AS5','X31','X33','X35','Y31','Y33','Y35','ZX1','ZX3','ZX5')
							THEN 'SATCHELS'
						WHEN pb.ServiceCode IN ('12','17','25','55','64','65','850','95','96','Z1')
							THEN 'OTHER'
						ELSE
							'PARCELS'
					END,
					ISNULL(og.[State], 'UNKNOWN'), ISNULL(dg.[State], 'UNKNOWN'),
					pb.RevenueOriginZone, pb.RevenueDestinationZone,
					CASE WHEN pb.RevenueOriginZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
															'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
						THEN 'R'
						ELSE 'C'
					END,
					CASE WHEN pb.RevenueDestinationZone IN ('CBR','GOS','WOL','NTL','SYD','BNE','MCY','OOL',
															'TWB','ADL','BAL','BEN','GEX','MEL','MTP','MWL','CFS','CNS','ABX')
						THEN 'R'
						ELSE 'C'
					END,
					ISNULL(pb.InsuranceCategory, '');


		/****** Object:  Index [PK_EdiRevenueReporting]    Script Date: 01/25/2013 13:43:49 ******/
		ALTER TABLE [dbo].[EdiRevenueReporting] ADD  CONSTRAINT [PK_EdiRevenueReporting] PRIMARY KEY CLUSTERED 
		(
			[AcctMonth] ASC,
			[AcctWeek] ASC,
			[ConsignmentDate] ASC,
			[AccountCode] ASC,
			[BUCode] ASC,
			[NWCategory] ASC,
			[ServCategory] ASC,
			[InsuranceCategory] ASC,
			[OriginState] ASC,
			[DestinationState] ASC,
			[OriginZone] ASC,
			[DestinationZone] ASC,
			[OriginLaneType] ASC,
			[DestinationLaneType] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];


END

GO
