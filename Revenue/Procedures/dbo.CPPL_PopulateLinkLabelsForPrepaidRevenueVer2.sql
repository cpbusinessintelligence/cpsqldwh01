SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_PopulateLinkLabelsForPrepaidRevenueVer2]
--(
--	@LabelNumber	varchar(50),
--	@CreatedDate	datetime
--)
AS
BEGIN

	CREATE TABLE #LinkLabels2 (LabelNumber varchar(50), LinkLabelNumber varchar(50));
	TRUNCATE TABLE #LinkLabels;

	-- populate any existing link labels that are in the permanent table
	INSERT INTO #LinkLabels
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT
			a.LabelNumber, a.LinkLabelNumber 
	FROM PrepaidRevenueLinkLabels (NOLOCK) a
	INNER JOIN #tLabelNumber b ON a.LabelNumber = b.LabelNumber

	-- populate any labels that this label is linked to
	INSERT INTO #LinkLabels 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT b.LabelNumber,
			LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
	FROM Scannergateway.dbo.TrackingEvent te (NOLOCK)
	INNER JOIN Scannergateway.dbo.Label l (NOLOCK) ON te.LabelId = l.Id 
			AND te.AdditionalText1 LIKE ('Link Coupon ' + l.LabelNumber + '%')
	INNER JOIN #tLabelNumber b ON l.LabelNumber = b.LabelNumber			
	LEFT JOIN #LinkLabels ll ON ll.LabelNumber = b.LabelNumber 
			AND  ll.LinkLabelNumber = LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
	-- allow for events up to 30 days behind the creation of the label record
	WHERE te.EventDateTime >= DATEADD(day, -30, b.CreatedDate)
	AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
	AND ll.LabelNumber IS NULL;


	-- populate any labels that are linked this label
	INSERT INTO #LinkLabels 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT b.LabelNumber,
			LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', ''))) AS col2
	FROM Scannergateway.dbo.TrackingEvent te (NOLOCK)
	INNER JOIN Scannergateway.dbo.Label l (NOLOCK) ON te.LabelId = l.Id 
	INNER JOIN #tLabelNumber b ON l.LabelNumber = b.LabelNumber	
	LEFT JOIN #LinkLabels ll ON ll.LabelNumber = b.LabelNumber 
			AND  ll.LinkLabelNumber = LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
	-- allow for events up to 30 days behind the creation of the label record
	WHERE te.EventDateTime >= DATEADD(day, -30, b.CreatedDate)
	AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
	AND ll.LabelNumber IS NULL;

	-- populate 2nd-level link labels
	INSERT INTO #LinkLabels 
	(LabelNumber, LinkLabelNumber)
	SELECT
			DISTINCT b.LabelNumber,
				LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		FROM Scannergateway.dbo.TrackingEvent te (NOLOCK)
		INNER JOIN Scannergateway.dbo.Label l (NOLOCK) ON te.LabelId = l.Id 
		INNER JOIN #tLabelNumber b ON l.LabelNumber = b.LabelNumber	
		INNER JOIN (SELECT DISTINCT LinkLabelNumber FROM #LinkLabels) lli ON te.AdditionalText1 LIKE ('Link Coupon ' + lli.LinkLabelNumber + '%')
		LEFT JOIN #LinkLabels ll ON ll.LabelNumber = b.LabelNumber AND ll.LinkLabelNumber = LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		-- allow for events up to 30 days behind the creation of the label record
		WHERE te.EventDateTime >= DATEADD(day, -30, b.CreatedDate)
		AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
		AND ll.LabelNumber IS NULL;


	--first delete 
	DELETE ll 
	FROM #LinkLabels ll
	WHERE LTRIM(RTRIM(ISNULL(ll.LinkLabelNumber, ''))) = ''
	
	--2nd delete
	DELETE ll
	FROM #LinkLabels ll
	INNER JOIN #tLabelNumber ln ON LTRIM(RTRIM(ISNULL(ll.LinkLabelNumber, ''))) = ln.LabelNumber
	
	IF EXISTS (SELECT 1 FROM #LinkLabels)
	BEGIN
	
		INSERT INTO PrepaidRevenueLinkLabels 
		(LabelNumber, LinkLabelNumber)
		SELECT
			DISTINCT LabelNumber, LinkLabelNumber 
		FROM #LinkLabels ll
		WHERE NOT EXISTS
		(
			SELECT 1 FROM PrepaidRevenueLinkLabels prl (NOLOCK)
			WHERE prl.LabelNumber = ll.LabelNumber
			AND prl.LinkLabelNumber = ll.LinkLabelNumber
		);

		--SELECT * FROM #LinkLabels;
		--BREAK;

	END

	-- make sure we've got only distinct records in #LinkLabels
	INSERT INTO #LinkLabels2 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT
			LabelNumber,
			LinkLabelNumber
	FROM #LinkLabels;
	
	TRUNCATE TABLE #LinkLabels;
	
	INSERT INTO #LinkLabels
	(LabelNumber, LinkLabelNumber)
	SELECT
		LabelNumber,
		LinkLabelNumber
	FROM #LinkLabels2;

	DROP TABLE #LinkLabels2;


/*


	CREATE TABLE #LinkLabels2 (LabelNumber varchar(50), LinkLabelNumber varchar(50));
	TRUNCATE TABLE #LinkLabels;
	
	-- populate any existing link labels that are in the permanent table
	INSERT INTO #LinkLabels
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT
			LabelNumber, LinkLabelNumber 
	FROM PrepaidRevenueLinkLabels (NOLOCK)
	WHERE LabelNumber = @LabelNumber;

	-- populate any labels that this label is linked to
	INSERT INTO #LinkLabels 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT @LabelNumber,
			--LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
			LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
	FROM ODS.dbo.TrackingEvent te (NOLOCK)
	JOIN ODS.dbo.Label l (NOLOCK)
	ON te.LabelId = l.Id 
	-- allow for events up to 30 days behind the creation of the label record
	WHERE te.EventDateTime >= DATEADD(day, -30, @CreatedDate)
	AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
	AND te.AdditionalText1 LIKE ('Link Coupon ' + @LabelNumber + '%')
	AND NOT EXISTS
	(
		SELECT 1 FROM #LinkLabels
		WHERE LabelNumber = @LabelNumber 
		AND LinkLabelNumber = LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
	);

	-- populate any labels that are linked this label
	INSERT INTO #LinkLabels 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT @LabelNumber,
			LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
	FROM ODS.dbo.TrackingEvent te (NOLOCK)
	JOIN ODS.dbo.Label l (NOLOCK)
	ON te.LabelId = l.Id 
	-- allow for events up to 30 days behind the creation of the label record
	WHERE te.EventDateTime >= DATEADD(day, -30, @CreatedDate)
	AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
	AND l.LabelNumber = @LabelNumber
	AND NOT EXISTS
	(
		SELECT 1 FROM #LinkLabels 
		WHERE LabelNumber = @LabelNumber
		AND LinkLabelNumber = LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
	);


--start code upgrade by noe 2013-03-07
		-- populate 2nd-level link labels
		INSERT INTO #LinkLabels (LabelNumber, LinkLabelNumber)
		SELECT
			DISTINCT @LabelNumber,
				--LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
				LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		FROM ODS.dbo.TrackingEvent te (NOLOCK)
		INNER JOIN ODS.dbo.Label l (NOLOCK) ON te.LabelId = l.Id 
		INNER JOIN (SELECT DISTINCT LinkLabelNumber FROM #LinkLabels) lli ON te.AdditionalText1 LIKE ('Link Coupon ' + lli.LinkLabelNumber + '%')
		LEFT JOIN #LinkLabels ll ON ll.LabelNumber = @LabelNumber AND ll.LinkLabelNumber = LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		-- allow for events up to 30 days behind the creation of the label record
		WHERE te.EventDateTime >= DATEADD(day, -30, @CreatedDate)
		AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
		AND ll.LabelNumber IS NULL
--end code upgrade by noe 2013-03-07

	
	DELETE FROM #LinkLabels 
	WHERE LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) = ''
	OR LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) = @LabelNumber;
	
	IF EXISTS (SELECT 1 FROM #LinkLabels)
	BEGIN
	
		INSERT INTO PrepaidRevenueLinkLabels 
		(LabelNumber, LinkLabelNumber)
		SELECT
			DISTINCT LabelNumber, LinkLabelNumber 
		FROM #LinkLabels ll
		WHERE NOT EXISTS
		(
			SELECT 1 FROM PrepaidRevenueLinkLabels prl
			WHERE prl.LabelNumber = ll.LabelNumber
			AND prl.LinkLabelNumber = ll.LinkLabelNumber
		);

		--SELECT * FROM #LinkLabels;
		--BREAK;

	END

	-- make sure we've got only distinct records in #LinkLabels
	INSERT INTO #LinkLabels2 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT
			LabelNumber,
			LinkLabelNumber
	FROM #LinkLabels;
	
	TRUNCATE TABLE #LinkLabels;
	
	INSERT INTO #LinkLabels
	(LabelNumber, LinkLabelNumber)
	SELECT
		LabelNumber,
		LinkLabelNumber
	FROM #LinkLabels2;

	DROP TABLE #LinkLabels2;
*/

END
GO
