SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_PopulateNewPrepaidRevenueReportingLabels]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @lastDate date;
	DECLARE @endDate date;
	SELECT @lastDate = Null;
	-- end date for selecting records is always yesterday
	SELECT @endDate = CONVERT(date, DATEADD(day, -1, GETDATE()));
	
	SELECT @lastDate = CONVERT(date, Value)
	                    FROM InterfaceConfig 
	                    WHERE Name = 'LastPrepaidRevenueReportingLabelCreatedDate';
	
	IF @lastDate Is Not Null
	BEGIN
		-- increment the last date by 1 day
		SELECT @lastDate = DATEADD(day, 1, @lastDate);
	END
	
	IF ISNULL(@lastDate, '1jan2050') > @endDate 
	BEGIN
		-- something wrong!
		--
		PRINT 'Last run date is greater than yesterday''s date.';
		RETURN 0;
	END



	BEGIN TRY
		BEGIN TRAN;
		    -------------------------------------------------------------------------------------------------------
			-- select all pre-paid labels in the required date range to insert into the PrepaidRevenueReporting table
			-------------------------------------------------------------------------------------------------------
			INSERT INTO PrepaidRevenueReporting 
			  (LabelNumber, CouponType,
			   LabelPrefix,IsFirstScanProcessed, 
			   IsProcessed, AnalysisDataExtracted, 
			   CreatedDate)
			SELECT DISTINCT LTRIM(RTRIM(l.SerialNumber)), dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(LTRIM(RTRIM(l.SerialNumber)), 3)),
				   LEFT(LTRIM(RTRIM(l.SerialNumber)), 3),0, 
				   0, 0,
				   MIN(ActivityDateTime)
			FROM Cosmos.dbo.ActivityImport  l (NOLOCK)
			WHERE  CONVERT(date, l.ActivityDateTime) >= @lastDate
			  AND  CONVERT(date, l.ActivityDateTime) <= @endDate 
			  AND  ISNUMERIC(l.SerialNumber) = 1
			  AND  LEN(LTRIM(RTRIM(l.SerialNumber))) = 11
			  AND NOT EXISTS (SELECT 1
				             FROM vPrepaidRevenueReporting prr
				             WHERE prr.LabelNumber = LTRIM(RTRIM(l.SerialNumber))
			                 )
            ----------------------------------------------			                 
			-- don't include Dynamic Supplies trackers
			----------------------------------------------
			  AND LTRIM(RTRIM(l.SerialNumber)) NOT LIKE '[23456789]73%'
			----------------------------------------------
			-- don't include stray scans from CTI (Perth)
			----------------------------------------------
			  AND LTRIM(RTRIM(l.SerialNumber)) NOT LIKE '417%'
			  AND LTRIM(RTRIM(l.SerialNumber)) NOT LIKE '483%'
			  AND dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(LTRIM(RTRIM(l.SerialNumber)), 3)) IS NOT NULL
			GROUP BY LTRIM(RTRIM(l.SerialNumber));

			-- update config table with last created date
			UPDATE InterfaceConfig 
			SET Value = CONVERT(varchar(30), CONVERT(date, @endDate), 113)
			WHERE Name = 'LastPrepaidRevenueReportingLabelCreatedDate';
		
		COMMIT TRAN;

		BEGIN TRAN;
		
			-- label numbers with math symbols will report as numeric, so we need to exclude them here
			-- 
			DELETE FROM PrepaidRevenueReporting 
			      WHERE  IsProcessed = 0
			         AND ((ISNULL(CHARINDEX('+', LabelNumber, 1), 0) > 0 
							OR
							ISNULL(CHARINDEX('-', LabelNumber, 1), 0) > 0));
		
		COMMIT TRAN;

		print '1 done'

		BEGIN TRAN;
		
			-- update consignment information for PrepaidRevenueReporting records
			EXEC dbo.CPPL_UpdatePrepaidRevenueReportingWithConsignmentDetails;
		
		COMMIT TRAN;

			

		BEGIN TRAN;
		
			-- update revenue and insurance details for PrepaidRevenueReporting records
			EXEC dbo.CPPL_UpdatePrepaidRevenueReportingWithProntoDetails;
		
		COMMIT TRAN;
				
		WHILE @@TRANCOUNT > 0
		BEGIN
			COMMIT WORK;
		END
		
	END TRY
	BEGIN CATCH
	
		WHILE @@TRANCOUNT > 0
		BEGIN
			ROLLBACK WORK;
		END
		
		EXEC dbo.CPPL_LogRethrowError @RethrowError = 1, @LogError = 1;
	
	END CATCH

	SET NOCOUNT OFF;

END
GO
