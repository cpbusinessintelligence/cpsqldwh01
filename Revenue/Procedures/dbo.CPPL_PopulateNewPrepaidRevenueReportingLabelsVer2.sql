SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_PopulateNewPrepaidRevenueReportingLabelsVer2]
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @lastDate date;
	DECLARE @endDate date;
	SELECT @lastDate = Null;
	-- end date for selecting records is always yesterday
	SELECT @endDate = CONVERT(date, DATEADD(day, -1, GETDATE()));
	
	SELECT @lastDate = CONVERT(date, Value)
	FROM InterfaceConfig 
	WHERE Name = 'LastPrepaidRevenueReportingLabelCreatedDate2';
	
	IF @lastDate Is Not Null
	BEGIN
		-- increment the last date by 1 day
		SELECT @lastDate = DATEADD(day, 1, @lastDate);
	END
	
	IF @lastDate > @endDate 
	BEGIN
		-- something wrong!
		--
		PRINT 'Last run date is greater than yesterday''s date.';
		RETURN 0;
	END


	--get prepaid revenuereporting
	SELECT LabelNumber INTO #prr 
	FROM dbo.v_PrepaidRevenueReporting2
	GROUP BY LabelNumber
	CREATE CLUSTERED INDEX #prr_idx ON #prr(LabelNumber)

	--select data to insert
	SELECT DISTINCT LTRIM(RTRIM(l.LabelNumber)) AS LabelNumber,
			dbo.CPPL_fn_GetCouponTypeFromPrefix(LEFT(LTRIM(RTRIM(l.LabelNumber)), 3)) AS CouponType,
			LEFT(LTRIM(RTRIM(l.LabelNumber)), 3) AS LabelPrefix,
			0 AS IsProcessed,
			0 AS AnalysisDataExtracted, 
			MIN(CreatedDate) AS CreatedDate
	INTO #ins			
	FROM dbo.v_Label l
	LEFT JOIN #prr prr ON LTRIM(RTRIM(l.LabelNumber)) = prr.LabelNumber
	WHERE CONVERT(date, l.CreatedDate) BETWEEN @lastDate AND @endDate 
	AND ISNUMERIC(l.LabelNumber) = 1
	AND LEN(LTRIM(RTRIM(l.LabelNumber))) = 11
	AND prr.LabelNumber IS NULL
	-- don't include Dynamic Supplies trackers
	AND LTRIM(RTRIM(l.LabelNumber)) NOT LIKE '[23456789]73%'
	-- don't include stray scans from CTI (Perth)
	AND LTRIM(RTRIM(l.LabelNumber)) NOT LIKE '417%'
	AND LTRIM(RTRIM(l.LabelNumber)) NOT LIKE '483%'
	GROUP BY LTRIM(RTRIM(l.LabelNumber));



	BEGIN TRY
	
		BEGIN TRAN;
		
			-- select all pre-paid labels in the required date range to insert into the PrepaidRevenueReporting table
			--
			INSERT INTO PrepaidRevenueReporting2
			(LabelNumber, CouponType, LabelPrefix, IsProcessed, AnalysisDataExtracted, CreatedDate)
			SELECT * FROM #ins
	
			-- update config table with last created date
			UPDATE InterfaceConfig 
			SET Value = CONVERT(varchar(30), CONVERT(date, @endDate), 113)
			WHERE Name = 'LastPrepaidRevenueReportingLabelCreatedDate2';
		
		COMMIT TRAN;

		BEGIN TRAN;
		
			-- label numbers with math symbols will report as numeric, so we need to exclude them here
			-- 
			DELETE FROM PrepaidRevenueReporting2 
			WHERE
			ISNULL(IsProcessed, 0) = 0
			AND
			(
				(ISNULL(CHARINDEX('+', LabelNumber, 1), 0) > 0
				OR
				ISNULL(CHARINDEX('-', LabelNumber, 1), 0) > 0)
			);
		
		COMMIT TRAN;
		
		BEGIN TRAN;
		
			-- update consignment information for PrepaidRevenueReporting records
			EXEC dbo.CPPL_UpdatePrepaidRevenueReportingWithConsignmentDetailsVer2;
		
		COMMIT TRAN;

		BEGIN TRAN;
		
			-- update revenue and insurance details for PrepaidRevenueReporting records
			EXEC dbo.CPPL_UpdatePrepaidRevenueReportingWithProntoDetailsVer2;
		
		COMMIT TRAN;
				
		WHILE @@TRANCOUNT > 0
		BEGIN
			COMMIT WORK;
		END
		
	END TRY
	BEGIN CATCH
	
		WHILE @@TRANCOUNT > 0
		BEGIN
			ROLLBACK WORK;
		END
		
		EXEC dbo.CPPL_LogRethrowError @RethrowError = 1, @LogError = 1;
	
	END CATCH

	SET NOCOUNT OFF;

END
GO
