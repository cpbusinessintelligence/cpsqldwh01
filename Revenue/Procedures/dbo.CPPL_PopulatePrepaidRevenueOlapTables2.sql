SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_PopulatePrepaidRevenueOlapTables2]
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRY
	
		BEGIN TRAN;
		
			INSERT INTO OlapDimAccount 
			(AccountCode, AccountName)
			SELECT DISTINCT prr.AccountCode, pd.Shortname
			FROM PrepaidRevenueReporting2 prr
			LEFT OUTER JOIN Pronto.dbo.[ProntoDebtor] pd
			ON prr.AccountCode = pd.Accountcode 
			WHERE prr.AccountCode Is Not Null
			AND NOT EXISTS
			(
				SELECT 1 FROM OlapDimAccount oa
				WHERE oa.AccountCode = prr.AccountCode 
				AND oa.AccountName = pd.Shortname 
			);

		COMMIT TRAN;

		TRUNCATE TABLE OlapFactPrepaidRevenueReporting2;
		
		BEGIN TRAN;
		
			-- need to set some IDs for 'N/A' records
			--
			DECLARE @naState int;
			DECLARE @naZone int;
			DECLARE @naNWType int;
			DECLARE @naAcct int;
			DECLARE @naInsur int;
			DECLARE @naCpn int;
			DECLARE @unknownBU int;
			DECLARE @unknownDepot int;

			SELECT @naState = StateId FROM OlapDimState WHERE StateCode = 'N/A';
			SELECT @naZone = ZoneId FROM OlapDimZone WHERE ZoneCode = 'N/A';
			SELECT @naNWType = NetworkTypeId FROM OlapDimNetworkType WHERE NetworkTypeName = 'N/A';
			SELECT @naAcct = AccountId FROM OlapDimAccount WHERE AccountCode = 'N/A';
			SELECT @naInsur = InsuranceCategoryId FROM OlapDimInsuranceCategory WHERE InsuranceCategory = 'N/A';
			SELECT @naCpn = CouponTypeId FROM OlapDimCouponType WHERE CouponType = 'N/A';
			SELECT @unknownBU = BusinessUnitId FROM OlapDimBusinessUnit WHERE BusinessUnitCode = 'UNKNOWN';
			SELECT @unknownDepot = DepotId FROM OlapDimDepot WHERE DepotName = 'UNKNOWN';

			--
			-- fact table insert by business unit / depot
			--
			INSERT INTO OlapFactPrepaidRevenueReporting2 
			(FinancialYearId, FinancialPeriodId, FinancialWeekId, CalendarDayId, BusinessUnitId,
			AccountId, NetworkCategoryId, ServiceCategoryId, CouponTypeId, OriginDepotId, OriginStateId, DestinationStateId,
			BusinessUnitStateId, OriginZoneId, DestinationZoneId, OriginNetworkTypeId,
			DestinationNetworkTypeId, FreightCharge, InsuranceCharge, InsuranceCategoryId, FuelSurcharge,
			PrimaryCouponTotal, AllCouponTotal, DeadWeightTotal, CubicWeightTotal, ProntoChargeableWeightTotal,
			PickupCost, DeliveryCost, BusinessDays, BillableWeightTotal)
			SELECT
			oyr.FinancialYearId, opd.FinancialPeriodId, owk.FinancialWeekId, oday.DayId,
			ISNULL(obu.BusinessUnitId, @unknownBU), ISNULL(oacc.AccountId, @naAcct), onw.NetworkCategoryId,
			osv.ServiceCategoryId, ISNULL(ocpn.CouponTypeId, @naCpn), ISNULL(odp.DepotId, @unknownDepot), @naState, @naState, 
			ISNULL(obu.StateId, @naState), @naZone, @naZone, @naNWType, @naNWType,
			--SUM((ISNULL(prr.RevenueAmount, 0.00) - ISNULL(prr.InsuranceAmount, 0.00))),
			SUM((ISNULL(prr.RevenueAmount, 0.00))),
			SUM(ISNULL(prr.InsuranceAmount, 0.00)), ISNULL(oins.InsuranceCategoryId, @naInsur), 0.00,
			SUM(CONVERT(int, (
				CASE WHEN ISNULL(ocpn.IncludeConsignmentCount, 0) = 1
					THEN 1
				ELSE
					0
			END))), COUNT(*), 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00
			FROM PrepaidRevenueReporting2 prr (NOLOCK)
			LEFT OUTER JOIN Pronto.dbo.ProntoAccountingPeriod ap (NOLOCK)
			ON prr.RevenueRecognisedDate = ap.DayId 
			LEFT OUTER JOIN OlapDimFinancialPeriod opd (NOLOCK)
			ON ap.PeriodName = opd.PeriodName 
			LEFT OUTER JOIN OlapDimFinancialWeek owk (NOLOCK)
			ON (LEFT(ap.WeekId, 4)+'-'+RIGHT(ap.WeekId, 2)) = owk.WeekName 
			AND owk.FinancialPeriodId = opd.FinancialPeriodId 
			--LEFT OUTER JOIN OlapDimFinancialPeriod opd (NOLOCK)
			--ON owk.FinancialPeriodId = opd.FinancialPeriodId 
			--AND ap.PeriodName = opd.PeriodName 
			LEFT OUTER JOIN OlapDimFinancialYear oyr (NOLOCK)
			ON opd.FinancialYearId = oyr.FinancialYearId 
			LEFT OUTER JOIN OlapDimCalendarDay oday (NOLOCK)
			ON prr.RevenueRecognisedDate = oday.DayDate 
			LEFT OUTER JOIN OlapDimBusinessUnit obu (NOLOCK)
			ON ISNULL(prr.BUCode, '') = obu.BusinessUnitCode 
			LEFT OUTER JOIN OlapDimAccount oacc (NOLOCK)
			ON ISNULL(prr.AccountCode, '') = ISNULL(oacc.AccountCode, '')
			LEFT OUTER JOIN OlapDimNetworkCategory onw (NOLOCK)
			ON onw.NetworkCategory =
				CASE ISNULL(prr.NetworkCategory, '')
					WHEN 'M' THEN 'METRO'
					WHEN 'I' THEN 'INTERSTATE'
					WHEN 'R' THEN 'REGIONAL'
					WHEN 'N' THEN 'INTRASTATE'
				ELSE 'UNKNOWN'
			END
			LEFT OUTER JOIN OlapDimServiceCategory osv (NOLOCK)
			ON osv.ServiceCategory =
				CASE ISNULL(prr.ServiceCategory, '')
					WHEN 'P' THEN 'PARCELS'
					WHEN 'S' THEN 'SATCHELS'
					WHEN 'R' THEN 'RETURNS'
				ELSE 'UNKNOWN'
			END
			LEFT OUTER JOIN OlapDimCouponType ocpn (NOLOCK)
			ON ISNULL(prr.CouponType, '') = ocpn.CouponType 
			LEFT OUTER JOIN OlapDimInsuranceCategory oins (NOLOCK)
			ON ISNULL(prr.InsuranceCategory, '') = oins.InsuranceCategory 
			LEFT OUTER JOIN OlapDimDepot odp (NOLOCK)
			ON odp.DepotName = CASE
				WHEN LTRIM(RTRIM(ISNULL(prr.FirstScanDepot, ''))) = ''
					THEN 'UNKNOWN'
				ELSE LTRIM(RTRIM(ISNULL(prr.FirstScanDepot, '')))
			END
			WHERE prr.RevenueRecognisedDate <= CONVERT(date, DATEADD(day, -1, GETDATE()))
			AND ISNULL(prr.IsProcessed, 0) = 1
			AND ISNULL(prr.AnalysisDataExtracted, 0) = 0
			GROUP BY
				ISNULL(prr.CouponType, ''), ISNULL(prr.NetworkCategory, ''), ISNULL(prr.ServiceCategory, ''),
				ISNULL(prr.BUCode, ''), CASE
					WHEN LTRIM(RTRIM(ISNULL(prr.FirstScanDepot, ''))) = ''
						THEN 'UNKNOWN'
					ELSE LTRIM(RTRIM(ISNULL(prr.FirstScanDepot, '')))
				END, oyr.FinancialYearId, opd.FinancialPeriodId, owk.FinancialWeekId,
				oday.DayId, ISNULL(obu.BusinessUnitId, @unknownBU), ISNULL(oacc.AccountId, @naAcct),
				onw.NetworkCategoryId, osv.ServiceCategoryId, ISNULL(ocpn.CouponTypeId, @naCpn),
				ISNULL(obu.StateId, @naState), ISNULL(oins.InsuranceCategoryId, @naInsur), ISNULL(odp.DepotId, @unknownDepot);


			--
			-- fact table national insert
			--
			DECLARE @AllBU int
			DECLARE @AllSt int
			DECLARE @AllDepot int

			SELECT @AllBU = BusinessUnitId
			FROM OlapDimBusinessUnit (NOLOCK)
			WHERE BusinessUnitCode = 'NATIONAL';

			SELECT @AllSt = StateId
			FROM OlapDimState (NOLOCK)
			WHERE StateCode = 'ALL';

			SELECT @AllDepot = DepotId
			FROM OlapDimDepot (NOLOCK)
			WHERE DepotName = 'ALL';

			INSERT INTO OlapFactPrepaidRevenueReporting2 
			(FinancialYearId, FinancialPeriodId, FinancialWeekId, CalendarDayId, BusinessUnitId,
			AccountId, NetworkCategoryId, ServiceCategoryId, CouponTypeId, OriginDepotId, OriginStateId, DestinationStateId,
			BusinessUnitStateId, OriginZoneId, DestinationZoneId, OriginNetworkTypeId,
			DestinationNetworkTypeId, FreightCharge, InsuranceCharge, InsuranceCategoryId, FuelSurcharge,
			PrimaryCouponTotal, AllCouponTotal, DeadWeightTotal, CubicWeightTotal, ProntoChargeableWeightTotal,
			PickupCost, DeliveryCost, BusinessDays, BillableWeightTotal)
			SELECT
			oyr.FinancialYearId, opd.FinancialPeriodId, owk.FinancialWeekId, oday.DayId,
			@AllBU, ISNULL(oacc.AccountId, @naAcct), onw.NetworkCategoryId,
			osv.ServiceCategoryId, ISNULL(ocpn.CouponTypeId, @naCpn), @AllDepot, @naState, @naState, 
			@AllSt, @naZone, @naZone, @naNWType, @naNWType,
			--SUM((ISNULL(prr.RevenueAmount, 0.00) - ISNULL(prr.InsuranceAmount, 0.00))),
			SUM((ISNULL(prr.RevenueAmount, 0.00))),
			SUM(ISNULL(prr.InsuranceAmount, 0.00)), ISNULL(oins.InsuranceCategoryId, @naInsur), 0.00,
			SUM(CONVERT(int, (
				CASE WHEN ISNULL(ocpn.IncludeConsignmentCount, 0) = 1
					THEN 1
				ELSE
					0
			END))), COUNT(*), 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00
			FROM PrepaidRevenueReporting2 prr (NOLOCK)
			LEFT OUTER JOIN Pronto.dbo.ProntoAccountingPeriod ap (NOLOCK)
			ON prr.RevenueRecognisedDate = ap.DayId 
			LEFT OUTER JOIN OlapDimFinancialPeriod opd (NOLOCK)
			ON ap.PeriodName = opd.PeriodName 
			LEFT OUTER JOIN OlapDimFinancialWeek owk (NOLOCK)
			ON (LEFT(ap.WeekId, 4)+'-'+RIGHT(ap.WeekId, 2)) = owk.WeekName 
			AND owk.FinancialPeriodId = opd.FinancialPeriodId 
			--LEFT OUTER JOIN OlapDimFinancialPeriod opd (NOLOCK)
			--ON owk.FinancialPeriodId = opd.FinancialPeriodId 
			--AND ap.PeriodName = opd.PeriodName 
			LEFT OUTER JOIN OlapDimFinancialYear oyr (NOLOCK)
			ON opd.FinancialYearId = oyr.FinancialYearId 
			LEFT OUTER JOIN OlapDimCalendarDay oday (NOLOCK)
			ON prr.RevenueRecognisedDate = oday.DayDate 
			LEFT OUTER JOIN OlapDimAccount oacc (NOLOCK)
			ON ISNULL(prr.AccountCode, '') = ISNULL(oacc.AccountCode, '')
			LEFT OUTER JOIN OlapDimNetworkCategory onw (NOLOCK)
			ON onw.NetworkCategory =
				CASE ISNULL(prr.NetworkCategory, '')
					WHEN 'M' THEN 'METRO'
					WHEN 'I' THEN 'INTERSTATE'
					WHEN 'R' THEN 'REGIONAL'
					WHEN 'N' THEN 'INTRASTATE'
				ELSE 'UNKNOWN'
			END
			LEFT OUTER JOIN OlapDimServiceCategory osv (NOLOCK)
			ON osv.ServiceCategory =
				CASE ISNULL(prr.ServiceCategory, '')
					WHEN 'P' THEN 'PARCELS'
					WHEN 'S' THEN 'SATCHELS'
					WHEN 'R' THEN 'RETURNS'
				ELSE 'UNKNOWN'
			END
			LEFT OUTER JOIN OlapDimCouponType ocpn (NOLOCK)
			ON ISNULL(prr.CouponType, '') = ocpn.CouponType 
			LEFT OUTER JOIN OlapDimInsuranceCategory oins (NOLOCK)
			ON ISNULL(prr.InsuranceCategory, '') = oins.InsuranceCategory 
			WHERE prr.RevenueRecognisedDate <= CONVERT(date, DATEADD(day, -1, GETDATE()))
			AND ISNULL(prr.IsProcessed, 0) = 1
			AND ISNULL(prr.AnalysisDataExtracted, 0) = 0
			GROUP BY
				ISNULL(prr.CouponType, ''), ISNULL(prr.NetworkCategory, ''), ISNULL(prr.ServiceCategory, ''),
				ISNULL(prr.BUCode, ''),	oyr.FinancialYearId, opd.FinancialPeriodId, owk.FinancialWeekId,
				oday.DayId, ISNULL(oacc.AccountId, @naAcct),
				onw.NetworkCategoryId, osv.ServiceCategoryId, ISNULL(ocpn.CouponTypeId, @naCpn),
				ISNULL(oins.InsuranceCategoryId, @naInsur);--, @AllBU, @AllSt;
			
			
			/*
			 *
			 * as the table size grows, it will probably be necessary to only extract the records that
			 * haven't previously been extracted.  For the time being, the table is just rebuilt
			 * based on the records that have been processed
			 *
			-- update the records that have just been extracted
			UPDATE PrepaidRevenueReporting 
			SET AnalysisDataExtracted = 1,
				AnalysisDataExtractedDateTime = GETDATE()
			WHERE RevenueRecognisedDate <= CONVERT(date, DATEADD(day, -1, GETDATE()))
			AND ISNULL(IsProcessed, 0) = 1
			AND ISNULL(AnalysisDataExtracted, 0) = 0;
			 */
			

		COMMIT TRAN;

		BEGIN TRAN;
		
			UPDATE OlapFactPrepaidRevenueReporting2 
			SET BusinessDays = err.BusinessDays 
			FROM OlapFactPrepaidRevenueReporting2 prr
			JOIN OlapFactEdiRevenueReporting err (NOLOCK)
			ON prr.FinancialPeriodId = err.FinancialPeriodId 
			AND prr.BusinessUnitId = err.BusinessUnitId 
			WHERE ISNULL(err.BusinessDays, 0) > 0
			--and ISNULL(prr.BusinessDays, 0) <= 0;

		COMMIT TRAN;

	END TRY
	BEGIN CATCH
	
		WHILE @@TRANCOUNT > 0
		BEGIN
			ROLLBACK WORK;
		END
		
		EXEC dbo.CPPL_LogRethrowError @RethrowError = 1, @LogError = 1;
	
	END CATCH
	
	SET NOCOUNT OFF;

END
GO
