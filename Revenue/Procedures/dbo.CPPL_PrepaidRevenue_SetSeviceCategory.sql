SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC CPPL_PrepaidRevenue_SetSeviceCategory
(@ID AS UNIQUEIDENTIFIER)
AS
BEGIN


/* Luke Grenfell (2013-04-26)
-------------------------------------------------------------------------------------------------------------
PrepaidRevenueReporting_LG is used for testing purposes
will need to be changed back to PrepaidRevenueReporting for live processing.
-------------------------------------------------------------------------------------------------------------
*/

	-- set the service category, if possible
	SELECT COUNT(*) FROM PrepaidRevenueReporting_LG
	WHERE UpdateId = @ID
	AND LTRIM(RTRIM(ISNULL(ServiceCategory, ''))) = ''
	AND LEFT(LabelNumber, 3) IN ('120','121','122','127','135','136','137') OR SUBSTRING(LabelNumber, 2, 2) IN ('20','21','22','36','37','68')

	UPDATE PrepaidRevenueReporting_LG
	SET ServiceCategory = 'S'
	WHERE UpdateId = @ID
	AND LTRIM(RTRIM(ISNULL(ServiceCategory, ''))) = ''
	AND LEFT(LabelNumber, 3) IN ('120','121','122','127','135','136','137') OR SUBSTRING(LabelNumber, 2, 2) IN ('20','21','22','36','37','68')
	--PRINT 'Set ServiceCategory for Satchels result: ' + CAST(@@ROWCOUNT AS VARCHAR(10));

	UPDATE PrepaidRevenueReporting_LG
	SET ServiceCategory = 'R'			
	WHERE UpdateId = @ID
	AND LTRIM(RTRIM(ISNULL(ServiceCategory, ''))) = ''
	AND LEFT(LabelNumber, 3) IN ('182','187','183') OR SUBSTRING(LabelNumber, 2, 2) IN ('93','94','14')
	--PRINT 'Set ServiceCategory for Returns result: ' + CAST(@@ROWCOUNT AS VARCHAR(10));

	-- assume all one-offs are parcels, we can't determine whether they're "returns" or not
	UPDATE PrepaidRevenueReporting_LG
	SET ServiceCategory = 'P'		
	WHERE UpdateId = @ID
	AND LTRIM(RTRIM(ISNULL(ServiceCategory, ''))) = ''
	AND LEFT(LabelNumber, 3) LIKE '[23456789]89%'
	--PRINT 'Set ServiceCategory for Parcels result: ' + CAST(@@ROWCOUNT AS VARCHAR(10));

	DECLARE @C AS VARCHAR(10);

	SET @C = (SELECT COUNT(*) FROM PrepaidRevenueReporting_LG WHERE UpdateId = @ID AND NULLIF(ServiceCategory, '') IS NULL);
	PRINT @C + ' Records still remain that have no service category';

	SELECT TOP 10 * FROM #LinkLabels;

	/* Luke Grenfell (2013-04-26)
	-------------------------------------------------------------------------------------------------------------
	This does not make sense as there has been no inserts into #LinkLabels table so I have commented out
	-------------------------------------------------------------------------------------------------------------

	-- we can't determine the service category any further here, so
	-- we need to check the linked labels for the coupon
	UPDATE PrepaidRevenueReporting_LG					
	SET ServiceCategory = 'R'--, ReturnsFlag = 1--, ServiceCategoryChanged = 1
	FROM PrepaidRevenueReporting_LG P
	JOIN #LinkLabels ON #LinkLabels.LabelNumber = P.LabelNumber AND LinkLabelNumber LIKE '[23456789]14%'
	WHERE UpdateId = @ID
	PRINT 'Set Linked Labels ServiceCategory for Returns result: ' + CAST(@@ROWCOUNT AS VARCHAR(10));
	*/
END
GO
