SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[CPPL_ProcessPrepaidRevenueRecordsSTKVALUE]
(
	@StartDate		date,
	@EndDate		date,
	@MaxRecords		int
)
AS
BEGIN
  SET NOCOUNT ON;
     -----------------------------------------------------------------
   	 -- This is just a validation to make sure that we have given the right date paramaeters
     -----------------------------------------------------------------
     IF (ISNULL(@EndDate, '1jan2050') < ISNULL(@StartDate, '1jan1900'))
	 BEGIN
		PRINT Convert(Varchar(20),Getdate())+ '- End date cannot be less than start date.';
		RETURN;
	 END
	 ---------------------------------------------------------------
	 -- Get a Complete fetch from PrepaidRevenueReporting
     -----------------------------------------------------------------
     PRINT Convert(Varchar(20),Getdate())+ '- Starting To Load all Labels From Prepaid Revenue Reporting';
     SELECT  LTrim(Rtrim(LabelNumber)) as LabelNumber,CONVERT(Varchar(30),'') as LinkLabelNumber,
             ISNULL(CouponType, '') as CouponType,  ISNull(ServiceCategory,'') as ServiceCategory ,	FirstScanDateTime,
			 FirstScanDriverBranch,	FirstScanDriverNumber,	FirstScanContractorCode,
			 FirstScanDepot,	FirstScanType,IsFirstScanProcessed,CONVERT(Date,Null) as RevRecDate,
			 OtherScanDateTime,OtherScanDriverBranch,
			 OtherScanDriverNumber,OtherScanContractorCode,OtherScanDepot,
			 OtherScanType,NetworkCategory,BUCode,
			 BU,	ConsignmentNumber,	ConsignmentOriginState,
			 ConsignmentDestinationState,CreatedDate, ProntoOrderContractorCode,
			 ConsignmentCustomer,RevenueAmount,ConsolidateBarcode,
			 CONVERT(int,0) as UpdateFlag,CONVERT(int,0) as ServiceCategoryChanged,
		     CONVERT(int,0) as ReturnsFlag, CONVERT(int,0) as ConsolidateBarcodeChanged,
		     DATEDIFF(day, CONVERT(date, CreatedDate), CONVERT(date, GETDATE())) as ItemAge,
			 CONVERT(Varchar(100),'') as TrackingEventID,
			 CONVERT(Varchar(100),'') as OtherTrackingEventID,
			 CONVERT(Varchar(100),'') as ConsolidateOriginBranch,CONVERT(Varchar(100),'') as ConsolidateDestinationBranch,
			 CONVERT(Varchar(100),'') as OriginBranch,CONVERT(Varchar(100),'') as DestinationBranch,	CONVERT(Varchar(100),'') as OriginDepot,
			 CONVERT(Varchar(100),'') as DestinationDepot,CONVERT(Datetime,Null) as ConsolidateDateTime
     INTO #TempLabels
     FROM PrepaidRevenueReportingSTKVALUE (NOLOCK)
     WHERE ISNULL(IsProcessed, 0) = 0
		   AND RevenueAmount is not null
		   AND CONVERT(date, ISNULL(CreatedDate, '1jan1900')) >= ISNULL(@StartDate, CONVERT(date, GETDATE()))
		   AND CONVERT(date, ISNULL(CreatedDate, '1jan1900')) <= ISNULL(@EndDate, CONVERT(date, '1jan2050'))
		   
     CREATE CLUSTERED INDEX #TempLabelsIdx ON #TempLabels(LabelNumber)     

     PRINT Convert(Varchar(20),Getdate())+ '- Load Complete  Prepaid Revenue Reporting' ;   
    ------------------------------------------------------------
    ---Load all Link Labels in  prepaidRevenueLinkLabels Table
    ------------------------------------------------------------
     
    Update #Templabels SET LinkLabelNumber = PRL.LinkLabelNumber From  #Templabels TMPL Join prepaidRevenueLinkLabels PRL (NOLOCK)  ON TMPL.LabelNumber = PRL.LabelNumber 
        WHere TMPL.LabelNumber <> Rtrim(Ltrim(PRL.LinkLabelNumber))
        
     ------------------------------------------------------------
    ---Load all First Level Link Labels from   Tracking Event Table
    ------------------------------------------------------------
           
     Update #Templabels SET LinkLabelNumber = LTRIM(RTRIM(REPLACE(REPLACE(EVT.AdditionalText1, 'link', ''), 'coupon', '')))
     FROM Scannergateway.dbo.vTrackingEvent EVT (NOLOCK)
	            JOIN Scannergateway.dbo.Label LBL (NOLOCK)	ON EVT.LabelId = LBL.Id 
	            JOIN #TemplAbels TML on EVT.AdditionalText1 = 'Link Coupon ' + Ltrim(Rtrim(TML.LabelNumber ))
     WHERE EVT.EventDateTime >= DATEADD(day, -30, TML.CreatedDate)	-- allow for events up to 30 days behind the creation of the label record
		   AND EVT.EventTypeId  = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
		   AND LinkLabelNumber = ''
		   AND TML.LabelNumber <> LTRIM(RTRIM(REPLACE(REPLACE(EVT.AdditionalText1, 'link', ''), 'coupon', '')))
        
    ------------------------------------------------------------
    ---Find all labels that are linked to this label
    ------------------------------------------------------------
    
    
    Update #Templabels  SET  LinkLabelNumber = ISNull((Select Top 1 LabelNumber From #TempLabels TML where TML.LinkLabelNumber = #Templabels.LabelNumber ),'') where LinkLabelNumber = ''
      ----------------------------------------------------------------------
      --  Insering valuses to PrepaidRevenueLinkLabels if the record is not there
      ----------------------------------------------------------------------
  

       INSERT INTO PrepaidRevenueLinkLabels 
           (LabelNumber, LinkLabelNumber)
            SELECT DISTINCT LabelNumber,
                   LinkLabelNumber 
            FROM #Templabels LL
            WHERE NOT EXISTS(SELECT 1 
                             FROM PrepaidRevenueLinkLabels prl
	                         WHERE prl.LabelNumber = ll.LabelNumber
	                              AND prl.LinkLabelNumber = ll.LinkLabelNumber)
	              AND LL.LinkLabelNumber <>''

    
     ------------------------------------------------------------
    ---About to start FInding First Scan date and there by
    -- calculate Revenue Rec date
    ------------------------------------------------------------		   
		   
		   
		   
     
       SELECT	DISTINCT LBL.LabelNumber as LabelNumer,
            EVT.EventDateTime,
            EVT.AdditionalText1 ,
		    EVT.LabelID,
		    EVT.ID as EventID,
		    EVT.DriverId ,
		    EVT.EventTypeId 
     INTO  #TempCoupunPickupSummary
     FROM Scannergateway.dbo.vTrackingEvent EVT (NOLOCK)
	         JOIN Scannergateway.dbo.Label LBL (NOLOCK)	ON EVT.LabelId = LBL.Id   
     WHERE  EVT.EventTypeId  in( '98EBB899-A15E-4826-8D05-516E744C466C',   --pickup
								 'B8D04A85-A65B-41EA-9056-A950BE2CB509',   --in-depot
								 'F47CABB2-55AA-4F19-B5EE-C2754268D1AF'  --consolidate  
								)  
			   AND LBL.LabelNumber in (Select Distinct LinkLabelNumber 
	                                      from #Templabels
	                                      WHere ISNULL(LinkLabelNumber, '') != '' and IsFirstScanProcessed = 0 
	                                    Union ALl
	                                    Select Distinct Labelnumber 
	                                      from #TempLAbels Where IsFirstScanProcessed = 0 )   
     
     
     
     -----------------------------------------------------------------------
      --  We Calculate The First Scan and Mark These as FirstScanProcessed		
     ------------------------------------------------------------------------- 	
	  	  
		Select   #TempCoupunPickupSummary.EventID,
                   CASE EventTypeId WHEN '98EBB899-A15E-4826-8D05-516E744C466C'     --pickup  
                                    THEN 0 
                                    WHEN 'B8D04A85-A65B-41EA-9056-A950BE2CB509' --in-depot
                                    THEN 1
                                    WHEN 'F47CABB2-55AA-4F19-B5EE-C2754268D1AF' --consolidate 
                                    THEN 1
                                    ELSE 2 
                                    END
                                    as [Priority],
                  #TempCoupunPickupSummary.LabelNumer,
                  #TempCoupunPickupSummary.EventDateTime  
       INTO #tc1
       From #TempCoupunPickupSummary 
       	    
        create clustered index #tc1idx ON #tc1(labelnumer, [priority])
	     ---Get all Scan from LAbel Itself 

         Update #TempLabels 
	     Set TrackingEventID = (Select Top 1 Ids.EventID
	                            From #tc1 IDs 
	                            WHere  IDs.LabelNumer = #TempLAbels.LabelNumber 
	                            Order by IDs.[Priority] asc,EventDateTime asc)
	        WHere  ISNULL(TrackingEventID,'')='' ;


     
	     Update #TempLabels 
	     Set TrackingEventID = (Select Top 1 Ids.EventID
	                            From #tc1 IDs 	                                         
	                            WHere  IDs.LabelNumer =#TempLabels.LinkLabelNumber 
	                            Order by IDs.[Priority] asc,EventDateTime asc)
	     WHere  ISNULL(TrackingEventID,'')='' 
	     	
	     	     
	     	     


 
	    PRINT  Convert(Varchar(20),Getdate())+ '- Loaded all in depot and consolidate' ; 
        
 	           
	   SELECT	 Te.ID,
	             UPPER(ISNULL(b.CosmosBranch, 'UNKNOWN')) as FirstScanBranch,
			     te.EventDateTime as FirstScanDateTime,
				 d.Code as FirstScanDriver,
				 UPPER(ISNULL(dp.Name, 'UNKNOWN')) as FirstScanDepot,
				 UPPER(ISNULL(et.[Description], 'UNKNOWN')) as FirstScanType,
				 ISNULL(d.ProntoDriverCode, '') as FirstScanPronto
			into #TempFirstScanDetails
			FROM Scannergateway.dbo.vTrackingEvent te (NOLOCK)
				 JOIN Scannergateway.dbo.Driver d (NOLOCK)	ON te.DriverId = d.Id 
				 JOIN Scannergateway.dbo.Branch b (NOLOCK) ON d.BranchId = b.Id 
				 JOIN Scannergateway.dbo.EventType et (NOLOCK)	ON te.EventTypeId = et.Id 
				 LEFT OUTER JOIN Scannergateway.dbo.Depot dp (NOLOCK) ON d.DepotId = dp.Id 
		    WHERE te.Id in ( Select Distinct TrackingEventId 
		                         from #TempLabels
		                         WHere ISNULL (TrackingEventId,'') <> '' );
					
				
		   Update #TempLabels Set #TempLabels.IsFirstScanProcessed =1 ,     
		                          #TempLabels.FirstScanDriverBranch =  #TempFirstScanDetails.FirstScanBranch  ,
		                          #TempLabels.FirstScanDateTime     =   #TempFirstScanDetails.FirstScanDateTime ,
		                          #TempLabels.FirstScanDriverNumber =   #TempFirstScanDetails.FirstScanDriver  ,  
							      #TempLabels.FirstScanDepot        =   #TempFirstScanDetails.FirstScanDepot   ,
                                  #TempLabels.FirstScanType         =   #TempFirstScanDetails.FirstScanType    ,  
							      #TempLabels.FirstScanContractorCode = #TempFirstScanDetails.FirstScanPronto 
							  From #TempLabels Join #TempFirstScanDetails on #TempLabels.TrackingEventID = #TempFirstScanDetails.Id
		       Where ISnull(#TempLabels.TrackingEventID,'') <>''
		               and  #TempLabels.IsFirstScanProcessed =0
		
		 Update #TempLabels Set #TempLabels.IsFirstScanProcessed =1 , 
		                        #TempLabels.FirstScanDateTime = #TempLabels.CreatedDate Where  #TempLabels.IsFirstScanProcessed =0 
		                        
		                        
	                   
      
         Update #TempLabels Set #TempLabels.RevRecDate =  CONVERT(Date,FirstScanDateTime) where FirstScanDateTime is not null

	 ------------------------------------------------------------------------
     --- Updating #TempLabels Table to apply some business logic  
     -------------------------------------------------------------------------  
     
     PRINT Convert(Varchar(20),Getdate())+ '- About to apply some business logic' ;
     ------------------------------------------------------------------------
     --- Checking Whether satchels either national or local branch  
     ------------------------------------------------------------------------- 
      
     UPDATE #TempLabels 
	    SET ServiceCategory =  'S',
	        ServiceCategoryChanged = 1   
	 WHERE ISNULL(ServiceCategory,'') ='' 
	       AND (LEFT(LabelNumber, 3) IN ('120','121','122','127','135','136','137','188')	OR (SUBSTRING(LabelNumber, 2, 2) IN ('20','21','22','36','37','68')))
	    
	 ------------------------------------------------------------------------
     --- Checking Whether the labels are reurn RP Tracker coupons
     ------------------------------------------------------------------------- 
       
     UPDATE #TempLabels 
	    SET ServiceCategory =  'R',
	        ServiceCategoryChanged = 1 ,
	        ReturnsFlag =1           
      WHERE ISNULL(ServiceCategory,'') ='' 
           AND (LEFT(LabelNumber, 3) IN ('182','187','183') OR (SUBSTRING(LabelNumber, 2, 2) IN ('93','94','14')))
	    
	    
	 -----------------------------------------------------------------------
     --- assume all one-offs are parcels, we can't determine whether they're "returns" or not ------?
     ------------------------------------------------------------------------- 
       
     UPDATE #TempLabels 
	      SET ServiceCategory =  'P',
	          ServiceCategoryChanged = 1            
     WHERE ISNULL(ServiceCategory,'') ='' 
          AND  LEFT(LabelNumber, 3) LIKE '[23456789]89%'
          

	                 
     -----------------------------------------------------------------------
     -- we can't determine the service category any further here, so
	 -- we need to check the linked labels for the coupon				
     ------------------------------------------------------------------------- 
     UPDATE #TempLabels 
	    SET ServiceCategory =  'R',
	        ServiceCategoryChanged = 1 ,
	        ReturnsFlag =1           
     WHERE ISNULL(ServiceCategory,'') ='' 
          AND LinkLabelNumber  Like '[23456789]14%'        
          
               
       Update #TempLabels SET ServiceCategory = 'P' Where LTRIM(RTRIM(ISNULL(ServiceCategory, ''))) = ''
	 -----------------------------------------------------------------------
      --  we check for Sscans on the original label, as well as any linked			
     ------------------------------------------------------------------------- 	
     PRINT Convert(Varchar(20),Getdate())+ '- Updated all Service Category' ;
          
     SELECT	DISTINCT LBL.LabelNumber as LabelNumer,
            EVT.EventDateTime,
            EVT.AdditionalText1 ,
		    EVT.LabelID,
		    EVT.ID as EventID,
		    EVT.DriverId ,
		    EVT.EventTypeId 
     INTO  #TempCoupunActionsummary
     FROM Scannergateway.dbo.vTrackingEvent EVT (NOLOCK)
	         JOIN Scannergateway.dbo.Label LBL (NOLOCK)	ON EVT.LabelId = LBL.Id   
     WHERE  EVT.EventTypeId  in( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3',   --delivered
							     '41A8F8F9-D57E-40F0-9D9D-97767AC3069E',   --delivery
							     'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2',   --attempt 
							     '1A35AB45-B82A-492A-A1E8-6415BF846C75',   --handover
							     '93B2E381-6A89-4F2E-9131-2DC2FB300941',   --on-board 
							     'E293FFDE-76E3-4E69-BCEB-473F91B4350C')   --transfer
							   
			   AND LBL.LabelNumber in  (Select Distinct LinkLabelNumber 
	                                      from #Templabels
	                                      WHere ISNULL(LinkLabelNumber, '') != ''
	                                    Union ALl
	                                    Select Distinct Labelnumber 
	                                      from #TempLAbels)   
	                                        
     CREATE CLUSTERED INDEX #TempCoupunActionsummaryIDx ON #TempCoupunActionsummary(EventTypeId) ;
 
     PRINT Convert(Varchar(20),Getdate())+ '- Loaded all Delivery events' ;
 

	     ------------------------------------------------------------
	     --Calculating Second Scans
	     ----------------------------------------------------------
     
	 -----------------------------------------------------------------------
	-- first, we check if there's actually a delivery scan
	-- retrieve the "other" scan information, attempt/delivery get priority,
	-- or another scan that's in a different state
     ------------------------------------------------------------------------- 		
     
                        
			Select     #TempCoupunActionsummary.EventID,
                       0 as [Priority] ,
                       #TempCoupunActionsummary.LabelNumer,
                       #TempCoupunActionsummary.EventDateTime
			INTO #tc3
               From #TempCoupunActionsummary join #TempLabels ON #TempCoupunActionsummary.LabelNumer = #Templabels.LabelNumber 
               Where #TempCoupunActionsummary.EventTypeId in( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3', --delivered
							                                 '41A8F8F9-D57E-40F0-9D9D-97767AC3069E', --delivery
							                                 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2') --attempt 
           -- order by EventDateTime --asc               
		    create clustered index #tc3idx on #tc3(LabelNumer)            
                
                
	   Update #TempLabels 
	     Set OtherTrackingEventID = (Select Top 1 Ids.EventID
	                            From #tc3 IDs 
	                            WHere  IDs.LabelNumer = #TempLAbels.LabelNumber 
	                            Order by IDs.[Priority] asc,EventDateTime asc)
	     WHere OtherScanDateTime is Null 
	           and ISNULL(OtherScanDriverBranch,'')='' ; 
	           
     
     			      

	   SELECT te.Id,
			 UPPER(ISNULL(b.CosmosBranch, 'UNKNOWN')) as OtherScanBranch,
			 te.EventDateTime as OtherScanDateTime,
			 d.Code as OtherScanDriver,
			 UPPER(ISNULL(dp.Name, 'UNKNOWN')) as OtherScanDepot,
			 UPPER(ISNULL(et.[Description], 'UNKNOWN')) as OtherScanType,
			 ISNULL(d.ProntoDriverCode, '') as OtherScanPronto
	   INTO #TempOtherScanDetails
	   FROM Scannergateway.dbo.vTrackingEvent te (NOLOCK) 
	        JOIN Scannergateway.dbo.Driver d (NOLOCK)	ON te.DriverId = d.Id 
			JOIN Scannergateway.dbo.Branch b (NOLOCK)  ON d.BranchId = b.Id 
			JOIN Scannergateway.dbo.EventType et (NOLOCK)	ON te.EventTypeId = et.Id 
			LEFT OUTER JOIN Scannergateway.dbo.Depot dp (NOLOCK)ON d.DepotId = dp.Id 
			WHERE te.Id in ( Select Distinct OtherTrackingEventID 
                     from #TempLabels
                     WHere ISNULL (OtherTrackingEventID,'') <> '' );
     
 	
  		
	   Update #TempLabels 
	       Set  #TempLabels.OtherScanDriverBranch =  #TempOtherScanDetails.OtherScanBranch ,
		       #TempLabels.OtherScanDateTime     =  #TempOtherScanDetails.OtherScanDateTime ,
		       #TempLabels.OtherScanDriverNumber =  #TempOtherScanDetails.OtherScanDriver  ,  
			   #TempLabels.OtherScanDepot        =  #TempOtherScanDetails.OtherScanDepot   ,
               #TempLabels.OtherScanType         =  #TempOtherScanDetails.OtherScanType    ,  
			   #TempLabels.OtherScanContractorCode =  #TempOtherScanDetails.OtherScanPronto  
			   From #TempLabels Join #TempOtherScanDetails ON #TempLabels.OtherTrackingEventID = #TempOtherScanDetails.Id
		   Where ISnull(#TempLabels.OtherTrackingEventID,'') <>''
		        and   #TempLabels.OtherScanDateTime is Null 
	            and ISNULL( #TempLabels.OtherScanDriverBranch,'')='' 
	            

	 	  PRINT  Convert(Varchar(20),Getdate())+ '- Updated all Delivery  Scans Found' ; 			      
 
	  -------------------------------------------------------------------------  
	   -- check the STKVALUE label, and linked labels, for "other" scan event
	   -- retrieve the "other" scan information, attempt/delivery get priority,
	   -- or another scan that's in a different state
	  -------------------------------------------------------------------------
	  
	  --
	  Select          #TempCoupunActionsummary.EventID,
                       0 as [Priority] ,
                      #TempCoupunActionsummary.LabelNumer,
                      #TempCoupunActionsummary.EventDateTime 
		INTO #ts1                     
	   From #TempCoupunActionsummary join #TempLabels ON #TempCoupunActionsummary.LabelNumer = #Templabels.LabelNumber  
	   Where EventTypeId in( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3', --delivered
							 '41A8F8F9-D57E-40F0-9D9D-97767AC3069E', --delivery
							 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2') --attempt 
		--order by EventDateTime 
		create clustered index #ts1idx ON #ts1(LabelNumer, [Priority])
		
		--
		SELECT           te.[EventId], 
			             1 AS [Priority],
			             te. LabelNumer,
			              te.EventDateTime
		INTO #ts2			             
		FROM #TempCoupunActionsummary te  join #TempLabels ON te.LabelNumer = #Templabels.LabelNumber  
			JOIN Scannergateway.dbo.Driver d (NOLOCK) ON te.DriverId = d.Id 
		JOIN Scannergateway.dbo.Label l (NOLOCK)
		ON te.LabelId = l.Id 
		WHERE  te.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' --on-board	-- on-board scan by a contractor takes priority over transfer/handover
			 AND ISNULL(d.IsContractor, 0) = 1
		-- ORDER BY te.EventDateTime		
		create clustered index #ts2idx ON #ts2(LabelNumer, [Priority])
                
	 --
	 SELECT te.[EventId], 
	         3 AS [Priority],
	         te.LabelNumer,
	         te.EventDateTime
	INTO #ts3	         
			FROM #TempCoupunActionsummary te	  join #TempLabels ON te.LabelNumer = #Templabels.LabelNumber  
			   JOIN Scannergateway.dbo.Driver d (NOLOCK)
			ON te.DriverId = d.Id 
	 WHERE	te.EventTypeId IN(--'B8D04A85-A65B-41EA-9056-A950BE2CB509', --in-depot
				              --'F47CABB2-55AA-4F19-B5EE-C2754268D1AF', --consolidate
				 	          --'93B2E381-6A89-4F2E-9131-2DC2FB300941', --on-board
							 '1A35AB45-B82A-492A-A1E8-6415BF846C75', --handover
							 'E293FFDE-76E3-4E69-BCEB-473F91B4350C') --transfer
				OR	 (te.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' --on-board ("accept in-depot") --- we'll take an "on-board" scan by a non-contractor as an "in-depot" scan
					     AND ISNULL(d.IsContractor, 0) = 0)              --not a contractor (agent or depot)					
		--ORDER BY te.EventDateTime	   
	create clustered index #ts3idx ON #ts3(LabelNumer, [Priority])
	
	  INSERT INTO #ts1
        SELECT a.*
        FROM #ts2 a
        LEFT JOIN #ts1 b ON a.LabelNumer = b.LabelNumer
        WHERE b.LabelNumer IS NULL 
	
	
	  INSERT INTO #ts1
        SELECT a.*
        FROM #ts3 a
        LEFT JOIN #ts1 b ON a.LabelNumer = b.LabelNumer
        WHERE b.LabelNumer IS NULL 
	
	--
	Update #TempLabels 
        Set OtherTrackingEventID = (Select Top 1 Ids.EventID
                From #ts1 IDs 
                WHere  IDs.LabelNumer = #TempLAbels.LabelNumber 
                Order by IDs.[Priority] asc,IDs.EventDateTime asc)
       WHere  ISNULL(OtherTrackingEventID,'')='' ; 
     
      Update #TempLabels 
         Set OtherTrackingEventID = (Select Top 1 Ids.EventID
	                            From #tc1 IDs 	                                         
	                            WHere  IDs.LabelNumer =#TempLAbels.LinkLabelNumber 
	                              Order by IDs.[Priority] asc,EventDateTime asc)
	        WHere  ISNULL(OtherTrackingEventID,'')='' ; 

		  Update #TempLabels 
         Set OtherTrackingEventID = (Select Top 1 Ids.EventID
	                            From #tc1 IDs 	                                         
	                            WHere  IDs.LabelNumer =#TempLAbels.LinkLabelNumber
	                            Order by IDs.[Priority] desc,EventDateTime asc)
	        WHere  ISNULL(OtherTrackingEventID,'')='' ; 

	   SELECT te.Id,
			 UPPER(ISNULL(b.CosmosBranch, 'UNKNOWN')) as OtherScanBranch,
			 te.EventDateTime as OtherScanDateTime,
			 d.Code as OtherScanDriver,
			 UPPER(ISNULL(dp.Name, 'UNKNOWN')) as OtherScanDepot,
			 UPPER(ISNULL(et.[Description], 'UNKNOWN')) as OtherScanType,
			 ISNULL(d.ProntoDriverCode, '') as OtherScanPronto
	   INTO #TempOtherScanDetails2
	   FROM Scannergateway.dbo.vTrackingEvent te (NOLOCK) 
	        JOIN Scannergateway.dbo.Driver d (NOLOCK)	ON te.DriverId = d.Id 
			JOIN Scannergateway.dbo.Branch b (NOLOCK)  ON d.BranchId = b.Id 
			JOIN Scannergateway.dbo.EventType et (NOLOCK)	ON te.EventTypeId = et.Id 
			LEFT OUTER JOIN Scannergateway.dbo.Depot dp (NOLOCK)ON d.DepotId = dp.Id 
			WHERE te.Id in ( Select Distinct OtherTrackingEventID 
                     from #TempLabels
                     WHere ISNULL (OtherTrackingEventID,'') <> '');
     
 
   		  Update #TempLabels 
	       Set #TempLabels.OtherScanDriverBranch =  #TempOtherScanDetails2.OtherScanBranch   ,
		       #TempLabels.OtherScanDateTime     =  #TempOtherScanDetails2.OtherScanDateTime ,
		       #TempLabels.OtherScanDriverNumber =  #TempOtherScanDetails2.OtherScanDriver   ,  
			   #TempLabels.OtherScanDepot        =  #TempOtherScanDetails2.OtherScanDepot    ,
               #TempLabels.OtherScanType         =  #TempOtherScanDetails2.OtherScanType     ,  
			   #TempLabels.OtherScanContractorCode = #TempOtherScanDetails2.OtherScanPronto  
		   From #TempLabels join   #TempOtherScanDetails2 on #Templabels.OtherTrackingEventID = #TempOtherScanDetails2.Id
		   Where ISnull(#TempLabels.OtherTrackingEventID,'') <>''
		        and  #Templabels.OtherScanDateTime Is Null AND ISNULL(#Templabels.OtherScanDriverBranch, '') = ''
		  
		  
		  	
 	  PRINT  Convert(Varchar(20),Getdate())+ '- Updated all Other  Scans Found' ; 		
	
	 	        
	-------------------------------------------------------------------------
    	-- check labels for consolidation scan - this will tell us whether it's
		-- travelling interstate or not
		-- for "consolidate" events, we take the most recent one, as that should
		-- give us the correct information about the destination of the item
     -------------------------------------------------------------------------
    	
		        
	  Update #TempLabels 
	            Set ConsolidateBarcode =Ltrim(Rtrim(ISNULL((Select Top 1 #TempCoupunActionsummary.AdditionalText1
	                                                         from #TempCoupunActionsummary 
	                                                         Where EventTypeId = 'F47CABB2-55AA-4F19-B5EE-C2754268D1AF' and  #TempCoupunActionsummary.LabelNumer = #TempLabels.LabelNumber 
	                                                         order by #TempCoupunActionsummary.EventDateTime desc),''))) ,
	            ConsolidateDateTime = (Select Top 1 #TempCoupunActionsummary.EventDateTime
	                                                         from #TempCoupunActionsummary 
	                                                         Where EventTypeId = 'F47CABB2-55AA-4F19-B5EE-C2754268D1AF' and  #TempCoupunActionsummary.LabelNumer = #TempLabels.LabelNumber 
	                                                         order by #TempCoupunActionsummary.EventDateTime desc)	

	       Where ISNULL(#TempLabels.ConsolidateBarcode,'') = '' 
	       
	       
	      	        
		 	  
	       
	  Update #TempLabels SET  ConsolidateOriginBranch =   CASE RIGHT(Rtrim(LTrim(ConsolidateBarcode)), 1)WHEN 'A' THEN 'ADELAIDE'
																						      WHEN 'B' THEN 'BRISBANE'
																							  WHEN 'C' THEN 'CANBERRA'
																							  WHEN 'M' THEN 'MELBOURNE'
																						      WHEN 'O' THEN 'GOLDCOAST'
																							  WHEN 'S' THEN 'SYDNEY'
																						      WHEN 'P' THEN 'PERTH'
																						      ELSE ''
																						      END,
                             ConsolidateDestinationBranch =   CASE SUBSTRING(Rtrim(LTrim(ConsolidateBarcode)),3, 1)WHEN 'A' THEN 'ADELAIDE'
																						      WHEN 'B' THEN 'BRISBANE'
																							  WHEN 'C' THEN 'CANBERRA'
																							  WHEN 'M' THEN 'MELBOURNE'
																						      WHEN 'O' THEN 'GOLDCOAST'
																							  WHEN 'S' THEN 'SYDNEY'
																						      WHEN 'P' THEN 'PERTH'
																						      ELSE ''
																						      END,    
			            ConsolidateBarcodeChanged = 1 
			 Where ISNULL(#TempLabels.ConsolidateBarcode,'') <> '' 
			    and LEN(ConsolidateBarcode) = 14 
			    AND ConsolidateBarcode LIKE 'CP[ABMOSPC][0123456789]%[ABMOSPC]' 
		        
	 	  PRINT  Convert(Varchar(20),Getdate())+ '- Updated all Consolidate  Scans Found' ; 		
		     
     -------------------------------------------------------------------------
	 -- if there's no consignment details on this particular label, then we'll
	 -- check if there are consignment details on one of the "linked" labels.
	-- We just take the first one we find, if there happen to be multiples
     -------------------------------------------------------------------------
     
     Update #TempLabels Set ConsignmentOriginState = prr.ConsignmentOriginState ,
     ConsignmentDestinationState = prr.ConsignmentDestinationState
      From #TempLabels Join PrepaidRevenueReportingSTKVALUE PRR ON #TempLabels.LinkLabelNumber = PRR.LabelNumber 
         Where PRR.ConsignmentDestinationState is not null
          and PRR.ConsignmentOriginState is not null 
     
     
     --Update #TempLabels SET ConsignmentOriginState = ISNull((SELECT TOP 1 	prr.ConsignmentOriginState
     --                                                   	FROM PrepaidRevenueReporting prr
					--										     JOIN #LinkLabels ll	ON prr.LabelNumber = ll.LinkLabelNumber
					--										WHERE prr.ConsignmentOriginState Is Not Null
					--											AND prr.ConsignmentDestinationState Is Not Null
					--											And prr.LabelNumber = #TempLabels.LabelNumber ),''),										
     --                        ConsignmentDestinationState =ISNull((SELECT TOP 1 	prr.ConsignmentDestinationState 
     --                                                   	FROM PrepaidRevenueReporting prr
					--										     JOIN #LinkLabels ll	ON prr.LabelNumber = ll.LinkLabelNumber
					--										WHERE prr.ConsignmentOriginState Is Not Null
					--											AND prr.ConsignmentDestinationState Is Not Null
					--											And prr.LabelNumber = #TempLabels.LabelNumber ),'')  
     --         WHere ConsignmentOriginState is null --and SkipRevenueRecognition = 0
     --                and ConsignmentDestinationState is null
     --                and  EXISTS (SELECT 1
     --                      FROM PrepaidRevenueReporting prr
			  --                  JOIN #TempLabels ll	ON prr.LabelNumber = ll.LinkLabelNumber
					--	   WHERE prr.ConsignmentOriginState Is Not Null
					--		 AND prr.ConsignmentDestinationState Is Not Null)
							 
							
	 -------------------------------------------------------------------------
			-- Drop all Temperory table here
     -------------------------------------------------------------------------						 	
			Drop table #TempCoupunActionsummary 
			Drop Table #TempFirstScanDetails
			Drop Table #TempOtherScanDetails
			--Drop Table #TempEventsummary 
			--Drop Table #LinkLabels
			--Drop Table #TempOtherScanDetails2				 
	  -------------------------------------------------------------------------
			-- try and set origin / destination details
     -------------------------------------------------------------------------
     
       	  -------------------------------------------------------------------------
			-- try and set origin / destination details
			-- either we have enough information to set the relevant properties for revenue recognition,
			-- or this particular record has gone past the threshold days, in which case we process it 			
     -------------------------------------------------------------------------			   		 			
     Update #TempLabels SET OriginBranch = FirstScanDriverBranch 
           WHere ISNULL(FirstScanDriverBranch,'') <>''
            and ISNULL(OriginBranch,'') =''
           
     Update #TempLabels SET OriginBranch = ConsolidateOriginBranch 
                                       WHere ISNULL(ConsolidateOriginBranch,'') <>'' 
                                       and ISNULL(OriginBranch,'') = '' 
     Update #TempLabels SET OriginBranch = CASE Ltrim(Rtrim(ConsignmentOriginState))WHEN 'WA' THEN 'PERTH'
																					WHEN 'VIC' THEN 'MELBOURNE'
																					WHEN 'TAS' THEN 'MELBOURNE'
																					WHEN 'SA' THEN 'ADELAIDE'
																					WHEN 'NT' THEN 'ADELAIDE'
																					WHEN 'QLD' THEN 'BRISBANE'
																					WHEN 'NSW' THEN 'SYDNEY'
																					WHEN 'ACT' THEN 'SYDNEY'
																					ELSE 'UNKNOWN'
																					END 
		  WHere ISNULL(ConsignmentOriginState,'') <>'' 
		  and ISNULL(OriginBranch,'') = '' 
     Update #TempLabels SET OriginBranch = 'UNKNOWN' WHEre ISNULL(OriginBranch,'' ) = ''
       and ISNULL(OriginBranch,'') = '' 
     
     
     Update #TempLabels SET OriginDepot = FirstScanDepot Where ISNULL(FirstScanDepot,'') <>'' 
     and ISNULL(OriginDepot,'') = '' 
     Update #TempLabels SET OriginDepot = 'UNKNOWN' Where ISNULL(OriginDepot,'') <>'' 
     and ISNULL(OriginDepot,'') = '' 	
      	  PRINT  Convert(Varchar(20),Getdate())+ '- Updated all Parcel Origin Details' ; 		
	
	  Update #TempLabels SET DestinationBranch  = ConsolidateDestinationBranch Where ISNULL(ConsolidateDestinationBranch,'') <>''  
     and ISNULL(DestinationBranch,'') = '' 
     Update #TempLabels SET DestinationBranch =  OtherScanDriverBranch Where ISNULL(OtherScanDriverBranch,'') <>'' 
     and ISNULL(DestinationBranch,'') = '' 
     Update #TempLabels SET DestinationBranch = CASE Ltrim(Rtrim(ConsignmentDestinationState))WHEN 'WA' THEN 'PERTH'
																					          WHEN 'VIC' THEN 'MELBOURNE'
																							  WHEN 'TAS' THEN 'MELBOURNE'
																							  WHEN 'SA' THEN 'ADELAIDE'
																						      WHEN 'NT' THEN 'ADELAIDE'
																							  WHEN 'QLD' THEN 'BRISBANE'
																							  WHEN 'NSW' THEN 'SYDNEY'
																							  WHEN 'ACT' THEN 'SYDNEY'
																							  ELSE 'UNKNOWN'
																						      END 
		  WHere ISNULL(ConsignmentDestinationState,'') <>'' 
		  and ISNULL(DestinationBranch,'') = '' 
	 Update #TempLabels SET DestinationBranch = 'UNKNOWN' WHEre ISNULL(DestinationBranch,'' ) = '' 	
	 and ISNULL(DestinationBranch,'') = '' 
		  
     Update #TempLabels SET DestinationDepot = OtherScanDepot Where ISNULL(OtherScanDepot,'') <>''
     and ISNULL(DestinationDepot,'') = '' 
     Update #TempLabels SET DestinationDepot = 'UNKNOWN' Where ISNULL(DestinationDepot,'') <>''
     and ISNULL(DestinationDepot,'') = '' 
     
    
							                                    
      


  	       		 												                                    
    
      
	   Update #TempLabels SET  BUCode = dbo.CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue(
															ProntoOrderContractorCode,
															ReturnsFlag,
															FirstScanDriverBranch,
															FirstScanDepot,
															OtherScanDriverBranch,
															OtherScanDepot,
															LEFT(LTRIM(RTRIM(#TempLabels.LabelNumber)), 3),
															ConsolidateOriginBranch,
															ConsolidateDestinationBranch,
															ConsignmentOriginState,
															ConsignmentDestinationState)  
    
															
	 Update #TempLabels SET  BUCode =	'UNKNOWN' Where (ISNULL(BUCode, '') = '') 
    
	 
	 Update #TempLabels SET BU = CASE BUCode
						WHEN 'CAD' THEN 'ADELAIDE'
						WHEN 'CCB' THEN 'CANBERRA'
						WHEN 'CBN' THEN 'BRISBANE'
						WHEN 'CSC' THEN 'SUNSHINE COAST'
						WHEN 'CSY' THEN 'SYDNEY'
						WHEN 'CME' THEN 'MELBOURNE'
						WHEN 'CCC' THEN 'CENTRAL COAST'
						WHEN 'COO' THEN 'GOLD COAST'
						WHEN 'CPE' THEN 'PERTH'
						ELSE 'UNKNOWN'
					END 
    
     -------------------------------------------------------------------------
    	-- if the item is less than 14 days' old, and we haven't had some kind of "delivery" scan yet,
		-- then we'll leave it until the next day's processing - otherwise, continue processing
     -------------------------------------------------------------------------
     
	 	UPDATE PrepaidRevenueReportingSTKVALUE 
					SET PrepaidRevenueReportingSTKVALUE.LinkLabelNumber  = #TempLabels.LinkLabelNumber,
					    PrepaidRevenueReportingSTKVALUE.FirstScanContractorCode = #TempLabels.FirstScanContractorCode,
						PrepaidRevenueReportingSTKVALUE.FirstScanDateTime = #TempLabels.FirstScanDateTime,
						PrepaidRevenueReportingSTKVALUE.FirstScanDepot = #TempLabels.FirstScanDepot,
						PrepaidRevenueReportingSTKVALUE.FirstScanDriverBranch = #TempLabels.FirstScanDriverBranch,
						PrepaidRevenueReportingSTKVALUE.FirstScanDriverNumber = #TempLabels.FirstScanDriverNumber ,
						PrepaidRevenueReportingSTKVALUE.FirstScanType = #TempLabels.FirstScanType,
						PrepaidRevenueReportingSTKVALUE.OtherScanContractorCode = #TempLabels.OtherScanContractorCode,
						PrepaidRevenueReportingSTKVALUE.OtherScanDateTime = #TempLabels.OtherScanDateTime,
						PrepaidRevenueReportingSTKVALUE.OtherScanDepot = #TempLabels.OtherScanDepot,
						PrepaidRevenueReportingSTKVALUE.OtherScanDriverBranch = #TempLabels.OtherScanDriverBranch,
						PrepaidRevenueReportingSTKVALUE.OtherScanDriverNumber = #TempLabels.OtherScanDriverNumber,
						PrepaidRevenueReportingSTKVALUE.OtherScanType = #TempLabels.OtherScanType,
						PrepaidRevenueReportingSTKVALUE.ServiceCategory = #TempLabels.ServiceCategory,
						PrepaidRevenueReportingSTKVALUE.RevenueProcessedDateTime = GETDATE(),
						PrepaidRevenueReportingSTKVALUE.RevenueRecognisedDate = #TempLabels.RevRecDate,
						PrepaidRevenueReportingSTKVALUE.OriginDepot = #TempLabels.OriginDepot,
						PrepaidRevenueReportingSTKVALUE.OriginBranch = #TempLabels.OriginBranch,
						PrepaidRevenueReportingSTKVALUE.BU = #TempLabels.BU,
						PrepaidRevenueReportingSTKVALUE.BUCode = #TempLabels.BUCode,
						PrepaidRevenueReportingSTKVALUE.ISFirstScanProcessed = 1,
						PrepaidRevenueReportingSTKVALUE.NetworkCategory = #TempLabels.NetworkCategory,
						PrepaidRevenueReportingSTKVALUE.ConsolidateBarcode = ISNULL(#TempLabels.ConsolidateBarcode, ''),
						PrepaidRevenueReportingSTKVALUE.ConsolidateScanDateTime = #TempLabels.ConsolidateDateTime
				    from PrepaidRevenueReportingSTKVALUE Inner Join #TempLabels on PrepaidRevenueReportingSTKVALUE.LabelNumber = #TempLabels.LabelNumber 	
				    WHere #TempLabels.OtherScanDateTime is null and #TempLabels.ItemAge <14

    
	   Delete  #TempLabels --SET SkipRevenueRecognition =1 
	        Where OtherScanDateTime is null and ItemAge <14 ;
	   
          	  PRINT  Convert(Varchar(20),Getdate())+ '- Updated Main table with Parcel Origin Details' ; 		
          	   
   
				
		---------------------------------------			
    	-- need to determine the network category
    	---------------------------------------
	 	--Update #TempLabels Set NetworkCategory = Null	Where  UpdateFlag =1
    
	 	------------------------------------------------------------------------------
	 	 -- if there's a consolidation barcode, then we assume it's interstate, except for Sydney-Canberra transfer	
	 	 ------------------------------------------------------------------------------
	 Update #TempLabels SET NetworkCategory =  Null WHere ((ConsolidateDestinationBranch IN ('SYDNEY','CANBERRA'))AND (ConsolidateOriginBranch IN ('SYDNEY','CANBERRA')))
	                                        AND ((ISNULL(ConsolidateDestinationBranch, '') != '') AND 	(ISNULL(ConsolidateOriginBranch, '') != ''))	 --don't categorise Sydney-Canberra yet
	                                       -- and UpdateFlag =1 
    
      Update #TempLabels SET NetworkCategory = 'I' WHere 	((ConsolidateDestinationBranch NOT IN ('SYDNEY','CANBERRA'))AND (ConsolidateOriginBranch NOT IN ('SYDNEY','CANBERRA')))                                         
														    AND ((ISNULL(ConsolidateDestinationBranch, '') != '') AND 	(ISNULL(ConsolidateOriginBranch, '') != ''))	--interstate
														--    and UpdateFlag =1
    
														    
														    
	   Update #TempLabels SET NetworkCategory = dbo.CPPL_fn_GetNetworkCategoryForPrepaidRevenue 
								(
									FirstScanDriverBranch,
									FirstScanDepot,
									ConsignmentOriginState,
									OtherScanDriverBranch,
									OtherScanDepot,
									ConsignmentDestinationState,
									LEFT(LTRIM(RTRIM(#TempLabels.LabelNumber)), 3),
									CouponType
						)	 
						Where  ISNULL(NetworkCategory, '') = ''--and UpdateFlag =1
    	
						
	 Update #TempLabels SET NetworkCategory = 'X'	Where  ISNULL(NetworkCategory, '') = ''	
-- Delete From #Templabels Where  ISNULL(NetworkCategory, '') = '' or ISNULL(NetworkCategory, '') = 'X'	--and UpdateFlag =1
	 
          	  PRINT  Convert(Varchar(20),Getdate())+ '- Applied all Business Logic' ; 		
	 	UPDATE PrepaidRevenueReportingSTKVALUE 
					SET PrepaidRevenueReportingSTKVALUE.LinkLabelNumber  = #TempLabels.LinkLabelNumber,
					    PrepaidRevenueReportingSTKVALUE.FirstScanContractorCode = #TempLabels.FirstScanContractorCode,
						PrepaidRevenueReportingSTKVALUE.FirstScanDateTime = #TempLabels.FirstScanDateTime,
						PrepaidRevenueReportingSTKVALUE.FirstScanDepot = #TempLabels.FirstScanDepot,
						PrepaidRevenueReportingSTKVALUE.FirstScanDriverBranch = #TempLabels.FirstScanDriverBranch,
						PrepaidRevenueReportingSTKVALUE.FirstScanDriverNumber = #TempLabels.FirstScanDriverNumber ,
						PrepaidRevenueReportingSTKVALUE.FirstScanType = #TempLabels.FirstScanType,
						PrepaidRevenueReportingSTKVALUE.OtherScanContractorCode = #TempLabels.OtherScanContractorCode,
						PrepaidRevenueReportingSTKVALUE.OtherScanDateTime = #TempLabels.OtherScanDateTime,
						PrepaidRevenueReportingSTKVALUE.OtherScanDepot = #TempLabels.OtherScanDepot,
						PrepaidRevenueReportingSTKVALUE.OtherScanDriverBranch = #TempLabels.OtherScanDriverBranch,
						PrepaidRevenueReportingSTKVALUE.OtherScanDriverNumber = #TempLabels.OtherScanDriverNumber,
						PrepaidRevenueReportingSTKVALUE.OtherScanType = #TempLabels.OtherScanType,
						PrepaidRevenueReportingSTKVALUE.ServiceCategory = #TempLabels.ServiceCategory,
						PrepaidRevenueReportingSTKVALUE.OriginDepot = #TempLabels.OriginDepot,
						PrepaidRevenueReportingSTKVALUE.OriginBranch = #TempLabels.OriginBranch,
						PrepaidRevenueReportingSTKVALUE.DestinationDepot = #TempLabels.DestinationDepot,
						PrepaidRevenueReportingSTKVALUE.DestinationBranch = #TempLabels.DestinationBranch,						
						PrepaidRevenueReportingSTKVALUE.RevenueProcessedDateTime = GETDATE(),
						PrepaidRevenueReportingSTKVALUE.RevenueRecognisedDate = #TempLabels.RevRecDate,
						PrepaidRevenueReportingSTKVALUE.BU = #TempLabels.BU,
						PrepaidRevenueReportingSTKVALUE.BUCode = #TempLabels.BUCode,
						PrepaidRevenueReportingSTKVALUE.IsProcessed = 1,
						PrepaidRevenueReportingSTKVALUE.IsFirstScanProcessed = 1,
						PrepaidRevenueReportingSTKVALUE.NetworkCategory = #TempLabels.NetworkCategory,
						PrepaidRevenueReportingSTKVALUE.ConsolidateBarcode = ISNULL(#TempLabels.ConsolidateBarcode, ''),
						PrepaidRevenueReportingSTKVALUE.ConsolidateScanDateTime = #TempLabels.ConsolidateDateTime
				    from PrepaidRevenueReportingSTKVALUE Inner Join #TempLabels on PrepaidRevenueReportingSTKVALUE.LabelNumber = #TempLabels.LabelNumber 	

           	  PRINT  Convert(Varchar(20),Getdate())+ '- Updated main Table and Finished Processing' ; 		   
	
 --Select CouponType,* from #TempLAbels--  WHere NetworkCategory = 'X'    and FirstScanDateTime is null   
                                     
                                                                                          
		   SET NOCOUNT OFF;
  

END


--[CPPL_ProcessPrepaidRevenueRecords] '30 June 2013','9 July 2013',0
GO
