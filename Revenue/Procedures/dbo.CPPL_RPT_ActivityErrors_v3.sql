SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_RPT_ActivityErrors_v3](@branch varchar(20))

AS
BEGIN
  SET NOCOUNT ON;
     SELECT [ActivityFileOn] as ProntoActivityErrorAsOn
      ,[ActivityDateTime]
      ,[SerialNumber]
      ,CONVERT(varchar(20),'') as StartSerialNumber
      ,CONVERT(varchar(20),'')  as CouponType
      ,CONVERT(varchar(20),'')  as Branch,
	   CONVERT(varchar(20),'')  as Whsecode
      ,1 as CouponCount
      ,CONVERT(varchar(20),'')  as Reason
      ,DATEDIFF(day,[ActivityDateTime],getdate())  as duration
     INTO #Temp1
     FROM [Revenue].[dbo].[ActivityErrorImport]


     
   Update #Temp1 SET CouponType = 'INVALID COUPONS' WHere LEN(Rtrim(Ltrim([SerialNumber])))<>11  or ISNUMERIC([SerialNumber]) = 0
   Update #Temp1 SET CouponType =  ISnull(Revenue.dbo.CPPL_fn_GetCouponTypeFromPrefix(Left([SerialNumber],3)) ,'INVALID COUPONS' )
   Update #Temp1 SET Reason = 'Invalid Coupons' Where CouponType = 'INVALID COUPONS' 
   Update #Temp1 SET CouponType = '' Where CouponType = 'INVALID COUPONS' 
   
  -- Update #Temp1 SET Branch = Revenue.dbo.CPPL_fn_GetCouponBranchFromPrefix(Left([SerialNumber],3)) Where Reason <> 'INVALID COUPONS'
   Update #Temp1 SET Whsecode=P.SerialWhseCode  from #Temp1 T  join [Pronto].[dbo].[ProntoStockSerialNumber] P on  T.SerialNumber=P.SerialNumber
   
   Update #Temp1 SET  Reason = 'Conversi Coupons'  From #Temp1 T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber and C.LastSoldReference = 'CONVERSI' WHERE T.Reason = ''
   Update #Temp1 SET  Reason = 'Coupons Dont Exist'  From #Temp1 T Left Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber WHERE T.Reason = '' and  C.SerialNumber  is null 
   ---Updated by PV on 28/09/2020: Check if the coupon exist in Purge DB
   Update #Temp1 SET  Reason = 'Coupons Dont Exist'  From #Temp1 T Left Join [ProntoPurge_Bkup_Do_Not Delete].dbo.[ProntoCouponDetails_bkup_12062020_do_not_delete] C on T.SerialNumber = C.SerialNumber WHERE T.Reason = 'Coupons Dont Exist' --and  C.SerialNumber  is null 
   --
   Update #Temp1 SET  Reason = 'Coupons Not Sold'   From #Temp1 T Left Join Pronto.dbo.ProntoStockSerialLink C on T.SerialNumber = C.SerialNumber and c.SerialLinkType = 'S' WHERE T.Reason = '' and  C.SerialNumber  is null 
   
   Update #Temp1 SET  StartSerialNumber = C.StartSerialNumber  From #Temp1 T Join Pronto.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
   Update #Temp1 SET Branch = 'UNKNOWN' WHere Branch =''
   Update #Temp1 SET Reason = 'Unknown' WHere Reason =''
    Update #Temp1 SET Branch=
                               CASE WHEN #Temp1.WhseCode  like 'S%'   THEN 'SYDNEY'
                                    WHEN #Temp1.WhseCode like 'M%'THEN  'MELBOURNE' 
                                    WHEN #Temp1.WhseCode like 'A%'THEN  'ADELAIDE'
                                    WHEN #Temp1.WhseCode like 'P%'THEN  'PERTH'
                                    WHEN #Temp1.WhseCode like 'B%'THEN  'BRISBANE'
                                    WHEN #Temp1.WhseCode like 'G%'THEN  'GOLD COAST' 
                                    ELSE  case when #Temp1.WhseCode like 'CAD%' then 'ADELAIDE'
									           when #Temp1.WhseCode like 'CBN%' then 'BRISBANE'
											   when #Temp1.WhseCode like 'CME%' then 'MELBOURNE'
											   when #Temp1.WhseCode like 'COO%' then  'GOLD COAST'
											   when #Temp1.WhseCode like 'CPE%' then  'PERTH'
											   when #Temp1.WhseCode like 'CCB%' or  #Temp1.WhseCode like 'CSY%' or #Temp1.WhseCode like 'C[0-9][0-9][0-9]' then 'SYDNEY' else ''  END  END
                                     Where Reason <> 'INVALID COUPONS'

   IF @Branch = 'ALL'
      Select * from #Temp1 
	 -- where  Reason <> 'Conversi Coupons'  
	  order by duration desc
 ELSE 
    Select  * from #Temp1 WHERE Branch = @Branch 
	--and Reason <> 'Conversi Coupons' 
	order by duration desc
  
  
                                                                      
   SET NOCOUNT OFF;
  

END
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_ActivityErrors_v3]
	TO [ReportUser]
GO
