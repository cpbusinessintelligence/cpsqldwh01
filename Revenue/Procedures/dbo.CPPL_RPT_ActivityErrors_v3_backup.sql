SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[CPPL_RPT_ActivityErrors_v3_backup](@branch varchar(20))

AS
BEGIN
  SET NOCOUNT ON;
     SELECT [ActivityFileOn] as ProntoActivityErrorAsOn
      ,[ActivityDateTime]
      ,[SerialNumber]
      ,CONVERT(varchar(20),'') as StartSerialNumber
      ,CONVERT(varchar(20),'')  as CouponType
      ,CONVERT(varchar(20),'')  as Branch
      ,1 as CouponCount
      ,CONVERT(varchar(20),'')  as Reason
      ,DATEDIFF(day,[ActivityDateTime],getdate())  as duration
     INTO #Temp1
     FROM [Revenue].[dbo].[ActivityErrorImport]
     
   Update #Temp1 SET CouponType = 'INVALID COUPONS' WHere LEN(Rtrim(Ltrim([SerialNumber])))<>11  or ISNUMERIC([SerialNumber]) = 0
   Update #Temp1 SET CouponType =  ISnull(Revenue.dbo.CPPL_fn_GetCouponTypeFromPrefix(Left([SerialNumber],3)) ,'INVALID COUPONS' )
   Update #Temp1 SET Reason = 'Invalid Coupons' Where CouponType = 'INVALID COUPONS' 
   Update #Temp1 SET  CouponType = '' Where CouponType = 'INVALID COUPONS' 
   Update #Temp1 SET Branch = Revenue.dbo.CPPL_fn_GetCouponBranchFromPrefix(Left([SerialNumber],3)) Where Reason <> 'INVALID COUPONS'
   
   Update #Temp1 SET  Reason = 'Conversi Coupons'  From #Temp1 T Join RDS.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber and C.LastSoldReference = 'CONVERSI' WHERE T.Reason = ''
   Update #Temp1 SET  Reason = 'Coupons Dont Exist'  From #Temp1 T Left Join RDS.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber WHERE T.Reason = '' and  C.SerialNumber  is null 
   Update #Temp1 SET  Reason = 'Coupons Not Sold'   From #Temp1 T Left Join RDS.dbo.ProntoStockSerialLink C on T.SerialNumber = C.SerialNumber and c.SerialLinkType = 'S' WHERE T.Reason = '' and  C.SerialNumber  is null 
   
   Update #Temp1 SET  StartSerialNumber = C.StartSerialNumber  From #Temp1 T Join RDS.dbo.ProntoCouponDetails C on T.SerialNumber = C.SerialNumber 
   Update #Temp1 SET Branch = 'UNKNOWN' WHere Branch =''
   Update #Temp1 SET Reason = 'Unknown' WHere Reason =''
   

   IF @Branch = 'ALL'
      Select * from #Temp1 order by duration desc
 ELSE 
    Select  * from #Temp1 WHERE Branch = @Branch  order by duration desc
  
  
                                                                      
   SET NOCOUNT OFF;
  

END
GO
