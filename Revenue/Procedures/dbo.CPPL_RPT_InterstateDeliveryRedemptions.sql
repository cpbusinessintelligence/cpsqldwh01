SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[CPPL_RPT_InterstateDeliveryRedemptions]
(
	@Year		int,
	@Month		int,
	@BU  varchar(20)
)
AS
BEGIN
  SET NOCOUNT ON;

   Select CouponPrefix,
         CASE BranchCode WHEN 'CSY' THEN 'SYDNEY' WHEN 'CBN' THEN 'BRISBANE' WHEN 'COO' THEN 'GOLDCOAST' WHEN 'CME' THEN 'MELBOURNE' WHEN 'CAD' THEN 'ADELAIDE' ELSE '' END as Branch,
         ContractorCode,
         RedemptionAmount 
      Into #TempLookup
      from Pronto.dbo.ProntoStockChargesOverride where RedemptionType = 'D'  
          
       Select CouponPrefix,
         CASE BranchCode WHEN 'CSY' THEN 'SYDNEY' WHEN 'CBN' THEN 'BRISBANE' WHEN 'COO' THEN 'GOLDCOAST' WHEN 'CME' THEN 'MELBOURNE' WHEN 'CAD' THEN 'ADELAIDE' ELSE '' END as Branch,
         ContractorCode,
         RedemptionAmount 
      Into #TempPickUpCharges
      from Pronto.dbo.ProntoStockChargesOverride where RedemptionType = 'P'  
      
      
      
	Select LabelPrefix,
	       CouponType,
	       RevenueAmount,
	       IsNull(OtherScanContractorCode,'') as DestinationContractor,
	       OriginBranch,
	       IsNull(FirstScanContractorCode,'') As OriginCOntractor,
	       DestinationBranch,
	       CONVERT(decimal(12,2),0) as PickUpRedemption,
	       CONVERT(decimal(12,2),0) as DeliveryRedemption
	    into #TempFinal  
	     from  Revenue.dbo.v_PrepaidRevenueReporting 
	     Where NetworkCategory = 'I' 
	            
	           and (Datepart(year,RevenueRecognisedDate)*100) + Datepart(Month,RevenueRecognisedDate)  = (@Year*100) +    @Month
	           and BU= @BU
	           and CouponType not in ('LINK','ATL','REDELIVERY CARD','IRP TRK','RETURN TRK') 
	
   
  Update #TempFinal SET  DeliveryRedemption = TL.RedemptionAmount
         From #TempFinal TF Join #TempLookup  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.DestinationBranch = TL.Branch
         and TF.DestinationContractor = TL.ContractorCode
         and TF.DeliveryRedemption = 0
         
    Update #TempFinal SET  DeliveryRedemption = TL.RedemptionAmount
         From #TempFinal TF Join #TempLookup  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.DestinationBranch = TL.Branch
         and TF.DeliveryRedemption = 0
         
      Update #TempFinal SET  DeliveryRedemption = TL.RedemptionAmount
         From #TempFinal TF Join #TempLookup  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.DeliveryRedemption = 0
 
 
 --PickUp Redemption
   
  Update #TempFinal SET  PickUpRedemption = TL.RedemptionAmount
         From #TempFinal TF Join #TempPickUpCharges  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.OriginBranch = TL.Branch
         and TF.OriginCOntractor = TL.ContractorCode
         and TF.PickUpRedemption = 0
         
    Update #TempFinal SET  PickUpRedemption = TL.RedemptionAmount
         From #TempFinal TF Join #TempPickUpCharges  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.OriginBranch = TL.Branch
         and TF.PickUpRedemption = 0
         
      Update #TempFinal SET  PickUpRedemption = TL.RedemptionAmount
         From #TempFinal TF Join #TempPickUpCharges  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.PickUpRedemption = 0
  
  Select (@Year*100) + @Month As YearMonth , 
          @BU as BusinessUnit ,
          DestinationBranch,
             CouponType,
          COUNT(*) as CouponCount,
          SUM(RevenueAmount)as RevenueAmount,
          SUM(PickupRedemption) as PickUpRedemption,
          SUM(DeliveryRedemption) as DeliveryRedemption
             from #TempFinal 
             Group By   CouponType,DestinationBranch
             order by  CouponType asc,DestinationBranch
   
  
  
                                                                      
   SET NOCOUNT OFF;
  

END

GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_InterstateDeliveryRedemptions]
	TO [ReportUser]
GO
