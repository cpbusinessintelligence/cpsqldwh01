SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CPPL_RPT_TotCouponsPerTypePerMonth](@Year varchar(10),@Quarter varchar(10)) as
begin
select 
      (datepart(yyyy,RevenueRecognisedDate)*100)+ datepart(mm,RevenueRecognisedDate) as MonthKey,
       [dbo].[CPPL_fn_GetCouponTypeFromPrefix](LabelPrefix) as [Type],
       LabelPrefix,
       StkDescription as Description,
       count(*) as CouponCount 
into #temp from v_PrepaidRevenueReporting join Pronto.[dbo].[ProntoStockMaster] on stockcode=labelprefix
where year(RevenueRecogniseddate)=@Year and datepart(quarter,RevenueRecognisedDate)=@Quarter
group by  (datepart(yyyy,RevenueRecognisedDate)*100)+ datepart(mm,RevenueRecognisedDate),[dbo].[CPPL_fn_GetCouponTypeFromPrefix](LabelPrefix) ,LabelPrefix,StkDescription

select MonthKey,
       [Type],
       LabelPrefix,
	   Description,
	   sum(CouponCount) as CouponCount 
from #temp group by MonthKey,[Type],LabelPrefix,Description

end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_TotCouponsPerTypePerMonth]
	TO [ReportUser]
GO
