SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_UpdatePrepaidRevenueReportingWithConsignmentDetails]
AS
BEGIN

    ------------------------------------------
	-- get consigment information based on label number
	------------------------------------------
	UPDATE PrepaidRevenueReporting 
	      SET ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReporting prr (NOLOCK)
	                    JOIN cpplEDI.dbo.cdcoupon cc (NOLOCK) on cc_coupon=prr.labelnumber
                        JOIN cpplEDI.dbo.consignment con (NOLOCK) on cc_consignment=cd_id
                       -- JOIN cpplEDI.dbo.companyaccount ca (NOLOCK) on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu (NOLOCK) on c_id=cd_company_id
                        JOIN cpplEDI.dbo.branchs b (NOLOCK) on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1  (NOLOCK) on b1.b_id=con.cd_deliver_branch

	WHERE prr.ConsignmentDestinationState is null
	     AND prr.ConsignmentOriginState is null
	     AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
	     AND ISNULL(prr.IsProcessed, 0) = 0;

		 print '2 done'
	
    ------------------------------------------
	-- get consignment information based on label with leading zero
    ------------------------------------------
	UPDATE PrepaidRevenueReporting 
	      SET ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReporting prr (NOLOCK)
	                   JOIN cpplEDI.dbo.cdcoupon cc (NOLOCK) on cc_coupon=('0' + LTRIM(RTRIM(prr.LabelNumber)))
                        JOIN cpplEDI.dbo.consignment con (NOLOCK) on cc_consignment=cd_id
                     --   JOIN cpplEDI.dbo.companyaccount ca (NOLOCK) on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu  (NOLOCK) on c_id=cd_company_id
                        JOIN cpplEDI.dbo.branchs b (NOLOCK) on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=con.cd_deliver_branch
					--JOIN ODS.dbo.Label l (NOLOCK)	ON ('0' + LTRIM(RTRIM(prr.LabelNumber))) = l.LabelNumber 
					--JOIN ODS.dbo.ConsignmentItemLabel cil (NOLOCK)	ON cil.LabelId = l.Id 
					--JOIN ODS.dbo.ConsignmentItem ci (NOLOCK)	ON cil.ConsignmentItemId = ci.Id 
					--JOIN ODS.dbo.Consignment con (NOLOCK)	ON ci.ConsignmentId = con.Id 
					--JOIN ODS.dbo.Customer cu (NOLOCK)	ON con.CustomerId = cu.Id 
					--JOIN ODS.dbo.CustomerAddress oca (NOLOCK)	ON con.OriginCustomerAddressId = oca.Id 
					--JOIN ODS.dbo.[Address] oa (NOLOCK)	ON oca.AddressId = oa.Id 
					--JOIN ODS.dbo.Suburb osub (NOLOCK)	ON oa.SuburbId = osub.Id 
					--JOIN ODS.dbo.[State] ost (NOLOCK)	ON osub.StateId = ost.Id 
					--JOIN ODS.dbo.CustomerAddress dca (NOLOCK)	ON con.DestinationCustomerAddressId = dca.Id 
					--JOIN ODS.dbo.[Address] da (NOLOCK)	ON dca.AddressId = da.Id 
					--JOIN ODS.dbo.Suburb dsub (NOLOCK)	ON da.SuburbId = dsub.Id 
					--JOIN ODS.dbo.[State] dst (NOLOCK)	ON dsub.StateId = dst.Id 
	WHERE prr.ConsignmentDestinationState is null
		AND prr.ConsignmentOriginState is null
		AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
		AND ISNULL(prr.IsProcessed, 0) = 0;

		print '3 done'

    ---------------------------------------------------------------
	-- get consigment information based on matching consignment number
	--------------------------------------------------------------
	UPDATE PrepaidRevenueReporting 
			set ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReporting prr (NOLOCK)
	                   --JOIN cpplEDI.dbo.cdcoupon cc on cc_coupon=('0' + LTRIM(RTRIM(prr.LabelNumber)))
                        JOIN cpplEDI.dbo.consignment con (NOLOCK) on prr.LabelNumber=cd_connote
                      --  JOIN cpplEDI.dbo.companyaccount ca (NOLOCK) on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu (NOLOCK) on c_id=cd_company_id
                        JOIN cpplEDI.dbo.branchs b (NOLOCK) on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=con.cd_deliver_branch

	               
					--JOIN ODS.dbo.Consignment con (NOLOCK)	ON prr.LabelNumber = con.ConsignmentNumber  
					--JOIN ODS.dbo.Customer cu (NOLOCK)	ON con.CustomerId = cu.Id 
					--JOIN ODS.dbo.CustomerAddress oca (NOLOCK)	ON con.OriginCustomerAddressId = oca.Id 
					--JOIN ODS.dbo.[Address] oa (NOLOCK) ON oca.AddressId = oa.Id 
					--JOIN ODS.dbo.Suburb osub (NOLOCK)	ON oa.SuburbId = osub.Id 
					--JOIN ODS.dbo.[State] ost (NOLOCK)	ON osub.StateId = ost.Id 
					--JOIN ODS.dbo.CustomerAddress dca (NOLOCK)	ON con.DestinationCustomerAddressId = dca.Id 
					--JOIN ODS.dbo.[Address] da (NOLOCK)	ON dca.AddressId = da.Id 
					--JOIN ODS.dbo.Suburb dsub (NOLOCK)	ON da.SuburbId = dsub.Id 
					--JOIN ODS.dbo.[State] dst (NOLOCK)	ON dsub.StateId = dst.Id 
	WHERE prr.ConsignmentDestinationState is null
		AND prr.ConsignmentOriginState is null
		AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
		AND ISNULL(prr.IsProcessed, 0) = 0;

		print '4 done'

	--------------------------------------------------------------
	-- get consignment information based on matching consignment number with leading zero
	--------------------------------------------------------------
	UPDATE PrepaidRevenueReporting 
			SET ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReporting prr (NOLOCK)
	                JOIN cpplEDI.dbo.consignment con (NOLOCK) on ('0' + LTRIM(RTRIM(prr.LabelNumber)))=cd_connote
                       -- JOIN cpplEDI.dbo.companyaccount ca (NOLOCK) on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu (NOLOCK) on c_id=cd_company_id
                        JOIN cpplEDI.dbo.branchs b (NOLOCK) on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1  (NOLOCK) on b1.b_id=con.cd_deliver_branch

					--JOIN ODS.dbo.Consignment con (NOLOCK) ON ('0' + LTRIM(RTRIM(prr.LabelNumber))) = con.ConsignmentNumber 
					--JOIN ODS.dbo.Customer cu (NOLOCK)	ON con.CustomerId = cu.Id 
					--JOIN ODS.dbo.CustomerAddress oca (NOLOCK)	ON con.OriginCustomerAddressId = oca.Id 
					--JOIN ODS.dbo.[Address] oa (NOLOCK)	ON oca.AddressId = oa.Id 
					--JOIN ODS.dbo.Suburb osub (NOLOCK)	ON oa.SuburbId = osub.Id 
					--JOIN ODS.dbo.[State] ost (NOLOCK)	ON osub.StateId = ost.Id 
					--JOIN ODS.dbo.CustomerAddress dca (NOLOCK)	ON con.DestinationCustomerAddressId = dca.Id 
					--JOIN ODS.dbo.[Address] da (NOLOCK)	ON dca.AddressId = da.Id 
					--JOIN ODS.dbo.Suburb dsub (NOLOCK)	ON da.SuburbId = dsub.Id 
					--JOIN ODS.dbo.[State] dst (NOLOCK)	ON dsub.StateId = dst.Id 
	WHERE prr.ConsignmentDestinationState is null
	    AND prr.ConsignmentOriginState is null
	    AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
	    AND ISNULL(prr.IsProcessed, 0) = 0;

		print '5 done'


END
GO
