SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_UpdatePrepaidRevenueReportingWithConsignmentDetailsSTKVALUE]
AS
BEGIN

    ------------------------------------------
	-- get consigment information based on label number
	------------------------------------------
	UPDATE PrepaidRevenueReportingSTKVALUE 
	      	      SET ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReportingSTKVALUE prr (NOLOCK)
	                    JOIN cpplEDI.dbo.cdcoupon cc on cc_coupon=prr.labelnumber
                        JOIN cpplEDI.dbo.consignment con on cc_consignment=cd_id
                        JOIN cpplEDI.dbo.companyaccount ca on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu on c_id=ca_company_id
                        JOIN cpplEDI.dbo.branchs b on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1 on b1.b_id=con.cd_deliver_branch

	WHERE prr.ConsignmentDestinationState is null
	     AND prr.ConsignmentOriginState is null
	     AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
	     AND ISNULL(prr.IsProcessed, 0) = 0;
    ------------------------------------------
	-- get consignment information based on label with leading zero
    ------------------------------------------
	UPDATE PrepaidRevenueReportingSTKVALUE 
	     SET ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReportingSTKVALUE prr (NOLOCK)
	                   JOIN cpplEDI.dbo.cdcoupon cc (NOLOCK) on cc_coupon=('0' + LTRIM(RTRIM(prr.LabelNumber)))
                        JOIN cpplEDI.dbo.consignment con (NOLOCK) on cc_consignment=cd_id
                        JOIN cpplEDI.dbo.companyaccount ca (NOLOCK) on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu  (NOLOCK) on c_id=ca_company_id
                        JOIN cpplEDI.dbo.branchs b (NOLOCK) on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=con.cd_deliver_branch
			
	WHERE prr.ConsignmentDestinationState is null
		AND prr.ConsignmentOriginState is null
		AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
		AND ISNULL(prr.IsProcessed, 0) = 0;



    ---------------------------------------------------------------
	-- get consigment information based on matching consignment number
	--------------------------------------------------------------
	UPDATE PrepaidRevenueReportingSTKVALUE 
			set ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReportingSTKVALUE prr (NOLOCK)
	                   --JOIN cpplEDI.dbo.cdcoupon cc on cc_coupon=('0' + LTRIM(RTRIM(prr.LabelNumber)))
                        JOIN cpplEDI.dbo.consignment con (NOLOCK) on prr.LabelNumber=cd_connote
                        JOIN cpplEDI.dbo.companyaccount ca (NOLOCK) on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu (NOLOCK) on c_id=ca_company_id
                        JOIN cpplEDI.dbo.branchs b (NOLOCK) on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=con.cd_deliver_branch
	WHERE prr.ConsignmentDestinationState is null
		AND prr.ConsignmentOriginState is null
		AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
		AND ISNULL(prr.IsProcessed, 0) = 0;

	--------------------------------------------------------------
	-- get consignment information based on matching consignment number with leading zero
	--------------------------------------------------------------
	UPDATE PrepaidRevenueReportingSTKVALUE 
		SET ConsignmentNumber = con.cd_connote,
	          ConsignmentCustomer = cu.c_Name,
	          ConsignmentOriginState = case when b.b_name = 'Adelaide' then 'SA'
			                                when b.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b.b_name = 'Melbourne' then 'VIC'
											when b.b_name = 'Sydney' then 'NSW'
											when b.b_name = 'Perth' then 'WA'
											when b.b_name = 'Sydney Centre Inbound' then 'SA'
											when b.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end,
											--when b_name = 'Adelaide' then 'SA'

	          ConsignmentDestinationState = case when b1.b_name = 'Adelaide' then 'SA'
			                                when b1.b_name in ('Brisbane','Gold Coast','GoldCoast') then 'QLD'
											when b1.b_name = 'Melbourne' then 'VIC'
											when b1.b_name = 'Sydney' then 'NSW'
											when b1.b_name = 'Perth' then 'WA'
											when b1.b_name = 'Sydney Centre Inbound' then 'SA'
											when b1.b_name = 'Canberra' then 'ACT'
											else 'Unknown' end
	FROM PrepaidRevenueReportingSTKVALUE prr (NOLOCK)
	                JOIN cpplEDI.dbo.consignment con (NOLOCK) on ('0' + LTRIM(RTRIM(prr.LabelNumber)))=cd_connote
                        JOIN cpplEDI.dbo.companyaccount ca (NOLOCK) on ca.ca_account=cd_account
                        JOIN cpplEDI.dbo.companies cu (NOLOCK) on c_id=ca_company_id
                        JOIN cpplEDI.dbo.branchs b (NOLOCK) on b.b_id=con.cd_pickup_branch
                        JOIN cpplEDI.dbo.branchs b1  (NOLOCK) on b1.b_id=con.cd_deliver_branch
	WHERE prr.ConsignmentDestinationState is null
	    AND prr.ConsignmentOriginState is null
	    AND ((cu.c_Name like 'cppl%') OR (cu.c_Name like 'couriers%pleas%'))
	    AND ISNULL(prr.IsProcessed, 0) = 0;


END
GO
