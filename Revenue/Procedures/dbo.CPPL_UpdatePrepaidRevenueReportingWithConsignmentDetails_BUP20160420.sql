SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create PROC [dbo].[CPPL_UpdatePrepaidRevenueReportingWithConsignmentDetails_BUP20160420]
AS
BEGIN

    ------------------------------------------
	-- get consigment information based on label number
	------------------------------------------
	UPDATE PrepaidRevenueReporting 
	      SET ConsignmentNumber = con.ConsignmentNumber,
	          ConsignmentCustomer = cu.Name,
	          ConsignmentOriginState = ost.Code,
	          ConsignmentDestinationState = dst.Code
	FROM PrepaidRevenueReporting prr (NOLOCK)
	            	JOIN ODS.dbo.Label l (NOLOCK)     ON prr.LabelNumber = l.LabelNumber 
	                JOIN ODS.dbo.ConsignmentItemLabel cil (NOLOCK)	ON cil.LabelId = l.Id 
					JOIN ODS.dbo.ConsignmentItem ci (NOLOCK) ON cil.ConsignmentItemId = ci.Id 
					JOIN ODS.dbo.Consignment con (NOLOCK)	ON ci.ConsignmentId = con.Id 
					JOIN ODS.dbo.Customer cu (NOLOCK)	ON con.CustomerId = cu.Id 
					JOIN ODS.dbo.CustomerAddress oca (NOLOCK)	ON con.OriginCustomerAddressId = oca.Id 
					JOIN ODS.dbo.[Address] oa (NOLOCK)	ON oca.AddressId = oa.Id 
					JOIN ODS.dbo.Suburb osub (NOLOCK)	ON oa.SuburbId = osub.Id 
					JOIN ODS.dbo.[State] ost (NOLOCK)	ON osub.StateId = ost.Id 
					JOIN ODS.dbo.CustomerAddress dca (NOLOCK)	ON con.DestinationCustomerAddressId = dca.Id 
					JOIN ODS.dbo.[Address] da (NOLOCK)	ON dca.AddressId = da.Id 
					JOIN ODS.dbo.Suburb dsub (NOLOCK)	ON da.SuburbId = dsub.Id 
					JOIN ODS.dbo.[State] dst (NOLOCK)	ON dsub.StateId = dst.Id 
	WHERE prr.ConsignmentDestinationState is null
	     AND prr.ConsignmentOriginState is null
	     AND ((cu.Name like 'cppl%') OR (cu.Name like 'couriers%pleas%'))
	     AND ISNULL(prr.IsProcessed, 0) = 0;

		 print '2 done'
	
    ------------------------------------------
	-- get consignment information based on label with leading zero
    ------------------------------------------
	UPDATE PrepaidRevenueReporting 
	      SET ConsignmentNumber = con.ConsignmentNumber,
			  ConsignmentCustomer = cu.Name,
			  ConsignmentOriginState = ost.Code,
			  ConsignmentDestinationState = dst.Code
	FROM PrepaidRevenueReporting prr (NOLOCK)
					JOIN ODS.dbo.Label l (NOLOCK)	ON ('0' + LTRIM(RTRIM(prr.LabelNumber))) = l.LabelNumber 
					JOIN ODS.dbo.ConsignmentItemLabel cil (NOLOCK)	ON cil.LabelId = l.Id 
					JOIN ODS.dbo.ConsignmentItem ci (NOLOCK)	ON cil.ConsignmentItemId = ci.Id 
					JOIN ODS.dbo.Consignment con (NOLOCK)	ON ci.ConsignmentId = con.Id 
					JOIN ODS.dbo.Customer cu (NOLOCK)	ON con.CustomerId = cu.Id 
					JOIN ODS.dbo.CustomerAddress oca (NOLOCK)	ON con.OriginCustomerAddressId = oca.Id 
					JOIN ODS.dbo.[Address] oa (NOLOCK)	ON oca.AddressId = oa.Id 
					JOIN ODS.dbo.Suburb osub (NOLOCK)	ON oa.SuburbId = osub.Id 
					JOIN ODS.dbo.[State] ost (NOLOCK)	ON osub.StateId = ost.Id 
					JOIN ODS.dbo.CustomerAddress dca (NOLOCK)	ON con.DestinationCustomerAddressId = dca.Id 
					JOIN ODS.dbo.[Address] da (NOLOCK)	ON dca.AddressId = da.Id 
					JOIN ODS.dbo.Suburb dsub (NOLOCK)	ON da.SuburbId = dsub.Id 
					JOIN ODS.dbo.[State] dst (NOLOCK)	ON dsub.StateId = dst.Id 
	WHERE prr.ConsignmentDestinationState is null
		AND prr.ConsignmentOriginState is null
		AND ((cu.Name like 'cppl%') OR (cu.Name like 'couriers%pleas%'))
		AND ISNULL(prr.IsProcessed, 0) = 0;

		print '3 done'

    ---------------------------------------------------------------
	-- get consigment information based on matching consignment number
	--------------------------------------------------------------
	UPDATE PrepaidRevenueReporting 
			SET ConsignmentNumber = con.ConsignmentNumber,
				ConsignmentCustomer = cu.Name,
				ConsignmentOriginState = ost.Code,
				ConsignmentDestinationState = dst.Code
	FROM PrepaidRevenueReporting prr (NOLOCK)
					JOIN ODS.dbo.Consignment con (NOLOCK)	ON prr.LabelNumber = con.ConsignmentNumber  
					JOIN ODS.dbo.Customer cu (NOLOCK)	ON con.CustomerId = cu.Id 
					JOIN ODS.dbo.CustomerAddress oca (NOLOCK)	ON con.OriginCustomerAddressId = oca.Id 
					JOIN ODS.dbo.[Address] oa (NOLOCK) ON oca.AddressId = oa.Id 
					JOIN ODS.dbo.Suburb osub (NOLOCK)	ON oa.SuburbId = osub.Id 
					JOIN ODS.dbo.[State] ost (NOLOCK)	ON osub.StateId = ost.Id 
					JOIN ODS.dbo.CustomerAddress dca (NOLOCK)	ON con.DestinationCustomerAddressId = dca.Id 
					JOIN ODS.dbo.[Address] da (NOLOCK)	ON dca.AddressId = da.Id 
					JOIN ODS.dbo.Suburb dsub (NOLOCK)	ON da.SuburbId = dsub.Id 
					JOIN ODS.dbo.[State] dst (NOLOCK)	ON dsub.StateId = dst.Id 
	WHERE prr.ConsignmentDestinationState is null
		AND prr.ConsignmentOriginState is null
		AND ((cu.Name like 'cppl%') OR (cu.Name like 'couriers%pleas%'))
		AND ISNULL(prr.IsProcessed, 0) = 0;

		print '4 done'

	--------------------------------------------------------------
	-- get consignment information based on matching consignment number with leading zero
	--------------------------------------------------------------
	UPDATE PrepaidRevenueReporting 
			SET ConsignmentNumber = con.ConsignmentNumber,
				ConsignmentCustomer = cu.Name,
				ConsignmentOriginState = ost.Code,
				ConsignmentDestinationState = dst.Code
	FROM PrepaidRevenueReporting prr (NOLOCK)
					JOIN ODS.dbo.Consignment con (NOLOCK) ON ('0' + LTRIM(RTRIM(prr.LabelNumber))) = con.ConsignmentNumber 
					JOIN ODS.dbo.Customer cu (NOLOCK)	ON con.CustomerId = cu.Id 
					JOIN ODS.dbo.CustomerAddress oca (NOLOCK)	ON con.OriginCustomerAddressId = oca.Id 
					JOIN ODS.dbo.[Address] oa (NOLOCK)	ON oca.AddressId = oa.Id 
					JOIN ODS.dbo.Suburb osub (NOLOCK)	ON oa.SuburbId = osub.Id 
					JOIN ODS.dbo.[State] ost (NOLOCK)	ON osub.StateId = ost.Id 
					JOIN ODS.dbo.CustomerAddress dca (NOLOCK)	ON con.DestinationCustomerAddressId = dca.Id 
					JOIN ODS.dbo.[Address] da (NOLOCK)	ON dca.AddressId = da.Id 
					JOIN ODS.dbo.Suburb dsub (NOLOCK)	ON da.SuburbId = dsub.Id 
					JOIN ODS.dbo.[State] dst (NOLOCK)	ON dsub.StateId = dst.Id 
	WHERE prr.ConsignmentDestinationState is null
	    AND prr.ConsignmentOriginState is null
	    AND ((cu.Name like 'cppl%') OR (cu.Name like 'couriers%pleas%'))
	    AND ISNULL(prr.IsProcessed, 0) = 0;

		print '5 done'


END
GO
