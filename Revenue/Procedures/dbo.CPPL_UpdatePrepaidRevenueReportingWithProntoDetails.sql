SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_UpdatePrepaidRevenueReportingWithProntoDetails]
AS
BEGIN

	UPDATE PrepaidRevenueReporting 
	     SET RevenueAmount = CASE WHEN ISNULL(sm.StkConversionFactor, 0.00) > 0.00
								     THEN ((ISNULL(ol.GrossAmount, 0.00) - ISNULL(cd.InsuranceAmount, 0.00))/ sm.StkConversionFactor)
		                          ELSE (ISNULL(ol.GrossAmount, 0.00) - ISNULL(cd.InsuranceAmount, 0.00))
	                          END,
			 InsuranceAmount = ISNULL(cd.InsuranceAmount, 0.00),
			 InsuranceCategory = ISNULL(cd.InsuranceCategory, ''),
			 AccountCode = so.CustomerCode,
			 AccountName = d.Shortname,
			 ProntoOrderNumber = so.OrderNumber,
			 ProntoOrderLine = ol.LineSequence,
			 ProntoOrderDate = so.AccountingDate,
			 ProntoOrderContractorCode = ISNULL(so.WarehouseCode, '')
	FROM PrepaidRevenueReporting prr (NOLOCK)
    	       JOIN Pronto.dbo.ProntoStockSerialLink sl (NOLOCK)   ON prr.LabelNumber = sl.SerialNumber 
	           JOIN Pronto.dbo.ProntoSalesOrderLines ol (NOLOCK)	ON sl.SerialLinkCode = ol.OrderNumber 
	                                                             	AND sl.SerialLinkSeqNo = ol.LineSequence 
																	AND ol.CompanyCode = 'CL1'
																	AND sl.SerialLinkType = 'S'
			   JOIN Pronto.dbo.ProntoSalesOrder so (NOLOCK)    	ON so.OrderNumber = ol.OrderNumber 
																	AND so.CompanyCode = ol.CompanyCode 
			   JOIN Pronto.dbo.ProntoStockMaster sm (NOLOCK)   	ON sm.StockCode = ol.StockCode 
			   JOIN Pronto.dbo.ProntoCouponDetails cd (NOLOCK)  	ON cd.SerialNumber = prr.LabelNumber 
			   JOIN Pronto.dbo.ProntoDebtor d (NOLOCK)         	ON d.Accountcode = so.CustomerCode 
	WHERE prr.RevenueAmount Is Null;

	print '6 done'

	/* update from Coupon Details table, if other table records missing */
	--------------------------------------------------
	-- only for records older than 2 weeks
	--------------------------------------------------
	UPDATE PrepaidRevenueReporting 
	SET RevenueAmount = cd.RevenueAmount,
	InsuranceAmount = ISNULL(cd.InsuranceAmount, 0.00),
	InsuranceCategory = ISNULL(cd.InsuranceCategory, ''),
	ProntoOrderNumber = cd.LastSoldReference,
	ProntoOrderLine = 0,
	ProntoOrderDate = cd.LastSoldDate
	FROM PrepaidRevenueReporting prr (NOLOCK)
	JOIN Pronto.dbo.ProntoCouponDetails cd (NOLOCK)
		ON cd.SerialNumber = prr.LabelNumber 
	WHERE prr.RevenueAmount Is Null AND cd.RevenueAmount > 0.00 and cd.LastSoldReference = 'CONVERSI'
	--AND prr.CreatedDate < DATEADD(day, -14, GETDATE());


   UPDATE PrepaidRevenueReporting 
    	SET RevenueAmount = 0,
	        InsuranceAmount = 0
	 Where IsProcessed = 0 
	     and RevenueAmount is null 
	     and (LabelPrefix  = '183' 
                   OR LabelPrefix = '190'
                   OR LabelPrefix = '191'
                   OR LabelPrefix LIKE '[23456789]14'
                   OR LabelPrefix LIKE '[23456789]23'
                   OR LabelPrefix LIKE '[23456789]38')
                   
         print '7 done'          
   ----------------------------------
   --Any coupons that dont have a revenue amount coming from Pornto will have to be moved to an exceptions table
   -----------------------------------                

   Delete from PrepaidRevenueReporting 
        OUTPUT DELETED.* 
        INTO dbo.PrepaidRevenueReportingExceptions
        Where  RevenueAmount is null

		print '8 done'
----------------------------------
   --Look to see if the revenue issue has been fixed for exceptions table in Pronto
   -----------------------------------                

	UPDATE PrepaidRevenueReportingExceptions 
	     SET RevenueAmount = CASE WHEN ISNULL(sm.StkConversionFactor, 0.00) > 0.00
								     THEN ((ISNULL(ol.GrossAmount, 0.00) - ISNULL(cd.InsuranceAmount, 0.00))/ sm.StkConversionFactor)
		                          ELSE (ISNULL(ol.GrossAmount, 0.00) - ISNULL(cd.InsuranceAmount, 0.00))
	                          END,
			 InsuranceAmount = ISNULL(cd.InsuranceAmount, 0.00),
			 InsuranceCategory = ISNULL(cd.InsuranceCategory, ''),
			 AccountCode = so.CustomerCode,
			 AccountName = d.Shortname,
			 ProntoOrderNumber = so.OrderNumber,
			 ProntoOrderLine = ol.LineSequence,
			 ProntoOrderDate = so.AccountingDate,
			 ProntoOrderContractorCode = ISNULL(so.WarehouseCode, '')
	FROM PrepaidRevenueReportingExceptions prr (NOLOCK)
    	       JOIN Pronto.dbo.ProntoStockSerialLink sl (NOLOCK)   ON prr.LabelNumber = sl.SerialNumber 
	           JOIN Pronto.dbo.ProntoSalesOrderLines ol (NOLOCK)	ON sl.SerialLinkCode = ol.OrderNumber 
	                                                             	AND sl.SerialLinkSeqNo = ol.LineSequence 
																	AND ol.CompanyCode = 'CL1'
																	AND sl.SerialLinkType = 'S'
			   JOIN Pronto.dbo.ProntoSalesOrder so (NOLOCK)    	ON so.OrderNumber = ol.OrderNumber 
																	AND so.CompanyCode = ol.CompanyCode 
			   JOIN Pronto.dbo.ProntoStockMaster sm (NOLOCK)   	ON sm.StockCode = ol.StockCode 
			   JOIN Pronto.dbo.ProntoCouponDetails cd (NOLOCK)  	ON cd.SerialNumber = prr.LabelNumber 
			   JOIN Pronto.dbo.ProntoDebtor d (NOLOCK)         	ON d.Accountcode = so.CustomerCode 
	WHERE prr.RevenueAmount Is Null;

	print '9 done'


END
GO
