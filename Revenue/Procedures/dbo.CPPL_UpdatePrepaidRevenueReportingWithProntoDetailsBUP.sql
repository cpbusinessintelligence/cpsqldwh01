SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CPPL_UpdatePrepaidRevenueReportingWithProntoDetailsBUP]
AS
BEGIN

	UPDATE PrepaidRevenueReporting 
	     SET RevenueAmount = cd.RevenueAmount,
			 InsuranceAmount = ISNULL(cd.InsuranceAmount, 0.00),
			 InsuranceCategory = ISNULL(cd.InsuranceCategory, ''),
			 AccountCode = so.CustomerCode,
			 AccountName = d.Shortname,
			 ProntoOrderNumber = so.OrderNumber,
			 --ProntoOrderLine = ol.LineSequence,
			 ProntoOrderDate = so.AccountingDate,
			 ProntoOrderContractorCode = ISNULL(so.WarehouseCode, '')
	FROM PrepaidRevenueReporting prr (NOLOCK)
    	        JOIN RDS.dbo.ProntoCouponDetails cd (NOLOCK)  	ON cd.SerialNumber = prr.LabelNumber        
			   JOIN RDS.dbo.ProntoSalesOrder so (NOLOCK)    	ON so.OrderNumber = cd.Lastsoldreference
																	AND so.CompanyCode = 'CL1'
			   JOIN RDS.dbo.ProntoStockMaster sm (NOLOCK)   	ON sm.StockCode = prr.LabelPrefix
			  
			   JOIN RDS.dbo.ProntoDebtor d (NOLOCK)         	ON d.Accountcode = so.CustomerCode 
	WHERE prr.RevenueAmount Is Null;

	/* update from Coupon Details table, if other table records missing */
	--------------------------------------------------
	-- only for records older than 2 weeks
	--------------------------------------------------
	UPDATE PrepaidRevenueReporting 
	SET RevenueAmount = cd.RevenueAmount,
	InsuranceAmount = ISNULL(cd.InsuranceAmount, 0.00),
	InsuranceCategory = ISNULL(cd.InsuranceCategory, ''),
	ProntoOrderNumber = cd.LastSoldReference,
	ProntoOrderLine = 0,
	ProntoOrderDate = cd.LastSoldDate
	FROM PrepaidRevenueReporting prr (NOLOCK)
	JOIN RDS.dbo.ProntoCouponDetails cd (NOLOCK)
		ON cd.SerialNumber = prr.LabelNumber 
	WHERE prr.RevenueAmount Is Null AND cd.RevenueAmount > 0.00 and cd.LastSoldReference = 'CONVERSI'
	--AND prr.CreatedDate < DATEADD(day, -14, GETDATE());


   UPDATE PrepaidRevenueReporting 
    	SET RevenueAmount = 0,
	        InsuranceAmount = 0
	 Where IsProcessed = 0 
	     and RevenueAmount is null 
	     and (LabelPrefix  = '183' 
                   OR LabelPrefix = '190'
                   OR LabelPrefix = '191'
                   OR LabelPrefix LIKE '[23456789]14'
                   OR LabelPrefix LIKE '[23456789]23'
                   OR LabelPrefix LIKE '[23456789]38')
                   
                   
   ----------------------------------
   --Any coupons that dont have a revenue amount coming from Pornto will have to be moved to an exceptions table
   -----------------------------------                

   Delete from PrepaidRevenueReporting 
        OUTPUT DELETED.* 
        INTO dbo.PrepaidRevenueReportingExceptions
        Where  RevenueAmount is null
----------------------------------
   --Look to see if the revenue issue has been fixed for exceptions table in Pronto
   -----------------------------------                

	UPDATE PrepaidRevenueReportingExceptions 
	     SET RevenueAmount = CD.revenueamount,
			 InsuranceAmount = ISNULL(cd.InsuranceAmount, 0.00),
			 InsuranceCategory = ISNULL(cd.InsuranceCategory, ''),
			 AccountCode = so.CustomerCode,
			 AccountName = d.Shortname,
			 ProntoOrderNumber = so.OrderNumber,
			 ProntoOrderLine = ol.LineSequence,
			 ProntoOrderDate = so.AccountingDate,
			 ProntoOrderContractorCode = ISNULL(so.WarehouseCode, '')
	FROM PrepaidRevenueReportingExceptions prr (NOLOCK)
    	       JOIN RDS.dbo.ProntoStockSerialLink sl (NOLOCK)   ON prr.LabelNumber = sl.SerialNumber 
	           JOIN RDS.dbo.ProntoSalesOrderLines ol (NOLOCK)	ON sl.SerialLinkCode = ol.OrderNumber 
	                                                             	AND sl.SerialLinkSeqNo = ol.LineSequence 
																	AND ol.CompanyCode = 'CL1'
																	AND sl.SerialLinkType = 'S'
			   JOIN RDS.dbo.ProntoSalesOrder so (NOLOCK)    	ON so.OrderNumber = ol.OrderNumber 
																	AND so.CompanyCode = ol.CompanyCode 
			   JOIN RDS.dbo.ProntoStockMaster sm (NOLOCK)   	ON sm.StockCode = ol.StockCode 
			   JOIN RDS.dbo.ProntoCouponDetails cd (NOLOCK)  	ON cd.SerialNumber = prr.LabelNumber 
			   JOIN RDS.dbo.ProntoDebtor d (NOLOCK)         	ON d.Accountcode = so.CustomerCode 
	WHERE prr.RevenueAmount Is Null;


END
GO
