SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CPPL_WriteErrorLog]
(
	@ErrorMessage		nvarchar(4000),
	@ErrorNumber		int = 0,
	@ErrorProcedure		nvarchar(128) = Null,
	@ErrorSeverity		int = 0,
	@ErrorState			int = 0,
	@ErrorLine			int = Null
)
AS
BEGIN

  		INSERT INTO [InterfaceErrorLog]
  		WITH (TABLOCK)
			   ([ErrorNumber]
			   ,[ErrorSeverity]
			   ,[ErrorState]
			   ,[ErrorProcedure]
			   ,[ErrorLine]
			   ,[ErrorMessage]
			   , [CreateDate]) 
		VALUES
		(
			@ErrorNumber ,
			@ErrorSeverity ,
			@ErrorState ,
			@ErrorProcedure ,
			@ErrorLine ,
			@ErrorMessage ,
			GETDATE()
		)

END

GO
