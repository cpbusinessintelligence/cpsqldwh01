SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[CPPL_deleteallIntRechargeTables](@Monthkey int) as
begin


IF EXISTS(Select * from [IntRecharge_PrepaidDetails] where monthkey=@MonthKey)
delete from [IntRecharge_PrepaidDetails] where monthkey=@MonthKey;

IF EXISTS(Select * from [IntRecharge_PrepaidSummary] where monthkey=@MonthKey)
delete from [IntRecharge_PrepaidSummary] where monthkey=@MonthKey;

IF EXISTS(Select * from [dbo].[IntRecharge_EDISummary] where monthkey=@MonthKey)
delete from [dbo].[IntRecharge_EDISummary] where monthkey=@MonthKey;

IF EXISTS(Select * from [dbo].[IntRecharge_EDIDetail] where monthkey=@MonthKey)
delete from [dbo].[IntRecharge_EDIDetail] where monthkey=@MonthKey;

IF EXISTS(Select * from [IntRechargeMonthlyTotals] where monthkey= @Monthkey)
delete from [IntRechargeMonthlyTotals] where monthkey=@Monthkey  ;

end
GO
