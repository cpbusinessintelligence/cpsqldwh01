SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[JP_ArchiveProceedure] (@ArchiveTrackingEventDate DateTime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


---- 6 weeks for orders that have been picked
--	Declare @ArchiveTrackingEventDate datetime
----	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
--    Select @ArchiveTrackingEventDate = '2013-03-28 23:59:59.999'
	
  
-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION PrepaidRevenueReporting
INSERT INTO [Revenue].[dbo].[PrepaidRevenueReporting_2015_2016_2017]
           ([LabelNumber]
           ,[LinkLabelNumber]
           ,[LabelPrefix]
           ,[ServiceCategory]
           ,[CouponType]
           ,[RevenueRecognisedDate]
           ,[RevenueProcessedDateTime]
           ,[IsFirstScanProcessed]
           ,[IsProcessed]
           ,[RevenueAmount]
           ,[InsuranceAmount]
           ,[InsuranceCategory]
           ,[FirstScanDateTime]
           ,[FirstScanDriverBranch]
           ,[FirstScanDriverNumber]
           ,[FirstScanContractorCode]
           ,[FirstScanDepot]
           ,[FirstScanType]
           ,[OtherScanDateTime]
           ,[OtherScanDriverBranch]
           ,[OtherScanDriverNumber]
           ,[OtherScanContractorCode]
           ,[OtherScanDepot]
           ,[OtherScanType]
           ,[NetworkCategory]
           ,[BUCode]
           ,[BU]
           ,[AccountCode]
           ,[AccountName]
           ,[ConsignmentNumber]
           ,[ConsignmentOriginState]
           ,[ConsignmentDestinationState]
           ,[ProntoOrderNumber]
           ,[ProntoOrderLine]
           ,[ProntoOrderDate]
           ,[ProntoOrderContractorCode]
           ,[AnalysisDataExtracted]
           ,[AnalysisDataExtractedDateTime]
           ,[CreatedDate]
           ,[ConsignmentCustomer]
           ,[ConsignmentDeliveryDate]
           ,[OriginBranch]
           ,[OriginDepot]
           ,[DestinationBranch]
           ,[DestinationDepot]
           ,[ConsolidateScanDateTime]
           ,[ConsolidateBarcode])
   SELECT [LabelNumber]
      ,[LinkLabelNumber]
      ,[LabelPrefix]
      ,[ServiceCategory]
      ,[CouponType]
      ,[RevenueRecognisedDate]
      ,[RevenueProcessedDateTime]
      ,[IsFirstScanProcessed]
      ,[IsProcessed]
      ,[RevenueAmount]
      ,[InsuranceAmount]
      ,[InsuranceCategory]
      ,[FirstScanDateTime]
      ,[FirstScanDriverBranch]
      ,[FirstScanDriverNumber]
      ,[FirstScanContractorCode]
      ,[FirstScanDepot]
      ,[FirstScanType]
      ,[OtherScanDateTime]
      ,[OtherScanDriverBranch]
      ,[OtherScanDriverNumber]
      ,[OtherScanContractorCode]
      ,[OtherScanDepot]
      ,[OtherScanType]
      ,[NetworkCategory]
      ,[BUCode]
      ,[BU]
      ,[AccountCode]
      ,[AccountName]
      ,[ConsignmentNumber]
      ,[ConsignmentOriginState]
      ,[ConsignmentDestinationState]
      ,[ProntoOrderNumber]
      ,[ProntoOrderLine]
      ,[ProntoOrderDate]
      ,[ProntoOrderContractorCode]
      ,[AnalysisDataExtracted]
      ,[AnalysisDataExtractedDateTime]
      ,[CreatedDate]
      ,[ConsignmentCustomer]
      ,[ConsignmentDeliveryDate]
      ,[OriginBranch]
      ,[OriginDepot]
      ,[DestinationBranch]
      ,[DestinationDepot]
      ,[ConsolidateScanDateTime]
      ,[ConsolidateBarcode]
  FROM [Revenue].[dbo].[PrepaidRevenueReporting]
  Where [RevenueRecognisedDate] <=@ArchiveTrackingEventDate
	
	Delete FROM [Revenue].[dbo].[PrepaidRevenueReporting]
          Where [RevenueRecognisedDate] <=@ArchiveTrackingEventDate


	COMMIT TRANSACTION PrepaidRevenueReporting

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------

GO
