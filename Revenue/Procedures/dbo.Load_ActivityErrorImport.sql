SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Load_ActivityErrorImport]
AS

BEGIN

 Declare @Date as Date 
 Select @Date = CONVERT(Date,Getdate())
 
 INSERT INTO [Revenue].[dbo].[ActivityErrorImportHistory]
           ([ActivityDateTime]
           ,[SerialNumber]
           ,[ActivityFlag]
           ,[ActivityFileOn])
      SELECT  [ActivityDateTime]
            ,[SerialNumber]
            ,[ActivityFlag]
            ,[ActivityFileOn]
      FROM [Revenue].[dbo].[ActivityErrorImport]
 Truncate table [ActivityErrorImport]
 
 UPDATE dbo.Load_ActivityErrors SET SerialNumber = Rtrim(Ltrim(REPLACE(SerialNumber,'"','')))
 UPDATE dbo.Load_ActivityErrors SET ActivityFlag = Rtrim(Ltrim(REPLACE(ActivityFlag,'"','')))
 
 INSERT INTO Revenue.[dbo].[ActivityErrorImport]
           ([ActivityDateTime]
           ,[SerialNumber]
           ,[ActivityFlag]
           ,[ActivityFileOn])
           
        SELECT  Convert(DateTime,[ActivityDateTime])
      ,[SerialNumber]
      ,[ActivityFlag]
      ,@Date
  FROM Revenue.[dbo].[Load_ActivityErrors]
  
  Truncate table Revenue.[dbo].[Load_ActivityErrors]
    Select 1
END


GO
GRANT EXECUTE
	ON [dbo].[Load_ActivityErrorImport]
	TO [SSISUser]
GO
