SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_RevenueDatabaseMaintenance_Updated]

@StartDate datetime = null --'2015-05-28'

As 
Begin
Begin Try
BEGIN TRAN 

PRINT '-------- START DISABLING TABLE CONSTRAINT --------';
		ALTER TABLE   [dbo].[_Activity] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[_ActivityError] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[_ActivityImport] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[ActivityErrorImport] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[ActivityErrorImportHistory] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[AgentDataDecember2013] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[ConNote] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[EdiRevenueReporting] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[gl_Analysis] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[InterfaceConfig] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[InterfaceErrorLog] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_Agents] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_Agents_old] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_BudgetPricing] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_EDIDetail] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_EDISummary] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_PrepaidDetails] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_PrepaidSummary] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRechargeMonthlyTotals] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IRPandOneoff] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Load_ActivityErrors] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[name_data] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimAccount] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimAccount_temp] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimBusinessUnit] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCalendarDay] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCalendarMonth] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCalendarYear] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCouponType] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimDepot] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimFinancialPeriod] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimFinancialWeek] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimFinancialYear] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimInsuranceCategory] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimNetworkCategory] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimNetworkType] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimRevenueType] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimServiceCategory] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimState] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimZone] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactEdiRevenueReporting] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactPrepaidRevenueReporting] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactPrepaidRevenueReporting2] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactPrepaidRevenueReportingSTKVALUE] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Prepaid_RedeemedAudit] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Prepaid_RedeemedAudit_Temp] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Prepaid_RedeemedAudit1] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueLinkLabels] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueRecognisedExceptions] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2011] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2012] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2013] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2015_2016_2017] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup1201710] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170919] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170919_1] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170920] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170921] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170921_1] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20171003] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20171004] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20171004_1] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup4201709] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReportingCURRENT] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReportingExceptions] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup1201710] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919_1] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170920] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921_1] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171003] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004_1] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup4201709] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReportingSTKVALUE] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Primary Coupons List] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Recharge_Pricing] NOCHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Temp_PrepaidRevNov15] NOCHECK  CONSTRAINT ALL

PRINT '-------- END DISABLING TABLE CONSTRAINT --------';

PRINT '-------- START COPYING DATA--------';
				------------Add Table Address Record In Archive-----------------------


PRINT  '------------------------COPY  [dbo].[_Activity]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[_Activity_Archive]  from  [dbo].[_Activity]
PRINT  '------------------------COPY  [dbo].[_ActivityError]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[_ActivityError_Archive]  from  [dbo].[_ActivityError]
PRINT  '------------------------COPY  [dbo].[_ActivityImport]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[_ActivityImport_Archive]  from  [dbo].[_ActivityImport]
PRINT  '------------------------COPY  [dbo].[ActivityErrorImport]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[ActivityErrorImport_Archive]  from  [dbo].[ActivityErrorImport]
PRINT  '------------------------COPY  [dbo].[ActivityErrorImportHistory]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[ActivityErrorImportHistory_Archive]  from  [dbo].[ActivityErrorImportHistory]
PRINT  '------------------------COPY  [dbo].[AgentDataDecember2013]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[AgentDataDecember2013_Archive]  from  [dbo].[AgentDataDecember2013]
PRINT  '------------------------COPY  [dbo].[ConNote]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[ConNote_Archive]  from  [dbo].[ConNote]
PRINT  '------------------------COPY  [dbo].[EdiRevenueReporting]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[EdiRevenueReporting_Archive]  from  [dbo].[EdiRevenueReporting]
PRINT  '------------------------COPY  [dbo].[gl_Analysis]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[gl_Analysis_Archive]  from  [dbo].[gl_Analysis]
PRINT  '------------------------COPY  [dbo].[InterfaceConfig]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[InterfaceConfig_Archive]  from  [dbo].[InterfaceConfig]
PRINT  '------------------------COPY  [dbo].[InterfaceErrorLog]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[InterfaceErrorLog_Archive]  from  [dbo].[InterfaceErrorLog]
PRINT  '------------------------COPY  [dbo].[IntRecharge_Agents]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRecharge_Agents_Archive]  from  [dbo].[IntRecharge_Agents]
PRINT  '------------------------COPY  [dbo].[IntRecharge_Agents_old]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRecharge_Agents_old_Archive]  from  [dbo].[IntRecharge_Agents_old]
PRINT  '------------------------COPY  [dbo].[IntRecharge_BudgetPricing]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRecharge_BudgetPricing_Archive]  from  [dbo].[IntRecharge_BudgetPricing]
PRINT  '------------------------COPY  [dbo].[IntRecharge_EDIDetail]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRecharge_EDIDetail_Archive]  from  [dbo].[IntRecharge_EDIDetail]
PRINT  '------------------------COPY  [dbo].[IntRecharge_EDISummary]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRecharge_EDISummary_Archive]  from  [dbo].[IntRecharge_EDISummary]
PRINT  '------------------------COPY  [dbo].[IntRecharge_PrepaidDetails]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRecharge_PrepaidDetails_Archive]  from  [dbo].[IntRecharge_PrepaidDetails]
PRINT  '------------------------COPY  [dbo].[IntRecharge_PrepaidSummary]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRecharge_PrepaidSummary_Archive]  from  [dbo].[IntRecharge_PrepaidSummary]
PRINT  '------------------------COPY  [dbo].[IntRechargeMonthlyTotals]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IntRechargeMonthlyTotals_Archive]  from  [dbo].[IntRechargeMonthlyTotals]
PRINT  '------------------------COPY  [dbo].[IRPandOneoff]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[IRPandOneoff_Archive]  from  [dbo].[IRPandOneoff]
PRINT  '------------------------COPY  [dbo].[Load_ActivityErrors]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[Load_ActivityErrors_Archive]  from  [dbo].[Load_ActivityErrors]
PRINT  '------------------------COPY  [dbo].[name_data]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[name_data_Archive]  from  [dbo].[name_data]
PRINT  '------------------------COPY  [dbo].[OlapDimAccount]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimAccount_Archive]  from  [dbo].[OlapDimAccount]
PRINT  '------------------------COPY  [dbo].[OlapDimAccount_temp]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimAccount_temp_Archive]  from  [dbo].[OlapDimAccount_temp]
PRINT  '------------------------COPY  [dbo].[OlapDimBusinessUnit]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimBusinessUnit_Archive]  from  [dbo].[OlapDimBusinessUnit]
PRINT  '------------------------COPY  [dbo].[OlapDimCalendarDay]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimCalendarDay_Archive]  from  [dbo].[OlapDimCalendarDay]
PRINT  '------------------------COPY  [dbo].[OlapDimCalendarMonth]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimCalendarMonth_Archive]  from  [dbo].[OlapDimCalendarMonth]
PRINT  '------------------------COPY  [dbo].[OlapDimCalendarYear]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimCalendarYear_Archive]  from  [dbo].[OlapDimCalendarYear]
PRINT  '------------------------COPY  [dbo].[OlapDimCouponType]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimCouponType_Archive]  from  [dbo].[OlapDimCouponType]
PRINT  '------------------------COPY  [dbo].[OlapDimDepot]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimDepot_Archive]  from  [dbo].[OlapDimDepot]
PRINT  '------------------------COPY  [dbo].[OlapDimFinancialPeriod]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimFinancialPeriod_Archive]  from  [dbo].[OlapDimFinancialPeriod]
PRINT  '------------------------COPY  [dbo].[OlapDimFinancialWeek]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimFinancialWeek_Archive]  from  [dbo].[OlapDimFinancialWeek]
PRINT  '------------------------COPY  [dbo].[OlapDimFinancialYear]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimFinancialYear_Archive]  from  [dbo].[OlapDimFinancialYear]
PRINT  '------------------------COPY  [dbo].[OlapDimInsuranceCategory]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimInsuranceCategory_Archive]  from  [dbo].[OlapDimInsuranceCategory]
PRINT  '------------------------COPY  [dbo].[OlapDimNetworkCategory]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimNetworkCategory_Archive]  from  [dbo].[OlapDimNetworkCategory]
PRINT  '------------------------COPY  [dbo].[OlapDimNetworkType]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimNetworkType_Archive]  from  [dbo].[OlapDimNetworkType]
PRINT  '------------------------COPY  [dbo].[OlapDimRevenueType]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimRevenueType_Archive]  from  [dbo].[OlapDimRevenueType]
PRINT  '------------------------COPY  [dbo].[OlapDimServiceCategory]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimServiceCategory_Archive]  from  [dbo].[OlapDimServiceCategory]
PRINT  '------------------------COPY  [dbo].[OlapDimState]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimState_Archive]  from  [dbo].[OlapDimState]
PRINT  '------------------------COPY  [dbo].[OlapDimZone]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapDimZone_Archive]  from  [dbo].[OlapDimZone]
PRINT  '------------------------COPY  [dbo].[OlapFactEdiRevenueReporting]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapFactEdiRevenueReporting_Archive]  from  [dbo].[OlapFactEdiRevenueReporting]
PRINT  '------------------------COPY  [dbo].[OlapFactPrepaidRevenueReporting]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapFactPrepaidRevenueReporting_Archive]  from  [dbo].[OlapFactPrepaidRevenueReporting]
PRINT  '------------------------COPY  [dbo].[OlapFactPrepaidRevenueReporting2]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapFactPrepaidRevenueReporting2_Archive]  from  [dbo].[OlapFactPrepaidRevenueReporting2]
PRINT  '------------------------COPY  [dbo].[OlapFactPrepaidRevenueReportingSTKVALUE]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[OlapFactPrepaidRevenueReportingSTKVALUE_Archive]  from  [dbo].[OlapFactPrepaidRevenueReportingSTKVALUE]
PRINT  '------------------------COPY  [dbo].[Prepaid_RedeemedAudit]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[Prepaid_RedeemedAudit_Archive]  from  [dbo].[Prepaid_RedeemedAudit]
PRINT  '------------------------COPY  [dbo].[Prepaid_RedeemedAudit_Temp]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[Prepaid_RedeemedAudit_Temp_Archive]  from  [dbo].[Prepaid_RedeemedAudit_Temp]
PRINT  '------------------------COPY  [dbo].[Prepaid_RedeemedAudit1]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[Prepaid_RedeemedAudit1_Archive]  from  [dbo].[Prepaid_RedeemedAudit1]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueLinkLabels]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueLinkLabels_Archive]  from  [dbo].[PrepaidRevenueLinkLabels]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueRecognisedExceptions]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueRecognisedExceptions_Archive]  from  [dbo].[PrepaidRevenueRecognisedExceptions]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReporting]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReporting_Archive]  from  [dbo].[PrepaidRevenueReporting]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReporting_2011]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReporting_2011_Archive]  from  [dbo].[PrepaidRevenueReporting_2011]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReporting_2012]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReporting_2012_Archive]  from  [dbo].[PrepaidRevenueReporting_2012]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReporting_2013]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReporting_2013_Archive]  from  [dbo].[PrepaidRevenueReporting_2013]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReporting_2015_2016_2017]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReporting_2015_2016_2017_Archive]  from  [dbo].[PrepaidRevenueReporting_2015_2016_2017]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup1201710]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup1201710_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup1201710]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20170919]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20170919_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20170919]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20170919_1]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20170919_1_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20170919_1]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20170920]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20170920_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20170920]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20170921]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20170921_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20170921]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20170921_1]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20170921_1_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20170921_1]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20171003]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20171003_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20171003]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20171004]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20171004_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20171004]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup20171004_1]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup20171004_1_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup20171004_1]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReporting_weekbackup4201709]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReporting_weekbackup4201709_Archive]  from  [dbo].[PRepaidRevenueReporting_weekbackup4201709]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReportingCURRENT]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReportingCURRENT_Archive]  from  [dbo].[PrepaidRevenueReportingCURRENT]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReportingExceptions]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReportingExceptions_Archive]  from  [dbo].[PrepaidRevenueReportingExceptions]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup1201710]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup1201710_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup1201710]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919_1]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919_1_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919_1]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170920]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170920_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170920]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921_1]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921_1_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921_1]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171003]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171003_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171003]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004_1]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004_1_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004_1]
PRINT  '------------------------COPY  [dbo].[PRepaidRevenueReportingExceptions_weekbackup4201709]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PRepaidRevenueReportingExceptions_weekbackup4201709_Archive]  from  [dbo].[PRepaidRevenueReportingExceptions_weekbackup4201709]
PRINT  '------------------------COPY  [dbo].[PrepaidRevenueReportingSTKVALUE]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[PrepaidRevenueReportingSTKVALUE_Archive]  from  [dbo].[PrepaidRevenueReportingSTKVALUE]
PRINT  '------------------------COPY  [dbo].[Primary Coupons List]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[Primary Coupons List_Archive]  from  [dbo].[Primary Coupons List]
PRINT  '------------------------COPY  [dbo].[Recharge_Pricing]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[Recharge_Pricing_Archive]  from  [dbo].[Recharge_Pricing]
PRINT  '------------------------COPY  [dbo].[tblErrorLog]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[tblErrorLog_Archive]  from  [dbo].[tblErrorLog]
PRINT  '------------------------COPY  [dbo].[Temp_PrepaidRevNov15]  TABLE---------------------------------'  
SELECT * INTO  [dbo].[Temp_PrepaidRevNov15_Archive]  from  [dbo].[Temp_PrepaidRevNov15]


----Delete Record from table---------------------

PRINT '-------- DELETE DATA START --------';

--DELETE FROM  [dbo].[_Activity]
--DELETE FROM  [dbo].[_ActivityError]
--DELETE FROM  [dbo].[_ActivityImport]
--DELETE FROM  [dbo].[ActivityErrorImport]
--DELETE FROM  [dbo].[ActivityErrorImportHistory]
--DELETE FROM  [dbo].[AgentDataDecember2013]
--DELETE FROM  [dbo].[ConNote]
--DELETE FROM  [dbo].[EdiRevenueReporting]
--DELETE FROM  [dbo].[gl_Analysis]
--DELETE FROM  [dbo].[InterfaceConfig]
--DELETE FROM  [dbo].[InterfaceErrorLog]
--DELETE FROM  [dbo].[IntRecharge_Agents]
--DELETE FROM  [dbo].[IntRecharge_Agents_old]
--DELETE FROM  [dbo].[IntRecharge_BudgetPricing]
--DELETE FROM  [dbo].[IntRecharge_EDIDetail]
--DELETE FROM  [dbo].[IntRecharge_EDISummary]
--DELETE FROM  [dbo].[IntRecharge_PrepaidDetails]
--DELETE FROM  [dbo].[IntRecharge_PrepaidSummary]
--DELETE FROM  [dbo].[IntRechargeMonthlyTotals]
--DELETE FROM  [dbo].[IRPandOneoff]
--DELETE FROM  [dbo].[Load_ActivityErrors]
--DELETE FROM  [dbo].[name_data]
--DELETE FROM  [dbo].[OlapDimAccount]
--DELETE FROM  [dbo].[OlapDimAccount_temp]
--DELETE FROM  [dbo].[OlapDimBusinessUnit]
--DELETE FROM  [dbo].[OlapDimCalendarDay]
--DELETE FROM  [dbo].[OlapDimCalendarMonth]
--DELETE FROM  [dbo].[OlapDimCalendarYear]
--DELETE FROM  [dbo].[OlapDimCouponType]
--DELETE FROM  [dbo].[OlapDimDepot]
--DELETE FROM  [dbo].[OlapDimFinancialPeriod]
--DELETE FROM  [dbo].[OlapDimFinancialWeek]
--DELETE FROM  [dbo].[OlapDimFinancialYear]
--DELETE FROM  [dbo].[OlapDimInsuranceCategory]
--DELETE FROM  [dbo].[OlapDimNetworkCategory]
--DELETE FROM  [dbo].[OlapDimNetworkType]
--DELETE FROM  [dbo].[OlapDimRevenueType]
--DELETE FROM  [dbo].[OlapDimServiceCategory]
--DELETE FROM  [dbo].[OlapDimState]
--DELETE FROM  [dbo].[OlapDimZone]
--DELETE FROM  [dbo].[OlapFactEdiRevenueReporting]
--DELETE FROM  [dbo].[OlapFactPrepaidRevenueReporting]
--DELETE FROM  [dbo].[OlapFactPrepaidRevenueReporting2]
--DELETE FROM  [dbo].[OlapFactPrepaidRevenueReportingSTKVALUE]
--DELETE FROM  [dbo].[Prepaid_RedeemedAudit]
--DELETE FROM  [dbo].[Prepaid_RedeemedAudit_Temp]
--DELETE FROM  [dbo].[Prepaid_RedeemedAudit1]
--DELETE FROM  [dbo].[PrepaidRevenueLinkLabels]
--DELETE FROM  [dbo].[PrepaidRevenueRecognisedExceptions]
--DELETE FROM  [dbo].[PrepaidRevenueReporting]
--DELETE FROM  [dbo].[PrepaidRevenueReporting_2011]
--DELETE FROM  [dbo].[PrepaidRevenueReporting_2012]
--DELETE FROM  [dbo].[PrepaidRevenueReporting_2013]
--DELETE FROM  [dbo].[PrepaidRevenueReporting_2015_2016_2017]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup1201710]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20170919]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20170919_1]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20170920]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20170921]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20170921_1]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20171003]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20171004]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup20171004_1]
--DELETE FROM  [dbo].[PRepaidRevenueReporting_weekbackup4201709]
--DELETE FROM  [dbo].[PrepaidRevenueReportingCURRENT]
--DELETE FROM  [dbo].[PrepaidRevenueReportingExceptions]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup1201710]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919_1]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170920]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921_1]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171003]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004_1]
--DELETE FROM  [dbo].[PRepaidRevenueReportingExceptions_weekbackup4201709]
--DELETE FROM  [dbo].[PrepaidRevenueReportingSTKVALUE]
--DELETE FROM  [dbo].[Primary Coupons List]
--DELETE FROM  [dbo].[Recharge_Pricing]
--DELETE FROM  [dbo].[tblErrorLog]
--DELETE FROM  [dbo].[Temp_PrepaidRevNov15]

PRINT '-------- DELETE DATA END --------';

--EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
PRINT '-------- START ENABLING TABLE CONSTRAINT --------';

		ALTER TABLE   [dbo].[_Activity] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[_ActivityError] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[_ActivityImport] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[ActivityErrorImport] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[ActivityErrorImportHistory] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[AgentDataDecember2013] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[ConNote] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[EdiRevenueReporting] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[gl_Analysis] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[InterfaceConfig] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[InterfaceErrorLog] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_Agents] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_Agents_old] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_BudgetPricing] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_EDIDetail] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_EDISummary] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_PrepaidDetails] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRecharge_PrepaidSummary] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IntRechargeMonthlyTotals] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[IRPandOneoff] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Load_ActivityErrors] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[name_data] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimAccount] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimAccount_temp] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimBusinessUnit] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCalendarDay] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCalendarMonth] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCalendarYear] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimCouponType] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimDepot] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimFinancialPeriod] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimFinancialWeek] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimFinancialYear] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimInsuranceCategory] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimNetworkCategory] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimNetworkType] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimRevenueType] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimServiceCategory] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimState] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapDimZone] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactEdiRevenueReporting] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactPrepaidRevenueReporting] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactPrepaidRevenueReporting2] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[OlapFactPrepaidRevenueReportingSTKVALUE] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Prepaid_RedeemedAudit] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Prepaid_RedeemedAudit_Temp] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Prepaid_RedeemedAudit1] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueLinkLabels] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueRecognisedExceptions] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2011] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2012] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2013] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReporting_2015_2016_2017] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup1201710] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170919] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170919_1] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170920] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170921] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20170921_1] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20171003] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20171004] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup20171004_1] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReporting_weekbackup4201709] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReportingCURRENT] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReportingExceptions] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup1201710] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170919_1] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170920] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20170921_1] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171003] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup20171004_1] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PRepaidRevenueReportingExceptions_weekbackup4201709] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[PrepaidRevenueReportingSTKVALUE] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Primary Coupons List] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Recharge_Pricing] CHECK  CONSTRAINT ALL
		ALTER TABLE   [dbo].[Temp_PrepaidRevNov15] CHECK  CONSTRAINT ALL

PRINT '-------- END ENABLING TABLE CONSTRAINT --------';
select 'OK' as Result

		COMMIT TRAN 
		--rollback tran
		END TRY
				BEGIN CATCH
				begin
					rollback tran
					INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
					 VALUES
						   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
						   ,'SP_RevenueDatabaseMaintenance', 2)

						   select cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as Result
				end
				END CATCH

END 
GO
