SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptRevenueDifferencesInProntoAndPivotByDate]   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	   
  Select LabelNumber ,[RevenueRecognisedDate],CouponType,BU, RevenueAmount ,Convert(Date,CreatedDate) as CreatedDate
  into #temp
  FROM [Revenue].[dbo].[PrepaidRevenueReporting] 
  Where [IsFirstScanProcessed] = 1 and [RevenueRecognisedDate] >= '2016-11-01' and RevenueAmount >0
  union all
    Select LabelNumber, [RevenueRecognisedDate],CouponType,BU, RevenueAmount ,Convert(Date,CreatedDate) as CreatedDate
       FROM [Revenue].[dbo].[PrepaidRevenueReportingExceptions]
  Where [IsFirstScanProcessed] = 1 and [RevenueRecognisedDate] >= '2016-11-01'and RevenueAmount >0
   
  Delete from  #temp where  RevenueAmount  = 0
  
  Select T.LabelNumber ,T.[RevenueRecognisedDate],T.CouponType,BU, T.RevenueAmount,CreatedDate ,P.RevenueDate , P.RevenueAmount as ProntoRevenue
   into #temp2
   from #Temp T left join [Pronto].[dbo].[ProntoCouponDetails] P on  T.LabelNumber = P.[SerialNumber]
     
   Select LabelNumber,RevenueRecognisedDate,CouponType,BU,RevenueAmount,CreatedDate,RevenueDate,ProntoRevenue from  #Temp2 Where RevenueRecognisedDate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) 
and RevenueAmount <> ProntoRevenue
   order by 2 desc
END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptRevenueDifferencesInProntoAndPivotByDate]
	TO [ReportUser]
GO
