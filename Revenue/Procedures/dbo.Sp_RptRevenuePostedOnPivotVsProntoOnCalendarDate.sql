SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Sp_RptRevenuePostedOnPivotVsProntoOnCalendarDate 
As
BEGIN

SELECT Convert(Date,CreatedDate) as Date, Count(*) as Count ,Sum(RevenueAmount) as Revenue
into #temp1
  FROM [Revenue].[dbo].[PrepaidRevenueReportingExceptions]

  where Convert(Date,CreatedDate) >= '2017-02-01' and IsFirstScanProcessed =1
  group by   Convert(Date,CreatedDate)
 
  union all
 
  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT Convert(Date,CreatedDate) as Date, Count(*) as Count,Sum(RevenueAmount) as Revenue
  FROM [Revenue].[dbo].[PrepaidRevenueReporting]
  where Convert(Date,CreatedDate) >= '2017-02-01' and IsFirstScanProcessed =1
  group by   Convert(Date,CreatedDate)
 
  --Drop Table #Temp1 
 
  Select   Date,  Sum(Revenue) as Revenue
  into #Temp2
from  #temp1
  group by   date
  order by 1 desc
 
 
 
   Select RevenueDate, Sum(RevenueAmount)  as RevenueAmount 
   into #Temp3
   from Pronto.dbo.ProntoCouponDetails
   Where RevenueDate > '2017-02-01'
 
   Group by RevenueDate
 
 
 
Select Datepart(year,Date) *100 + Datepart(Month,Date) as MonthKey,Date as CalenderDate , Revenue as PrepaidRevenue , Isnull(RevenueAmount,0) as ProntoRevenue from  #temp2 Left Join #Temp3 on Date=RevenueDate
 

END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptRevenuePostedOnPivotVsProntoOnCalendarDate]
	TO [ReportUser]
GO
