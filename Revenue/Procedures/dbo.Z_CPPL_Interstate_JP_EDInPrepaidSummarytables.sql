SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create procedure [Z_CPPL_Interstate_JP_EDInPrepaidSummarytables](@MonthKey int) as
begin
SELECT 
datepart(yyyy,cd_date)*100+datepart(mm,cd_date) as [MonthKey] ,
[ConNote]
,convert(varchar(20),'') as Type 
,convert(varchar(20),'') as category
      ,[cd_date]
      ,[cd_items]
      ,[cd_coupons]
      ,[CouponCount]
      ,[cd_pricecode]
      ,[cd_deadweight]
      ,[cd_volume]
      ,[PickupScanDepot]
      ,[PickupScanBranch]
      ,[cd_deliver_driver]
      ,[DelivScanDepot]
      ,[DelivScanBranch]
      ,[DeliveryAgentName]
	  into #temp
  FROM [OperationalReporting].[dbo].[JP_Recharge_EDI_Normal_Drivers] where  datepart(yyyy,cd_date)*100+datepart(mm,cd_date)  =@Monthkey



  SELECT 
  datepart(yyyy,cd_date)*100+datepart(mm,cd_date) as [MonthKey] ,
  [ConNote]
,convert(varchar(20),'') as Type 
,convert(varchar(20),'') as category
      ,[cd_date]
      ,[cd_items]
      ,[cd_coupons]
      ,[CouponCount]
      ,[cd_pricecode]
      ,[cd_deadweight]
      ,[cd_volume]
      ,[PickupScanDepot]
      ,[PickupScanBranch]
      ,[cd_deliver_driver]
      ,[DelivScanDepot]
      ,[DelivScanBranch]
      ,[DeliveryAgentName]
	  into #temp1
  FROM [OperationalReporting].[dbo].[JP_Recharge_EDI_Special_Drivers] where  datepart(yyyy,cd_date)*100+datepart(mm,cd_date)  =@Monthkey

  select * into #temp2 from #temp 
  union all 
  select * from #temp1

  update #temp2 set Type=(case when (len(cd_Connote)=11 and isnumeric(cd_connote)=1) then 'Prepaid' else 'EDI' end)
   Update #Temp2 SET [Category] =(case when cd_deliver_driver not  in ('2000','2001','229','7181','2540','300','407','409','411','301') then 'NORMAL' Else 'SPECIAL' end)



     insert into [dbo].[IntRecharge_EDISummary_1](
	  Monthkey,
	  Type,
	   Category ,
	   [BusinessUnit] ,
 [cd_pricecode] ,
 [PickupScanDepot] ,
 [DelivScanBranch] ,
  [DelivScanDepot] ,
 [DeliveryAgentName] ,
       [ConCount] ,
       [Items] ,
       [Volume] ,
       [DeadWeight] ,
	   [CreatedDate],
       [CreatedBy] )


 Select [MonthKey],[Type],[Category],[PickupScanBranch] as BusinessUnit,cd_pricecode,[PickupScanDepot],[DelivScanBranch],[DelivScanDepot],[DeliveryAgentName],sum(couponcount) as concount,SUM(cd_Items) as Items,SUM(cd_volume) as Volume, SUM(cd_deadweight) as DeadWeight,MAx(Getdate()),MAx('Abhigna')
From  #Temp2
Group by [MonthKey],Type,[Category],[PickupScanBranch],cd_pricecode,[PickupScanDepot],[DelivScanBranch],[DelivScanDepot],[DeliveryAgentName]
order by 1,2,3,4,5,6,7,8



   select [PP_SUM_ID]
  , datepart(yyyy,cd_date)*100+datepart(mm,cd_date) as [MonthKey] 
,'Prepaid' as Type 
,'NORMAL' as category
      ,[Month]
      ,[BusinessUnit]
      ,[PickupScanDepot]
      ,[DelivScanDriverBranch]
      ,[DelivScanDepot]
      ,[DeliveryAgentName]
      ,[Count]
	  into #temp10 from
[dbo].[Recharge_PrePaid_Summary] where  datepart(yyyy,cd_date)*100+datepart(mm,cd_date)  =@Monthkey


select [PP_SUM_ID]
,  datepart(yyyy,cd_date)*100+datepart(mm,cd_date) as [MonthKey] 
,'Prepaid' as Type 
,'SPECIAL' as category
      ,[Month]
      ,[BusinessUnit]
      ,[PickupScanDepot]
      ,[DelivScanDriverBranch]
      ,[DelivScanDepot]
      ,[DeliveryAgentName]
      ,[Count]
	  into #temp11 from [dbo].[Recharge_PrePaid_Special_Drivers] where  datepart(yyyy,cd_date)*100+datepart(mm,cd_date)  =@Monthkey


	  select * into #temp12 from #temp10
	  union all 
	  select * from #temp11

	Insert into  [dbo].[IntRecharge_PrepaidSummary_1](
	[MonthKey] ,
	Type,
	[Category] ,
	[BusinessUnit] ,
	[PickupScanDepot] ,
	[DelivScanDriverBranch] ,
	[DeliveryAgentID],
	[DelivScanDepot] ,
	[DeliveryAgentName] ,
	[ItemCount] ,
	[CreatedDate] ,
	[CreatedBy] )
	
	 
Select [MonthKey],Type,[Category], FirstScanDriverBranch as BusinessUnit,
PickupScanDepot,OtherScanDriverBranch as DelivScanDriverBranch,DeliveryAgentID,DelivScanDepot,DeliveryAgentName,COUNT(*) as [ItemCount], MAx(Getdate()),MAx('Abhigna')
From  #Temp12
Group by [MonthKey],Type,[Category],FirstScanDriverBranch,PickupScanDepot,OtherScanDriverBranch,DelivScanDepot,DeliveryAgentID,DeliveryAgentName
order by 1,2,3,4,5,6,7,8

end

GO
