SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[Z_CPPL_PopulateLinkLabelsForPrepaidRevenueVer2_OLD]
(
	@LabelNumber	varchar(50),
	@CreatedDate	datetime
)
AS
BEGIN


--DECLARE @LabelNumber varchar(50) = '10406265623', @CreatedDate datetime = '2013-03-01';

	--DROP TABLE #LinkLabels
	SELECT @LabelNumber = LTRIM(RTRIM(@LabelNumber));
	CREATE TABLE #LinkLabels2 (LabelNumber varchar(50), LinkLabelNumber varchar(50));
	TRUNCATE TABLE #LinkLabels;
	
	-- populate any existing link labels that are in the permanent table
	INSERT INTO #LinkLabels
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT
			LabelNumber, LinkLabelNumber 
	FROM PrepaidRevenueLinkLabels (NOLOCK)
	WHERE LabelNumber = @LabelNumber;

	-- populate any labels that this label is linked to
	INSERT INTO #LinkLabels 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT @LabelNumber,
			--LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
			LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
	FROM ODS.dbo.TrackingEvent te (NOLOCK)
	JOIN ODS.dbo.Label l (NOLOCK)
	ON te.LabelId = l.Id 
	-- allow for events up to 30 days behind the creation of the label record
	WHERE te.EventDateTime >= DATEADD(day, -30, @CreatedDate)
	AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
	AND te.AdditionalText1 LIKE ('Link Coupon ' + @LabelNumber + '%')
	AND NOT EXISTS
	(
		SELECT 1 FROM #LinkLabels
		WHERE LabelNumber = @LabelNumber 
		AND LinkLabelNumber = LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
	);

	-- populate any labels that are linked this label
	INSERT INTO #LinkLabels 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT @LabelNumber,
			LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
	FROM ODS.dbo.TrackingEvent te (NOLOCK)
	JOIN ODS.dbo.Label l (NOLOCK)
	ON te.LabelId = l.Id 
	-- allow for events up to 30 days behind the creation of the label record
	WHERE te.EventDateTime >= DATEADD(day, -30, @CreatedDate)
	AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
	AND l.LabelNumber = @LabelNumber
	AND NOT EXISTS
	(
		SELECT 1 FROM #LinkLabels 
		WHERE LabelNumber = @LabelNumber
		AND LinkLabelNumber = LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
	);


--start code upgrade by noe 2013-03-07
		-- populate 2nd-level link labels
		INSERT INTO #LinkLabels (LabelNumber, LinkLabelNumber)
		SELECT
			DISTINCT @LabelNumber,
				--LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
				LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		FROM ODS.dbo.TrackingEvent te (NOLOCK)
		INNER JOIN ODS.dbo.Label l (NOLOCK) ON te.LabelId = l.Id 
		INNER JOIN (SELECT DISTINCT LinkLabelNumber FROM #LinkLabels) lli ON te.AdditionalText1 LIKE ('Link Coupon ' + lli.LinkLabelNumber + '%')
		LEFT JOIN #LinkLabels ll ON ll.LabelNumber = @LabelNumber AND ll.LinkLabelNumber = LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		-- allow for events up to 30 days behind the creation of the label record
		WHERE te.EventDateTime >= DATEADD(day, -30, @CreatedDate)
		AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
		AND ll.LabelNumber IS NULL
--end code upgrade by noe 2013-03-07

	
/*
--start commented by noe for optimization process.

	-- populate 2nd-level link labels
	DECLARE @ll varchar(50);
	DECLARE curLL CURSOR READ_ONLY STATIC FOR
	SELECT DISTINCT LinkLabelNumber
	FROM #LinkLabels;
	
	OPEN curLL;
	
	FETCH NEXT FROM curLL INTO @ll;
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		INSERT INTO #LinkLabels 
		(LabelNumber, LinkLabelNumber)
		SELECT
			DISTINCT @LabelNumber,
				--LTRIM(RTRIM(REPLACE(REPLACE(te.AdditionalText1, 'link', ''), 'coupon', '')))
				LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		FROM ODS.dbo.TrackingEvent te (NOLOCK)
		JOIN ODS.dbo.Label l (NOLOCK)
		ON te.LabelId = l.Id 
		/*JOIN #LinkLabels ll
		ON te.AdditionalText1 LIKE ('Link Coupon ' + ll.LinkLabelNumber + '%')*/
		-- allow for events up to 30 days behind the creation of the label record
		WHERE te.EventDateTime >= DATEADD(day, -30, @CreatedDate)
		AND te.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' --link
		AND te.AdditionalText1 LIKE ('Link Coupon ' + @ll + '%')
		AND NOT EXISTS
		(
			SELECT 1 FROM #LinkLabels 
			WHERE LabelNumber = @LabelNumber
			AND LinkLabelNumber = LTRIM(RTRIM(ISNULL(l.LabelNumber, '')))
		);
		
		FETCH NEXT FROM curLL INTO @ll;
	
	END
	
	CLOSE curLL;
	DEALLOCATE curLL;

--end commented by noe for optimization process.	
*/	
	
	DELETE FROM #LinkLabels 
	WHERE LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) = ''
	OR LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) = @LabelNumber;
	
	IF EXISTS (SELECT 1 FROM #LinkLabels)
	BEGIN
	
		INSERT INTO PrepaidRevenueLinkLabels 
		(LabelNumber, LinkLabelNumber)
		SELECT
			DISTINCT LabelNumber, LinkLabelNumber 
		FROM #LinkLabels ll
		WHERE NOT EXISTS
		(
			SELECT 1 FROM PrepaidRevenueLinkLabels prl
			WHERE prl.LabelNumber = ll.LabelNumber
			AND prl.LinkLabelNumber = ll.LinkLabelNumber
		);

		--SELECT * FROM #LinkLabels;
		--BREAK;

	END

	-- make sure we've got only distinct records in #LinkLabels
	INSERT INTO #LinkLabels2 
	(LabelNumber, LinkLabelNumber)
	SELECT
		DISTINCT
			LabelNumber,
			LinkLabelNumber
	FROM #LinkLabels;
	
	TRUNCATE TABLE #LinkLabels;
	
	INSERT INTO #LinkLabels
	(LabelNumber, LinkLabelNumber)
	SELECT
		LabelNumber,
		LinkLabelNumber
	FROM #LinkLabels2;

	DROP TABLE #LinkLabels2;

END
GO
