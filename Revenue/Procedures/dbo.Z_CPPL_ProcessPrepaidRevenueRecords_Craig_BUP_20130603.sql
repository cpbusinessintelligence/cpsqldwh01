SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[Z_CPPL_ProcessPrepaidRevenueRecords_Craig_BUP_20130603]
(
	@StartDate		date,
	@EndDate		date,
	@MaxRecords		int
)
AS
BEGIN


	/*
	select * from ods.dbo.TrackingEvent 
	where EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D'
	and AdditionalText1 like 'link cou%6020018%'
	*/

	--=======================================================


	SET NOCOUNT ON;


	DECLARE @counter int;
	SELECT @counter = -1;

	DECLARE @lbl varchar(50);
	DECLARE @RevenueRecognitionThresholdDays tinyint;
	-- this is the absolute threshold for an item to wait for the 
	-- necessary scans to properly categorise it
	SELECT @RevenueRecognitionThresholdDays = 7;

	-- variables for label details
	DECLARE @ServiceCategory char(1), @ReturnsFlag bit,
			@FirstScanDateTime datetime, @FirstScanBranch varchar(20),
			@FirstScanDriver varchar(15), @FirstScanPronto varchar(10),
			@FirstScanDepot varchar(40), @FirstScanType varchar(20),
			@RevRecDate date, @FirstScanId uniqueidentifier,
			@OtherScanDateTime datetime, @OtherScanBranch varchar(20),
			@OtherScanDriver varchar(15), @OtherScanPronto varchar(10),
			@OtherScanDepot varchar(40), @OtherScanType varchar(20),
			@NetworkCategory char(1), @OtherScanId uniqueidentifier,
			@BUCode varchar(8), @BU varchar(20), @ItemAge smallint,
			@ConsignmentNumber varchar(50), @conOriginState varchar(10),
			@conDestinationState varchar(10), @CreatedDate datetime,
			@TrackingEventId uniqueidentifier, @LinkLabelsPopulated bit,
			@ProntoOrderContractorCode varchar(20), @RevenueAmount decimal(13,2),
			@FirstScanDetailsChanged bit, @OtherScanDetailsChanged bit,
			@ServiceCategoryChanged bit, @ConsignmentCustomer varchar(100),
			@ConsolidateBarcode varchar(20), @ConsolidateOriginBranch varchar(20),
			@ConsolidateDestinationBranch varchar(20), @ConsolidateDateTime datetime,
			@OriginBranch varchar(20), @OriginDepot varchar(40), @OriginState varchar(10),
			@DestinationBranch varchar(20), @DestinationDepot varchar(40),
			@DestinationState varchar(10), @SkipRevenueRecognition bit,
			@ConsolidateBarcodeChanged bit, @CouponType varchar(15);

	CREATE TABLE #LinkLabels (LabelNumber varchar(50), LinkLabelNumber varchar(50));
	CREATE CLUSTERED INDEX idx_LinkLabels_PK ON #LinkLabels (LabelNumber, LinkLabelNumber);

	IF (ISNULL(@EndDate, '1jan2050') < ISNULL(@StartDate, '1jan1900'))
	BEGIN
		PRINT 'End date cannot be less than start date.';
		RETURN;
	END

	DECLARE @curREV CURSOR;
	
	IF ISNULL(@MaxRecords, 0) > 0
	BEGIN
		SET @curRev = CURSOR READ_ONLY STATIC FOR
		SELECT TOP (@MaxRecords) LabelNumber
		FROM PrepaidRevenueReporting
		WHERE ISNULL(IsProcessed, 0) = 0
		AND CONVERT(date, ISNULL(CreatedDate, '1jan1900')) >= ISNULL(@StartDate, CONVERT(date, GETDATE()))
		AND CONVERT(date, ISNULL(CreatedDate, '1jan1900')) <= ISNULL(@EndDate, CONVERT(date, '1jan2050'));
	END
	ELSE
	BEGIN
		SET @curRev = CURSOR READ_ONLY STATIC FOR
		SELECT LabelNumber
		FROM PrepaidRevenueReporting
		WHERE ISNULL(IsProcessed, 0) = 0
		AND CONVERT(date, ISNULL(CreatedDate, '1jan1900')) >= ISNULL(@StartDate, CONVERT(date, GETDATE()))
		AND CONVERT(date, ISNULL(CreatedDate, '1jan1900')) <= ISNULL(@EndDate, CONVERT(date, '1jan2050'));
	END	

	OPEN @curRev;

	FETCH NEXT FROM @curRev INTO @lbl;
	
	IF (ISNULL(@MaxRecords, 0) > 0)
	BEGIN
		SELECT @counter = @counter + 1;
	END
	ELSE
	BEGIN
		SELECT @MaxRecords = 0;
	END

	WHILE ((@@FETCH_STATUS = 0) AND (@counter < @MaxRecords))
	BEGIN

		IF (@MaxRecords > 0)
		BEGIN
			SELECT @counter = @counter + 1;
		END

		IF (LTRIM(RTRIM(ISNULL(@lbl, ''))) != '')
		BEGIN

			TRUNCATE TABLE #LinkLabels;

			SELECT @ServiceCategory = Null, @FirstScanDateTime = Null, @FirstScanBranch = Null,
				@FirstScanDriver = Null, @FirstScanPronto = Null, @FirstScanDepot = Null,
				@FirstScanType = Null, @RevRecDate = Null, @OtherScanDateTime = Null,
				@OtherScanBranch = Null, @OtherScanDriver = Null, @OtherScanPronto = Null,
				@OtherScanDepot = Null, @OtherScanType = Null, @NetworkCategory = Null,
				@BUCode = Null, @BU = Null, @ConsignmentNumber = Null, @SkipRevenueRecognition = 0,
				@conOriginState = Null, @conDestinationState = Null, @ConsignmentCustomer = Null,
				@FirstScanId = Null, @OtherScanId = Null, @CreatedDate = Null,
				@TrackingEventId = Null, @LinkLabelsPopulated = 0, @ProntoOrderContractorCode = Null,
				@ServiceCategoryChanged = 0, @FirstScanDetailsChanged = 0, @CouponType = Null,
				@OtherScanDetailsChanged = 0, @RevenueAmount = Null, @ConsolidateBarcode = Null,
				@ConsolidateOriginBranch = Null, @ConsolidateDestinationBranch = Null,
				@ItemAge = Null, @ConsolidateBarcodeChanged = 0, @ReturnsFlag = 0;
			
			SELECT
				@ServiceCategory = ServiceCategory,
				@FirstScanDateTime = FirstScanDateTime,
				@FirstScanBranch = FirstScanDriverBranch,
				@FirstScanDriver = FirstScanDriverNumber,
				@FirstScanPronto = FirstScanContractorCode,
				@FirstScanDepot = FirstScanDepot,
				@FirstScanType = FirstScanType,
				@RevRecDate = RevenueRecognisedDate,
				@OtherScanDateTime = OtherScanDateTime,
				@OtherScanBranch = OtherScanDriverBranch,
				@OtherScanDriver = OtherScanDriverNumber,
				@OtherScanPronto = OtherScanContractorCode,
				@OtherScanDepot = OtherScanDepot,
				@OtherScanType = OtherScanType,
				@NetworkCategory = NetworkCategory,
				@BUCode = BUCode, @BU = BU,
				@ConsignmentNumber = ConsignmentNumber,
				@conOriginState = ConsignmentOriginState,
				@conDestinationState = ConsignmentDestinationState,
				@CreatedDate = CreatedDate, 
				@ProntoOrderContractorCode = ProntoOrderContractorCode,
				@ConsignmentCustomer = ConsignmentCustomer,
				@RevenueAmount = RevenueAmount,
				@ConsolidateBarcode = ConsolidateBarcode,
				@CouponType = ISNULL(CouponType, '')
			FROM PrepaidRevenueReporting
			WHERE LabelNumber = @lbl;
			
			/* populate / re-populate all linked labels for the current label.
			   There may be additional links that have occurred since the last process, 
			   which will help to finalise the categorisation */
			EXEC dbo.CPPL_PopulateLinkLabelsForPrepaidRevenue @lbl, @CreatedDate;
			-- flag this variable for later
			SELECT @LinkLabelsPopulated = 1;

			-- set the item age
			SELECT @ItemAge = DATEDIFF(day, CONVERT(date, @CreatedDate), CONVERT(date, GETDATE()));

			-- set the service category, if possible
			IF LTRIM(RTRIM(ISNULL(@ServiceCategory, ''))) = ''
			BEGIN
				IF ((LEFT(@lbl, 3) IN ('120','121','122','127','135','136','137'))
						OR (SUBSTRING(@lbl, 2, 2) IN ('20','21','22','36','37','68')))
				BEGIN
					-- these are all satchel prefixes, either national or local branch
					SELECT @ServiceCategory = 'SATCHELS';
					SELECT @ServiceCategoryChanged = 1;
				END
				ELSE
				IF ((LEFT(@lbl, 3) IN ('182','187','183'))
						OR (SUBSTRING(@lbl, 2, 2) IN ('93','94','14')))
				BEGIN
					-- these are returns prefixes (IRP or "return/RP tracker" coupons)
					SELECT @ServiceCategory = 'RETURNS';
					SELECT @ServiceCategoryChanged = 1;
					SELECT @ReturnsFlag = 1;
				END
				ELSE
				IF (LEFT(@lbl, 3) LIKE '[23456789]89%')
				BEGIN
					-- assume all one-offs are parcels, we can't determine whether they're "returns" or not
					SELECT @ServiceCategory = 'PARCELS';
					SELECT @ServiceCategoryChanged = 1;
				END
				ELSE
				BEGIN
					-- we can't determine the service category any further here, so
					-- we need to check the linked labels for the coupon
					
					--IF EXISTS (SELECT 1 FROM PrepaidRevenueLinkLabels 
					IF EXISTS (SELECT 1 FROM #LinkLabels  
								WHERE LabelNumber = @lbl
								AND LinkLabelNumber LIKE '[23456789]14%')
					BEGIN
						SELECT @ServiceCategory = 'RETURNS';
						SELECT @ServiceCategoryChanged = 1;
						SELECT @ReturnsFlag = 1;
					END
				END
			END		-- LTRIM(RTRIM(ISNULL(@ServiceCategory, ''))) = ''

			IF @FirstScanDateTime Is Null AND ISNULL(@FirstScanBranch, '') = ''
			BEGIN

				-- we check for scans on the original label, as well as any linked
				SELECT @TrackingEventId = Null;
				
				WITH Ids AS
				(
					SELECT TOP 1 te.Id AS [EventId], 0 AS [Priority]
					FROM ODS.dbo.TrackingEvent te (NOLOCK)
					JOIN ODS.dbo.Label l (NOLOCK)
					ON te.LabelId = l.Id 
					WHERE (l.LabelNumber IN
						(
							SELECT DISTINCT LinkLabelNumber AS [TheLabel]
							FROM #LinkLabels
							WHERE LabelNumber = @lbl 
							AND LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) != ''
							UNION ALL
							SELECT @lbl AS [TheLabel]
						)
					) 
					AND te.EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' --pickup
					ORDER BY te.EventDateTime ASC
					UNION 
					SELECT TOP 1 te.Id AS [EventId], 1 AS [Priority]
					FROM ODS.dbo.TrackingEvent te (NOLOCK)
					JOIN ODS.dbo.Label l (NOLOCK)
					ON te.LabelId = l.Id 
					WHERE (l.LabelNumber IN
						(
							SELECT DISTINCT LinkLabelNumber AS [TheLabel]
							FROM #LinkLabels
							WHERE LabelNumber = @lbl 
							AND LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) != ''
							UNION ALL
							SELECT @lbl AS [TheLabel]
						)
					) 
					AND
					(
						(te.EventTypeId IN
						(
							'B8D04A85-A65B-41EA-9056-A950BE2CB509', --in-depot
							'F47CABB2-55AA-4F19-B5EE-C2754268D1AF' --consolidate
							--'1A35AB45-B82A-492A-A1E8-6415BF846C75' --handover
						))
					)
					ORDER BY te.EventDateTime ASC
				)
				SELECT 
				TOP 1 @TrackingEventId = Ids.EventId
				FROM Ids
				ORDER BY Ids.[Priority] ASC;

				IF @TrackingEventId Is Not Null
				BEGIN
				
					SELECT
						@FirstScanBranch = UPPER(ISNULL(b.CosmosBranch, 'UNKNOWN')),
						@FirstScanDateTime = te.EventDateTime,
						@FirstScanDriver = d.Code,
						@FirstScanDepot = UPPER(ISNULL(dp.Name, 'UNKNOWN')),
						@FirstScanType = UPPER(ISNULL(et.[Description], 'UNKNOWN')),
						@FirstScanPronto = ISNULL(d.ProntoDriverCode, '')
					FROM ODS.dbo.TrackingEvent te (NOLOCK)
					JOIN ODS.dbo.Driver d (NOLOCK)
					ON te.DriverId = d.Id 
					JOIN ODS.dbo.Branch b (NOLOCK)
					ON d.BranchId = b.Id 
					JOIN ODS.dbo.EventType et (NOLOCK)
					ON te.EventTypeId = et.Id 
					LEFT OUTER JOIN ods.dbo.Depot dp (NOLOCK)
					ON d.DepotId = dp.Id 
					WHERE te.Id = @TrackingEventId;
					
					SELECT @FirstScanDetailsChanged = 1;
					
				END		-- @TrackingEventId Is Not Null

			END		-- @FirstScanDateTime Is Null AND ISNULL(@FirstScanBranch, '') = ''

			IF @FirstScanDateTime Is Null AND
			(
					(@ItemAge < 2)
					OR
					((@ItemAge < 14) AND (@RevenueAmount Is Null))
			)
				--
				-- if the item is less than 2 days' old, (or it's less than 10 days' old without
				-- a revenue value from Pronto yet), and we haven't had a "first" scan yet,
				-- then we'll leave it until the next day's processing - otherwise, continue processing
				--
				-- (the 10 days with no revenue should account for any delays in invoice processing, 
				-- plus the fact that the data's only coming out of Pronto once a week)
			BEGIN
				SELECT @SkipRevenueRecognition = 1;
			END
			ELSE
			BEGIN

				--
				-- first, we check if there's actually a delivery scan
				--
				IF @OtherScanDateTime Is Null AND ISNULL(@OtherScanBranch, '') = ''
				BEGIN
					-- check the current label, and linked labels, for "other" scan event
					SELECT @TrackingEventId = Null;

					-- retrieve the "other" scan information, attempt/delivery get priority,
					-- or another scan that's in a different state
					WITH Ids AS
					(
						SELECT TOP 1 te.Id AS [EventId], 0 AS [Priority]
						FROM ODS.dbo.TrackingEvent te (NOLOCK)
						JOIN ODS.dbo.Label l (NOLOCK)
						ON te.LabelId = l.Id 
						WHERE (l.LabelNumber IN
							(
								SELECT DISTINCT LinkLabelNumber AS [TheLabel]
								FROM #LinkLabels
								WHERE LabelNumber = @lbl 
								AND LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) != ''
								UNION ALL
								SELECT @lbl AS [TheLabel]
							)
						) 
						AND te.EventTypeId IN
						(
							'47CFA05F-3897-4F1F-BDF4-00C6A69152E3', --delivered
							'41A8F8F9-D57E-40F0-9D9D-97767AC3069E', --delivery
							'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' --attempt
						)
						ORDER BY te.EventDateTime ASC
					)
					SELECT 
					TOP 1 @TrackingEventId = Ids.EventId
					FROM Ids
					ORDER BY Ids.[Priority] ASC;
					
					IF @TrackingEventId Is Not Null
					BEGIN
					
						SELECT
							@OtherScanBranch = UPPER(ISNULL(b.CosmosBranch, 'UNKNOWN')),
							@OtherScanDateTime = te.EventDateTime,
							@OtherScanDriver = d.Code,
							@OtherScanDepot = UPPER(ISNULL(dp.Name, 'UNKNOWN')),
							@OtherScanType = UPPER(ISNULL(et.[Description], 'UNKNOWN')),
							@OtherScanPronto = ISNULL(d.ProntoDriverCode, '')
						FROM ODS.dbo.TrackingEvent te (NOLOCK)
						JOIN ODS.dbo.Driver d (NOLOCK)
						ON te.DriverId = d.Id 
						JOIN ODS.dbo.Branch b (NOLOCK)
						ON d.BranchId = b.Id 
						JOIN ODS.dbo.EventType et (NOLOCK)
						ON te.EventTypeId = et.Id 
						LEFT OUTER JOIN ods.dbo.Depot dp (NOLOCK)
						ON d.DepotId = dp.Id 
						WHERE te.Id = @TrackingEventId;
						
						SELECT @OtherScanDetailsChanged = 1;
						
					END		-- @TrackingEventId Is Not Null

				END		-- @OtherScanDateTime Is Null AND ISNULL(@OtherScanBranch, '') = ''

				IF @OtherScanDateTime Is Null AND @ItemAge < 5
					--
					-- if the item is less than 5 days' old, and we haven't had some kind of "delivery" scan yet,
					-- then we'll leave it until the next day's processing - otherwise, continue processing
					--
				BEGIN
					SELECT @SkipRevenueRecognition = 1;
				END
				ELSE
				BEGIN

					-- check the current label, and linked labels, for "other" scan event
					SELECT @TrackingEventId = Null;

					-- retrieve the "other" scan information, attempt/delivery get priority,
					-- or another scan that's in a different state
					WITH Ids AS
					(
						SELECT TOP 1 te.Id AS [EventId], 0 AS [Priority]
						FROM ODS.dbo.TrackingEvent te (NOLOCK)
						JOIN ODS.dbo.Label l (NOLOCK)
						ON te.LabelId = l.Id 
						WHERE (l.LabelNumber IN
							(
								SELECT DISTINCT LinkLabelNumber AS [TheLabel]
								FROM #LinkLabels
								WHERE LabelNumber = @lbl 
								AND LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) != ''
								UNION ALL
								SELECT @lbl AS [TheLabel]
							)
						) 
						AND te.EventTypeId IN
						(
							'47CFA05F-3897-4F1F-BDF4-00C6A69152E3', --delivered
							'41A8F8F9-D57E-40F0-9D9D-97767AC3069E', --delivery
							'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' --attempt
						)
						ORDER BY te.EventDateTime ASC
						UNION 
						SELECT TOP 1 te.Id AS [EventId], 1 AS [Priority]
						FROM ODS.dbo.TrackingEvent te (NOLOCK)
						JOIN ODS.dbo.Driver d (NOLOCK)
						ON te.DriverId = d.Id 
						JOIN ODS.dbo.Label l (NOLOCK)
						ON te.LabelId = l.Id 
						WHERE (l.LabelNumber IN
							(
								SELECT DISTINCT LinkLabelNumber AS [TheLabel]
								FROM #LinkLabels
								WHERE LabelNumber = @lbl 
								AND LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) != ''
								UNION ALL
								SELECT @lbl AS [TheLabel]
							)
						) 
						AND
						(
							(te.EventTypeId IN
								(
									-- on-board scan by a contractor takes priority over transfer/handover
									'93B2E381-6A89-4F2E-9131-2DC2FB300941' --on-board
								)
								AND ISNULL(d.IsContractor, 0) = 1
							)
						)
						ORDER BY te.EventDateTime ASC
						UNION 
						SELECT TOP 1 te.Id AS [EventId], 3 AS [Priority]
						FROM ODS.dbo.TrackingEvent te (NOLOCK)
						JOIN ODS.dbo.Driver d (NOLOCK)
						ON te.DriverId = d.Id 
						JOIN ODS.dbo.Label l (NOLOCK)
						ON te.LabelId = l.Id 
						WHERE (l.LabelNumber IN
							(
								SELECT DISTINCT LinkLabelNumber AS [TheLabel]
								FROM #LinkLabels
								WHERE LabelNumber = @lbl 
								AND LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) != ''
								UNION ALL
								SELECT @lbl AS [TheLabel]
							)
						) 
						AND
						(
							(te.EventTypeId IN
							(
								--'B8D04A85-A65B-41EA-9056-A950BE2CB509', --in-depot
								--'F47CABB2-55AA-4F19-B5EE-C2754268D1AF', --consolidate
								--'93B2E381-6A89-4F2E-9131-2DC2FB300941', --on-board
								'1A35AB45-B82A-492A-A1E8-6415BF846C75', --handover
								'E293FFDE-76E3-4E69-BCEB-473F91B4350C' --transfer
							))
							OR
							-- we'll take an "on-board" scan by a non-contractor as an "in-depot" scan
							(
								te.EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' --on-board ("accept in-depot")
								AND ISNULL(d.IsContractor, 0) = 0 --not a contractor (agent or depot)
							)
						)
						ORDER BY te.EventDateTime ASC
					)
					SELECT 
					TOP 1 @TrackingEventId = Ids.EventId
					FROM Ids
					ORDER BY Ids.[Priority] ASC;
					
					IF @TrackingEventId Is Not Null
					BEGIN
					
						SELECT
							@OtherScanBranch = UPPER(ISNULL(b.CosmosBranch, 'UNKNOWN')),
							@OtherScanDateTime = te.EventDateTime,
							@OtherScanDriver = d.Code,
							@OtherScanDepot = UPPER(ISNULL(dp.Name, 'UNKNOWN')),
							@OtherScanType = UPPER(ISNULL(et.[Description], 'UNKNOWN')),
							@OtherScanPronto = ISNULL(d.ProntoDriverCode, '')
						FROM ODS.dbo.TrackingEvent te (NOLOCK)
						JOIN ODS.dbo.Driver d (NOLOCK)
						ON te.DriverId = d.Id 
						JOIN ODS.dbo.Branch b (NOLOCK)
						ON d.BranchId = b.Id 
						JOIN ODS.dbo.EventType et (NOLOCK)
						ON te.EventTypeId = et.Id 
						LEFT OUTER JOIN ods.dbo.Depot dp (NOLOCK)
						ON d.DepotId = dp.Id 
						WHERE te.Id = @TrackingEventId;
						
						SELECT @OtherScanDetailsChanged = 1;
						
					END		-- @TrackingEventId Is Not Null

				END		-- @OtherScanDateTime Is Null AND @ItemAge < 4

				IF ISNULL(@ConsolidateBarcode, '') = ''
				BEGIN
					-- check labels for consolidation scan - this will tell us whether it's
					-- travelling interstate or not
					--
					SELECT
						TOP 1
							@ConsolidateBarcode = LTRIM(RTRIM(ISNULL(AdditionalText1, ''))),
							@ConsolidateDateTime = te.EventDateTime 
					FROM ODS.dbo.TrackingEvent te (NOLOCK)
					JOIN ODS.dbo.Label l (NOLOCK)
					ON te.LabelId = l.Id 
					WHERE (l.LabelNumber IN
						(
							SELECT DISTINCT LinkLabelNumber AS [TheLabel]
							FROM #LinkLabels
							WHERE LabelNumber = @lbl 
							AND LTRIM(RTRIM(ISNULL(LinkLabelNumber, ''))) != ''
							UNION ALL
							SELECT @lbl AS [TheLabel]
						)
					) 
					AND te.EventTypeId IN
					(
						'F47CABB2-55AA-4F19-B5EE-C2754268D1AF' --consolidate
					)
					--
					-- for "consolidate" events, we take the most recent one, as that should
					-- give us the correct information about the destination of the item
					--
					ORDER BY te.EventDateTime DESC;

				END		-- ISNULL(@ConsolidateBarcode, '') = ''

				IF ISNULL(@ConsolidateBarcode, '') != ''
				BEGIN
					SELECT @ConsolidateBarcode = LTRIM(RTRIM(@ConsolidateBarcode));
					
					IF ((LEN(@ConsolidateBarcode) = 14) AND (@ConsolidateBarcode LIKE 'CP[ABMOSPC][0123456789]%[ABMOSPC]'))
					BEGIN
						SELECT @ConsolidateOriginBranch = 
									CASE RIGHT(@ConsolidateBarcode, 1)
										WHEN 'A' THEN 'ADELAIDE'
										WHEN 'B' THEN 'BRISBANE'
										WHEN 'C' THEN 'CANBERRA'
										WHEN 'M' THEN 'MELBOURNE'
										WHEN 'O' THEN 'GOLDCOAST'
										WHEN 'S' THEN 'SYDNEY'
										WHEN 'P' THEN 'PERTH'
										ELSE ''
									END;
									
						SELECT @ConsolidateDestinationBranch = 
									CASE SUBSTRING(@ConsolidateBarcode, 3, 1)
										WHEN 'A' THEN 'ADELAIDE'
										WHEN 'B' THEN 'BRISBANE'
										WHEN 'C' THEN 'CANBERRA'
										WHEN 'M' THEN 'MELBOURNE'
										WHEN 'O' THEN 'GOLDCOAST'
										WHEN 'S' THEN 'SYDNEY'
										WHEN 'P' THEN 'PERTH'
										ELSE ''
									END;
					END
					
					SELECT @ConsolidateBarcodeChanged = 1;
				END

				IF ISNULL(@SkipRevenueRecognition, 0) = 0
				BEGIN
					--
					-- if there's no consignment details on this particular label, then we'll
					-- check if there are consignment details on one of the "linked" labels.
					-- We just take the first one we find, if there happen to be multiples
					--
					IF ((@conOriginState Is Null) AND (@conDestinationState Is Null))
					BEGIN
						IF EXISTS (SELECT 1 FROM PrepaidRevenueReporting prr (NOLOCK)
										JOIN #LinkLabels ll
										ON prr.LabelNumber = ll.LinkLabelNumber 
										WHERE prr.ConsignmentOriginState Is Not Null
										AND prr.ConsignmentDestinationState Is Not Null)
						BEGIN
							SELECT TOP 1
								@conOriginState = prr.ConsignmentOriginState,
								@conDestinationState = prr.ConsignmentDestinationState
							FROM PrepaidRevenueReporting prr
							JOIN #LinkLabels ll
							ON prr.LabelNumber = ll.LinkLabelNumber
							WHERE prr.ConsignmentOriginState Is Not Null
							AND prr.ConsignmentDestinationState Is Not Null;
						END
					END
				
					-- try and set origin / destination details
					--
					IF (ISNULL(@FirstScanBranch, '') != '')
					BEGIN
						SELECT @OriginBranch = @FirstScanBranch;
					END
					ELSE
					IF (ISNULL(@ConsolidateOriginBranch, '') != '')
					BEGIN
						SELECT @OriginBranch = @ConsolidateOriginBranch;
					END
					ELSE
					IF (ISNULL(@conOriginState, '') != '')
					BEGIN
						SELECT @OriginBranch = CASE @conOriginState 
							WHEN 'WA' THEN 'PERTH'
							WHEN 'VIC' THEN 'MELBOURNE'
							WHEN 'TAS' THEN 'MELBOURNE'
							WHEN 'SA' THEN 'ADELAIDE'
							WHEN 'NT' THEN 'ADELAIDE'
							WHEN 'QLD' THEN 'BRISBANE'
							WHEN 'NSW' THEN 'SYDNEY'
							WHEN 'ACT' THEN 'SYDNEY'
							ELSE 'UNKNOWN'
						END;
					END
					ELSE
					BEGIN
						SELECT @OriginBranch = 'UNKNOWN';
					END

					IF (ISNULL(@FirstScanDepot, '') != '')
					BEGIN
						SELECT @OriginDepot = @FirstScanDepot;
					END
					ELSE
					BEGIN
						SELECT @OriginDepot = 'UNKNOWN';
					END

					IF (ISNULL(@ConsolidateDestinationBranch, '') != '')
					BEGIN
						SELECT @DestinationBranch = @ConsolidateDestinationBranch;
					END
					ELSE
					IF (ISNULL(@OtherScanBranch, '') != '')
					BEGIN
						SELECT @DestinationBranch = @OtherScanBranch;
					END
					ELSE
					IF (ISNULL(@conDestinationState, '') != '')
					BEGIN
						SELECT @DestinationBranch = CASE @conDestinationState 
							WHEN 'WA' THEN 'PERTH'
							WHEN 'VIC' THEN 'MELBOURNE'
							WHEN 'TAS' THEN 'MELBOURNE'
							WHEN 'SA' THEN 'ADELAIDE'
							WHEN 'NT' THEN 'ADELAIDE'
							WHEN 'QLD' THEN 'BRISBANE'
							WHEN 'NSW' THEN 'SYDNEY'
							WHEN 'ACT' THEN 'SYDNEY'
							ELSE 'UNKNOWN'
						END;
					END
					ELSE
					BEGIN
						SELECT @DestinationBranch = 'UNKNOWN';
					END

					IF (ISNULL(@OtherScanDepot, '') != '')
					BEGIN
						SELECT @DestinationDepot = @OtherScanDepot;
					END
					ELSE
					BEGIN
						SELECT @DestinationDepot = 'UNKNOWN';
					END
					
					/* end of setting origin / destination details */
				END

				/* either we have enough information to set the relevant properties for revenue recognition,
					or this particular record has gone past the threshold days, in which case we process it */
				--
				IF
				(
					((ISNULL(@SkipRevenueRecognition, 0) = 0)
						AND (@RevenueAmount Is Not Null))
					AND
					(
						(
							(LTRIM(RTRIM(ISNULL(@FirstScanBranch, ''))) != '')
							AND
							(LTRIM(RTRIM(ISNULL(@FirstScanDepot, ''))) != '')
							AND
							(
								((LTRIM(RTRIM(ISNULL(@OtherScanBranch, ''))) != '')
								AND 
								(LTRIM(RTRIM(ISNULL(@OtherScanDepot, ''))) != ''))
								OR
								((LTRIM(RTRIM(ISNULL(@conDestinationState, ''))) != '')
								AND (ISNULL(@ConsignmentCustomer, '') LIKE '%cppl%'))
								OR
								(ISNULL(@ConsolidateDestinationBranch, '') != '')
							)
						)
						OR
						(
							(LTRIM(RTRIM(ISNULL(@OtherScanBranch, ''))) != '')
							AND
							(LTRIM(RTRIM(ISNULL(@OtherScanDepot, ''))) != '')
							AND
							(
								((LTRIM(RTRIM(ISNULL(@conOriginState, ''))) != '')
								AND (ISNULL(@ConsignmentCustomer, '') LIKE '%cppl%'))
								OR
								(ISNULL(@ConsolidateOriginBranch, '') != '')
							)
						)
						OR
						(
							(((LTRIM(RTRIM(ISNULL(@conDestinationState, ''))) != '')
								AND (ISNULL(@ConsignmentCustomer, '') LIKE '%cppl%'))
								OR (ISNULL(@ConsolidateDestinationBranch, '') != ''))
							AND
							(((LTRIM(RTRIM(ISNULL(@conOriginState, ''))) != '')
								AND (ISNULL(@ConsignmentCustomer, '') LIKE '%cppl%'))
								OR (ISNULL(@ConsolidateOriginBranch, '') != ''))
							--AND
							--(ISNULL(@ConsignmentCustomer, '') LIKE '%cppl%')
						)
						OR
						(ISNULL(@ItemAge, 0) > @RevenueRecognitionThresholdDays)
					)
				)
				BEGIN

					--
					-- for historical processing, revenue gets allocated based on the scan events,
					-- or just defaults to the day the label was first imported
					--
					-- (we'll assume it's historical processing if the "end date" parameter
					--  is more than 5 days' ago.  As at 5th March, prepaid revenue records have
					--  been processed for July 2012 -> Jan 2013
					--
					IF (ISNULL(@EndDate, '1jan2099') < CONVERT(date, (DATEADD(day, -5, GETDATE()))))
					BEGIN
						IF @FirstScanDateTime Is Not Null
						BEGIN
							SELECT @RevRecDate = CONVERT(date, @FirstScanDateTime);
						END
						ELSE
						BEGIN
							IF (@CreatedDate Is Not Null)
							BEGIN
								SELECT @RevRecDate = CONVERT(date, @CreatedDate);
							END
							ELSE
							IF @OtherScanDateTime Is Not Null
							BEGIN
								SELECT @RevRecDate = CONVERT(date, @OtherScanDateTime);
							END
						END
					END

					-- default yesterday for anything else that hasn't had a recognition date set
					IF @RevRecDate Is Null
					BEGIN
						SELECT @RevRecDate = CONVERT(date, DATEADD(day, -1, GETDATE()));
					END;
					
					IF LTRIM(RTRIM(ISNULL(@ServiceCategory, ''))) = ''
					BEGIN
						-- service category couldn't be determined previously, so we'll drop in to parcels
						SELECT @ServiceCategory = 'PARCELS';
					END

					--
					-- set BU details
					--
					SELECT @BUCode = dbo.CPPL_fn_GetBusinessUnitCodeForPrepaidRevenue(
															@ProntoOrderContractorCode,
															@ReturnsFlag,
															@FirstScanBranch,
															@FirstScanDepot,
															@OtherScanBranch,
															@OtherScanDepot,
															LEFT(LTRIM(RTRIM(@lbl)), 3),
															@ConsolidateOriginBranch,
															@ConsolidateDestinationBranch,
															@conOriginState,
															@conDestinationState
						);
					
					IF (ISNULL(@BUCode, '') = '')
					BEGIN
						SELECT @BUCode = 'UNKNOWN';
					END
					
					SELECT @BU = CASE @BUCode
						WHEN 'CAD' THEN 'ADELAIDE'
						WHEN 'CCB' THEN 'CANBERRA'
						WHEN 'CBN' THEN 'BRISBANE'
						WHEN 'CSC' THEN 'SUNSHINE COAST'
						WHEN 'CSY' THEN 'SYDNEY'
						WHEN 'CME' THEN 'MELBOURNE'
						WHEN 'CCC' THEN 'CENTRAL COAST'
						WHEN 'COO' THEN 'GOLD COAST'
						WHEN 'CPE' THEN 'PERTH'
						ELSE 'UNKNOWN'
					END;
					
					-- need to determine the network category
					--
					SELECT @NetworkCategory = Null;
					
					-- if there's a consolidation barcode, then we assume it's interstate, except for Sydney-Canberra transfer
					--
					IF ((ISNULL(@ConsolidateDestinationBranch, '') != '') AND 
							(ISNULL(@ConsolidateOriginBranch, '') != ''))
					BEGIN
						IF ((@ConsolidateDestinationBranch IN ('SYDNEY','CANBERRA'))
								AND (@ConsolidateOriginBranch IN ('SYDNEY','CANBERRA')))
						BEGIN
							SELECT @NetworkCategory = Null; --don't categorise Sydney-Canberra yet
						END
						ELSE
						BEGIN
							SELECT @NetworkCategory = 'I'; --interstate
						END
					END
					
					IF ISNULL(@NetworkCategory, '') = ''
					BEGIN
						SELECT @NetworkCategory = dbo.CPPL_fn_GetNetworkCategoryForPrepaidRevenue 
								(
									@FirstScanBranch,
									@FirstScanDepot,
									@conOriginState,
									@OtherScanBranch,
									@OtherScanDepot,
									@conDestinationState,
									LEFT(LTRIM(RTRIM(@lbl)), 3),
									@CouponType
						);
					END
					
					IF ISNULL(@NetworkCategory, '') = ''
					BEGIN
						-- something's incomplete, so it drops into 'UNKNOWN'
						SELECT @NetworkCategory = 'X';
					END
					
					-- update the record
					UPDATE PrepaidRevenueReporting 
					SET FirstScanContractorCode = @FirstScanPronto,
						FirstScanDateTime = @FirstScanDateTime,
						FirstScanDepot = @FirstScanDepot,
						FirstScanDriverBranch = @FirstScanBranch,
						FirstScanDriverNumber = @FirstScanDriver,
						FirstScanType = @FirstScanType,
						OtherScanContractorCode = @OtherScanPronto,
						OtherScanDateTime = @OtherScanDateTime,
						OtherScanDepot = @OtherScanDepot,
						OtherScanDriverBranch = @OtherScanBranch,
						OtherScanDriverNumber = @OtherScanDriver,
						OtherScanType = @OtherScanType,
						ServiceCategory = @ServiceCategory,
						RevenueProcessedDateTime = GETDATE(),
						RevenueRecognisedDate = @RevRecDate,
						BU = @BU,
						BUCode = @BUCode,
						IsProcessed = 1,
						NetworkCategory = @NetworkCategory,
						ConsolidateBarcode = ISNULL(@ConsolidateBarcode, ''),
						ConsolidateScanDateTime = @ConsolidateDateTime
					WHERE LabelNumber = @lbl;
					
				END		-- (LTRIM(RTRIM(ISNULL(@FirstScanBranch, ''))) != '') ...
				ELSE
				BEGIN
				
					-- we can update the information that we do already know
					IF (@FirstScanDateTime Is Not Null AND ISNULL(@FirstScanDetailsChanged, 0) = 1)
							OR (@OtherScanDateTime Is Not Null AND ISNULL(@OtherScanDetailsChanged, 0) = 1)
							OR (@ServiceCategory Is Not Null AND ISNULL(@ServiceCategoryChanged, 0) = 1)
							OR (ISNULL(@ConsolidateBarcodeChanged, 0) = 1)
					BEGIN

						UPDATE PrepaidRevenueReporting
						SET FirstScanContractorCode = @FirstScanPronto,
							FirstScanDateTime = @FirstScanDateTime,
							FirstScanDepot = @FirstScanDepot,
							FirstScanDriverBranch = @FirstScanBranch,
							FirstScanDriverNumber = @FirstScanDriver,
							FirstScanType = @FirstScanType,
							OtherScanContractorCode = @OtherScanPronto,
							OtherScanDateTime = @OtherScanDateTime,
							OtherScanDepot = @OtherScanDepot,
							OtherScanDriverBranch = @OtherScanBranch,
							OtherScanDriverNumber = @OtherScanDriver,
							OtherScanType = @OtherScanType,
							ServiceCategory = @ServiceCategory,
							ConsolidateBarcode = ISNULL(@ConsolidateBarcode, ''),
							ConsolidateScanDateTime = @ConsolidateDateTime
						WHERE LabelNumber = @lbl;
					
					END		-- (@FirstScanDateTime Is Not Null AND @FirstScanDetailsChanged = 1) ...
				
				END		-- (LTRIM(RTRIM(ISNULL(@FirstScanBranch, ''))) != '') ...
				
			END		-- @FirstScanDateTime Is Null AND @ItemAge < 2

		END		-- (LTRIM(RTRIM(ISNULL(@lbl, ''))) != '')
		
		FETCH NEXT FROM @curRev INTO @lbl;
		
	END		-- WHILE ((@@FETCH_STATUS = 0) AND (@counter < 500))

	CLOSE @curRev;
	DEALLOCATE @curRev;


	--DROP TABLE #LinkLabels;


	SET NOCOUNT OFF;


END
GO
