SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[Z_CPPL_RPT_COSMOSCustomers withNoProntoID]
	
AS
BEGIN
  SET NOCOUNT ON;

 SELECT  [Branch]
      ,[CustomerId]
      ,[Code]
      ,[Name]
      ,CONVERT(varchar(20),'') as ExistInOtherBranch
      ,CONVERT(varchar(20),'') as ExistInBranch
      ,CONVERT(varchar(20),'') as ProntoAccountCodefromOtherBranch
       ,[ProntoAccountCode]
      ,[DeliveryAddress1]
      ,[DeliverySuburb]
      ,[Contact1]
      ,[EmailAddress1]
      ,[Phone1]
      ,[DriverNumber]
      ,[DepotNumber]
      ,[Startdate]
      ,[Lastused1]
      ,[Lastused2]
      ,[RegularRun]
      ,[PickupDays]
      ,[Status]
      ,[AddedWhen]
      ,CONVERT(varchar(20),'') as PossibelProntoID
      ,CONVERT(varchar(50),'') as CustomerAddress1
      ,CONVERT(varchar(50),'') as PhoneNumber
 Into #TempFinal   
 FROM 
 [Cosmos].[dbo].[Customer] Where IsActive =1 and isdeleted=0 and  (Coupons1 is not null or Coupons2 is not null 
 or Coupons3 is not null or Coupons4 is not null 
 or Coupons5 is not null or Coupons6 is not null 
 or Coupons7 is not null or Coupons8 is not null )
 and [ProntoAccountCode] is   null and Lastused1 > '2013-11-10' and Code <> 'EDI'
  and Coupons1 not in ('17','18')
  
  
  Update #TempFinal SET ExistInOtherBranch = 'YES' ,ExistInBranch = C.Branch,PRontoAccountCodefromOtherBranch = C.ProntoAccountCode
           From  #TempFinal T Join [Cosmos].[dbo].[Customer] C on T.Code = C.Code and C.ProntoAccountCode is not null and C.IsActive =1 
 
  
 
 Delete from #TempFinal WHere Branch = 'Brisbane' and ExistInBranch = 'Goldcoast' and PRontoAccountCodefromOtherBranch not like 'ZCUS%'
 Delete from #TempFinal WHere Branch = 'Goldcoast' and ExistInBranch = 'Brisbane' and PRontoAccountCodefromOtherBranch not like 'ZCUS%'
 
 
  Select [COLUMN 0]  as AccountCode,[COLUMN 2] as CustName , [COLUMN 3] as CustomerAddress1, [COLUMN 4] as CustomerAddress2 
       ,[COLUMN 11] as PhoneNumber, CONVERT(Varchar(20),[COLUMN 11]) as PhoneNumberFormatted
 into #Temp1
 from Revenue.dbo.name_data WHere [Column 1] = 'C'
 
  Update #Temp1 SET  PhoneNumberFormatted = Rtrim(Ltrim(REPLACE(PhoneNumberFormatted,' ','')))  WHere LEN(PhoneNumberFormatted) <> 10
  Update #Temp1 SET  PhoneNumberFormatted = Rtrim(Ltrim(REPLACE(PhoneNumberFormatted,'-','')))  WHere LEN(PhoneNumberFormatted) <> 10
  Update #Temp1 SET  PhoneNumberFormatted = Rtrim(Ltrim(REPLACE(PhoneNumberFormatted,'.','')))  WHere LEN(PhoneNumberFormatted) <> 10
  Update #Temp1 SET  PhoneNumberFormatted = Rtrim(Ltrim(REPLACE(PhoneNumberFormatted,'(','')))  WHere LEN(PhoneNumberFormatted) <> 10
  Update #Temp1 SET  PhoneNumberFormatted = Rtrim(Ltrim(REPLACE(PhoneNumberFormatted,')','')))  WHere LEN(PhoneNumberFormatted) <> 10
  Update #Temp1 SET PhoneNumberFormatted = '0'+ PhoneNumberFormatted WHere LEN(PhoneNumberFormatted) = 9 and PhoneNumberFormatted like'4%'
  
  Update #Temp1 SET  PhoneNumberFormatted = Case Left(RTRIM(Ltrim(PhoneNumberFormatted)),2)  WHEN '04' Then  RTRIM(Ltrim(PhoneNumberFormatted)) 
																							 WHEN '02' THEN RTRIM(Ltrim(REPLACE(PhoneNumberFormatted,'02','')))
																							 WHEN '03' THEN RTRIM(Ltrim(REPLACE(PhoneNumberFormatted,'03','')))
																							 WHEN '08' THEN RTRIM(Ltrim(REPLACE(PhoneNumberFormatted,'08','')))
																							 WHEN '07' THEN RTRIM(Ltrim(REPLACE(PhoneNumberFormatted,'07','')))
																							 ELSE  RTRIM(Ltrim(PhoneNumberFormatted)) END 
                                                     
                                               WHERE LEN(PhoneNumberFormatted) = 10 and ISNUMERIC(PhoneNumberFormatted ) = 1
                                               
                                               
 Delete from #TempFinal  WHere (Branch = 'GoldCoast' and  ExistInBranch = 'Brisbane') OR  (Branch = 'Brisbane' and  ExistInBranch = 'Goldcoast')  and PRontoAccountCodefromOtherBranch not like 'ZCUS%'
 
 Update #TempFinal SET    PossibelProntoID =  ( Select Top 1 T1.AccountCode  From  #Temp1  T1 Where  #TempFinal.Code = T1.PhoneNumberFormatted )
 Update #TempFinal SET    CustomerAddress1 =  ( Select Top 1 T1.CustomerAddress1  From  #Temp1  T1 Where  #TempFinal.Code = T1.PhoneNumberFormatted )
 Update #TempFinal SET    PhoneNumber =  ( Select Top 1 T1.PhoneNumber  From  #Temp1  T1 Where  #TempFinal.Code = T1.PhoneNumberFormatted )
                               
  --Select [Branch]
  --      ,[Code]
  --      ,[Name]
  --      ,[DeliveryAddress1]
  --      ,[DriverNumber]
  --      ,PossibelProntoID
  --      ,CustomerAddress1
  --      ,PhoneNumber
  Select * from #TempFinal where code<>'Ezysend'
                                                                      
   SET NOCOUNT OFF;
  

END

GO
