SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[Z_CPPL_RPT_InterstatePrepaidRedemptions]
(
	@Year		int,
	@Month		int,
	@BU  varchar(20),
	@Activity Varchar(20)
)
AS
BEGIN
  SET NOCOUNT ON;

   Select CouponPrefix,
         CASE BranchCode WHEN 'CSY' THEN 'SYDNEY' WHEN 'CBN' THEN 'BRISBANE' WHEN 'COO' THEN 'GOLDCOAST' WHEN 'CME' THEN 'MELBOURNE' WHEN 'CAD' THEN 'ADELAIDE' ELSE '' END as Branch,
         ContractorCode,
         RedemptionAmount
      Into #TempLookup
      from RDS.dbo.ProntoStockChargesOverride  where RedemptionType = @Activity
          
	Select LabelPrefix,
	       CouponType,
	       RevenueAmount,
	       IsNull(OtherScanContractorCode,'') as DestinationContractor,
	       DestinationBranch,
	       DestinationDepot,
	       CONVERT(decimal(12,2),0) as PickUpRedemption,
	       CONVERT(decimal(12,2),0) as DeliveryRedemption
	    into #TempFinal  
	     from  Revenue.dbo.v_PrepaidRevenueReporting 
	     Where NetworkCategory = 'I' 
	            
	           and (Datepart(year,RevenueRecognisedDate)*100) + Datepart(Month,RevenueRecognisedDate)  = (@Year*100) +    @Month
	           and BU= @BU
	           and CouponType not in ('LINK','ATL','REDELIVERY CARD','IRP TRK','RETURN TRK') 

if @Activity = 'D'
BEGIN	
   
  Update #TempFinal SET  DeliveryRedemption = TL.RedemptionAmount
         From #TempFinal TF Join  RDS.dbo.ProntoStockChargesOverride  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.DestinationBranch = CASE TL.BranchCode WHEN 'CSY' THEN 'SYDNEY' WHEN 'CBN' THEN 'BRISBANE' WHEN 'COO' THEN 'GOLDCOAST' WHEN 'CME' THEN 'MELBOURNE' WHEN 'CAD' THEN 'ADELAIDE' ELSE '' END
         and TF.DestinationContractor = TL.ContractorCode
         and TF.DeliveryRedemption = 0 and TL.RedemptionType = 'D'
         
    Update #TempFinal SET  DeliveryRedemption = TL.RedemptionAmount
         From #TempFinal TF Join RDS.dbo.ProntoStockChargesOverride   TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.DestinationBranch = CASE TL.BranchCode WHEN 'CSY' THEN 'SYDNEY' WHEN 'CBN' THEN 'BRISBANE' WHEN 'COO' THEN 'GOLDCOAST' WHEN 'CME' THEN 'MELBOURNE' WHEN 'CAD' THEN 'ADELAIDE' ELSE '' END
         and TF.DeliveryRedemption = 0 and TL.RedemptionType = 'D'
         
      Update #TempFinal SET  DeliveryRedemption = TL.RedemptionAmount
         From #TempFinal TF Join  RDS.dbo.ProntoStockChargesOverride  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.DeliveryRedemption = 0 and TL.RedemptionType = 'D'
   
END
ELSE
BEGIN
   
  Update #TempFinal SET  PickUpRedemption  = TL.RedemptionAmount
         From #TempFinal TF Join  RDS.dbo.ProntoStockChargesOverride  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.Pic = CASE TL.BranchCode WHEN 'CSY' THEN 'SYDNEY' WHEN 'CBN' THEN 'BRISBANE' WHEN 'COO' THEN 'GOLDCOAST' WHEN 'CME' THEN 'MELBOURNE' WHEN 'CAD' THEN 'ADELAIDE' ELSE '' END
         and TF.DestinationContractor = TL.ContractorCode
         and TF.PickUpRedemption  = 0 and TL.RedemptionType = 'P'
         
    Update #TempFinal SET  PickUpRedemption = TL.RedemptionAmount
         From #TempFinal TF Join RDS.dbo.ProntoStockChargesOverride   TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.DestinationBranch = CASE TL.BranchCode WHEN 'CSY' THEN 'SYDNEY' WHEN 'CBN' THEN 'BRISBANE' WHEN 'COO' THEN 'GOLDCOAST' WHEN 'CME' THEN 'MELBOURNE' WHEN 'CAD' THEN 'ADELAIDE' ELSE '' END
         and TF.PickUpRedemption = 0 and TL.RedemptionType = 'P'
         
      Update #TempFinal SET  PickUpRedemption = TL.RedemptionAmount
         From #TempFinal TF Join  RDS.dbo.ProntoStockChargesOverride  TL on  TF.LabelPrefix = TL.CouponPrefix 
         and TF.PickUpRedemption = 0 and TL.RedemptionType = 'P'
   
 END
 
  Select (@Year*100) + @Month As YearMonth , 
          @BU as BusinessUnit ,
          DestinationBranch,
          DestinationDepot ,
          CouponType,
          COUNT(*) as CouponCount,
          SUM(RevenueAmount)as RevenueAmount,
          CASE @Activity WHEN 'P' THEN SUM(PickUpRedemption)  WHEN 'D' THEN  SUM(DeliveryRedemption) END as Redemption
          
             from #TempFinal 
             Group By   CouponType,DestinationBranch,DestinationDepot
             order by DestinationDepot asc, CouponType asc
   
  
  
                                                                      
   SET NOCOUNT OFF;
  

END

GO
