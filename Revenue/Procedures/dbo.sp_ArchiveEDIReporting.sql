SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveEDIReporting]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--637
    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min([ConsignmentDate]) from [dbo].[EdiRevenueReporting]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 200,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[EdiRevenueReporting_Archive]
	SELECT [FYear]
      ,[FYearSort]
      ,[AcctMonth]
      ,[AcctMonthSort]
      ,[AcctWeek]
      ,[AcctWeekSort]
      ,[ConsignmentDate]
      ,[AccountCode]
      ,[AccountName]
      ,[BUCode]
      ,[BU]
      ,[NWCategory]
      ,[ServCategory]
      ,[FreightCharge]
      ,[InsuranceCharge]
      ,[InsuranceCategory]
      ,[FuelSurcharge]
      ,[StateCode]
      ,[ConsignmentCount]
      ,[DeadWeightTotal]
      ,[CubicWeightTotal]
      ,[ChargeWeightTotal]
      ,[ItemCount]
      ,[OriginState]
      ,[DestinationState]
      ,[PickupCost]
      ,[DeliveryCost]
      ,[OriginZone]
      ,[DestinationZone]
      ,[OriginLaneType]
      ,[DestinationLaneType]
	   from [dbo].[EdiRevenueReporting] (nolock) where [ConsignmentDate] >= @MinDate and  [ConsignmentDate] <= DATEADD(day, 1,@MinDate)


	DELETE FROM [dbo].[EdiRevenueReporting] where  [ConsignmentDate] >= @MinDate and  [ConsignmentDate] <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END



GO
