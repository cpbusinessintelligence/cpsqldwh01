SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create proc sp_ArchiveEDIRevenueReportingSS (@ConsignmentDate Date)
as begin

BEGIN TRANSACTION [EdiRevenueReporting]

INSERT INTO [dbo].[EdiRevenueReporting_Archive]
SELECT * FROM DBO.[EdiRevenueReporting]
WHERE ConsignmentDate <= @ConsignmentDate

Delete from DBO.[EdiRevenueReporting]
WHERE ConsignmentDate <= @ConsignmentDate

Commit Transaction [EdiRevenueReporting]

End
GO
