SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_IntRecharge_EDIDetail_SS (@CreatedDate DateTime)
as

Begin transaction PerfRep6

Insert into [dbo].[IntRecharge_EDIDetail_Archive]
select * from [dbo].[IntRecharge_EDIDetail]
where [CreatedDate] < = @CreatedDate

Delete from [dbo].[IntRecharge_EDIDetail]
where [CreatedDate] < = @CreatedDate


Commit transaction PerfRep6

GO
