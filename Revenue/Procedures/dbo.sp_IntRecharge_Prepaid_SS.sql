SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Proc sp_IntRecharge_Prepaid_SS (@CreatedDate DateTime)
as

Begin transaction PerfRep7

Insert into [dbo].[IntRecharge_PrepaidDetails_Archive]
select * from [dbo].[IntRecharge_PrepaidDetails]
where [CreatedDate] < = @CreatedDate

Delete from [dbo].[IntRecharge_PrepaidDetails]
where [CreatedDate] < = @CreatedDate


Commit transaction PerfRep7

GO
