SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Praveen Valappil
-- Create date: 2020-12-16
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptProntoActivityError_Status]
	-- Add the parameters for the stored procedure here
	--@ReportType varchar(20),
	--@SerialNumber varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Select 
	--AE.*,PCD.LastSoldDate
	--, Case When LastSoldDate Is Null Then 'Check' ELse 'Release' End As [Status]  
	--, Revenue.[dbo].[CPPL_fn_GetCouponBranchFromPrefix](LEFT(AE.SerialNumber,3)) 
	--From [ActivityErrorImport] AE
	--Left Join [ProntoPurge_Bkup_Do_Not Delete]..[ProntoCouponDetails_bkup_12062020_do_not_delete] PCD On AE.SerialNumber = PCD.SerialNumber And PCD.LastSoldDate < '2018-01-01'
	
	Select 			
		AE.*
		, LastSoldDate
		, Case When LastSoldDate Is Null Then 'Check' ELse 'Release' End As [Status]
		, 'Coupon Not Found' [Reason]
		,case when PickupDate is null or DeliveryDate is null then 'Unused' else 'Used' end [Used/Unused]
		,case when left(Serialwhsecode,1)='S' then 'Sydney'
				when left(Serialwhsecode,1)='M' then 'Melbourne'
				when left(Serialwhsecode,1)='B' then 'Brisbane'
				when left(Serialwhsecode,1)='G' then 'Goldcoast'
				when left(Serialwhsecode,1)='A' then 'Adelaide'
				end
				[Branch]
				,SerialStatus,
				Case when SerialStatus=04 then 'Proforma Order'
					when serialstatus=05 then 'Transfer Hold Apprvl'
					when serialstatus=70 then 'Rdy to Print Invoice'
					when serialstatus=90 then 'Finished'
					when serialstatus=99 then 'CANCELLED'
				end [Serial Status Description]


	From [ActivityErrorImport] AE
	Join [ProntoPurge_Bkup_Do_Not Delete]..[ProntoCouponDetails_bkup_12062020_do_not_delete] PCD On AE.SerialNumber = PCD.SerialNumber And PCD.LastSoldDate < '2018-01-01'
	Join [ProntoPurge_Bkup_Do_Not Delete].[dbo].[ProntoStockSerialNumber] ss on ss.SerialNumber=AE.SerialNumber
END

--select * from [ProntoPurge_Bkup_Do_Not Delete].[dbo].[ProntoStockSerialNumber]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptProntoActivityError_Status]
	TO [ReportUser]
GO
