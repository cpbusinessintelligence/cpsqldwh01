SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



Create Procedure sp_Tuesday_Job_Automated
As
Begin	
	/***Tuesday Job Automated by PV**/
	---------------------------------------------------------------
	--Use [Revenue]

	Declare @InsQry1 Varchar(1000) ='', @InsQry2 Varchar(1000) ='', @DelQry1 Varchar(1000) ='', @DelQry2 Varchar(1000) =''
	Declare @Date Varchar(10) = Convert(varchar,GETDATE(),112)
	Declare @FirstDateOfCurrentMonth Date = Dateadd(m,Datediff(M,0,GETDATE()),0)

	SET @InsQry1 = 'select * into PrepaidRevenueReporting_weekbackup'+@Date+' from PrepaidRevenueReporting where Convert(date,createddate) >= ''' + Convert(Varchar,@FirstDateOfCurrentMonth) + ''''
	SET @InsQry2 = 'select * into PrepaidRevenueReportingExceptions_weekbackup'+@Date+' from PrepaidRevenueReportingExceptions where Convert(date,createddate) >= ''' + Convert(varchar,@FirstDateOfCurrentMonth) + ''''
	Print (@InsQry1)
	Print (@InsQry2)
	EXEC (@InsQry1)
	EXEC (@InsQry2)
	SET @DelQry1 = 'delete from PrepaidRevenueReporting where Convert(date,createddate) >= ''' + Convert(Varchar,@FirstDateOfCurrentMonth) + ''''
	SET @DelQry2 = 'delete from PrepaidRevenueReportingExceptions where Convert(date,createddate) >= ''' + Convert(Varchar,@FirstDateOfCurrentMonth) + ''''
	Print (@DelQry1)
	Print (@DelQry2)
	EXEC (@DelQry1)
	EXEC (@DelQry2)
	

	
	Declare @LastDayOfPreviousMonth Varchar(20) = FORMAT(Dateadd(d,-1,Dateadd(m,Datediff(M,0,Getdate()),0)), 'dd MMM yyyy')
	update [Revenue].[dbo].[InterfaceConfig] set value= @LastDayOfPreviousMonth where name='LastPrepaidRevenueReportingLabelCreatedDate'
	SELECT * FROM [Revenue].[dbo].[InterfaceConfig] Where name='LastPrepaidRevenueReportingLabelCreatedDate'
	

	
	Exec [dbo].[CPPL_PopulateNewPrepaidRevenueReportingLabels]
	
	Declare @ParamDate Date
	Declare @RunDate Date = Getdate()
	Declare @FirstDateOfCurrentMonth1 Date = Dateadd(m,Datediff(M,0,@RunDate),0)
	Declare @datediff int = Datediff(d,@FirstDateOfCurrentMonth1,@RunDate)+1
	Declare @Count int =1

	SET @ParamDate = @FirstDateOfCurrentMonth
	While @Count <= @datediff
	Begin
		Print 'Started : ' + Convert(varchar,@ParamDate)
		exec [dbo].[CPPL_ProcessPrepaidRevenueRecords] @ParamDate,@ParamDate,0
		Set @ParamDate = DATEADD(D,1,@ParamDate)
		Set @Count = @Count + 1
	End
	
	EXEC msdb.dbo.sp_start_job N'Poplulate PrePaid Revenue Tables and Process Revenue Cube' 

End

GO
