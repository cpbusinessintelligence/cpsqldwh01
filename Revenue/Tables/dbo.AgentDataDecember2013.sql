SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentDataDecember2013] (
		[Column 0]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConDate]       [date] NULL,
		[InGateway]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RevDate]       [date] NULL
)
GO
ALTER TABLE [dbo].[AgentDataDecember2013] SET (LOCK_ESCALATION = TABLE)
GO
