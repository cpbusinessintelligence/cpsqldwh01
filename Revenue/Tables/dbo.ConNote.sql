SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConNote] (
		[CON]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ScanDate]     [datetime] NULL,
		[ScanType]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ConNote] SET (LOCK_ESCALATION = TABLE)
GO
