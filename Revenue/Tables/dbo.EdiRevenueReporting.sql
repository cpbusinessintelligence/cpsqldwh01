SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EdiRevenueReporting] (
		[FYear]                   [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[FYearSort]               [smallint] NOT NULL,
		[AcctMonth]               [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[AcctMonthSort]           [int] NOT NULL,
		[AcctWeek]                [varchar](12) COLLATE Latin1_General_CI_AS NOT NULL,
		[AcctWeekSort]            [int] NOT NULL,
		[ConsignmentDate]         [date] NOT NULL,
		[AccountCode]             [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[BUCode]                  [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[BU]                      [varchar](25) COLLATE Latin1_General_CI_AS NOT NULL,
		[NWCategory]              [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[ServCategory]            [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[FreightCharge]           [decimal](18, 3) NOT NULL,
		[InsuranceCharge]         [decimal](18, 3) NOT NULL,
		[InsuranceCategory]       [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[FuelSurcharge]           [decimal](18, 3) NOT NULL,
		[StateCode]               [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentCount]        [int] NULL,
		[DeadWeightTotal]         [decimal](18, 2) NULL,
		[CubicWeightTotal]        [decimal](18, 2) NULL,
		[ChargeWeightTotal]       [decimal](18, 2) NULL,
		[ItemCount]               [int] NULL,
		[OriginState]             [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationState]        [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[PickupCost]              [decimal](18, 3) NULL,
		[DeliveryCost]            [decimal](18, 3) NULL,
		[OriginZone]              [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationZone]         [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[OriginLaneType]          [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[DestinationLaneType]     [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_EdiRevenueReporting]
		PRIMARY KEY
		CLUSTERED
		([AcctMonth], [AcctWeek], [ConsignmentDate], [AccountCode], [BUCode], [NWCategory], [ServCategory], [InsuranceCategory], [OriginState], [DestinationState], [OriginZone], [DestinationZone], [OriginLaneType], [DestinationLaneType])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[EdiRevenueReporting] SET (LOCK_ESCALATION = TABLE)
GO
