SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EdiRevenueReporting_Archive] (
		[FYear]                   [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[FYearSort]               [smallint] NULL,
		[AcctMonth]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AcctMonthSort]           [int] NULL,
		[AcctWeek]                [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[AcctWeekSort]            [int] NULL,
		[ConsignmentDate]         [date] NULL,
		[AccountCode]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[AccountName]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[BU]                      [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[NWCategory]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ServCategory]            [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[FreightCharge]           [decimal](18, 3) NULL,
		[InsuranceCharge]         [decimal](18, 3) NULL,
		[InsuranceCategory]       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[FuelSurcharge]           [decimal](18, 3) NULL,
		[StateCode]               [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentCount]        [int] NULL,
		[DeadWeightTotal]         [decimal](18, 2) NULL,
		[CubicWeightTotal]        [decimal](18, 2) NULL,
		[ChargeWeightTotal]       [decimal](18, 2) NULL,
		[ItemCount]               [int] NULL,
		[OriginState]             [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[DestinationState]        [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[PickupCost]              [decimal](18, 3) NULL,
		[DeliveryCost]            [decimal](18, 3) NULL,
		[OriginZone]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DestinationZone]         [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[OriginLaneType]          [char](1) COLLATE Latin1_General_CI_AS NULL,
		[DestinationLaneType]     [char](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EdiRevenueReporting_Archive] SET (LOCK_ESCALATION = TABLE)
GO
