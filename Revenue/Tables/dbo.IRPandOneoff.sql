SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IRPandOneoff] (
		[Company]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Consignment]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pickup Suburb]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode1]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Pickup Pay Date]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Suburb]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Postcode2]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Delivery Pay Date]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Items]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Non-tracker coupons]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons1]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons2]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons3]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons4]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons5]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons6]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons7]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons8]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons9]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons10]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons11]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons12]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons13]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons14]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons15]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons16]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons17]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons18]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons19]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons20]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Coupons21]               [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[IRPandOneoff] SET (LOCK_ESCALATION = TABLE)
GO
