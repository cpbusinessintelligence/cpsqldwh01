SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRechargeMonthlyTotals] (
		[MonthKey]              [int] NULL,
		[Type]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[RechargeCategory]      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BusinessUnit]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanBranch]       [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentName]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Itemcount]             [int] NULL,
		[AgentCost]             [decimal](15, 2) NULL,
		[RedShirtCost]          [decimal](15, 2) NULL,
		[HandlingCost]          [decimal](15, 2) NULL,
		[CreatedDate]           [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140729-115400]
	ON [dbo].[IntRechargeMonthlyTotals] ([MonthKey])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntRechargeMonthlyTotals] SET (LOCK_ESCALATION = TABLE)
GO
