SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_Agents] (
		[a_id]                 [int] NOT NULL,
		[a_shortname]          [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[a_name]               [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[a_supplier_code]      [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_cppl]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_querybranch]        [int] NULL,
		[a_driver]             [int] NULL,
		[a_booking_branch]     [int] NULL,
		[a_style]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[agentstate]           [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__IntRecha__566AFA9A6C3839F9]
		PRIMARY KEY
		CLUSTERED
		([a_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_sho__3EB236BE]
	DEFAULT ('') FOR [a_shortname]
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_nam__3FA65AF7]
	DEFAULT ('') FOR [a_name]
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_sup__409A7F30]
	DEFAULT ('') FOR [a_supplier_code]
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_cpp__418EA369]
	DEFAULT ('N') FOR [a_cppl]
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_que__4282C7A2]
	DEFAULT (NULL) FOR [a_querybranch]
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_dri__4376EBDB]
	DEFAULT ('0') FOR [a_driver]
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_boo__446B1014]
	DEFAULT (NULL) FOR [a_booking_branch]
GO
ALTER TABLE [dbo].[IntRecharge_Agents]
	ADD
	CONSTRAINT [DF__IntRechar__a_sty__455F344D]
	DEFAULT ('') FOR [a_style]
GO
ALTER TABLE [dbo].[IntRecharge_Agents] SET (LOCK_ESCALATION = TABLE)
GO
