SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_Agents_old] (
		[a_id]                 [int] NOT NULL,
		[a_shortname]          [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[a_name]               [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
		[a_supplier_code]      [varchar](32) COLLATE Latin1_General_CI_AS NULL,
		[a_cppl]               [char](1) COLLATE Latin1_General_CI_AS NULL,
		[a_querybranch]        [int] NULL,
		[a_driver]             [int] NULL,
		[a_booking_branch]     [int] NULL,
		[a_style]              [char](1) COLLATE Latin1_General_CI_AS NULL,
		[agentstate]           [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__IntRecha__566AFA9A2FBA0BF1]
		PRIMARY KEY
		CLUSTERED
		([a_id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_sho__3296789C]
	DEFAULT ('') FOR [a_shortname]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_nam__338A9CD5]
	DEFAULT ('') FOR [a_name]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_sup__347EC10E]
	DEFAULT ('') FOR [a_supplier_code]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_cpp__3572E547]
	DEFAULT ('N') FOR [a_cppl]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_que__36670980]
	DEFAULT (NULL) FOR [a_querybranch]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_dri__375B2DB9]
	DEFAULT ('0') FOR [a_driver]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_boo__384F51F2]
	DEFAULT (NULL) FOR [a_booking_branch]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old]
	ADD
	CONSTRAINT [DF__IntRechar__a_sty__3943762B]
	DEFAULT ('') FOR [a_style]
GO
ALTER TABLE [dbo].[IntRecharge_Agents_old] SET (LOCK_ESCALATION = TABLE)
GO
