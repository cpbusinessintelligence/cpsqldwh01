SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_BudgetPricing] (
		[monthkey]          [int] NULL,
		[Type]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BusinessUnit]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedshirtPrice]     [decimal](38, 3) NULL,
		[AgentPrice]        [decimal](38, 3) NULL,
		[HandlingPrice]     [decimal](38, 3) NULL,
		[CreatedDate]       [datetime] NULL,
		[CreatedBy]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[IntRecharge_BudgetPricing] SET (LOCK_ESCALATION = TABLE)
GO
