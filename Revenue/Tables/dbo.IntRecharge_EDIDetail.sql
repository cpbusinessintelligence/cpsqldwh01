SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_EDIDetail] (
		[MonthKey]              [int] NOT NULL,
		[Category]              [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConNote]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_date]               [datetime] NULL,
		[cd_items]              [int] NULL,
		[CouponCount]           [int] NULL,
		[cd_pricecode]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_deadweight]         [decimal](18, 3) NULL,
		[cd_volume]             [decimal](18, 3) NULL,
		[PickupScanDepot]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupScanBranch]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Deliver_driver]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanDepot]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanBranch]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentName]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]           [datetime] NOT NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[IntRecharge_EDIDetail] SET (LOCK_ESCALATION = TABLE)
GO
