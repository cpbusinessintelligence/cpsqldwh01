SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_EDIDetail_Archive] (
		[MonthKey]              [int] NULL,
		[Category]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ConNote]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_date]               [datetime] NULL,
		[cd_items]              [int] NULL,
		[CouponCount]           [int] NULL,
		[cd_pricecode]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[cd_deadweight]         [decimal](18, 3) NULL,
		[cd_volume]             [decimal](18, 3) NULL,
		[PickupScanDepot]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupScanBranch]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Deliver_driver]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanDepot]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanBranch]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentName]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]           [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]                  [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[IntRecharge_EDIDetail_Archive] SET (LOCK_ESCALATION = TABLE)
GO
