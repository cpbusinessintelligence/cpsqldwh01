SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IntRecharge_EDISummary] (
		[Monthkey]              [int] NOT NULL,
		[Type]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Category]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BusinessUnit]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[cd_pricecode]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupScanDepot]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanBranch]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanDepot]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentName]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[ConCount]              [int] NULL,
		[Items]                 [int] NULL,
		[Volume]                [decimal](18, 3) NULL,
		[DeadWeight]            [decimal](18, 3) NULL,
		[CreatedDate]           [datetime] NOT NULL,
		[CreatedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[IntRecharge_EDISummary] SET (LOCK_ESCALATION = TABLE)
GO
