SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_PrepaidDetails] (
		[MonthKey]                    [int] NOT NULL,
		[Category]                    [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[LabelNumber]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CouponType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueRecognisedDate]       [datetime] NULL,
		[FirstScanDriverBranch]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupScanDepot]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FirstScanType]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanDateTime]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanDriverBranch]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanDriverNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanContractorCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanDepot]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentID]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentName]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanType]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]                 [datetime] NOT NULL,
		[CreatedBy]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]                        [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140729-115319]
	ON [dbo].[IntRecharge_PrepaidDetails] ([MonthKey])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntRecharge_PrepaidDetails] SET (LOCK_ESCALATION = TABLE)
GO
