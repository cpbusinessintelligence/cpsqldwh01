SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_PrepaidDetails_Archive] (
		[MonthKey]                    [int] NULL,
		[Category]                    [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CouponType]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueRecognisedDate]       [datetime] NULL,
		[FirstScanDriverBranch]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupScanDepot]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[FirstScanType]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanDateTime]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanDriverBranch]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanDriverNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanContractorCode]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanDepot]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentID]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentName]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[OtherScanType]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BUCode]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]                 [datetime] NULL,
		[CreatedBy]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Type]                        [varchar](30) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[IntRecharge_PrepaidDetails_Archive] SET (LOCK_ESCALATION = TABLE)
GO
