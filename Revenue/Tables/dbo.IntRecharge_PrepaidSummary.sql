SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntRecharge_PrepaidSummary] (
		[MonthKey]                  [int] NOT NULL,
		[Type]                      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Category]                  [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[BusinessUnit]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PickupScanDepot]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanDriverBranch]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DelivScanDepot]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentName]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ItemCount]                 [int] NULL,
		[CreatedDate]               [datetime] NOT NULL,
		[CreatedBy]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAgentID]           [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140729-115344]
	ON [dbo].[IntRecharge_PrepaidSummary] ([MonthKey])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntRecharge_PrepaidSummary] SET (LOCK_ESCALATION = TABLE)
GO
