SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InterfaceConfig] (
		[Name]      [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[Value]     [varchar](3000) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_InterfaceConfig]
		PRIMARY KEY
		CLUSTERED
		([Name])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[InterfaceConfig] SET (LOCK_ESCALATION = TABLE)
GO
