SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InterfaceErrorLog] (
		[Id]                 [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[ErrorNumber]        [int] NULL,
		[ErrorSeverity]      [int] NULL,
		[ErrorState]         [int] NULL,
		[ErrorProcedure]     [nvarchar](128) COLLATE Latin1_General_CI_AS NULL,
		[ErrorLine]          [int] NULL,
		[ErrorMessage]       [nvarchar](4000) COLLATE Latin1_General_CI_AS NULL,
		[CreateDate]         [datetime] NULL,
		CONSTRAINT [PK_InterfaceErrorLog]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[InterfaceErrorLog]
	ADD
	CONSTRAINT [DF_InterfaceErrorLog_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[InterfaceErrorLog]
	ADD
	CONSTRAINT [DF_InterfaceErrorLog_CreateDate]
	DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[InterfaceErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
