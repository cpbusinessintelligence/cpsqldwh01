SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ActivityErrors] (
		[ActivityDateTime]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFlag]         [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ActivityErrors] SET (LOCK_ESCALATION = TABLE)
GO
