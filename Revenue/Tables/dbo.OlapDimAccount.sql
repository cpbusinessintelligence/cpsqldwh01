SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimAccount] (
		[AccountId]       [int] IDENTITY(1, 1) NOT NULL,
		[AccountCode]     [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_OlapDimAccount]
		PRIMARY KEY
		CLUSTERED
		([AccountId])
	ON [PRIMARY]
)
GO
CREATE NONCLUSTERED INDEX [_dta_index_OlapDimAccount_32_101575400__K2]
	ON [dbo].[OlapDimAccount] ([AccountCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[OlapDimAccount] SET (LOCK_ESCALATION = TABLE)
GO
