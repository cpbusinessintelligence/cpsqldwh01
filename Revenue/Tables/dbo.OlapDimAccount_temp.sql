SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimAccount_temp] (
		[AccountId]       [int] IDENTITY(1, 1) NOT NULL,
		[AccountCode]     [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[AccountName]     [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[OlapDimAccount_temp] SET (LOCK_ESCALATION = TABLE)
GO
