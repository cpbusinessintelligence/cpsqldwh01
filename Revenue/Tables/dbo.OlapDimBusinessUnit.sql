SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimBusinessUnit] (
		[BusinessUnitId]       [int] IDENTITY(1, 1) NOT NULL,
		[BusinessUnitCode]     [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[BusinessUnitName]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[BusinessUnitSort]     [smallint] NOT NULL,
		[StateId]              [int] NOT NULL,
		CONSTRAINT [PK_OlapDimBusinessUnit]
		PRIMARY KEY
		CLUSTERED
		([BusinessUnitId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimBusinessUnit] SET (LOCK_ESCALATION = TABLE)
GO
