SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[OlapDimCalendarDay] (
		[DayId]       [int] IDENTITY(1, 1) NOT NULL,
		[DayDate]     [date] NOT NULL,
		[MonthId]     [int] NOT NULL,
		[YearId]      [int] NOT NULL,
		CONSTRAINT [PK_OlapDimCalendarDay]
		PRIMARY KEY
		CLUSTERED
		([DayId])
	ON [PRIMARY]
)
GO
CREATE STATISTICS [_dta_stat_165575628_2_1]
	ON [dbo].[OlapDimCalendarDay] ([DayDate], [DayId])
GO
ALTER TABLE [dbo].[OlapDimCalendarDay] SET (LOCK_ESCALATION = TABLE)
GO
