SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[OlapDimCalendarDay_BUP_2020] (
		[DayId]       [int] IDENTITY(1, 1) NOT NULL,
		[DayDate]     [date] NOT NULL,
		[MonthId]     [int] NOT NULL,
		[YearId]      [int] NOT NULL
)
GO
ALTER TABLE [dbo].[OlapDimCalendarDay_BUP_2020] SET (LOCK_ESCALATION = TABLE)
GO
