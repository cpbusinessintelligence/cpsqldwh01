SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimCalendarMonth] (
		[MonthId]            [int] IDENTITY(1, 1) NOT NULL,
		[MonthName]          [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[MonthShortName]     [char](3) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_OlapDimCalendarMonth]
		PRIMARY KEY
		CLUSTERED
		([MonthId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimCalendarMonth] SET (LOCK_ESCALATION = TABLE)
GO
