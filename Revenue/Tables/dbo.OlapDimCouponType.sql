SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimCouponType] (
		[CouponTypeId]                [int] IDENTITY(1, 1) NOT NULL,
		[CouponType]                  [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[CouponTypeSort]              [smallint] NOT NULL,
		[IncludeConsignmentCount]     [bit] NULL,
		CONSTRAINT [PK_OlapDimCouponType]
		PRIMARY KEY
		CLUSTERED
		([CouponTypeId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimCouponType]
	ADD
	CONSTRAINT [DF_OlapDimCouponType_IncludeConsignmentCount]
	DEFAULT ((0)) FOR [IncludeConsignmentCount]
GO
ALTER TABLE [dbo].[OlapDimCouponType] SET (LOCK_ESCALATION = TABLE)
GO
