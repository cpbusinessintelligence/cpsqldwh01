SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimDepot] (
		[DepotId]        [int] IDENTITY(1, 1) NOT NULL,
		[DepotName]      [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[DepotGroup]     [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_OlapDimDepot]
		PRIMARY KEY
		CLUSTERED
		([DepotId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimDepot] SET (LOCK_ESCALATION = TABLE)
GO
