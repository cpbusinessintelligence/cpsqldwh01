SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimFinancialPeriod] (
		[FinancialPeriodId]     [int] IDENTITY(1, 1) NOT NULL,
		[FinancialYearId]       [int] NOT NULL,
		[FinancialPeriod]       [smallint] NOT NULL,
		[PeriodName]            [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_OlapDimFinancialPeriod]
		PRIMARY KEY
		CLUSTERED
		([FinancialPeriodId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimFinancialPeriod] SET (LOCK_ESCALATION = TABLE)
GO
