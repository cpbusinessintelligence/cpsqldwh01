SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimFinancialWeek] (
		[FinancialWeekId]       [int] IDENTITY(1, 1) NOT NULL,
		[FinancialPeriodId]     [int] NOT NULL,
		[FinancialWeek]         [smallint] NOT NULL,
		[WeekName]              [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[WeekEndingDate]        [date] NOT NULL,
		[WeekSortOrder]         [smallint] NOT NULL,
		CONSTRAINT [PK_OlapDimFinancialWeek]
		PRIMARY KEY
		CLUSTERED
		([FinancialWeekId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimFinancialWeek] SET (LOCK_ESCALATION = TABLE)
GO
