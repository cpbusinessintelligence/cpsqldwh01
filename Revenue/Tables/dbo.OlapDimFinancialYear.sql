SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimFinancialYear] (
		[FinancialYearId]       [int] IDENTITY(1, 1) NOT NULL,
		[FinancialYear]         [smallint] NOT NULL,
		[YearSort]              [smallint] NOT NULL,
		[FinancialYearName]     [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_OlapDimFinancialYear]
		PRIMARY KEY
		CLUSTERED
		([FinancialYearId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimFinancialYear] SET (LOCK_ESCALATION = TABLE)
GO
