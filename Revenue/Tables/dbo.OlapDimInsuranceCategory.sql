SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimInsuranceCategory] (
		[InsuranceCategoryId]     [int] IDENTITY(1, 1) NOT NULL,
		[InsuranceCategory]       [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CategorySort]            [smallint] NOT NULL,
		CONSTRAINT [PK_OlapDimInsuranceCategory]
		PRIMARY KEY
		CLUSTERED
		([InsuranceCategoryId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimInsuranceCategory] SET (LOCK_ESCALATION = TABLE)
GO
