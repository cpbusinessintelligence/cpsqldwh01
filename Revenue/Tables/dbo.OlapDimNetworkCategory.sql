SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimNetworkCategory] (
		[NetworkCategoryId]     [int] IDENTITY(1, 1) NOT NULL,
		[NetworkCategory]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CategorySort]          [smallint] NOT NULL,
		CONSTRAINT [PK_OlapDimNetworkCategory]
		PRIMARY KEY
		CLUSTERED
		([NetworkCategoryId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimNetworkCategory] SET (LOCK_ESCALATION = TABLE)
GO
