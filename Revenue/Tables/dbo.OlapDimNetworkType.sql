SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimNetworkType] (
		[NetworkTypeId]       [int] IDENTITY(1, 1) NOT NULL,
		[NetworkTypeCode]     [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
		[NetworkTypeName]     [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[TypeSort]            [smallint] NOT NULL,
		CONSTRAINT [PK_OlapDimNetworkType]
		PRIMARY KEY
		CLUSTERED
		([NetworkTypeId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimNetworkType] SET (LOCK_ESCALATION = TABLE)
GO
