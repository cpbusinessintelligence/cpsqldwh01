SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimRevenueType] (
		[RevenueTypeId]       [int] IDENTITY(1, 1) NOT NULL,
		[RevenueType]         [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[RevenueTypeSort]     [smallint] NOT NULL,
		CONSTRAINT [PK_OlapDimRevenueType]
		PRIMARY KEY
		CLUSTERED
		([RevenueTypeId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimRevenueType] SET (LOCK_ESCALATION = TABLE)
GO
