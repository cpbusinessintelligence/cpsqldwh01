SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimServiceCategory] (
		[ServiceCategoryId]     [int] IDENTITY(1, 1) NOT NULL,
		[ServiceCategory]       [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[CategorySort]          [smallint] NULL,
		CONSTRAINT [PK_OlapDimServiceCategory]
		PRIMARY KEY
		CLUSTERED
		([ServiceCategoryId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimServiceCategory] SET (LOCK_ESCALATION = TABLE)
GO
