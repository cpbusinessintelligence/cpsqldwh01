SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlapDimState] (
		[StateId]       [int] IDENTITY(1, 1) NOT NULL,
		[StateCode]     [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[StateSort]     [smallint] NOT NULL,
		CONSTRAINT [PK_OlapDimState]
		PRIMARY KEY
		CLUSTERED
		([StateId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapDimState] SET (LOCK_ESCALATION = TABLE)
GO
