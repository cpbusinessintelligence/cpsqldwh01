SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[OlapFactPrepaidRevenueReporting2] (
		[RevenueReportingId]              [int] IDENTITY(1, 1) NOT NULL,
		[FinancialYearId]                 [int] NOT NULL,
		[FinancialPeriodId]               [int] NOT NULL,
		[FinancialWeekId]                 [int] NOT NULL,
		[CalendarDayId]                   [int] NOT NULL,
		[BusinessUnitId]                  [int] NOT NULL,
		[AccountId]                       [int] NOT NULL,
		[NetworkCategoryId]               [int] NOT NULL,
		[ServiceCategoryId]               [int] NOT NULL,
		[CouponTypeId]                    [int] NOT NULL,
		[OriginDepotId]                   [int] NOT NULL,
		[OriginStateId]                   [int] NOT NULL,
		[DestinationStateId]              [int] NOT NULL,
		[BusinessUnitStateId]             [int] NOT NULL,
		[OriginZoneId]                    [int] NOT NULL,
		[DestinationZoneId]               [int] NOT NULL,
		[OriginNetworkTypeId]             [int] NOT NULL,
		[DestinationNetworkTypeId]        [int] NOT NULL,
		[FreightCharge]                   [decimal](18, 3) NOT NULL,
		[InsuranceCharge]                 [decimal](18, 3) NOT NULL,
		[InsuranceCategoryId]             [int] NOT NULL,
		[FuelSurcharge]                   [decimal](18, 3) NOT NULL,
		[PrimaryCouponTotal]              [int] NOT NULL,
		[AllCouponTotal]                  [int] NOT NULL,
		[DeadWeightTotal]                 [decimal](18, 3) NOT NULL,
		[CubicWeightTotal]                [decimal](18, 3) NOT NULL,
		[ProntoChargeableWeightTotal]     [decimal](18, 3) NOT NULL,
		[PickupCost]                      [decimal](18, 3) NOT NULL,
		[DeliveryCost]                    [decimal](18, 3) NOT NULL,
		[BusinessDays]                    [smallint] NOT NULL,
		[BillableWeightTotal]             [decimal](18, 3) NOT NULL,
		CONSTRAINT [PK_OlapFactPrepaidRevenueReporting2]
		PRIMARY KEY
		CLUSTERED
		([RevenueReportingId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[OlapFactPrepaidRevenueReporting2] SET (LOCK_ESCALATION = TABLE)
GO
