SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrepaidRevenueLinkLabels] (
		[LabelNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[LinkLabelNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		CONSTRAINT [PK_PrepaidRevenueLinkLabels]
		PRIMARY KEY
		CLUSTERED
		([LabelNumber], [LinkLabelNumber])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[PrepaidRevenueLinkLabels] SET (LOCK_ESCALATION = TABLE)
GO
