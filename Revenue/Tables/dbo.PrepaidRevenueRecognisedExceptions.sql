SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrepaidRevenueRecognisedExceptions] (
		[LabelNumber]               [float] NULL,
		[FirstScanDateTime]         [datetime] NULL,
		[BUCode]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[location]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RevenueRecognisedDate]     [datetime] NULL,
		[Revenue]                   [float] NULL
)
GO
ALTER TABLE [dbo].[PrepaidRevenueRecognisedExceptions] SET (LOCK_ESCALATION = TABLE)
GO
