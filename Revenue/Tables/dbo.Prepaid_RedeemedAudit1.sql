SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prepaid_RedeemedAudit1] (
		[CouponNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[SoldDate]              [date] NULL,
		[SoldReference]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RevenueDate]           [date] NULL,
		[RevenueAmount]         [decimal](13, 2) NULL,
		[InsuranceAmount]       [decimal](13, 2) NULL,
		[InsuranceCategory]     [varchar](20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Prepaid_RedeemedAudit1] SET (LOCK_ESCALATION = TABLE)
GO
