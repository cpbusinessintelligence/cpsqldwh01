SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Primary Coupons List] (
		[RevenueType]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Code]                [float] NULL,
		[Category]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RevenueCategory]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Primary Coupons List] SET (LOCK_ESCALATION = TABLE)
GO
