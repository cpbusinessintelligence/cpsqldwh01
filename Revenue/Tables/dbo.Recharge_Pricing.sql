SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Recharge_Pricing] (
		[CostID]               [int] NOT NULL,
		[RechargeCategory]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BusinessUnit]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Price]                [money] NULL,
		[AddWho]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddWhen]              [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[Recharge_Pricing] SET (LOCK_ESCALATION = TABLE)
GO
