SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_Activity] (
		[ActivityDateTime]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFlag]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFileOn]       [date] NULL,
		[CraetedOn]            [datetime] NULL
)
GO
ALTER TABLE [dbo].[_Activity]
	ADD
	CONSTRAINT [DF_Activity_CraetedOn]
	DEFAULT (getdate()) FOR [CraetedOn]
GO
ALTER TABLE [dbo].[_Activity] SET (LOCK_ESCALATION = TABLE)
GO
