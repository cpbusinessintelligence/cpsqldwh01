SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_ActivityImport] (
		[FileName]             [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[ActivityDateTime]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SerialNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFlag]         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ActivityFileOn]       [date] NULL,
		[CreatedOn]            [datetime] NULL
)
GO
ALTER TABLE [dbo].[_ActivityImport] SET (LOCK_ESCALATION = TABLE)
GO
