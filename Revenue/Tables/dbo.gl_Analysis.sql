SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gl_Analysis] (
		[GL_AccountCode]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SerialNumber]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[GL_TransType]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GL_transDate]       [date] NULL,
		[Invoice]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Order]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Driver]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[BatchRef]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isRepeated]         [int] NULL,
		[GLRevAmount]        [decimal](12, 2) NULL,
		[IsExistinCube]      [int] NULL,
		[CubeRevAmount]      [decimal](12, 2) NULL,
		[CubeRevDate]        [date] NULL,
		[CubeCreated]        [date] NULL
)
GO
ALTER TABLE [dbo].[gl_Analysis] SET (LOCK_ESCALATION = TABLE)
GO
