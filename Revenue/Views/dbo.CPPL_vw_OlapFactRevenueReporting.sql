SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[CPPL_vw_OlapFactRevenueReporting]
AS
SELECT [RevenueReportingId]
      ,[FinancialYearId]
      ,[FinancialPeriodId]
      ,[FinancialWeekId]
      ,[CalendarDayId]
      ,[BusinessUnitId]
      ,[AccountId]
      ,[NetworkCategoryId]
      ,[ServiceCategoryId]
      ,[CouponTypeId]
      ,[OriginStateId]
      ,[DestinationStateId]
      ,[BusinessUnitStateId]
      ,[OriginZoneId]
      ,[DestinationZoneId]
      ,[OriginNetworkTypeId]
      ,[DestinationNetworkTypeId]
      ,[FreightCharge]
      ,[InsuranceCharge]
      ,[InsuranceCategoryId]
      ,[FuelSurcharge]
      , ConsignmentTotal 
      , ItemTotal
      ,0 AS [PrimaryCouponTotal]
      ,0 AS [AllCouponTotal]
      ,[DeadWeightTotal]
      ,[CubicWeightTotal]
      ,[ProntoChargeableWeightTotal]
      ,[PickupCost]
      ,[DeliveryCost]
      ,[BusinessDays]
      ,[BillableWeightTotal],
(
	SELECT ISNULL(RevenueTypeId, 0)
	FROM OlapDimRevenueType (NOLOCK)
	WHERE RevenueType = 'EDI'
) AS [RevenueTypeId], --EDI
(
	SELECT ISNULL(DepotId, 0)
	FROM OlapDimDepot (NOLOCK)
	WHERE DepotName = 'N/A'
) AS [OriginDepotId]
FROM dbo.OlapFactEdiRevenueReporting (NOLOCK)
UNION ALL
SELECT [RevenueReportingId]
      ,[FinancialYearId]
      ,[FinancialPeriodId]
      ,[FinancialWeekId]
      ,[CalendarDayId]
      ,[BusinessUnitId]
      ,[AccountId]
      ,[NetworkCategoryId]
      ,[ServiceCategoryId]
      ,[CouponTypeId]
      ,[OriginStateId]
      ,[DestinationStateId]
      ,[BusinessUnitStateId]
      ,[OriginZoneId]
      ,[DestinationZoneId]
      ,[OriginNetworkTypeId]
      ,[DestinationNetworkTypeId]
      ,[FreightCharge]
      ,[InsuranceCharge]
      ,[InsuranceCategoryId]
      ,[FuelSurcharge]
      ,0 AS ConsignmentTotal 
      ,0 AS ItemTotal
      ,[PrimaryCouponTotal]
      ,[AllCouponTotal]
      ,[DeadWeightTotal]
      ,[CubicWeightTotal]
      ,[ProntoChargeableWeightTotal]
      ,[PickupCost]
      ,[DeliveryCost]
      ,[BusinessDays]
      ,[BillableWeightTotal],
(
	SELECT ISNULL(RevenueTypeId, 0)
	FROM OlapDimRevenueType (NOLOCK)
	WHERE RevenueType = 'PREPAID'
) AS [RevenueTypeId], --PREPAID
OriginDepotId
FROM dbo.OlapFactPrepaidRevenueReporting (NOLOCK)




GO
