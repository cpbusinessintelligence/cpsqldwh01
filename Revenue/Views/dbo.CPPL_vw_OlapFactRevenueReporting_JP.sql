SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[CPPL_vw_OlapFactRevenueReporting_JP]
AS
SELECT [RevenueReportingId]
      ,[FinancialYearId]
      ,[FinancialPeriodId]
      ,[FinancialWeekId]
      ,[CalendarDayId]
      ,[BusinessUnitId]
      ,[AccountId]
      ,[NetworkCategoryId]
      ,[ServiceCategoryId]
      ,[CouponTypeId]
      ,[OriginStateId]
      ,[DestinationStateId]
      ,[BusinessUnitStateId]
      ,[OriginZoneId]
      ,[DestinationZoneId]
      ,[OriginNetworkTypeId]
      ,[DestinationNetworkTypeId]
      ,[FreightCharge]
      ,[InsuranceCharge]
      ,[InsuranceCategoryId]
      ,[FuelSurcharge]
      ,0 AS ConsignmentTotal 
      ,0 AS ItemTotal
      ,[PrimaryCouponTotal]
      ,[AllCouponTotal]
      ,[DeadWeightTotal]
      ,[CubicWeightTotal]
      ,[ProntoChargeableWeightTotal]
      ,[PickupCost]
      ,[DeliveryCost]
      ,[BusinessDays]
      ,[BillableWeightTotal],
(
	SELECT ISNULL(RevenueTypeId, 0)
	FROM OlapDimRevenueType (NOLOCK)
	WHERE RevenueType = 'PREPAID'
) AS [RevenueTypeId], --PREPAID
OriginDepotId
FROM dbo.OlapFactPrepaidRevenueReporting_JP (NOLOCK)










GO
