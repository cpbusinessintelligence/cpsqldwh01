SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW dbo.CPPL_vw_PrepaidRevenueReportingWithNullRevenue
AS
SELECT 
	LabelNumber,
	CASE ISNULL(ServiceCategory, 'X')
		WHEN 'X' THEN 'UNKNOWN'
		WHEN 'S' THEN 'SATCHELS'
		WHEN 'R' THEN 'RETURNS'
		WHEN 'P' THEN 'PARCELS'
		ELSE 'UNKNOWN'
	END AS [ServiceCategory],
	ISNULL(CouponType, 'UNKNOWN') AS [CouponType],
	FirstScanDateTime,
	FirstScanDriverBranch,
	FirstScanDriverNumber,
	FirstScanContractorCode,
	FirstScanDepot,
	FirstScanType,
	RevenueRecognisedDate,
	RevenueProcessedDateTime,
	ISNULL(InsuranceAmount, 0.00) AS [InsuranceAmount],
	ISNULL(InsuranceCategory, '') AS [InsuranceCategory],
	OtherScanDateTime,
	OtherScanDriverBranch,
	OtherScanDriverNumber,
	OtherScanContractorCode,
	OtherScanDepot,
	OtherScanType,
	CASE ISNULL(NetworkCategory, 'X')
		WHEN 'N' THEN 'INTRASTATE'
		WHEN 'M' THEN 'METRO'
		WHEN 'X' THEN 'UNKNOWN'
		WHEN 'R' THEN 'REGIONAL'
		WHEN 'I' THEN 'INTERSTATE'
		ELSE 'UNKNOWN'
	END AS [NetworkCategory],
	ISNULL(BU, 'UNKNOWN') AS [BusinessUnit],
	AccountCode AS [CustomerCode],
	AccountName AS [CustomerName],
	ProntoOrderNumber,
	ProntoOrderLine,
	ProntoOrderDate,
	ProntoOrderContractorCode,
	ConsignmentNumber,
	ConsignmentOriginState,
	ConsignmentDestinationState,
	ConsignmentCustomer,
	OriginBranch,
	OriginDepot,
	DestinationBranch,
	DestinationDepot,
	ConsolidateBarcode,
	ConsolidateScanDateTime
FROM PrepaidRevenueReporting (NOLOCK)
-- only include records older than 14 days
WHERE CreatedDate < DATEADD(day, -14, GETDATE())
AND RevenueAmount Is Null
-- here we can filter out stray label numbers that we know are 
-- definitely not valid prepaid, or do not have an actual
-- revenue amount associated with them (eg. ATL)
AND LabelNumber Not Like '[23456789]38%'
AND LabelNumber Not Like '[23456789]14%'
AND LabelNumber Not Like '[149]00%'
AND ISNULL(IsProcessed, 0) = 0;
--ORDER BY LabelNumber ASC;


GO
