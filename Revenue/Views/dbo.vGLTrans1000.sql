SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vGLTrans1000]
AS

Select * from dbo.gl_trans_july2012_001000
Union all 
Select * from dbo.gl_trans_aug2012_001000
Union all
Select * from dbo.gl_trans_Sept2012_001000
Union all 
Select * from dbo.gl_trans_Oct2012_001000
Union all
Select * from dbo.gl_trans_Nov2012_001000
Union all 
Select * from dbo.gl_trans_Dec2012_001000
Union all
Select * from dbo.gl_trans_Jan2013_001000
Union all 
Select * from dbo.gl_trans_Feb2013_001000
Union all
Select * from dbo.gl_trans_March2013_001000
Union all 
Select * from dbo.gl_trans_April2013_001000
Union all
Select * from dbo.gl_trans_May2013_001000
Union all
Select * from dbo.gl_trans_June2013_001000
Union all
Select * from dbo.gl_trans_July2013_001000

GO
