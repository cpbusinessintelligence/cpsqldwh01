SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO











CREATE VIEW [dbo].[vPrepaidRevenueReporting]
AS
SELECT * FROM dbo.PrepaidRevenueReporting (NOLOCK) 
Union all
SELECT * FROM dbo.PrepaidRevenueReporting_2012 (NOLOCK)
Union all
SELECT * FROM dbo.PrepaidRevenueReporting_2013 (NOLOCK)
Union all
SELECT * FROM dbo.PrepaidRevenueReportingExceptions (NOLOCK) 










GO
