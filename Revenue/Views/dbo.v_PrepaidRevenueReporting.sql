SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[v_PrepaidRevenueReporting]
AS
SELECT * 
FROM dbo.PrepaidRevenueReporting(NOLOCK)
WHERE (LabelPrefix  not in( '183','190', '191')
              OR LabelPrefix not LIKE '[23456789]14'
              OR LabelPrefix not LIKE '[23456789]23'
              OR LabelPrefix not LIKE '[23456789]38')
Union all
SELECT * 
FROM dbo.PrepaidRevenueReporting_2012(NOLOCK)
WHERE (LabelPrefix  not in( '183','190', '191')
              OR LabelPrefix not LIKE '[23456789]14'
              OR LabelPrefix not LIKE '[23456789]23'
              OR LabelPrefix not LIKE '[23456789]38')              


Union all
SELECT * 
FROM dbo.PrepaidRevenueReporting_2013(NOLOCK)
WHERE (LabelPrefix  not in( '183','190', '191')
              OR LabelPrefix not LIKE '[23456789]14'
              OR LabelPrefix not LIKE '[23456789]23'
              OR LabelPrefix not LIKE '[23456789]38') 



Union all
SELECT * 
FROM dbo.PrepaidRevenueReportingExceptions (NOLOCK)
WHERE (LabelPrefix  not in( '183','190', '191')
              OR LabelPrefix not LIKE '[23456789]14'
              OR LabelPrefix not LIKE '[23456789]23'
              OR LabelPrefix not LIKE '[23456789]38') 



GO
