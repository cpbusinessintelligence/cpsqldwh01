SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_TrackingEvents]
AS
SELECT te.Id,
	b.CosmosBranch,
	te.EventDateTime,
	d.Code,
	dp.Name,
	et.[Description],
	d.ProntoDriverCode,
	d.IsContractor
FROM ScannerGateway.dbo.TrackingEvent te (NOLOCK)
INNER JOIN ScannerGateway.dbo.Driver d (NOLOCK) ON te.DriverId = d.Id 
INNER JOIN ScannerGateway.dbo.Branch b (NOLOCK) ON d.BranchId = b.Id 
INNER JOIN ScannerGateway.dbo.EventType et (NOLOCK) ON te.EventTypeId = et.Id 
LEFT OUTER JOIN ScannerGateway.dbo.Depot dp (NOLOCK) ON d.DepotId = dp.Id 

GO
