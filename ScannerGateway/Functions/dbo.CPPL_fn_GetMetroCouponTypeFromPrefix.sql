SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create FUNCTION [dbo].[CPPL_fn_GetMetroCouponTypeFromPrefix]
(
	@Prefix		char(3)
)
RETURNS varchar(15)
AS
BEGIN

	DECLARE @ret varchar(15);
	SELECT @ret = Null;

	SELECT @ret = CASE
		              WHEN @Prefix LIKE '12[0127]' THEN 'SATCHEL'
              --WHEN @Prefix LIKE '13[567]' THEN 'SATCHEL'
              WHEN @Prefix = '188' THEN 'SAT500'
              WHEN @Prefix = '135' THEN 'SAT1KG'
              WHEN @Prefix = '136' THEN 'SAT3KG'

              WHEN @Prefix = '137' THEN 'SAT5KG'


              WHEN @Prefix = '186' THEN 'PROMO'
              WHEN @Prefix = '190' THEN 'REDELIVERY CPN'

              WHEN @Prefix LIKE '[23456789]0[12]' THEN 'GOLD'
              WHEN @Prefix LIKE '[23456789]0[34]' THEN 'YELLOW'
              WHEN @Prefix LIKE '[23456789]05' THEN 'BLUE'
              WHEN @Prefix LIKE '[23456789]06' THEN 'RED'
              WHEN @Prefix LIKE '[23456789]07' THEN 'GREEN'
              WHEN @Prefix LIKE '[23456789]08' THEN 'GREY'
              WHEN @Prefix LIKE '[23456789]10' THEN 'COD'
              WHEN @Prefix LIKE '[2345678]11' THEN 'SPECIAL'
              WHEN @Prefix = '911' THEN 'ORANGE'

              WHEN @Prefix = '712' THEN 'REGIONAL'

              WHEN @Prefix LIKE '[23456789]1[56]' THEN 'REGIONAL'
              WHEN @Prefix LIKE '[23456789]18' THEN 'BULK O/NIGHT'
              WHEN @Prefix LIKE '[23456789]2[0127]' THEN 'SATCHEL'
              WHEN @Prefix LIKE '[23456789]23' THEN 'PROFORMA'

              WHEN @Prefix LIKE '[23456789]2[456]' THEN 'CARTON'

              WHEN @Prefix LIKE '[23456789]3[67]' THEN 'SATCHEL'

              WHEN @Prefix LIKE '[2345689]40' THEN 'PLATINUM'
              WHEN @Prefix = '740' THEN 'PROMO'

              WHEN @Prefix = '268' THEN 'SATCHEL'
              WHEN @Prefix LIKE '[23456789]84' THEN 'COD'
              WHEN @Prefix = '286' THEN 'PROMO'
              WHEN @Prefix LIKE '[23456789]89' THEN 'ONE-OFF'
              WHEN @Prefix LIKE '[23456789]9[34]' THEN 'RETURN'
              WHEN @Prefix LIKE '[56]92' THEN 'BLACK'
              WHEN @Prefix = '295' THEN 'OUTER METRO'
              WHEN @Prefix LIKE '34[12]' THEN 'REGIONAL'
              WHEN @Prefix LIKE '[34568]43' THEN 'COD'

              WHEN @Prefix = '870' THEN 'BLACK'
              WHEN @Prefix LIKE '[6]4[789]' THEN 'CHRISCO'
              WHEN @Prefix = '985' THEN 'PROMO'
              WHEN @Prefix = '993' THEN 'RETURN'
              WHEN @Prefix = '639' THEN 'SUPER BULK'
              WHEN @Prefix = '170' THEN 'ACE COUPON'
              WHEN @Prefix = '176' THEN 'PRIORITY SATCHEL'
              WHEN @Prefix = '178' THEN 'PRIORITY CPN' 
              WHEN @Prefix = '179' THEN 'PRIORITY CPN'
              WHEN @Prefix = '395' THEN 'OUTER METRO'

	END

	RETURN @ret;
	
END
GO
