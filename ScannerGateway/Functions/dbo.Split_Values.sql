SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[Split_Values]
(
   @String varchar(4000)
,   @Delimiter varchar(5)
) 
returns @Split_Values table(String_Value varchar(200))
as

begin
   
   declare @Split_Length int

   while len(@String) > 0
   begin
      select @Split_Length =   case charindex(@Delimiter,@String)
                           when 0 then len(@String)
                           else charindex(@Delimiter,@String) - 1
                        end
                        
      insert into @Split_Values
      (
         String_Value
      )
      (
         select   substring(@String,1,@Split_Length)
      )
      
      select @String =   case (len(@String) - @Split_Length)
                        when 0 then ''
                        else right(@String,len(@String) - @Split_Length - 1)
                     end
   end
   
   return
   
end
GO
