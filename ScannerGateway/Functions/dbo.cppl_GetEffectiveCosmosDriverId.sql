SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[cppl_GetEffectiveCosmosDriverId]
(
	@CosmosDriverNumber		varchar(20),
	@CosmosBranchCode		varchar(10),
	--@ProntoDriverCode		varchar(10),
	@Date					datetime
)
RETURNS uniqueidentifier
AS
BEGIN


     --'=====================================================================
    --' CP -Stored Procedure -[cppl_GetEffectiveCosmosDriverId]
    --' ---------------------------
    --' Purpose: Gets effective Cosmos Driver ID-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 26 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                                              Bookmark
    --' ----          ---     ---     -----                                                                                                -------
    --' 26/09/2014    AB      1.00    Removed ProntoDriverCode and considered only drivers active(isactive=1)                             --AB20140926
	--  17/11/2014    AB      1.1     Removed Active drivers condition                                                                    --AB20141117
    --'=====================================================================

	DECLARE @DriverId			uniqueidentifier
	SELECT @DriverId = Null;

	WITH TempDrivers (Id, EffectiveDate, IsActive)
	AS
	(	
		SELECT d.Id, d.EffectiveDate, d.IsActive
		FROM Driver d
		JOIN Branch b
		ON d.BranchId = b.Id
		WHERE
			d.Code = ISNULL(@CosmosDriverNumber, '')
			--AND ISNULL(d.ProntoDriverCode, '') = ISNULL(@ProntoDriverCode, '')---#AB20140926
			AND b.CosmosBranch = ISNULL(@CosmosBranchCode, '')
			AND (d.EffectiveDate <= @Date
					OR CONVERT(date, d.EffectiveDate) = CONVERT(date, @Date))
				--	and d.isactive=1---#AB20141117
	)
	SELECT TOP 1 @DriverId = Id
	FROM TempDrivers
	ORDER BY EffectiveDate DESC, IsActive DESC;

	IF @DriverId Is Null
	BEGIN
		/* Sometimes we only get a driver record from Cosmos
		   2-3 days after the scanner was provided to the driver.
		   
		   If we haven't got a valid Driver Id here, we'll check and 
		   see if there's just one driver record with an effective
		   date within a couple of days of the scan date.
		   
		   If there's more than one driver record, then we can't really
		   assume which one is the correct one - it will have to be reviewed
		   and the records updated manually, as required */

		IF (SELECT COUNT(*) FROM Driver d
				JOIN Branch b
				ON d.BranchId = b.Id
				WHERE
					d.Code = ISNULL(@CosmosDriverNumber, '')
					--AND ISNULL(d.ProntoDriverCode, '') = ISNULL(@ProntoDriverCode, '')---#AB20140926
					AND b.CosmosBranch = ISNULL(@CosmosBranchCode, '') ) = 1------#AB20140926
		BEGIN
			-- there's only one driver record, so we'll assume this is the one
			-- as long as the EffectiveDate is within a week of the scan date
			DECLARE @id uniqueidentifier, @dt datetime
			
			SELECT @id = d.Id, @dt = d.EffectiveDate
			FROM Driver d
				JOIN Branch b
				ON d.BranchId = b.Id
				WHERE
					d.Code = ISNULL(@CosmosDriverNumber, '')
					--AND ISNULL(d.ProntoDriverCode, '') = ISNULL(@ProntoDriverCode, '')---#AB20140926
					AND b.CosmosBranch = ISNULL(@CosmosBranchCode, '')
					--and d.isactive=1---#AB20141117
					
			IF ((ISNULL(@dt, '1jan1900') BETWEEN 
				(DATEADD(day, -7, GETDATE())) AND (DATEADD(day, 7, GETDATE())))
				AND
				(@id Is Not Null))
			BEGIN
				-- let's assume this driver is OK to use
				SELECT @DriverId = @id
			END
		END

	END

	RETURN @DriverId;
END


GO
