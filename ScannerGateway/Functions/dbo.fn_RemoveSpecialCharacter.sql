SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 22/06/2020
-- Description:	To remove special character and replace with '-'
-- =============================================
CREATE FUNCTION [dbo].[fn_RemoveSpecialCharacter] 
(
@str VARCHAR(500)
)
RETURNS VARCHAR(500)
AS
BEGIN
	
	DECLARE @expression  VARCHAR(50) = '%[~,@,#,$,%,&,*,(,),.,!,/,\,?,`,:,;,^,'''']%'
	WHILE PATINDEX( @expression, ISNULL(@str,'') ) > 0
          SET @str = REPLACE( @str, SUBSTRING( @str, PATINDEX( @expression, @str ), 1 ),'-')
		  	
	RETURN Isnull(@str,'')
END
GO
