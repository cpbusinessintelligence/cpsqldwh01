SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[CPPL_RPTQueryFreightByCompanyAdhocRep_BackupHB_10072018]
(  @Branch Varchar(100),
@StartDate DateTime,
@EndDate DateTime
)
 as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[CPPL_RPTQueryFreightByCompanyAdhocRep] 'Melbourne','2018-01-25','2018-02-05'
    --' ---------------------------
    --' Purpose: Updates Freight Exception table everyday for Freight Exception Report-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 15 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                                                              Bookmark
    --' ----          ---     ---     -----                                                                                                               -------
    --' 15/09/2014    AB      1.00    To Prevent empty reports being sent in subscription,Created a table and loaded into it    
	--' 09/02/2018    TS      1.00    Added New Exception --- 6515 - Expired POP Parcel  to the stored procedure ( As per Req ID - 6546)                          --AB20140915
	--  15/02/2018    SS      1.01    Added 6512 and 6514
    --'=====================================================================
	--execute [dbo].[CPPL_RPTQueryFreightByCompany]




  
      CREATE TABLE #TempScanners(
	        [ScannerNumber] [varchar](20) NULL,
	        [Name] [varchar](50) NULL)
	        
	 -- INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6500','QUERY FREIGHT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6501','SHORT/SPLIT CONSIGNMENT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6502','INCORRECT/INSUFFICIENT ADDRESS')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6503','DAMAGED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6504','NO FREIGHT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6505','CLOSED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6506','CONNOTE REQUIRED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6507','CARD LEFT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6508','DG PAPERWORK REQUIRED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6509','REFUSED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6510','NO ACCESS TO LEAVE CARD')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6511','UNSAFE TO LEAVE')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6512','INCORRECT WEIGHT / CUBE')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6513', 'RETURN TO SENDER')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6514', 'INCORRECT SERVICE CODE')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6515', 'EXPIRED POP POINT PARCEL')
      CREATE clustered index #TempScannersidx ON #TempScanners([ScannerNumber])


     	  SELECT ET.DriverId,ET.LabelId  ,ET.AdditionalText1 ,T.Name,ET.SourceReference ,max(ET.EventDateTime ) as EventDateTime
	       INTO #Temp1
	       from scannergateway.dbo.TrackingEvent ET (NOLOCK) Join #TempScanners T on ET.AdditionalText1 = T.ScannerNumber
	       WHere ET.EventDateTime >= @StartDate and ET.EventDateTime <= DATEADD(day, 1, @EndDate)
		   group by ET.DriverId,ET.LabelId  ,ET.AdditionalText1 ,T.Name,ET.SourceReference
     
      
      Select T.EventDateTime, DATEDIFF(DAY,T.EventDateTime, GETDATE()) as Duration ,T.AdditionalText1 as QueryCage ,T.SourceReference as Label
                         ,D.Code,D.Name as DriverName ,DP.Name as Depot ,D.ProntoDriverCode,B.Name as Branch
                            into #Temp2
                            from #Temp1 T LEft Join scannergateway.dbo.Driver D on T.DriverId = D.Id
                                          Left Join scannergateway.dbo.Depot DP on D.DepotId = DP.Id 
                                          Left Join scannergateway.dbo.Branch B on D.BranchId = B.iD where B.Name  in (select B.name from fnSplitString( @Branch,','))
                            ORDER By T.EventDateTime asc
      


      CREATE clustered index #Temp2idx ON #Temp2(Label)

      --Select T.Label as LabelNumber into #TempDelivered  from #Temp2 T 
      --                      Join  scannergateway.dbo.TrackingEvent ET (NOLOCK) on T.Label=ET.Sourcereference and ET.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and ET.Eventdatetime>T.EventDateTime
                                             
      
      --Select L.LabelNumber into #TempDelivered  from #Temp2 T Join scannergateway.dbo.Label  L (NOLOCK) on T.Label =  L.LabelNumber 
      --                      Join  scannergateway.dbo.TrackingEvent ET (NOLOCK) on L.Id = ET.LabelId and ET.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and ET.Eventdatetime>T.EventDateTime
                                                   
     --Delete #Temp2    From #Temp2 T2 Join  #TempDelivered TD on T2.Label  = Td.LabelNumber  
      

     Select Label,MAX(QUeryCage) as QueryCage, 
                 CONVERT(Varchar(200),'') as cd_id,
                 CONVERT(Varchar(200),'') as ConNote,
                 CONVERT(Date,Null) as ConNoteDate,
                 CONVERT(Varchar(5000),'') as DeliveryName,
				  CONVERT(Varchar(200),'') as DeliveryContactPhone,
				  CONVERT(Varchar(200),'') as DeliveryContactEmail,
                 CONVERT(Varchar(50),'') as Reference, 
                 CONVERT(Varchar(20),'') as RevType, 
                 CONVERT(Varchar(500),'') as CustomerAccount, 
                 CONVERT(Varchar(500),'') as CustomerName, 
				 CONVERT(Varchar(500),'') as ParentCustomerAcount,
                 CONVERT(Varchar(500),'') as SendingBranch,
                 CONVERT(Varchar(50),'') as SellingBranch,
                 CONVERT(DateTime,Null) as DateScanned,
                 CONVERT(Varchar(20),'') as ScannedDriverCode, 
                 CONVERT(Varchar(50),'') as ScannedDriverName, 
                 CONVERT(Varchar(20),'') as ScannedProntoDriverCode, 
                 CONVERT(Varchar(20),'') as ScannedBranch
               
      into #TempQuery  From #Temp2 Group by  Label
     Update #TempQuery SET RevType =  CASE WHEN LEN(Label) =11 and ISNUMERIC(label) =1 THEN 'Prepaid' ELSE 'EDI' END 
     
     
     
     Update #TempQuery SET CustomerAccount = S.Accountcode ,SellingBranch = S.SerialWhseCode From #TempQuery T Join Pronto.dbo.ProntoStockSerialNumber S on T.Label = S.SerialNumber 
                  
                      
     Update #TempQuery SET cd_id = CD.cd_id, ConNote = CD.cd_connote,ConNoteDate = CD.cd_date,DeliveryName =CD.cd_delivery_addr0, DeliveryContactPhone=CD.cd_delivery_contact_phone,DeliveryContactEmail=cd_delivery_email,CustomerAccount = CD.cd_account,SendingBranch = B.b_name From #TempQuery T Join cppledi.dbo.cdcoupon C on T.Label = C.cc_coupon 
                                                                  Join cppledi.dbo.consignment CD on  C.cc_consignment = CD.cd_id 
                                                                  Join cppledi.dbo.branchs B on CD.cd_pickup_branch = B.b_id
                                                WHERE isnull(CustomerAccount,'') = ''
                                                
      --Delete  #TempQuery From #TempQuery T   Join cppledi.dbo.cdcoupon C on T.Label = C.cc_coupon 
      --                                       Join cppledi.dbo.consignment CD on  C.cc_consignment = CD.cd_id   
      --                                       WHERE CD.cd_deliver_stamp is not null                                   
 
     
      Update   #TempQuery SET Reference =  Left(R.cr_reference,50)  From #TempQuery T join cpplEDI.dbo.cdref R on T.cd_id = R.cr_consignment   
      Update #TempQuery SET CustomerName = D.Shortname , ParentCustomerAcount=D.BillTo   From #TempQuery T join Pronto.dbo.ProntoDebtor D on T.CustomerAccount = D.Accountcode


      Update #TempQuery SET SendingBranch =  CASE WHEN (SellingBranch  like 'A%' OR  SellingBranch = 'CAD')  THEN 'Adelaide'
                                                  WHEN (SellingBranch like 'B%' OR  SellingBranch = 'CBN') THEN 'Brisbane' 
                                                  WHEN (SellingBranch like 'S%' OR SellingBranch like 'C%' OR  SellingBranch = 'CSY' OR  SellingBranch = 'CCB' )   THEN 'Sydney' 
                                                  WHEN (SellingBranch like 'M%'OR  SellingBranch = 'CME') THEN 'Melbourne' 
                                                  WHEN (SellingBranch like 'G%'OR  SellingBranch = 'COO') THEN  'Gold Coast' 
                                                  WHEN (SellingBranch like 'P%' OR  SellingBranch = 'CPE') THEN 'Perth' ELSE 'Unknown' END   where SendingBranch = '' 
                                                  
    Update #TempQuery SET DateScanned =  T2.EventDateTime ,ScannedDriverCode = T2.Code ,ScannedDriverName = T2.DriverName 
                         ,ScannedProntoDriverCode = T2.ProntoDriverCode,ScannedBranch =T2.Branch 
    from #TempQuery TQ Join #Temp2 T2 on TQ.Label = T2.Label and TQ.QueryCage = T2.QueryCage 
    
 
    Select *,CONVERT(Varchar(20),'') as LinkCardNumber, CONVERT(Varchar(20),'') as LinkCardDate Into #TempFinal from #TempQuery TQ  Join #TempScanners TS on  TQ.QueryCage = TS.ScannerNumber 
   
    Select T.Label , Rtrim(Ltrim(Replace(ET.AdditionalText1,'Link Coupon',''))) as AdditionalText1, ET.EventDateTime  Into #TempLinkScans from #TempFinal T Join   scannergateway.dbo.Label L on L.LabelNumber =  T.Label  
                         Join scannergateway.dbo.TrackingEvent ET on ET.LabelId = L.ID   
                         and Et.EventTypeId =   'A341A7FC-3E0E-4124-B16E-6569C5080C6D'
                         
   Update    #tempFinal SET   LinkCardNumber = TL.AdditionalText1 , LinkCardDate = TL.EventDateTime  From      #tempFinal TF join    #TempLinkScans TL on TF.Label= TL.Label  WHere    TL.AdditionalText1 Like '191%'  or TL.AdditionalText1 like '%DPCNA%'
 

   
   --Select T.Label,T.AdditionalText1 ,ET.EventDateTime into #TempD  From #TempLinkScans T Join   scannergateway.dbo.Label L on L.LabelNumber =  T.AdditionalText1 
   --                      Join scannergateway.dbo.TrackingEvent ET on ET.LabelId = L.ID          
   --      WHere    ET.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
         
         
         
         
  ----Delete From  #tempFinal  Where Label in (Select Label From  #TempD)

   
   Select ConNote,Label,Reference,ConNoteDate,QueryCage,[Name]  ,CustomerAccount ,CustomerName,ParentCustomerAcount,DeliveryName, DateScanned ,ScannedBranch,SendingBranch,LinkCardNumber ,LinkCardDate,getdate(),DeliveryContactPhone as DeliveryContactPhone,DeliveryContactEmail as DeliveryEmail from #tempFinal  order by CustomerAccount,ConNoteDate

   end
GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPTQueryFreightByCompanyAdhocRep_BackupHB_10072018]
	TO [ReportUser]
GO
