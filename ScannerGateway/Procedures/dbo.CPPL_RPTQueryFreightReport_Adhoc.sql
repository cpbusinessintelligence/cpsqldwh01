SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CPPL_RPTQueryFreightReport_Adhoc] 
(  @Branch Varchar(20),
@StartDate DateTime,
@EndDate DateTime


)
AS
      --Declare @StartDate As DateTime = DateAdd(day,-14,getdate())
      --Declare @EndDate  as Datetime =  GETDATE()

  
      CREATE TABLE #TempScanners(
	        [ScannerNumber] [varchar](20) NULL,
	        [Name] [varchar](50) NULL)


	--' 09/02/2018    TS      1.00    Added New Exception --- 6515 - Expired POP Parcel  to the stored procedure ( As per Req ID - 6546)
	        
	  --INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6500','QUERY FREIGHT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6501','SHORT/SPLIT CONSIGNMENT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6502','INCORRECT/INSUFFICIENT ADDRESS')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6503','DAMAGED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6504','NO FREIGHT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6505','CLOSED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6506','CONNOTE REQUIRED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6507','CARD LEFT')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6508','DG PAPERWORK REQUIRED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6509','REFUSED')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6510','NO ACCESS TO LEAVE CARD')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6511','UNSAFE TO LEAVE')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6512','Incorrect Weight / Cube')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6513','Return to Sender')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6514','Incorrect Service Code')
	  INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6515', 'EXPIRED POP POINT PARCEL')
      CREATE clustered index #TempScannersidx ON #TempScanners([ScannerNumber])
      
      
     
	
	  SELECT ET.EventDateTime ,ET.DriverId,ET.LabelId  ,ET.AdditionalText1 ,T.Name,ET.SourceReference 
	       INTO #Temp1
	       from dbo.TrackingEvent ET Join #TempScanners T on ET.AdditionalText1 = T.ScannerNumber
	       WHere ET.EventDateTime >= @StartDate 
     
      
      Select T.EventDateTime, DATEDIFF(DAY,T.EventDateTime, GETDATE()) as Duration ,T.AdditionalText1 as QueryCage ,T.SourceReference as Label
                         ,D.Code,D.Name as DriverName ,DP.Name as Depot ,D.ProntoDriverCode,B.Name as Branch
                            into #Temp2
                            from #Temp1 T LEft Join dbo.Driver D on T.DriverId = D.Id
                                          Left Join dbo.Depot DP on D.DepotId = DP.Id 
                                          Left Join dbo.Branch B on D.BranchId = B.iD
                            ORDER By T.EventDateTime asc
      
      CREATE clustered index #Temp2idx ON #Temp2(Label)
      
     Select L.LabelNumber into #TempDelivered  from #Temp2 T Join dbo.Label  L on T.Label =  L.LabelNumber 
                            Join  dbo.TrackingEvent ET on L.Id = ET.LabelId and ET.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
                                                   
     Delete #Temp2    From #Temp2 T2 Join  #TempDelivered TD on T2.Label  = Td.LabelNumber  
    
   --  Select * from #Temp2
     
     Select Label,MAX(QUeryCage) as QueryCage, 
                 CONVERT(Varchar(20),'') as RevType, 
                 CONVERT(Varchar(20),'') as CustomerAccount, 
                 CONVERT(Varchar(50),'') as CustomerName, 
                 CONVERT(Varchar(50),'') as ConNote, 
                 CONVERT(Varchar(50),'') as SendingBranch,
                 CONVERT(Varchar(50),'') as SellingBranch,
                 CONVERT(DateTime,Null) as DateScanned,
                 CONVERT(Varchar(20),'') as ScannedDriverCode, 
                 CONVERT(Varchar(50),'') as ScannedDriverName, 
                 CONVERT(Varchar(20),'') as ScannedProntoDriverCode, 
                 CONVERT(Varchar(20),'') as ScannedBranch
               
      into #TempQuery  From #Temp2 Group by  Label
     Update #TempQuery SET RevType =  CASE WHEN LEN(Label) =11 and ISNUMERIC(label) =1 THEN 'Prepaid' ELSE 'EDI' END 
    
 
     
     Update #TempQuery SET CustomerAccount = S.Accountcode ,SellingBranch = S.SerialWhseCode From #TempQuery T Join Pronto.dbo.ProntoStockSerialNumber S on T.Label = S.SerialNumber 
                    --  Where T.RevType = 'Prepaid'
                      
     Update #TempQuery SET CustomerAccount = CD.cd_account,SendingBranch = B.b_name ,ConNote = CD.cd_connote 
                                               From #TempQuery T Join cppledi.dbo.cdcoupon C on T.Label = C.cc_coupon 
                                                                  Join cppledi.dbo.consignment CD on  C.cc_consignment = CD.cd_id 
                                                                  Join cppledi.dbo.branchs B on CD.cd_pickup_branch = B.b_id
                                                WHERE isnull(CustomerAccount,'') = ''
                                                
      Delete  #TempQuery From #TempQuery T   Join cppledi.dbo.cdcoupon C on T.Label = C.cc_coupon 
                                             Join cppledi.dbo.consignment CD on  C.cc_consignment = CD.cd_id   
                                             WHERE CD.cd_deliver_stamp is not null                                   
 
     
  
      Update #TempQuery SET CustomerName = D.Shortname From #TempQuery T join Pronto.dbo.ProntoDebtor D on T.CustomerAccount = D.Accountcode
      Update #TempQuery SET SendingBranch =  CASE WHEN (SellingBranch  like 'A%' OR  SellingBranch = 'CAD')  THEN 'Adelaide'
                                                  WHEN (SellingBranch like 'B%' OR  SellingBranch = 'CBN') THEN 'Brisbane' 
                                                  WHEN (SellingBranch like 'S%' OR SellingBranch like 'C%' OR  SellingBranch = 'CSY' OR  SellingBranch = 'CCB' )   THEN 'Sydney' 
                                                  WHEN (SellingBranch like 'M%'OR  SellingBranch = 'CME') THEN 'Melbourne' 
                                                  WHEN (SellingBranch like 'G%'OR  SellingBranch = 'COO') THEN  'Gold Coast' 
                                                  WHEN (SellingBranch like 'P%' OR  SellingBranch = 'CPE') THEN 'Perth' ELSE 'Unknown' END   where SendingBranch = '' 
                                                  
    Update #TempQuery SET DateScanned =  T2.EventDateTime ,ScannedDriverCode = T2.Code ,ScannedDriverName = T2.DriverName 
                         ,ScannedProntoDriverCode = T2.ProntoDriverCode,ScannedBranch =T2.Branch 
    from #TempQuery TQ Join #Temp2 T2 on TQ.Label = T2.Label and TQ.QueryCage = T2.QueryCage 
    
    -- SElect * from #TempQuery TQ Join #Temp2 T2 on TQ.Label = T2.Label and TQ.QueryCage = T2.QueryCage 
  
       Select T.Label , Rtrim(Ltrim(Replace(ET.AdditionalText1,'Link Coupon',''))) as AdditionalText1, ET.EventDateTime  Into #TempLinkScans from #TempQuery T Join   dbo.Label L on L.LabelNumber =  T.Label  
                         Join dbo.TrackingEvent ET on ET.LabelId = L.ID   
                         and Et.EventTypeId =   'A341A7FC-3E0E-4124-B16E-6569C5080C6D'
                         
      
                         
     Select T.Label,T.AdditionalText1 ,ET.EventDateTime into #TempD  From #TempLinkScans T Join   dbo.Label L on L.LabelNumber =  T.AdditionalText1 
                         Join dbo.TrackingEvent ET on ET.LabelId = L.ID          
         WHere    ET.EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
         
 
         
    Delete From  #TempQuery  Where Label in (Select Label From  #TempD)                    
      
    IF @Branch = 'ALL'
          Select TS.*,TQ.* from #TempQuery TQ  Join #TempScanners TS on  TQ.QueryCage = TS.ScannerNumber
    ELSE 
         Select TS.*,TQ.* from #TempQuery TQ  Join #TempScanners TS on  TQ.QueryCage = TS.ScannerNumber Where TQ.ScannedBranch =@Branch


GO
