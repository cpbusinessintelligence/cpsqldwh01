SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CPPL_RPT_ContractorScansVsRedemptions]



 (@Branch Varchar(20),



  @RunNumber Varchar(10),



  @StartDate Date,



  @EndDate Date,



  @RunAll varchar(5))



AS



BEGIN







	SET NOCOUNT ON;



	



-- Get all Scanning Events fom Scanner Gateway



SELECT L.LabelNumber 



       ,ISNULL(Pronto.dbo.CPPL_fn_GetCouponTypeFromPrefix(CASE when (ISNUMERIC(L.LabelNumber) = 1 and len(L.LabelNumber)=11) THEN LEFT( L.LabelNumber,3) ELSE '' END),'EDI') AS COuponType



       ,CONVERT (varchar(20),'') as COnnote



      ,E.Description as EventDescr



      ,D.Code



      ,D.ProntoDriverCode



      ,B.CosmosBranch



      ,[EventDateTime]



      ,CASE E.Description WHen 'Link Scan' THEN Rtrim(Ltrim(Replace([AdditionalText1],'Link Coupon',''))) ELse '' END As LinkCoupon



      ,[NumberOfItems]



      --,CONVERT(Date,Null) as RCTIDate



      --,CONVERT(decimal(12,0),0) as RCTIAmount



      --,Convert(varchar(50),'') as Reason



  INTo #TempScans



  FROM 
  
  [dbo].[TrackingEvent] T (NOLOCK) 

--  (select * from [dbo].[TrackingEvent]
--union
--select * from [dbo].[TrackingEvent_Archive2016_17]) T
  
  Join 
   
   
   dbo.Label L (NOLOCK) on T.LabelId = L.Id 



                                     Join dbo.Driver D (NOLOCK) on T.DriverId = D.ID



                                     Join dbo.EventType E (NOLOCK) on T.EventTypeId = E.ID



                                     Join dbo.Branch B (NOLOCK) on  D.BranchId = B.Id 



  



  Where   CONVERT(Date,[EventDateTime]) between @StartDate  and @EndDate



    And       B.CosmosBranch = @Branch and D.Code  = @RunNumber



    ANd EventTypeId in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3'



                       ,'41A8F8F9-D57E-40F0-9D9D-97767AC3069E'



                       ,'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'



                       ,'A341A7FC-3E0E-4124-B16E-6569C5080C6D'



                       ,'98EBB899-A15E-4826-8D05-516E744C466C')



   



   







    --GEt all The Link Scans



    



   --Insert Into #TempScans 



   -- Select LinkCoupon,'LINK','','',Code,ProntoDriverCode,CosmosBranch,[EventDateTime],'',0 From #TempScans Where EventDescr = 'Link Scan' and ISNUMERIC(LinkCOupon) = 1



      



  --GEt the consignment Number form EDI Gateway for EDI Scans 



      



  Update #TempScans SEt COnnote = CD.cd_connote



  From #TempScans T LEft Join cpplEDI.dbo.cdcoupon CC ON T.LabelNumber = CC.cc_coupon 



                                    Join CpplEDI.dbo.consignment CD ON CD.cd_id = CC.cc_consignment



                                    Where T.COuponType= 'EDI'



                                    



                                                        











SELECT distinct LabelNumber 



       ,COuponType



      ,CASE COuponType When 'EDI' Then 'EDI' ELSE 'Prepaid' END as CouponCategory



      ,EventDescr



      ,Code



      ,ProntoDriverCode



      ,CosmosBranch



      ,EventDateTime



      ,LinkCoupon



      ,[NumberOfItems]



      ,CONVERT(Date,Null) as SoldDate



      ,CONVERT(varchar(20),'') as RCTItype



      ,CONVERT(Date,Null) as RCTIDate



      ,CONVERT(decimal(12,2),0) as ProntoRCTIAmount



      ,CONVERT(decimal(12,2),0) as GatewayRCTIAmount



      ,Convert(varchar(50),'') as Reason



  INTO #TempFinal



  From #TempScans Where COuponType <>'EDI' and EventDescr <> 'Link Scan'



  Union all



  Select Distinct COnnote,'EDI' ,'EDI',min(EventDescr),MAX(code),MAX(ProntoDriverCode),MAX(CosmosBranch),MAX(EventDateTime),'',COUNT(*),Null,'',Null,0,0,''



   from #TempScans Where COuponType = 'EDI'



  Group by  COnnote







  update #tempfinal set COuponType=(case when isnumeric(labelnumber)=1 and len(labelnumber)=11 then 'Prepaid' else 'EDI' end)



  where COuponType='Prepaid'







  --Get all RCTI details from Pronto COupon Details                     



                       



     Select distinct T.LabelNumber 



			,PCD.StartSerialNumber 



			,PCD.TransferDate 



			,PCD.LastSoldDate as SoldDate



			,PCD.LastSoldReference as SoldRef



			,PCD.PickupContractor



			,PCD.PickupRctiAmount



			,PCD.PickupRctiDate



			,PCD.DeliveryContractor



			,PCD.DeliveryRctiAmount



			,PCD.DeliveryRctiDate



      INto #TempRCTI



      from #TempFinal T LEft join Pronto.Dbo.ProntoCouponDetails  PCD     on T.LabelNumber = PCD.SerialNumber 



   --   Where T.COuponType <> 'EDI' 



   -- Union ALl



   -- Select distinct  T.connote 



			--	, PCD.StartSerialNumber 



			--	,PCD.TransferDate 



			--	,PCD.LastSoldDate as SoldDate



			--	,PCD.LastSoldReference as SoldRef



			--	,PCD.PickupContractor



			--	,PCD.PickupRctiAmount



			--	,PCD.PickupRctiDate



			--	,PCD.DeliveryContractor



			--	,PCD.DeliveryRctiAmount



			--	,PCD.DeliveryRctiDate



   --    from #TempScans T LEft join RDS.Dbo.ProntoCouponDetails  PCD     on T.connote = PCD.SerialNumber 



   --    Where T.COuponType = 'EDI'  











                                    



 --Select distinct cd_connote ,COUNT(*) as ct from #TempEDI group by cd_connote                             



                                    



      -- where Coupontype = 'EDI'       



        



     Update #TempFinal SEt RCTItype = 'PICKUP', RCTIDate = T2.PickupRCTiDate, ProntoRCTIAmount  = T2.PickupRctiAmount



      From #TempFinal T1 Join   #TempRCTI T2 On T1.LabelNumber = T2.LabelNumber



        Where  T1.ProntoDriverCode = T2.PickupContractor   and t1.EventDescr = 'Pickup'           



                                       



                                       



                                       



   



     Update #TempFinal SEt RCTItype = 'DELIVERY', RCTIDate = T2.DeliveryRctiDate, ProntoRCTIAmount = T2.DeliveryRctiAmount



      From #TempFinal T1 Join   #TempRCTI T2 On T1.LabelNumber = T2.LabelNumber



        Where  T1.ProntoDriverCode = T2.DeliveryContractor   and t1.EventDescr in ('Delivered','Attempted Delivery')



        



      Update #TempFinal SET GatewayRCTIAmount = CASE RCTItype WHEN 'PICKUP' THEN C.cd_pickup_payment WHEN 'DELIVERY' THEN C.cd_deliver_payment  ELSE 0  END  From #TempFinal T Join  cpplEDI.dbo.consignment C  on T.LabelNumber = C.cd_connote 



               Where T.COuponType = 'EDI' 



                                       



                                       



        



      Update #TempFinal SEt Solddate = T2.SoldDate



      From #TempFinal T1 Join   #TempRCTI T2 On T1.LabelNumber = T2.LabelNumber



        Where  COuponType<>'EDI'      



             Update  #TempFinal SET Reason = 'No RCTI Dates Untill Last Saturday' WHere ProntoRCTIAmount>0 and RCTIDate is null    and COuponType <> 'EDI'                    



        Update  #TempFinal SET Reason = 'Coupon Not Sold Until Last Saturday' WHere SoldDate is null and COuponType <> 'EDI'  and CouponType not in ( 'ATL','RETURN TRK') 



              Update  #TempFinal SET Reason = 'Please Check' WHere  Reason = '' and ProntoRCTIAmount = 0 and COuponType <> 'EDI' and CouponType not in ( 'ATL','RETURN TRK') 



             update #tempfinal set Reason='To be removed' from #tempfinal t join [Pronto].[dbo].[ProntoCouponDetails] c on c.serialnumber=t.labelnumber where t.EventDescr='Delivered' and t.Reason='Please Check'



		  delete from #tempFinal where Reason='To be removed'



       IF @RunAll= 'Y'



         Select  distinct *  from #TempFinal 



       ELSE



         Select  distinct *  from #TempFinal Where CouponType not in ( 'ATL') and RCTIDate is null  and labelnumber not like ('[2-9]14%') and labelnumber not like ('183%') 



		 union all 



		 Select  distinct *  from #TempFinal Where ProntoRCTIAmount<>GatewayRCTIAmount and COuponType='EDI'



  	SET NOCOUNT OFF;



	



END












GO
GRANT EXECUTE
	ON [dbo].[CPPL_RPT_ContractorScansVsRedemptions]
	TO [ReportUser]
GO
