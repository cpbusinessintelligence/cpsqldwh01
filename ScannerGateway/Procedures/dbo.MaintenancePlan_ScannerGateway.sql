SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[MaintenancePlan_ScannerGateway]
As
Begin

---REORGANIZE	
	
	ALTER INDEX [IX_Agent_ShortCode] ON [dbo].[Agent] REORGANIZE  
	
	
	
	ALTER INDEX [PK_Agent] ON [dbo].[Agent] REORGANIZE  
	
	
	
	ALTER INDEX [PK__AgentSta__CA1FE4641B497A39] ON [dbo].[AgentStatusStaging] REORGANIZE  
	
	
	
	ALTER INDEX [PK_now_bookings] ON [dbo].[Archive_now_bookings] REORGANIZE  
	
	
	
	ALTER INDEX [idx_Branch_IdCosmosBranch] ON [dbo].[Branch] REORGANIZE  
	
	
	
	ALTER INDEX [IX_Branch_Code] ON [dbo].[Branch] REORGANIZE  
	
	
	
	ALTER INDEX [PK_Branch] ON [dbo].[Branch] REORGANIZE  
	
	
	
	ALTER INDEX [PK_Country] ON [dbo].[Country] REORGANIZE  
	
	
	
	ALTER INDEX [IX_Depot_Code] ON [dbo].[Depot] REORGANIZE  
	
	
	
	ALTER INDEX [PK_Depot] ON [dbo].[Depot] REORGANIZE  
	
	
	
	ALTER INDEX [idx_Driver_CodeProntoEffectiveBranch] ON [dbo].[Driver] REORGANIZE  
	
	
	
	ALTER INDEX [idx_Driver_IdProntoCode] ON [dbo].[Driver] REORGANIZE  
	
	
	
	ALTER INDEX [IX_Driver] ON [dbo].[Driver] REORGANIZE  
	
	
	
	ALTER INDEX [IX_Driver_BranchId] ON [dbo].[Driver] REORGANIZE  
	
	
	
	ALTER INDEX [PK_Driver] ON [dbo].[Driver] REORGANIZE  
	
	
	
	ALTER INDEX [PK_cppl_ScannerGW_ErrorLog] ON [dbo].[ErrorLog] REORGANIZE  
	
	
	
	ALTER INDEX [IX_EventType_Code] ON [dbo].[EventType] REORGANIZE  
	
	
	
	ALTER INDEX [PK_EventType] ON [dbo].[EventType] REORGANIZE  
	
	
	
	ALTER INDEX [idx_EventTypeMap_CodeType] ON [dbo].[EventTypeMap] REORGANIZE  
	
	
	
	ALTER INDEX [idx_EventTypeMap_TypeIdCodeIsDeleted] ON [dbo].[EventTypeMap] REORGANIZE  
	
	
	
	ALTER INDEX [PK_EventTypeMap] ON [dbo].[EventTypeMap] REORGANIZE  
	
	
	
	ALTER INDEX [PK__EzyTrakS__CA1FE464C73F0EF0] ON [dbo].[EzyTrakStatusUpdateRequest] REORGANIZE  
	
	
	
	ALTER INDEX [idx_HubbedStaging_sno] ON [dbo].[HubbedStaging] REORGANIZE  
	
	
	
	ALTER INDEX [idx_Label_IdNumber] ON [dbo].[Label] REORGANIZE  
	
	
	
	ALTER INDEX [IX_Label_LabelNumber] ON [dbo].[Label] REORGANIZE  
	
	
	
	ALTER INDEX [PK_Label] ON [dbo].[Label] REORGANIZE  
	
	
	
	ALTER INDEX [idx_LabelArchive_IdNumber] ON [dbo].[Label_Archive2010-15] REORGANIZE  
	
	
	
	ALTER INDEX [PK_LoadAgentTrackingEvents] ON [dbo].[LoadAgentTrackingEvents] REORGANIZE  
	
	
	
	ALTER INDEX [PK_LoadScannerGatewayData3] ON [dbo].[LoadScannerGatewayData] REORGANIZE  
	
	
	
	ALTER INDEX [PK_State] ON [dbo].[State] REORGANIZE  
	
	
	
	ALTER INDEX [PK_SystemCodes] ON [dbo].[SystemCodes] REORGANIZE  
	
	
	
	ALTER INDEX [PK_tbl_CrunchTable_new] ON [dbo].[tbl_CrunchTable] REORGANIZE  
	
	
	
	ALTER INDEX [PK_tblCrunchTable_new] ON [dbo].[tblCrunchTable_all] REORGANIZE  
	
	
	
	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REORGANIZE  
	
	
	
	ALTER INDEX [PK__tblFMSCo__3214EC27C1018B52] ON [dbo].[tblFMSConsignments_Staging] REORGANIZE  
	
	
	
	ALTER INDEX [PK__tblFMSCo__3214EC27B64F7B77] ON [dbo].[tblFMSConsignments_Status] REORGANIZE  
	
	
	
	ALTER INDEX [idx_TrackingEvent_EventTypeText1DateTime] ON [dbo].[TrackingEvent] REORGANIZE  
	
	
	
	ALTER INDEX [idx_TrackingEvent_LabelDateIsDeleted] ON [dbo].[TrackingEvent] REORGANIZE  
	
	
	
	ALTER INDEX [IX_TrackingEvent_EventDateTime] ON [dbo].[TrackingEvent] REORGANIZE  
	
	
	
	ALTER INDEX [PK_TrackingEvent] ON [dbo].[TrackingEvent] REORGANIZE  
	
	
	
	ALTER INDEX [idx_TrackingEvent_Archive_EventTypeText1DateTime] ON [dbo].[TrackingEvent_Archive] REORGANIZE  
	
	
	
	ALTER INDEX [idx_TrackingEvent_Archive_LabelDateIsDeleted] ON [dbo].[TrackingEvent_Archive] REORGANIZE  
	
	
	
	ALTER INDEX [PK_tblTrustPilotAPIExcludedAccounts] ON [dbo].[TrustPilotAPIExcludedAccounts] REORGANIZE  
	
	
	
	ALTER INDEX [IX_TrustPilotAPIStagingData_DeliveryDate] ON [dbo].[TrustPilotAPIStagingData] REORGANIZE  

----REBUILD


	
	
	ALTER INDEX [IX_Agent_ShortCode] ON [dbo].[Agent] REBUILD  
	
	
	
	ALTER INDEX [PK_Agent] ON [dbo].[Agent] REBUILD  
	
	
	
	ALTER INDEX [PK__AgentSta__CA1FE4641B497A39] ON [dbo].[AgentStatusStaging] REBUILD  
	
	
	
	ALTER INDEX [PK_now_bookings] ON [dbo].[Archive_now_bookings] REBUILD  
	
	
	
	ALTER INDEX [idx_Branch_IdCosmosBranch] ON [dbo].[Branch] REBUILD  
	
	
	
	ALTER INDEX [IX_Branch_Code] ON [dbo].[Branch] REBUILD  
	
	
	
	ALTER INDEX [PK_Branch] ON [dbo].[Branch] REBUILD  
	
	
	
	ALTER INDEX [PK_Country] ON [dbo].[Country] REBUILD  
	
	
	
	ALTER INDEX [IX_Depot_Code] ON [dbo].[Depot] REBUILD  
	
	
	
	ALTER INDEX [PK_Depot] ON [dbo].[Depot] REBUILD  
	
	
	
	ALTER INDEX [idx_Driver_CodeProntoEffectiveBranch] ON [dbo].[Driver] REBUILD  
	
	
	
	ALTER INDEX [idx_Driver_IdProntoCode] ON [dbo].[Driver] REBUILD  
	
	
	
	ALTER INDEX [IX_Driver] ON [dbo].[Driver] REBUILD  
	
	
	
	ALTER INDEX [IX_Driver_BranchId] ON [dbo].[Driver] REBUILD  
	
	
	
	ALTER INDEX [PK_Driver] ON [dbo].[Driver] REBUILD  
	
	
	
	ALTER INDEX [PK_cppl_ScannerGW_ErrorLog] ON [dbo].[ErrorLog] REBUILD  
	
	
	
	ALTER INDEX [IX_EventType_Code] ON [dbo].[EventType] REBUILD  
	
	
	
	ALTER INDEX [PK_EventType] ON [dbo].[EventType] REBUILD  
	
	
	
	ALTER INDEX [idx_EventTypeMap_CodeType] ON [dbo].[EventTypeMap] REBUILD  
	
	
	
	ALTER INDEX [idx_EventTypeMap_TypeIdCodeIsDeleted] ON [dbo].[EventTypeMap] REBUILD  
	
	
	
	ALTER INDEX [PK_EventTypeMap] ON [dbo].[EventTypeMap] REBUILD  
	
	
	
	ALTER INDEX [PK__EzyTrakS__CA1FE464C73F0EF0] ON [dbo].[EzyTrakStatusUpdateRequest] REBUILD  
	
	
	
	ALTER INDEX [idx_HubbedStaging_sno] ON [dbo].[HubbedStaging] REBUILD  
	
	
	
	ALTER INDEX [idx_Label_IdNumber] ON [dbo].[Label] REBUILD  
	
	
	
	ALTER INDEX [IX_Label_LabelNumber] ON [dbo].[Label] REBUILD  
	
	
	
	ALTER INDEX [PK_Label] ON [dbo].[Label] REBUILD  
	
	
	
	ALTER INDEX [idx_LabelArchive_IdNumber] ON [dbo].[Label_Archive2010-15] REBUILD  
	
	
	
	ALTER INDEX [PK_LoadAgentTrackingEvents] ON [dbo].[LoadAgentTrackingEvents] REBUILD  
	
	
	
	ALTER INDEX [PK_LoadScannerGatewayData3] ON [dbo].[LoadScannerGatewayData] REBUILD  
	
	
	
	ALTER INDEX [PK_State] ON [dbo].[State] REBUILD  
	
	
	
	ALTER INDEX [PK_SystemCodes] ON [dbo].[SystemCodes] REBUILD  
	
	
	
	ALTER INDEX [PK_tbl_CrunchTable_new] ON [dbo].[tbl_CrunchTable] REBUILD  
	
	
	
	ALTER INDEX [PK_tblCrunchTable_new] ON [dbo].[tblCrunchTable_all] REBUILD  
	
	
	
	ALTER INDEX [PK_tblErrorLog] ON [dbo].[tblErrorLog] REBUILD  
	
	
	
	ALTER INDEX [PK__tblFMSCo__3214EC27C1018B52] ON [dbo].[tblFMSConsignments_Staging] REBUILD  
	
	
	
	ALTER INDEX [PK__tblFMSCo__3214EC27B64F7B77] ON [dbo].[tblFMSConsignments_Status] REBUILD  
	
	
	
	ALTER INDEX [idx_TrackingEvent_EventTypeText1DateTime] ON [dbo].[TrackingEvent] REBUILD  
	
	
	
	ALTER INDEX [idx_TrackingEvent_LabelDateIsDeleted] ON [dbo].[TrackingEvent] REBUILD  
	
	
	
	ALTER INDEX [IX_TrackingEvent_EventDateTime] ON [dbo].[TrackingEvent] REBUILD  
	
	
	
	ALTER INDEX [PK_TrackingEvent] ON [dbo].[TrackingEvent] REBUILD  
	
	
	
	ALTER INDEX [idx_TrackingEvent_Archive_EventTypeText1DateTime] ON [dbo].[TrackingEvent_Archive] REBUILD  
	
	
	
	ALTER INDEX [idx_TrackingEvent_Archive_LabelDateIsDeleted] ON [dbo].[TrackingEvent_Archive] REBUILD  
	
	
	
	ALTER INDEX [PK_tblTrustPilotAPIExcludedAccounts] ON [dbo].[TrustPilotAPIExcludedAccounts] REBUILD  
	
	
	
	ALTER INDEX [IX_TrustPilotAPIStagingData_DeliveryDate] ON [dbo].[TrustPilotAPIStagingData] REBUILD  

----	Update Statistics



	UPDATE STATISTICS [dbo].[Agent] 
	



	UPDATE STATISTICS [dbo].[AgentEventTypeMapping] 
	



	UPDATE STATISTICS [dbo].[AgentPODStatus] 
	



	UPDATE STATISTICS [dbo].[AgentStatusStaging] 
	



	UPDATE STATISTICS [dbo].[AgentStatusStaging_Data] 
	



	UPDATE STATISTICS [dbo].[AgentStatusStaging_Error] 
	



	UPDATE STATISTICS [dbo].[AgentStatusStaging_Image] 
	



	UPDATE STATISTICS [dbo].[Archive_now_bookings] 
	



	UPDATE STATISTICS [dbo].[Barcode Sample] 
	



	UPDATE STATISTICS [dbo].[BEApiResponse] 
	



	UPDATE STATISTICS [dbo].[BEIntegrationStaging] 
	



	UPDATE STATISTICS [dbo].[BEPODApiResponse] 
	



	UPDATE STATISTICS [dbo].[BEPODIntegration] 
	



	UPDATE STATISTICS [dbo].[BorderExpressPODStatus] 
	



	UPDATE STATISTICS [dbo].[Branch] 
	



	UPDATE STATISTICS [dbo].[Call _Usage_Records] 
	



	UPDATE STATISTICS [dbo].[CommonAgentStatusMapping] 
	



	UPDATE STATISTICS [dbo].[CommonAgentStatusStaging] 
	



	UPDATE STATISTICS [dbo].[ConsDate] 
	



	UPDATE STATISTICS [dbo].[ConsignmentNumberSamplesBEX] 
	



	UPDATE STATISTICS [dbo].[CosmoSarchiveFileNames] 
	



	UPDATE STATISTICS [dbo].[Country] 
	



	UPDATE STATISTICS [dbo].[CP_Apr19] 
	



	UPDATE STATISTICS [dbo].[CP_May19] 
	



	UPDATE STATISTICS [dbo].[CustomerLabelStatus] 
	



	UPDATE STATISTICS [dbo].[CustomerStatusCodemapping] 
	



	UPDATE STATISTICS [dbo].[DeliveredData] 
	



	UPDATE STATISTICS [dbo].[DeliverEScanFilesData] 
	



	UPDATE STATISTICS [dbo].[Depot] 
	



	UPDATE STATISTICS [dbo].[Driver] 
	



	UPDATE STATISTICS [dbo].[Error_inTrackevent] 
	



	UPDATE STATISTICS [dbo].[ErrorLog] 
	



	UPDATE STATISTICS [dbo].[EventType] 
	



	UPDATE STATISTICS [dbo].[EventTypeMap] 
	



	UPDATE STATISTICS [dbo].[ExternalStatusStaging] 
	



	UPDATE STATISTICS [dbo].[EzyTrakEventMapping] 
	



	UPDATE STATISTICS [dbo].[EzyTrakStatusUpdateRequest] 
	



	UPDATE STATISTICS [dbo].[EzytrakStatusUpdateResponse] 
	



	UPDATE STATISTICS [dbo].[FreightExceptionReport_Microsoft] 
	



	UPDATE STATISTICS [dbo].[FreightExceptionTable] 
	



	UPDATE STATISTICS [dbo].[FreightExceptionTableArchive] 
	



	UPDATE STATISTICS [dbo].[HS] 
	



	UPDATE STATISTICS [dbo].[HubbedCardStaging] 
	



	UPDATE STATISTICS [dbo].[HubbedStaging] 
	



	UPDATE STATISTICS [dbo].[HubbedStaging_Archive_Before2020] 
	



	UPDATE STATISTICS [dbo].[HubbedStaging_Archive_Before2020July] 
	



	UPDATE STATISTICS [dbo].[HubbedStagingTMP] 
	



	UPDATE STATISTICS [dbo].[HubbedTMP] 
	



	UPDATE STATISTICS [dbo].[ImportTrackingEvents] 
	



	UPDATE STATISTICS [dbo].[JP_TEST] 
	



	UPDATE STATISTICS [dbo].[JustfreightPODStatus] 
	



	UPDATE STATISTICS [dbo].[Label] 
	



	UPDATE STATISTICS [dbo].[Label_Archive2010-15] 
	



	UPDATE STATISTICS [dbo].[Load_ImportTrackingEvents_Albury] 
	



	UPDATE STATISTICS [dbo].[LoadAgentTrackingEvents] 
	



	UPDATE STATISTICS [dbo].[LoadScannerGatewayData] 
	



	UPDATE STATISTICS [dbo].[Manual_Inv] 
	



	UPDATE STATISTICS [dbo].[ManualInvoicePP] 
	



	UPDATE STATISTICS [dbo].[ManualInvoiceTemp] 
	



	UPDATE STATISTICS [dbo].[MetroStatusMapping] 
	



	UPDATE STATISTICS [dbo].[MetroStatusStaing] 
	



	UPDATE STATISTICS [dbo].[MetroStatusStaing_Error] 
	



	UPDATE STATISTICS [dbo].[Mh] 
	



	UPDATE STATISTICS [dbo].[MidstatePODStatus] 
	



	UPDATE STATISTICS [dbo].[MidStatusMapping] 
	



	UPDATE STATISTICS [dbo].[MidStatusStaging] 
	



	UPDATE STATISTICS [dbo].[MidStatusStaing_Error] 
	



	UPDATE STATISTICS [dbo].[NowDrivers] 
	



	UPDATE STATISTICS [dbo].[NZPostAPIResponse] 
	



	UPDATE STATISTICS [dbo].[NZPostAPIResponseStaging] 
	



	UPDATE STATISTICS [dbo].[PerformanceReport_Pickup] 
	



	UPDATE STATISTICS [dbo].[PremonitionPOD] 
	



	UPDATE STATISTICS [dbo].[PremonitionPODStatus] 
	



	UPDATE STATISTICS [dbo].[Recreated Data] 
	



	UPDATE STATISTICS [dbo].[Report_RightwrongScans] 
	



	UPDATE STATISTICS [dbo].[ResentscansTemp] 
	



	UPDATE STATISTICS [dbo].[RP COupons] 
	



	UPDATE STATISTICS [dbo].[RPs] 
	



	UPDATE STATISTICS [dbo].[RREPODStatus] 
	



	UPDATE STATISTICS [dbo].[RTRNStag] 
	



	UPDATE STATISTICS [dbo].[RTRNStagCon] 
	



	UPDATE STATISTICS [dbo].[ScaanerFeb29temp] 
	



	UPDATE STATISTICS [dbo].[SekoStatusMapping] 
	



	UPDATE STATISTICS [dbo].[SekoStatusResponse] 
	



	UPDATE STATISTICS [dbo].[SekoStatusStaging_Error] 
	



	UPDATE STATISTICS [dbo].[SendMailAPI_Errors] 
	



	UPDATE STATISTICS [dbo].[SendMailAPIResponse] 
	



	UPDATE STATISTICS [dbo].[State] 
	



	UPDATE STATISTICS [dbo].[StatusMapping] 
	



	UPDATE STATISTICS [dbo].[StatusMapping_Seko] 
	



	UPDATE STATISTICS [dbo].[SystemCodes] 
	



	UPDATE STATISTICS [dbo].[tbl_CrunchTable] 
	



	UPDATE STATISTICS [dbo].[tbl_TempLoadPremonCountData_Prepaid] 
	



	UPDATE STATISTICS [dbo].[tbl_trackingevent_agent_Errors] 
	



	UPDATE STATISTICS [dbo].[tbl_TrackingEvent_Gateway] 
	



	UPDATE STATISTICS [dbo].[Tbl_TrackingEventFromAzure] 
	



	UPDATE STATISTICS [dbo].[tbl_TrackingEventNow] 
	



	UPDATE STATISTICS [dbo].[tblCrunchTable_all] 
	



	UPDATE STATISTICS [dbo].[tblErrorLog] 
	



	UPDATE STATISTICS [dbo].[tblFMSConsignments_Staging] 
	



	UPDATE STATISTICS [dbo].[tblFMSConsignments_Status] 
	



	UPDATE STATISTICS [dbo].[tblWineDeliveryCustomerStatusDeliverAPIResponse] 
	



	UPDATE STATISTICS [dbo].[Temp_JP_NowBilling] 
	



	UPDATE STATISTICS [dbo].[tempLabelNumber] 
	



	UPDATE STATISTICS [dbo].[Tempmask] 
	



	UPDATE STATISTICS [dbo].[TestSeko] 
	



	UPDATE STATISTICS [dbo].[tmp_CouponVsNowScan] 
	



	UPDATE STATISTICS [dbo].[tmp_MissingPODConsignment] 
	



	UPDATE STATISTICS [dbo].[tmp_MissingPODLabel] 
	



	UPDATE STATISTICS [dbo].[TollProcessedLabels] 
	



	UPDATE STATISTICS [dbo].[TollStatusMapping] 
	



	UPDATE STATISTICS [dbo].[TollStatusStaging] 
	



	UPDATE STATISTICS [dbo].[TollStatusStating_Error] 
	



	UPDATE STATISTICS [dbo].['Total Customer Lists'] 
	



	UPDATE STATISTICS [dbo].[TotalCustomerLists] 
	



	UPDATE STATISTICS [dbo].[TrackingEvent] 
	



	UPDATE STATISTICS [dbo].[TrackingEvent_Archive] 
	



	UPDATE STATISTICS [dbo].[TrackingEvent_Archive2018_19] 
	



	UPDATE STATISTICS [dbo].[trackingevent_deleted] 
	



	UPDATE STATISTICS [dbo].[trackingevent_temp] 
	



	UPDATE STATISTICS [dbo].[TransvirtualPODStatus] 
	



	UPDATE STATISTICS [dbo].[TrustPilotAPIExceptionResponse] 
	



	UPDATE STATISTICS [dbo].[TrustPilotAPIExcludedAccounts] 
	



	UPDATE STATISTICS [dbo].[TrustPilotAPIResponse] 
	



	UPDATE STATISTICS [dbo].[TrustPilotAPIStagingData] 
	



	UPDATE STATISTICS [dbo].[TrustPilotAPIStagingData_Archive] 
	



	UPDATE STATISTICS [dbo].[VictasPODStatus] 
	



	UPDATE STATISTICS [dbo].[VicTasStatusAgentStaging] 
	



	UPDATE STATISTICS [dbo].[VicTasStatusAgentStaging_SS1908] 
	



	UPDATE STATISTICS [dbo].[VicTasStatusMapping] 
	



	UPDATE STATISTICS [dbo].[WoolWorthsReportSatchelRange] 
	



	UPDATE STATISTICS [dbo].[WoolworthsStaging] 
	



	UPDATE STATISTICS [dbo].[workflow] 
	



	UPDATE STATISTICS [dbo].[Workflow_All] 
	



	UPDATE STATISTICS [dbo].[Workflowupdated] 
	

-- Shrink DB

	DBCC SHRINKDATABASE(N'ScannerGateway')
	
End	
GO
