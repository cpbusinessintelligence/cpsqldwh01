SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_GetCWCImages]
@Barcode nvarchar(50)=''
AS
BEGIN

Declare @IsFTP int=0;
Declare @IsCPSortation int=0;
Declare @Result varchar(50)='';

select code,DriverId,convert(varchar, EventDateTime, 112) as EventDate into #tempCode from Label l
join ScannerGateway..TrackingEvent t on t.LabelId =  l.id
join ScannerGateway..Driver d on d.Id = t.DriverId
where labelnumber = @Barcode 


Select @IsFTP=count(Code) from #tempCode where Code='9994'
Select @IsCPSortation=count(Code) from #tempCode where Code='9416'

if(@IsFTP>0)
BEGIN

select Code,[Name] from Branch with (nolock) where Id in (select BranchId from Driver where Id in (Select DriverId from #tempCode where Code='9994'))

Select top 1 EventDate from #tempCode where Code='9994'

Set @Result= 'FTP'
Select @Result as Result
END
ELSE IF(@IsCPSortation>0)
BEGIN
Set @Result= 'CPSortation'
Select @Result as Result
END
ELSE
BEGIN
Set @Result= 'No Result'
Select @Result as Result
END

drop table #tempCode

END
GO
GRANT EXECUTE
	ON [dbo].[SP_GetCWCImages]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[SP_GetCWCImages]
	TO [OffShoreUser]
GO
