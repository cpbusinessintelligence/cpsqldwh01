SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_ScannerGatewayDatabaseMaintenance_Updated]

@StartDate datetime = null --'2015-05-28'

As 
Begin
Begin Try
BEGIN TRAN 

--EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

		PRINT '-------- START DISABLING TABLE CONSTRAINT --------';

		ALTER TABLE tblAddress NOCHECK CONSTRAINT ALL
		ALTER TABLE tblConsignmentStaging NOCHECK CONSTRAINT ALL
		ALTER TABLE tblConsignment NOCHECK CONSTRAINT ALL
		ALTER TABLE tblItemLabel NOCHECK CONSTRAINT ALL
		ALTER TABLE tblSalesOrder NOCHECK CONSTRAINT ALL
		ALTER TABLE tblSalesOrderDetail NOCHECK CONSTRAINT ALL
		ALTER TABLE tblInvoice NOCHECK CONSTRAINT ALL
		ALTER TABLE tblRecharge NOCHECK CONSTRAINT ALL
		ALTER TABLE tblRefund NOCHECK CONSTRAINT ALL
		ALTER TABLE tblItemLabelImage NOCHECK CONSTRAINT ALL
		ALTER TABLE tblDHLBarCodeImage NOCHECK CONSTRAINT ALL

		ALTER TABLE tblRedirectedConsignment NOCHECK CONSTRAINT ALL
		ALTER TABLE tblRedirectedItemLabel NOCHECK CONSTRAINT ALL
		ALTER TABLE tblRedirectedRefund NOCHECK CONSTRAINT ALL

		PRINT '-------- END DISABLING TABLE CONSTRAINT --------';

				PRINT '-------- START COPYING DATA--------';
				------------Add Table Address Record In Archive-----------------------

				PRINT '-------- COPY ADDRESS --------';
				INSERT INTO tblAddress_Archive 
				select * from tblAddress with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(IsRegisterAddress,0) = 0 and isnull([UserID],'-1') = -1 and isnull(IsEDIUser,0) = 0

				------------Add Table ConsignmentStaging Record In Archive-----------------------

				PRINT '-------- COPY TCONSIGNMENT STAGING TABLE--------';
				INSERT INTO tblConsignmentStaging_Archive 
				select * from tblConsignmentStaging with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 


				------------Add Table ConsignmentStaging Record In Archive-----------------------

				PRINT '-------- COPY CONSIGNMENT TABLE --------';
				INSERT INTO tblConsignment_Archive 
				select * from tblConsignment with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				------------Add Table tblItemLabel Record In Archive-----------------------

				PRINT '-------- COPY ITEM LABEL TABLE --------';
				INSERT INTO tblItemLabel_Archive 
				select * from tblItemLabel with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				------------Add Table tblSalesOrder Record In Archive-----------------------
				PRINT '-------- COPY SALES ORDER TABLE --------';
				INSERT INTO tblSalesOrder_Archive 
				select * from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				--INSERT INTO tblSalesOrder_Archive 
				--select * from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(SalesOrderType,'') <> 'REDELIVERY'
				--and isnull(ConsignmentCode,'') not in (select ConsignmentCode from tblRedirectedConsignment)


				------------Add Table tblSalesOrderDetail Record In Archive-----------------------
				PRINT '-------- COPY SALES ORDER DETAILS TABLE --------';
				INSERT INTO tblSalesOrderDetails_Archive
				select * from tblSalesOrderDetail with(NoLock) where  convert(Date, CreatedDateTime, 103) <= @StartDate 

				--INSERT INTO tblSalesOrderDetails_Archive
				--select SD.* from tblSalesOrderDetail SD with(NoLock) where  convert(Date, SD.CreatedDateTime, 103) <= @StartDate 
				--and SD.SalesOrderID in (select SalesOrderID from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(SalesOrderType,'') <> 'REDELIVERY'
				--and isnull(ConsignmentCode,'') not in (select ConsignmentCode from tblRedirectedConsignment))

				------------Add Table tblInvoice Record In Archive-----------------------
				PRINT '-------- COPY INVOICE TABLE --------';
				INSERT INTO tblInvoice_Archive
				select * from tblInvoice with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 

				--INSERT INTO tblInvoice_Archive
				--select * from tblInvoice with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate 
				--and InvoiceNumber in (select InvoiceNo from tblSalesOrder with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(SalesOrderType,'') <> 'REDELIVERY'
				--and isnull(ConsignmentCode,'') not in (select ConsignmentCode from tblRedirectedConsignment))


				------------Add Table tblRecharge Record In Archive-----------------------
				PRINT '-------- COPY RECHARGE TABLE --------';
				INSERT INTO tblRecharge_Archive
				select * from tblRecharge with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				------------Add Table tblRefund Record In Archive-----------------------
				PRINT '-------- COPY REFUND TABLE --------';
				INSERT INTO tblRefund_Archive
				select * from tblRefund with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				------------Add Table tblItemLabelImage Record In Archive-----------------------
				PRINT '-------- COPY LABEL IMAGE TABLE --------';
				INSERT INTO tblItemLabelImage_Archive
				select * from tblItemLabelImage with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				------------Add Table tblDHLBarCodeImage Record In Archive-----------------------
				PRINT '-------- COPY DHL BARCODE TABLE --------';
				INSERT INTO tblDHLBarCodeImage_Archive
				select * from tblDHLBarCodeImage with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate



				------------Add Table tblRedirectedConsignment Record In Archive-----------------------
				PRINT '-------- COPY REDIRECTED CONSIGNMENT TABLE --------';
				INSERT INTO tblRedirectedConsignment_Archive
				select * from tblRedirectedConsignment with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate


				------------Add Table tblRedirectedItemLabel Record In Archive-----------------------

				INSERT INTO tblRedirectedItemLabel_Archive
				select * from tblRedirectedItemLabel with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				------------Add Table tblRedirectedRefund Record In Archive-----------------------
				PRINT '-------- COPY REDIRECTED LABEL TABLE --------';
				INSERT INTO tblRedirectedRefund_Archive
				select * from tblRedirectedRefund with(NoLock) where convert(Date, CreatedDateTime, 103) <= @StartDate

				----------------------------------------------------------------------------------------------------------------------------
				PRINT '-------- END COPYING DATA --------';

		----Delete Record from table---------------------

				PRINT '-------- DELETE DATA START --------';

				Delete from tblAddress where convert(Date, CreatedDateTime, 103) <= @StartDate and isnull(IsRegisterAddress,0) = 0 and isnull([UserID],'-1') = -1 and isnull(IsEDIUser,0) = 0
		
				Delete from tblConsignmentStaging where convert(Date, CreatedDateTime, 103) <= @StartDate
		
				Delete from tblConsignment where convert(Date, CreatedDateTime, 103) <= @StartDate 
		
				Delete from tblItemLabel where convert(Date,CreatedDateTime, 103) <= @StartDate 

				Delete from tblSalesOrder where convert(Date, CreatedDateTime, 103) <= @StartDate 
		
				Delete from tblSalesOrderDetail where convert(Date, CreatedDateTime, 103) <= @StartDate
		
				Delete from tblInvoice where convert(Date, CreatedDateTime, 103) <= @StartDate  

				Delete from tblRecharge  where convert(Date, CreatedDateTime, 103) <= @StartDate
		
				Delete from tblRefund where convert(Date, CreatedDateTime, 103) <= @StartDate 
		
				Delete from tblItemLabelImage where convert(Date, CreatedDateTime, 103) <= @StartDate 
		
				Delete from tblDHLBarCodeImage where convert(Date, CreatedDateTime, 103) <= @StartDate 

				--Redirection Tables
		
				Delete from tblRedirectedConsignment where convert(Date, CreatedDateTime, 103) <= @StartDate

				Delete from tblRedirectedItemLabel where convert(Date, CreatedDateTime, 103) <= @StartDate

				Delete from tblRedirectedRefund where convert(Date, CreatedDateTime, 103) <= @StartDate

				PRINT '-------- DELETE DATA END --------';

		--EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
		PRINT '-------- START ENABLING TABLE CONSTRAINT --------';

		ALTER TABLE tblAddress CHECK CONSTRAINT ALL
		ALTER TABLE tblConsignmentStaging CHECK CONSTRAINT ALL
		ALTER TABLE tblConsignment CHECK CONSTRAINT ALL
		ALTER TABLE tblItemLabel CHECK CONSTRAINT ALL
		ALTER TABLE tblSalesOrder CHECK CONSTRAINT ALL
		ALTER TABLE tblSalesOrderDetail CHECK CONSTRAINT ALL
		ALTER TABLE tblInvoice CHECK CONSTRAINT ALL
		ALTER TABLE tblRecharge CHECK CONSTRAINT ALL
		ALTER TABLE tblRefund CHECK CONSTRAINT ALL
		ALTER TABLE tblItemLabelImage CHECK CONSTRAINT ALL
		ALTER TABLE tblDHLBarCodeImage CHECK CONSTRAINT ALL

		ALTER TABLE tblRedirectedConsignment CHECK CONSTRAINT ALL
		ALTER TABLE tblRedirectedItemLabel CHECK CONSTRAINT ALL
		ALTER TABLE tblRedirectedRefund CHECK CONSTRAINT ALL

		PRINT '-------- END ENABLING TABLE CONSTRAINT --------';
		select 'OK' as Result

		COMMIT TRAN 
		--rollback tran
		END TRY
				BEGIN CATCH
				begin
					rollback tran
					INSERT INTO [dbo].[tblErrorLog] ([Error],[FunctionInfo],[ClientId])
					 VALUES
						   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
						   ,'SPCPPL_EzyFreightDatabaseMaintenance', 2)

						   select cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as Result
				end
				END CATCH

END 
GO
