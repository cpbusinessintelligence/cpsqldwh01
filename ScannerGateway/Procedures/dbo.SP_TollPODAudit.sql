SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE SP_TollPODAudit
(        @StartDate DATE,
		 @EndDate DATE
)
AS
BEGIN
    --'=====================================================================
    --' SP_TollPODAudit '2017-08-16','2017-08-19'
    --' ---------------------------
    --' Purpose: sp_RptRedirectedItemCount-----
    --' Developer: SINSHITH (Couriers Please Pty Ltd)
    --' Date: 11 Oct 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 11/10/2016    AB      1.00    Created the procedure                             --AB20161011
	--   09/06/2017   SS      1.01    Added Account Number parameter
    --'=====================================================================
  
  SELECT   ConsignmentNumber,
	  TrackingNumber LabelNumber,
	  StatusDateTime,
	  Scanevent,
	  Toll_Consignment AS [Toll Consignment],
	  Toll_Label AS [Toll Label]  INTO #Temp1
  FROM TollStatusstaging with (NOLOCK)
  WHERE Scanevent ='Delivered' AND convert(date,StatusDateTime)
  BETWEEN @StartDate AND @EndDate
  
  SELECT 
	  ConsignmentNumber,
	  LabelNumber,
	  StatusDateTime,
	  Scanevent,
	  [Toll Consignment],
	  [Toll Label] 
  FROM #Temp1 a
  LEFT JOIN [dbo].[AgentPODStatus] b
  ON(a.ConsignmentNumber = Substring(b.Filename,1,Charindex('_',b.FileName)-1) )
  WHERE Substring(b.Filename,1,Charindex('_',b.FileName)-1) is null

  END
GO
GRANT CONTROL
	ON [dbo].[SP_TollPODAudit]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[SP_TollPODAudit]
	TO [ReportUser]
GO
