SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Sp_DownloadImagesFromLink @AccountNumber varchar(20),@StartDate Date,@EndDate Date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select * 
	into #temp
	from cppledi.[dbo].[consignment] c join cppledi.dbo.cdcoupon cd on c.cd_id=cd.cc_consignment
	where cd_account=@AccountNumber
	and cd_date between @StartDate and @EndDate

	select cc_coupon as LabelNumber,CosmosSignatureId as Id,CosmosBranchId as Branch from #temp t left join [ScannerGateway].dbo.Label l on l.LabelNumber = t.cc_coupon	
	   left join  [ScannerGateway].dbo.TrackingEvent te on te.LabelId= l.Id
	where CosmosSignatureId<>0

END
GO
