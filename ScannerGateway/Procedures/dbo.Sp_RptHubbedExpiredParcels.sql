SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--updated by HB as per the request on 11/11/2020
-- =============================================
CREATE PROCEDURE [dbo].[Sp_RptHubbedExpiredParcels]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
	  ,DC.[State] as [State]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]
      ,HS.[CreatedDatetime]
      ,[AccessPin]
      ,[Jobnumber]
	  ,Dc.Latitude [Latitude]
	  ,dc.Longtitude [Longtitude]
	  ,Category
	  ,[Operation Hours]
	  ,concat(DC.Address1,'',DC.Address2,'',Dc.Address3) as [Address]
	  ,Suburb
	  ,PostCode
  into #Temp1
  FROM [ScannerGateway].[dbo].[HubbedStaging] HS
  left join [CouponCalculator].[dbo].[DeliveryChoices] DC
  ON rtrim(HS.[AgentId])= rtrim(DC.[DeliveryChoiceID])
  where  HS.CreatedDatetime > =  Getdate() -30
  and ScanType  in ('Accepted by Newsagent')




  SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
	  ,null as [State]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]
      ,HS.[CreatedDatetime]
      ,[AccessPin]
      ,[Jobnumber]
	  ,null as  [Latitude]
	  ,null as [Longtitude]

  into #Temp2
  FROM [ScannerGateway].[dbo].[HubbedStaging] HS
  -- join [CouponCalculator].[dbo].[DeliveryChoices] DC   ON HS.[AgentId]= DC.[DeliveryChoiceID]
  where  HS.CreatedDatetime > =  Getdate() -90
  and ScanType in ( 'DEL')

    SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
	  ,null as [State]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]
      ,HS.[CreatedDatetime]
      ,[AccessPin]
      ,[Jobnumber]
	   ,null as  [Latitude]
	  ,null as [Longtitude]
  into #Temp3
   FROM [ScannerGateway].[dbo].[HubbedStaging] HS
   -- join [CouponCalculator].[dbo].[DeliveryChoices] DC   ON HS.[AgentId]= DC.[DeliveryChoiceID]
  where  HS.CreatedDatetime > =  Getdate() -90
  and ScanType = 'Returned to Courier'


  
  Select t.* into #Temp4 from #Temp1 T left Join #Temp2 T1 on T.TrackingNumber=  T1.TrackingNumber where T1.TrackingNumber is null and T.ScanTime < getdate()-7

  Select T.*,DATEDIFF(day,T.[ScanTime],getdate()) as Age into #Temp5 from #Temp4 T left Join #Temp3 T1 on T.TrackingNumber=  T1.TrackingNumber where T1.TrackingNumber is null  and len(T.JobNumber) <2
  order by T.TrackingNumber,T.ScanTime,DATEDIFF(day,T.[ScanTime],getdate()) desc

   Select distinct T5.*   from #Temp5 T5 JOIN [ScannerGateway].[dbo].[HubbedStaging] HS ON T5.[TrackingNumber]=HS.TrackingNumber Where LTRIM(RTRIM(HS.ScanType)) not like 'DEL' or LTRIM(RTRIM(HS.ScanType)) not like 'Returned to Courier'


  --Select * from #Temp5 T5 JOIN [dbo].[TrackingEvent] TE ON T5.[TrackingNumber]=TE.[SourceReference] Where TE.[EventTypeId]<>'47CFA05F-3897-4F1F-BDF4-00C6A69152E3'

END
GO
GRANT EXECUTE
	ON [dbo].[Sp_RptHubbedExpiredParcels]
	TO [ReportUser]
GO
