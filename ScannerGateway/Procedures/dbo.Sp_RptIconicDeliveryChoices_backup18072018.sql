SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






  --'=====================================================================
    --' [Sp_RptIconicDeliveryChoices] '113058192','2017-08-01','2017-08-31'
    --' ---------------------------
    --' Purpose: sp_RptCoupONSalesData-----
    --' Developer: Sinshith
    --' Date: 06/07/2017
    --' Copyright:  Couriers PleASe Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     ReASON                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --'=====================================================================
Create PROCEDURE [dbo].[Sp_RptIconicDeliveryChoices_backup18072018] (@Account varchar(20), @StartDate Date, @EndDate Date) AS 
BEGIN
	
	SET NOCOUNT ON;


	-- drop table #temp1


Select        cd_Connote ,
              cd_id,
              cc_coupon,
              cd_Account,
              cd_items, 
              cd_date ,
              convert(varchar(30),'') as DeliveryType,
              convert(varchar(30),'') as DeliverySubType,
              cd_pickup_suburb ,
              cd_pickup_postcode, 
              cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
			  convert(Datetime,Null)  as RedeliveryRequestedDateTime,
			    convert(Datetime,Null)  as RedirectionRequestedDateTime,
              convert(Datetime,Null)  as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard,
			  convert(Datetime,Null)  as [Re-AttemptedDeliveryDate],
              convert(varchar(20),'') as [Re-AttemptedDeliveryCard]
into #Temp1
from cpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
Where  cd_account = @Account
and cd_date >= @StartDate and cd_date <= @EndDate
and cd_delivery_addr0 <> 'Iconic C/O Seko' 

Update #Temp1 SET AttemptedDeliveryCard = replace(additionaltext1,'Link Coupon ',''), AttemptedDeliveryDate = E.EventDateTime  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference Where
eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA' or additionaltext1 like 'Link Coupon 191%') 


Update #Temp1 Set DeliveryType =  'DEPOT Redelivery' Where (AttemptedDeliveryCard like '191%' OR AttemptedDeliveryCard like '%DPCNA') 
Update #Temp1 Set DeliveryType =  'POPSHOP Redelivery' Where (AttemptedDeliveryCard like 'NHCL%' OR AttemptedDeliveryCard like '%RTCNA') 
Update #Temp1 Set DeliveryType =  'POPSTATION Redelivery' Where  AttemptedDeliveryCard like '%SLCNA' 
Update #Temp1 Set DeliverySubType = Case WHEN ISnull(RedeliveryType,'') = 'Deliver with signature' 
THEN 'Signature' ELSE 'Authority To Leave' END ,RedeliveryRequestedDateTime = R.CreatedDateTime
     From #Temp1 T join EzYFreight.dbo.tblRedelivery R on T.cc_coupon= R.LableNumber Where IsProcessed =1


Update #Temp1 Set DeliverySubType = 'No Actions by Cust'  Where DeliveryType =  'DEPOT Redelivery' and DeliverySubType =''

Update #Temp1 SET DeliveryType = 'Redirection', DeliverySubType  = R.SelectedDeliveryOption ,RedirectionRequestedDateTime = CASE WHEN R.SelectedDeliveryOption = 'Authority To Leave' THEN R.CreatedDateTime ELSE Null END
From #Temp1 T join EzYFreight.dbo.tblRedirectedConsignment R on T.cd_connote= R.ConsignmentCode Where IsProcessed =1
Update #Temp1 SET DeliveryType = 'Normal Delivery' where DeliveryType = ''


----

Update #Temp1 SET [Re-AttemptedDeliveryCard] = replace(additionaltext1,'Link Coupon ',''), 
             [Re-AttemptedDeliveryDate] = E.EventDateTime 
			  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference Where
eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon %DPCNA' or additionaltext1 like 'Link Coupon 191%') 
 and  DeliverySubType = 'Authority To Leave'
 and E.EventDateTime > RedirectionRequestedDateTime


 Update #Temp1 SET [Re-AttemptedDeliveryCard] = replace(additionaltext1,'Link Coupon ',''), 
             [Re-AttemptedDeliveryDate] = E.EventDateTime 
			  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference Where
eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon %DPCNA' or additionaltext1 like 'Link Coupon 191%') 
 and  DeliverySubType = 'Authority To Leave'
 and E.EventDateTime > RedeliveryRequestedDateTime
 and [Re-AttemptedDeliveryDate] is null



select deliveryType,count(*) Count into #Temp2
From #Temp1
group by deliveryType



--1st TIME DELIVERY				


Declare @TotalCount int
Declare @NumberOFParcels1stExRe int
Declare @PercTotalExRe int
Declare @NumberOFParcels1stInRe int
Declare @PercTotalInRe int

Select @TotalCount = count(*)  from #Temp1

Select @NumberOFParcels1stExRe = Sum (count)  from #Temp2 where deliveryType in ('Redirection',
'Normal Delivery')

--Select @PercTotal = convert(Float,@NumberOFParcels1st/@NumberOFParcels1st)

Select @PercTotalExRe = Round(@NumberOFParcels1stExRe*100.0/@TotalCount,0)


Select @NumberOFParcels1stInRe = Count from #Temp2 where deliveryType = 'Normal Delivery'

Select @PercTotalInRe = Convert(int,Round(@NumberOFParcels1stInRe*100/@TotalCount,1),0)



--ON DEMAND DELIVERY				

Declare @NumberOfNotActioned Int

Select @NumberOfNotActioned = Count from #Temp2 where deliveryType = 'Redirection'

Declare @NotActionedTotVolume int
Select @NotActionedTotVolume = Convert(int,Round(@NumberOfNotActioned*100/@TotalCount,1),0)


Declare @ATL int

select @ATL = count(*) from #temp1 where DeliveryType = 'Redirection' and DeliverySubType = 'Authority To Leave'


Declare @ATLPerc int 
Select @ATLPerc = Convert(int,Round(@ATL*100/@TotalCount,1),0)


Declare @TotalNotiAct int
Select @TotalNotiAct = Convert(int,Round(@ATL*100/@NumberOfNotActioned,1),0)


Declare @PopPoint int
select @PopPoint = count(*) from #Temp1 where DeliveryType = 'Redirection' and DeliverySubType = 'POPPoint'


Declare @POPPointTotal int
Select @POPPointTotal = Convert(int,Round(@PopPoint*100/@TotalCount,1),0)



Declare @POPPointTotalact int
Select @POPPointTotalact = Convert(int,Round(@PopPoint*100/@NumberOfNotActioned,1),0)



Declare @AltAddr int
Select @AltAddr = count(*) from #Temp1 where DeliveryType = 'Redirection' and DeliverySubType = 'Alternate Address'


Declare @AltAddrTotal int
Select @AltAddrTotal = Convert(int,Round(@AltAddr*100/@TotalCount,1),0)


Declare @AltAddrTotalact int
Select @AltAddrTotalact = Convert(int,Round(@AltAddr*100/@NumberOfNotActioned,1),0)


--Neighbour Delivery

Declare @NeighDeli int
Select @NeighDeli = count(*) from #Temp1 where DeliveryType = 'Redirection' and DeliverySubType = 'Neighbour Delivery'


Declare @NeighDeliTotal int
Select @NeighDeliTotal = Convert(int,Round(@NeighDeli*100/@TotalCount,1),0)


Declare @NeighDeliact int
Select @NeighDeliact = Convert(int,Round(@NeighDeli*100/@NumberOfNotActioned,1),0)


------Carded Delivery

Declare @CardedDelivery int
Select @CardedDelivery = count(*) from #Temp1 where DeliveryType in ('POPSTATION Redelivery',
'DEPOT Redelivery' ,'POPSHOP Redelivery')

Declare @CardedDeliveryTotVolume int
Select @CardedDeliveryTotVolume = Convert(int,Round(@CardedDelivery*100/@TotalCount,1),0)

--POPSTATION
Declare @POPSTATIONRedelivery int
Select @POPSTATIONRedelivery =  count(*) from #Temp1 where DeliveryType in ('POPSTATION Redelivery')


Declare @POPSTATIONRedeliveryTotal int
Select @POPSTATIONRedeliveryTotal = Convert(int,Round(@POPSTATIONRedelivery*100/@TotalCount,1),0)


Declare @POPSTATIONRedeliveryTotalcardleft int
Select @POPSTATIONRedeliveryTotalcardleft = Convert(int,Round(@POPSTATIONRedelivery*100/@CardedDelivery,1),0)


--POPShop

Declare @POPShopRedelivery int
Select @POPShopRedelivery =  count(*) from #Temp1 where DeliveryType in ('POPShop Redelivery')


Declare @POPSHopRedeliveryTotal int
Select @POPSHopRedeliveryTotal = Convert(int,Round(@POPShopRedelivery*100/@TotalCount,1),0)


Declare @POPShopRedeliveryTotalcardleft int
Select @POPShopRedeliveryTotalcardleft = Convert(int,Round(@POPShopRedelivery*100/@CardedDelivery,1),0)


--Depot Card
Declare @DepotCardRedelivery int
Select @DepotCardRedelivery =  count(*) from #Temp1 where DeliveryType in ('DEPOT Redelivery')


Declare @DepotCardRedeliveryTotal int
Select @DepotCardRedeliveryTotal = Convert(int,Round(@DepotCardRedelivery*100/@TotalCount,1),0)


Declare @DepotCardRedeliveryTotalcardleft int
Select @DepotCardRedeliveryTotalcardleft = Convert(int,Round(@DepotCardRedelivery*100/@CardedDelivery,1),0)


----------------Re Delivery-----------------------------------

Declare @DepotCardRedeliveryATL int
Select @DepotCardRedeliveryATL =  count(*) from #Temp1 where DeliveryType in ('DEPOT Redelivery') and DeliverySubType = 'Authority To Leave'


Declare @DepotCardRedeliveryATLTotal int
Select @DepotCardRedeliveryATLTotal = Convert(int,Round(@DepotCardRedeliveryATL*100/@TotalCount,1),0)


--Signature

Declare @SignatureReq int
select @SignatureReq = Count(*) from #Temp1 where DeliveryType =
'DEPOT Redelivery' and DeliverySubType = 'Signature'


Declare @SignatureReqTotal int
Select @SignatureReqTotal = Convert(int,Round(@SignatureReq*100/@TotalCount,1),0)


--NotActioned

Declare @NoActions int
select @NoActions = Count(*) from #Temp1 where DeliveryType =
'DEPOT Redelivery' and DeliverySubType = 'No Actions by Cust'


Declare @NoActionsTotal int
Select @NoActionsTotal = Convert(int,Round(@NoActions*100/@TotalCount,1),0)


-----------------ReDElivery
Declare @RedeliveryNew int
select @RedeliveryNew = Count(*) from #Temp1 where DeliverySubType = 'Authority To Leave' 


Declare @CardedRedelivery int
Select @CardedRedelivery = count(*) from #temp1 where [Re-AttemptedDeliveryCard] <> ''

--Drop table #temp7
Select Col=case when contextid=1 then 'Emails before Redirection' when Contextid=2 then 'SMS before Redirection' else '' end,count(*) as [Count] into 
#temp7

from redirection..SendMAil Sm 
inner join #Temp1 b
on(sm.parameter1 = b.cd_Connote)
where isnull(iserror,0)=0 and transmitflag<>9 and contextid in (1,2) and convert(date,adddatetime)>='2016-10-06' and convert(date,adddatetime) between @StartDate and @EndDate

group by case when contextid=1 then 'Emails before Redirection' when Contextid=2 then 'SMS before Redirection' else '' end 

Declare @Email int
Declare @Sms int

Select @Email = Count from #temp7 where Col = 'Emails before Redirection'
Select @SMs = Count from #temp7 where Col = 'SMS before Redirection'


-------& "%"----------------Final Select Query------------------------------------


Select
convert(varchar(11),datepart(day,@StartDate)) + ' - '+ convert(varchar(11),datename(month,@StartDate)) + ' to ' + convert(varchar(11),datepart(day,@EndDate)) + ' - '+ convert(varchar(11),datename(month,@EndDate)) DateColumn,
@TotalCount as TotalCount,
@NumberOFParcels1stExRe as Numberofparcelsdelivered,
@PercTotalExRe as PercoutofTotal1,
@NumberOFParcels1stInRe as NormalDelivery,
@PercTotalInRe as PercOutofTotal,
@NumberOfNotActioned as NoofNotActioned,
@NotActionedTotVolume as NoactionedVolume,
@ATL as NoofRequest,
@ATLPerc as ATLPeroutof,
@TotalNotiAct as ATLNotActioned,
@PopPoint as NoofPOPpointrequest,
@POPPointTotal as PopPointTotal,
@POPPointTotalact as PopPointTotalact,
@AltAddr as ATLAddr,
@AltAddrTotal as AltAddrTotal,
@AltAddrTotalact as AltAddrTotalact,
@NeighDeli as NeighDeli,
@NeighDeliTotal as NeighDeliTotal,
@NeighDeliact as NeighDeliact,
@CardedDelivery as CardedDelivery,
@CardedDeliveryTotVolume as CardedDeliveryTotVolume,
@POPSTATIONRedelivery as POPSTATIONRedelivery,
@POPSTATIONRedeliveryTotal as POPSTATIONRedeliveryTotal,
@POPSTATIONRedeliveryTotalcardleft as POPSTATIONRedeliveryTotalcardleft,
@POPShopRedelivery as POPShopRedelivery,
@POPSHopRedeliveryTotal as POPSHopRedeliveryTotal,
@POPShopRedeliveryTotalcardleft as POPShopRedeliveryTotalcardleft,
@DepotCardRedelivery as DepotCardRedelivery,
@DepotCardRedeliveryTotal as DepotCardRedeliveryTotal,
@DepotCardRedeliveryTotalcardleft as DepotCardRedeliveryTotalcardleft,
@DepotCardRedeliveryATL as DepotCardRedeliveryATL,
@DepotCardRedeliveryATLTotal as DepotCardRedeliveryATLTotal,
@SignatureReq as SignatureReq,
@SignatureReqTotal as SignatureReqTotal,
@NoActions as NoActions,
@NoActionsTotal as NoActionsTotal,
@RedeliveryNew as Redeliverynew,
@CardedRedelivery as CardedRedelivery,
@Email as Email,
@SMs as SMs


end












--drop table #Temp1




GO
