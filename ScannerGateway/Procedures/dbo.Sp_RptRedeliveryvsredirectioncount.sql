SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





  --'=====================================================================
    --' [Sp_RptRedeliveryvsredirectioncount] '112893730','2017-06-01','2017-06-30'
    --' ---------------------------
    --' Purpose: sp_RptCoupONSalesData-----
    --' Developer: Sinshith
    --' Date: 06/07/2017
    --' Copyright:  Couriers PleASe Pty Ltd
    --' Change Log:
    --' Date          Who     Ver     ReASON                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --'=====================================================================
CREATE PROCEDURE [dbo].[Sp_RptRedeliveryvsredirectioncount] (@Account varchar(20), @StartDate Date, @EndDate Date) AS 
BEGIN
	
	SET NOCOUNT ON;

Select        cd_Connote ,
              cd_id,
              cc_coupon,
              cd_Account,
              cd_items, 
              cd_date ,
              convert(varchar(30),'') as DeliveryType,
              convert(varchar(30),'') as DeliverySubType,
              cd_pickup_suburb ,
              cd_pickup_postcode, 
              cd_delivery_addr0,
              cd_delivery_addr1,
              cd_delivery_addr2,
              cd_delivery_suburb,
              cd_delivery_postcode , 
			  convert(Datetime,Null)  as RedeliveryRequestedDateTime,
			    convert(Datetime,Null)  as RedirectionRequestedDateTime,
              convert(Datetime,Null)  as AttemptedDeliveryDate,
              convert(varchar(20),'') as AttemptedDeliveryCard,
			  convert(Datetime,Null)  as [Re-AttemptedDeliveryDate],
              convert(varchar(20),'') as [Re-AttemptedDeliveryCard]
into #Temp1
from cpplEDI.dbo.consignment C (Nolock) join cppledi.dbo.cdcoupon (Nolock) D on C.cd_id =D.cc_consignment
Where  cd_account = '113058192'
and cd_date >= @StartDate and cd_date <= @EndDate
and cd_delivery_addr0 <> 'Iconic C/O Seko' 

Update #Temp1 SET AttemptedDeliveryCard = replace(additionaltext1,'Link Coupon ',''), AttemptedDeliveryDate = E.EventDateTime  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference Where
eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA' or additionaltext1 like 'Link Coupon 191%') 


Update #Temp1 Set DeliveryType =  'DEPOT Redelivery' Where (AttemptedDeliveryCard like '191%' OR AttemptedDeliveryCard like '%DPCNA') 
Update #Temp1 Set DeliveryType =  'POPSHOP Redelivery' Where (AttemptedDeliveryCard like 'NHCL%' OR AttemptedDeliveryCard like '%RTCNA') 
Update #Temp1 Set DeliveryType =  'POPSTATION Redelivery' Where  AttemptedDeliveryCard like '%SLCNA' 
Update #Temp1 Set DeliverySubType = Case WHEN ISnull(RedeliveryType,'') = 'Deliver with signature' 
THEN 'Signature' ELSE 'Authority To Leave' END ,RedeliveryRequestedDateTime = R.CreatedDateTime
     From #Temp1 T join EzYFreight.dbo.tblRedelivery R on T.cc_coupon= R.LableNumber Where IsProcessed =1


Update #Temp1 Set DeliverySubType = 'No Actions by Cust'  Where DeliveryType =  'DEPOT Redelivery' and DeliverySubType =''

Update #Temp1 SET DeliveryType = 'Redirection', DeliverySubType  = R.SelectedDeliveryOption ,RedirectionRequestedDateTime = CASE WHEN R.SelectedDeliveryOption = 'Authority To Leave' THEN R.CreatedDateTime ELSE Null END
From #Temp1 T join EzYFreight.dbo.tblRedirectedConsignment R on T.cd_connote= R.ConsignmentCode Where IsProcessed =1
Update #Temp1 SET DeliveryType = 'Normal Delivery' where DeliveryType = ''


----

--Update #Temp1 SET [Re-AttemptedDeliveryCard] = replace(additionaltext1,'Link Coupon ',''), 
--             [Re-AttemptedDeliveryDate] = E.EventDateTime 
--			  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E with(nolock)on T.cc_coupon = E.SourceReference Where
--eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
-- and (additionaltext1 like 'Link Coupon %DPCNA' or additionaltext1 like 'Link Coupon 191%') 
-- and  DeliverySubType = 'Authority To Leave'
-- and E.EventDateTime > RedirectionRequestedDateTime


-- Update #Temp1 SET [Re-AttemptedDeliveryCard] = replace(additionaltext1,'Link Coupon ',''), 
--             [Re-AttemptedDeliveryDate] = E.EventDateTime 
--			  From #Temp1 T Join ScannerGateway.dbo.TrackingEvent E on T.cc_coupon = E.SourceReference Where
--eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
-- and (additionaltext1 like 'Link Coupon %DPCNA' or additionaltext1 like 'Link Coupon 191%') 
-- and  DeliverySubType = 'Authority To Leave'
-- and E.EventDateTime > RedeliveryRequestedDateTime
-- and [Re-AttemptedDeliveryDate] is null



Select  *      
--cd_Connote ,
--              cd_id,
--              cc_coupon,
--              cd_Account,
--              cd_items, 
--              cd_date ,
--               DeliveryType,
--               DeliverySubType,
--              cd_pickup_suburb ,
--              cd_pickup_postcode, 
--              cd_delivery_addr0,
--              cd_delivery_addr1,
--              cd_delivery_addr2,
--              cd_delivery_suburb,
--              cd_delivery_postcode , 
		
--               AttemptedDeliveryDate,
--             AttemptedDeliveryCard,
--		     [Re-AttemptedDeliveryDate],
--              [Re-AttemptedDeliveryCard]
from #Temp1

END

--drop table #Temp1



GO
GRANT EXECUTE
	ON [dbo].[Sp_RptRedeliveryvsredirectioncount]
	TO [ReportUser]
GO
