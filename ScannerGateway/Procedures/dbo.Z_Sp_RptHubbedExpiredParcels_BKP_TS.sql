SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Z_Sp_RptHubbedExpiredParcels_BKP_TS]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]

      ,[CreatedDatetime]

      ,[AccessPin]
      ,[Jobnumber]
  into #Temp1
  FROM [ScannerGateway].[dbo].[HubbedStaging]

  where  CreatedDatetime > =  Getdate() -30
  and ScanType = 'Accepted by Newsagent'

  SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]

      ,[CreatedDatetime]

      ,[AccessPin]
      ,[Jobnumber]
  into #Temp2
  FROM [ScannerGateway].[dbo].[HubbedStaging]

  where  CreatedDatetime > =  Getdate() -90
  and ScanType = 'DEL'

    SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]

      ,[CreatedDatetime]

      ,[AccessPin]
      ,[Jobnumber]
  into #Temp3
  FROM [ScannerGateway].[dbo].[HubbedStaging]

  where  CreatedDatetime > =  Getdate() -90
  and ScanType = 'Returned to Courier'
  
  Select t.* into #Temp4 from #Temp1 T left Join #Temp2 T1 on T.TrackingNumber=  T1.TrackingNumber where T1.TrackingNumber is null and T.ScanTime < getdate()-7

  Select T.* from #Temp4 T left Join #Temp3 T1 on T.TrackingNumber=  T1.TrackingNumber where T1.TrackingNumber is null  and len(T.JobNumber) <2
  order by 2 desc


END
GO
