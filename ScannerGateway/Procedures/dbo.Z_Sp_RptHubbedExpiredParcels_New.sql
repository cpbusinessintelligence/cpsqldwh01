SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Z_Sp_RptHubbedExpiredParcels_New]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
	  ,DC.[State] as [State]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]
      ,HS.[CreatedDatetime]
      ,[AccessPin]
      ,[Jobnumber]
  into #Temp1
  FROM [ScannerGateway].[dbo].[HubbedStaging] HS
  join [CouponCalculator].[dbo].[DeliveryChoices] DC
  ON HS.[AgentId]= DC.[DeliveryChoiceID]
  where  HS.CreatedDatetime > =  Getdate() -30
  and ScanType = 'Accepted by Newsagent' and ScanType not like 'DEL%'


SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
	  ,DC.[State] as [State]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]
      ,HS.[CreatedDatetime]
      ,[AccessPin]
      ,[Jobnumber]
  into #Temp3
   FROM [ScannerGateway].[dbo].[HubbedStaging] HS
    join [CouponCalculator].[dbo].[DeliveryChoices] DC
  ON HS.[AgentId]= DC.[DeliveryChoiceID]
  where  HS.CreatedDatetime > =  Getdate() -90
  and ScanType = 'Returned to Courier' and ScanType not like 'DEL%'
  
  --Select t.* into #Temp4 from #Temp1 T left Join #Temp2 T1 on T.TrackingNumber=  T1.TrackingNumber where T1.TrackingNumber is null and T.ScanTime < getdate()-7

  Select distinct T.*,DATEDIFF(day,T.[ScanTime],getdate()) as Age from #Temp1 T left Join #Temp3 T1 on T.TrackingNumber=  T1.TrackingNumber 
  join [dbo].[TrackingEvent] TE ON T.TrackingNumber=TE.SourceReference
  where  T1.TrackingNumber is null  and len(T.JobNumber) <2 and T.ScanTime < getdate()-7 and TE.EventTypeId <>'47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
  order by T.TrackingNumber,T.ScanTime,DATEDIFF(day,T.[ScanTime],getdate()) desc


END
GO
