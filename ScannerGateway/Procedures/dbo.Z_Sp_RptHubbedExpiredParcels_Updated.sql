SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Z_Sp_RptHubbedExpiredParcels_Updated]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/********* Get the Tracking Numbers accepted by NewsAgents********************************/

SELECT distinct [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId]
      ,[AgentName]
	  ,DC.[State] as [State]
      ,[SignedName]
      ,[SignaturePoints]
      ,[HubbedCard]
      ,HS.[CreatedDatetime]
      ,[AccessPin]
      ,[Jobnumber]
  into #Temp1
  FROM [ScannerGateway].[dbo].[HubbedStaging] HS
  join [CouponCalculator].[dbo].[DeliveryChoices] DC
  ON HS.[AgentId]= DC.[DeliveryChoiceID]
  where  HS.ScanTime < getdate()-7 and HS.CreatedDatetime > =  Getdate() -30
  and ScanType = 'Accepted by Newsagent'

/****** Remove all 'DEL' events from HubbedStaging table***********************************/


DELETE HS FROM [ScannerGateway].[dbo].[HubbedStaging]
INNER JOIN  #Temp1 T ON HS.TrackingNumber=T.TrackingNumber Where HS.ScanType='DEL'

/*******************Remove the Delivered Tracking from the table **************************/

Select T.*,TE.EventDateTime,TE.EventTypeId into #TempDelivered
 from #Temp1 T JOIN [dbo].[TrackingEvent] TE ON T.[TrackingNumber]=TE.[SourceReference] where TE.[EventTypeId]='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and TE.EventDateTime >= T.ScanTime


select *,DATEDIFF(day,[ScanTime],getdate()) as Age  from #Temp1 (NOLOCK) where [TrackingNumber] NOT IN (Select  [TrackingNumber] from #TempDelivered) 
ORDER BY [TrackingNumber],[ScanTime],DATEDIFF(day,[ScanTime],getdate()) DESC


END
GO
