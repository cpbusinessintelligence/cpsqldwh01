SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[Z_sp_HubbedCardupdate_BUP20160812] as
begin



--Select * from HubbedStaging where trackingnumber in ('CPA25VS0000046','CPA25VS0000047','CPA25VS0000049','CPA25VS0000050','CPA25VS0000051','CPA25VS0000052','CPA25VS0000053','CPA25VS0000054')

--createddatetime>='2016-08-05 08:30:30.533' and isprocessed=1

 Update HubbedStaging set hubbedcard=replace(additionaltext1,'Link Coupon ','')  from trackingevent where sourcereference=trackingnumber and eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and ScanType='Accepted by NewsAgent'
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA') and isprocessed=0


 -------------Redelivery--------
 --select * from HubbedStaging order by createddatetime desc
 
Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
,[Parameter5]
,[Parameter6]
,[parameter7]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])


Select  distinct  8,
       'noreply@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   --'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   'CPReporting@couriersplease.com.au,kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	 --  Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   0,
	   ls.TrackingNumber,
     HubbedCard,
	   AgentName,
	   [Address1]+','+Suburb+','+Postcode+','+[State],
    [cd_delivery_addr0]+'<br/>'+isnull(cd_delivery_contact,'')+'<br/>'+[cd_delivery_addr1]+'<br/>'+[cd_delivery_addr2]+'<br/>'+[cd_delivery_addr3]+'<br/>'+[cd_delivery_suburb]+','+case when b1.b_name='Sydney' then 'NSW'
	     when b1.b_name='Brisbane' then 'QLD'
		 when b1.b_name='Gold Coast' then 'QLD'
		 when b1.b_name='Melbourne' then 'VIC'
		 when b1.b_name='Perth' then 'WA'
		 when b1.b_name='Canberra' then 'NSW'
		 when b1.b_name='Adelaide' then 'SA'
		 else 'Unknown' end +','+convert(varchar(50),[cd_delivery_postcode]),
	   [Operation Hours],
	 --  'kirsty.tuffley@couriersplease.com.au',
    Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   'AK',
	   getdate(),
	   getdate()

 from [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) 
                                                               join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
															   join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															   join cpplEDI.dbo.branchs b1 on b1.b_id=cd_deliver_branch
															   join [EzyTrak Integration].[dbo].deliverychoices(NOLOCK) on Agentname=Deliverychoicedescription
where
ls.isProcessed=0 and [ScanType] ='Accepted by Newsagent'  and Hubbedcard is not null and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))=1
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=8)
and ls.ScanTime>='2016-08-12 06:00:00.000'
--and ls.trackingnumber  in ('CPA25VS0000066','CPA25VS0000064','CPA25VS0000062')


--Insert into [Redirection].[dbo].[SendMail]([ContextID]
--      ,[FromAddr]
--      ,[ToAddr]
--	  ,[BCC]
--	  ,[TransmitFlag]
--	  ,[Parameter1]
--      ,[Parameter2]
--      ,[Parameter3]
--      ,[Parameter4]
--	  ,[AddWho]
--	  ,[AddDateTime]
--	  ,[ModifiedDateTime])



--Select  distinct  9,
--       'noreply@couriersplease.com.au',
--	   '0466419484@preview.pcsms.com.au',
--	   'CPReporting@couriersplease.com.au',
--	  -- Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) +'@preview.pcsms.com.au'
--	   0,
--	   ls.TrackingNumber,
--     HubbedCard,
--	   AgentID,
--	    '0466419484',
--    -- Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone),
--	   'AK',
--	   getdate(),
--	   getdate()

--from [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) 
--                                                              join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
--															  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															  
--															  join [EzyTrak Integration].[dbo].deliverychoices(NOLOCK) on Agentname=Deliverychoicedescription
--where 
--ls.isProcessed=0 and [ScanType] ='Accepted by Newsagent'  and Hubbedcard is not null and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
--and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))<>1 and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1
--and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=9)




-----------------1st time delivery or Redirection----------------
 
Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
,[Parameter5]
,[Parameter6]
,[parameter7]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])



Select  distinct   12,
       'noreply@couriersplease.com.au',
	 --  'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   0,
	   ls.TrackingNumber,
     HubbedCard,
	   AgentName,
	   [Address1]+','+Suburb+','+Postcode+','+State+','+Country,
    [cd_delivery_addr0]+'<br/>'+case when cd_delivery_contact='' then [cd_delivery_addr0] else  cd_delivery_contact end+'<br/>'+[cd_delivery_addr1]+'<br/>'+[cd_delivery_addr2]+'<br/>'+[cd_delivery_addr3]+'<br/>'+[cd_delivery_suburb]+','+case when b1.b_name='Sydney' then 'NSW'
	     when b1.b_name='Brisbane' then 'QLD'
		 when b1.b_name='Gold Coast' then 'QLD'
		 when b1.b_name='Melbourne' then 'VIC'
		 when b1.b_name='Perth' then 'WA'
		 when b1.b_name='Canberra' then 'NSW'
		 when b1.b_name='Adelaide' then 'SA'
		 else 'Unknown' end +','+convert(varchar(50),[cd_delivery_postcode]),
	   [Operation Hours],
	 --  'kirsty.tuffley@couriersplease.com.au',
    Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   'AK',
	   getdate(),
	   getdate()

 from [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) 
                                                               join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
															   join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															   join cpplEDI.dbo.branchs b1 on b1.b_id=cd_deliver_branch
															   join [EzyTrak Integration].[dbo].deliverychoices(NOLOCK) on Agentname=Deliverychoicedescription
where
ls.isProcessed=0 and [ScanType] ='Accepted by Newsagent'   and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))=1
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=12)
and ls.ScanTime>='2016-08-12 06:00:00.000'
--and ls.trackingnumber in ('CPA25VS0000066','CPA25VS0000064','CPA25VS0000062')


--Insert into [Redirection].[dbo].[SendMail]([ContextID]
--      ,[FromAddr]
--      ,[ToAddr]
--	  ,[TransmitFlag]
--	  ,[Parameter1]
--      ,[Parameter2]
--      ,[Parameter3]
--	  ,[AddWho]
--	  ,[AddDateTime]
--	  ,[ModifiedDateTime])




--Select  distinct top 1 13,
--       'noreply@couriersplease.com.au',
--	   '0466419484@preview.pcsms.com.au',
--	   --Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) +'@preview.pcsms.com.au'
--	   0,
--	   ls.TrackingNumber,
--	   AgentID,
--	   '0466419484',
--     --Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone)
--	   'AK',
--	   getdate(),
--	   getdate()

--from [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) 
--                                                              join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
--															  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															  
--															  join [EzyTrak Integration].[dbo].deliverychoices(NOLOCK) on Agentname=Deliverychoicedescription
--where 
--ls.isProcessed=0 and [ScanType] ='Accepted by Newsagent'   and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
-- and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1
-- --and ls.trackingnumber in ('CPA25VS0000066','CPA25VS0000064','CPA25VS0000062')
--and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=13)

end
GO
