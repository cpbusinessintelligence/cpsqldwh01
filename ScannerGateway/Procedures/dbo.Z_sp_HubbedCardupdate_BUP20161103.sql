SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[Z_sp_HubbedCardupdate_BUP20161103] as
begin



--Select * from HubbedStaging where ReadytoSendMail=1 and  [ScanType] ='Accepted by Newsagent' and trackingnumber like 'CPW%' trackingnumber in ('CPA25VS0000046','CPA25VS0000047','CPA25VS0000049','CPA25VS0000050','CPA25VS0000051','CPA25VS0000052','CPA25VS0000053','CPA25VS0000054')

--createddatetime>='2016-08-05 08:30:30.533' and isprocessed=1

 Update HubbedStaging set hubbedcard=replace(additionaltext1,'Link Coupon ','')  from trackingevent where sourcereference=trackingnumber and eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and ScanType='Accepted by NewsAgent'
 and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA') and isprocessed=0 and hubbedcard is null



 BEGIN TRAN INSERTMAIL

 -------------Redelivery--------
 --select * from HubbedStaging order by createddatetime desc

 Select  SNo  into #temp from HubbedStaging where ReadytoSendMail=0 and  [ScanType] ='Accepted by Newsagent'

 Select  SNo  into #temp1 from HubbedStaging where ReadytoSendSMS=0 and  [ScanType] ='Accepted by Newsagent'

 
Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
,[Parameter5]
,[Parameter6]
,[parameter7]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])


Select  distinct  8,
       'noreply@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   'CPReporting@couriersplease.com.au,kirsty.tuffley@couriersplease.com.au',
	   0,
	   ls.TrackingNumber,
     HubbedCard,
	   AgentName,
	   isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
    isnull([cd_delivery_addr0],'')+'<br/>'+isnull([cd_delivery_addr1],'')+'<br/>'+isnull([cd_delivery_addr2],'')+'<br/>'+isnull([cd_delivery_addr3],'')+'<br/>'+isnull([cd_delivery_suburb],'')+','+isnull(case when b1.b_name='Sydney' then 'NSW'
	     when b1.b_name='Brisbane' then 'QLD'
		 when b1.b_name='Gold Coast' then 'QLD'
		 when b1.b_name='Melbourne' then 'VIC'
		 when b1.b_name='Perth' then 'WA'
		 when b1.b_name='Canberra' then 'NSW'
		 when b1.b_name='Adelaide' then 'SA'
		 else 'Unknown' end,'') +','+convert(varchar(50),isnull([cd_delivery_postcode],'')),
	   [Operation Hours],
	 --  'kirsty.tuffley@couriersplease.com.au',
    Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   'AK',
	   getdate(),
	   getdate()

 from #temp t join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
                                                               join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
															   join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															   join cpplEDI.dbo.branchs b1 on b1.b_id=cd_deliver_branch
															    join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
where
ls.ReadytoSendMail=0 and 
[ScanType] ='Accepted by Newsagent'  and Hubbedcard is not null and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))=1
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=8)
and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber not like 'CPW%'
--and ls.trackingnumber  ='CPAQORZ0087411001 '



Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
,[Parameter5]
,[Parameter6]
,[parameter7]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])


Select  distinct  8,
       'noreply@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleanseEmail](email),
	   'CPReporting@couriersplease.com.au,kirsty.tuffley@couriersplease.com.au',
	   0,
	   ls.TrackingNumber,
      HubbedCard,
	   AgentName,
	   isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
    isnull(a.CompanyName,'')+'<br/>'+isnull(a.address1,'')+'<br/>'+isnull(a.address2,'')+'<br/>'+isnull(a.suburb,'')+','+isnull(s.statecode,a.statename) +','+convert(varchar(50),isnull(a.postcode,'')),
	   [Operation Hours],
	 --  'kirsty.tuffley@couriersplease.com.au',
    Redirection.[dbo].[fn_CleanseEmail](email),
	   'AK',
	   getdate(),
	   getdate()

 from #temp t join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
                                                                join ezyfreight.dbo.tblitemlabel l on l.labelnumber=ls.trackingnumber
															   join ezyfreight.dbo.tblconsignment(NOLOCK)  c1 on c1.consignmentid=l.consignmentid
															   join ezyfreight.dbo.tbladdress a on a.addressid=c1.destinationid
															   left join ezyfreight.dbo.tblstate s on s.stateid=a.stateid
															   join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
															   left join ezyfreight.dbo.tblcompanyusers u on u.userid=c1.userid
															   left join ezyfreight.dbo.tblcompany c2 on c2.CompanyID=u.CompanyID
where
ls.ReadytoSendMail=0 and 
[ScanType] ='Accepted by Newsagent'  and Hubbedcard is not null and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](email))=1
and isnull(c2.accountnumber,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=8)
and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber like 'CPW%'
--and ls.trackingnumber  ='CPAQORZ0087411001 '


--select * from ezyfreight.dbo.tbladdress


Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[BCC]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])



Select   distinct  9,
       'noreply@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) +'@preview.pcsms.com.au',
	   'kirsty.tuffley@couriersplease.com.au,CPReporting@couriersplease.com.au',
	   0,
	   ls.TrackingNumber,
       HubbedCard,
	   AgentID,
	   Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone),
	   'AK',
	   getdate(),
	   getdate()

from #temp1 t join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
                                                              join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
															  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															  
															   join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
where 
ls.ReadytoSendSMS=0 and [ScanType] ='Accepted by Newsagent'  and Hubbedcard is not null and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))<>1 and Redirection.[dbo].[fn_ValidatePhone](Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=9)
and ls.ScanTime>='2016-08-19 13:00:00.000'



-----------------1st time delivery or Redirection----------------
 
Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
,[Parameter5]
,[Parameter6]
,[parameter7]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])



Select  distinct   12,
       'noreply@couriersplease.com.au',
	 --  'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   0,
	   ls.TrackingNumber,
     HubbedCard,
	   AgentName,
	   isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
    isnull([cd_delivery_addr0],'')+'<br/>'+isnull([cd_delivery_addr1],'')+'<br/>'+isnull([cd_delivery_addr2],'')+'<br/>'+isnull([cd_delivery_addr3],'')+'<br/>'+isnull([cd_delivery_suburb],'')+','+isnull(case when b1.b_name='Sydney' then 'NSW'
	     when b1.b_name='Brisbane' then 'QLD'
		 when b1.b_name='Gold Coast' then 'QLD'
		 when b1.b_name='Melbourne' then 'VIC'
		 when b1.b_name='Perth' then 'WA'
		 when b1.b_name='Canberra' then 'NSW'
		 when b1.b_name='Adelaide' then 'SA'
		 else 'Unknown' end,'') +','+convert(varchar(50),isnull([cd_delivery_postcode],'')),
	   [Operation Hours],
	 --  'kirsty.tuffley@couriersplease.com.au',
    Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
	   'AK',
	   getdate(),
	   getdate()

from #temp t join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
                                                               join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
															   join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															   join cpplEDI.dbo.branchs b1 on b1.b_id=cd_deliver_branch
															   join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
where
ls.ReadytoSendMail=0  and [ScanType] ='Accepted by Newsagent'   and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))=1
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=12)
and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber not like 'CPW%'
--and ls.trackingnumber in ('CPA25VS0000066','CPA25VS0000064','CPA25VS0000062')




Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
      ,[Parameter4]
,[Parameter5]
,[Parameter6]
,[parameter7]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])



Select  distinct   12,
       'noreply@couriersplease.com.au',
	 --  'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
	   Redirection.[dbo].[fn_CleanseEmail](email),
	   0,
	   ls.TrackingNumber,
     HubbedCard,
	   AgentName,
	    isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
    isnull(a.CompanyName,'')+'<br/>'+isnull(a.address1,'')+'<br/>'+isnull(a.address2,'')+'<br/>'+isnull(a.suburb,'')+','+isnull(s.statecode,a.statename) +','+convert(varchar(50),isnull(a.postcode,'')) ,
	   [Operation Hours],
	 --  'kirsty.tuffley@couriersplease.com.au',
    Redirection.[dbo].[fn_CleanseEmail](email),
	   'AK',
	   getdate(),
	   getdate()

from #temp t join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
                                                               join ezyfreight.dbo.tblitemlabel l on l.labelnumber=ls.trackingnumber
															   join ezyfreight.dbo.tblconsignment(NOLOCK)  c1 on c1.consignmentid=l.consignmentid
															   join ezyfreight.dbo.tbladdress a on a.addressid=c1.destinationid
															   left join ezyfreight.dbo.tblstate s on s.stateid=a.stateid
															   join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
															   left join ezyfreight.dbo.tblcompanyusers u on u.userid=c1.userid
															   left join ezyfreight.dbo.tblcompany c2 on c2.CompanyID=u.CompanyID
where
ls.ReadytoSendMail=0  and [ScanType] ='Accepted by Newsagent'   and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](email))=1
and isnull(c2.accountnumber,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=12)
and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber like 'CPW%'





Insert into [Redirection].[dbo].[SendMail]([ContextID]
      ,[FromAddr]
      ,[ToAddr]
	  ,[TransmitFlag]
	  ,[Parameter1]
      ,[Parameter2]
      ,[Parameter3]
	  ,[AddWho]
	  ,[AddDateTime]
	  ,[ModifiedDateTime])




Select  distinct 13,
       'noreply@couriersplease.com.au',
	   --'0466419484@preview.pcsms.com.au',
	   Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) +'@preview.pcsms.com.au',
	   0,
	   ls.TrackingNumber,
	   AgentID,
     Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone),
	   'AK',
	   getdate(),
	   getdate()

from #temp1 t join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
                                                              join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
															  join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
															  
															   join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
where 
ls.ReadytoSendSMS=0  and [ScanType] ='Accepted by Newsagent'   and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft)
 and Redirection.[dbo].[fn_ValidatePhone](Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1
and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] where mailtemplateid=13)
and ls.ScanTime>='2016-08-19 13:00:00.000'

update  [ScannerGateway].[dbo].[HubbedStaging] set [ReadytoSendMail]=1 where sno in (Select Sno from #temp)

update  [ScannerGateway].[dbo].[HubbedStaging] set [ReadytoSendSMS]=1 where sno in (Select Sno from #temp1)

 COMMIT TRAN INSERTMAIL


 Select consignmentcode,c.pickupid,Agentid,JobNumber into #temp3 
 from [dbo].[HubbedStaging] join cpsqlweb01.ezyfreight.dbo.tblitemlabel l on l.labelnumber COLLATE SQL_Latin1_General_CP1_CI_AS=trackingnumber COLLATE SQL_Latin1_General_CP1_CI_AS 
                            join cpsqlweb01.ezyfreight.dbo.tblconsignment c on c.consignmentid=l.consignmentid
			 where isupdatejob=0 and scantype='Dropped off at POPShop'



 update ezyfreight.dbo.tbladdress set address1=deliverychoicename,address2=d.address1,suburb=d.suburb,PostCode=d.postcode,StateName=d.State,country=d.country
from #temp3 join ezyfreight.dbo.tbladdress on addressid=pickupid 
            join [EzyTrak Integration].[dbo].deliverychoices d on agentid=deliverychoiceid



-- update cpsqlweb01.ezyfreight.dbo.tbladdress set address1=deliverychoicename,address2=d.address1,suburb=d.suburb,PostCode=d.postcode,StateName=d.State,country=d.country
--from #temp3 join cpsqlweb01.ezyfreight.dbo.tbladdress on addressid=pickupid 
--            join [EzyTrak Integration].[dbo].deliverychoices d on agentid=deliverychoiceid

 
update  [ScannerGateway].[dbo].[HubbedStaging] set isupdatejob=1 where isupdatejob=0 and scantype='Dropped off at POPShop'

end
GO
