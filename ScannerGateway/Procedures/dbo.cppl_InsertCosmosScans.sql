SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[cppl_InsertCosmosScans]
(
	@MaxRecords		int = Null
)
AS

BEGIN


     --'=====================================================================
    --' CP -Stored Procedure -[cppl_InsertCosmosScans]
    --' ---------------------------
    --' Purpose: Updates TrackingEvent Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 17 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                    Bookmark
    --' ----          ---     ---     -----                                                                      -------
    --' 17/09/2014    AB      1.00    Created the procedure                                                      --AB20140917
	--- 26/09/2014    AB      1.00    Get effective cosmos driver id without ProntoID                            --AB20140926
	--  02/12/2014    AB2     1.00  To Accommodate Links attached only to Primary and not links                    --AB20141202
    --'=====================================================================



	SET NOCOUNT ON
	
	/* Insert labels into label table and then insert records into reference map 

	BEGIN TRANSACTION InsertLabelNumbers
	INSERT INTO Label(LabelNumber)
	SELECT DISTINCT LabelNumber
	FROM [CosmosScanData]
	EXCEPT SELECT Label.LabelNumber FROM Label;

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRANSACTION InsertLabelNumbers;
	END
	COMMIT TRANSACTION InsertLabelNumbers;*/

	
	SELECT @MaxRecords = ISNULL(@MaxRecords, 0);

	
	DECLARE @errFlag int
	SELECT @errFlag = 0;

	/*** LUKE GRENFELL -- 31/5/2010
	BEGIN TRY
	
		BEGIN TRANSACTION DriversRuns
		
		EXEC dbo.cppl_InsertGatewayDriver;
		EXEC dbo.cppl_InsertGatewayRun;
	
		COMMIT TRANSACTION DriversRuns
		
	END TRY
	BEGIN CATCH

		WHILE @@TRANCOUNT > 0
		BEGIN
			ROLLBACK WORK;
		END
	
		SELECT @errFlag = 1;
		
		EXEC cppl_LogRethrowError;
	
	END CATCH
	*/
	
	DECLARE @minId int, @maxId int, @startId int, @endId int, @RecordBatchNum int, @counter int

	DECLARE @InsertedTrackingEvents TABLE( 
				LabelId UNIQUEIDENTIFIER, 
				EventId UNIQUEIDENTIFIER,
				UNIQUE(LabelId, EventId));
				
	DECLARE @LinkLabels TABLE(
				PrimaryLabel varchar(100),
				LinkLabel varchar(100),
				UNIQUE(PrimaryLabel, LinkLabel));
				
	DECLARE @AllInsertedTrackingEvents TABLE(
				LabelId uniqueidentifier,
				EventId uniqueidentifier,
				UNIQUE(LabelId, EventId));

	/*
	 *
	 * 01/12/2010 - Craig Parris
	 *
	 * We need to make sure Label records exist for the link coupons.
	 * Sometimes, it seems, if the scans and links are in different files,
	 * then the Label records for the link coupons won't always get created
	 *
	 */
	BEGIN TRY
	
		BEGIN TRANSACTION InsertLinkLabels;
		
		MERGE Label AS Target
		USING
		(
			SELECT DISTINCT(LinkCoupon) AS [LabelNumber]
			FROM [dbo].[LoadScannerGatewayData]
			WHERE ISNULL(ErrorFlag, 0) = 0
			AND LinkCoupon Is Not Null
		)
		AS Source
		ON Target.LabelNumber = Source.LabelNumber
		WHEN NOT MATCHED THEN
			INSERT
				(
					LabelNumber
				)
			VALUES
				(
					Source.LabelNumber
				);
	
		COMMIT TRANSACTION InsertLinkLabels;
		
	END TRY
	BEGIN CATCH

		WHILE @@TRANCOUNT > 0
		BEGIN
			ROLLBACK WORK;
		END
	
		SELECT @errFlag = 1;
		
		EXEC cppl_LogRethrowError;
	
	END CATCH
	
	/*
	 * 03/06/2010 - Craig Parris
	 * 
	 * first we'll flag as errored any records that don't match ODS driver details
	 *
	 */
	BEGIN TRY
	
		-- reset errored records from 2 days ago, and try to re-process them again
		-- (only ones with driver mis-match, though)
		BEGIN TRANSACTION ResetErrors


update loadscannergatewaydata set action=ltrim(rtrim(replace(exception,'Status:',''))) ,ErrorFlag=0 where  exception  like 'status%' and charindex(',',exception,1)=0 and ErrorReason='Invalid action - no corresponding ODS event type found'
and exception not like '%Attempted Delivery%' and exception not like '%Consolidated%' and exception not like '%Transit to Depot%' 

update loadscannergatewaydata set action=ltrim(rtrim(replace(substring(exception,1,(charindex(',',exception,1)-1)),'Status:',''))) ,ErrorFlag=0 where  exception  like 'status%,%' and ErrorReason='Invalid action - no corresponding ODS event type found'
and exception not like '%Attempted Delivery%' and exception not like '%Consolidated%' and exception not like '%Transit to Depot%' 

update loadscannergatewaydata set action='failed' ,ErrorFlag=0 where  exception  like 'status%'  and ErrorReason='Invalid action - no corresponding ODS event type found'
and exception  like '%Attempted Delivery%' 

		
		UPDATE [LoadScannerGatewayData]
		SET ErrorFlag = 0,
			ErrorReason = Null
		WHERE ISNULL(ErrorFlag, 0) != 0
			AND ISNULL(ErrorReason, '') LIKE '%not consis%'
			AND [TimeStamp] < DATEADD(day, -2, GETDATE());
		
		COMMIT TRANSACTION ResetErrors;
	
		BEGIN TRANSACTION DriverErrors
		
		UPDATE [LoadScannerGatewayData] 
		WITH (TABLOCK)
			SET ErrorFlag = 1,
			ErrorReason = 'Cosmos/ODS driver details are not consistent'
		FROM
			[LoadScannerGatewayData] 
			WHERE dbo.cppl_GetEffectiveCosmosDriverId(LoadScannerGatewayData.Driver, 
														LoadScannerGatewayData.Branch,
														--LoadScannerGatewayData.ProntoWarehouseID,---#AB20140926
														LoadScannerGatewayData.[TimeStamp]) Is Null
			AND ISNULL(ErrorFlag, 0) = 0;
		
		UPDATE [dbo].[LoadScannerGatewayData]
		WITH (TABLOCK)
			SET ErrorFlag = 1,
			ErrorReason = 'Invalid action - no corresponding ODS event type found'
		FROM
			[dbo].[LoadScannerGatewayData] c
			LEFT OUTER JOIN EventTypeMap m
			ON c.[Action] = m.Code 
			LEFT OUTER JOIN EventType e
			ON m.EventTypeId = e.Id 
			WHERE ((m.Id Is Null)
			OR (e.Id Is Null))
			AND ISNULL(c.ErrorFlag, 0) = 0;

		UPDATE [LoadScannerGatewayData]
		WITH (TABLOCK)
			SET ErrorFlag = 1,
			ErrorReason = 'Label record not found for Cosmos label number'
		FROM
			[LoadScannerGatewayData] c
			LEFT OUTER JOIN Label l
			ON c.LabelNumber = l.LabelNumber 
			WHERE (l.Id Is Null)
			AND ISNULL(c.ErrorFlag, 0) = 0;
			
		UPDATE [LoadScannerGatewayData]
		WITH (TABLOCK)
			SET ErrorFlag = 1,
			ErrorReason = 'Label record not found for Cosmos link coupon number'
		FROM
			[LoadScannerGatewayData] c
			LEFT OUTER JOIN Label l
			ON c.LinkCoupon = l.LabelNumber 
			WHERE (l.Id Is Null)
			AND c.LinkCoupon Is Not Null
			AND ISNULL(c.ErrorFlag, 0) = 0;
		
		COMMIT TRANSACTION DriverErrors;

	END TRY
	BEGIN CATCH
	
		WHILE @@TRANCOUNT > 0
		BEGIN
			ROLLBACK WORK;
		END
	
		SELECT @errFlag = 1;
		print 'error';

		EXEC cppl_LogRethrowError;


	
	END CATCH

	IF @errFlag = 0
	BEGIN
	
		SELECT @counter = 0;
		
		SELECT @minId = MIN(Id), @maxId = MAX(Id)
		FROM [LoadScannerGatewayData] 
		WHERE ISNULL(ErrorFlag, 0) = 0;
		
		IF ((ISNULL(@minId, -1) < 0) OR (ISNULL(@maxId, -1) < 0))
		BEGIN
			-- 11/06/2010 - Craig Parris
			--
			-- we don't throw an error here anymore, because if all the records have been
			-- flagged as errors in the update statements above, then there won't be any
			-- to process.  We'll just return from the proc
			--
			-- RAISERROR('Cannot determine start Id and/or end Id for CosmosScanData table', 16, 1);
			RETURN;
		END
		
		-- @RecordBatchNum = how many scan records to process at a time
		--
		-- if you change the value of this variable, you should also change
		-- the optimize values for @startId and @endId in the main SELECT statement
		--
		SELECT @RecordBatchNum = 1500
		SELECT @startId = @minId 
		SELECT @endId = (@startId + @RecordBatchNum)

		/*
		INSERT INTO @LinkLabels 
		(PrimaryLabel, LinkLabel)
		SELECT DISTINCT LabelNumber, LinkCoupon
		FROM CosmosScanData 
		WHERE [Action] = 'link'
		AND ISNULL(LabelNumber, '') != ''
		AND ISNULL(LinkCoupon, '') != ''
		AND ISNULL(ErrorFlag, 0) = 0;
		*/

	END
	ELSE
	BEGIN
		RETURN;
	END
	
	/* Insert labels into label table and then insert records into reference map */
	WHILE ((@errFlag = 0) AND (@startId <= @maxId))
	BEGIN

		IF @MaxRecords > 0
		BEGIN
			IF @counter > @MaxRecords 
			BEGIN
				BREAK;
			END
		END

		
		BEGIN TRY
		
			IF @errFlag = 0
			BEGIN

				
				DELETE FROM @InsertedTrackingEvents;

				
				BEGIN TRANSACTION InsertTrackingEvents;

				
					
				WITH ScansForInsert AS
				(
					/*
					 * 03/06/2010 - Craig Parris
					 *
					 * now we just do a join between the CosmosScanData table
					 * and the Driver table to determine the DriverId
					 *
					 * Runs are not used anymore for Cosmos scans, as the driver/contractor
					 * relationship is always one to one
					 *
					 */
					SELECT 
					EventType.Id AS [EventTypeId]
					,LoadScannerGatewayData.[TimeStamp] AS [EventDateTime]
					,NULL AS [Description]
					,Label.Id AS [LabelId]
					,NULL AS [AgentId]
					,Driver.Id AS [DriverId]
					--,Run.Id AS [RunId]
					,NULL AS [RunId]
					,NULL AS [EventTypeReasonId]
					,NULL AS [EventTypeReasonDetailId]
					,NULL AS [EventLocation]
					,LEFT(LoadScannerGatewayData.Exception, 50) AS [ExceptionReason]
					,
						CASE LoadScannerGatewayData.Action
							WHEN 'link' THEN (LEFT(('Link Coupon ' + ISNULL(LoadScannerGatewayData.LinkCoupon, '')), 100))
							WHEN 'delivery' THEN LEFT(LoadScannerGatewayData.SignatureName, 100)
							WHEN 'transfer' THEN ISNULL(LoadScannerGatewayData.TransferDriverNumber, '')
							WHEN 'transit' THEN ISNULL(LoadScannerGatewayData.TransitID, '')
							ELSE Null
						END AS [AdditionalText1]
					,
						CASE WHEN ((LTRIM(RTRIM(LTRIM(RTRIM(ISNULL(LoadScannerGatewayData.LocationCustomer, ''))) + LTRIM(RTRIM(ISNULL(LoadScannerGatewayData.LocationID, ''))) + LTRIM(RTRIM(ISNULL(LoadScannerGatewayData.LocationName, ''))))) IN ('', '0', '00'))
								OR (LTRIM(RTRIM(ISNULL(LoadScannerGatewayData.LocationID, ''))) = '0'))
							THEN Null
							ELSE
								(LEFT((LTRIM(RTRIM('DLB ' + LTRIM(RTRIM(ISNULL(LoadScannerGatewayData.LocationID, ''))) + ' - ' + LTRIM(RTRIM(ISNULL(LoadScannerGatewayData.LocationName, ''))) + ', ' + LTRIM(RTRIM(ISNULL(LoadScannerGatewayData.LocationCustomer, '')))))), 100))
						END AS [AdditionalText2]
					,
					LoadScannerGatewayData.Items AS [NumberOfItems]
					,NULL AS [DeliveryLocationId]
					,(SELECT TOP 1 Id 
					FROM SystemCodes
					WHERE Discriminator = 'SourceSystem'
					AND FriendlyCode = 'Cosmos')  AS [SourceId]
					,LEFT(LoadScannerGatewayData.LabelNumber, 100)  AS [SourceReference]
					,LoadScannerGatewayData.SignatureId AS [CosmosSignatureId]
					--,branchs.b_id AS [CosmosBranchId]
					,ISNULL(Branch.CosmosBranchId, 0) AS [CosmosBranchId]
					FROM LoadScannerGatewayData
					/* LEFT JOIN [ECA-SQL2K8-1\REPORTING].cpplEDI.dbo.branchs ON branchs.b_emmcode = CosmosScanData.Branch
					 * LEFT JOIN [ECA-SQL2K8-1\REPORTING].cpplEDI.dbo.driver AS CosmosDriver ON CosmosDriver.dr_number = CosmosScanData.Driver 
					 * AND ((CosmosDriver.dr_branch = branchs.b_id)
					 *	OR
					 * (CosmosScanData.Branch = 'nkope' AND CosmosDriver.dr_branch = 
					 *	(SELECT b1.b_id FROM [ECA-SQL2K8-1\REPORTING].cpplEDI.dbo.branchs b1
					 *		WHERE b1.b_emmcode = 'adelaide'))
					 *	OR
					 * (CosmosScanData.Branch = 'adelaide' AND CosmosDriver.dr_branch = 
					 *	(SELECT b2.b_id FROM [ECA-SQL2K8-1\REPORTING].cpplEDI.dbo.branchs b2
					 *		WHERE b2.b_emmcode = 'nkope')))
					 * LEFT JOIN Driver AS ProntoDriver ON ProntoDriver.ProntoDriverCode = CosmosScanData.ProntoWarehouseID
					 * LEFT JOIN DriverMap ON DriverMap.SourceId = CosmosDriver.dr_id
					 * LEFT JOIN Driver ON Driver.Id = DriverMap.DriverId
					 * LEFT JOIN RunMap ON RunMap.SourceId = CosmosDriver.dr_id
					 * LEFT JOIN Run ON Run.Id = RunMap.RunId
					 */
					JOIN Driver ON Driver.Id = dbo.cppl_GetEffectiveCosmosDriverId(LoadScannerGatewayData.Driver,
																					LoadScannerGatewayData.Branch,
																					--LoadScannerGatewayData.ProntoWarehouseID,--#AB20140926
																					LoadScannerGatewayData.[TimeStamp])
					-- 	AND ISNULL(CosmosScanData.ProntoWarehouseID, '') = ISNULL(Driver.ProntoDriverCode, '')
					JOIN Branch ON Driver.BranchId = Branch.Id
					-- 	AND CosmosScanData.Branch = Branch.CosmosBranch
					JOIN Label ON Label.LabelNumber = LoadScannerGatewayData.LabelNumber
					JOIN EventTypeMap ON EventTypeMap.Code = LoadScannerGatewayData.[Action] --AND EventTypeMap.IsDeleted = 0
																					 -- include deleted EventTypeMap records
					JOIN EventType ON EventType.ID = EventTypeMap.EventTypeID --AND EventType.IsDeleted = 0
																			  -- include deleted EventType records
					WHERE LoadScannerGatewayData.Id BETWEEN @startId AND @endId 
					AND ISNULL(LoadScannerGatewayData.ErrorFlag, 0) = 0
					AND Driver.Id Is Not Null
					--WHERE ISNULL(CosmosScanData.[Action], '') NOT IN ('', 'link')
				)
				INSERT INTO TrackingEvent
				--WITH (TABLOCK)
						   (
						   [EventTypeId]
						   ,[EventDateTime]
						   ,[Description]
						   ,[LabelId]
						   ,[AgentId]
						   ,[DriverId]
						   ,[RunId]
						   ,[EventLocation]
						   ,[ExceptionReason]
						   ,[AdditionalText1]
						   ,[AdditionalText2]
						   ,[NumberOfItems]
						   ,[SourceId]
						   ,[SourceReference]
						   ,[CosmosSignatureId]
						   ,[CosmosBranchId])
				   --OUTPUT INSERTED.LabelId, INSERTED.Id INTO @InsertedTrackingEvents
				SELECT ScansForInsert.[EventTypeId]
					   ,ScansForInsert.[EventDateTime]
					   ,ScansForInsert.[Description]
					   ,ScansForInsert.[LabelId]
					   ,ScansForInsert.[AgentId]
					   ,ScansForInsert.[DriverId]
					   ,ScansForInsert.[RunId]
					   ,ScansForInsert.[EventLocation]
					   ,ScansForInsert.[ExceptionReason]
					   ,ScansForInsert.[AdditionalText1]
					   ,ScansForInsert.[AdditionalText2]
					   ,ScansForInsert.[NumberOfItems]
					   ,ScansForInsert.[SourceId]
					   ,ScansForInsert.[SourceReference]
					   ,ScansForInsert.[CosmosSignatureId]
					   ,ScansForInsert.[CosmosBranchId]
				FROM ScansForInsert 
					LEFT OUTER JOIN TrackingEvent 
						ON ScansForInsert.EventTypeId = TrackingEvent.EventTypeId 
							AND ScansForInsert.LabelId = TrackingEvent.LabelId 
							AND ScansForInsert.EventDateTime = TrackingEvent.EventDateTime 
							AND ScansForInsert.DriverId = TrackingEvent.DriverId
							-- AND ScansForInsert.RunId = TrackingEvent.RunId
							AND ScansForInsert.SourceId = TrackingEvent.SourceId 
							and isnull(ScansForInsert.Additionaltext1,'')=isnull(Trackingevent.AdditionalText1,'')--AB20141202--To Accommodate Links attached only to Primary and not links
						WHERE TrackingEvent.Id Is Null
						OPTION (OPTIMIZE FOR (@startId = 1, @endId = 1500));
						/*
						 * When there are a large amount of records in the CosmosScanData table
						 * the query optimiser doesn't work so well with the @startId and @endId variables,
						 * so we just force them to literal values matching the number of records
						 * that get processed at a time
						 *
						 */
						--OPTION (KEEPFIXED PLAN);

				

				
				/*
				INSERT INTO @AllInsertedTrackingEvents 
				(LabelId, EventId)
				SELECT LabelId, EventId
				FROM @InsertedTrackingEvents;
				*/

				

			END
			
			--IF @@ERROR <> 0 
			--BEGIN
			--	EXEC cppl_LogRethrowError;
			--	ROLLBACK TRANSACTION InsertTrackingEvents;
			--END
				

			COMMIT TRANSACTION InsertTrackingEvents;

				

		END TRY
		BEGIN CATCH

			/* 
			==================================================================================
			FAILED! -- LOG ERROR 
			==================================================================================
			*/
			WHILE @@TRANCOUNT > 0
			BEGIN
				ROLLBACK WORK;
			END
		
			SELECT @errFlag = 1;
			
			EXEC cppl_LogRethrowError;
			
		END CATCH;

		/* 31 Mar 2010 - Craig Parris
		
		   -- no need for ReferenceMap records anymore
		*/

		----BEGIN TRY

		----	IF @errFlag = 0
		----	BEGIN
				
		----		BEGIN TRANSACTION InsertReferenceMap
				
		----		/* insert the ReferenceMap record */
		----		/*
		----		 25 Feb 2010 - Craig Parris
				
		----		 this has been changed from a MERGE statement to a straight insert
				
		----		 there'll never be any existing TrackingEvent records in the 
		----		 @InsertedTrackingEvents table as they are all new inserts from above
				
		----		MERGE [ODS].[dbo].[ReferenceMap] AS Target
		----		USING (SELECT EventId AS TrackingEventId,
		----						LabelId AS LabelId,
		----						'LabelTrackingEvent' AS Discriminator
		----						FROM @InsertedTrackingEvents) AS Source
		----		ON Target.Discriminator = Source.Discriminator
		----		AND Target.LabelId = Source.LabelId
		----		AND Target.TrackingEventId = Source.TrackingEventId
		----		WHEN MATCHED THEN
		----			UPDATE
		----				SET Target.IsDeleted = 0
		----				, Target.LastModifiedDate = GETDATE()
		----		WHEN NOT MATCHED THEN
		----			INSERT
		----				(
		----				 Discriminator
		----				 ,LabelId
		----				 ,TrackingEventId
		----				 ,IsDeleted
		----				 ,CreatedDate
		----				 ,LastModifiedDate
		----				 )
		----			VALUES
		----				(
		----				 'LabelTrackingEvent'
		----				 ,Source.LabelId
		----				 ,Source.TrackingEventId
		----				 ,0
		----				 ,GETDATE()
		----				 ,GETDATE()
		----				);
		----		*/

		----		IF @@ERROR <> 0 
		----		BEGIN
		----			EXEC cppl_LogRethrowError;
		----			ROLLBACK TRANSACTION InsertReferenceMap;
		----		END

		----		INSERT INTO ReferenceMap 
		----		(
		----			Discriminator, LabelId, TrackingEventId, IsDeleted, CreatedDate, LastModifiedDate
		----		)
		----		SELECT
		----			'LabelTrackingEvent',
		----			LabelId,
		----			EventId,
		----			0,
		----			GETDATE(),
		----			GETDATE()
		----		FROM @InsertedTrackingEvents;

		----		COMMIT TRANSACTION InsertReferenceMap;
				
		----	END
					
		----END TRY
		----BEGIN CATCH

		----	/* 
		----	==================================================================================
		----	FAILED! -- LOG ERROR 
		----	==================================================================================
		----	*/
		----	WHILE @@TRANCOUNT > 0
		----	BEGIN
		----		ROLLBACK WORK;
		----	END

		----	SELECT @errFlag = 1;

		----	EXEC cppl_LogRethrowError;
			
		----END CATCH;

		IF @errFlag = 0
		/* BEGIN

			TRUNCATE TABLE CosmosScanData;
			
		END */
		-- just delete the records that have been processed
		BEGIN
		
			DELETE FROM LoadScannerGatewayData 
			WHERE Id BETWEEN @startId AND @endId
			AND ISNULL(ErrorFlag, 0) = 0;
		
		END
		ELSE
		BEGIN
			BREAK;
		END

		-- set the start and end Ids to process the next batch
		SELECT @startId = -1;
		
		SELECT @startId = MIN(Id)
		FROM LoadScannerGatewayData
		WHERE Id > @endId 
		AND ISNULL(ErrorFlag, 0) = 0;
		
		-- check if we've run out of records to process
		IF ISNULL(@startId, 0) <= 0
		BEGIN
			SELECT @startId = @maxId + 1;
		END

		--SELECT @startId = @endId + 1
		SELECT @endId = @startId + @RecordBatchNum;

		-- increment the counter, which gets (loosely) checked against the @MaxRecords variable
		SELECT @counter = @counter + 1;

		

	END

	/*
	 *
	 * 17 Nov 2010 - Craig Parris
	 *
	 * removing the TrackingEventRelationship inserts as we
	 * don't need this data anymore
	 *
	 *
		BEGIN TRY

				BEGIN TRANSACTION TrackingEventRelationship
				
				--/--*
				-- 26 Feb 2010 - Craig Parris
				--
				-- revised TrackingEventRelationship insert statement
				--
				----INSERT INTO TrackingEventRelationship
				----(ParentTrackingEventId, ChildTrackingEventId)
				----SELECT 
				----TrackingEvent.Id ParentTrackingEventId,
				----LinkTrackingEvent.Id ChildTrackingEventId
				----FROM
				----Label
				----JOIN ReferenceMap ON ReferenceMap.LabelId = Label.Id
				----JOIN TrackingEvent ON ReferenceMap.TrackingEventId = TrackingEvent.Id
				----JOIN EventType ON TrackingEvent.EventTypeId = EventType.Id
				----JOIN EventTypeMap ON EventTypeMap.EventTypeId = EventType.Id
				----JOIN CosmosScanData ON Label.LabelNumber = CosmosScanData.LabelNumber
				----JOIN Label LinkLabel ON CosmosScanData.LinkCoupon = LinkLabel.LabelNumber
				----JOIN ReferenceMap LinkReferenceMap ON LinkReferenceMap.LabelId = LinkLabel.Id
				----JOIN TrackingEvent LinkTrackingEvent ON LinkReferenceMap.TrackingEventId = LinkTrackingEvent.Id
				----JOIN @InsertedTrackingEvents ite ON
				----	((ite.EventId = TrackingEvent.Id)
				----		OR
				----	(ite.EventId = LinkTrackingEvent.Id))
				----WHERE EventTypeMap.Code = 'link'
				----EXCEPT
				----SELECT ParentTrackingEventId, ChildTrackingEventId FROM TrackingEventRelationship
				------*--/
				
				------IF @@ERROR <> 0 
				------BEGIN
				------	EXEC cppl_LogRethrowError;
				------	ROLLBACK TRANSACTION TrackingEventRelationship;
				------END

				IF @Debug = 1
				BEGIN
					PRINT 'Before TrackingEventRelationship insert: ' + 
						CONVERT(varchar(10), DATEPART(hour, GETDATE())) + ':' + 
						CONVERT(varchar(10), DATEPART(minute, GETDATE())) + ':' + 
						CONVERT(varchar(10), DATEPART(second, GETDATE())) + '.' + 
						CONVERT(varchar(10), DATEPART(millisecond, GETDATE()));
				END

				INSERT INTO TrackingEventRelationship 
				WITH (TABLOCK)
				(ParentTrackingEventId, ChildTrackingEventId)
				SELECT
					pte.Id,
					cte.Id
				FROM TrackingEvent pte
				JOIN Label pl
				ON pte.LabelId = pl.Id 
				JOIN @LinkLabels ll
				ON ll.PrimaryLabel = pl.LabelNumber 
				JOIN Label cl
				ON ll.LinkLabel = cl.LabelNumber 
				JOIN TrackingEvent cte
				ON cte.LabelId = cl.Id 
				JOIN @AllInsertedTrackingEvents pite
				ON pite.EventId = pte.Id 
				JOIN @AllInsertedTrackingEvents cite
				ON cite.EventId = cte.Id 
				WHERE NOT EXISTS
					(
						SELECT 1 FROM TrackingEventRelationship ter
						WHERE ter.ParentTrackingEventId = pte.Id 
						AND ter.ChildTrackingEventId = cte.Id
					)
				 --/--* EXCEPT
					SELECT ParentTrackingEventId, ChildTrackingEventId
					FROM TrackingEventRelationship --*--/
				OPTION (KEEPFIXED PLAN);

				IF @Debug = 1
				BEGIN
					PRINT 'After TrackingEventRelationship insert: ' + 
						CONVERT(varchar(10), DATEPART(hour, GETDATE())) + ':' + 
						CONVERT(varchar(10), DATEPART(minute, GETDATE())) + ':' + 
						CONVERT(varchar(10), DATEPART(second, GETDATE())) + '.' + 
						CONVERT(varchar(10), DATEPART(millisecond, GETDATE()));
				END

				COMMIT TRANSACTION TrackingEventRelationship;
			
		END TRY
		BEGIN CATCH

			--/--* 
			==================================================================================
			FAILED! -- LOG ERROR 
			==================================================================================
			--*--/
			WHILE @@TRANCOUNT > 0
			BEGIN
				ROLLBACK WORK;
			END

			EXEC cppl_LogRethrowError;
				
		END CATCH;

	*/



	SET NOCOUNT OFF;

	-- 29/11/2010 - Craig Parris
	--
	-- temporary addition to run duplicate label fix-up after each scan file processing
	--
	-- EXEC dbo.cppl_RemoveDuplicateLabelNumbers @MaxRecords = 2500;
	
END



GO
