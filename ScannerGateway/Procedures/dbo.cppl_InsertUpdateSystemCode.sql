SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Luke Grenfell
-- Create date: 5/8/2010
-- Description:	Inserts new system codes
-- =============================================
CREATE PROCEDURE [dbo].[cppl_InsertUpdateSystemCode] 
	@Discriminator varchar(100) = NULL, 
	@FriendlyCode varchar(50) = NULL,
	@Description VARCHAR(50) = NULL
AS
BEGIN


      --'=====================================================================
    --' CP -Stored Procedure -[cppl_InsertUpdateSystemCode] 
    --' ---------------------------
    --' Purpose: Updates systemcodes table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/09/2014    AB      1.00    Created the procedure                             --AB20140903

    --'=====================================================================


	SET NOCOUNT ON;

	/**** Format the description so that the first letter is in UPPER case. ****/
	SET @Description = UPPER(LEFT(@Description, 1)) + RIGHT(@Description, LEN(@Description)-1);

	/**** Set the FriendlyCode to UPPER CASE ****/
	SET @FriendlyCode = UPPER(@FriendlyCode);

	/**** Get maximum code and add 1 to it for unique code ****/
	DECLARE @Code INT, @SystemCodeCategoryTypeId INT
	--SET @Code = (SELECT MAX(Code) FROM [SystemCodes]) + 1

	/**** Get SystemCodeCategoryTypeID for discriminitar if exists ****/
	SET @SystemCodeCategoryTypeId = (SELECT MAX(SystemCodeCategoryTypeId) FROM [SystemCodes] WHERE Discriminator = @Discriminator)
	/**** Get new SystemCodeCategoryTypeID if not already exists ****/
	IF @SystemCodeCategoryTypeId = 0 OR @SystemCodeCategoryTypeId IS NULL
		BEGIN
			SET @SystemCodeCategoryTypeId = (SELECT MAX(SystemCodeCategoryTypeId) FROM [SystemCodes]) + 1
		END
	/**** Get Id for discriminator and friendly code ****/
	DECLARE @ID UNIQUEIDENTIFIER = (SELECT Id FROM [SystemCodes] WHERE Discriminator = @Discriminator AND FriendlyCode = @FriendlyCode) 
	
	/**** Insert new record if discriminator and friendly code does not already exist****/
	IF @ID IS NULL
		BEGIN
			/**** Get maximum code for current category and add 1 to it for unique code ****/
			SET @Code = 0;
			SET @Code = ((SELECT MAX(Code) FROM [SystemCodes]
							WHERE SystemCodeCategoryTypeId = @SystemCodeCategoryTypeId) + 1);
			
			IF ISNULL(@Code, 0) < 1
			BEGIN
				SET @Code = 1;
			END

			INSERT INTO [SystemCodes] ([Code], [Discriminator], [SystemCodeCategoryTypeId], [Description], [FriendlyCode])
			VALUES (@Code, @Discriminator, @SystemCodeCategoryTypeId, @Description, @FriendlyCode)
		END
	/**** Update existing record if discriminator and friendly code does exists****/
	ELSE
		BEGIN
			UPDATE [ScannerGateway].[dbo].[SystemCodes]
			SET [SystemCodeCategoryTypeId] = @SystemCodeCategoryTypeId
			  ,[Description] = @Description
			  ,[LastModifiedDate] = GETDATE()
			  ,[IsDeleted] = 0
			WHERE Id = @ID;
		END
END
GO
