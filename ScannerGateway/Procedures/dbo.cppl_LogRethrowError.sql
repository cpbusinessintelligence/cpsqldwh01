SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[cppl_LogRethrowError] (@RethrowError bit = 1, @LogError bit = 1) AS
    
	

     --'=====================================================================
    --' CP -Stored Procedure -[cppl_LogRethrowError]
    --' ---------------------------
    --' Purpose: Throws Error-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 17 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 17/09/2014    AB      1.00    Created the procedure                             --AB20140917

    --'=====================================================================
	
	Begin
	
	
	-- Return if there is no error information to retrieve.
    IF ERROR_NUMBER() IS NULL
        RETURN;
   
    DECLARE 
        @ErrorMessage    NVARCHAR(4000),
        @ErrorNumber     INT,
        @ErrorSeverity   INT,
        @ErrorState      INT,
        @ErrorLine       INT,
        @ErrorProcedure  NVARCHAR(200);

    -- Assign variables to error-handling functions that 
    -- capture information for RAISERROR.
    SELECT 
		@ErrorMessage =  ERROR_MESSAGE(),
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ERROR_PROCEDURE();

  IF @LogError = 1
  BEGIN
		EXEC [dbo].[cppl_WriteErrorLog]
			@ErrorMessage ,
			@ErrorNumber ,
			@ErrorProcedure ,
			@ErrorSeverity ,
			@ErrorState ,
			@ErrorLine;
 END;

    -- Raise an error: msg_str parameter of RAISERROR will contain
    -- the original error information.
  IF @RethrowError = 1
  BEGIN
	DECLARE @e varchar(2000)
	
	-- Build the message string that will contain original
    -- error information.
    SELECT @e = 
        N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();

    RAISERROR 
        (
        @e, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
  END

  end
      

GO
