SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[cppl_RPT_ExceptionScanReportByScanType] 
--DECLARE
@FromDate DATE,
@ToDate DATE,
@Branches varchar(2000),
@ScanTypes varchar(500),
@PrepaidOnly bit,
@ExcludePrefixes varchar(1000)
AS
BEGIN

	SET NOCOUNT ON;

	IF @FromDate IS NULL 
		SET @FromDate = CAST(GETDATE() AS DATE);
	IF @ToDate IS NULL 
		SET @ToDate = CAST(GETDATE() AS DATE);		

	DECLARE @FilterBranches TABLE (FilterBranch varchar(50) PRIMARY KEY);
	DECLARE @FilterScanTypes TABLE (FilterScanType varchar(20) PRIMARY KEY);
	DECLARE @FilterPrefixes TABLE (ExcludePrefix varchar(30) PRIMARY KEY);
	DECLARE @HasBranchFilter bit;
	DECLARE @HasScanTypeFilter bit;
	DECLARE @HasPrefixFilter bit;
	
	SELECT @HasBranchFilter = 0;
	SELECT @HasScanTypeFilter = 0;
	SELECT @HasPrefixFilter = 0;
	SELECT @PrepaidOnly = ISNULL(@PrepaidOnly, 0);

	IF LTRIM(RTRIM(ISNULL(@Branches, ''))) != ''
	BEGIN
		INSERT INTO @FilterBranches 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(s.Data)) FROM
		Pronto.dbo.Split(@Branches, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @FilterBranches)
		BEGIN
			IF EXISTS (SELECT 1 FROM @FilterBranches 
						WHERE FilterBranch = '<ALL>')
			BEGIN
				DELETE FROM @FilterBranches;
				SELECT @HasBranchFilter = 0;
			END
			ELSE
			BEGIN
				SELECT @HasBranchFilter = 1;
			END
		END
		ELSE
		BEGIN
			SELECT @HasBranchFilter = 0;
		END
	END

	IF LTRIM(RTRIM(ISNULL(@ScanTypes, ''))) != ''
	BEGIN
		INSERT INTO @FilterScanTypes 
		SELECT
		  DISTINCT
		    LTRIM(RTRIM(s.Data)) FROM
		Pronto.dbo.Split(@ScanTypes, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @FilterScanTypes)
		BEGIN
			IF EXISTS (SELECT 1 FROM @FilterScanTypes 
						WHERE FilterScanType = '<ALL>')
			BEGIN
				DELETE FROM @FilterScanTypes;
				SELECT @HasScanTypeFilter = 0;
			END
			ELSE
			BEGIN
				SELECT @HasScanTypeFilter = 1;
			END
		END
		ELSE
		BEGIN
			SELECT @HasScanTypeFilter = 0;
		END
	END

	IF LTRIM(RTRIM(ISNULL(@ExcludePrefixes, ''))) != ''
	BEGIN
		INSERT INTO @FilterPrefixes  
		SELECT
		  DISTINCT
		    (LTRIM(RTRIM(s.Data)) + '%') FROM
		Pronto.dbo.Split(@ExcludePrefixes, ',') s
		WHERE LTRIM(RTRIM(ISNULL(s.Data, ''))) != '';
		
		IF EXISTS (SELECT 1 FROM @FilterPrefixes)
		BEGIN
			SELECT @HasPrefixFilter = 1;
		END
		ELSE
		BEGIN
			SELECT @HasPrefixFilter = 0;
		END
	END

	IF @PrepaidOnly = 1
	BEGIN
	
		SELECT DISTINCT 
			--ISNULL(c.ConsignmentNumber, '') AS [ConsignmentNumber], 
			'' AS [ConsignmentNumber],
			l.LabelNumber AS [LabelNumber],
			l.Id AS LabelId, 
			te.DriverId AS ExceptionDriverId,
			te.Id AS [TrackingEventId],
			te.EventDateTime AS [ExceptionScanTime],
			et.[Description] AS [ExceptionScanType],
			LTRIM(RTRIM(ISNULL(te.ExceptionReason, ''))) AS [ExceptionReason],
			b.CosmosBranch AS [Branch],
			d.Code AS [DriverNumber],
			d.[Name] AS [DriverName],
			--ISNULL(cu.[Name], '') AS [CustomerName]
			'' AS [CustomerName]
		FROM 
		(TrackingEvent te (NOLOCK)
			JOIN EventType et (NOLOCK)
				ON te.EventTypeId = et.Id)
		JOIN Label l (NOLOCK)
		ON te.LabelId = l.Id 
		JOIN Driver d (NOLOCK)
		ON te.DriverId = d.Id
		AND d.IsDeleted = 0
		JOIN Branch b (NOLOCK)
		ON d.BranchId = b.Id 
		AND b.IsDeleted = 0
		WHERE 
		CAST(te.EventDateTime AS DATE) BETWEEN @FromDate AND @ToDate 
		AND
			((b.CosmosBranch IN 
				(SELECT FilterBranch FROM @FilterBranches))
			OR
			(@HasBranchFilter = 0))
		AND ((l.LabelNumber NOT LIKE
				(SELECT ExcludePrefix FROM @FilterPrefixes))
			OR
			(@HasPrefixFilter = 0))
		AND (((ISNUMERIC(l.LabelNumber) = 1)
				AND
			 (LEN(LTRIM(RTRIM(l.LabelNumber))) = 11)))
		AND 
			(
				(et.Code IN ('N'))
					OR
				(
					(et.Code NOT IN ('N', 'O', 'P'))
					AND (LTRIM(RTRIM(ISNULL(te.ExceptionReason, ''))) != '')
				)
					OR
				/* 15/02/2011 - Craig Parris
				 *
				 * need to exclude "Out For Delivery" with "In Depot" exception, as this is a 
				 * particular scan which is done by CPPL drivers to record that the freight is 
				 * back in the depot (for handover to another driver), but it is not actually an issue
				 *
				 */
				(
					(et.Code IN ('O', 'P'))
					AND (LTRIM(RTRIM(ISNULL(te.ExceptionReason, ''))) NOT IN ('', 'in depot', 'indepot'))
				)
			)
		AND
			((et.Code IN
				(SELECT FilterScanType FROM @FilterScanTypes))
			OR
			(@HasScanTypeFilter = 0))
	
	END
	ELSE
	BEGIN
	
		SELECT DISTINCT 
			ISNULL(c.ConsignmentNumber, '') AS [ConsignmentNumber], 
			l.LabelNumber AS [LabelNumber],
			l.Id AS LabelId, 
			te.DriverId AS ExceptionDriverId,
			te.Id AS [TrackingEventId],
			te.EventDateTime AS [ExceptionScanTime],
			et.[Description] AS [ExceptionScanType],
			LTRIM(RTRIM(ISNULL(te.ExceptionReason, ''))) AS [ExceptionReason],
			b.CosmosBranch AS [Branch],
			d.Code AS [DriverNumber],
			d.[Name] AS [DriverName],
			ISNULL(cu.[Name], '') AS [CustomerName]
		FROM 
		(TrackingEvent te (NOLOCK)
			JOIN EventType et (NOLOCK)
				ON te.EventTypeId = et.Id)
		JOIN Label l (NOLOCK)
		ON te.LabelId = l.Id 
		JOIN Driver d (NOLOCK)
		ON te.DriverId = d.Id
		AND d.IsDeleted = 0
		JOIN Branch b (NOLOCK)
		ON d.BranchId = b.Id 
		AND b.IsDeleted = 0
		LEFT OUTER JOIN ConsignmentItemLabel cil (NOLOCK)
		ON l.Id = cil.LabelId 
		AND cil.IsDeleted = 0
		LEFT OUTER JOIN ConsignmentItem ci (NOLOCK)
		ON cil.ConsignmentItemId = ci.Id
		AND ci.IsDeleted = 0
		LEFT OUTER JOIN Consignment c (NOLOCK)
		ON ci.ConsignmentId = c.Id 
		AND c.IsDeleted = 0
		LEFT OUTER JOIN Customer cu (NOLOCK)
		ON c.CustomerId = cu.Id 
		AND cu.IsDeleted = 0
		WHERE 
		CAST(te.EventDateTime AS DATE) BETWEEN @FromDate AND @ToDate 
		AND
			((b.CosmosBranch IN 
				(SELECT FilterBranch FROM @FilterBranches))
			OR
			(@HasBranchFilter = 0))
		AND ((l.LabelNumber NOT LIKE
				(SELECT ExcludePrefix FROM @FilterPrefixes))
			OR
			(@HasPrefixFilter = 0))
		AND 
			(
				(et.Code IN ('N'))
					OR
				(
					(et.Code NOT IN ('N', 'O', 'P'))
					AND (LTRIM(RTRIM(ISNULL(te.ExceptionReason, ''))) != '')
				)
					OR
				/* 15/02/2011 - Craig Parris
				 *
				 * need to exclude "Out For Delivery" with "In Depot" exception, as this is a 
				 * particular scan which is done by CPPL drivers to record that the freight is 
				 * back in the depot (for handover to another driver), but it is not actually an issue
				 *
				 */
				(
					(et.Code IN ('O', 'P'))
					AND (LTRIM(RTRIM(ISNULL(te.ExceptionReason, ''))) NOT IN ('', 'in depot', 'indepot'))
				)
			)
		AND
			((et.Code IN
				(SELECT FilterScanType FROM @FilterScanTypes))
			OR
			(@HasScanTypeFilter = 0))
	
	END
	
		/* 15/02/2011 - Craig Parris
		 *
		 * don't exclude any driver types
		 *
		AND d.DriverTypeId IN
			(
				SELECT sc.Id
				FROM SystemCodes sc
				WHERE sc.Discriminator = 'DriverTypeCode'
				AND sc.FriendlyCode IN ('C')
			)
		 */
		--ORDER BY
		--	d.Code ASC,
		--	et.[Description] ASC, 
		--	te.EventDateTime ASC;

	SET NOCOUNT OFF;

END

GO
GRANT EXECUTE
	ON [dbo].[cppl_RPT_ExceptionScanReportByScanType]
	TO [ReportUser]
GO
