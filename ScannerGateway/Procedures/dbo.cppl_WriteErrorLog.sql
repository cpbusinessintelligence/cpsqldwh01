SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[cppl_WriteErrorLog]
(
	@ErrorMessage		nvarchar(4000),
	@ErrorNumber		int = 0,
	@ErrorProcedure		nvarchar(128) = Null,
	@ErrorSeverity		int = 0,
	@ErrorState			int = 0,
	@ErrorLine			int = Null
)
AS
BEGIN


     --'=====================================================================
    --' CP -Stored Procedure -[cppl_WriteErrorLog]
    --' ---------------------------
    --' Purpose: Writes Errors into ErrorLog Table-----
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 17 Sep 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 17/09/2014    AB      1.00    Created the procedure                             --AB20140917

    --'=====================================================================
	

  		INSERT INTO [ErrorLog]
  		WITH (TABLOCK)
			   ([ErrorNumber]
			   ,[ErrorSeverity]
			   ,[ErrorState]
			   ,[ErrorProcedure]
			   ,[ErrorLine]
			   ,[ErrorMessage]
			   , [CreateDate]) 
		VALUES
		(
			@ErrorNumber ,
			@ErrorSeverity ,
			@ErrorState ,
			@ErrorProcedure ,
			@ErrorLine ,
			@ErrorMessage ,
			GETDATE()
		)

END
GO
