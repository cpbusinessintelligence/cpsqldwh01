SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_ArchiveProceedure_BUP20161104] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    Select @ArchiveTrackingEventDate = @ArchiveDate
	

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION trnTrackingEvent_2016_FH
	

INSERT INTO [dbo].TrackingEvent_Archive2016_17
              ([Id],[EventTypeId],[EventDateTime]
				,[Description],[LabelId],[AgentId]
                ,[DriverId],[RunId],[EventLocation]
			    ,[ExceptionReason],[AdditionalText1],[AdditionalText2]
           ,[NumberOfItems]
           ,[SourceId]
           ,[SourceReference]
           ,[CosmosSignatureId]
           ,[CosmosBranchId]
           ,[IsDeleted]
           ,[CreatedDate]
           ,[LastModifiedDate])
     			SELECT [Id]
      ,[EventTypeId]
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]
      ,[AgentId]
      ,[DriverId]
      ,[RunId]
      ,[EventLocation]
      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
      ,[NumberOfItems]
      ,[SourceId]
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
      ,[IsDeleted]
      ,[CreatedDate]
      ,[LastModifiedDate]
  FROM [dbo].[TrackingEvent]

	           WHERE [EventDateTime]<=@ArchiveTrackingEventDate
	
	DELETE FROM [TrackingEvent] WHERE [EventDateTime]<=@ArchiveTrackingEventDate

	COMMIT TRANSACTION trnTrackingEvent_2016_FH

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------

GO
