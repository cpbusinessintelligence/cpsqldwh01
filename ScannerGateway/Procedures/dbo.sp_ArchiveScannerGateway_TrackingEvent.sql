SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ArchiveScannerGateway_TrackingEvent]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @MaxDate datetime
DECLARE @MinDate datetime
DECLARE @Insertquery nvarchar(max)
DECLARE @Deletequery nvarchar(max)
select  @MinDate=min([EventDateTime]) from [dbo].[TrackingEvent]
PRINT @MinDAte
Select  @MaxDate= DATEADD(day, 30,@MinDate) 
PRINT 'MaxDate '  + CAST(@Maxdate  as varchar(20))
select @MaxDate
WHILE @MinDate <= @MaxDate
BEGIN
--SET @InsertQuery = '
	INSERT INTO [dbo].[Trackingevent_Archive]
	SELECT [id]
	  ,[EventTypeId]
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]
      ,[AgentId]
      ,[DriverId]
      ,[RunId]
      ,[EventLocation]
      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
      ,[NumberOfItems]
      ,[SourceId]
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
      ,[IsDeleted]
      ,[CreatedDate]
      ,[LastModifiedDate]
  FROM [ScannerGateway].[dbo].[TrackingEvent] (nolock) where EventDateTime >= @MinDate and  EventDateTime <= DATEADD(day, 1,@MinDate)


	DELETE FROM [ScannerGateway].[dbo].[TrackingEvent] where  EventDateTime >= @MinDate and  EventDateTime <= DATEADD(day, 1,@MinDate)
	 
	 
	-- PRINT 'MinDate '  + CAST(@MinDate as varchar(20))
	
    SET @MinDate = DATEADD(day, 1,@MinDate)
	--PRINT @MinDate
    IF @MinDate >= @MaxDate
        BREAK
   
END
END

GO
