SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE Proc [dbo].[sp_BEIntegrationInsert_SS15112019]
as
Begin
Insert into BEIntegrationStaging(ScanTypeId,Barcode,UserId,ScanDate,LocationType,BranchId,MethodId,ApplicationId,AreaId,isprocessed)
Select distinct
Case when EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509' then -1
     when EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941' then 3010
	 end as ScanTypeId,
	 SourceReference as Barcode,
 Case 
		    when cd_pickup_branch = 5 Then  4285
          when cd_pickup_branch = 4 Then  4206 
		  when cd_pickup_branch = 2 Then  4284
          when cd_pickup_branch = 3 Then  4301
          when cd_pickup_branch = 1 Then  4283
	      when cd_pickup_branch = 6 Then 4215 
		  else 4285 end  
		  as userid,
	CONVERT(VARCHAR(33), eventdatetime, 127)+'+11:00'as Scandate,
	 1 as LocationType,
	 Case 
	      
		  when cd_pickup_branch = 5 Then  176
		  when cd_pickup_branch = 4 Then  169
		  when cd_pickup_branch = 2 Then  175
		  when cd_pickup_branch = 3 Then  13
		  when cd_pickup_branch = 1 Then  174
	      when cd_pickup_branch = 6 Then  172 
		  
		  else 176 end 
		  as BranchID,
	 1 as MethodID,
	 12 as ApplicationId,
	 0 as AreaId,
	 0 as Isprocessed

From TrackingEvent Te
inner join cppledi..cdcoupon cdc
on(Te.SourceReference = cdc.cc_coupon)
inner join cppledi..consignment Co
on(cdc.cc_consignment = co.cd_id)
--Left join [BEApiResponse] BEA
--On(Te.SourceReference = BEA.LabelNo)
Left join BEIntegrationStaging BES
On(TE.SourceReference = BES.Barcode and CONVERT(VARCHAR(33), eventdatetime, 127)+'+11:00' = BES.ScanDate)
where EventTypeId in ('93B2E381-6A89-4F2E-9131-2DC2FB300941','B8D04A85-A65B-41EA-9056-A950BE2CB509')
--and ResponseCode <> 'Accepted' 
and cd_account = '113118624' and BES.Barcode is null and BES.ScanDate is null
end





GO
