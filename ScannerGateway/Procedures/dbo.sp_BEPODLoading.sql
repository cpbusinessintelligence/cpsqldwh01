SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[sp_BEPODLoading]
As
Begin
	Insert Into BEPODIntegration (ManifestNumber,ConnoteNumber,DateSigned,SignedBy,CollectedByUserId,SignatureBase64,Isprocessed)
	Select Distinct 
		0 as ManifestNumber,
		cd_connote as ConnoteNumber,
		CONVERT(VARCHAR(33), eventdatetime, 127)+'+11:00'as DateSigned,
		AdditionalText1 as SignedBy,
		Case     
			When cd_pickup_branch = 5 and cd_pickup_postcode = 2609 Then 4359 
			When cd_pickup_branch = 5 and cd_pickup_postcode = 2285 Then 4371 
			When cd_pickup_branch = 5 and cd_pickup_postcode = 2259 Then 4370  
			When cd_pickup_branch = 5 Then  4285
			When cd_pickup_branch = 4 Then  4206 
			When cd_pickup_branch = 2 Then  4284
			When cd_pickup_branch = 3 Then  4301
			When cd_pickup_branch = 1 Then  4283
			When cd_pickup_branch = 6 Then 4215 
			Else 4285 End as  CollectedByUserId,
		Case 
			When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and additionaltext1 is not null Then 'https://edi.couriersplease.com.au/index.php?fn=signature&id='+convert(varchar(100),cosmossignatureid)+'&coupon='+sourcereference+'&branch='+convert(varchar(100),cosmosbranchid) 
			When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and exceptionreason like '%pop%' then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/Poppoint_1.tif?st=2019-09-26T10%3A01%3A13Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=JSl3vAjACq5%2FteiukVJJ17nI0f6QYoj42CGCcUkwoE4%3D'
			When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and AdditionalText2 like '%DLB%'  then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'
			When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and additionaltext1 is null then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'
		End as SignatureBase64,
		0 as Isprocessed  
	From TrackingEvent (nolock) A
		Inner Join cppledi..cdcoupon (nolock) cdc on(a.SourceReference = cdc.cc_coupon)
		Inner Join cppledi..consignment (nolock) Co on(cdc.cc_consignment = co.cd_id)
		--Left join BEPODIntegration BE On(co.cd_connote = BE.ConnoteNumber)
		Left Join [BEPODApiResponse] (nolock) BEPOD On(Co.cd_connote = BEPOD.ConnoteNo)
	Where 
		eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3')
	and cd_account in ( '113118624'
, '113136659'
, '113136667'
, '113136733'
, '113136758'
, '113136766'
, '113136642'
, '113136709'
, '113136717'
, '113136600'
, '113136683'
, '113136618'
, '113136675'
, '113136691'
, '113136725'
, '113136741')
	and BEPOD.ConnoteNo is null and len(cd_connote)<26 
	--and sourcereference = '00093477800054406113'
End
GO
