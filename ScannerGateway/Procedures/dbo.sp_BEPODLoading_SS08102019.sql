SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE Proc [dbo].[sp_BEPODLoading_SS08102019]
as
Begin
Insert into BEPODIntegration (ManifestNumber,ConnoteNumber,DateSigned,SignedBy,CollectedByUserId,SignatureBase64,Isprocessed)
Select distinct 0 as ManifestNumber,
cd_connote as ConnoteNumber,
CONVERT(VARCHAR(33), eventdatetime, 127)+'+11:00'as DateSigned,
additionalText1 as SignedBy,
Case when cd_pickup_branch = 4 Then  4206
	      when cd_pickup_branch = 6 Then 4215 else 4206 end as  CollectedByUserId,
Case when EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and additionaltext1 is not null Then 'https://edi.couriersplease.com.au/index.php?fn=signature&id='+convert(varchar(100),cosmossignatureid)+'&coupon='+sourcereference+'&branch='+convert(varchar(100),cosmosbranchid) 
when EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and AdditionalText2 like '%DLB%'  then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/Poppoint_1.tif?st=2019-09-26T10%3A01%3A13Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=JSl3vAjACq5%2FteiukVJJ17nI0f6QYoj42CGCcUkwoE4%3D'
when  EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and additionaltext1 is null then
'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'
end
as SignatureBase64,
0 as Isprocessed  
from TrackingEvent A
inner join cppledi..cdcoupon cdc
on(a.SourceReference = cdc.cc_coupon)
inner join cppledi..consignment Co
on(cdc.cc_consignment = co.cd_id)
--Left join BEPODIntegration BE
--On(co.cd_connote = BE.ConnoteNumber)
Left join [BEPODApiResponse] BEPOD
On(Co.cd_connote = BEPOD.ConnoteNo)
where 
 eventtypeid in 
('47CFA05F-3897-4F1F-BDF4-00C6A69152E3'
)
and cd_account = '113118624' 
and BEPOD.ConnoteNo is null
--and sourcereference = '00093477800054406113'
end



GO
