SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[sp_BEPODLoading_SS2409]
as
Begin
Insert into BEPODIntegration (ManifestNumber,ConnoteNumber,DateSigned,SignedBy,CollectedByUserId,SignatureBase64,Isprocessed)
Select distinct 0 as ManifestNumber,
cd_connote as ConnoteNumber,
CONVERT(VARCHAR(33), eventdatetime, 127)+'+10:00'as DateSigned,
additionalText1 as SignedBy,
Case when cd_pickup_branch = 4 Then  4206
	      when cd_pickup_branch = 6 Then 4215 else 4206 end as  CollectedByUserId,
Case when EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' Then 'https://edi.couriersplease.com.au/index.php?fn=signature&id='+convert(varchar(100),cosmossignatureid)+'&coupon='+sourcereference+'&branch='+convert(varchar(100),cosmosbranchid) 
when EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2' then
'ftp://borderxp:XPborder1@ftp.couriersplease.com.au/atlimage/ATL.tiff'
end
as SignatureBase64,
0 as Isprocessed  
from TrackingEvent A
inner join cppledi..cdcoupon cdc
on(a.SourceReference = cdc.cc_coupon)
inner join cppledi..consignment Co
on(cdc.cc_consignment = co.cd_id)
--Left join BEPODIntegration BE
--On(co.cd_connote = BE.ConnoteNumber)
Left join [BEPODApiResponse] BEPOD
On(Co.cd_connote = BEPOD.ConnoteNo)
where
 eventtypeid in 
('47CFA05F-3897-4F1F-BDF4-00C6A69152E3',
'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')
and cd_account = '113118624'
and BEPOD.ConnoteNo is null
end

GO
