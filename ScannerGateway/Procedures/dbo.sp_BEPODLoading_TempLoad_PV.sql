SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sp_BEPODLoading_TempLoad_PV]
AS
Begin
	INSERT Into tbl_TempBEPODLoadToSend_PV
	Select Distinct 
		0 as ManifestNumber,
		cd_connote as ConnoteNumber,
		CONVERT(VARCHAR(33), eventdatetime, 127)+'+11:00'as DateSigned,
		SigneeName as SignedBy,
		Case     
			When cd_pickup_branch = 5 and cd_pickup_postcode = 2609 Then 4359 
			When cd_pickup_branch = 5 and cd_pickup_postcode = 2285 Then 4371 
			When cd_pickup_branch = 5 and cd_pickup_postcode = 2259 Then 4370  
			When cd_pickup_branch = 5 Then  4285
			When cd_pickup_branch = 4 Then  4206 
			When cd_pickup_branch = 2 Then  4284
			When cd_pickup_branch = 3 Then  4301
			When cd_pickup_branch = 1 Then  4283
			When cd_pickup_branch = 6 Then 4215 
			Else 4285 End as  CollectedByUserId,
		Case 
			--When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and additionaltext1 is not null Then 'https://edi.couriersplease.com.au/index.php?fn=signature&id='+convert(varchar(100),cosmossignatureid)+'&coupon='+sourcereference+'&branch='+convert(varchar(100),cosmosbranchid) 
			--When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and exceptionreason like '%pop%' then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/Poppoint_1.tif?st=2019-09-26T10%3A01%3A13Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=JSl3vAjACq5%2FteiukVJJ17nI0f6QYoj42CGCcUkwoE4%3D'
			--When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and AdditionalText2 like '%DLB%'  then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'
			--When EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and additionaltext1 is null then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'
			
			When Attribute is not null and Attribute='Signature' Then 'https://cpprodnowgostorage1.blob.core.windows.net/trackingpod/'+convert(varchar,PODImageID)+'.png?st=2020-06-29T04%3A26%3A25Z&se=2020-06-30T04%3A26%3A25Z&sp=rl&sv=2018-03-28&sr=c&sig=M053IrUv9tVwQpORN2p3azMSIUQ5QKb064VzOdCmcL0%3D'
			When Attribute like '%pop%' then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/Poppoint_1.tif?st=2019-09-26T10%3A01%3A13Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=JSl3vAjACq5%2FteiukVJJ17nI0f6QYoj42CGCcUkwoE4%3D'
			When AttributeValue like '%DLB%'  then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'
			When Attribute like '%ATL%' then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'
			When Attribute is null then 'https://cpprodnowgostorage1.blob.core.windows.net/borderexpress-integration/ATL.tif?st=2019-09-26T10%3A01%3A58Z&se=2039-09-27T10%3A01%3A00Z&sp=rl&sv=2018-03-28&sr=b&sig=p5h%2BAPwxwWHrTXEBYAzQEpWJ3mrBkzDE9Gz8uVdkSwE%3D'

		End as SignatureBase64,
		0 as Isprocessed,
		NULL as Response,
		GETDATE() as CreatedDate,
		NULL as ProcessedDate
	From tbl_TempBEPODLoad_PV A 
		Inner Join cppledi..cdcoupon cdc on(a.LabelNumber = cdc.cc_coupon)
		Inner Join cppledi..consignment Co on(cdc.cc_consignment = co.cd_id)
		Inner Join Branch B on CosmosBranch = A.Branch
	Where 
	cd_account = '113118624' 
	And IsLoadedToBETable IS NULL
	Order by cd_connote;

	Update tbl_TempBEPODLoad_PV Set IsLoadedToBETable =1 Where IsLoadedToBETable Is Null

END
GO
