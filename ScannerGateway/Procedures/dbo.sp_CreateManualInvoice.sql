SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_CreateManualInvoice]
as
begin

  --'=====================================================================
    --' CP -Stored Procedure - sp_CreateManualInvoice
    --' ---------------------------
    --' Purpose: sp_CreateManualInvoice - To create manual Invoice----
    --' Developer: Tejes S
    --' Date: 31/01/2018
    --' Copyright: 2018 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
                    

    --'=====================================================================


SELECT  [LabelNumber]
       ,[Customer]
       ,convert(varchar(100),'') as Status,
        convert(datetime,Null) as PUDateTime,
        convert(varchar(100),'') as PUBranch,
        convert(varchar(100),'') as PUDriver,
        convert(varchar(100),'') as PUETAZone,
        convert(datetime,Null) as OnBoardDateTime,
        convert(varchar(100),'') as OnBoardBranch,
        convert(varchar(100),'') as OnBoardDriver,
        convert(varchar(100),'') as OnBoardETAZone,
        convert(datetime,Null) as AttDelDateTime,
        convert(varchar(100),'') as AttDelBranch,
        convert(varchar(100),'') as AttDelDriver,
        convert(varchar(100),'') as AttDelETAZone,
        convert(varchar(100),'') as RedeliveryCard,
        convert(datetime,Null) as DelDateTime,
        convert(varchar(100),'') as DelBranch,
        convert(varchar(100),'') as DelDriver,
        convert(varchar(100),'') as DelETAZone
  INTO #Temp1
  FROM [ScannerGateway].[dbo].[ManualInvoiceTemp](NOLOCK)


--Drop Table #Temp1
  --Select * from #Temp1 
  -- (Just Remeber to run against Archives)
  
Update #Temp1 SET PUDateTime = E.EventDateTime, PUDriver =D.COde, PUBranch = CASE Convert(Varchar(20),E.CosmosBranchID) WHen 1 THEN 'Adelaide' 
                                                                                  WHEN 2 THEN 'Brisbane' 
                                                                                                                                           WHEN 4 THEN 'Melbourne' 
                                                                                                                                           WHEN 0 Then 'Canberra' 
                                                                                                                                           WHEN 3 THEN 'Gold Coast'
                                                                                                                                           WHEN 6 THEN 'Perth'
                                                                                                                                           WHEN 5 THEN 'Sydney'
                                                                                                                                           ELSE Convert(Varchar(20),E.CosmosBranchID) END
  FROM
    #Temp1 T join    ScannerGateway .dbo.Label L (NOLOCK) on T.LabelNumber=L.LabelNumber
                Join ScannerGateway.dbo.TrackingEvent E (NOLOCK) on L.Id =E.LabelId 
                      left Join ScannerGateway.dbo.Driver  D (NOLOCK) on E.DriverID =D.ID
                     
                      
                      where EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C' --Pickup


Update #Temp1 SET DelDateTime = E.EventDateTime, DelDriver =D.COde, DelBranch = CASE Convert(Varchar(20),E.CosmosBranchID) WHen 1 THEN 'Adelaide' 
                                                                                  WHEN 2 THEN 'Brisbane' 
                                                                                                                                           WHEN 4 THEN 'Melbourne' 
                                                                                                                                           WHEN 0 Then 'Canberra' 
                                                                                                                                           WHEN 3 THEN 'Gold Coast'
                                                                                                                                           WHEN 6 THEN 'Perth'
                                                                                                                                           WHEN 5 THEN 'Sydney'
                                                                                                                                           ELSE Convert(Varchar(20),E.CosmosBranchID) END
FROM #Temp1 T join    ScannerGateway .dbo.Label L (NOLOCK) on T.LabelNumber=L.LabelNumber
                Join ScannerGateway.dbo.TrackingEvent E (NOLOCK) on L.Id =E.LabelId 
                      left Join ScannerGateway.dbo.Driver  D (NOLOCK) on E.DriverID =D.ID
                      where EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'  --Delivery


Update #Temp1 SET AttDelDateTime = E.EventDateTime, AttDelDriver =D.COde, AttDelBranch = CASE Convert(Varchar(20),E.CosmosBranchID) WHen 1 THEN 'Adelaide' 
                                                                                  WHEN 2 THEN 'Brisbane' 
                                                                                                                                           WHEN 4 THEN 'Melbourne' 
                                                                                                                                           WHEN 0 Then 'Canberra' 
                                                                                                                                           WHEN 3 THEN 'Gold Coast'
                                                                                                                                           WHEN 6 THEN 'Perth'
                                                                                                                                           WHEN 5 THEN 'Sydney'
                                                                                                                                           ELSE Convert(Varchar(20),E.CosmosBranchID) END ,RedeliveryCard =E.AdditionalText1
FROM #Temp1 T join    ScannerGateway .dbo.Label L (NOLOCK) on T.LabelNumber=L.LabelNumber
                Join ScannerGateway.dbo.TrackingEvent E (NOLOCK) on L.Id =E.LabelId 
                      left Join ScannerGateway.dbo.Driver  D (NOLOCK) on E.DriverID =D.ID
where EventTypeId = 'FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'   --Attempted Delivery



Update #Temp1 SET OnBoardDateTime = E.EventDateTime, OnBoardDriver =D.COde, OnBoardBranch = CASE Convert(Varchar(20),E.CosmosBranchID) WHen 1 THEN 'Adelaide' 
                                                                                  WHEN 2 THEN 'Brisbane' 
																				  WHEN 4 THEN 'Melbourne' 
																				  WHEN 0 Then 'Canberra' 
																				  WHEN 3 THEN 'Gold Coast'
																				  WHEN 6 THEN 'Perth'                                                                                                                                         WHEN 5 THEN 'Sydney'
																				  ELSE Convert(Varchar(20),E.CosmosBranchID) END
FROM #Temp1 T join    ScannerGateway .dbo.Label L (NOLOCK) on T.LabelNumber=L.LabelNumber
              Join ScannerGateway.dbo.TrackingEvent E (NOLOCK) on L.Id =E.LabelId 
              left Join ScannerGateway.dbo.Driver  D (NOLOCK) on E.DriverID =D.ID  
              where EventTypeId = '93B2E381-6A89-4F2E-9131-2DC2FB300941'  --On Board Scan

Update #Temp1 Set Status =  'Delivered' where Status = '' and DelDateTime is not null
Update #Temp1 Set Status =  'Att Delivery' where Status = '' and AttDelDateTime is not null
Update #Temp1 Set Status =  'On Board' where Status = '' and OnBoardDateTime is not null
Update #Temp1 Set Status =  'PickUp' where Status = '' and PUDateTime is not null


Update #Temp1 SET PUETAZone = D.ETAZone From #Temp1 T join PerformanceReporting.dbo.DimContractor D on T.PUBranch = D.Branch and T.PUDriver=D.DriverNumber
Update #Temp1 SET AttDelETAZone = D.ETAZone From #Temp1 T join PerformanceReporting.dbo.DimContractor D on T.AttDelBranch = D.Branch and T.AttDelDriver=D.DriverNumber
Update #Temp1 SET DelETAZone = D.ETAZone From #Temp1 T join PerformanceReporting.dbo.DimContractor D on T.DelBranch = D.Branch and T.DelDriver=D.DriverNumber
Update #Temp1 SET OnBoardETAZone = D.ETAZone From #Temp1 T join PerformanceReporting.dbo.DimContractor D on T.OnBoardBranch = D.Branch and T.OnBoardDriver=D.DriverNumber



Select  [LabelNumber],
        [Customer],
        [Status],
        [PUDateTime],
	    [PUBranch],
		[PUDriver],
		[PUETAZone],
		[OnBoardDateTime],
		[OnBoardBranch],
		[OnBoardDriver],
		[OnBoardETAZone],
		[AttDelDateTime],
		[AttDelBranch],
		[AttDelDriver],
		[AttDelETAZone],
		[RedeliveryCard],
		[DelDateTime],
		[DelBranch],
		[DelDriver],
		[DelETAZone] from #Temp1


END
GO
