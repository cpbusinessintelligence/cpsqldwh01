SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 12/06/2020
-- Description:	To delete 90 days old data
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteTrustPilotAPIStagingData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---Archive the data
	INSERT INTO TrustPilotAPIStagingData_Archive 
	SELECT * FROM TrustPilotAPIStagingData (NOLOCK) WHERE DATEDIFF(D,CreatedDate,GETDATE()) > 30
	
	-----delete 90 days old data from Load table
	DELETE FROM TrustPilotAPIStagingData WHERE DATEDIFF(D,CreatedDate,GETDATE()) > 30

	-----delete 180 days old data from Archive table
	DELETE FROM TrustPilotAPIStagingData_Archive WHERE DATEDIFF(D,CreatedDate,GETDATE()) > 180

	-----delete API Response table data
	DELETE [dbo].[TrustPilotAPIResponse] WHERE DATEDIFF(D,CreatedDate,GETDATE()) > 90

END
GO
