SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_ExportHubbedItemStatustoHUB] 
AS
BEGIN

	drop table RTRNStag
	drop table RTRNStagCon

	Select * into RTRNStag From [dbo].[HubbedStaging] (nolock) Where trackingnumber like '%rtr%' 
	Select * into RTRNStagCon From RTRNStag Where TrackingNumber in ( Select consignmentcode From ezyfreight.dbo.tblconsignment with(nolock))

	Select 
		[SNo]
		,[TrackingNumber]
		,[ScanTime]
		,[ScanType]
		,[AgentId]
		,[AgentName]
		,[SignedName]
		,[SignaturePoints]
		,[TrackingNumber] as [HubbedCard]
		,'PU Job:'+Jobnumber as  ExceptionReason 
	FROM [dbo].HubbedStaging (nolock)
	Where ScanType='Dropped Off at POPShop'
		And isprocessed=0
	
	Union
	
	Select 
		[SNo]
		,[TrackingNumber]
		,[ScanTime]
		,[ScanType]
		,[AgentId]
		,[AgentName]
		,[SignedName]
		,[SignaturePoints]
		,[HubbedCard]
		,AgentId + ':' + 'POPShop@' + DeliveryChoiceDescription as ExceptionReason 
	FROM [dbo].HubbedStaging (nolock)
		join [EzyTrak Integration].[dbo].[DeliveryChoices] (nolock) on deliverychoiceid=AgentId
	Where ScanType='Accepted by NewsAgent' 
		And isnull(replace(replace(jobnumber,char(13),''),char(10),''),'') ='' 
		And category like '%HUBBED%' 
		And isprocessed=0
	
	Union
	  
	Select 
		[SNo]
		,[HubbedCard] as [TrackingNumber]
		,[ScanTime]
		,[ScanType]
		,[AgentId]
		,[AgentName]
		,[SignedName]
		,[SignaturePoints]
		,[HubbedCard]
		,AgentId + ':' + 'POPShop@' + DeliveryChoiceDescription as ExceptionReason 
	FROM [dbo].HubbedStaging (nolock) 
		Join [EzyTrak Integration].[dbo].[DeliveryChoices] (nolock) on deliverychoiceid=AgentId
	Where ScanType='Accepted by NewsAgent' 
		And isnull(replace(replace(jobnumber,char(13),''),char(10),''),'') ='' 
		And category like '%HUBBED%' 
		And hubbedcard is not null and isprocessed=9
	Union
	 
	Select 
		[SNo]
		,[TrackingNumber]
		,[ScanTime]
		,case when [ScanType] ='DEL' then 'Delivered' else scantype end as [ScanType]
		,[AgentId]
		,[AgentName]
		,[SignedName]
		,[SignaturePoints]
		,[HubbedCard]
		,case when [ScanType] ='DEL' then SignedName else '' end as  ExceptionReason 
	FROM [dbo].HubbedStaging  (nolock)
	Where ScanType not in ('Accepted by NewsAgent' ,'Dropped Off at POPShop','Dropped off at HUBBED') 
		And isprocessed=0
	
	Union
	
	Select 
		[SNo]
		,[TrackingNumber]
		,[ScanTime]
		,[ScanType]
		,[AgentId]
		,[AgentName]
		,[SignedName]
		,[SignaturePoints]
		,[TrackingNumber] as [HubbedCard]
		,case when jobnumber='' then '' else 'Job Number : '+Jobnumber end as  ExceptionReason 
	FROM [dbo].HubbedStaging (nolock)
	Where ScanType='Dropped off at HUBBED' 
		And isprocessed=0

	Union 

	Select 
		[SNo]
		,LabelNumber
		,[ScanTime]
		,  'Dropped off at POPShop '
		--,Case when TrackingNumber = 'cpwgec00000223301'Then 'Dropped off at POPShop' else 'Dropped off at HUBBED' end
		,[AgentId]
		,[AgentName]
		,[SignedName]
		,[SignaturePoints]
		,[TrackingNumber] as [HubbedCard]
		,case when jobnumber='' then '' else 'Job Number - '+Jobnumber end as  ExceptionReason 
	FROM RTRNStagCon a (nolock)
		inner join ezyfreight.dbo.tblconsignment b with(nolock)	on(a.TrackingNumber = b.ConsignmentCode)
		inner join ezyfreight.dbo.tblitemlabel c with(nolock) on(b.ConsignmentID = c.ConsignmentID)
	Where ScanType='Accepted by NewsAgent'   
		And isnull(replace(replace(jobnumber,char(13),''),char(10),''),'') <>'' 
		And trackingnumber like '%rtr%' 
		And a.isprocessed=0

	Union

	Select 
		[SNo]
		,[TrackingNumber]
		,[ScanTime]
		,  'Dropped off at POPShop '
		--,Case when TrackingNumber = 'cpwgec00000223301'Then 'Dropped off at POPShop' else 'Dropped off at HUBBED' end
		,[AgentId]
		,[AgentName]
		,[SignedName]
		,[SignaturePoints]
		,[TrackingNumber] as [HubbedCard]
		,case when jobnumber='' then '' else left([AgentName],29) +', Job:'+convert(varchar(50),Jobnumber) end as  ExceptionReason 
	FROM [dbo].HubbedStaging (nolock) 
	Where ScanType='Accepted by NewsAgent'  
		And isnull(replace(replace(jobnumber,char(13),''),char(10),''),'') <>'' 
		And trackingnumber not in (select trackingnumber from RTRNStagCon (nolock))
		And isprocessed=0

End

GO
GRANT EXECUTE
	ON [dbo].[sp_ExportHubbedItemStatustoHUB]
	TO [SSISUser]
GO
