SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ExportHubbedItemStatustoHUB_TestSinshith] as

begin





select [SNo]

      ,[TrackingNumber]

      ,[ScanTime]

      ,[ScanType]

      ,[AgentId]

      ,[AgentName]

      ,[SignedName]

      ,[SignaturePoints]

      ,[TrackingNumber] as [HubbedCard]

	  ,'PU Job:'+Jobnumber as  ExceptionReason FROM [dbo].HubbedStaging where ScanType='Dropped Off at POPShop'  and isprocessed=0



union





	  select [SNo]

      ,[TrackingNumber]

      ,[ScanTime]

      ,[ScanType]

      ,[AgentId]

      ,[AgentName]

      ,[SignedName]

      ,[SignaturePoints]

      ,[HubbedCard]

	  ,AgentId + ':' + 'POPShop@' + DeliveryChoiceDescription as ExceptionReason FROM [dbo].HubbedStaging  

	  join [EzyTrak Integration].[dbo].[DeliveryChoices] on deliverychoiceid=AgentId

	  where ScanType='Accepted by NewsAgent' and isnull(replace(replace(jobnumber,char(13),''),char(10),''),'') ='' 

	  and category like '%HUBBED%' 

	  and isprocessed=0



union



	  select [SNo]

      ,[HubbedCard] as [TrackingNumber]

      ,[ScanTime]

      ,[ScanType]

      ,[AgentId]

      ,[AgentName]

      ,[SignedName]

      ,[SignaturePoints]

      ,[HubbedCard]

	  ,AgentId + ':' + 'POPShop@' + DeliveryChoiceDescription as ExceptionReason FROM [dbo].HubbedStaging  

	  join [EzyTrak Integration].[dbo].[DeliveryChoices] on deliverychoiceid=AgentId

	  where ScanType='Accepted by NewsAgent' and isnull(replace(replace(jobnumber,char(13),''),char(10),''),'') ='' 

	  and category like '%HUBBED%' 

	  and hubbedcard is not null and isprocessed=9



union



	  select [SNo]

      ,[TrackingNumber]

      ,[ScanTime]

      ,case when [ScanType] ='DEL' then 'Delivered' else scantype end as [ScanType]

      ,[AgentId]

      ,[AgentName]

      ,[SignedName]

      ,[SignaturePoints]

      ,[HubbedCard]

	  ,case when [ScanType] ='DEL' then SignedName else '' end as  ExceptionReason FROM [dbo].HubbedStaging  

	  where ScanType not in ('Accepted by NewsAgent' ,'Dropped Off at POPShop','Dropped off at HUBBED') and isprocessed=0



union



select [SNo]

      ,[TrackingNumber]

      ,[ScanTime]

      ,[ScanType]

      ,[AgentId]

      ,[AgentName]

      ,[SignedName]

      ,[SignaturePoints]

      ,[TrackingNumber] as [HubbedCard]

	  ,case when jobnumber='' then '' else 'Job Number : '+Jobnumber end as  ExceptionReason FROM [dbo].HubbedStaging where ScanType='Dropped off at HUBBED'   and isprocessed=0



union

	  select [SNo]

      ,[TrackingNumber]

      ,[ScanTime]

      ,Case when TrackingNumber = 'cpwgec00000223301' Then 'Dropped off at POPShop' else 'Dropped off at HUBBED' end

      ,[AgentId]

      ,[AgentName]

      ,[SignedName]

      ,[SignaturePoints]

      ,[TrackingNumber] as [HubbedCard]

	  ,case when jobnumber='' then '' else 
	  (Case when TrackingNumber = 'cpwgec00000223301'Then 'Job Number - '+Jobnumber else 'Job Number : '+Jobnumber end)
	  end as  ExceptionReason FROM [dbo].HubbedStaging where ScanType='Accepted by NewsAgent'   and isprocessed=0

	   and isnull(replace(replace(jobnumber,char(13),''),char(10),''),'') <>'' 



end
GO
