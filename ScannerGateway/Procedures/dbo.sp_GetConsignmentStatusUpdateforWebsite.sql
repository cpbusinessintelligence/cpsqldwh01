SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetConsignmentStatusUpdateforWebsite] as
begin

    --'=====================================================================
    --' CP -Stored Procedure - [sp_GetConsignmentStatusUpdateforWebsite]
    --' ---------------------------
    --' Purpose: GetConsignmentStatusUpdateforWebsite-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 09 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 09/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Select ConsignmentCode,LabelNumber,convert(datetime,'') as Maxeventdatetime,convert(varchar(100),'') as LastStatus,convert(varchar(100),'') as Statustobedisplayed,convert(int,0) as StatusId,convert(int,0) as TotalCount,convert(int,0) as DeliveredCount,convert(int,0) as PickupCount,convert(int,0) as InTransitCount
into #temp
from [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c join [cpsqlweb01].[EzyFreight].[dbo].[tblItemLabel] i on c.consignmentid=i.consignmentid
where c.CreatedDateTime>=dateadd("month",-1,getdate())

--sp_help trackingevent

alter table #temp
alter column labelnumber varchar(100) collate Latin1_General_CI_AS

Update #temp set  Maxeventdatetime=(select max(eventdatetime) from scannergateway.dbo.trackingevent t(NOLOCK) where t.sourcereference=#temp.labelnumber)
from scannergateway.dbo.trackingevent t (NOLOCK)  where t.sourcereference=#temp.LabelNumber

update #temp set LastStatus=et.description from scannergateway.dbo.trackingevent t(NOLOCK) join scannergateway.[dbo].[EventType] et on et.id=t.eventtypeid
where eventdatetime=Maxeventdatetime and LabelNumber=sourcereference and eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')

update #temp set LastStatus= et.description from scannergateway.dbo.trackingevent t(NOLOCK) join scannergateway.[dbo].[EventType] et on et.id=t.eventtypeid
where eventdatetime=Maxeventdatetime and LabelNumber=sourcereference and laststatus=''




Select ConsignmentCode,count(*) as TotalCount into #temp1 from #temp
group by ConsignmentCode

Select ConsignmentCode,LastStatus,count(*) as DeliveredCount into #temp2 from #temp
where LastStatus='Delivered'
group by ConsignmentCode,LastStatus



    
update #temp set Totalcount=t1.Totalcount from #temp t join #temp1 t1 on t.ConsignmentCode=t1.ConsignmentCode

update #temp set Deliveredcount=t1.Deliveredcount from #temp t join #temp2 t1 on t.ConsignmentCode=t1.ConsignmentCode


Update #temp set Statustobedisplayed=case when TotalCount-DeliveredCount>0 then 'Consignment Partially Delivered' else 'Consignment Delivered' end
 where deliveredcount>0 


Select ConsignmentCode,count(*) as PickupCount into #temp3
from #temp
where Statustobedisplayed ='' and LastStatus='Pickup'
group by ConsignmentCode

--drop table #temp4
Select ConsignmentCode,count(*) as IntransitCount into #temp4
from #temp
where Statustobedisplayed not in ('Consignment Delivered','Consignment Partially Delivered') and (LastStatus<>'' and LastStatus<>'Pickup')
group by ConsignmentCode

update #temp set Pickupcount=t1.Pickupcount from #temp t join #temp3 t1 on t.ConsignmentCode=t1.ConsignmentCode


update #temp set Intransitcount=t1.Intransitcount from #temp t join #temp4 t1 on t.ConsignmentCode=t1.ConsignmentCode

update #temp set Statustobedisplayed='Consignment in Transit' where statustobedisplayed not in ('Consignment Delivered','Consignment Partially Delivered') and Intransitcount>=1

update #temp set Statustobedisplayed='Consignment Pickup' where statustobedisplayed ='' and Pickupcount>=1

Select * into #temp5 from [cpsqlweb01].[EzyFreight].[dbo].[tblstatus]

alter table #temp5
alter column statusdescription varchar(100) collate Latin1_General_CI_AS

Update #temp set StatusId=s.statusid
from #temp t join #temp5 s on s.statusdescription=t.Statustobedisplayed

--Select * from #temp

--alter table #temp
--alter column consignmentcode varchar(40) collate Latin1_General_CI_AS

Update [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment]  set ConsignmentStatus=T.StatusId from #temp T join [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c on c.consignmentcode=t.consignmentcode where T.StatusId<>0

Update [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment]  set Courierpickupdate=T.Maxeventdatetime from #temp T join [cpsqlweb01].[EzyFreight].[dbo].[tblConsignment] c on c.consignmentcode=t.consignmentcode where T.Statustobedisplayed='Consignment Pickup'


end
GO
