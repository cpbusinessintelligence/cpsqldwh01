SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GetItemsforEzyTrakStatusUpdate](@Account varchar(100)) as
begin

Declare  @Temp table(Connote varchar(100),Label varchar(50),Account varchar(50))



--Insert into @Temp
--select 'CPA0YJT0100201'

Insert into @Temp

Select distinct cd_connote,cc_coupon,cd_account
from cpplEDI.dbo.consignment(NOLOCK) join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_consignment=cd_id 
where cd_account in ('113033518','113019665') and cd_date>=convert(Date,dateadd(day,-21,getdate()))
--where cd_connote in ('TEST0026' )

--('TEST0034',
--'TEST0029',
--'TEST0033',
--'TEST0034',
--'TEST0035',
--'TEST0036',
--'TEST0037',
--'TEST0038')



Declare @Tempc table (Connote varchar(100),count int)

Insert into @Tempc
Select Connote,count(*) as Count 
--into #temp1
from @Temp
group by Connote

--Select * from @Tempc

--Declare  @Temp1 table(Sourcereference varchar(100),Status varchar(100),TrackingDescription varchar(100),StatusDate varchar(100),ReasonCode varchar(20),ReasonDescription varchar(100))

--Insert into @Temp1 values('TEST2222','D4O','Pickup','2016-07-25T10:51:00','',''),
--('TEST2222','D3I','In Depot','2016-07-25T10:52:00','',''),
--('TEST2222','D3O','Transfer','2016-07-25T10:53:00','',''),

Declare  @Temp1 table(Connote varchar(100),Account varchar(50),Sourcereference varchar(100),TrackingDescription varchar(100),Status varchar(100),ExceptionReason varchar(100),StatusDate varchar(100),ReasonCode varchar(20),ReasonDescription varchar(100))



Insert into @Temp1
Select Connote,Account,Sourcereference,e.description,convert(varchar(100),'') as  Status,isnull(t.exceptionreason,'') as ExceptionReason, convert(varchar(20),cast(t.eventdatetime as date))+'T'+convert(varchar(8),cast(t.eventdatetime as Time)) as StatusDate,convert(varchar(20),'') as ReasonCode,convert(varchar(100),'') as ReasonDescription

from @Temp join scannergateway.dbo.trackingevent t(NOLOCK) on sourcereference=Label
           join scannergateway.dbo.eventtype e on e.id=t.eventtypeid 
		 --  join EzyTrakEventMapping et on e.description=et.description
		 order by t.eventdatetime 	
                                                            
Update @Temp1 set Reasoncode=EzyTrakReasoncode from EzyTrakEventMapping e where e.Reasondescription=ExceptionReason 

Update @Temp1 set Status=et.EzyTrakStatus from EzyTrakEventMapping et where  description=TrackingDescription and EzyTrakReasoncode=Reasoncode 

Update @Temp1 set Reasoncode='DBC0',Status='DBC' where TrackingDescription='Delivered'

Update	@Temp1	    set Status='D32' ,  Reasoncode ='D320' where TrackingDescription='Attempteddelivery' and ExceptionReason=''

--Select * from @Temp1

Declare @Temp2 table(Connote varchar(100),Trackingdescription varchar(100),StatusCount int,ItemCount int)

Insert into @Temp2

Select t.connote,TrackingDescription,count(*) as StatusCount,Count
from @Temp1 t join @Tempc t1 on t.connote=t1.connote
group by t.connote,TrackingDescription,Count
having count(*)=Count



--Select  distinct ltrim(rtrim(t.connote)) as Sourcereference,ltrim(rtrim([Status])) as [Status],ltrim(rtrim(t.TrackingDescription)) as StatuscodeDescription,min(ltrim(rtrim(StatusDate))) as StatusDate,ltrim(rtrim(Reasoncode)) as Reasoncode ,ltrim(rtrim(ReasonDescription)) as ReasonDescription,'Couriers Please' as AgentId 
--from @Temp1 t
--join @Temp2 t1 on t.connote=t1.connote and t.TrackingDescription=t1.TrackingDescription
--where ltrim(rtrim(t.connote))+ltrim(rtrim(t.TrackingDescription)) not in (Select Sourcereference+StatuscodeDescription from EzyTrakStatusUpdateRequest)
--group by ltrim(rtrim(t.connote)) ,ltrim(rtrim([Status])),ltrim(rtrim(t.TrackingDescription)) ,ltrim(rtrim(Reasoncode))  ,ltrim(rtrim(ReasonDescription)) 
--order by ltrim(rtrim(t.connote)),min(ltrim(rtrim(StatusDate))) asc


--truncate table EzyTrakStatusUpdateRequest

Insert into  EzyTrakStatusUpdateRequest([Sourcereference]
      ,[Status]
      ,[StatuscodeDescription]
      ,[StatusDate]
      ,[Reasoncode]
      ,[ReasonDescription]
      ,[AgentId]
	  ,isprocessed
	  ,Accountnumber)

Select  distinct ltrim(rtrim(t.connote)) as Sourcereference,ltrim(rtrim([Status])) as [Status],ltrim(rtrim(t.TrackingDescription)) as StatuscodeDescription,min(ltrim(rtrim(StatusDate))) as StatusDate,ltrim(rtrim(Reasoncode)) as Reasoncode ,ltrim(rtrim(ReasonDescription)) as ReasonDescription,'Couriers Please' as AgentId,0,Account
from @Temp1 t
join @Temp2 t1 on t.connote=t1.connote and t.TrackingDescription=t1.TrackingDescription
where ltrim(rtrim(t.connote))+ltrim(rtrim(t.TrackingDescription)) not in (Select Sourcereference+StatuscodeDescription from EzyTrakStatusUpdateRequest)
group by ltrim(rtrim(t.connote)) ,ltrim(rtrim([Status])),ltrim(rtrim(t.TrackingDescription)) ,ltrim(rtrim(Reasoncode))  ,ltrim(rtrim(ReasonDescription)) ,Account
order by ltrim(rtrim(t.connote)),min(ltrim(rtrim(StatusDate))) asc

Select * from EzyTrakStatusUpdateRequest where  isnull(isprocessed,0)=0   order by Sourcereference,statusdate 
--[Sourcereference]='CPA0YJT0100323'



--Select  ltrim(rtrim(Sourcereference)) as Sourcereference,ltrim(rtrim([Status])) as [Status],ltrim(rtrim(TrackingDescription)) as StatuscodeDescription,ltrim(rtrim(StatusDate)) as StatusDate,ltrim(rtrim(Reasoncode)) as Reasoncode ,ltrim(rtrim(ReasonDescription)) as ReasonDescription,'Couriers Please' as AgentId from @Temp1 
----where ltrim(rtrim([Status])) in ('D33') and ltrim(rtrim(Sourcereference)) ='TEST12346' 
--order by Sourcereference asc

--Select  Sourcereference,Status,TrackingDescription as StatuscodeDescription,StatusDate,Reasoncode,ReasonDescription,'Couriers Please' as AgentId from @Temp1 
--where status in ('D3O','D4O') and sourcereference<>'CPA0YJT0100201' 
----not(sourcereference='CPA0YJT0100201' and status in ('D3O','D3I','D31')) 
--order by StatusDate desc

----'pfvwmb9yTstpOHvW+yy3bm1MF6fyAz+T61VvTpCie4fQvlSEN4aqKpHZeQQNFDqC' as AccessToken,

----Select 'ASTEST21' as Sourcereference,'DBC' as Status,'2016-05-17T11:54:05' as StatusDate,'' as Reasoncode,'' as ReasonDescription,'pfvwmb9yTstpOHvW+yy3bm1MF6fyAz+T61VvTpCie4fQvlSEN4aqKpHZeQQNFDqC' as AccessToken,'LHT-API' as AgentId


----Select 'ASTEST21' as Sourcereference,'DBC' as Status,'2016-05-17T11:54:05' as StatusDate,'' as Reasoncode,'' as ReasonDescription,'LHT-API' as AgentId


end
GO
