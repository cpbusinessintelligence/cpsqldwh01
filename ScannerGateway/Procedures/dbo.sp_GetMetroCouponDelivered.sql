SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc sp_GetMetroCouponDelivered
(@StartDate Date,
@EndDate Date
)
As
Begin

--Declare @StartDate Date,
--@EndDate Date
--set @StartDate = '2019-10-01'
--set @EndDate = '2019-10-15'

select  distinct LabelNumber  into #Temp1 from tbl_TrackingEventNowGo where isnumeric(Labelnumber) = 1 and len(LabelNumber) =11 and consignmentNumber is null
and  LabelNumber like '_14%' and convert(date,EventDateTime) between @StartDate and @EndDate

--Select distinct LabelNumber 
--into #Temp2 
--from #Temp1 Trk1
--inner join scannergateway..TrackingEvent TRK2
--on(Trk1.LabelNumber = trk2.SourceReference)
--where AdditionalText1 like '%link%' 

select distinct LabelNumber into #Temp3  from #Temp1 a
inner join trackingevent b
on(a.labelnumber = b.sourcereference)
where eventtypeid in ( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2')


Select LabelNumber into #Temp4 from #Temp1
except
Select * from #Temp3

select distinct Labelnumber,Branch,DriverRunNumber,workflowtype
 from tbl_TrackingEventNowGo where labelnumber in (select LabelNumber from #Temp4)
  order by 1

--select * from #Temp4
End
GO
GRANT EXECUTE
	ON [dbo].[sp_GetMetroCouponDelivered]
	TO [ReportUser]
GO
