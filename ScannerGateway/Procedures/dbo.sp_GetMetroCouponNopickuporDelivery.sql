SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Proc sp_GetMetroCouponNopickuporDelivery
(@StartDate Date,
@EndDate Date
)
As
Begin

select  distinct LabelNumber  into #Temp1 from tbl_TrackingEventNowGo where isnumeric(Labelnumber) = 1 and len(LabelNumber) =11 and consignmentNumber is null
and  LabelNumber like '_14%' and convert(date,EventDateTime) between @StartDate and @EndDate

Select distinct LabelNumber 
into #Temp2 
from #Temp1 Trk1
inner join scannergateway..TrackingEvent TRK2
on(Trk1.LabelNumber = trk2.SourceReference)
where AdditionalText1 like '%link%' 

select distinct LabelNumber into #Temp3  from #Temp2 a
inner join trackingevent b
on(a.labelnumber = b.sourcereference)
where eventtypeid  in ( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2','98EBB899-A15E-4826-8D05-516E744C466C')


select * from #Temp2
except
select * from #Temp3

End
GO
GRANT EXECUTE
	ON [dbo].[sp_GetMetroCouponNopickuporDelivery]
	TO [ReportUser]
GO
