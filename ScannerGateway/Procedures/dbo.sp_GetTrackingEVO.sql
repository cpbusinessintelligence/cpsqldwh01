SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Proc dbo.sp_GetTrackingEVO
As
Begin
Select Labelnumber as TrackingNumber,
'' as Consignmentnumber,
EventDatetime as StatusDateTime,
Case when Outcome = 'failed-not-home-or-closed'  and workflowtype = 'deliver' Then 'Attempted Delivery' else Outcome end  as ScanEvent,
DriverExtRef as Contractor,
'' as Exceptionreason ,
isprocessed 
--into #Temp1
From [dbo].[tbl_TrackingEvent] where EventDatetime>= Dateadd(day, -2, Getdate()) and isprocessed = 0
End
GO
