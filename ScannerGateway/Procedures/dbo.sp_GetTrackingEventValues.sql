SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure sp_GetTrackingEventValues
--'=====================================================================
    --' CP -Stored Procedure - [sp_GetTrackingEventValues]
    --' ---------------------------
    --' Purpose: sp_GetTrackingEventValues-----
    --' Developer: SS (Couriers Please Pty Ltd)
    --' Date: 
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
                            

    --'=====================================================================
As
Begin
select
id, 
TrackingNumber,
[ConsignmentNumber],
[StatusDateTime],
[ScanEvent],
[Contractor],
Ltrim(Rtrim([ExceptionReason])) [ExceptionReason]
 from LoadAgentTrackingEvents where isprocessed = 0
 End
GO
