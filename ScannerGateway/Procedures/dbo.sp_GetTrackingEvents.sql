SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc dbo.sp_GetTrackingEvents
as
Begin
Select 
EventTrackingNumber as TrackingNumber ,
ConsignmentNumber,
StatusDateTime,
ScanEvent,
Contractor,
ExceptionReason,
Processed
 from  [dbo].[tbl_TrackingEvent_Gateway]
 where Processed = 0
 end
GO
