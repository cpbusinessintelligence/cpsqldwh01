SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_GetTrackingEventsforNZPost] as
begin
     --'=====================================================================
    --' CP -Stored Procedure - [sp_GetTrackingEventsforNZPost]
    --' ---------------------------
    --' Purpose: GetTrackingEventsforNZPost-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 22 July 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 22/07/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

declare @Temp table(TrackingNumber varchar(100) collate Latin1_General_CI_AS)
Insert into @Temp
Select l.labelnumber as TrackingNumber 
from [cpsqlweb01].[ezyfreight].[dbo].[tblconsignment] c join [cpsqlweb01].[ezyfreight].[dbo].tblitemlabel l on l.consignmentid=c.consignmentid 
                      left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                      left join [cpsqlweb01].[EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
					  where a1.countrycode='NZ' and consignmentcode like 'CPWSAV%' and convert(date,c.createddatetime)=convert(date,dateadd("dd",-1,getdate()))

--alter table @temp
--alter column TrackingNumber  varchar(100) COLLATE Latin1_General_CI_AS;



declare @Temp1 table(TrackingNumber varchar(100),event_code varchar(50),event_type varchar(50),occured_at varchar(50),long_description varchar(100),short_description varchar(50),action_required_by varchar(50),source_data varchar(50))

Insert into @Temp1
select TrackingNumber,
       e.description as event_code,
      convert(varchar(50),'') as event_type,
      (convert(varchar(4),datepart(year,t.Eventdatetime))+right('00'+convert(varchar(2),datepart(month,t.Eventdatetime)),2)+ right('00'+convert(varchar(2),datepart(day,t.Eventdatetime)),2)+'T'+convert(varchar(20),cast(t.Eventdatetime as Time))+'+11:00') as occurred_at,
      NULL as long_description,
      e.description as short_description,
      NULL as action_required_by,
      '   ' as source_data
--into #temp1
from @temp c join [cpsqldwh01].[ScannerGateway].[dbo].[Trackingevent] t on t.sourcereference=c.TrackingNumber
			 join [cpsqldwh01].[ScannerGateway].[dbo].[Eventtype] e on e.id=t.eventtypeid

declare @x xml
set @x=(select * from @temp1 for XML PATH,root )

declare @x1 varchar(1000)
 
set @x1=(select [DWH].dbo.fn_XmlToJson_Get(@x) )

select @x1 as col1

end


GO
GRANT EXECUTE
	ON [dbo].[sp_GetTrackingEventsforNZPost]
	TO [SSISUser]
GO
