SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_GetUndeliveredNZPostLabels] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_GetUndeliveredNZPostLabels
    --' ---------------------------
    --' Purpose: Get Undelivered NZ Post Labels-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 25 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 25/08/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

Declare  @Temp table(LabelNumber varchar(100))

Insert into @Temp
Select  L.labelnumber from   [EzyFreight].[dbo].[tblConsignment] C      left join  [EzyFreight].[dbo].[tblItemlabel] l on l.consignmentid=c.consignmentid
			                                              left join  [EzyFreight].[dbo].[tblAddress] a on a.addressid=pickupid
                                                          left join [EzyFreight].[dbo].[tblAddress] a1 on a1.addressid=destinationid
											              left JOIN [EzyFreight].[dbo].[tblState] S ON S.StateID=a.StateID
											              left JOIN [EzyFreight].[dbo].[tblState] S1 ON S1.StateID=a1.StateID
														  where c.isinternational=1 and a1.countrycode='NZ' and c.ratecardid like 'SAV%'
														  and labelnumber <>'CPWSAV990000747'
														  order by L.labelnumber COLLATE Latin1_General_CI_AS 



Select  distinct t.* from @Temp t left join [scannergateway].[dbo].[trackingevent](NOLOCK) e on e.sourcereference=LabelNumber
	 where e.eventtypeid <>'47CFA05F-3897-4F1F-BDF4-00C6A69152E3' 
	 order by labelnumber

end
GO
GRANT EXECUTE
	ON [dbo].[sp_GetUndeliveredNZPostLabels]
	TO [SSISUser]
GO
