SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_HubbedCardupdate_20210223_PV] as
BEGIN

	--Select * from HubbedStaging where ReadytoSendMail=1 and  [ScanType] ='Accepted by Newsagent' and trackingnumber like 'CPW%' trackingnumber in ('CPA25VS0000046','CPA25VS0000047','CPA25VS0000049','CPA25VS0000050','CPA25VS0000051','CPA25VS0000052','CPA25VS0000053','CPA25VS0000054')
	--createddatetime>='2016-08-05 08:30:30.533' and isprocessed=1
	
	Update HubbedStaging set hubbedcard=replace(additionaltext1,'Link Coupon ',''),isprocessed=9  
	From trackingevent (nolock)
	Where sourcereference = trackingnumber 
		and eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' 
		and ScanType='Accepted by NewsAgent'
		and (additionaltext1 like 'Link Coupon NHCL%' or additionaltext1 like 'Link Coupon %CNA') 
		and isprocessed=1 
		and hubbedcard is null  

	BEGIN TRAN INSERTMAIL
		-------------Redelivery--------
		--select * from HubbedStaging order by createddatetime desc
		Select  SNo  into #temp from HubbedStaging where ReadytoSendMail=0 and  [ScanType] ='Accepted by Newsagent' and hubbedcard is null and TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock)) and    len (Isnull(Jobnumber,'')) <3
		Select  SNo  into #tempR from HubbedStaging where ReadytoSendMail=0 and  [ScanType] ='Accepted by Newsagent' and hubbedcard is not null and TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock)) and    len (Isnull(Jobnumber,'')) <3
		Select  SNo  into #temp1 from HubbedStaging where ReadytoSendSMS=0 and  [ScanType] ='Accepted by Newsagent' and hubbedcard is null and TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock) ) and    len (Isnull(Jobnumber,'')) <3
		Select  SNo  into #temp1R from HubbedStaging where ReadytoSendSMS=0 and  [ScanType] ='Accepted by Newsagent' and hubbedcard is not null and TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock)) and    len (Isnull(Jobnumber,'')) <3

		Insert into [Redirection].[dbo].[SendMail]
		(
			[ContextID]
			,[FromAddr]
			,[ToAddr]
			,[BCC]
			,[TransmitFlag]
			,[Parameter1]
			,[Parameter2]
			,[Parameter3]
			,[Parameter4]
			,[Parameter5]
			,[Parameter6]
			,[parameter7]
			,[parameter8]
			,[AddWho]
			,[AddDateTime]
			,[ModifiedDateTime]
		)
		Select 
			distinct  8,
			'noreply@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
			'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
			0,
			ls.TrackingNumber,
			HubbedCard,
			AgentName,
			isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
			isnull([cd_delivery_addr0],'')+'<br/>'+isnull([cd_delivery_addr1],'')+'<br/>'+isnull([cd_delivery_addr2],'')+'<br/>'+isnull([cd_delivery_addr3],'')+'<br/>'+isnull([cd_delivery_suburb],'')+','+isnull(case when b1.b_name='Sydney' then 'NSW'
			when b1.b_name='Brisbane' then 'QLD'
			when b1.b_name='Gold Coast' then 'QLD'
			when b1.b_name='Melbourne' then 'VIC'
			when b1.b_name='Perth' then 'WA'
			when b1.b_name='Canberra' then 'NSW'
			when b1.b_name='Adelaide' then 'SA'
			else 'Unknown' end,'') +','+convert(varchar(50),isnull([cd_delivery_postcode],'')),
			[Operation Hours],
			--  'kirsty.tuffley@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
			ls.AccessPin,
			'AK',
			getdate(),
			getdate()
		From #tempR t 
			join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
			join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
			join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
			join cpplEDI.dbo.branchs(NOLOCK) b1 on b1.b_id=cd_deliver_branch
			join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
		Where
			ls.ReadytoSendMail=0 
			and [ScanType] ='Accepted by Newsagent' 
			and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock))
			and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))=1
			and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions]  (nolock) where mailtemplateid=8 and isactive=1)
			and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber not like 'CPW%'
			and convert(Date,ls.ScanTime)>=convert(date,dateadd(day,-1,getdate()))
			--and ls.trackingnumber  ='CPAQORZ0087411001 '

		Insert into [Redirection].[dbo].[SendMail]
		(
			[ContextID]
			,[FromAddr]
			,[ToAddr]
			,[BCC]
			,[TransmitFlag]
			,[Parameter1]
			,[Parameter2]
			,[Parameter3]
			,[Parameter4]
			,[Parameter5]
			,[Parameter6]
			,[parameter7]
			,[parameter8]
			,[AddWho]
			,[AddDateTime]
			,[ModifiedDateTime]
		)
		Select  
			distinct  8,
			'noreply@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](email),
			'CPReporting@couriersplease.com.au,deliverychoices@couriersplease.com.au',
			0,
			ls.TrackingNumber,
			HubbedCard,
			AgentName,
			isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
			isnull(a.CompanyName,'')+'<br/>'+isnull(a.address1,'')+'<br/>'+isnull(a.address2,'')+'<br/>'+isnull(a.suburb,'')+','+isnull(s.statecode,a.statename) +','+convert(varchar(50),isnull(a.postcode,'')),
			[Operation Hours],
			--  'kirsty.tuffley@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](email),
			ls.AccessPin,
			'AK',
			getdate(),
			getdate()
		From #tempR t 
			join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
			join ezyfreight.dbo.tblitemlabel l  (nolock) on l.labelnumber=ls.trackingnumber
			join ezyfreight.dbo.tblconsignment(NOLOCK)  c1 on c1.consignmentid=l.consignmentid
			join ezyfreight.dbo.tbladdress a (nolock) on a.addressid=c1.destinationid
			left join ezyfreight.dbo.tblstate s (nolock) on s.stateid=a.stateid
			join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
			left join ezyfreight.dbo.tblcompanyusers u (nolock) on u.userid=c1.userid
			left join ezyfreight.dbo.tblcompany c2 (nolock) on c2.CompanyID=u.CompanyID
		Where
			ls.ReadytoSendMail=0 
			and [ScanType] ='Accepted by Newsagent' 
			and Hubbedcard is not null 
			and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock))
			and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](email))=1
			and isnull(c2.accountnumber,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions]  (nolock) where mailtemplateid=8 and isactive=1)
			and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber like 'CPW%'
			and convert(Date,ls.ScanTime)>=convert(date,dateadd(day,-1,getdate()))
			--and ls.trackingnumber  ='CPAQORZ0087411001 '
		
		--select * from ezyfreight.dbo.tbladdress

		Insert into [Redirection].[dbo].[SendMail]
		(
			[ContextID]
			,[FromAddr]
			,[ToAddr]
			,[BCC]
			,[TransmitFlag]
			,[Parameter1]
			,[Parameter2]
			,[Parameter3]
			,[Parameter4]
			,[parameter5]
			,[parameter6]
			,[AddWho]
			,[AddDateTime]
			,[ModifiedDateTime]
		)
		Select   
			distinct  9,
			'noreply@couriersplease.com.au',
			Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) +'@preview.pcsms.com.au',
			'deliverychoices@couriersplease.com.au,CPReporting@couriersplease.com.au',
			0,
			ls.TrackingNumber,
			HubbedCard,
			AgentID,
			Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone),
			ls.AccessPin,
			c.Deliverychoicedescription,
			'AK',
			getdate(),
			getdate()
		From #temp1R t 
			join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
			join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
			join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id															  
			join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
		Where 
			ls.ReadytoSendSMS=0 
			and [ScanType] ='Accepted by Newsagent'  
			and Hubbedcard is not null 
			and ls.TrackingNumber in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock))
			and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))<>1 
			and Redirection.[dbo].[fn_ValidatePhone](Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1
			and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] (nolock) where mailtemplateid=9 and isactive=1)
			and ls.ScanTime>='2016-08-19 13:00:00.000'
			and convert(Date,ls.ScanTime)>=convert(date,dateadd(day,-1,getdate()))

	-----------------1st time delivery or Redirection---------------- 
		Insert into [Redirection].[dbo].[SendMail]
		(
			[ContextID]
			,[FromAddr]
			,[ToAddr]
			,[BCC]
			,[TransmitFlag]
			,[Parameter1]
			,[Parameter2]
			,[Parameter3]
			,[Parameter4]
			,[Parameter5]
			,[Parameter6]
			,[parameter7]
			,[parameter8]
			,[AddWho]
			,[AddDateTime]
			,[ModifiedDateTime]
		)
		Select  
			distinct   12,
			'noreply@couriersplease.com.au',
			--  'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
			'deliverychoices@couriersplease.com.au',
			0,
			ls.TrackingNumber,
			HubbedCard,
			AgentName,
			isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
			isnull([cd_delivery_addr0],'')+'<br/>'+isnull([cd_delivery_addr1],'')+'<br/>'+isnull([cd_delivery_addr2],'')+'<br/>'+isnull([cd_delivery_addr3],'')+'<br/>'+isnull([cd_delivery_suburb],'')+','+isnull(case when b1.b_name='Sydney' then 'NSW'
			when b1.b_name='Brisbane' then 'QLD'
			when b1.b_name='Gold Coast' then 'QLD'
			when b1.b_name='Melbourne' then 'VIC'
			when b1.b_name='Perth' then 'WA'
			when b1.b_name='Canberra' then 'NSW'
			when b1.b_name='Adelaide' then 'SA'
			else 'Unknown' end,'') +','+convert(varchar(50),isnull([cd_delivery_postcode],'')),
			[Operation Hours],
			--  'kirsty.tuffley@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email),
			ls.AccessPin,
			'AK',
			getdate(),
			getdate()
		From #temp t 
			join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
			join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
			join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id
			join cpplEDI.dbo.branchs b1 (NOLOCK) on b1.b_id=cd_deliver_branch
			join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
		Where
			ls.ReadytoSendMail=0  
			and [ScanType] ='Accepted by Newsagent' 
			and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock))
			and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](cd_delivery_email))=1
			and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions] (nolock) where mailtemplateid=12 and isactive=1)
			and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber not like 'CPW%'
			--and ls.trackingnumber in ('CPA25VS0000066','CPA25VS0000064','CPA25VS0000062')
			and convert(Date,ls.ScanTime)>=convert(date,dateadd(day,-1,getdate()))

		Insert into [Redirection].[dbo].[SendMail]
		(
			[ContextID]
			,[FromAddr]
			,[ToAddr]
			,BCC
			,[TransmitFlag]
			,[Parameter1]
			,[Parameter2]
			,[Parameter3]
			,[Parameter4]
			,[Parameter5]
			,[Parameter6]
			,[parameter7]
			,[parameter8]
			,[AddWho]
			,[AddDateTime]
			,[ModifiedDateTime]
		)
		Select  
			distinct   12,
			'noreply@couriersplease.com.au',
			--  'kirsty.tuffley@couriersplease.com.au,abhigna.kona@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](email),
			'deliverychoices@couriersplease.com.au',
			0,
			ls.TrackingNumber,
			HubbedCard,
			AgentName,
			isnull(c.Address1,'')+','+isnull(c.Suburb,'')+','+isnull(c.Postcode,'')+','+isnull(c.State,''),
			isnull(a.CompanyName,'')+'<br/>'+isnull(a.address1,'')+'<br/>'+isnull(a.address2,'')+'<br/>'+isnull(a.suburb,'')+','+isnull(s.statecode,a.statename) +','+convert(varchar(50),isnull(a.postcode,'')) ,
			[Operation Hours],
			--  'kirsty.tuffley@couriersplease.com.au',
			Redirection.[dbo].[fn_CleanseEmail](email),
			ls.AccessPin,
			'AK',
			getdate(),
			getdate()
		From #temp t 
			join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
			join ezyfreight.dbo.tblitemlabel l  (nolock) on l.labelnumber=ls.trackingnumber
			join ezyfreight.dbo.tblconsignment(NOLOCK)  c1 on c1.consignmentid=l.consignmentid
			join ezyfreight.dbo.tbladdress  (nolock) a on a.addressid=c1.destinationid
			left join ezyfreight.dbo.tblstate (nolock) s on s.stateid=a.stateid
			join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
			left join ezyfreight.dbo.tblcompanyusers u  (nolock) on u.userid=c1.userid
			left join ezyfreight.dbo.tblcompany c2  (nolock) on c2.CompanyID=u.CompanyID
		Where
			ls.ReadytoSendMail=0 
			and [ScanType] ='Accepted by Newsagent'
			and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock))
			and Redirection.dbo.fn_ValidateEmail(Redirection.[dbo].[fn_CleanseEmail](email))=1
			and isnull(c2.accountnumber,'') not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions]  (nolock) where mailtemplateid=12 and isactive=1)
			and ls.ScanTime>='2016-08-12 06:00:00.000' and ls.TrackingNumber like 'CPW%'
			and convert(Date,ls.ScanTime)>=convert(date,dateadd(day,-1,getdate()))

		Insert into [Redirection].[dbo].[SendMail]
		(
			[ContextID]
			,[FromAddr]
			,[ToAddr]
			,BCC
			,[TransmitFlag]
			,[Parameter1]
			,[Parameter2]
			,[Parameter3]
			,[parameter4]
			,[parameter5]
			,[AddWho]
			,[AddDateTime]
			,[ModifiedDateTime]
		)

		Select  
			distinct 13,
			'noreply@couriersplease.com.au',
			--'0466419484@preview.pcsms.com.au',
			Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone) +'@preview.pcsms.com.au',
			'deliverychoices@couriersplease.com.au',
			0,
			ls.TrackingNumber,
			AgentID,
			Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone),
			ls.AccessPin,
			c.Deliverychoicedescription,
			'AK',
			getdate(),
			getdate()
		From #temp1 t 
			join [ScannerGateway].[dbo].[HubbedStaging] ls(NOLOCK) on t.sno=ls.sno
			join cpplEDI.dbo.cdcoupon(NOLOCK) on cc_coupon=ls.trackingnumber
			join cpplEDI.dbo.consignment(NOLOCK) on cc_consignment=cd_id															  
			join [EzyTrak Integration].[dbo].deliverychoices c(NOLOCK) on c.deliverychoiceid=ls.AgentId
		Where 
			ls.ReadytoSendSMS=0 
			and [ScanType] ='Accepted by Newsagent'
			and ls.TrackingNumber not in (Select Labelnumber from Redirection.dbo.FailedDeliveryCardLeft (nolock))
			and Redirection.[dbo].[fn_ValidatePhone](Redirection.[dbo].[fn_CleansePhonenumber](cd_delivery_contact_phone))=1
			and cd_account not in (Select accountnumber from [Redirection].[dbo].[CustomerExceptions]  (nolock) where mailtemplateid=13 and isactive=1 )
			and ls.ScanTime>='2016-08-19 13:00:00.000'
			and convert(Date,ls.ScanTime)>=convert(date,dateadd(day,-1,getdate()))

		Update  [ScannerGateway].[dbo].[HubbedStaging] set [ReadytoSendMail]=1 where sno in (Select Sno from #temp)
		Update  [ScannerGateway].[dbo].[HubbedStaging] set [ReadytoSendMail]=1 where sno in (Select Sno from #tempR)
		Update  [ScannerGateway].[dbo].[HubbedStaging] set [ReadytoSendSMS]=1 where sno in (Select Sno from #temp1)
		Update  [ScannerGateway].[dbo].[HubbedStaging] set [ReadytoSendSMS]=1 where sno in (Select Sno from #temp1R)

	COMMIT TRAN INSERTMAIL

	Select consignmentcode,c.pickupid,Agentid,JobNumber 
	Into #temp3 
	From [dbo].[HubbedStaging]  (nolock) 
		join cpsqlweb01.ezyfreight.dbo.tblitemlabel l (nolock) on l.labelnumber COLLATE SQL_Latin1_General_CP1_CI_AS=trackingnumber COLLATE SQL_Latin1_General_CP1_CI_AS 
		join cpsqlweb01.ezyfreight.dbo.tblconsignment c (nolock) on c.consignmentid=l.consignmentid
	Where isupdatejob=0 and scantype='Dropped off at POPShop'
	
	Update cpsqlweb01.ezyfreight.dbo.tbladdress set address1=deliverychoicename,address2=d.address1,suburb=d.suburb,PostCode=d.postcode,StateName=d.State,country=d.country
	From #temp3 
		join cpsqlweb01.ezyfreight.dbo.tbladdress  on addressid=pickupid 
		join [EzyTrak Integration].[dbo].deliverychoices  d on agentid=deliverychoiceid
		
	-- update cpsqlweb01.ezyfreight.dbo.tbladdress set address1=deliverychoicename,address2=d.address1,suburb=d.suburb,PostCode=d.postcode,StateName=d.State,country=d.country
	--from #temp3 join cpsqlweb01.ezyfreight.dbo.tbladdress on addressid=pickupid 
	--            join [EzyTrak Integration].[dbo].deliverychoices d on agentid=deliverychoiceid

	Update  [ScannerGateway].[dbo].[HubbedStaging] set isupdatejob=1 where isupdatejob=0 and scantype='Dropped off at POPShop'
End
GO
