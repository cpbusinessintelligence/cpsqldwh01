SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Praveen Valappil
-- Create Date: 2020-08-14
-- Description: To get list of linvalid DriverExtRef
-- =============================================
CREATE PROCEDURE [dbo].[sp_InvlidDriverExtRef_Report]
@FromDate DateTime = NULL,
@ToDate Datetime = NULL
AS
BEGIN
    SET NOCOUNT ON
	
	--Declare @FromDate DateTime = NULL, @ToDate Datetime = NULL

	IF @FromDate Is NULL
		Set @FromDate = DATEADD(DAY, -30, CONVERT(DATE, GETDATE()))
	
	IF @ToDate IS NULL
		Set @ToDate = DATEADD(DAY, 1, CONVERT(DATE, GETDATE()))
	Else
		Set @ToDate = DATEADD(DAY, 1, CONVERT(DATE, @ToDate))

	--Select @FromDate,@ToDate

	Select LabelNumber, WorkflowType, Outcome, DriverExtRef, DriverRunNumber, Branch, EventDateTime 
	From tbl_TrackingEventNowGo(nolock) 
	Where 
		--ISNUMERIC(RIGHT(DriverExtRef,4)) <> 1 and	
		(DriverRunNumber IS NULL Or DriverRunNumber =0 )
		and EventDateTime >= @FromDate and EventDateTime < @ToDate
		and DriverExtRef <> 'delivere-delegate-driver'

--GRANT EXECUTE
--ON OBJECT::[dbo].sp_InvlidDriverExtRef_Report TO [ReportUser]
--AS [dbo];

END
GO
GRANT EXECUTE
	ON [dbo].[sp_InvlidDriverExtRef_Report]
	TO [ReportUser]
GO
