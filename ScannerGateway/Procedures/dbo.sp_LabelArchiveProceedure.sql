SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[sp_LabelArchiveProceedure] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveLabelEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    

Select @ArchiveLabelEventDate = @ArchiveDate

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION trnLabelEvent_2014


INSERT INTO [dbo].[Label_Archive2010-15]
              ([Id],
	           [LabelNumber] ,
	           [CreatedDate] ,
	           [LastModifiedDate])
     			SELECT [Id],
	           [LabelNumber] ,
	           [CreatedDate] ,
	           [LastModifiedDate]
  FROM [dbo].[Label](NOLOCK)
  WHERE [CreatedDate]<=@ArchiveLabelEventDate 


	DELETE FROM [Label] WHERE [CreatedDate]<=@ArchiveLabelEventDate

	COMMIT TRANSACTION trnLabelEvent_2014

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------

GO
