SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_LoadFMSConsignmentAndTrackingDetails] (@Rows int OUTPUT)as
BEGIN
     --'=====================================================================
    --' CP -Stored Procedure - [sp_LoadFMSConsignmentAndTrackingDetails]
    --' ---------------------------
    --' Purpose: LoadFMSConsignmentAndTrackingDetails-----
    --' Developer: Tejes S (Couriers Please Pty Ltd)
    --' Date: 13 December 2017
    --' Copyright: 2017 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 13/12/2017    TS      1.00    Created the procedure                            

    --'=====================================================================

CREATE TABLE #ScanTypes(
               [EventID] uniqueidentifier NULL,
               [Description] [varchar](100) NULL)
               
        -- INSERT INTO  #TempScanners ([ScannerNumber],[Name]) VALUES ('6500','QUERY FREIGHT')
         INSERT INTO  #ScanTypes ([EventID],[Description]) VALUES ('98EBB899-A15E-4826-8D05-516E744C466C','PICKUP SCANNED')
         INSERT INTO  #ScanTypes ([EventID],[Description]) VALUES ('A16CC2A9-519F-4B2D-B8D0-D23A277D9A00','IN TRANSIT')
         INSERT INTO  #ScanTypes ([EventID],[Description]) VALUES ('E11E3D49-8608-4A8D-B45D-0548EAE7B068','PATIAL DELIVERY')
         INSERT INTO  #ScanTypes ([EventID],[Description]) VALUES ('707E0EFE-B4B9-4B85-BE52-6FBAB4CBCE23','DELAYED')
         INSERT INTO  #ScanTypes ([EventID],[Description]) VALUES ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','DELIVERED')
         INSERT INTO  #ScanTypes ([EventID],[Description]) VALUES ('B0E6CBE0-06B0-4DBC-A704-F893423FE53C','REFUSED BY CLIENT')
      CREATE clustered index #SacanTypesidx ON #ScanTypes([EventID])

Declare @Temp table(ConsignmentCode varchar(40),[AccountCode] varchar(40),[DeliveryNote] varchar(100),[Status] varchar(100), [StatusDate] varchar(19),[EstimatedDeliveryDate] varchar(19)
                     ,[DeliveryDate] varchar(19),[POD] varchar(max),BranchID varchar(5),[IsProcessed] bit) 
Insert into @Temp

select [cd_connote] as ConsignmentNumber,
       [cd_account] as AccountCode,
	   s.[Description] as [DeliveryNote],
       s.[Description] as [Status],
       convert(varchar(19),[EventDateTime],121) as StatusDate,
       convert(varchar(19),[cd_eta_date],121) as [EstimatedDeliveryDate],
	   CASE When t.EventTypeId='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' THEN convert(varchar(19),t.EventDateTime,121) 
	   --when e.id='93B2E381-6A89-4F2E-9131-2DC2FB300941'THEN convert(varchar(19),[EventDateTime],121)
	   --when e.id='FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'THEN convert(varchar(19),[EventDateTime],121)
	   --when e.id='23CA300C-420E-4B23-98C7-13DCC97750B4'THEN convert(varchar(19),[EventDateTime],121)
	   --when e.id='A16CC2A9-519F-4B2D-B8D0-D23A277D9A00' and convert(varchar(19),t.EventDateTime,121) > convert(varchar(19),[cd_eta_date],121)
	   --THEN convert(varchar(19),[EventDateTime],121)
	   --	   when e.id='C71FCCF6-779A-4B6A-AE69-1053851DF3D7'THEN convert(varchar(19),[EventDateTime],121)
       ELSE convert(varchar(19),[cd_eta_date],121)
	   END as [DeliveryDate],
	   CASE When t.EventTypeId='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' THEN ISNULL(t.[CosmosSignatureId],'') ELSE '' END as [POD],
	   t.[CosmosBranchId] as BranchID,
	   0 
	   from [CpplEDI].[dbo].[consignment] c (NOLOCK)
       JOIN [CpplEDI].dbo.cdcoupon cd (NOLOCK) ON c.cd_id= cd.cc_consignment 
	   join [cpsqldwh01].[ScannerGateway].[dbo].[Trackingevent] t (NOLOCK) on t.sourcereference=cd.[cc_coupon]
       join #ScanTypes S ON t.EventTypeId=S.EventID
	   --join [cpsqldwh01].[ScannerGateway].[dbo].[Eventtype] e (NOLOCK) on e.id=t.eventtypeid
	   where [cd_Account] IN ('113072045','113024327','113069702') and convert(date,cd_date)>='2017-12-10' 

   IF NOT EXISTS (SELECT * FROM [dbo].[tblFMSConsignments_Staging] F
                   JOIN @Temp T ON F.[AccountCode]=T.AccountCode
                   WHERE F.[ConsignmentNumber]=T.ConsignmentCode and F.[Status]=T.[Status] and F.[StatusDate]=T.[StatusDate])
   BEGIN
       INSERT INTO [dbo].[tblFMSConsignments_Staging]
           ([FMSClient]
           ,[ConsignmentNumber]
           ,[AccountCode]
           ,[DeliveryNote]
           ,[Status]
           ,[StatusDate]
           ,[EstimatedDeliveryDate]
           ,[DeliveryDate]
           ,[POD]
           ,[BranchID]
           ,[Request]
           ,[JSONResponse]
           ,[IsProcessed])
       Select 'AZZW' as [FMSClient]
           ,[ConsignmentCode]
           ,[AccountCode]
           ,[DeliveryNote]
           ,[Status]
           ,[StatusDate]
           ,[EstimatedDeliveryDate]
           ,[DeliveryDate]
           ,[POD]
           ,[BranchID]
           ,'' as [Request]
           ,'' as [JSONResponse]
           ,[IsProcessed] from @Temp

SELECT @Rows=@@ROWCOUNT

   END

END

GO
