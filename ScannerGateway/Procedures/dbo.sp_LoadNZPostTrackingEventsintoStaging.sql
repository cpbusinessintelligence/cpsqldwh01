SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_LoadNZPostTrackingEventsintoStaging](@JsonString varchar(max)) as
begin

     --'=====================================================================
    --' CP -Stored Procedure - sp_LoadNZPostTrackingEventsintoStaging
    --' ---------------------------
    --' Purpose: Load NZPost TrackingEvents into Staging-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 25 Aug 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 25/08/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

--declare @Json varchar(max)
--set @Json='{"parcel":{"uuid":"5ffdab90-abcf-4df4-be3f-4f8689f70a1e","tracking_code":"CPWSAV990000076","events":[{"event_type":"LIFECYCLE","event_code":"SHIPPED","occurred_at":"2015-08-20T08:23:49+12:00","action_required_by":null,"short_description":"Picked up","long_description":"Your item has been collected and is in transit to a depot."},{"event_type":"LIFECYCLE","event_code":"OUT_FOR_DELIVERY","occurred_at":"2015-08-20T08:27:42+12:00","action_required_by":null,"short_description":"Item is out for delivery","long_description":"Item is out for delivery"},{"event_type":"INFO","event_code":"HELD","occurred_at":"2015-08-20T08:43:00+12:00","action_required_by":"RECEIVER","short_description":"Held for clearance","long_description":"Your item is currently being held for clearance."},{"event_type":"LIFECYCLE","event_code":"DELIVERED","occurred_at":"2015-08-20T08:43:28+12:00","action_required_by":null,"short_description":"Delivery complete","long_description":"Your item has been delivered"}]},"success":true}'

--drop table #temp2
--Create table NZPostAPIResponse (Sno int identity(1,1),Consignmentcode varchar(50),EventType varchar(50),EventCode varchar(50),Occuredat varchar(100),actionrequiredby varchar(50),shortdescription varchar(50) ,longdescription varchar(100),isprocessed bit default(0))


declare @Consignmentcode varchar(100)
set @Consignmentcode=substring(@JsonString,charindex('tracking_code:',@JsonString,1)+16,15)
 Select Parent_id,
        Convert(varchar(100),'') as Label,
      -- case when name='tracking_code' then convert(Varchar(50),StringValue) else '' end as Conscode,
       max(case when name='event_type' then convert(Varchar(50),StringValue) else '' end) as event_type,
       max(case when name='event_code' then convert(Varchar(50),StringValue) else '' end) as event_code,
       max(case when name='occurred_at' then convert(Varchar(50),StringValue) else '' end) as occurred_at,
       max(case when name='action_required_by' then convert(Varchar(50),StringValue) else ''  end) as action_required_by,
       max(case when name='short_description' then convert(Varchar(50),StringValue) else '' end) as short_description, 
	   max(case when name='long_description' then convert(Varchar(50),StringValue) else '' end) as long_description
	      into #temp1 
	    from  dbo.parseJSON(@JsonString)
 where ValueType = 'string'
group by parent_ID

Update #temp1 set Label=(Select max(case when name='tracking_code' then convert(Varchar(50),StringValue) else '' end) from  dbo.parseJSON(@JsonString)
 where ValueType = 'string' )


 Insert into NZPostAPIResponse (LabelNumber,EventType,EventCode,Occuredat,actionrequiredby,shortdescription,longdescription)
Select Label,event_type,
      event_code,
      occurred_at,
       action_required_by,
      short_description, 
	 long_description from #temp1 where event_type<>'' and event_code<>''
	 and event_code not in(Select eventcode from NZPostAPIResponse n where Label=LabelNumber)

--select * from NZPostAPIResponse


end

GO
GRANT EXECUTE
	ON [dbo].[sp_LoadNZPostTrackingEventsintoStaging]
	TO [SSISUser]
GO
