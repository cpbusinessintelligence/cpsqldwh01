SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


Create proc [dbo].[sp_LoadRecreatedDataForDebbie] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_LoadRecreatedDataForDebbie]
    --' ---------------------------
    --' Purpose: Load TrackingEvents for Albury-----
    --' Developer: Tejes (Couriers Please Pty Ltd)
    --' Date: 14 Feb 2018
    --' Copyright: 2018 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 14/02/2018    TS     1.00    Created the procedure to load the recreate data for debbie                        

    --'=====================================================================

Select L.LabelNumber,E.EventDateTime, D.COde as DelDriver,CASE Convert(Varchar(20),E.CosmosBranchID)                                      
WHen 1 THEN 'ADELAIDE' 
WHEN 2 THEN 'BRISBANE' 
WHEN 4 THEN 'MELBOURNE' 
WHEN 0 Then 'CANBERRA' 
WHEN 3 THEN 'BRISBANE'
WHEN 6 THEN 'PERTH'
WHEN 5 THEN 'SYDNEY'
ELSE Convert(Varchar(20),E.CosmosBranchID) END as DelBranch,
CASE Convert(Varchar(20),E.CosmosBranchID)                                      
WHen 1 THEN '5000' 
WHEN 2 THEN '4000' 
WHEN 4 THEN '3000' 
WHEN 0 Then '2600' 
WHEN 3 THEN '4000'
WHEN 6 THEN '6000'
WHEN 5 THEN '2000'
ELSE Convert(Varchar(20),E.CosmosBranchID) END as DelPostCode
into #Temp
FROM ScannerGateway .dbo.Label L (NOLOCK)
Join ScannerGateway.[dbo].[TrackingEvent] E (NOLOCK) on L.Id =E.LabelId 
left Join ScannerGateway.dbo.Driver  D on E.DriverID =D.ID
join [dbo].[Recreated Data] R ON L.LabelNumber=R.Label
where EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and EventTypeId <> 'C71FCCF6-779A-4B6A-AE69-1053851DF3D7'




end

GO
