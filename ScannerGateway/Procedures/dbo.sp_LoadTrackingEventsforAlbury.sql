SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[sp_LoadTrackingEventsforAlbury] as
begin

     --'=====================================================================
    --' CP -Stored Procedure - [sp_LoadTrackingEventsforAlbury]]
    --' ---------------------------
    --' Purpose: Load TrackingEvents for Albury-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 12 May 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 12/05/2015    AK      1.00    Created the procedure                            

    --'=====================================================================
Insert into ImportTrackingEvents

Select TrackingNumber
      ,ConsignmentNumber
      ,StatusDateTime
      ,ScanEvent
      ,Contractor
      ,ExceptionReason
      ,1
      ,'Albury'
      ,'AK'
      ,getdate()
      ,'AK'
      ,getdate()
from Load_ImportTrackingEvents_Albury

Truncate table Load_ImportTrackingEvents_Albury


end
GO
GRANT EXECUTE
	ON [dbo].[sp_LoadTrackingEventsforAlbury]
	TO [SSISUser]
GO
