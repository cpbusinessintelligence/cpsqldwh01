SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Praveen Valappil
-- Create date: 14/05/2020
-- Description:	To get list of delivered tracking events to send Trust Pilot
-- =============================================
CREATE PROCEDURE [dbo].[sp_LoadTrustPilotAPIStagingData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

	Insert Into ScannerGateway.dbo.TrustPilotAPIStagingData
	(
		SenderName
		,Consignment
		,DeliveryDate
		,ConsumerName
		,ConsumerEmail
		,DeliveryAddress
		,Postcode
		,Suburb
		,[State]
		,DepotId
		,DeliveryChoice
		,FranchiseName
		,RunNumber
		,SLA
		,IsProcessed
	)
	Select Distinct 
		cd_pickup_addr0	[SenderName]
		, cd_connote [Consignment]
		, TE.EventDateTime [DeliveryDate]
		, cd_delivery_addr0 [ConsumerName]
		, cd_delivery_email [ConsumerEmail] 
		, cd_delivery_addr1 [DeliveryAddress]
		, convert(varchar(20),cd_delivery_postcode) [Postcode]
		, cd_delivery_suburb [Suburb]
		, ST.Code [State]
		, DP.Code [DepotId]
		, TE.ExceptionReason [DeliveryChoice]
		, DR.[Name] [FranchiseName]
		, DR.Code [RunNumber]
		, '' [SLA] --ETAC.ETA [SLA]
		, 0 [IsProcessed]
	From [ScannerGateway].[dbo].[TrackingEvent] (nolock) TE
		Join [CpplEDI].[dbo].[cdcoupon] (nolock) CC On TE.SourceReference = CC.cc_coupon
		Join [CpplEDI].[dbo].[consignment] (nolock) CG On CC.cc_consignment = CG.cd_id
		Join [ScannerGateway].[dbo].[EventType] ET On TE.EventTypeId = ET.Id And ET.Code ='D'
		Left Join [ScannerGateway].[dbo].[Driver] DR On TE.DriverId = DR.Id
		Left Join [ScannerGateway].[dbo].[Depot] DP On DR.DepotId = DP.Id
		Left Join [ScannerGateway].[dbo].[State] ST On DP.StateId = ST.Id
		--Join [CouponCalculator].[dbo].[PostCodes] PC1 On CG.cd_pickup_suburb = PC1.Suburb And CG.cd_pickup_postcode = PC1.PostCode
		--Join [CouponCalculator].[dbo].[PostCodes] PC2 On CG.cd_delivery_suburb = PC2.Suburb And CG.cd_delivery_postcode = PC2.PostCode
		--Join [CouponCalculator].[dbo].[ETACalculator] ETAC On PC1.ETAZone = ETAC.FromZone and PC2.ETAZone = ETAC.ToZone
		Left Join [ScannerGateway].[dbo].[TrustPilotAPIStagingData] TPLD ON (CG.cd_connote = TPLD.Consignment)
	Where ET.Code = 'D' 
		And CONVERT(date,EventDateTime) >= CONVERT(date,GETDATE()-1)
		And ISNULL(cd_account,'') NOT IN (SELECT AccountCode FROM TrustPilotAPIExcludedAccounts WHERE IsActive=1)		
		And ISNULL(cd_account,'') NOT IN (SELECT AccountNumber FROM [Redirection].[dbo].[CustomerExceptions] Where isactive = 1 and MailTemplateid = 15)
		And TPLD.Consignment IS NULL
		--And cd_delivery_email like '%_@__%.__%'
		
	UNION
	
	Select Distinct
		TAP.FirstName+' '+ IsNull(TAP.LastName,'')[SenderName]
		, ConsignmentCode [Consignment]		
		, TE.EventDateTime [DeliveryDate]
		, TAD.FirstName+' '+TAD.LastName [ConsumerName]
		, TAD.Email [ConsumerEmail] 
		, TAD.Address1 [DeliveryAddress]
		, Convert(varchar(10),TAD.PostCode) [Postcode]
		, TAD.Suburb [Suburb]
		, ST.Code [State]
		, DP.Code [DepotId]
		, TE.ExceptionReason [DeliveryChoice]
		, DR.[Name] [FranchiseName]
		, DR.Code [RunNumber]
		, '' [SLA] --ETAC.ETA [SLA]
		, 0 [IsProcessed]
	From [ScannerGateway].[dbo].[TrackingEvent] (nolock) TE
		Join [ScannerGateway].[dbo].[EventType] ET On TE.EventTypeId = ET.Id And ET.Code ='D'
		Join [EzyFreight].[dbo].[tblItemLabel] (nolock) IL On TE.SourceReference = IL.LabelNumber
		Join [EzyFreight].[dbo].[tblConsignment] (nolock) CG On IL.ConsignmentID = CG.ConsignmentID
		Join [EzyFreight].[dbo].[tblAddress] TAP (nolock) On CG.PickupID = TAP.AddressID
		Join [EzyFreight].[dbo].[tblAddress] TAD (nolock) On CG.DestinationID = TAD.AddressID
		Left Join [EzyFreight].[dbo].[tblCompanyUsers] TCU (nolock) On CG.UserID = TCU.UserID 
		Left Join [EzyFreight].[dbo].[tblCompany] TC (nolock) On TCU.CompanyID = TC.CompanyID
		Left Join [ScannerGateway].[dbo].[Driver] DR On TE.DriverId = DR.Id
		Left Join [ScannerGateway].[dbo].[Depot] DP On DR.DepotId = DP.Id
		Left Join [ScannerGateway].[dbo].[State] ST On DP.StateId = ST.Id
		--Join [CouponCalculator].[dbo].[PostCodes] PC1 On TAP.Suburb = PC1.Suburb And TAP.PostCode = PC1.PostCode
		--Join [CouponCalculator].[dbo].[PostCodes] PC2 On TAD.Suburb = PC2.Suburb And TAD.PostCode = PC2.PostCode
		--Join [CouponCalculator].[dbo].[ETACalculator] ETAC On PC1.ETAZone = ETAC.FromZone and PC2.ETAZone = ETAC.ToZone
		Left Join [ScannerGateway].[dbo].[TrustPilotAPIStagingData] TPLD ON (CG.ConsignmentCode = TPLD.Consignment)
	Where ET.Code = 'D' 
		And	CONVERT(date,EventDateTime) >= CONVERT(date,GETDATE()-1)
		And ISNULL(TC.AccountNumber,'') NOT IN (SELECT AccountCode FROM TrustPilotAPIExcludedAccounts WHERE IsActive=1)
		And ISNULL(TC.AccountNumber,'') NOT IN (SELECT AccountNumber FROM [Redirection].[dbo].[CustomerExceptions] Where isactive = 1 and MailTemplateid = 15)
		And TPLD.Consignment IS NULL
		--And ISNUMERIC(TAD.PostCode) = 1
		--And (TAD.PostCode <> 'N91H504' and TAD.PostCode <>'N/A' and TAD.PostCode <> 'WR11 2LS' and TAD.PostCode <> '140-160')
		--And TAD.Email like '%_@__%.__%' 
	
	----Fetching 8000 Records for each Batch. SSIS Script task will update Isprocessed and Responsecode Columns values.
	----Each time the SELECT will exclude already processed data i.e Isprocessed =1 and ResponseCode = 'Accepted'
	Select Distinct Top 8000
		LTRIM(RTRIM([ConsumerName])) [ConsumerName]
		, LTRIM(RTRIM([ConsumerEmail])) [ConsumerEmail]
		, 'CouriersPlease' [SenderName]
		, 'noreply@couriersplease.com.au' [SenderEmail]
		, LTRIM(RTRIM([Consignment])) [Consignment]
		, LTRIM(RTRIM(ISNULL(DeliveryAddress,'') +' '+ ISNULL(Suburb,'') + ' ' + ISNULL([State],'') + ' ' + Convert(varchar, Postcode))) [DeliveryAddress]
		, 'Date-' + CONVERT(VARCHAR(10),[DeliveryDate],120) [DeliveryDate]
		, 'Suburb-' + REPLACE([dbo].[fn_RemoveSpecialCharacter](Suburb),' ','') [Suburb]
		, 'PostCode-' + REPLACE([dbo].[fn_RemoveSpecialCharacter](Postcode),' ','') [Postcode]
		, 'State-' + REPLACE([dbo].[fn_RemoveSpecialCharacter]([State]),' ','') [State]
		, 'DelChoice-' + REPLACE([dbo].[fn_RemoveSpecialCharacter](DeliveryChoice),' ','') [DeliveryChoice]
		, 'RunNumber-' + REPLACE([dbo].[fn_RemoveSpecialCharacter](RunNumber),' ','') [RunNumber]
		, 'SenderName-' + REPLACE(REPLACE([dbo].[fn_RemoveSpecialCharacter]([SenderName]),' ',''),'Intâ€™l','Int') [SenderNameTag]
		, IsProcessed
		, ResponseCode
		, CONVERT(VARCHAR, DATEADD(MINUTE,120,GETUTCDATE()), 120) [SendTime]
	From ScannerGateway.dbo.TrustPilotAPIStagingData NOLOCK	
	Where (IsProcessed = 0 OR ResponseCode <> 'Accepted')
		AND ConsumerEmail like '%_@__%.__%'	
	--Order By ResponseCode

END
GO
