SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc dbo.sp_NowGoEventReconcil (@StartDate Datetime,@EndDate Datetime, @DriverID Varchar(250), @Showall Varchar (200))
As
Begin
SELECT N.[LabelNumber] as LabelNumber
      ,WorkflowType as NowGoScanEvent
          ,Outcome as NowGoOutcome
      ,[DriverExtRef] As NowGoDriverID
      ,[DriverRunNumber] AS NowGoCosmosDriver
      ,[Branch] as NowGoCosmosBranch
       ,CASE [IsProcessed] when 1 THEN 'Yes' else 'No'  end as [IsProcessed]  
      ,[EventDateTime] as NowGoEvent
         ,L.Id as LabelID
         ,L.CreatedDate AS COsmosCreated
          , Convert(datetime,Null) as CosmoseventDateTime
         , Convert(varchar(200),'') as Cosmosevent
  into #Temp
  FROM [ScannerGateway].[dbo].[tbl_TrackingEventNowGo] N  LEft Join ScannerGateway..Label L on N.LabelNumber= L.LabelNumber
  WHere Convert(Date,EventDateTime) between @StartDate and @EndDate and ([DriverExtRef] = @DriverID  or @DriverID is null)

 Select 
 Sourcereference,
 EventDateTime,
 Et.Description 
 into #Temp3
    from ScannerGateway.dbo.TrackingEvent Te
    inner join [dbo].[EventType] Et
    On(Te.EventTypeId =Et.id)

  Update #Temp
  Set CosmoseventDateTime = eventDateTime,Cosmosevent = te.Description
  From #Temp3 te
  Inner join #Temp tem
  On(te.SourceReference = tem.LabelNumber and te.EventDateTime = tem.Nowgoevent)

  if @Showall = 'Missed'
  select * from #Temp  where CosmoseventDateTime is null
  else
  select * from #Temp 

  End
GO
GRANT EXECUTE
	ON [dbo].[sp_NowGoEventReconcil]
	TO [ReportUser]
GO
