SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[sp_NowGoReconcil] (@StartDate Datetime,@EndDate Datetime, @DriverID Varchar(250), @Value Varchar(100)
)
As
Begin

SELECT distinct N.[LabelNumber] as LabelNumber
      ,[DriverExtRef] As NowGoDriverID
      ,[DriverRunNumber] AS NowGoCosmosDriver
      ,[Branch] as NowGoCosmosBranch
       ,CASE [IsProcessed] when 1 THEN 'Yes' else 'No'  end as [IsProcessed]  
      ,Convert(Date,EventDateTime) as NowGoEvent
         ,Convert(Date,L.CreatedDate) AS CosmosCreated

  FROM [ScannerGateway].[dbo].[tbl_TrackingEventNowGo] N left Join ScannerGateway..Label L on N.LabelNumber= L.LabelNumber
  WHere Convert(Date,EventDateTime) between @StartDate and @EndDate and ([DriverExtRef] = @DriverID  or @DriverID is null) 
  and L.LabelNumber  is NULL

  union all

  SELECT distinct N.[LabelNumber] as LabelNumber
      ,[DriverExtRef] As NowGoDriverID
      ,[DriverRunNumber] AS NowGoCosmosDriver
      ,[Branch] as NowGoCosmosBranch
       ,CASE [IsProcessed] when 1 THEN 'Yes' else 'No'  end as [IsProcessed]  
      ,Convert(Date,EventDateTime) as NowGoEvent
         ,Convert(Date,L.CreatedDate) AS CosmosCreated

  FROM [ScannerGateway].[dbo].[tbl_TrackingEventNowGo] N left Join ScannerGateway..Label L on N.LabelNumber= L.LabelNumber
  WHere Convert(Date,EventDateTime) between @StartDate and @EndDate and ([DriverExtRef] = @DriverID  or @DriverID is null) 

  End
GO
GRANT EXECUTE
	ON [dbo].[sp_NowGoReconcil]
	TO [ReportUser]
GO
