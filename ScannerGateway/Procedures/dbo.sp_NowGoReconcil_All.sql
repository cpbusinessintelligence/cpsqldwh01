SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc dbo.sp_NowGoReconcil_All (@StartDate Datetime,@EndDate Datetime, @DriverID Varchar(250)
)
As
Begin

SELECT distinct N.[LabelNumber] as LabelNumber
      ,[DriverExtRef] As NowGoDriverID
      ,[DriverRunNumber] AS NowGoCosmosDriver
      ,[Branch] as NowGoCosmosBranch
       ,CASE [IsProcessed] when 1 THEN 'Yes' else 'No'  end as [IsProcessed]  
        ,Convert(date,[EventDateTime]) as NowGoEvent
        ,Convert(date,L.CreatedDate) AS CosmosCreated

  FROM [ScannerGateway].[dbo].[tbl_TrackingEventNowGo] N LEft Join ScannerGateway..Label L on N.LabelNumber= L.LabelNumber
  WHere Convert(Date,EventDateTime) between @StartDate and @EndDate and ([DriverExtRef] = @DriverID  or @DriverID is null) 
 
  End
GO
