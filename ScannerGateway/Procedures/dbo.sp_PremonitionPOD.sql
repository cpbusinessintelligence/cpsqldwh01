SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



 
 CREATE Procedure [dbo].[sp_PremonitionPOD] 
 As
 Begin

 Declare @LabelNumber varchar(1000)
 Select @LabelNumber = substring(PODName,1,charindex('$',PODName)-1) from PremonitionPOD

 select ConsignmentCode into #temp1 from  [cpsqldwh01].[EzyFreight].[dbo].[tblConsignment] c with (Nolock)
inner  join [cpsqldwh01].[EzyFreight].[dbo].[tblItemLabel] l with (Nolock) on l.consignmentid=c.consignmentid
where LabelNumber = @LabelNumber
union all
Select cd_connote from 
[cpsqldwh01].[cpplEDI].[dbo].[Consignment] c (NOLOCK) 
left join [cpsqldwh01].[cpplEDI].[dbo].[cdcoupon] cd (NOLOCK) on cc_consignment=cd_id
where cd.cc_coupon =@LabelNumber


Select labelnumber into #Temp2 from [cpsqldwh01].[ScannerGateway].[dbo].[Label] 
where labelnumber = @LabelNumber
union all
select @LabelNumber

Select * into #Temp3 from #temp1
union all
Select * from #Temp2  

--select * from #temp1

Select top 1 ConsignmentCode +'.png' from #Temp3 
end


--drop table #temp1


GO
