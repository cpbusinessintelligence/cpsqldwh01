SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


 
 CREATE Procedure [dbo].[sp_PremonitionPOD_SS19092018] 
 As
 Begin

 Declare @LabelNumber varchar(1000)
 Select @LabelNumber = substring(PODName,1,charindex('$',PODName)-1) from PremonitionPOD

 select ConsignmentCode into #temp1 from  [cpsqldwh01].[EzyFreight].[dbo].[tblConsignment] c with (Nolock)
inner  join [cpsqldwh01].[EzyFreight].[dbo].[tblItemLabel] l with (Nolock) on l.consignmentid=c.consignmentid
where LabelNumber = @LabelNumber
union all
Select cd_connote from 
[cpsqldwh01].[cpplEDI].[dbo].[Consignment] c (NOLOCK) 
left join [cpsqldwh01].[cpplEDI].[dbo].[cdcoupon] cd (NOLOCK) on cc_consignment=cd_id
where cd.cc_coupon =@LabelNumber
union all
Select labelnumber from [cpsqldwh01].[ScannerGateway].[dbo].[Label] 
where labelnumber = @LabelNumber
union all
select @LabelNumber

Select top 1 ConsignmentCode +'.png' from #temp1 
end




GO
