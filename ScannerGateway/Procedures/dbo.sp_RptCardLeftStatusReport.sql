SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_RptCardLeftStatusReport(@StartDate date,@EndDate date) as
begin

  --'=====================================================================
    --' CP -Stored Procedure - [[sp_RptCardLeftStatusReport]]
    --' ---------------------------
    --' Purpose: sp_RptCardLeftStatusReport-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 17 Aug 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 17/08/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

select  distinct sourcereference,replace(additionaltext1,'Link Coupon ','') as CardLeft ,convert(Date,eventdatetime) as Date,convert(varchar(100),'') as Accountnumber,convert(varchar(100),'') as AccountName,convert(varchar(100),'') as Branch,case when ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon %RTCNA') then 'Hubbed'
	        when isnull(additionaltext1,'') like 'Link Coupon %SLCNA' then 'POPStation'
			when  ( isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon %DPCNA')  then 'Depot' else '' end as Category
into #temp 
from vw_trackingevent(NOLOCK) where 
(isnull(additionaltext1,'') like 'Link Coupon NH%' or isnull(additionaltext1,'') like 'Link Coupon %CNA' or isnull(additionaltext1,'') like 'Link Coupon 191%' )
and eventtypeid in ('A341A7FC-3E0E-4124-B16E-6569C5080C6D')
and convert(date,eventdatetime)>=@StartDate and convert(date,eventdatetime)<=@EndDate

Insert into #temp(sourcereference,CardLeft,Date,Category)

Select distinct sourcereference,additionaltext1,convert(Date,eventdatetime) as Date,'Hubbed' as Category
from vw_trackingevent(NOLOCK) where 
eventtypeid like '47%E3' and isnull(additionaltext1,'') like 'NHCL%'
--(isnull(additionaltext1,'') like 'Link Coupon NH%' or isnull(additionaltext1,'') like 'Link Coupon %CNA' or isnull(additionaltext1,'') like 'Link Coupon 191%' )
and convert(date,eventdatetime)>=@StartDate and convert(date,eventdatetime)<=@EndDate



update #temp set Accountnumber=cd_account,branch=b.b_name
from #temp join cpplEDI.dbo.cdcoupon on sourcereference=cc_coupon join cpplEDI.dbo.consignment on cc_consignment=cd_id
           join cpplEDI.dbo.branchs b on b.b_id=cd_deliver_branch

update #temp set AccountName=p.shortname
from Pronto.dbo.Prontodebtor p where accountnumber=p.accountcode

Select * from #temp order  by [date]


end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptCardLeftStatusReport]
	TO [ReportUser]
GO
