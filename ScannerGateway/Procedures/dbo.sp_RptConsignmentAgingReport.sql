SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[sp_RptConsignmentAgingReport] 
(
@Branch Varchar(50)
)

As

Begin

 --'=====================================================================
    --' CP -Stored Procedure [sp_RptConsignmentAgingReport] 'Sydney'
    --' ---------------------------
    --' Purpose: Get all coupons linked to NewsAgent and find their last status
    --' Developer: Sinshith
    --' Date: 30 08 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------

    --'=====================================================================

select  
  SourceReference,DriverId,CosmosBranchId,ExceptionReason, convert(varchar(20),'') as BranchName,convert(varchar(20),'') as DriverNumber  into #temp1 
  from [dbo].[TrackingEvent] with(Nolock)
  where EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
  and eventDatetime >= getdate()-1

  Update #Temp1 SET DriverNumber =D.Code  
  From #Temp1 T Join [dbo].[Driver] D 
  on T.DriverId=D.Id Where IsActive =1
  
  Update #Temp1 SET BranchName =CASE CosmosBranchId
  WHEN 0 THEN 'Canberra' 
  WHEN 1 THEN 'Adelaide' 
  WHEN 2 THEN 'Brisbane'  
  WHEN 3 THEN 'Gold Coast'
  WHEN 4 THEN 'Melbourne'
  WHEN 5 THEN  'Sydney'
  WHEN 6 THEN  'Perth'
  WHEN 7 THEN  'Northern Kope'
  ELSE '' END


Select T.SourceReference 
       into #temp2 
  from #Temp1 T 
  join dbo.Label L on T.SourceReference= L.LabelNumber
  join dbo.TrackingEvent E on l.ID =E.LabelId
  Where EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'

Delete #Temp1 
       From #Temp1 T join #Temp2 T2 on 
       T.SourceReference =T2.SourceReference


--Drop table #Temp3
  Select  T.SourceReference,
	   Min(E.EventDatetime) as FirstScanned,
	   Max(E.EventDatetime) as LastScanned,
	   datediff (day,min(E.EventDatetime), Max(E.EventDatetime) ) as #Days,
	   Convert(Varchar(20),'') as DriverNumber,
	   Convert(Varchar(20),'') as BranchNumber,
	    Convert(Varchar(200),'') as Zone
	   into #Temp3
  from #Temp1 T
  join dbo.Label L on T.SourceReference= L.LabelNumber  
  join dbo.TrackingEvent E with(Nolock)
  on L.ID =E.LabelId
	   Where e.EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
	   Group by T.SourceReference
	   having count(*)>1


 Update #Temp3 
 SET DriverNumber = T1.DriverNumber
 , BranchNumber= T1.BranchName
 , Zone = T1.ExceptionReason
 From #Temp3 T3 join #Temp1 T1 on T3.SourceReference=T1.SourceReference
 --where  T1.DriverNumber <> 9407

   Select Distinct C.cd_connote ConsignmentNo,
         T.SourceReference as LabelNo,
		 DriverNumber as DriverNum,
		 FirstScanned,
		 LastScanned,
		 #Days,
		 cd_delivery_addr0 RecieverName,
		 c.cd_delivery_suburb DeliverySuburb,
		 cd_delivery_postcode DeliveryPostcode,
		 BranchNumber as DeliveryBranch,
		   Zone,
		      Case   when E.CosmosBranchId =1 then 'SA'
		             when E.CosmosBranchId =2 then 'QLD'
					 when E.CosmosBranchId =3 then 'QLD'
					 when E.CosmosBranchId =4 then 'VIC'
					 when E.CosmosBranchId =5 then 'NSW'
					 when E.CosmosBranchId =6 then 'WA'
					 when E.CosmosBranchId =7 then 'TAS' 
					 end as State
	from #Temp3 T 
	left join CpplEDI.dbo.cdcoupon cd with(Nolock)
	on T.sourcereference=cd.cc_coupon
    join CpplEDI.dbo.consignment c with(Nolock)
	on cd.cc_consignment= c.cd_id
	left join dbo.Branch E with(Nolock) 
	on E.CosmosBranchId = c.cd_stats_branch
	Where BranchNumber = @Branch
	 --where cd_connote = 'CAJYRE0007355'
	and DriverNumber <> 9407
	order by 6 desc	   

	End				



GO
GRANT CONTROL
	ON [dbo].[sp_RptConsignmentAgingReport]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptConsignmentAgingReport]
	TO [ReportUser]
GO
