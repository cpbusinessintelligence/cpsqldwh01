SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_RptConsignmentAgingReport_SS30082017] 
(
@Branch Varchar(50)
)

As

Begin

 --'=====================================================================
    --' CP -Stored Procedure -[sp_RptConsignmentAgingReport] 'Sydney'
    --' ---------------------------
    --' Purpose: Get all coupons linked to NewsAgent and find their last status
    --' Developer: Sinshith
    --' Date: 30 08 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------

    --'=====================================================================


select  
  SourceReference into #temp1 
  from [dbo].[TrackingEvent] with(Nolock)
  where EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
  and eventDatetime >= getdate()-1

Select T.SourceReference 
       into #temp2 
  from #Temp1 T 
  join dbo.Label L on T.SourceReference= L.LabelNumber
  join dbo.TrackingEvent E on l.ID =E.LabelId
  Where EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'

Delete #Temp1 
       From #Temp1 T join #Temp2 T2 on 
       T.SourceReference =T2.SourceReference

Select  T.SourceReference,
	   Min(E.EventDatetime) as FirstScanned,
	   Max(E.EventDatetime) as LastScanned,
	   datediff (day,min(E.EventDatetime), Max(E.EventDatetime) ) as daysdifference into #Temp3
  from #Temp1 T
  join dbo.Label L on T.SourceReference= L.LabelNumber  
  join dbo.TrackingEvent E with(Nolock)
  on L.ID =E.LabelId
	   Where EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
	   Group by T.SourceReference
	   having count(*)>1
	   order by 2 desc

  Select Distinct C.cd_connote ConsignmentNo,
         T.SourceReference as LabelNo,
		 code DriverNum,
		 c.cd_delivery_suburb DeliverySuburb,
		 cd_delivery_postcode DeliveryPostcode,
		 B.b_name as DeliveryBranch,
		 '' as Zone,
		 FirstScanned,
		 LastScanned,
		 daysdifference
	from #Temp3 T 
    join dbo.TrackingEvent E with(Nolock) on T.SourceReference =E.SourceReference
	join CpplEDI.dbo.cdcoupon cd with(Nolock) on E.sourcereference=cd.cc_coupon
    join CpplEDI.dbo.consignment c with(Nolock)
	on cd.cc_consignment= c.cd_id
	join Driver D with(Nolock) on E.DriverId = D.Id
	join CpplEDI.dbo.Branchs B with(Nolock) on B.b_id = c.cd_stats_branch
	Where EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
	and E.EventDateTime> Getdate()-10 
	and B.b_name = @Branch
	order by 10 desc
	End				

GO
