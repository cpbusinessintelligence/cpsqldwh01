SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[sp_RptConsignmentAgingReport_Updated_SS10092017] 
(
@Branch Varchar(50)
)

As

Begin

 --'=====================================================================
    --' CP -Stored Procedure -[sp_RptConsignmentAgingReport] 'Sydney'
    --' ---------------------------
    --' Purpose: Get all coupons linked to NewsAgent and find their last status
    --' Developer: Sinshith
    --' Date: 30 08 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------

    --'=====================================================================


select  
  SourceReference into #temp1 
  from [dbo].[TrackingEvent] with(Nolock)
  where EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
  and eventDatetime >= getdate()-3

Select T.SourceReference 
       into #temp2 
  from #Temp1 T 
  join dbo.Label L on T.SourceReference= L.LabelNumber
  join dbo.TrackingEvent E on l.ID =E.LabelId
  Where EventTypeId = '47CFA05F-3897-4F1F-BDF4-00C6A69152E3'

Delete #Temp1 
       From #Temp1 T join #Temp2 T2 on 
       T.SourceReference =T2.SourceReference

Select  T.SourceReference,
      e.DriverId   DriverId,
	   Min(E.EventDatetime) as FirstScanned,
	   Max(E.EventDatetime) as LastScanned,
	   datediff (day,min(E.EventDatetime), Max(E.EventDatetime) ) as #Days into #temp6
	   --into #Temp3
  from #Temp1 T
  join dbo.Label L on T.SourceReference= L.LabelNumber  
  join dbo.TrackingEvent E with(Nolock)
  on L.ID =E.LabelId
	   Where e.EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
	   And E.EventDateTime> Getdate()-10 
	   Group by T.SourceReference,e.DriverId,E.EventDatetime
	

   Select SourceReference,DriverId,max(LastScanned) Dates 
   into #temp7 from #temp6
	   --where SourceReference ='CPA8DXT0024276'
   group by SourceReference,DriverId

   update #temp6
   set Driverid = a.DriverId
   from #temp7 a where #temp6.SourceReference =a.SourceReference


  Select  T.SourceReference,
      Max(e.DriverId) DriverId,
	   Min(E.EventDatetime) as FirstScanned,
	   Max(E.EventDatetime) as LastScanned,
	   datediff (day,min(E.EventDatetime), Max(E.EventDatetime) ) as #Days 
	   into #Temp3
  from #Temp6 T
  join dbo.Label L on T.SourceReference= L.LabelNumber  
  join dbo.TrackingEvent E with(Nolock)
  on L.ID =E.LabelId
	   Where e.EventTypeId = 'B8D04A85-A65B-41EA-9056-A950BE2CB509'
	   Group by T.SourceReference
	   having count(*)>1
 
   Select Distinct C.cd_connote ConsignmentNo,
         T.SourceReference as LabelNo,
		 D.code DriverNum,
		 FirstScanned,
		 LastScanned,
		 #Days,
		 cd_delivery_addr0 RecieverName,
		 c.cd_delivery_suburb DeliverySuburb,
		 cd_delivery_postcode DeliveryPostcode,
		 Br.name as DeliveryBranch,
		 '' as Zone,
		      Case   when E.b_id =1 then 'SA'
		             when E.b_id =2 then 'QLD'
					 when E.b_id =3 then 'QLD'
					 when E.b_id =4 then 'VIC'
					 when E.b_id =5 then 'NSW'
					 when E.b_id =6 then 'WA'
					 when E.b_id =7 then 'TAS' 
					 end as State
	from #Temp3 T 
	left join CpplEDI.dbo.cdcoupon cd with(Nolock)
	on T.sourcereference=cd.cc_coupon
    join CpplEDI.dbo.consignment c with(Nolock)
	on cd.cc_consignment= c.cd_id
	left join Driver D with(Nolock) 
	on T.DriverId = D.Id
	left join CpplEDI.dbo.Branchs B with(Nolock) 
	on B.b_id = c.cd_stats_branch
	left join Branch Br with(Nolock) 
	on Br.id = D.BranchId
	left join CpplEDI.dbo.Branchs E with(Nolock) 
	on E.b_id = c.cd_stats_branch
	Where Br.name = @Branch
	And D.code <> 9407
	order by 6 desc	   

	End				


GO
