SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_RptConsignmentBuildOnDataWithNoActivity] (@StartDate DATETIME,@EndDate DATETIME)
AS
	-----------------------------------------------------
	-- Purpose : Generate reports for Consignments build on data with no activity
	-----------------------------------------------------
	-- [sp_RptConsignmentBuildOnDataWithNoActivity] '2017-06-01','2017-06-06'
	--------------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	22/06/2017	SS		-  Created
	-----------------------------------------------------
BEGIN

SELECT   [ConsignmentReference],
         [AccountCode] 
         INTO #temp_Pb
FROM [Pronto].[dbo].[ProntoBilling](NOLOCK) 
WHERE [BillingDate] BETWEEN @StartDate AND @endDate

SELECT   [ConsignmentReference],
         [cc_coupon]
         INTO #Customers_Data
FROM #temp_Pb Pb
		 JOIN CpplEDI.dbo.consignment(NOLOCK) Co 
		 ON (pb.ConsignmentReference = Co.cd_connote)
		 JOIN CpplEDI.dbo.cdcoupon(NOLOCK) Cou
		 ON(Co.cd_id = Cou.cc_consignment)

SELECT [ConsignmentReference],cc_coupon ,LabelNumber 
INTO #final_table 
        FROM #Customers_Data Cd
        LEFT JOIN  [ScannerGateway].dbo.Label(NOLOCK) La
        ON(cd.cc_coupon = La.LabelNumber)



SELECT a.[ConsignmentReference],MAX(a.LabelNumber) ln 
INTO #SELECTION
            FROM #final_table a left join #temp_Pb B 
            ON a.[ConsignmentReference] = b.[ConsignmentReference] 
			GROUP BY a.[ConsignmentReference]
			HAVING MAX(a.LabelNumber) is null

SELECT     Pb.[ConsignmentReference]
		  ,[AccountCode]
		  ,[AccountName]
		  ,[AccountBillToCode]
		  ,[AccountBillToName]
		  ,[ServiceCode]
		  ,[BillingDate]
		  ,[InvoiceNumber]
		  ,[ConsignmentDate]
		  ,[ItemQuantity]
		  ,[RevenueBusinessUnit]
		  ,[RevenueOriginZone]
		  ,[RevenueDestinationZone]
		  ,[BilledFreightCharge]
		  ,[BilledFuelSurcharge]
		  ,[BilledTransportCharge]
		  ,[BilledInsurance]
		  ,[BilledOtherCharge]
		  ,[BilledTotal] 
FROM #SELECTION Ft 
INNER JOIN  
[Pronto].[dbo].[ProntoBilling](NOLOCK) Pb
ON(Ft.[ConsignmentReference] = Pb.[ConsignmentReference])


END
GO
GRANT EXECUTE
	ON [dbo].[sp_RptConsignmentBuildOnDataWithNoActivity]
	TO [ReportUser]
GO
