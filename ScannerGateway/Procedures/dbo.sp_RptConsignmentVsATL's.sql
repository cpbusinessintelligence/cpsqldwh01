SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptConsignmentVsATL's](@FromDate date,@Todate date,@Customername varchar(50)) as
begin
/****** Script for SelectTopNRows command from SSMS  ******/

     --'=====================================================================
    --' CP -Stored Procedure - [sp_RptConsignmentVsATL's]
    --' ---------------------------
    --' Purpose: Consignment Vs ATL's-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 09 Nov 2015
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 09/11/2015    AK      1.00    Created the procedure                            

    --'=====================================================================

SELECT [cd_account]
      ,[cd_connote]
      ,[cd_date]
      ,[cd_customer_eta]
      ,[cd_pickup_addr0]
      ,[cd_pickup_addr1]
      ,[cd_pickup_addr2]
      ,[cd_pickup_addr3]
      ,[cd_pickup_suburb]
      ,[cd_pickup_postcode]
      ,[cd_pickup_contact]
      ,[cd_pickup_contact_phone]
      ,[cd_delivery_addr0]
      ,[cd_delivery_addr1]
      ,[cd_delivery_addr2]
      ,[cd_delivery_addr3]
      ,[cd_delivery_suburb]
      ,[cd_delivery_postcode],
      D.cc_coupon 
	  ,co.c_name
into #Temp1
  FROM [cpplEDI].[dbo].[consignment] C LEft Join  cpplEDI.dbo.cdcoupon D On C.cd_id = D.cc_consignment
  left join [cpplEDI].[dbo].[companyaccount] ca on ca.ca_account=[cd_account]
  left join [cpplEDI].[dbo].[companies] co on co.c_id=ca.ca_company_id
  Where C.cd_date >= @FromDate and C.cd_date <= @Todate
  and co.c_name like '%'+@CustomerName+'%'
  
  
  Select TE.*,L.LabelNumber,ET.Description as pp 
       into #Temp2 from dbo.TrackingEvent TE Join  dbo.Label L on TE.LabelId = L.Id
                                         Join dbo.EventType ET on TE.EventTypeId = ET.ID
  
   Where  L.LabelNumber in (Select distinct cc_coupon from #Temp1) and TE.AdditionalText1 Like '%ATL%'
   
   
     Select TE.*,L.LabelNumber,ET.Description as pp into #Temp4 from dbo.TrackingEvent TE Join  dbo.Label L on TE.LabelId = L.Id
                                         Join dbo.EventType ET on TE.EventTypeId = ET.ID
  
   Where  L.LabelNumber in (Select distinct cc_coupon from #Temp1) and TE.EventTypeId = 'A341A7FC-3E0E-4124-B16E-6569C5080C6D' and TE.AdditionalText1 Like 'Link Coupon [23456789]38%'
   

   
   Select #temp1.*,#Temp2.AdditionalText1 ,#Temp2.EventDateTime  into #tempfinal
        from #temp1 Left Join #Temp2 on  #temp1.cc_coupon =#Temp2.LabelNumber
   
    Update #tempfinal SEt AdditionalText1 =#Temp4.AdditionalText1    From #tempfinal Join #Temp4 On  #tempfinal.cc_coupon = #Temp4.LabelNumber 
   Update #tempfinal SEt EventDateTime =#Temp4.EventDateTime    From #tempfinal Join #Temp4 On  #tempfinal.cc_coupon = #Temp4.LabelNumber 
    
   
   Select * from #TempFinal

--Drop Table #Temp2

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptConsignmentVsATL's]
	TO [ReportUser]
GO
