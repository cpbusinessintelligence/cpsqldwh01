SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[sp_RptConsignmentWithUnprocessedLabels] (@StartDate DATETIME,@EndDate DATETIME,@Account Varchar(500))
AS
	-----------------------------------------------------
	-- Purpose : Generate reports for Consignments with unprocessed labels
	-----------------------------------------------------
	-- [sp_RptConsignmentWithUnprocessedLabels] '2017-06-01','2017-06-30','113024541'
	--------------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	22/06/2017	SS		-  Created
	-----------------------------------------------------
BEGIN

SELECT   [ConsignmentReference],
         [AccountCode] 
         INTO #temp_Pb
FROM [Pronto].[dbo].[ProntoBilling](NOLOCK) 
where AccountCode = @Account
and [BillingDate] BETWEEN @StartDate AND @endDate

SELECT   [ConsignmentReference],
         [cc_coupon]
         INTO #Customers_Data
FROM #temp_Pb Pb
              JOIN CpplEDI.dbo.consignment(NOLOCK) Co 
               ON (pb.ConsignmentReference = Co.cd_connote)
              JOIN CpplEDI.dbo.cdcoupon(NOLOCK) Cou
              ON(Co.cd_id = Cou.cc_consignment)


SELECT [ConsignmentReference],cc_coupon ,LabelNumber 
INTO #final_table 
        FROM #Customers_Data Cd
        LEFT JOIN  [ScannerGateway].dbo.Label(NOLOCK) La
        ON(cd.cc_coupon = La.LabelNumber)
        WHERE La.LabelNumber is null

	Select ConsignmentReference as Connote,
	cc_coupon as LabelNo,
	cd_items as LabelCount,
	cd_last_status as "Status",
	cd_consignment_date as ConsignmentDate,
	cd_delivery_addr0 as Reciever_Name,
	cd_delivery_addr1 as Address1 ,
	cd_delivery_suburb as Suburb,
	cd_delivery_postcode as Postcode into #Grouping
		from #final_table a
		inner join CpplEDI.dbo.consignment(NOLOCK) Cons
		On(a.ConsignmentReference = cons.cd_connote)

			Select connote,LabelCount,count(*) count into #Label
	From #Grouping
	where "Status" = ''
	group by connote,LabelCount
	having LabelCount=count(*)


	update #Grouping 
	Set "Status" = 'No Activity'
	From #Grouping Gr
	join #Label St
	On(Gr.Connote = St.Connote)

	Select * from #Grouping

		END

GO
GRANT EXECUTE
	ON [dbo].[sp_RptConsignmentWithUnprocessedLabels]
	TO [ReportUser]
GO
