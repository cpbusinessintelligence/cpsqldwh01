SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_RptCustomerLabelStatus](@Account varchar(50)) as
begin

  --'=====================================================================
    --' CP -Stored Procedure - [[sp_RptCustomerLabelStatus]]
    --' ---------------------------
    --' Purpose: sp_RptCustomerLabelStatus-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 15 Feb 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 15/02/2017    AK      1.00    Created the procedure                            

    --'=====================================================================

Select cd_connote,cc_coupon,cd_deliver_pod as PODName
into #temp
 from cpplEDI.dbo.consignment join cpplEDI.dbo.cdcoupon on cc_consignment=cd_id 
 where cd_account=
 @Account
 --'112976535' 
 and convert(date,cd_date)>=convert(date,dateadd(day,-30,getdate()))



 Select t.*,e.Description as ActualStatus,convert(varchar(50),'') as MappedStatuscode,t1.eventdatetime as StatusDatetime,isnull(t1.exceptionreason,'') as exceptionreason
 into #temp1
 from #temp t join trackingevent t1 on sourcereference=cc_coupon 
            join eventtype e on t1.eventtypeid=e.id
where convert(date,t1.eventdatetime)=convert(date,getdate())


Update #temp1 set MappedStatuscode=code from CustomerStatusCodemapping where status=ActualStatus and ExceptionInformation=exceptionreason


Insert into CustomerLabelStatus([Consignment]
      ,[Label]
	  ,[PODName]
      ,[ActualStatus]
      ,[MappedStatuscode]
      ,[StatusDatetime]
      ,[ExceptionReason])

select 	t.* from #temp1 t left join CustomerLabelStatus c on Label=cc_coupon and t.[ActualStatus]=c.ActualStatus 
where c.Label is null


Select * from CustomerLabelStatus where isprocessed=0

end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptCustomerLabelStatus]
	TO [ReportUser]
GO
