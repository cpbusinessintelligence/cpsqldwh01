SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptDetailScanReport] as
begin

 --'=====================================================================
    --' CP -Stored Procedure - sp_RptDetailScanReport
    --' ---------------------------
    --' Purpose: sp_RptDetailScanReport-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 12 Apr 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 12/04/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

--Create table WoolworthsStaging (LabelNumber varchar(100),
--                     PUDateTime datetime,PUBranch varchar(100),PUDriver varchar(100),PUETAZone varchar(100),
--					 TransferDateTime datetime,TransferBranch varchar(100),TransferDriver varchar(100),TransferETAZone varchar(100),QueryCageNumber varchar(100),QueryCageDescription varchar(100),
--					 OnBoardDateTime datetime,OnBoardBranch varchar(100),OnBoardDriver varchar(100),OnBoardETAZone varchar(100),
--					 HandOverDateTime datetime,HandOverBranch varchar(100),HandOverDriver varchar(100),HandOverETAZone varchar(100),TransferredtoAgent varchar(100),
--					 AttDelDateTime datetime,AttDelBranch varchar(100),AttDelDriver varchar(100),AttDelETAZone varchar(100),RedeliveryCard varchar(100),
--					 DelDateTime datetime,DelBranch varchar(100),DelDriver varchar(100),DelETAZone varchar(100),[CreatedBy] [varchar](100) NULL DEFAULT ('Admin'),
--	[CreatedDateTime] [datetime] NULL DE(FAULT (getdate()),
--	[EditedBy] [varchar](100) NULL DEFAULT ('Admin'),
--	[EditedDateTime] [datetime] NULL DEFAULT (getdate()))
   
   
   Truncate table WoolworthsStaging                  

Insert into WoolworthsStaging([LabelNumber]
      ,[PUDateTime]
      ,[PUBranch]
      ,[PUDriver]
      ,[PUETAZone]
      ,[TransferDateTime]
      ,[TransferBranch]
      ,[TransferDriver]
      ,[TransferETAZone]
      ,[QueryCageNumber]
      ,[QueryCageDescription]
      ,[OnBoardDateTime]
      ,[OnBoardBranch]
      ,[OnBoardDriver]
      ,[OnBoardETAZone]
      ,[HandOverDateTime]
      ,[HandOverBranch]
      ,[HandOverDriver]
      ,[HandOverETAZone]
      ,[TransferredtoAgent]
      ,[AttDelDateTime]
      ,[AttDelBranch]
      ,[AttDelDriver]
      ,[AttDelETAZone]
      ,[RedeliveryCard]
      ,[DelDateTime]
      ,[DelBranch]
      ,[DelDriver]
      ,[DelETAZone])
Select  distinct Label as LabelNumber,
               --RevenueRecogniseddate,
			   convert(varchar(100),NULL) as PUDateTime,
			   convert(varchar(100),'') as PUBranch,
			   convert(varchar(100),'') as PUDriver,
			   convert(varchar(100),'') as PUETAZone,
			   convert(varchar(100),NULL) as TransferDateTime,
			   convert(varchar(100),'') as TransferBranch,
			   convert(varchar(100),'') as TransferDriver,
			   convert(varchar(100),'') as TransferETAZone,
			   convert(varchar(100),'') as QueryCageNumber,
			   convert(varchar(100),'') as QueryCageDescription,
			   convert(varchar(100),NULL) as OnBoardDateTime,
			   convert(varchar(100),'') as OnBoardBranch,
			   convert(varchar(100),'') as OnBoardDriver,
			   convert(varchar(100),'') as OnBoardETAZone,
			   convert(varchar(100),NULL) as HandOverDateTime,
			   convert(varchar(100),'') as HandOverBranch,
			   convert(varchar(100),'') as HandOverDriver,
			   convert(varchar(100),'') as HandOverETAZone,
			   convert(varchar(100),'') as TransferredtoAgent,
			   convert(varchar(100),NULL) as AttDelDateTime,
			   convert(varchar(100),'') as AttDelBranch,
			   convert(varchar(100),'') as AttDelDriver,
			   convert(varchar(100),'') as AttDelETAZone,
			   convert(varchar(100),'') as RedeliveryCard,
			    convert(varchar(100),NULL) as DelDateTime,
			   convert(varchar(100),'') as DelBranch,
			   convert(varchar(100),'') as DelDriver,
			   convert(varchar(100),'') as DelETAZone
from [dbo].[WoolWorthsReportSatchelRange]  

 --drop table #temp

 --Select * from trackingevent where sourcereference='13502308821 '


 Update WoolworthsStaging set PUDateTime=eventdatetime,
			      PUBranch=b.name,
			      PUDriver=d.code,
			      PUETAZone=b1.etazone
from WoolworthsStaging join scannergateway.dbo.trackingevent (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join scannergateway.dbo.driver d (NOLOCK) on d.id=driverid
		   join branch b (NOLOCK) on b.id=d.branchid
		   left join cosmos.dbo.driver b1 (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='98EBB899-A15E-4826-8D05-516E744C466C'


Update WoolworthsStaging set TransferDateTime=eventdatetime,
			      TransferBranch=b.name,
			      TransferDriver=d.code,
			      TransferETAZone=b1.etazone,
				  QueryCageNumber=
				  --additionaltext1,
				  case when ltrim(rtrim(additionaltext1)) in ('6501','6502','6503','6504','6505','6506','6507','6508','6509','6510','6511') then additionaltext1 else '' end,
				  QueryCageDescription=case QueryCageNumber when '6501' then'SHORT/SPLIT CONSIGNMENT'
				                                            when '6502' then'INCORRECT/INSUFFICIENT ADDRESS'
															when '6503' then'DAMAGED'
															when '6504' then'NO FREIGHT'
															when '6505' then'CLOSED'
															when '6506' then'CONNOTE REQUIRED'
															when '6507' then'CARD LEFT'
															when '6508' then'DG PAPERWORK REQUIRED'
															when '6509' then'REFUSED'
															when '6510' then'NO ACCESS TO LEAVE CARD'
															when '6511' then'UNSAFE TO LEAVE'
															else '' end
from WoolworthsStaging join scannergateway.dbo.trackingevent  (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join scannergateway.dbo.driver d (NOLOCK) on d.id=driverid
		   join branch b (NOLOCK) on b.id=d.branchid
		   left join cosmos.dbo.driver b1 (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='E293FFDE-76E3-4E69-BCEB-473F91B4350C'


Update WoolworthsStaging set OnBoardDateTime=eventdatetime,
			      OnBoardBranch=b.name,
			      OnBoardDriver=d.code,
			      OnBoardETAZone=b1.etazone
from WoolworthsStaging join scannergateway.dbo.trackingevent (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join scannergateway.dbo.driver d (NOLOCK) on d.id=driverid
		   join branch b (NOLOCK) on b.id=d.branchid
		   left join cosmos.dbo.driver b1 (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='93B2E381-6A89-4F2E-9131-2DC2FB300941'



Update WoolworthsStaging set HandOverDateTime=eventdatetime,
			     HandOverBranch=b.name,
			     HandOverDriver=d.code,
			     HandOverETAZone=b1.etazone,
				 TransferredtoAgent=isnull(Additionaltext2,'')
from WoolworthsStaging join scannergateway.dbo.trackingevent (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join scannergateway.dbo.driver d (NOLOCK) on d.id=driverid
		   join branch b  (NOLOCK) on b.id=d.branchid
		   left join cosmos.dbo.driver b1 (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='1A35AB45-B82A-492A-A1E8-6415BF846C75'



Update WoolworthsStaging set AttDelDateTime=eventdatetime,
			     AttDelBranch=b.name,
			     AttDelDriver=d.code,
			     AttDelETAZone=b1.etazone
				-- TransferredtoAgent=isnull(Additionaltext2,'')
from WoolworthsStaging join scannergateway.dbo.trackingevent  (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join scannergateway.dbo.driver d (NOLOCK) on d.id=driverid
		   join branch b (NOLOCK) on b.id=d.branchid
		   left join cosmos.dbo.driver b1 (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='FCFC0FB1-C46C-43C7-92B7-3143BA7173D2'


Update WoolworthsStaging set RedeliveryCard=replace(additionaltext1,'Link Coupon ','') 
from  WoolworthsStaging join scannergateway.dbo.trackingevent (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
where eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and (additionaltext1 like 'Link Coupon 191%' or additionaltext1 like 'Link Coupon NH%')


Update WoolworthsStaging set DelDateTime=eventdatetime,
			     DelBranch=b.name,
			     DelDriver=d.code,
			     DelETAZone=b1.etazone
				-- TransferredtoAgent=isnull(Additionaltext2,'')
from WoolworthsStaging join scannergateway.dbo.trackingevent (NOLOCK) on ltrim(rtrim(sourcereference))=ltrim(rtrim(labelnumber))
           join scannergateway.dbo.driver d (NOLOCK) on d.id=driverid
		   join branch b (NOLOCK) on b.id=d.branchid
		   left join cosmos.dbo.driver b1 (NOLOCK) on b1.branch=case when b.name='Gold Coast' then 'GoldCoast' else b.name end and d.code=b1.drivernumber
where eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3'


--Select [LabelNumber]
--      ,[PUDateTime]
--      --,CASE WHEN CONVERT(DATE, [PUDateTime]) = '1900-01-01' THEN '' ELSE CONVERT(CHAR(10), [PUDateTime], 120)+ ' ' + CONVERT(CHAR(8), [PUDateTime], 108)END as [PUDateTime]
--      ,[PUBranch]
--      ,[PUDriver]
--      ,isnull([PUETAZone],'') as [PUETAZone]
--	  ,[TransferDateTime]
--      --,CASE WHEN CONVERT(DATE, [TransferDateTime]) = '1900-01-01' THEN '' ELSE CONVERT(CHAR(10), [TransferDateTime], 120) + ' ' + CONVERT(CHAR(8), [TransferDateTime], 108) END as [TransferDateTime] 
--      ,[TransferBranch]
--      ,[TransferDriver]
--      ,isnull([TransferETAZone],'') as [TransferETAZone]
--      ,[QueryCageNumber]
--      ,[QueryCageDescription]
--	  ,[OnBoardDateTime]
--      --,CASE WHEN CONVERT(DATE, [OnBoardDateTime]) = '1900-01-01' THEN '' ELSE CONVERT(CHAR(10), [OnBoardDateTime], 120)+ ' ' + CONVERT(CHAR(8), [OnBoardDateTime], 108)END as [OnBoardDateTime]
--      ,[OnBoardBranch]
--      ,[OnBoardDriver]
--      ,isnull([OnBoardETAZone],'') as [OnBoardETAZone]
--	  ,[HandOverDateTime]
--     -- ,CASE WHEN CONVERT(DATE, [HandOverDateTime]) = '1900-01-01' THEN '' ELSE CONVERT(CHAR(10), [HandOverDateTime], 120)+ ' ' + CONVERT(CHAR(8), [HandOverDateTime], 108)END  as [HandOverDateTime]
--      ,[HandOverBranch]
--      ,[HandOverDriver]
--      ,isnull([HandOverETAZone],'') as [HandOverETAZone]
--      ,[TransferredtoAgent]
--	  ,[AttDelDateTime]
--     -- ,CASE WHEN CONVERT(DATE, [AttDelDateTime]) = '1900-01-01' THEN '' ELSE CONVERT(CHAR(10), [AttDelDateTime], 120)+ ' ' + CONVERT(CHAR(8), [AttDelDateTime], 108)END  as   [AttDelDateTime]
--      ,[AttDelBranch]
--      ,[AttDelDriver]
--      ,isnull([AttDelETAZone],'') as [AttDelETAZone]
--      ,[RedeliveryCard]
--	  ,[DelDateTime]
--      --,CASE WHEN CONVERT(DATE, [DelDateTime]) = '1900-01-01' THEN '' ELSE CONVERT(CHAR(10), [DelDateTime], 120)+ ' ' + CONVERT(CHAR(8), [DelDateTime], 108)END  as  [DelDateTime]
--      ,[DelBranch]
--      ,[DelDriver]
--      ,isnull([DelETAZone],'') as [DelETAZone] from WoolworthsStaging where (PUDateTime is not null and PUDateTime<>'1900-01-01 00:00:00.000') or 
--                           (TransferDateTime is not null and TransferDateTime<>'1900-01-01 00:00:00.000') or
--						   (OnBoardDateTime is not null and OnBoardDateTime<>'1900-01-01 00:00:00.000') or
--						   (HandOverDateTime is not null and HandOverDateTime<>'1900-01-01 00:00:00.000') or
--						   (AttDelDateTime is not null and AttDelDateTime<>'1900-01-01 00:00:00.000') or
--						   (DelDateTime is not null and DelDateTime<>'1900-01-01 00:00:00.000')

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDetailScanReport]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptDetailScanReport]
	TO [SSISUser]
GO
