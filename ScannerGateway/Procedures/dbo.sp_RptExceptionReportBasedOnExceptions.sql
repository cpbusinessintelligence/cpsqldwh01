SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[sp_RptExceptionReportBasedOnExceptions] 
(
@AccountCode Varchar(50),@Exceptionname varchar(max),@StartDate date, @EndDate date
)

As

Begin

 SELECT        ConNote, Label, Reference, ConNoteDate, QueryCage, Name, CustomerAccount, CustomerName, ParentCustomerAccount, DeliveryName, DateScanned, ScannedBranch, SendingBranch, LinkCardNumber, 
                         LinkCardDate, createddate, DeliveryContactPhone, DeliveryEmail
FROM            FreightExceptionTable WITH (NOLOCK)
WHERE        (CustomerAccount = @AccountCode) AND (Name IN
                             (SELECT        Value
                               FROM            dbo.SplitMultivaluedString(@ExceptionName, ',') AS SplitMultivaluedString_1)) AND (CONVERT(date, createddate) BETWEEN @StartDate AND @EndDate)
ORDER BY ConNoteDate

	End				



GO
GRANT EXECUTE
	ON [dbo].[sp_RptExceptionReportBasedOnExceptions]
	TO [ReportUser]
GO
