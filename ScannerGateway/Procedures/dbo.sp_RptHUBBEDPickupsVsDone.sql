SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Heena Bajaj>
-- Create date: <Create Date,,>
-- Description:	Script for SelectTopNRows command from SSMS
--Updated by HB on 5/03/2021
-- =============================================
CREATE PROCEDURE [dbo].[sp_RptHUBBEDPickupsVsDone]
	--@StartDate DATETIME,
	--@EndDate DATETIME
AS
BEGIN
	
	SET NOCOUNT ON;
	 
SELECT [TrackingNumber]
      ,[ScanTime]
      ,[ScanType]
      ,[AgentId] 
      ,[AgentName]
	  ,Case [isprocessed] When 0 Then ' Scan not proceesed' else 'Scan  proceesed' End as IsProcessed
	  ,[AccessPin]
      ,[Jobnumber]
	    ,Dc.Latitude [Latitude]
	  ,dc.Longtitude [Longtitude]
	  ,cd_connote
	  ,com.c_name [supplier]
	  ,cd_deliver_driver,cd_delivery_contact
	,cd_delivery_contact_phone
	,cd_pickup_driver
	,cd_delivery_email

	,cd_last_stamp
	,case when ce_key is null then 'N' else 'Y' end as [Drop off Status]
	  ,convert(datetime,Null) as [PickupAt] 
	  ,convert(datetime,Null) as [DeliveredAt]
	  ,convert(datetime,Null) as [DeliveredbyPopstationAt]
	   ,convert(datetime,Null) as [Droppoff_HUBBEDAt]
	    ,convert(datetime,Null) as [DropoffinPOPStationAt]
		 ,convert(datetime,Null) as [DroppedOffatPOPShopAt]
		  ,convert(datetime,Null) as [ExpiredFromPOPStationAt]
		    ,convert(datetime,Null) as [PODDeliveredAt]
			,convert(datetime,null) as [ReturntoCourierAt]
			,convert(datetime,null) as [TransferscanAt]
			,convert(datetime,null) as [IndepotAt]
	  ,DATEDIFF(day,[ScanTime],getdate()) as Age
	  ,DC.[State] as [State]
	   ,Category
	  ,[Operation Hours]
	  ,concat(DC.Address1,'',DC.Address2,'',Dc.Address3) as [Address]
	  ,Suburb
	  ,PostCode
	  ,p.LastScanType
	  ,p.AttemptedDeliveryCardNumber

  INTO #Temp1
  FROM [ScannerGateway].[dbo].[HubbedStaging] (Nolock) HS
  join  CpplEDI.dbo.cdcoupon (nolock) cc on HS.TrackingNumber=cc.cc_coupon
  join CpplEDI.dbo.consignment (nolock) c on cd_id=cc_consignment
  left join CpplEDI.[dbo].[companies] (nolock) com on com.c_id=c.cd_company_id

   left join [CouponCalculator].[dbo].[DeliveryChoices] DC
  ON rtrim(HS.[AgentId])= rtrim(DC.[DeliveryChoiceID])
  left join CpplEDI.[dbo].[cdextra] cd on cd.ce_consignment=c.cd_id
  left join performancereporting.dbo.primarylabels p on p.LabelNumber=cc.cc_coupon
 --where TrackingNumber='CPAFXLT12205007'
 -- WHERE Rtrim(Ltrim(isnull(Jobnumber,''))) <> '' AND len (Jobnumber) >3 
  --AND HS.CreatedDatetime > = @StartDate AND Hs.CreatedDatetime < = @EndDate
 
 --SELECT *
 --  FROM #Temp1 T 
 --  JOIN ScannerGateway.dbo.Label L 
 --  ON T.[TrackingNumber] = L.LabelNumber
 --     JOIN ScannerGateway.dbo.TrackingEvent E ON L.Id =  E.LabelId
 --     WHERE EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'


 update #temp1 set PODDeliveredAt=H.ScanTime from #temp1 T JOIN [ScannerGateway].[dbo].[HubbedStaging] H on T.TrackingNumber=H.TrackingNumber where H.ScanType='DEL'

  UPDATE #temp1 SET PickupAt =E.EventDateTime 
   FROM #Temp1 T  JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'

	    UPDATE #temp1 SET PickupAt =EA.EventDateTime
   FROM #Temp1 T  JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE PickupAt is null and EventTypeId = '98EBB899-A15E-4826-8D05-516E744C466C'


  UPDATE #temp1 SET DeliveredAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId in ( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3','41A8F8F9-D57E-40F0-9D9D-97767AC3069E')

	   UPDATE #temp1 SET DeliveredAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE DeliveredAt is null and EventTypeId in ( '47CFA05F-3897-4F1F-BDF4-00C6A69152E3','41A8F8F9-D57E-40F0-9D9D-97767AC3069E')


  UPDATE #temp1 SET DeliveredbyPopstationAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = '49242882-32FA-46E6-B222-1DFB2372354F'

	UPDATE #temp1 SET DeliveredbyPopstationAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE DeliveredbyPopstationAt is null and EventTypeId = '49242882-32FA-46E6-B222-1DFB2372354F'



  UPDATE #temp1 SET Droppoff_HUBBEDAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = 'E15A66BC-E701-4C9B-A8BE-9EF063F63D52'

	     UPDATE #temp1 SET Droppoff_HUBBEDAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE Droppoff_HUBBEDAt is null and EventTypeId = 'E15A66BC-E701-4C9B-A8BE-9EF063F63D52'


	  UPDATE #temp1 SET DropoffinPOPStationAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = 'C79189AA-ED10-4437-A257-F3ECAC28F6BE'

	   UPDATE #temp1 SET DropoffinPOPStationAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE DropoffinPOPStationAt is null and EventTypeId = 'C79189AA-ED10-4437-A257-F3ECAC28F6BE'


	  UPDATE #temp1 SET DroppedOffatPOPShopAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
   WHERE EventTypeId = 'D00ED2B8-690A-4841-83BA-2B0E419D65F0'

   
	  UPDATE #temp1 SET DroppedOffatPOPShopAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
   WHERE DroppedOffatPOPShopAt is null and EventTypeId = 'D00ED2B8-690A-4841-83BA-2B0E419D65F0'

	  UPDATE #temp1 SET ExpiredFromPOPStationAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = '12A864E4-FF1F-4421-9451-CA5BA1AA431E'
  
    UPDATE #temp1 SET ExpiredFromPOPStationAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE ExpiredFromPOPStationAt is null and EventTypeId = '12A864E4-FF1F-4421-9451-CA5BA1AA431E'

	     UPDATE #temp1 SET ReturntoCourierAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = 'B0E6CBE0-06B0-4DBC-A704-F893423FE53C'
  
    UPDATE #temp1 SET ReturntoCourierAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE ExpiredFromPOPStationAt is null and EventTypeId = 'B0E6CBE0-06B0-4DBC-A704-F893423FE53C'


	       UPDATE #temp1 SET TransferscanAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = 'E293FFDE-76E3-4E69-BCEB-473F91B4350C'
  
    UPDATE #temp1 SET TransferscanAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE ExpiredFromPOPStationAt is null and EventTypeId = 'E293FFDE-76E3-4E69-BCEB-473F91B4350C'


	       UPDATE #temp1 SET IndepotAt =E.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent E ON E.sourcereference =  t.trackingnumber
       WHERE EventTypeId = 	'B8D04A85-A65B-41EA-9056-A950BE2CB509'
  
    UPDATE #temp1 SET IndepotAt =EA.EventDateTime
   FROM #Temp1 T JOIN ScannerGateway.dbo.TrackingEvent_Archive EA ON EA.sourcereference =  t.trackingnumber
       WHERE ExpiredFromPOPStationAt is null and EventTypeId = 	'B8D04A85-A65B-41EA-9056-A950BE2CB509'

 
 /*SELECT cd_connote,com.c_name [supplier],cd_deliver_driver,cd_delivery_contact
 ,cd_delivery_contact_phone,cd_last_stamp,t.* into #temp2 
 FROM #temp1 t join  CpplEDI.dbo.cdcoupon (nolock) cc on t.TrackingNumber=cc.cc_coupon
  join CpplEDI.dbo.consignment (nolock) c on cd_id=cc_consignment
  left join CpplEDI.[dbo].[companies] (nolock) com on com.c_id=c.cd_id*/

  select * from #temp1

END


/*select top 100 * from    [ScannerGateway].[dbo].[HubbedStaging] (Nolock) HS
  join  CpplEDI.dbo.cdcoupon (nolock) cc on HS.TrackingNumber=cc.cc_coupon
  join CpplEDI.dbo.consignment (nolock) c on cd_id=cc_consignment
  left join CpplEDI.[dbo].[companies]
  
   left join [CouponCalculator].[dbo].[DeliveryChoices] DC
  ON rtrim(HS.[AgentId])= rtrim(DC.[DeliveryChoiceID])
  left join CpplEDI.[dbo].[cdextra] cd on cd.ce_consignment=c.cd_id

  select * from EventType where Id='A341A7FC-3E0E-4124-B16E-6569C5080C6D'

  select * from #Temp1 where lastscantype is not null

  select * from CpplEDI.dbo.cdcoupon (nolock) where cc_consignment=98703641

  select * from CpplEDI.dbo.consignment (nolock) where cd_connote='CPAFXLC12205007'

  select * from performancereporting.dbo.primarylabels (nolock) where labelnumber='CPAFXLT12205007'*/
GO
GRANT EXECUTE
	ON [dbo].[sp_RptHUBBEDPickupsVsDone]
	TO [ReportUser]
GO
