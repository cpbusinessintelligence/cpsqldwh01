SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptJobStatus]
as
begin

  --'=====================================================================
    --' CP -Stored Procedure - sp_RptJobStatus
    --' ---------------------------
    --' Purpose: sp_RptJobStatus-----
    --' Developer: SS
    --' Date: 05/09/2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
                    

    --'=====================================================================


Select
       SUBSTRING(T2.name,1,140) as [Job Name],Max(msdb.dbo.agent_datetime(run_date, run_time)) as "Latest Failure",
       Min(msdb.dbo.agent_datetime(run_date, run_time)) as "First Failure",
	   count( SUBSTRING(T2.name,1,140)) "Number of Failures",
	   CASE(T4.freq_subday_interval)
       WHEN 0 THEN 'Once in a day'
       ELSE cast('Every ' 
            + right(T4.freq_subday_interval,2) 
            + ' '
            +     CASE(T4.freq_subday_type)
                     WHEN 1 THEN 'Once in a day'
                     WHEN 4 THEN 'Minutes'
                     WHEN 8 THEN 'Hours'
                  END as char(16))
       END as 'Frequency'
	--,
	--T1.message AS [Error Message]
From  msdb..sysjobhistory T1 INNER JOIN msdb..sysjobs T2 ON T1.job_id = T2.job_id
LEFT OUTER JOIN msdb..sysjobschedules T3
ON T2.job_id = T3.job_id
INNER JOIN msdb..sysschedules T4
 ON T3.schedule_id =T4.schedule_id 
WHERE
     T1.run_status NOT IN (1, 4)
     AND T1.step_id != 0
     AND  
	 --Datediff(hh, Convert(Datetime, RTrim(run_date)), Getdate()) < 24
	 run_date >  CONVERT(CHAR(8), (SELECT DATEADD (DAY,(-2), GETDATE())), 112) and run_time >'70000'
Group by SUBSTRING(T2.name,1,140),freq_subday_interval,freq_subday_type
--,T1.message
order by 4 desc
end
GO
GRANT CONTROL
	ON [dbo].[sp_RptJobStatus]
	TO [ReportUser]
GO
GRANT EXECUTE
	ON [dbo].[sp_RptJobStatus]
	TO [ReportUser]
GO
