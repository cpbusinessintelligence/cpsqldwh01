SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptPOPShopLabelsTimeline](@StartDate date,@EndDate date) as
begin

--'=====================================================================
    --' CP -Stored Procedure - [[sp_RptPOPShopLabelsTimeline]]
    --' ---------------------------
    --' Purpose: sp_RptPOPStationLabelsTimeline-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 09 Mar 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 09/03/2017    AK      1.00    Created the procedure                            

    --'=====================================================================


Select SourceReference,replace(isnull(Additionaltext1,''),'Link Coupon ','') as CardNumber,EventDatetime as CardLeftTime,convert(datetime,'') as AcceptedbyNewsAgentTime,convert(datetime,'') as DeliveredByNewsAgentTime
into #temp
from Scannergateway.dbo.trackingevent 
where isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon %RTCNA' or isnull(additionaltext1,'') like 'NHCLC%' and convert(Date,EventDatetime) between @StartDate and @EndDate

Update #temp set AcceptedbyNewsAgentTime=EventDatetime from Scannergateway.dbo.trackingevent t where eventtypeid='C71FCCF6-779A-4B6A-AE69-1053851DF3D7' and t.sourcereference=#temp.Sourcereference

Update #temp set DeliveredByNewsAgentTime=EventDatetime from Scannergateway.dbo.trackingevent t where eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and t.sourcereference=#temp.Sourcereference and t.eventdatetime>=AcceptedbyNewsAgentTime

Select  SourceReference, CardNumber, CardLeftTime, AcceptedbyNewsAgentTime, DeliveredByNewsAgentTime, (Datediff(Hour,CardLeftTime, AcceptedbyNewsAgentTime)/24.0 ) as Days1,
(Datediff(Hour,AcceptedbyNewsAgentTime, DeliveredByNewsAgentTime)/24.0) as Days2  
into #temp2
from #temp

Update #temp2 set Days1=(CASE WHEN AcceptedbyNewsAgentTime = '1900-01-01 00:00:00.000' OR CardLeftTime = '1900-01-01 00:00:00.000' THEN NULL ELSE ROUND(Days1,2) END)
Update #temp2 set Days2=(CASE WHEN DeliveredByNewsAgentTime = '1900-01-01 00:00:00.000' OR AcceptedbyNewsAgentTime = '1900-01-01 00:00:00.000' THEN NULL ELSE ROUND(Days2,2) END)

Select SourceReference, CardNumber, CardLeftTime, AcceptedbyNewsAgentTime, Days1 as DaysBetweenCardLeftAndAcceptedbyNewsAgent, DeliveredByNewsAgentTime,
Days2 as DaysBetweenDropOffAndDeliveredByNewsAgent 
from #temp2


end

GO
GRANT EXECUTE
	ON [dbo].[sp_RptPOPShopLabelsTimeline]
	TO [ReportUser]
GO
