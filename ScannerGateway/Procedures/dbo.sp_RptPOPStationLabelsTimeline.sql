SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RptPOPStationLabelsTimeline](@StartDate date,@EndDate date) as
begin

--'=====================================================================
    --' CP -Stored Procedure - [[sp_RptPOPStationLabelsTimeline]]
    --' ---------------------------
    --' Purpose: sp_RptPOPStationLabelsTimeline-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 09 Mar 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 09/03/2017    AK      1.00    Created the procedure                            

    --'=====================================================================


Select SourceReference,replace(isnull(Additionaltext1,''),'Link Coupon ','') as CardNumber,EventDatetime as CardLeftTime,convert(datetime,'') as DropOffinPOPStationTime,convert(datetime,'') as ExpiredFromPOPStationTime,convert(datetime,'') as DeliveredByPOPStationTime
into #temp
from Scannergateway.dbo.trackingevent 
where isnull(Additionaltext1,'') like 'Link Coupon %SLCNA' and convert(Date,EventDatetime) between @StartDate and @EndDate

Update #temp set DropOffinPOPStationTime=EventDatetime from Scannergateway.dbo.trackingevent t where eventtypeid='C79189AA-ED10-4437-A257-F3ECAC28F6BE' and t.sourcereference=#temp.Sourcereference

Update #temp set ExpiredFromPOPStationTime=EventDatetime from Scannergateway.dbo.trackingevent t where eventtypeid='12A864E4-FF1F-4421-9451-CA5BA1AA431E' and t.sourcereference=#temp.Sourcereference

Update #temp set DeliveredByPOPStationTime=EventDatetime from Scannergateway.dbo.trackingevent t  where eventtypeid='49242882-32FA-46E6-B222-1DFB2372354F' and t.sourcereference=#temp.Sourcereference

Select  SourceReference, CardNumber, CardLeftTime, DropOffinPOPStationTime, ExpiredFromPOPStationTime, DeliveredByPOPStationTime, (Datediff(Hour,CardLeftTime, DropOffinPOPStationTime)/24.0 ) as Days1,
(Datediff(Hour,DropOffinPOPStationTime, ExpiredFromPOPStationTime)/24.0) as Days2, (Datediff(Hour,ExpiredFromPOPStationTime, DeliveredByPOPStationTime)/24.0) as Days3
into #temp2
from #temp

Update #temp2 set Days1=(CASE WHEN DropOffinPOPStationTime = '1900-01-01 00:00:00.000' OR CardLeftTime = '1900-01-01 00:00:00.000' THEN NULL ELSE ROUND(Days1,2) END)
Update #temp2 set Days2=(CASE WHEN ExpiredFromPOPStationTime = '1900-01-01 00:00:00.000' OR DropOffinPOPStationTime = '1900-01-01 00:00:00.000' THEN NULL ELSE ROUND(Days2,2) END)
Update #temp2 set Days3=(CASE WHEN DeliveredByPOPStationTime = '1900-01-01 00:00:00.000' OR ExpiredFromPOPStationTime = '1900-01-01 00:00:00.000' THEN NULL ELSE ROUND(Days3,2) END)
 

Select SourceReference, CardNumber, CardLeftTime, DropOffinPOPStationTime, Days1 as DaysBetweenCardLeftAndDropOff, ExpiredFromPOPStationTime,
Days2 as DaysBetweenDropOffAndExpiredFromPOPStation, DeliveredByPOPStationTime, Days3 as DaysBetweenExpiredFromPopStationAndDeliveredByPOPStation 
 from #temp2

end


GO
GRANT EXECUTE
	ON [dbo].[sp_RptPOPStationLabelsTimeline]
	TO [ReportUser]
GO
