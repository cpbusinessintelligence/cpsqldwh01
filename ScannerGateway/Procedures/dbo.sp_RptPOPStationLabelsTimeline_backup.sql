SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_RptPOPStationLabelsTimeline_backup](@StartDate date,@EndDate date) as
begin

--'=====================================================================
    --' CP -Stored Procedure - [[sp_RptPOPStationLabelsTimeline_backup]]
    --' ---------------------------
    --' Purpose: sp_RptPOPStationLabelsTimeline-----
    --' Developer: Abhigna Kona (Couriers Please Pty Ltd)
    --' Date: 09 Mar 2017
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 09/03/2017    AK      1.00    Created the procedure                            

    --'=====================================================================


Select SourceReference,replace(isnull(Additionaltext1,''),'Link Coupon ','') as CardNumber,EventDatetime as CardLeftTime,convert(datetime,'') as DropOffinPOPStationTime,convert(datetime,'') as ExpiredFromPOPStationTime,convert(datetime,'') as DeliveredByPOPStationTime
into #temp
from Scannergateway.dbo.trackingevent 
where isnull(Additionaltext1,'') like 'Link Coupon %SLCNA' and convert(Date,EventDatetime) between @StartDate and @EndDate

Update #temp set DropOffinPOPStationTime=EventDatetime from Scannergateway.dbo.trackingevent t where eventtypeid='C79189AA-ED10-4437-A257-F3ECAC28F6BE' and t.sourcereference=#temp.Sourcereference

Update #temp set ExpiredFromPOPStationTime=EventDatetime from Scannergateway.dbo.trackingevent t where eventtypeid='12A864E4-FF1F-4421-9451-CA5BA1AA431E' and t.sourcereference=#temp.Sourcereference

Update #temp set DeliveredByPOPStationTime=EventDatetime from Scannergateway.dbo.trackingevent t  where eventtypeid='49242882-32FA-46E6-B222-1DFB2372354F' and t.sourcereference=#temp.Sourcereference

Select * from #temp
end


GO
