SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_RptQuantiumscans] as
begin

declare  @Temp table(StatusCode varchar(100),	StatusDescription varchar(100),	ReasonCode varchar(100),ReasonDescription varchar(100))


Insert into @temp values ('DBC','Delivered','DBC0','Delivery complete and dues received'),
                         ('DBH','Cannot be delivered','DBH1','Address not reachable/Entry Restrictions'),
						 ('DBH','Cannot be delivered','DBH2','Consignee does not stay at given address'),
						 ('DBR','Refused','DBR0','consignee refused')

Declare  @temp1 Table (Labelnumber varchar(200),
                       NewStatuscode varchar(100),
					   NewStatusDescription varchar(100),
					   EventName varchar(100),
					   NewReasoncode varchar(100),
					   NewReasonDescription varchar(100),
					   DeliveryDate Date,
					   ReceiverName varchar(200),
					   Remarks varchar(100),
					   Exceptionreason varchar(400))

Insert into @Temp1

select  l.LabelNumber,
       convert(varchar(100),'') as NewStatuscode,
	   convert(varchar(100),'') as NewStatusDescription,
	   e.Description as EventName,
	   convert(varchar(100),'') as NewReasoncode,
	   convert(varchar(100),'') as NewReasonDescription,
	   convert(date,EventDateTime) as DeliveryDate,
	   isnull(case when e.Description='Delivered' then isnull(replace(ExceptionReason,'POD:',''),AdditionalText1) else '' end,'') as ReceiverName,
	   convert(varchar(100),'') as Remarks,
	   Exceptionreason
from trackingevent t left join label l on l.id=t.labelid left join eventtype e on t.eventtypeid=e.id 
where eventtypeid in ('47CFA05F-3897-4F1F-BDF4-00C6A69152E3','FCFC0FB1-C46C-43C7-92B7-3143BA7173D2') and  eventdatetime>=Convert(datetime, Convert(varchar(10), dateadd(day,-1,getdate()), 103) + ' 15:30:00', 103)
and eventdatetime<=Convert(datetime, Convert(varchar(10), getdate(), 103) + ' 15:29:00', 103)
and labelnumber like 'CPA0YJ%'

update @temp1 set NewStatuscode='DBC',NewStatusDescription='Delivered',NewReasoncode='DBC0',NewReasonDescription='Delivery complete and dues received'
where EventName='Delivered'

update @temp1 set NewStatuscode='DBH',NewStatusDescription='Cannot be delivered',NewReasoncode='DBH1',NewReasonDescription='Address not reachable/Entry Restrictions'
where EventName='Attempted Delivery' and exceptionreason='Return to Sender - Insufficient Address'

update @temp1 set NewStatuscode='DBH',NewStatusDescription='Cannot be delivered',NewReasoncode='DBH2',NewReasonDescription='Consignee does not stay at given address'
where EventName='Attempted Delivery' and exceptionreason='Return to Sender - Wrong Address'

update @temp1 set NewStatuscode='DBR',NewStatusDescription='Refused',NewReasoncode='DBR0',NewReasonDescription='Consignee refused'
where EventName='Attempted Delivery' and exceptionreason='Return to Sender - Refused delivery'


Select distinct Labelnumber as TrackingNo,
                NewStatusCode as NewStatus,
				NewReasoncode as NewReason,
				DeliveryDate,
				ReceiverName,
				Remarks
from @temp1 t1 join @Temp t on t.ReasonCode=t1.NewReasonCode
order by LabelNumber


end
GO
