SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[sp_RptReturnsCageScanReport_Seko_20180322] @Date Date

AS

BEGIN

       SET FMTONLY OFF 
       SET NOCOUNT ON;

       --set @AccountCode='112976535'
--Declare @Date DateTime
--set @Date= '2018-03-09'


DECLARE @StartDate DateTime
DECLARE @EndDate DateTime

SET @StartDate=dateadd(dd, datediff(dd,0,@Date),0)- 1 + '14:00:00' 
SEt @EndDate=(dateadd(dd, datediff(dd,0,@Date),0)) + '13:59:59' 

SELECT  convert(Varchar(50),[SourceReference]) as LabelNumber
	  , convert(Varchar(50),'') as ConsignmentNumber
	  , convert(Varchar(50),'') as AccountCode
	  	  , convert(Varchar(50),'') as PickUpAddress1
	  , convert(Varchar(50),'') as PickupAddress2
	  	  , convert(Varchar(50),'') as PickupSuburb
	  , convert(Varchar(50),'') as PickuPPostcode
	  , convert(Varchar(50),'') as ConsignmentDate
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]




      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
     
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
 Into #Temp1
  FROM [ScannerGateway].[dbo].[TrackingEvent] where AdditionalText2 like '%DLB 20325%' 
  and [EventDateTime] between @StartDate and @Enddate

  Update #Temp1 SET ConsignmentNumber = CON.cd_connote,
                   PickUpAddress1 = CON.cd_pickup_addr0,
				   PickUpAddress2 = CON.cd_pickup_addr1,
				   PickuPPostcode = CON.cd_pickup_postcode,
				   PickupSuburb= CON.cd_pickup_suburb,
                    AccountCode = CON.cd_account,
					ConsignmentDate = CON.cd_date
  
                   From #Temp1 T left join CpplEDI.dbo.cdcoupon C on T.LabelNumber= c.cc_coupon
                                                         left join CpplEDI.dbo.consignment CON on c.cc_consignment = CON.cd_id


/*Select C.cd_account,C.cd_connote , P.cc_coupon,c.cd_id,c.cd_date,[cd_pickup_addr0]
      ,[cd_pickup_addr1]
      ,[cd_pickup_suburb]
      ,[cd_pickup_postcode]
into #Temp1
from cpplEDI.dbo.consignment C Join cpplEDI.dbo.cdcoupon P on C.cd_id = P.cc_consignment
where cd_account in ('113058192','113066385',
'112952007',
'112951975',
'112951991',
'112951983',
'112887229',
'113015440',
'112923644',
'112935846',
'112935853',
'112935861',
'112935887',
'113015424',
'112906136'
)and cd_date < = getdate()  and cd_date > = Dateadd(day, -30, getdate())



Select T.cd_account as AccountNumber
     ,T.cd_connote as ConsignmentNumber
       ,t.cc_coupon as LabelNumber
       ,t.cd_id as consignmnentId
       ,L.Id as LabelId
       ,T.cd_date as ConsignmentDate
       ,[cd_pickup_addr0] as PickupAddress
     ,[cd_pickup_suburb] as PickupSuburb
     ,[cd_pickup_postcode] as PickupPostCode
into #Temp2  
from #Temp1 T left Join ScannerGateway.dbo.Label L on T.cc_coupon = L.LabelNumber


Select AccountNumber
     ,ConsignmentNumber
       ,LabelNumber
       ,consignmnentId
       ,T.LabelId
       ,ConsignmentDate
       ,PickupAddress
     ,PickupSuburb
     ,PickupPostCode into #Temp3 from #Temp2 T Join   ScannerGateway.[dbo].[TrackingEvent] P on T.LabelId = P.LabelID
where P.EventTypeId='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and P.[AdditionalText2] like 'DLB 20325%' 

*/
Select  AccountCode as AccountNumber
     ,ConsignmentNumber
       ,LabelNumber
       ,convert(date,ConsignmentDate) as [ConsignmentDate]
	   , [EventDateTime] as ScannedOn
       ,PickupAddress1
	     ,PickupAddress2
     ,PickupSuburb
     ,PickupPostCode
from  #Temp1

ORDER BY ConsignmentNumber,LabelNumber, ConsignmentDate

END


GO
