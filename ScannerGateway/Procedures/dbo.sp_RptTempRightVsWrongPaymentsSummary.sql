SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_RptTempRightVsWrongPaymentsSummary(@contractor varchar(20),@SelOptions varchar(500)) as
begin


select distinct consignmentnumber,scandate,scantype,convert(varchar(100),'') as RightBranch,DriverNumber as RightDriverNumber,RightProntoid,pickupcontractor as ContractorPaid,pickuprctiamount  as RCTIAmount
into #temp
from Report_RightwrongScans where 
rightprontoid<>pickupcontractor and scantype='pickup' and isnull(rightprontoid,'')<>''and isnull(pickupcontractor,'')<>''and (pickupcontractor like 'S%' or pickupcontractor like 'C%') and incorrectscans='Y'

union all

select distinct consignmentnumber,scandate,scantype,'' as RightBranch,DriverNumber as RightDriverNumber,RightProntoid,Deliverycontractor as ContractorPaid,Deliveryrctiamount  as RCTIAmount
from	Report_RightwrongScans	
where rightprontoid<>deliverycontractor and scantype<>'pickup' and isnull(rightprontoid,'')<>''and isnull(deliverycontractor,'')<>'' and (deliverycontractor like 'S%'  or deliverycontractor like 'C%')and incorrectscans='Y'
order by consignmentnumber


update #temp set RightBranch=CASE LEFT(ISNULL(ltrim(rtrim(ContractorPaid)), ' '), 1)
			WHEN 'B' THEN 'BRISBANE'
			WHEN 'C' THEN 'SYDNEY'
			WHEN 'A' THEN 'ADELAIDE'
			WHEN 'M' THEN 'MELBOURNE'
			WHEN 'G' THEN 'GOLDCOAST'
			WHEN 'O' THEN 'GOLDCOAST'
			WHEN 'S' THEN 'SYDNEY'
			when 'W' then 'PERTH'
			ELSE ''
		END


--Select * from #temp
--If(@contractor='' ) and (@SelOptions='None')
--Select * from #temp order by ConsignmentNumber

If (@contractor<>'' ) and (@SelOptions='ContractorCurrentlyPaid')
Select ContractorPaid as Contractor,count(*) as Count,sum(RCTIAmount) as RCTIAmount from #temp 
where ContractorPaid=@contractor
group by ContractorPaid

--(@contractor<>'' ) and (@SelOptions='RightContractortobePaid')
else 
Select RightProntoid as Contractor,count(*) as Count,sum(RCTIAmount) as RCTIAmount from #temp 
where RightProntoid=@contractor
group by RightProntoid 

end
GO
GRANT EXECUTE
	ON [dbo].[sp_RptTempRightVsWrongPaymentsSummary]
	TO [ReportUser]
GO
