SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_Rpt_CouponsLinkedtoNewsAgent]
as 
begin

     --'=====================================================================
    --' CP -Stored Procedure -[sp_Rpt_CouponsLinkedtoNewsAgent]
    --' ---------------------------
    --' Purpose: Get all coupons linked to NewsAgent and find their last status
    --' Developer: Abhigna (Couriers Please Pty Ltd)
    --' Date: 03 Dec 2014
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            Bookmark
    --' ----          ---     ---     -----                                             -------
    --' 03/12/2014    AB      1.00    - Created the procedure                           --AB20141203

    --'=====================================================================

--Get all Labels linked to NewsAgent yesterday from Trackingevent--

select distinct te.eventdatetime as LinkScandate,
       te.labelid,
	   l.labelnumber,
	   substring(ltrim(rtrim(additionaltext1)),13,len(ltrim(rtrim(additionaltext1)))) as Cardnumber 
into #temp 
from dbo.trackingevent te left join dbo.label l on l.id=labelid 
	   where eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D'
             and additionaltext1 like 'Link Coupon NH%' 
			 and convert(date,eventdatetime)=convert(date,dateadd("DD",-1,getdate()))

			 
----Get Maximum eventdate for those label numbers to get last event happened on that label---

select distinct  te.labelid,
                 max(te.eventdatetime) as Lasteventdatetime,
				 Linkscandate,
				 Labelnumber,
				 Cardnumber 
into #temp1 
from #temp t left join dbo.trackingevent te on t.labelid=te.labelid
group by te.labelid,Linkscandate,Labelnumber,Cardnumber


--Rename the Coupon status more meaningfully---
select distinct t.*,
                case when te.AdditionalText2 like 'DLB%' then 'Delivered to NewsAgent'  else case when isnull(te.ExceptionReason,'') like 'Status: Accepted by Newsagent%' then 'Accepted by Newsagent' else et.Description end  end as CouponStatus,
                isnull(te.AdditionalText2,'') as AdditionalComments,
				isnull(te.ExceptionReason,'') as ExceptionReason
into #temp2 
from #temp1 t left join dbo.trackingevent te on t.labelid=te.labelid 
                                                and te.eventdatetime=t.lasteventdatetime 
			  left join  dbo.eventtype et on et.id=te.EventTypeId

---Get how many times each label is repeated in the table---

select labelnumber,
       Cardnumber,
	   Linkscandate,
	   Lasteventdatetime,
	   count(*) as Count 
into #tempcount 
from #temp2
group by labelnumber,Cardnumber,Linkscandate,Lasteventdatetime

---Generally eventdate is same for two scans in which one of which is a link scan.
--We want the coupon status as second scan, (other than link scan) for that label and rename first scan status to NULL and we display only second scan for that label---
select t.Cardnumber,
       t.Labelnumber,
	   t.Linkscandate,
	   case when tc.count=1 then t.Couponstatus else case when t.Couponstatus not  like '%Link Scan%' then t.Couponstatus else NULL end end as Couponstatus,
	   t.Lasteventdatetime
into #temp3 from #temp2 t join #tempcount tc on t.labelnumber=tc.labelnumber 
                                             and t.linkscandate=tc.linkscandate 
											 and t.lasteventdatetime=tc.lasteventdatetime

----Sometimes a Coupon is linked to newsagent twice,it should be avoided.Get the count of number of times a coupon is linked to NewsAgent---

select distinct 
       labelid,
	   labelnumber
	 into #temp4
from #temp

select labelnumber,count(*) as NumberoftimeslinkedtoNewsAgent
into #temp5 from #temp4 t left join dbo.trackingevent te on t.labelid=te.labelid
where additionaltext1 like 'Link Coupon NH%' 
			 and convert(date,eventdatetime)>=convert(date,dateadd("DD",-7,getdate()))
			 group by labelnumber

--select * from #temp5
--select distinct t.Labelnumber,
--                count(*) as NumberoftimeslinkedtoNewsAgent 
--into #temp4 
--from #temp3 t 
--where couponstatus is not null
--group by   t.Labelnumber

---Display the list--
select t.Cardnumber,
       t.Labelnumber,
	   t.Linkscandate,
	   Couponstatus,
	   t.Lasteventdatetime,
	   t1.NumberoftimeslinkedtoNewsAgent 
	   from #temp3 t left join #temp5 t1 on t.labelnumber=t1.labelnumber
	   --join #temp4 t1 on t.labelnumber=t1.labelnumber 
       where Couponstatus is not null

	   end
GO
GRANT EXECUTE
	ON [dbo].[sp_Rpt_CouponsLinkedtoNewsAgent]
	TO [ReportUser]
GO
