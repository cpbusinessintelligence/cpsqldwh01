SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_TrackingArchiveProceedure] (@ArchiveDate Datetime)
as
	-----------------------------------------------------
	-- Purpose : Archives Different tables in ODS!
	-----------------------------------------------------
	-- Revision History
	-- ----------------
	--	Date		Who		Notes
	--	----		---		-----
	--	13/05/2013	JP		-  Created
	-----------------------------------------------------
	
	-- Determine Archival date ranges


-- 6 weeks for orders that have been picked
	Declare @ArchiveTrackingEventDate datetime
--	Select @ArchiveTrackingEventDate = Convert(DateTime, Convert(varchar(10), DateAdd(wk, -6, GetDate()), 103), 103)
    
	 
	 If(@ArchiveDate='')
select @ArchiveTrackingEventDate=dateadd(month,-4,getdate())
else
Select @ArchiveTrackingEventDate = @ArchiveDate

-----------------------------------------------------------------
	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] ON

	BEGIN TRANSACTION trnTrackingEvent_2016_FH


INSERT INTO [dbo].[TrackingEvent_Archive2014_15]
              ([Id],[EventTypeId],[EventDateTime]
				,[Description],[LabelId],[AgentId]
                ,[DriverId],[RunId],[EventLocation]
			    ,[ExceptionReason],[AdditionalText1],[AdditionalText2]
           ,[NumberOfItems]
           ,[SourceId]
           ,[SourceReference]
           ,[CosmosSignatureId]
           ,[CosmosBranchId]
           ,[IsDeleted]
           ,[CreatedDate]
           ,[LastModifiedDate])
     			SELECT [Id]
      ,[EventTypeId]
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]
      ,[AgentId]
      ,[DriverId]
      ,[RunId]
      ,[EventLocation]
      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
      ,[NumberOfItems]
      ,[SourceId]
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
      ,[IsDeleted]
      ,[CreatedDate]
      ,[LastModifiedDate]
  FROM [dbo].[TrackingEvent](NOLOCK)
  WHERE (year(eventdatetime)*100)+Month(eventdatetime) in (201406,201407,201408,201409,201410,201411,201412,201501,201502)
  and [EventDateTime]<=@ArchiveTrackingEventDate 


			   
INSERT INTO [dbo].[TrackingEvent_Archive2015_16]
              ([Id],[EventTypeId],[EventDateTime]
				,[Description],[LabelId],[AgentId]
                ,[DriverId],[RunId],[EventLocation]
			    ,[ExceptionReason],[AdditionalText1],[AdditionalText2]
           ,[NumberOfItems]
           ,[SourceId]
           ,[SourceReference]
           ,[CosmosSignatureId]
           ,[CosmosBranchId]
           ,[IsDeleted]
           ,[CreatedDate]
           ,[LastModifiedDate])
     			SELECT [Id]
      ,[EventTypeId]
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]
      ,[AgentId]
      ,[DriverId]
      ,[RunId]
      ,[EventLocation]
      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
      ,[NumberOfItems]
      ,[SourceId]
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
      ,[IsDeleted]
      ,[CreatedDate]
      ,[LastModifiedDate]
  FROM [dbo].[TrackingEvent] (NOLOCK)
WHERE (year(eventdatetime)*100)+Month(eventdatetime) in (201503,201504,201505,201506,201507,201508,201509,201510,201511,201512,201601,201602,201603)
and [EventDateTime]<=@ArchiveTrackingEventDate 



	

INSERT INTO [dbo].TrackingEvent_Archive2016_17
              ([Id],[EventTypeId],[EventDateTime]
				,[Description],[LabelId],[AgentId]
                ,[DriverId],[RunId],[EventLocation]
			    ,[ExceptionReason],[AdditionalText1],[AdditionalText2]
           ,[NumberOfItems]
           ,[SourceId]
           ,[SourceReference]
           ,[CosmosSignatureId]
           ,[CosmosBranchId]
           ,[IsDeleted]
           ,[CreatedDate]
           ,[LastModifiedDate])
     			SELECT [Id]
      ,[EventTypeId]
      ,[EventDateTime]
      ,[Description]
      ,[LabelId]
      ,[AgentId]
      ,[DriverId]
      ,[RunId]
      ,[EventLocation]
      ,[ExceptionReason]
      ,[AdditionalText1]
      ,[AdditionalText2]
      ,[NumberOfItems]
      ,[SourceId]
      ,[SourceReference]
      ,[CosmosSignatureId]
      ,[CosmosBranchId]
      ,[IsDeleted]
      ,[CreatedDate]
      ,[LastModifiedDate]
  FROM [dbo].[TrackingEvent](NOLOCK)

	           WHERE [EventDateTime]<=@ArchiveTrackingEventDate and (year(eventdatetime)*100)+Month(eventdatetime)>=201604
	 
	DELETE FROM [TrackingEvent] WHERE [EventDateTime]<=@ArchiveTrackingEventDate

	COMMIT TRANSACTION trnTrackingEvent_2016_FH

	--SET IDENTITY_INSERT [TrackingEvent_2011_SH] OFF

-----------------------------------------------------------------

GO
