SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_UpdateHubbedTrackingEvents as
begin

 --'=====================================================================
    --' CP -Stored Procedure - sp_UpdateHubbedTrackingEvents
    --' ---------------------------
    --' Purpose: sp_UpdateHubbedTrackingEvents-----
    --' Developer: Abhigna KONA (Couriers Please Pty Ltd)
    --' Date: 27 May 2016
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                            
    --' ----          ---     ---     -----                                            
    --' 27/05/2016    AK      1.00    Created the procedure                            

    --'=====================================================================

Insert into HubbedCardStaging(HubbedCard ,PrimaryCoupon ,isdelivered,isprocessed)

Select Labelnumber as HubbedCard,
       replace(additionaltext1,'Link Coupon ','') as PrimaryCoupon,
	   0,
	   0
from label l(NOLOCK) join trackingevent t(NOLOCK) on sourcereference=labelnumber where labelnumber like '%RTCNA' and convert(Date,l.createddate)>='2016-05-27'
and eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and not exists(Select HubbedCard from HubbedCardStaging where HubbedCard=Labelnumber)

Update HubbedCardStaging set isdelivered=1 , DeliveryLocation=substring(isnull(Additionaltext2,''),12,len(isnull(Additionaltext2,''))-11),DeliveryDatetime=Eventdatetime
from Trackingevent where sourcereference=PrimaryCoupon
and eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and isnull(additionaltext2,'') like 'DLB%'

end
GO
GRANT EXECUTE
	ON [dbo].[sp_UpdateHubbedTrackingEvents]
	TO [SSISUser]
GO
