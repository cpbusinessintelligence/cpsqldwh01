SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure sp_UpdateRedeliveryCardTracking as
begin


Insert into ExternalStatusStaging(TrackingNumber,ConsignmentNumber,StatusDateTime,ScanEvent,Contractor,ExceptionReason,Category)


Select  replace(additionaltext1,'Link Coupon ',''), 
        ConsignmentNumber,
		StatusDateTime,
		ScanEvent,
		Contractor,
		e.ExceptionReason,
		Category
from ExternalStatusStaging e (NOLOCK) join trackingevent t (NOLOCK) on t.sourcereference=trackingnumber where ScanEvent like '%Accepted by%'
and eventtypeid='A341A7FC-3E0E-4124-B16E-6569C5080C6D' and ( isnull(additionaltext1,'') like 'Link Coupon NHCLC%' or isnull(additionaltext1,'') like 'Link Coupon 191%' or isnull(additionaltext1,'') like 'Link Coupon%CNA')



Insert into ExternalStatusStaging(TrackingNumber,ConsignmentNumber,StatusDateTime,ScanEvent,Contractor,ExceptionReason,Category)


Select  additionaltext1, 
        ConsignmentNumber,
		StatusDateTime,
		ScanEvent,
		Contractor,
		e.ExceptionReason,
		Category
from ExternalStatusStaging e (NOLOCK) join trackingevent t (NOLOCK) on t.sourcereference=trackingnumber where ScanEvent like '%Accepted by%'
and eventtypeid='47CFA05F-3897-4F1F-BDF4-00C6A69152E3' and ( isnull(additionaltext1,'') like 'NHCLC%' )



end
GO
GRANT EXECUTE
	ON [dbo].[sp_UpdateRedeliveryCardTracking]
	TO [SSISUser]
GO
