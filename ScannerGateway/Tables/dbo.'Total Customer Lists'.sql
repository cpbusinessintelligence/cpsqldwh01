SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].['Total Customer Lists'] (
		[State]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[RepCode]              [float] NULL,
		[OriginalRepName]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Territory]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Accountcode]          [float] NULL,
		[BillTo]               [float] NULL,
		[Shortname]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address1]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Email]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[TARIFF]               [float] NULL,
		[SalesPersonEmail]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[BccEmail]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Contact Name]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Remarks]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[EMail_Gemma]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].['Total Customer Lists'] SET (LOCK_ESCALATION = TABLE)
GO
