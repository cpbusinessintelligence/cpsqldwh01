SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Agent] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[ShortCode]            [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                 [varchar](120) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]             [bit] NOT NULL,
		[IsDeleted]            [bit] NOT NULL,
		[CreatedDate]          [datetime] NOT NULL,
		[LastModifiedDate]     [datetime] NOT NULL,
		CONSTRAINT [PK_Agent]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Agent]
	ADD
	CONSTRAINT [DF_Agent_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Agent]
	ADD
	CONSTRAINT [DF_Agent_IsActive]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Agent]
	ADD
	CONSTRAINT [DF_Agent_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Agent]
	ADD
	CONSTRAINT [DF_Agent_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Agent]
	ADD
	CONSTRAINT [DF_Agent_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_Agent_ShortCode]
	ON [dbo].[Agent] ([ShortCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Agent] SET (LOCK_ESCALATION = TABLE)
GO
