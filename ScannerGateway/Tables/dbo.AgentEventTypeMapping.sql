SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentEventTypeMapping] (
		[Customer_Status]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Event_Type]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Exception_Reason]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AgentEventTypeMapping] SET (LOCK_ESCALATION = TABLE)
GO
