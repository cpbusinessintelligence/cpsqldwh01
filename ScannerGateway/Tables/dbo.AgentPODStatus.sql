SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentPODStatus] (
		[FileNo]          [int] IDENTITY(1, 1) NOT NULL,
		[FileName]        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[AgentPODStatus]
	ADD
	CONSTRAINT [DF__AgentPODS__Creat__46D27B73]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[AgentPODStatus] SET (LOCK_ESCALATION = TABLE)
GO
