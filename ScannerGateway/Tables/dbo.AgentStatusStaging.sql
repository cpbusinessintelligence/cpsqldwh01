SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentStatusStaging] (
		[Sno]                  [int] IDENTITY(1, 1) NOT NULL,
		[ItemType]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Item]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TimeStamp]            [datetime] NULL,
		[Status]               [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Description]          [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Count]                [int] NULL,
		[TotalCount]           [int] NULL,
		[PODName]              [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Reference]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]          [bit] NULL,
		[CreatedDatetime]      [datetime] NULL,
		[CreatedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]       [datetime] NULL,
		[EditedBy]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[format]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ImageString]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[isImageProcessed]     [bit] NULL,
		[Consignment]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__AgentSta__CA1FE4641B497A39]
		PRIMARY KEY
		CLUSTERED
		([Sno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[AgentStatusStaging]
	ADD
	CONSTRAINT [DF__AgentStat__ispro__0CDAE408]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[AgentStatusStaging]
	ADD
	CONSTRAINT [DF__AgentStat__Creat__0DCF0841]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[AgentStatusStaging]
	ADD
	CONSTRAINT [DF__AgentStat__Creat__0EC32C7A]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[AgentStatusStaging]
	ADD
	CONSTRAINT [DF__AgentStat__Edite__0FB750B3]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[AgentStatusStaging]
	ADD
	CONSTRAINT [DF__AgentStat__Edite__10AB74EC]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[AgentStatusStaging]
	ADD
	CONSTRAINT [DF__AgentStat__isIma__4CC05EF3]
	DEFAULT ((0)) FOR [isImageProcessed]
GO
ALTER TABLE [dbo].[AgentStatusStaging] SET (LOCK_ESCALATION = TABLE)
GO
