SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentStatusStaging_Data] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[ItemType]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Item]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TimeStamp]           [datetime] NULL,
		[Status]              [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[Description]         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Count]               [int] NULL,
		[TotalCount]          [int] NULL,
		[PODName]             [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Reference]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]         [bit] NULL,
		[CreatedDatetime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[format]              [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ImageString]         [varchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AgentStatusStaging_Data] SET (LOCK_ESCALATION = TABLE)
GO
