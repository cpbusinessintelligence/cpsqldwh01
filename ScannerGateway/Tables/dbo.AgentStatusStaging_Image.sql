SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentStatusStaging_Image] (
		[encoding]        [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[format]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Status_Id]       [int] NOT NULL,
		[Sno]             [int] IDENTITY(1, 1) NOT NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[AgentStatusStaging_Image]
	ADD
	CONSTRAINT [DF__AgentStat__Creat__58F12BAE]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[AgentStatusStaging_Image] SET (LOCK_ESCALATION = TABLE)
GO
