SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BEApiResponse] (
		[Sno]              [int] IDENTITY(1, 1) NOT NULL,
		[LabelNo]          [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ResponseCode]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]      [datetime] NULL,
		[IsProcessed]      [bit] NULL
)
GO
ALTER TABLE [dbo].[BEApiResponse]
	ADD
	CONSTRAINT [DefDatetime]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[BEApiResponse]
	ADD
	CONSTRAINT [DF__BEApiResp__IsPro__3E131840]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[BEApiResponse] SET (LOCK_ESCALATION = TABLE)
GO
