SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BEPODApiResponse] (
		[Sno]              [int] IDENTITY(1, 1) NOT NULL,
		[ConnoteNo]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ResponseCode]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]      [datetime] NULL,
		[IsProcessed]      [bit] NULL
)
GO
ALTER TABLE [dbo].[BEPODApiResponse]
	ADD
	CONSTRAINT [DF__BEPODApiR__IsPro__47326E26]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[BEPODApiResponse]
	ADD
	CONSTRAINT [DefDatetimepod]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[BEPODApiResponse] SET (LOCK_ESCALATION = TABLE)
GO
