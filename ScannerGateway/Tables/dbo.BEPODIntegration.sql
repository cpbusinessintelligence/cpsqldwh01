SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BEPODIntegration] (
		[ManifestNumber]        [int] NOT NULL,
		[ConnoteNumber]         [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[DateSigned]            [varchar](40) COLLATE Latin1_General_CI_AS NOT NULL,
		[SignedBy]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CollectedByUserId]     [int] NOT NULL,
		[SignatureBase64]       [varchar](376) COLLATE Latin1_General_CI_AS NULL,
		[Isprocessed]           [int] NOT NULL,
		[StatusID]              [int] IDENTITY(1, 1) NOT NULL,
		[CreatedDate]           [datetime] NULL
)
GO
ALTER TABLE [dbo].[BEPODIntegration]
	ADD
	CONSTRAINT [dt_clmnpod]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[BEPODIntegration] SET (LOCK_ESCALATION = TABLE)
GO
