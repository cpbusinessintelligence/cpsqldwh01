SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Barcode Sample] (
		[Barcode]               [nvarchar](150) COLLATE Latin1_General_CI_AS NOT NULL,
		[Type]                  [nvarchar](510) COLLATE Latin1_General_CI_AS NOT NULL,
		[Length]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Prefix]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Can be Used By CP]     [nchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Rule]                  [nvarchar](510) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Barcode Sample] SET (LOCK_ESCALATION = TABLE)
GO
