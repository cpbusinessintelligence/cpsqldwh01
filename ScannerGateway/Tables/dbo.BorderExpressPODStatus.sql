SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BorderExpressPODStatus] (
		[FileNo]          [int] IDENTITY(1, 1) NOT NULL,
		[FileName]        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[BorderExpressPODStatus]
	ADD
	CONSTRAINT [DF__BorderExp__Creat__3631FF56]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[BorderExpressPODStatus] SET (LOCK_ESCALATION = TABLE)
GO
