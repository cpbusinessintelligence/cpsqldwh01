SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Branch] (
		[Id]                             [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                           [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                           [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[CosmosBranch]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CosmosBranchId]                 [int] NULL,
		[StateId]                        [uniqueidentifier] NOT NULL,
		[ProntoBranchCode]               [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]                       [bit] NOT NULL,
		[IsDeleted]                      [bit] NOT NULL,
		[CreatedDate]                    [datetime] NOT NULL,
		[LastModifiedDate]               [datetime] NOT NULL,
		[CouponPrefixNumber]             [char](1) COLLATE Latin1_General_CI_AS NULL,
		[OneOffPrefixNumber]             [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		[OneOffLastSeqNumber]            [int] NULL,
		[OneOffNotificationEmail]        [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[OneOffSatchelPrefixNumber]      [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		[OneOffSatchelLastSeqNumber]     [int] NULL,
		CONSTRAINT [PK_Branch]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Branch]
	ADD
	CONSTRAINT [DF_Branch_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Branch]
	ADD
	CONSTRAINT [DF_Branch_IsActive]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Branch]
	ADD
	CONSTRAINT [DF_Branch_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Branch]
	ADD
	CONSTRAINT [DF_Branch_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Branch]
	ADD
	CONSTRAINT [DF_Branch_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Branch]
	WITH CHECK
	ADD CONSTRAINT [FK_Branch_State]
	FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[Branch]
	CHECK CONSTRAINT [FK_Branch_State]

GO
CREATE UNIQUE CLUSTERED INDEX [IX_Branch_Code]
	ON [dbo].[Branch] ([Code])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Branch_IdCosmosBranch]
	ON [dbo].[Branch] ([Id], [CosmosBranch])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Branch] SET (LOCK_ESCALATION = TABLE)
GO
