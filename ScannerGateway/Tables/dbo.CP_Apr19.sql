SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CP_Apr19] (
		[Transaction_ID]           [int] NULL,
		[Line_Item_ID]             [int] NULL,
		[Date]                     [datetime2](7) NULL,
		[Time]                     [datetime2](7) NULL,
		[Time_Zone]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]              [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Credit]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Debit]                    [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Account]                  [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Store_Name]               [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Store_DLB]                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Store_Suburb]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Store_State]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Store_Postcode]           [int] NULL,
		[Store_Type]               [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Consignment_Number]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sender_Suburb]            [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Sender_State]             [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Sender_Postcode]          [nvarchar](1) COLLATE Latin1_General_CI_AS NULL,
		[Destination_Suburb]       [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Destination_State]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Destination_Postcode]     [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Destination_Country]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Total_Weight__kg_]        [nvarchar](1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CP_Apr19] SET (LOCK_ESCALATION = TABLE)
GO
