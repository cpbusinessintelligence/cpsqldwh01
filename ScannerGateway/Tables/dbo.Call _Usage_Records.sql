SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Call _Usage_Records] (
		[Bill Date]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Account Number]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Account Descriptor]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Node Name]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service Descriptor]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Call Date]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Call Time]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Number Dialled]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Call Origin]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Call Destination]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Duration(hh mm ss)]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Volume (Mb)]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[GST Flag]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Call Charge $]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Call Type]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Product name]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Service Description]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Rate Flag]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Hierarchy Name]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 19]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 20]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 21]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 22]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 23]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 24]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 25]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 26]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 27]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 28]               [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Call _Usage_Records] SET (LOCK_ESCALATION = TABLE)
GO
