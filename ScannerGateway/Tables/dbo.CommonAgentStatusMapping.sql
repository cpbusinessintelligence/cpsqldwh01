SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommonAgentStatusMapping] (
		[AgentStatus]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Exception_Reason]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CommonAgentStatusMapping] SET (LOCK_ESCALATION = TABLE)
GO
