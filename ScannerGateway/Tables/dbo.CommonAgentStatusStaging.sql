SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommonAgentStatusStaging] (
		[sno]              [int] IDENTITY(1, 1) NOT NULL,
		[ItemNumber]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Consignment]      [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Status]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Date]             [date] NULL,
		[Time]             [time](7) NULL,
		[ReceiverName]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isProcessed]      [bit] NULL,
		[CreatedDate]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[CommonAgentStatusStaging]
	ADD
	CONSTRAINT [DF__CommonSta__isPro__07E124C1]
	DEFAULT ((0)) FOR [isProcessed]
GO
ALTER TABLE [dbo].[CommonAgentStatusStaging]
	ADD
	CONSTRAINT [DF__CommonAge__Creat__08D548FA]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[CommonAgentStatusStaging] SET (LOCK_ESCALATION = TABLE)
GO
