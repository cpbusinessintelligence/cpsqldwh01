SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConsDate] (
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[statusdatetime]        [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ConsDate] SET (LOCK_ESCALATION = TABLE)
GO
