SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConsignmentNumberSamplesBEX] (
		[Consignment]     [varchar](250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ConsignmentNumberSamplesBEX] SET (LOCK_ESCALATION = TABLE)
GO
