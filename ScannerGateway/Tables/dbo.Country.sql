SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                 [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                 [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]             [bit] NOT NULL,
		[IsDeleted]            [bit] NOT NULL,
		[CreatedDate]          [datetime] NOT NULL,
		[LastModifiedDate]     [datetime] NOT NULL,
		CONSTRAINT [PK_Country]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Country]
	ADD
	CONSTRAINT [DF_Country_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Country]
	ADD
	CONSTRAINT [DF_Country_IsActive]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Country]
	ADD
	CONSTRAINT [DF_Country_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Country]
	ADD
	CONSTRAINT [DF_Country_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Country]
	ADD
	CONSTRAINT [DF_Country_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Country] SET (LOCK_ESCALATION = TABLE)
GO
