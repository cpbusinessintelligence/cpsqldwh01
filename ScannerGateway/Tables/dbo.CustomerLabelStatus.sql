SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerLabelStatus] (
		[Sno]                  [int] IDENTITY(1, 1) NOT NULL,
		[Consignment]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Label]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ActualStatus]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MappedStatuscode]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[StatusDatetime]       [datetime] NULL,
		[ExceptionReason]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PODName]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]          [bit] NULL,
		[CreatedDatetime]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[CustomerLabelStatus]
	ADD
	CONSTRAINT [DF__CustomerS__ispro__4EA8A765]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[CustomerLabelStatus]
	ADD
	CONSTRAINT [DF__CustomerS__Creat__4F9CCB9E]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[CustomerLabelStatus] SET (LOCK_ESCALATION = TABLE)
GO
