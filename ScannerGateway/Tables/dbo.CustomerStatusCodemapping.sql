SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerStatusCodemapping] (
		[Sno]                      [int] IDENTITY(1, 1) NOT NULL,
		[Status]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionInformation]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Code]                     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Description]              [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CustomerStatusCodemapping] SET (LOCK_ESCALATION = TABLE)
GO
