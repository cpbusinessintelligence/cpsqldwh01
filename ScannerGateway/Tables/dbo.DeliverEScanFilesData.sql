SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliverEScanFilesData] (
		[TrackingNumber]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ScanEvent]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CPScanEvent]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SourceFileName]        [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DeliverEScanFilesData] SET (LOCK_ESCALATION = TABLE)
GO
