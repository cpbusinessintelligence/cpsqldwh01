SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliveredData] (
		[LabelNumber]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Description]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PUDateTime]         [datetime] NULL,
		[PUBranch]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PUDriver]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PUETAZone]          [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[AttDelDateTime]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AttDelBranch]       [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AttDelDriver]       [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[AttDelETAZone]      [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[RedeliveryCard]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[DelDateTime]        [datetime] NULL,
		[DelBranch]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DelDriver]          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DelETAZone]         [varchar](10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DeliveredData] SET (LOCK_ESCALATION = TABLE)
GO
