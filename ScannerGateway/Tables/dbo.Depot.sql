SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Depot] (
		[Id]                        [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                      [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                      [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[BranchId]                  [uniqueidentifier] NOT NULL,
		[StateId]                   [uniqueidentifier] NOT NULL,
		[CosmosDepotCode]           [int] NULL,
		[IsProntoExport]            [bit] NULL,
		[ProntoExportStartDate]     [date] NULL,
		[IsActive]                  [bit] NOT NULL,
		[IsDeleted]                 [bit] NOT NULL,
		[CreatedDate]               [datetime] NOT NULL,
		[LastModifiedDate]          [datetime] NOT NULL,
		CONSTRAINT [PK_Depot]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Depot]
	ADD
	CONSTRAINT [DF_Depot_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Depot]
	ADD
	CONSTRAINT [DF_Depot_IsProntoExport]
	DEFAULT ((0)) FOR [IsProntoExport]
GO
ALTER TABLE [dbo].[Depot]
	ADD
	CONSTRAINT [DF_Depot_IsActive]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Depot]
	ADD
	CONSTRAINT [DF_Depot_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Depot]
	ADD
	CONSTRAINT [DF_Depot_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Depot]
	ADD
	CONSTRAINT [DF_Depot_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Depot]
	WITH CHECK
	ADD CONSTRAINT [FK_Depot_State]
	FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[Depot]
	CHECK CONSTRAINT [FK_Depot_State]

GO
ALTER TABLE [dbo].[Depot]
	WITH CHECK
	ADD CONSTRAINT [FK_Depot_Branch]
	FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id])
ALTER TABLE [dbo].[Depot]
	CHECK CONSTRAINT [FK_Depot_Branch]

GO
CREATE UNIQUE CLUSTERED INDEX [IX_Depot_Code]
	ON [dbo].[Depot] ([Code])
	ON [PRIMARY]
GO
GRANT DELETE
	ON [dbo].[Depot]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[Depot]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[Depot]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[Depot]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[Depot] SET (LOCK_ESCALATION = TABLE)
GO
