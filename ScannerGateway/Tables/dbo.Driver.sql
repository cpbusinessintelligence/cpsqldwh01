SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Driver] (
		[Id]                     [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                   [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                   [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[DepotId]                [uniqueidentifier] NULL,
		[FirstName]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LastName]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverTypeId]           [uniqueidentifier] NULL,
		[ProntoDriverCode]       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IsActive]               [bit] NOT NULL,
		[IsContractor]           [bit] NOT NULL,
		[DriverStartDate]        [date] NULL,
		[DriverEndDate]          [date] NULL,
		[ContractTypeId]         [uniqueidentifier] NULL,
		[ContractReviewDate]     [date] NULL,
		[ContractExpiryDate]     [date] NULL,
		[BranchId]               [uniqueidentifier] NULL,
		[IsDeleted]              [bit] NOT NULL,
		[CreatedDate]            [datetime] NOT NULL,
		[LastModifiedDate]       [datetime] NOT NULL,
		[EffectiveDate]          [datetime] NOT NULL,
		CONSTRAINT [PK_Driver]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_IsActive]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_IsContractor]
	DEFAULT ((0)) FOR [IsContractor]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[Driver]
	ADD
	CONSTRAINT [DF_Driver_EffectiveDate]
	DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [dbo].[Driver]
	WITH CHECK
	ADD CONSTRAINT [FK_Driver_Depot]
	FOREIGN KEY ([DepotId]) REFERENCES [dbo].[Depot] ([Id])
ALTER TABLE [dbo].[Driver]
	CHECK CONSTRAINT [FK_Driver_Depot]

GO
ALTER TABLE [dbo].[Driver]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Driver_Branch]
	FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id])
ALTER TABLE [dbo].[Driver]
	CHECK CONSTRAINT [FK_Driver_Branch]

GO
CREATE UNIQUE CLUSTERED INDEX [IX_Driver]
	ON [dbo].[Driver] ([Code], [BranchId], [EffectiveDate], [IsActive])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Driver_CodeProntoEffectiveBranch]
	ON [dbo].[Driver] ([Code], [ProntoDriverCode], [BranchId], [EffectiveDate])
	INCLUDE ([Id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Driver_IdProntoCode]
	ON [dbo].[Driver] ([ProntoDriverCode], [IsActive], [IsDeleted])
	INCLUDE ([Id], [Code], [Name], [DepotId], [DriverTypeId], [BranchId], [EffectiveDate])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Driver_BranchId]
	ON [dbo].[Driver] ([BranchId])
	ON [PRIMARY]
GO
GRANT DELETE
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[Driver]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[Driver] SET (LOCK_ESCALATION = TABLE)
GO
