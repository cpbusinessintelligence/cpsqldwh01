SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ErrorLog] (
		[Id]                 [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[ErrorNumber]        [int] NULL,
		[ErrorSeverity]      [int] NULL,
		[ErrorState]         [int] NULL,
		[ErrorProcedure]     [nvarchar](128) COLLATE Latin1_General_CI_AS NULL,
		[ErrorLine]          [int] NULL,
		[ErrorMessage]       [nvarchar](4000) COLLATE Latin1_General_CI_AS NULL,
		[CreateDate]         [datetime] NULL,
		CONSTRAINT [PK_cppl_ScannerGW_ErrorLog]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[ErrorLog]
	ADD
	CONSTRAINT [DF_cppl_ErrorLog_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[ErrorLog]
	ADD
	CONSTRAINT [DF_cppl_ErrorLog_CreateDate]
	DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[ErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
