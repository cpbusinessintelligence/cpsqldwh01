SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventType] (
		[Id]                        [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                      [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]               [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsWebEventType]            [bit] NOT NULL,
		[HasName]                   [bit] NOT NULL,
		[HasImage]                  [bit] NOT NULL,
		[HasReason]                 [bit] NOT NULL,
		[HasComment]                [bit] NOT NULL,
		[HasNumOfItems]             [bit] NOT NULL,
		[HasDeliveryLocation]       [bit] NOT NULL,
		[ForceName]                 [bit] NOT NULL,
		[ForceImage]                [bit] NOT NULL,
		[ForceReason]               [bit] NOT NULL,
		[ForceComment]              [bit] NOT NULL,
		[ForceNumOfItems]           [bit] NOT NULL,
		[ForceDeliveryLocation]     [bit] NOT NULL,
		[ProntoTransactionType]     [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]                 [bit] NOT NULL,
		[CreatedDate]               [datetime] NOT NULL,
		[LastModifiedDate]          [datetime] NOT NULL,
		CONSTRAINT [PK_EventType]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_IsWebEventType]
	DEFAULT ((0)) FOR [IsWebEventType]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_HasName]
	DEFAULT ((0)) FOR [HasName]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_HasImage]
	DEFAULT ((0)) FOR [HasImage]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_HasReason]
	DEFAULT ((0)) FOR [HasReason]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_HasComment]
	DEFAULT ((0)) FOR [HasComment]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_HasNumOfItems]
	DEFAULT ((0)) FOR [HasNumOfItems]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_HasDeliveryLocation]
	DEFAULT ((0)) FOR [HasDeliveryLocation]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_ForceName]
	DEFAULT ((0)) FOR [ForceName]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_ForceImage]
	DEFAULT ((0)) FOR [ForceImage]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_ForceReason]
	DEFAULT ((0)) FOR [ForceReason]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_ForceComment]
	DEFAULT ((0)) FOR [ForceComment]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_ForceNumOfItems]
	DEFAULT ((0)) FOR [ForceNumOfItems]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_ForceDeliveryLocation]
	DEFAULT ((0)) FOR [ForceDeliveryLocation]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EventType]
	ADD
	CONSTRAINT [DF_EventTypes_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_EventType_Code]
	ON [dbo].[EventType] ([Code])
	ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[EventType]
	TO [DataFactoryUser]
GO
GRANT DELETE
	ON [dbo].[EventType]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[EventType]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[EventType]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[EventType]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[EventType] SET (LOCK_ESCALATION = TABLE)
GO
