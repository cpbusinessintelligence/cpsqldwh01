SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventTypeMap] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                 [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Description]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SourceSystem]         [uniqueidentifier] NOT NULL,
		[EventTypeId]          [uniqueidentifier] NULL,
		[IsDeleted]            [bit] NOT NULL,
		[CreatedDate]          [datetime] NOT NULL,
		[LastModifiedDate]     [datetime] NOT NULL,
		CONSTRAINT [PK_EventTypeMap]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[EventTypeMap]
	ADD
	CONSTRAINT [DF_EventTypeMap_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[EventTypeMap]
	ADD
	CONSTRAINT [DF_EventTypeMap_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[EventTypeMap]
	ADD
	CONSTRAINT [DF_EventTypeMap_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EventTypeMap]
	ADD
	CONSTRAINT [DF_EventTypeMap_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[EventTypeMap]
	WITH CHECK
	ADD CONSTRAINT [FK_EventTypeMap_EventType]
	FOREIGN KEY ([EventTypeId]) REFERENCES [dbo].[EventType] ([Id])
ALTER TABLE [dbo].[EventTypeMap]
	CHECK CONSTRAINT [FK_EventTypeMap_EventType]

GO
CREATE NONCLUSTERED INDEX [idx_EventTypeMap_CodeType]
	ON [dbo].[EventTypeMap] ([Code], [EventTypeId])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_EventTypeMap_TypeIdCodeIsDeleted]
	ON [dbo].[EventTypeMap] ([Code], [EventTypeId], [IsDeleted])
	INCLUDE ([Id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventTypeMap] SET (LOCK_ESCALATION = TABLE)
GO
