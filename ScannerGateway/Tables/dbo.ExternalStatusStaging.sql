SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExternalStatusStaging] (
		[sno]                   [int] IDENTITY(1, 1) NOT NULL,
		[TrackingNumber]        [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]     [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]        [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanEvent]             [nvarchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]            [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [nvarchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Category]              [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]           [bit] NULL
)
GO
ALTER TABLE [dbo].[ExternalStatusStaging]
	ADD
	CONSTRAINT [DF__ExternalS__ispro__373B3228]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[ExternalStatusStaging] SET (LOCK_ESCALATION = TABLE)
GO
