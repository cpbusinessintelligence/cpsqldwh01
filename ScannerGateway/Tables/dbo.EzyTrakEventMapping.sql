SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EzyTrakEventMapping] (
		[Code]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Description]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EzyTrakStatus]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EzyTrakReasoncode]     [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[ReasonDescription]     [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Createdby]             [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]       [datetime] NULL,
		[EditedBy]              [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]        [datetime] NULL
)
GO
ALTER TABLE [dbo].[EzyTrakEventMapping]
	ADD
	CONSTRAINT [DF__EzyTrakEv__Creat__4865BE2A]
	DEFAULT ('Admin') FOR [Createdby]
GO
ALTER TABLE [dbo].[EzyTrakEventMapping]
	ADD
	CONSTRAINT [DF__EzyTrakEv__Creat__4959E263]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[EzyTrakEventMapping]
	ADD
	CONSTRAINT [DF__EzyTrakEv__Edite__4A4E069C]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[EzyTrakEventMapping]
	ADD
	CONSTRAINT [DF__EzyTrakEv__Edite__4B422AD5]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[EzyTrakEventMapping] SET (LOCK_ESCALATION = TABLE)
GO
