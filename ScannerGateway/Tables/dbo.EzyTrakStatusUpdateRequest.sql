SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EzyTrakStatusUpdateRequest] (
		[Sno]                       [int] IDENTITY(1, 1) NOT NULL,
		[Sourcereference]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Status]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatuscodeDescription]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusDate]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Reasoncode]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ReasonDescription]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AgentId]                   [varchar](15) COLLATE Latin1_General_CI_AS NOT NULL,
		[isprocessed]               [bit] NULL,
		[Accountnumber]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK__EzyTrakS__CA1FE464C73F0EF0]
		PRIMARY KEY
		CLUSTERED
		([Sno])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[EzyTrakStatusUpdateRequest]
	ADD
	CONSTRAINT [DF__EzyTrakSt__ispro__3DB3258D]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[EzyTrakStatusUpdateRequest] SET (LOCK_ESCALATION = TABLE)
GO
