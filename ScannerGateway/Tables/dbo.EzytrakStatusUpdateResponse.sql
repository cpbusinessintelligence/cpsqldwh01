SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EzytrakStatusUpdateResponse] (
		[Result]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TrackingNumber]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Error]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Createdby]           [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[EditedBy]            [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[EzytrakStatusUpdateResponse]
	ADD
	CONSTRAINT [DF__EzytrakSt__Creat__42ACE4D4]
	DEFAULT ('Admin') FOR [Createdby]
GO
ALTER TABLE [dbo].[EzytrakStatusUpdateResponse]
	ADD
	CONSTRAINT [DF__EzytrakSt__Creat__43A1090D]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[EzytrakStatusUpdateResponse]
	ADD
	CONSTRAINT [DF__EzytrakSt__Edite__44952D46]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[EzytrakStatusUpdateResponse]
	ADD
	CONSTRAINT [DF__EzytrakSt__Edite__4589517F]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
ALTER TABLE [dbo].[EzytrakStatusUpdateResponse] SET (LOCK_ESCALATION = TABLE)
GO
