SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FreightExceptionReport_Microsoft] (
		[Connote]                                  [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Label]                                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Reference]                                [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[ConNoteDate]                              [date] NULL,
		[QueryCage]                                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Name]                                     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAccount]                          [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CustomerName]                             [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryName]                             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionalStatusDate]                    [datetime] NULL,
		[ScannedBranch]                            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SendingBranch]                            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PickupDateFromQSWarehouse]                [datetime] NULL,
		[LinkCardNumber]                           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LinkCardDate]                             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ETADeliveryDate]                          [date] NULL,
		[ReDeliveryDateArrangedbyCustomers]        [date] NULL,
		[ExceptionalStatusForRedelivery]           [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[DateofExceptionalStatusForRedelivery]     [datetime] NULL,
		[EstimatedRTSDate]                         [date] NULL,
		[createddate]                              [datetime] NULL
)
GO
ALTER TABLE [dbo].[FreightExceptionReport_Microsoft] SET (LOCK_ESCALATION = TABLE)
GO
