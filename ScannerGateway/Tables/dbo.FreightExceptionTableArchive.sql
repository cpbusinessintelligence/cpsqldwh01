SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FreightExceptionTableArchive] (
		[ConNote]                   [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Label]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Reference]                 [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[ConNoteDate]               [date] NULL,
		[QueryCage]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Name]                      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CustomerAccount]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[CustomerName]              [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[ParentCustomerAccount]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryName]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DateScanned]               [datetime] NULL,
		[ScannedBranch]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SendingBranch]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LinkCardNumber]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[LinkCardDate]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Createddate]               [datetime] NULL,
		[DeliveryContactPhone]      [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryEmail]             [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FreightExceptionTableArchive] SET (LOCK_ESCALATION = TABLE)
GO
