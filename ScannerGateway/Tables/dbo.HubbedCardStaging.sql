SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HubbedCardStaging] (
		[Sno]                  [int] IDENTITY(1, 1) NOT NULL,
		[HubbedCard]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PrimaryCoupon]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDatetime]     [datetime] NULL,
		[isdelivered]          [bit] NULL,
		[DeliveryLocation]     [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]          [bit] NULL,
		[CreatedBy]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]      [datetime] NULL,
		[EditedBy]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EditedDateTime]       [datetime] NULL
)
GO
ALTER TABLE [dbo].[HubbedCardStaging]
	ADD
	CONSTRAINT [DF__HubbedCar__isdel__603D47BB]
	DEFAULT ((0)) FOR [isdelivered]
GO
ALTER TABLE [dbo].[HubbedCardStaging]
	ADD
	CONSTRAINT [DF__HubbedCar__ispro__61316BF4]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[HubbedCardStaging]
	ADD
	CONSTRAINT [DF__HubbedCar__Creat__6225902D]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[HubbedCardStaging]
	ADD
	CONSTRAINT [DF__HubbedCar__Creat__6319B466]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[HubbedCardStaging]
	ADD
	CONSTRAINT [DF__HubbedCar__Edite__640DD89F]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[HubbedCardStaging]
	ADD
	CONSTRAINT [DF__HubbedCar__Edite__6501FCD8]
	DEFAULT (getdate()) FOR [EditedDateTime]
GO
ALTER TABLE [dbo].[HubbedCardStaging] SET (LOCK_ESCALATION = TABLE)
GO
