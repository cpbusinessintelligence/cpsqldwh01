SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HubbedStaging] (
		[SNo]                 [int] IDENTITY(1, 1) NOT NULL,
		[TrackingNumber]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanTime]            [datetime] NULL,
		[ScanType]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AgentId]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AgentName]           [varchar](6000) COLLATE Latin1_General_CI_AS NULL,
		[SignedName]          [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[SignaturePoints]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[HubbedCard]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Createdby]           [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[Editedby]            [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[isprocessed]         [int] NULL,
		[ReadytoSendMail]     [bit] NULL,
		[ReadytoSendSMS]      [bit] NULL,
		[AccessPin]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Jobnumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isupdateJob]         [bit] NULL
)
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__ispro__00750D23]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__Ready__3F9B6DFF]
	DEFAULT ((0)) FOR [ReadytoSendMail]
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__Ready__408F9238]
	DEFAULT ((0)) FOR [ReadytoSendSMS]
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__isupd__6B79F03D]
	DEFAULT ((0)) FOR [isupdateJob]
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__Creat__6D9742D9]
	DEFAULT ('Admin') FOR [Createdby]
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__Creat__6E8B6712]
	DEFAULT (getdate()) FOR [CreatedDatetime]
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__Edite__6F7F8B4B]
	DEFAULT ('Admin') FOR [Editedby]
GO
ALTER TABLE [dbo].[HubbedStaging]
	ADD
	CONSTRAINT [DF__HubbedSta__Edite__7073AF84]
	DEFAULT (getdate()) FOR [EditedDatetime]
GO
CREATE NONCLUSTERED INDEX [idx_HubbedStaging_sno]
	ON [dbo].[HubbedStaging] ([SNo])
	INCLUDE ([TrackingNumber])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[HubbedStaging] SET (LOCK_ESCALATION = TABLE)
GO
