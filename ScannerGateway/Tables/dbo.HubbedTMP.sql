SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HubbedTMP] (
		[Store Name]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store Account]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store Suburb]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store State]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store Postcode]         [float] NULL,
		[Store Type]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Store DLB]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Consignment Number]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Courier]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Check-in time]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[HubbedTMP] SET (LOCK_ESCALATION = TABLE)
GO
