SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImportTrackingEvents] (
		[TrackingNumber]        [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]        [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[ScanEvent]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]           [bit] NOT NULL,
		[AgentName]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Addwho]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AddDateTime]           [datetime] NULL,
		[Editwho]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EditDateTime]          [datetime] NULL
)
GO
ALTER TABLE [dbo].[ImportTrackingEvents] SET (LOCK_ESCALATION = TABLE)
GO
