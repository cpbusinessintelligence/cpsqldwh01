SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_TEST] (
		["LabelNumber"]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		["ActualStatus"]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		["StatusDateTime"]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		["MappedStatusCode"]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		["PODName"]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		["ExceptionReason"]      [varchar](250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[JP_TEST] SET (LOCK_ESCALATION = TABLE)
GO
