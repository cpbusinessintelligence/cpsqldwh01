SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JustfreightPODStatus] (
		[FileNo]          [int] IDENTITY(1, 1) NOT NULL,
		[FileName]        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[JustfreightPODStatus]
	ADD
	CONSTRAINT [DF__Justfreig__Creat__100C566E]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[JustfreightPODStatus] SET (LOCK_ESCALATION = TABLE)
GO
