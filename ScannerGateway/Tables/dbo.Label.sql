SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Label] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[LabelNumber]          [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDate]          [datetime] NOT NULL,
		[LastModifiedDate]     [datetime] NOT NULL,
		CONSTRAINT [PK_Label]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [ARCHIVE]
)
GO
ALTER TABLE [dbo].[Label]
	ADD
	CONSTRAINT [DF_Label_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Label]
	ADD
	CONSTRAINT [DF_Label_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Label]
	ADD
	CONSTRAINT [DF_Label_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_Label_LabelNumber]
	ON [dbo].[Label] ([LabelNumber])
	ON [ARCHIVE]
GO
CREATE NONCLUSTERED INDEX [idx_Label_IdNumber]
	ON [dbo].[Label] ([Id], [LabelNumber])
	ON [ARCHIVE]
GO
GRANT ALTER
	ON [dbo].[Label]
	TO [DataFactoryUser]
GO
GRANT DELETE
	ON [dbo].[Label]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[Label]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[Label]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[Label] SET (LOCK_ESCALATION = TABLE)
GO
