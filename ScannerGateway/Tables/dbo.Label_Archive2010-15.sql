SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Label_Archive2010-15] (
		[Id]                   [uniqueidentifier] NULL,
		[LabelNumber]          [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDate]          [datetime] NOT NULL,
		[LastModifiedDate]     [datetime] NOT NULL
)
GO
CREATE NONCLUSTERED INDEX [idx_LabelArchive_IdNumber]
	ON [dbo].[Label_Archive2010-15] ([Id], [LabelNumber])
	ON [ARCHIVE]
GO
ALTER TABLE [dbo].[Label_Archive2010-15] SET (LOCK_ESCALATION = TABLE)
GO
