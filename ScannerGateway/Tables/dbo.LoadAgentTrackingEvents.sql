SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoadAgentTrackingEvents] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[AgentName]             [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[TrackingNumber]        [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]     [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]        [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[ScanEvent]             [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]            [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]           [bit] NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		CONSTRAINT [PK_LoadAgentTrackingEvents]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[LoadAgentTrackingEvents]
	ADD
	CONSTRAINT [DF__LoadAgent__IsPro__131DCD43]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[LoadAgentTrackingEvents]
	ADD
	CONSTRAINT [DF__LoadAgent__Creat__1411F17C]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[LoadAgentTrackingEvents]
	ADD
	CONSTRAINT [DF__LoadAgent__Updat__150615B5]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[LoadAgentTrackingEvents] SET (LOCK_ESCALATION = TABLE)
GO
