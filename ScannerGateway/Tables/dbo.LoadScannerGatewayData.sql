SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoadScannerGatewayData] (
		[Id]                        [int] IDENTITY(1, 1) NOT NULL,
		[LabelNumber]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TimeStamp]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Action]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Items]                     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Driver]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ProntoWarehouseID]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TransferDriverNumber]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LocationID]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LocationName]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LocationCustomer]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TransitID]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[LinkCoupon]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ManuallyEntered]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CodTagged]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[MultiCouponAreaTagged]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SignaturePresent]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SignatureName]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Exception]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SignatureID]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ErrorFlag]                 [bit] NULL,
		[ErrorReason]               [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_LoadScannerGatewayData3]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[LoadScannerGatewayData] SET (LOCK_ESCALATION = TABLE)
GO
