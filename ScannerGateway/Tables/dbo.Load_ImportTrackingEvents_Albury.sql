SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Load_ImportTrackingEvents_Albury] (
		[TrackingNumber]        [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]        [varchar](30) COLLATE Latin1_General_CI_AS NOT NULL,
		[ScanEvent]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Load_ImportTrackingEvents_Albury] SET (LOCK_ESCALATION = TABLE)
GO
