SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManualInvoicePP] (
		[LabelNumber]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Custmer]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Status]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Branch]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[junk]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ManualInvoicePP] SET (LOCK_ESCALATION = TABLE)
GO
