SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManualInvoiceTemp] (
		[LabelNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Customer]         [varchar](120) COLLATE Latin1_General_CI_AS NULL,
		[Date]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Branch]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Junk]             [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ManualInvoiceTemp] SET (LOCK_ESCALATION = TABLE)
GO
