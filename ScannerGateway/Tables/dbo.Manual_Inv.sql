SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Manual_Inv] (
		[LabelNumber]      [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Customer]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Date]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Status]           [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Branch]           [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]     [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Manual_Inv] SET (LOCK_ESCALATION = TABLE)
GO
