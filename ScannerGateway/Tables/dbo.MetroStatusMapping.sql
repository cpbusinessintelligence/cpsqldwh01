SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MetroStatusMapping] (
		[Event_Type]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Exception_Reason]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]               [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[MetroStatusMapping] SET (LOCK_ESCALATION = TABLE)
GO
