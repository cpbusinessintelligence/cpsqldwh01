SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MetroStatusStaing] (
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DateTime]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReceivedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]           [datetime] NULL,
		[UpdatedDate]           [datetime] NULL,
		[Sno]                   [int] IDENTITY(1, 1) NOT NULL,
		[isprocessed]           [bit] NULL
)
GO
ALTER TABLE [dbo].[MetroStatusStaing]
	ADD
	CONSTRAINT [DF__MetroStat__ispro__658C0CBD]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[MetroStatusStaing] SET (LOCK_ESCALATION = TABLE)
GO
