SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MidStatusStaging] (
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DateTime]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReceivedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]           [datetime] NULL,
		[UpdatedDate]           [datetime] NULL,
		[Sno]                   [int] IDENTITY(1, 1) NOT NULL,
		[isprocessed]           [bit] NULL
)
GO
ALTER TABLE [dbo].[MidStatusStaging]
	ADD
	CONSTRAINT [DF__MidStatus__ispro__17236851]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[MidStatusStaging] SET (LOCK_ESCALATION = TABLE)
GO
