SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MidStatusStaing_Error] (
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DateTime]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReceivedBy]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]           [datetime] NULL,
		[UpdatedDate]           [datetime] NULL,
		[isprocessed]           [bit] NULL,
		[Sno]                   [int] IDENTITY(1, 1) NOT NULL
)
GO
ALTER TABLE [dbo].[MidStatusStaing_Error] SET (LOCK_ESCALATION = TABLE)
GO
