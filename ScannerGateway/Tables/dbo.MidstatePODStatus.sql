SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MidstatePODStatus] (
		[FileNo]          [int] IDENTITY(1, 1) NOT NULL,
		[FileName]        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[MidstatePODStatus]
	ADD
	CONSTRAINT [DF__MidstateP__Creat__6DEC4894]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MidstatePODStatus] SET (LOCK_ESCALATION = TABLE)
GO
