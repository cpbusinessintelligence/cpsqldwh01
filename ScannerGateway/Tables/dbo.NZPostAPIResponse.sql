SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NZPostAPIResponse] (
		[Sno]                  [int] IDENTITY(1, 1) NOT NULL,
		[LabelNumber]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventCode]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Occuredat]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[actionrequiredby]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[shortdescription]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[longdescription]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[isprocessed]          [bit] NULL,
		[createdby]            [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[createddatetime]      [datetime] NULL,
		[editedby]             [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[editeddatetime]       [datetime] NULL
)
GO
ALTER TABLE [dbo].[NZPostAPIResponse]
	ADD
	CONSTRAINT [DF__NZPostAPI__ispro__65370702]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[NZPostAPIResponse]
	ADD
	CONSTRAINT [DF__NZPostAPI__creat__681373AD]
	DEFAULT ('Admin') FOR [createdby]
GO
ALTER TABLE [dbo].[NZPostAPIResponse]
	ADD
	CONSTRAINT [DF__NZPostAPI__creat__690797E6]
	DEFAULT (getdate()) FOR [createddatetime]
GO
ALTER TABLE [dbo].[NZPostAPIResponse]
	ADD
	CONSTRAINT [DF__NZPostAPI__edite__69FBBC1F]
	DEFAULT ('Admin') FOR [editedby]
GO
ALTER TABLE [dbo].[NZPostAPIResponse]
	ADD
	CONSTRAINT [DF__NZPostAPI__edite__6AEFE058]
	DEFAULT (getdate()) FOR [editeddatetime]
GO
ALTER TABLE [dbo].[NZPostAPIResponse] SET (LOCK_ESCALATION = TABLE)
GO
