SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NowgoDrivers] (
		[DriverID]         [int] IDENTITY(1, 1) NOT NULL,
		[DriverNo]         [int] NULL,
		[Branch]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DriverExtRef]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[NowgoDrivers]
	ADD
	CONSTRAINT [DF__NowgoDriv__Creat__611C5D5B]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[NowgoDrivers] SET (LOCK_ESCALATION = TABLE)
GO
