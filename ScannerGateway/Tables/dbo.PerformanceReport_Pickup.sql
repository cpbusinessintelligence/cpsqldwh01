SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PerformanceReport_Pickup] (
		[EventDate]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[EventTime]           [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[SourceReference]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DriverId]            [uniqueidentifier] NULL,
		[DriverCode]          [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[cd_connote]          [varchar](32) COLLATE Latin1_General_CI_AS NOT NULL,
		[FromZone]            [varchar](800) COLLATE Latin1_General_CI_AS NULL,
		[ToZone]              [varchar](800) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PerformanceReport_Pickup] SET (LOCK_ESCALATION = TABLE)
GO
