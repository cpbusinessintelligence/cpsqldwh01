SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PremonitionPODStatus] (
		[FileNo]          [int] IDENTITY(1, 1) NOT NULL,
		[FileName]        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[PremonitionPODStatus]
	ADD
	CONSTRAINT [DF__Premoniti__Creat__664B26CC]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PremonitionPODStatus] SET (LOCK_ESCALATION = TABLE)
GO
