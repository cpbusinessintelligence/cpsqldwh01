SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RP COupons] (
		[LabelNumber]                     [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[datetimer]                       [nvarchar](max) COLLATE Latin1_General_CI_AS NOT NULL,
		[RP_onboard_scan]                 [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Link_Scan]                       [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Metro__coupon_Linked]            [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Metro_coupon_delivery_event]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RP COupons] SET (LOCK_ESCALATION = TABLE)
GO
