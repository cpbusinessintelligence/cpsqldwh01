SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPs] (
		[Column 0]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 1]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 2]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 3]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 4]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 5]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 6]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 7]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 8]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 9]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 10]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 11]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Column 12]     [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RPs] SET (LOCK_ESCALATION = TABLE)
GO
