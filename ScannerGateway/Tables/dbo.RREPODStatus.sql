SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RREPODStatus] (
		[FileNo]          [int] IDENTITY(1, 1) NOT NULL,
		[FileName]        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[RREPODStatus]
	ADD
	CONSTRAINT [DF__RREPODSta__Creat__76818E95]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RREPODStatus] SET (LOCK_ESCALATION = TABLE)
GO
