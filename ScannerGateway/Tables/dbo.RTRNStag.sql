SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RTRNStag] (
		[SNo]                 [int] IDENTITY(1, 1) NOT NULL,
		[TrackingNumber]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanTime]            [datetime] NULL,
		[ScanType]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AgentId]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AgentName]           [varchar](6000) COLLATE Latin1_General_CI_AS NULL,
		[SignedName]          [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[SignaturePoints]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[HubbedCard]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Createdby]           [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[Editedby]            [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[isprocessed]         [int] NULL,
		[ReadytoSendMail]     [bit] NULL,
		[ReadytoSendSMS]      [bit] NULL,
		[AccessPin]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Jobnumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isupdateJob]         [bit] NULL
)
GO
ALTER TABLE [dbo].[RTRNStag] SET (LOCK_ESCALATION = TABLE)
GO
