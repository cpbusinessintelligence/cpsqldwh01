SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Recreated Data] (
		[Connote]              [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Status]               [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Receiver Address]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Address ]             [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDriver]       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[DeliverySuburb]       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryPostcode]     [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Item]                 [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Label]                [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Reference]            [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Weight]               [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Cubic]                [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Company name]         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress1]       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress2]       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupAddress3]       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupSuburb]         [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[PickupPostcode]       [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Service]              [varchar](200) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Recreated Data] SET (LOCK_ESCALATION = TABLE)
GO
