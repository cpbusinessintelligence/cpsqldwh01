SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Report_RightwrongScans] (
		[Branch]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverNumber]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RightProntoId]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanType]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]      [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanDate]               [datetime] NULL,
		[TrackingBranch]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Driver]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[WrongProntoId]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SerialNumber]           [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[PickupContractor]       [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[PickupDate]             [date] NULL,
		[PickupRctiDate]         [date] NULL,
		[PickupRctiAmount]       [money] NULL,
		[DeliveryContractor]     [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]           [date] NULL,
		[DeliveryRctiDate]       [date] NULL,
		[DeliveryRctiAmount]     [money] NULL,
		[incorrectscans]         [varchar](10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Report_RightwrongScans]
	ADD
	CONSTRAINT [DF__Report_Ri__incor__0FEC5ADD]
	DEFAULT ('Y') FOR [incorrectscans]
GO
ALTER TABLE [dbo].[Report_RightwrongScans] SET (LOCK_ESCALATION = TABLE)
GO
