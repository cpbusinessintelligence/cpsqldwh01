SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ResentscansTemp] (
		[Branch]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Drivernumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ScanType]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ScanDate]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Barcode]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Linkbarcode]      [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ResentscansTemp] SET (LOCK_ESCALATION = TABLE)
GO
