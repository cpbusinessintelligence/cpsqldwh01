SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScaanerFeb29temp] (
		[Id]                    [uniqueidentifier] NOT NULL,
		[EventTypeId]           [uniqueidentifier] NOT NULL,
		[EventDateTime]         [datetime] NOT NULL,
		[Description]           [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[LabelId]               [uniqueidentifier] NULL,
		[AgentId]               [uniqueidentifier] NULL,
		[DriverId]              [uniqueidentifier] NULL,
		[RunId]                 [uniqueidentifier] NULL,
		[EventLocation]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AdditionalText1]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AdditionalText2]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NumberOfItems]         [smallint] NULL,
		[SourceId]              [uniqueidentifier] NULL,
		[SourceReference]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CosmosSignatureId]     [int] NULL,
		[CosmosBranchId]        [int] NULL,
		[IsDeleted]             [bit] NOT NULL,
		[CreatedDate]           [datetime] NOT NULL,
		[LastModifiedDate]      [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ScaanerFeb29temp] SET (LOCK_ESCALATION = TABLE)
GO
