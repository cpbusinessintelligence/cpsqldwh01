SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SekoStatusMapping] (
		[code]                [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Description]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ScanEvent]           [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SekoStatusMapping] SET (LOCK_ESCALATION = TABLE)
GO
