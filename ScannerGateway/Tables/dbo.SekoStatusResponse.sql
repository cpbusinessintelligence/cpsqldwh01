SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SekoStatusResponse] (
		[Sno]              [int] IDENTITY(1, 10) NOT NULL,
		[Consignment]      [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Status]           [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Picked]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Delivered]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Tracking]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[EventDT]          [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[code1]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Description1]     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Location]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[part]             [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[Connote]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[isProcessed]      [bit] NULL
)
GO
ALTER TABLE [dbo].[SekoStatusResponse]
	ADD
	CONSTRAINT [DF__SekoStatu__isPro__668030F6]
	DEFAULT ((0)) FOR [isProcessed]
GO
ALTER TABLE [dbo].[SekoStatusResponse] SET (LOCK_ESCALATION = TABLE)
GO
