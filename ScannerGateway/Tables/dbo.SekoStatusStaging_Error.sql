SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SekoStatusStaging_Error] (
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TrackingNum]           [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[TimeStamp]             [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[created]               [datetime] NULL,
		[Description1]          [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SekoStatusStaging_Error]
	ADD
	CONSTRAINT [DF__SekoStatu__creat__68687968]
	DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[SekoStatusStaging_Error] SET (LOCK_ESCALATION = TABLE)
GO
