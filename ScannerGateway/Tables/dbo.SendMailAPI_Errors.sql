SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SendMailAPI_Errors] (
		[ErrorID]              [int] IDENTITY(1, 1) NOT NULL,
		[ErrorPackageName]     [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[ErrorDesc]            [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]          [datetime] NULL
)
GO
ALTER TABLE [dbo].[SendMailAPI_Errors]
	ADD
	CONSTRAINT [DF__SendMailA__Creat__5C5A0A43]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SendMailAPI_Errors] SET (LOCK_ESCALATION = TABLE)
GO
