SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[State] (
		[Id]                   [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[Name]                 [varchar](120) COLLATE Latin1_General_CI_AS NOT NULL,
		[CountryId]            [uniqueidentifier] NOT NULL,
		[IsActive]             [bit] NOT NULL,
		[IsDeleted]            [bit] NOT NULL,
		[CreatedDate]          [datetime] NOT NULL,
		[LastModifiedDate]     [datetime] NOT NULL,
		CONSTRAINT [PK_State]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[State]
	ADD
	CONSTRAINT [DF_State_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[State]
	ADD
	CONSTRAINT [DF_State_IsActive]
	DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[State]
	ADD
	CONSTRAINT [DF_State_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[State]
	ADD
	CONSTRAINT [DF_State_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[State]
	ADD
	CONSTRAINT [DF_State_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[State]
	WITH CHECK
	ADD CONSTRAINT [FK_State_Country]
	FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id])
ALTER TABLE [dbo].[State]
	CHECK CONSTRAINT [FK_State_Country]

GO
ALTER TABLE [dbo].[State] SET (LOCK_ESCALATION = TABLE)
GO
