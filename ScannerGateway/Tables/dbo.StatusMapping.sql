SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatusMapping] (
		[status]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ScanEvent]           [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[StatusMapping] SET (LOCK_ESCALATION = TABLE)
GO
