SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemCodes] (
		[Id]                           [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[Code]                         [int] NOT NULL,
		[Discriminator]                [nvarchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[SystemCodeCategoryTypeId]     [int] NULL,
		[Description]                  [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[FriendlyCode]                 [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[IsDeleted]                    [bit] NOT NULL,
		[CreatedDate]                  [datetime] NOT NULL,
		[LastModifiedDate]             [datetime] NOT NULL,
		CONSTRAINT [PK_SystemCodes]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[SystemCodes]
	ADD
	CONSTRAINT [DF_SystemCodes_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SystemCodes]
	ADD
	CONSTRAINT [DF_SystemCodes_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SystemCodes]
	ADD
	CONSTRAINT [DF_SystemCodes_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SystemCodes]
	ADD
	CONSTRAINT [DF_SystemCodes_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[SystemCodes] SET (LOCK_ESCALATION = TABLE)
GO
