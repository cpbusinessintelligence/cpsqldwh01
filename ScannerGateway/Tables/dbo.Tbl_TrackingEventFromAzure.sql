SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_TrackingEventFromAzure] (
		[EventID]                          [int] NULL,
		[WorkflowType]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Outcome]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Attribute]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[LabelNumber]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Relation]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ItemCount]                        [int] NULL,
		[EventDateTime]                    [datetime] NULL,
		[DriverId]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverExtRef]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DriverRunNumber]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Branch]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeviceId]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Latitude]                         [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Longitude]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SigneeName]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsSuccessful]                     [bit] NULL,
		[NowGoWorkflowCompletionID]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NowGoConsignmentID]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[NowGoSubjectArticleID]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]                      [bit] NULL,
		[CreatedDateTime]                  [datetime] NULL,
		[AttributeValue]                   [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EventCode]                        [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EventDateTimeUTC]                 [datetime] NULL,
		[DLBCode]                          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedeliveryBarCode]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[RedeliveryBarCodeType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReferenceBarcode1]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ReferenceBarcode2]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[SubjectArticleCustomData]         [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[WorkflowCompletionCustomData]     [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[EvidenceCustomData]               [nvarchar](max) COLLATE Latin1_General_CI_AS NULL,
		[CreatedBy]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]                  [datetime] NULL,
		[UpdatedBy]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventJSON]                        [nvarchar](max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Tbl_TrackingEventFromAzure] SET (LOCK_ESCALATION = TABLE)
GO
