SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tempmask] (
		[LabelNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[mask]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[EventDatetime]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[count]             [int] NULL,
		[LabelID]           [varchar](200) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Tempmask] SET (LOCK_ESCALATION = TABLE)
GO
