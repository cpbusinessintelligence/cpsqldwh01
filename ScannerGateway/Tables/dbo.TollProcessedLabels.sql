SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TollProcessedLabels] (
		[sno]                    [int] NULL,
		[consginmentNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TrackingNum]            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ScanDateTime]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Scanevent_original]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[contractor]             [int] NULL,
		[isProcessed]            [bit] NULL
)
GO
ALTER TABLE [dbo].[TollProcessedLabels]
	ADD
	CONSTRAINT [DF__TollProce__isPro__71F1E3A2]
	DEFAULT ((0)) FOR [isProcessed]
GO
ALTER TABLE [dbo].[TollProcessedLabels] SET (LOCK_ESCALATION = TABLE)
GO
