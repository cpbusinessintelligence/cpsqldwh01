SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TollStatusStaging] (
		[Sno]                    [int] IDENTITY(1, 1) NOT NULL,
		[TrackingNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Scanevent]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]            [date] NULL,
		[isProcessed]            [bit] NULL,
		[Scanevent_original]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Toll_Consignment]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Toll_Label]             [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[isImageProcessed]       [bit] NULL
)
GO
ALTER TABLE [dbo].[TollStatusStaging]
	ADD
	CONSTRAINT [DF__TollStatu__isIma__0CA5D9DE]
	DEFAULT ((0)) FOR [isImageProcessed]
GO
ALTER TABLE [dbo].[TollStatusStaging]
	ADD
	CONSTRAINT [DF__TollStatu__creat__6D2D2E85]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[TollStatusStaging]
	ADD
	CONSTRAINT [DF__TollStatu__isPro__6E2152BE]
	DEFAULT ((0)) FOR [isProcessed]
GO
ALTER TABLE [dbo].[TollStatusStaging] SET (LOCK_ESCALATION = TABLE)
GO
