SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TollStatusStating_Error] (
		[ConsignmentNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[TrackingNum]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TimeStamp]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Description1]          [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]           [datetime] NULL
)
GO
ALTER TABLE [dbo].[TollStatusStating_Error]
	ADD
	CONSTRAINT [DF__TollStatu__Creat__6C040022]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TollStatusStating_Error] SET (LOCK_ESCALATION = TABLE)
GO
