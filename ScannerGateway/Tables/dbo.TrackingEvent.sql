SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrackingEvent] (
		[Id]                    [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[EventTypeId]           [uniqueidentifier] NOT NULL,
		[EventDateTime]         [datetime] NOT NULL,
		[Description]           [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[LabelId]               [uniqueidentifier] NULL,
		[AgentId]               [uniqueidentifier] NULL,
		[DriverId]              [uniqueidentifier] NULL,
		[RunId]                 [uniqueidentifier] NULL,
		[EventLocation]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AdditionalText1]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AdditionalText2]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NumberOfItems]         [smallint] NULL,
		[SourceId]              [uniqueidentifier] NULL,
		[SourceReference]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CosmosSignatureId]     [int] NULL,
		[CosmosBranchId]        [int] NULL,
		[IsDeleted]             [bit] NOT NULL,
		[CreatedDate]           [datetime] NOT NULL,
		[LastModifiedDate]      [datetime] NOT NULL,
		CONSTRAINT [PK_TrackingEvent]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [ARCHIVE]
)
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_NumberOfItems]
	DEFAULT ((1)) FOR [NumberOfItems]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[TrackingEvent]
	WITH NOCHECK
	ADD CONSTRAINT [FK_TrackingEvent_Agent]
	FOREIGN KEY ([AgentId]) REFERENCES [dbo].[Agent] ([Id])
	NOT FOR REPLICATION
ALTER TABLE [dbo].[TrackingEvent]
	CHECK CONSTRAINT [FK_TrackingEvent_Agent]

GO
CREATE CLUSTERED INDEX [IX_TrackingEvent_EventDateTime]
	ON [dbo].[TrackingEvent] ([EventDateTime])
	ON [ARCHIVE]
GO
CREATE NONCLUSTERED INDEX [idx_TrackingEvent_LabelDateIsDeleted]
	ON [dbo].[TrackingEvent] ([LabelId], [IsDeleted], [EventTypeId], [EventDateTime], [SourceId])
	INCLUDE ([Id], [DriverId])
	ON [ARCHIVE]
GO
CREATE NONCLUSTERED INDEX [idx_TrackingEvent_EventTypeText1DateTime]
	ON [dbo].[TrackingEvent] ([EventTypeId], [AdditionalText1], [EventDateTime])
	ON [ARCHIVE]
GO
GRANT ALTER
	ON [dbo].[TrackingEvent]
	TO [DataFactoryUser]
GO
GRANT INSERT
	ON [dbo].[TrackingEvent]
	TO [DataFactoryUser]
GO
GRANT SELECT
	ON [dbo].[TrackingEvent]
	TO [DataFactoryUser]
GO
GRANT UPDATE
	ON [dbo].[TrackingEvent]
	TO [DataFactoryUser]
GO
ALTER TABLE [dbo].[TrackingEvent] SET (LOCK_ESCALATION = TABLE)
GO
