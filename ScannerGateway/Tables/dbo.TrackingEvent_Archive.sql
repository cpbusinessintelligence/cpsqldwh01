SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrackingEvent_Archive] (
		[Id]                    [uniqueidentifier] NULL,
		[EventTypeId]           [uniqueidentifier] NULL,
		[EventDateTime]         [datetime] NULL,
		[Description]           [varchar](200) COLLATE Latin1_General_CI_AS NULL,
		[LabelId]               [uniqueidentifier] NULL,
		[AgentId]               [uniqueidentifier] NULL,
		[DriverId]              [uniqueidentifier] NULL,
		[RunId]                 [uniqueidentifier] NULL,
		[EventLocation]         [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AdditionalText1]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AdditionalText2]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[NumberOfItems]         [smallint] NULL,
		[SourceId]              [uniqueidentifier] NULL,
		[SourceReference]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CosmosSignatureId]     [int] NULL,
		[CosmosBranchId]        [int] NULL,
		[IsDeleted]             [bit] NULL,
		[CreatedDate]           [datetime] NULL,
		[LastModifiedDate]      [datetime] NULL
)
GO
CREATE NONCLUSTERED INDEX [idx_TrackingEvent_Archive_EventTypeText1DateTime]
	ON [dbo].[TrackingEvent_Archive] ([EventTypeId], [AdditionalText1], [EventDateTime])
	ON [ARCHIVE]
GO
CREATE NONCLUSTERED INDEX [idx_TrackingEvent_Archive_LabelDateIsDeleted]
	ON [dbo].[TrackingEvent_Archive] ([LabelId], [IsDeleted], [EventTypeId], [EventDateTime], [SourceId])
	INCLUDE ([Id], [DriverId])
	ON [ARCHIVE]
GO
ALTER TABLE [dbo].[TrackingEvent_Archive] SET (LOCK_ESCALATION = TABLE)
GO
