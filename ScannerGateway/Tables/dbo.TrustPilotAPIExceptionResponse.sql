SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrustPilotAPIExceptionResponse] (
		[Sno]              [int] IDENTITY(1, 1) NOT NULL,
		[ConnoteNo]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ResponseCode]     [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]      [datetime] NULL,
		[IsProcessed]      [bit] NULL
)
GO
ALTER TABLE [dbo].[TrustPilotAPIExceptionResponse]
	ADD
	CONSTRAINT [TPDefaultCreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TrustPilotAPIExceptionResponse]
	ADD
	CONSTRAINT [DF__TrustPilo__IsPro__39B18332]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[TrustPilotAPIExceptionResponse] SET (LOCK_ESCALATION = TABLE)
GO
