SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrustPilotAPIExcludedAccounts] (
		[AccountCode]     [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[IsActive]        [int] NOT NULL,
		CONSTRAINT [PK_tblTrustPilotAPIExcludedAccounts]
		PRIMARY KEY
		CLUSTERED
		([AccountCode])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[TrustPilotAPIExcludedAccounts] SET (LOCK_ESCALATION = TABLE)
GO
