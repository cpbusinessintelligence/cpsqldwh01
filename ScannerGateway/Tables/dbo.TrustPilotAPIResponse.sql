SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrustPilotAPIResponse] (
		[Sno]              [int] IDENTITY(1, 1) NOT NULL,
		[ConnoteNo]        [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[ResponseCode]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]      [datetime] NULL,
		[IsProcessed]      [bit] NULL
)
GO
ALTER TABLE [dbo].[TrustPilotAPIResponse]
	ADD
	CONSTRAINT [TrustPilotAPIResponseDefDatetime]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TrustPilotAPIResponse]
	ADD
	CONSTRAINT [DF__TrustPilo__IsPro__442F11A5]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[TrustPilotAPIResponse] SET (LOCK_ESCALATION = TABLE)
GO
