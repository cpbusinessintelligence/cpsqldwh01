SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrustPilotAPIStagingData_Archive] (
		[SenderName]             [varchar](101) COLLATE Latin1_General_CI_AS NULL,
		[Consignment]            [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]           [datetime] NULL,
		[ConsumerName]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[ConsumerEmail]          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryAddress]        [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]               [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Suburb]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[State]                  [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DepotId]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryChoice]         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FranchiseName]          [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[RunNumber]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[SLA]                    [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]            [int] NOT NULL,
		[ResponseCode]           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[SenderEmail]            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]            [datetime] NULL,
		[ResponseUpdateDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[TrustPilotAPIStagingData_Archive] SET (LOCK_ESCALATION = TABLE)
GO
