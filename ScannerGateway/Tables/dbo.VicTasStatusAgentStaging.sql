SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VicTasStatusAgentStaging] (
		[SSC]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Connum]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date]            [date] NULL,
		[Time]            [time](7) NULL,
		[Depot]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]     [date] NULL,
		[isProcessed]     [bit] NULL
)
GO
ALTER TABLE [dbo].[VicTasStatusAgentStaging]
	ADD
	CONSTRAINT [DF__VicTasSta__creat__546180BB]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[VicTasStatusAgentStaging]
	ADD
	CONSTRAINT [DF__VicTasSta__isPro__5555A4F4]
	DEFAULT ((0)) FOR [isProcessed]
GO
ALTER TABLE [dbo].[VicTasStatusAgentStaging] SET (LOCK_ESCALATION = TABLE)
GO
