SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VicTasStatusAgentStaging_SS1908] (
		[SSC]             [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Connum]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Date]            [date] NULL,
		[Time]            [time](7) NULL,
		[Depot]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Status]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[createdDate]     [date] NULL,
		[isProcessed]     [bit] NULL
)
GO
ALTER TABLE [dbo].[VicTasStatusAgentStaging_SS1908] SET (LOCK_ESCALATION = TABLE)
GO
