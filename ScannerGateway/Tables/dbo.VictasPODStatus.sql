SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VictasPODStatus] (
		[FileNo]          [int] IDENTITY(1, 1) NOT NULL,
		[FileName]        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDate]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[VictasPODStatus]
	ADD
	CONSTRAINT [DF__VictasPOD__Creat__058EC7FB]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[VictasPODStatus] SET (LOCK_ESCALATION = TABLE)
GO
