SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WoolWorthsReportSatchelRange] (
		[Sno]       [int] IDENTITY(1, 1) NOT NULL,
		[Label]     [varchar](100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[WoolWorthsReportSatchelRange] SET (LOCK_ESCALATION = TABLE)
GO
