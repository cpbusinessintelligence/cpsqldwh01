SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WoolworthsStaging] (
		[LabelNumber]              [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PUDateTime]               [datetime] NULL,
		[PUBranch]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PUDriver]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[PUETAZone]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TransferDateTime]         [datetime] NULL,
		[TransferBranch]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TransferDriver]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TransferETAZone]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[QueryCageNumber]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[QueryCageDescription]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[OnBoardDateTime]          [datetime] NULL,
		[OnBoardBranch]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[OnBoardDriver]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[OnBoardETAZone]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[HandOverDateTime]         [datetime] NULL,
		[HandOverBranch]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[HandOverDriver]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[HandOverETAZone]          [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[TransferredtoAgent]       [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AttDelDateTime]           [datetime] NULL,
		[AttDelBranch]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AttDelDriver]             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[AttDelETAZone]            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[RedeliveryCard]           [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DelDateTime]              [datetime] NULL,
		[DelBranch]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DelDriver]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[DelETAZone]               [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedBy]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[CreatedDateTime]          [datetime] NULL,
		[EditedBy]                 [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[EditedDateTime]           [datetime] NULL
)
GO
ALTER TABLE [dbo].[WoolworthsStaging]
	ADD
	CONSTRAINT [DF__Woolworth__Creat__66EA454A]
	DEFAULT ('Admin') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[WoolworthsStaging]
	ADD
	CONSTRAINT [DF__Woolworth__Creat__67DE6983]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[WoolworthsStaging]
	ADD
	CONSTRAINT [DF__Woolworth__Edite__68D28DBC]
	DEFAULT ('Admin') FOR [EditedBy]
GO
ALTER TABLE [dbo].[WoolworthsStaging]
	ADD
	CONSTRAINT [DF__Woolworth__Edite__69C6B1F5]
	DEFAULT (getdate()) FOR [EditedDateTime]
GO
ALTER TABLE [dbo].[WoolworthsStaging] SET (LOCK_ESCALATION = TABLE)
GO
