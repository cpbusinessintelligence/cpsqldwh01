SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Workflow_All] (
		[workflow_completion_id]             [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[driver_id]                          [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[driver_ext_ref]                     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[driver_name]                        [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[driver_default_channel_id]          [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[driver_default_channel_ext_ref]     [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[driver_default_channel_name]        [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[time]                               [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[workflow]                           [varchar](5000) COLLATE Latin1_General_CI_AS NULL,
		[is_successful]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[outcome]                            [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[custom_data]                        [varchar](4000) COLLATE Latin1_General_CI_AS NULL,
		[consignment_id]                     [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[article_id]                         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[identifier]                         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[relation]                           [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Workflow_All] SET (LOCK_ESCALATION = TABLE)
GO
