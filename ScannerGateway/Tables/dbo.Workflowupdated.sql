SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Workflowupdated] (
		[workflow_completion_id]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[driver_id]                  [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[driver_ext_ref]             [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[driver_name]                [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[time]                       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[workflow]                   [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[is_successful]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[outcome]                    [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[identifier]                 [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[relation]                   [varchar](500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Workflowupdated] SET (LOCK_ESCALATION = TABLE)
GO
