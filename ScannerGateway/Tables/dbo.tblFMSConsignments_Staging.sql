SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFMSConsignments_Staging] (
		[ID]                        [int] IDENTITY(1, 1) NOT NULL,
		[FMSClient]                 [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[AccountCode]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryNote]              [varchar](300) COLLATE Latin1_General_CI_AS NULL,
		[Status]                    [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[StatusDate]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[EstimatedDeliveryDate]     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[DeliveryDate]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[POD]                       [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[BranchID]                  [varchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Request]                   [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[JSONResponse]              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[IsProcessed]               [bit] NOT NULL,
		[CreatedBy]                 [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
		[CreatedDateTime]           [datetime] NOT NULL,
		[UpdatedBy]                 [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[UpdatedDateTime]           [datetime] NULL,
		CONSTRAINT [PK__tblFMSCo__3214EC27C1018B52]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tblFMSConsignments_Staging]
	ADD
	CONSTRAINT [DF__tblFMSCon__IsPro__40E497F3]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblFMSConsignments_Staging]
	ADD
	CONSTRAINT [DF__tblFMSCon__Creat__41D8BC2C]
	DEFAULT ('TS') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblFMSConsignments_Staging]
	ADD
	CONSTRAINT [DF__tblFMSCon__Creat__42CCE065]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblFMSConsignments_Staging]
	ADD
	CONSTRAINT [DF__tblFMSCon__Updat__43C1049E]
	DEFAULT (getdate()) FOR [UpdatedDateTime]
GO
ALTER TABLE [dbo].[tblFMSConsignments_Staging] SET (LOCK_ESCALATION = TABLE)
GO
