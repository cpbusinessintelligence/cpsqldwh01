SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblWineDeliveryCustomerStatusDeliverAPIResponse] (
		[ID]                 [int] IDENTITY(1, 1) NOT NULL,
		[FullFileName]       [varchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
		[APIResponse]        [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
		[JobRanDateTime]     [datetime] NULL,
		[JobType]            [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblWineDeliveryCustomerStatusDeliverAPIResponse]
	ADD
	CONSTRAINT [DF__tblWineDe__JobRa__560F616C]
	DEFAULT (getdate()) FOR [JobRanDateTime]
GO
ALTER TABLE [dbo].[tblWineDeliveryCustomerStatusDeliverAPIResponse] SET (LOCK_ESCALATION = TABLE)
GO
