SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CrunchTable] (
		[crunchtableId]         [int] IDENTITY(1, 1) NOT NULL,
		[state]                 [varchar](3) COLLATE Latin1_General_CI_AS NULL,
		[postcode]              [varchar](12) COLLATE Latin1_General_CI_AS NULL,
		[suburb]                [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[street_name]           [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[street_type]           [varchar](4) COLLATE Latin1_General_CI_AS NULL,
		[house_number_from]     [int] NULL,
		[house_number_to]       [int] NULL,
		[interval_type]         [varchar](5) COLLATE Latin1_General_CI_AS NULL,
		[building]              [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[destination_id]        [varchar](15) COLLATE Latin1_General_CI_AS NULL,
		[crtd_datetime]         [datetime] NULL,
		[crtd_userid]           [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		[last_upd_datetime]     [datetime] NULL,
		[last_upd_userid]       [varchar](40) COLLATE Latin1_General_CI_AS NULL,
		CONSTRAINT [PK_tbl_CrunchTable_new]
		PRIMARY KEY
		CLUSTERED
		([crunchtableId])
	ON [PRIMARY]
)
GO
ALTER TABLE [dbo].[tbl_CrunchTable] SET (LOCK_ESCALATION = TABLE)
GO
