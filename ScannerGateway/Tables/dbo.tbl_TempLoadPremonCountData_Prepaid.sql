SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempLoadPremonCountData_Prepaid] (
		[CYear]           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[CMonth]          [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Consignment]     [varchar](6000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tbl_TempLoadPremonCountData_Prepaid] SET (LOCK_ESCALATION = TABLE)
GO
