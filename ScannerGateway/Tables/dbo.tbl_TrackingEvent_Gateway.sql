SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_Gateway] (
		[EventTrackingNumber]     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ConsignmentNumber]       [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[StatusDateTime]          [datetime] NULL,
		[ScanEvent]               [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]              [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[ExceptionReason]         [varchar](500) COLLATE Latin1_General_CI_AS NULL,
		[Processed]               [int] NULL,
		[CreatedDate]             [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Gateway]
	ADD
	CONSTRAINT [DF__tbl_Track__Proce__3AF6B473]
	DEFAULT ((0)) FOR [Processed]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Gateway]
	ADD
	CONSTRAINT [DF__tbl_Track__Creat__3BEAD8AC]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Gateway] SET (LOCK_ESCALATION = TABLE)
GO
