SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_CouponVsNowGoScan] (
		[Seq]                            [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Rec. Type]                      [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Trans. Type]                    [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Cons. Type]                     [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Date/Time]                      [varchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Contractor]                     [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Coupon No.]                     [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Link Coupon No.]                [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Book Start Serial No.]          [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Link Book Start Serial No.]     [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Warehouse]                      [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Receiver Pays Coupon No.]       [varchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Account]                        [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Phone]                          [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Invoice No.]                    [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Insurance Code]                 [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Insurance Value]                [varchar](25) COLLATE Latin1_General_CI_AS NULL,
		[Reason]                         [varchar](25) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tmp_CouponVsNowGoScan] SET (LOCK_ESCALATION = TABLE)
GO
