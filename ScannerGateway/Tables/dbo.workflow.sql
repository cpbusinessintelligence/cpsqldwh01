SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[workflow] (
		[workflow_completion_id]             [float] NULL,
		[driver_id]                          [int] NULL,
		[driver_ext_ref]                     [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[driver_name]                        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[driver_default_channel_id]          [int] NULL,
		[driver_default_channel_ext_ref]     [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[driver_default_channel_name]        [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[time]                               [datetime2](7) NULL,
		[workflow]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[is_successful]                      [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[outcome]                            [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[custom_data]                        [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[consignment_id]                     [float] NULL,
		[article_id]                         [float] NULL,
		[identifier]                         [varchar](50) COLLATE Latin1_General_CI_AS NULL,
		[relation]                           [varchar](50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[workflow] SET (LOCK_ESCALATION = TABLE)
GO
