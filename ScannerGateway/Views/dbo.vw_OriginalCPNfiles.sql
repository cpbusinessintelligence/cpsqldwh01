SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create View  vw_OriginalCPNfiles as

Select * from [CPN-Adelaide]
union all
Select * from [CPN-Brisbane]
union all
Select * from [CPN-GoldCoast]
union all
Select * from [CPN-Melbourne]
union all
Select * from [CPN-Perth]
union all
Select * from [CPN-Sydney]
GO
